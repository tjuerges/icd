/*! \mainpage 
\htmlinclude header.html
\n
<H2>
INDEX </H2>
\li \ref one \n
\li \ref two \n
\li \ref three \n
\li \ref four \n
\li \ref five \n
\li \ref six \n
\li \ref seven \n
\li \ref eight \n
\li \ref nine \n
\li \ref ten \n
\li \ref eleven \n
\li \ref twelve \n
\n
<HR>
\section one Purpose
The purpose of the DataCapturer component is to translate
between the telescope domain and the science domain. It acquires metadata from
the control system and the correlator and results from the telescope
calibration sub system and exports data to the telescope calibration subsystem,
QuickLook and the Archive for further use. The incoming data is in the form of
Alma Export Data Format xml- string. The exported data uses the same format.
\n
\n
<HR>
\section two Namespace
For IDL interfaces the <i>alma.offline.<interface_name></i> namespace shall be used. For later implementations, guidelines will be specified later (e.g. <i>alma.offline.<interface_name>.impl</i>). 
\n
\n
<HR>
\section three Interfaces
Only the interfaces used by other ALMA software subsystems are shown in this document.
\n
\subsection subsection1 Interface ComponentLifecycle
All subsystem components implement the lifecycle interface of the container-component model. This interface is part of ACS allowing for the container to start, stop, initialize, update and query the status of the component. The main user of this interface is the array controller of the control system which instantiate one DataCapturer per sub array.
\n
\code interface ComponentLifecycle { \endcode
\code void setComponentName(string instanceName); \endcode
\code // Sets the name that is assigned at deployment time or  \endcode
\code // dynamically by the framework \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code void setContainerServices(ContainerServices service); \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code initialize(); // Initialize the component \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code void execute(); \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code // Called after (@link ComponentLifecycle#initialize) to  \endcode
\code // tell the component that it has to be ready to accept \endcode
\code // incoming functional calls any time. \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code void cleanup(); \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code // Called when component no longer needed \endcode
\code // => Releases resources \endcode
\code string getComponentName(); \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code boolean status(); \endcode
\code // Returns status \endcode
\code // Will return an enum w/ state info in next release \endcode
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
\code void aboutToAbort(); \endcode  \n
 \n
 \c }
\n
\subsection subsection2 Interface DataCapturer
The DataCapturer interface is the interface to fill in the
metadata and retrieve it from the DataCapturer. Control and Correlator use the
add methods to fill in the sampled metadata into the DataCapturer. The addStopExecBlockEntity() method writes the data labels with the execBlockUID into the Archive.
\n
\n
The data is exported to QuickLook subsystem and the TelCal subsystem.
\n
\n
\verbatim
interface DataCapturer : ACS::ACSComponent
  {

    /* Method to add a new DataEntity to the alma data set*/
    /* Input Methods from Control */

                        void addStartExecBlockEntity(in string execBlockUID,
                                 in string antennaTableXML,
                                 in string calDeviceTableXML,
                                 in string corrModeTableXML,
                                 in string configDescriptionTableXML,
                                 in string dataDescriptionTableXML,
                                 in string feedTableXML,
                                 in string fieldTableXML,
                                 in string polarizationTableXML,
                                 in string processorTableXML,
                                 in string receiverTableXML,
                                 in string sourceTableXML,
                                 in string sourceParaTableXML,
                                 in string spectralWindowTableXML,
                                 in string stateTableXML,
                                 in string switchCycleTableXML)
      raises(DataCaptureExceptions::ParsingXMLStringErrorEx);

    void addStopExecBlockEntity()
      raises(DataCaptureExceptions::ParsingXMLStringErrorEx,
             DataCaptureExceptions::WriteToArchiveErrorEx);

    void addStartScanEntity();

    void addStopScanEntity(in string scanSummaryTableXML)
       raises(DataCaptureExceptions::ParsingXMLStringErrorEx);

    void addStartSubScanEntity();

    void addStopSubScanEntity(in string focusTableXML,
                              in string gainTrackingTableXML,
                              in string subscanSummaryTableXML,
                              in string pointingTableXML,
                              in string totPowMonTableXML,
                              in string weatherTableXML)
     raises(DataCaptureExceptions::ParsingXMLStringErrorEx);

    /* Input Methods from Correlator */

  void addStopSubScanData(in asdmIDL::MainTableIDL
            correlatorSubScanMainTable)
     raises(DataCaptureExceptions::ReadingASDMStructErrorEx);

    /* retrieving data  from Memory for use in QuickLook and TelCal */

    ASDMTable getInitialMetaData(in string scanId, in string subscanId)
      raises(DataCaptureExceptions::RetrievingInitialMetaDataErrorEx);

    ASDMTable getFinalMetaData(in string scanId, in string subscanId)
      raises(DataCaptureExceptions::RetrievingFinalMetaDataErrorEx);

    ASDMTable getFinalMetaDataQL(in string scanId)
      raises(DataCaptureExceptions::RetrievingFinalMetaDataErrorEx);

  };
};

\endverbatim

\n
\n
<HR>
\section four Data types and Entities
The DataCapturer uses the infrastructure of the ALMA Science Data Model ASDM classes, which implement the creation of xml based ASDM tables. It uses internally the ASDM types (java objects) to store the metadata a working dataset of tables. For import and export we use CORBA IDL-structs. These build up an ASDM table defined in the DataCapture.idl
\n
\n
\verbatim
struct ASDMTable {
    asdmIDL::AlmaCorrelatorModeTableIDL    almaCorrelatorMode;
    asdmIDL::AntennaTableIDL               antenna;
    asdmIDL::BeamTableIDL                  beam;
    asdmIDL::CalDeviceTableIDL             calDevice;
    asdmIDL::ConfigDescriptionTableIDL     configDescription;
    asdmIDL::DataDescriptionTableIDL       dataDescription;
    asdmIDL::DopplerTableIDL               doppler;
    asdmIDL::EphemerisTableIDL             ephemeris;
    asdmIDL::ExecBlockTableIDL             execBlock;
    asdmIDL::FeedTableIDL                  feed;
    asdmIDL::FieldTableIDL                 field;
    asdmIDL::FlagCmdTableIDL               flagCmd;
    asdmIDL::FocusTableIDL                 focus;
    asdmIDL::FreqOffsetTableIDL            freqOffset;
    asdmIDL::GainTrackingTableIDL          gainTracking;
    asdmIDL::HistoryTableIDL               history;
    asdmIDL::MainTableIDL                  main;
    asdmIDL::ObservationTableIDL           observation;
    asdmIDL::PointingModelTableIDL         pointingModel;
    asdmIDL::PointingTableIDL              pointing;
    asdmIDL::PolarizationTableIDL          polarization;
    asdmIDL::ProcessorTableIDL             processor;
    asdmIDL::ReceiverTableIDL              receiver;
    asdmIDL::SBSummaryTableIDL             sbSummary;
    asdmIDL::ScanTableIDL                  scan;
    asdmIDL::SeeingTableIDL                seeing;
    asdmIDL::SourceParameterTableIDL       sourceParameter;
    asdmIDL::SourceTableIDL                source;
    asdmIDL::SpectralWindowTableIDL        spectralWindow;
    asdmIDL::StateTableIDL                 state;
    asdmIDL::SubscanTableIDL               subscan;
    asdmIDL::SwitchCycleTableIDL           switchCycle;
    asdmIDL::SysCalTableIDL                sysCal;
    asdmIDL::TotalPowerMonitoringTableIDL  totalPowerMonitoring;
    asdmIDL::TransitionTableIDL            transition;
    asdmIDL::WeatherTableIDL               weather;
    asdmIDL::WVMCalTableIDL                wvmCal;
  };
\endverbatim
\n
\n
<HR>
\section five Published Events
This section defines the events that are raised by a subsystem, their data (i.e. transported entities) and the conditions, under which the event is raised. There will be an event for each step in the data acquisition process. The actual results are available to the Control, QuickLook and TelCal Subsystem through the offline interface.
\n
\n
\subsection ss51 ExecBlockProcessedEvent
Published when all the metadata for the entire execBlock is available.
\n
\verbatim
struct ExecBlockProcessedEvent{
    string dataCapturerId;
    string status;
    ExecBlockId processedExecBlockId;
    DCCTime finishedAt;
  };
\endverbatim
\n
\n
\subsection ss52 ScanProcessedEvent
Published, when all the meta data sent by Control, is accumulated in the dataset to be stored in the archive and is ready for export, using the getFinalMetaData(...) method.
\n
\verbatim
struct ScanProcessedEvent{
    string dataCapturerId;
    string status;
    ExecBlockId processedExecBlockId;
    ScanId processedScanId;
    DCCTime finishedAt;
  };
\endverbatim
\n
\n
\subsection ss53 ScanReducedEvent
Published when all the metadata sent generated by the TelCal subsystem was mapped to the accumulated dataset to be stored in the archive. The finalMetaDataSet is updated and is ready for export, using the getFinalMetaData(...) method.
\n
\verbatim
struct ScanReducedEvent{
    string dataCapturerId;
    string status;
    ExecBlockId reducedExecBlockId;
    ScanId reducedScanId;
    DCCTime finishedAt;
  };
\endverbatim
\n
\n
\subsection ss54 SubscanProcessedEvent
Published when all the meta data sent by Control and Correlator was accumulated in the dataset to be stored in the archive and is ready for export, using the getInitialMetaData(...) method.
\n
\verbatim
struct SubScanProcessedEvent{
    string dataCapturerId;
    string status;
    ExecBlockId processedExecBlockId;
    ScanId processedScanId;
    SubScanId processedSubScanId;
    DCCTime finishedAt;
  };
\endverbatim
\n
\n
\subsection ss55 SubScanDataReadyEvent
This is event is published when the Correlator has sent the rows in the main table with all the acquired integrations and sub integrations for this subscan. The event is consumed by Control. Control sends then in return the missing tables for this subscan.
\n
\verbatim
  struct SubScanDataReadyEvent{
    string dataCapturerId;
    string status;
    ExecBlockId receivedExecBlockId;
    ScanId receivedScanId;
    SubScanId receivedSubscanId;
    DCCTime finishedAt;
  };
\endverbatim
\n
\n
<HR>
\section six Received Events
This section defines the events that are received by the DataCapturer Subsystem and how it will react to them. See TelCal Subsystem ICD for the detailed description of these events. \n
\n
\subsection ss61 AmpliCalReducedEvent
Received, whenever an Amplitude Calibration is finished. The execBlockId, scanId and subscanId are retrieved from the event. Then the data is retrieved from the TelCal subsystem using these ids and mapped to the appropriate tables in the working data set.
\n
\n
\subsection ss62 AtmosphereReducedEvent
Received, whenever an Atmosphere Calibration is finished. The execBlockId, scanId and subscanId are retrieved from the event. Then the data is retrieved from the TelCal subsystem using these ids and mapped to the appropriate tables in the working data set.
\n
\n
\subsection ss63 PhaseCalReducedEvent
Received whenever a Phase Calibration is finished. The execBlockId, scanId and subscanId are retrieved from the event. Then the data is retrieved from the TelCal subsystem using these ids and mapped to the appropriate tables in the working data set.
\n
\n
\subsection ss64 SkydipReducedEvent
Received whenever a skydip Calibration was performed and is finished. The execBlockId, scanId and subscanId are retrieved from the event. Then the data is retrieved from the TelCal subsystem using these ids and mapped to the appropriate tables in the working data set.
\n
\n
<HR>
\section seven Consumed Streams
This section defines the streamed that are consumed by the subsystem. At the moment there are no streams consumed by the DataCapturer. All data is transmitted by methods from other subsystems.\n
\n
<HR>
\section eight Required interfaces and subsystems
\subsection ss81 XMLStore (provided by the Archive Subsystem)
The XMLStore interface is used to archive results computed by the TelCal Subsystem. All results are archived as part of the publishing process when the addStopExecBlockEntity() method is called. \n
\n
\subsection ss82 Interface TelCalPublisher (provided by the TelCal subsystem)
The  GetTelCalResults interface is used to get the results from the TelCal subsystem. They are invoked when the appropriate event was received.  After that the results as CORBA-struct ASDM table set the results are mapped to the ASDM tables in memory.
\n
\n
\verbatim
    interface GetTelCalResults {
alma.offline.ASDMtable getPhaseCalResult (ExecBlockId execBlockId, ScanID scanId);
// Phase Calibration results
alma.offline.ASDMtable getAmpliCalResult (ExecBlockId execBlockId, ScanID scanId);
// Amplitude Calibration results
alma.offline.ASDMtable getAtmosphereResult (ExecBlockId execBlockId, ScanID scanId);
// Atmosphere Calibration results
alma.offline.ASDMtable getPathCorrectionResult (ExecBlockId execBlockId, ScanID scanId);
// Path Correction results
alma.offline.ASDMtable getPhaseCurveResult (ExecBlockId execBlockId, ScanID scanId);
// Phase Curve results
alma.offline.ASDMtable getAmpCurveResult (ExecBlockId execBlockId, ScanID scanId);
// Amplitude Curve results
alma.offline.ASDMtable getPointingResult (ExecBlockId execBlockId, ScanID scanId);
// Pointing results
alma.offline.ASDMtable getFocusResult (ExecBlockId execBlockId, ScanID scanId);
// Focus results
alma.offline.ASDMtable getSideBandResult (ExecBlockId execBlockId, ScanID scanId);
// Side Band results
alma.offline.ASDMtable getDelayResult (ExecBlockId execBlockId, ScanID scanId);
// Delay results
alma.offline.ASDMtable getBandPassResult (ExecBlockId execBlockId, ScanID scanId);
// Band Pass results
alma.offline.ASDMtable getSkydipResult (ExecBlockId execBlockId, ScanID scanId);
// Skydip results
alma.offline.ASDMtable getPointingModelResult (ExecBlockId execBlockId, ScanID scanId);
// Pointing Model results
alma.offline.ASDMtable getAntennaPositionResult (ExecBlockId execBlockId, ScanID scanId);
// Antenna position results
alma.offline.ASDMtable getHolographyResult (ExecBlockId execBlockId, ScanID scanId);
// Holography results
alma.offline.ASDMtable getPrimaryBeamResult (ExecBlockId execBlockId, ScanID scanId);
// Primary Beam results
    }
\endverbatim
\n
\n
<HR>
\section nine Dependencies on External Data
None \n
\n
<HR>
\section ten ACS Dependencies
\li Component-container model services
\li Logging
\li Publishing and receiving events
\li Receiving data streams
\n
\n
<HR>
\section eleven References
\li DataCapturer design document
\li Telescope Calibration ICD document
*/
