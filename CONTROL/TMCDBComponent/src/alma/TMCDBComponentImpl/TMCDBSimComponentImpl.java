/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBSimComponentImpl.java
 */
package alma.TMCDBComponentImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.TMCDBComponentOperations;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AntennaPointingModelIDL;
import alma.TMCDB_IDL.AntennaPointingModelTermIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.PointingModelIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupPhotonicReferenceIDL;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.TmcdbSqlEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateKeyEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbInitializationFailureEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.TMCDB.ModelTerm;

//Refactoring:
import java.util.ArrayList;
import java.util.List;

import java.lang.Float;
import java.io.File;
import java.io.IOException;

import org.omg.CORBA.DoubleHolder;

//XML parsers
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;


public class TMCDBSimComponentImpl implements TMCDBComponentOperations, ComponentLifecycle {

    /**
     * The name of this Controller.
     */
    protected String instanceName;

    /**
     * The ACS container services.
     */
    protected ContainerServices container;

    /**
     * The ACS Logger.
     */
    protected Logger logger;

    /** Startup antenna information to be setup from test cases. */
    private StartupAntennaIDL[] testStartupAntennasInfo;

    /** Antenna information to be setup from test cases. */
    private Map<String, AntennaIDL> testAntennaInfo;

    /** Antenna pad information to be setup from test cases. */
    private Map<String, PadIDL> testPadInfo;

    /** Pointing model information to be setup from test cases. */
    private PointingModelIDL testPointingModelInfo;

    /** Central LO startup configuration to be setup from test cases. */        
    private StartupCLOIDL testStartupCentralLOInfo;
    
    // The antenna focus model, for each antenna
    private Map<String, ModelTerm[]> antennaFocusModel = null;
    // The offsets in the focus model for each band
    private Map<Integer, ModelTerm[]> bandFocusModel = null;
        
    // The antenna pointing model, for each antenna
    private Map<String, ModelTerm[]> antennaPointingModel = null;
    // The offsets in the pointing model for each band
    private Map<Integer, ModelTerm[]> bandPointingModel = null;
        
     /** Configuration name */
    private String configurationName;
    
    /**Map AntennaName with current PadName */
    Map<String, String> antennaPad = new HashMap<String, String>();
    
    /**
     * The TMCDBComponentImpl constructor.  This constructor doesn't really do anything.
     * The real work is done in the initialization functions.
     */
    public TMCDBSimComponentImpl() {
        super();
    }

    /**
     * @param container The container to set.
     */
    protected void setContainer(ContainerServices container) {
        this.container = container;
    }

    /**
     * @param logger The logger to set.
     */
    protected void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     *
     * @param name The name of this resource.
     */
    protected void setName(String name) {
        this.instanceName = name;
    }

    //////////////////////////////////////////////////////
    // Lifecycle Methods                                //
    //////////////////////////////////////////////////////

    public void aboutToAbort() {
        cleanUp();
    }

    public void cleanUp() {
    }

    public void execute() throws ComponentLifecycleException {
    }

    public void initialize(ContainerServices cs) throws ComponentLifecycleException {
        if (cs != null) {
            setContainer(cs);
            setName(container.getName());
            setLogger(container.getLogger());
        }

        testAntennaInfo = new HashMap<String, AntennaIDL>();
        testPadInfo = new HashMap<String, PadIDL>();

        // Set the configuration name. This is used by the observing modes to select
        // which tuning solutions to use.
        try {
            String tmcdbConfName = System.getenv("TMCDB_CONFIGURATION_NAME");
            if (tmcdbConfName == null) {
                logger.config("No TMCDB_CONFIGURATION_NAME env. variable has been defined. " +
                        "Using \"Test\".");
                tmcdbConfName = "Test";
            } else
                logger.config("Using TMCDB Configuration Name \""+tmcdbConfName+"\".");
            setConfigurationName(tmcdbConfName);
        } catch (AcsJTmcdbErrTypeEx ex) {
            throw new ComponentLifecycleException(ex);
        }
    }
    
    public void setConfigurationName(String configName)
        throws AcsJTmcdbInitializationFailureEx,
        AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx {
        
        configurationName = configName;
    }
    
    public String getConfigurationName() throws TmcdbErrorEx {
        return configurationName;
    }
    
    /////////////////////////////////////////////////////////////
    // Implementation of ACSComponent
    /////////////////////////////////////////////////////////////

    public ComponentStates componentState() {
    return container.getComponentStateManager().getCurrentState();
    }
    public String name() {
    return container.getName();
    }

    /////////////////////////////////////////////////
    // TMCDB Internal Operations
    /////////////////////////////////////////////////
    
    /**
     * Yet another temporary hack while we finally get the TMCDB fully
     * implemented and working.
     * This function, implemented in the TMCDB base class, is overriden 
     * in order to set the configuration that this component must provide.
     */
    public static StartupAntenna createAntennaStartUp(String antennaName, String
            padName, List<String> deviceList, List<String> feList){
        StartupAntenna ant = new StartupAntenna ();
        ant.setAntennaName(antennaName);
        ant.setPadName(padName);
        if(antennaName.substring(0, 2).equals("DV") || 
           antennaName.substring(0, 2).equals("LA")){
            ant.setUiDisplayOrder((short) Integer.valueOf(antennaName.substring(2, 4)).intValue()  );
        } else if (antennaName.substring(0, 2).equals("DA")){
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()-15));
        } else if (antennaName.substring(0, 2).equals("PM")) {
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+62));
        } else {//CM case
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+50));
        }

        ant.setFrontEndName("none");

        AssemblyLocation[] FeDeviceList = new AssemblyLocation[feList.size()];
        for (int i = 0; i < FeDeviceList.length; ++i) {
            FeDeviceList[i] = new AssemblyLocation ();
            FeDeviceList[i].setAssemblyRoleName("");
            FeDeviceList[i].setAssemblyTypeName("none");
            FeDeviceList[i].setBaseAddress(0);
            FeDeviceList[i].setChannelNumber(0);
            FeDeviceList[i].setRca(0);
        }
        //FeDeviceList[0].setAssemblyRoleName("ColdCart3");
        //FeDeviceList[1].setAssemblyRoleName("ColdCart6");
        //FeDeviceList[2].setAssemblyRoleName("ColdCart7");
        //FeDeviceList[3].setAssemblyRoleName("ColdCart9");
        //FeDeviceList[4].setAssemblyRoleName("Cryostat");
        //FeDeviceList[5].setAssemblyRoleName("IFSwitch");
        //FeDeviceList[6].setAssemblyRoleName("LPR");
        //FeDeviceList[7].setAssemblyRoleName("PowerDist3");
        //FeDeviceList[8].setAssemblyRoleName("PowerDist6");
        //FeDeviceList[9].setAssemblyRoleName("PowerDist7");
        //FeDeviceList[10].setAssemblyRoleName("PowerDist9");
        //FeDeviceList[11].setAssemblyRoleName("WCA3");
        //FeDeviceList[12].setAssemblyRoleName("WCA6");
        //FeDeviceList[13].setAssemblyRoleName("WCA7");
        //FeDeviceList[14].setAssemblyRoleName("WCA9");
        for (int i = 0; i < feList.size(); i++)
            FeDeviceList[i].setAssemblyRoleName(feList.get(i));
        ant.setFrontEndAssembly(FeDeviceList);

        AssemblyLocation[] devices = new AssemblyLocation[deviceList.size()];
        for (int i = 0; i < devices.length; ++i) {
            devices[i] = new AssemblyLocation ();
            devices[i].setAssemblyRoleName("");
            devices[i].setAssemblyTypeName("none");
            devices[i].setBaseAddress(0);
            devices[i].setChannelNumber(0);
            devices[i].setRca(0);
        }

        for (int i=0;i<deviceList.size();i++)
            devices[i].setAssemblyRoleName(deviceList.get(i));

        ant.setAntennaAssembly(devices);
        return ant;

    }

    public void readAntennaPadMap() {
            //Creat antena pad Map
            AntennaPadMapXMLParser mapparser = new AntennaPadMapXMLParser();
            try {
                mapparser.parse();
                antennaPad = mapparser.getMap();
            } catch (Exception e) {
                e.printStackTrace();
                logger.severe("Error while parsing Antena Pad Map file");
            }
    }

    public StartupAntenna[] getStartUpAntennasInfo()  {
            List<StartupAntenna> antennaList = new ArrayList<StartupAntenna>();
           
            //Make sure we have the antenna pad map
            readAntennaPadMap();
            
            //Crean antennas
            AntennaXMLParser antparser = new AntennaXMLParser(antennaPad);
            try {
                antparser.parse();
                antennaList = antparser.getAntennaList();
            } catch (Exception e) {
                e.printStackTrace();
                logger.severe("Error while parsing Antennas file");
            }

            StartupAntenna[] ants = new StartupAntenna[antennaList.size()];
            for(int i=0;i < antennaList.size();i++)
                ants[i] = antennaList.get(i);
                
            return ants;
        }

    ///////////////////////////////
    // TMCDB External Operations //
    ///////////////////////////////

    public StartupAntennaIDL[] getStartupAntennasInfo() {

        if (testStartupAntennasInfo == null) {
            StartupAntenna[] antenna = null;

            antenna = getStartUpAntennasInfo();
            StartupAntennaIDL[] list = new StartupAntennaIDL [antenna.length];
            for (int i = 0; i < list.length; ++i) {
                list[i] = new StartupAntennaIDL();
                list[i].antennaName = antenna[i].getAntennaName();
                list[i].padName = antenna[i].getPadName();
                list[i].frontEndName = antenna[i].getFrontEndName();
                list[i].uiDisplayOrder  = antenna[i].getUiDisplayOrder();
                AssemblyLocation[] loc = antenna[i].getFrontEndAssembly();
                list[i].frontEndAssembly = new AssemblyLocationIDL [loc.length];
                for (int j = 0; j < list[i].frontEndAssembly.length; ++j) {
                    list[i].frontEndAssembly[j] = new AssemblyLocationIDL ();
                    list[i].frontEndAssembly[j].assemblyTypeName = loc[j].getAssemblyTypeName();
                    list[i].frontEndAssembly[j].assemblyRoleName = loc[j].getAssemblyRoleName();
                    list[i].frontEndAssembly[j].rca = loc[j].getRca();
                    list[i].frontEndAssembly[j].channelNumber = loc[j].getChannelNumber();
                    list[i].frontEndAssembly[j].baseAddress = loc[j].getBaseAddress();
                }
                loc = antenna[i].getAntennaAssembly();
                list[i].antennaAssembly = new AssemblyLocationIDL [loc.length];
                for (int j = 0; j < list[i].antennaAssembly.length; ++j) {
                    list[i].antennaAssembly[j] = new AssemblyLocationIDL ();
                    list[i].antennaAssembly[j].assemblyTypeName = loc[j].getAssemblyTypeName();
                    list[i].antennaAssembly[j].assemblyRoleName = loc[j].getAssemblyRoleName();
                    list[i].antennaAssembly[j].rca = loc[j].getRca();
                    list[i].antennaAssembly[j].channelNumber = loc[j].getChannelNumber();
                    list[i].antennaAssembly[j].baseAddress = loc[j].getBaseAddress();
                }
            }
            return list;
        } else {
            return testStartupAntennasInfo;
        }
    }

    public StartupAOSTimingIDL getStartupAOSTimingInfo() throws TmcdbErrorEx {
        AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
        AOSTimingXMLParser parser = new AOSTimingXMLParser();
        try {
            parser.parse();
            assemblies = parser.getAssemblies();
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing AOSTiming file");
        }

        return new StartupAOSTimingIDL(assemblies);
    }

    public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo() throws TmcdbErrorEx {
        AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
        WeatherStationControllerXMLParser parser = new WeatherStationControllerXMLParser();
        try {
            parser.parse();
            assemblies = parser.getAssemblies();
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing WeatherStationController file");
        }

        return new StartupWeatherStationControllerIDL(assemblies);
    }

    public StartupCLOIDL getStartupCLOInfo() throws TmcdbErrorEx {
        if (testStartupCentralLOInfo == null) {
            AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
            StartupPhotonicReferenceIDL[] photonicRef = new StartupPhotonicReferenceIDL[0];
            CLOXMLParser parser = new CLOXMLParser();
            try {
                parser.parse();
                assemblies = parser.getAssemblies();
                photonicRef = parser.getPhoto();
            } catch (Exception e) {
                e.printStackTrace();
                logger.severe("Error while parsing CLO file");
            }
        
                return new StartupCLOIDL(assemblies, photonicRef);
        } else {
                return testStartupCentralLOInfo;
        }
    }
    
    public AntennaIDL getAntennaInfo(String antennaName) throws TmcdbNoSuchRowEx  {
        Antenna antenna = new Antenna ();
        if(antennaName.substring(0, 2).equals("DV") || 
            antennaName.substring(0, 2).equals("DA") ||
            antennaName.substring(0, 2).equals("LA")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("twelveMeter");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2009,2,6,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(7.0));
        } else if (antennaName.substring(0, 2).equals("PM")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("totalPower");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(7.5));
        } else if(antennaName.substring(0, 2).equals("CM")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("sevenMeter");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(0.0));
        }
        antenna.setComponentId(0);
        //antenna.setBaseElementId(2);
        //antenna.setComputerId(0); // TODO: Verify that removal is correct
        //antenna.setConfigurationId(1);
        antenna.setXOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        antenna.setYOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        antenna.setZOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        return antenna.toIDL();
    }

    public PadIDL getCurrentAntennaPadInfo(String antennaName) throws TmcdbNoSuchRowEx  {
            String padName=null;
            PadIDL pad = new PadIDL();
            PadXMLParser parser = new PadXMLParser();
            //make sure we have the antenna pad map
            readAntennaPadMap();
            try{
                padName=antennaPad.get(antennaName);
                //              System.out.println("padName="+ padName);
            }catch (java.lang.NullPointerException exce1){
                logger.severe("Antenna "+antennaName+ " doesn't exist");
                exce1.printStackTrace();
            }
            if(padName == null) {
                //No pad found, maybe we are in testing environment
                pad = testPadInfo.get(antennaName);
                //              System.out.println("padname not found, defaulting to testPadInfo");
                if (pad == null) {
                    AcsJTmcdbNoSuchRowEx ex = new AcsJTmcdbNoSuchRowEx("There is no such antenna as " + antennaName);
                    throw ex.toTmcdbNoSuchRowEx();
                } else {
                    return pad;
                }
            }
            try {
                parser.parse();
                pad = parser.getPadIDL(padName);
            } catch (Exception e) {
                e.printStackTrace();
                logger.severe("Error while parsing Antenna Pad file");
            }
            return pad;
    }

    public PointingModelIDL getPMData(String antennaName) throws AcsJTmcdbNoSuchRowEx {
        PointingModelIDL x = new PointingModelIDL ();
        x.antennaName = antennaName;
       // x.padName = antennaPad.get(antennaName);
        try {
        x.padName = getCurrentAntennaPadInfo(antennaName).PadName;
        } catch ( alma.TmcdbErrType.TmcdbNoSuchRowEx ex){
            throw new AcsJTmcdbNoSuchRowEx(ex);
        } 
        x.pointingModel = new AntennaPointingModelIDL ();
        x.pointingModel.AntennaId = 1;
        x.pointingModel.AsdmUID = "none";
        x.pointingModel.PadId = 1;
        x.pointingModel.PointingModelId = 0;
        alma.hla.runtime.asdm.types.ArrayTime t = new alma.hla.runtime.asdm.types.ArrayTime(2006,10,10,8,0,0.0);
        x.pointingModel.StartTime = t.toIDLArrayTime();
        x.pointingModel.StartValidTime = t.toIDLArrayTime();
        x.pointingModel.EndValidTime = new alma.asdmIDLTypes.IDLArrayTime (0);          
        x.term = new AntennaPointingModelTermIDL [18];
        for (int i = 0; i < x.term.length; ++i) {
            x.term[i] = new AntennaPointingModelTermIDL ();
            x.term[i].PointingModelId = 0;
            x.term[i].CoeffError = 0.0F;
            x.term[i].CoeffValue = 0.0F;
        }
        x.term[0].CoeffName = "IA";
        x.term[1].CoeffName = "IE";
        x.term[2].CoeffName = "HASA";
        x.term[3].CoeffName = "HACA";
        x.term[4].CoeffName = "HESE";
        x.term[5].CoeffName = "HECE";
        x.term[6].CoeffName = "HESA";
        x.term[7].CoeffName = "HASA2";
        x.term[8].CoeffName = "HACA2";
        x.term[9].CoeffName = "HESA2";
        x.term[10].CoeffName = "HECA2";
        x.term[11].CoeffName = "HACA3";
        x.term[12].CoeffName = "HECA3";
        x.term[13].CoeffName = "HESA3";
        x.term[14].CoeffName = "NPAE";
        x.term[15].CoeffName = "CA";
        x.term[16].CoeffName = "AN";
        x.term[17].CoeffName = "AW";
        return x;
    }

    public PointingModelIDL getPointingModelInfo(String antennaName) throws TmcdbNoSuchRowEx {
        if (testPointingModelInfo == null) {
            try {
                return getPMData(antennaName);
            } catch (AcsJTmcdbNoSuchRowEx e) {
                throw e.toTmcdbNoSuchRowEx();
            }
        } else {
            return testPointingModelInfo;
        }
    }

    public PointingModelIDL getRecentPointingModelInfo(String antennaName) throws TmcdbNoSuchRowEx {
        if (testPointingModelInfo == null) {
            try {
                return getPMData(antennaName);
            } catch (AcsJTmcdbNoSuchRowEx e) {
                throw e.toTmcdbNoSuchRowEx();
            }
        } else {
            return testPointingModelInfo;
        }
    }

    public PointingModelIDL[] getPointingModelsInfo(String antennaName) throws TmcdbNoSuchRowEx {
        PointingModelIDL[] x = new PointingModelIDL [1];
        try {
            x[0] = getPMData(antennaName);
            return x;
        }catch (AcsJTmcdbNoSuchRowEx e) {
            AcsJTmcdbNoSuchRowEx ex = new AcsJTmcdbNoSuchRowEx("There is no such antenna as " + antennaName);
            throw ex.toTmcdbNoSuchRowEx();
        }
    }

    /**
     * Sets up the startup antennas information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    public void setStartupAntennasInfo(StartupAntennaIDL[] sai) {
        logger.info("Setting startup antennas information of length " + sai.length);
        testStartupAntennasInfo = sai;
    }

    /**
     * Sets up the antennas information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    public void setAntennaInfo(String an, AntennaIDL ai) {
        testAntennaInfo.put(an, ai);
    }

    /**
     * Sets up the antenna pads information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    public void setAntennaPadInfo(String an, PadIDL api) {
        testPadInfo.put(an, api);
    }

    /**
     * Sets up the pointing model data. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    public void setPointingModelData(PointingModelIDL pm) {
        testPointingModelInfo = pm;
    }

    /**
     * Sets up the central lo startup configuration. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */     
    @Override
    public void setStartupCLOInfo(StartupCLOIDL clo) {
        testStartupCentralLOInfo = clo;
            
    }
    
    public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber) throws TmcdbSqlEx, TmcdbNoSuchRowEx {
        AssemblyConfigXMLData data = new AssemblyConfigXMLData();
        data.xmlDoc = "";
        data.schema = "";
        return data;
    }

    public AssemblyConfigXMLData getComponentConfigData(String componentName) throws TmcdbSqlEx, TmcdbNoSuchRowEx {
        return null;
    }

    @Override
    public double[] getMetrologyCoefficients(String antennaName) {
        double[] coeffs = new double[2];
        coeffs[0] = 0.0;
        coeffs[1] = 0.0;
        PadXMLParser parser = new PadXMLParser();
        //make sure we have the antenna pad map
        String padName;
        readAntennaPadMap();
        try{
            padName=antennaPad.get(antennaName);
	    System.out.println("padName="+ padName);
        }catch (java.lang.NullPointerException exce1){
            exce1.printStackTrace();
            logger.severe("Antenna "+antennaName+ " doesn't exist");
            return coeffs;
        }
        if (padName == null) {
           logger.severe("Metrology Coefficients not found, returning 0s");
           return coeffs;
        }
        try {
            parser.parse();
            coeffs = parser.getCoeffs(padName);
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing pad file");
        }

        return coeffs;
    }

    @Override
    public void getCurrentAntennaDelays(String antennaName,
            DoubleHolder antennaDelayAvgCoeff, DoubleHolder padDelayAvgCoeff) {
        antennaDelayAvgCoeff.value = 0.0;
        padDelayAvgCoeff.value = 0.0;
        PadXMLParser parser = new PadXMLParser();
        //make sure we have the antenna pad map
        String padName;
        readAntennaPadMap();
        try{
            padName=antennaPad.get(antennaName);
	    System.out.println("padName="+ padName);
        }catch (java.lang.NullPointerException exce1){
            exce1.printStackTrace();
            logger.severe("Antenna "+antennaName+ " doesn't exist");
            return;
        }
        if (padName == null) {
           logger.severe("Pad delay not found, returning 0");
           return;
        }
        try {
            parser.parse();
            padDelayAvgCoeff.value = parser.getDelay(padName);
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing pad file");
        }
    }

    @Override
    public ArrayReferenceLocation getArrayReferenceLocation() {
        ArrayReferenceLocation loc = null;
        ArrayReferenceXMLParser parser = new ArrayReferenceXMLParser();
        try {
            parser.parse();
            loc = parser.getReference();
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing Array Reference file");
        }
        if(loc == null){
            loc = new ArrayReferenceLocation();
            loc.x = 2202175.078;
            loc.y = -5445230.603;
            loc.z = -2485310.452;
        }
        return loc;
    }
    
   static public boolean isAntennaNameValid(String antennaName) {
        if (antennaName.length() != 4) {
            return false;
        }
        final String prefix = antennaName.substring(0, 2).toUpperCase();
        short number;
        try {
            number = new Integer(antennaName.substring(2, 4)).shortValue();
        } catch (NumberFormatException ex) {
            return false;
        }

        if ((prefix.equals("DV") && number >= 1 && number <= 25)
                || (prefix.equals("DA") && number >= 41 && number <= 65)
                || (prefix.equals("PM") && number >= 1 && number <= 4)
                || (prefix.equals("CM") && number >= 1 && number <= 12)) {
            return true;
        }
        return false;
    }

    public ModelTerm[] getCurrentAntennaFocusModel(String antennaName)
            throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        if (!TMCDBSimComponentImpl.isAntennaNameValid(antennaName)) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Antenna '" + antennaName
                    + "' is not a recognized antenna name.");
            jex.log(logger);
            throw jex.toTmcdbNoSuchRowEx();
        }

        // Always reload the focus model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the focus model has
        // changed.
        antennaFocusModel = null;
        bandFocusModel = null;

        if (antennaFocusModel == null) {
            FocusModelXMLParser parser = new FocusModelXMLParser(logger);
            parser.TMCDBParse();
            antennaFocusModel = parser.getAntennaFocusModel();
            bandFocusModel = parser.getBandFocusModel();
        }
        final String upCaseName = antennaName.toUpperCase();
        if (antennaFocusModel.containsKey(upCaseName)) {
            return antennaFocusModel.get(upCaseName);
        } else {
            return new ModelTerm[0];
        }
    }

    public ModelTerm[] getCurrentBandFocusModel(short band,
            boolean for12MAntenna) throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        // TODO. Work out how to support the 7m antennas.
        // Make sure its a valid band name
        if (band < 1 || band > 10) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Band numbers must be between 1 and 10."
                    + " Band " + band + " is not allowed.");
            jex.log(logger);
            throw jex.toTmcdbNoSuchRowEx();
        }

        // Always reload the focus model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the focus model has
        // changed.
        antennaFocusModel = null;
        bandFocusModel = null;

        if (bandFocusModel == null) {
            FocusModelXMLParser parser = new FocusModelXMLParser(logger);
            parser.TMCDBParse();
            antennaFocusModel = parser.getAntennaFocusModel();
            bandFocusModel = parser.getBandFocusModel();
        }
        final Integer bandNum = (int) band;
        if (bandFocusModel.containsKey(bandNum)) {
            return bandFocusModel.get(bandNum);
        } else {
            return new ModelTerm[0];
        }
    }

    public ModelTerm[] getCurrentAntennaPointingModel(String antennaName)
            throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        if (!TMCDBSimComponentImpl.isAntennaNameValid(antennaName)) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Antenna '" + antennaName
                    + "' is not a recognized antenna name.");
            jex.log(logger);
            throw jex.toTmcdbNoSuchRowEx();
        }

        // Always reload the pointing model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the pointing model has
        // changed.
        antennaPointingModel = null;
        bandPointingModel = null;

        if (antennaPointingModel == null) {
            PointingModelXMLParser parser = new PointingModelXMLParser(logger);
            parser.TMCDBParse();
            antennaPointingModel = parser.getAntennaPointingModel();
            bandPointingModel = parser.getBandPointingModel();
        }
        final String upCaseName = antennaName.toUpperCase();
        if (antennaPointingModel.containsKey(upCaseName)) {
            return antennaPointingModel.get(upCaseName);
        } else {
            return new ModelTerm[0];
        }
    }

    public ModelTerm[] getCurrentBandPointingModel(short band,
            boolean for12MAntenna) throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        // TODO. Work out how to support the 7m antennas.
        // Make sure its a valid band name
        if (band < 0 || band > 10) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Band numbers must be between 0 and 10."
                    + " Band " + band + " is not allowed.");
            jex.log(logger);
            throw jex.toTmcdbNoSuchRowEx();
        }

        // Always reload the pointing model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the pointing model has
        // changed.
        antennaPointingModel = null;
        bandPointingModel = null;

        if (bandPointingModel == null) {
            PointingModelXMLParser parser = new PointingModelXMLParser(logger);
            parser.TMCDBParse();
            antennaPointingModel = parser.getAntennaPointingModel();
            bandPointingModel = parser.getBandPointingModel();
        }
        final Integer bandNum = (int) band;
        if (bandPointingModel.containsKey(bandNum)) {
            return bandPointingModel.get(bandNum);
        } else {
            return new ModelTerm[0];
        }
     }
}


///////////////////////////////
//  XML parsing Operations   //
///////////////////////////////
abstract class GenereicXmlParser extends DefaultHandler {

    String filename;
    public GenereicXmlParser(String infilename) {
        filename = infilename;
    }
    
    public void parse() throws SAXException,
    	ParserConfigurationException,IOException {
    	//get a factory
    	SAXParserFactory spf = SAXParserFactory.newInstance();
    	String acsCdb = java.lang.System.getenv("ACS_CDB");
    	String acsRoot = java.lang.System.getenv("ACSROOT");
        String scienceDir = "/groups/science/PadData/";
        String location = java.lang.System.getenv("LOCATION");
        //get a new instance of parser
        SAXParser sp = spf.newSAXParser();
                
        File file;
        file = new File(scienceDir + filename+ "-"+location+".xml");
        if(!file.exists()) {
            file = new File(scienceDir + filename+".xml");
            if(!file.exists()) {
                file = new File(acsRoot + "/config/SIMTMCDB/"+filename+"-"+location+".xml");
                if(!file.exists()) {
                    file = new File(acsRoot + "/config/SIMTMCDB/"+filename+".xml");
                    if (!file.exists()) {
                        String trueAcsCdbDir = java.lang.System.getenv("ACS_CDB");
                        if (acsCdb != null) {
                            String fn = acsCdb + "/SIMTMCDB/" + filename + ".xml";                    
                            File f = new File(fn);
                            if(!f.exists()) {
                                throw new IOException("File " + fn + " not found");
                            }
                            //parse the file and also register this class for call backs
                            sp.parse(f, this);
                            return;
                        }
                
                    }
                }
             }
        }
        //parse the file and also register this class for call backs
        System.out.print("Using: " +file.getPath()+ "/" + file.getName()+"\n");
        sp.parse(file, this);
    }
    //Event Handlers
    public abstract void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException;
    
    public abstract void characters(char[] ch, int start, int length) 
        throws SAXException;
    
    public abstract void endElement(String uri, String localName, String qName) 
        throws SAXException;
}


class AntennaXMLParser extends GenereicXmlParser {

    List<StartupAntenna> antennaList;
    List<String> deviceList; 
    List<String> feList; 
    String antennaName;
    String tempVal;
    Map<String, String>  antennaPad;

    public AntennaXMLParser(Map<String, String> map) {
        super("StartupAntenna");
        antennaList = new ArrayList<StartupAntenna>();
        antennaPad = map;
    }

    public List<StartupAntenna> getAntennaList() {
        return antennaList;
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        //reset
        tempVal = "";
        if(qName.equalsIgnoreCase("Antenna")) {
                deviceList = new ArrayList<String>();
                feList = new ArrayList<String>();
                antennaName = attributes.getValue("name");
        }
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Antenna")) {
                antennaList.add(TMCDBSimComponentImpl.createAntennaStartUp(antennaName,
                                        antennaPad.get(antennaName), deviceList, feList));
        } else if(qName.equalsIgnoreCase("Assembly")) {
                deviceList.add(tempVal);
        } else if(qName.equalsIgnoreCase("FEAssembly")) {
                feList.add(tempVal);
        }
    }
}


class AntennaPadMapXMLParser extends GenereicXmlParser {
    Map<String, String> antennaPad = new HashMap<String, String>();
    String tempVal;
    String antennaName;
    String padName;

    public AntennaPadMapXMLParser() {
        super("AntennaPadMap");
    }

    public Map<String, String> getMap() {
        return antennaPad;
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        //reset
        tempVal = "";
        if(qName.equalsIgnoreCase("Map")) {
                antennaName = attributes.getValue("antenna");
                padName = attributes.getValue("pad");
        }
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Map")) {
            antennaPad.put(antennaName, padName);
        }
    }
}


class PadXMLParser extends GenereicXmlParser {
   
    Map<String, Pad> padList;
    Map<String, Double> delayList;
    Map<String, double[]> coeffsList;
    String padName;
    String x;
    String y;
    String z;
    String delay;
    String an0;
    String aw0;
    
    String tempVal;
    

    public PadXMLParser() {
        super("Pad");
        padList = new HashMap<String, Pad>();
        delayList = new HashMap<String, Double>();
        coeffsList = new HashMap<String, double[]>();
    }

    public PadIDL getPadIDL(String padName) {
        return padList.get(padName).toIDL();
    }

    public double[] getCoeffs(String padName) {
        return coeffsList.get(padName);
    }

    public double getDelay(String padName) {
        return delayList.get(padName).doubleValue();
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        //reset
        tempVal = "";
        if(qName.equalsIgnoreCase("Pad")) {
            padName = attributes.getValue("name");
            x = attributes.getValue("x");
            y = attributes.getValue("y");
            z = attributes.getValue("z");

            delay = attributes.getValue("delay");
            if (delay == null)
                delay = new String("0");
            an0 = attributes.getValue("an0");
            if (an0 == null)
                an0 = "0";
            aw0 = attributes.getValue("aw0");
            if (aw0 == null)
                aw0 = "0";
    	}
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Pad")) {
            Pad pad = new Pad();
            pad.setPadName(padName);
            pad.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
            pad.setXPosition(new alma.hla.runtime.asdm.types.Length(new Double(x).doubleValue()));
            pad.setYPosition(new alma.hla.runtime.asdm.types.Length(new Double(y).doubleValue()));
            pad.setZPosition(new alma.hla.runtime.asdm.types.Length(new Double(z).doubleValue()));
            padList.put(padName, pad);

            delayList.put(padName, new Double(delay));
            double tcoeffs[] = new double[2];
            tcoeffs[0] = new Double(an0).doubleValue();
            tcoeffs[1] = new Double(aw0).doubleValue();
            coeffsList.put(padName, tcoeffs);
    	}
    }
}


class AOSTimingXMLParser extends GenereicXmlParser {

    List<AssemblyLocationIDL> assemblyList;
    String tempVal;

    public AOSTimingXMLParser() {
        super("AOSTiming");
        assemblyList = new ArrayList<AssemblyLocationIDL>();
    }

    public AssemblyLocationIDL[] getAssemblies() {
        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        tempVal = "";
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Assembly")) {
                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
                assembly.assemblyRoleName = tempVal; 
                assembly.assemblyTypeName = "none";
                assembly.baseAddress = 0;
                assembly.channelNumber = 0;
                assembly.rca = 0;
                assemblyList.add(assembly);
        }
    }
}


class CLOXMLParser extends GenereicXmlParser {

    List<AssemblyLocationIDL> assemblyList; 
    List<StartupPhotonicReferenceIDL> photoList;
    List<AssemblyLocationIDL> photoAssemblyList;
    String tempVal;
    StartupPhotonicReferenceIDL tempPhoto;

    public CLOXMLParser() {
        super("CentralLO");
        assemblyList = new ArrayList<AssemblyLocationIDL>();
        photoList = new ArrayList<StartupPhotonicReferenceIDL>();
    }

    public AssemblyLocationIDL[] getAssemblies() {
        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
    }

    public StartupPhotonicReferenceIDL[] getPhoto() {
        return ( StartupPhotonicReferenceIDL[] )photoList.toArray( new StartupPhotonicReferenceIDL[ photoList.size() ] );
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        tempVal = "";
        if(qName.equalsIgnoreCase("PhotonicReference")) {
            tempPhoto = new StartupPhotonicReferenceIDL();
            photoAssemblyList = new ArrayList<AssemblyLocationIDL>();
            tempPhoto.name = attributes.getValue("name");
        }
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Assembly")) {
                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
                assembly.assemblyRoleName = tempVal; 
                assembly.assemblyTypeName = "none";
                assembly.baseAddress = 0;
                assembly.channelNumber = 0;
                assembly.rca = 0;
                if (tempPhoto == null) {
                    assemblyList.add(assembly);
                } else {
                    photoAssemblyList.add(assembly);
                }
        } else if (qName.equalsIgnoreCase("PhotonicReference")) {
                tempPhoto.assemblies = ( AssemblyLocationIDL[] )photoAssemblyList.toArray( new AssemblyLocationIDL[ photoAssemblyList.size() ] );
                photoList.add(tempPhoto);
                tempPhoto = null;
        }
    }
}


class WeatherStationControllerXMLParser extends GenereicXmlParser {

    List<AssemblyLocationIDL> assemblyList;
    String tempVal;

    public WeatherStationControllerXMLParser() {
        super("WeatherStationController");
        assemblyList = new ArrayList<AssemblyLocationIDL>();
    }

    public AssemblyLocationIDL[] getAssemblies() {
        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
    }

    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        tempVal = "";
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("Assembly")) {
                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
                assembly.assemblyRoleName = tempVal; 
                assembly.assemblyTypeName = "none";
                assembly.baseAddress = 0;
                assembly.channelNumber = 0;
                assembly.rca = 0;
                assemblyList.add(assembly);
        }
    }
}


class ArrayReferenceXMLParser extends GenereicXmlParser {

    String tempVal;
    ArrayReferenceLocation loc;
    String x;
    String y;
    String z;

    public ArrayReferenceXMLParser() {
        super("ArrayReference");
    }

    public ArrayReferenceLocation getReference() {
        return loc;
    }


    public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
        tempVal = "";
        if(qName.equalsIgnoreCase("ArrayReference")) {
            x = attributes.getValue("x");
            y = attributes.getValue("y");
            z = attributes.getValue("z");
        }
    }
    
    
    public void characters(char[] ch, int start, int length) 
        throws SAXException {
        tempVal = new String(ch,start,length);
    }
    
    public void endElement(String uri, String localName, String qName) 
        throws SAXException {
        if(qName.equalsIgnoreCase("ArrayReference")) {
            loc = new ArrayReferenceLocation();
            loc.x = new Float(x).floatValue();
            loc.y = new Float(y).floatValue();
            loc.z = new Float(z).floatValue();
        }
    }
}


class ModelXMLParser extends GenereicXmlParser {
    Logger logger;
    // The current antenna or band we are parsing. only one of these
    // should be non-nill at any one time.
    String curAntenna;
    Integer curBand;
    // The accumulated terms in the antenna or band we are currently parsing
    ArrayList<ModelTerm> curModel;

    // Once we have completed parsing an antenna or band the data is
    // moved from the cur* member variables )above into either the
    // antModel or bandModel maps (below).
    Map<String, ModelTerm[]> antModel = new HashMap<String, ModelTerm[]>();
    Map<Integer, ModelTerm[]> bandModel = new HashMap<Integer, ModelTerm[]>();
// TODO. Add support for a separate set of band offsets in the 7m antennas. 
// Whats commented out below is a partial implementation
//    Map<Integer, ModelTerm[]> bandModel7m = new HashMap<Integer, ModelTerm[]>();

    // This string is just used in error messages
    String filename;

    public ModelXMLParser(String filename, Logger logger) {
        super(filename);
        this.filename = filename + ".xml file";
        this.logger = logger;
    }

    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        try {
            if (qName.equalsIgnoreCase("Antenna")) {
                curAntenna = attributes.getValue("name");
                curModel = new ArrayList<ModelTerm>();
            } else if (qName.equalsIgnoreCase("BandOffset")) {
                curBand = Integer.valueOf(attributes.getValue("name"));
                curModel = new ArrayList<ModelTerm>();
//          } else if (qName.equalsIgnoreCase("BandOffset7m")) {
//                 curBand = Integer.valueOf(attributes.getValue("name"));
//                 curModel = new ArrayList<ModelTerm>();
            } else if (qName.equalsIgnoreCase("Term")) {
                String termName = attributes.getValue("name");
                double termValue = Double.valueOf(attributes.getValue("value")).doubleValue();
                if (curModel == null) {
                    final String msg = filename + " is incorrectly structured.";
                    throw new SAXException(msg);
                }
                curModel.add(new ModelTerm(termName, termValue));
            }
        } catch (NumberFormatException ex) {
            final String msg = filename + " contains incorrect numbers.";
            throw new SAXException(msg, ex);
        }
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (qName.equalsIgnoreCase("Antenna")) {
            if (curModel == null) {
                String msg = filename + " is incorrectly structured..";
                throw new SAXException(msg);
            }
            antModel.put(curAntenna, curModel.toArray(new ModelTerm[0]));
        } else if (qName.equalsIgnoreCase("BandOffset")) {
            if (curModel == null) {
                String msg = filename + " is incorrectly structured...";
                throw new SAXException(msg);
            }
            bandModel.put(curBand, curModel.toArray(new ModelTerm[0]));
//         } else if (qName.equalsIgnoreCase("BandOffset7m")) {
//             if (curModel == null) {
//                 String msg = filename + " is incorrectly structured...";
//                 throw new SAXException(msg);
//             }
//             bandModel7m.put(curBand, curModel.toArray(new ModelTerm[0]));
        }
    }

    public void TMCDBParse() throws TmcdbErrorEx {
        try {
            super.parse();
        } catch (Exception ex) {
            AcsJTmcdbErrorEx jex = new AcsJTmcdbErrorEx(ex);
            jex.log(logger);
            throw jex.toTmcdbErrorEx();
        }
    }

    protected Map<String, ModelTerm[]> getAntennaModel() {
        return antModel;
    }

    protected Map<Integer, ModelTerm[]> getBandModel() {
        return bandModel;
    }

//     protected Map<Integer, ModelTerm[]> getBandModel7m() {
//         return bandModel7m;
//     }
}


class FocusModelXMLParser extends ModelXMLParser {
    public FocusModelXMLParser(Logger logger) {
        super("FocusModel", logger);
    }
    
    public Map<String, ModelTerm[]> getAntennaFocusModel() {
        return getAntennaModel();
    }

    public Map<Integer, ModelTerm[]> getBandFocusModel() {
        return getBandModel();
    }
}


class PointingModelXMLParser extends ModelXMLParser {
    public PointingModelXMLParser(Logger logger) {
        super("PointingModel", logger);
    }
    
    public Map<String, ModelTerm[]> getAntennaPointingModel() {
        return getAntennaModel();
    }

    public Map<Integer, ModelTerm[]> getBandPointingModel() {
        return getBandModel();
    }
}
