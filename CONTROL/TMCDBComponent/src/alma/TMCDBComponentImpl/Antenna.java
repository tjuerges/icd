/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * /////////////////////////////////////////////////////////////////
 * // WARNING!  DO NOT MODIFY THIS FILE!                          //
 * //  ---------------------------------------------------------  //
 * // | This is generated code!  Do not modify this file.       | //
 * // | Any changes will be lost when the file is re-generated. | //
 * //  ---------------------------------------------------------  //
 * /////////////////////////////////////////////////////////////////
 *
 * File Antenna.java
 */
package alma.TMCDBComponentImpl;

import alma.hla.runtime.asdm.types.ArrayTime;

import alma.hla.runtime.asdm.types.Length;

import alma.TMCDB_IDL.AntennaIDL;

/**

 * The Antenna table represents the general properties of an ALMA antenna.

 * The x-y-z position is the position from the pad position to the point of rotation of the antenna.  The x-y-z offset is the offset, if any, from that position to the point from which the feeds offsets are measured.

 * Included is the name of the software component that executes the antenna.

   * Key: BaseElementId

 *
 */
public class Antenna implements java.io.Serializable {
    static private final String newline = System.getProperty("line.separator");

    private int BaseElementId;

    private String AntennaName;

    // private boolean nullAntennaName;

    private String AntennaType;

    private Length DishDiameter;

    private ArrayTime CommissionDate;

    private Length XPosition;

    private Length YPosition;

    private Length ZPosition;

    private Length XOffset;

    private Length YOffset;

    private Length ZOffset;

    private int ComponentId;

    /**
     * Default Constructor for Antenna.  Setter methods must be used to insert data.
     */
    public Antenna () {

        // nullAntennaName = true;

    }

    /**
     * Create a Antenna by specifiying all data values.
     */
    public Antenna (

        int BaseElementId,

        String AntennaName,

        String AntennaType,

        Length DishDiameter,

        ArrayTime CommissionDate,

        Length XPosition,

        Length YPosition,

        Length ZPosition,

        Length XOffset,

        Length YOffset,

        Length ZOffset,

        int ComponentId

    ) {

		setBaseElementId(BaseElementId);

		setAntennaName(AntennaName);

		setAntennaType(AntennaType);

		setDishDiameter(DishDiameter);

		setCommissionDate(CommissionDate);

		setXPosition(XPosition);

		setYPosition(YPosition);

		setZPosition(ZPosition);

		setXOffset(XOffset);

		setYOffset(YOffset);

		setZOffset(ZOffset);

		setComponentId(ComponentId);

    }

    /**
     * Create a Antenna by specifiying data values as an array of strings.
     */
    public Antenna (String[] data) {
    	if (data.length != 12)
    		throw new IllegalArgumentException("Wrong number of items in the data array!  (" + data.length + " are specified; should be 12)");
    	int i = 0;

	  try {

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.BaseElementId = new Integer(Integer.parseInt(data[i]));

		}

	  } catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid number format: (" + data[i] + ").");
	  }

		++i;

		if (data[i] == null || data[i].length() == 0) {

			// nullAntennaName = true;
			// this.AntennaName = null;

		} else {

			// nullAntennaName = false;

			this.AntennaName = data[i];

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.AntennaType = data[i];

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.DishDiameter = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.CommissionDate = new ArrayTime(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.XPosition = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.YPosition = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.ZPosition = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.XOffset = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.YOffset = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.ZOffset = new Length(data[i]);

		}

		++i;

	  try {

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.ComponentId = new Integer(Integer.parseInt(data[i]));

		}

	  } catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid number format: (" + data[i] + ").");
	  }

		++i;

    }

    /**
     * Display the values of this object.
     */
    public String toString() {
    	String s =  "Antenna:" + newline;

        s += "\tBaseElementId: " + BaseElementId + newline;

    	// if (AntennaName == null)
    	//	s += "\tAntennaName: null" + newline;
    	// else

        s += "\tAntennaName: " + AntennaName + newline;

        s += "\tAntennaType: " + AntennaType + newline;

        s += "\tDishDiameter: " + DishDiameter + newline;

        s += "\tCommissionDate: " + CommissionDate.toFITS() + newline;

        s += "\tXPosition: " + XPosition + newline;

        s += "\tYPosition: " + YPosition + newline;

        s += "\tZPosition: " + ZPosition + newline;

        s += "\tXOffset: " + XOffset + newline;

        s += "\tYOffset: " + YOffset + newline;

        s += "\tZOffset: " + ZOffset + newline;

        s += "\tComponentId: " + ComponentId + newline;

    	return s;
    }

    /**
     * Create a string in the "unload" format.
     */
    public String toString(String delimiter) {
    	String s =  "Antenna" + delimiter;  

        s += BaseElementId + delimiter;

        // if (nullAntennaName)
        // 	s += delimiter;
        // else

        	// s += AntennaName + delimiter;

        s += AntennaType + delimiter;

        s += DishDiameter + delimiter;

		s += new String(CommissionDate.toFITS()) + delimiter;

        s += XPosition + delimiter;

        s += YPosition + delimiter;

        s += ZPosition + delimiter;

        s += XOffset + delimiter;

        s += YOffset + delimiter;

        s += ZOffset + delimiter;

        s += ComponentId + delimiter;

    	return s;
    }

    /**
     * Return the number of columns in the table.
     */
    public static int getNumberColumns() {
    	return 12;
    }

    /**
     * Create a string with the column names in the "unload" format.
     */
    public static String getColumnNames(String delimiter) {
    	String s =  "#Antenna" + delimiter  

        	+ "BaseElementId" + delimiter

        	+ "AntennaName" + delimiter

        	+ "AntennaType" + delimiter

        	+ "DishDiameter" + delimiter

        	+ "CommissionDate" + delimiter

        	+ "XPosition" + delimiter

        	+ "YPosition" + delimiter

        	+ "ZPosition" + delimiter

        	+ "XOffset" + delimiter

        	+ "YOffset" + delimiter

        	+ "ZOffset" + delimiter

        	+ "ComponentId" + delimiter

			;
    	return s;
    }

    /**
     * Create a string with the column names in the "unload" format.
     */
    public String getTheColumnNames(String delimiter) {
    	return getColumnNames(delimiter);
    }

    /**
     * Compare this oblect with another object of the same type.
     */
    public boolean equals(Object obj) {
    	if (obj == null) return false;
    	if (!(obj instanceof Antenna)) return false;
    	Antenna arg = (Antenna) obj;

		if (this.BaseElementId != arg.BaseElementId)
			return false;

		if (this.AntennaName == null) {	// Two null strings are equal
			if (arg.AntennaName == null)
				return true;
			else
				return false;
		}
		if (!this.AntennaName.equals(arg.AntennaName))
			return false; 

		if (this.AntennaType == null) {	// Two null strings are equal
			if (arg.AntennaType == null)
				return true;
			else
				return false;
		}
		if (!this.AntennaType.equals(arg.AntennaType))
			return false; 

		if (this.DishDiameter.get() != arg.DishDiameter.get())
			return false;

		if (this.CommissionDate.get() != arg.CommissionDate.get())
			return false;

		if (this.XPosition.get() != arg.XPosition.get())
			return false;

		if (this.YPosition.get() != arg.YPosition.get())
			return false;

		if (this.ZPosition.get() != arg.ZPosition.get())
			return false;

		if (this.XOffset.get() != arg.XOffset.get())
			return false;

		if (this.YOffset.get() != arg.YOffset.get())
			return false;

		if (this.ZOffset.get() != arg.ZOffset.get())
			return false;

		if (this.ComponentId != arg.ComponentId)
			return false;

		return true;   
    }

    /**
     * Convert this object to its IDL format.
     */
    public AntennaIDL toIDL() {
    	AntennaIDL x = new AntennaIDL ();

		x.BaseElementId = this.BaseElementId;

		x.AntennaName = this.AntennaName;

        // x.nullAntennaName = this.nullAntennaName;

		x.AntennaType = this.AntennaType;

		x.DishDiameter = this.DishDiameter.toIDLLength();

		x.CommissionDate = this.CommissionDate.toIDLArrayTime();

		x.XPosition = this.XPosition.toIDLLength();

		x.YPosition = this.YPosition.toIDLLength();

		x.ZPosition = this.ZPosition.toIDLLength();

		x.XOffset = this.XOffset.toIDLLength();

		x.YOffset = this.YOffset.toIDLLength();

		x.ZOffset = this.ZOffset.toIDLLength();

		x.ComponentId = this.ComponentId;

    	return x;
    }

    /**
     *  Populate this object from an IDL format.
     */
    public void fromIDL(AntennaIDL x) {

		this.BaseElementId = x.BaseElementId;

		this.AntennaName = x.AntennaName;

        // this.nullAntennaName = x.nullAntennaName;

		this.AntennaType = x.AntennaType;

		this.DishDiameter = new Length(x.DishDiameter);

		this.CommissionDate = new ArrayTime(x.CommissionDate);

		this.XPosition = new Length(x.XPosition);

		this.YPosition = new Length(x.YPosition);

		this.ZPosition = new Length(x.ZPosition);

		this.XOffset = new Length(x.XOffset);

		this.YOffset = new Length(x.YOffset);

		this.ZOffset = new Length(x.ZOffset);

		this.ComponentId = x.ComponentId;

    }

    /*
     * If this is a database entry has a generated key, return the value
     * of its generated id; otherwise, return 0.
     */
    public int getId() {

    	return 0;

    }

    /////////////////////////////////////////////////////////////
    // Getter and Setter Methods for Antenna.
    /////////////////////////////////////////////////////////////

    /**
     * Get the value for BaseElementId.
     */
    public int getBaseElementId () {
        return BaseElementId;
    }

    /**
     * Set BaseElementId to the specified value.
     */
    public void setBaseElementId(int BaseElementId) {

        this.BaseElementId = BaseElementId;

    }

    /**
     * Get the value for AntennaName.
     */
    public String getAntennaName () {
        return AntennaName;
    }

    /**
     * Set AntennaName to the specified value.
     */
    public void setAntennaName(String AntennaName) {

        // nullAntennaName = false;

        this.AntennaName = AntennaName;

    }

    /*
     * Is the AntennaName null?
     */
    // public boolean isAntennaNameNull() {
    //	return nullAntennaName;
    // }

    /*
     * Set the null indicator for AntennaName
     */       
    // public void setAntennaNameNull() {
    // 	nullAntennaName = true;
    // }

    /**
     * Get the value for AntennaType.
     */
    public String getAntennaType () {
        return AntennaType;
    }

    /**
     * Set AntennaType to the specified value.
     */
    public void setAntennaType(String AntennaType) {

        this.AntennaType = AntennaType;

    }

    /**
     * Get the value for DishDiameter.
     */
    public Length getDishDiameter () {
        return DishDiameter;
    }

    /**
     * Set DishDiameter to the specified value.
     */
    public void setDishDiameter(Length DishDiameter) {

        this.DishDiameter = DishDiameter;

    }

    /**
     * Get the value for CommissionDate.
     */
    public ArrayTime getCommissionDate () {
        return CommissionDate;
    }

    /**
     * Set CommissionDate to the specified value.
     */
    public void setCommissionDate(ArrayTime CommissionDate) {

        this.CommissionDate = CommissionDate;

    }

    /**
     * Get the value for XPosition.
     */
    public Length getXPosition () {
        return XPosition;
    }

    /**
     * Set XPosition to the specified value.
     */
    public void setXPosition(Length XPosition) {

        this.XPosition = XPosition;

    }

    /**
     * Get the value for YPosition.
     */
    public Length getYPosition () {
        return YPosition;
    }

    /**
     * Set YPosition to the specified value.
     */
    public void setYPosition(Length YPosition) {

        this.YPosition = YPosition;

    }

    /**
     * Get the value for ZPosition.
     */
    public Length getZPosition () {
        return ZPosition;
    }

    /**
     * Set ZPosition to the specified value.
     */
    public void setZPosition(Length ZPosition) {

        this.ZPosition = ZPosition;

    }

    /**
     * Get the value for XOffset.
     */
    public Length getXOffset () {
        return XOffset;
    }

    /**
     * Set XOffset to the specified value.
     */
    public void setXOffset(Length XOffset) {

        this.XOffset = XOffset;

    }

    /**
     * Get the value for YOffset.
     */
    public Length getYOffset () {
        return YOffset;
    }

    /**
     * Set YOffset to the specified value.
     */
    public void setYOffset(Length YOffset) {

        this.YOffset = YOffset;

    }

    /**
     * Get the value for ZOffset.
     */
    public Length getZOffset () {
        return ZOffset;
    }

    /**
     * Set ZOffset to the specified value.
     */
    public void setZOffset(Length ZOffset) {

        this.ZOffset = ZOffset;

    }

    /**
     * Get the value for ComponentId.
     */
    public int getComponentId () {
        return ComponentId;
    }

    /**
     * Set ComponentId to the specified value.
     */
    public void setComponentId(int ComponentId) {

        this.ComponentId = ComponentId;

    }

}
