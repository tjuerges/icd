/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBSimComponentImplCreator.java
 */
package alma.TMCDBComponentImpl;

import java.util.logging.Logger;

import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ComponentHelper;
import alma.TMCDB.TMCDBComponentOperations;
import alma.TMCDB.TMCDBComponentPOATie;
import alma.maciErrType.wrappers.AcsJComponentCreationEx;

public class TMCDBSimComponentImplCreator extends ComponentHelper {

    /**
     * Provide the interface necessary to create an ScriptExecutor component.
     */
    public TMCDBSimComponentImplCreator(Logger containerLogger) {
        super(containerLogger);
    }

    /**
     * @see alma.acs.container.ComponentHelper#_createComponentImpl()
     */
    @Override
	protected ComponentLifecycle _createComponentImpl() throws AcsJComponentCreationEx {
        return new TMCDBSimComponentImpl();
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getPOATieClass()
     */
    @Override
	protected Class _getPOATieClass() {
        return TMCDBComponentPOATie.class;
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getOperationsInterface()
     */
    @Override
	protected Class _getOperationsInterface() {
        return TMCDBComponentOperations.class;
    }


}
