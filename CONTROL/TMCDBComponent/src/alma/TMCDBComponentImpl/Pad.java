/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * /////////////////////////////////////////////////////////////////
 * // WARNING!  DO NOT MODIFY THIS FILE!                          //
 * //  ---------------------------------------------------------  //
 * // | This is generated code!  Do not modify this file.       | //
 * // | Any changes will be lost when the file is re-generated. | //
 * //  ---------------------------------------------------------  //
 * /////////////////////////////////////////////////////////////////
 *
 * File Pad.java
 */
package alma.TMCDBComponentImpl;

import alma.hla.runtime.asdm.types.ArrayTime;

import alma.hla.runtime.asdm.types.Length;

import alma.TMCDB_IDL.PadIDL;

/**

 * The most important thing about pads is their location.  Locations are in meters.

   * Key: BaseElementId

 *
 */
public class Pad implements java.io.Serializable {
    static private final String newline = System.getProperty("line.separator");

    private int BaseElementId;

    private String PadName;

    // private boolean nullPadName;

    private ArrayTime CommissionDate;

    private Length XPosition;

    private Length YPosition;

    private Length ZPosition;

    /**
     * Default Constructor for Pad.  Setter methods must be used to insert data.
     */
    public Pad () {

        // nullPadName = true;

    }

    /**
     * Create a Pad by specifiying all data values.
     */
    public Pad (

        int BaseElementId,

        String PadName,

        ArrayTime CommissionDate,

        Length XPosition,

        Length YPosition,

        Length ZPosition

    ) {

		setBaseElementId(BaseElementId);

		setPadName(PadName);

		setCommissionDate(CommissionDate);

		setXPosition(XPosition);

		setYPosition(YPosition);

		setZPosition(ZPosition);

    }

    /**
     * Create a Pad by specifiying data values as an array of strings.
     */
    public Pad (String[] data) {
    	if (data.length != 6)
    		throw new IllegalArgumentException("Wrong number of items in the data array!  (" + data.length + " are specified; should be 6)");
    	int i = 0;

	  try {

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.BaseElementId = new Integer(Integer.parseInt(data[i]));

		}

	  } catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid number format: (" + data[i] + ").");
	  }

		++i;

		if (data[i] == null || data[i].length() == 0) {

			// nullPadName = true;
			// this.PadName = null;

		} else {

			// nullPadName = false;

			this.PadName = data[i];

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.CommissionDate = new ArrayTime(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.XPosition = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.YPosition = new Length(data[i]);

		}

		++i;

		if (data[i] == null || data[i].length() == 0) {

			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");

		} else {

			this.ZPosition = new Length(data[i]);

		}

		++i;

    }

    /**
     * Display the values of this object.
     */
    public String toString() {
    	String s =  "Pad:" + newline;

        s += "\tBaseElementId: " + BaseElementId + newline;

    	// if (PadName == null)
    	//	s += "\tPadName: null" + newline;
    	// else

        s += "\tPadName: " + PadName + newline;

        s += "\tCommissionDate: " + CommissionDate.toFITS() + newline;

        s += "\tXPosition: " + XPosition + newline;

        s += "\tYPosition: " + YPosition + newline;

        s += "\tZPosition: " + ZPosition + newline;

    	return s;
    }

    /**
     * Create a string in the "unload" format.
     */
    public String toString(String delimiter) {
    	String s =  "Pad" + delimiter;  

        s += BaseElementId + delimiter;

        // if (nullPadName)
        // 	s += delimiter;
        // else

        	// s += PadName + delimiter;

		s += new String(CommissionDate.toFITS()) + delimiter;

        s += XPosition + delimiter;

        s += YPosition + delimiter;

        s += ZPosition + delimiter;

    	return s;
    }

    /**
     * Return the number of columns in the table.
     */
    public static int getNumberColumns() {
    	return 6;
    }

    /**
     * Create a string with the column names in the "unload" format.
     */
    public static String getColumnNames(String delimiter) {
    	String s =  "#Pad" + delimiter  

        	+ "BaseElementId" + delimiter

        	+ "PadName" + delimiter

        	+ "CommissionDate" + delimiter

        	+ "XPosition" + delimiter

        	+ "YPosition" + delimiter

        	+ "ZPosition" + delimiter

			;
    	return s;
    }

    /**
     * Create a string with the column names in the "unload" format.
     */
    public String getTheColumnNames(String delimiter) {
    	return getColumnNames(delimiter);
    }

    /**
     * Compare this oblect with another object of the same type.
     */
    public boolean equals(Object obj) {
    	if (obj == null) return false;
    	if (!(obj instanceof Pad)) return false;
    	Pad arg = (Pad) obj;

		if (this.BaseElementId != arg.BaseElementId)
			return false;

		if (this.PadName == null) {	// Two null strings are equal
			if (arg.PadName == null)
				return true;
			else
				return false;
		}
		if (!this.PadName.equals(arg.PadName))
			return false; 

		if (this.CommissionDate.get() != arg.CommissionDate.get())
			return false;

		if (this.XPosition.get() != arg.XPosition.get())
			return false;

		if (this.YPosition.get() != arg.YPosition.get())
			return false;

		if (this.ZPosition.get() != arg.ZPosition.get())
			return false;

		return true;   
    }

    /**
     * Convert this object to its IDL format.
     */
    public PadIDL toIDL() {
    	PadIDL x = new PadIDL ();

		x.BaseElementId = this.BaseElementId;

		x.PadName = this.PadName;

        // x.nullPadName = this.nullPadName;

		x.CommissionDate = this.CommissionDate.toIDLArrayTime();

		x.XPosition = this.XPosition.toIDLLength();

		x.YPosition = this.YPosition.toIDLLength();

		x.ZPosition = this.ZPosition.toIDLLength();

    	return x;
    }

    /**
     *  Populate this object from an IDL format.
     */
    public void fromIDL(PadIDL x) {

		this.BaseElementId = x.BaseElementId;

		this.PadName = x.PadName;

        // this.nullPadName = x.nullPadName;

		this.CommissionDate = new ArrayTime(x.CommissionDate);

		this.XPosition = new Length(x.XPosition);

		this.YPosition = new Length(x.YPosition);

		this.ZPosition = new Length(x.ZPosition);

    }

    /*
     * If this is a database entry has a generated key, return the value
     * of its generated id; otherwise, return 0.
     */
    public int getId() {

    	return 0;

    }

    /////////////////////////////////////////////////////////////
    // Getter and Setter Methods for Pad.
    /////////////////////////////////////////////////////////////

    /**
     * Get the value for BaseElementId.
     */
    public int getBaseElementId () {
        return BaseElementId;
    }

    /**
     * Set BaseElementId to the specified value.
     */
    public void setBaseElementId(int BaseElementId) {

        this.BaseElementId = BaseElementId;

    }

    /**
     * Get the value for PadName.
     */
    public String getPadName () {
        return PadName;
    }

    /**
     * Set PadName to the specified value.
     */
    public void setPadName(String PadName) {

        // nullPadName = false;

        this.PadName = PadName;

    }

    /*
     * Is the PadName null?
     */
    // public boolean isPadNameNull() {
    //	return nullPadName;
    // }

    /*
     * Set the null indicator for PadName
     */       
    // public void setPadNameNull() {
    // 	nullPadName = true;
    // }

    /**
     * Get the value for CommissionDate.
     */
    public ArrayTime getCommissionDate () {
        return CommissionDate;
    }

    /**
     * Set CommissionDate to the specified value.
     */
    public void setCommissionDate(ArrayTime CommissionDate) {

        this.CommissionDate = CommissionDate;

    }

    /**
     * Get the value for XPosition.
     */
    public Length getXPosition () {
        return XPosition;
    }

    /**
     * Set XPosition to the specified value.
     */
    public void setXPosition(Length XPosition) {

        this.XPosition = XPosition;

    }

    /**
     * Get the value for YPosition.
     */
    public Length getYPosition () {
        return YPosition;
    }

    /**
     * Set YPosition to the specified value.
     */
    public void setYPosition(Length YPosition) {

        this.YPosition = YPosition;

    }

    /**
     * Get the value for ZPosition.
     */
    public Length getZPosition () {
        return ZPosition;
    }

    /**
     * Set ZPosition to the specified value.
     */
    public void setZPosition(Length ZPosition) {

        this.ZPosition = ZPosition;

    }

}
