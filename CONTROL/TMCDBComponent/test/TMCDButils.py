#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2007-02-05  created
#

from Acspy.Clients.SimpleClient import PySimpleClient
import sys
import TMCDB_IDL
import asdmIDLTypes

TMCDB_CURL = "IDL:alma/TMCDB/TMCDBComponent:1.0"
MASTER_CURL = "CONTROL/MASTER"

def setupTMCDB(tmcdb):
    """Sets the TMCDB with a basic configuration.
    Just one antenna, 'AntSimVA1'.
    """

    hrxConfig = TMCDB_IDL.AssemblyLocationIDL("Holography Receiver",
                                              "HoloRx",
                                              0x1c,
                                              0,
                                              0)
    hdspConfig = TMCDB_IDL.AssemblyLocationIDL("Holography DSP",
                                               "HoloDSP",
                                               0x1d,
                                               0,
                                               0)
    otConfig = TMCDB_IDL.AssemblyLocationIDL("optical telescope",
                                             "OpticalTelescope",
                                             0x1f,
                                             0,
                                             0)
    mcConfig = TMCDB_IDL.AssemblyLocationIDL("Mount",
                                             "Mount",
                                             0,
                                             0,
                                             0)
    sai = TMCDB_IDL.StartupAntennaIDL("AntSimVA1",
                                      "padname",
                                      "",
                                      0,
                                      [],
                                      [otConfig, mcConfig])
    tmcdb.setStartupAntennasInfo([sai])

    ai = TMCDB_IDL.AntennaIDL(0,
                              0,
                              0,
                              "AntSimVA1",
                              "",
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLArrayTime(0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              0)
    tmcdb.setAntennaInfo("AntSimVA1", ai)

    pi = TMCDB_IDL.PadIDL(0,
                          0,
                          0,
                          "padName",
                          asdmIDLTypes.IDLArrayTime(0),
                          asdmIDLTypes.IDLLength(0.0),
                          asdmIDLTypes.IDLLength(0.0),
                          asdmIDLTypes.IDLLength(0.0))
    tmcdb.setAntennaPadInfo("AntSimVA1", pi)


if __name__ == "__main__":

    client = PySimpleClient("setupTMCDB")
    tmcdb = client.getDefaultComponent(TMCDB_CURL)    
    setupTMCDB(tmcdb)
    print "TMCDB has been setup."
    # print "Press [Enter] to continue..."
    # sys.stdin.readline()

    master = client.getComponent(MASTER_CURL)
    # marrn = master.createManualArray(master.getAvailableAntennas())
    # print "Manual array has been created."
    # print "Press [Enter] to continue..."
    # sys.stdin.readline()
   
    # master.destroyArray(marrn)
    # client.releaseComponent(marrn)
    # client.releaseComponent(MASTER_CURL)
    # client.releaseComponent(TMCDB_CURL)
 
    # Get the CONTROL system to an operational state
    master.startupPass1()
    master.startupPass2()
    
    print "CONTROL system is being initialized."
    print "Press [Enter] to continue..."
    sys.stdin.readline()
    
    master.shutdownPass1()
    master.shutdownPass2()
    
    client.disconnect()

#    
# __oOo__
