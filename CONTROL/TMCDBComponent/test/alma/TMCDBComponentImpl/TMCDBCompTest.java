package alma.TMCDBComponentImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TMCDBCompTest extends TestCase {

    public TMCDBCompTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public static Test suite() throws Exception {
        TestSuite suite = new TestSuite();
        suite.addTest(new TMCDBComponentTest("testLoadingTMCDBComponent"));
        suite.addTest(new TMCDBComponentTest("testGetStartupAntennaInfo"));
        return suite;
    }
}
