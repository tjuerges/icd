/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.TMCDBComponentImpl;

import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

public class TMCDBComponentTest extends ComponentClientTestCase {

    private ContainerServices container;
    private TMCDBComponent tmcdbIF;
    
    public TMCDBComponentTest(String name) throws Exception {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        container = getContainerServices();
        tmcdbIF = TMCDBComponentHelper.narrow(container.getComponent("TMCDB"));
    }

    protected void tearDown() throws Exception {
        container.releaseComponent("TMCDBReal");
        super.tearDown();
    }

    public void testLoadingTMCDBComponent() {};
    
    public void testGetStartupAntennaInfo() throws Exception {
        StartupAntennaIDL[] sa = tmcdbIF.getStartupAntennasInfo();
        assertEquals(2, sa.length);
        System.out.println(sa[0].antennaName);
        assertEquals("DV01", sa[0].antennaName);
        assertEquals("DA41",sa[1].antennaName);
        assertEquals("dv01-FE", sa[0].frontEndName);
        assertEquals("da41-FE",sa[1].frontEndName);
        assertEquals("Pad01", sa[0].padName);
        assertEquals("Pad02", sa[1].padName);
        assertEquals(6, sa[0].antennaAssembly.length);
        assertEquals(6, sa[1].antennaAssembly.length);
        //First antenna
        assertEquals("PSABE", sa[0].antennaAssembly[0].assemblyRoleName);
        assertEquals("PSABE", sa[0].antennaAssembly[0].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[0].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[0].channelNumber);
        assertEquals(0x60, sa[0].antennaAssembly[0].rca);
        
        assertEquals("PSDBE", sa[0].antennaAssembly[1].assemblyRoleName);
        assertEquals("PSDBE", sa[0].antennaAssembly[1].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[1].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[1].channelNumber);
        assertEquals(0x61, sa[0].antennaAssembly[1].rca);

        assertEquals("PSLLC", sa[0].antennaAssembly[2].assemblyRoleName);
        assertEquals("PSLLC", sa[0].antennaAssembly[2].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[2].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[2].channelNumber);
        assertEquals(0x62, sa[0].antennaAssembly[2].rca);

        assertEquals("PSSAS", sa[0].antennaAssembly[3].assemblyRoleName);
        assertEquals("PSSAS", sa[0].antennaAssembly[3].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[3].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[3].channelNumber);
        assertEquals(0x63, sa[0].antennaAssembly[3].rca);

        assertEquals("DGCK", sa[0].antennaAssembly[4].assemblyRoleName);
        assertEquals("DGCK", sa[0].antennaAssembly[4].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[4].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[4].channelNumber);
        assertEquals(0xA, sa[0].antennaAssembly[4].rca);

        assertEquals("LO2", sa[0].antennaAssembly[5].assemblyRoleName);
        assertEquals("LO2", sa[0].antennaAssembly[5].assemblyTypeName);
        assertEquals(0, sa[0].antennaAssembly[5].baseAddress);
        assertEquals(0, sa[0].antennaAssembly[5].channelNumber);
        assertEquals(0xB, sa[0].antennaAssembly[5].rca);

        //Second Antenna
        assertEquals("PSABE", sa[1].antennaAssembly[0].assemblyRoleName);
        assertEquals("PSABE", sa[1].antennaAssembly[0].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[0].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[0].channelNumber);
        assertEquals(0x60, sa[1].antennaAssembly[0].rca);
        
        assertEquals("PSDBE", sa[1].antennaAssembly[1].assemblyRoleName);
        assertEquals("PSDBE", sa[1].antennaAssembly[1].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[1].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[1].channelNumber);
        assertEquals(0x61, sa[1].antennaAssembly[1].rca);

        assertEquals("PSLLC", sa[1].antennaAssembly[2].assemblyRoleName);
        assertEquals("PSLLC", sa[1].antennaAssembly[2].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[2].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[2].channelNumber);
        assertEquals(0x62, sa[1].antennaAssembly[2].rca);

        assertEquals("PSSAS", sa[1].antennaAssembly[3].assemblyRoleName);
        assertEquals("PSSAS", sa[1].antennaAssembly[3].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[3].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[3].channelNumber);
        assertEquals(0x63, sa[1].antennaAssembly[3].rca);

        assertEquals("DGCK", sa[1].antennaAssembly[4].assemblyRoleName);
        assertEquals("DGCK", sa[1].antennaAssembly[4].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[4].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[4].channelNumber);
        assertEquals(0xA, sa[1].antennaAssembly[4].rca);

        assertEquals("LO2", sa[1].antennaAssembly[5].assemblyRoleName);
        assertEquals("LO2", sa[1].antennaAssembly[5].assemblyTypeName);
        assertEquals(0, sa[1].antennaAssembly[5].baseAddress);
        assertEquals(0, sa[1].antennaAssembly[5].channelNumber);
        assertEquals(0xB, sa[1].antennaAssembly[5].rca);

        //FRONT END
        assertEquals(1, sa[0].frontEndAssembly.length);
        assertEquals(1, sa[1].frontEndAssembly.length);
        //Front End va antenna
        assertEquals("WCA3", sa[0].frontEndAssembly[0].assemblyRoleName);
        assertEquals("WCA3", sa[0].frontEndAssembly[0].assemblyTypeName);
        assertEquals(0, sa[0].frontEndAssembly[0].baseAddress);
        assertEquals(0, sa[0].frontEndAssembly[0].channelNumber);
        assertEquals(0x14, sa[0].frontEndAssembly[0].rca);
        //FrontEnd aec antenna
        
        assertEquals("WCA3", sa[0].frontEndAssembly[0].assemblyRoleName);
        assertEquals("WCA3", sa[0].frontEndAssembly[0].assemblyTypeName);
        assertEquals(0, sa[0].frontEndAssembly[0].baseAddress);
        assertEquals(0, sa[0].frontEndAssembly[0].channelNumber);
        assertEquals(0x14, sa[0].frontEndAssembly[0].rca);
    }
}
