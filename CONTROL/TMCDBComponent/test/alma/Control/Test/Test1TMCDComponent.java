/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Test1TMCDComponent.java
 */
package alma.Control.Test;

import alma.TMCDB_IDL.*;
import alma.TMCDBComponentImpl.*;
import alma.TMCDB.*;
import alma.TMCDB.generated.*;
import alma.TmcdbErrType.TmcdbErrTypeEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.acs.component.ComponentLifecycleException;

public class Test1TMCDComponent {
	
	public static void main(String[] arg) {
		test1();
	}
	
    static private final String newline = System.getProperty("line.separator");
	public static String toString(AssemblyLocationIDL x) {
		return "AssemblyLocationIDL: " + newline + 
			"\tassemblyTypeName: " + x.assemblyTypeName + newline +
			"\tassemblyRoleName: " + x.assemblyRoleName + newline +
			"\trca: " + x.rca + newline +
			"\tchannelNumber: " + x.channelNumber + newline +
			"\tbaseAddress: " + x.baseAddress + newline;
	}
	public static String toString(StartupAntennaIDL x) {
		String s = "StartupAntennaIDL: " + newline +
			"\tantennaName: " + x.antennaName + newline +
			"\tpadName: " + x.padName + newline +
			"\tfrontEndName: " + x.frontEndName + newline;
		for (int i = 0; i < x.frontEndAssembly.length; ++i) 
			s += toString(x.frontEndAssembly[i]);
		for (int i = 0; i < x.antennaAssembly.length; ++i) 
			s += toString(x.antennaAssembly[i]);
		return s;
	}
	public static String toString(PointingModelIDL x) {
		String s = "PointingModelIDL: " + newline +
			"\tantennaName: " + x.antennaName + newline +
			"\tpadName: " +x.padName + newline;
		AntennaPointingModel m = new AntennaPointingModel (); m.fromIDL(x.pointingModel);
		s += m.toString();
		for (int i = 0; i < x.term.length; ++i) {
			AntennaPointingModelTerm t = new AntennaPointingModelTerm (); t.fromIDL(x.term[i]);
			s += t.toString();
		}
		return s;
	}

	////////////////////////////////////////////
	public static void test1() {
		  try {
			System.out.println("Test for accessing TMCDB");
			// Create the database object, connect to it, and initialize it.
			TMCDBSimComponentImpl tmcdb = new TMCDBSimComponentImpl ();
			//TMCDBComponentImpl tmcdb = new TMCDBComponentImpl ();
			alma.acs.container.ContainerServices cs = null;
			tmcdb.initialize(cs);
			System.out.println("TMCDBComponent ready.");
			
			// Get the initial antennas and pads.
			System.out.println("Get the initial antennas and pads.");
			StartupAntennaIDL[] antenna = tmcdb.getStartupAntennasInfo();
			for (int i = 0; i < antenna.length; ++i)
				System.out.println(toString(antenna[i]));
			
			// Get an antenna and its pad.
			System.out.println("Get an antenna and its pad.");
			AntennaIDL a1 = tmcdb.getAntennaInfo("AntSimVA1");
			Antenna aa1 = new Antenna (); aa1.fromIDL(a1);
			System.out.println(aa1.toString());
			PadIDL p1 = tmcdb.getCurrentAntennaPadInfo("AntSimVA1");
			if (p1 == null)
				System.out.println("p1 is null!");
			else {
				Pad pp1 = new Pad(); pp1.fromIDL(p1);
				System.out.println(pp1.toString());
			}
			AntennaIDL a2 = tmcdb.getAntennaInfo("AntSimAEC1");
			Antenna aa2 = new Antenna (); aa2.fromIDL(a2);
			System.out.println(aa2.toString());
			PadIDL p2 = tmcdb.getCurrentAntennaPadInfo("AntSimAEC1");
			if (p2 == null)
				System.out.println("p2 is null!");
			else {
				Pad pp2 = new Pad(); pp2.fromIDL(p2);
				System.out.println(pp2.toString());
			}
			//AntennaIDL a3 = tmcdb.getAntennaInfo("ALMA03");
			
			// Get all pointing models.
			System.out.println("Get all pointing models.");
			PointingModelIDL[] model = tmcdb.getPointingModelsInfo("AntSimAEC1");
			System.out.println("Models for AntSimAEC1: " + model.length);
			for (int i = 0; i < model.length; ++i) {
				System.out.println(toString(model[i]));
			}
						
			// Get the current pointing model.
			System.out.println("Get the current pointing model.");
			PointingModelIDL m = tmcdb.getPointingModelInfo("AntSimAEC1");
			System.out.println("Current model for AntSimAEC1");
			System.out.println(toString(m));

			// Get the recent pointing model.
			System.out.println("Get the recent pointing model.");
			PointingModelIDL r = tmcdb.getRecentPointingModelInfo("AntSimAEC1");
			System.out.println("Current model for AntSimAEC1");
			System.out.println(toString(r));
		  
		  } catch (ComponentLifecycleException err) {
			  err.printStackTrace();
		  } catch (TmcdbNoSuchRowEx e) {
			  AcsJTmcdbNoSuchRowEx.fromTmcdbNoSuchRowEx(e).printStackTrace();
		}
		}
		////////////////////////////////////////////

}
