#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

# Stop the TMCDB Monitor.

import sys, os, signal,time
from TestCommon import *
                
# Stop CONTROL/ACC/javaContainer.
#logging.info('Stoping CONTROL/ACC/javaContainer.')
if os.path.exists('./tmp1/CONTROL_ACC_javaContainer.Start.log'):
    os.system('acsStopContainer CONTROL/ACC/javaContainer &> ./tmp1/CONTROL_ACC_javaContainer.Stop.log')
    time.sleep(15)
#else:
    #logging.info('Container CONTROL/ACC/javaContainer was not running.')
# Stop ACS.
#if os.path.exists('./tmp1/acs.pid'):
env=os.environ
ACSDATA=env['ACSDATA']
if os.path.exists(ACSDATA+'/tmp/ACS_INSTANCE.'+str(0)):
    #logging.info('Stoping ACS...')
    os.system('acsStop &> ./tmp1/acsStop.log')
time.sleep(15)
# Stop database (if necessary).
if dbBackend == HSQLDB:
    #logging.info('Stoping HSQLDB...')
    os.system('java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc --sql "SHUTDOWN;" localhost-sa')
