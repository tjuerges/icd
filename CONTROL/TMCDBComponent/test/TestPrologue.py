#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
# pburgos   2008-07-01  Synchcronization between forks/call added.
# pburgos   2008-07-03  acsStartup now with -noloadifr to speed up testing process

#Note: If  this script produces any output TAT thinks the prologue has
# failed. So all output is sent to a log file

import sys, os, logging
import traceback
import unittest
#import thread
import time
from TestCommon import *
              
# Create the temporary directory.
if os.path.exists('./tmp1'):
    for f in os.listdir('./tmp1'): os.unlink('./tmp1/' + f)
    os.rmdir('./tmp1')
os.mkdir('./tmp1')
       
# Start database (if necessary.) If ORACLE is used instead of HSQLDB, it is asumed
# running and configured.
if dbBackend == HSQLDB:
    #logging.info('Starting HSQLDB...')
    os.system('java -cp ../lib/hsqldb.jar org.hsqldb.Server -port 8090 -no_system_exit true &> ./tmp1/HSQLStart.log &')
    time.sleep(5)
        
#Configure the database.
if dbBackend == HSQLDB:
     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa DropAllTables.sql &> ./tmp1/DropAllTables.log")
     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa CreateHsqldbTables.sql &>./tmp1/CreateHsqldbTables.log")
     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa InsertTestAntennas.sql &>./tmp1/InsertTestAntennas.log")            

# Start ACS (only if it hasn't been started yet.)
# Using fork/exec combination here to be able to modify the
# environment variables.
ACS_INSTANCE=0;
inst_file = "%s/tmp1/ACS_INSTANCE.%d" % (os.environ['ACSDATA'], ACS_INSTANCE)

if not os.path.exists(inst_file):
    
    logf = open('./tmp1/acsStart.log', 'w') # log file
    
    #logging.info('Loading Enviroment variables')
    env = os.environ
    env['ACS_CDB'] = os.getcwd() 
    env['ENABLE_TMCDB'] = 'false'
    env['TMCDB_CONFIGURATION_NAME'] = 'ATF1'
    #env['IFRHACK'] = 'true'
    #logging.info('Starting ACS...')
    acs_pid = os.fork()
    if acs_pid == 0: # child
        
        os.dup2(logf.fileno(), sys.stdout.fileno()) # connect stdout to log file
        os.dup2(logf.fileno(), sys.stderr.fileno()) # connect stderr to log file
        #logging.info('Creating ./acs.pid lock file')
        pidf = open('./tmp1/acs.pid', 'w')
        pidf.write(str(acs_pid))
        pidf.close()
        argsTuple=('acsStart','-noloadifr',)
        os.execvpe('acsStart', argsTuple,env)
        os._exit(0)
    
        
        
    else:            # parent
        #logging.info("I'm about to wait for the child process")
        rpid, status = os.wait() # wait for the child process
        #logging.info("Child process returned")
        #logging.info("child %d exited with status %d" %(rpid, os.WEXITSTATUS(status)))
        logf = open('./tmp1/acsstartupLoadIFR.log', 'w') # log file
        #Load  IFR
        ifr_pid = os.fork()
        if ifr_pid == 0: # ifrloading process child
            os.dup2(logf.fileno(), sys.stdout.fileno()) # connect stdout to log file
            os.dup2(logf.fileno(), sys.stderr.fileno()) # connect stderr to log file
            #logging.info("Loading IFR")
            os.execlpe('acsstartupLoadIFR','acsstartupLoadIFR', 'TMCDBComponent.idl',env)
        else:
            time.sleep(10)
            fpid,status =os.wait()
            if (os.WEXITSTATUS(status)!=0):
               #logging.info("IFR could NOT be loaded. Error reported")
               sys.exit(1)
            else:
                #logging.info("IFR Loaded Succesfully");
                #logging.info('Starting CONTROL/ACC/javaContainer container')
                cont_pid = os.fork()
                if cont_pid == 0:
                    #os.setpgrp() # now this is the leader process
                    logf = open('./tmp1/CONTROL_ACC_javaContainer.log', 'w')
                    os.dup2(logf.fileno(), sys.stdout.fileno()) # connect stdout to log file
                    os.dup2(logf.fileno(), sys.stderr.fileno()) # connect stderr to log file
                    #argsTuple=('acsStartContainer','-java ','CONTROL/ACC/javaContainer',)
                    #os.execvpe('acsStartContainer',argsTuple,env)
                    #
                    # Next command is executed in a subshell.
                    #os.system('acsutilBlock -t 60 -f ./tmp1/CONTROL_ACC_javaContainer.Start.log -b "Container Activated" -x acsStartContainer -java CONTROL/ACC/javaContainer ./tmp1/acsutilBlock-1.log 2>&1 ')
                    os.system('acsStartContainer -java CONTROL/ACC/javaContainer &> ./tmp1/CONTROL_ACC_javaContainer.Start.log &')
                    #logging.info("acsStartContainer launched")
                    time.sleep(10) #This is absolutely needed since acsStartContainer returns inmmediatly
                else:# parent
                    #logging.info("Waiting for CONTROL/ACC/javaContainer to startup")
                 #   ##logging.info("Container CONTROL/ACC/javaContainer started")
                    pidf = open('./tmp1/CONTROL_ACC_javaContainer.Start.pid', 'w')
                    pidf.write(str(cont_pid))
                    pidf.close()
                    #logf.close()
                    time.sleep(10)
                    os._exit(0)
                 #   time.sleep(15) #This is absolutely needed since acsStartContainer returns inmmediatly
                 #   
sys.exit(0)                    
# Start the containers              
#
# __oOo__
