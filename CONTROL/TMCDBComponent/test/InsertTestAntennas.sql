-- /////////////////////////////////////////////////////////////////
-- TMCDB SQL INSERT DATA Version 1.0 2008-JUN-30
-- Script to populate a test Database
--
-- Pablo Burgos
--
-- /////////////////////////////////////////////////////////////////

--To store a pointing model for an ANTENNAid located on a specific PADid we need to first create an antena on the database with its components, containers and computers involved.

--Each antena (and its configuration) is associated with a configuration id. So let's start inserting this info on Configuration Table. 

INSERT INTO  Configuration (ConfigurationId,ConfigurationName,FullName,CreationTime,Description) values (111,'ATF1', 'ATF20080211', 20080211,'This is a startup configuration for ATF. Its includes DA41 and DV01 with the components and containers up to know running over there. The main purpose of this one is to enable storing and loading pointing models on DB(pb)'); 

INSERT INTO LoggingConfig (LoggingConfigId,ConfigurationId,MinLogLevelDefault, MinLogLevelLocalDefault,CentralizedLogger,DispatchPacketSize,ImmediateDispatchLevel, FlushPeriodSeconds, MaxLogQueueSize) values (2008021101,111,2,2,'Log',010,010,010,1000);


--After this, we can start to insert information about computers, containers and  components (in this order):
--============================
INSERT INTO Computer (ComputerId,ComputerName,ConfigurationId,HostName,RealTime,ProcessorType,PhysicalLocation) values (1,'dv01-abm',111,'dv01-abm','1','smp','Inside Cabin close to BackEnd');

INSERT INTO Computer (ComputerId,ComputerName,ConfigurationId,HostName,RealTime,ProcessorType,PhysicalLocation) values (2,'da41-abm',111,'da41-abm','1','smp','Inside Cabin close to BackEnd');
--============================
INSERT INTO Container (ContainerId,ContainerName,Path,ConfigurationId,LoggingConfigId,ComputerId,ImplLang,TypeModifiers,RealTime,RealTimeType,KernelModuleLocation,KernelModule,AcsInstance,CmdLineArgs,KeepAliveTime,ServerThreads,ManagerRetry,CallTimeout,Recovery ,AutoloadSharedLibs) values (1,'dv01-cppContainer','CONTROL/DV01/cppContainer',111,2008021101,1,'cpp','nose',true,'ABM','/alma/ACS-7.0/ACSSW/config','CAMBServerTPMC901.lkm',0,'starCpp',3,2,10,240,true,'');


INSERT INTO Container (ContainerId,ContainerName,Path,ConfigurationId,LoggingConfigId,ComputerId,ImplLang,TypeModifiers,RealTime,RealTimeType,KernelModuleLocation,KernelModule,AcsInstance,CmdLineArgs,KeepAliveTime,ServerThreads,ManagerRetry,CallTimeout,Recovery ,AutoloadSharedLibs) values (2,'da41-cppContainer','CONTROL/DA41/cppContainer',111,2008021101,00002,'cpp','nose','1','ABM','/alma/ACS-7.0/ACSSW/config','CAMBServerTPMC901.lkm',000,'starCpp',3,2,10,240,1,'');

--Note that some components like antenna mode controller have an * as the container to the one it 's attached to. Tha means that container is 
-- give it to the component at runtime. For TMCDB point of view, that container is already created, so must be somewhere on the Container Table

--======================
-- Phisical Assemblies have a component (Software impl) associated with it
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(1,'idl:/alma/Control/PSABE:1.0', null); 
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(2,'idl:/alma/Control/PSDBE:1.0', null);
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(3,'idl:/alma/Control/PSLLC:1.0', null);
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(4,'idl:/alma/Control/PSSAS:1.0', null);
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(6,'idl:/alma/Control/DGCK:1.0', null);
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(7,'idl:/alma/Control/LO2:1.0', null);
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(8,'idl:/alma/Control/WCA3:1.0', null);
--Moreover antennas are components,so ...
INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(5,'IDL:alma/Control/Antenna:1.0', null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC)
values (10002, 'PSABE', 111,1,1,'cpp','1','PSABEImpl','CONTROL/DV01/PSABE','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC) 
values (10003, 'PSABE', 111,2,2,'cpp','1','PSABEImpl','CONTROL/DA41/PSABE','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC) 
values (10004, 'PSDBE', 111,1,3,'cpp','1','PSDBEImpl','CONTROL/DV01/PSDBE','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC) 
values (10005, 'PSDBE', 111,2,4,'cpp','1','PSDBEImpl','CONTROL/DA41/PSDBE','1','1','1','10',2,2,null);

--INSERT INTO COMPONENTTYPE (COMPONENTTYPEID, IDL, SCHEMA) values(6,'IDL:alma/Control/Antenna:1.0', null);
INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC)  
values (10000, 'DV01', 111,1,4,'cpp','1','Antenna','CONTROL/Antenna/DV01','1','1','1','10',2,2,null);
INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC) 
values (10001, 'DA41', 111,2,4,'cpp','1','Antenna','CONTROL/Antenna/DV01','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC)  
values (10006, 'DGCK', 111,2,4,'cpp','1','DGCKImpl','CONTROL/Antenna/DV01','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC)  
values (10007, 'LO2', 111,2,4,'cpp','1','LO2Impl','CONTROL/Antenna/DV01','1','1','1','10',2,2,null);

INSERT INTO Component (ComponentId, ComponentName, ConfigurationId,ContainerId, ComponentInterfaceId,ImplLang, RealTime,Code,Path,IsAutostart, IsDefault , IsStandaloneDefined  ,KeepAliveTime, MinLogLevel ,MinLogLevelLocal,XMLDOC) 
values (10008, 'WCA3', 111,2,4,'cpp','1','WCA3Impl','CONTROL/Antenna/DV01','1','1','1','10',2,2,null);

--=============================

--===========================================

-- INSERT INTO AntennaPointingModel (PointingModelId, AntennaId,  PadId,StartValidTime,EndValidTime,AsdmUID) values (1,1,1,20071102,NULL,'uid://X34/X34/X1');

-- INSERT INTO AntennaPointingModel (PointingModelId, AntennaId,  PadId,StartValidTime,EndValidTime,AsdmUID) values (2,2,2,20071111,NULL,'uid://X64/X364/X16');


--===========================================
--Ahora insertamos un pointing model para la antennaid=1
--Enabled        1
--BAND           3
--IA             -84.62
--IE            +254.63
--HASA            +0.00
--HASA2           -0.83
--HESE            -3.64
--HECE            +3.65
--HESA2           -1.04
--HECA2           +2.99
--NPAE           +28.64
--CA            +273.19
--AN              -3.13
--AW              +9.85

-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(1,'Enabled',1,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(1,'BAND',11,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(1,'IA',1001,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(1,'IE',1001,0);

-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(2,'Enabled',1,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(2,'BAND',11,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(2,'IA',1001,0);
-- INSERT INTO AntennaPointingModelTerm(PointingModelId,CoeffName,CoeffValue,CoeffError) values(2,'IE',1001,0);

--***************************************************************************************************
--Nota: Aca lo mejor era definir pointing model en AntennaPointingModelTerm sin tener referencias externas hacia antennaPointingModel. Asi podriamos crear todos los pointing model que queramos y despues asociarlos a una Antenna y pad en AntennaPointingModel

--En el modelo actual se debe crear (insert) una linea  con la asociacion entre Antennaid PadId y PMid (creando PMid antes de definirlo en la tabla PointingModelTerm) lo cual crea una inconsistencia momentanea.

-- Next SQL Select is performed by TMCDBComponentImpl method getStartupAntennasInfo()
--SELECT startup.STARTUPID FROM configuration, startup WHERE configuration.CONFIGURATIONNAME = 'ATF1' AND startup.CONFIGURATIONID = 111;
INSERT INTO STARTUP(STARTUPID,CONFIGURATIONID,STARTUPNAME) values (222,111,'ATF1 Startup for testing(pburgos)');
--SELECT baseelement.baseid FROM baseelementstartup, baseelement WHERE baseelement.baseelementid = baseelementstartup.baseelementid AND baseelementstartup.startupid = 222 AND baseelement.basetype = 'Antenna';
--SELECT antennaid, antennaname FROM antenna WHERE baseelementid = 1;
--Please note this constraint. CHECK (BaseType IN ('Antenna', 'Pad', 'CorrQuadrant', 'FrontEnd', 'WeatherStation', 'CentralRack', 'MasterClock', 'HolographyTower', 'Array'))
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (500,1,'Antenna','DV01',111);
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (501,2,'Antenna','DA41',111);
--now we fill up the table baseelementstartup
INSERT INTO BASEELEMENTSTARTUP(BASEELEMENTID,STARTUPID) values (500,222);
INSERT INTO BASEELEMENTSTARTUP(BASEELEMENTID,STARTUPID) values (501,222);

--Now we fill up Antenna Table
--be aware of this restrictions  
--CONSTRAINT AntennaComponent FOREIGN KEY (ComponentId) REFERENCES Component,
--CONSTRAINT AntennaType CHECK (AntennaType IN ('VA', 'AEC', 'ACA'))
--Does Antenna really need a relationship with a componentID as stated in this table?? Is an Antenna a Component?
INSERT INTO ANTENNA(ANTENNAID,CONFIGURATIONID,BASEELEMENTID,ANTENNANAME,ANTENNATYPE,DISHDIAMETER,COMMISSIONDATE,XPOSITION,YPOSITION,ZPOSITION,XOFFSET,YOFFSET,ZOFFSET,COMPONENTID) values (1,111,500,'DV01','VA',1212.3,200807011236,0,0,0,0,0,0,10000);

INSERT INTO ANTENNA(ANTENNAID,CONFIGURATIONID,BASEELEMENTID,ANTENNANAME,ANTENNATYPE,DISHDIAMETER,COMMISSIONDATE,XPOSITION,YPOSITION,ZPOSITION,XOFFSET,YOFFSET,ZOFFSET,COMPONENTID) values (2,111,501,'DA41','AEC',1212.3,200807011236,0,0,0,0,0,0,10001);

--Now the Pads info is comming :)
--First we create a Baseelement whose type is Pad. The we fill in Pad Table and finally AntennaToPad Table
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (502,3,'Pad','Pad01',111);
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (503,4,'Pad','Pad02',111);

--For insert into PAD be aware of this restriction  CONSTRAINT PadBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
INSERT INTO PAD(PADID,CONFIGURATIONID,BASEELEMENTID,PADNAME,COMMISSIONDATE,XPOSITION,YPOSITION,ZPOSITION) values (1,111,502,'Pad01',20080701,0,0,0);
INSERT INTO PAD(PADID,CONFIGURATIONID,BASEELEMENTID,PADNAME,COMMISSIONDATE,XPOSITION,YPOSITION,ZPOSITION) values (2,111,503,'Pad02',20080701,0,0,0);

-- btw this is what TMCDComponentImpl queries  SELECT padname FROM pad, antennatopad " +
-- WHERE pad.padid = antennatopad.padid " +
-- "AND antennatopad.antennaid = " + antennaId;

--Now into antennatopad table we link a pad with an antenna
INSERT INTO ANTENNATOPAD(ANTENNAID,PADID,STARTTIME,ENDTIME,PLANNED) values (1,1,20080615,null,20091001);
INSERT INTO ANTENNATOPAD(ANTENNAID,PADID,STARTTIME,ENDTIME,PLANNED) values (2,2,20080615,null,20091001);



--Now TMCDBComponentImpl get FrontEnd Information
--now the query performed is displayed
--SELECT f.frontendname, f.baseelementid FROM frontend f JOIN antennatofrontend af ON af.frontendid = f.frontendid 
--                      WHERE af.antennaid = 1;-- antennaId;
--As usual, first we start adding rows to baseelement
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (504,5,'FrontEnd','dv01-FE',111);
INSERT INTO BASEELEMENT(BASEELEMENTID,BASEID,BASETYPE,BASEELEMENTNAME,CONFIGURATIONID) values (505,6,'FrontEnd','da41-FE',111);

                        
INSERT INTO FRONTEND(FRONTENDID,CONFIGURATIONID,BASEELEMENTID,FRONTENDNAME,COMMISSIONDATE,COMPONENTID) values (1,111,504,'dv01-FE',20080630,10000);
INSERT INTO FRONTEND(FRONTENDID,CONFIGURATIONID,BASEELEMENTID,FRONTENDNAME,COMMISSIONDATE,COMPONENTID) values (2,111,505,'da41-FE',20080530,10001);

-- Now the link betweeen Antenna and FrontEnd comes
INSERT INTO ANTENNATOFRONTEND(ANTENNAID,FRONTENDID,STARTTIME,ENDTIME) values (1,1,20080601,20210622);
INSERT INTO ANTENNATOFRONTEND(ANTENNAID,FRONTENDID,STARTTIME,ENDTIME) values (2,2,20080601,20210622);

--Now we will insert Some devices to assembly type. First we have to register the on LRUTYPE table
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('PSABE','PowerSupply for Analog BackEnd Rack','EDM-ALMA-2007PS',20080403,'This device allow for powering analog rack on BE','Now there is a firmware problem reported');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('PSDBE','PowerSupply for DIGITAL BackEnd Rack','EDM-ALMA-2007PS',20080403,'This device allow for powering digital rack on BE','Now there is a firmware problem reported');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('PSLLC','PowerSupply for LLC Central LO Rack','EDM-ALMA-2007PS',20080403,'This device allow for powering llc rack on CENTRAL BUILDING','Now there is a firmware problem reported');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('PSSAS','PowerSupply for SAS Central LO Rack','EDM-ALMA-2007PS',20080403,'This device allow for powering sas rack on CENTRAL BUILDING','Now there is a firmware problem reported');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('DGCK','Digital Clock BackEnd','EDM-ALMA-2007DGCK',20080403,'This device is important for SFI','This device rocks');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('LO2','BackEnd Second LO','EDM-ALMA-2007LO2',20080403,'This device is the second local oscillator','Device pretty stable');
INSERT INTO LRUTYPE(LRUNAME,FULLNAME,ICD,ICDDATE,DESCRIPTION,NOTES) values ('WCA3','W Catridge Assembly Band3','EDM-ALMA-2007WCA3',20080403,'This device is WCA','Device very talkative');


-- Now AssemblyType table

--sql> \d assemblytype
--name             datatype          width  no-nulls
-- ---------------  -----------  ----------  --------
-- ASSEMBLYNAME     VARCHAR             256  *
-- LRUNAME          VARCHAR             128  *
-- FULLNAME         VARCHAR             256  *
-- NODEADDRESS      VARCHAR              16  *
-- CHANNELNUMBER    TINYINT               4  *
-- BASEADDRESS      VARCHAR              16  
-- DESCRIPTION      LONGVARCHAR  2147483647  *
-- NOTES            LONGVARCHAR  2147483647  
-- COMPONENTTYPEID  INTEGER              11  *

INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('PSABE','PSABE','PowerSupply','96',0,0,'Description for assemblytype','notes for assembly type',1);

INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('PSDBE','PSDBE','PowerSupply','97',0,0,'Description for assemblytype','notes for assembly type',2);
INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('PSLLC','PSLLC','PowerSupply','98',0,0,'Description for assemblytype','notes for assembly type',3);
INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('PSSAS','PSSAS','PowerSupply','99',0,0,'Description for assemblytype','notes for assembly type',4);

INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('DGCK','DGCK','Digiclock','10',0,0,'Description for assemblytype','notes for assembly type',4);


INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('LO2','LO2','Second Local Oscillator','11',0,0,'Description for assemblytype','notes for assembly type',4);

INSERT INTO ASSEMBLYTYPE(ASSEMBLYNAME,LRUNAME,FULLNAME,NODEADDRESS,CHANNELNUMBER,BASEADDRESS,DESCRIPTION,NOTES,COMPONENTTYPEID) values
('WCA3','WCA3','W Catridge Assembly Banda 3','20',0,0,'Description for assemblytype','notes for assembly type',5);


INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (1,'PSABE',111,121,null);
INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (2,'PSDBE',111,122,null);
INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (3,'PSLLC',111,123,null);
INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (4,'PSSAS',111,124,null);
INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (5,'DGCK',111,125,null);
INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (6,'LO2',111,126,null);

INSERT INTO Assembly (ASSEMBLYID,ASSEMBLYNAME,CONFIGURATIONID,SERIALNUMBER,DATA) values (7,'WCA3',111,127,null);
--ASSEMBLYID       INTEGER              11  *
--ASSEMBLYNAME     VARCHAR             256  *
--CONFIGURATIONID  INTEGER              11  *
--SERIALNUMBER     VARCHAR             256  *
--DATA             LONGVARCHAR  2147483647  

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(1,500,222,10002,'PSABE',5,'PSABE',0,'96','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(2,500,222,10003,'PSDBE',5,'PSDBE',0,'97','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(3,500,222,10004,'PSLLC',5,'PSLLC',0,'98','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(4,500,222,10005,'PSSAS',5,'PSSAS',0,'99','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(5,500,222,10006,'DGCK',5,'DGCK',0,'10','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(6,500,222,10007,'LO2',5,'LO2',0,'11','0');



--==Next device belogs to frontend baseelement :)

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(7,504,222,10008,'WCA3',5,'WCA3',0,'11','0');

-- ==== Now the other antenna

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(1,501,222,10002,'PSABE',5,'PSABE',0,'96','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(2,501,222,10003,'PSDBE',5,'PSDBE',0,'97','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(3,501,222,10004,'PSLLC',5,'PSLLC',0,'98','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(4,501,222,10005,'PSSAS',5,'PSSAS',0,'99','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(5,501,222,10002,'DGCK',5,'DGCK',0,'10','0');

INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(6,501,222,10002,'LO2',5,'LO2',0,'11','0');
--Front end
INSERT INTO ASSEMBLYSTARTUP(ASSEMBLYID,BASEELEMENTID,STARTUPID,COMPONENTID,GROUPNAME,ORDERTAG,ROLENAME,CHANNELNUMBER,NODEADDRESS,BASEADDRESS) values 
(7,505,222,10008,'WCA3',5,'WCA3',0,'20','0');

--ASSEMBLYID     INTEGER      11  *
--BASEELEMENTID  INTEGER      11  *
--STARTUPID      INTEGER      11  *
--COMPONENTID    INTEGER      11  *
--GROUPNAME      VARCHAR     128  
--ORDERTAG       SMALLINT      6  
--ROLENAME       VARCHAR     128  
--CHANNELNUMBER  TINYINT       4  
--NODEADDRESS    VARCHAR      16  
--BASEADDRESS    VARCHAR      16  


--Commit all this changes
commit;


