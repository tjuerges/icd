-- TMCDB SQL TABLE DEFINITIONS Version 1.4 2007-MAR-9
--
-- /////////////////////////////////////////////////////////////////
-- // WARNING!  DO NOT MODIFY THIS FILE!                          //
-- //  ---------------------------------------------------------  //
-- // | This is generated code!  Do not modify this file.       | //
-- // | Any changes will be lost when the file is re-generated. | //
-- //  ---------------------------------------------------------  //
-- /////////////////////////////////////////////////////////////////
CREATE  TABLE Configuration (
    ConfigurationId INTEGER IDENTITY
,
    ConfigurationName VARCHAR (128)  NOT NULL 
,
    FullName VARCHAR (256)  NOT NULL 
,
    CreationTime BIGINT  NOT NULL 
,
    Description LONGVARCHAR  NOT NULL 
,
	CONSTRAINT ConfigAltKey UNIQUE (ConfigurationName)
);
CREATE  TABLE Computer (
    ComputerId INTEGER IDENTITY
,
    ComputerName VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    HostName VARCHAR (256)  NOT NULL 
,
    RealTime BOOLEAN  NOT NULL 
,
    ProcessorType CHAR (3)  NOT NULL 
,
    PhysicalLocation LONGVARCHAR  NULL 
,
	CONSTRAINT ComputerAltKey UNIQUE (ComputerName, ConfigurationId),
    CONSTRAINT ComputerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComputerProcessorType CHECK (ProcessorType IN ('uni', 'smp'))
);
CREATE  TABLE LoggingConfig (
    LoggingConfigId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    MinLogLevelDefault TINYINT  DEFAULT 2
,
    MinLogLevelLocalDefault TINYINT  DEFAULT 2
,
    CentralizedLogger VARCHAR (16)  DEFAULT 'Log'
,
    DispatchPacketSize TINYINT  DEFAULT 10
,
    ImmediateDispatchLevel TINYINT  DEFAULT 10
,
    FlushPeriodSeconds TINYINT  DEFAULT 10
,
    MaxLogQueueSize INTEGER  DEFAULT 1000
,
    CONSTRAINT LoggingConfigConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NamedLoggerConfig (
    NamedLoggerConfigId INTEGER IDENTITY
,
    LoggingConfigId INTEGER  NOT NULL 
,
    Name VARCHAR (64)  NOT NULL 
,
    MinLogLevel TINYINT  DEFAULT 2
,
    MinLogLevelLocal TINYINT  DEFAULT 2
,
	CONSTRAINT NamedLCAltKey UNIQUE (LoggingConfigId, Name),
    CONSTRAINT NamedLoggerConfigLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
);
CREATE  TABLE Manager (
    ManagerId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    LoggingConfigId INTEGER  NOT NULL 
,
    Startup VARCHAR (256)  NULL 
,
    ServiceComponents VARCHAR (256)  NULL 
,
    Timeout INTEGER  DEFAULT 50
,
    ClientPingInterval INTEGER  DEFAULT 60
,
    AdministratorPingInterval INTEGER  DEFAULT 45
,
    ContainerPingInterval INTEGER  DEFAULT 30
,
    ServerThreads TINYINT  DEFAULT 10
,
    DeadlockTimeout INTEGER  DEFAULT 180
,
    CONSTRAINT ManagerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig,
    CONSTRAINT ManagerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Container (
    ContainerId INTEGER IDENTITY
,
    ContainerName VARCHAR (256)  NOT NULL 
,
    Path VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    LoggingConfigId INTEGER  NOT NULL 
,
    ComputerId INTEGER  NULL 
,
    ImplLang VARCHAR (6)  NOT NULL 
,
    TypeModifiers VARCHAR (64)  NULL 
,
    RealTime BOOLEAN  DEFAULT FALSE
,
    RealTimeType VARCHAR (4)  DEFAULT 'NONE'
,
    KernelModuleLocation LONGVARCHAR  NULL 
,
    KernelModule LONGVARCHAR  NULL 
,
    AcsInstance TINYINT  DEFAULT 0
,
    CmdLineArgs VARCHAR (256)  NULL 
,
    KeepAliveTime INTEGER  DEFAULT -1
,
    ServerThreads TINYINT  DEFAULT 5
,
    ManagerRetry INTEGER  DEFAULT 10
,
    CallTimeout INTEGER  DEFAULT 240
,
    Recovery BOOLEAN  DEFAULT TRUE
,
    AutoloadSharedLibs VARCHAR (64)  NULL 
,
	CONSTRAINT ContainerAltKey UNIQUE (ContainerName, Path, ConfigurationId),
    CONSTRAINT ContainerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ContainerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig,
    CONSTRAINT ContainerImplLang CHECK (ImplLang IN ('java', 'cpp', 'py')),
    CONSTRAINT ContainerRealTimeType CHECK (RealTimeType IN ('NONE', 'ABM', 'CORR'))
);
CREATE  TABLE ComponentType (
    ComponentTypeId INTEGER IDENTITY
,
    IDL VARCHAR (256)  NOT NULL 
,
    Schema LONGVARCHAR  NULL 
,
	CONSTRAINT ComponTAltKey UNIQUE (IDL)
);
CREATE  TABLE Component (
    ComponentId INTEGER IDENTITY
,
    ComponentName VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    ContainerId INTEGER  NOT NULL 
,
    ComponentInterfaceId INTEGER  NOT NULL 
,
    ImplLang VARCHAR (6)  NOT NULL 
,
    RealTime BOOLEAN  NOT NULL 
,
    Code VARCHAR (256)  NOT NULL 
,
    Path VARCHAR (256)  NOT NULL 
,
    IsAutostart BOOLEAN  NOT NULL 
,
    IsDefault BOOLEAN  NOT NULL 
,
    IsStandaloneDefined BOOLEAN  NULL 
,
    KeepAliveTime INTEGER  NOT NULL 
,
    MinLogLevel TINYINT  DEFAULT -1
,
    MinLogLevelLocal TINYINT  DEFAULT -1
,
    XMLDoc LONGVARCHAR  NULL 
,
	CONSTRAINT ComponentAltKey UNIQUE (Path, ComponentName, ConfigurationId),
    CONSTRAINT ComponentIDL FOREIGN KEY (ComponentInterfaceId) REFERENCES ComponentType,
    CONSTRAINT ComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComponentImplLang CHECK (ImplLang IN ('java', 'cpp', 'py'))
);
CREATE  TABLE LRUType (
    LRUName VARCHAR (128)  NOT NULL 
,
    FullName VARCHAR (256)  NOT NULL 
,
    ICD VARCHAR (256)  NOT NULL 
,
    ICDDate BIGINT  NOT NULL 
,
    Description LONGVARCHAR  NOT NULL 
,
    Notes LONGVARCHAR  NULL 
,
    CONSTRAINT LRUTypeKey PRIMARY KEY (LRUName)
);
CREATE  TABLE AssemblyType (
    AssemblyName VARCHAR (256)  NOT NULL 
,
    LRUName VARCHAR (128)  NOT NULL 
,
    FullName VARCHAR (256)  NOT NULL 
,
    NodeAddress VARCHAR (16)  NOT NULL 
,
    ChannelNumber TINYINT  NOT NULL 
,
    BaseAddress VARCHAR (16)  NULL 
,
    Description LONGVARCHAR  NOT NULL 
,
    Notes LONGVARCHAR  NULL 
,
    ComponentTypeId INTEGER  NOT NULL 
,
    CONSTRAINT AssemblyTypeKey PRIMARY KEY (AssemblyName),
    CONSTRAINT AssemblyTypeLRUName FOREIGN KEY (LRUName) REFERENCES LRUType,
    CONSTRAINT AssemblyTypeComponent FOREIGN KEY (ComponentTypeId) REFERENCES ComponentType
);
CREATE  TABLE PropertyType (
    PropertyTypeId INTEGER IDENTITY
,
    PropertyName VARCHAR (128)  NOT NULL 
,
    AssemblyName VARCHAR (256)  NOT NULL 
,
    DataType VARCHAR (16)  NOT NULL 
,
    TableName VARCHAR (128)  NOT NULL 
,
    RCA VARCHAR (16)  NOT NULL 
,
    TeRelated BOOLEAN  NOT NULL 
,
    RawDataType VARCHAR (24)  NOT NULL 
,
    WorldDataType VARCHAR (24)  NOT NULL 
,
    Units VARCHAR (24)  NULL 
,
    Scale DOUBLE  NULL 
,
    Offset DOUBLE  NULL 
,
    MinRange DOUBLE  NULL 
,
    MaxRange DOUBLE  NULL 
,
    SamplingInterval FLOAT  NOT NULL 
,
    GraphMin FLOAT  NULL 
,
    GraphMax FLOAT  NULL 
,
    GraphFormat VARCHAR (16)  NULL 
,
    GraphTitle VARCHAR (256)  NULL 
,
    Description LONGVARCHAR  NOT NULL 
,
	CONSTRAINT PropertyTypeAltKey UNIQUE (PropertyName, AssemblyName),
    CONSTRAINT PropertyTypeAssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType,
    CONSTRAINT PropertyTypeDatatype CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer', 'enum', 'blob')),
    CONSTRAINT PropertyTypeTableName CHECK (TableName IN ('FloatProperty', 'DoubleProperty', 'BooleanProperty', 'StringProperty', 'IntegerProperty', 'EnumProperty', 'BLOBProperty'))
);
CREATE  TABLE BACIPropertyType (
    PropertyTypeId INTEGER IDENTITY
,
    PropertyName VARCHAR (128)  NOT NULL 
,
    AssemblyName VARCHAR (256)  NOT NULL 
,
    description LONGVARCHAR  NOT NULL 
,
    format VARCHAR (16)  NOT NULL 
,
    units VARCHAR (24)  NOT NULL 
,
    resolution INTEGER  NOT NULL 
,
    archive_priority INTEGER  NOT NULL 
,
    archive_min_int DOUBLE  NOT NULL 
,
    archive_max_int DOUBLE  NOT NULL 
,
    default_timer_trig DOUBLE  NOT NULL 
,
    min_timer_trig DOUBLE  NOT NULL 
,
    initialize_devio BOOLEAN  NOT NULL 
,
    min_delta_trig DOUBLE  NULL 
,
    default_value LONGVARCHAR  NOT NULL 
,
    graph_min DOUBLE  NULL 
,
    graph_max DOUBLE  NULL 
,
    min_step DOUBLE  NULL 
,
    archive_delta DOUBLE  NOT NULL 
,
    alarm_high_on DOUBLE  NULL 
,
    alarm_low_on DOUBLE  NULL 
,
    alarm_high_off DOUBLE  NULL 
,
    alarm_low_off DOUBLE  NULL 
,
    alarm_timer_trig DOUBLE  NULL 
,
    min_value DOUBLE  NULL 
,
    max_value DOUBLE  NULL 
,
    bitDescription LONGVARCHAR  NULL 
,
    whenSet LONGVARCHAR  NULL 
,
    whenCleared LONGVARCHAR  NULL 
,
    statesDescription LONGVARCHAR  NULL 
,
    condition LONGVARCHAR  NULL 
,
    alarm_on LONGVARCHAR  NULL 
,
    alarm_off LONGVARCHAR  NULL 
,
    Data LONGVARCHAR  NULL 
,
	CONSTRAINT BACIPrTAltKey UNIQUE (PropertyName, AssemblyName),
    CONSTRAINT BACIPropertyTypeAssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
);
CREATE  TABLE EnumWord (
    PropertyTypeId INTEGER  NOT NULL 
,
    OrderNumber TINYINT  NOT NULL 
,
    Word VARCHAR (128)  NOT NULL 
,
    CONSTRAINT EnumWordKey PRIMARY KEY (PropertyTypeId, OrderNumber),
    CONSTRAINT EnumWordPropertyType FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE Assembly (
    AssemblyId INTEGER IDENTITY
,
    AssemblyName VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    SerialNumber VARCHAR (256)  NOT NULL 
,
    Data LONGVARCHAR  NULL 
,
	CONSTRAINT AssemblyAltKey UNIQUE (SerialNumber, ConfigurationId),
    CONSTRAINT AssemblyConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT AssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
);
CREATE  TABLE FloatProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value FLOAT  NOT NULL 
,
    CONSTRAINT FloatPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT FloatPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT FloatPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE StringProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value VARCHAR (24)  NOT NULL 
,
    CONSTRAINT StringPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT StringPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT StringPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BooleanProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value BOOLEAN  NOT NULL 
,
    CONSTRAINT BooleaPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BooleanPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT BooleanPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE DoubleProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value DOUBLE  NOT NULL 
,
    CONSTRAINT DoublePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT DoublePropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT DoublePropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE IntegerProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value INTEGER  NOT NULL 
,
    CONSTRAINT IntegePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT IntegerPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT IntegerPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE EnumProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    Value TINYINT  NOT NULL 
,
    CONSTRAINT EnumPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT EnumPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT EnumPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BlobProperty (
    AssemblyId INTEGER  NOT NULL 
,
    PropertyTypeId INTEGER  NOT NULL 
,
    SampleTime BIGINT  NOT NULL 
,
    DataType VARCHAR (8)  NOT NULL 
,
    NumberItems SMALLINT  NOT NULL 
,
    Value VARBINARY  NOT NULL 
,
    CONSTRAINT BlobPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BlobPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT BlobPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType,
    CONSTRAINT BlobPropertyValue CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer'))
);
CREATE  TABLE BaseElement (
    BaseElementId INTEGER IDENTITY
,
    BaseId INTEGER  NOT NULL 
,
    BaseType VARCHAR (24)  NOT NULL 
,
    BaseElementName VARCHAR (24)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
	CONSTRAINT BaseElementAltKey UNIQUE (BaseId, BaseType, ConfigurationId),
    CONSTRAINT BEConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT BEType CHECK (BaseType IN ('Antenna', 'Pad', 'CorrQuadrant', 'FrontEnd', 'WeatherStation', 'CentralRack', 'MasterClock', 'HolographyTower', 'Array'))
);
CREATE  TABLE BaseElementOnline (
    BaseElementOnlineId INTEGER IDENTITY
,
    BaseElementId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
	CONSTRAINT BaseElOAltKey UNIQUE (BaseElementId, ConfigurationId, StartTime),
    CONSTRAINT BEOnlineId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT BEOnlineConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Antenna (
    AntennaId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    AntennaName VARCHAR (128)  NOT NULL 
,
    AntennaType VARCHAR (4)  NOT NULL 
,
    DishDiameter DOUBLE  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    XPosition DOUBLE  NOT NULL 
,
    YPosition DOUBLE  NOT NULL 
,
    ZPosition DOUBLE  NOT NULL 
,
    XOffset DOUBLE  NOT NULL 
,
    YOffset DOUBLE  NOT NULL 
,
    ZOffset DOUBLE  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT AntennaAltKey UNIQUE (AntennaName, ConfigurationId),
    CONSTRAINT AntennaConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT AntennaBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT AntennaComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT AntennaType CHECK (AntennaType IN ('VA', 'AEC', 'ACA'))
);
CREATE  TABLE Pad (
    PadId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    PadName VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    XPosition DOUBLE  NOT NULL 
,
    YPosition DOUBLE  NOT NULL 
,
    ZPosition DOUBLE  NOT NULL 
,
	CONSTRAINT PadAltKey UNIQUE (PadName, ConfigurationId),
    CONSTRAINT PadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT PadBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE CorrQuadrant (
    CorrQuadrantId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    CorrName VARCHAR (128)  NOT NULL 
,
    Quadrant TINYINT  NOT NULL 
,
    NumberOfAntennas TINYINT  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
    NumberOfRacks TINYINT  NOT NULL 
,
    NumberOfBins TINYINT  NOT NULL 
,
	CONSTRAINT CorrQuadrantAltKey UNIQUE (CorrName, Quadrant, ConfigurationId),
    CONSTRAINT CorrQuadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT CorrQuadBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT CorrQuadComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT CorrQuadNumber CHECK (Quadrant IN ('0', '1', '2', '3'))
);
CREATE  TABLE CorrQuadrantRack (
    CorrQuadrantRackId INTEGER IDENTITY
,
    CorrQuadrantId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    RackName VARCHAR (128)  NOT NULL 
,
    NumberOfBins TINYINT  NOT NULL 
,
	CONSTRAINT CorrQuRAltKey UNIQUE (RackName, CorrQuadrantId, ConfigurationId),
    CONSTRAINT CorrQuadRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE CorrQuadrantBin (
    CorrQuadrantBinId INTEGER IDENTITY
,
    CorrQuadrantRackId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    BinName VARCHAR (128)  NOT NULL 
,
    NumberOfCards SMALLINT  NOT NULL 
,
	CONSTRAINT CorrQuBAltKey UNIQUE (BinName, CorrQuadrantRackId, ConfigurationId),
    CONSTRAINT CorrQuadBinConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE FrontEnd (
    FrontEndId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    FrontEndName VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT FrontEndAltKey UNIQUE (FrontEndName, ConfigurationId),
    CONSTRAINT FrontEndConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT FrontEndBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT FrontEndComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE WeatherStation (
    WeatherStationId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    SerialNumber VARCHAR (128)  NOT NULL 
,
    WeatherStationName VARCHAR (128)  NOT NULL 
,
    WeatherStationType VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    XPosition DOUBLE  NOT NULL 
,
    YPosition DOUBLE  NOT NULL 
,
    ZPosition DOUBLE  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT WeatheSAltKey UNIQUE (WeatherStationName, ConfigurationId),
    CONSTRAINT WeatherStationComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT WeatherStationConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT WeatherStationBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE CentralRack (
    CentralRackId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    CentralRackName VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT CentralRackAltKey UNIQUE (CentralRackName, ConfigurationId),
    CONSTRAINT CentralRackComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT CentralRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT CentralRackBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE MasterClock (
    MasterClockId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    MasterClockName VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT MasterClockAltKey UNIQUE (MasterClockName, ConfigurationId),
    CONSTRAINT MasterClockComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT MasterClockConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT MasterClockBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE HolographyTower (
    HolographyTowerId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    HolographyTowerName VARCHAR (128)  NOT NULL 
,
    CommissionDate BIGINT  NOT NULL 
,
    XPosition DOUBLE  NOT NULL 
,
    YPosition DOUBLE  NOT NULL 
,
    ZPosition DOUBLE  NOT NULL 
,
	CONSTRAINT HologrTAltKey UNIQUE (HolographyTowerName, ConfigurationId),
    CONSTRAINT HolographyTowerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT HolographyTowerBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE Array (
    ArrayId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    ArrayName VARCHAR (128)  NOT NULL 
,
    Type VARCHAR (9)  NOT NULL 
,
    UserId VARCHAR (256)  NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
	CONSTRAINT ArrayAltKey UNIQUE (ArrayName, StartTime, ConfigurationId),
    CONSTRAINT ArrayConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ArrayComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT ArrayBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT ArrayType CHECK (Type IN ('automatic', 'manual'))
);
CREATE  TABLE AntennaToPad (
    AntennaId INTEGER  NOT NULL 
,
    PadId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    Planned BOOLEAN  NOT NULL 
,
    CONSTRAINT AntennaToPadKey PRIMARY KEY (AntennaId, PadId, StartTime),
    CONSTRAINT AntennaToPadAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToPadPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaPointingModel (
    PointingModelId INTEGER IDENTITY
,
    AntennaId INTEGER  NOT NULL 
,
    PadId INTEGER  NOT NULL 
,
    StartValidTime BIGINT  NOT NULL 
,
    EndValidTime BIGINT  NULL 
,
    AsdmUID VARCHAR (256)  NOT NULL 
,
	CONSTRAINT AntennPMAltKey UNIQUE (AntennaId, PadId, StartValidTime),
    CONSTRAINT AntennaPMAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaPMPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaPointingModelTerm (
    PointingModelId INTEGER  NOT NULL 
,
    CoeffName VARCHAR (128)  NOT NULL 
,
    CoeffValue FLOAT  NOT NULL 
,
    CoeffError FLOAT  NOT NULL 
,
    CONSTRAINT AntennPMTKey PRIMARY KEY (PointingModelId, CoeffName),
    CONSTRAINT AntPMTermPointingModelId FOREIGN KEY (PointingModelId) REFERENCES AntennaPointingModel
);
CREATE  TABLE AntennaDelayTerms (
    AntennaId INTEGER  NOT NULL 
,
    PadId INTEGER  NOT NULL 
,
    TimeOfMeasurement BIGINT  NOT NULL 
,
    ParameterName VARCHAR (128)  NOT NULL 
,
    ParameterValue DOUBLE  NOT NULL 
,
    CONSTRAINT AntennDTKey PRIMARY KEY (AntennaId, PadId, TimeOfMeasurement),
    CONSTRAINT AntennaDTAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaDTPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaToFrontEnd (
    AntennaId INTEGER  NOT NULL 
,
    FrontEndId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    CONSTRAINT AntennTFEKey PRIMARY KEY (AntennaId, FrontEndId, StartTime),
    CONSTRAINT AntennaToFEAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToFEFrontEndId FOREIGN KEY (FrontEndId) REFERENCES FrontEnd
);
CREATE  TABLE AntennaToArray (
    AntennaId INTEGER  NOT NULL 
,
    ArrayId INTEGER  NOT NULL 
,
    CONSTRAINT AntennTAKey PRIMARY KEY (AntennaId, ArrayId),
    CONSTRAINT AntennaToArrayAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToArrayArrayid FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE SBExecution (
    ArrayId INTEGER  NOT NULL 
,
    SbUID VARCHAR (256)  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    CONSTRAINT SBExecutionKey PRIMARY KEY (ArrayId, SbUID, StartTime),
    CONSTRAINT SBExecutionArrayId FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE AntennaToCorr (
    AntennaId INTEGER  NOT NULL 
,
    CorrId INTEGER  NOT NULL 
,
    AntennaNumber TINYINT  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    CONSTRAINT AntennTCKey PRIMARY KEY (AntennaId, CorrId, StartTime),
    CONSTRAINT AntToCorrAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntToCorrId FOREIGN KEY (CorrId) REFERENCES CorrQuadrant
);
CREATE  TABLE BaseElementAssemblyGroup (
    GroupName VARCHAR (128)  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    CONSTRAINT BaseElAGKey PRIMARY KEY (GroupName, BaseElementId),
    CONSTRAINT BEAssemblyGroupId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE BaseElementAssemblyList (
    AssemblyId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    GroupName VARCHAR (128)  NULL 
,
    OrderTag SMALLINT  NULL 
,
    RoleName VARCHAR (128)  NULL 
,
    ChannelNumber TINYINT  NULL 
,
    NodeAddress VARCHAR (16)  NULL 
,
    BaseAddress VARCHAR (16)  NULL 
,
    CONSTRAINT BaseElALKey PRIMARY KEY (AssemblyId, BaseElementId),
    CONSTRAINT BEAssemblyListId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT BEAssemblyListAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
CREATE  TABLE MasterComponent (
    MasterComponentId INTEGER IDENTITY
,
    ComponentId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    SubsystemName VARCHAR (256)  NOT NULL 
,
	CONSTRAINT MasterCAltKey UNIQUE (ComponentId, ConfigurationId),
    CONSTRAINT MComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT MComponentId FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE ComputerExecution (
    ComputerExecutionId INTEGER IDENTITY
,
    ComputerId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
	CONSTRAINT ComputEAltKey UNIQUE (ComputerId, ConfigurationId, StartTime),
    CONSTRAINT ComputerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComputerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
CREATE  TABLE ContainerExecution (
    ContainerExecutionId INTEGER IDENTITY
,
    ContainerId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    ComputerId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
	CONSTRAINT ContaiEAltKey UNIQUE (ContainerId, ComputerId, StartTime, ConfigurationId),
    CONSTRAINT ContainerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ContainerExecContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT ContainerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
CREATE  TABLE ComponentExecution (
    ComponentExecutionId INTEGER IDENTITY
,
    ComponentExecutionAcsId BIGINT  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    ContainerId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    BaseElementOnlineId INTEGER  NULL 
,
    AssemblyId INTEGER  NULL 
,
	CONSTRAINT ComponEAltKey UNIQUE (ComponentId, StartTime, ConfigurationId),
    CONSTRAINT ComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComponentExecComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT ComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT ComponentExecBaseElement FOREIGN KEY (BaseElementOnlineId) REFERENCES BaseElementOnline,
    CONSTRAINT ComponentExecAssembly FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
CREATE  TABLE MasterComponentExecution (
    MasterComponentExecId INTEGER IDENTITY
,
    MasterComponentId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    ContainerId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
	CONSTRAINT MasterCEAltKey UNIQUE (MasterComponentId, ContainerId, StartTime, ConfigurationId),
    CONSTRAINT MComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT MComponentExecComponent FOREIGN KEY (MasterComponentId) REFERENCES MasterComponent,
    CONSTRAINT MComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container
);
CREATE  TABLE ACS (
    ACSId INTEGER IDENTITY
,
    ACSVersion VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    ComputerId INTEGER  NOT NULL 
,
    Var1 VARCHAR (256)  NULL 
,
    Var2 VARCHAR (256)  NULL 
,
    Var3 VARCHAR (256)  NULL 
,
	CONSTRAINT ACSAltKey UNIQUE (ACSVersion, ConfigurationId, ComputerId),
    CONSTRAINT ACSComputer FOREIGN KEY (ComputerId) REFERENCES Computer,
    CONSTRAINT ACSConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE ACSExecution (
    ACSId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    CONSTRAINT ACSExecutionKey PRIMARY KEY (ACSId, ConfigurationId, StartTime),
    CONSTRAINT ACSExecACS FOREIGN KEY (ACSId) REFERENCES ACS,
    CONSTRAINT ACSExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NotificationChannel (
    NCId INTEGER IDENTITY
,
    NCName VARCHAR (256)  NOT NULL 
,
    SubsystemName VARCHAR (256)  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    QOS1 VARCHAR (256)  NULL 
,
    QOS2 VARCHAR (256)  NULL 
,
    QOS3 VARCHAR (256)  NULL 
,
	CONSTRAINT NotifiCAltKey UNIQUE (NCName, ConfigurationId),
    CONSTRAINT NCConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NCExecution (
    NCId INTEGER  NOT NULL 
,
    ACSId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    CONSTRAINT NCExecutionKey PRIMARY KEY (NCId, ConfigurationId, StartTime),
    CONSTRAINT NCExecNC FOREIGN KEY (NCId) REFERENCES NotificationChannel,
    CONSTRAINT NCExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT NCExecACS FOREIGN KEY (ACSId) REFERENCES ACS
);
CREATE  TABLE Startup (
    StartupId INTEGER IDENTITY
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartupName VARCHAR (256)  NOT NULL 
,
	CONSTRAINT StartupAltKey UNIQUE (StartupName, ConfigurationId),
    CONSTRAINT StartupConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE SystemExecution (
    StartupId INTEGER  NOT NULL 
,
    ConfigurationId INTEGER  NOT NULL 
,
    StartTime BIGINT  NOT NULL 
,
    EndTime BIGINT  NULL 
,
    NormalStart BOOLEAN  NOT NULL 
,
    NormalTermination BOOLEAN  NOT NULL 
,
    CONSTRAINT SystemEKey PRIMARY KEY (StartupId, ConfigurationId, StartTime),
    CONSTRAINT SystemExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT SystemExecStartupId FOREIGN KEY (StartupId) REFERENCES Startup
);
CREATE  TABLE DeploymentStartup (
    StartupId INTEGER  NOT NULL 
,
    OrderNumber SMALLINT  NOT NULL 
,
    DependsOn SMALLINT  NOT NULL 
,
    AllWait BOOLEAN  NOT NULL 
,
    StartupType VARCHAR (10)  NOT NULL 
,
    NameId SMALLINT  NOT NULL 
,
    AssociatedType VARCHAR (10)  NULL 
,
    AssociatedId SMALLINT  NULL 
,
    CONSTRAINT DeploySKey PRIMARY KEY (StartupId, OrderNumber),
    CONSTRAINT DeployStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT DeployStartupType CHECK (StartupType IN ('ACS', 'computer', 'container', 'component')),
    CONSTRAINT DeployAssociatedType CHECK (AssociatedType IN ('computer', 'container'))
);
CREATE  TABLE BaseElementStartup (
    BaseElementId INTEGER  NOT NULL 
,
    StartupId INTEGER  NOT NULL 
,
    CONSTRAINT BaseElSKey PRIMARY KEY (BaseElementId, StartupId),
    CONSTRAINT BEStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT BEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssociatedBaseElement (
    BaseElementId INTEGER  NOT NULL 
,
    StartupId INTEGER  NOT NULL 
,
    AssociatedId INTEGER  NOT NULL 
,
    AssociationType VARCHAR (24)  NOT NULL 
,
    CONSTRAINT AssociBEKey PRIMARY KEY (BaseElementId, StartupId, AssociatedId),
    CONSTRAINT ABEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT ABEStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT ABEAssociated FOREIGN KEY (AssociatedId) REFERENCES BaseElement,
    CONSTRAINT ABEAssociationType CHECK (AssociationType IN ('AntennaToPad', 'AntennaToFrontEnd', 'AntennaToCorr'))
);
CREATE  TABLE AssemblyGroupStartup (
    GroupName VARCHAR (128)  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    StartupId INTEGER  NOT NULL 
,
    CONSTRAINT AssembGSKey PRIMARY KEY (GroupName, BaseElementId, StartupId),
    CONSTRAINT AGStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT AGStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssemblyStartup (
    AssemblyId INTEGER  NOT NULL 
,
    BaseElementId INTEGER  NOT NULL 
,
    StartupId INTEGER  NOT NULL 
,
    ComponentId INTEGER  NOT NULL 
,
    GroupName VARCHAR (128)  NULL 
,
    OrderTag SMALLINT  NULL 
,
    RoleName VARCHAR (128)  NULL 
,
    ChannelNumber TINYINT  NULL 
,
    NodeAddress VARCHAR (16)  NULL 
,
    BaseAddress VARCHAR (16)  NULL 
,
    CONSTRAINT AssembSKey PRIMARY KEY (AssemblyId, BaseElementId, StartupId),
    CONSTRAINT AssemblyStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT AssemblyStartupIdA FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT AssemblyStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT AssemblyStartupComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE SystemCounters (
    ConfigurationId INTEGER  NOT NULL 
,
    UpdateTime BIGINT  NOT NULL 
,
    AutoArrayCount SMALLINT  NOT NULL 
,
    ManArrayCount SMALLINT  NOT NULL 
,
    DataCaptureCount SMALLINT  NOT NULL 
,
    CONSTRAINT SystemCKey PRIMARY KEY (ConfigurationId),
    CONSTRAINT SystemCountersConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Location (
    LocationId INTEGER IDENTITY
,
    Building VARCHAR (256)  NULL 
,
    Floor VARCHAR (128)  NULL 
,
    Room VARCHAR (256)  NULL 
,
    Mnemonic VARCHAR (256)  NULL 
,
    LocationPosition VARCHAR (256)  NULL 
);
CREATE  TABLE Contact (
    ContactId INTEGER IDENTITY
,
    ContactName VARCHAR (256)  NOT NULL 
,
    Email VARCHAR (256)  NULL 
,
    Gsm VARCHAR (256)  NULL 
,
	CONSTRAINT ContactAltKey UNIQUE (ContactName)
);
CREATE  TABLE Category (
    CategoryName VARCHAR (128)  NOT NULL 
,
    Description LONGVARCHAR  NOT NULL 
,
    Path VARCHAR (256)  NOT NULL 
,
    IsDefault BOOLEAN  NOT NULL 
,
    CONSTRAINT CategoryKey PRIMARY KEY (CategoryName)
);
CREATE  TABLE FaultFamily (
    FaultFamilyId INTEGER IDENTITY
,
    FamilyName VARCHAR (256)  NOT NULL 
,
    CategoryName VARCHAR (128)  NULL 
,
    AlarmSource VARCHAR (256)  DEFAULT 'ALARM_SYSTEM_SOURCES'
,
    HelpURL VARCHAR (256)  NULL 
,
    ContactId INTEGER  NOT NULL 
,
	CONSTRAINT FaultFamilyAltKey UNIQUE (FamilyName),
    CONSTRAINT FaultFamilyContact FOREIGN KEY (ContactId) REFERENCES Contact,
    CONSTRAINT FaultFamilyCategoryName FOREIGN KEY (CategoryName) REFERENCES Category
);
CREATE  TABLE FaultMember (
    MemberName VARCHAR (256)  NOT NULL 
,
    FaultFamilyId INTEGER  NOT NULL 
,
    LocationId INTEGER  NULL 
,
    CONSTRAINT FaultMemberKey PRIMARY KEY (MemberName),
    CONSTRAINT MemberFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily,
    CONSTRAINT LocationRef FOREIGN KEY (LocationId) REFERENCES Location
);
CREATE  TABLE DefaultMember (
    DefaultMemberId INTEGER  NOT NULL 
,
    FaultFamilyId INTEGER  NOT NULL 
,
    LocationID INTEGER  NULL 
,
    CONSTRAINT DefaulMKey PRIMARY KEY (DefaultMemberId),
    CONSTRAINT DefaultMemberFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily,
    CONSTRAINT DefaultMemberLocationRef FOREIGN KEY (LocationId) REFERENCES Location
);
CREATE  TABLE FaultCode (
    FaultCodeId INTEGER IDENTITY
,
    FaultFamilyId INTEGER  NOT NULL 
,
    CodeValue INTEGER  NOT NULL 
,
    Priority INTEGER  NOT NULL 
,
    Cause VARCHAR (256)  NULL 
,
    Action LONGVARCHAR  NULL 
,
    Consequence LONGVARCHAR  NULL 
,
    ProblemDescription LONGVARCHAR  NOT NULL 
,
    IsInstant BOOLEAN  NOT NULL 
,
	CONSTRAINT FaultCodeAltKey UNIQUE (FaultFamilyId, CodeValue),
    CONSTRAINT CodeFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily,
    CONSTRAINT PriorityValue CHECK (Priority IN (0,1,2,3))
);
