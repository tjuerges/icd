// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef CURRENTWEATHER_IDL
#define CURRENTWEATHER_IDL

#pragma prefix "alma"

#include <ControlExceptions.idl> // for the exception declarations
#include <acscommon.idl> // for ACS::Time
#include <acscomponent.idl>


module Control
{
    /// This interface provides functions that return the current
    /// weather for ALMA. It aggregates the information from a number of
    /// weather stations and provides and the best estimate of the
    /// weather at any particular location for ALMA. It is a read-only
    /// interface and to manipulate the type of information that is
    /// returned by these functions, such as turning on simulation or
    /// enabling specific weather stations, you need to use the
    /// WeatherStationController interface.

    interface CurrentWeather : ACS::ACSComponent
    {
        /// The functions in this section allow you to get the pressure
        
        struct Pressure {
            double value; // The pressure in Pascals (1 hPa = 100Pa)
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. Zero if
            // the weather station is disabled.
        };

        /// Return the pressure, in Pascals (1 hectoPascal is 100 Pascals), at
        /// the specified pad . This is an interpolated value that is the best
        /// estimate for the value at this location and ignores any disabled
        /// weather stations. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        Pressure getPressureAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the pressure, in Pascals (1 hectoPascal is 100 Pascals), at
        /// the specified weather station. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        Pressure getPressure(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section allow you to get the wind-speed

        struct WindSpeed {
            double value; // The wind-speed in meters per second
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. Zero if
            // the weather station is disabled.
        };

        /// Return the wind-speed, in m/s, at the specified pad. This is
        /// an interpolated value that is the best estimate for the value at
        /// this location and ignores any disabled weather stations. All names
        /// are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        WindSpeed getWindSpeedAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the wind-speed, in m/s, at the specified weather
        /// station. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        WindSpeed getWindSpeed(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section allow you to get the wind direction

        struct WindDirection {
            double value; // The wind-direction in radians. 0 is from the
                          // North and pi/2 is from the East
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. Zero if
            // the weather station is disabled.
        };

        /// Return the wind direction, in radians, at the specified pad. 0 is
        /// from the North and pi/2 is from the East. This is an interpolated
        /// value that is the best estimate for the value at this location and
        /// ignores any disabled weather stations. All names are case
        /// insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        WindDirection getWindDirectionAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the wind-direction, in radians, at the specified weather
        /// station. 0 is from the North and pi/2 is from the East. All names
        /// are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        WindDirection getWindDirection(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section allow you to get the temperature

        struct Temperature {
            double value; // The temperature in degrees C.
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. Zero if
            // the weather station is disabled.
        };

        /// Return the temperature, in degrees C, at the specified pad. This is
        /// an interpolated value that is the best estimate for the value at
        /// this location and ignores any disabled weather stations. All names
        /// are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        Temperature getTemperatureAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the temperature, in degrees C, at the specified weather
        /// station. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        Temperature getTemperature(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section allow you to get the dew point

        struct DewPoint {
            double value; // The dew point in degrees C.
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. Zero if
            // the weather station is disabled.
        };

        /// Return the dew point, in degrees C, at the specified pad. This is
        /// an interpolated value that is the best estimate for the value at
        /// this location and ignores any disabled weather stations. All names
        /// are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        DewPoint getDewPointAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the dew point, in degrees C, at the specified weather
        /// station. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        DewPoint getDewPoint(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section allow you to get the relative
        /// humidity. Relative humidity is a derived value that depends on the
        /// temperature and dew point using the equation.
        /// rh = exp(log(10.0)*7.5*   dewPoint/(237.7+dewPoint)-
        ///          log(10.0)*7.5*temperature/(237.7+temperature));

        struct Humidity {
            double value; // The relative humidity (normally between 0 an 1)
            boolean valid; // False if the specified weather station is
                           // disabled. Simulated values are valid!
            ACS::Time timestamp; // When this value was measured. This is the
                                 // oldest of the temperaure and dew-point
                                 // timestamps. Zero if the weather station is
                                 // disabled.
        };

        /// Return the relative humidity (normally between 0 and 1), at the
        /// specified pad . This is an interpolated value that is the best
        /// estimate for the value at this location and ignores any disabled
        /// weather stations. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the pad name is not recognized.
        Humidity getHumidityAtPad(in string padName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// Return the  relative humidity (normally between 0 and 1), at
        /// the specified weather station. All names are case insensitive.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the weather station name is not recognized.
        Humidity getHumidity(in string wsName) 
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section provide information on the
        /// individual weather stations that compose this weather station
        /// controller.
        struct Location {
            string name;  // weather station name
            // The distance, in meters, from the centre of the earth. These
            // are in the ITRF reference frame
            double x; 
            double y; 
            double z; 
        };
        typedef sequence<Location> LocationSeq;

        /// Returns the location of all the weather stations that the
        /// weather station controller is aware of.
        LocationSeq weatherStationLocations();

        /// Returns true if the specified weather station is enabled
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the antenna or weather station name is not recognized.
        boolean isEnabled(in string wsName)
            raises(ControlExceptions::IllegalParameterErrorEx);

        /// The functions in this section provide information on the simulation
        /// of the primary weather station parameters. Derived parameters are
        /// simulated by setting all the primary parameters to a simulated
        /// value.

        /// Returns true if the pressure is being simulated
        boolean isPressureSimulated();

        /// Returns true if the wind-speed is being simulated
        boolean isWindSpeedSimulated();

        /// Returns true if the wind-direction is being simulated
        boolean isWindDirectionSimulated();

        /// Returns true if the temperature is being simulated
        boolean isTemperatureSimulated();

        /// Returns true if the dew-point is being simulated
        boolean isDewPointSimulated();

        /// Returns true if the humidity is being simulated. This is
        /// true if either the temperature or dew point is simulated.
        boolean isHumiditySimulated();
    };
};
#endif // CURRENTWEATHER_IDL
