#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#

#*******************************************************************************
# This Makefile follows VLT Standards (see Makefile(5) for more).
#*******************************************************************************
# REMARKS
#    None
#------------------------------------------------------------------------

# Jarfiles and their directories
#
JARFILES= TMCDB
TMCDB_DIRS=alma
TMCDB_EXTRAS=alma/TMCDB/generated/hibernate-mappings.hbm.xml \
             SQL/DropAllTables.sql \
             SQL/DropAllOracleSequences.sql \
             SQL/CreateHsqldbTables.sql \
             SQL/CreateOracleTables.sql

#
# java sources in Jarfile on/off
DEBUG= on

ACSERRDEF = TmcdbErrType
# 
# IDL Files and flags
# 
IDL_FILES = TMCDBBase_IDL TMCDB_IDL
TMCDBBase_IDLStubs_LIBS = asdmIDLTypesStubs
TMCDB_IDLStubs_LIBS = TMCDBBase_IDLStubs


CDB_SCHEMAS = ControlDevice


# Scripts (public and local)
# ----------------------------
SCRIPTS         =

#
# other files to be installed
#----------------------------

INSTALL_FILES =


#
# list of all possible C-sources (used to create automatic dependencies)
# ------------------------------
CSOURCENAMES = \
	$(foreach exe, $(EXECUTABLES) $(EXECUTABLES_L), $($(exe)_OBJECTS)) \
	$(foreach rtos, $(RTAI_MODULES) , $($(rtos)_OBJECTS)) \
	$(foreach lib, $(LIBRARIES) $(LIBRARIES_L), $($(lib)_OBJECTS))

#
#>>>>> END OF standard rules

#
# INCLUDE STANDARDS
# -----------------

MAKEDIRTMP := $(shell searchFile include/acsMakefile)
ifneq ($(MAKEDIRTMP),\#error\#)
   MAKEDIR := $(MAKEDIRTMP)/include
   include $(MAKEDIR)/acsMakefile
endif

#
# TARGETS
# -------
.NOTPARALLEL: all
all:	preant do_all
	@echo " . . . 'all' done" 

clean : clean_all 
	CLASSPATH="" ant clean
	rm -rf ../bin/alma
	rm -rf SQL
	rm -f hibernate.cfg.xml
	@echo " . . . clean done"

clean_dist : clean_all clean_dist_all 
	@echo " . . . clean_dist done"

man   : do_man 
	@echo " . . . man page(s) done"

install : install_all
	@echo " . . . installation done"

preant :
	CLASSPATH="" ant
	@echo " . . . code generation (via ant) done"
	cp -r ../idl/SQL .

all_nogen: do_all
	@echo " . . . 'all_nogen' done"

#___oOo___
