/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File SetEndTime.java
 */
package alma.TMCDB;

import alma.TMCDB.generated.TMCDBBase;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;

public class TMCDBSetEndTime extends TMCDBBase {
    /**
     * Constructor for SetEndTime.
     */	
    public TMCDBSetEndTime () {
    	super();
	}

    /**
     * Initializer for SetEndTime.
     * @throws AcsJTmcdbErrTypeEx 
     */	
    public void initialize () throws AcsJTmcdbSqlEx {
    	super.initialize();
    }

	public void setEndTimeOnArray(int id, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

	public void setEndTimeOnBaseElementOnline(int baseElementOnlineId, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

	public void setEndTimeOnComponentExecution(int componentExecutionId, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

	public void setEndTimeOnComputerExecution(int computerExecutionId, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

	public void setEndTimeOnContainerExecution(int containerExecutionId, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

	public void setEndTimeOnSystemExecution(int configurationId, long startTime, long endTime) throws AcsJTmcdbErrTypeEx {
		// TODO Auto-generated method stub
		
	}

    
}


