/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBAntenna.java
 */
package alma.TMCDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import alma.TMCDB.generated.Antenna;
import alma.TMCDB.generated.AntennaPointingModel;
import alma.TMCDB.generated.AntennaPointingModelTerm;
import alma.TMCDB.generated.AntennaToPad;
import alma.TMCDB.generated.Pad;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateKeyEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;

public class TMCDBAntenna extends TMCDBSetEndTime {

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IMPORTANT NOTE!  In the 'getAntennaPointingModel' method, "band3" has been hard-coded.				//
	// This interface should be changed to return an array of pointing model, one for each receiver band,	//
	// or, change it to ask for a specific receiver band.													//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Get the AntennaToPad for the current antenna/pad.
	protected PreparedStatement sqlGetCurrentAntennaToPad;
	// Get the pointing models for antenna/pad.
	protected PreparedStatement sqlGetRecentPointingModel1;
	protected PreparedStatement sqlGetRecentPointingModel2;
	protected PreparedStatement sqlGetRecentPointingModel3;
	// Get all pointing models for a given antenna. 
	protected PreparedStatement sqlGetAllPointingModels;
	// Get all the pointing model terms for a pointing model.
	protected PreparedStatement sqlGetPointingModelTerms;

    /**
     * Constructor for TMCDBAntenna.
     */	
    public TMCDBAntenna () {
    	super();
	}

    /**
     * Initializer for TMCDBAntenna.
     * @throws AcsJTmcdbSqlEx 
     */	
	public void initialize() throws AcsJTmcdbSqlEx {
		super.initialize();
		try {
			sqlGetPointingModelTerms = conn.prepareStatement("SELECT * FROM AntennaPointingModelTerm WHERE PointingModelId = ?");
			sqlGetAllPointingModels = conn.prepareStatement("SELECT * FROM AntennaPointingModel WHERE AntennaId = ?");
			sqlGetRecentPointingModel1 = conn.prepareStatement("SELECT max(StartValidTime) FROM AntennaPointingModel WHERE AntennaId = ? AND PadId = ?");
			sqlGetRecentPointingModel2 = conn.prepareStatement("SELECT max(StartValidTime) FROM AntennaPointingModel WHERE AntennaId = ?");
			sqlGetRecentPointingModel3 = conn.prepareStatement("SELECT PadId FROM AntennaPointingModel WHERE AntennaId = ? AND StartValidTime = ?");
			sqlGetCurrentAntennaToPad = conn.prepareStatement("SELECT * FROM AntennaToPad WHERE AntennaId = ? and EndTime is NULL and Planned = 'n'");
		} catch (SQLException e) {
			throw new AcsJTmcdbSqlEx (e);
		}
	}

	/**
	 * The getStartupAntennas method supplies the information needed to initialize
	 * all antennas.  Included are the name of the antenna, the pad on which
	 * it resides, the name of front end, the assembly locations in the 
	 * front end, and the assembly locations in the antenna.
	 */
	public StartupAntenna[] getStartUpAntennas() {
		// This is only temporary -- for SS2.
		StartupAntenna[] ant = new StartupAntenna [2];
		
		ant[0] = new StartupAntenna ();
		ant[0].setAntennaName("DV01");
		ant[0].setUiDisplayOrder((short)1);
		ant[0].setPadName("PAD001");
		ant[0].setFrontEndName("none");
		ant[0].setFrontEndAssembly(new AssemblyLocation[0]);
		AssemblyLocation[] locVA = new AssemblyLocation [12];
		for (int i = 0; i < locVA.length; ++i) {
			locVA[i] = new AssemblyLocation ();
			locVA[i].setAssemblyRoleName("");
			locVA[i].setAssemblyTypeName("none");
			locVA[i].setBaseAddress(0);
			locVA[i].setChannelNumber(0);
			locVA[i].setRca(0);
		}
		locVA[0].setAssemblyRoleName("MountController");
		locVA[1].setAssemblyRoleName("LORR");
		locVA[2].setAssemblyRoleName("FLOOG");
		locVA[3].setAssemblyRoleName("DGCK");
		locVA[4].setAssemblyRoleName("LO2A");
		locVA[5].setAssemblyRoleName("LO2B");
		locVA[6].setAssemblyRoleName("LO2C");
		locVA[7].setAssemblyRoleName("LO2D");
		locVA[8].setAssemblyRoleName("DTSTransmitter");
		locVA[9].setAssemblyRoleName("OpticalTelescope");
		locVA[10].setAssemblyRoleName("HoloRx");
		locVA[11].setAssemblyRoleName("HoloDSP");
		ant[0].setAntennaAssembly(locVA);
		
		ant[1] = new StartupAntenna ();
		ant[1].setAntennaName("DA41");
		ant[1].setUiDisplayOrder((short)25);
		ant[1].setPadName("PAD002");
		ant[1].setFrontEndName("none");
		ant[1].setFrontEndAssembly(new AssemblyLocation[0]);
		AssemblyLocation[] locAEC = new AssemblyLocation [10];
		for (int i = 0; i < locAEC.length; ++i) {
			locAEC[i] = new AssemblyLocation ();
			locAEC[i].setAssemblyRoleName("");
			locAEC[i].setAssemblyTypeName("");
			locAEC[i].setBaseAddress(0);
			locAEC[i].setChannelNumber(0);
			locAEC[i].setRca(0);
		}
		locAEC[0].setAssemblyRoleName("MountController");
		locAEC[1].setAssemblyRoleName("LORR");
		locAEC[2].setAssemblyRoleName("FLOOG");
		locAEC[3].setAssemblyRoleName("DGCK");
		locAEC[4].setAssemblyRoleName("LO2A");
		locAEC[5].setAssemblyRoleName("LO2B");
		locAEC[6].setAssemblyRoleName("LO2C");
		locAEC[7].setAssemblyRoleName("LO2D");
		locAEC[8].setAssemblyRoleName("DTSTransmitter");
		locAEC[9].setAssemblyRoleName("OpticalTelescope");
		ant[1].setAntennaAssembly(locAEC);
		return ant;
	}

//	public Antenna getAntenna(String antennaName) throws AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx  {
//		return this.getAntenna(antennaName,configurationId);
//	}
//
//	/**
//	 * The getCurrentAntennaPad method returns the pad data structure for the 
//	 * pad on which the antenna currently resides.  The SQL query is: select 
//	 * the pad for antennaName where AntennaToPad.EndTime is null and 
//	 * AntennaToPad.Planned is ?n?.  If there is no such selection, null is 
//	 * returned.  In this case the antenna is not currently on a pad, i.e. it 
//	 * is being transported or in the workshop.
//	 * 
//	 * @param antennaName The name of the antenna for which the pad is desired
//	 * @return The Pad data structure representing the pad the anetnna is on,
//	 * or null if the antenna is not currently on a pad.
//	 * @throws AcsJTmcdbSqlEx 
//	 * @throws AcsJTmcdbDuplicateKeyEx 
//	 * @throws AcsJTmcdbNoSuchRowEx 
//	 * @throws AcsJTmcdbRowAlreadyExistsEx 
//	 */
//	public Pad getCurrentAntennaPad(String antennaName) throws AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx, AcsJTmcdbRowAlreadyExistsEx  {
//		try {
//			Antenna antenna = getAntenna(antennaName);
//			sqlGetCurrentAntennaToPad.setInt(1,antenna.getAntennaId());
//			ResultSet result = sqlGetCurrentAntennaToPad.executeQuery();
//			if (result.next() == false)
//				return null;
//			AntennaToPad x = new AntennaToPad();
//			assignAntennaToPad(x,result);
//			if (result.next()) {
//				throw new AcsJTmcdbRowAlreadyExistsEx ("TMCDB: Database Error: Duplicate rows in AntennaToPad table for antenna " + antennaName);
//			}
//			return getPad(x.getPadId());
//		} catch (SQLException err) {
//			throw new AcsJTmcdbSqlEx (err);
//		}
//	}
//	
//	/**
//	 * An antenna pointing model is a combination of information about the 
//	 * pointing model itself and the terms in the pointing model.  The data 
//	 * structure defining the pointing model contains:
//	 * <ul>
//	 * <li>String antennaName;
//	 * <li>String padName;
//	 * <li>AntennaPointingModel pointintModel;
//	 * <li>AntennaPointingModelTerm[] term;
//	 * </ul>
//	 * The getPointingModel method returns the current pointing model for the 
//	 * current combination of antenna and pad.  If there is no current 
//	 * pointing model or if the antenna is not on a pad, null is returned.
//	 * 
//	 * @param antennaName
//	 * @return the desired pointing model
//	 * @throws AcsJTmcdbSqlEx 
//	 * @throws AcsJTmcdbDuplicateKeyEx 
//	 * @throws AcsJTmcdbNoSuchRowEx 
//	 * @throws AcsJTmcdbRowAlreadyExistsEx 
//	 */
//	public PointingModel getPointingModel(String antennaName) throws AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx, AcsJTmcdbRowAlreadyExistsEx  {
//		try {
//			Antenna antenna = getAntenna(antennaName);
//			sqlGetCurrentAntennaToPad.setInt(1,antenna.getAntennaId());
//			ResultSet result = sqlGetCurrentAntennaToPad.executeQuery();
//			if (result.next() == false)
//				return null; // there is no current pad.
//			AntennaToPad x = new AntennaToPad();
//			assignAntennaToPad(x,result);
//			if (result.next()) {
//				throw new AcsJTmcdbRowAlreadyExistsEx ("TMCDB: Database Error: Duplicate rows in AntennaToPad table for antenna " + antennaName);
//			}
//			// OK, we've got the antenna and the pad; now get the pointing model.
//			sqlGetRecentPointingModel1.setInt(1, antenna.getAntennaId());
//			sqlGetRecentPointingModel1.setInt(2, x.getPadId());
//			result = sqlGetRecentPointingModel1.executeQuery();
//			if (result.next() == false)
//				return null; // There is no pointing model.
//			Timestamp t = result.getTimestamp(1);
//			if (result.next()) {
//				throw new AcsJTmcdbRowAlreadyExistsEx ("TMCDB: Database Error: Duplicate rows in AntennaPointingModel table for antenna " + antennaName);
//			}
//			AntennaPointingModel model = getAntennaPointingModel(antenna.getAntennaId(),x.getPadId(),"band3",this.convertTimestampToArrayTime(t));
//			if (model.getEndValidTime().get() != 0)
//				return null;
//			PointingModel rtn = new PointingModel ();
//			rtn.setAntennaName(antenna.getAntennaName());
//			rtn.setPadName(getPad(x.getPadId()).getPadName());
//			rtn.setPointingModel(model);
//			ArrayList termList = new ArrayList ();
//			int modelId = model.getPointingModelId();
//			sqlGetPointingModelTerms.setInt(1,modelId);
//			result = sqlGetPointingModelTerms.executeQuery();
//			if (result.next() != false) {
//				do {
//					AntennaPointingModelTerm tmp = new AntennaPointingModelTerm ();
//					assignAntennaPointingModelTerm(tmp,result);
//					termList.add(tmp);
//				} while (result.next());
//				AntennaPointingModelTerm[] terms = new AntennaPointingModelTerm [termList.size()];
//				rtn.setTerm((AntennaPointingModelTerm[])termList.toArray(terms));
//			}
//			return rtn;
//		} catch (SQLException err) {
//			throw new AcsJTmcdbSqlEx(err);
//		}
//	}
//	
//	/**
//	 * The getRecentPointingModel method return the most recent pointing model for 
//	 * the named antenna and its pad, regardless of whether the model is current or not.
//	 * If the antenna is not currently on a pad, the most recent pointing model is returned.
//	 * 
//	 * @param antennaName
//	 * @return
//	 * @throws AcsJTmcdbSqlEx 
//	 * @throws AcsJTmcdbDuplicateKeyEx 
//	 * @throws AcsJTmcdbNoSuchRowEx 
//	 * @throws AcsJTmcdbDuplicateRowEx 
//	 */
//	public PointingModel getRecentPointingModel(String antennaName) throws AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx, AcsJTmcdbDuplicateRowEx  {
//		try {
//			Antenna antenna = getAntenna(antennaName);
//			sqlGetCurrentAntennaToPad.setInt(1,antenna.getAntennaId());
//			ResultSet result = sqlGetCurrentAntennaToPad.executeQuery();
//			int padId = 0;
//			Timestamp t = null;
//			if (result.next() == false) {
//				// In this case, the antenna is not on a pad.
//				if (result.next()) {
//					throw new AcsJTmcdbDuplicateRowEx ("TMCDB: Database Error: Duplicate rows in AntennaToPad table for antenna " + antennaName);
//				}
//				// OK, get the most recent pointing model for this antenna . . . 
//				sqlGetRecentPointingModel2.setInt(1, antenna.getAntennaId());
//				result = sqlGetRecentPointingModel2.executeQuery();
//				if (result.next() == false)
//					return null; // There is no pointing model.
//				t = result.getTimestamp(1);
//				if (result.next()) {
//					throw new AcsJTmcdbDuplicateRowEx ("TMCDB: Database Error: Duplicate rows in AntennaPointingModel table for antenna " + antennaName);
//				}
//				// . . . and find out what pad it is for.
//				sqlGetRecentPointingModel3.setInt(1, antenna.getAntennaId());
//				sqlGetRecentPointingModel3.setTimestamp(2,t);
//				result = sqlGetRecentPointingModel3.executeQuery();
//				padId = result.getInt(1);
//			} else {
//				// OK, the antenna is on a pad.
//				AntennaToPad x = new AntennaToPad();
//				assignAntennaToPad(x,result);
//				if (result.next()) {
//					throw new AcsJTmcdbDuplicateRowEx ("TMCDB: Database Error: Duplicate rows in AntennaToPad table for antenna " + antennaName);
//				}
//				padId = x.getPadId();
//				// OK, we've got the antenna and the pad; now get the pointing model.
//				sqlGetRecentPointingModel1.setInt(1, antenna.getAntennaId());
//				sqlGetRecentPointingModel1.setInt(2, padId);
//				result = sqlGetRecentPointingModel1.executeQuery();
//				if (result.next() == false)
//					return null; // There is no pointing model.
//				t = result.getTimestamp(1);
//				if (result.next()) {
//					throw new AcsJTmcdbDuplicateRowEx ("TMCDB: Database Error: Duplicate rows in AntennaPointingModel table for antenna " + antennaName);
//				}				
//			}			
//			AntennaPointingModel model = getAntennaPointingModel(antenna.getAntennaId(),padId,"band3",this.convertTimestampToArrayTime(t));
//			PointingModel rtn = new PointingModel ();
//			rtn.setAntennaName(antenna.getAntennaName());
//			rtn.setPadName(getPad(padId).getPadName());
//			rtn.setPointingModel(model);
//			ArrayList termList = new ArrayList ();
//			int modelId = model.getPointingModelId();
//			sqlGetPointingModelTerms.setInt(1,modelId);
//			result = sqlGetPointingModelTerms.executeQuery();
//			if (result.next() != false) {
//				do {
//					AntennaPointingModelTerm tmp = new AntennaPointingModelTerm ();
//					assignAntennaPointingModelTerm(tmp,result);
//					termList.add(tmp);
//				} while (result.next());
//				AntennaPointingModelTerm[] terms = new AntennaPointingModelTerm [termList.size()];
//				rtn.setTerm((AntennaPointingModelTerm[])termList.toArray(terms));
//			}
//			return rtn;
//		} catch (SQLException err) {
//			throw new AcsJTmcdbSqlEx (err);
//		}
//	}
//	
//	/**
//	 * The getPointingModels method in this group returns all pointing models for the antenna.
//	 * 
//	 * @param antennaName
//	 * @return
//	 * @throws AcsJTmcdbSqlEx 
//	 * @throws AcsJTmcdbDuplicateKeyEx 
//	 * @throws AcsJTmcdbNoSuchRowEx 
//	 */
//	public PointingModel[] getPointingModels(String antennaName) throws AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx  {
//		try {
//			Antenna antenna = getAntenna(antennaName);
//			int antennaId = antenna.getAntennaId();
//			sqlGetAllPointingModels.setInt(1,antennaId);
//			ResultSet result = sqlGetAllPointingModels.executeQuery();
//			if (result.next() == false)
//				return new PointingModel [0];
//			ArrayList modelList = new ArrayList ();
//			do {
//				AntennaPointingModel x = new AntennaPointingModel ();
//				assignAntennaPointingModel(x,result);
//				modelList.add(x);
//			} while (result.next());			
//			PointingModel[] model = new PointingModel [modelList.size()];
//			for (int i = 0; i < model.length; ++i) {
//				model[i] = new PointingModel ();
//				model[i].setAntennaName(antennaName);
//				model[i].setPointingModel((AntennaPointingModel)modelList.get(i));
//				int padId = model[i].getPointingModel().getPadId();
//				model[i].setPadName(getPad(padId).getPadName());
//				ArrayList termList = new ArrayList ();
//				int modelId = model[i].getPointingModel().getPointingModelId();
//				sqlGetPointingModelTerms.setInt(1,modelId);
//				result = sqlGetPointingModelTerms.executeQuery();
//				if (result.next() == false)
//					model[i].setTerm(new AntennaPointingModelTerm [0]);
//				else {
//					do {
//						AntennaPointingModelTerm x = new AntennaPointingModelTerm ();
//						assignAntennaPointingModelTerm(x,result);
//						termList.add(x);
//					} while (result.next());
//					AntennaPointingModelTerm[] terms = new AntennaPointingModelTerm [termList.size()];
//					model[i].setTerm((AntennaPointingModelTerm[])termList.toArray(terms));
//				}
//			}
//			return model;
//		} catch (SQLException err) {
//			throw new AcsJTmcdbSqlEx (err);
//		}
//	}
//	
//	/**
//	 * The storePointingModel method stores a pointing model for the antenna/pad combination.
//	 * 
//	 * @param model A pointing model.
//	 * @throws AcsJTmcdbSqlEx 
//	 * @throws AcsJTmcdbNoSuchRowEx
//	 * @throws AcsJTmcdbDuplicateKeyEx 
//	 * @throws AcsJTmcdbRowAlreadyExistsEx 
//	 */	
//	public void storePointingModel(PointingModel model) throws AcsJTmcdbSqlEx, AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbRowAlreadyExistsEx {
//		try {
//			model.getPointingModel().setAntennaId(getAntenna(model.getAntennaName()).getAntennaId());
//			model.getPointingModel().setPadId(getPad(model.getPadName(),configurationId).getPadId());		
//			int id = addAntennaPointingModel(model.getPointingModel());
//			for (int i = 0; i < model.getTerm().length; ++i) {
//				model.getTerm()[i].setPointingModelId(id);
//				addAntennaPointingModelTerm(model.getTerm()[i]);
//			}
//		} catch (AcsJTmcdbNoSuchRowEx err) {
//			try {
//				err.log(logger);
//				conn.rollback();
//			} catch (SQLException e) {
//				throw new AcsJTmcdbSqlEx ("  Error while handling rollback: " , e);
//			}
//			throw err;
//		} catch (AcsJTmcdbDuplicateKeyEx err) {
//			try {
//				err.log(logger);
//				conn.rollback();
//			} catch (SQLException e) {
//				throw new AcsJTmcdbSqlEx ("  Error while handling rollback: " , e);
//			}
//			throw err;
//		} catch (AcsJTmcdbRowAlreadyExistsEx err) {
//			try {
//				err.log(logger);
//				conn.rollback();
//			} catch (SQLException e) {
//				throw new AcsJTmcdbSqlEx ("  Error while handling rollback: " , e);
//			}
//			throw err;
//		}
//		try {
//			conn.commit();
//		} catch (SQLException e) {
//			throw new AcsJTmcdbSqlEx (e);
//		}
//	}
	
}


