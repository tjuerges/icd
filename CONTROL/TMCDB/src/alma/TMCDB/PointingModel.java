/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PointintModel.java
 */
package alma.TMCDB;

import alma.TMCDB.generated.*;

/**
 * The PointingModel class is merely a data structure that is used to supply 
 * information about an antenna pointing model.  Included is the antenna name,
 * the pad on which the antenna was located at the time of the measurement,
 * the pointing model itself, including a link to the ASDM that made the 
 * measurement, and the terms of the pointing model.
 */
public class PointingModel {
    static private final String newline = System.getProperty("line.separator");

	private String antennaName;
	private String padName;
	private AntennaPointingModel pointingModel;
	private AntennaPointingModelTerm[] term;
	
	public PointingModel () {		
	}
	
	public PointingModel (
			String antennaName,
			String padName,
			AntennaPointingModel pointingModel,
			AntennaPointingModelTerm[] term) {
		this.antennaName = antennaName;
		this.padName = padName;
		this.pointingModel = pointingModel;
		this.term = term;
	}
	
	public String toString() {
    	String s =  "PointingModel:" + newline +
    		"\tantennaName: " + antennaName + newline +
    		"\tpadName: " + padName + newline +
    		"\tpointingModel: " + pointingModel.toString() + newline;
    	for (int i = 0; i < term.length; ++i)
    		s += "\tterm[" + i + "]: " + term[i].toString() + newline;
		return s;
	}

	public String getAntennaName() {
		return antennaName;
	}

	public void setAntennaName(String antennaName) {
		this.antennaName = antennaName;
	}

	public String getPadName() {
		return padName;
	}

	public void setPadName(String padName) {
		this.padName = padName;
	}

	public AntennaPointingModel getPointingModel() {
		return pointingModel;
	}

	public void setPointingModel(AntennaPointingModel pointintModel) {
		this.pointingModel = pointintModel;
	}

	public AntennaPointingModelTerm[] getTerm() {
		return term;
	}

	public void setTerm(AntennaPointingModelTerm[] term) {
		this.term = term;
	}
	
}
