/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBUtil.java
 */
package alma.TMCDB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Logger;

import oracle.jdbc.pool.OracleDataSource;

import org.hsqldb.jdbc.JDBCDataSource;

import alma.TmcdbErrType.wrappers.AcsJTmcdbConnectionFailureEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbInitializationFailureEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.acs.logging.ClientLogManager;
import alma.archive.database.helpers.DBConfiguration;
import alma.archive.exceptions.general.DatabaseException;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Length;

public class TMCDBUtil {

	protected String dbUser;
	protected String dbPassword;
	protected String dbUrl;
	
	protected Connection conn;
	protected OracleDataSource ds;
	protected JDBCDataSource hds;

    protected String configurationName;
    protected int configurationId;
    
    protected final Logger logger;
    
    // is set, when connection is established (and at the same time dbConfig.properties is read)
    protected boolean useOracle=false; 
    
	public TMCDBUtil () {
		configurationName = "NONE";
		configurationId = 0;
		logger = ClientLogManager.getAcsLogManager().getLoggerForApplication(
				"TMCDB", false);
	}

	public void initialize() throws AcsJTmcdbSqlEx {
		
	}
	
	// Database utility functions.

	public Connection connect(String dbUser, String dbPassword, String dbUrl) throws AcsJTmcdbSqlEx {
                logger.info("Connecting using dbUser="+dbUser+"; dbPassword="+dbPassword+"; dbUrl="+dbUrl);            
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.dbUrl = dbUrl;
		try {
			ds = new OracleDataSource();
			ds.setURL(dbUrl);
			conn = ds.getConnection(dbUser, dbPassword);
			conn.setAutoCommit(false);  // We have to commit explicitly.
		} catch (SQLException err) {
			throw new AcsJTmcdbSqlEx (err);
		}
		return conn;
	}
	/*
	 * reads connection info from dbConfig (eg which DB to connect to) 
	 * and connects to the DB accordingly. 
	 * 
	 * Returns the created Connection object. 
	 */
	public Connection connectDB() throws AcsJTmcdbInitializationFailureEx, AcsJTmcdbSqlEx, AcsJTmcdbConnectionFailureEx {

	DBConfiguration config;
	try {
		config = DBConfiguration.instance(logger);
	} catch (DatabaseException e) {
		logger.warning("Exception when reading dbConfig.properties: "
				+ e.toString());
		AcsJTmcdbInitializationFailureEx ex = new AcsJTmcdbInitializationFailureEx("Exception when reading dbConfig.properties",e);
		throw ex;
	}
	// read config info:
	String backend = config.get("archive.tmcdb.backend");
	String user = config.get("archive.tmcdb.user");
	String pwd = config.get("archive.tmcdb.passwd");
	String location = config.get("archive.tmcdb.location");

	if (backend==null) {
		throw new AcsJTmcdbInitializationFailureEx("No backend specified for TMCDB! Check property alma.tmcdb.backend in dbConfig.properties.");
	}
	
	if (backend.equalsIgnoreCase("oracle")) {
		useOracle=true;
		String service = config.get("archive.tmcdb.service");
		String connectionString = "jdbc:oracle:thin:@//" + location + "/"
				+ service;
		return connectOracle(user, pwd == null ? "alma$dba" : pwd, connectionString);
	} else {
		// we use HsqlDB
		useOracle=false;
		return connectHsqldb(user, pwd == null ? "" : pwd, location);
	}
	
	}
	
	public void connect() throws AcsJTmcdbSqlEx {
		String x = "��������������/������������������������������������";
        StringBuffer tmp =  new StringBuffer ();
        for (int i = 0; i < x.length(); ++i)
        	tmp.appendCodePoint(x.charAt(i) - 128);
        String u = tmp.toString();
		connect(u.substring(u.indexOf('^') + 1,u.indexOf('^',u.indexOf('^') + 1)),u.substring(u.indexOf('^',u.indexOf('^') + 1) + 1),u.substring(0,u.indexOf('^')));
	}

	public Connection connectOracle(String dbUser, String dbPassword, String dbUrl) throws AcsJTmcdbSqlEx {
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.dbUrl = dbUrl;
		try {
			logger.info("Connecting to TMCDB in Oracle as " + dbUser + " with: "
					+ dbUrl);
			ds = new OracleDataSource();
			ds.setURL(dbUrl);
			conn = ds.getConnection(dbUser, dbPassword);
			conn.setAutoCommit(false);  // We have to commit explicitly.
		} catch (SQLException err) {
			throw new AcsJTmcdbSqlEx (err);
		}
		return conn;
	}
	public Connection connectHsqldb(String dbUser, String dbPassword, String dbUrl) throws AcsJTmcdbSqlEx, AcsJTmcdbConnectionFailureEx {
		
		// Load the HSQL Database Engine JDBC driver
        // hsqldb.jar should be in the class path or made part of the current jar
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.dbUrl = dbUrl;
		try {
			logger.info("Connecting to TMCDB in HsqlDB as " + dbUser + " with: "
					+ dbUrl);
		    Class.forName("org.hsqldb.jdbcDriver");
		    hds = new JDBCDataSource();
		    hds.setDatabase(dbUrl);
		    conn = hds.getConnection(dbUser, dbPassword);
	        // connect to the database.   This will load the db files and start the
	        // database if it is not alread running.
	        // db_file_name_prefix is used to open or create files that hold the state
	        // of the db.
	        // It can contain directory names relative to the
	        // current working directory
	        /* conn = DriverManager.getConnection(dbUrl,    	// filenames
	                                           dbUser,      // username
	                                           dbPassword); // password
	         */

			conn.setAutoCommit(false);  // We have to commit explicitly.
		} catch (SQLException err) {
			throw new AcsJTmcdbSqlEx (err);
		} catch (ClassNotFoundException err) {
			throw new AcsJTmcdbConnectionFailureEx ("Can't load jdbc driver for HSQLDB.", err);
		}
		return conn;
	}

	public void loadAndExecuteScript(Connection connection, String filename) throws Exception {
		BufferedReader csr = null;
		Statement stmt = null;

		try {
			stmt = connection.createStatement();
			
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
			if (inputStream == null)
				throw new FileNotFoundException(filename);
			csr = new BufferedReader(new InputStreamReader(inputStream));
			//csr = new BufferedReader(new FileReader(filename));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		String line;
		StringBuffer sb = new StringBuffer();
		boolean plsqlMode = false;
		if (csr != null && stmt != null) try {
			while((line = csr.readLine()) != null) {
				line=line.trim();
				if (line.startsWith("--") || line.matches("^/")) continue;	// SQL Comment
				if (line.contains("CREATE OR REPLACE TRIGGER"))
					plsqlMode = true;
				if (line.contains(";")) {
					if (!plsqlMode)
						line = line.replace(";","");
					sb.append(line);
					sb.append(" ");
					if (plsqlMode && !line.startsWith("END"))
						continue;
					line = sb.toString();
					//System.out.println(line);
					try {
						stmt.execute(line);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						if (!line.matches("^DROP (TABLE|SEQUENCE).*")) 
							throw e;
						else
							System.out.println(e.getCause());  // Usually means that the table wasn't there, so don't worry
					}
					sb = new StringBuffer();
					plsqlMode = false;
				} else {
					sb.append(line);
					sb.append(" ");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

	}

	public void release() throws AcsJTmcdbSqlEx {
		
	}
	
	public void commit() throws AcsJTmcdbSqlEx {
		try {
			conn.commit();
		} catch (SQLException err) {
			throw new AcsJTmcdbSqlEx (err);
		}
	}
	
	/**
	 * Get the name of the default TMCDB configuration currently being accessed.
	 * @return The name of the default TMCDB configuration currently being accessed.
	 */
	public String getConfigurationName() {
		return configurationName;
	}
	
	/**
	 * Get the identifier of the default TMCDB configuration currently being accessed.
	 * @return The identifier of the default TMCDB configuration currently being accessed.
	 */
	public int getConfigurationId() {
		return configurationId;
	}

	/**
	 * Get password used to connect to the DB, <code>null</code> if not yet tried to connect.
	 * @return the dbPassword
	 */
	public String getDbPassword() {
		return dbPassword;
	}

	/**
	 * Get URL used to connect to the DB, <code>null</code> if not yet tried to connect.
	 * @return the dbUrl
	 */
	public String getDbUrl() {
		return dbUrl;
	}

	/**
	 * Get user used to connect to the DB, <code>null</code> if not yet tried to connect.
	 * @return the dbUser
	 */
	public String getDbUser() {
		return dbUser;
	}	
	
	// Utilities
	
	private static final String NullTime = (new Timestamp (-42,10,17,0,0,0,0)).toString();
	protected ArrayTime convertTimestampToArrayTime(Timestamp x) {
		if (x == null || x.toString().equals(NullTime))
			return new ArrayTime();
		return new ArrayTime((x.getYear() + 1900), (x.getMonth() + 1), x.getDate(),
				x.getHours(), x.getMinutes(), (x.getSeconds() + (x.getNanos()/1000000000.0)));
	}	

	protected Timestamp convertArrayTimeToTimestamp(ArrayTime x) {
		int[] t = x.getDateTime();
		return new Timestamp((t[0] - 1900), (t[1] - 1), t[2], t[3], t[4], t[5], t[6]);
	}	

	protected Length convertDoubleToLength(double x) {
		return new Length(x);
	}	

	protected double convertLengthToDouble(Length x) {
		return x.get();
	}

}
