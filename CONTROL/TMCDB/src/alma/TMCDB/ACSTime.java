/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ACSTime.java
 */
package alma.TMCDB;

import alma.hla.runtime.asdm.types.ArrayTime;

public class ACSTime {

		private long value;
		
		public ACSTime (long t) {
			value = t;
		}
		
		/**
		 * Convert ACS time to ArrayTime
		 * 
		 * ACS: hundreds of nanoseconds since 15 October 1582 00:00:00 UTC
		 * ArrayTime: nanoseconds since 17 November 1858 00:00:00 UTC base
		 * difference in days: JD(17 November 1858 00:00:00 UTC) minus JD(15 October 1582 00:00:00 UTC) 
		 * ArrayTime = (ACS.time * 100) minus (baseDifference * 86400000000000) 
		 * ArrayTime = 100*ACS.time - (2400000.5 - 2299160.5) * 86400000000000 
		 * ArrayTime = 100*ACS.time - (100840) * 86400000000000
		 * ArrayTime = 100*ACS.time - 8712576000000000000
		 * 
		 * @return the ArrayTime.
		 */
		public ArrayTime toArrayTime() {
			return new ArrayTime(100L * value - 8712576000000000000L);
		}
}


