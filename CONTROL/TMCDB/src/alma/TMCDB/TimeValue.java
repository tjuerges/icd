/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TimeValue.java
 */
package alma.TMCDB;

import alma.hla.runtime.asdm.types.ArrayTime;

public class TimeValue {
	private ArrayTime time;
	private Object value;
	
	public TimeValue (ArrayTime time, Float value) {
		this.time = time;
		this.value = value;
	}
	public TimeValue (ArrayTime time, Double value) {
		this.time = time;
		this.value = value;
	}
	public TimeValue (ArrayTime time, Integer value) {
		this.time = time;
		this.value = value;
	}
	public TimeValue (ArrayTime time, String value) {
		this.time = time;
		this.value = value;
	}
	public TimeValue (ArrayTime time, Boolean value) {
		this.time = time;
		this.value = value;
	}
	public TimeValue (ArrayTime time, Byte value) {
		this.time = time;
		this.value = value;
	}

	public ArrayTime getTime() {
		return time;
	}
	
	public Object getValue() {
		return value;
	}
	
	public String toString() {
		return time.toFITS() + " " + value;
	}
	
	static public void main(String[] arg) {
		TimeValue x = new TimeValue(new ArrayTime(), 123.45F);
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
		x = new TimeValue(new ArrayTime(), 123.45);
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
		x = new TimeValue(new ArrayTime(), 12345);
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
		x = new TimeValue(new ArrayTime(), "123.45F");
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
		x = new TimeValue(new ArrayTime(), false);
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
		x = new TimeValue(new ArrayTime(), (byte)12);
		System.out.println("x = " + x + " is a " + x.getValue().getClass().getName());
	}
}


