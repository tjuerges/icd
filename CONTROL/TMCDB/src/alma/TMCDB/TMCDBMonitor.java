/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBMonitor.java
 */
package alma.TMCDB;

import alma.TMCDB.generated.Antenna;
import alma.TMCDB.generated.AntennaPointingModel;
import alma.TMCDB.generated.Assembly;
import alma.TMCDB.generated.AssemblyType;
import alma.TMCDB.generated.LRUType;
import alma.TMCDB.generated.EnumWord;
import alma.TMCDB.generated.BooleanProperty;
import alma.TMCDB.generated.DoubleProperty;
import alma.TMCDB.generated.EnumProperty;
import alma.TMCDB.generated.FloatProperty;
import alma.TMCDB.generated.IntegerProperty;
import alma.TMCDB.generated.PropertyType;
import alma.TMCDB.generated.StringProperty;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateKeyEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbInvalidDataTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import alma.hla.runtime.asdm.types.ArrayTime;

public class TMCDBMonitor extends TMCDBAntenna {

	// Get all assemblies.
	protected PreparedStatement sqlGetAllAssemblies;
	// Get all properties  that belong to an assembly.
	protected PreparedStatement sqlGetAssemblyProperties;
	// Get the Enum words that belong to an enumeration property.
	protected PreparedStatement sqlGetEnumWords;
	// Get the monitor data that belongs to a property of an assembly.
	protected PreparedStatement sqlGetFloatMonitorData;
	protected PreparedStatement sqlGetDoubleMonitorData;
	protected PreparedStatement sqlGetIntegerMonitorData;
	protected PreparedStatement sqlGetStringMonitorData;
	protected PreparedStatement sqlGetBooleanMonitorData;
	protected PreparedStatement sqlGetEnumMonitorData;

    /**
     * Constructor for TMCDBMonitor.
     */	
    public TMCDBMonitor () {
    	super();
	}

    /**
     * Initializer for TMCDBMonitor.
     * @throws AcsJTmcdbErrTypeEx 
     */	
	public void initialize() throws AcsJTmcdbSqlEx {
		super.initialize();
		try {
			sqlGetAllAssemblies = conn.prepareStatement("SELECT * FROM Assembly WHERE ConfigurationId = " + configurationId);
			sqlGetAssemblyProperties = conn.prepareStatement("SELECT * FROM PropertyType WHERE AssemblyName = ?");
			sqlGetEnumWords = conn.prepareStatement("SELECT Word FROM EnumWord WHERE PropertyTypeId = ? ORDER BY OrderNumber ");
			sqlGetFloatMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM FloatProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime");
			sqlGetDoubleMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM DoubleProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime");
			sqlGetIntegerMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM IntegerProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime ");
			sqlGetStringMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM StringProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime");
			sqlGetBooleanMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM BooleanProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime");
			sqlGetEnumMonitorData = conn.prepareStatement("SELECT SampleTime, Value FROM EnumProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND ? <= SampleTime AND SampleTime <= ? ORDER BY SampleTime");
		} catch (SQLException e) {
			throw new AcsJTmcdbSqlEx (e);
		}
	}
	
	/**
	 * Add the specified array of float properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbErrorEx Thrown if any error before the commit
	 * @throws AcsJTmcdbSqlEx Thrown if error on commit
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 */
	public void addProperty(FloatProperty[] data) throws AcsJTmcdbErrorEx, AcsJTmcdbSqlEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
		for (int i = 0; i < data.length; ++i) {
            try {
			    addFloatProperty(data[i]);
            } catch(AcsJTmcdbRowAlreadyExistsEx ex){
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}
	
	/**
	 * Add the specified array of double properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbErrorEx 
	 * @throws AcsJTmcdbSqlEx 
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 */
	public void addProperty(DoubleProperty[] data) throws AcsJTmcdbErrorEx, AcsJTmcdbSqlEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
        for (int i = 0; i < data.length; ++i) {
            try {
			    addDoubleProperty(data[i]);
            } catch (AcsJTmcdbRowAlreadyExistsEx ex){
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}

	/**
	 * Add the specified array of integer properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbErrorEx 
	 * @throws AcsJTmcdbSqlEx 
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 * 										Thrown if any error occurred during the addition.
	 */
	public void addProperty(IntegerProperty[] data) throws AcsJTmcdbErrorEx, AcsJTmcdbSqlEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
		for (int i = 0; i < data.length; ++i) {
            try {
			    addIntegerProperty(data[i]);
            } catch (AcsJTmcdbRowAlreadyExistsEx ex){
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}

	/**
	 * Add the specified array of string properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbSqlEx 
	 * @throws AcsJTmcdbErrorEx 
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 */
	public void addProperty(StringProperty[] data) throws AcsJTmcdbSqlEx, AcsJTmcdbErrorEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
		for (int i = 0; i < data.length; ++i) {
            try {
			    addStringProperty(data[i]);
            } catch (AcsJTmcdbRowAlreadyExistsEx ex) {
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}

	/**
	 * Add the specified array of boolean properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 */
	public void addProperty(BooleanProperty[] data) throws AcsJTmcdbSqlEx, AcsJTmcdbErrorEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
		for (int i = 0; i < data.length; ++i) {
            try {
			    addBooleanProperty(data[i]);
            } catch(AcsJTmcdbRowAlreadyExistsEx ex){
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}

	/**
	 * Add the specified array of enumerated properties to the TMCDB.  The database commit is not
	 * done until all members of the array have been added.
	 * @param data The array to be added.
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbRowAlreadyExistsEx 
	 */
	public void addProperty(EnumProperty[] data) throws AcsJTmcdbSqlEx, AcsJTmcdbErrorEx, AcsJTmcdbRowAlreadyExistsEx, AcsJTmcdbDuplicateKeyEx {
        StringBuffer msg = new StringBuffer();
        msg.append("Duplicate entries when inserting into database\n");
        int count = 0;
		for (int i = 0; i < data.length; ++i) {
            try {
			    addEnumProperty(data[i]);
            } catch(AcsJTmcdbRowAlreadyExistsEx ex){
                count++;
                msg.append("("+count+") ");
                msg.append("AssemblyId: " + ex.getProperty("AssemblyId"));
                msg.append("; PropertyTypeId: " + ex.getProperty("PropertyTypeId"));
                msg.append("; SampleTime: " + ex.getProperty("SampleTime") + "\n");
            }
		}
        if (count > 0)
            logger.severe(msg.toString());
		commit();
	}

	/**
	 * Get all assemblies in the TMCDB.  If there are no assemblies, an array of length zero is returned.
	 * @return An array of Assembly objects representing all assemblies in the TMCDB. 
	 * @throws AcsJTmcdbSqlEx Thrown if any database error occurs.
	 */
	public Assembly[] getAssemblies() throws AcsJTmcdbSqlEx {
		try {
			ResultSet result = sqlGetAllAssemblies.executeQuery();
			if (result.next() == false) {
				return new Assembly [0];
			}
			ArrayList<Assembly> assembly = new ArrayList<Assembly> ();
			do {
				Assembly x = new Assembly ();
				assignAssembly(x,result);
				assembly.add(x);
			} while (result.next());
			Assembly[] rtn = new Assembly [assembly.size()];
			rtn = assembly.toArray(rtn);
			return rtn;
		} catch (SQLException err) {
            logger.severe("SQL Error: " + err.toString());
			throw new AcsJTmcdbSqlEx (err);
		}
	}

	/**
	 * Get all properties that belong to the specified assembly type.
	 * @param assemblyName The name of the type of assembly.
	 * @return An array of PropertyType that represents all types of properties that belong to this type of assembly.
	 * @throws AcsJTmcdbSqlEx Thrown if any database error occurs.
	 */
	public PropertyType[] getAssemblyProperties(String assemblyName) throws AcsJTmcdbSqlEx {
		try {
			sqlGetAssemblyProperties.setString(1, assemblyName);
			ResultSet result = sqlGetAssemblyProperties.executeQuery();
			if (result.next() == false)
				return new PropertyType [0];
			ArrayList<PropertyType> property = new ArrayList<PropertyType> ();
			do {
				PropertyType x = new PropertyType ();
				assignPropertyType(x,result);
				property.add(x);
			} while (result.next());
			PropertyType[] rtn = new PropertyType [property.size()];
			rtn = property.toArray(rtn);
			return rtn;
		} catch (SQLException err) {
			err.printStackTrace();
			throw new AcsJTmcdbSqlEx (err);
		}
	}

	/**
	 * Get the list of enumerated words that are associated with the specified property ordered by their index.
	 * @param propertyTypeId The database id of the property.
	 * @return The enumerated words ordered by their index.
	 * @throws AcsJTmcdbSqlEx Thrown if any database error occurs.
	 */
	public String[] getEnumWords(int propertyTypeId) throws AcsJTmcdbSqlEx {
		try {
			sqlGetEnumWords.setInt(1, propertyTypeId);
			ResultSet result = sqlGetEnumWords.executeQuery();
			if (result.next() == false)
				return new String [0];
			ArrayList<String> word = new ArrayList<String> ();
			do {
				word.add(result.getString(1));
			} while (result.next());
			String[] rtn = new String [word.size()];
			rtn = word.toArray(rtn);
			return rtn;
		} catch (SQLException err) {
			err.printStackTrace();
			throw new AcsJTmcdbSqlEx (err);
		}
	}
	
	/**
	 * Get the monitored data about the specified property of the specified assembly during the 
	 * specified interval of time as an array of time-value pairs.
	 * @param assemblyId The database id of the assembly.
	 * @param propertyTypeId The database id of the property.
	 * @param datatype The data type of the property.
	 * @param beginThe beginning of the time interval of interest.
	 * @param endThe end of the time interval of interest.
	 * @return The data of interest as an array of time-value pairs ordered by increasing time.
	 * @throws AcsJTmcdbSqlEx  Thrown if any database error occurs.
	 */
	public TimeValue[] getMonitorData(int assemblyId, int propertyTypeId, String datatype, ArrayTime begin, ArrayTime end) throws AcsJTmcdbInvalidDataTypeEx, AcsJTmcdbSqlEx   {
		try {
			if (datatype.equals("float")) {
				sqlGetFloatMonitorData.setInt(1, assemblyId);
				sqlGetFloatMonitorData.setInt(2, propertyTypeId);
				sqlGetFloatMonitorData.setLong(3, begin.get());
				sqlGetFloatMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetFloatMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getFloat(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			} else if (datatype.equals("double")) {
				sqlGetDoubleMonitorData.setInt(1, assemblyId);
				sqlGetDoubleMonitorData.setInt(2, propertyTypeId);
				sqlGetDoubleMonitorData.setLong(3, begin.get());
				sqlGetDoubleMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetDoubleMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getDouble(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			} else if (datatype.equals("integer")) {
				sqlGetIntegerMonitorData.setInt(1, assemblyId);
				sqlGetIntegerMonitorData.setInt(2, propertyTypeId);
				sqlGetIntegerMonitorData.setLong(3, begin.get());
				sqlGetIntegerMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetIntegerMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getInt(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			} else if (datatype.equals("string")) {
				sqlGetStringMonitorData.setInt(1, assemblyId);
				sqlGetStringMonitorData.setInt(2, propertyTypeId);
				sqlGetStringMonitorData.setLong(3, begin.get());
				sqlGetStringMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetStringMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getString(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			} else if (datatype.equals("boolean")) {
				sqlGetBooleanMonitorData.setInt(1, assemblyId);
				sqlGetBooleanMonitorData.setInt(2, propertyTypeId);
				sqlGetBooleanMonitorData.setLong(3, begin.get());
				sqlGetBooleanMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetBooleanMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getBoolean(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			} else if (datatype.equals("enum")) {
				sqlGetEnumMonitorData.setInt(1, assemblyId);
				sqlGetEnumMonitorData.setInt(2, propertyTypeId);
				sqlGetEnumMonitorData.setLong(3, begin.get());
				sqlGetEnumMonitorData.setLong(4, end.get());
				ResultSet result = sqlGetEnumMonitorData.executeQuery();
				if (result.next() == false)
					return new TimeValue [0];
				ArrayList<TimeValue> x = new ArrayList<TimeValue> ();
				do {
					x.add(new TimeValue(new ArrayTime(result.getLong(1)),result.getByte(2)));
				} while (result.next());
				TimeValue[] rtn = new TimeValue [x.size()];
				rtn = x.toArray(rtn);
				return rtn;			
			}
			AcsJTmcdbInvalidDataTypeEx ex = new AcsJTmcdbInvalidDataTypeEx();
			ex.setProperty("Datatype", datatype);
			throw ex;
		} catch (SQLException err) {
			err.printStackTrace();
			throw new AcsJTmcdbSqlEx (err);
		}
	}
}


