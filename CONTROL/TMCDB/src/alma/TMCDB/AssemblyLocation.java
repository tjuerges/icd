/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AssemblyLocation.java
 */
package alma.TMCDB;

/**
 * The AsssemblyLocation class gives the data necessary to locate an assembly 
 * within the context of a collection of assemblies and can busses.  Included
 * are the type of assembly, its role name (in case there are more than one),
 * its relative can address, channel number and base address.
 */
public class AssemblyLocation {
    static private final String newline = System.getProperty("line.separator");
	
	private String assemblyTypeName;
	private String assemblyRoleName;
	private int rca;
	private int channelNumber;
	private int baseAddress;
	
	public AssemblyLocation () {
	}

	public AssemblyLocation (
	    	 String assemblyTypeName,
	    	 String assemblyRoleName,
	    	 int rca,
	    	 int channelNumber,
	    	 int baseAddress ) {
		this.assemblyTypeName = assemblyTypeName;
		this.assemblyRoleName = assemblyRoleName;
		this.rca = rca;
		this.channelNumber = channelNumber;
		this.baseAddress = baseAddress;
	}

	public String toString() {
    	String s =  "AssemblyLocation:" + newline +
    		"\tassemblyRoleName: " + assemblyRoleName + newline +
    		"\trca: " + rca + newline +
    		"\tchannelNumber: " + channelNumber + newline +
    		"\tbaseAddress: " + baseAddress + newline;
		return s;
	}

	public String getAssemblyRoleName() {
		return assemblyRoleName;
	}

	public void setAssemblyRoleName(String assemblyRoleName) {
		this.assemblyRoleName = assemblyRoleName;
	}

	public String getAssemblyTypeName() {
		return assemblyTypeName;
	}

	public void setAssemblyTypeName(String assemblyTypeName) {
		this.assemblyTypeName = assemblyTypeName;
	}

	public int getBaseAddress() {
		return baseAddress;
	}

	public void setBaseAddress(int baseAddress) {
		this.baseAddress = baseAddress;
	}

	public int getChannelNumber() {
		return channelNumber;
	}

	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}

	public int getRca() {
		return rca;
	}

	public void setRca(int rca) {
		this.rca = rca;
	}

}
