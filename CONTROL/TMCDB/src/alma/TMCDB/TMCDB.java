/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDB.java
 */
package alma.TMCDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import alma.TMCDB.generated.Configuration;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateKeyEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbInitializationFailureEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.hla.runtime.asdm.types.ArrayTime;

public class TMCDB extends TMCDBMonitor {
	
	////////////////////////////////////////////////////////////////////////////////////
	// Important notice!                                                              //
	// -----------------                                                              //
	// There are no commits or rollbacks in the TMCDBBase class.  This means that all //
	// transactions involving add, deletes, or updates must do their own commits and  //
	// rollbacks.  In other words transaction processing must be defined at higher    // 
	// levels, e.g. in the methods defined in the API.                                //
	////////////////////////////////////////////////////////////////////////////////////
	
	
	public TMCDB () {
		super();
	}
	
	/**
	 * The initialize method must be called prior to using the database interface methods.
	 * This method constructs all of the Prepared Statements used to access the database.
	 */
	public void initialize() throws AcsJTmcdbSqlEx {
		super.initialize();
	}

	////////////////////////////////////////////////////////////
	// Set the default configuration for all database access. //
	////////////////////////////////////////////////////////////
	
	/**
	 * An important part of the TMCDB is the ability to store multiple configurations without these 
	 * interfering with each other.  However, specifying the name of the configuration on every 
	 * database access method is a nuisance. To alleviate this situation the setConfigurationName 
	 * method is used to set the configuration name.  
	 * 
	 * This method must be called after the TMCDB is created and before any other database access 
	 * methods are called.  To be more precise, this method must be called AFTER the connect() method
	 * has been called but BEFORE the initialize() method has been called.  If the using application 
	 * fails to set this method, the database will appear to be empty; many methods will throw an 
	 * exception that says there are no rows in the database with the specified key (and the 
	 * configuration is part of the key).
	 * 
	 * @param configurationName The name of the TMCDB configuration.
	 * @throws AcsJTmcdbNoSuchRowEx 
	 * @throws AcsJTmcdbDuplicateKeyEx 
	 * @throws AcsJTmcdbSqlEx 
	 * @throws TMCDBException Thrown if the specified configuration name is not in the database.
	 */
	public void setConfigurationName(String configName) throws AcsJTmcdbInitializationFailureEx, AcsJTmcdbNoSuchRowEx, AcsJTmcdbDuplicateKeyEx, AcsJTmcdbSqlEx {
		//Configuration config = getConfiguration (configName);
        try {
        	PreparedStatement getConfigurationAlt = conn.prepareStatement("SELECT ConfigurationId FROM Configuration WHERE ConfigurationName = '" + configName + "'");
            ResultSet result = getConfigurationAlt.executeQuery();
            if (result.next() == false)
                throw new AcsJTmcdbNoSuchRowEx("TMCDB: Database error: There are no rows in Configuration tables with key " + 
                		configName);
            this.configurationId = result.getInt(1);
            this.configurationName = configName;
            if (result.next())
                throw new AcsJTmcdbDuplicateKeyEx("TMCDB: Database error: Duplicate keys for Configuration table (key " + 
                		configName + ")");
        } catch (SQLException err) {
            throw new AcsJTmcdbSqlEx(err);
        }
	}

}
