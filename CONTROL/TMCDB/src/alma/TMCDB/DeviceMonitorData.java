/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File DeviceMonitorData.java
 */
package alma.TMCDB;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.TMCDB.generated.Assembly;
import alma.TMCDB.generated.LRUType;
import alma.TMCDB.generated.AssemblyType;


public class DeviceMonitorData {
	private ArrayTime beginTime;
	private ArrayTime endTime;
	private Assembly assembly;
	private LRUType lru;
	private AssemblyType assemblyType;
	private PropertyData[] property;
	
	public DeviceMonitorData (ArrayTime beginTime, ArrayTime endTime, Assembly assembly,
			LRUType lru, AssemblyType assemblyType, PropertyData[] property) {
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.assembly = assembly;
		this.lru = lru;
		this.assemblyType = assemblyType;
		this.property = property;
	}

	public Assembly getAssembly() {
		return assembly;
	}

	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	public ArrayTime getBeginTime() {
		return beginTime;
	}

	public ArrayTime getEndTime() {
		return endTime;
	}

	public LRUType getLru() {
		return lru;
	}

	public PropertyData[] getProperty() {
		return property;
	}
	
}


