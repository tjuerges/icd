package alma.Control.datamodel;

import java.io.File;

import alma.Control.datamodel.meta.TMCDBModel;

import org.openarchitectureware.workflow.WfCHelper;
import org.openarchitectureware.workflow.WorkflowContext;
import org.openarchitectureware.workflow.issues.Issues;
import org.openarchitectureware.workflow.oawclassic.util.WorkflowComponentWithME;
import org.openarchitectureware.workflow.monitor.ProgressMonitor;
import org.openarchitectureware.core.meta.MetaEnvironment;

public class SQLWorkflowInitializer extends WorkflowComponentWithME {
	private String sqlFilename;
	private String sqlDir;
	private String projectDir;
	private String tmcdbOutput;
	private String modelSlot;
	
	public void checkConfiguration(Issues issues) {
		super.checkConfiguration(issues);
		if ( !WfCHelper.isParamSet( modelSlot )) issues.addError( this, "model slot not specified" );
	}

	public void invoke(WorkflowContext ctx, ProgressMonitor monitor, Issues issues) {
	  System.out.println(">>>>>SQLWorkflowInitializer.invoke");
	  System.out.println(">>>>>SQLWorkflowInitializer.invoke sqlFile = " + sqlFilename);

	  File f = new File (projectDir + '/' + sqlDir, sqlFilename);
	  System.out.println(">>>>>SQLWorkflowInitializer filename = " + f.getAbsolutePath());

	  System.getProperties().put("SQL_DIR", projectDir + '/' + sqlDir);
	  System.getProperties().put("SQL_FILENAME", sqlFilename);
 
        System.getProperties().put("TMCDB_OUTPUT", tmcdbOutput + "/.");
        // tmcdbOutput is a directory outside of the normal build directory tree
        // All generated files will first go here before the ant build copies them
        // into the normal tree (under idl, src, and so on). This keeps -- at least
        // for awhile -- the generated artifacts separate; once SE adds the possibility
        // of using Make on an additional directory tree structure, this will pay off.
        // Note that this directory will be different when the generation is done by
        // running the workflow directly from Eclipse than when it is done by running
        // the ant build.xml file (invoked by the Makefile and also invocable from
        // Eclise).
        MetaEnvironment me = getMetaEnv(ctx,issues);
        TMCDBModel t = new TMCDBModel ();
        t.setMetaEnvironment(me);
        me.addElement(t);
        t.setName("TMCDBModel");
        t.CreateModel();        
        ctx.set(modelSlot,t);
        System.out.println(">>>>>SQLWorkflowInitializer completed.");
	}

	public void setSqlFile(String cf) {
		System.out.println(">>>>>SQLWorkflowInitializer.setSqlFile cf = " + cf);
		this.sqlFilename = cf;
	}
	
	public void setSqlDir(String cd) {
		System.out.println(">>>>>SQLWorkflowInitializer.setSqlDir cd = " + cd);
		this.sqlDir = cd;
	}
	
	public void setProjectDir(String to) {
		System.out.println(">>>>>SQLWorkflowInitializer.setProjectDir to = " + to);
		this.projectDir = to;
	}
	
	public void setTmcdbOutput(String to) {
		System.out.println(">>>>>SQLWorkflowInitializer.setTmcdbOutput to = " + to);
		this.tmcdbOutput = to;
	}


	public void setModelSlot(String s) {
		System.out.println(">>>>>SQLWorkflowInitializer.setModelSlot = " + s);
		this.modelSlot = s;
	}
}
