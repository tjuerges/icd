/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColumnDefinition.java
 */
package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.core.ElementSet;

public class ColumnDefinition extends SQLStatement {
	
	private String columnName;
	private int columnNumber;
	private int keyIndex;
	private String dataType;
	private String optionWord;
	private String other;
	private ElementSet comment;
	private boolean charType;
	private int size;
	private boolean last;

	public ColumnDefinition (int lineNumber, String line, MetaEnvironment meta) {
		super(lineNumber, line, meta);
		setStatementType(Util.COLUMN_STATEMENT);
		this.setName(Util.STATEMENT_NAME[Util.COLUMN_STATEMENT]);
		columnName = "";
		columnNumber = -1;
		keyIndex = -1;
		dataType = "";
		optionWord = "";
		other = "";
		comment = new ElementSet();
		charType = false;
		size = -1;
		last = false;
	}

	public String ColumnName() {
		return columnName;
	}
	
	public String ShortColumnName() {
		if (columnName.length() <= 12)
			return columnName;
		String s = columnName.substring(0,6);
		for (int i = 6; i < columnName.length(); ++i)
			if (Character.isUpperCase(columnName.charAt(i)))
				s += columnName.charAt(i);
		return s;
	}

	public String ColumnNumber() {
		return Integer.toString(columnNumber);
	}

	public String KeyIndex() {
		return Integer.toString(keyIndex);
	}
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public void setColumnNumber(int number) {
		this.columnNumber = number;
	}

	public void setKeyIndex(int number) {
		this.keyIndex = number;
	}
	
	public ElementSet Comment() {
		return comment;
	}

	public void setComment(ElementSet comment) {
		this.comment = comment;
	}

	public String DataType() {
		return dataType;
	}
	
	public boolean isCharType() {
		return charType;
	}

	public String SizeInBytes() {
		return Integer.toString(size);
	}
	
	public boolean isLast() {
		return last;
	}
	
	public void setLast(boolean last) {
		this.last = last;
	}
	
	public void setDataType(String dataType, boolean charType, int size) {
		this.dataType = dataType;
		this.charType = charType;
		this.size = size;
	}

	public String OptionWord() {
		return optionWord;
	}

	public void setOptionWord(String word) {
		this.optionWord = word;
	}

	public boolean CanBeNULL() {
		return this.optionWord.equals(Util.NULL_ID);
	}
	
	public String Other() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String toString() {
		return columnName + ", " + dataType + ", " + optionWord + ", " + other;
	}
	
	public boolean isABlob() {
		return dataType.equals(Util.BLOB);
	}
	public boolean isATime() {
		return dataType.equals(Util.TIME);
	}
	public boolean isALength() {
		return dataType.equals(Util.LENGTH);
	}
	public boolean isABoolean() {
		return dataType.equals(Util.BOOLEAN);
	}

	// The following return the datatype in various forms.
	
	public String OracleType() {
		return Util.OracleType(dataType);
	}
	public String HSQLType() {
		return Util.HSQLType(dataType);
	}
	public String IDLType() {
		return Util.IDLType(dataType);
	}
	public String XMLType() {
		return Util.XMLType(dataType);
	}
	public String JavaType() {
		if (CanBeNULL())
			return Util.JavaTypeNullable(dataType);
		else
			return Util.JavaType(dataType);
	}
	public String JavaTypeNullable() {
		return Util.JavaTypeNullable(dataType);
	}
	public String JDBCType() {
		return Util.JDBCType(dataType);
	}
	public String JDBCStaticType() {
		return Util.JDBCStaticType(dataType);
	}
}


