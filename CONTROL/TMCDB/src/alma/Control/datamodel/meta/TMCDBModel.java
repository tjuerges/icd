/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBModel.java
 */
package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.core.constraint.DesignError;

public class TMCDBModel extends Model {
	
	private ElementSet table;
	private Version version;
	private ElementSet note;
	
    protected void initializeModelDependencies() {
    }

	public String CreateModel() {
		System.out.println(">>>>>TMCDBModel: beginning CreateModel.");
		
		// Parse the SQL table definition.
		
		ParseTMCDBTableDefinition def = new ParseTMCDBTableDefinition (System.getProperty("SQL_DIR"),System.getProperty("SQL_FILENAME"),this.getMetaEnvironment());
		try {
			def.createModel();
		} catch (DesignError err) {
			err.printStackTrace();
			System.out.println(err.toString());
			System.exit(0);
		}
		table = def.TableDefinition();
		version = def.Version();
		note = def.Note();
		
		System.out.println(">>>>>TMCDBModel: CreateModel complete.");
		return "";
	}
	
	public ElementSet TableDefinition () {
		return table;
	}

	public ElementSet TableDefinitionReverseOrder () {
		ElementSet x = new ElementSet ();
		for (int i = table.size() - 1; i > -1; --i)
			x.add(table.get(i));
		return x;
	}
	

	public Version Version() {
		return version;
	}
	
	public ElementSet Note() {
		return note;
	}

    public String TheEnd() {
    	System.out.println(">>>>>Ending.");
        String dir = System.getProperty("TMCDB_OUTPUT");
        Util.RemoveLines(dir + "/idl");
        Util.RemoveLines(dir + "/idl/schemas");
        Util.RemoveLines(dir + "/doc");
        Util.RemoveLinesFromFile(dir + "/idl/SQL/", "CreateOracleTables.sql", 0);
        Util.RemoveLinesFromFile(dir + "/idl/SQL/", "DropAllTables.sql", 0);
        Util.RemoveLinesFromFile(dir + "/idl/SQL/", "DropAllOracleSequences.sql", 0);
        Util.RemoveLinesFromFile(dir + "/idl/SQL/", "CreateHsqldbTables.sql", 0);
        Util.RemoveLines(dir + "/src/alma/TMCDB/generated");
        System.out.println(">>>>>Code generation for TMCDB done.");
        return "";
    }


}
