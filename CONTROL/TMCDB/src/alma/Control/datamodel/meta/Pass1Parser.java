/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Pass1Parser.java
 */
package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.constraint.DesignError;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Pass1Parser {
	
	static private final String newline = System.getProperty("line.separator");
	
	private MetaEnvironment meta;
	private String dirName;
	private String fileName;

	private BufferedReader in;
	private int lineCounter;
	
	private ArrayList<SQLStatement> statement;
	private ArrayList<String> errorMsg;

	public Pass1Parser (String dirName, String fileName, MetaEnvironment meta) {
		this.dirName = dirName;
		this.fileName = fileName;
		this.meta = meta;
		statement = new ArrayList<SQLStatement> ();
		errorMsg = new ArrayList<String> ();
	}
	
	public SQLStatement[] parse() throws DesignError {
		// Open the file.
		openFile();
		
		// Go through the file and parse eash individual statement
		SQLStatement s = getNextStatement();
		while (s != null) {
			statement.add(s);
			s = getNextStatement();
		}
		
		// Close the file.
		release();
				
		// Return the collection of SLQ statements.
		SQLStatement[] rtn = new SQLStatement [statement.size()];
		return statement.toArray(rtn);
	}
	
	public String[] getErrors() {
		String[] rtn = new String[errorMsg.size()];
		return errorMsg.toArray(rtn);
	}

	private void error (SQLStatement statement, String err) {
		String s = "TMCDB Table Definition error on line number " + statement.getLineNumber() + newline +
			"\t>>" + statement.Line() + newline + "\t" + err;				
		errorMsg.add(s);
	}
	private void error (String err) {
		String s = "TMCDB Table Definition error: " + err;				
		throw new DesignError (s);
	}

	private void openFile() throws DesignError {
		File dir = new File(dirName);
		if (!dir.isDirectory())
			error("Directory " + dirName + " does not exist.");
		File file = new File(dir, fileName);
		if (!file.exists())
			error("File " + fileName + " in directory " + dirName + " does not exist.");
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (IOException e) {
			error(e.getMessage());
		}
		lineCounter = 0;
	}

	private void release() throws DesignError {
		try {
			in.close();
		} catch (IOException e) {
			error(e.getMessage());
		}		
	}

	private SQLStatement getNextStatement() throws DesignError {
		SQLStatement s = null;
		try {
			String line = in.readLine();
			if (line == null)
				return null;
			++lineCounter;
			while (line.trim().length() == 0) {
				line = in.readLine();
				if (line == null)
					return null;
				++lineCounter;					
			}
			s = createStatement(lineCounter,line);
		} catch (IOException e) {
			error(e.getMessage());
		} catch (StatementError e) {
			error(s,e.toString());
		}
		return s;
	}
	
	private boolean dbON = false;
	private boolean noteON = false;
	private SQLStatement createStatement(int lineCounter, String line) throws StatementError {
		SQLStatement statement = null;
		
		if (line.startsWith(Util.DB_ID)) {
			BeginDBStatement x = new BeginDBStatement (lineCounter,line,meta);
			parseBeginDBStatement(x);
			statement = x;
			dbON = true;
		} else if (line.startsWith(Util.ENDDB_ID)) {
			EndDBStatement x = new EndDBStatement (lineCounter,line,meta);
			parseEndDBStatement(x);
			statement = x;
			dbON = false;
		} else if (dbON) {
			DBStatement x = new DBStatement (lineCounter,line,meta);
			parseDBStatement(x);
			statement = x;
		} else if (line.startsWith(Util.NOTE_ID)) {
			BeginNoteStatement x = new BeginNoteStatement (lineCounter,line,meta);
			parseBeginNoteStatement(x);
			statement = x;
			noteON = true;
		} else if (line.startsWith(Util.ENDNOTE_ID)) {
			EndNoteStatement x = new EndNoteStatement (lineCounter,line,meta);
			parseEndNoteStatement(x);
			statement = x;
			noteON = false;
		} else if (noteON) {
			NoteStatement x = new NoteStatement (lineCounter,line,meta);
			parseNoteStatement(x);
			statement = x;
		} else if (line.startsWith(Util.TITLE_ID)) {
			Version x = new Version (lineCounter,line,meta);
			parseVersionStatement(x);
			statement = x;
		} else if (line.startsWith(Util.TABLE_ID)) {
			TableDefinition x = new TableDefinition (lineCounter,line,meta);
			parseTableStatement(x);
			statement = x;
		} else if (line.startsWith(Util.TABLE_END_ID)) {
			EndTableDefinition x = new EndTableDefinition (lineCounter,line,meta);
			parseEndTableStatement(x);
			statement = x;
		} else if (line.startsWith(Util.CONSTRAINT_ID, Util.getNextWordStart(line, 0, ""))) {
			ConstraintSpecification x = new ConstraintSpecification (lineCounter,line,meta);
			parseConstraintStatement(x);
			statement = x;
		} else if (line.startsWith(Util.KEY_ID, Util.getNextWordStart(line, 0, ""))) {
			String[] tmp = line.split("[ |\t]+");
			boolean isGenerated = false;
			for (int i = 2; i < tmp.length; ++i) {
				if (tmp[i].equals(Util.GENERATE_ID))
					isGenerated = true;
			}
			if (isGenerated) {
				GeneratedKeySpecification x = new GeneratedKeySpecification (lineCounter,line,meta);
				parseGeneratedKeyStatement(x);
				statement = x;
			} else {
				KeySpecification x = new KeySpecification (lineCounter,line,meta);
				parseKeyStatement(x);
				statement = x;
			}
		} else if (line.matches("^ *"+Util.COMMENT_ID+".*")) {
			Comment x = new Comment (lineCounter,line,meta);
			parseCommentStatement(x);
			statement = x;
		} else {
			ColumnDefinition x = new ColumnDefinition (lineCounter,line,meta);
			parseColumnStatement(x);
			statement = x;
		}
		
		return statement;
	}
	
	// The following methods parse the statements to extract data.
	
	private void parseVersionStatement(Version s) {
		// <version-statement> ::= <title> <version> <date>
		String[] tmp = s.Line().split("[ |\t]+");
		if (tmp.length != 7) {
			error(s,"Invalid syntax in version statement.");
			return;
		}
		if (!tmp[4].equals(Util.VERSION_ID)) {
			error(s,"There is no version keyword.");
			return;
		}
		s.setTitle(Util.TITLE_ID);
		s.setVersion(tmp[5]);
		s.setDate(tmp[6]);
	} 
	private void parseCommentStatement(Comment s) {
		// Nothing to do.
	}
	private void parseKeyStatement(KeySpecification s) {
		// <key-statement> ::= Key <column-name-list> <eol>
		String[] tmp = s.Line().split("[ |\t]+");
		int offset = 0;
		for (int i = 0; i < tmp.length; ++i) 
			if (tmp[i].equals(Util.KEY_ID))
				offset = i;
		if (tmp.length < (offset + 2)) {
			error(s,"Invalid syntax in key specification -- missing key list.");
			return;
		}
		String[] list = new String [tmp.length - (offset + 1)];
		for (int i = (offset + 1); i < tmp.length; ++i) {
			if (!verifyName(tmp[i])) {
				error(s,"Invalid name in key specification: " + tmp[i]);
				return;
			}
			list[i - (offset + 1)] = tmp[i];
		}
		s.setKeyName(list);			
	}
	private void parseGeneratedKeyStatement(GeneratedKeySpecification s) {
		// <key-statement> ::= Key <column-name> GENERATED FROM <column-name-list> <eol>
		String[] tmp = s.Line().split("[ |\t]+");
		int offset = 0;
		for (int i = 0; i < tmp.length; ++i) 
			if (tmp[i].equals(Util.KEY_ID))
				offset = i;
		if (tmp.length < (offset + 4)) {
			error(s,"Invalid syntax in key specification -- missing key column name.");
			return;
		}
		if (!(tmp[offset + 2].equals(Util.GENERATE_ID) && tmp[offset + 3].equals(Util.FROM_ID))) {
			error(s,"Invalid syntax in key Specification -- missing 'GENERATED FROM' clause.");
			return;
		}
		if (!verifyName(tmp[offset + 1])) {
			error(s,"Invalid name in key specification: " + tmp[offset + 1]);
			return;
		}
		s.setKeyName(tmp[offset + 1]);
		String[] list = new String [tmp.length - (offset + 4)];
		for (int i = offset + 4; i < tmp.length; ++i) {
			if (!verifyName(tmp[i])) {
				error(s,"Invalid name in key specification: " + tmp[i]);
				return;
			}
			list[i - (offset + 4)] = tmp[i];
		}
		s.setFromName(list);
	}
	private void parseTableStatement(TableDefinition s) {
		// <begin-table-definition> ::= TABLE <table-name> [<ignore-word>...] <eol>
		String[] tmp = s.Line().split("[ |\t]+");
		if (tmp.length < 2) {
			error(s,"Invalid syntax on TABLE setatement -- missing table name.");
			return;
		}
		if (!verifyName(tmp[1])) {
			error(s,"Invalid table name: " + tmp[1]);
			return;
		}
		s.setTableName(tmp[1]);
		if (tmp.length > 2) {
			s.setOther(s.Line().substring(s.Line().indexOf(tmp[1]) + tmp[1].length()));
		}
	}
	private void parseColumnStatement(ColumnDefinition s) {
		//<column-definition> ::= 
		//		[ <ws> ] <column-name> <datatype> { NULL | NOT NULL | DEFAULT } <any!Comma>] <eol>
		String[] tmp = s.Line().split("[ |\t]+");
		int offset = 0;
		for (int i = 0; i < tmp.length; ++i)  {
			if (tmp[i].length() > 0) {
				offset = i;
				break;
			}
		}
		// Set the column name.
		if (!verifyName(tmp[offset + 0])) {
			error(s,"Invalid column name: " + tmp[0]);
			return;
		}
		s.setColumnName(tmp[offset + 0]);
		// Set the NULL flag.
		boolean isNull = false;
		boolean isNotNull = false;
		boolean isDefault = false;
		for (int i = 1; i < tmp.length; ++i) {
			if (tmp[i].equals(Util.NULL_ID)) {
				isNull = true;
				break;
			}
			if (tmp[i].equals(Util.NOT_ID) && i < (tmp.length - 1) && tmp[i + 1].equals(Util.NULL_ID)) {
				isNotNull = true;
				break;
			}
			if (tmp[i].equals(Util.DEFAULT_ID)) {
				isDefault = true;
				break;
			}
		}
		if ((isNull && isNotNull) ||
			(isNull && isDefault) ||
			(isNotNull && isDefault)) {
			error(s,"Can only specify one of NULL, NOT_NULL, or DEFAULT on a column definition.");
			return;
		}
		if (!isNull && !isNotNull && !isDefault) {
			error(s,"NULL, NOT_NULL or DEFAULT is not specified.");
			return;
		}
		if (isNull)
			s.setOptionWord(Util.NULL_ID);
		if (isNotNull)
			s.setOptionWord(Util.NOT_ID + " " + Util.NULL_ID);
		if (isDefault)
			s.setOptionWord(Util.DEFAULT_ID);
		// Get the datatype (which is between columnName and NULL/NOT_NULL.
		int n = s.Line().indexOf(s.ColumnName());
		int m = isNull? s.Line().indexOf(Util.NULL_ID) : s.Line().indexOf(Util.NOT_ID);
		if (m == -1)
			m = s.Line().indexOf(Util.DEFAULT_ID);
		String datatype = s.Line().substring((n + s.ColumnName().length()), m - 1).trim();
		DatatypeInfo d = verifyDatatype(datatype);
		if (d == null) {
			error(s,"Invalid datatype: " + datatype);
			return;
		}		
		s.setDataType(d.type,d.charType,d.size);
		// Get the other words, if any.
		n = s.Line().indexOf(Util.NULL_ID);
		int l = Util.NULL_ID.length();
		if (n == -1) {
			n = s.Line().indexOf(Util.DEFAULT_ID);
			l = Util.DEFAULT_ID.length();
		}
		String other = s.Line().substring(n + l).trim();
		if (other.length() > 0)
			s.setOther(other);
	}
	private void parseConstraintStatement(ConstraintSpecification s) {
		// Nothing to do.
	}
	private void parseEndTableStatement(EndTableDefinition s) {
		// Nothing to do.
	}
	private void parseBeginDBStatement(BeginDBStatement s) {
		//<begin-db-statement> ::= DB [ <name> ] <eol>
		int n = Util.getNextWordStart(s.Line(), 0, Util.DB_ID);
		String name = Util.getWord(s.Line(),n);
		if (name.length() != 0) {
			int i = 0;
			for (; i < Util.DATABASE_VENDOR.length; ++i) {
				if (name.equals(Util.DATABASE_VENDOR[i]))
					break;
			}
			if (i == Util.DATABASE_VENDOR.length) {
				error(s,"Unrecognized database vendor name.");
				s.setDbVendor("");
			}
		}
		s.setDbVendor(name);
	}
	private void parseDBStatement(DBStatement s) {
		// Nothing to do.
	}
	private void parseEndDBStatement(EndDBStatement s) {
		// Nothing to do.
	}
	private void parseBeginNoteStatement(BeginNoteStatement s) {
		// Nothing to do.
	}
	private void parseNoteStatement(NoteStatement s) {
		// Nothing to do.
	}
	private void parseEndNoteStatement(EndNoteStatement s) {
		// Nothing to do.
	}

	private boolean verifyName(String s) {
		char c = 0;
		for (int i = 0; i < s.length(); ++i) {
			c = s.charAt(i);
			if (!((c >= 'a' && c <= 'z') || 
				(c >= 'A' && c <= 'Z') ||
				(c >= '0' && c <= '9') || c == '_'))
				return false;
		}
		return true;
	}
	
	private DatatypeInfo verifyDatatype(String datatype) {
		int size = 0;
		int i = 0;
		for (; i < Util.SQLDataType.length; ++i) {
			if (datatype.startsWith(Util.SQLDataType[i]))
				break;
		}
		if (i == Util.SQLDataType.length)
			return null;
		if (Util.isSizeRequired[i]) {
			// Verify that a size is specified.
			int n = datatype.indexOf("(");
			int m = datatype.indexOf(")");
			if (n == -1 || m == -1)
				return null;
			try {
				size = Integer.parseInt(datatype.substring(n + 1,m));
			} catch (NumberFormatException err) {
				return null;
			}
		} else {
			if (!datatype.equals(Util.SQLDataType[i]))
				return null;
		}		
		DatatypeInfo x = new DatatypeInfo ();
		x.type = Util.SQLDataType[i];
		x.charType = Util.isCharType[i];
		x.size = Util.isSizeRequired[i] ? size : Util.sizeInBytes[i];
		return x;
	}
}

class DatatypeInfo {
	String type;
	boolean charType;
	int size;
}
class StatementError extends Exception {
	
}
