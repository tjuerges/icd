/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ConstraintSpecification.java
 */
package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.core.ElementSet;

public class ConstraintSpecification extends SQLStatement {
	
	private String other;
	private ElementSet comment;
	private boolean last;

	public ConstraintSpecification (int lineNumber, String line, MetaEnvironment meta) {
		super(lineNumber, line, meta);
		setStatementType(Util.CONSTRAINT_STATEMENT);
		this.setName(Util.STATEMENT_NAME[Util.CONSTRAINT_STATEMENT]);
		other = "";
		comment = new ElementSet ();
		last = false;
	}

	public ElementSet Comment() {
		return comment;
	}

	public void setComment(ElementSet comment) {
		this.comment = comment;
	}

	public String Other() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
	
	public String toString() {
		return Line();
	}

	public boolean isLast() {
		return last;
	}
	
	public void setLast(boolean last) {
		this.last = last;
	}

	public String TrimmedLine() {
		return Line().trim();
	}

}


