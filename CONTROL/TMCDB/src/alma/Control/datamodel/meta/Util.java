/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Util.java
 */
package alma.Control.datamodel.meta;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;

public class Util {
	
	static public final String[] DATABASE_VENDOR = {
		"ORACLE",
		"HSQLDB",
		"MYSQL"
	};
	
	// Statement Types and Names
	static public final String[] STATEMENT_NAME = {
		"Version",
		"Comment",
		"KeySpecification",
		"GeneratedKeySpecification",
		"TableDefinition",
		"ColumnDefinition",
		"ConstraintSpecification",
		"EndTableDefinition",
		"BeginDBStatement",
		"DBStatement",
		"EndDBStatement",
		"BeginNoteStatement",
		"NoteStatement",
		"EndNoteStatement"
	};
	
	static public final int VERSION_STATEMENT 						= 0;
	static public final int COMMENT_STATEMENT 						= 1;
	static public final int KEY_SPECIFICATION_STATEMENT 			= 2;
	static public final int GENERATED_KEY_SPECIFICATION_STATEMENT 	= 3;
	static public final int TABLE_STATEMENT 						= 4;
	static public final int COLUMN_STATEMENT 						= 5;
	static public final int CONSTRAINT_STATEMENT 					= 6;
	static public final int END_TABLE_STATEMENT 					= 7;
	static public final int BEGIN_DB_STATEMENT 						= 8;
	static public final int DB_STATEMENT 							= 9;
	static public final int END_DB_STATEMENT 						= 10;
	static public final int BEGIN_NOTE_STATEMENT 					= 11;
	static public final int NOTE_STATEMENT 							= 12;
	static public final int END_NOTE_STATEMENT 						= 13;
	
	// Strings to identify elements of the grammar.
	static public final String TITLE_ID = "TMCDB SQL TABLE DEFINITIONS";
	static public final String VERSION_ID = "VERSION";
	static public final String COMMENT_ID = "//";
	static public final String GENERATE_ID = "GENERATED";
	static public final String FROM_ID = "FROM";
	static public final String KEY_ID = "KEY";
	static public final String LIST_DELIMITER_ID = ",";
	static public final String TABLE_ID = "TABLE";
	static public final String TABLE_END_ID = "ENDTABLE";
	static public final String NULL_ID = "NULL";
	static public final String NOT_ID = "NOT";
	static public final String DEFAULT_ID = "DEFAULT";
	static public final String CONSTRAINT_ID = "CONSTRAINT";
	static public final String DB_ID = "DB";
	static public final String ENDDB_ID = "ENDDB";
	static public final String NOTE_ID = "NOTE";
	static public final String ENDNOTE_ID = "ENDNOTE";
		
	static public final String[] SQLDataType = {
		"TINYINT",
		"SMALLINT",
		"INT",
		"INTEGER",
		"BIGINT",
		"FLOAT",
		"DOUBLE",
		"BLOB",
		"CHAR",
		"VARCHAR",
		"LONGVARCHAR",
		"BOOLEAN",
		"LENGTH",
		"TIME",
		"CLOB",
		"TEXT",
		"NAME",
		"LONGNAME"
	};
	static public final boolean[] isCharType = {
		false, 	// "TINYINT"
		false, 	// "SMALLINT"
		false, 	// "INT"
		false, 	// "INTEGER"
		false, 	// "BIGINT"
		false, 	// "FLOAT"
		false, 	// "DOUBLE"
		false, 	// "BLOB"
		true,	// "CHAR"
		true,	// "VARCHAR"
		true,	// "LONGVARCHAR"
		false, 	// "BOOLEAN"
		false, 	// "LENGTH"
		false, 	// "TIME"
		false, 	// "CLOB"
		true, 	// "TEXT"
		true, 	// "NAME"
		true 	// "LONGNAME"
	};
	static public final boolean[] isSizeRequired = {
		false, 	// "TINYINT"
		false, 	// "SMALLINT"
		false, 	// "INT"
		false, 	// "INTEGER"
		false, 	// "BIGINT"
		false, 	// "FLOAT"
		false, 	// "DOUBLE"
		false, 	// "BLOB"
		true,	// "CHAR"
		true,	// "VARCHAR"
		true,	// "LONGVARCHAR"
		false, 	// "BOOLEAN"
		false, 	// "LENGTH"
		false, 	// "TIME"
		false, 	// "CLOB"
		false, 	// "TEXT"
		false, 	// "NAME"
		false 	// "LONGNAME"
	};
	static public final int[] sizeInBytes = {
		1, 		// "TINYINT"
		2, 		// "SMALLINT"
		4, 		// "INT"
		4, 		// "INTEGER"
		8, 		// "BIGINT"
		4, 		// "FLOAT"
		8, 		// "DOUBLE"
		-1, 	// "BLOB"
		-1,		// "CHAR"
		-1,		// "VARCHAR"
		-1,		// "LONGVARCHAR"
		1, 		// "BOOLEAN"
		8, 		// "LENGTH"
		8, 		// "TIME"
		-1, 	// "CLOB"
		1024, 	// "TEXT"
		128, 	// "NAME"
		256 		// "LONGNAME"
	};
	
	// Special datatype names
	static public final String TIME = "TIME";
	static public final String LENGTH = "LENGTH";
	static public final String BLOB = "BLOB";
	static public final String CLOB = "CLOB";
	static public final String BOOLEAN = "BOOLEAN";
	
	// Return the datatype in various forms.
	
	static public final String[] DataTypeToOracleType = {
		"NUMBER (3)", 			// "TINYINT"
		"NUMBER (5)", 			// "SMALLINT"
		"NUMBER (10)", 			// "INT"
		"NUMBER (10)", 			// "INTEGER"
		"NUMBER (19)", 			// "BIGINT"
		"BINARY_FLOAT", 		// "FLOAT"
		"BINARY_DOUBLE", 		// "DOUBLE"
		"BLOB", 				// "BLOB"
		"CHAR",					// "CHAR"
		"VARCHAR",				// "VARCHAR"
		"VARCHAR2",				// "LONGVARCHAR"
		"CHAR (1)", 			// "BOOLEAN"
		"BINARY_DOUBLE", 		// "LENGTH"
		"NUMBER (19)", 			// "TIME"
		"CLOB", 				// "CLOB"
		"VARCHAR", 				// "TEXT"
		"VARCHAR", 				// "NAME"
		"VARCHAR" 				// "LONGNAME"
	};
	static public final String[] DataTypeToHSQLType = {
		"TINYINT", 				// "TINYINT"
		"SMALLINT", 			// "SMALLINT"
		"INTEGER", 				// "INT"
		"INTEGER", 				// "INTEGER"
		"BIGINT", 				// "BIGINT"
		"FLOAT", 				// "FLOAT"
		"DOUBLE", 				// "DOUBLE"
		"VARBINARY", 			// "BLOB"
		"CHAR",					// "CHAR"
		"VARCHAR",				// "VARCHAR"
		"LONGVARCHAR",			// "LONGVARCHAR"
		"BOOLEAN", 				// "BOOLEAN"
		"DOUBLE", 				// "LENGTH"
		"BIGINT", 				// "TIME"
		"LONGVARCHAR", 			// "CLOB"
		"LONGVARCHAR", 			// "TEXT"
		"VARCHAR", 	        	// "NAME"
		"VARCHAR"    			// "LONGNAME"
	};
	static public final String[] DataTypeToIDLType = {
		"long", 				// "TINYINT"
		"long", 				// "SMALLINT"
		"long", 				// "INT"
		"long", 				// "INTEGER"
		"long long", 			// "BIGINT"
		"float", 				// "FLOAT"
		"double", 				// "DOUBLE"
		"OctetSeq", 			// "BLOB"
		"string",				// "CHAR"
		"string",				// "VARCHAR"
		"string",				// "LONGVARCHAR"
		"boolean", 				// "BOOLEAN"
		"asdmIDLTypes::IDLLength", // "LENGTH"
		"asdmIDLTypes::IDLArrayTime", // "TIME"
		"string", 				// "CLOB"
		"string", 				// "TEXT"
		"string", 				// "NAME"
		"string" 				// "LONGNAME"
	};
	static public final String[] DataTypeToXMLType = {
		"integer", 				// "TINYINT"
		"integer", 				// "SMALLINT"
		"integer", 				// "INT"
		"integer", 				// "INTEGER"
		"integer", 				// "BIGINT"
		"float", 				// "FLOAT"
		"double", 				// "DOUBLE"
		"string", 				// "BLOB"
		"string",				// "CHAR"
		"string",				// "VARCHAR"
		"string",				// "LONGVARCHAR"
		"boolean", 				// "BOOLEAN"
		"double", 				// "LENGTH"
		"integer", 				// "TIME"
		"string", 				// "CLOB"
		"string", 				// "TEXT"
		"string", 				// "NAME"
		"string" 				// "LONGNAME"
	};
	static public final String[] DataTypeToJavaType = {
		"int", 					// "TINYINT"
		"int", 					// "SMALLINT"
		"int", 					// "INT"
		"int", 					// "INTEGER"
		"long", 				// "BIGINT"
		"float", 				// "FLOAT"
		"double", 				// "DOUBLE"
		"byte[]", 				// "BLOB"
		"String",				// "CHAR"
		"String",				// "VARCHAR"
		"String",				// "LONGVARCHAR"
		"boolean", 				// "BOOLEAN"
		"Length", 				// "LENGTH"
		"ArrayTime",			// "TIME"
		"String", 				// "CLOB"
		"String", 				// "TEXT"
		"String", 				// "NAME"
		"String" 				// "LONGNAME"
	};
	static public final String[] DataTypeToJavaTypeNullable = {
		"Integer", 				// "TINYINT"
		"Integer", 				// "SMALLINT"
		"Integer", 				// "INT"
		"Integer", 				// "INTEGER"
		"Long", 				// "BIGINT"
		"Float", 				// "FLOAT"
		"Double", 				// "DOUBLE"
		"byte[]", 				// "BLOB"
		"String",				// "CHAR"
		"String",				// "VARCHAR"
		"String",				// "LONGVARCHAR"
		"Boolean", 				// "BOOLEAN"
		"Length", 				// "LENGTH"
		"ArrayTime",			// "TIME"
		"String", 				// "CLOB"
		"String", 				// "TEXT"
		"String", 				// "NAME"
		"String" 				// "LONGNAME"
	};
	static public final String[] DataTypeToJDBCType = {
		"Int", 					// "TINYINT"
		"Int", 					// "SMALLINT"
		"Int", 					// "INT"
		"Int", 					// "INTEGER"
		"Long", 				// "BIGINT"
		"Float", 				// "FLOAT"
		"Double", 				// "DOUBLE"
		"Blob", 				// "BLOB"
		"String",				// "CHAR"
		"String",				// "VARCHAR"
		"String",				// "LONGVARCHAR"
		"Boolean", 				// "BOOLEAN"
		"Double", 				// "LENGTH"
		"Long",					// "TIME"
		"String", 				// "CLOB"
		"String", 				// "TEXT"
		"String", 				// "NAME"
		"String" 				// "LONGNAME"
	};
	static public final String[] DataTypeToJDBCStaticType = {
		"INTEGER", 				// "TINYINT"
		"INTEGER", 				// "SMALLINT"
		"INTEGER", 				// "INT"
		"INTEGER", 				// "INTEGER"
		"BIGINT", 				// "BIGINT"
		"FLOAT", 				// "FLOAT"
		"DOUBLE", 				// "DOUBLE"
		"BLOB", 				// "BLOB"
		"CHAR",					// "CHAR"
		"VARCHAR",				// "VARCHAR"
		"LONGVARCHAR",			// "LONGVARCHAR"
		"BOOLEAN", 				// "BOOLEAN"
		"DOUBLE", 				// "LENGTH"
		"BIGINT",				// "TIME"
		"LONGVARCHAR", 			// "CLOB"
		"LONGVARCHAR", 			// "TEXT"
		"VARCHAR", 				// "NAME"
		"VARCHAR" 			// "LONGNAME"
	};

	static private int indexDataType(String  type) {
		for (int i = 0; i < SQLDataType.length; ++i) {
			if (SQLDataType[i].equals(type))
				return i;
		}
		return -1;
	}
	static public String OracleType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToOracleType[index]; 
	}
	static public String HSQLType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToHSQLType[index]; 
	}
	static public String IDLType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToIDLType[index]; 
	}
	static public String XMLType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToXMLType[index]; 
	}
	static public String JavaType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToJavaType[index]; 
	}
	static public String JavaTypeNullable(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToJavaTypeNullable[index]; 
	}
	static public String JDBCType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToJDBCType[index]; 
	}
	static public String JDBCStaticType(String type) {
		int index = indexDataType(type);
		return (index == -1) ? "" : DataTypeToJDBCStaticType[index]; 
	}
	
	// Functions for returning a list of names in various forms.
	
	// one, two, three
	static public String Namelist (String[] name) {
        String result = "";
		for (int i = 0; i < name.length; ++i) {
			result += name[i] + (i == (name.length - 1) ? "" : ", ");
		}
		return result;
	 }

	// <nop>one, <nop>two, <nop>three
	static public String NopNamelist (String[] name) {
        String result = "";
		for (int i = 0; i < name.length; ++i) {
			result += "<nop>" + name[i] + (i == (name.length - 1) ? "" : ", ");
		}
		return result;
	 }

	// one + ", " + two + ", " + three
	static public String PrintList (String[] name) {
        String result = "";
        for (int i = 0; i < name.length; ++i) {
        	result += name[i] + (i == (name.length - 1) ? "" : " + \", \" + ");
        }
        return result;
    }

	// Starting at 'pos' return the posiition in 's' determined by skipping 'name' and the 
	// white space that follows it.
	static public int getNextWordStart(String s, int pos, String name) {
		int n = pos + name.length();
		while (n < s.length()) {
			if (!Character.isWhitespace(s.charAt(n)))
				break;
			else
				++n;
		}
		return n;
	}
	
	// Get the next word starting at the specified position in s.
	static public String getWord(String s, int pos) {
		String tmp = "";
		while (pos < s.length()) {
			if (Character.isWhitespace(s.charAt(pos)))
				break;
			else {
				tmp += s.charAt(pos);
				++pos;
			}
		}
		return tmp;
	}
	
	static public String UpperCaseName(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1);
	}

	static public String LowerCaseName(String s) {
		return s.substring(0,1).toLowerCase() + s.substring(1);
	}
		
	private static String decodeHTMLChars(String s) {
		if (s == null || s.length() == 0)
			return s;
		StringBuffer x = new StringBuffer();		
		int index = 0;
		int index1 = 0;
		int index2 = 0;
		int index3 = 0;
		int index4 = 0;
		int current = 0;
		while (true) {
			// look for any HTML encoded character
			index1 = s.indexOf("&lt;", current);
			index2 = s.indexOf("&gt;", current);
			index3 = s.indexOf("&amp;", current);
			index4 = s.indexOf("&#xd;", current);
			// if there aren't any, we're done
			if (index1 == -1 && index2 == -1 && index3 == -1 && index4 == -1) {
			    x.append(s.substring(current));
			    break;
			}
			// set index to the least value that is not -1
			index = index1;
			if (index == -1 || (index2 != -1 && index2 < index))
			    index = index2;
			if (index == -1 || (index3 != -1 && index3 < index))
			    index = index3;
			if (index == -1 || (index4 != -1 && index4 < index))
			    index = index4;
			// append the current text up to index
			x.append(s.substring(current,index));
			// append the correct character and skip over the HTML encoded characters
			if (index == index1) {
			    x.append('<');
			    current = index + 4;
			} else if (index == index2) {
			    x.append('>');
			    current = index + 4;
			} else if (index == index3) {
			    x.append('&');
			    current = index + 5;
			} else if (index == index4) {
			    x.append('\n');
			    current = index + 5;
			}
		}
		return new String (x);
	}

	
	static private final String newline = System.getProperty("line.separator");
	static private final int maxLength = 64;
	static private String formatDescriptionBase(String s, String prefix) {
		if (s == null)
			return "";
		String raw = decodeHTMLChars(s);
		StringBuffer extract = new StringBuffer ();
		int begin = 0;
		int current = 0;
		int len = raw.length();
		char c = 0;
		while (current < len) {
			c = raw.charAt(current);
			if (c == '&' && (current + 4) <= len && raw.charAt(current + 1) == '#' && 
						raw.charAt(current + 2) == '1' && raw.charAt(current + 3) == '0' &&
						raw.charAt(current + 4) == ';') {
				c = ' ';
				current += 4;
			}
			extract.append(c);
			if ((current - begin) > maxLength && c == ' ') {
				extract.append(newline);
				extract.append(prefix);
				begin = current + 1;
			}
			++current;
		}		
		return extract.toString();
	}
	static public String formatDescription(String s) {
		return formatDescriptionBase(s,"");
	}
	static public String formatDescriptionL1(String s) {
		return formatDescriptionBase(s," * ");
	}
	static public String formatDescriptionL2(String s) {
		return formatDescriptionBase(s,"     * ");
	}
    static public String formatDescriptionL4(String s) {
        return formatDescriptionBase(s,"// ");
    }
    static public String formatDescriptionL5(String s) {
        return formatDescriptionBase(s,"    // ");
    }

	static public String descriptionAsString(String s) {
		if (s == null)
			return "";
		String raw = decodeHTMLChars(s);
		StringBuffer extract = new StringBuffer ();
		int current = 0;
		int len = raw.length();
		char c = 0;
		while (current < len) {
			c = raw.charAt(current);
			if (c == '&' && (current + 4) <= len && raw.charAt(current + 1) == '#' && 
						raw.charAt(current + 2) == '1' && raw.charAt(current + 3) == '0' &&
						raw.charAt(current + 4) == ';') {
				c = ' ';
				current += 4;
			}
			extract.append(c);
			++current;
		}		
		return extract.toString();
	}

    private static void error(String msg) {
        System.out.println(msg);
        System.exit(0);
    }
    /**
     * The RemoveLines is a simple program to remove blank lines from a file.
     * Input is the directory name.  Multiple blank lines are replaced by a single
     * blank line.
     */
    public static void RemoveLines(String dirName, int replace) {
        System.out.println("\tReading directory " + dirName + " eliminating blank lines.");
        System.out.println("\tReplacing multiple blank lines with " + replace + " line.");

        PrintStream out = null;
        LineNumberReader in = null;
        String line = null;

        File dir = new File (dirName);
        if (!dir.isDirectory())
            error(dir.getAbsolutePath() + " is not a directory.");
        File[] file = dir.listFiles();

        for (int i = 0; i < file.length; ++i) {
            if (!(file[i].isFile() && file[i].getName().endsWith(".java") ||
                    file[i].isFile() && file[i].getName().endsWith(".html") ||
                    file[i].isFile() && file[i].getName().endsWith(".idl") ||
                    file[i].isFile() && file[i].getName().endsWith(".h") ||
                    file[i].isFile() && file[i].getName().endsWith(".cpp") ||
                    file[i].isFile() && file[i].getName().endsWith(".xsd") ||
                    file[i].isFile() && file[i].getName().endsWith(".xml") ||
                    file[i].isFile() && file[i].getName().endsWith(".sql") ||
                    file[i].isFile() && file[i].getName().endsWith(".txt")
            ))
                continue;

            try {
                // Open the input file.
                in = new LineNumberReader(new FileReader(file[i]));

                // Create the output file.
                String newName = file[i].getAbsolutePath() + ".tmp";
                out = new PrintStream (new FileOutputStream (new File(newName)));

                line = in.readLine();
                // Skip all blank lines at the beginning.
                while (true) {
                    if (line != null && line.trim().length() == 0)
                        line = in.readLine();
                    else
                        break;
                }
                boolean blankFound = false;
                while (line != null) {
                    if (line.trim().length() == 0) {
                        blankFound = true;
                    } else {
                        if (blankFound) {
                            blankFound = false;
                            if (replace == 1)
                                out.println();
                        }
                        out.println(line);
                    }
                    line = in.readLine();
                }

                // Close the input file and the output file.
                in.close();
                out.close();

                // Delete the old file and rename the new file.
                if (!file[i].delete())
                    error("Could not delete " + file[i].getAbsolutePath());
                File newFile = new File (newName);
                newFile.renameTo(file[i]);

            } catch (IOException err) {
                error(err.toString());
            }

        }
    }
    public static void RemoveLines(String dirName) {
    	RemoveLines(dirName,1);
    }
    public static void RemoveBlanks(String dirName) {
    	RemoveLines(dirName,0);
    }
    public static void RemoveLinesFromFile(String dirName, String filename, int replace){
        System.out.println("\tReading file " + filename + " in directory " + dirName + " eliminating blank lines.");
        System.out.println("\tReplacing multiple blank lines with " + replace + " line.");

        PrintStream out = null;
        LineNumberReader in = null;
        String line = null;

        File file = new File (dirName,filename);
        if (!file.isFile())
            error(file.getAbsolutePath() + " is not a file.");

        try {
            // Open the input file.
            in = new LineNumberReader(new FileReader(file));

            // Create the output file.
            String newName = file.getAbsolutePath() + ".tmp";
            out = new PrintStream (new FileOutputStream (new File(newName)));

            line = in.readLine();
            // Skip all blank lines at the beginning.
            while (true) {
                if (line != null && line.trim().length() == 0)
                    line = in.readLine();
                else
                    break;
            }
            boolean blankFound = false;
            while (line != null) {
                if (line.trim().length() == 0) {
                    blankFound = true;
                } else {
                    if (blankFound) {
                        blankFound = false;
                        if (replace == 1)
                            out.println();
                    }
                    out.println(line);
                }
                line = in.readLine();
            }

            // Close the input file and the output file.
            in.close();
            out.close();

            // Delete the old file and rename the new file.
            if (!file.delete())
                error("Could not delete " + file.getAbsolutePath());
            File newFile = new File (newName);
            newFile.renameTo(file);

        } catch (IOException err) {
            error(err.toString());
        }

    }
    
	private static final String [] MONTH = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};  
    public static String toDatabaseDate(String s) {
    	int pos = 0;
    	int n = s.indexOf('-'); 	if (n == -1) return "";
    	String yy = s.substring(pos,n);
    	if (yy.startsWith("20"))
    		yy = yy.substring(2);
    	pos = n + 1;
    	n = s.indexOf('-',pos); 	if (n == -1) return "";
    	String mm = s.substring(pos,n);
    	int nmm = 0;
    	try {
    		nmm = Integer.parseInt(mm);
    	} catch (NumberFormatException err) {
    		return "";
    	}
    	if (nmm < 1 || nmm > 12) return "";
    	pos = n + 1;
    	n = s.indexOf('T',pos); 	if (n == -1) return "";
    	String dd = s.substring(pos,n);
    	return yy + '-' +  MONTH[nmm - 1] + '-' + dd;
    }
    
    public static String lineToTWiki (String s) {
    	String[] word = s.split("\\s+");
    	boolean[] isNOP = new boolean [word.length];
    	for (int i = 0; i < word.length; ++i) {
    		isNOP[i] = false;
    		if (word[i].length() < 3)
    			continue;
    		if (Character.isUpperCase(word[i].charAt(0))) {
    			for (int j = 2; j < word[i].length(); ++j) {
    				if (Character.isUpperCase(word[i].charAt(j))) {
    					isNOP[i] = true;
    					break;
    				}
    			}
    		}
    	}
    	StringBuffer x = new StringBuffer ();
    	int pos = 0;
    	int index = 0;
    	for (int i = 0; i < word.length; ++i) {
    		if (isNOP[i]) {
    			index = s.indexOf(word[i],pos);
    			x.append(s.substring(pos,index));
    			x.append("<nop>");
    			x.append(word[i]);
    			pos = index + word[i].length();
    		}
    	}
    	if (pos < s.length())
    		x.append(s.substring(pos));
    	return x.toString();
    }
    
    public static void main(String[] arg) {
    	String in = "PropertyTypeId is a generated unique key.  The real requirement is that combination of AssemblyName and PropertyName are unique.";
    	String out = lineToTWiki(in);
    	System.out.println(">>in  " + in);
    	System.out.println(">>out " + out);
	
    }
}
