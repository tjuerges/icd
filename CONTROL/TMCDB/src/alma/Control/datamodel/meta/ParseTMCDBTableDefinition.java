package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.constraint.DesignError;

import java.util.ArrayList;
import java.util.Iterator;

public class ParseTMCDBTableDefinition {

	static private final String newline = System.getProperty("line.separator");

	private MetaEnvironment meta;
	private String dirName;
	private String fileName;
	
	private ElementSet tableList;
	private Version version;
	private ElementSet note;
	private SQLStatement[] sql;

	public ParseTMCDBTableDefinition (String dirName, String fileName, MetaEnvironment meta) {
		this.dirName = dirName;
		this.fileName = fileName;
		this.meta = meta;
		tableList = new ElementSet ();
		version = new Version (0,"",meta);
		note = new ElementSet ();
		sql = new SQLStatement [0];
	}
	
	public Version Version() {
		return version;
	}
	
	public ElementSet TableDefinition() {
		return tableList;
	}
	
	public ElementSet Note() {
		return note;
	}
	
	public void createModel() throws DesignError {
		// Do the pass1 parsing.
		Pass1Parser p = new Pass1Parser(dirName,fileName,meta);
		// Check for errors.
		sql = p.parse();
		if (sql.length == 0)
			error("Processing termindated!  There are no statemnts.");			
		String[] err = p.getErrors();
		if (err.length > 0) {
			String s = "";
			for (int i = 0; i < err.length; ++i) {
				s += err[i] + newline;
			}
			throw new DesignError ("Processing termindated because of errors in TMCDB Table Definition." + newline + s);
		}
		
		// OK, on to pass2 processing.

		statementCount = 0;
		comment = new ArrayList<Comment> ();
		tableON = false;
		dbON = false;
		currentTable = null;
		dbVendor = "";

		// First, get the Version statement.
		if (!(sql[statementCount] instanceof Version))
			error("Processing termindated!  The first statement must be a version statement.");
		version = (Version)sql[statementCount];
		++statementCount;
		
		// Next, get any notes that might be attached.
		if (sql[statementCount] instanceof BeginNoteStatement) {
			++statementCount;
			NoteStatement x = null;
			boolean found = false;
			for (int i = statementCount; i < sql.length; ++i) {
				if (sql[statementCount] instanceof EndNoteStatement) {
					++statementCount;
					found = true;
					break;
				}
				if (sql[statementCount] instanceof NoteStatement) {
					x = (NoteStatement)sql[statementCount];
					note.add(x);
					++statementCount;
				} else {
					error(sql[statementCount], "Internal Error.");
				}
			}
			if (!found)
				error("There is no terminating " + Util.ENDNOTE_ID + ".");
		}

		// Now, all we have left are the table definitions.
		for (int i = statementCount; i < sql.length; ++i) {
			// For debugging only.
			// System.out.println(sql[i].getLineNumber() + ". [" + sql[i].NameS() + "][" + sql[i].getStatementType() + "] " + sql[i].getLine());
			
			switch (sql[i].getStatementType()) {
			case Util.COMMENT_STATEMENT: doCommentStatement(sql[i]); break;
			case Util.TABLE_STATEMENT: doTableStatement(sql[i]); break;
			case Util.KEY_SPECIFICATION_STATEMENT: doKeyStatement(sql[i]); break;
			case Util.GENERATED_KEY_SPECIFICATION_STATEMENT: doGeneratedKeyStatement(sql[i]); break;
			case Util.COLUMN_STATEMENT: doColumnStatement(sql[i]); break;
			case Util.CONSTRAINT_STATEMENT: doConstraintStatement(sql[i]); break;
			case Util.END_TABLE_STATEMENT: doEndTableStatement(sql[i]); break;
			case Util.BEGIN_DB_STATEMENT: doBeginDBStatement(sql[i]); break;
			case Util.DB_STATEMENT: doDBStatement(sql[i]); break;
			case Util.END_DB_STATEMENT: doEndDBStatement(sql[i]); break;
			default: error(sql[statementCount], "Unrecognized statement.");
			};
			
			++statementCount;
		}
		
		// Check for any errors.
		if (tableON)
			error("There is no end of table definition statement matching table at line " + currentTable.getLineNumber());
		if (dbON)
			error("There is no end DB statement.");			
		if (tableList.size() == 0)
			error("There are no tables defined.");
	}
	
	private void error (String err) {
		String s = "TMCDB Table Definition error: " + err;				
		throw new DesignError (s);
	}
	private void error (SQLStatement s, String err) {
		String x = "TMCDB Table Definition error: " + err + " Line number " + s.getLineNumber() + " >>" + s.Line();
		throw new DesignError (x);
	}
	
	private int statementCount;
	private boolean tableON;
	private boolean dbON;
	private ArrayList<Comment> comment;
	private TableDefinition currentTable;
	private String dbVendor;
	
	// Comment statement
	private void doCommentStatement(SQLStatement s) {
		comment.add((Comment)sql[statementCount]);
	}
	
	// Key statement
	private void doKeyStatement(SQLStatement s) {
		if (!tableON)
			error(s,"Misplaced statement -- key statement is not within a table definition.");
		KeySpecification key = (KeySpecification)sql[statementCount];
		if (currentTable.isKeySet())
			error(s,"Key has already been specified.  Only one key spcification is permitted.");
		currentTable.setKey(key);
	}
	
	// Generated key statement
	private void doGeneratedKeyStatement(SQLStatement s) {
		if (!tableON)
			error(s,"Misplaced statement -- generated key statement is not within a table definition.");
		GeneratedKeySpecification key = (GeneratedKeySpecification)sql[statementCount];
		if (currentTable.isKeySet())
			error(s,"Key has already been specified.  Only one key specification is permitted.");
		currentTable.setGeneratedKey(key);
	}
	
	// Table statement
	private void doTableStatement(SQLStatement s) {
		if (tableON)
			error(s,"Misplaced statement -- no end table statement on previous table definition.");
		tableON = true;
		currentTable = (TableDefinition)sql[statementCount];
		ElementSet x = new ElementSet ();
		Iterator<Comment> iter = comment.iterator();
		while (iter.hasNext()) {
			x.add(iter.next());
		}
		currentTable.setComment(x);
		tableList.add(currentTable);
		comment.clear();
	}
	
	// Column statement
	private void doColumnStatement(SQLStatement s) {
		if (!tableON)
			error(s,"Misplaced statement -- column definition is not within a table definition.");
		ColumnDefinition col = (ColumnDefinition)sql[statementCount];
		ElementSet x = new ElementSet ();
		Iterator<Comment> iter = comment.iterator();
		while (iter.hasNext()) {
			x.add(iter.next());
		}
		col.setComment(x);
		currentTable.addColumn(col);
		comment.clear();
	}
	
	// Constraint statement
	private void doConstraintStatement(SQLStatement s) {
		if (!tableON)
			error(s,"Misplaced statement -- constraint statement is not within a table definition.");
		ConstraintSpecification con = (ConstraintSpecification)sql[statementCount];
		ElementSet x = new ElementSet ();
		Iterator<Comment> iter = comment.iterator();
		while (iter.hasNext()) {
			x.add(iter.next());
		}
		con.setComment(x);
		currentTable.addConstraint(con);
		comment.clear();		
	}
	
	// End table statement
	private void doEndTableStatement(SQLStatement s) {
		if (!tableON)
			error(s,"Misplaced statement -- end table statement without a table definition.");
		tableON = false;
		
		// There must be a key specification.
		if ( !currentTable.isKeySet() || 
			 (currentTable.isGenerated() && currentTable.GeneratedKey() == null) ||
			 (!currentTable.isGenerated() && currentTable.Key() == null) )
			error("There is no key specification in the table definition beginning at line number " + currentTable.getLineNumber());
		
		// There must be at least one column.
		if (currentTable.Column().size() == 0)
			error("The table definition beginning at line number " + currentTable.getLineNumber() + " hase no column definitions.");
		
		// Set the last column in the table.
		currentTable.setLastColumn();
		
		// Get the key or generated key and set the references to the columns.
		if (currentTable.isGenerated()) {
			GeneratedKeySpecification x = (GeneratedKeySpecification)currentTable.GeneratedKey();
			ElementSet column = currentTable.Column();
			boolean found = false;
			String key = x.KeyName();
			Iterator iter = column.iterator();
			while (iter.hasNext()) {
				ColumnDefinition c = (ColumnDefinition)iter.next();
				if (c.ColumnName().equals(key)) {
					x.setKey(c);
					found = true;
					break;
				}
			}
			if (!found)
				error("In the table defined beginning at line number " + currentTable.getLineNumber() + 
						" in the generated key specification the key name, " + key + ", does not refer to a named column in the table.");
			ElementSet from = x.From();
			String[] fromName = x.getFromName();
			for (int i = 0; i < fromName.length; ++i) {
				found = false;
				iter = column.iterator();
				while (iter.hasNext()) {
					ColumnDefinition c = (ColumnDefinition)iter.next();
					if (c.ColumnName().equals(fromName[i])) {
						c.setKeyIndex(i + 1);
						from.add(c);
						found = true;
						break;
					}
				}
				if (!found)
					error("In the table defined beginning at line number " + currentTable.getLineNumber() + 
							" in the generated key specification a name, " + fromName[i] + ", does not refer to a named column in the table.");
			}			
		} else {
			KeySpecification x = (KeySpecification)currentTable.Key();
			ElementSet key = x.Key();
			String[] name = x.getKeyName();
			ElementSet column = currentTable.Column();
			boolean found;
			for (int i = 0; i < name.length; ++i) {
				found = false;
				Iterator iter = column.iterator();
				while (iter.hasNext()) {
					ColumnDefinition c = (ColumnDefinition)iter.next();
					if (c.ColumnName().equals(name[i])) {
						c.setKeyIndex(i + 1);
						key.add(c);
						found = true;
						break;
					}
				}
				if (!found)
					error("In the table defined beginning at line number " + currentTable.getLineNumber() + 
							" in the key specification a name, " + name[i] + ", does not refer to a named column in the table.");
			}
		}
		
		// Do any end-table processing
		currentTable.setAtEnd();
	}
	
	// Begin DB statement
	private void doBeginDBStatement(SQLStatement s) {
		if (currentTable == null) 
			error(s,"Misplaced statement -- DB statement block must follow table definition.");
		if (tableON)
			error(s,"Misplaced statement -- DB statement block must not be in a table definition; it must follow.");
		if (dbON)
			error(s,"The previous DB statement has no end-DB statement.");
		dbON = true;
		dbVendor = ((BeginDBStatement)sql[statementCount]).DbVendor();
	}
	
	// DB statement
	private void doDBStatement(SQLStatement s) {
		if (!dbON)
			error(s,"Misplaced statement -- DB statement must not be within a db statement block.");
		DBStatement x = (DBStatement)sql[statementCount];
		x.setDbVendor(dbVendor);
		currentTable.addDB(x);
	}
	
	// End DB statement
	private void doEndDBStatement(SQLStatement s) {
		if (!dbON)
			error(s,"Misplaced statement -- end DB statement without a begin DB statement.");
		dbON = false;
		dbVendor = "";
	}

}
