/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TableDefinition.java
 */
package alma.Control.datamodel.meta;

import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;

import java.util.Iterator;

public class TableDefinition extends SQLStatement {
	
	private String tableName;
	private String other;
	private ElementSet comment;
	private boolean generated;
	private boolean keySet;
	private KeySpecification key;
	private GeneratedKeySpecification generatedKey;
	private ElementSet column;
	private ElementSet constraint;
	private ElementSet db;
	private int columnCount;
	private boolean hasATimeColumn;
	private boolean hasALengthColumn;

	public TableDefinition (int lineNumber, String line, MetaEnvironment meta) {
		super(lineNumber, line, meta);
		setStatementType(Util.TABLE_STATEMENT);
		this.setName(Util.STATEMENT_NAME[Util.TABLE_STATEMENT]);
		tableName = "";
		other = "";
		comment = new ElementSet ();
		generated = false;
		keySet = false;
		key = null;
		generatedKey = null;
		column = new ElementSet ();
		constraint = new ElementSet ();
		db = new ElementSet();
		columnCount = 0;
		hasATimeColumn = false;
		hasALengthColumn = false;
	}

	public ElementSet Column() {
		return column;
	}

	public void addColumn(ColumnDefinition column) {
		++columnCount;
		column.setColumnNumber(columnCount);
		this.column.add(column);
	}
	
	public void setLastColumn() {
		if (constraint.size() > 0) {
			ConstraintSpecification c = null;
			Iterator iter = constraint.iterator();
			while (iter.hasNext())
				c = (ConstraintSpecification)iter.next();
			c.setLast(true);
		}
		ColumnDefinition x = null;
		Iterator iter = column.iterator();
		while (iter.hasNext()) {
			x = (ColumnDefinition)iter.next();
			if (x.ColumnNumber().equals(Integer.toString(columnCount))) {
				x.setLast(true);
				return;
			}
		}
	}

	public ElementSet Comment() {
		return comment;
	}

	public void setComment(ElementSet comment) {
		this.comment = comment;
	}

	public ElementSet Constraint() {
		return constraint;
	}

	public void addConstraint(ConstraintSpecification constraint) {
		this.constraint.add(constraint);
	}

	public ElementSet DB() {
		return db;
	}

	public void addDB(DBStatement db) {
		this.db.add(db);
	}

	public boolean isGenerated() {
		return generated;
	}
	
	boolean isKeySet() {
		return keySet;
	}
	
	public KeySpecification Key() {
		return key;
	}

	public GeneratedKeySpecification GeneratedKey() {
		return generatedKey;
	}

	public void setKey(KeySpecification key) {
		generated = false;
		keySet = true;
		this.key = key;
	}

	public void setGeneratedKey(GeneratedKeySpecification generatedKey) {
		generated = true;
		keySet = true;
		this.generatedKey = generatedKey;
	}

	public String Other() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String TableName() {
		return tableName;
	}

	public String ShortTableName() {
		if (tableName.length() <= 12)
			return tableName;
		String s = tableName.substring(0,6);
		for (int i = 6; i < tableName.length(); ++i)
			if (Character.isUpperCase(tableName.charAt(i)))
				s += tableName.charAt(i);
		return s;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String toString() {
		return tableName + ", " + other;
	}
	
	public void setAtEnd() {
		Iterator iter = column.iterator();
		ColumnDefinition c = null;
		while (iter.hasNext()) {
			c = (ColumnDefinition)iter.next();
			if (c.DataType().equals(Util.TIME))
				hasATimeColumn = true;
			if (c.DataType().equals(Util.LENGTH))
				hasALengthColumn = true;
		}
		
	}
	
	public boolean HasTime() {
		return hasATimeColumn;
	}
	
	public boolean HasLength() {
		return hasALengthColumn;
	}
	
	public boolean HasConstraints() {
		return constraint.size() > 0;
	}

	public String InsertPhrase() {
		String s = "";
		int istart = 0;
		if (isGenerated()) istart = 1;	// Don't set the automatically generated ID on insert
		for (int i = istart; i < column.size(); ++i)
			s += (i == (column.size() - 1)) ? "?" : "?, ";
		return s;
	}
	
	public String InsertColumnNamesPhrase() {
		if (!isGenerated()) return null;	// This is only necessary for automatically generated keys
		String s = "";
		String cname;
		for (int i = 1; i < column.size(); ++i) {
			cname = ((ColumnDefinition)(column.get(i))).ColumnName();
			s += (i == (column.size() - 1)) ? cname : cname+", ";
		}
		return s;
	}

	public String SetPhrase() {
		String s = "";
		for (int i = 0; i < column.size(); ++i) {
			s += ((ColumnDefinition)column.get(i)).ColumnName() + " = ?";
			if (i < column.size() - 1)
				s += ", ";
			else
				s += " ";
		}
		return s;
	}

	public String NumberColumns() {
		return Integer.toString(column.size());
	}
}


