/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File GeneratedKeySpecification.java
 */
package alma.Control.datamodel.meta;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.DesignError;
import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;

public class GeneratedKeySpecification extends SQLStatement {
	
	private String keyName;
	private String[] fromName;
	private Element key;
	private ElementSet from;
	
	public GeneratedKeySpecification (int lineNumber, String line, MetaEnvironment meta) {
		super(lineNumber, line, meta);
		setStatementType(Util.GENERATED_KEY_SPECIFICATION_STATEMENT);
		this.setName(Util.STATEMENT_NAME[Util.GENERATED_KEY_SPECIFICATION_STATEMENT]);
		keyName = "";
		fromName = new String [0];
		key = null;
		from = new ElementSet();
	}

	public ElementSet From() {
		return from;
	}

	public void setFrom(ElementSet from) {
		this.from = from;
	}

	public String[] getFromName() {
		return fromName;
	}

	public void setFromName(String[] fromName) {
		if (fromName == null || fromName.length == 0)
		; else //	throw new DesignError("Error on line number " + getLineNumber()  + ": [" + this.Line() + "] 'GENERATED FROM' phrase cannot be null.");
		this.fromName = fromName;
	}

	public Element Key() {
		return key;
	}

	public void setKey(Element key) {
		this.key = key;
	}

	public String KeyName() {
		return keyName;
	}
	
	public String NopKeyName() {
		return "<nop>" + keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String toString() {
		String s = keyName + ": ";
		for (int i = 0; i < fromName.length; ++i)
			s += fromName[i] + (i < fromName.length -1 ? ", " : "");
		return s;
	}
	
	public boolean isNoKeyDependency() {
		return fromName.length == 0;
	}
	
	public String FromList() {
		return Util.Namelist(fromName);
	}

	public String NopFromList() {
		return Util.NopNamelist(fromName);
	}
	
	public String PrintList() {
		return Util.PrintList(fromName);
	}

	public String KeyPhrase () {
		return keyName + " = ?";
	}

	public String AltPhrase() {
		String s = "";
		for (int i = 0; i < fromName.length; ++i)
			s += fromName[i] + ((i == (fromName.length - 1)) ? " = ?" : " = ? AND ");
		return s;
	}

	public String ArgPhrase() {
		return "int " + keyName;
	}
	
	public String AltArgPhrase() {
		Iterator iter = from.iterator();
		ColumnDefinition c = null;
		String result = "";
		for (int i = 0; i < fromName.length; ++i) {
			if (!iter.hasNext())
				throw new DesignError("Internal Error!  Mismatch between key names and elements.");
			c = (ColumnDefinition)iter.next();
			result += c.JavaType() + " " + fromName[i] + (i == (fromName.length - 1) ? "" : ", ");
        }
		return result;
	}

	public String AltDataPhrase() {
		String s = "";
		for (int i = 0; i < fromName.length; ++i)
			s += "data.get" + fromName[i] + "()" + ((i == (fromName.length - 1)) ? " " : ", ");
		return s;
	}

	public String AltPrintPhrase() {
		if (fromName.length == 0)
			return "\" \"";
		String s = "";
		for (int i = 0; i < fromName.length; ++i)
			s += "data.get" + fromName[i] + "()" + ((i == (fromName.length - 1)) ? "" : " + \", \" + ");
		return s;
	}

}




