SET SERVEROUTPUT ON;

DECLARE
    xAntennaId NUMBER;
    xPadId NUMBER;
	xPointingModelId NUMBER;
	
BEGIN

SELECT COUNT(PointingModelId) INTO xPointingModelId FROM AntennaPointingModel;

xAntennaId := 2;
xPadId := 2;
xPointingModelId := xPointingModelId + 1;
INSERT INTO AntennaPointingModel VALUES (
    xPointingModelId,
    xAntennaId, 
    xPadId, 
    '01-OCT-10 8:0:0.0 AM',
    '',
    'No-ASDM-link'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'IA',
    '-105.36',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'IE',
    '-700.95',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HASA',
    '0.17',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HACA',
    '-0.46',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HESE',
    '10.29',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HECE',
    '-25.95',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HESA',
    '-1.42',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HASA2',
    '-1.31',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HACA2',
    '3.60',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HESA2',
    '-2.46',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HECA2',
    '2.51',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HACA3',
    '0.27',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HECA3',
    '-0.06',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'HESA3',
    '0.0',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'NPAE',
    '28.9',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'CA',
    '-837.14',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'AN',
    '6.76',
    '0.0'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'AW',
    '5.93',
    '0.0'
);



COMMIT;
END;
/
.
