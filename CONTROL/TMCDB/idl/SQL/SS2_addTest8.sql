SET SERVEROUTPUT ON;

DECLARE
    xAntennaId NUMBER;
    xPadId NUMBER;
	xPointingModelId NUMBER;
	
BEGIN

SELECT COUNT(PointingModelId) INTO xPointingModelId FROM AntennaPointingModel;

xAntennaId := 1;
xPadId := 1;
xPointingModelId := xPointingModelId + 1;
INSERT INTO AntennaPointingModel VALUES (
    xPointingModelId,
    xAntennaId, 
    xPadId, 
    '01-OCT-06 6:10:5.0 PM',
    '',
    'No-ASDM-link'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term1',
    '11.0',
    '0.11'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term2',
    '12.0',
    '0.12'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term3',
    '13.0',
    '0.13'
);

xAntennaId := 2;
xPadId := 2;
xPointingModelId := xPointingModelId + 1;
INSERT INTO AntennaPointingModel VALUES (
    xPointingModelId,
    xAntennaId, 
    xPadId, 
    '01-OCT-06 6:10:5.0 PM',
    '',
    'No-ASDM-link'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term1',
    '21.0',
    '0.21'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term2',
    '22.0',
    '0.22'
);
INSERT INTO AntennaPointingModelTerm VALUES (
    xPointingModelId,
    'Term3',
    '23.0',
    '0.23'
);



COMMIT;
END;
/
.
