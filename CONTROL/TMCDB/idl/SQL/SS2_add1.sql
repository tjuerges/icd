SET SERVEROUTPUT ON;

DECLARE
    xConfigurationId NUMBER;
    xAntennaId NUMBER;
    xPadId NUMBER;
    xComputerId NUMBER;
    xContainerId NUMBER;
    xComponentId NUMBER;

BEGIN

SELECT COUNT(ConfigurationId) INTO xConfigurationId FROM Configuration;
SELECT COUNT(BaseElementId) INTO xAntennaId FROM Antenna;
SELECT COUNT(BaseElementId) INTO xPadId FROM Pad;
SELECT COUNT(ComputerId) INTO xComputerId FROM Computer;
SELECT COUNT(ContainerId) INTO xContainerId FROM Container;
SELECT COUNT(ComponentId) INTO xComponentId FROM Component;

xConfigurationId := xConfigurationId + 1;
INSERT INTO Configuration VALUES (
    xConfigurationId,
    'SharedSim2', 
    'Shared Simulator 2', 
    '01-OCT-06',
    'End to end testing of elementary single-field interferometry: A continuation of Shared Simulator 1 with an emphasis on the internal contents of the ASDM.'
);

xComputerId := xComputerId + 1;
INSERT INTO Computer VALUES (
    xComputerId,
    'alpha',
    xConfigurationId,
    'its04.aoc.nrao.edu',
    'y',
    'uni',
    'NRAO AOC room #223'
);

xContainerId := xContainerId + 1;
INSERT INTO Container VALUES (
    xContainerId,
    'CONTROL/ABM01/cppContainer',
    xConfigurationId,
    'C++',
    'y',
    'ABM',
    '',
    ''
);

xComponentId := xComponentId + 1;
INSERT INTO Component VALUES (
    xComponentId,
    'CONTROL/ABM01',
    xConfigurationId,
    'C++',
    'y',
    'antenna',
    'IDL:alma/Control/Antenna:1.0'
);

xAntennaId := xAntennaId + 1;
INSERT INTO Antenna VALUES (
    xAntennaId,
    xConfigurationId,
    'AntSimVA1',
    'VA',
    '12.0',
    '01-OCT-06',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    xComputerId,
    xContainerId,
    xComponentId
);

xPadId := xPadId + 1;
INSERT INTO Pad VALUES (
    xPadId,
    xConfigurationId,
    'PAD001',
    '01-OCT-06',
    '-1601361.555455',
    '-5042191.805932',
    '3554531.803007'
);

INSERT INTO AntennaToPad VALUES (
    xAntennaId,
    xPadId,
    '01-OCT-06 6:5:15.0 PM',
    '',
    'n'
);

xComputerId := xComputerId + 1;
INSERT INTO Computer VALUES (
    xComputerId,
    'beta',
    xConfigurationId,
    'its05.aoc.nrao.edu',
    'y',
    'uni',
    'NRAO AOC room #224'
);

xContainerId := xContainerId + 1;
INSERT INTO Container VALUES (
    xContainerId,
    'CONTROL/ABM02/cppContainer',
    xConfigurationId,
    'C++',
    'y',
    'ABM',
    '',
    ''
);

xComponentId := xComponentId + 1;
INSERT INTO Component VALUES (
    xComponentId,
    'CONTROL/ABM02',
    xConfigurationId,
    'C++',
    'y',
    'antenna',
    'IDL:alma/Control/Antenna:1.0'
);

xAntennaId := xAntennaId + 1;
INSERT INTO Antenna VALUES (
    xAntennaId,
    xConfigurationId,
    'AntSimAEC1',
    'AEC',
    '12.0',
    '01-OCT-06',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    '0.0',
    xComputerId,
    xContainerId,
    xComponentId
);

xPadId := xPadId + 1;
INSERT INTO Pad VALUES (
    xPadId,
    xConfigurationId,
    'PAD002',
    '01-OCT-06',
    '-1601328.438917',
    '-5042203.194271',
    '3554532.360703'
);

INSERT INTO AntennaToPad VALUES (
    xAntennaId,
    xPadId,
    '01-OCT-06 6:10:5.0 PM',
    '',
    'n'
);


COMMIT;
END;
/
.
