�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::Control::datamodel::meta�

�DEFINE Root FOR TMCDBModel�
�EXPAND JavaClass FOREACH (Set[TableDefinition])TableDefinition�
�ENDDEFINE�

�DEFINE JavaClass FOR TableDefinition�
�FILE "src/alma/TMCDB/generated/"+TableName+".java"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * /////////////////////////////////////////////////////////////////
 * // WARNING!  DO NOT MODIFY THIS FILE!                          //
 * //  ---------------------------------------------------------  //
 * // | This is generated code!  Do not modify this file.       | //
 * // | Any changes will be lost when the file is re-generated. | //
 * //  ---------------------------------------------------------  //
 * /////////////////////////////////////////////////////////////////
 *
 * File �TableName�.java
 */
package alma.TMCDB.generated;
�IF HasTime�
import alma.hla.runtime.asdm.types.ArrayTime;
�ENDIF�
�IF HasLength�
import alma.hla.runtime.asdm.types.Length;
�ENDIF�
import alma.TMCDB_IDL.�TableName�IDL;

/**
�FOREACH (Set[Comment])Comment AS x�
 * �x.Comment�
�ENDFOREACH�
�IF isGenerated�
	�IF GeneratedKey.isNoKeyDependency�
   * Key: �GeneratedKey.KeyName� is automatically generated.
	�ELSE�
   * Key: �GeneratedKey.KeyName� is automatically generated from:  �GeneratedKey.FromList�
	�ENDIF�
�ELSE�
   * Key: �Key.KeyName�
�ENDIF�
 *
 */
public class �TableName� implements alma.TMCDB.TableBase, java.io.Serializable {
    static private final String newline = System.getProperty("line.separator");
�FOREACH (List[ColumnDefinition])Column AS c�
    private �c.JavaType� �c.ColumnName�;
    �IF c.CanBeNULL�
    // private boolean null�c.ColumnName�;
    �ENDIF�
�ENDFOREACH�
    /**
     * Default Constructor for �TableName�.  Setter methods must be used to insert data.
     */
    public �TableName� () {
�FOREACH (List[ColumnDefinition])Column AS c�
    �IF c.CanBeNULL�
        // null�c.ColumnName� = true;
    �ENDIF�
�ENDFOREACH�
    }
    
    /**
     * Create a �TableName� by specifiying all data values.
     */
    public �TableName� (
�FOREACH (List[ColumnDefinition])Column AS c�
        �c.JavaType� �c.ColumnName��IF !c.isLast�,�ENDIF�
�ENDFOREACH�
    ) {
�FOREACH (List[ColumnDefinition])Column AS c�
		set�c.ColumnName�(�c.ColumnName�);
�ENDFOREACH�
    }

    /**
     * Create a �TableName� by specifiying data values as an array of strings.
     */
    public �TableName� (String[] data) {
    	if (data.length != �NumberColumns�)
    		throw new IllegalArgumentException("Wrong number of items in the data array!  (" + data.length + " are specified; should be �NumberColumns�)");
    	int i = 0;
�FOREACH (List[ColumnDefinition])Column AS c�
	�IF c.JavaTypeNullable == "Integer" || c.JavaTypeNullable == "Long" || c.JavaTypeNullable == "Float" || c.JavaTypeNullable == "Double"�
	  try {
	�ENDIF�
		if (data[i] == null || data[i].length() == 0) {
	�IF c.CanBeNULL�
			// null�c.ColumnName� = true;
			// this.�c.ColumnName� = null;
	�ELSE�
			throw new IllegalArgumentException("Invalid data format: Data item number " + i + " cannot be null.");
	�ENDIF�
		} else {
	�IF c.CanBeNULL�
			// null�c.ColumnName� = false;
	�ENDIF�
	�IF c.isABlob�
			this.�c.ColumnName� = new byte[data[i].length()];
			byte[] b = data[i].getBytes();
			System.arraycopy(b, 0, this.�c.ColumnName�, 0, data[i].length());
	�ELSEIF c.isATime�
			this.�c.ColumnName� = new ArrayTime(data[i]);
	�ELSEIF c.isALength�
			this.�c.ColumnName� = new Length(data[i]);
	�ELSEIF c.isABoolean�
			this.�c.ColumnName� = new Boolean((data[i].equals("true") || data[i].equals("TRUE")) ? true : false);
	�ELSEIF c.JavaTypeNullable == "String"�
			this.�c.ColumnName� = data[i];
	�ELSEIF c.JavaTypeNullable == "Integer"�
			this.�c.ColumnName� = new Integer(Integer.parseInt(data[i]));
	�ELSE�
			this.�c.ColumnName� = new �c.JavaTypeNullable�(�c.JavaTypeNullable�.parse�c.JavaTypeNullable�(data[i]));
	�ENDIF�		
		}
	�IF c.JavaTypeNullable == "Integer" || c.JavaTypeNullable == "Long" || c.JavaTypeNullable == "Float" || c.JavaTypeNullable == "Double"�
	  } catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid number format: (" + data[i] + ").");
	  }
	�ENDIF�
		++i;
�ENDFOREACH�
    }
    
    /**
     * Display the values of this object.
     */
    public String toString() {
    	String s =  "�TableName�:" + newline;
�FOREACH (List[ColumnDefinition])Column AS c�
    �IF c.CanBeNULL�
    	// if (�c.ColumnName� == null)
    	//	s += "\t�c.ColumnName�: null" + newline;
    	// else
	�IF c.isATime�
        s += "\t�c.ColumnName�: " + �c.ColumnName�.toFITS() + newline;
	�ELSEIF c.isABlob�
			s += "\t�c.ColumnName�: " + new String(�c.ColumnName�) + newline;
	�ELSE�
        s += "\t�c.ColumnName�: " + �c.ColumnName� + newline;
	�ENDIF�
    �ELSE�
	�IF c.isATime�
        s += "\t�c.ColumnName�: " + �c.ColumnName�.toFITS() + newline;
	�ELSEIF c.isABlob�
		s += "\t�c.ColumnName�: " + new String(�c.ColumnName�) + newline;
	�ELSE�
        s += "\t�c.ColumnName�: " + �c.ColumnName� + newline;
	�ENDIF�
    �ENDIF�
�ENDFOREACH�    	
    	return s;
    }
    
    /**
     * Create a string in the "unload" format.
     */
    public String toString(String delimiter) {
    	String s =  "�TableName�" + delimiter;  
�FOREACH (List[ColumnDefinition])Column AS c�
    �IF c.CanBeNULL�
        // if (null�c.ColumnName�)
        // 	s += delimiter;
        // else
		�IF c.isABlob�
		// 	s += new String(�c.ColumnName�) + delimiter;
		�ELSEIF c.isATime�
		// s += new String(�c.ColumnName�.toFITS()) + delimiter;
		�ELSE�
        	// s += �c.ColumnName� + delimiter;
    	�ENDIF�
    �ELSE�
		�IF c.isABlob�
		s += new String(�c.ColumnName�) + delimiter;
		�ELSEIF c.isATime�
		s += new String(�c.ColumnName�.toFITS()) + delimiter;
		�ELSE�
        s += �c.ColumnName� + delimiter;
    	�ENDIF�
    �ENDIF�
�ENDFOREACH�
    	return s;
    }
    
    /**
     * Return the number of columns in the table.
     */
    public static int getNumberColumns() {
    	return �NumberColumns�;
    }
    
    /**
     * Create a string with the column names in the "unload" format.
     */
    public static String getColumnNames(String delimiter) {
    	String s =  "#�TableName�" + delimiter  
�FOREACH (List[ColumnDefinition])Column AS c�
        	+ "�c.ColumnName�" + delimiter
�ENDFOREACH�
			;
    	return s;
    }

    /**
     * Create a string with the column names in the "unload" format.
     */
    public String getTheColumnNames(String delimiter) {
    	return getColumnNames(delimiter);
    }
       
    /**
     * Compare this oblect with another object of the same type.
     */
    public boolean equals(Object obj) {
    	if (obj == null) return false;
    	if (!(obj instanceof �TableName�)) return false;
    	�TableName� arg = (�TableName�) obj;
�FOREACH (List[ColumnDefinition])Column AS c�
	�IF c.isABlob�
		for (int i = 0; i < �c.ColumnName�.length; ++i)
			if (�c.ColumnName�[i] != arg.�c.ColumnName�[i])
				return false;
	�ELSEIF c.isCharType�
		if (this.�c.ColumnName� == null) {	// Two null strings are equal
			if (arg.�c.ColumnName� == null)
				return true;
			else
				return false;
		}
		if (!this.�c.ColumnName�.equals(arg.�c.ColumnName�))
			return false; 
	�ELSEIF c.isATime || c.isALength�
		if (this.�c.ColumnName�.get() != arg.�c.ColumnName�.get())
			return false;
	�ELSE�
		if (this.�c.ColumnName� != arg.�c.ColumnName�)
			return false;
	�ENDIF�
�ENDFOREACH�
		return true;   
    }
    
    /**
     * Convert this object to its IDL format.
     */
    public �TableName�IDL toIDL() {
    	�TableName�IDL x = new �TableName�IDL ();
�FOREACH (List[ColumnDefinition])Column AS c�
	�IF c.isATime || c.isALength || c.isABlob�
		�IF c.isATime�
		x.�c.ColumnName� = this.�c.ColumnName�.toIDLArrayTime();
		�ENDIF�
		�IF c.isALength�
		x.�c.ColumnName� = this.�c.ColumnName�.toIDLLength();
		�ENDIF�
		�IF c.isABlob�
		x.�c.ColumnName� = new byte[this.�c.ColumnName�.length];
		System.arraycopy(this.�c.ColumnName�, 0, x.�c.ColumnName�, 0, this.�c.ColumnName�.length);
		�ENDIF�
	�ELSE�
		x.�c.ColumnName� = this.�c.ColumnName�;
	�ENDIF�
	�IF c.CanBeNULL�
        // x.null�c.ColumnName� = this.null�c.ColumnName�;
    �ENDIF�
�ENDFOREACH�    	
    	return x;
    }
    
    /**
     *  Populate this object from an IDL format.
     */
    public void fromIDL(�TableName�IDL x) {
�FOREACH (List[ColumnDefinition])Column AS c�
	�IF c.isATime || c.isALength || c.isABlob�
		�IF c.isATime�
		this.�c.ColumnName� = new ArrayTime(x.�c.ColumnName�);
		�ENDIF�
		�IF c.isALength�
		this.�c.ColumnName� = new Length(x.�c.ColumnName�);
		�ENDIF�
		�IF c.isABlob�
		this.�c.ColumnName� = new byte[x.�c.ColumnName�.length];
		System.arraycopy(x.�c.ColumnName�, 0, this.�c.ColumnName�, 0, x.�c.ColumnName�.length);
		�ENDIF�
	�ELSE�
		this.�c.ColumnName� = x.�c.ColumnName�;
	�ENDIF�
	�IF c.CanBeNULL�
        // this.null�c.ColumnName� = x.null�c.ColumnName�;
    �ENDIF�
�ENDFOREACH�    	
    }
    
    /*
     * If this is a database entry has a generated key, return the value
     * of its generated id; otherwise, return 0.
     */
    public int getId() {
�IF isGenerated�
		return �GeneratedKey.KeyName�;
�ELSE�
    	return 0;
�ENDIF�
    }
    
    /////////////////////////////////////////////////////////////
    // Getter and Setter Methods for �TableName�.
    /////////////////////////////////////////////////////////////
    
�FOREACH (List[ColumnDefinition])Column AS c�
    /**
     * Get the value for �c.ColumnName�.
     */
    public �c.JavaType� get�c.ColumnName� () {
        return �c.ColumnName�;
    }
    
    /**
     * Set �c.ColumnName� to the specified value.
     */
    public void set�c.ColumnName�(�IF c.isATime�ArrayTime �c.ColumnName��ELSE��c.JavaType� �c.ColumnName��ENDIF�) {
    �IF c.CanBeNULL�
        // null�c.ColumnName� = false;
    �ENDIF�
	�IF c.isABlob�
		this.�c.ColumnName� = new byte[�c.ColumnName�.length];
		System.arraycopy(�c.ColumnName�, 0, this.�c.ColumnName�, 0, �c.ColumnName�.length);
	�ELSE�
        �IF c.isATime�this.�c.ColumnName� = �c.ColumnName��ELSE�this.�c.ColumnName� = �c.ColumnName��ENDIF�;
	�ENDIF�
    }
    �IF c.CanBeNULL�
    /*
     * Is the �c.ColumnName� null?
     */
    // public boolean is�c.ColumnName�Null() {
    //	return null�c.ColumnName�;
    // }
     
    /*
     * Set the null indicator for �c.ColumnName�
     */       
    // public void set�c.ColumnName�Null() {
    // 	null�c.ColumnName� = true;
    // }
    �ENDIF�
�ENDFOREACH�
}
�ENDFILE�
�ENDDEFINE�
