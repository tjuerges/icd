#!/bin/python
import logging
from Acspy.Clients.SimpleClient import PySimpleClient
class loadComponent():
    def startComponent(self,componentName):
        self.client = PySimpleClient.getInstance()
        logging.fatal("Nombre del componente que estoy llamando :  %s\n\n\n" % (componentName))
        component = self.client.getComponent(str(componentName))
        return component
    def shutdownComponent(self,componentName):
        self.client.releaseComponent(str(componentName))
        self.client.disconnect()
        del(self.client)

