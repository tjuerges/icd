#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Other imports
from time import sleep
class acsEnv( ):
	def tmcdbEnable(self,isEnable):
		if(isEnable):
			os.putenv("ENABLE_TMCDB","1")
			os.putenv("TMCDB_CONFIGURATION_NAME","Test")
			os.putenv("LOAD_FROM_XML","1")
		else:
			os.putenv("ENABLE_TMCDB","0")
			os.putenv("TMCDB_CONFIGURATION_NAME"," ")
			os.putenv("LOAD_FROM_XML","0")
		
	def acsStart(self,enableTMCDB):
		self.tmcdbEnable(enableTMCDB)
		os.system("acsStart -noloadifr")
	def acsStartContainer(self,container,langImpl):
		os.system("acsStartContainer  -"+langImpl+"  "+container+' &');
		sleep(15)
	def acsStopContainer(self,container):
		os.system("acsStopContainer  "+container);
	def acsLoadIfr(self,idl):
		os.system("acsstartupLoadIFR  "+idl)
	def acsStop(self):
		os.system("acsStop ")
	def acsKill(self):
		os.system("killACS ")
# Automated test class
# MAIN Program
if __name__ == '__main__':
	acs = acsEnv()
	if str(sys.argv[1]) ==  "Start":
		if str(sys.argv[2]) == "TMCDB_ENABLE":
			acs.acsStart(True)
		else:
			acs.acsStart(False)
	if str(sys.argv[1]) == "Stop":
		acs.acsStop()
		acs.acsKill()	
