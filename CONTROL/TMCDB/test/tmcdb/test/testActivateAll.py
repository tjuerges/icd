#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os
import logging
import omniORB
# Other imports
from time import sleep
from  acsEnv import acsEnv
from ActivateAllComponent import *

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):
        self.listComponentForNotRead = []
        self.isTMCDBEnable = False 
    def tearDown(self):
        acs = acsEnv()
        acs.acsStop()

    def notTakeInAcount(self,componentName):
        self.listComponentForNotRead.append('PBEND_B_01')
        self.listComponentForNotRead.append('PBUMP_B_01')
        self.listComponentForNotRead.append('PBUMP_B_02')
        self.listComponentForNotRead.append('TEST_PS_1')
        self.listComponentForNotRead.append('TEST_PS_2')
        self.listComponentForNotRead.append('TEST_PS_3')
        self.listComponentForNotRead.append('TEST_PS_4')
        self.listComponentForNotRead.append('TEST_PS_5')
        self.listComponentForNotRead.append('TEST_PS_6')
        self.listComponentForNotRead.append('TEST_PS_7')
        self.listComponentForNotRead.append('TEST_PS_8')
        self.listComponentForNotRead.append('TEST_PS_9')
        self.listComponentForNotRead.append('TEST_PS_10')
        self.listComponentForNotRead.append('TEST_PS_11')
        self.listComponentForNotRead.append('TEST_PS_12')
        self.listComponentForNotRead.append('TEST_PS_13')
        self.listComponentForNotRead.append('TEST_PS_14')
        self.listComponentForNotRead.append('TEST_PS_15')
        self.listComponentForNotRead.append('TEST_PS_16')
        self.listComponentForNotRead.append('TEST_PS_17')
        self.listComponentForNotRead.append('TEST_PS_18')
#      self.listComponentForNotRead.append('MOUNT1')
        self.listComponentForNotRead.append('MOUNT2')
        self.listComponentForNotRead.append('MOUNT3')
        self.listComponentForNotRead.append('MOUNT4')
        self.listComponentForNotRead.append('MOUNT5')
#       MOUNT6 doesn't work !!    
        self.listComponentForNotRead.append('MOUNT6')
        self.listComponentForNotRead.append('LAMP1')
        self.listComponentForNotRead.append('HELLODEMO1')
        self.listComponentForNotRead.append('HELLOLAMP1')
        self.listComponentForNotRead.append('XMLCOMP1')
        self.listComponentForNotRead.append('LAMPACCESS1')
        self.listComponentForNotRead.append('LAMPCALLBACK1')
        for i in self.listComponentForNotRead:
            if(i==componentName):
                return True
        return False

    def startAllComponent(self):
        acs = acsEnv()
        client = None
        if (self.isTMCDBEnable):
            import Acspy.Clients.SimpleClient
            reload(Acspy.Clients.SimpleClient)
            client=Acspy.Clients.SimpleClient.PySimpleClient()  
        else:
            from Acspy.Clients.SimpleClient import PySimpleClient
            client = PySimpleClient()
        for compList in getAll():
            if (compList[0]=='' or compList[1]=='' or compList[2]=='' or compList[3]==''):
                self.listComponentForNotRead.append(str(compList[0]))	
            if (self.notTakeInAcount(compList[0])):
                logging.fatal("Nothing to do for  %s" % (compList[0]))	
            else:
                acs.acsLoadIfr(compList[2])
                acs.acsStartContainer(compList[1],compList[3])
                logging.fatal("Getting component: %s" % (str(compList[0])))
                comp=None
                componentState = None
                try:
                    comp = client.getComponent(str(compList[0]))
                    logging.fatal("GET STATE:")
                    logging.fatal(comp._get_componentState())
                    componentState = comp._get_componentState()  
                except omniORB.CORBA.COMM_FAILURE:
                    from Acspy.Clients.SimpleClient import PySimpleClient
                    client = PySimpleClient()
                    comp = client.getComponent(str(compList[0]))
                    logging.fatal("GET STATE:")
                    logging.fatal(comp._get_componentState())
                    componentState = comp._get_componentState() 
                del(comp)
                acs.acsStopContainer(compList[1])
        assert("COMPSTATE_OPERATIONAL",str(componentState)) 
        del(client) 
    def test01(self):
        acs = acsEnv()
        acs.acsStart(False)
        self.startAllComponent()
        acs.acsStop()

    def test02(self):
        acs = acsEnv()
        acs.acsStart(True)
        self.isTMCDBEnable = True
        self.startAllComponent()
        acs.acsStop()

# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
    unittest.main()
