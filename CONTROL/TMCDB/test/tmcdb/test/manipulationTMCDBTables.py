#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
This file has two test to manipulate data from MOUNT1 component.

test01: 
It was retrieved xml implLang information from TMCDB.  For this purpose
PySimpleClient.getCDBElement was used.  Then  implLang was updated through sql
querys. After that, was compared the implLang value retrieved, using
PySimpleClient.getCDBElement method.  Finally restore the original data.

test02: 
ImplLang's value was obtained through sql querys. The next step was retrieve
the same information through PySimpleClient.getCDBElement method. Finally it
was compared both results.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os
import logging
# Other imports
from time import sleep

# Automated test class
class automated( unittest.TestCase ):
    
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def updateTableFromOutSide(self,codeTmp):
        os.system("echo \"UPDATE COMPONENT set IMPLLANG='"+codeTmp+"' where COMPONENTNAME='MOUNT1'; \" > tmp/updateSQL.sql")
        os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa tmp/updateSQL.sql &> /dev/null")
        os.system("rm tmp/updateSQL.sql")
    def selectFromOutSide(self):
        os.system("echo \"\\o tmp/tmpResult.txt\" > tmp/selectSQL.sql")
        os.system("echo \"select IMPLLANG from Component where COMPONENTNAME='MOUNT1'; \" >> tmp/selectSQL.sql")
        os.system("echo \"\\o tmp/tmpResult.txt\" >> tmp/selectSQL.sql")
        os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa tmp/selectSQL.sql &> /dev/null")
        os.system("rm tmp/selectSQL.sql")
        file = open ('tmp/tmpResult.txt','r')        
        result = file.readlines()
        file.close()
        resultStr = result[2]
        impllang = resultStr.rstrip('\n')
        os.system("rm tmp/tmpResult.txt")
        return impllang
        
    def getDataFromTMCDB(self):
        from Acspy.Clients.SimpleClient import PySimpleClient
        client = PySimpleClient.getInstance()
        xml=client.getCDBElement('MACI/Components/MOUNT1','MOUNT1')
        implLang = str(xml[0]['ImplLang'])
        # To accomplished the restriccion in the tables, only cpp,py,java
        # statement
        if implLang == 'cpp' :
            implLangToInsert='java'
        elif implLang == 'java':
            implLangToInsert = 'py'
        elif implLang == 'py':
            implLangToInsert = 'cpp'
        self.updateTableFromOutSide(implLangToInsert)
        newImplLangRetrive = str(xml[0]['ImplLang'])
        self.updateTableFromOutSide(implLang)
        del(client)
        assert(implLangToInsert,newImplLangRetrive)

    def test01(self):
        self.getDataFromTMCDB()

    def test02(self):
        implLangRetrive = self.selectFromOutSide()
        from Acspy.Clients.SimpleClient import PySimpleClient
        client = PySimpleClient.getInstance()
        xml=client.getCDBElement('MACI/Components/MOUNT1','MOUNT1')
        implLang = str(xml[0]['ImplLang'])
        del(client)
        assert(implLang,implLangRetrive)
# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
    unittest.main()

