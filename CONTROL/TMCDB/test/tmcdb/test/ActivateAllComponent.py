#!/bin/python
import os
from xml.dom import minidom

def getFileNames(path):
	output = os.popen3("find "+path+" -name .xml -prune -o -type f -print |grep xml")
	list = []
	for i in output[1]:
		list.append(i.rstrip('\n'))
	return list

def parserXML(path):
	 return minidom.parse(path)

def lookForContainersAndNames():
	componentClass = []
	componentFiles = getFileNames('CDB/MACI/Components')
	for path in componentFiles:
		node = parserXML(path)
		#TODO search why in CDBdemo the tag is Components, and tag Name is _ instead Component
		# nodeComponentList = node.getElementsByTagName('Component')
		nodeComponentList = node.getElementsByTagName('_')
		i=0
		for n in nodeComponentList:
			nodeMap = nodeComponentList.item(i)
			cont = []
			cont.append(nodeMap.getAttribute('Name'))
			cont.append(nodeMap.getAttribute('Container'))
			idl=nodeMap.getAttribute('Type')
			start = idl.rfind("/")
			ends = idl.rfind(":")
			c = 0
			newIdl=""
			for letter in idl:
				if(c>start and c<ends):
					newIdl+=letter
				c+=1
			newIdl=newIdl + ".idl"
			cont.append(newIdl)
			cont.append('')
			componentClass.append(cont)
			i+=1
	#print componentClass.__len__()
	return componentClass

def lookForContainersAndImpl():
	containerClass = []
	containerFiles = getFileNames('CDB/MACI/Containers')
	for path in containerFiles:
		node = parserXML(path)
		nodeContainerList = node.getElementsByTagName('Container')
		i=0
		for n in nodeContainerList:
			nodeMap = nodeContainerList.item(i)
			cont = []
			cont.append(nodeMap.getAttribute('ImplLang'))
			cont.append(path)
			containerClass.append(cont)
			i+=1	
	return containerClass

def addImplLangToContainer():
	componentClass = lookForContainersAndNames()
	containerClass = lookForContainersAndImpl()
	for componentObj in componentClass:
		name = componentObj[0]
		container = componentObj[1]
		for cont in containerClass:
			if cont[1].rfind(container) > -1 :
				componentObj[3] = cont[0]
				break
	return componentClass

def getAll():
	return addImplLangToContainer()
for i in getAll():
	print "Component name: %s" % i[0]
    #for add to prohibited list:
	#print "listComponentForNotRead.append('%s')" % i[0]
