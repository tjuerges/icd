#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
'''

# Import unit test
import unittest

import sys
import os
import logging
import cdbErrType

# Other imports
from time import sleep
from Acspy.Util import ACSCorba
# Automated test class
class automated( unittest.TestCase ):
	def setUp(self):
		pass
	def tearDown(self):
		pass
	
	def getCorrComponents(self):
		componentList = []
		dal = ACSCorba.cdb()
		while(1):
			if(componentList.__len__()==0):
				componentNode = "alma/CORR_MASTER_COMP/component"
			else:
				componentNode = "alma/CORR_MASTER_COMP/component" + str(componentList.__len__())
			try:
				dao = dal.get_DAO_Servant(componentNode)
				componentList.append(dao.get_string("nameOrType"))
				print "Component "+dao.get_string("nameOrType")+" found in CDB"
			except cdbErrType.CDBRecordDoesNotExistEx:
				if (componentList.__len__() == 0) :
					assert componentList.__len__()>0,"no found components in the CDB"
				else:
					break
			
	def test01(self):
		self.getCorrComponents()
	
# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
    unittest.main()

