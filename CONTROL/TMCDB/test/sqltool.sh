if test -a $ACSROOT/lib/hsqldb.jar; then
    HSQLDB_JAR=$ACSROOT/lib/hsqldb.jar
elif test -a $INTROOT/lib/hsqldb.jar; then
    HSQLDB_JAR=$INTROOT/lib/hsqldb.jar
fi

java -jar $HSQLDB_JAR --rcFile ./sqltool.rc localhost-sa

