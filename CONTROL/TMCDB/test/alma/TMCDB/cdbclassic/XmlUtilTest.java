package alma.TMCDB.cdbclassic;

import junit.framework.TestCase;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlUtilTest extends TestCase
{
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testAddChildElementWithPath() throws Exception {
		String ns = "myNamespace";
		Document doc = XmlUtil.getDocumentBuilder().newDocument();
		Element parentElem = doc.createElementNS(ns, "myRootElem");
		doc.appendChild(parentElem);
		XmlUtil.addChildElementWithPath(parentElem, "FirstChild");
		Element sndChld = XmlUtil.addChildElementWithPath(parentElem, "/SecondChild/"); // slash should be ignored
		XmlUtil.addChildElementWithPath(parentElem, "FirstChild/FirstGrandchild"); // FirstChild should be reused
		Element sndGrChld = XmlUtil.addChildElementWithPath(parentElem, "ThirdChild/SecondGrandchild"); // both elements should be created
		
		System.out.println(XmlUtil.printDOM(parentElem, true));
		
		assertEquals(1, doc.getElementsByTagNameNS(ns, "SecondChild").getLength());
		assertSame(sndChld, doc.getElementsByTagNameNS(ns, "SecondChild").item(0));
		assertEquals("FirstChild", doc.getElementsByTagNameNS(ns, "FirstGrandchild").item(0).getParentNode().getNodeName());
		assertEquals("SecondGrandchild", sndGrChld.getNodeName());
	}
}
