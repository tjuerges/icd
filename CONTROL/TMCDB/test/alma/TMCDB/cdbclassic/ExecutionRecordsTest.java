package alma.TMCDB.cdbclassic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;

import com.cosylab.CDB.DAL;

import si.ijs.maci.AdministratorOperations;
import alma.TMCDB.TMCDB;
import alma.TMCDB.generated.ComponentType;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.AdvancedContainerServices;
import alma.acs.container.AdvancedContainerServicesImpl;
import alma.acs.container.ContainerServices;
import alma.demo.HelloDemo;

/**
 * 
 * Copied this file from Joe, and adapted it to test the ExecutionRecordArchiver
 * 
 * @author jschwarz
 * To run this test:
 * 1) ACS must be up and running
 * 2) Tables must be prepared in Oracle or HsqlDB and filled with values for components and containers.
 * 3) the frodoContainer (a Java container) must be up and running
 */
public class ExecutionRecordsTest extends ComponentClientTestCase {
	
	private String testName;
	private Connection c;
	private String compName = "HELLODEMO1";
	//private String compName = "MOUNT1";
	private TMCDB tmcdb;
	private ContainerServices cs;
	private AdvancedContainerServices advCS;
	private ExecutionRecordArchiver execRecArchiver;
	
	private final int NUM_ITERATIONS = 25; // Change this to 2500 to make things more interesting!

	public static void main(String[] args) throws Exception {
		ExecutionRecordsTest test = new ExecutionRecordsTest("bla");
		test.setUp();
		test.testInsertOneRecord();
		test.tearDown();
	}
	
	public ExecutionRecordsTest(String testName) throws Exception {
		super(testName);
		System.out.println("AAA CTOR");
		this.testName = testName;
//		tmcdb = new TMCDB();
//		c = tmcdb.connectHsqldb("sa", "", "jdbc:hsqldb:hsql://localhost");
//		c = tmcdb.connectHsqldb("sa", "", "jdbc:hsqldb:mem:tmcdb");
//		tmcdb.initialize();
//		Class.forName("org.hsqldb.jdbcDriver");
//		c = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost", "sa", "");
	}

	private HelloDemo myNewComp; // Java client from jcontexmpl
	
	protected void setUp() throws Exception {
		super.setUp();
		// TODO: setup an hsqldb database with tables defined
		// TODO: load it with whatever the manager needs to get started
		cs = getContainerServices();
		advCS = (AdvancedContainerServices) cs.getAdvancedContainerServices();
		execRecArchiver = new ExecutionRecordArchiver(testName, m_logger, cs);
		execRecArchiver.setConfigId(1);
		advCS.connectManagerAdmin(execRecArchiver, false);
		System.out.println("AAA SETUP");
	}
	
	public void testInsertOneRecord() throws Exception {
		for (int i = 0; i < NUM_ITERATIONS; i++) {
			System.out.println("AAA START INSERT"+i);
			//myNewComp = alma.alarmsystemdemo.MountHelper.narrow(cs
			//		.getComponent(compName));	
			myNewComp = alma.demo.HelloDemoHelper.narrow(cs
			.getComponent(compName));
			
			//		Thread.sleep(250);
			// TODO: Check that the startup time has been filled in
			//		Statement s = c.createStatement();
			//		ResultSet r = s.executeQuery("SELECT ConfigurationId, ConfigurationName FROM Configuration");
			//		r.next();
			//		m_logger.log(Level.INFO,"Configuration name is: "+r.getString("ConfigurationName"));
			//		PreparedStatement p = c.prepareStatement("SELECT ComponentId, ComponentInterfaceId FROM Component WHERE ComponentName = '"+CompName+"'");
			//		r = p.executeQuery();
			//		r.next();
			//		int ctypeId = r.getInt("ComponentInterfaceId");
			//		int cid = r.getInt("ComponentId");
			//		m_logger.log(Level.INFO,"Component interface ID is: "+ctypeId);
			//		ComponentType ctype = tmcdb.getComponentType(ctypeId);
			//		m_logger.log(Level.INFO,ctype.toString());
			//		p = c.prepareStatement("SELECT COUNT (*) FROM ComponentExecution WHERE ComponentId = "+cid);
			//		r = p.executeQuery();
			//		r.next();
			//		m_logger.log(Level.INFO,"There are "+ r.getInt(1)+" rows in the ComponentExecution table with Component ID = "+cid);
			cs.releaseComponent(compName);
			// TODO: Check that the termination time has been filled in
		}
	}

	protected void tearDown() throws Exception {
		advCS.disconnectManagerAdmin(execRecArchiver);
		System.out.println("AAA TEARDOWN");
		super.tearDown();
	}

}
