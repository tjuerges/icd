package alma.TMCDB.cdbclassic;

import java.util.logging.Logger;

import junit.framework.TestCase;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.cosylab.CDB.JDAL;

import alma.acs.logging.ClientLogManager;
import alma.maci.containerconfig.Container;

/**
 * Test for {@link MaciContainersReader}, {@link MaciComponentsReader} etc. 
 * Make sure the System property <code>cdbIOR</code> is set 
 * to the IOR of a running CDB. No other runtime ACS is needed.
 *  
 * @author hsommer
 */
public class MaciReaderTest extends TestCase
{
	private Logger logger;
	private JDAL jdal; 
	
	public MaciReaderTest() throws Exception {
		super("MaciReaderTest");
	}
	
	protected void setUp() throws Exception {
		logger = ClientLogManager.getAcsLogManager().getLoggerForApplication("MaciReaderTest", false);
		jdal = (new JDalAccess()).getJDAL();
	}

//	protected void tearDown() throws Exception {
//		super.tearDown();
//	}

	public void _testContainerGetAndTransformDom() throws Exception {
		MaciContainersReader maciContainersReader = new MaciContainersReader(logger);
		assertEquals("MACI/Containers", maciContainersReader.getCDBPath());
		Document dom = maciContainersReader.getAndTransformDom(jdal);
		
		// test DOM data
		Element rootElem = dom.getDocumentElement();
		assertEquals(2, rootElem.getChildNodes().getLength());
		Element frodoContainerConfig = (Element) rootElem.getFirstChild();
		assertEquals("frodoContainer", frodoContainerConfig.getNodeName());
		
		// test corresponding castor binding data
		Container castorContainer = maciContainersReader.getCastorContainerInfo(frodoContainerConfig);
		assertEquals(200.0, castorContainer.getTimeout());
		assertEquals(9999, castorContainer.getDeployInfo().getKeepAliveTime());
		assertEquals("baci", castorContainer.getAutoload().getArrayItem(0).get().getString());
	}
	
	
	public void testComponentGetAndTransformDom() throws Exception {
		MaciComponentsReader maciComponentsReader = new MaciComponentsReader(logger);
		assertEquals("MACI/Components", maciComponentsReader.getCDBPath());
		
		Document dom = maciComponentsReader.getAndTransformDom(jdal);
		
		System.out.println(XmlUtil.printDOM(dom, true));
	}			
}
