package alma.tmcdb.test;

public class ContainerSpec {
	private String name;
	private String type;
	
	public ContainerSpec(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public String getStartupCommand() {
		return "acsStartContainer -" + type + " " + name; 
	}
	
	public String getShutdownCommand() {
		return "acsStopContainer " + name; 
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
}