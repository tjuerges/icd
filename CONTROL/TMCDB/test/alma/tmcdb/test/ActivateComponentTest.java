/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.tmcdb.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;

/**
 * @author nsaez
 *
 */
public class ActivateComponentTest extends TestCase {
	
    private static final String TMP_DIR = "./tmp";

    static ComponentClient compClient = null;

    private ACSTestEnvironment acsenv = null;

    private ContainerServices container = null;

    private Logger logger;

    FileOutputStream acsLog = null;

    int depthLevel = 0;
    
	Set<String> containerList = new HashSet<String>();
	
	Set<String> componentNameList = new HashSet<String>();
    
	Map<String,String> containerMap = new HashMap<String, String>();
    /**
     * @return suite
     * @throws Exception
     */
    public static Test suite() throws Exception {
        // String s = System.getProperty("suite");
        TestSuite suite = new TestSuite();
        suite.addTest(new ActivateComponentTest("testMissingAttr"));
        return suite;
    }    

    /**
     * @param test
     */
    public ActivateComponentTest(String test) {
        super(test);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        logger = Logger.getLogger("ActivateComponents");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * @throws Exception
     */
    public void testMissingAttr() throws Exception { 
    	/** 
    	 * For each xml file , looking for the component and his container
    	 * 
    	 */
    	String path1 ="./CDB/MACI/Components";
    	String path[]= path1.split(""+File.pathSeparatorChar);
    	Vector<String> v = getFilenames(path,"xml");
    	for(int m=0;m<v.size();m++){
    		String path2= (String) v.get(m);
    		String pathRefTmp = readFileAsString(path2);
    		Document doc = parserXML(pathRefTmp);
    		lookForContainersAndNames(doc,path2);
    	}	
    	/** 
    	 * For each container looking for the container and his implementation
    	 * language
    	 */
    	
		 path1 ="./CDB/MACI/Containers";
		 String pathTmp[]= path1.split(""+File.pathSeparatorChar);
		 v = getFilenames(pathTmp,"xml");
		 String pathRef =null;
		for(int m=0;m<v.size();m++){
			pathRef= (String) v.get(m);
			NodeList nodeList = getNodeList("/*", pathRef);
			for(int i=0;i<nodeList.getLength();i++){
				Element element = (Element) nodeList.item(i);
				addImplLangToContainer(pathRef, element.getAttribute("ImplLang"));
			}
		}
		/**
		 * Starting ACS with the containers found in each component xml file.
		 * 
		 */
		startACS(false,containerMap);
		loadComponent();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		shutdownACS();
    }

    
    
    
    public void loadComponent() throws AcsJContainerServicesEx {
    	//private Test01 client_comp = alma.TEST_01.Test01Helper.narrow(getContainerServices().getComponent("Test01JAVA"));
	}

	/**
     * addImplLangToContainer  function Add to a MAP collection
     * the name of the container like key , and the implementation
     * language as value.
     * 
     * @param path
     * @param implLang
     * @return
     */
    
	private boolean addImplLangToContainer(String path, String implLang) {
		String containerName=null;
		for (Iterator<String> it = (Iterator<String>) containerList.iterator();it.hasNext();){
			containerName=it.next();
			//TODO a path.matches would be better
			if(path.contains(containerName)){
				containerMap.put(containerName, implLang);
				return true;
			}
			
		}
		return false;
	}
    
    /**
     * getFilenames function returns a Vector<String> with the 
     * name of the files found in the path given that matches 
     * with the type.
     * 
     * @param paths
     * @param type
     * @return Vector<String>
     */
    
	protected Vector<String> getFilenames(String paths[],String type){
		Vector<String> vector = new Vector<String>();
		String files[];
		/*
		 * Scans the list of paths.
		 */
		for(int i=0;i<paths.length;i++)
		{
			if(paths[i].length() != 0) 
			{
				File file = new File(paths[i]);
				if(file.exists())
				{
					if(file.isFile())
					{ 
						if (paths[i].endsWith("."+type))
							vector.add(paths[i]);
					} 
					else
					{ 
						if(!(paths[i].endsWith("/"))) 
							paths[i]=paths[i]+"/";

						files = (new File(paths[i])).list();
						//Add the files to the vector.
						if (files!=null)
							for (int j=0; j < files.length; j++){
								//filtering for 'type'
								if(files[j].endsWith(type)){		
									vector.addElement(paths[i]+files[j]);
								}
							}
						String[] dirs = (new File(paths[i])).list();
						if(dirs.length != 0)
							for (int j=0; j < dirs.length; j++){
								dirs[j]=paths[i]+dirs[j]+"/";
							}
						vector.addAll(getFilenames(dirs,type));	
					}
				}
			}
		}
		return vector;
	}
	
	/**
	 * searchElement function return an org.w3c.dom.Element
	 * given an XPATH expression and a file path to search.
	 * 
	 * It return only the first Element found.
	 * 
	 * @param expression
	 * @param pathFile
	 * @return
	 * @throws Exception
	 */
	
	private Element searchElement (String expression, String pathFile)  throws Exception{
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodeList = (NodeList) xpath.evaluate(expression, new InputSource(new FileReader(pathFile)),XPathConstants.NODESET);
		Element element = (Element) nodeList.item(0);	
		return element;
	}

	/**
	 * lookForContainersAndNames function that look recursively 
	 * for all component names and their containers.
	 * 
	 * @param node
	 * @param pathComponents
	 * @throws Exception
	 */
	private void lookForContainersAndNames (Node node,String pathComponents) throws Exception {
		int type = node.getNodeType();
		String expression="/*";
		
		if (type == Node.ELEMENT_NODE) {
			if(node.hasAttributes()) 
			{	//To find the same level of depth in the xml file to compare				
				for (int n=1;n<depthLevel;n++)
					expression+=expression;	
				String containerExpression = expression+"[@Container]";
				String nameExpression = expression+"[@Name]";
				NamedNodeMap AttributesList = node.getAttributes();
				for(int j = 0; j < AttributesList.getLength(); j++) {
					NodeList nodeList = getNodeList(expression,pathComponents);
					for (int k=1;k<=nodeList.getLength();k++){
						Element elementWithContainer = searchElement(containerExpression, pathComponents);
						Element elementWithName = searchElement(nameExpression, pathComponents);
						if (elementWithContainer!=null ){
							containerList.add(elementWithContainer.getAttribute("Container"));
						}
						if(elementWithName!=null)
							componentNameList.add(elementWithName.getAttribute("Name"));	
					}
				}
			}
		}

		NodeList children = node.getChildNodes();
		if (children != null) {
			for (int i=0; i< children.getLength();i++) {
				depthLevel++;
				lookForContainersAndNames(children.item(i),pathComponents);
				depthLevel--;
			}
		}
	}

	
	
	
	/**
	 * getNodeList function return an org.w3c.dom.NodeList
	 * given an XPATH expression and a file path to search.
	 * 
	 * @param expression
	 * @param path
	 * @return NodeList
	 * @throws XPathExpressionException
	 * @throws FileNotFoundException
	 */
	
	private NodeList getNodeList(String expression, String path) throws XPathExpressionException, FileNotFoundException {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodeList = (NodeList) xpath.evaluate(expression, new InputSource(new FileReader(path)),XPathConstants.NODESET);
		return nodeList;
	}

	/**
	 * parserXML function return a org.w3c.dom.Document 
	 * This document was generated by a String (xmlDataToParse)
	 * that contains XML data.
	 * 
	 * @param xmlDataToParse
	 * @return Document
	 * @throws SAXException
	 * @throws IOException
	 */
	private Document  parserXML(String xmlDataToParse) throws SAXException, IOException{
		DOMParser parser = new DOMParser();
		parser.parse(new InputSource(new StringReader(xmlDataToParse)));
		Document doc = parser.getDocument();
		return doc;
		}
	
	/**
	 * readFileAsString 
	 * Given a file path , this function convert 
	 * the data to a string
	 * 
	 * @param filePath
	 * @return String
	 * @throws java.io.IOException
	 */
	private String readFileAsString(String filePath)
	throws java.io.IOException{
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(
				new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead=0;
		while((numRead=reader.read(buf)) != -1){
			fileData.append(buf, 0, numRead);
		}
		reader.close();
		return fileData.toString();
	}

/**
 * 
 * @param enableTMCDB
 * @param containerMap
 * @throws Exception
 */
    public void startACS(boolean enableTMCDB,Map<String,String> containerMap) throws Exception {
        ContainerSpec[] cont = new ContainerSpec[containerList.size()];
        int i=0;
		for (Iterator<String> it = (Iterator<String>) containerList.iterator();it.hasNext();){
			String containerName=(String) it.next();
			cont[i] = new ContainerSpec(containerName,containerMap.get(containerName));
			 i++;
		}
        String[] env = null;
        if (enableTMCDB) {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=true";
            env[2] = "LOAD_FROM_XML=true";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        } else {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=false";
            env[2] = "LOAD_FROM_XML=false";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        }

        acsenv = new ACSTestEnvironment(cont, env);

        acsLog = new FileOutputStream("./tmp/ACS.log");
        acsenv.startACS(acsLog);
        acsenv.startContainers(System.out, System.err);
        acsenv.acsStatus();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("managerLoc = " + managerLoc);
        if (compClient == null)
            compClient = new ComponentClient(null, managerLoc, "ActivateComponentTest");
        container = compClient.getContainerServices();
        logger = container.getLogger();
    }
    
    /**
     * Shutdown ACS
     * @throws Exception
     */
    private void shutdownACS() throws Exception {
        acsenv.shutdownACS(acsLog);
    }
}
