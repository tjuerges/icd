/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.tmcdb.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import alma.cdbErrType.CDBRecordDoesNotExistEx;
import alma.cdbErrType.CDBXMLErrorEx;

import com.cosylab.CDB.DAL;

/**
 * @author nsaez
 *
 */
public class GetRecordsTest extends TestCase {
	
    private static final String TMP_DIR = "./tmp";

    private static final String CDB_DIR = "./tmp/xmlCDB";

    private static final String TMCDB_DIR = "./tmp/xmlTMCDB";

    private static boolean recordsCreated = false;

    static ComponentClient compClient = null;

    private ACSTestEnvironment acsenv = null;

    private ContainerServices container = null;

    private Logger logger;

    FileOutputStream acsLog = null;

    int depthLevel = 0;
    
    Set<String> missingAttributes = new HashSet<String>();
    
    Set<String> wrongAttributes = new HashSet<String>();
    
    /**
     * @return test
     * @throws Exception
     */
    public static Test suite() throws Exception {
        TestSuite suite = new TestSuite();
        suite.addTest(new GetRecordsTest("testGetRecords"));
        return suite;
    }    

    /**
     * @param test
     */
    public GetRecordsTest(String test) {
        super(test);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        logger = Logger.getLogger("GetRecordsTest");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    
    private String getFileName(String path){
    	return (new File(path)).getName();
    }
    
    
    /**
     * getFilenames function returns a Vector<String> with the 
     * name of the files found in the path given that matches 
     * with the type.
     * 
     * @param paths
     * @param type
     * @return Vector<String>
     */
	protected Vector<String> getFilenames(String paths[],String type){
		Vector<String> vector = new Vector<String>();
		String files[];
		/*
		 * Scans the list of paths.
		 */
		for(int i=0;i<paths.length;i++)
		{
			if(paths[i].length() != 0) 
			{
				File file = new File(paths[i]);
				if(file.exists())
				{
					if(file.isFile())
					{ 
						if (paths[i].endsWith("."+type))
							vector.add(paths[i]);
					} 
					else
					{ 
						if(!(paths[i].endsWith("/"))) 
							paths[i]=paths[i]+"/";

						files = (new File(paths[i])).list();
						//Add the files to the vector.
						if (files!=null)
							for (int j=0; j < files.length; j++){
								//filtering for 'type'
								if(files[j].endsWith(type)){		
									vector.addElement(paths[i]+files[j]);
								}
							}
						String[] dirs = (new File(paths[i])).list();
						if(dirs.length != 0)
							for (int j=0; j < dirs.length; j++){
								dirs[j]=paths[i]+dirs[j]+"/";
							}
						vector.addAll(getFilenames(dirs,type));	
					}
				}
			}
		}
		return vector;
	}
	  
	
	/**
	 * writeFile 
	 * given the string daoReference and the fileName
	 * this function try to get the xml data of a part
	 * of CDB (by that match with the string daoReference) 
	 * 
	 * Once this data it's obtains, try to write a file
	 * with the name contains into filename.
	 * 
	 * This will be useful to have files to compare between 
	 * TMCDB and CDB.
	 * 
	 * @param daoReference
	 * @param fileName
	 * @throws InterruptedException 
	 */
	
	private void writeFile(String daoReference,String fileName) throws InterruptedException{
		try{
			DAL dal = container.getCDB();
			String xmldoc = dal.get_DAO(daoReference);
			File tmp = new File(fileName);
			BufferedWriter out = new BufferedWriter(new FileWriter(tmp));
			out.write(xmldoc);
			out.close();
		}catch (AcsJContainerServicesEx e){
			e.printStackTrace();
			
		}catch (CDBXMLErrorEx e){
			e.printStackTrace();
		}catch (CDBRecordDoesNotExistEx e){
			e.printStackTrace();
			acsenv.shutdownACS(acsLog);
			String[] tmp =fileName.split("/");
			String[] cbdConfigChocen =tmp[2].split("xml");
			logger.severe("Couldn't get_DAO from: "+daoReference+ " with: "+cbdConfigChocen[1]+" configuration\n CDB Record Does Not Exist or Not Found !!");
			fail("Couldn't get_DAO from: "+daoReference+ " with: "+cbdConfigChocen[1]+" configuration\n CDB Record Does Not Exist or Not Found !!");
		}catch (IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * This function obtains records of TMCDB or CDB
	 * if they are created, just return. 
	 * @throws Exception
	 */
	public void testGetRecords() throws Exception {

		if (recordsCreated)
			return;

		recordsCreated = true;

		File tmpDir = new File(TMP_DIR);
		tmpDir.mkdir();
		File xmlCDBDir = new File(CDB_DIR);
		xmlCDBDir.mkdir();
		File xmlTMCDBDir = new File(TMCDB_DIR);
		xmlTMCDBDir.mkdir();
 
		String path1 ="./CDB/alma/";
		String path[]= path1.split(""+File.pathSeparatorChar);
		path1="./CDB";
		Vector<String> v = getFilenames(path,"xml");
		startACS(false);
		for(int m=0;m<v.size();m++){
			String pathFileName= (String) v.get(m);
			//CDB
			String daoReferenceName =  pathFileName.substring(path1.length(),pathFileName.lastIndexOf("/"));
			System.out.println("With CDB configuration getting XML = container.getCDB().get_DAO("+daoReferenceName+")");
			if(!isACSRunning()){
				fail("ACS is not running see details at ./tmp/ACS.log");
			}
			writeFile(daoReferenceName,CDB_DIR + "/"+getFileName(pathFileName));
		}
		shutdownACS();
		
		startACS(true);
		for(int m=0;m<v.size();m++){
			String pathFileName= (String) v.get(m);
			//TMCDB
			String daoReferenceName =  pathFileName.substring(path1.length(),pathFileName.lastIndexOf("/"));
			System.out.println("With TMCDB configuration getting XML = container.getCDB().get_DAO("+daoReferenceName+")");
			if(!isACSRunning()){
				fail("ACS is not running.It's seems that TMCDB couldn't charge. see details at ./tmp/ACS.log");
			}
			writeFile(daoReferenceName,TMCDB_DIR +"/"+getFileName(pathFileName));
		}
		shutdownACS();
	}

	/**
	 * Start ACS with the configuration given
	 * 
	 * @param enableTMCDB
	 * @throws Exception
	 */
    private void startACS(boolean enableTMCDB) throws Exception {

        ContainerSpec[] cont = new ContainerSpec[0];
        String[] env = null;
        if (enableTMCDB) {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=true";
            env[2] = "LOAD_FROM_XML=true";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        } else {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=false";
            env[2] = "LOAD_FROM_XML=false";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        }

        acsenv = new ACSTestEnvironment(cont, env);

        acsLog = new FileOutputStream("./tmp/ACS.log");
        acsenv.startACS(acsLog);

        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("managerLoc = " + managerLoc);
        if (compClient == null)
            compClient = new ComponentClient(null, managerLoc, "GetRecordsTest");
        container = compClient.getContainerServices();
        logger = container.getLogger();
    }

    /**
     * Shutdown ACS
     * @throws Exception
     */
    private void shutdownACS() throws Exception {
        acsenv.shutdownACS(acsLog);
    }
    
    private boolean isACSRunning(){
    	return  acsenv.isACSRunning(0);
    }
}
