/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.tmcdb.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;

import com.cosylab.CDB.DAL;

/**
 * @author nsaez
 *
 */
public class RoundTripTest extends TestCase {

    private static final String TMP_DIR = "./tmp";

    private static final String CDB_DIR = "./tmp/xmlCDB";

    private static final String TMCDB_DIR = "./tmp/xmlTMCDB";

    private static boolean recordsCreated = false;

    static ComponentClient compClient = null;

    private ACSTestEnvironment acsenv = null;

    private ContainerServices container = null;

    private Logger logger;

    FileOutputStream acsLog = null;

    /**
     * @return test
     * @throws Exception
     */
    public static Test suite() throws Exception {
        // String s = System.getProperty("suite");
        TestSuite suite = new TestSuite();
        suite.addTest(new RoundTripTest("testMissingAttrManagers"));
        suite.addTest(new RoundTripTest("testMissingAttrContainers"));
        suite.addTest(new RoundTripTest("testMissingAttrComponents"));
        return suite;
    }    

    /**
     * @param test
     */
    public RoundTripTest(String test) {
        super(test);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getRecords();
        logger = Logger.getLogger("RoundTripTest");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * @throws Exception
     */
    public void testMissingAttrComponents() throws Exception {
        // Components
        String pathRef = CDB_DIR + "/Components.xml";
        String pathToCompare = TMCDB_DIR + "/Components.xml";
        String expression = "/urn:schemas-cosylab-com:Components:1.0:Components/";
        lookForMissingAttributes(pathRef, pathToCompare, expression);
    }

    /**
     * @throws Exception
     */
    public void testMissingAttrManagers() throws Exception {
        // Managers
        String pathRef = CDB_DIR + "/Managers.xml";
        String pathToCompare = TMCDB_DIR + "/Managers.xml";
        String expression = "/Managers/";
        lookForMissingAttributes(pathRef, pathToCompare, expression);
    }

    /**
     * @throws Exception
     */
    public void testMissingAttrContainers() throws Exception {
        // Containers
        String pathRef = CDB_DIR + "/Containers.xml";
        String pathToCompare = TMCDB_DIR + "/Containers.xml";
        String expression = "/Containers/";
        lookForMissingAttributes(pathRef, pathToCompare, expression);
    }

    private boolean attributeExists(String expression, String pathToCompare) throws Exception {

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        NodeList nodeList = (NodeList) xpath.evaluate(expression,
                new InputSource(new FileReader(pathToCompare)),
                XPathConstants.NODESET);
        if (nodeList.getLength()>0) {
            return true;
        }
        return false;
    }

    private void lookForMissingAttributes(String pathRef, String pathToCompare,
            String expression) throws DOMException, Exception {

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        Set<String> missingAttributes = new HashSet<String>();
        String expressionTmp = "";
        NodeList nodeList = (NodeList) xpath.evaluate(expression + "*",
                new InputSource(new FileReader(pathRef)),
                XPathConstants.NODESET);

        // The XML generated for Component nodes from jDAL contains namespaces,
        // while the XML generated from hibernateJDAL doesn't. This is a workaround
        // to be able to reuse the XPath expression previously used with jDAL XML
        // with the hibernateJDAL XML.
        // TODO: May be it is possible to deactivate namespaces in XPath queries?
        if (expression
                .equals("/urn:schemas-cosylab-com:Components:1.0:Components/")) {
            expression = "/Components/";
        }

        // Compare level 1 depth of node attributes 
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element node = (Element) nodeList.item(i);
            NamedNodeMap nm = node.getAttributes();
            logger.info("Comparing attributes for Component XML node '" + node.getNodeName() +
                    "' from " + pathRef);
            for (int j = 0; j < nm.getLength(); j++) {
                expressionTmp = expression + node.getNodeName() + "[@"+ nm.item(j).getNodeName() +
                "='" + nm.item(j).getNodeValue() + "']"; 
                boolean exists = attributeExists(expressionTmp, pathToCompare);
                expressionTmp = expression;
                String msg = "Attribute name/value pair '" + nm.item(j).getNodeName() + 
                    "=" + nm.item(j).getNodeValue() + "' in node '" + node.getNodeName();

                if (exists) {     
                    msg += "' exists in " + pathToCompare;
                    logger.finer(msg);
                } else {
                	if (!isAnException(nm.item(j).getNodeName())) {
                		// Namespace differences are not taking into account.
                		msg += "' doesn't exists in " + pathToCompare;
                		logger.severe(msg);
                	}
                	if (isAnException(nm.item(j).getNodeName()))
                		logger.finer(" not found in attribute "+nm.item(j).getNodeName()+"(May be this attribute doesn't matters or it's a Namespace)");
                	else
                		missingAttributes.add(nm.item(j).getNodeName());
                }


            }

            // Compare level 2 depth of node attributes 
            NodeList nodeChildList = node.getChildNodes();
            for (int j = 0; j < nodeChildList.getLength(); j++) {
            	if (nodeChildList.item(j).getNodeType() == Node.ELEMENT_NODE) {
            		Element nodeChild = (Element) nodeChildList.item(j);
            		NamedNodeMap nmc = nodeChild.getAttributes();
            		logger.info("Comparing attributes for Component XML node '" + node.getNodeName() +
            				"/" +  nodeChild.getNodeName() + "' from " + pathRef);
            		for (int k = 0; k < nmc.getLength(); k++) {
            			expressionTmp = expression + node.getNodeName() + "/" + nodeChild.getNodeName() +
            			"[@"+ nmc.item(k).getNodeName() + "='" + nmc.item(k).getNodeValue() + "']"; 
            			boolean exists = attributeExists(expressionTmp, pathToCompare);
            			expressionTmp = expression;
            			String msg = "Attribute name/value pair '" + nm.item(j).getNodeName() + 
            			"' in node '" + node.getNodeName() + "/" + nodeChild.getNodeName();
            			if (exists) {
            				msg += "' exists in " + pathToCompare;
            				logger.finest(msg);
            			} 
            			else 
            			{
            				if (!isAnException(nmc.item(j).getNodeName())) {
            					msg += "' doesn't exists in " + pathToCompare;
            					logger.severe(msg);
            				}
            				if (isAnException(nmc.item(k).getNodeName()))
            					logger.finer(" not found in attribute "+nmc.item(k).getNodeName()+" (May be this attribute doesn't matters or it's a Namespace");
            				else 
            					missingAttributes.add(nmc.item(k).getNodeName());
            			}
            		}
            	}
            }
        }
        logger.info("Number of Attribute(s) not found: "
                + missingAttributes.size());
        assertEquals(0, missingAttributes.size());
    }

    private boolean isAnException(String nodeName) throws XPathExpressionException, FileNotFoundException {
    	Set<String> attributesExceptions = getAttributesExceptions("attributesExceptions.xml");
    	for (Iterator<String> it = (Iterator<String>) attributesExceptions.iterator();it.hasNext();){
    		String tmp = it.next();
    		if(nodeName.contains(tmp)){
    			return true;
    		}
    	}
    	return false;
    }
    
    private Set<String> getAttributesExceptions(String path) throws XPathExpressionException, FileNotFoundException {
    	Set<String> attributesExceptions = new HashSet<String>();
    	XPathFactory factory = XPathFactory.newInstance();
    	XPath xpath = factory.newXPath();
    	NodeList nodeList = (NodeList) xpath.evaluate("/*",new InputSource(new FileReader(path)), XPathConstants.NODESET);
    	Element node = (Element) nodeList.item(0);
    	NamedNodeMap nm = node.getAttributes();
    	for (int j = 0; j < nm.getLength(); j++) {
    		attributesExceptions.add(nm.item(j).getNodeName());
    	}
    	return attributesExceptions;
    }
	private void getRecords() throws Exception {

        if (recordsCreated)
            return;
        
        File tmpDir = new File(TMP_DIR);
        tmpDir.mkdir();
        File xmlCDBDir = new File(CDB_DIR);
        xmlCDBDir.mkdir();
        File xmlTMCDBDir = new File(TMCDB_DIR);
        xmlTMCDBDir.mkdir();

        startACS(false);

        DAL dal = container.getCDB();
        String xmldoc = dal.get_DAO("/MACI");
        File tmp = new File(CDB_DIR + "/MACI.xml");
        BufferedWriter out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Components");
        tmp = new File(CDB_DIR + "/Components.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Managers");
        tmp = new File(CDB_DIR + "/Managers.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Containers");
        tmp = new File(CDB_DIR + "/Containers.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        shutdownACS();

        startACS(true);

        dal = container.getCDB();
        xmldoc = dal.get_DAO("/MACI");
        tmp = new File(TMCDB_DIR + "/MACI.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Components");
        tmp = new File(TMCDB_DIR + "/Components.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Managers");
        tmp = new File(TMCDB_DIR + "/Managers.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        xmldoc = dal.get_DAO("/MACI/Containers");
        tmp = new File(TMCDB_DIR + "/Containers.xml");
        out = new BufferedWriter(new FileWriter(tmp));
        out.write(xmldoc);
        out.close();

        shutdownACS();
    }

    private void startACS(boolean enableTMCDB) throws Exception {

        ContainerSpec[] cont = new ContainerSpec[0];
        String[] env = null;
        if (enableTMCDB) {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=true";
            env[2] = "LOAD_FROM_XML=true";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        } else {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=false";
            env[2] = "LOAD_FROM_XML=false";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        }

        acsenv = new ACSTestEnvironment(cont, env);

        acsLog = new FileOutputStream("./tmp/ACS.log");
        acsenv.startACS(acsLog);

        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("managerLoc = " + managerLoc);
        if (compClient == null)
            compClient = new ComponentClient(null, managerLoc, "RoundTripTest");
        container = compClient.getContainerServices();
        logger = container.getLogger();
    }

    private void shutdownACS() throws Exception {
        acsenv.shutdownACS(acsLog);
    }
}
