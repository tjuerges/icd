/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.tmcdb.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import alma.cdbErrType.CDBRecordDoesNotExistEx;
import alma.cdbErrType.CDBXMLErrorEx;

import com.cosylab.CDB.DAL;

/**
 * @author nsaez
 *
 */
public class RoundTripAlmaSideTest extends TestCase {
	
    private static final String TMP_DIR = "./tmp";

    private static final String CDB_DIR = "./tmp/xmlCDB";

    private static final String TMCDB_DIR = "./tmp/xmlTMCDB";

    private static boolean recordsCreated = false;

    static ComponentClient compClient = null;

    private ACSTestEnvironment acsenv = null;

    private ContainerServices container = null;

    private Logger logger;

    FileOutputStream acsLog = null;

    int depthLevel = 0;
    
    Set<String> missingAttributes = new HashSet<String>();
    
    Set<String> wrongAttributes = new HashSet<String>();
    
    /**
     * @return suite
     * @throws Exception
     */
    public static Test suite() throws Exception {
        // String s = System.getProperty("suite");
        TestSuite suite = new TestSuite();
        suite.addTest(new RoundTripAlmaSideTest("testMissingAttr"));
        return suite;
    }    

    /**
     * @param test
     */
    public RoundTripAlmaSideTest(String test) {
        super(test);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getRecords();
        logger = Logger.getLogger("LoadComponentsTest");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    
    private String getFileName(String path){
    	return (new File(path)).getName();
    }
    /**
     * @throws Exception
     */
    public void testMissingAttr() throws Exception {

    	//String ACS_CDB = System.getenv("ACS_CDB");  
    	String path1 ="./CDB/alma/";
    	String path[]= path1.split(""+File.pathSeparatorChar);
    	Vector<String> v = getFilenames(path,"xml");
    	for(int m=0;m<v.size();m++){
    		String path2= (String) v.get(m);
    		System.out.println(path2+" "+getFileName(path2));
    		String pathRef = CDB_DIR + "/"+getFileName(path2);
    		String pathToCompare = TMCDB_DIR + "/"+getFileName(path2);
    		String pathRefTmp = readFileAsString(pathRef);
    		Document doc = parserXML(pathRefTmp);
    		compareAttributes(doc,pathToCompare);
    		if(wrongAttributes.size()>0){
    			System.out.println("Wrong Attributes:");
    			for (Iterator<String> it = (Iterator<String>) wrongAttributes.iterator();it.hasNext();)
    				System.out.println(it.next());
    		}
    		assertEquals(wrongAttributes.size(), 0);
    	}	    
    }

    /**
     * getFilenames function returns a Vector<String> with the 
     * name of the files found in the path given that matches 
     * with the type.
     * 
     * @param paths
     * @param type
     * @return Vector<String>
     */
    
	protected Vector<String> getFilenames(String paths[],String type){
		Vector<String> vector = new Vector<String>();
		String files[];
		/*
		 * Scans the list of paths.
		 */
		for(int i=0;i<paths.length;i++)
		{
			if(paths[i].length() != 0) 
			{
				File file = new File(paths[i]);
				if(file.exists())
				{
					if(file.isFile())
					{ 
						if (paths[i].endsWith("."+type))
							vector.add(paths[i]);
					} 
					else
					{ 
						if(!(paths[i].endsWith("/"))) 
							paths[i]=paths[i]+"/";

						files = (new File(paths[i])).list();
						//Add the files to the vector.
						if (files!=null)
							for (int j=0; j < files.length; j++){
								//filtering for 'type'
								if(files[j].endsWith(type)){		
									vector.addElement(paths[i]+files[j]);
								}
							}
						String[] dirs = (new File(paths[i])).list();
						if(dirs.length != 0)
							for (int j=0; j < dirs.length; j++){
								dirs[j]=paths[i]+dirs[j]+"/";
							}
						vector.addAll(getFilenames(dirs,type));	
					}
				}
			}
		}
		return vector;
	}
	
	/**
	 * searchElement function return an org.w3c.dom.Element
	 * given an XPATH expression and a file path to search.
	 * 
	 * It return only the first Element found.
	 * 
	 * @param expression
	 * @param pathFile
	 * @return
	 * @throws Exception
	 */
	
	private Element searchElement (String expression, String pathFile)  throws Exception{
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodeList = (NodeList) xpath.evaluate(expression, new InputSource(new FileReader(pathFile)),XPathConstants.NODESET);
		Element element = (Element) nodeList.item(0);	
		return element;
	}

	/**
	 * Compare attributes between two XML files.
	 * To compare, this function receive a Node
	 * of the first XML file (parsed previously) 
	 * and the path of the second XML file.
	 * 
	 * For each attribute of the Node it created a expression in 
	 * XPath , in agree with the depth of the Node.
	 * 
	 * Once the expression is created, the function get (when is possible)
	 * the value search and compare if the attribute match or not.
	 * 
	 * The attribute can:
	 * match
	 * doesn't exist 
	 * doesn't match
	 * doesn't matters if don't exist
	 * 
	 * 
	 * Finally this function called to itself recursively until compare
	 * all his Element nodes.
	 * 
	 * @param nodeRef
	 * @param pathToCompare
	 * @throws Exception
	 */
	
	private void compareAttributes (Node nodeRef,String pathToCompare) throws Exception {
		int type = nodeRef.getNodeType();
		String expression="/*";
		if (type == Node.ELEMENT_NODE) {
			logger.finer("Node Name: "+nodeRef.getNodeName());
			if(nodeRef.hasAttributes()) 
			{	//To find the same level of depth in the xml file to compare				
				for (int n=1;n<depthLevel;n++)
					expression+=expression;	

				NamedNodeMap AttributesList = nodeRef.getAttributes();
				for(int j = 0; j < AttributesList.getLength(); j++) {
					NodeList nodeList = getNodeList(expression,pathToCompare);


					for (int k=1;k<=nodeList.getLength();k++){
						//This is for select the wish element compare in this depth level
						String expressionTmp = expression+"["+k+"]";

						Element elementToCompare = searchElement(expressionTmp, pathToCompare);
						if ((elementToCompare!=null)){
							if (!isAnException(AttributesList.item(j).getNodeName())) {
								//Core of the function =========================
								if(!AttributesList.item(j).getNodeValue().equals(elementToCompare.getAttribute(AttributesList.item(j).getNodeName()))){
									logger.severe("[ERROR] The attribute: "+AttributesList.item(j).getNodeName()+" doesn't matches \n CDB  : "+AttributesList.item(j).getNodeValue()+" !=  TMCDB: "+elementToCompare.getAttribute(AttributesList.item(j).getNodeName()));
									wrongAttributes.add(AttributesList.item(j).getNodeName());
									
								}else
									logger.finer("[OK] the attribute "+AttributesList.item(j).getNodeName()+" has "+elementToCompare.getAttribute(AttributesList.item(j).getNodeName())+" in both CDB's");
								//END ==========================================
							}
						}else{
							if (isAnException(AttributesList.item(j).getNodeName()))
								logger.finer("Attribute "+AttributesList.item(j).getNodeName()+" Not found !! (May be this attribute doesn't matters or it's a Namespace");
							else {
								missingAttributes.add(AttributesList.item(j).getNodeName());
								logger.severe("[ERROR] The attribute: "+AttributesList.item(j).getNodeName()+" was not found in TMCDB");
							}
						}
					}
				}
			}
		}

		//The variable depthLevel it's used to know which element search in the another XML
		NodeList children = nodeRef.getChildNodes();
		if (children != null) {
			for (int i=0; i< children.getLength();i++) {
				depthLevel++;
				compareAttributes(children.item(i),pathToCompare);
				depthLevel--;
			}
		}
	}

	
	/**
	 * getNodeList function return an org.w3c.dom.NodeList
	 * given an XPATH expression and a file path to search.
	 * 
	 * @param expression
	 * @param path
	 * @return NodeList
	 * @throws XPathExpressionException
	 * @throws FileNotFoundException
	 */
	
	private NodeList getNodeList(String expression, String path) throws XPathExpressionException, FileNotFoundException {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodeList = (NodeList) xpath.evaluate(expression, new InputSource(new FileReader(path)),XPathConstants.NODESET);
		return nodeList;
	}

	/**
	 * parserXML function return a org.w3c.dom.Document 
	 * This document was generated by a String (xmlDataToParse)
	 * that contains XML data.
	 * 
	 * @param xmlDataToParse
	 * @return Document
	 * @throws SAXException
	 * @throws IOException
	 */
	private Document  parserXML(String xmlDataToParse) throws SAXException, IOException{
		DOMParser parser = new DOMParser();
		parser.parse(new InputSource(new StringReader(xmlDataToParse)));
		Document doc = parser.getDocument();
		return doc;
		}
	
	/**
	 * readFileAsString 
	 * Given a file path , this function convert 
	 * the data to a string
	 * 
	 * @param filePath
	 * @return String
	 * @throws java.io.IOException
	 */
	private String readFileAsString(String filePath)
	throws java.io.IOException{
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(
				new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead=0;
		while((numRead=reader.read(buf)) != -1){
			fileData.append(buf, 0, numRead);
		}
		reader.close();
		return fileData.toString();
	}
   
	/**
	 * writeFile 
	 * given the string daoReference and the fileName
	 * this function try to get the xml data of a part
	 * of CDB (by that match with the string daoReference) 
	 * 
	 * Once this data it's obtains, try to write a file
	 * with the name contains into filename.
	 * 
	 * This will be useful to have files to compare between 
	 * TMCDB and CDB.
	 * 
	 * @param daoReference
	 * @param fileName
	 * @throws InterruptedException 
	 */
	
	private void writeFile(String daoReference,String fileName) throws InterruptedException {
		try{
			DAL dal = container.getCDB();
			String xmldoc = dal.get_DAO(daoReference);
			File tmp = new File(fileName);
			BufferedWriter out = new BufferedWriter(new FileWriter(tmp));
			out.write(xmldoc);
			out.close();
		}catch (AcsJContainerServicesEx e){
			e.printStackTrace();
			
		}catch (CDBXMLErrorEx e){
			e.printStackTrace();
		}catch (CDBRecordDoesNotExistEx e){
			e.printStackTrace();
			acsenv.shutdownACS(acsLog);
			String[] tmp =fileName.split("/");
			String[] cbdConfigChocen =tmp[2].split("xml");
			fail("Couldn't get_DAO from: "+daoReference+ " with: "+cbdConfigChocen[1]+" configuration\n CDB Record Does Not Exist or Not Found !!");
		}catch (IOException e){
			e.printStackTrace();
		}/*finally{
			acsenv.shutdownACS(acsLog);
			String[] tmp =fileName.split("/");
			String[] cbdConfigChocen =tmp[2].split("xml");
			fail("Couldn't get_DAO from: "+daoReference+ " with: "+cbdConfigChocen[1]+" configuration\n CDB Record Does Not Exist or Not Found !!");
		}*/
	}

	/**
	 * This function obtains records of TMCDB or CDB
	 * if they are created, just return. 
	 * @throws Exception
	 */
	private void getRecords() throws Exception {

		if (recordsCreated)
			return;

		recordsCreated = true;

		File tmpDir = new File(TMP_DIR);
		tmpDir.mkdir();
		File xmlCDBDir = new File(CDB_DIR);
		xmlCDBDir.mkdir();
		File xmlTMCDBDir = new File(TMCDB_DIR);
		xmlTMCDBDir.mkdir();


		String path1 ="./CDB/alma/";
		String path[]= path1.split(""+File.pathSeparatorChar);
		path1="./CDB";
		Vector<String> v = getFilenames(path,"xml");
		startACS(false);
		for(int m=0;m<v.size();m++){
			String pathFileName= (String) v.get(m);
			//CDB
			String daoReferenceName =  pathFileName.substring(path1.length(),pathFileName.lastIndexOf("/"));
			System.out.println("With CDB configuration getting XML = container.getCDB().get_DAO("+daoReferenceName+")");
			if(!isACSRunning()){
				fail("ACS is not running see details at ./tmp/ACS.log");
			}
			writeFile(daoReferenceName,CDB_DIR + "/"+getFileName(pathFileName));
		}
		shutdownACS();
		
		startACS(true);
		for(int m=0;m<v.size();m++){
			String pathFileName= (String) v.get(m);
			//TMCDB
			String daoReferenceName =  pathFileName.substring(path1.length(),pathFileName.lastIndexOf("/"));
			System.out.println("With TMCDB configuration getting XML = container.getCDB().get_DAO("+daoReferenceName+")");
			if(!isACSRunning()){
				fail("ACS is not running.It's seems that TMCDB couldn't charge. see details at ./tmp/ACS.log");
			}
			writeFile(daoReferenceName,TMCDB_DIR +"/"+getFileName(pathFileName));
		}
		shutdownACS();
	}

	
	/**
	 * Start ACS with the configuration given
	 * 
	 * @param enableTMCDB
	 * @throws Exception
	 */
    private void startACS(boolean enableTMCDB) throws Exception {

        ContainerSpec[] cont = new ContainerSpec[0];
        String[] env = null;
        if (enableTMCDB) {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=true";
            env[2] = "LOAD_FROM_XML=true";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        } else {
            env = new String[4];
            env[0] = "ACS_CDB=.";
            env[1] = "ENABLE_TMCDB=false";
            env[2] = "LOAD_FROM_XML=false";
            env[3] = "TMCDB_CONFIGURATION_NAME=Test";
        }

        acsenv = new ACSTestEnvironment(cont, env);

        acsLog = new FileOutputStream("./tmp/ACS.log");
        acsenv.startACS(acsLog);

        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("managerLoc = " + managerLoc);
        if (compClient == null)
            compClient = new ComponentClient(null, managerLoc, "RoundTripAlmaSide");
        container = compClient.getContainerServices();
        logger = container.getLogger();
    }

    /**
     * Shutdown ACS
     * @throws Exception
     */
    private void shutdownACS() throws Exception {
        acsenv.shutdownACS(acsLog);
    }

    
    /**
     * isAnException. This function search if a Node Name
     * is a node or attribute that can be dismiss.
     * 
     * @param nodeName
     * @return boolean
     * @throws XPathExpressionException
     * @throws FileNotFoundException
     */
	private boolean isAnException(String nodeName) throws XPathExpressionException, FileNotFoundException {
		Set<String> attributesExceptions = getAttributesExceptions("attributesExceptions.xml");
		for (Iterator<String> it = (Iterator<String>) attributesExceptions.iterator();it.hasNext();){
			String tmp = it.next();
			if(nodeName.contains(tmp)){
				return true;
			}
		}
		return false;
	}

	/**
	 * getAttributesExceptions
	 * This function obtains the name of the attributes that can be dismiss.
	 * 
	 * It returns a Set<String> with the name of the attributes that can be dismiss.
	 * 
	 * @param path
	 * @return
	 * @throws XPathExpressionException
	 * @throws FileNotFoundException
	 */
	private Set<String> getAttributesExceptions(String path) throws XPathExpressionException, FileNotFoundException {
		Set<String> attributesExceptions = new HashSet<String>();
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		NodeList nodeList = (NodeList) xpath.evaluate("/*",new InputSource(new FileReader(path)), XPathConstants.NODESET);
		Element node = (Element) nodeList.item(0);
		NamedNodeMap nm = node.getAttributes();
		for (int j = 0; j < nm.getLength(); j++) {
			attributesExceptions.add(nm.item(j).getNodeName());
		}
		return attributesExceptions;
	}
	
    private boolean isACSRunning(){
    	return  acsenv.isACSRunning(0);
    }
}
