/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestLoad.java
 */
package alma.Control.test.TMCDB;

import alma.TMCDB.load.Add;
import alma.TMCDB.load.Load;
import alma.TMCDB.load.Unload;

public class TestLoad {
	public static void main(String[] arg) {
		test1();
		try {
			Thread.sleep(5000L);
		} catch (InterruptedException e) {
		}
		test2();
	}
	public static void test1() {
		test("load","Configuration","ConfigurationData-0.txt");
		test("unload","Monitor","UnloadMonitor-Empty.txt");
		test("unload","Configuration","UnloadConfiguration-Empty.txt");
		//test("load","Monitor","MonitorData-1.txt");
		//test("unload","Monitor","UnloadMonitor-1.txt");
		//test("unload","Configuration","UnloadConfiguration-1.txt");
		//test("load","Configuration","ConfigurationData-1.txt");
		//test("unload","Configuration","UnloadConfiguration-2.txt");
		//test("add","WeatherStation","WeatherStation-W1.txt");
		//test("unload","Configuration","UnloadConfiguration-3.txt");
		//test("add","Antenna","Antenna-DA41.txt");
		//test("unload","Configuration","UnloadConfiguration-4.txt");
	}
	public static void test2() {
		//test("add","PointingModel","Antenna-DA41-PointingModel.txt");
		//test("unload","Configuration","UnloadConfiguration-5.txt");
		//test("add","DelayModel","Antenna-DA41-DelayModel.txt");
		//test("unload","Configuration","UnloadConfiguration-6.txt");
		//test("add","FocusModel","Antenna-DA41-FocusModel.txt");
		//test("unload","Configuration","UnloadConfiguration-7.txt");
	}
	static final String dbUser = "alma1";
	static final String dbPassword = "alma$dba";
	static final String dbUrl = "jdbc:oracle:thin:@//192.168.35.131:1521/XE";	
	//static final String dbUser = "sa";
	//static final String dbPassword = "";
        //static final String dbUrl ="jdbc:hsqldb:hsql://localhost:8081";     
	public static void test(String mode, String option, String filename) {
		Load load = new Load(); Unload unload = new Unload (); Add add = new Add();
		try {
			if (mode.equals("load") && option.equals("Monitor")) {
				load.loadMonitorDB(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("unload") && option.equals("Monitor")) {
				unload.unloadMonitorDB(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("load") && option.equals("Configuration")) {
				load.loadConfiguration(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("unload") && option.equals("Configuration")) {
				unload.unloadConfiguration(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("add") && option.equals("Antenna")) {
				add.antenna(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("add") && option.equals("WeatherStation")) {
				add.weatherStation(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("add") && option.equals("PointingModel")) {
				add.pointingModel(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			} else if (mode.equals("add") && option.equals("DelayModel")) {
				add.delayModel(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			}  else if (mode.equals("add") && option.equals("FocusModel")) {
				add.focusModel(dbUser, dbPassword, dbUrl, "ATF", filename, "\t");
			}
		} catch (Exception err) {
			err.printStackTrace();
			System.exit(0);
		}	
		System.out.println("---------------------------------------"); 
		System.out.println();
	}
}


