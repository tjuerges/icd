package alma.Control.test.TMCDB;

import alma.TMCDB.TMCDB;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.acs.logging.ClientLogManager;
import alma.archive.database.helpers.DBConfiguration;
import alma.archive.exceptions.general.DatabaseException;

public class OracleTest extends TMCDBTest1  {
	public void setUp() throws Exception {
		logger = ClientLogManager.getAcsLogManager().getLoggerForApplication("DalReader", false); 
		DBConfiguration conf;
		try {
			conf = DBConfiguration.instance(logger);

			dbUser = conf.get("archive.tmcdb.user");
			String location = conf.get("archive.tmcdb.location");
			String service =  conf.get("archive.tmcdb.service");
			location="localhost:1521";
			service="XE";
			dbPassword = "tmc$dba";
			dbUser = "TMCDB";
			dbPassword = "software";
			db = new TMCDB ();
			dbUrl = "jdbc:oracle:thin:@//"+location+"/"+service;
			conn = db.connect(dbUser,dbPassword,dbUrl);
			System.out.println("Connection to TMCDB established.");
		} catch (DatabaseException e) {
			fail("Exception when reading dbConfig.properties: "+e.toString());
		} catch (AcsJTmcdbSqlEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Exception when connecting to Oracle: "+e.toString());
		}
			try {
				db.loadAndExecuteScript(conn, "SQL/DropAllTables.sql");
				db.loadAndExecuteScript(conn, "SQL/DropAllOracleSequences.sql");
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Tables dropped.");
			db.loadAndExecuteScript(conn, "SQL/CreateOracleTables.sql");
			
//			String oraclePath = "/usr/lib/oracle/xe/app/oracle/product/10.2.0/server/bin/";
//			String command = oraclePath+"sqlplus"+dbUser+"/"+dbPassword+"@XE";
//			
//			Runtime runtime = Runtime.getRuntime();
//			Process process = null;
//			try {
//			         process = runtime.exec(command);
//			}//end try
//			catch (Exception e) {
//			         System.out.println("Problem with SQL Plus: " + e);
//			}//end catch
			System.out.println("Tables created.");

		
	}
}
