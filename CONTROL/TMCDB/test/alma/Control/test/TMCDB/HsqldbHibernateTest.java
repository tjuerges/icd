package alma.Control.test.TMCDB;

import alma.TMCDB.generated.AssemblyType;
import alma.TMCDB.generated.PropertyType;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.hla.runtime.asdm.types.Length;

import com.cosylab.cdb.jdal.hibernate.HibernateDBUtil;
import com.cosylab.cdb.jdal.hibernate.HibernateDBUtil.TMCDBConnectionPair;

public class HsqldbHibernateTest extends TMCDBTest1 {

	public void setUp() {
		TMCDBConnectionPair tcp = HibernateDBUtil.setUp(true, true, false);
		db = tcp.db;
		conn = tcp.conn;
	}
	
	public void testSimpleData() throws AcsJTmcdbErrTypeEx
	{
		super.testSimpleData();
		
		try
		{
			AssemblyType t = new AssemblyType ();
			t.setAssemblyName("LO2");
			t.setBaseAddress("0x0");
			t.setNodeAddress("0x0034");
			t.setChannelNumber(0);
			t.setDescription("Assembly description");
			t.setNotes("Some notes here...");
			t.setLRUName("One");
			t.setFullName("Fullname of assembly");
			t.setComponentTypeId(0);	// Assumes only one component type (ID generation by Hsqldb starts from zero)
			db.addAssemblyType(t);
			
			/*
			AssemblyStartup s = new AssemblyStartup ();
			s.setAssemblyId(AssemblyId);
			s.setStartupId(StartupId);
			s.setBaseElementId(BaseElementId);
			s.setGroupName("GroupName");
			s.setOrderTag(OrderTag);
			s.setRoleName("LO2A");
			s.setBaseAddress("0x4002");
			s.setNodeAddress("0x1234");
			s.setChannelNumber(3);
			db.addAssemblyStartup(s);
			 */
	
			PropertyType p = new PropertyType ();
			p.setPropertyName("Voltage");
			p.setTableName("FloatProperty");
			p.setDataType("float");
			p.setAssemblyName("LO2");
			p.setRCA("RCA");
			p.setTeRelated(false);
			p.setRawDataType("RawDataType");
			p.setWorldDataType("WorldDataType");
			p.setUnits("V");
			p.setScale(10.0);
			p.setOffset(new Length(0.0));
			p.setMinRange(0.0);
			p.setMaxRange(10.0);
			p.setSamplingInterval(1.23f);
			p.setGraphMin(-10.0f);
			p.setGraphMax(10.0f);
			p.setGraphFormat("%4.2f");
			p.setGraphTitle("Graph title");
			p.setDescription("Voltage description");
			db.addPropertyType(p);
			
			p = new PropertyType ();
			p.setPropertyName("Current");
			p.setTableName("DoubleProperty");
			p.setDataType("double");
			p.setAssemblyName("LO2");
			p.setRCA("RCA");
			p.setTeRelated(false);
			p.setRawDataType("RawDataType");
			p.setWorldDataType("WorldDataType");
			p.setUnits("A");
			p.setScale(20.0);
			p.setOffset(new Length(0.0));
			p.setMinRange(0.0);
			p.setMaxRange(3.0);
			p.setSamplingInterval(5.43f);
			p.setGraphMin(-3.0f);
			p.setGraphMax(3.0f);
			p.setGraphFormat("%2.3f");
			p.setGraphTitle("Graph title");
			p.setDescription("Current description");
			db.addPropertyType(p);

			db.commit();
		}
		catch (Throwable th)
		{
			th.printStackTrace();
			fail();
		}
		
		/*
		try {
			System.out.println(HibernateUtil.getList(alma.TMCDB.generated.Configuration.class));
			System.out.println(HibernateUtil.getList(Computer.class));
			System.out.println(HibernateUtil.getList(LoggingConfig.class));
			System.out.println(HibernateUtil.getList(Container.class));
			System.out.println(HibernateUtil.getList(Component.class));
			System.out.println(HibernateUtil.getList(Antenna.class));
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
		*/
	}
}
