/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBTest1.java
 */
package alma.Control.test.TMCDB;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import junit.framework.TestCase;

import alma.TMCDB.*;
import alma.TMCDB_IDL.*;
import alma.TMCDB.generated.*;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Length;


/**
 * Test case for populating and quering database tables.
 * 
 *  1. Configuration - populate the table
 * 		- use two configuations.
 *  2. Computer - populate the table
 * 		- ten computers, five computers each for the two configurations
 *  3. Container - populate the table
 * 		- ten containers, one for each computer
 *  4. Component - populate the table
 * 		- ten components (Mount), one for each container
 *  5. Antenna - populate the table
 * 		- ten antennas, one for each container
 *  6. Query the database
 * 		- retrievals are performed by key and alternate key.
 *  7. Delete an antenna by name and configuration and also by index
 *  8. Update an antenna
 *  9. Add and get LRUTypes
 * 10. Delete an LRUType
 * 11. Update an LRUType
 */
public abstract class TMCDBTest1 extends TestCase {
	static protected  String dbUser = "TMCDB";
	static protected  String dbPassword = "software";
	static protected  String dbUrl;

	static protected TMCDB db;
	static protected Logger logger;
	static protected Connection conn;

	public static int[] doConfiguration() throws AcsJTmcdbErrTypeEx {
		int[] index = new int [2];
		Configuration x = new Configuration ();
		x.setConfigurationId(1);
		x.setConfigurationName("Config1");
		x.setCreationTime(new ArrayTime (2007,3,8,16,15,30));
		x.setDescription("This is test configuration 1 of the TMCBD.");
		x.setFullName("Configuration Number One");
		index[0] = db.addConfiguration(x);
		System.out.println("Configuration added: index = " + index[0]);
		x = new Configuration ();
		x.setConfigurationId(2);
		x.setConfigurationName("Config2");
		x.setCreationTime(new ArrayTime (2007,3,8,16,15,45));
		x.setDescription("This is test configuration 2 of the TMCBD.");
		x.setFullName("Configuration Number Two");
		index[1] = db.addConfiguration(x);
		System.out.println("Configuration added: index = " + index[1]);
		db.commit();
		return index;
	}
	
	private static int[] doLoggingConfig(int[] config) throws AcsJTmcdbErrTypeEx {
		int numberConfigs = config.length;
		int[] index = new int [numberConfigs];
		LoggingConfig[] x = new LoggingConfig [numberConfigs];
		for (int i = 0; i < x.length; ++i) {
			x[i] = new LoggingConfig();
//			x[i].setLoggingConfigId(i);
			x[i].setConfigurationId(config[i]);
			x[i].setCentralizedLogger("Log");
			x[i].setMaxLogQueueSize(500);
			x[i].setFlushPeriodSeconds(10);
			x[i].setDispatchPacketSize(5);
			index[i] = db.addLoggingConfig(x[i]);
			
			int keyNL = i*100;
			db.addNamedLoggerConfig(new NamedLoggerConfig(++keyNL, index[i], "NamedLogger" + keyNL, 2, 2));
			db.addNamedLoggerConfig(new NamedLoggerConfig(++keyNL, index[i], "NamedLogger" + keyNL, 4, 3));
		}


		return index;
	}

	public static int[] doComputer(int[] config) throws AcsJTmcdbErrTypeEx {
		int count = -1;
		int numberComputers = 5;
		int[] index = new int [numberComputers * config.length];
		Computer[] x = new Computer [numberComputers * config.length];
		for (int i = 0; i < config.length; ++i) {
			for (int j = 0; j < numberComputers; ++j) {
				++count;
				x[count] = new Computer ();
				x[count].setComputerId(0);
				x[count].setComputerName((count < 10 ? "ALMA0" : "ALMA") + (count + 1));
				x[count].setConfigurationId(config[i]);
				x[count].setHostName((count < 10 ? "abm0" : "abm") + (count + 1));
				x[count].setPhysicalLocation("Somewhere at the ATF");
				x[count].setProcessorType("uni");
				x[count].setRealTime(true);
				index[count] = db.addComputer(x[count]);
				System.out.println("Computer added: index = " + index[count]);
			}
		}
		db.commit();
		return index;
	}
	
	public static int[] doContainer(int[] computer, int[] loggingConfig) throws AcsJTmcdbErrTypeEx {
		int count = -1;
		int numberContainers = computer.length;
		int numberLoggingConfigs = loggingConfig.length;
		int[] index = new int [numberContainers];
		Container[] x = new Container [numberContainers];
		for (int i = 0; i < x.length; ++i) {
			++count;
			x[count] = new Container ();
			x[count].setConfigurationId((db.getComputer(computer[i])).getConfigurationId());
			x[count].setContainerId(0);
			x[count].setComputerId(computer[i]);
			x[count].setContainerName("Container-" + i);
			x[count].setPath(".");
			x[count].setKernelModule("kernel-module-" + i);
			x[count].setKernelModuleLocation("location-" + i);
			x[count].setRealTime(true);
			x[count].setRealTimeType("ABM");
			x[count].setImplLang("cpp");
			x[count].setCmdLineArgs("arg1");
			x[count].setRecovery(true);
			x[count].setLoggingConfigId(loggingConfig[i%numberLoggingConfigs]);
			index[count] = db.addContainer(x[count]);
			System.out.println("Container added: index = " + index[count]);
		}
		db.commit();
		return index;
	}
	
	public static int[] doComponentType() throws AcsJTmcdbErrTypeEx {
		int count = -1;
		ComponentType x = new ComponentType();
		x.setIDL("Some IDL string");
		int[] returnedIds = new int[1];
		returnedIds[0] = db.addComponentType(x);
		return returnedIds;
	}
	
	private static int componentTypeId;
	
	public static int[] doComponent(int[] container) throws AcsJTmcdbErrTypeEx {
		int count = -1;
		int numberComponents = container.length;
		int[] index = new int [numberComponents];
		Component[] x = new Component [numberComponents];
		for (int i = 0; i < x.length; ++i) {
			++count;
			x[count] = new Component ();
			x[count].setCode("code-base-" + i);
			x[count].setComponentId(0);
			x[count].setComponentName("component-" + i);
			x[count].setConfigurationId((db.getComputer(container[i])).getConfigurationId());
			x[count].setContainerId(container[i]);
			// TODO: put in a real reference here
			x[count].setComponentInterfaceId(componentTypeId);
//			x[count].setIDL("idl-" + i);
			x[count].setRealTime(true);
			x[count].setImplLang("cpp");
			x[count].setIsAutostart(false);
			x[count].setIsDefault(false);
			x[count].setIsStandaloneDefined(true);
			x[count].setKeepAliveTime(60);
			x[count].setPath("/one/two/three");
			index[count] = db.addComponent(x[count]);
			System.out.println("Component added: index = " + index[count]);
		}
		db.commit();
		return index;
	}
	
	private static int baseElementId;
	
	public static int[] doBaseElement(int[] config) throws  AcsJTmcdbErrTypeEx {
		BaseElement be = new BaseElement();
		be.setBaseId(1);
		be.setBaseType("Antenna");
		be.setBaseElementName("AntennaPlaceHolder");
		be.setConfigurationId(config[0]);
		baseElementId = db.addBaseElement(be);
		return new int[] {baseElementId};
	}
	
	public static int[] doAntenna(int[] computer, int[] container, int[] component) throws AcsJTmcdbErrTypeEx {
		int count = -1;
		int numberAntennas = computer.length;
		int[] index = new int [numberAntennas];
		Antenna[] x = new Antenna [numberAntennas];
		for (int i = 0; i < x.length; ++i) {
			++count;
			x[count] = new Antenna ();
			x[count].setBaseElementId(baseElementId);
			x[count].setAntennaName("ALMA0" + ((computer[i] % 5) == 0 ? 5 : (computer[i] % 5)));
			x[count].setAntennaType("VA");
			x[count].setCommissionDate(new ArrayTime (2007,3,8,0,0,0));
			x[count].setComponentId(component[i]);
//			x[count].setComputerId(computer[i]);
			x[count].setConfigurationId((db.getComputer(container[i])).getConfigurationId());
//			x[count].setContainerId(container[i]);
			x[count].setDishDiameter(new Length (12.0));
			x[count].setXOffset(new Length (1.1));
			x[count].setYOffset(new Length (1.2));
			x[count].setZOffset(new Length (1.3));
			x[count].setXPosition(new Length (2.1));
			x[count].setYPosition(new Length (2.2));
			x[count].setZPosition(new Length (2.3));
			index[count] = db.addAntenna(x[count]);
			System.out.println("Antenna added: index = " + index[count]);
		}
		db.commit();
		return index;
	}

	public static void showConfiguration(int index) throws AcsJTmcdbErrTypeEx {
		Configuration x = db.getConfiguration(index);
		System.out.println("Configuration Table index number " + index);
		System.out.println(x.toString());
		// Check the to/from IDL methods.
		ConfigurationIDL idl = x.toIDL();
		Configuration y = new Configuration ();
		y.fromIDL(idl);
		assertEquals("ERROR! IDL conversion methods are not equal.", y, x);
	}
	public static void showComputer(int index) throws AcsJTmcdbErrTypeEx {
		Computer x = db.getComputer(index);
		System.out.println("Computer Table index number " + index);
		System.out.println(x.toString());
		// Check the to/from IDL methods.
		ComputerIDL idl = x.toIDL();
		Computer y = new Computer ();
		y.fromIDL(idl);
		assertEquals("ERROR! IDL conversion methods are not equal.", y, x);
	}
	private static void showLoggingConfig(int i, int j) {
		// TODO Auto-generated method stub
		
	}

	public static void showContainer(int count, int index, int computer, int loggingConfig[]) throws AcsJTmcdbErrTypeEx {
		Container x = db.getContainer(index);
		System.out.println("Container Table index number " + index);
		System.out.println(x.toString());
		int numberLoggingConfigs = loggingConfig.length;
		// Check the to/from IDL methods.
		ContainerIDL idl = x.toIDL();
		Container y = new Container ();
		y.fromIDL(idl);
		assertEquals(x,y);	// Verify IDL conversion methods
		/* Compare output with input */
		assertEquals(x.getConfigurationId(),db.getComputer(computer).getConfigurationId());
		assertEquals(x.getContainerId(),index);
		assertEquals(x.getContainerName(),"Container-" + count);
		assertEquals(x.getKernelModule(),"kernel-module-" + count);
		assertEquals(x.getKernelModuleLocation(),"location-" + count);
		assertTrue(x.getRealTime());
		assertEquals(x.getRealTimeType(),"ABM");
		assertEquals(x.getImplLang(),"cpp");
		assertEquals(x.getCmdLineArgs(),"arg1");
		assertTrue(x.getRecovery());
		assertEquals(x.getLoggingConfigId(),loggingConfig[count%numberLoggingConfigs]);
	}
	
	public static void showComponentType(int index) throws AcsJTmcdbErrTypeEx {
		ComponentType x = db.getComponentType(index);
	}
	
	public static void showComponent(int index) throws AcsJTmcdbErrTypeEx {
		Component x = db.getComponent(index);
		System.out.println("Component Table index number " + index);
		System.out.println(x.toString());
		// Check the to/from IDL methods.
		ComponentIDL idl = x.toIDL();
		Component y = new Component ();
		y.fromIDL(idl);
		assertEquals("ERROR! IDL conversion methods are not equal.", y, x);
	}
	public static void showAntenna(int index) throws AcsJTmcdbErrTypeEx {
		Antenna x = db.getAntenna(index);
		System.out.println("Antenna Table index number " + index);
		System.out.println(x.toString());
		// Check the to/from IDL methods.
		AntennaIDL idl = x.toIDL();
		Antenna y = new Antenna ();
		y.fromIDL(idl);
		assertEquals("ERROR! IDL conversion methods are not equal.", y, x);
	}
	
	public void testSimpleData() throws AcsJTmcdbErrTypeEx {
		try {
			System.out.println("TMCDBTest1 beginning.");

			db.initialize();
			System.out.println("TMCDB initialized.");
			System.out.println();
			
			// Add Configurations.
			int[] config = doConfiguration();
			for (int i = 0; i < config.length; ++i) {
				showConfiguration(config[i]);
				System.out.println();
			}
			System.out.println();

			// Add computers.
			int[] computer = doComputer(config);
			for (int i = 0; i < computer.length; ++i) {
				showComputer(computer[i]);
				System.out.println();
			}
			System.out.println();
			
			// Add LoggingConfig
			int [] loggingConfig = doLoggingConfig(config);
			for (int i = 0; i < loggingConfig.length; ++i) {
				showLoggingConfig(i, loggingConfig[i]);
				System.out.println();
			}

			// Add Containers.
			int[] container = doContainer(computer, loggingConfig);
			for (int i = 0; i < container.length; ++i) {
				showContainer(i, container[i], computer[i], loggingConfig);
				System.out.println();
			}
			System.out.println();
			
			// Add ComponentTypes
			int[] componentType = doComponentType();
			for (int i = 0; i < componentType.length; ++i) {
				showComponentType(componentType[i]);
				System.out.println();
			}
			
			componentTypeId = componentType[0];
			
			// Add Components.
			int[] component = doComponent(container);
			for (int i = 0; i < component.length; ++i) {
				showComponent(component[i]);
				System.out.println();
			}
			System.out.println();
			
			int[] baseElement = doBaseElement(config);
			
			// Add Antennas.
			int[] antenna = doAntenna(computer, container, component);
			for (int i = 0; i < antenna.length; ++i) {
				showAntenna(antenna[i]);
				System.out.println();
			}
			System.out.println();
			
			System.out.println("Additional tests.");
			System.out.println();
			
			// Get an antenna by name and configuration.
			System.out.println("Get an antenna by name and configuration.");
			Antenna ant1 = db.getAntenna("ALMA05",1);
			System.out.println(ant1.toString());
			
			// Delete an antenna by name and configuration.
			System.out.println("Delete an antenna by name and configuration.");
			db.deleteAntenna("ALMA05",1);
			db.commit();
			
			// Verify it is really deleted.
			try {
				ant1 = db.getAntenna("ALMA05",1);
				fail("There shouldn't be an antenna ALMA05/2 at this point.");
			} catch (AcsJTmcdbErrTypeEx err) {
				System.out.println("We should get an error that there is no antenna ALMA05/2");
				System.out.println(err.toString());
			}
			
			// Get an antenna by index.
			System.out.println("Get an antenna by index.");
			Antenna ant2 = db.getAntenna(6);
			System.out.println(ant2.toString());
			System.out.println();
			
			// Delete an antenna by index.
			System.out.println("Delete an antenna by index.");
			db.deleteAntenna(6);
			db.commit();
			
			// Verify it is really deleted.
			try {
				ant2 = db.getAntenna(6);
				fail("This isn't supposed to happen.");
			} catch (AcsJTmcdbErrTypeEx err) {
				System.out.println("We should get an error that there is no antenna with an index of 5.");
				System.out.println(err.toString());
			}
			System.out.println();
			
			// Update an antenna.
			System.out.println("Update an antenna by name and configuration.");
			Antenna ant3 = db.getAntenna("ALMA04",1);
			System.out.println(ant3.toString());
			ant3.setXPosition(new Length (9.6));
			ant3.setYPosition(new Length (9.7));
			ant3.setZPosition(new Length (9.8));
			db.updateAntenna(ant3);
			db.commit();
			System.out.println("Antenna updated.");
			Antenna ant4 = db.getAntenna("ALMA04",1);
			System.out.println(ant4.toString());			
			
			// Add LRUTypes.
			System.out.println("Add LRUTypes.");
			LRUType lru1 = new LRUType ();
			lru1.setDescription("This is device number 1.");
			lru1.setFullName("Device number one");
			lru1.setICD("ICD-number-1");
			lru1.setICDDate(new ArrayTime (2007,3,9,0,0,0));
			lru1.setLRUName("One");
			lru1.setNotes("There are very few notes.");
			db.addLRUType(lru1);
			LRUType lru2 = new LRUType ();
			lru2.setDescription("This is device number 2.");
			lru2.setFullName("Device number two");
			lru2.setICD("ICD-number-2");
			lru2.setICDDate(new ArrayTime (2007,3,8,0,0,0));
			lru2.setLRUName("Two");
			db.addLRUType(lru2);
			db.commit();
			System.out.println("LRUTypes added.");
			System.out.println();

			// Get LRUTypes.
			System.out.println("Get LRUTypes.");
			LRUType x = db.getLRUType("One");
			System.out.println(x.toString());
			x = db.getLRUType("Two");
			System.out.println(x.toString());
			System.out.println();
			
			// Delete an LRUType.
			System.out.println("Delete an LRUType.");
			db.deleteLRUType("Two");
			db.commit();
			System.out.println("LRUType Two deleted.");
			System.out.println();
			
			// Verify it is really deleted.
			try {
				/*LRUType lru3 = */db.getLRUType("Two");
				fail("There shouldn't be any LRUType of 'Two' at this point.");
			} catch (AcsJTmcdbErrTypeEx err) {
				System.out.println("We should get an error that there is no LRU with a name of Two.");
				System.out.println(err.toString());
			}
			System.out.println();

			// Update an LRUType.
			System.out.println("Update an LRUType.");
			LRUType lru4 = db.getLRUType("One");
			System.out.println(lru4.toString());
			lru4.setDescription("This is a new description of LRUType One.");
			lru4.setICD("New-version-of-ICD-One");
			db.updateLRUType(lru4);
			db.commit();
			System.out.println("LRUType updated.");
			LRUType lru5 = db.getLRUType("One");
			System.out.println(lru5.toString());			

			db.release();
			System.out.println("TMCDB released.");
			System.out.println("TMCDBTest1 ended.");
		} catch (AcsJTmcdbErrTypeEx err) {
			err.printStackTrace();
			throw err;
		}
	}
	
	public void tearDown() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Couldn't close the connection properly.");
		}
	}

}


