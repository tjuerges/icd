package alma.Control.test.TMCDB;


import java.io.*;
import java.util.logging.*;
import java.util.concurrent.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import alma.TMCDB.load.Add;
import alma.TMCDB.load.Load;
import alma.TMCDB.load.Unload;
//TODO I changed the way to access the Database for Load, Unload and Add classes. Now it does not matter if as Backend DB we have HSQLDB or Oracle
//since I'm reading dbconfig.properties file for configuring this. 
//So is no longer needed to have a method signature like this one:
//load.loadConfiguration(dbUser, dbPassword, dbUrl, "ATF", "CSVInputFiles/ConfigurationData-0.txt", "\t");
//that API must be change to 
//loadConfiguration(String filename, String tabulator); 
//Example:
//load.loadConfiguration("CSVInputFiles/ConfigurationData-0.txt", "\t");
public class LoadTest extends Thread {
    static final String dbUser = "sa";
    static final String dbPassword = "";
    static final String dbUrl = "jdbc:hsqldb:hsql://localhost";    
    public static boolean deleteDir(File dir) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i=0; i<children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }
        
            // The directory is now empty so delete it
            return dir.delete();
        }

    public LoadTest() {
    }
    public void run() {
        try{
        Process startDatabase = Runtime.getRuntime().exec("java -cp ../lib/hsqldb.jar org.hsqldb.Server -port 8081 &> tmp1/HSQLStart.log");
        BufferedReader Resultset = new BufferedReader(
                                new InputStreamReader (
                                startDatabase.getInputStream()));
         
                String line;
                while ((line = Resultset.readLine()) != null) {
                        System.out.println(line);
                        }
        System.out.println("Database Started");
        } catch (Exception ex){
            
        }
    }

    @Before
    public void setUp() throws Exception {
        //This method is run everytime a TestCase is executed
        System.out.println("Creating all HSQLDB tables");
        String createHSQLDBTablesStmt= "./CreateAllHsqldbTables.sh";
        Process createHSQLDBTables= Runtime.getRuntime().exec(createHSQLDBTablesStmt);
        Thread.sleep(5000);
        
    }

    @After
    public void tearDown() throws Exception {
        //Here we Drop all tables on HSQLDB
        String dropHSQLDBTablesStmt= "./DropAllHsqldbTables.sh";
        Process dropHSQLDBTables= Runtime.getRuntime().exec(dropHSQLDBTablesStmt);
        Thread.sleep(3000);   
    }

    @BeforeClass
    public static void setUpBeforeClass()  throws Exception {
        //Let's check if CSVOutputFiles folder is created. If it's, get rid of it. If not, create it empty
        File CSVOutputFolder= new File("CSVOutputFiles");
        if (!CSVOutputFolder.exists() || !CSVOutputFolder.isDirectory() ){
            System.out.println("There's no CSVOutputFiles folder created....Creating one..");
            CSVOutputFolder.mkdirs();
        } else {
            System.out.println("CSVOutputFiles Folder Exists...Cleaning its contents..");
            deleteDir(CSVOutputFolder);
            CSVOutputFolder.mkdirs();
        }
        
        //Let's check if tmp1 is already created...
        File tmp1Dir = new File("tmp1");
        if (!tmp1Dir.exists() || !tmp1Dir.isDirectory()){
            System.out.println("There's no directory tmp1!!!. Creating tmp1. ");
            tmp1Dir.mkdirs();
                }else{
                    System.out.println("tmp1 exist!");
                }
        System.out.println("Starting Database");
        (new LoadTest()).start(); //a new thread for starting HSQLDB
        
        Thread.sleep(5000); // Here we wait till HSQLDB is started. pause 5 seconds
        //Here we both start-up and create tables on  HSQLDB
        String dropHSQLDBTablesStmt= "./DropAllHsqldbTables.sh";
        Process dropHSQLDBTables= Runtime.getRuntime().exec(dropHSQLDBTablesStmt);
        Thread.sleep(5000);
        //System.out.println("Creating all HSQLDB tables");
        //String createHSQLDBTablesStmt= "./CreateAllHsqldbTables.sh";
        //Process createHSQLDBTables= Runtime.getRuntime().exec(createHSQLDBTablesStmt);
        //Thread.sleep(5000);
        
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {

        //Here we Drop all tables on HSQLDB
        String dropHSQLDBTablesStmt= "./DropAllHsqldbTables.sh";
        Process dropHSQLDBTables= Runtime.getRuntime().exec(dropHSQLDBTablesStmt);
        Thread.sleep(3000);
        //And then shutdown the database
        String shutdownStmt= "./stop_hsqldb.sh";
        Process shutdownDB= Runtime.getRuntime().exec(shutdownStmt);
        Thread.sleep(3000);        
    }

    /**
     * @see alma.TMCDB.load.Load#load(alma.TMCDB.load.TableAddOption,alma.TMCDB.load.IdentifierMap,String,String,String,String,alma.TMCDB.load.LoadList)
     */
    @Test
    public void testLoadUnloadConfigurationRoundTripSimple() {
        //Assert.fail("Unimplemented");
        try {
            Load load = new Load();
            load.loadConfiguration(dbUser, dbPassword, dbUrl, "ATF", "CSVInputFiles/ConfigurationData-0.txt", "\t");
            System.out.println("First Test");
            Assert.assertTrue("True",1==1);
        } catch (Exception err) {
                Assert.assertFalse("True",1==1);
                err.printStackTrace();
                //System.exit(0);
        }       
        System.out.println("---------------------------------------"); 
        System.out.println();
        try {
            Unload unload = new Unload();
            unload.unloadConfiguration(dbUser,dbPassword,dbUrl, "ATF","CSVOutputFiles/UnloadConfiguration-Empty.txt", "\t");
            

        } catch (Exception ex) {
            Assert.assertFalse("Unload Configuration from DB not possible",1==1);
            ex.printStackTrace();
        }
        System.out.println("---------------------------------------"); 
        System.out.println();
    }
    
    @Test
    public void testLoadUnloadConfigurationRoundTripComplex(){
        try {
            Load load = new Load();
            load.loadConfiguration(dbUser, dbPassword, dbUrl, "ATF", "CSVInputFiles/ConfigurationData-1.txt", "\t");
            System.out.println("Second Test");
            Assert.assertTrue("True",1==1);
        } catch (Exception err) {
                Assert.assertFalse("True",1==1);
                err.printStackTrace();
                //System.exit(0);
        }       
        System.out.println("---------------------------------------"); 
        System.out.println();
        try {
            Unload unload = new Unload();
            unload.unloadConfiguration(dbUser,dbPassword,dbUrl, "ATF","CSVOutputFiles/UnloadConfiguration-1.txt", "\t");
            

        } catch (Exception ex) {
            Assert.assertFalse("Unload Configuration from DB not possible",1==1);
            ex.printStackTrace();
        }
        System.out.println("---------------------------------------"); 
        System.out.println();
        }

    
}
