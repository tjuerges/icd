/**
 * 
 */
package alma.Control.test.TMCDB;

import java.io.StringWriter;
import java.util.List;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.acs.alarmsystem.generated.Category;
import alma.acs.alarmsystem.generated.FaultFamily;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

import com.cosylab.acs.laser.dao.ACSAlarmDAOImpl;
import com.cosylab.acs.laser.dao.ACSCategoryDAOImpl;
import com.cosylab.acs.laser.dao.ACSResponsiblePersonDAOImpl;
import com.cosylab.acs.laser.dao.ConfigurationAccessor;
import com.cosylab.cdb.jdal.WDALImpl;

/**
 * TMCDB has to be started already.
 * @author msekoranja
 *
 */
public class AlarmSystemConfigurationTMCDBTest extends ComponentClientTestCase {
	private ContainerServices cs;
	
	public AlarmSystemConfigurationTMCDBTest(String testName) throws Exception {
		super(testName);
	}

	protected void setUp() throws Exception {
		super.setUp();
		cs = getContainerServices();
	}
	
	class AlarmSystemConfigurationData
	{
		final List<FaultFamily> families;
		final Category[] categories;

		public AlarmSystemConfigurationData(List<FaultFamily> families, Category[] categories) {
			super();
			this.families = families;
			this.categories = categories;
		}
	}
	
	private AlarmSystemConfigurationData getAlarmSystemConfigurationData(ConfigurationAccessor conf)
		throws Throwable
	{
		// create DAOs
    	ACSAlarmDAOImpl alarmDAO = new ACSAlarmDAOImpl(m_logger);
    	ACSCategoryDAOImpl categoryDAO = new ACSCategoryDAOImpl(m_logger, alarmDAO);
    	ACSResponsiblePersonDAOImpl responsiblePersonDAO = new ACSResponsiblePersonDAOImpl();
    	
    	// configure
		alarmDAO.setConfAccessor(conf);
		alarmDAO.setSurveillanceAlarmId("SURVEILLANCE:SOURCE:1");
		alarmDAO.setResponsiblePersonDAO(responsiblePersonDAO);

		categoryDAO.setConfAccessor(conf);
		//categoryDAO.setCategoryTreeRoot("ACS");
		categoryDAO.setCategoryTreeRoot("ROOT");
		categoryDAO.setSurveillanceCategoryPath("ACS.SURVEILLANCE");

		responsiblePersonDAO.setAlarmDAO(alarmDAO);
    	
    	// load
		final List<alma.acs.alarmsystem.generated.FaultFamily> families = alarmDAO.loadAlarms();
		final alma.acs.alarmsystem.generated.Category[] categories = categoryDAO.loadCategories();

		return new AlarmSystemConfigurationData(families, categories);
	}
	
	public void testAlarmSystemConfiguration() throws Throwable
	{
		// read config via TMCDB
    	m_logger.config("Reading ACS Alarm System configuration from CDB...");
    	
		ConfigurationAccessor confViaCDB = new ConfigurationAccessor()
		{
			public String getConfiguration(String path) throws Exception {
				return cs.getCDB().get_DAO(path);
			}

			public boolean isWriteable() {
				return false;
			}

			public void setConfiguration(String path, String data) throws Exception {
				// noop
			}
			
			public void deleteConfiguration(String path) throws Exception {
				// noop
			}
		};

		AlarmSystemConfigurationData cdbData = getAlarmSystemConfigurationData(confViaCDB);
		final String cdbDataXMLs = toString(cdbData);
		
    	m_logger.config("\tdone.");
    	m_logger.config("Reading ACS Alarm System configuration from XML...");

    	String[] args = new String[] { "-root", System.getProperty("ACS.cdb", "./") };
    	
 		final WDALImpl jDALImpl = new WDALImpl(args, null, null);
		
		ConfigurationAccessor confViaXML = new ConfigurationAccessor()
		{
			public String getConfiguration(String path) throws Exception {
				return jDALImpl.get_DAO(path);
			}

			public boolean isWriteable() {
				return false;
			}

			public void setConfiguration(String path, String data) throws Exception {
				// noop
			}
			
			public void deleteConfiguration(String path) throws Exception {
				// noop
			}
		};

		AlarmSystemConfigurationData xmlData = getAlarmSystemConfigurationData(confViaXML);
		final String xmlDataXMLs = toString(xmlData);

    	m_logger.config("\tdone.");
    	/*
    	FileWriter fw = new FileWriter("cdb.txt");
    	fw.write(cdbDataXMLs);
    	fw.close();
    	
    	fw = new FileWriter("xml.txt");
    	fw.write(xmlDataXMLs);
    	fw.close();
    	*/
    	assertEquals(xmlDataXMLs, cdbDataXMLs);
	}

	/**
	 * @param cdbData
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private String toString(AlarmSystemConfigurationData cdbData) throws MarshalException, ValidationException {
		StringWriter dataXML = new StringWriter();
		for (Category category : cdbData.categories)
			category.marshal(dataXML);
		
		for (FaultFamily faultFamily : cdbData.families)
			faultFamily.marshal(dataXML);
		
		return dataXML.toString();
	}

}
