/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Pass2Test1.java
 */
package alma.Control.test.TMCDB;

import alma.Control.datamodel.meta.ParseTMCDBTableDefinition;
import org.openarchitectureware.core.constraint.DesignError;

/**
 * The Pass2Test1 is a test of the pass-2 parser.  It takes two arguments: the directory
 * where the SQL file is located and the name of the file to be processed.  The test 
 * parses the SQL file, prints each line with the line number and the type of statement
 * it is.  This list is followed by a list of any errors.
 *
 */
public class Pass2Test1 {
	
	public static void main (String[] arg) {
		if (arg.length != 2) {
			System.out.println("Invalid execution: Pass2Test1 <directory> <filename>");
			return;
		}
		
		System.out.println("Running test 1 of ParseTMCDBTableDefinition.");
		
		ParseTMCDBTableDefinition p = new ParseTMCDBTableDefinition(arg[0],arg[1],null);
		
		try {
			p.createModel();
			System.out.println("No errors detected in TMCDB Table Definition.");
		} catch (DesignError err) {
			err.printStackTrace();
		}

		System.out.println("End test 1 of ParseTMCDBTableDefinition.");
	}

}


