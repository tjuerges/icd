package alma.Control.test.TMCDB;

import alma.TMCDB.TMCDB;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;


public class HsqldbTest extends TMCDBTest1 {
	public void setUp() throws Exception {
		dbUser = "sa";
		dbPassword = "";
		// static final String dbUrl = "jdbc:hsqldb:hsql://localhost/tmcdb";
		dbUrl = "jdbc:hsqldb:mem:tmcdb";
		try {
			db = new TMCDB();
			conn = db.connectHsqldb(dbUser, dbPassword, dbUrl);
			System.out.println("Connection to TMCDB established.");
			
			if (!dbUrl.endsWith("mem:tmcdb")) {
				db.loadAndExecuteScript(conn, "SQL/DropAllTables.sql");
			}			
			db.loadAndExecuteScript(conn, "SQL/CreateHsqldbTables.sql");
		} catch (AcsJTmcdbSqlEx e) {
			fail("Exception when reading connecting to Hsqldb: " + e.toString());
		}

	}
}
