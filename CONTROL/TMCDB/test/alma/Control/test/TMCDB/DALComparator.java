package alma.Control.test.TMCDB;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.omg.CORBA.ORB;

import alma.ACSErrTypeCommon.wrappers.AcsJCORBAProblemEx;
import alma.ACSErrTypeCommon.wrappers.AcsJGenericErrorEx;
import alma.acs.container.corba.OrbConfigurator;
import alma.acs.logging.ClientLogManager;
import alma.acs.util.ACSPorts;
import alma.acs.util.CmdLineArgs;
import alma.acs.util.CmdLineRegisteredOption;

import com.cosylab.CDB.DAL;
import com.cosylab.CDB.DALHelper;
import com.cosylab.CDB.DAO;

/**
 * <p>
 * Currently there is no need to inherit from ComponentClient because we get the CDB reference directly
 * using CORBA mechanisms.
 * 
 * @author hsommer
 */
public class DALComparator 
{
	private final Logger logger;
	private String cdbIOR1;
	private String cdbIOR2;
	private ORB orb;

	private DAL dal1 = null;
	private DAL dal2 = null;
	
	public DALComparator() {
		OrbConfigurator orbConfig = OrbConfigurator.getOrbConfigurator();		
		orb = ORB.init(orbConfig.getOptions(), orbConfig.getProperties());
		logger = ClientLogManager.getAcsLogManager().getLoggerForApplication(this.getClass().getName(), false); 
	}

	public void execute(String[] args) throws Throwable
	{
		setOptions(args);
		
		getJDAL1();
		getJDAL2();
		
		//compareNodes("");
		compareNodes("MACI", false);
		compareNodes("MACI/Managers", true);
		compareNodes("MACI/Containers", true);
		compareNodes("MACI/Components", true);
	}

	public static final void fillSet(Set<String> set, String[] array)
	{
		for (String element : array)
			set.add(element);
	}
	
	private Set<String> getListNodes(DAL dal, String node, boolean recursive)
	{
		// to have the same order (sorted)
		TreeSet<String> subnodes = new TreeSet<String>();
	    
	    LinkedList<String> stack = new LinkedList<String>();
		stack.addLast(node);
		while (!stack.isEmpty())
		{
		    String parentNode = stack.removeLast().toString();
		    
		    String nodes = dal.list_nodes(parentNode);
			//TreeSet<String> localTreeSet = new TreeSet<String>();
			if (nodes.length() > 0)
			{
			    StringTokenizer tokenizer = new StringTokenizer(nodes);
			    while (tokenizer.hasMoreTokens())
			    {
			        String nodeName = tokenizer.nextToken();
			        if (nodeName.endsWith(".xml"))
			            continue;
			        //localTreeSet.add(nodeName);
			        
		        	String fullName = parentNode + "/" + nodeName;

		        	if (recursive)
			        	stack.addLast(fullName);

		        	// strip off relative path
			        subnodes.add(fullName.substring(node.length()+1));
			    }
			}
			//System.out.println("list_nodes for '" + parentNode + "' = " + localTreeSet);
		}				
	    
		return subnodes;
	}
	
	public static final String standardiseValue(String value)
	{
		if (value == null)
			return null;
		
		// check if integer
		// if it is, just return value (integers are represented in only one way)
		try
		{
			Integer.valueOf(value);
			return value;
		}
		catch (NumberFormatException nfe)
		{
			// noop
		}
		
		// check if floating point value
		// if it is, do the conversion
		try
		{
			double dblVal = Double.valueOf(value);
			return String.valueOf(dblVal);
		}
		catch (NumberFormatException nfe)
		{
			// noop
		}
		
		return value;
	}

	
	public void compareNodes(String root, boolean compareAttributeValues) throws Throwable
	{
		System.out.println();
		System.out.println("Node list comparison of '" + root + "'");

		Set<String> rootNodes1 = getListNodes(dal1, root, false);
		Set<String> rootNodes2 = getListNodes(dal2, root, false);
		
		System.out.println("[1] Node of '" + root + "': " + rootNodes1);			
		System.out.println("[2] Node of '" + root + "': " + rootNodes2);		
		boolean equals = rootNodes1.equals(rootNodes2);
		System.out.println("List of nodes equals: " + equals);	
		
		if (rootNodes1.size() > 0)
		{
			System.out.println();
			System.out.println("Attribute list comparison");
	
			// compare attributes
			for (String subnode : rootNodes1)
			{
				final String path = root + "/" + subnode;
				DAO dao1 = dal1.get_DAO_Servant(path);
				DAO dao2 = dal2.get_DAO_Servant(path);
				
				TreeSet<String> attributes1 = new TreeSet<String>();
				TreeSet<String> attributes2 = new TreeSet<String>();
	
				fillSet(attributes1, dao1.get_string_seq(""));
				fillSet(attributes2, dao2.get_string_seq(""));
				
				System.out.println("[1] Attributes of '" + path + "': " + attributes1);			
				System.out.println("[2] Attributes of '" + path + "': " + attributes2);		
				boolean equalsAttr = attributes1.equals(attributes2);
				System.out.println("List of attributes equals: " + equalsAttr);	
				
				// create intersection
				attributes1.retainAll(attributes2);

				// compare attribute values
				if (compareAttributeValues && attributes1.size() > 0 /* && equalsAttr*/)
				{
					boolean allEquals = true;
	
					System.out.println();
					System.out.println("Attribute value comparison of '" + path + "'");
					
					for (String attribute : attributes1)
					{
						try
						{
							String value1 = standardiseValue(dao1.get_string(attribute));
							String value2 = standardiseValue(dao2.get_string(attribute));
							
							final boolean eq = value1.equals(value2);
							final String eqStr = eq ? "==" : "!=";
							System.out.println("  Attribute '" + attribute + "' value: '" + value1 + "' " + eqStr + " '" + value2 + "'");			
							if (allEquals)
								allEquals = eq;
						}
						catch (Throwable th)
						{
							System.out.println("-- Failed to read attribute '" + attribute + "'.");
							allEquals = false;
						}
					}
					System.out.println("All attribute values equals: " + allEquals);	
				}
				
			}
		}		
	}

	public DAL getJDAL1() throws AcsJCORBAProblemEx {
		if (dal1 == null)
		{
			try {
				dal1 = DALHelper.narrow(orb.string_to_object(cdbIOR1));
			}
			catch (Throwable thr) {
				throw new AcsJCORBAProblemEx(thr);
			}		
		}
		return dal1;
	}

	public DAL getJDAL2() throws AcsJCORBAProblemEx {
		if (dal2 == null)
		{
			try {
				dal2 = DALHelper.narrow(orb.string_to_object(cdbIOR2));
			}
			catch (Throwable thr) {
				throw new AcsJCORBAProblemEx(thr);
			}		
		}
		return dal2;
	}

	public void setOptions(String[] args) throws AcsJGenericErrorEx {
		try {
			CmdLineArgs cmdArgs = new CmdLineArgs();
			CmdLineRegisteredOption optCdbIOR1 = new CmdLineRegisteredOption("-cdbIOR1", 1);
			CmdLineRegisteredOption optCdbIOR2 = new CmdLineRegisteredOption("-cdbIOR2", 1);
			cmdArgs.registerOption(optCdbIOR1);		
			cmdArgs.registerOption(optCdbIOR2);		
			cmdArgs.parseArgs(args);
			
			if (cmdArgs.isSpecified(optCdbIOR1)) {
				cdbIOR1 = cmdArgs.getValues(optCdbIOR1)[0].trim();
			}
			else {
				cdbIOR1 = "corbaloc::" + InetAddress.getLocalHost().getHostName() + ":" + ACSPorts.getCDBPort() + "/CDB";			
			}
			logger.info("CDB IOR 1:" + cdbIOR1);

			if (cmdArgs.isSpecified(optCdbIOR2)) {
				cdbIOR2 = cmdArgs.getValues(optCdbIOR2)[0].trim();
			}
			else {
				cdbIOR2 = "corbaloc::" + InetAddress.getLocalHost().getHostName() + ":" + ACSPorts.getCDBPort() + "/CDB";			
			}
			logger.info("CDB IOR 2:" + cdbIOR2);
		}
		catch (Throwable thr) {
			throw new AcsJGenericErrorEx(thr); // @todo better fitting ex
		}		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)  throws Throwable {
		new DALComparator().execute(args);
	}
	
}
