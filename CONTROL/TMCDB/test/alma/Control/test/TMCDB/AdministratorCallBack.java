package alma.Control.test.TMCDB;

import java.util.logging.Level;
import java.util.logging.Logger;

import alma.TMCDB.TMCDB;
import alma.TMCDB.generated.ComponentExecution;
import alma.TmcdbErrType.wrappers.AcsJTmcdbDuplicateKeyEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.acs.util.UTCUtility;

import si.ijs.maci.AdministratorOperations;
import si.ijs.maci.AuthenticationData;
import si.ijs.maci.ClientInfo;
import si.ijs.maci.ClientOperations;
import si.ijs.maci.ClientType;
import si.ijs.maci.ComponentInfo;
import si.ijs.maci.ContainerInfo;
import si.ijs.maci.ImplLangType;
import si.ijs.maci.SynchronousAdministratorOperations;

public class AdministratorCallBack implements SynchronousAdministratorOperations, ClientOperations {
	
	private Logger logger;
    private final long startTimeUTClong;
    private final long timestampToMs = 1000000;

    private long executionId = -1;
	private String m_clientName;
	private long numberActivations = 0;
	private double totalTime = 0.0;
	private double sumOfSquares = 0.0;
	private double maxDeviation = 0.0;
	private long lastTimestamp;
	private TMCDB tmcdb;
	
	class LoadStatistics {
		public double average;
		public double rms;
		public double maxDeviation;
		
		LoadStatistics(double avg, double rms, double deviation) {
			this.average = avg;
			this.rms = rms;
			this.maxDeviation = deviation;
		}
	}
	
	public AdministratorCallBack(String clientName, Logger logger, TMCDB tmcdb) {
		this.logger = logger;
		m_clientName = clientName;
		this.tmcdb = tmcdb;
    	startTimeUTClong = UTCUtility.utcJavaToOmg(System.currentTimeMillis());		
	}

	public void client_logged_in(ClientInfo info, long timestamp,
			long execution_id) {
		logger.log(Level.INFO,"Client logged in.");

	}

	public void client_logged_out(int h, long timestamp) {
		// TODO Auto-generated method stub
		logger.log(Level.INFO,"Client logged out.");
	}

	public void component_activated(ComponentInfo info, long timestamp,
			long execution_id) {
		// TODO Auto-generated method stub
		logger.log(Level.INFO,++numberActivations+"th Activation of component "+info.name);
		if (numberActivations > 1) {
			double diff = timestamp - lastTimestamp;
			totalTime += diff;
			sumOfSquares += diff*diff;
			maxDeviation = Math.max(Math.abs(diff), maxDeviation);
		}
		lastTimestamp = timestamp;
		ComponentExecution ce = new ComponentExecution();
		ce.setComponentExecutionAcsId(execution_id);
		//TODO: fill in the rest
		try {
			tmcdb.addComponentExecution(ce);
		} catch (AcsJTmcdbRowAlreadyExistsEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJTmcdbSqlEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJTmcdbDuplicateKeyEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJTmcdbNoSuchRowEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void component_deactivated(int h, long timestamp) {
		logger.log(Level.INFO,"Component deactivated.");

	}

	public void components_released(int[] clients, int[] components,
			long timestamp) {
		logger.log(Level.INFO,"Components released.");

	}

	public void components_requested(int[] clients, int[] components,
			long timestamp) {
		logger.log(Level.INFO,"Components requested.");

	}

	public void container_logged_in(ContainerInfo info, long timestamp,
			long execution_id) {
		logger.log(Level.INFO,"Container logged in.");

	}

	public void container_logged_out(int h, long timestamp) {
		logger.log(Level.INFO,"Container logged out.");

	}

	/**
	 * @see si.ijs.maci.ClientOperations#authenticate(java.lang.String)
	 */
	public AuthenticationData authenticate(long execution_id, String question)
	{
        // keep old executionId if it exists
        if (executionId < 0) {
        	executionId = execution_id;
        }
        
		AuthenticationData ret = new AuthenticationData(
				 "C", 
				 ClientType.ADMINISTRATOR_TYPE,
				 ImplLangType.JAVA,
				 false, 
				 startTimeUTClong,
				 executionId);
		return ret;
	}

	public void components_available(ComponentInfo[] components) {
		// TODO Auto-generated method stub

	}

	public void components_unavailable(String[] component_names) {
		// TODO Auto-generated method stub

	}

	public void disconnect() {
		// TODO Auto-generated method stub

	}

	public void message(short type, String message) {
		logger.log(Level.INFO,"Message received from Manager: "+message);

	}

	public void taggedmessage(short type, short messageID, String message) {
		logger.log(Level.INFO,"Tagged message received from Manager: "+message);

	}

	public String name() {
		return m_clientName;
	}

	public boolean ping() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public LoadStatistics getLoadStatistics() {
		double calculatedAvg = totalTime/(numberActivations-1);
		double square = (sumOfSquares - (numberActivations-1)*calculatedAvg);
		double calculatedRms = Math.sqrt(square)/(numberActivations-2);
		return new LoadStatistics(calculatedAvg/timestampToMs, calculatedRms/timestampToMs, maxDeviation/timestampToMs);
	}

}
