package alma.Control.test.TMCDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;

import si.ijs.maci.AdministratorOperations;
import alma.MOUNT_ACS.Mount;
import alma.TMCDB.TMCDB;
import alma.TMCDB.generated.ComponentExecution;
import alma.TMCDB.generated.ComponentType;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.AdvancedContainerServices;
import alma.acs.container.AdvancedContainerServicesImpl;
import alma.acs.container.ContainerServices;
import alma.demo.HelloDemo;

/**
 * @author jschwarz
 * To run this test:
 * 1) ACS must be up and running
 * 2) the Hibernate DAL must be running
 * 3) the frodoContainer (a Java container) must be up and running
 */
public class ExecutionRecordsTest extends ComponentClientTestCase {
	
	private String testName;
	private Connection c;
	private String compName = "HELLODEMO1";
	private String mountComponent = "MOUNT1";
	private TMCDB tmcdb;
	private ContainerServices cs;
	private AdvancedContainerServices advCS;
	private AdministratorOperations adminOp;
	private final boolean runCplus = false;
	private final boolean runJava = true;
	
	private ComponentExecution ce;
	
	private final long NUM_ITERATIONS = 500000; // Change this to 25000 to make things more interesting!

	public ExecutionRecordsTest(String testName) throws Exception {
		super(testName);
		this.testName = testName;
//		tmcdb = new TMCDB();
//		c = tmcdb.connectHsqldb("sa", "", "jdbc:hsqldb:hsql://localhost");
//		c = tmcdb.connectHsqldb("sa", "", "jdbc:hsqldb:mem:tmcdb");
//		tmcdb.initialize();
//		ce = new ComponentExecution();
//		Class.forName("org.hsqldb.jdbcDriver");
//		c = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost", "sa", "");
	}

	private HelloDemo myNewComp; // Java client from jcontexmpl
	private Mount myMount;
	
	protected void setUp() throws Exception {
		super.setUp();
		// TODO: setup an hsqldb database with tables defined
		// TODO: load it with whatever the manager needs to get started
		cs = getContainerServices();
		advCS = (AdvancedContainerServices) cs.getAdvancedContainerServices();
		adminOp = new AdministratorCallBack(testName, m_logger, tmcdb);
		advCS.connectManagerAdmin(adminOp, false);
	}
	
	public void testInsertOneRecord() throws Exception {
		if (!runJava) return;
		for (long i = 0; i < NUM_ITERATIONS; i++) {
			myNewComp = alma.demo.HelloDemoHelper.narrow(cs
					.getComponent(compName));

			// TODO: Check that the startup time has been filled in
			//		Statement s = c.createStatement();
			//		ResultSet r = s.executeQuery("SELECT ConfigurationId, ConfigurationName FROM Configuration");
			//		r.next();
			//		m_logger.log(Level.INFO,"Configuration name is: "+r.getString("ConfigurationName"));
			//		PreparedStatement p = c.prepareStatement("SELECT ComponentId, ComponentInterfaceId FROM Component WHERE ComponentName = '"+CompName+"'");
			//		r = p.executeQuery();
			//		r.next();
			//		int ctypeId = r.getInt("ComponentInterfaceId");
			//		int cid = r.getInt("ComponentId");
			//		m_logger.log(Level.INFO,"Component interface ID is: "+ctypeId);
			//		ComponentType ctype = tmcdb.getComponentType(ctypeId);
			//		m_logger.log(Level.INFO,ctype.toString());
			//		p = c.prepareStatement("SELECT COUNT (*) FROM ComponentExecution WHERE ComponentId = "+cid);
			//		r = p.executeQuery();
			//		r.next();
			//		m_logger.log(Level.INFO,"There are "+ r.getInt(1)+" rows in the ComponentExecution table with Component ID = "+cid);
			
			cs.releaseComponent(compName);
			// TODO: Check that the termination time has been filled in
		}
	}
		
	public void testLoadReleaseCplus() throws Exception {
		if (!runCplus) return;
		for (long i = 0; i < NUM_ITERATIONS; i++) {
			myMount = alma.MOUNT_ACS.MountHelper.narrow(cs.getComponent(mountComponent));
			cs.releaseComponent(mountComponent);
		}
	}

	protected void tearDown() throws Exception {
		advCS.disconnectManagerAdmin(adminOp);
		double avg = ((AdministratorCallBack)adminOp).getLoadStatistics().average;
		double rms = ((AdministratorCallBack)adminOp).getLoadStatistics().rms;
		double maxDeviation = ((AdministratorCallBack)adminOp).getLoadStatistics().maxDeviation;
		m_logger.log(Level.INFO, "Average time between loads: " + avg + "ms. rms: "+rms+" ms.");
		m_logger.log(Level.INFO, "Maximum time between loads: " + maxDeviation + " ms. in "+NUM_ITERATIONS+" iterations.");
		super.tearDown();
	}

}
