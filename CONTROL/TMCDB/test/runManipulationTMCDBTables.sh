#!/bin/bash

##############################################################################
# ALMA - Atacama Large Millimiter Array
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration),
# All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# $Id$
#

# Documentation about the test goes here.
#
#



declare TEST_CLASS=tmcdb/test/manipulationTMCDBTables.py
declare TEST_SUITE=ALL
declare TEST_LOG=./tmp/manipulationTMCDBTables.log
if test $# -ge 1; then
  TEST_SUITE=$1
  if test $# -eq 2; then
    TEST_LOG=$2
  fi
fi

#Check LOAD_FROM_XML flag:
if [ "$LOAD_FROM_XML" = "true" ] || [ "$LOAD_FROM_XML" = "y" ] || [ "$LOAD_FROM_XML" = "1" ]; then
    rm -fr TMCDB/ &> /dev/null
fi

sh start_hsqldb.sh &> /dev/null &
sleep 1
java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa ../src/SQL/CreateHsqldbTables.sql &> /dev/null

export ENABLE_TMCDB=1
export LOAD_FROM_XML=1
export TMCDB_CONFIGURATION_NAME='Test'
acsStart -noloadifr &> $TEST_LOG
unset ENABLE_TMCDB
unset LOAD_FROM_XML
unset TMCDB_CONFIGURATION_NAME

python $TEST_CLASS &> /dev/null
RESULT=$?
if [ "$RESULT" = "0" ]; then
    printf "OK\n"
else
    printf "ERROR\n"
fi

#Create a sql file to quit from hsqldb. Check if  already exists, then remove it anyway and create a new one.
if [ -s "tmpQuit.sql" ]; then
	rm tmpQuit.sql
	echo "shutdown;" >> tmpQuit.sql  
	echo "\q">> tmpQuit.sql
else
	echo "shutdown;" >> tmpQuit.sql  
	echo "\q">> tmpQuit.sql
fi

(acsStop && killACS && acsdataClean -all ) &> /dev/null
java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa tmpQuit.sql &> /dev/null
rm tmpQuit.sql
# __oOo__
