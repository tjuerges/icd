if test -a $ACSROOT/lib/hsqldb.jar; then
    export CLASSPATH=$ACSROOT/lib/hsqldb.jar:$CLASSPATH
elif test -a $INTROOT/lib/hsqldb.jar; then
    export CLASSPATH=$INTROOT/lib/hsqldb.jar:$CLASSPATH
fi

java org.hsqldb.Server -port 8081

