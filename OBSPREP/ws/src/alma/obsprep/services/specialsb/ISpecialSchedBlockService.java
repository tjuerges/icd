/*
 * Created on Jul 12, 2006 by mschilli
 */
package alma.obsprep.services.specialsb;

import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.acs.container.ContainerServices;
import alma.acs.entityutil.EntityException;
import alma.hla.runtime.obsprep.util.UnknownEntityException;
import alma.xmlentity.XmlEntityStruct;



/**
 * A service offered by ObsPrep to other subsystems: Creation of <i>Special SBs</i> in
 * the format understood by the Alma archive.
 * 
 * This class performs some necessary initializations, its
 * {@link #init(ContainerServices)} method needs to be called before the
 * rest of this package can be used.
 * 
 * @author mschilli
 */
public interface ISpecialSchedBlockService {

	/**
	 * Performs necessary initializations before {@link ISpecialSchedBlockBuilder}
	 * and {@link ISpecialProjectBuilder} can be used.
	 * 
	 * ContainerServices need to be provided, so the OT's datamodel logic (the business
	 * objects) can ask them for <code>EntityId</code>s.
	 * 
	 * It is possible to set system property "ServiceProvider" to "offline", and then pass
	 * a <code>null</code> to this method.
	 * 
	 * @param cs ContainerServices for creating EntityIds
	 */
	public void init (ContainerServices cs);

	/**
	 * Sets the logger used by the (de-)serialization logic
	 * in implementations of ISpecialSchedBlockBuilder and ISpecialProjectBuilder.
	 * 
	 * @param logger the logger used by the {@link ISpecialSchedBlockBuilder}s.
	 */
	public void setLogger (Logger logger);

	/**
	 * Creates an instance of {@link ISpecialSchedBlockBuilder} with default content,
	 * it is up to you to assign reasonable values to its fields afterwards.
	 */
	public ISpecialSchedBlockBuilder addEmptySchedBlock (ISpecialProjectBuilder x);

	/**
	 * Creates an instance of {@link ISpecialProjectBuilder} which is typically 
	 * a starting point for adding {@link ISpecialSchedBlockBuilder}s to it.
	 */
	public ISpecialProjectBuilder createEmptyProject ();

	
	/**
	 * Creates an instance of {@link ISpecialProjectBuilder} with content filled from the
	 * specified XmlEntityStruct.
	 * @throws EntityException 
	 * @throws ValidationException 
	 * @throws MarshalException 
	 */
	public ISpecialProjectBuilder materializeProject (XmlEntityStruct xes) throws MarshalException, ValidationException, EntityException;

	
	/**
	 * Creates an instance of {@link ISpecialSchedBlockBuilder} with content filled from the
	 * specified XmlEntityStruct.
	 * @throws EntityException 
	 * @throws ValidationException 
	 * @throws MarshalException
	 * @throws UnknownEntityException
	 */
	public ISpecialSchedBlockBuilder materializeSchedBlock (XmlEntityStruct xes) throws MarshalException, ValidationException,
			EntityException, UnknownEntityException;

}
