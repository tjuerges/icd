
package alma.obsprep.services.specialsb;


/**
 * A little helper class to encapsulate loading the implementation
 * class of the service interface defined here in Obsprep's ICD. This
 * may well be an interim solution, it might become consolidated in
 * some bigger class that deals with other services, too. 
 *
 * @author mschilli
 */
public class SpecialSchedBlockServiceBootstrap {
	
	
	/**
	 * Returns an implementation of the ISpecialSchedBlockService interface,
	 * provided it is available.
	 */
	public ISpecialSchedBlockService getSpecialSchedBlockService() throws IllegalStateException {
		try {

			ISpecialSchedBlockService service;
			String serviceImpl = "alma.obsprep.services.specialsb.SpecialSchedBlockService";
			service = (ISpecialSchedBlockService) Class.forName(serviceImpl).newInstance();
			return service;
			
		} catch (Exception exc) {
			throw new IllegalStateException("Can't start Special-SB service. Probably ObservingTool.jar not in classpath", exc);
		}
		
	}

	
}

