/*
 * Created on Jul 12, 2006 by mschilli
 */
package alma.obsprep.services.specialsb;

import alma.acs.entityutil.EntityException;
import alma.hla.runtime.obsprep.util.UnknownEntityException;
import alma.xmlentity.XmlEntityStruct;


/**
 * A "flat" view on a <i>Special SB Project</i> which supports conversion to/from the
 * archive database format.
 * 
 * @author mschilli
 */
public interface ISpecialProjectBuilder {

	/**
	 */
	public String getPi ();

	/**
	 */
	public void setPi (String pi);

	/**
	 */
	public String getProjectName ();

	/**
	 */
	public void setProjectName (String projectName);


	/** 
	 */
	public void addSchedBlock (ISpecialSchedBlockBuilder sb);

	/**
	 */
	public void removeSchedBlock (ISpecialSchedBlockBuilder sb);

	
	/** 
	 * To retrieve all SchedBlocks previously added with {@link #addSchedBlock(ISpecialSchedBlockBuilder)}.
	 */
	public ISpecialSchedBlockBuilder[] getSchedBlocks () throws UnknownEntityException;

	/**
	 * Returns all Ids of the SchedBlocks belonging to this Project.
	 * This is useful when loading entities from the database.
	 */
	public String[] getSchedBlockIds () throws EntityException;

	/**
	 * From this object, create an <code>XmlEntityStruct</code> that can be
	 * fed into the archive.
	 * 
	 * @return the content of this object, ready to be stored
	 */
	public XmlEntityStruct serialize () throws EntityException;

}
