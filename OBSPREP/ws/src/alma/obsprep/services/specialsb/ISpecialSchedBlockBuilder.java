/*
 * Created on Jul 12, 2006 by mschilli
 */
package alma.obsprep.services.specialsb;

import alma.acs.entityutil.EntityException;
import alma.xmlentity.XmlEntityStruct;


/**
 * A "flat view" on a <i>Special SB</i> which supports conversion to/from the archive
 * database format.
 * 
 * @author mschilli
 */
public interface ISpecialSchedBlockBuilder {

	
	/**
	 * 
	 */
	public boolean isStandardMode ();

	/**
	 * 
	 */
	public void setStandardMode (boolean standardMode);

	/**
	 * 
	 */
	public String getModeType ();

	/**
	 * 
	 */
	public void setModeType (String modeType);

	/**
	 * 
	 */
	public String getModeName ();

	/**
	 * 
	 */
	public void setModeName (String modeName);

	/**
	 * 
	 */
	public String getStartTime ();

	/**
	 * 
	 */
	public void setStartTime (String startTime);

	/**
	 * 
	 */
	public String getEndTime ();

	/**
	 * 
	 */
	public void setEndTime (String endTime);

	/**
	 * 
	 */
	public double getMarginSec ();

	/**
	 * 
	 */
	public void setMarginSec (double marginSec);

	/**
	 * 
	 */
	public String getCalendarId ();

	/**
	 * 
	 */
	public void setCalendarId (String calendarId);

	/**
	 * 
	 */
	public String getReqId ();

	/**
	 * 
	 */
	public void setReqId (String reqId);

	/**
	 * 
	 */
	public String getReason ();

	/**
	 * @param reason One of the constants ReservationParametersTReasonType.ABCDEF.toString()
	 * @see alma.entity.xmlbinding.schedblock.types.ReservationParametersTReasonType
	 */
	public void setReason (String reason);

	/**
	 * 
	 */
	public String getStaffMember ();

	/**
	 * 
	 */
	public void setStaffMember (String staffMember);

	/**
	 * 
	 */
	public String[] getResourceList ();

	/**
	 * 
	 */
	public void setResourceList (String[] resourceList);

	/**
	 * 
	 */
	public String getDescription ();

	/**
	 * 
	 */
	public void setDescription (String description);

	/**
	 * 
	 */
	public String getSummary ();

	/**
	 * 
	 */
	public void setSummary (String summary);

	/**
	 * From this object, create an <code>XmlEntityStruct</code> that can be
	 * fed into the archive.
	 * 
	 * @return the content of this object, ready to be stored
	 * @throws EntityException
	 */
	public XmlEntityStruct serialize () throws EntityException;

}
