import java.util.HashMap;
import java.util.logging.Logger;

import alma.entity.xmlbinding.schedblock.types.ReservationParametersTReasonType;
import alma.obsprep.services.specialsb.ISpecialProjectBuilder;
import alma.obsprep.services.specialsb.ISpecialSchedBlockBuilder;
import alma.obsprep.services.specialsb.ISpecialSchedBlockService;
import alma.obsprep.services.specialsb.SpecialSchedBlockServiceBootstrap;
import alma.xmlentity.XmlEntityStruct;


/**
 * This is Sample Code to demonstrate usage of the Special SB package.
 * 
 * When run, it will print out some time stamps for benchmarking purposes.
 */
public class Example1 {

	public static void main (String[] args) {
		try {
			example();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	private static void example () throws Exception {

		// Initialize the Service
		// -------------------------------
		time("started");
		
		System.setProperty("ServiceProvider", "offline");
		ISpecialSchedBlockService service = new SpecialSchedBlockServiceBootstrap().getSpecialSchedBlockService();
		service.setLogger(Logger.global);
		service.init(null);
		time("inited");

		// Create Project and SchedBlocks
		// -------------------------------

		ISpecialProjectBuilder prj = service.createEmptyProject();
		prj.setPi("pI");
		prj.setProjectName("projectName");

		for (int i = 0; i < 5; i++) {
			ISpecialSchedBlockBuilder inst = service.addEmptySchedBlock(prj);
			inst.setStandardMode(false);
			inst.setModeType("modeType");
			inst.setModeName("modeName");
			inst.setStartTime("startTime");
			inst.setEndTime("endTime");
			inst.setMarginSec(0.0);
			inst.setCalendarId("calendarId");
			inst.setReqId("reqId");
			inst.setReason(ReservationParametersTReasonType.REMOVAL.toString());
			inst.setStaffMember("staffMember");
			inst.setResourceList(new String[]{"resourceList"});
			inst.setDescription("description");
			time(i + " created");
		}

		// Serialize and save
		// -------------------------------
		Database database = new Database(); // database mock-up

		XmlEntityStruct xes = prj.serialize();
		String projectId = xes.entityId;
		database.save(xes);

		ISpecialSchedBlockBuilder[] sbs = prj.getSchedBlocks();
		for (int i = 0; i < sbs.length; i++) {
			xes = sbs[i].serialize();
			time(i + " serialized");

			database.save(xes);
		}


		// And, how to de-serialize
		// -------------------------------
		XmlEntityStruct loadedXes = database.load(projectId);
		ISpecialProjectBuilder loadedPrj = service.materializeProject(loadedXes);

		String[] loadedIds = loadedPrj.getSchedBlockIds();
		for (int i = 0; i < loadedIds.length; i++) {
			loadedXes = database.load(loadedIds[i]);
			ISpecialSchedBlockBuilder loadedSb = service.materializeSchedBlock(loadedXes);
			time(i + " deserialized");
		}

	}

	/**
	 * 
	 */
	private static class Database extends HashMap {

		void save (XmlEntityStruct xes) {
			put(xes.entityId, xes);
		}

		XmlEntityStruct load (String id) {
			return (XmlEntityStruct) get(id);
		}
	}
	
	/**
	 * 
	 */
	private static void time (String msg) {
		long now = System.currentTimeMillis();
		msg += (prev == 0) ? "" : ": " + (now - prev) + "ms";
		prev = now;
		System.out.println(msg);
	}	

	/**
	 * 
	 */
	private static long prev;
	
}