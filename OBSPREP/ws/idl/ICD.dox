/*! \mainpage 
 
 \htmlinclude header.html
 \n
 <H2> INDEX </H2>\n
 \n
 \li \ref one \n
 \li \ref two \n
 \li \ref three \n
 \li \ref four \n
 \li \ref five \n
 \li \ref six \n
 \li \ref seven \n
 \li \ref eight \n
 \li \ref nine \n
 \li \ref ten \n
 \li \ref eleven \n
 \li \ref twelve \n
 \n 

<HR>

 \section one Purpose

The purpose of the observation preparation system is to create and
manage the project descriptions. ObsPrep has the following tasks (and
interfaces): 
<ul>
<li>Creating, filling in and validating observing projects.</li>
<li>Storing, updating and retrieving observing projects.</li>
<li>Handling the review process for observing projects.</li>
</ul>
Some components of this subsystem run at the ALMA site, as servers,
the rest form part of the Observing Tool (OT). This document publishes
the interfaces for the server-side components, even though some are
intended for use just within this subsystem.

For further details on the design of the subsystem, see Observation
Preparation Subsystem Design Document, ALMA-70.60.00.00-001-H-GEN.

 \section two Namespace

For IDL interfaces the alma.obsprep.\<interface_name\>
namespace shall be used.

For XML schemas we will use the Alma/ObsPrep namespace. Note that this is a relative
namespace declaration where a suitable root has to be added when we
decide on how it should look (e.g. http://www.alma.int/alma).

 \section three Interfaces

The Observing Preparation subsystem implements the Component Life
Cycle interface for the
Server side components. The subsystem also implements two interfaces as part
of its Server Utilities package: Project Repository and
Characteristics. It is not clear that these will be used outside the
subsystem, but they will reside at ALMA server sites, and be publicly
accessible, for the distributed copies of the Observing Tool to use.

Note: The container will
handle authorization issues, so in the interface definitions below
although it is noted which methods should raise authorization
exceptions, these exceptions are not noted in the OCL.  

Candidate Components for this subsystem are: the Project Repository,
the SBFactory, the Characteristics and the OT itself. It is possible
that some others may exist within the OT.

The following interfaces are described here:
<ul>
<li>ProjectRepository \ref threeA</li>
<li>SBFactory \ref threeB</li>
<li>Characteristics \ref threeC</li>
</ul>

The following IDL definitions use the type names defined below.
<ul>
<li>	EntityId		(defined in ACS)
<li>	typedef sequence<EntityId> EntityIdSeq;
</ul>
<p>
The following objects are defined by the ObsPrep subsystem and are used in the IDL interfaces.
<ul>
<li>    typedef string EntitySearchSpec;
<li>    typedef string ValidationReport;
<li>    typedef xmlentity::XmlEntityStruct ObsProject;
<li>    typedef xmlentity::XmlEntityStruct ObsProposal;
<li>    typedef xmlentity::XmlEntityStruct SchedBlock;
<li>    typedef sequence <SchedBlock> SchedBlockSeq;
<li>    typedef sequence <ObsProject> ObsProjectSeq;
<li>    typedef string SBType;
<li>    typedef string BlockType;
<li>    typedef string CharacteristicsBlock;
</ul>
<p>
The following exceptions are defined and used in the IDL interfaces.
<ul>
<li><A HREF="exceptionProjectRepositoryIF_1_1CreationFailure.html">CreationFailure</A>Failed
to create.
<li><A HREF="exceptionProjectRepositoryIF_1_1NotFound.html">NotFound</A>Requested object was not found.
<li><A HREF="exceptionProjectRepositoryIF_1_1RepositoryInternalError.html">RepositoryInternalError</A>Internal error in the repository.
<li><A HREF="exceptionProjectRepositoryIF_1_1MalformedURI.html">MalformedURI</A>The URI given was malformed.</ul>

\subsection threeA Interface ProjectRepository
This interface is used by clients to store, update, retrieve and
validate Observing Projects.

See <A HREF="interfaceProjectRepositoryIF_1_1LightweightProjectRepository.html">ProjectRepositoryIF</A> documentation.

See the <A HREF="ProjectRepositoryIF_8idl-source.html">ProjectRepositoryIF</A> source code

\subsection threeB Interface SBFactory
This interface is used by clients to create and store
individual Scheduling Blocks as required. Note that all SBs must be
associated with a project, so an existing ObsProject is a required
argument. The SBType argument is a description of the type of
Scheduling Block to be created: "normal" (for normal observing SBs),
"maintenance", "interactive", "manual", VLBI", and any others that may
be defined.

See <A HREF="interfaceSBFactoryIF_1_1SBFactory.html">SBFactoryIF</A> documentation.

See the <A HREF="SBFactoryIF_8idl-source.html">SBFactoryIF</A> source code


\subsection threeC Interface Characteristics
This interface is used by the Observing Tool to validate its own
version, and to update the information contained in the Observatory
Characteristics package. It may also be used to store updated
information.

See <A HREF="interfaceCharacteristicsIF_1_1Characteristics.html">CharacteristicsIF</A> documentation.

See the <A HREF="CharacteristicsIF_8idl-source.html">CharacteristicsIF</A> source code


\section four Data types and Entities

Data models for the required objects are presented. All of these remain under
development at the detailed level, though the top level structure is
stable.

Currently the ObsProject related data models are being devloped as UML
data models, and then XML schema definitions are generated from
them. This is a development in progress, which is why it has not yet
been applied to the other objects. Thus for the ObsProject related
models the UML class diagrams are presented.


 \subsection fourA The ObsProject Object
This is the object that describes the top level Observing Project
entity. This includes references to the Scheduling Block object and
the Observing Proposal Object. An ObsProject document will vary in
size depending on the number of observations defined in the project; 
a minimum size would be approximately 5Kbytes. However, 
typical projects will be much larger.

Figure 1 The ObsProject object
\image  html ObsProject.png

<ul>
<li>	Size: 		300 kilobytes (range perhaps 10 to 2000 kilobytes)</li>
<li>	Data rate:
	<ul>
	<li>PI Submission:	5 per day, but rising to perhaps 200 per hour (near a deadline)</li>
	<li>By ObsProject Manager: 10 per day </li>
	</ul></li>
<li>	Indices:
	<ul>
	<li>EntityId</li>
	<li>ProjectName + Version</li>
	<li>PI</li>
	</ul></li>
</ul>

 \subsection fourB The ObsProposal Object
This object describes the Observing Proposal entity. It is not very
developed yet as the details of what will be required remain to be
defined. 

A minimum size for this will be around 1Kbyte, however, it could be
very much larger if images form part of the proposal, as would
frequently be the case. The size and frequency estimates given here
are necessarily rough, but are backed up by genuine information on a
set of VLT proposals kindly provided by ESO.

The ObsProposal object is also shown in Figure 1. There is much detail
still to be added to it.

<ul>
<li>	Size: 		1.5 megabytes (range perhaps 1 to 5000 kilobytes)</li>
<li>	Data rate:	2 per day, but rising to perhaps 200 per hour (near a deadline)</li>
<li>	Indices:
	<ul>
	<li>EntityId</li>
	<li>Title + Version</li>
	<li>PI</li>
	</ul></li>
</ul>

 \subsection fourC The System View Object : Scheduling Blocks and ObsUnitSets

This object represents the structure (in the OT) that contains all of
the Phase 2 observing information. In particular it contains the
the Scheduling Blocks and ObsUnitSets. 

The approximate characteristics of the Objects presented here are
included in the ObsProject estimate, with the exception of the
Scheduling Block tree, as that is a separate entity. The Scheduling
Block estimate are given in section \ref fourD.

Figure 3 The System View object
\image html  SystemView.png

 \subsection fourD The Scheduling Block Object

The details of the Scheduling Block object are included in the system
view at section \ref fourC.

In the size and rate estimates below note that the size of
SchedBlocks created by the OT are additional to the ObsProject size
quoted in section \ref fourA.

\image  YourXsd.gif

<ul>
<li>	Size: 		10 kilobytes</li>
<li>	Data rate:
	<ul>
	<li>Scheduler Use: 50 per day</li>
	<li>PI Submission: 100 per ObsProject submission</li>
	</ul></li>
<li>	Indices:
	<ul>
	<li>EntityId</li>
	<li>ObsProjectRef</li>
	</ul></li>
</ul>
.

 \subsection fourE The ObsUnit and ObsUnitSet Object
The ObsUnit and ObsUnitSet object are abstract types that allow
hierarchies describing complex observations to be built. The SB is a
type of ObsUnit and the ObsProgram is a type of ObsUnitSet, and their
diagrams above do include the definitions of ObsUnit and ObsUnitSet,
so they are not repeated here. The full definitions can be found in
the object definition files.


 \subsection fourF The Characteristics Schema
This section describes the entities outlined in section \ref elevenA that are
required by this subsystem. There are three main components, ALMA
Capabilities, Site Characteristics and Policies. Each of these is
outlined here only at the very top level. The details need further
work.

ALMA Capabilities is intended to hold descriptions of the current
capabilities of the ALMA telescope, including the Observing User
Guide. These capabilities may change over time (e.g. new observing
modes, new receivers, modified receivers).

Site Characteristics contains descriptions of the ALMA site, including
statistical environmental (weather, tau) information.

Policies contain descriptions of the current Observatory Policies that
are to be applied to observing. 

The Characteristics database, in the
form of an XML document, is likely to be distributed with the
Observing Tool, and thus not normally transmitted alone
whole. However, small updates may be transmitted to the OT as a patch
(i.e. modifications only) to the existing document.

Figure 6 The Characteristics Schema
\subsubsection fourF1 ALMA Capabilities
\image html  ALMACapabilities.png
<P>
\subsubsection fourF2 Site Characteristics
\image html  SiteCharacteristics.png
<p>
\subsubsection fourF3 Policies
\image html  Policies.png

<ul>
<li>	Size: 		100 kilobytes</li>
<li>	Data rate:	500 per semester? (1 per installed OT client)</li>
<li>	Indices:
	<ul>
	<li>EntityId</li>
	<li>Version</li>
	</ul></li>
</ul>


 \section five Published Events

This subsystem does not publish any events. 

NOTE: This may change as there may be a requirement for the ProjectRepository component to issue an event when updated ObsProjects are received and placed in the Archive, particularly in the case where Breakpoint Responses are included. The Scheduling subsystem would listen for this event. However, in the present design the Scheduling subsystem will poll the Archive for updated ObsProjects.

 \section six Received Events
This subsystem does not listen for any events.

 \section seven Published Streams
This subsystem does not publish any streams.

 \section eight Consumed Streams
This subsystem does not consume any streams.

 \section nine Dependencies on external data

\subsection nineA Control System API (provided by Control)
The Observing Tool will
require an API published by (and maintained by) the control
system. Also included within this API will be the APIs for the
standard observing scripts (see section \ref nineB) developed by staff
astronomers. 

This API will be published in the form of an XML
document, and distributed with the Observing Tool. Updates will be
obtained via the mechanism discussed in section \ref threeC. 

This XML document
is described in the Control Subsystem ICD section [TBC]. 

\subsection nineB  Observing Scripts (provided by SSR/ALMA staff?) 
The Observing Tool will require the actual observing scripts (the API
for which are included in section \ref nineA) so that they may be made
available to the expert user and the staff astronomers. 

These scripts
will be distributed with the Observing Tool. Updates will be obtained
via the mechanism described in section \ref threeC.

It will be possible to develop these scripts using the OT, or a
standard editor.

This is briefly described in the SSR Software Development Plan, section 1.1, item 3.

\subsection nineC Pipeline Scripts (provided by Pipeline) 
The interface to the pipeline subsystem is not well
understood. However, the current baseline is that Observing Projects
will allow the specification of a pipeline reduction recipe to be
inserted at various points in the ObsUnitSet hierarchy. Although in
many cases this recipe may be already in the template, or
automatically chosen, there should be provision for the expert user to
specify his or her own recipe. 

To facilitate this the Pipeline subsystem should provide a list
(library) of recipes, with brief descriptions and any other associated
information. This library should be published in the form of an XML
document, and distributed with the Observing Tool. Updates will be
obtained via the mechanism described in section \ref threeC.

\subsection nineD Characteristics Data (provided by various subsystems?)

The information stored in the Characteristics repositories will need
to be entered by ALMA staff and/or SSR team members. The Control
System API and Observing Scripts are two examples, highlighted in this
section because of their importance. However, other information must
also be supplied, as indicated in section \ref elevenA. Much of this will be
relatively static information, and it is expected that XML editors
could be used to enter the information in the correct structure. Note:
some data may be encoded in XML in a "bulk" form: e.g. the control
system API (more natural to define in python), and tabular information
on, for instance, atmospheric transparency (there is no value added to
ALMA by converting it - but if an external body does the work then we
will use it).

 \section ten ACS Dependencies

\subsection tenA Assign Unique Entity ID (provided by ACS) 
Used to assign unique IDs for Observing Projects (and SBs).

This has been available since ACS 2.0.1p2.

\subsection tenB Web access (provided by ACS) 
The ACS should provide a common means of using standard web protocols
(http, SOAP, ftp, etc.) to be used by this subsystem and others, for
accessing web services such as image repositories and object and line
catalogs provided by ALMA or External bodies. (See section \ref elevenE). If it
transpires that only this subsystem requires these communication
protocols then perhaps it can be handled internally. 

No reference
found in ACS documents [TBC]. 

\subsection tenC Portable "ACSLite" 
The parts of the ACS used by the OT will need to run on Operating
Systems other than Linux, specifically flavours of Windows and MacOS,
perhaps others. 

Note that this subsystem already depends on the
following parts (in addition to AssignUniqueEntityID): 

<ul>
<li>Logging service;</li>
<li>Java container activation;</li>
<li>XML marshalling;</li>
</ul>
This is covered in
the ALMA Common Software Architecture, section 2.3.

Availabilty of this "ACSLite" is almost complete in ACS 3.1.

 \section eleven Required interfaces and subsystems

\subsection elevenA Characteristics (provided by this subsystem and others) 

Although an interface to the Characteristics database
is provided by this subsystem and is described above (section \ref
threeC), the responsibility for creating the database may be taken on
by others, but with this subsystem providing information.

Used
to check for and retrieve updates to the Observing Tool (and perhaps
to store new templates created by operations staff). Minor updates to
the Observing Tool can be automatically retrieved; major updates may
direct the user to a download URL. 

The Observing Tool will rely on a
variety of information about the ALMA Observatory. This information
will be distributed with a new, complete, version of the OT (typically
in advance of proposal submission deadlines) but for minor updates it
is desired to provide only modified information. This will keep the
information in the OT, and thus the Observing Projects it creates, up
to date. 

Typical pieces of information are described in section \ref fourF.

Another service that may be provided by this interface will be
the uploading of new versions of these data. A possible implementation
for delivering the updates is the Java Web Start tool. 

\subsection elevenB Simulator (provided by offline DR) 
Used (by the OT) to request the full
simulation of the data that a project would produce. The OT requests
the simulation, but will not receive, display or analyze the
results. This is described in the Offline Subsystem ICD, section 3.

\subsection elevenC User Manager (provided by Executive) 
Used to create and verify user ids, and to establish which rights the
user has. This is described in the Executive Subsystem ICD, sections
3.1 to 3.3. 

\subsection elevenD XMLStore (provided by Archive) 
Used to provide persistent storage of XML entities. This interface
will be used by the server side of ObsPrep (the ProjectRepository),
probably via the Data Access Object. This is described in the
Archiving Subsystem, ICD, section 3.1. 

This subsystem already depends
on the MicroArchive, the interim Archive. But it should be noted that
we intend to use the MicroArchive in our delivered software, as part
of the local repository for the deployed OT.

\subsection elevenE Correlator Validator (provided by Correlator)
The Correlator subsystem is expected to provide a static correlator
configuration validator which the OT will use to validate correlator
setups. This should be a Java component that can be distributed with
the OT client, as well as used on the server-side with the
ObsProjectRepository. 

This interface is described in the Correlator Subsystem ICD, Section
3.7, "Configuration Validator".

\subsection elevenF External Repositories (external to project) 
The Observing Tool will depend on a number of repositories provided by
external sites. Typically these may be source catalogs (e.g. IRAS),
image repositories (e.g. Digital Sky Survey) or line catalogs
(e.g. JPL). These will be accessed via the web. This does not preclude
copies being distributed with the Observing Tool, subject to size and
any copyright/licensing issues. Wrappers for the http communications
may be provided by ACS, especially if these communication protocols
are required by more than one subsystem.

 \section twelve References
 \subsection twelveA Other Documents
<p>
The following docuemnts are useful in understanding the full context of this interface document.
<ol>
<li> Observation Preparation Subsystem Design Document in <a href="http://almaedm.tuc.nrao.edu/forums/alma/dispatch.cgi/SubsystemDesign">http://almaedm.tuc.nrao.edu/forums/alma/dispatch.cgi/SubsystemDesign</a>
<li> ACS Subsystem Design
<li> Scheduling Subsystem ICD
<li> Control Subsystem ICD
<li> Archive Subsytem ICD
</ol>

 \subsection twelveB The ProjectRepository IDL File 
 \include ProjectRepositoryIF.idl
 \subsection twelveC The SBFactory IDL File 
 \include SBFactoryIF.idl
 \subsection twelveD The Characteristics IDL File 
 \include CharacteristicsIF.idl

 \include YourEntireXMLFile.xml

*/
