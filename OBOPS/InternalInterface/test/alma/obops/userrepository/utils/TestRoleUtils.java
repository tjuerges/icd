/**
 * TestRoleDetailsComposer.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import junit.framework.TestCase;
import alma.obops.userrepository.domain.Role;
import alma.obops.userrepository.ldap.LdapRole;

/**
 * @author amchavan, Mar 17, 2011
 * @version $Revision: 1.2 $
 */

// $Id: TestRoleUtils.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class TestRoleUtils extends TestCase {
	
    public void testDiff() {
        Role aa = new LdapRole( "a", "a" );
        Role ab = new LdapRole( "a", "b" );
        Role ac = new LdapRole( "a", "c" );
        Role ba = new LdapRole( "b", "a" );
        Role bb = new LdapRole( "b", "b" );
        Role bc = new LdapRole( "b", "c" );
        
        Role aa2 = new LdapRole( "a", "a" );    // to make sure equal() works

        Set<Role> a = new HashSet<Role>();
        a.add( aa );
        a.add( ab );
        a.add( ac );
        a.add( ba );
        a.add( bb );
        a.add( bc );
        
        Set<Role> b = new HashSet<Role>();
        b.add( aa2 );
        b.add( ab );
        b.add( ac );
        b.add( ba );
        b.add( bb );
        b.add( bc );
        
        Set<Role> diff = RoleUtils.diff( a, b );
        assertNotNull( diff );
        assertEquals( 0, diff.size() );
        
        b.remove( ba );
        diff = RoleUtils.diff( a, b );
        assertNotNull( diff );
        assertEquals( 1, diff.size() );
        assertTrue( diff.contains( ba ));

        b.remove( bb );
        diff = RoleUtils.diff( a, b );
        assertNotNull( diff );
        assertEquals( 2, diff.size() );
        assertTrue( diff.contains( ba ));
        assertTrue( diff.contains( bb ));

        b.remove( bc );
        diff = RoleUtils.diff( a, b );
        assertNotNull( diff );
        assertEquals( 3, diff.size() );
        assertTrue( diff.contains( ba ));
        assertTrue( diff.contains( bb ));
        assertTrue( diff.contains( bc ));
    }
    
    public void testRoleSetConversions() {
    	
        Role aa = new LdapRole( "a", "a" );
        Role ab = new LdapRole( "a", "b" );
        Role ac = new LdapRole( "a", "c" );
        Role ba = new LdapRole( "b", "a" );
        Role bb = new LdapRole( "b", "b" );
        Role bc = new LdapRole( "b", "c" );

        Set<Role> roles = new TreeSet<Role>();
        roles.add( aa );
        roles.add( ab );
        roles.add( ac );
        roles.add( ba );
        roles.add( bb );
        roles.add( bc );
        
        String[] roleNames = RoleUtils.convertToRoleArray(roles);
        assertEquals( 6, roleNames.length );
        assertEquals( "a/a", roleNames[0] );
        assertEquals( "a/b", roleNames[1] );
        assertEquals( "a/c", roleNames[2] );
        assertEquals( "b/a", roleNames[3] );
        assertEquals( "b/b", roleNames[4] );
        assertEquals( "b/c", roleNames[5] );
        
        roles = RoleUtils.convertToRoleSet(roleNames);
        assertEquals( 6, roles.size() );
        assertTrue( roles.contains( aa ));
        assertTrue( roles.contains( ab ));
        assertTrue( roles.contains( ac ));
        assertTrue( roles.contains( ba ));
        assertTrue( roles.contains( bb ));
        assertTrue( roles.contains( bc ));
    }

	public void testRoleSetConversions2() {
		
	    Role aa = new LdapRole( "a", "a" );
	    Role ab = new LdapRole( "a", "b" );
	    Role ac = new LdapRole( "a", "c" );
	    Role ba = new LdapRole( "b", "a" );
	    Role bb = new LdapRole( "b", "b" );
	    Role bc = new LdapRole( "b", "c" );
	
	    Set<Role> roles = new TreeSet<Role>();
	    roles.add( aa );
	    roles.add( ab );
	    roles.add( ac );
	    roles.add( ba );
	    roles.add( bb );
	    roles.add( bc );
	    
	    String roleNames = RoleUtils.convertToRoleString(roles);
	    assertEquals( 6, roleNames.split(RoleUtils.ROLES_SEPARATOR).length );
	    assertTrue( roleNames.contains( "a/a" ));
	    assertTrue( roleNames.contains( "a/b" ));
	    assertTrue( roleNames.contains( "a/c" ));
	    assertTrue( roleNames.contains( "b/a" ));
	    assertTrue( roleNames.contains( "b/b" ));
	    assertTrue( roleNames.contains( "b/c" ));
	    
	    roles = RoleUtils.convertToRoleSet(roleNames);
	    assertEquals( 6, roles.size() );
	    assertTrue( roles.contains( aa ));
	    assertTrue( roles.contains( ab ));
	    assertTrue( roles.contains( ac ));
	    assertTrue( roles.contains( ba ));
	    assertTrue( roles.contains( bb ));
	    assertTrue( roles.contains( bc ));
	}
}
