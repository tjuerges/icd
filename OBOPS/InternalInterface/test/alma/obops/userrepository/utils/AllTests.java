/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.utils;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Mar 17, 2011
 * @version $Revision: 1.2 $
 */

// $Id: AllTests.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( AllTests.class.getName() );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestRoleUtils.class );
        //$JUnit-END$
        return suite;
    }

}
