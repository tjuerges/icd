/**
 * TestInstitutionsTable.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.io.IOException;
import java.net.URL;

import junit.framework.TestCase;

import org.apache.xmlrpc313.XmlRpcException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import alma.obops.userrepository.domain.Institution;
import alma.obops.userrepository.domain.InstitutionTable;
import alma.obops.utils.TestUtilities;

/**
 * @author amchavan, Oct 25, 2010
 * @version $Revision$
 */

// $Id$

public class TestInstitutionTable extends TestCase {
    
    private static final String DATADIR = "alma/obops/userrepository/testdata/";
    private static final String ALMOST_EMPTY = DATADIR + "almost-empty.csv";
    private static final String EMPTY = DATADIR + "empty.csv";
    private static final String INSTITUTE_HEADERS = DATADIR + "institute-headers.csv";
    private static final String INSTITUTES_DATA = DATADIR + "institutes.csv";
    private static final String INSTITUTES_DATA_UTF_8Y = DATADIR + "institutes-utf-8y.csv";

    public void setUp() {
        if( ! TestUtilities.isCanonicalTestDir() ) {
            fail( "This test should be run from " + TestUtilities.CANONICAL_TEST_DIR );
        }
    }
    
    /**
     * Simple implementation of ErrorHandler, calls <code>fail()</code> if
     * anything happens
     */
    public class JUnitErrorHandler implements ErrorHandler {
        
        public void error(SAXParseException e) throws SAXException {
            fail( getMessage( e ));
        }
        
        public void fatalError(SAXParseException e) throws SAXException {
            fail( getMessage( e ));
        }

        private String getMessage( SAXParseException e ) {
            String ret = "line " + e.getLineNumber() + ", " +
                         "col " + e.getColumnNumber() + ": " +
                         e.getMessage();
            return ret;
        }

        public void warning( SAXParseException e ) throws SAXException {
            fail( getMessage( e ));
        }
    }

    // Queries: return all countries
    public void testGetCountries00() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        assertEquals( 0, table.getCountries().length );
        
        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );

        assertEquals( 0, table.getCountries().length );
        table.load( institutes );
        String[] countries = table.getCountries();
        assertEquals( 9, countries.length );
        
        InstitutionTable t0 = table.queryByCountry( "USA" );
        assertEquals( 1, t0.getCountries().length );
    }

    private URL getURL( String resourceName ) {
        ClassLoader cl = this.getClass().getClassLoader();
        URL url = cl.getResource( resourceName );
        return url;
    }

    // Queries: return all states of a country
    public void testGetStates00() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        assertEquals( 0, table.getStates( "" ).length );

        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );
        
        String[] ukStates = table.getStates( "United Kingdom" );
        assertEquals( 2, ukStates.length ); 
        assertEquals( Institution.NO_NAME_STATE, ukStates[0] );
        assertEquals( "Cambridgeshire",          ukStates[1] );
        
        assertEquals( 1, table.getStates( "USA" ).length );
        assertEquals( 0, table.getStates( "France" ).length );
    }
    
    // very basic load test
    public void testLoad00() throws IOException {

        URL institutes = getURL(INSTITUTES_DATA);
        assertNotNull( institutes );
        InstitutionTable table = new InstitutionTable();
        table.load( institutes );
        assertEquals( 14, table.size() );
        Institution i = table.get( 4 );
        assertNotNull( i );

        assertEquals( "University of Cambridge", i.getName1() );
        assertEquals( "United Kingdom", i.getCountry() );
        assertEquals( "eu", i.getExecutive() );
        assertEquals( 1004, i.getSiblingNo().intValue() );
        assertEquals( "", i.getState() );
    }
    
    // very basic load test
    public void testLoad00_UTF_8Y() throws IOException {

        URL institutes = getURL( INSTITUTES_DATA_UTF_8Y );
        assertNotNull( institutes );
        InstitutionTable table = new InstitutionTable();
        table.load( institutes );
        assertEquals( 14, table.size() );
        Institution i = table.get( 4 );
        assertNotNull( i );

        assertEquals( "University of Cambridge", i.getName1() );
        assertEquals( "United Kingdom", i.getCountry() );
        assertEquals( "eu", i.getExecutive() );
        assertEquals( 1004, i.getSiblingNo().intValue() );
        assertEquals( "", i.getState() );
    }
    
    // load test, make sure we have deal with all valid headers
    public void testLoad01() throws IOException {

        URL institutes = getURL( INSTITUTE_HEADERS );
        assertNotNull( institutes );
        InstitutionTable table = new InstitutionTable();
        table.load( institutes );
        assertEquals( 0, table.size() );
    }
    
    // "bad load" test: no headers
    public void testLoadBad00() throws IOException  {
        
        URL institutes = getURL( EMPTY );
        assertNotNull( institutes );
        InstitutionTable table = new InstitutionTable();
        try {
            table.load( institutes );
            fail( "Expected RuntimeException" );
        }
        catch( RuntimeException e ) {
            assertTrue( e.getMessage().contains( "no column headers" ));
        }
    }
    
    // "bad load" test: missing required header
    public void testLoadBad01() throws IOException {
        
        URL institutes = getURL( ALMOST_EMPTY );
        assertNotNull( institutes );
        InstitutionTable table = new InstitutionTable();
        try {
            table.load( institutes );
            fail( "Expected RuntimeException" );
        }
        catch( RuntimeException e ) {
            assertTrue( e.getMessage().contains( "Required header" ));
        }
    }

    // Queries: country queries should return nothing
    public void testQueryByCountry00() throws Exception {
        InstitutionTable table = new InstitutionTable();
        InstitutionTable ret0 = table.queryByCountry( "" );
        assertEquals( 0, ret0.size() );

        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );
        InstitutionTable ret1 = table.queryByCountry( "Bogus country" );
        assertEquals( 0, ret1.size() );
    }
    
    // Queries: country queries
    public void testQueryByCountry01() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );

        InstitutionTable ret1 = table.queryByCountry( "USA" );
        assertEquals( 3, ret1.size() );
        InstitutionTable ret2 = table.queryByCountry( "France" );
        assertEquals( 1, ret2.size() );
    }
    
    // Queries: country + state queries
    public void testQueryByCountryState00() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );
        
        InstitutionTable ret3 = table.queryByCountryState( "USA", "CA" );
        assertEquals( 3, ret3.size() );   
        InstitutionTable ret4 = table.queryByCountryState( "United Kingdom", "Cambridgeshire" );
        assertEquals( 1, ret4.size() );
        InstitutionTable ret7 = table.queryByCountryState( "United Kingdom", Institution.NO_NAME_STATE );
        assertEquals( 1, ret7.size() );
        InstitutionTable ret5 = table.queryByCountryState( "France", null );
        assertEquals( 1, ret5.size() );
        InstitutionTable ret6 = table.queryByCountryState( "France", "Normandie" );
        assertEquals( 0, ret6.size() );
    }
    
    // Queries: queries should return something
    public void testQueryByCountryStateName00() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );

        InstitutionTable t;
        t = table.queryByCountryStateName( "USA", "CA", "^U" );
        assertEquals( 2, t.size() );
        t = table.queryByCountryStateName( "USA", null, "Berkeley" );
        assertEquals( 3, t.size() );
        t = table.queryByCountryStateName( "France", "", "M$" );
        assertEquals( 1, t.size() );
        t = table.queryByCountryStateName( "United Kingdom", 
                                           Institution.NO_NAME_STATE, 
                                           "^Univer" );
        assertEquals( 1, t.size() );
    }

    // Queries: queries should return something
    public void testQueryById00() throws Exception {
        
        InstitutionTable table = new InstitutionTable();
        URL institutes = getURL( INSTITUTES_DATA );
        assertNotNull( institutes );
        table.load( institutes );
    
        Institution t;
        t = table.queryById( -1 );
        assertNull( t );
        t = table.queryById( 1006 );
        assertNotNull( t );
        assertEquals( "France", t.getCountry() );
    }


    public void testAddUserInstitution() throws XmlRpcException {
        InstitutionTable table = new InstitutionTable();
        long instNo1 = table.addUserInstitution( "amchavan", "ESO Alma" );
        assertTrue( instNo1 < 0 );
        long instNo2 = table.addUserInstitution( "paul", "ESO Vitacura" );
        assertTrue( instNo2 < 0 );
        
        assertTrue( instNo1 != instNo2 );
        
        Institution result; 
        
        result = table.queryById( instNo1 );
        assertNotNull( result ); 
        assertEquals( "ESO Alma", result.getName1() );
        assertEquals( "amchavan", result.getRecorder() );
        
        result = table.queryById( instNo2 );
        assertNotNull( result ); 
        assertEquals( "ESO Vitacura", result.getName1() );
        assertEquals( "paul", result.getRecorder() );
        
    }
//    // XML generation
//    // Make sure we generate an *empty* XML document that conforms to
//    // alma.obops.institutions.InstitutionTable.SCHEMA_FILE
//    public void testXml00() throws Exception {
//        
//        InstitutionTable table = new InstitutionTable();
//        String xml = table.toXML();
//        ClassLoader loader = this.getClass().getClassLoader();
//        URL schema = loader.getResource( SCHEMA_FILE );
//        XmlUtils.validate( xml, schema, new JUnitErrorHandler() );
//    }
//    
//    // XML generation
//    // Make sure we generate an XML document that conforms to
//    // alma.obops.institutions.InstitutionTable.SCHEMA_FILE
//    public void testXml01() throws Exception {
//
//        Class<? extends TestInstitutionTable> clasz = this.getClass();
//        URL institutes = clasz.getResource( "data/institutes.csv" );
//        assertNotNull( institutes );
//        InstitutionTable table = new InstitutionTable();
//        table.load( institutes );
//        
//        String xml = table.toXML();
//        URL schema = clasz.getClassLoader().getResource( SCHEMA_FILE );
//        XmlUtils.validate( xml, schema, new JUnitErrorHandler() );
//    }
}
