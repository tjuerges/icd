/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * TODO
 *
 * @author amchavan, Nov 9, 2010
 * @version $Revision: 1.2 $
 */

// $Id: AllTests.java,v 1.2 2011/02/03 10:28:57 rkurowsk Exp $

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( AllTests.class.getName() );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestInstitutionTable.class );
        suite.addTestSuite( TestInstitution.class );
        //$JUnit-END$
        return suite;
    }

}
