/**
 * TestInstitutionsTable.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.util.HashMap;

import junit.framework.TestCase;
import alma.entity.xmlbinding.obsproposal.types.InvestigatorTAssociatedArcType;

/**
 * @author amchavan, Oct 26, 2010
 * @version $Revision: 1.4 $
 */

// $Id: TestInstitution.java,v 1.4 2011/04/15 15:04:51 fjulbe Exp $

public class TestInstitution extends TestCase {

    public void testToHashMap() {
        Institution i = new Institution();
        i.setInstNo( 1L );
        i.setSiblingNo( i.getInstNo() );
        i.setName1( "n" );
        i.setCountry( "c" );
        i.setState( "s" );
        i.setExecutive( "x" );
        i.setStreet( "a" );

        HashMap<String,Object> hmap = i.toHashMap();
        assertEquals( 7, hmap.size() );

        assertTrue( hmap.containsKey( "instNo" ));
        assertTrue( hmap.containsKey( "siblingNo" ));
        assertTrue( hmap.containsKey( "name1" ));
        assertTrue( hmap.containsKey( "country" ));
        assertTrue( hmap.containsKey( "state" ));
        assertTrue( hmap.containsKey( "executive" ));
        assertTrue( hmap.containsKey( "street" ));
        
        // make sure we do not send out Longs, Python doesn't support them
        assertTrue( hmap.get( "instNo" ) instanceof Integer );
        assertTrue( hmap.get( "siblingNo" ) instanceof Integer );
    }
    
    public void testGetDefaultArc() {

        String[] defArcs;
        Institution inst = new Institution();
        
        inst.setExecutive( InvestigatorTAssociatedArcType.EA.toString() );
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 1, defArcs.length );
        assertEquals( AlmaRegionalCenter.EA.name().toLowerCase(), defArcs[0] );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.EU.toString() );
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 1, defArcs.length );
        assertEquals( AlmaRegionalCenter.EU.name().toLowerCase(), defArcs[0] );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.NA.toString() );
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 1, defArcs.length );
        assertEquals( AlmaRegionalCenter.NA.name().toLowerCase(), defArcs[0] );
        
        inst.setExecutive( "ea/na" ); // No InvestigatorTAssociatedArcType for that
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 2, defArcs.length );
        assertEquals( AlmaRegionalCenter.EA.name().toLowerCase(), defArcs[0] );
        assertEquals( AlmaRegionalCenter.NA.name().toLowerCase(), defArcs[1] );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.CL.toString() );
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 0, defArcs.length );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.OTHER.toString() );
        
        defArcs = Institution.getDefaultARCs( inst );
        assertNotNull( defArcs );
        assertEquals( 0, defArcs.length );
    }
    
    public void testHasRealExecutive() {
        Institution inst = new Institution();
        
        inst.setExecutive( InvestigatorTAssociatedArcType.EA.toString() );
        assertTrue( inst.hasRealExecutive() );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.EU.toString() );
        assertTrue( inst.hasRealExecutive() );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.NA.toString() );
        assertTrue( inst.hasRealExecutive() );
        
        inst.setExecutive( "ea/na" ); // No InvestigatorTAssociatedArcType for that
        assertFalse( inst.hasRealExecutive() );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.CL.toString() );
        assertTrue( inst.hasRealExecutive() );
        
        inst.setExecutive( InvestigatorTAssociatedArcType.OTHER.toString() );
        assertTrue( inst.hasRealExecutive() );
    }
    
    
//    // XML generation
//    public void testXml00() throws IOException {
//        
//        Institution i = new Institution();
//        i.setInstNo( "1" );
//        i.setName1( "n" );
//        i.setCountry( "c" );
//        i.setState( "s" );
//        i.setExecutive( "x" );
//        i.setStreet( "a" );
//        String xml = "<institution><id>1</id>" +
//              "<name>n</name>" +
//              "<country>c</country>" +
//              "<state>s</state>" +
//              "<exec>x</exec>" +
//              "<address>a</address></institution>";
//        assertEquals( xml, i.toXML() );
//    }
//    
//    // XML generation, escaping of XML-like tags, e.g.
//    // Byurakan Astrophysical Observatory (BAO)<NL>Armenian National Academy of Sciences
//    public void testXml01() throws IOException {
//        
//        Institution i = new Institution();
//        i.setId( "1" );
//        i.setName( "name1<NL>name2" );
//        i.setCountry( "c" );
//        i.setState( "s" );
//        i.setExecutive( "x" );
//        i.setPostalAddress( "a" );
//        String xml = "<institution><id>1</id>" +
//                "<name>name1&lt;NL&gt;name2</name>" +
//                "<country>c</country>" +
//                "<state>s</state>" +
//                "<exec>x</exec>" +
//                "<address>a</address></institution>";
//        assertEquals( xml, i.toXML() );
//    }
//    
//    // XML generation, escaping of ampersand chars, e.g.
//    // Centre for Astrophysics & Supercomputing
//    public void testXml02() throws IOException {
//        
//        Institution i = new Institution();
//        i.setId( "1" );
//        i.setName( "name1&name2" );
//        i.setCountry( "c" );
//        i.setState( "s" );
//        i.setExecutive( "x" );
//        i.setPostalAddress( "a" );
//        String xml = "<institution><id>1</id>" +
//                "<name>name1&amp;name2</name>" +
//                "<country>c</country>" +
//                "<state>s</state>" +
//                "<exec>x</exec>" +
//                "<address>a</address></institution>";
//        assertEquals( xml, i.toXML() );
//    }
    
}
