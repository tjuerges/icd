package alma.obops.userrepository;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {

        TestSuite suite = new TestSuite( "Test for alma.obops.userrepository" );

        suite.addTest( alma.obops.userrepository.domain.AllTests.suite() );
        suite.addTest( alma.obops.userrepository.xmlrpc.AllTests.suite() );
        suite.addTest( alma.obops.userrepository.utils.AllTests.suite() );

        //$JUnit-BEGIN$
        suite.addTestSuite( TestUserRepository.class );
        //$JUnit-END$
        
        return suite;
    }
}
