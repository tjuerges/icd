package alma.obops.userrepository;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * NOT A UNIT TEST: this is a simple example of how to do an LDAP query, using
 * only low-level javax.naming classes and constants; written to understand a
 * bit how it all works.
 * 
 * @author amchavan, Oct 28, 2010
 * @version $Revision: 1.3 $
 */

// $Id: SimpleQuery.java,v 1.3 2011/02/23 10:23:45 rkurowsk Exp $

public class SimpleQuery {

    private static final String SUN_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    private static final String LDAP_NGAS01 = "ldap://ngas01.hq.eso.org:389";
    private static final String USER_DN = "uid=readUser,ou=master,dc=alma,dc=info";
    private static final String PASSWORD = "oS!iMLDAP!";
//    private static final String USER_DN = "uid=admin,ou=people,ou=master,dc=alma,dc=info";
//    private static final String PASSWORD = "admin";

    public static void main( String[] ignored ) throws NamingException {

        // Configure the LDAP connection
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put( Context.INITIAL_CONTEXT_FACTORY, SUN_CTX_FACTORY );
        env.put( Context.PROVIDER_URL, LDAP_NGAS01 );
        env.put( Context.SECURITY_PRINCIPAL, USER_DN );
        env.put( Context.SECURITY_CREDENTIALS, PASSWORD );
        DirContext context = new InitialDirContext( env );

        // Configure the search itself 
        SearchControls sarchCtrl = new SearchControls();
        sarchCtrl.setSearchScope( SearchControls.SUBTREE_SCOPE );

        // What do we want to search on?
        String BASE = "ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info";
        String FILTER = "cn=DSOA";
        search( BASE, FILTER, context, sarchCtrl );
    }

    private static void search( String base, 
                                String filter, 
                                DirContext context, 
                                SearchControls ctrl )
            throws NamingException {

        // perform the search
        NamingEnumeration<SearchResult> results = context.search( base, 
                                                                  filter, 
                                                                  ctrl );
        
        // loop over the result set
        while( results.hasMore() ) {
            
            // retrieve all the attributes of the entry, then loop over them
            SearchResult result = (SearchResult) results.next();
            Attributes attributes = result.getAttributes();
            NamingEnumeration<? extends Attribute> nEnum = attributes.getAll();
            while( nEnum.hasMore() ) {
                Attribute attribute = nEnum.next();
                String name = attribute.getID();
                NamingEnumeration<?> values = attribute.getAll();
                while( values.hasMore() ) {
                    Object value = values.next();
                    System.out.println( name + ": " + value );
                }
            }
        }
    }
}