/**
 * TestPortalUserRepositoryFactory.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository;

import java.io.File;
import java.util.List;

import junit.framework.TestCase;
import alma.obops.userrepository.UserRepositoryFactory.Backend;
import alma.obops.userrepository.domain.Role;
import alma.obops.userrepository.domain.User;
import alma.obops.userrepository.ldap.LdapUser;
import alma.obops.utils.TestUtilities;

/**
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.7 $
 */

// $Id: TestUserRepository.java,v 1.7 2011/03/09 15:19:28 fjulbe Exp $

public class TestUserRepository extends TestCase {
    
    private static final String LATIN_LAST_NAME = "Müller";
    private static final String LATIN_FIRST_NAME = "Françoise";
    private static final String FIRST_NAME = "F. N.";
    private static final String SURNAME = "Surname";
    private static final String className = TestUserRepository.class.getSimpleName();
    
    public void setUp() {
        if( !TestUtilities.isCanonicalTestDir() ) {
            fail( TestUtilities.CANONICAL_DIR_MSG );
        }
        
        File here = new File( "." );
        final String ACSDATA = "ACS.data";
        System.setProperty( ACSDATA, here.getAbsolutePath() );
    }

    // Example usage of read/write features using Latin-1 characters
    public void testLatinWriteExample() throws UserRepositoryEx {
        
        UserRepositoryFactory f = new UserRepositoryFactory();
        UserRepository urep = f.getReadWrite( Backend.LDAP, "admin", "admin" );
        
        // Create a new user
        //-----------------------------------------------
        final String uid = TestUtilities.newID( className );
        final String fullName = LATIN_FIRST_NAME + " " + LATIN_LAST_NAME;
        User p = new LdapUser();
        p.setUid( uid );
        p.setFullName( fullName );
        p.setSurname( LATIN_LAST_NAME );
        urep.create( p );
        
        User found = urep.findUser( uid );
        assertNotNull( found );
        assertEquals( fullName, found.getFullName() );
        assertEquals( LATIN_LAST_NAME,  found.getSurname() );
        
        urep.delete( uid );
    }

    // Example usage of read-only features
    public void testReadOnlyExample() throws UserRepositoryEx {
        UserRepositoryFactory f = new UserRepositoryFactory();
        UserRepository urep = f.getReadOnly( Backend.LDAP );

        // print out all user IDs in the repository
        List<String> uids = urep.listUsers();
        for( String uid : uids ) {
            System.out.println( uid );
        }

        // Lookup a single user and print out their details
        String uid = "amchavan";
        if( urep.exists( uid )) {
            User found = urep.findUser( uid );
            System.out.println( "UID:         " + found.getUid() );
            System.out.println( "Full name:   " + found.getFullName() );
            System.out.println( "Description: " + found.getDescription() );
            System.out.println( "E-mail addr: " + found.getEmail() );
            System.out.println( "Executive:   " + found.getExecutive() );
            System.out.println( "Pref. ARC:   " + found.getPreferredArc() );
            System.out.println( "Inst. ID:    " + found.getInstitutionID() );
            System.out.print  ( "Roles:       " );
            for( Role role : found.getRoles() ) {
                System.out.print( role + " " );
            }
            System.out.println();
        }
    }

    // Example usage of read/write features
    public void testReadWriteExample() throws UserRepositoryEx {
        
        UserRepositoryFactory f = new UserRepositoryFactory();
        UserRepository urep = f.getReadWrite( Backend.LDAP, "admin", "admin" );
        
        // Create a new user
        //-----------------------------------------------
        final String uid = TestUtilities.newID( className );
        User p = new LdapUser();
        p.setUid( uid );
        p.setFullName( FIRST_NAME + " " + SURNAME );
        p.setSurname( SURNAME );
        p.setInstitutionID( 333L );
        urep.create( p );
        
        // Update an existing user
        //-----------------------------------------------
        User found = urep.findUser( uid );
        found.setFirstName( FIRST_NAME );
        urep.update( found );
        
        // Assign a role to that user, then revoke it
        //-----------------------------------------------
        Role aod = urep.findRole( "OBOPS", "QAA" );
        urep.assign( uid, aod );    
        urep.revoke( uid, aod );
        
        // Finally, delete that user
        //-----------------------------------------------
        urep.delete( uid );
        assertFalse( urep.exists( uid ));
        List<String> uids = urep.listUsers();
        assertFalse( uids.contains( uid ));
    }

    // Test of roles features
    public void testRoles00() throws UserRepositoryEx {
        UserRepositoryFactory f = 
            new UserRepositoryFactory();
        UserRepository urep = f.getReadWrite( Backend.LDAP, "admin", "admin" );
        
        final String uid = TestUtilities.newID( className );  
        final String firstName = FIRST_NAME;
        final String surname = SURNAME;
        final String description = "Created by " + this.getClass().getCanonicalName();

        // Get a list of all roles we know about
        //-----------------------------------------------
        List<Role> allRoles = urep.listRoles();
        assertNotNull( allRoles );
        
        // Let's find some roles
        //-----------------------------------------------
        Role role = allRoles.get( 1 );
        Role found = urep.findRole( role.getApplication(), role.getName() );
        assertNotNull( found );
        assertEquals( role, found );
        
        // Create a new user
        //-----------------------------------------------
        User p = new LdapUser();
        p.setUid( uid );
        p.setFirstName( firstName );
        p.setFullName( firstName + " " + surname );
        p.setSurname( surname );
        p.setDescription( description );
        urep.create( p );
        
        // Zero roles, so far
        //-----------------------------------------------
        assertNotNull( p.getRoles() );
        assertEquals( 0, p.getRoles().size() );
        
        // Assign a role to that user: by assigning
        // AOD, we also assign ALMASTAFF (it's inherited)
        //-----------------------------------------------
        Role staff = urep.findRole( "OBOPS", "DSOA" );
        Role   aod = urep.findRole( "OBOPS", "AOD" );
        urep.assign( uid, aod );    

        final List<Role> allUserRoles = urep.getRoles( uid );
        assertNotNull( allUserRoles);
        assertTrue( allUserRoles.contains( aod ));
        assertTrue( allUserRoles.contains( staff ));

        final List<Role> explicitRoles = urep.getExplicitRoles( uid );
        assertNotNull( explicitRoles);
        assertTrue( explicitRoles.contains( aod ));
        assertFalse( explicitRoles.contains( staff ));   // "DSOA" is inherited

        User retrieved = urep.findUser( uid );
        final List<Role> retrievedRoles = retrieved.getRoles();
        assertNotNull( retrievedRoles );
        assertEquals( allUserRoles, retrievedRoles );
        
        // See if that user is found when we lookup users by role
        final List<String> staffIDs = urep.findUsers( staff );
        assertNotNull( staffIDs );
        assertTrue( staffIDs.size() > 0 );
        assertTrue( staffIDs.contains( uid ));
        
        // Now revoke that role
        //-----------------------------------------------
        urep.revoke( uid, aod );
        final List<Role> noUserRoles = urep.getRoles( uid );
        assertNotNull( noUserRoles );
        assertTrue( noUserRoles.size() == 0 );
        
        // Finally, delete that user
        //-----------------------------------------------
        urep.delete( uid );
        assertFalse( urep.exists( uid ));
        List<String> uids = urep.listUsers();
        assertFalse( uids.contains( uid ));
    }

    // Test of user features
    public void testUser00() throws UserRepositoryEx {
        UserRepositoryFactory f = 
            new UserRepositoryFactory();
        UserRepository urep = f.getReadWrite( Backend.LDAP, "admin", "admin" );
        
        final String uid = TestUtilities.newID( className  );  
        final String firstName = FIRST_NAME;
        final String surname = SURNAME;
        final String fullName = "Full Name";
        final String description = "Created by " + this.getClass().getCanonicalName();
        
        // Create a new user
        //-----------------------------------------------
        User p = new LdapUser();
        p.setUid( uid );
        p.setFullName( fullName );
        p.setSurname( surname );
        p.setDescription( description );
        urep.create( p );
        assertTrue( urep.exists( uid ));
        
        List<String> uids = urep.listUsers();
        assertTrue( uids.contains( uid ));
        
        // Update an existing user
        //-----------------------------------------------
        User found = urep.findUser( uid );
        assertNotNull( found );
        found.setFirstName( firstName );
        urep.update( found );
        
        User foundAgain = urep.findUser( uid );
        assertEquals( uid, foundAgain.getUid() );
        assertEquals( fullName, foundAgain.getFullName() );
        assertEquals( surname, foundAgain.getSurname() );
        assertEquals( firstName, foundAgain.getFirstName() );
        assertEquals( description, foundAgain.getDescription() );
            
        // Finally, delete that user
        //-----------------------------------------------
        urep.delete( uid );
        assertFalse( urep.exists( uid ));
        uids = urep.listUsers();
        assertFalse( uids.contains( uid ));
    }
}
