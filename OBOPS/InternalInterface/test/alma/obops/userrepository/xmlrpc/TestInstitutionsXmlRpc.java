/**
 * TestInstitutionsXmlRpc.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.xmlrpc;

import java.util.HashMap;
import java.util.List;

import org.apache.xmlrpc313.XmlRpcException;

import alma.obops.userrepository.domain.InstitutionMapComparator;
import alma.obops.xmlrpc.XmlRpcTestCase;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.6 $
 */

// $Id: TestInstitutionsXmlRpc.java,v 1.6 2011/04/15 15:04:51 fjulbe Exp $

public class TestInstitutionsXmlRpc extends XmlRpcTestCase {
    private static final String DATADIR = "alma/obops/userrepository/testdata/";
    private static final String INSTITUTES_DATA = DATADIR + "institutes.csv";

    private static final String MAPPINGS = 
        "alma/obops/userrepository/handler-mappings.properties";
    private XmlRpcInstitutionService service;
    
    public TestInstitutionsXmlRpc() {
        super( MAPPINGS );
    }
    
    // Launches an XML-RPC server
    @Override
    public void setUp() {
        XmlRpcInstitutionServiceTableImpl.setCsvFile( INSTITUTES_DATA );
        super.setUp();
        service = (XmlRpcInstitutionService) clientFactory.newInstance( XmlRpcInstitutionService.class );
        XmlRpcInstitutionServiceTableImpl.clear();
    }

    // Terminates the XML-RPC server
    @Override
    public void tearDown() {
        super.tearDown();
    }
    
    public void testGetCountries00() throws XmlRpcException {
        List<String> result = service.getCountries();
        assertEquals( 9, result.size() );
        
        // make sure array is sorted
        assertTrue( result.get(0).compareTo( result.get(1) ) < 0 );
        assertTrue( result.get(1).compareTo( result.get(2) ) < 0 );
        assertTrue( result.get(2).compareTo( result.get(3) ) < 0 );
        assertTrue( result.get(3).compareTo( result.get(4) ) < 0 );
        assertTrue( result.get(4).compareTo( result.get(5) ) < 0 );
        assertTrue( result.get(5).compareTo( result.get(6) ) < 0 );
        assertTrue( result.get(6).compareTo( result.get(7) ) < 0 );
        assertTrue( result.get(7).compareTo( result.get(8) ) < 0 );
    }
    
    public void testGetStates00() throws XmlRpcException {
        assertEquals( 2, service.getStates( "United Kingdom" ).size() );
        assertEquals( 1, service.getStates( "USA" ).size() );
        assertEquals( 0, service.getStates( "France" ).size() );
    }
    
    public void testQueryByCountryStateName() throws XmlRpcException {
        List<HashMap<String,Object>> result; 
        
        InstitutionMapComparator mapcomp = new InstitutionMapComparator();
        
        result = service.queryByCountryStateName( "USA", "CA", "" );
        assertEquals( 3, result.size() );
        assertTrue( mapcomp.compare( result.get(0), result.get(1)) < 0 );
        assertTrue( mapcomp.compare( result.get(1), result.get(2)) < 0 );
        
        result = service.queryByCountryStateName( "USA", "CA", "^U" );
        assertEquals( 2, result.size() );
        assertTrue( mapcomp.compare( result.get(0), result.get(1)) < 0 );
        
        result = service.queryByCountryStateName( "France", "", "M$" );
        assertEquals( 1, result.size() );
    }

    public void testQueryById() throws XmlRpcException {
        HashMap<String,Object> result; 
        
        result = service.queryById( -9999 );
        assertNull( result ); 

        result = service.queryById( 1006 );
        assertNotNull( result ); 
        assertEquals( "France", result.get( "country" ));
    }

    public void testQueryByName() throws XmlRpcException {
        List<HashMap<String,Object>> result; 
        
        result = service.queryByName( "not there" );
        assertNotNull( result ); 
        assertEquals( 0, result.size() ); 

        result = service.queryByName( "University of California at Berkeley" );
        assertNotNull( result ); 
        assertEquals( 2, result.size() ); 
        assertEquals( "USA", result.get(1).get( "country" ));
    }

    public void testAddUserInstitution() throws XmlRpcException {
        
        HashMap<String,Object> result; 
        
        long instNo1 = service.addUserInstitution( "amchavan", "ESO Alma" );
        assertTrue( instNo1 < 0 );
        result = service.queryById( (int) instNo1 );
        assertNotNull( result ); 
        assertEquals( "ESO Alma", result.get( "name1" ));
        assertEquals( "amchavan", result.get( "recorder" ));

        long instNo2 = service.addUserInstitution( "paul", "ESO Vitacura" );
        assertTrue( instNo2 < 0 );
        result = service.queryById( (int) instNo2 );
        assertNotNull( result ); 
        assertEquals( "ESO Vitacura", result.get( "name1" ));
        assertEquals( "paul", result.get( "recorder" ));
        assertEquals( "other", result.get( "executive" )); // test for COMP-5229
        
        assertFalse( instNo1 == instNo2 );
    }
}
