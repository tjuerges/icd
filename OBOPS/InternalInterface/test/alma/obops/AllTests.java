/*
 * AllTests
 * 
 * $Revision: 1.3 $
 *
 * Sep 12, 2007
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @version $Revision: 1.3 $
 * @author amchavan, Sep 12, 2007
 */

// $Id: AllTests.java,v 1.3 2011/03/24 09:49:31 fjulbe Exp $

public class AllTests {

    public static Test suite() {

        TestSuite suite = new TestSuite( "Test for xmlrpc and userrepository" );

        //$JUnit-BEGIN$
        //$JUnit-END$
        suite.addTest( alma.obops.userrepository.AllTests.suite() );
        suite.addTest( alma.obops.xmlrpc.AllTests.suite() );
        return suite;
    }
}
