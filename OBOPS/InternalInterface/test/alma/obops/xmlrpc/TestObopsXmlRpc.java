/**
 * TestInstitutionsXmlRpc.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

import java.util.HashMap;
import java.util.List;

import org.apache.xmlrpc313.XmlRpcException;

import alma.obops.xmlrpc.demo.Adder;
import alma.obops.xmlrpc.demo.Echo;
import alma.obops.xmlrpc.demo.Pojo;
import alma.obops.xmlrpc.demo.PojoGenerator;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.3 $
 */

// $Id: TestObopsXmlRpc.java,v 1.3 2011/02/23 10:23:45 rkurowsk Exp $

public class TestObopsXmlRpc extends XmlRpcTestCase {

    private static final String MAPPINGS = 
        "alma/obops/xmlrpc/test-handler-mappings.properties";
    
    public TestObopsXmlRpc() {
        super( MAPPINGS );
    }
    
    @Override
    public void setUp() {
        super.setUp();
    }

    // Terminates the XML-RPC server
    @Override
    public void tearDown() {
        super.tearDown();
    }    

    // regular call
    public void testAdd00() throws XmlRpcException {
        final String remoteMethod = "alma.obops.xmlrpc.demo.Adder.add";
        Object[] params = new Object[] { 2, 3 };
        Integer result = (Integer) client.execute( remoteMethod, params );
        assertEquals( (Integer) 5, result );
    }

    // regular call
    public void testAdd01() throws XmlRpcException {
        // make a call using dynamic proxy
        Adder adder = (Adder) clientFactory.newInstance( Adder.class );
        int result = adder.add( 1, 4 );
        assertEquals( 5, result );
    }
    
    // Try the echo service for internationalized strings
    public void testEcho00() throws XmlRpcException {
        Echo echo = (Echo) clientFactory.newInstance( Echo.class );
        String sSent = "abcd";
        String sBack = echo.echo( sSent );
        assertEquals( sSent, sBack );
        sSent = "Michèle";
        sBack = echo.echo( sSent );
        assertEquals( sSent, sBack );
    }
    
    // Try the echo service to see if non-primitive objects
    // can be exchanged
    public void testEcho01() throws XmlRpcException {
        Echo echo = (Echo) clientFactory.newInstance( Echo.class );
        Pojo pSent = new Pojo( "phone", 12345 );
        Pojo pBack = echo.echo( pSent );
        assertEquals( pSent.getName(),  pBack.getName()  );
        assertEquals( pSent.getValue(), pBack.getValue() );
        
        HashMap<String,Object> mSent = pSent.toHashMap();
        HashMap<String,Object> mBack = echo.echo( mSent );
        assertEquals( mSent.get( Pojo.NAME_KEY ),  mBack.get( Pojo.NAME_KEY )  );
        assertEquals( mSent.get( Pojo.VALUE_KEY ), mBack.get( Pojo.VALUE_KEY ) );
    }

    // Try the echo service to see if lists of hash maps can be exchanged
    public void testEcho02() throws XmlRpcException {
        PojoGenerator pgen = 
            (PojoGenerator) clientFactory.newInstance( PojoGenerator.class );
        List<HashMap<String,Object>> pojos = pgen.getPojos( 3 );
        assertNotNull( pojos );
        assertEquals( 3, pojos.size() );
        
        HashMap<String,Object> pojo = pojos.get( 0 );
        Object o = pojo.get( Pojo.NAME_KEY );
        assertNotNull( o );
        assertTrue( o instanceof String );
        String name = (String) o;
        assertTrue( name.startsWith( Pojo.POJO_BASE_NAME ));
        
        o = pojo.get( Pojo.VALUE_KEY );
        assertNotNull( o );
        assertTrue( o instanceof Integer );
        Integer value = (Integer) o;
        assertTrue( ((Integer) value ) >= 0 );
    }
}
