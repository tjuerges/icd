/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Nov 9, 2010
 * @version $Revision: 1.2 $
 */

// $Id: AllTests.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( AllTests.class.getName() );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestObopsXmlRpc.class );
        //$JUnit-END$
        return suite;
    }

}
