/**
 * InstitutionsTable.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.xmlrpc;

import java.util.HashMap;
import java.util.List;

import alma.obops.userrepository.domain.Institution;

/**
 * Definition of the XML-RPC interface to the Institutions table
 *
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.5 $
 */

// $Id: XmlRpcInstitutionService.java,v 1.5 2011/03/09 15:19:28 fjulbe Exp $

public interface XmlRpcInstitutionService {

    /**
     * @return The list of unique countries in the Institutions table, sorted
     *         alphabetically
     */
    public List<String> getCountries();

    /**
     * @return The list of unique states for a given country in the Institutions
     *         table, sorted alphabetically.<br/>
     *         <strong>Note</strong>: if some institutions of that country have
     *         state information and other institutions do not, a conventional
     *         "state" with no name appears at the top of the returned list
     *         (That's a data quality error, in fact.)<br/>
     *         The name of that "state" is {@value Institution#NO_NAME_STATE}
     */
    public List<String> getStates( String country );

    /**
     * Query this table and return all institutions matching the input country,
     * state and institution name. An exact match is performed for both country
     * and state; a partial regexp match for name.
     * 
     * @param country
     *            Country name: "Austria", "Japan", "Chile", etc. Should not be
     *            <code>null</code> or empty.
     * 
     * @param state
     *            State code: must be a state of the given country. A
     *            <code>null</code> or empty string means that no state
     *            information should be used when querying. If institutions with
     *            an empty state name should be returned, give
     *            {@link Institution#NO_NAME_STATE} as value of this parameter.
     * 
     * @param nameRE
     *            A simplified regular expression: institution names will be
     *            matched against it. A partial match will be sufficient for
     *            triggering inclusion in the returned set; that is, the RE does
     *            not need to match the entire institution name.<br/>
     *            For instance, a value of "^A" will match all institution names
     *            starting with "A"; a value of "nomy$" all names ending with
     *            "nomy"; etc.<br/>
     *            <strong>Note</strong>: only the "^" and "$" meta-characters
     *            are supported.<br/>
     *            Should not be <code>null</code>; an empty string will match
     *            any institution name.
     * 
     * @return A list of all retrieved institutions, sorted alphabetically by
     *         name1. The institutions themselves are converted to a map of [
     *         <em>fieldName</em>,<em>fieldValue</em>] pairs, see
     *         {@link Institution#toHashMap()}.
     */
    public List<HashMap<String, Object>> queryByCountryStateName( String country,
                                                                  String state,
                                                                  String nameRE );

    /**
     * @return The institution with the given institution number, or
     *         <code>null</code> if none was found.
     *         The institution itself is converted to a map of 
     *         [<em>fieldName</em>,<em>filedValue</em>] pairs, 
     *         see {@link Institution#toHashMap()}.
     */
    public HashMap<String,Object> queryById( int instNo );

    /**
     * @return The list of institutions with the given name; it can be an empty
     *         list if none was found; should never be <code>null</code>.<br/>
     *         An exact match is performed against field <code>name1</code>. <br/>
     *         The institution itself is converted to a map of 
     *         [<em>fieldName</em>, <em>filedValue</em>] pairs, see
     *         {@link Institution#toHashMap()}.
     */
    public List<HashMap<String,Object>> queryByName( String name1 );
    /**
     * Register a new "user institution", that is, an institution for which only
     * partial, unstructured information is available.
     * 
     * @param userID
     *            ID of the user registering their institution; it will be
     *            stored as is in field "recorder" of the Institution, see
     *            {@link Institution#getRecorder()}
     * 
     * @param institution
     *            A free-text description of the institution; it will be stored
     *            as is in field "name1" of the Institution, see
     *            {@link Institution#getName1()}.
     * 
     * @return The institution number of the new user institution.
     */
    public int addUserInstitution( String userID, String institution );
}
