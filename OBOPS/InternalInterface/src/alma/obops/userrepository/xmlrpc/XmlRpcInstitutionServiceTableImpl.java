/**
 * InstitutionsImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.xmlrpc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import alma.obops.userrepository.domain.Institution;
import alma.obops.userrepository.domain.InstitutionComparator;
import alma.obops.userrepository.domain.InstitutionTable;

/**
 * An implementation of the {@link XmlRpcInstitutionService} interface using an
 * {@link InstitutionTable} as back-end.
 * 
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: XmlRpcInstitutionServiceTableImpl.java,v 1.2 2011/03/09 15:19:28 fjulbe Exp $

public class XmlRpcInstitutionServiceTableImpl implements XmlRpcInstitutionService {

    private static InstitutionTable instTable;

    /** Pathname of the file where the institution data is kept */
    private static String csvFile;
    
    /**
     * Sets the pathname of the file where the institution data is kept
     */
    public static void setCsvFile( String pathname ) {
        XmlRpcInstitutionServiceTableImpl.csvFile = pathname;
    }
    
    /**
     * Zero-arg constructor
     * 
     * @throws IOException
     */
    public XmlRpcInstitutionServiceTableImpl() throws IOException {
        if( instTable == null ) {
            instTable = new InstitutionTable();

            File f = new File( csvFile );
            if( f.exists() && f.canRead() ) {
                instTable.load( csvFile );
            }
            else {
                ClassLoader cloader = this.getClass().getClassLoader();
                URL url = cloader.getResource( csvFile );
                if( url == null ) {
                    String msg = "Institutions file not found: '" + csvFile
                            + "'";
                    throw new RuntimeException( msg );
                }
                InputStream stream = url.openStream();
                instTable.load( stream );
            }
        }
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#queryById(int)
     */
    @Override
    public HashMap<String,Object> queryById( int instNo ) {
        Institution inst = instTable.queryById( (long) instNo );
        if( inst == null ) {
            return null;
        }
        return inst.toHashMap();
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#queryByCountryStateName(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public List<HashMap<String, Object>> queryByCountryStateName( String country,
                                                                  String state,
                                                                  String nameRE ) {
        InstitutionTable t = instTable.queryByCountryStateName( country, 
                                                                state, 
                                                                nameRE );
        List<HashMap<String,Object>> ret = new ArrayList<HashMap<String,Object>>();
        List<Institution> institutions = t.getInstitutions();
        Collections.sort( institutions, new InstitutionComparator() );
        for( Institution institution : institutions ) {
            ret.add( institution.toHashMap() );
        }
        return ret;
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#getStates(java.lang.String)
     */
    @Override
    public List<String> getStates( String country ) {
        String[] tmp = instTable.getStates( country );
        List<String> ret = new ArrayList<String>();
        for( int i = 0; i < tmp.length; i++ ) {
            ret.add( tmp[i] );
        }
        return ret;
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#getCountries()
     */
    @Override
    public List<String> getCountries() {
        String[] tmp = instTable.getCountries();
        List<String> ret = new ArrayList<String>();
        for( int i = 0; i < tmp.length; i++ ) {
            ret.add( tmp[i] );
        }
        return ret;
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#addUserInstitution(java.lang.String, java.lang.String)
     */
    @Override
    public int addUserInstitution( String userID, String institution ) {
        int ret = instTable.addUserInstitution( userID, institution );
        return ret;
    }
    
    // FOR TESTING ONLY
    // clear all data caches
    static void clear() {
        instTable = null;
    }

    /**
     * @see alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService#queryByName(java.lang.String)
     */
    @Override
    public List<HashMap<String, Object>> queryByName( String name1 ) {
        InstitutionTable t = instTable.queryByName( name1 );
        List<HashMap<String,Object>> ret = new ArrayList<HashMap<String,Object>>();
        for( Institution institution : t.getInstitutions() ) {
            ret.add( institution.toHashMap() );
        }
        return ret;
    }
}
