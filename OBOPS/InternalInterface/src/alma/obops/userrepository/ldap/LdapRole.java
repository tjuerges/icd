/**
 * LdapRole.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

/**
 * A wrapper around alma.obops.userrepository.domain.Role
 *
 * @author amchavan, Aug 20, 2010
 * @version $Revision: 1.3 $
 */

// $Id: LdapRole.java,v 1.3 2011/03/24 09:49:31 fjulbe Exp $

public class LdapRole 
    extends alma.userrepository.roledirectory.Role 
    implements alma.obops.userrepository.domain.Role {

    /**
     * @param name
     * @param application
     */
    public LdapRole( String application, String name  ) {
        super( name, application );
    }

    @Override
    public boolean equals( Object other ) {
        if( other == null ) {
            return false;
        }
        if( other instanceof LdapRole ) {
            LdapRole otherRole = (LdapRole) other;
            return getApplication().equals( otherRole.getApplication() ) && 
                   getName().equals( otherRole.getName() );
        }
        return false;
    }
    
    /**
     * @see alma.obops.userrepository.domain.Role#getApplication()
     */
    @Override
    public String getApplication() {
        return super.getApplicationName();
    }

    /**
     * @see alma.obops.userrepository.domain.Role#getName()
     */
    @Override
    public String getName() {
        return super.getRoleName();
    }
    
    @Override
    public String toString() {
        return toAlmaNotation();
    }

    /**
     * @see alma.obops.userrepository.domain.Role#toAlmaNotation()
     */
    @Override
    public String toAlmaNotation() {
        StringBuilder sb = new StringBuilder();
        sb.append( getApplication() ).append( "/" ).append( getName() );
        return sb.toString();
    }
}
