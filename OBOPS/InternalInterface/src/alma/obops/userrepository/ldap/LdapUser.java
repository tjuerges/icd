/**
 * AlmaUser.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

import java.util.ArrayList;
import java.util.List;

import alma.obops.userrepository.domain.Role;
import alma.obops.userrepository.domain.User;
import alma.userrepository.addressbook.ldap.UserInfoProperties;
import alma.userrepository.addressbook.ldap.beans.LdapAddress;
import alma.userrepository.addressbook.ldap.beans.LdapPreferences;
import alma.userrepository.domainmodel.Address;
import alma.userrepository.domainmodel.AddressBookEntry;
import alma.userrepository.domainmodel.Preferences;

/**
 * Represents an entry in the LDAP-based ALMA User Repository; only
 * Portal-related attributes are considered.
 * 
 * It wraps an AddressBookEntry; that is, a
 * alma.userrepository.addressbook.ldap.beans.LdapUser, its preferences and its
 * addresses.
 * 
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.3 $
 */

// $Id: LdapUser.java,v 1.3 2011/02/03 10:28:56 rkurowsk Exp $

public class LdapUser implements User {
    
    protected alma.userrepository.domainmodel.User wrappedUser;
    protected Preferences prefs;
    protected List<Address> addresses;
    protected AddressBookEntry entry;
    protected List<Role> roles;
    
    public LdapUser() {
        this.wrappedUser = new alma.userrepository.addressbook.ldap.beans.LdapUser();
        this.prefs = new LdapPreferences();
        this.addresses = new ArrayList<Address>();
        this.addresses.add( new LdapAddress() );
        this.roles = new ArrayList<Role>();
        
     
    }

    public LdapUser( AddressBookEntry entry ) {
        this.wrappedUser = (alma.userrepository.addressbook.ldap.beans.LdapUser) entry.getUser();
        this.prefs = entry.getPreferences();
        this.addresses = entry.getAddresses();
        this.entry = entry;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public String getDescription() {
        return wrappedUser.getNotes();
    }
    
    public String getEmail() {
        return addresses.get( 0 ).getEmail();
    }
    
    public AddressBookEntry getEntry() {
        return entry;
    }
    
    public String getExecutive() {
        return prefs.getPreference2();
    }

    public String getFirstName() {
        return wrappedUser.getFirstName();
    }

    public String getFullName() {
        return wrappedUser.getFullName();
    }

    /**
     * Implementation note: the User Portal registration page stores the
     * Institution ID into attribute "ou" (Organizational Unit) of the user's
     * LDAP entry. Upon reading, the value of that attribute ends up in field
     * <em>department</em> of {@link LdapAddress}, as indicated in
     * {@link UserInfoProperties}, field <em>department</em>.<br/>
     * Sounds confusing? It is.
     * 
     * @see alma.obops.userrepository.domain.User#getInstitutionID()
     */
    @Override
    public Long getInstitutionID() {
        String tmp = addresses.get( 0 ).getDepartment();
        if( tmp == null ) {
            return null;
        }
        return Long.parseLong( tmp );
    }

    public alma.userrepository.domainmodel.User getLdapUser() {
        return wrappedUser;
    }
    
    public String getPreferredArc() {
        return prefs.getPreference1();
    }

    public Preferences getPrefs() {
        return prefs;
    }
    
    public List<Role> getRoles() {
        return roles;
    }
    
    public String getSurname() {
        return wrappedUser.getSurname();
    }
    
    public String getUid() {
        return wrappedUser.getUid();
    }
    
    public void setDescription( String description ) {
        wrappedUser.setNotes( description );
    }
    
    public void setEmail( String email ) {
        addresses.get( 0 ).setEmail( email );
    }
    
    public void setExecutive( String executive ) {
        prefs.setPreference2( executive );
    }
    
    public void setFirstName( String firstName ) {
        wrappedUser.setFirstName( firstName );
    }
    
    public void setFullName( String fullName ) {
        wrappedUser.setFullName( fullName );
    }

    /**
     * Implementation note: the convention we have with the User Portal is to
     * store the Institution ID into attribute "ou" (Organizational Unit) of the
     * user's LDAP entry. Upon writing, the value of that attribute is read from
     * field <em>department</em> of {@link LdapAddress}, as indicated in
     * {@link UserInfoProperties}, field <em>department</em>.<br/>
     * Sounds confusing? It is.
     * 
     * @see alma.obops.userrepository.domain.User#setInstitutionID(java.lang.String)
     */
    @Override
    public void setInstitutionID( Long institutionID ) {
        String tmp = Long.toString( institutionID.longValue() );
        addresses.get( 0 ).setDepartment( tmp );
    }
    
    public void setPreferredArc( String preferredArc ) {
        prefs.setPreference1( preferredArc );
    }
    
    protected void setRoles( List<Role> roles ) {
        this.roles = roles;
    }

    public void setSurname( String surname ) {
        wrappedUser.setSurname( surname );
    }

    public void setUid( String uid ) {
        wrappedUser.setUid( uid );
    }
}
