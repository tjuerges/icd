/**
 * ObopsLdapRoleDirSession.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import alma.obops.userrepository.UserRepositoryEx;
import alma.obops.userrepository.domain.Role;
import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.roledirectory.ldap.LdapRoleDirectorySession;
import alma.userrepository.shared.ldap.DnDirectory;


/**
 * @author amchavan, Oct 28, 2010
 * @version $Revision: 1.2 $
 */

// $Id: ObopsLdapRoleDirectory.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class ObopsLdapRoleDirectory 
    extends LdapRoleDirectorySession
    implements ObopsRoleDirectory {

    private static final String[] ATTRIBUTES = { "member" };
    private DirContext context;
    
    /**
     * Public constructor.
     * @param context  The directory service interface
     * @param logger   Where to log our stuff
     * @throws UserRepositoryException
     */
    public ObopsLdapRoleDirectory( DirContext context, Logger logger )
            throws UserRepositoryException {
        super( context, logger );
        this.context = context;
    }

    /**
     * @return The list of IDs of all users having that role; it will never be
     *         <code>null</code>.
     */
    @Override
    public List<String> findUsers( Role role )
            throws UserRepositoryEx {

        String base = "ou=" + role.getApplication() + "," + DnDirectory.ROLE_BASE;
        String filter = "cn=" + role.getName();
        try {
            Set<String> memberSet = findMembers( base, filter, context );
            List<String> ret = new ArrayList<String>();
            for( String memberDn : memberSet ) {
                String uid = extractUID( memberDn );
                
                // NOTE: in the ALMA User Repository user "nobody" is used
                // as a placeholder in "empty" groups, as LDAP groups cannot
                // be empty (why?).
                // Here we filter that out.
                if( ! uid.equals( "nobody" )) {
                    ret.add( uid );
                }
            }
            return ret;
        }
        catch( NamingException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @param A user DN, e.g. <code>uid=fjulbe,ou=people,ou=master,dc=alma,dc=info</code>
     * @return The UID part of it, e.g. <code>fjulbe</code>
     */
    private String extractUID( String dn ) {
        String[]  dnElements = dn.split( "," );
        String[] uidElements = dnElements[0].split( "=" );
        return uidElements[1];
    }

    /**
     * @param base
     *            Search base, e.g.
     *            <code>ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</code>
     * @param filter
     *            What to look for, e.g. <code>cn=ALMASTAFF</code>
     * @param context
     *            Search context, including LDAP server URL, security info, etc.
     * 
     * @return A set of DNs, corresponding to all users having the input role
     * @throws NamingException
     */
    private static Set<String> findMembers( String base,
                                            String filter, 
                                            DirContext context )
            throws NamingException {

        Set<String> ret = new HashSet<String>(); // this is what we return
        
        // perform the search
        SearchControls ctrl = new SearchControls();
        ctrl.setSearchScope( SearchControls.SUBTREE_SCOPE );
        ctrl.setReturningAttributes( ATTRIBUTES );  // only get members
        NamingEnumeration<SearchResult> results = context.search( base, filter, ctrl );
        
        // loop over the result set
        while( results.hasMore() ) {
            
            // retrieve all the member attributes of the entry, then loop over them
            SearchResult result = (SearchResult) results.next();
            Attributes attribs = result.getAttributes();
            NamingEnumeration<? extends Attribute> nEnum = attribs.getAll();
            while( nEnum.hasMore() ) {
                
                // loop over all (member) attributes: if they are 
                // people, add them them to our ret list, otherwise
                // assume they are sub-groups and recurse over them
                Attribute attribute = nEnum.next();
                NamingEnumeration<?> members = attribute.getAll();
                while( members.hasMore() ) {
                    
                    String memberDN = (String) members.next();
                    if( memberDN.length() == 0 ) {
                        continue;               // just in case
                    }
                    
                    if( memberDN.startsWith( "uid=" )) {
                        ret.add( memberDN );      // if it's a user, add it
                        continue;
                    }
                    
                    // build the recursive query
                    int comma = memberDN.indexOf( "," );
                    if( comma < 0 ) {
                        // what happened?!?
                        String msg = "Unsupported member type: " + memberDN;
                        throw new RuntimeException( msg );
                    }
                    filter = memberDN.substring( 0, comma );
                    base = memberDN.substring( comma + 1 );
                    ret.addAll( findMembers( base, filter, context ));
                }
            }
        }
        return ret;
    }
}
