/**
 * LdapUserRepository.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import alma.obops.userrepository.UserRepository;
import alma.obops.userrepository.UserRepositoryEx;
import alma.obops.userrepository.domain.Role;
import alma.obops.userrepository.domain.User;
import alma.userrepository.addressbook.AddressBookSession;
import alma.userrepository.addressbook.ldap.beans.LdapAddressBookEntry;
import alma.userrepository.domainmodel.Address;
import alma.userrepository.domainmodel.AddressBookEntry;
import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.roledirectory.RoleDirectory;

/**
 * LDAP-based implementation of the ALMA User Repository. Based on the Archive's
 * internal interface.
 * 
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.3 $
 */

// $Id: LdapUserRepository.java,v 1.3 2011/02/03 10:28:56 rkurowsk Exp $

public class LdapUserRepository implements UserRepository {

    protected static Logger logger;
    protected static ObopsLdapDirectoryFactory dsFactory;
    
    protected AddressBookSession addressBook ;
    protected ObopsRoleDirectory roleBook;

    /**
     * Constructor for anonymous, read-only access. It will allow querying of
     * the repository but no modification.
     * 
     * @throws UserRepositoryEx
     */
    public LdapUserRepository() throws UserRepositoryEx {
        this( "", "" );
    }

    /**
     * Constructor for read/write access. It will allow querying and
     * modification of the repository.
     * 
     * @param username  Something like "george" -- not a full DN
     * @param password
     * @throws UserRepositoryEx
     */
    public LdapUserRepository( String username, String password ) 
        throws UserRepositoryEx {
        
        if( dsFactory == null ) { 
            logger = Logger.getLogger( LdapUserRepository.class.getSimpleName() );
            dsFactory = ObopsLdapDirectoryFactory.getInstance( logger );
        }

        try {
            addressBook = dsFactory.newAddressBookSession( username, password );
            roleBook = dsFactory.newRoleDirectorySession( username, password );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }


    /**
     * @see alma.obops.userrepository.UserRepository#assign(java.lang.String, alma.obops.userrepository.domain.Role)
     */
    @Override
    public boolean assign( String uid, Role role ) throws UserRepositoryEx {
        try {
            LdapRole ldapRole = (LdapRole) role;
            return roleBook.assignRole( uid, ldapRole );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }


    /**
     * Convert a RoleDirectory to a list of Roles
     */
    private List<Role> convert( RoleDirectory roleDir ) {
        List<Role> ret = new ArrayList<Role>();
        for( String appName : roleDir.getApplicationNames() ) {
            List<alma.userrepository.roledirectory.Role> roles = 
                roleDir.getRoles( appName );
            for( alma.userrepository.roledirectory.Role ldapRole : roles ) {
                
                Role role = new LdapRole( ldapRole.getApplicationName(),
                                          ldapRole.getRoleName() );
                ret.add( role );
            }
        }
        return ret;
    }

    /**
     * @see alma.obops.userrepository.UserRepository#create(alma.obops.userrepository.domain.User)
     */
    @Override
    public void create( User user ) throws UserRepositoryEx {

        LdapUser ldapUser = (LdapUser) user;
        
        List<Address> addresses = ldapUser.getAddresses();
        
        AddressBookEntry entry = 
            new LdapAddressBookEntry( ldapUser.getLdapUser(), addresses, ldapUser.getPrefs() );

        try {
            addressBook.commitEntry( entry );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
        
//        final RoleDirectorySession rds = getRoleDirectorySession();
//        RoleDirectory roles = rds.getAllUserRoles( uid );
//        if( ! roles.hasRole( USER )) {
//            rds.assignRole( uid, USER );
//            
//        }
    }

    /**
     * @throws UserRepositoryEx 
     * @see alma.obops.userrepository.UserRepository#delete(java.lang.String)
     */
    @Override
    public void delete( String uid ) throws UserRepositoryEx {
        try {
            addressBook.deleteEntryByUid( uid );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @throws UserRepositoryEx 
     * @see alma.obops.userrepository.UserRepository#exists(java.lang.String)
     */
    @Override
    public boolean exists( String uid ) throws UserRepositoryEx {
        try {
            return addressBook.exists( uid );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#findRole(java.lang.String, java.lang.String)
     */
    @Override
    public Role findRole( String application, String name )
            throws UserRepositoryEx {
        List<Role> allRoles = listRoles();
        for( Role role : allRoles ) {
            if( role.getApplication().equals( application ) && 
                role.getName().equals( name ) )
                return role;
        }
        return null;
    }

    /**
     * @throws UserRepositoryEx 
     * @see alma.obops.userrepository.UserRepository#findUser(java.lang.String)
     */
    @Override
    public User findUser( String uid ) throws UserRepositoryEx {
        try {
            if( ! addressBook.exists( uid )) {
                return null;
            }
            
            AddressBookEntry entry = addressBook.getEntry( uid );
            LdapUser ret = new LdapUser( entry );
            ret.setRoles( getRoles( uid ));
            return ret;
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#findUsers(alma.obops.userrepository.domain.Role)
     */
    @Override
    public List<String> findUsers( Role role )
            throws UserRepositoryEx {
        return roleBook.findUsers( role );
    }

    /**
     * @see alma.obops.userrepository.UserRepository#getExplicitRoles(java.lang.String)
     */
    @Override
    public List<Role> getExplicitRoles( String uid ) throws UserRepositoryEx {
        try {
            RoleDirectory rd = roleBook.getExplicitUserRoles( uid );
            return convert( rd );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#getRoles(java.lang.String)
     */
    @Override
    public List<Role> getRoles( String uid ) throws UserRepositoryEx {
        try {
            RoleDirectory rd = roleBook.getAllUserRoles( uid );
            return convert( rd );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#listRoles()
     */
    @Override
    public List<Role> listRoles() throws UserRepositoryEx {
        try {
            RoleDirectory rd = roleBook.getAllRoles();
            return convert( rd );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#listUsers()
     */
    @Override
    public List<String> listUsers() throws UserRepositoryEx {
        try {
            return addressBook.listUsers();
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#revoke(java.lang.String, alma.obops.userrepository.domain.Role)
     */
    @Override
    public boolean revoke( String uid, Role role ) throws UserRepositoryEx {
        try {
            LdapRole ldapRole = (LdapRole) role;
            return roleBook.revokeRole( uid, ldapRole );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @see alma.obops.userrepository.UserRepository#setPassword(java.lang.String, java.lang.String)
     */
    @Override
    public void setPassword( String uid, String password )
            throws UserRepositoryEx {
        try {
            addressBook.setPassword( uid, password );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }

    /**
     * @param user
     *            Must be an instance of PortalUser returned by a previous call
     *            to {@link #findUser(String)}
     *            
     * @see alma.obops.userrepository.UserRepository#update(alma.obops.userrepository.domain.User)
     */
    @Override
    public void update( User user ) throws UserRepositoryEx {
        LdapUser lpuser = (LdapUser) user;
        try {
            String uid = lpuser.getUid();
            if( ! addressBook.exists( uid )) {
                throw new UserRepositoryEx( "User '" + uid + "' not found" );
            }
            
            alma.userrepository.addressbook.ldap.beans.LdapUser luser = 
                (alma.userrepository.addressbook.ldap.beans.LdapUser) 
                lpuser.getLdapUser();
            if( luser.getModifiedTimestamp() == null ) {
                String msg = 
                    "Input user was not returned by a previous call to find()";
                throw new UserRepositoryEx( msg );
            }
            addressBook.commitEntry( lpuser.getEntry() );
        }
        catch( UserRepositoryException e ) {
            throw new UserRepositoryEx( e );
        }
    }
}
