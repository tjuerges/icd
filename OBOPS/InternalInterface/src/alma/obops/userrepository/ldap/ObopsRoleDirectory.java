/**
 * ObopsRoleDirSession.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

import java.util.List;

import alma.obops.userrepository.UserRepositoryEx;
import alma.obops.userrepository.domain.Role;
import alma.userrepository.roledirectory.RoleDirectorySession;

/**
 * An extension of {@link RoleDirectorySession} offering a method to lookup
 * users given a role.<br/>
 * (I'm not adding that confusing <em>Session</em> suffix to this class' name.)
 * 
 * @author amchavan, Oct 28, 2010
 * @version $Revision: 1.2 $
 */

// $Id: ObopsRoleDirectory.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public interface ObopsRoleDirectory extends RoleDirectorySession {

    /**
     * Retrieve all users with a given role.
     * 
     * @param role
     *            The role; e.g. <em>OBOPS/pht</em>
     * 
     * @return The list of IDs of all users having that role; it will never be
     *         <code>null</code>
     * 
     * @throws UserRepositoryEx
     *             in case anything goes wrong while retrieving the information.
     */
    public List<String> findUsers( Role role )
            throws UserRepositoryEx;
}
