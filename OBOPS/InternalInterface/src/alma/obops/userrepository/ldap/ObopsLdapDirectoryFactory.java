/**
 * ObopsLdapDirSessionFactory.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.ldap;

import java.util.logging.Logger;

import javax.naming.directory.DirContext;

import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.shared.DirectorySessionFactory;
import alma.userrepository.shared.LdapDirectorySessionFactory;

/**
 * A subclass of {@link DirectorySessionFactory} supporting the LDAP
 * implementation of the {@link ObopsRoleDirectory}.
 * 
 * @author amchavan, Oct 28, 2010
 * @version $Revision: 1.2 $
 */

// $Id: ObopsLdapDirectoryFactory.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class ObopsLdapDirectoryFactory extends LdapDirectorySessionFactory {

    private static ObopsLdapDirectoryFactory instance;

    /** Public constructor */
    public ObopsLdapDirectoryFactory( Logger inLogger ) {
        super( inLogger );
    }

    public static ObopsLdapDirectoryFactory getInstance( Logger inLogger ) {
        if( instance == null ) {
            synchronized( DirectorySessionFactory.class ) {
                if( instance == null ) {
                    instance = new ObopsLdapDirectoryFactory( inLogger );
                }
            }
        }
        return instance;
    }

    /**
     * @return An {@link ObopsRoleDirectory}.
     * @see alma.userrepository.shared.LdapDirectorySessionFactory#newRoleDirectorySession(java.lang.String, java.lang.String)
     */
    public ObopsRoleDirectory newRoleDirectorySession( String userId,
                                                       String password )
            throws UserRepositoryException {
        String userDn = getUserDn( userId );
        DirContext context = getContext( userDn, password );
        return new ObopsLdapRoleDirectory( context, this.myLogger );
    }
}
