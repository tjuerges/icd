/**
 * UserRepository.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository;

import java.util.List;

import alma.obops.userrepository.domain.User;
import alma.obops.userrepository.domain.Role;

/**
 * Defines the interface to the ALMA User Repository
 *
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.4 $
 */

// $Id: UserRepository.java,v 1.4 2011/02/03 10:28:56 rkurowsk Exp $

public interface UserRepository {

    /**
     * Assign the a role to a user.
     * 
     * @param uid
     *            A unique identifier for a Portal user
     * 
     * @param role
     *            the role to assign
     * 
     * @return <code>true</code> if the user did not have the role and it was
     *         added, <code>false</code> otherwise
     * 
     * @see #revoke(String, Role)
     * 
     * @throws UserRepositoryEx
     *             If the connection does not have the privileges required to
     *             update the entry or uid does not correspond to a user in the
     *             database.
     */
    public boolean assign( String uid, Role role ) throws UserRepositoryEx;

    /** 
     * Save to the repository a new user
     * @throws UserRepositoryEx 
     */
    public void create( User user ) throws UserRepositoryEx;

    /**
     * Delete a user from the Portal Repository
     * 
     * @param uid    A unique identifier for a Portal user
     * 
     * @throws UserRepositoryEx 
     */
    public void delete( String uid ) throws UserRepositoryEx;

    /**
     * @param uid    A unique identifier for a Portal user
     * 
     * @return <code>true</code> is there exists a Portal Repository user with
     *         the given UID, <code>false</code> otherwise
     *         
     * @throws UserRepositoryEx 
     */
    public boolean exists( String uid ) throws UserRepositoryEx;

    /**
     * Lookup a role in the Portal Repository
     * 
     * @param application    The role's application name
     * @param name           The role's own name
     * 
     * @return A Role instance corresponding to the input args, or
     *         <code>null</code> if none were found.
     *         
     * @throws UserRepositoryEx 
     */
    public Role findRole( String application, String name ) 
        throws UserRepositoryEx;

    /**
     * Lookup a user in the Portal Repository
     * 
     * @param uid    A unique identifier for a Portal user
     * 
     * @return A PortalUser instance corresponding to the input user ID, or
     *         <code>null</code> if none were found.
     *         
     * @throws UserRepositoryEx 
     */
    public User findUser( String uid ) throws UserRepositoryEx;
    
    /**
     * Retrieve all users with a given role.
     * 
     * @param role
     *            The role; e.g. <em>OBOPS/pht</em>
     * 
     * @return The list of IDs of all users having that role; it will never be
     *         <code>null</code>
     * 
     * @throws UserRepositoryException
     *             in case anything goes wrong while retrieving the information.
     */
    public List<String> findUsers( Role role )
            throws UserRepositoryEx;

    /**
     * Gets all roles explicitly assigned to a user. This method does not return
     * any additional roles gained through the role hierarchy.
     * 
     * @param uid    A unique identifier for a Portal user
     * 
     * @return All roles explicitly assigned to the user.
     * 
     * @throws UserRepositoryEx
     */
    public List<Role> getExplicitRoles( String uid ) throws UserRepositoryEx;

    /**
     * Gets all roles attributed to a user, whether assigned explicitly or
     * gained through role inheritance.
     * 
     * @param uid   A unique identifier for a Portal user
     * 
     * @return All roles assigned to the user.
     * 
     * @throws UserRepositoryEx
     */
    public List<Role> getRoles( String uid ) throws UserRepositoryEx;

    /**
     * Gets all roles available in the system.
     * 
     * @return All available roles in the system.
     * 
     * @throws UserRepositoryEx
     */
    public List<Role> listRoles() throws UserRepositoryEx;

    /**
     * List the UIDs of all users in the address book.
     * 
     * @return the list of user UIDs
     * 
     * @throws UserRepositoryEx
     */
    public List<String> listUsers() throws UserRepositoryEx;

    /**
     * Revokes a role from a user.
     * 
     * @param uid   A unique identifier for a Portal user
     * 
     * @param role
     *            the role from which should remove the user
     * 
     * @return <code>true</code> if the user had the role and it was revoked,
     *         <code>false</code> otherwise.
     * 
     * @see #assign(String, Role)
     * 
     * @throws UserRepositoryEx
     *             If the connection does not have the privileges required to
     *             update the entry or uid does not correspond to a user
     *             in the database.
     */
    public boolean revoke( String uid, Role role ) throws UserRepositoryEx;

    /**
     * Set the password for a user.
     * 
     * @param uid
     *            A unique identifier for a Portal user
     * @param password
     *            The new user password
     * 
     * @throws UserRepositoryEx
     *             If the connection does not have the privileges required to
     *             update the entry or uid does not correspond to a user
     *             in the database.
     */
    public void setPassword( String uid, String password )
            throws UserRepositoryEx;

    /** 
     * Update an existing user in the repository
     * 
     * @throws UserRepositoryEx 
     */
    public void update( User user ) throws UserRepositoryEx;
}
