/**
 * UserRepositoryFactory.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository;

import alma.obops.userrepository.ldap.LdapUserRepository;

/**
 * A factory class for User Repository instances
 *
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.4 $
 */

// $Id: UserRepositoryFactory.java,v 1.4 2011/02/03 10:28:56 rkurowsk Exp $

public class UserRepositoryFactory {
    
    /** Enumerates the available factory implementations */
    public enum Backend {
        LDAP,
        SQL
    }

    /**
     * Constructs a new User Repository factory.<p/>
     * 
     * The factory is configured using property files.
     * For an <em>LDAP-based</em> repository, the following properties will be read
     * from <em>archiveConfig.properties</em>:
     * <ul>
     * <li><em>archive.userrepository.provider.url</em>: 
     *     the URL of the LDAP server; e.g. 
     *     <code>ldap://ngas01.hq.eso.org:389</code>
     * </ul>
     * 
     * For an <em>SQL-based</em> repository, <strong>TBD</strong><p/>
     */
    public UserRepositoryFactory() {
        // no-op
    }
    
    /**
     * @param type Only FactoryType.LDAP is currently supported.
     * @return A User Repository for anonymous, read-only access. It will allow
     *         querying of the repository but no modification.
     * @throws UserRepositoryEx 
     */
    public UserRepository getReadOnly( Backend type ) 
        throws UserRepositoryEx {
        
        if( type == null || type != Backend.LDAP ) {
            final String msg = "Only FactoryType.LDAP is supported";
            throw new IllegalArgumentException( msg );
        }

        UserRepository ret = new LdapUserRepository();
        return ret;
    }
    
    /**
     * @return A User Repository for read/write access. It will allow
     *         querying and modification of the repository.
     * @param username  
     * @param password
     * @throws UserRepositoryEx 
     */
    public UserRepository getReadWrite( Backend type, String username, String password ) 
        throws UserRepositoryEx {
        
        if( type == null || type != Backend.LDAP ) {
            final String msg = "Only FactoryType.LDAP is supported";
            throw new IllegalArgumentException( msg );
        }
        
        UserRepository ret = new LdapUserRepository( username, password );
        return ret;
    }
}
