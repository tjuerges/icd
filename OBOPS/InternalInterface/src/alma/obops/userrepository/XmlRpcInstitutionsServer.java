package alma.obops.userrepository;

import alma.obops.userrepository.xmlrpc.XmlRpcInstitutionService;
import alma.obops.userrepository.xmlrpc.XmlRpcInstitutionServiceTableImpl;
import alma.obops.xmlrpc.ConfigurableServletServer;

/**
 * Runs a test server for the XML-RPC based {@link XmlRpcInstitutionService}.<p/>
 * 
 * Data is provided from a resource in the classpath, see
 * {@link #INSTITUTION_DATA}.  By default the server runs on port 
 * {@value #DEFAULT_PORT}; a different 
 * port number can be given on the command line. <p/>
 * The following Python script can be used to exercise the server:
 * <pre>
 * import xmlrpclib 
 * server = xmlrpclib.ServerProxy( "http://localhost:8888" )
 * # server = xmlrpclib.ServerProxy( "http://ga013255.ads.eso.org:8888" )
 * service = server.alma.obops.userrepository.xmlrpc.InstitutionService
 * service.getCountries()
 * service.getStates( "USA" )
 * service.getStates( "United Kingdom" )
 * service.getStates( "France" )
 * service.queryByCountryStateName( "USA", "CA", "" )
 * service.queryByCountryStateName( "USA", "CA", "^U" )
 * service.queryByCountryStateName( "France", "", "M$" )
 * service.queryById( 1006 )
 * service.addUserInstitution( "amchavan", "ESO Alma" )
 * </pre>
 * 
 * @author amchavan, Nov 10, 2010
 * @version $Revision: 1.3 $
 */

// $Id: XmlRpcInstitutionsServer.java,v 1.3 2011/03/09 15:19:28 fjulbe Exp $

public class XmlRpcInstitutionsServer {
    
    public static final int DEFAULT_PORT = 8888;
    private static final String HANDLER_MAPPINGS = 
            "alma/obops/userrepository/handler-mappings.properties";
    public static final String INSTITUTION_DATA = "alma/obops/userrepository/data/institutes.csv";
    
    public static void main( String[] args ) throws Exception {
        int port = DEFAULT_PORT;
        if( args.length > 0 ) {
            port = Integer.parseInt( args[0] );
        }
        
        XmlRpcInstitutionServiceTableImpl.setCsvFile( INSTITUTION_DATA );
        final ClassLoader cloader = XmlRpcInstitutionsServer.class.getClassLoader();
        ConfigurableServletServer server = 
            new ConfigurableServletServer( cloader, HANDLER_MAPPINGS, port );
        server.start();
    }
}