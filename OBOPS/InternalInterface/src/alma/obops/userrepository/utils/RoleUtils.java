/**
 * RoleUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.utils;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import alma.obops.userrepository.domain.Role;
import alma.obops.userrepository.ldap.LdapRole;

/**
 * A collection of methods and constants to help working with {@link Role}s
 *
 * @author amchavan, Mar 17, 2011
 * @version $Revision: 1.2 $
 */

// $Id: RoleUtils.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class RoleUtils {

    /** Used to separate roles in {@link #getRolesDescriptor()} */
	public static final String ROLES_SEPARATOR = " ";

    /** 
     * Compare two {@link Role} instances: they are equal if they
     * have the same <em>application</em> and <em>name</em>.
     */
    public static final Comparator<Role> COMPARATOR = 
        new Comparator<Role>() {
        @Override
        public int compare( Role r1, Role r2 ) {
            int applicationCompare = r1.getApplication().compareTo( r2.getApplication() );
            if( applicationCompare != 0 ) {
                return applicationCompare;
            }
            return r1.getName().compareTo( r2.getName() );
        }
    };

	/**
	 * @param roles
	 *            A set of {@link Role}s
	 * 
	 * @return An array of role names in Alma notation corresponding to the
	 *         input roles; e.g. [<em>MASTER/ADMINISTRATOR</em>,
	 *         <em>OBOPS/AOD</em>, <em>OMC/ENGINEER</em>]
	 */
    public static String[] convertToRoleArray( Set<Role> roles ) {
    	String[] ret = new String[roles.size()];
    	int i = 0;
        for( Role role : roles ) {
        	ret[i++] = role.toAlmaNotation();
        }
        return ret;
    }

	/**
	 * @param roles
	 *            A set of {@link Role}s
	 * 
	 * @return A String of role names in Alma notation separated by a
	 *         {@link #ROLES_SEPARATOR}, corresponding to the input roles; e.g.
	 *         <code>"MASTER/ADMINISTRATOR&nbsp;&nbsp;OBOPS/AOD&nbsp;OMC/ENGINEER"</code>
	 */
    public static String convertToRoleString( Set<Role> roles ) {
    	String[] roleNames = convertToRoleArray( roles );
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        
        for( String roleName : roleNames ) {
            if( ! first ) {
                sb.append( ROLES_SEPARATOR );
            }
            else {
                first = false;
            }
            sb.append( roleName );
        }
        return sb.toString();
    }

    /**
     * @param roleNames
     *            An array of role names in Alma notation; e.g. 
     *            [<em>MASTER/ADMINISTRATOR</em>, 
     *            <em>OBOPS/AOD</em>, <em>OMC/ENGINEER</em>]
     *            
     * @return A set of {@link Role}s, corresponding to the input names
     */
    public static Set<Role> convertToRoleSet( String[] roleNames ) {
        Set<Role> roles = new TreeSet<Role>( COMPARATOR );
        for( String roleName : roleNames ) {
            if( roleName.length() == 0 ) {
                continue;                   // skip empty entries
            }
            roles.add( fromAlmaNotation( roleName ) );
        }
        return roles;
    }

	/**
	 * @param roleNames
	 *            A String of role names in Alma notation separated by a
	 *            {@link #ROLES_SEPARATOR}; e.g.
	 *            <code>"MASTER/ADMINISTRATOR&nbsp;&nbsp;OBOPS/AOD&nbsp;OMC/ENGINEER"</code>
	 * 
	 * @return A set of {@link Role}s, corresponding to the input names
	 */
    public static Set<Role> convertToRoleSet( String roleNames ) {
    	return convertToRoleSet( roleNames.split( ROLES_SEPARATOR ));
    }

	/**
     * @return The (possibly empty) Set of roles that are members of Set
     *         <em>a</em> but not of Set <em>b</em>.
     */
    public static Set<Role> diff( Set<Role> a, Set<Role> b ) {
        Set<Role> ret = new TreeSet<Role>( a );
        for( Role bRole : b ) {
            ret.remove( bRole );
        }
        return ret;
    }

    /**
     * @return <code>true</code> if Sets <em>a</em> and <em>b</em> have the same
     *         size and all their elements are equals; <code>false</code>
     *         otherwise
     */
    public static boolean equals( Set<Role> a, Set<Role> b ) {
        
        if( a.size() != b.size() ) {
            return false;
        }
        for( Role aRole : a ) {
            if( ! b.contains( aRole )) {
                return false;
            }
        }
        return true;
    }

	/**
     * Factory method: convert a representation of a role like into an LDAP
     * role instance
     * 
     * @param almaRole A role in Alma notation, e.g. <em>MASTER/USER</em>
     * 
     * @return A new Role instance
     */
    public static Role fromAlmaNotation( String almaRole ) {
        String[] tmp = almaRole.split( "/" );
        if( tmp.length != 2 ) {
            final String message = "Invalid role: '" + almaRole + "'";
            throw new RuntimeException( message );
        }
        Role role = new LdapRole( tmp[0], tmp[1] );
        return role;
    }
}
