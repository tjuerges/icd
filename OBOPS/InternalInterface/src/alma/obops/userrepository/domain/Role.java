/**
 * AlmaRole.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

/**
 * A user role in ALMA
 *
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.3 $
 */

// $Id: Role.java,v 1.3 2011/03/24 09:49:31 fjulbe Exp $

public interface Role {

    /**
     * @return The application or subsystem for whom this role is meaningful
     */
    public String getApplication();
    
    /**
     * @return The name of the role
     */
    public String getName();

    /**
     * @return A representation of this role in the ALMA notation, e.g.
     *         <em>MASTER/USER</em>
     */
    public String toAlmaNotation();
}
