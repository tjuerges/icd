/**
 * InstitutionComparator.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.util.Comparator;

/**
 * Compares two institutions based on the concatenation of fields 
 * <em>name1</em>, <em>name2</em> and <em>name3</em>.
 * 
 * @author amchavan, Mar 4, 2011
 * @version $Revision: 1.2 $
 */

// $Id: InstitutionComparator.java,v 1.2 2011/03/09 15:19:28 fjulbe Exp $

public class InstitutionComparator implements Comparator<Institution> {

    /**
     * Compare two institutions based on the concatenation of fields 
     * <em>name1</em>, <em>name2</em> and <em>name3</em>.
     * 
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare( Institution inst1, Institution inst2 ) {
        
        String key1 = makeKey( inst1 );
        String key2 = makeKey( inst2 );
        
        return key1.compareTo( key2 );
    }

    /**
     * @return A concatenation of {@link Institution#getName1()},
     *         {@link Institution#getName2()} and 
     *         {@link Institution#getName3()}.
     */
    private String makeKey( Institution institution ) {
        String tmp;
        StringBuilder key = new StringBuilder();
        
        tmp = institution.getName1();
        if( tmp != null ) {
            key.append( tmp.trim() );
        }
        
        tmp = institution.getName2();
        if( tmp != null ) {
            key.append( tmp.trim() );
        }
        
        tmp = institution.getName3();
        if( tmp != null ) {
            key.append( tmp.trim() );
        }
        
        return key.toString();
    }
}
