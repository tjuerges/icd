/**
 * AlmaRegionalCenters.java
 *
 * Copyright European Southern Observatory 2011
 */

package alma.obops.userrepository.domain;

/**
 * TODO This belongs to the APDM
 *
 * @author amchavan, Apr 11, 2011
 * @version $Revision: 1.2 $
 */

// $Id: AlmaRegionalCenter.java,v 1.2 2011/04/15 15:04:51 fjulbe Exp $

public enum AlmaRegionalCenter {
    EA, EU, NA
}