/**
 * InstitutionsTable.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.csvreader.CsvReader;

/**
 * Holds a table of institutions, loaded from an external data source.
 *
 * @author amchavan, Oct 25, 2010
 * @version $Revision$
 */

// $Id$

public class InstitutionTable {
    // Inst No,Sibling No,Name1,Name2,Name3,Alternate names,Country,State,Executive,City,Postcode,Street,Email,Webpage,Telephone,Facsimile

    /** CSV file header for the ID column */
    public static final String HEADER_ID = "Number";
    
    /** CSV file header for the ID column */
    public static final String HEADER_SIBLING = "Sibling number";

    /** CSV file header for the institution name column */
    public static final String HEADER_NAME1 = "Institution name";
    
    /** CSV file header for the institution name column */
    public static final String HEADER_NAME2 = "Department name";
    
    /** CSV file header for the institution name column */
    public static final String HEADER_NAME3 = "Subdepartment name";
    
    /** CSV file header for the institution name column */
    public static final String HEADER_ALT_NAMES = "Alternative names";

    /** CSV file header for the country column */
    public static final String HEADER_COUNTRY = "Country";
    
    /** CSV file header for the state column */
    public static final String HEADER_STATE = "State";
    
    /** CSV file header for the executive column */
    public static final String HEADER_EXECUTIVE = "Executive";
    
    /** CSV file header for the executive column */
    public static final String HEADER_CITY = "Place";
    
    /** CSV file header for the executive column */
    public static final String HEADER_POSTCODE = "Postcode";
    
    /** CSV file header for the executive column */
    public static final String HEADER_STREET = "Street/number";
    
    /** CSV file header for the executive column */
    public static final String HEADER_EMAIL = "E-mail";
    
    /** CSV file header for the executive column */
    public static final String HEADER_URL = "Webpage";
    
    /** CSV file header for the executive column */
    public static final String HEADER_PHONE = "Telephone";
    
    /** CSV file header for the executive column */
    public static final String HEADER_FAX = "Facsimile";
    
    public static final String[] ALL_HEADERS = {
        HEADER_ID, HEADER_SIBLING, 
        HEADER_NAME1, HEADER_NAME2, HEADER_NAME3, HEADER_ALT_NAMES, 
        HEADER_EXECUTIVE, HEADER_COUNTRY, HEADER_STATE, 
        HEADER_CITY, HEADER_POSTCODE, HEADER_STREET, 
        HEADER_EMAIL, HEADER_URL, HEADER_PHONE, HEADER_FAX
    };
    
    /** CSV field delimiter */
    public static final char DELIMITER = ',';

    /** Encoding of the CSV data source file  */
    public static final String CSV_ENCODING = "UTF-8";

    private List<Institution> institutions;

    private URL url;
    
    public InstitutionTable() {
        institutions = new ArrayList<Institution>();
//        System.out.println( ">>> InstitutionTable()" );
    }

    /**
     * Load this table from the input reader
     * 
     * @see #load(CsvReader)
     * @throws IOException
     * @throws RuntimeException if something is wrong with the input CSV file
     */
    public void load( Reader reader ) throws IOException {
        CsvReader cvsr = new CsvReader( reader, DELIMITER );
        load( cvsr );
    }

    /** 
     * Load this table from the input URL 
     * @see #load(CsvReader)
     * @throws IOException 
     */
    public void load( URL url ) throws IOException {
        this.url = url;
        InputStream stream = url.openStream();
        load( stream );
    }

    /**
     * Load this table  from the input CsvReader.
     * 
     * Columns in the input CSV file are copied to Institution fields
     * according to the following scheme:
     * <table>
     * <tr><th>Field</th><th>Column header name</th></tr>
     * <tr><td>{@link Institution#setInstNo()}</td><td>{@value #HEADER_ID}</td></tr>
     * <tr><td>{@link Institution#setSiblingNo()}</td><td>{@value #HEADER_SIBLING}</td></tr> 
     * <tr><td>{@link Institution#setName1()}</td><td>{@value #HEADER_NAME1}</td></tr>
     * <tr><td>{@link Institution#setName2()}</td><td>{@value #HEADER_NAME2}</td></tr>
     * <tr><td>{@link Institution#setName3()}</td><td>{@value #HEADER_NAME3}</td></tr>
     * <tr><td>{@link Institution#setAltNames()}</td><td>{@value #HEADER_ALT_NAMES}</td></tr>
     * <tr><td>{@link Institution#setCountry()}</td><td>{@value #HEADER_COUNTRY}</td></tr>
     * <tr><td>{@link Institution#setState()}</td><td>{@value #HEADER_STATE}</td></tr>
     * <tr><td>{@link Institution#setExecutive()}</td><td>{@value #HEADER_EXECUTIVE}</td></tr>
     * <tr><td>{@link Institution#setCity()}</td><td>{@value #HEADER_CITY}</td></tr>
     * <tr><td>{@link Institution#setPostcode()}</td><td>{@value #HEADER_POSTCODE}</td></tr>
     * <tr><td>{@link Institution#setStreet()}</td><td>{@value #HEADER_STREET}</td></tr>
     * <tr><td>{@link Institution#setEmail()}</td><td>{@value #HEADER_EMAIL}</td></tr>
     * <tr><td>{@link Institution#setUrl()}</td><td>{@value #HEADER_URL}</td></tr>
     * <tr><td>{@link Institution#setPhone()}</td><td>{@value #HEADER_PHONE}</td></tr>
     * <tr><td>{@link Institution#setFax()}</td><td>{@value #HEADER_FAX}</td></tr>
     * <table>
     * @throws IOException 
     * @throws RuntimeException if something is wrong with the input CSV file
     */
    public void load( CsvReader reader ) throws IOException {
        
        // Initial checks
        checkHeaders( reader );
        
        while( reader.readRecord() ) {

            Institution i = new Institution();
            
            try {
                i.setInstNo( Long.parseLong( reader.get( HEADER_ID ).trim() ));
            }
            catch( NumberFormatException e ) {
                i.setInstNo( null );
            }
            
            try {
                i.setSiblingNo( Long.parseLong( reader.get( HEADER_SIBLING ).trim() ));
            }
            catch( NumberFormatException e ) {
                i.setSiblingNo( null );
            }
            
            i.setName1(     reader.get( HEADER_NAME1 ).trim() );
            i.setName2(     reader.get( HEADER_NAME2 ).trim() );
            i.setName3(     reader.get( HEADER_NAME3 ).trim() );
            i.setAltNames(  reader.get( HEADER_ALT_NAMES ).trim() );
            i.setCountry(   reader.get( HEADER_COUNTRY ).trim() );
            i.setState(     reader.get( HEADER_STATE ).trim() );
            i.setExecutive( reader.get( HEADER_EXECUTIVE ).trim() );
            i.setCity(      reader.get( HEADER_CITY ).trim() );
            i.setPostcode(  reader.get( HEADER_POSTCODE ).trim() );
            i.setStreet(    reader.get( HEADER_STREET ).trim() );
            i.setEmail(     reader.get( HEADER_EMAIL  ).trim() );
            i.setUrl(       reader.get( HEADER_URL  ).trim() );
            i.setPhone(     reader.get( HEADER_PHONE  ).trim() );
            i.setFax(       reader.get( HEADER_FAX  ).trim() );
            
            // skip empty institutions
            if( i.getName1().length() == 0 && 
                i.getName2().length() == 0 && 
                i.getName3().length() == 0 ) {
                continue;
            }
            
            add( i );
        }
    }

    /**
     * If all is well, nothing happens, otherwise an exception is thrown
     * @throws IOException
     * @throws RuntimeException
     */
    private void checkHeaders( CsvReader reader ) throws IOException,
            RuntimeException {
        
        // Check that we have headers
        boolean headersOK = reader.readHeaders();
        if( ! headersOK ) {
            throw new RuntimeException( "Input CSV file has no column headers" );
        }
        
        // check that we have all required headers
        StringBuilder errorMessage = new StringBuilder();
        String[] headers = reader.getHeaders();
        for( int i = 0; i < ALL_HEADERS.length; i++ ) {
            String requiredHeader = ALL_HEADERS[i];
            boolean found = false;
            for( int j = 0; j < headers.length; j++ ) {
                if( headers[j].equals( requiredHeader )) {
                    found = true;
                    break;
                }
            }
            if( ! found ) {
                String msg = "Required header '" + 
                             requiredHeader + 
                             "' was not found in the input CSV file\n";
                errorMessage.append( msg );
            }
        }
        if( errorMessage.length() > 0 ) {
            throw new RuntimeException( errorMessage.toString() );
        }
    }

    /**
     * Add an institution to this table
     * 
     * @return <code>true</code> if the input institution was added,
     *         <code>false</code> otherwise (the institution was already
     *         present)
     */
    private boolean add( Institution institution ) {
        return institutions.add( institution );
    }

    /** 
     * Load this table from a file
     * @param pathname The path to the file to use as the data source 
     * @see #load(CsvReader)
     * @throws IOException
     * @throws RuntimeException if something is wrong with the input CSV file
     */
    public void load( String pathname ) throws IOException {
        CsvReader cvsr = new CsvReader( pathname, DELIMITER );
        load( cvsr );
    }

    /**
     * Load this table from an input stream
     * 
     * @param stream
     *            An input stream for the the data source; it is assumed that
     *            the stream is UTF-8 encoded. <br/>
     *            A byte order mark (BOM), as produced e.g. by Windows Notepad,
     *            is also supported. See http://en.wikipedia.org/wiki/UTF-8 for
     *            more info.
     * @see #load(CsvReader)
     * @throws IOException
     * @throws RuntimeException
     *             if something is wrong with the input CSV file
     */
    public void load( InputStream stream ) throws IOException {
        // Fix for http://jira.alma.cl/browse/COMP-5418
        // See if the input file has a Byte Order Mark, as generated e.g.
        // by Notepad on Windows; if so, skip it.
        // For more info, see sec. "Byte order mark" of 
        // http://en.wikipedia.org/wiki/UTF-8
        final byte[] BOM = { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF };
        final int nBOM = BOM.length;
        BufferedInputStream bufferedStream = new BufferedInputStream( stream );
        
        byte[] firstChars = new byte[nBOM];
        bufferedStream.mark( 100 );
        int read = bufferedStream.read( firstChars, 0, nBOM );
        if( 0 <= read && read < nBOM ) {
            final String msg = 
                "Error reading the first chars of the input stream: got " +
                read +
                ", expected " +
                nBOM;
            throw new IOException( msg );
        }
        if( ! Arrays.equals( BOM, firstChars )) {
            bufferedStream.reset();
        }
        // end of fix for COMP-5418

        CsvReader cvsr = new CsvReader( bufferedStream, 
                                        Charset.forName( CSV_ENCODING ));
        load( cvsr );
    }
    
    /** @return The number of institutions in this list */
    public int size() {
        return institutions.size();
    }

    /**
     * @return The institution at the given index in the table
     * @throws IllegalArgumentException if index is negative or too large
     */
    public Institution get( int index ) {
        if( index < 0 || index >= size() ) {
            throw new IllegalArgumentException( "Invalid index" );
        }
        return institutions.get( index );
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append( this.getClass().getSimpleName() )
          .append( "[size=" )
          .append( size() )
          .append( ",url=" )
          .append( url )
          .append( "]" );
        return sb.toString();

    }
    
//    /**
//     * @return An XML representation of this table; the generated XML is
//     *         guaranteed to conform to {@link #SCHEMA_FILE}
//     */
//    public String toXML() {
//        StringBuilder sb = new StringBuilder();
//        sb.append( "<?xml version=\"1.0\" encoding=\"" )
//          .append( XML_ENCODING )
//          .append( "\"?>\n<institutions>\n" );
//        for( Institution institution : institutions ) {
//            sb.append( institution.toXML() ).append( "\n" );
//        }
//        sb.append( "</institutions>\n" );
//        return sb.toString();
//    }

    /**
     * Query this table and return all institutions matching the input
     * country -- exact match.
     */
    InstitutionTable queryByCountry( String country ) {
        if( country == null ) {
            throw new IllegalArgumentException( "Null country" );
        }
        InstitutionTable ret = new InstitutionTable();
        for( Institution institution : institutions ) {
            if( country.equals( institution.getCountry() )) {
                ret.add( institution );
            }
        }
        return ret;
    }

    /**
     * Query this table and return all institutions matching the input country
     * and state -- exact match for both. If institutions with an empty state
     * name should be returned, give {@link Institution#NO_NAME_STATE} as value
     * of the state parameter.
     * 
     * 
     * @param country
     *            Country name, should not be <code>null</code>
     * @param state
     *            If <code>null</code> or empty, state information will not be
     *            considered when querying
     */
    InstitutionTable queryByCountryState( String country, String state ) {
        if( country == null ) {
            throw new IllegalArgumentException( "Null country" );
        }
        
        boolean anyState = false;
        if( state == null ) {
            anyState = true;
        }
        
        InstitutionTable countryList = this.queryByCountry( country );
        InstitutionTable ret = new InstitutionTable();
        for( Institution institution : countryList.getInstitutions() ) {
            String institutionState = institution.getState();
            if( anyState || 
                state.equals( institution.getState() ) || 
                ( state.equals( Institution.NO_NAME_STATE ) && 
                  institutionState.length() == 0 )) {
                ret.add( institution );
            }
        }
        return ret;
    }

    /**
     * Query this table and return all institutions matching the input country,
     * state and institution name. An exact match is performed for both country
     * and state; a partial regexp match for name.
     * 
     * @param country
     *            Country name: "Austria", "Japan", "Chile", etc. Should not be
     *            <code>null</code> or empty.
     * 
     * @param state
     *            State code: must be a state of the given country. If
     *            <code>null</code> or empty, state information will not be
     *            considered when querying. If institutions with
     *            an empty state name should be returned, give
     *            {@link Institution#NO_NAME_STATE} as value of this parameter.
     * 
     * @param nameRE
     *            A regular expression: institution names will be matched
     *            against it. A partial match will be sufficient for triggering
     *            inclusion in the returned set; that is, the RE does not need
     *            to match the entire institution name.<br/>
     *            For instance, a value of "^A" will match all institution names
     *            starting with "A"; a value of "nomy$" all names ending with
     *            "nomy"; etc.
     *            <br/>
     *            Should not be <code>null</code>; an empty string will match
     *            any institution name.
     */
    public InstitutionTable queryByCountryStateName( String country, 
                                                     String state,
                                                     String nameRE ) {
        if( nameRE == null ) {
            throw new IllegalArgumentException( "Null name regexp" );
        }
        
        InstitutionTable temp = queryByCountryState( country, state );
        InstitutionTable ret = new InstitutionTable();
        Pattern pattern = Pattern.compile( nameRE );

        for( Institution institution : temp.getInstitutions() ) {
            String iName = institution.getName1();
            Matcher matcher = pattern.matcher( iName );
            if( matcher.find() ) {
                ret.add( institution );
            }
        }
        return ret;
    }

    /**
     * @return The internal list of institutions
     */
    public List<Institution> getInstitutions() {
        return institutions;
    }

    /**
     * @return The list of unique states for a given country in this table
     */
    public String[] getStates( String country ) {
        
        Set<String> stateSet = new TreeSet<String>();
        InstitutionTable table = queryByCountry( country );
        
        boolean hasEmptyStates = false;     // assume no empty states
        for( Institution institution : table.getInstitutions()) {
            String state = institution.getState();
            if( state != null && state.length() > 0 ) {
                stateSet.add( state );
            }
            else {
                hasEmptyStates = true;
            }
        }
        if( hasEmptyStates && stateSet.size() > 0 ) {
            stateSet.add( Institution.NO_NAME_STATE );
        }
        String[] states = stateSet.toArray( new String[0] );
        return states;
    }

    /**
     * @return The list of unique countries in this table
     */
    public String[] getCountries() {
        Set<String> t = new HashSet<String>( 100 );
        for( Institution institution : getInstitutions() ) {
            String country = institution.getCountry();
            if (country != null) {
                t.add( country );
            }
        }
        String[] countries = t.toArray( new String[0] );
        Arrays.sort( countries );
        return countries;
    }

    /**
     * @return The institution with the given institution number, or
     *         <code>null</code> if none was found.
     */
    public Institution queryById( long index ) {
        for( Institution institution : getInstitutions() ) {
            Long instNo = institution.getInstNo();
            if( instNo != null && instNo.longValue() == index ) {
                return institution;
            }
        }
        return null;
    }

    /**
     * @return The list of institutions with the given name; it can be empty
     *         but it's never <code>null</code>. An exact match is performed
     *         against field <code>name1</code>.
     */
    public InstitutionTable queryByName( String name ) {
        InstitutionTable ret = new InstitutionTable();
        for( Institution institution : getInstitutions() ) {
            String tmp = institution.getName1();
            if( tmp != null && tmp.equals( name )) {
                ret.add( institution );
            }
        }
        return ret;
    }

    /**
     * Store a new user institution in the table.
     * <p/>
     * 
     * Note that this implementation <em>does not</em> persist the user user
     * institution; it will be lost after a system restart.
     * 
     * @param userID
     *            ID of the user registering the institution; will be stored in
     *            field 'recorder' of the institution
     *            
     * @param institutionDesc
     *            A free-text description of the institution; will be stored in
     *            field 'name1' of the institution
     *            
     * @return The institution number of the new institution, guaranteed to be
     *         negative.
     */
    public int addUserInstitution( String userID, String institutionDesc ) {
        
        // compute a unique negative ID
        long instNo = Math.min( 0L, findMinInstNo() );
        instNo -= 1;
        Institution institution = new Institution();
        institution.setInstNo( instNo );
        institution.setName1( institutionDesc );
        institution.setRecorder( userID );
        // The value of the executive should be
        //    InvestigatorTAssociatedArcType.OTHER.toString().toLowerCase()
        // but I don't want to make this module depend on the APDM
        // and in any case this is a junk class I want to get rid of soon
        institution.setExecutive( "other" ); 
        institutions.add( institution );
        
        return (int) instNo;
    }

    /**
     * @return the lowest institution number
     */
    private long findMinInstNo() {
        long ret = Integer.MAX_VALUE;
        for( Institution institution : institutions ) {
            final long instNo = institution.getInstNo();
            if( instNo < ret ) {
                ret = instNo;
            }
        }
        return ret;
    }
}
