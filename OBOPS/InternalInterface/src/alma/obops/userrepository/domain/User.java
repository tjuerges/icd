/**
 * AlmaUser.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.util.List;


/**
 * Represents an entry in the ALMA User Repository; only Portal-related
 * attributes are considered.
 * 
 * @author amchavan, Aug 19, 2010
 * @version $Revision: 1.3 $
 */

// $Id: User.java,v 1.3 2011/02/03 10:28:56 rkurowsk Exp $

public interface User {
    
    public String getDescription();

    public String getEmail();

    public String getExecutive();

    public String getFirstName();

    public String getFullName();
    
    /**
     * @return The ID of the Institution this user belongs to
     */
    public Long getInstitutionID();
    
    public String getPreferredArc();

    /**
     * @return The list of all roles assigned to this user at the time it was
     *         retrieved from the repository.
     */
    public List<Role> getRoles();
    
    public String getSurname();
    
    public String getUid();
    
    public void setDescription( String description );
    
    public void setEmail( String email );
    
    public void setExecutive( String executive );
    
    public void setFirstName( String firstName );
    
    public void setFullName( String fullName );

    /**
     * @param institutionID The ID of the Institution this user belongs to
     */
    public void setInstitutionID( Long institutionID );
    
    public void setPreferredArc( String preferredArc );
    
    public void setSurname( String surname );

    public void setUid( String uid );
}
