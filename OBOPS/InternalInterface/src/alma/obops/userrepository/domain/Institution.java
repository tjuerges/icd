/**
 * Institution.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.HashMap;

import alma.entity.xmlbinding.obsproposal.types.InvestigatorTAssociatedArcType;

/**
 * Represents a scientific institution
 *
 * @author amchavan, Oct 25, 2010
 * @version $Revision$
 */

// $Id$

public class Institution {

    // syntactic sugar
    // TODO remove these definitions when we have proper exec definitions in
    //      the APDM
    private static final String EA_EXEC = InvestigatorTAssociatedArcType.EA.toString().toLowerCase();
    private static final String NA_EXEC = InvestigatorTAssociatedArcType.NA.toString().toLowerCase();
    private static final String EA_NA_EXEC = EA_EXEC + "/" + NA_EXEC;
    
    /**
     * Used to mark an institution as <em>user-defined</em>; that is, entered by
     * the user on the Portal registration page. It populates the 'country'
     * field of the institution.<br/>
     * WARNING: if you change this, make sure that report definitions are updated
     */
    // By prefixing the value with "AAA" we make it come up first in sorted lists
    public static final String USER_DEFINED = "AAA-USER-DEFINED";

    /**
     * Conventional name of the "state" with no name; it appears in the list of
     * states of a country when some institutions of that country have state
     * information and other institutions do not. (That's a data quality error,
     * in fact.)
     * <br/>
     * The value of this constant should be such that it is likely
     * to end up as the first element of a sorted list of state
     * names.
     */
    public static final String NO_NAME_STATE = "(other)";

    /**
     * @param institution
     * 
     * @return The list of default ALMA Regional Center codes for the input
     *         Institution, based on the Institution's executive; elements of
     *         the list are one the the possible ARCs, see
     *         {@link AlmaRegionalCenter}. <br/>
     *         If no default ARCs can be determined (because no Executive info
     *         is available or there is no such default ARC), an empty list is
     *         returned instead.
     */
    public static String[] getDefaultARCs( final Institution institution ) {
        
        String[] none = new String[0];
        
        String executive = institution.getExecutive();
        if( executive == null || executive.length() == 0 ) {
            return none;
        }
        executive = executive.toLowerCase();    // just in case
        
        // EA, EU and NA have ARCs, everyone else does not
        // ----------------------------------------------------
        for( AlmaRegionalCenter arc : AlmaRegionalCenter.values() ) {
            String arcName = arc.name().toLowerCase();

            if( executive.equals( arcName ) ) {
                String[] ret = new String[1];
                ret[0] = executive;
                return ret;
            }
        }
        
        // Special case: Taiwanese institutions can refer to either the
        // EA or NA ARCs
        //-------------------------------------------------------------
        if( executive.equals( EA_NA_EXEC )) { 
                
                String[] ret = new String[2];
                ret[0] = AlmaRegionalCenter.EA.name().toLowerCase();
                ret[1] = AlmaRegionalCenter.NA.name().toLowerCase();;
                return ret;
        }
        
        // In all other cases there are no default ARCs
        //---------------------------------------------
        return none;
    }
    
    protected Long   instNo;
    protected Long   siblingNo;
    protected String name1;
    protected String name2;
    protected String name3;
    protected String altNames;
    protected String city;
    protected String postcode;
    protected String street;
    protected Long   supersedingNo;
    protected String executive;
    protected String country;
    protected String state;
    protected String email;
    protected String url;
    protected String phone;
    protected String fax;
    
    protected String recorder;

    // Caches an XML representation of this institution (small optimization)
    protected String xml;
    
//    // Make sure we produce valid XML
//    private String encodeXML( String in ) {
//        return in.replaceAll( "&", "&amp;" )
//                 .replaceAll( "<", "&lt;" )
//                 .replaceAll( ">", "&gt;" );
//    }

    /** Zero-arg constructor */
    public Institution() {
    }

    public String getAltNames() {
        return altNames;
    }
    
    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getExecutive() {
        return executive;
    }

    /**
     * @return <code>true</code> if this Institution's executive is a proper
     *         one, that is, one of the values of the
     *         {@link InvestigatorTAssociatedArcType} enumeration.
     */
    public boolean hasRealExecutive() {
        Enumeration<?> e = InvestigatorTAssociatedArcType.enumerate();
        while( e.hasMoreElements() ) {
            if( e.nextElement().toString().toLowerCase().equals( executive )) {
                return true;
            }
        }
        return false;
    }

    /**
     * Implementation note: user-defined institutions have their
     * {@link #country} field set to {@value #USER_DEFINED}
     * 
     * @return <code>true</code> if this is a user-defined institution;
     *         <code>false</code> otherwise
     */
    public boolean isUserDefined() {
        if( getCountry() == null ) {
            return false;
        }
        return getCountry().equals( USER_DEFINED );
    }
    
    /**
     * Mark this Institution as being user-defined.<br/>
     * Implementation note: user-defined institutions have their
     * {@link #country} field set to {@value #USER_DEFINED}
     */
    public void setUserDefined() {
        this.country = USER_DEFINED;
    }
    

    
    public String getFax() {
        return fax;
    }

    public Long getInstNo() {
        return instNo;
    }
    
    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    public String getName3() {
        return name3;
    }

    public String getPhone() {
        return phone;
    }

    public String getPostcode() {
        return postcode;
    }

    /**
     * @return The ID of the user who recorded this institution. It can be
     *         <code>null</code>, in which case the institution is an 'official'
     *         one.
     */
    public String getRecorder() {
        return recorder;
    }

    public Long getSiblingNo() {
        return siblingNo;
    }

    public String getState() {
        return state;
    }

    public String getStreet() {
        return street;
    }

    public Long getSupersedingNo() {
        return supersedingNo;
    }

    public String getUrl() {
        return url;
    }

    public String getXml() {
        return xml;
    }

    public void setAltNames( String altNames ) {
        this.altNames = altNames;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    public void setCountry( String country ) {
        this.country = country;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public void setExecutive( String executive ) {
        this.executive = executive.toLowerCase();   // just in case
    }

    public void setFax( String fax ) {
        this.fax = fax;
    }

    public void setInstNo( Long instNo ) {
        this.instNo = instNo;
    }

    public void setName1( String name1 ) {
        this.name1 = name1;
    }

    public void setName2( String name2 ) {
        this.name2 = name2;
    }

    public void setName3( String name3 ) {
        this.name3 = name3;
    }

    public void setPhone( String phone ) {
        this.phone = phone;
    }

    public void setPostcode( String postcode ) {
        this.postcode = postcode;
    }

    /**
     * @param recorder
     *            ID of the user who recorded this institution. It can be
     *            <code>null</code>, in which case the institution is an
     *            'official' one.
     */
    public void setRecorder( String recorder ) {
        this.recorder = recorder;
    }

    public void setSiblingNo( Long siblingNo ) {
        this.siblingNo = siblingNo;
    }

    public void setState( String state ) {
        this.state = state;
    }

    public void setStreet( String street ) {
        this.street = street;
    }

    public void setSupersedingNo( Long supersedingNo ) {
        this.supersedingNo = supersedingNo;
    }

    public void setUrl( String url ) {
        this.url = url;
    }

    /**
     * @return This object converted to a map of [<em>fieldName</em>,
     *         <em>fieldValue</em>] pairs, where <em>fieldName</em> is the name
     *         of a field of this class and <em>fieldValue</em> is that field's
     *         value; for instance
     *         <code>[name1=ESO,executive=EU,city=Garching]</code>.<br/>
     *         Note that fields having a <code>null</code> value are not
     *         included in the map.
     *         <br/>
     *         The complete list of fields is <em>not final</em> and includes:
     *         <ul>
     *         <li>altNames
     *         <li>city
     *         <li>country
     *         <li>email
     *         <li>executive
     *         <li>fax
     *         <li>instNo
     *         <li>name1
     *         <li>name2
     *         <li>name3
     *         <li>phone
     *         <li>postcode
     *         <li>recorder
     *         <li>siblingNo
     *         <li>state
     *         <li>street
     *         <li>url
     *         </ul>
     * 
     */
    public HashMap<String, Object> toHashMap() {
        
        HashMap<String, Object> ret = new HashMap<String, Object>();
        
        Method[] methods = this.getClass().getMethods();
        
        for( int i = 0; i < methods.length; i++ ) {

            // Skip all non-getter methods as well as all static methods
            Method method = methods[i];
            String methodName = method.getName();
            
            if( (! methodName.startsWith( "get" )) || 
                   methodName.equals( "getClass" ) ||
                   Modifier.isStatic( method.getModifiers() )) {
                continue;
            }
            
            try {
//                System.out.println( method.getName() );
                Object value = method.invoke( this );
                if( value == null ) {
                    // skip null values
                    continue;
                }
                    
                if( value instanceof Long ) {
                    // Longs are not supported by Python -- need to
                    // convert to Integers
                    Long ltmp = (Long) value;
                    Integer itmp = new Integer( ltmp.intValue() );
                    value = itmp;
                }   
                
                // convert from the method name to the field name:
                // getInstNo ==> instNo
                String keyword = methodName.substring( 3 );   
                keyword = keyword.substring( 0, 1 ).toLowerCase() +
                          keyword.substring( 1 );
                ret.put( keyword, value );
            }
            catch( Exception e ) {
                // should never happen
                e.printStackTrace();
                throw new RuntimeException( e );
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append( this.getClass().getSimpleName() )
          .append( "[" )
          .append( getInstNo() )
          .append( "," )
          .append( getName1() )
          .append( "," )
          .append( getCountry() )
          .append( "," )
          .append( getState() )
          .append( "," )
          .append( getExecutive() )
          .append( "," )
          .append( getStreet() )
          .append( "]" );
        return sb.toString();
    }
    

// KEEP JUST IN CASE
//    /**
//     * @return An XML representation of this Institution; the generated XML
//     *         element is guaranteed to conform to the definition of
//     *         <em>institution</em> in {@link #SCHEMA_FILE}.<br/>
//     *         The returned string contains no extra whitespace.
//     */
//    public String toXML() {
//        
//        // Small optimization: if we already computed this string, let's
//        // just return it
//        if( xml != null ) {
//            return xml;
//        }
//        
//        StringBuilder sb = new StringBuilder();
//        sb.append( "<institution><id>" )
//          .append( getInstNo() )
//          .append( "</id><name>" )
//          .append( encodeXML( getName1() ))
//          .append( "</name><country>" )
//          .append( getCountry() )
//          .append( "</country><exec>" )
//          .append( getExecutive() )
//          .append( "</exec>" );
//        
//        String address = getStreet();
//        if( address != null  && address.length() > 0 ) {
//          sb.append( "<address>" )
//            .append( encodeXML( address ))
//            .append( "</address>" );
//        }
//        
//        String state = getState();
//        if( state != null  && state.length() > 0 ) {
//          sb.append( "<state>" ).append( getState() ).append( "</state>" );
//        }
//        
//        sb.append( "</institution>" );
//        
//        xml = sb.toString();
//        return xml;
//    }
}
