/**
 * InstitutionMapComparator.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.userrepository.domain;

import java.util.Comparator;
import java.util.HashMap;


/**
 * Compares two Institution maps based on the concatenation of elements 
 * <em>name1</em>, <em>name2</em> and <em>name3</em>.
 * 
 * @see Institution#toHashMap()
 * 
 * @author amchavan, Mar 4, 2011
 * @version $Revision: 1.2 $
 */

// $Id: InstitutionMapComparator.java,v 1.2 2011/03/09 15:19:28 fjulbe Exp $

public class InstitutionMapComparator 
    implements Comparator<HashMap<String,Object>> {

    /**
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare( HashMap<String, Object> map1,
                        HashMap<String, Object> map2 ) {
        String key1 = makeKey( map1 );
        String key2 = makeKey( map2 );
        return key1.compareTo( key2 );
    }
    
    private String makeKey( HashMap<String, Object> map ) {
        StringBuilder sb = 
            new StringBuilder( map.get( "name1" ).toString() )
                      .append( map.get( "name2" ).toString() )
                      .append( map.get( "name3" ).toString() );
        return sb.toString();
    }
}