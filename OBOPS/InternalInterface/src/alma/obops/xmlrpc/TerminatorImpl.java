/**
 * TerminatorImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;


/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: TerminatorImpl.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class TerminatorImpl implements Terminator {

    /**
     * @see alma.obops.xmlrpc.Terminator#terminate()
     */
    @Override
    public boolean terminate() {
        ConfigurableServletServer.terminate();
        return true;
    }
}
