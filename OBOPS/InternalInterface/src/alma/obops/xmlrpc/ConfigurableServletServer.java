package alma.obops.xmlrpc;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.xmlrpc313.XmlRpcException;
import org.apache.xmlrpc313.server.XmlRpcServer;
import org.apache.xmlrpc313.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc313.webserver.ServletWebServer;

/**
 * A simple Web server for XML-RPC testing purposes. <br/>
 * It uses a single ConfigurableXmlRpcServlet
 * 
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.3 $
 */
public class ConfigurableServletServer {
    
    private static ConfigurableServletServer lastInstance;  // semi-singleton ;-)
    
    private ServletWebServer webServer;
    
    /**
     * Shuts down the most recently created instance of this class -- a
     * semi-singleton, if you will :-)
     */
    public static void terminate() {
        lastInstance.shutdown();
    }
    
    /**
     * Creates a simple Web server for XML-RPC testing purposes.
     * 
     * @param cloader
     *            The class loader being used to load handler classes and
     *            resource files
     * @param resource
     *            The resource being used, for example
     *            <em>org/apache/xmlrpc/webserver/XmlRpcServlet.properties</em>.<br/>
     *            The property file contains a set of properties: the property
     *            key is taken as the handler name, the property value is taken
     *            as the name of a class being instantiated. (For any non-void,
     *            non-static, and public method in the class, an entry in the
     *            handler map is generated.) <br/>
     *            A typical use would be to specify interface names as the
     *            property keys and implementations as the values:<br/>
     *            <code>
     *            &nbsp;&nbsp;&nbsp;org.apache.xmlrpc.demo.Adder=org.apache.xmlrpc.demo.AdderImpl
     *            </code>
     * @param port
     *            The port the server will listen on
     * @throws IOException
     * @throws ObopsXmlRpcException 
     */
    public ConfigurableServletServer( ClassLoader cloader, 
                                      String resource, 
                                      int port )
        throws IOException, ObopsXmlRpcException {
        try {
            ConfigurableXmlRpcServlet servlet = 
                new SimpleXmlRpcServlet( cloader, resource );
            webServer = new ServletWebServer( servlet, port );

            XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();

//            XmlRpcServer xmlRpcServer = servlet.gets
            XmlRpcServerConfigImpl serverConfig =
                (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
            serverConfig.setEnabledForExtensions( true );
            serverConfig.setContentLengthOptional( false );
            
//            XmlRpcServerConfigImpl conf = new XmlRpcServerConfigImpl();
//            conf.setEnabledForExtensions( true );
//            conf.setEnabledForExceptions( true );
//            conf.setContentLengthOptional( true );
//            conf.setTimeZone( TimeZone.getTimeZone( "GMT" ));
//            
//            webServer.getXmlRpcServer().setConfig( conf );
            
        }
        catch( XmlRpcException e ) {
            throw new ObopsXmlRpcException( e );
        }
        catch( ServletException e ) {
            throw new ObopsXmlRpcException( e );
        }
        lastInstance = this;
    }
    
    /**
     * Run this server
     * @throws IOException
     */
    public void start() throws IOException {
        webServer.start();
//        XmlRpcServerConfigImpl t = 
//            (XmlRpcServerConfigImpl) webServer.getXmlRpcServer().getConfig();
//        System.out.println( "isEnabledForExtensions=" + t.isEnabledForExtensions() );
    }
    
    /**
     * Shutdown this server. The shutdown procedure is launched on a separate
     * thread after some delay to avoid unpleasant consequences for the client.
     * <br/>
     * Clients should use method {@link ConfigurableServletServer#terminate()}.
     */
    public void shutdown() {
        Runnable runnable = new Runnable () {

            @Override
            public void run() {
                try {
                    Thread.sleep( 10 );
                    webServer.shutdown();
                }
                catch( InterruptedException e ) {
                    // ignore this
                }
            }
        };
        Thread thread = new Thread( runnable, "Terminator" );
        thread.start();
    }
}