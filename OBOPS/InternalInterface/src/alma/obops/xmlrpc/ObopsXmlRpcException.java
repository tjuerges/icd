/**
 * XmlRpcException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: ObopsXmlRpcException.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class ObopsXmlRpcException extends Exception {

    private static final long serialVersionUID = 1296774435074219120L;
    private Throwable cause;

    public ObopsXmlRpcException() {
        super();
    }

    public ObopsXmlRpcException( String message ) {
        super( message );
    }

    public ObopsXmlRpcException( Throwable cause ) {
        super( cause );
        this.cause = cause;
    }

    @Override
    public String getMessage() {
        if( cause != null ) {
            return cause.getMessage();
        }
        return super.getMessage();
    }
}
