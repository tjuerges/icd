/**
 * TestInstitutionsXmlRpc.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

import junit.framework.TestCase;

import org.apache.xmlrpc313.client.util.ClientFactory;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: XmlRpcTestCase.java,v 1.2 2011/02/23 10:23:45 rkurowsk Exp $

public abstract class XmlRpcTestCase extends TestCase {

    private static final int BASE_PORT = 7000;
    private static int port = BASE_PORT;
    
    protected ConfigurableServletServer server;
    protected String mappings;
    protected ClientFactory clientFactory;
    protected ConfigurableXmlRpcClient client;
    
    public XmlRpcTestCase( String mappings ) {
        this.mappings = mappings;
    }
    
    // Launches an XML-RPC server
    @Override
    public void setUp() {
        try {
            ClassLoader cloader = this.getClass().getClassLoader();
            server = new ConfigurableServletServer( cloader, mappings, port );
            server.start();
            
            final String serverURL = "http://localhost:" + port + "/xmlrpc";
            client = new ConfigurableXmlRpcClient( serverURL, 60, 60 );
            clientFactory = new ClientFactory( client );
        }
        catch( Exception e ) {
            e.printStackTrace();
           fail( e.getMessage() );
        }
    }

    // Terminates the XML-RPC server
    @Override
    public void tearDown() {
        server.shutdown();
        port++;
    }
}
