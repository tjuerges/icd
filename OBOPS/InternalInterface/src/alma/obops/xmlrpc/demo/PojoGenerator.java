/**
 * PojoGenerator.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc.demo;

import java.util.HashMap;
import java.util.List;

/**
 * @author amchavan, Nov 9, 2010
 * @version $Revision: 1.2 $
 */

// $Id: PojoGenerator.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public interface PojoGenerator {

    public HashMap<String,Object> getPojo();
    
    public List<HashMap<String, Object>> getPojos( int n );
}
