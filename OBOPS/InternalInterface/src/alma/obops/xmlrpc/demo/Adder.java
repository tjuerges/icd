package alma.obops.xmlrpc.demo;

//$Id: Adder.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public interface Adder {
    public int add( int n1, int n2 );
}
