/**
 * PojoGeneratorImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * TODO
 *
 * @author amchavan, Nov 9, 2010
 * @version $Revision: 1.2 $
 */

// $Id: PojoGeneratorImpl.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class PojoGeneratorImpl implements PojoGenerator {

    private static int counter = 0;
    
    /**
     * @see alma.obops.xmlrpc.demo.PojoGenerator#getPojo()
     */
    @Override
    public HashMap<String, Object> getPojo() {
        return getPojos( 1 ).get( 0 );
    }

    /**
     * @see alma.obops.xmlrpc.demo.PojoGenerator#getPojos(int)
     */
    @Override
    public List<HashMap<String, Object>> getPojos( int n ) {
        List<HashMap<String, Object>> ret = 
            new ArrayList<HashMap<String, Object>>();
        for( int i = 0; i < n; i++ ) {
            String name = Pojo.POJO_BASE_NAME + counter;
            Pojo pojo = new Pojo( name, counter );
            ret.add( pojo.toHashMap() );
            counter++;
        }
        return ret;
    }

}
