/**
 * EchoImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc.demo;

import java.util.HashMap;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: EchoImpl.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class EchoImpl implements Echo {

    /**
     * @see alma.obops.xmlrpc.demo.Echo#echo(java.lang.String)
     */
    @Override
    public String echo( String in ) {
        return in;
    }

    /**
     * @see alma.obops.xmlrpc.demo.Echo#echo(alma.obops.xmlrpc.demo.Pojo)
     */
    @Override
    public Pojo echo( Pojo in ) {
        return in;
    }

    /**
     * @see alma.obops.xmlrpc.demo.Echo#echo(java.util.HashMap)
     */
    @Override
    public HashMap<String, Object> echo( HashMap<String, Object> in ) {
        return in ;
    }
}
