/**
 * Echo.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc.demo;

import java.util.HashMap;

/**
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: Echo.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public interface Echo {

    /** 
     * @return the input string, unchanged
     */
    public String echo( String in );

    /** 
     * @return the input POJO, unchanged
     */
    public Pojo echo( Pojo in );
    
    /** 
     * @return the input HashMap, unchanged
     */
    public HashMap<String,Object> echo( HashMap<String,Object> in );
}
