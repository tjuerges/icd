/**
 * Pojo.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc.demo;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author amchavan, Nov 8, 2010
 * @version $Revision: 1.2 $
 */

// $Id: Pojo.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class Pojo implements Serializable {
    private static final long serialVersionUID = -884142100006181720L;

    public static final String VALUE_KEY = "value";
    public static final String NAME_KEY = "name";
    public static final String POJO_BASE_NAME = "pojo";
    
    private String name;
    private int value;


    public Pojo( String name, int value ) {
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return name;
    }
    public int getValue() {
        return value;
    }
    public void setName( String name ) {
        this.name = name;
    }
    public void setValue( int value ) {
        this.value = value;
    }
    
    public HashMap<String,Object> toHashMap() {
        HashMap<String,Object> ret = new HashMap<String,Object>();
        ret.put( NAME_KEY, getName() );
        ret.put( VALUE_KEY, getValue() );
        
        return ret;
    }
}
