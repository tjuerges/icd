package alma.obops.xmlrpc.demo;

import alma.obops.xmlrpc.ConfigurableServletServer;


/**
 * Runs a demo server
 * 
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */
public class Server {
    
    private static final int PORT = 8080;
    private static final String DEMO_MAPPINGS = 
            "alma/obops/xmlrpc/demo/demo-handler-mappings.properties";
    
    public static void main( String[] ignored ) throws Exception {
        final ClassLoader cloader = Server.class.getClassLoader();
        ConfigurableServletServer server = 
            new ConfigurableServletServer( cloader, DEMO_MAPPINGS, PORT );
        server.start();
    }
}