package alma.obops.xmlrpc.demo;

import java.util.HashMap;

import org.apache.xmlrpc313.client.util.ClientFactory;

import alma.obops.xmlrpc.ConfigurableServletServer;
import alma.obops.xmlrpc.ConfigurableXmlRpcClient;
import alma.obops.xmlrpc.Terminator;

/**
 * Demo client for our {@link ConfigurableServletServer}.
 * 
 * @author amchavan, Nov 5, 2010; original version at
 *         http://ws.apache.org/xmlrpc/server.html
 *         
 * @version $Revision: 1.2 $
 */

// $Id: Client.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class Client {
    private static final String SERVER_URL = "http://localhost:8080/xmlrpc";

    public static void main( String[] args ) throws Exception {
        ConfigurableXmlRpcClient client = 
            new ConfigurableXmlRpcClient( SERVER_URL, 60, 60 );

        // make the a regular call
        Object[] params = new Object[] { 2, 3 };
        Integer result = 
            (Integer) client.execute( "alma.obops.xmlrpc.demo.Adder.add", params );
        System.out.println( "2 + 3 = " + result );

        // make a call using dynamic proxy
        ClientFactory factory = new ClientFactory( client );
        Adder adder = (Adder) factory.newInstance( Adder.class );
        int sum = adder.add( 2, 4 );
        System.out.println( "2 + 4 = " + sum );

        // Try the echo service for internationalized strings
        Echo echo = (Echo) factory.newInstance( Echo.class );
        String sSent = "abcd";
        String sBack = echo.echo( sSent );
        System.out.println( sSent + " --> " + sBack );
        sSent = "Michèle";
        sBack = echo.echo( sSent );
        System.out.println( sSent + " --> " + sBack );
        
        // Try the echo service to see if non-primitive objects
        // and hash maps can be exchanged
        Pojo pSent = new Pojo( "phone", 12345 );
        Pojo pBack = echo.echo( pSent );
        System.out.println( pSent.getName() + "=" + pSent.getValue() + " --> "
                          + pBack.getName() + "=" + pBack.getValue() );
        
        HashMap<String,Object> mSent = pSent.toHashMap();
        HashMap<String,Object> mBack = echo.echo( mSent );
        System.out.println( mSent.get( Pojo.NAME_KEY ) + "=" + mSent.get( Pojo.VALUE_KEY ) + " --> " 
                          + mBack.get( Pojo.NAME_KEY ) + "=" + mBack.get( Pojo.VALUE_KEY ) );
        
        // Try the Echo service to see if we can deal correctly with lists
        // of objects

        PojoGenerator pgen = (PojoGenerator) factory.newInstance( PojoGenerator.class );
        System.out.println( "pojo: " + pgen.getPojo() );
        for( HashMap<String,Object> pojo : pgen.getPojos( 3 )) {
            System.out.println( "pojo: " + pojo );
        }
        
        // Terminate the server
        Terminator terminator = 
            (Terminator) factory.newInstance( Terminator.class );
        boolean ret = false;
        try {
            ret = terminator.terminate();
        }
        catch( Throwable e ) {
            e.printStackTrace();
        }
        System.out.println( "terminate = " + ret );
    }
}