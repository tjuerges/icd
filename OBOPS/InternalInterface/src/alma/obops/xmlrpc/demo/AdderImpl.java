package alma.obops.xmlrpc.demo;

//$Id: AdderImpl.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class AdderImpl implements Adder {
    public int add( int n1, int n2 ) {
        return n1 + n2;
    }
}