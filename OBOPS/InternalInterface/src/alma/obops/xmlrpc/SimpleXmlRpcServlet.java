/**
 * SimpleXmlRpcServlet.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

import java.io.IOException;

import org.apache.xmlrpc313.XmlRpcException;

/**
 * A concrete implementation of AbstractXmlRpcServlet, for testing purposes.
 *
 * @author amchavan, Dec 8, 2010
 * @version $Revision: 1.2 $
 */

// $Id: SimpleXmlRpcServlet.java,v 1.2 2011/02/23 10:23:44 rkurowsk Exp $

public class SimpleXmlRpcServlet extends ConfigurableXmlRpcServlet {
    private static final long serialVersionUID = 690245374873603424L;

    /**
     * @see ConfigurableXmlRpcServlet
     */
    public SimpleXmlRpcServlet( ClassLoader cloader, String resource )
            throws IOException, XmlRpcException {
        super( cloader, resource );
    }
}
