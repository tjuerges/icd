package alma.obops.xmlrpc;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc313.client.XmlRpcClient;
import org.apache.xmlrpc313.client.XmlRpcClientConfigImpl;

/**
 * A basic XML-RPC client, for our {@link ConfigurableServletServer}.
 * Needed to hide XML-RPC-specific stuff from client code.
 * 
 * @author amchavan, Nov 5, 2010; original version at
 *         http://ws.apache.org/xmlrpc/server.html
 *         
 * @version $Revision: 1.2 $
 */

// $Id: ConfigurableXmlRpcClient.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class ConfigurableXmlRpcClient extends XmlRpcClient {
    
    /**
     * @param serverURL 
     * @param connectionTimeout In seconds
     * @param replyTimeout In seconds
     * @throws MalformedURLException
     */
    public ConfigurableXmlRpcClient( String serverURL, 
                                     int connectionTimeout, 
                                     int replyTimeout )
            throws MalformedURLException {
        super();
        
        // create configuration
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL( new URL( serverURL ));
        config.setEnabledForExtensions( true );
        config.setEnabledForExceptions( true );
        config.setContentLengthOptional( true );
        config.setConnectionTimeout( connectionTimeout * 1000 );
        config.setReplyTimeout( replyTimeout * 1000 );
        config.setEncoding( "ISO-8859-1" );
        setConfig( config );
    }
}