/**
 * Terminator.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

/**
 * Terminates the test Web server
 *
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.2 $
 */

// $Id: Terminator.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public interface Terminator {
    /** Cannot be void */
    public boolean terminate();
}
