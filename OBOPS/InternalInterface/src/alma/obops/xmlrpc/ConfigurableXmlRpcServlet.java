/**
 * InstitutionsServlet.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.xmlrpc;

import java.io.IOException;
import java.util.TimeZone;

import javax.servlet.ServletConfig;

import org.apache.xmlrpc313.XmlRpcException;
import org.apache.xmlrpc313.server.PropertyHandlerMapping;
import org.apache.xmlrpc313.server.XmlRpcHandlerMapping;
import org.apache.xmlrpc313.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc313.webserver.XmlRpcServlet;
import org.apache.xmlrpc313.webserver.XmlRpcServletServer;

/**
 * An XmlRpcServlet whose handler definitions are read from a properties file.
 * <p/>
 * 
 * <strong>Note</strong>: this class is abstract because it needs a properties
 * file to be loaded from the classpath; see {@link #setResource(String)}. That
 * resource is likely not to be accessible from the class loader of this class;
 * applications will need to subclass this class and make sure that the loader
 * of that (concrete) subclass can also access the properties file resource.
 * 
 * @author amchavan, Nov 5, 2010
 * @version $Revision: 1.3 $
 */

// $Id: ConfigurableXmlRpcServlet.java,v 1.3 2011/02/23 10:23:44 rkurowsk Exp $

@SuppressWarnings("serial")
public abstract class ConfigurableXmlRpcServlet extends XmlRpcServlet {

    protected XmlRpcHandlerMapping mapping;
    protected String resource;
    
    /** Required to instantiate this class -- not to be called from client code */
    public ConfigurableXmlRpcServlet() {
        // no-op
    }

    /**
     * Creates an XmlRpcServlet whose handler definitions are read from a
     * properties file.
     * 
     * @param cloader
     *            The class loader being used to load handler classes
     * @param resource
     *            The resource being used, for example
     *            <em>org/apache/xmlrpc/webserver/XmlRpcServlet.properties</em>.
     *            <br/>
     *            The property file contains a set of properties: the property
     *            key is taken as the handler name, the property value is taken
     *            as the name of a class being instantiated. (For any non-void,
     *            non-static, and public method in the class, an entry in the
     *            handler map is generated.)
     *            <br/> 
     *            A typical use would be to specify
     *            interface names as the property keys and implementations as
     *            the values:
     *            <br/>
     *            <code>
     *            &nbsp;&nbsp;&nbsp;org.apache.xmlrpc.demo.Adder=org.apache.xmlrpc.demo.AdderImpl
     *            </code>
     * 
     * @throws IOException
     * @throws XmlRpcException
     */
    public ConfigurableXmlRpcServlet( ClassLoader cloader, String resource ) 
        throws IOException, XmlRpcException {
        PropertyHandlerMapping phm = new PropertyHandlerMapping();
        phm.load( cloader, resource );
        this.mapping = phm;
    }

    /**
     * @return the resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * Creates a new handler mapping.
     * 
     * @see org.apache.xmlrpc313.webserver.XmlRpcServlet#newXmlRpcHandlerMapping()
     * @return An XmlRpcHandlerMapping initialized from the resources passed in
     *         to the constructor.
     */
    @Override
    protected XmlRpcHandlerMapping newXmlRpcHandlerMapping() 
        throws XmlRpcException {
        return mapping;
    }

    /**
     * Creates the XmlRpcServletServer (whatever that is in reality) and
     * configures it as we need it, with isEnabledForExtensions() returning
     * <code>true</code>.
     * 
     * @see org.apache.xmlrpc313.webserver.XmlRpcServlet#newXmlRpcServer(javax.servlet.ServletConfig)
     */
    @Override
    protected XmlRpcServletServer newXmlRpcServer( ServletConfig ignored )
            throws XmlRpcException {
        
        XmlRpcServerConfigImpl pConfig = new XmlRpcServerConfigImpl();
        pConfig.setEnabledForExtensions( true );
        pConfig.setEnabledForExceptions( true );
        pConfig.setTimeZone( TimeZone.getTimeZone( "UTC" ));
        
        XmlRpcServletServer ret = new XmlRpcServletServer();
        ret.setConfig( pConfig );
        return ret;
    }

    /**
     * Set the resource property file name.
     * 
     * As a side effect, it constructs a new {@link XmlRpcHandlerMapping}
     * instance from those properties and makes it our handler map
     * 
     * @param resource
     *            The resource being used, for example
     *            <em>org/apache/xmlrpc/webserver/XmlRpcServlet.properties</em>.
     *            <br/>
     *            The property file contains a set of properties: the property
     *            key is taken as the handler name, the property value is taken
     *            as the name of a class being instantiated. (For any non-void,
     *            non-static, and public method in the class, an entry in the
     *            handler map is generated.)
     *            <br/> 
     *            A typical use would be to specify
     *            interface names as the property keys and implementations as
     *            the values:
     *            <br/>
     *            <code>
     *            &nbsp;&nbsp;&nbsp;org.apache.xmlrpc.demo.Adder=org.apache.xmlrpc.demo.AdderImpl
     *            </code>
     * 
     * @throws XmlRpcException
     * @throws IOException
     */
    public void setResource( String resource ) throws IOException, XmlRpcException {
        this.resource = resource;
        
        ClassLoader cloader = this.getClass().getClassLoader();
        PropertyHandlerMapping phm = new PropertyHandlerMapping();
        phm.load( cloader, resource );
        this.mapping = phm;
    }
}
