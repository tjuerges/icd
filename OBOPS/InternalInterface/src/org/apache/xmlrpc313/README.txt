$Id: README.txt,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

Sources for version 3.1.3 of org.apache.xmlrpc

Copied here because of an incompatibility between this version and the 1.2 
version distributed with ACS 9.

Removed:
   org/apache/xmlrpc/client/XmlRpcCommonsTransport.java
   org/apache/xmlrpc/client/XmlRpcCommonsTransportFactory.java
because they require yet another package (org.apache.commons.httpclient) to 
build; we're not using those two classes anyway.

Renamed org.apache.xmlrpc to org.apache.xmlrpc313 in order to avoid said 
incompatibility.
   

M Chavan, 10-Nov-2010