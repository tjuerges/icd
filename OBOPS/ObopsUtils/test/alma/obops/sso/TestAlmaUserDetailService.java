/**
 * TestAlmaUserDetail.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.sso;

import junit.framework.TestCase;

import org.springframework.security.userdetails.UserDetails;

import alma.obops.utils.UnitTestUtilities;

/**
 * @author amchavan, Jan 27, 2010
 * @version $Revision: 1.6 $
 */

// $Id: TestAlmaUserDetailService.java,v 1.6 2011/03/24 09:49:31 fjulbe Exp $
public class TestAlmaUserDetailService extends TestCase {

    // should be there
    private static final String USERNAME = "paul";


    public void setUp() {
        UnitTestUtilities.resetConnectionProperties();
    }
    
    public void test00() {
        // No configuration, use built-in defaults
        AlmaUserDetailsService service = new AlmaUserDetailsService();
        UserDetails user = service.loadUserByUsername( USERNAME );
        assertNotNull( user );
    }
    /*
     * 
     * Most of this tests can be disabled, as now there are no parameters being passed
     * to the factory. It is done before. 
     * 
     *
    public void testConfig() throws IOException {
        URL url =
            TestAlmaUserDetailService.class.getResource("data/good.properties");
        InputStream stream = url.openStream();
        LdapSessionFactory.configure( stream );
        
        AlmaUserDetailsService service = new AlmaUserDetailsService();
        UserDetails user = service.loadUserByUsername( USERNAME );
        assertNotNull( user );
    }

    public void testBadConfig00() {
        Properties props = new Properties();
        props.setProperty( LdapSessionFactory.READ_ONLY_USER_PASSWORD_KEY, 
                           "this is for sure an invalid password" );
        LdapSessionFactory.configure( props );

        try {
            AlmaUserDetailsService service = new AlmaUserDetailsService();
            service.getRoleDirectorySession();
            fail( "Expected an exception" );
        }
        catch( Exception e ) {
            // no-op, expected
            // e.printStackTrace();
        }
    }

    /*
    public void testBadConfig01() throws IOException {
        URL url =
            TestAlmaUserDetailService.class.getResource("data/bad.properties");
        InputStream stream = url.openStream();
        LdapSessionFactory.configure( stream );

        try {
            AlmaUserDetailsService service = new AlmaUserDetailsService();
            service.getRoleDirectorySession();
            fail( "Expected an exception" );
        }
        catch( Exception e ) {
            // no-op, expected
            // e.printStackTrace();
        }
    }
	*/
    /*
     * Loading properties from configuration file and passing each
     * property to the constructor individually. Configuration file as loaded
     * by spring security web app. that will make use of this service.
     * 
     * NOTE!: With the new configuration, this should be disabled. All these 
     * properties are now in archiveConfig.properties.
     * 
     *
    public void testLoadingConfigFromRealFile() throws IOException {
    	
        URL url =
            TestAlmaUserDetailService.class.getResource("data/spring.config.properties");
        
        UnitTestUtilities.resetConnectionProperties();
        InputStream stream = url.openStream();
        Properties properties = new Properties(); 
        try { 
        	properties.load(stream); 

        	String factoryState="";
			String factoryObject="";
			String factoryInitial="";
			String securityAuthentication="";
			String providerUrl="";
			String userDN="";
			String userPass="";
			String subsystem="MASTER";			
        	
			// reading properties
	        if(properties.containsKey("ldap.factory.state")){
	        	factoryState=(String)properties.get("ldap.factory.state");
	        }
	        if(properties.containsKey("ldap.factory.object")){
	        	factoryObject=(String)properties.get("ldap.factory.object");
	        }
	        if(properties.containsKey("ldap.factory.initial")){
	        	factoryInitial=(String)properties.get("ldap.factory.initial");
	        }
	        if(properties.containsKey("ldap.provider.url")){
	        	providerUrl=(String)properties.get("ldap.provider.url");
	        }
	        if(properties.containsKey("ldap.user.dn")){
	        	userDN=(String)properties.get("ldap.user.dn");
	        }
	        if(properties.containsKey("ldap.user.pass")){
	        	userPass=(String)properties.get("ldap.user.pass");
	        }
	        if(properties.containsKey("ldap.security.authentication")){
	        	securityAuthentication=(String)properties.get("ldap.security.authentication");
	        }        
		        
	        AlmaUserDetailsService service = 
        				new AlmaUserDetailsService( factoryState,
						    						factoryObject,
									    			factoryInitial,
									    			securityAuthentication,
									    			providerUrl,
									    			userDN,
									    			userPass,
									    			subsystem);
        
        	UserDetails user = service.loadUserByUsername( USERNAME );
        	assertNotNull( user );        	
        	assertEquals( 7, user.getAuthorities().length );
        } 
        catch (IOException e) {
    	
        }
    }
 	*/
    // stress test, repeat query many times
    public void testStress() throws InterruptedException {
        final int repeats = 100;
        AlmaUserDetailsService service = new AlmaUserDetailsService();
        for( int i = 0; i < repeats; i++ ) {
            service.loadUserByUsername( USERNAME );
//            Thread.sleep( 0 );     // can't be too fast!
        }
    }    
}
