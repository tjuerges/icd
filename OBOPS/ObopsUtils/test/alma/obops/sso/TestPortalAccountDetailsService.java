/**
 * TestAlmaUserDetail.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.sso;

import junit.framework.TestCase;

import org.springframework.security.GrantedAuthority;

/**
 * @author amchavan, Jan 27, 2010
 * @version $Revision$
 */

// $Id$
public class TestPortalAccountDetailsService extends TestCase {

    // should be there
    private static final String USERNAME = "unit-test-user";
    private static final String SUBSYSTEM = " OBOPS\t";  // note the extra whitespace 

    public void setUp() {
    }
    
    public void test00() {
        // No configuration, use built-in defaults
        PortalAccountDetailsService service = new PortalAccountDetailsService( SUBSYSTEM );
        AlmaUserDetails user = (AlmaUserDetails) service.loadUserByUsername( USERNAME );
        assertNotNull( user );
        assertEquals( "obops@eso.org", user.getEmail() );
        
        GrantedAuthority[] authorities = user.getAuthorities();
        assertNotNull( authorities );
        assertEquals( 5, authorities.length );
    }
    
    // stress test, repeat query many times
    public void testStress() throws InterruptedException {
        final int repeats = 100;
        PortalAccountDetailsService service = new PortalAccountDetailsService( SUBSYSTEM );
        for( int i = 0; i < repeats; i++ ) {
            service.loadUserByUsername( USERNAME );
        }
    }    
}
