/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.sso;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jan 28, 2010
 * @version $Revision: 1.2 $
 */

// $Id: AllTests.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.obops.sso" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestPortalAccountDetailsService.class );
        suite.addTestSuite( TestAlmaUserDetailService.class );
        //$JUnit-END$
        return suite;
    }

}
