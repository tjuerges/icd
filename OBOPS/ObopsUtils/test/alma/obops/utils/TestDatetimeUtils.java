/**
 * TestDatetimeUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;
import alma.obops.utils.DatetimeUtils.Style;

/**
 * @author amchavan, Feb 5, 2008
 * @version $Revision$
 */

// $Id$

public class TestDatetimeUtils extends TestCase {

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#completeISODateString(java.lang.String)}.
     * @throws ParseException 
     * @throws NumberFormatException 
     */
    public void testCompleteISODateString() 
        throws NumberFormatException, ParseException {

        String in, out, expected;
        String now = "1997-08-14T00:00:00";
        String sample[] = {
            "        07            ", "1997-08-07T00:00:00.000",
            "     10-07            ", "1997-10-07T00:00:00.000",
            "  1998-2-1            ", "1998-02-01T00:00:00.000",
            "1998-01-01            ", "1998-01-01T00:00:00.000",
            "        15T05         ", "1997-08-15T05:00:00.000",
            "        15T05:30      ", "1997-08-15T05:30:00.000",
            "        20T5:5:30.00  ", "1997-08-20T05:05:30.000",
            "     10-16T21:30      ", "1997-10-16T21:30:00.000",
            "1999-12-31T23:59:59.99", "1999-12-31T23:59:59.990"
        };

        System.out.println( "" );
        for( int i = 0; i < sample.length; i++ ) {

            in       = sample[ i ];
            expected = sample[ ++i ];
//            System.out.print( ">>> '" + in + "' '" + expected + "'" );
            out      = DatetimeUtils.completeIsoDateString( in, now );
//            System.out.println( " '" + out + "'" );
            assertEquals( "Completion failed: ", expected, out ); 
        }
    }

    public void testFormatAsIso() {
        final String  expectedLong = "1970-01-01T00:00:00.000";
        final String expectedShort = "1970-01-01T00:00:00";
        Date in = new Date( 0 );
        String out = DatetimeUtils.formatAsIso( in );
        assertNotNull( out );
        assertEquals( expectedShort, out );
        
        out = DatetimeUtils.formatAsIso( in, Style.MEDIUM );
        assertNotNull( out );
        assertEquals( expectedLong, out );
        
        out = DatetimeUtils.formatAsIso( in, Style.SHORT );
        assertNotNull( out );
        assertEquals( expectedShort, out );
    }

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getCalendar()}.
     */
    public void testGetCalendar() {
        // bogus test -- there's nothing to test here, just see if it runs OK
        Calendar c = DatetimeUtils.getCalendar();
        assertNotNull( c );
    }

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getCalendar(java.util.TimeZone)}.
     */
    public void testGetCalendarTimeZone() {
        // bogus test -- there's nothing to test here, just see if it runs OK
        Calendar c = DatetimeUtils.getCalendar( DatetimeUtils.UT );
        assertNotNull( c );
    }

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getCalendar(java.util.TimeZone, java.util.Date)}.
     */
    public void testGetCalendarTimeZoneDate() {
        // bogus test -- there's nothing to test here, just see if it runs OK
        Calendar c = DatetimeUtils.getCalendar( DatetimeUtils.UT, 
                                                new Date( 0 ));
        assertNotNull( c );
    }

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getCalendar(java.util.TimeZone, java.lang.String)}.
     * @throws ParseException 
     */
    public void testGetCalendarTimeZoneString() throws ParseException {
        // bogus test -- there's nothing to test here, just see if it runs OK
        Calendar c = DatetimeUtils.getCalendar( DatetimeUtils.UT, 
                                                "1999-12-31T23:59:59");
        assertNotNull( c );
    }

    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getCurrentIsoDateTime()}.
     */
    public void testGetCurrentIsoDateTime() {
        // bogus test -- there's nothing to test here
        String date = DatetimeUtils.getCurrentIsoDateTime();
        assertNotNull( date );
    }
    
    public void testGetDate() throws NumberFormatException, ParseException {
        String input = "2008-02-10T08:15:58.000";
        Date d = DatetimeUtils.getDate( input );
        assertNotNull( d );
        
        input = "bogus";
        try {
            d = DatetimeUtils.getDate( input );
            fail( "Expected ParseException" );
        }
        catch( ParseException e ) {
            // no-op, expected
        }
    }
    
    /**
     * Test method for {@link alma.obops.common.utils.DatetimeUtils#getIsoDateFormat()}.
     */
    public void testGetIsoDateFormat() {
        // bogus test -- there's nothing to test here
        SimpleDateFormat fmt = DatetimeUtils.getIsoDateFormat();
        assertNotNull( fmt );
    }

    // Method parseIsoDateTime() is tested more thoroughly via 
    // the tests for completeISODateString()
    public void testParseIsoDateTime() throws ParseException {
        String in = "2001-10-09T10:52:30";
        int[] out = DatetimeUtils.parseIsoDateTime( in );
        assertEquals( 2001, out[0] );
        assertEquals(   10, out[1] );
        assertEquals(    9, out[2] );
        assertEquals(   10, out[3] );
        assertEquals(   52, out[4] );
        assertEquals(   30, out[5] );

        in = "2001-10-09T10:52:30.33";
        out = DatetimeUtils.parseIsoDateTime( in );
        assertEquals( 2001, out[0] );
        assertEquals(   10, out[1] );
        assertEquals(    9, out[2] );
        assertEquals(   10, out[3] );
        assertEquals(   52, out[4] );
        assertEquals(   30, out[5] );
        assertEquals(  330, out[6] );
        
        in = "";
        out = DatetimeUtils.parseIsoDateTime( in );
        assertEquals( 0, out[0] );
        assertEquals( 0, out[1] );
        assertEquals( 0, out[2] );
        assertEquals( 0, out[3] );
        assertEquals( 0, out[4] );
        assertEquals( 0, out[5] );
        
        String bad2 = "200q-10-09T10:52:30";
        try {
            DatetimeUtils.parseIsoDateTime( bad2 );
            fail( "Expected NumberFormatException" );
        }
        catch( NumberFormatException e ) {
            // no-op, expected
        }
        
        String bad3 = "2000-10-09T10:52:30.q";
        try {
            DatetimeUtils.parseIsoDateTime( bad3 );
            fail( "Expected NumberFormatException" );
        }
        catch( NumberFormatException e ) {
            // no-op, expected
        }
    }
    
    public void testRoundToHour() 
        throws NumberFormatException, ParseException {

        SimpleDateFormat fmt = 
            DatetimeUtils.getIsoDateFormat( DatetimeUtils.Style.MEDIUM );
        
        final String in0 = "2008-04-11T08:00:00.0";
        Date input = DatetimeUtils.getDate( in0, fmt );
        Date output = DatetimeUtils.getDate( in0, fmt );
        assertEquals( output, DatetimeUtils.roundToHour( input ));
        
        final String  in1 = "2008-04-16T08:32:29.500";
        final String out1 = "2008-04-16T09:00:00.000";
        input = DatetimeUtils.getDate( in1, fmt );
        output = DatetimeUtils.getDate( out1, fmt );
        assertEquals( output, DatetimeUtils.roundToHour( input ));
        
        final String  in2 = "2008-04-14T08:28:28.499";
        final String out2 = "2008-04-14T08:00:00.000";
        input = DatetimeUtils.getDate( in2, fmt );
        output = DatetimeUtils.getDate( out2, fmt );
        assertEquals( output, DatetimeUtils.roundToHour( input ));

        // Examples from Javadoc of DatetimeUtils.roundToHour()
        
        final String  inDoc1 = "2008-04-14T08:32:29.678";
        final String outDoc1 = "2008-04-14T09:00:00.000";
        input = DatetimeUtils.getDate( inDoc1, fmt );
        output = DatetimeUtils.getDate( outDoc1, fmt );
        assertEquals( output, DatetimeUtils.roundToHour( input ));
        
        final String  inDoc2 = "2008-04-14T08:28:15.400";
        final String outDoc2 = "2008-04-14T08:00:00.000";
        input = DatetimeUtils.getDate( inDoc2, fmt );
        output = DatetimeUtils.getDate( outDoc2, fmt );
        assertEquals( output, DatetimeUtils.roundToHour( input ));
    }
    
    public void testRoundToMinute() 
        throws NumberFormatException, ParseException {

        SimpleDateFormat fmt = 
            DatetimeUtils.getIsoDateFormat( DatetimeUtils.Style.MEDIUM );
        
        final String in0 = "2008-04-11T08:32:00.0";
        Date input = DatetimeUtils.getDate( in0, fmt );
        Date output = DatetimeUtils.getDate( in0, fmt );
        assertEquals( output, DatetimeUtils.roundToMinute( input ));
        
        final String  in1 = "2008-04-16T08:32:29.500";
        final String out1 = "2008-04-16T08:33:00.000";
        input = DatetimeUtils.getDate( in1, fmt );
        output = DatetimeUtils.getDate( out1, fmt );
        assertEquals( output, DatetimeUtils.roundToMinute( input ));
        
        final String  in2 = "2008-04-14T08:32:28.499";
        final String out2 = "2008-04-14T08:32:00.000";
        input = DatetimeUtils.getDate( in2, fmt );
        output = DatetimeUtils.getDate( out2, fmt );
        assertEquals( output, DatetimeUtils.roundToMinute( input ));

        // Examples from Javadoc of DatetimeUtils.roundToMinute()
        
        final String  inDoc1 = "2008-04-14T08:32:29.678";
        final String outDoc1 = "2008-04-14T08:33:00.000";
        input = DatetimeUtils.getDate( inDoc1, fmt );
        output = DatetimeUtils.getDate( outDoc1, fmt );
        assertEquals( output, DatetimeUtils.roundToMinute( input ));
        
        final String  inDoc2 = "2008-04-14T08:32:15.400";
        final String outDoc2 = "2008-04-14T08:32:00.000";
        input = DatetimeUtils.getDate( inDoc2, fmt );
        output = DatetimeUtils.getDate( outDoc2, fmt );
        assertEquals( output, DatetimeUtils.roundToMinute( input ));
    }
    
    public void testRoundToSecond() 
        throws NumberFormatException, ParseException {

        SimpleDateFormat fmt = 
            DatetimeUtils.getIsoDateFormat( DatetimeUtils.Style.MEDIUM );
        
        final String in0 = "2008-04-11T08:32:15.0";
        Date input = DatetimeUtils.getDate( in0, fmt );
        Date output = DatetimeUtils.getDate( in0, fmt );
        assertEquals( output, DatetimeUtils.roundToSecond( input ));
        
        final String  in1 = "2008-04-16T08:32:15.500";
        final String out1 = "2008-04-16T08:32:16.000";
        input = DatetimeUtils.getDate( in1, fmt );
        output = DatetimeUtils.getDate( out1, fmt );
        assertEquals( output, DatetimeUtils.roundToSecond( input ));
        
        final String  in2 = "2008-04-14T08:32:15.499";
        final String out2 = "2008-04-14T08:32:15.0";
        input = DatetimeUtils.getDate( in2, fmt );
        output = DatetimeUtils.getDate( out2, fmt );
        assertEquals( output, DatetimeUtils.roundToSecond( input ));
        
        // Examples from Javadoc of DatetimeUtils.roundToSecond()
        
        final String  inDoc1 = "2008-04-14T08:32:15.678";
        final String outDoc1 = "2008-04-14T08:32:16.000";
        input = DatetimeUtils.getDate( inDoc1, fmt );
        output = DatetimeUtils.getDate( outDoc1, fmt );
        assertEquals( output, DatetimeUtils.roundToSecond( input ));
        
        final String  inDoc2 = "2008-04-14T08:32:15.400";
        final String outDoc2 = "2008-04-14T08:32:15.000";
        input = DatetimeUtils.getDate( inDoc2, fmt );
        output = DatetimeUtils.getDate( outDoc2, fmt );
        assertEquals( output, DatetimeUtils.roundToSecond( input ));
    }
    
    public void testSetTime() throws NumberFormatException, ParseException {
        final String  sInput = "2008-04-11T08:32:15.000";
        final String sOutput = "2008-04-11T00:00:00.000";
        Date input = DatetimeUtils.getDate( sInput );
        Date output = DatetimeUtils.getDate( sOutput );
        assertEquals( output, DatetimeUtils.setTime( input, 0, 0, 0, 0 ) );
    }
}
