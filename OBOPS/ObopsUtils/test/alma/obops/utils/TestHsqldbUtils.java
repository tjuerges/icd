/*
 * TestHsqldbUtils
 * 
 * $Revision$
 *
 * Jan 29, 2007
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.File;
import java.io.IOException;

import alma.obops.utils.HsqldbUtilities.Mode;

import junit.framework.TestCase;

/**
 * @version $Revision$
 * @author amchavan, Jan 29, 2007
 */

// $Id$

public class TestHsqldbUtils extends TestCase {
    
    static final String DB_DDL = "config/shiftlog-test-hsqldb-ddl.sql";

    static final String DB_DIRECTORY = "hsqldb-files";    

    static final String DB_NAME = "shiftlog";
    
    public void testCreateLocalDatabase() throws Exception {

        // Make sure we have no database directory
        File dir = new File( DB_DIRECTORY );
        GeneralUtils.delete( dir );
        
        // Create the database
        HsqldbUtilities.createDatabase(Mode.File, DB_NAME, DB_DIRECTORY, DB_DDL, null );
        
        // Verify if the database directory exists and
        // is valid
        assertTrue( HsqldbUtilities.validDatabaseDirectory( DB_DIRECTORY, DB_NAME));
        
        // Cleanup
        GeneralUtils.delete( dir );
    }
    
    public void testValidDatabaseDirectory() throws IOException {
        String testdir = "testdir";
        String  dbname = "testdb";
        String  props  = dbname + ".properties";
        String  script = dbname + ".script";

        // Create and check a valid directory structure
        //---------------------------------------------
        File     dir = new File( testdir );
        File  fProps = new File( dir, props );
        File fScript = new File( dir, script );
        dir.mkdir();
        fProps.createNewFile();
        fScript.createNewFile();
        
        assertTrue( HsqldbUtilities.validDatabaseDirectory( testdir, dbname ));
        
        // Make that directory invalid, and check that we
        // understand that.
        //-----------------------------------------------
        fScript.delete();   // script file missing
        assertFalse( HsqldbUtilities.validDatabaseDirectory( testdir, dbname ));
        

        fScript.createNewFile();    // properties file missing
        fProps.delete();
        assertFalse( HsqldbUtilities.validDatabaseDirectory( testdir, dbname ));

        fScript.delete();   // script and properties files missing
        assertFalse( HsqldbUtilities.validDatabaseDirectory( testdir, dbname ));
        
        // Check than a non-existing directory is invalid
        //-----------------------------------------------
        GeneralUtils.delete( dir );
        assertFalse( HsqldbUtilities.validDatabaseDirectory( testdir, dbname ));
    }
}
