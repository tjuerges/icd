/*
 * TestFileIoUtils
 *
 * $Revision: 1.6 $
 *
 * Sep 10, 2007
 *
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import junit.framework.TestCase;
import alma.obops.utils.ZipIoUtils.ZipNtry;


/**
 * @version $Revision: 1.6 $
 * @author amchavan, Sep 10, 2007
 */

// $Id: TestZipIoUtils.java,v 1.6 2011/02/03 10:28:57 rkurowsk Exp $

public class TestZipIoUtils extends TestCase {

    private static final String TEXT = "this is some text\nbroken on two lines";
    private static final String TEMP_FILE_PREFIX =
        "alma.obops.common.unit-test";
    protected File tempZipFile;
//    String here =
//        TestZipIoUtils.class.getPackage().getName().replace( '.', '/' );
    protected File tempZipDir;

    public void setUp() throws IOException {
        //-------------------------------------------------------------
        // here we should be using
        // alma.obops.common.utils.TestUtilities.isCanonicalTestDir()
        // but it's in obopscommon
        final String testDir = "test";
        File fHere = new File( "" );
        String pHere = fHere.getAbsolutePath();
        if( ! pHere.endsWith( testDir ) ) {
            fail( "This test should be run from directory " + testDir );
        }
        //-------------------------------------------------------------

        tempZipFile = TestFileIoUtils.createTempFile();
        tempZipDir = createTempDirectory();
    }

    public void tearDown() {
        FileIoUtils.deleteDir( tempZipDir );
        if( ! tempZipFile.delete() ) {
            fail( "Deleting g: " + tempZipFile.getPath() );
        }
    }

    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#zipFiles(java.io.File[], java.io.File)}.
     * @throws IOException
     */
    public void testZipFiles00() throws IOException {

        final int numfiles = 5;
        populate( tempZipDir, numfiles );

        // now zip everything up
        File[] files = tempZipDir.listFiles();
        ZipIoUtils.zipFiles( files, tempZipFile );

        // Look into the resulting zipfile and see if it's OK
        ZipFile zip = new ZipFile( tempZipFile );
        Enumeration<? extends ZipEntry> entries = zip.entries();
        int count = 0;

        /*String zipDirName = */tempZipDir.getCanonicalPath();
        while( entries.hasMoreElements() ) {
            /*ZipEntry entry = */entries.nextElement();
            count++;
        }
        assertEquals( numfiles, count );   // easy check, count entries

        zip.close();
    }

    /**
     * @return A brand new temp directory
     */
    private File createTempDirectory() {
        String pathname = TEMP_FILE_PREFIX 
                        + "-"
                        + System.getProperty( "user.name" ) 
                        + "-zip.dir";
        File zipdir = new File( tempZipFile.getParentFile(), pathname );
        FileIoUtils.deleteDir( zipdir );
        boolean dirwasmade = zipdir.mkdir();
        assertTrue( dirwasmade );
        return zipdir;
    }

    // Create n bogus files in the input directory
    private static void populate( File dir, int n ) throws IOException {

        assertTrue( dir.exists() );
        assertTrue( dir.isDirectory() );

        // populate the temp directory with some files
        for( int i = 0; i < n; i++ ) {
            File f = new File( dir, "file" + i );
            Writer w = TestFileIoUtils.createTempWriter( f );
            w.append( TEXT );
            w.close();
        }
    }

    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#zipFiles(java.io.File[], java.io.File)}.
     * @throws IOException
     */
    public void testZipFiles01() throws IOException {

        final int numfiles = 125; // yes, 125, why not 125?
        populate( tempZipDir, numfiles );

        // prefix for zipfile entries
        final String prefix = "bogus";

        // now zip everything up
        File[] files = tempZipDir.listFiles();
        ZipIoUtils.zipFiles( files, prefix, tempZipFile );

        // Look into the resulting zipfile and see if it's OK
        ZipFile zip = new ZipFile( tempZipFile );
        Enumeration<? extends ZipEntry> entries = zip.entries();
        int count = 0;

        while( entries.hasMoreElements() ) {
            ZipEntry entry = entries.nextElement();
            String name = entry.getName();
            String dir = new File( name ).getParent();
            assertEquals( prefix, dir );
            count++;
        }
        assertEquals( numfiles, count ); // easy check, count entries

        zip.close();
    }
    
    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#zipFiles(java.io.File[], java.io.File)}.
     * @throws IOException
     */
    public void testZipFiles02() throws IOException {

        final int numfiles = 125; // yes, 125, why not 125?
        populate( tempZipDir, numfiles );

        // prefix for zipfile entries
        final String prefix = "bogus";
        
        // now zip everything up
        File[] files = tempZipDir.listFiles();
        // create the map of input streams
        Map<String, InputStream> sourceFiles = new TreeMap<String, InputStream>();
 		for (File file : files) {
 			FileInputStream in = new FileInputStream(file);
 			String name = file.getName();
 			sourceFiles.put(name, in);
 		}
        ZipIoUtils.zipFiles( sourceFiles, prefix, tempZipFile );

        // Look into the resulting zipfile and see if it's OK
        ZipFile zip = new ZipFile( tempZipFile );
        Enumeration<? extends ZipEntry> entries = zip.entries();
        int count = 0;

        while( entries.hasMoreElements() ) {
            ZipEntry entry = entries.nextElement();
            String name = entry.getName();
            String dir = new File( name ).getParent();
            assertEquals( prefix, dir );
            count++;
        }
        assertEquals( numfiles, count ); // easy check, count entries

        zip.close();
    }

    public void testGetZipEntry0() throws ZipException, IOException {
        URL url = this.getClass().getResource( "data/stars.zip" );
        List<ZipNtry> entries = ZipIoUtils.getZipEntries( url );

        assertNotNull( entries );
        assertEquals( 5, entries.size() );

        assertEquals( "stars/star0.gif", entries.get(0).getName() );
        assertEquals( "stars/star1.gif", entries.get(1).getName() );
        assertEquals( "stars/star2.gif", entries.get(2).getName() );
        assertEquals( "stars/star3.gif", entries.get(3).getName() );

        assertEquals( 1312, entries.get(0).getSize() );
        assertEquals( 1312, entries.get(1).getSize() );
        assertEquals( 1312, entries.get(2).getSize() );
        assertEquals( 1312, entries.get(3).getSize() );
    }

    public void testGetZipEntry1() throws ZipException, IOException {
        URL url = this.getClass().getResource( "data/sample-project.aot" );
        List<ZipNtry> entries = ZipIoUtils.getZipEntries( url );

        assertNotNull( entries );
        assertEquals( 4, entries.size() );

        assertEquals( "ObsProposal.xml", entries.get(0).getName() );
        assertEquals( "SchedBlock0.xml", entries.get(1).getName() );
        assertEquals( "SchedBlock1.xml", entries.get(2).getName() );
        assertEquals( "ObsProject.xml",  entries.get(3).getName() );

        assertEquals(  1350, entries.get(0).getSize() );
        assertEquals(  7618, entries.get(1).getSize() );
        assertEquals(  7647, entries.get(2).getSize() );
        assertEquals( 12017, entries.get(3).getSize() );
    }

    //add entry to stars2.zip and check it's there afterwards
    public void testAppendToZip0() throws IOException {

    	String dataDir = this.getClass().getResource("data").getPath();
  		String old = dataDir + File.separator + "stars.zip";
    	String newFileName = dataDir + File.separator + "stars2.zip";
    	File sourceFile = new File(this.getClass().getResource(".").getPath() + "data/in1.pdf");
    	FileIoUtils.copyFile(old, newFileName);

    	ZipIoUtils.appendToZip(sourceFile, "not_a_star", new File(newFileName));

        List<ZipNtry> entries = ZipIoUtils.getZipEntries( newFileName );

        assertNotNull( entries );
        assertEquals( 6, entries.size() );

        assertEquals( "stars/star0.gif", entries.get(0).getName() );
        assertEquals( "stars/star1.gif", entries.get(1).getName() );
        assertEquals( "stars/star2.gif", entries.get(2).getName() );
        assertEquals( "stars/star3.gif", entries.get(3).getName() );
        assertEquals( "not_a_star" +
                      File.separator +
                      "in1.pdf", entries.get(5).getName() );

        assertEquals( 1312, entries.get(0).getSize() );
        assertEquals( 1312, entries.get(1).getSize() );
        assertEquals( 1312, entries.get(2).getSize() );
        assertEquals( 1312, entries.get(3).getSize() );

        File newFile = new File( newFileName );
        newFile.delete();

    }

}
