/**
 * TestTestUtils.java
 *
 * Copyright European Southern Observatory 2011
 */

package alma.obops.utils;

import junit.framework.TestCase;

/**
 * @author amchavan, Apr 28, 2011
 * @version $Revision$
 */

// $Id$

public class TestTestUtilities extends TestCase {

    public void testMakeNewUid() {
        String uid = TestUtilities.makeNewArchiveUid();
        assertNotNull( uid );
        assertTrue( "No match for " + uid, 
                    uid.matches( "uid://X\\w+/X\\w+/X\\w+" ) );

        String uid2 = TestUtilities.makeNewArchiveUid();
        assertFalse( "uid == uid2", uid.equals( uid2 ));
    }
}
