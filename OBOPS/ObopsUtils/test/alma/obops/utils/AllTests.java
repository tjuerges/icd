/*
 * AllTests
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @version $Revision$
 * @author rkurowsk, Sep 25, 2009
 */

// $Id$
public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.obops.utils" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestPdfUtils.class );
        suite.addTestSuite( TestZipIoUtils.class );
        suite.addTestSuite( TestDatetimeUtils.class );
        suite.addTestSuite( TestGeneralUtils.class );
        suite.addTestSuite( TestBulkStoreUtils.class );
        suite.addTestSuite( TestTestUtilities.class );
        suite.addTestSuite( TestFileIoUtils.class );
        suite.addTestSuite( TestHsqldbUtils.class );
        //$JUnit-END$
        return suite;
    }

}
