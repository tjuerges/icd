/**
 * UnitTestUtilities.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;


/**
 * Unit Test facilities
 *
 * @author amchavan, Jan 28, 2010
 * @version $Revision$
 */

// $Id$

public class UnitTestUtilities {

    // taken from ConnectionProperties.java
//    private static String initialFactory = "com.sun.jndi.ldap.LdapCtxFactory";
//    private static String stateFactory = "alma.userrepository.addressbook.ldap.UserRepositoryStateFactory:com.sun.jndi.ldap.obj.LdapGroupFactory";
//    private static String objectFactory = "alma.userrepository.addressbook.ldap.UserRepositoryObjectFactory:com.sun.jndi.ldap.obj.LdapGroupFactory";
//    private static String authentication = "simple";
//    private static String providerUrl = "ldap://ngas01.hq.eso.org:389/";
//    private static String userName = "uid=readUser,ou=master,dc=alma,dc=info";
//    private static String userPassword = "oS!iMLDAP!";

    public static void resetConnectionProperties() {
//        ConnectionProperties cp = ConnectionProperties.getInstance();
//        cp.setAuthentication( authentication );
//        cp.setInitialFactory( initialFactory );
//        cp.setObjectFactory( objectFactory );
//        cp.setProviderUrl( providerUrl );
//        cp.setReadOnlyUserName( userName );
//        cp.setReadOnlyUserPassword( userPassword );
//        cp.setStateFactory( stateFactory );
    }
}
