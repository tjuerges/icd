/*
 * TestUtils
 * 
 * $Revision: 1.2 $
 *
 * Jan 29, 2007
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.File;
import java.io.IOException;

import alma.obops.utils.GeneralUtils;

import junit.framework.TestCase;


/**
 * @version $Revision: 1.2 $
 * @author amchavan, Jan 29, 2007
 */

// $Id: TestGeneralUtils.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class TestGeneralUtils extends TestCase {

    public void testDelete() throws IOException {
        
        // create a disposable directory structure
        File  dir0 = new File( "bogus-directory-0" );
        dir0.mkdir();
        File file0 = new File( dir0, "bogus-file-0" );
        file0.createNewFile();
        File dir1 = new File( dir0, "bogus-directory-1" );
        dir1.mkdir();
        File file1 = new File( dir1, "bogus-file-1" );
        file1.createNewFile();
        
        // now delete it
        GeneralUtils.delete( dir0 );
        assertFalse( dir0.exists() );
    }
}
