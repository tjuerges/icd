/*
 * TestFileIoUtils
 * 
 * $Revision$
 *
 * Sep 10, 2007
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

import junit.framework.TestCase;


/**
 * @version $Revision$
 * @author amchavan, Sep 10, 2007
 */

// $Id$

public class TestFileIoUtils extends TestCase {
    
    private static final String TEXT = "this is some text\nbroken on two lines";
//    private static final String UNIT_TEST_DATA_DIR = 
//        "data" + File.separator + "unit-test";
    private static final String TEMP_FILE_PREFIX = 
        "alma.obops.common.unit-test";
    protected static File createTempFile() throws IOException {
        final String suffix = null;
        return File.createTempFile( TEMP_FILE_PREFIX, suffix );
    }
    protected static PrintWriter createTempWriter( File f ) throws IOException {
        PrintWriter out = 
            new PrintWriter( new BufferedWriter( new FileWriter( f )));
        return out;
    }
    protected File f;
    protected File g;

    
    protected File tempdir;
    
    String here = 
        TestFileIoUtils.class.getPackage().getName().replace( '.', '/' );
    
    public void setUp() throws IOException {
        //-------------------------------------------------------------
        // here we should be using 
        // alma.obops.common.utils.TestUtilities.isCanonicalTestDir()
        // but it's in obopscommon 
        final String testDir = "test";
        File fHere = new File( "" );
        String pHere = fHere.getAbsolutePath();
        if( ! pHere.endsWith( testDir ) ) {
            fail( "This test should be run from directory " + testDir );
        }
        //-------------------------------------------------------------
        
        f = createTempFile();    
        g = createTempFile();    

        tempdir = f.getParentFile();
    }

    public void tearDown() {
        if( ! f.delete() ) {
            fail( "Deleting f: " + f.getPath() );    
        }
        if( ! g.delete() ) {
            fail( "Deleting g: " + g.getPath() );    
        }
    }
    
    /**
         * Test method for {@link alma.obops.common.utils.FileIoUtils#copyDir(java.lang.String, java.lang.String)}.
         * @throws IOException 
         */
        public void testCopyDir() throws IOException {
            
            @SuppressWarnings("unused")
            boolean dirwasmade;
            
            File fromdir = new File( tempdir, TEMP_FILE_PREFIX + "-from.dir" );
            dirwasmade = fromdir.mkdir();
            assertTrue( fromdir.exists() );
            assertTrue( fromdir.isDirectory() );
            
            File todir = new File( tempdir, TEMP_FILE_PREFIX + "-to.dir" );
            dirwasmade = todir.mkdir();
            assertTrue( todir.exists() );
            assertTrue( todir.isDirectory() );
    //        
            // populate the "from" directory with some files
            for( int i = 0; i < 5; i++ ) {
                File f = new File( fromdir, "file" + i );
                Writer w = createTempWriter( f );
                w.append( TEXT );
                w.close();
            }
            
    //        File origDir = new File( UNIT_TEST_DATA_DIR );
    //        FilenameFilter pdfff = new FilenameFilter() {
    //            // accept filenames ending with .pdf
    //            public boolean accept( File dir, String name ) {
    //                return name.endsWith( ".pdf" );
    //            }
    //        };
    //        String[] files = origDir.list( pdfff );
    //        for( int i = 0; i < files.length; i++ ) {
    //            FileIoUtils.copyFile( files[i], origDir )
    //        }
    //        FileIoUtils.copyFile( fromdir, todir )
            
            FileIoUtils.copyDir( fromdir.getCanonicalPath(), 
                                 todir.getCanonicalPath() );
            String[] originals = fromdir.list();
            String[]    copied = todir.list();
            assertEquals( originals.length, copied.length );
            for( int i = 0; i < originals.length; i++ ) {
                assertEquals( originals[i], copied[i] );
            }
            
            FileIoUtils.deleteDir( fromdir );
            FileIoUtils.deleteDir( todir );
            
        }
    
    
    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#copyFile(java.lang.String, java.lang.String)}.
     * @throws IOException 
     */
    public void testCopyFile() throws IOException {
        FileIoUtils.stringToFile( TEXT, f );
        FileIoUtils.copyFile( f, g );
        assertEquals(  f.length(), g.length() );
    }

    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#deleteDir(File)}.
     * @throws IOException 
     */
    public void testDeleteDir() throws IOException {

        @SuppressWarnings("unused")
        boolean dirwasmade;
        
        // create a temporary directories
        
        File fromdir = new File( tempdir, TEMP_FILE_PREFIX + "-from.dir" );
        dirwasmade = fromdir.mkdir();
        assertTrue( fromdir.exists() );
        assertTrue( fromdir.isDirectory() );
        
        // populate the "from" directory with some files
        for( int i = 0; i < 5; i++ ) {
            File f = new File( fromdir, "file" + i );
            Writer w = createTempWriter( f );
            w.append( TEXT );
            w.close();
        }
        
        FileIoUtils.deleteDir( fromdir );
        assertFalse( fromdir.exists() );
    }

    public void testFileToByteArray() throws IOException {
        String pathname = here + "/data/sample-project.aot";
        File file = new File( pathname );
        byte[] out = FileIoUtils.fileToByteArray( file );
        assertNotNull( out );
        assertEquals( file.length(), out.length );
    }

    //    /**
//     * Test method for {@link alma.obops.common.utils.FileIoUtils#deleteDir(java.io.File)}.
//     */
//    public void testDeleteDir() {
//        fail( "Not yet implemented" );
//    }
//
    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#fileToString(java.lang.String)}.
     * @throws IOException 
     */
    public void testFileToString() throws IOException {
        FileIoUtils.stringToFile( TEXT, f );
        String out = FileIoUtils.fileToString( f );
        assertEquals( TEXT, out );
    }

/**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#getResourceStream(java.lang.String)}.
     * @throws IOException 
     */
    public void testGetResourceStream() throws IOException {

        InputStream is = null;
        
        // A resource from the classpath
        try {
            String s = this.getClass().getName();
            s = s.replaceAll( "\\.", "/" ) + ".class";
            is = FileIoUtils.getResourceStream( s );
        }
        catch( Exception e ) {
            fail( e.getMessage() );
        }
        finally {
            if( is != null ) {
                is.close();
            }
        }
        
        // A resource from the file system
        try {
            String s = f.getCanonicalPath();
            is = FileIoUtils.getResourceStream( s );
        }
        catch( Exception e ) {
            fail( e.getMessage() );
        }
        finally {
            if( is != null ) {
                is.close();
            }
        }
    }

    public void testIsNewer() throws IOException, InterruptedException {
        File source = createTempFile();
        String sourcePath = source.getAbsolutePath();
        
        File dest = createTempFile();
        dest.setLastModified( source.lastModified() );
        String destPath = dest.getAbsolutePath();

        assertFalse( FileIoUtils.isNewer( "bogusFile", sourcePath ));
        assertFalse( FileIoUtils.isNewer( destPath, sourcePath ));
        
//        Writer w = new FileWriter( source );
//        w.append( "something" );
//        w.flush();
//        w.close();
//        Thread.sleep( 50 );
//        w = new FileWriter( dest );
//        w.append( "something" );
//        w.flush();
//        w.close();
        
        final File dest2 = new File( destPath );
        final File source2 = new File( sourcePath );
        final boolean ok = dest2.setLastModified( source2.lastModified() + 
                                                  2000L );
        if( ok ) {
            // setLastModified() does not succeed on all platforms, we only 
            // run the following test if it did succeed
            String msg = "not newer: ok=" + ok + 
                         ", source2=" + source2.lastModified() +
                         ", dest2=" + dest2.lastModified();
             assertTrue( msg, FileIoUtils.isNewer( dest2, source2));
        }
        
        source.delete();
        dest.delete();
    }

    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#readBytes(java.io.InputStream, int)}.
     * @throws IOException 
     */
    public void testReadBytes() throws IOException {
        PrintWriter w = createTempWriter( f );
        w.print( TEXT );
        w.close();
        
        InputStream s = new FileInputStream( f );
        byte[] b = FileIoUtils.readBytes( s, 2 );
        String ret = new String( b );
        assertEquals( TEXT, ret );
        s.close();
    }

    public void testReplaceSuffix() {
        assertEquals( "file.o", FileIoUtils.replaceSuffix( "file.c", ".c", ".o" ));
        assertEquals( "file.o", FileIoUtils.replaceSuffix( "file", ".c", ".o" ));
        assertEquals( "file.c", FileIoUtils.replaceSuffix( "file", ".c", ".c" ));
    }
    
    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#stringToFile(java.lang.String, java.io.File)}.
     * @throws IOException 
     */
    public void testStringToFile() throws IOException {
        FileIoUtils.stringToFile( TEXT, f );
        assertEquals( TEXT.length(), f.length() );
        
        Reader r = new FileReader( f );
        char[] chars = new char[ TEXT.length() ];
        int ret = r.read( chars );
        r.close();
        assertEquals( TEXT.length(), ret );
        assertEquals( TEXT, new String( chars ));
    }
    
    /**
     * Test method for {@link alma.obops.common.utils.FileIoUtils#writeBytes(byte[], java.io.OutputStream)}.
     * @throws IOException 
     */
    public void testWriteBytes() throws IOException {
        final int byteSize = 512;
        byte[] bytes = new byte[byteSize];
        for( int i = 0; i < bytes.length; i++ ) {
            bytes[i] = 0;
        }

        OutputStream s = new FileOutputStream( f );
        FileIoUtils.writeBytes( bytes, s );
        s.close();
        assertEquals( byteSize, f.length() );
    }
}
