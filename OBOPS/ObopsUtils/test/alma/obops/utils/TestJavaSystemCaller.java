/**
 * TestJavaSystemCaller.java
 *
 * Copyright European Southern Observatory 2011
 */

package alma.obops.utils;

import static alma.obops.utils.JavaSystemCaller.Exec;
import alma.obops.utils.JavaSystemCaller.ExecResults;
import junit.framework.TestCase;

/**
 * @author amchavan, May 25, 2011
 * @version $Revision$
 */

// $Id$

public class TestJavaSystemCaller extends TestCase {
    
    public void testExecute00() {
        ExecResults out = Exec.execute( "ls" );
        System.out.println( out.stdout );
    }
}
