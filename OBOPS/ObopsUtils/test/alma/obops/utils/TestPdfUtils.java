/**
 * TestPdfUtilitites.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;

import static alma.obops.utils.PdfUtils.IS_SECURED;
import static alma.obops.utils.PdfUtils.combine;
import static alma.obops.utils.PdfUtils.combineByName;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

/**
 * @author amchavan, Mar 12, 2010
 * @version $Revision$
 */

// $Id$

public class TestPdfUtils extends TestCase {

    protected String here = 
        TestPdfUtils.class.getPackage().getName().replace( '.', '/' );
    protected String[] pathnames = {
            here + "/data/in1.pdf",
            here + "/data/in2.pdf",
            here + "/data/in3.pdf",
            here + "/data/in4.pdf"
      };
      
    protected String secured = here + "/data/secured.pdf";
      
    protected List<String> fileNames = new ArrayList<String>();
    protected List<byte[]> files = new ArrayList<byte[]>();
    
    public void setUp() throws IOException {
        //-------------------------------------------------------------
        // here we should be using 
        // alma.obops.common.utils.TestUtilities.isCanonicalTestDir()
        // but it's in obopscommon 
        final String testDir = "test";
        File fHere = new File( "" );
        String pHere = fHere.getAbsolutePath();
        if( ! pHere.endsWith( testDir ) ) {
            fail( "This test should be run from directory " + testDir );
        }
        //-------------------------------------------------------------
    }

    
    public void testConcatenateBad0() {

        try {
            // Let's find a file that we surely cannot write
        	String notWritable = null;
        	String os = System.getProperty("os.name");
        	if( os.toLowerCase().contains("windows")) {
        		notWritable = "A:\\aaa.pdf";	// this should fail, unless your PC has a
        	            						// floppy disk drive
        	}
        	else {
        		notWritable = "/bin/q.pdf";			// this should fail, unless you're root
        	}
        	
            fileNames.clear();
            for( int i = 0; i < pathnames.length; i++ ) {
                fileNames.add( pathnames[i] );
            }
            combineByName( notWritable, fileNames );
            fail( "Expected IOException" );
        }
        catch( IOException e ) {
            // no-op, expected
        }
        catch( WrappedDocumentException e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
    }

    public void testConcatenateBad1() {
        try {
            combineByName( "good-file", null );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // no-op, expected
        }
        catch( Exception e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
    }

    public void testConcatenateBad2() {
        
        try {
            combineByName( (String) null, fileNames );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // no-op, expected
        }
        catch( Exception e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
    }

    public void testConcatenateBad3() {

        try {
            combineByName( (OutputStream) null, fileNames );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // no-op, expected
        }
        catch( Exception e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
    }
    
    public void testConcatenate0() throws IOException, WrappedDocumentException {

        final String dest = "test-out.pdf";
        fileNames.clear();
        for( int i = 0; i < pathnames.length; i++ ) {
            fileNames.add( pathnames[i] );
        }
        combineByName( dest, fileNames );
        
        File f = new File( dest );
        assertTrue( f.exists() && 
                    f.canRead() && 
                    f.length() > 0 );
        
        assertTrue( f.delete() );   // fail if for some reason we cannot
                                    // delete the temp file
    }
    
    public void testConcatenate1() throws IOException, WrappedDocumentException {
        
        final String dest = "test-out.pdf";
        files.clear();
        for( int i = 0; i < pathnames.length; i++ ) {
            files.add( FileIoUtils.fileToByteArray( new File( pathnames[i] )));
        }
        combine( dest, files );
        
        File f = new File( dest );
        assertTrue( f.exists() && 
                    f.canRead() && 
                    f.length() > 0 );
        
        assertTrue( f.delete() );   // fail if for some reason we cannot
                                    // delete the temp file
    }
    
    public void testCreate() throws WrappedDocumentException, IOException {
        final String text = 
            "This is a PDF file containing one line of text " +
            "and exactly 74 characters.";
        InputStream in = PdfUtils.create( text );
        byte[] tmp = FileIoUtils.readBytes( in );
        File out = File.createTempFile( this.getClass().getSimpleName(), ".pdf" );
        System.out.println( out.getAbsolutePath() );
        FileIoUtils.byteArrayToFile( tmp, out.getAbsolutePath() );
        assertTrue( out.delete() );   // fail if for some reason we cannot
                                      // delete the temp file
    }


	public void testSecured() throws IOException, WrappedDocumentException {
	    
	    files.clear();
	    files.add( FileIoUtils.fileToByteArray( new File( secured )));
	    final String dest = "test-out.pdf";
        
	    try {
			combine( dest, files );
			fail( "Expected WrappedDocumentException" );
		} 
	    catch (WrappedDocumentException e) {
			String msg = e.getMessage();
			assertTrue( msg.endsWith( IS_SECURED ));
	    }

	    File f = new File( dest );
        assertTrue( f.delete() );   // fail if for some reason we cannot
                                    // delete the temp file
	}

	public void testOrder_0() {
	    
	    byte[] pdf1 = "text in pdf1".getBytes();
	    byte[] pdf2 = "text in pdf2".getBytes();
	    Map<String, byte[]> pdfs = new HashMap<String, byte[]>();
	    pdfs.put("file1.pdf", pdf1);
	    pdfs.put("file2.pdf", pdf2);
	    
	    List<String> order = new ArrayList<String>();
	    order.add("file2.pdf");
	    
	    List<byte[]> ordered = PdfUtils.order(pdfs, order);
	    
	    assertEquals(ordered.size(), 1);
	    assertEquals(new String(ordered.get(0)), "text in pdf2");
	    
	}
	    
	public void testOrder_1() {
	    
	    byte[] pdf1 = "text in pdf1".getBytes();
	    byte[] pdf2 = "text in pdf2".getBytes();
	    Map<String, byte[]> pdfs = new HashMap<String, byte[]>();
	    pdfs.put("file1.pdf", pdf1);
	    pdfs.put("file2.pdf", pdf2);
	    
	    List<String> order = new ArrayList<String>();
	    order.add("file2.pdf");
	    order.add("file1.pdf");
	    
	    List<byte[]> ordered = PdfUtils.order(pdfs, order);
	    
	    assertEquals(ordered.size(), 2);
	    assertEquals(new String(ordered.get(0)), "text in pdf2");
	    assertEquals(new String(ordered.get(1)), "text in pdf1");
	    
	}	    

}
