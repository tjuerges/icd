/**
 * TestPdfUtilitites.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import junit.framework.TestCase;

/**
 * <strong>NOTE</strong>: when running this test case from Eclipse you will need
 * to make sure the <em>ACS.managerhost</em> and <em>ACS.baseport</em> system
 * properties are correctly set in the "VM arguments" panel of the run
 * configuration; for instance
 * <code>-DACS.managerhost=ite2.hq.eso.org&nbsp;-DACS.baseport=0</code>
 * 
 * @author mlonjare, July 09, 2010
 */

// $Id$

public class TestBulkStoreUtils extends TestCase {

    // TODO: if we had a method to insert pdfs in the bulkstore we could insert
    // one ourselves and test with that one.
    // For now test with the one Paola inserted for us.
	private static final String BULK_STORE_UID = "uid://X090/Xc6/X6";
	private static final String PDF1 = "TA_Cover.pdf";
    private static final String PDF2 = "CSVResubmitted.pdf";
    private static final String PDF3 = "TA_ScienceGoals.pdf";
    
    /** Size of the generated PDF file on Windows XP */
    private static final int WINDOWS_SIZE = 26277;
    
    /** Size of the generated PDF file on Linux */
    private static final int LINUX_SIZE = 26265;
    
    @Override
    public void setUp() {
        GeneralUtils.setLogLevel( "FINER" );
    }

    public void testRetrieve() throws IOException, WrappedAcsException,
            MessagingException, WrappedDocumentException {
    	
		List<String> ordering = new ArrayList<String>();
        ordering.add( PDF2 );
        ordering.add( PDF1 );
        ordering.add( PDF3 );
	   	InputStream stream = BulkStoreUtils.pdfRetrieve( BULK_STORE_UID, 
	   	                                                 ordering );
		byte[] tmp = FileIoUtils.readBytes( stream );
		final int size = 
		    System.getProperty( "os.name" ).contains( "dows" ) ? WINDOWS_SIZE 
		                                                       : LINUX_SIZE;
        if( tmp.length != size ) {
            File invalidSizePdf = new File( "invalid-size.pdf" );
            OutputStream os = new FileOutputStream( invalidSizePdf );
            os.write( tmp );
            os.flush();
            os.close();
            final String msg = "Invalid PDF file size; see " + 
                               invalidSizePdf.getAbsolutePath();
            assertEquals( msg, size, tmp.length );  // this will fail
        }
    }
}
