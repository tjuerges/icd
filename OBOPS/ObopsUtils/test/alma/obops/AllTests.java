/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jan 28, 2010
 * @version $Revision$
 */

// $Id$

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.obops" );
        //$JUnit-BEGIN$

        //$JUnit-END$

        suite.addTest( alma.obops.utils.AllTests.suite() );
        suite.addTest( alma.obops.ldap.AllTests.suite() );
        suite.addTest( alma.obops.sso.AllTests.suite() );
        suite.addTest( alma.obops.mailmerge.AllTests.suite() );
        return suite;
    }

}
