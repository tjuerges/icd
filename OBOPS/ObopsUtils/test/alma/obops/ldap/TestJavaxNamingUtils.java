/**
 * TestLdapUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import junit.framework.TestCase;

/**
 * @author amchavan, Mar 9, 2011
 * @version $Revision: 1.2 $
 */

// $Id: TestJavaxNamingUtils.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class TestJavaxNamingUtils extends TestCase {
    
// the settings below are for an LDAP server running locally
// within Apache Directory Studio -- useful for testing
// server restart and our resilience to that
//     protected static final String LDAP_URL = "ldap://localhost:10389";
//     protected static final String PRINCIPAL_DN = "uid=admin,ou=system";
//     protected static final String PASSWORD = "secret";
//     protected static final String ACCOUNTS_OU = "ou=users,ou=system";
//     protected static final String ROLES_OU = "ou=groups,ou=system";

// Settings for the standard ALMA readUser  
//    protected static final String PRINCIPAL_DN = "uid=readUser,ou=master,dc=alma,dc=info";
//    protected static final String PASSWORD = "oS!iMLDAP!";
    
     protected static final String LDAP_URL = "ldap://ngas01.hq.eso.org:389";
     protected static final String PRINCIPAL_DN = "uid=admin,ou=people,ou=master,dc=alma,dc=info";
     protected static final String PASSWORD = "admin";
     protected static final String ACCOUNTS_OU = "ou=people,ou=master,dc=alma,dc=info";
     protected static final String ROLES_OU = "ou=roles,ou=master,dc=alma,dc=info";


    protected InitialDirContext ldapContext;

    public void setUp() {
        try {
            ldapContext = JavaxNamingUtils.getLdapContext( LDAP_URL,
                                                           PRINCIPAL_DN,
                                                           PASSWORD );
        }
        catch( NamingException e ) {
            String msg = "failed to connect to " + LDAP_URL + " as "
                    + PRINCIPAL_DN + " (" + PASSWORD + "): " + e.getMessage();
            if( e.getCause() != null ) {
                msg += ", " + e.getCause().getMessage();
            }
            fail( msg );
        }
    }
    
    public void tearDown() throws NamingException {
        ldapContext.close();
    }

    // This test would always fail during NRI, so the assertFalse()
    // line is commented out
    public void testClone() throws NamingException {
        
        // before reaching the following line, stop the LDAP server
//        assertFalse( JavaxNamingUtils.isActive( ldapContext ));

        // before reaching the following line, restart the LDAP server
        InitialDirContext newContext = JavaxNamingUtils.clone( ldapContext );
        assertTrue( JavaxNamingUtils.isActive( newContext ));
    }
    
    public void testGetBaseDNs() throws NamingException {
        String[] baseDNs = JavaxNamingUtils.getBaseDNs( ldapContext );
        assertNotNull( baseDNs );
        assertFalse( baseDNs.length == 0 );
        
        for( int i = 0; i < baseDNs.length; i++ ) {
            String baseDN = baseDNs[i];     // baseDn is like dc=example,dc=com
            String[] tmp = baseDN.split( "," );
            for( int j = 0; j < tmp.length; j++ ) {
                String element = tmp[j];    // element is like dc=example
                assertTrue( element.contains( "=" ));
            }
        }
        
        // can't really make any other assumptions as to the contents of the 
        // server, so here we just pretty-print the base DNs
        System.out.print( "[" );
        for( int i = 0; i < baseDNs.length; i++ ) {
            if( i > 0 ) {
                System.out.print( ", " );
            }
            System.out.print( baseDNs[i] );
        }
        System.out.println( "]" );
    }
    
    public void testSearch() throws NamingException {
        NamingEnumeration<SearchResult> results = 
            JavaxNamingUtils.search( ldapContext,
                                     "(uid=*)",
                                     SearchControls.ONELEVEL_SCOPE,
                                     null,
                                     ACCOUNTS_OU );
        assertNotNull( results );
        assertTrue( results.hasMore() );
        
        while( results.hasMore() ) {
            SearchResult result = (SearchResult) results.next();
            System.out.println( result.getName());
        }
    }

    // This test would always fail during NRI, so the assertFalse()
    // line is commented out
    public void testStatus() {
        // before reaching the following line, make sure the LDAP server
        // is up and running
        assertTrue( JavaxNamingUtils.isActive( ldapContext ));
        
        // before reaching the following line, stop the LDAP server
//        assertFalse( JavaxNamingUtils.isActive( ldapContext ));
    }
}
