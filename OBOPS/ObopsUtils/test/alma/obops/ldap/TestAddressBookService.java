/**
 * TestAlmaUserDetail.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import java.io.IOException;
import java.util.Map;

import junit.framework.TestCase;
import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.roledirectory.RoleDirectory;
import alma.userrepository.roledirectory.RoleDirectorySession;

/**
 * @author amchavan, Jan 27, 2010
 * @version $Revision$
 */

// $Id$
public class TestAddressBookService extends TestCase {

    // should be always there in the test LDAP server
    private static final String USERNAME = "paul";

    public void setUp() {
        // terrible hack!
        if( System.getProperty( "ACS.data" ) == null ) {
            String pwd = System.getProperty( "user.dir" );
            if( pwd.endsWith( "test" )) {
                System.setProperty( "ACS.data", "." );  
            }
            else {
                System.setProperty( "ACS.data", "./test" );  
            }
        }
         //UnitTestUtilities.resetConnectionProperties();
    }
    
    public void test01() throws UserRepositoryException {
        AddressBookService service = new AddressBookService();
        String attr0 = AddressBookService.FIRST_NAME;
        String[] attrs = { attr0 };
        String out = service.getAttributes( USERNAME, attrs ).get( attr0 );
        assertEquals( "Paul", out );
    }
    
    public void test02() throws UserRepositoryException {
        AddressBookService service = new AddressBookService();
        String attr0 = AddressBookService.FIRST_NAME;
        String attr1 = AddressBookService.LAST_NAME;
        String[] attrs = { attr0, attr1 };
        Map<String,String> ret = service.getAttributes( USERNAME, attrs );
        assertEquals( "Paul", ret.get( attr0 ));
        assertEquals( "McCartney", ret.get( attr1 ));
    }
    
    // Full access to LDAP Directory to retrieve several attributes
    // at once about a user
    public void testFull() throws UserRepositoryException, IOException {
        
        // LDAP connection parameters are retrieved from a file
        // to be found in the classpath; we configure the LDAP
        // session factory with those parameters
    	// -----
    	// This is not going to be needed anymore
    	// ----    	
//        URL url =
//            TestAlmaUserDetailService.class.getResource("data/good.properties");
//        InputStream stream = url.openStream();
//        LdapSessionFactory.configure( stream );

        // Create an AddressBook service
        AddressBookService service = new AddressBookService();
        
        // Query for first name, last name and email
        String[] attrs = { AddressBookService.FIRST_NAME, 
                           AddressBookService.LAST_NAME, 
                           AddressBookService.EMAIL };
        Map<String,String> ret = service.getAttributes( USERNAME, attrs );
        
        // See that we got the right stuff
        assertEquals( "Paul", ret.get( AddressBookService.FIRST_NAME ));
        assertEquals( "McCartney", ret.get( AddressBookService.LAST_NAME ));
        assertEquals( "paul@thebeatles.com", ret.get( AddressBookService.EMAIL ));
    }

    
    public void test10() throws UserRepositoryException {
        AddressBookService service = new AddressBookService();
        String out = service.getAttribute( USERNAME, 
                                           AddressBookService.EMAIL );  
        assertEquals( "paul@thebeatles.com", out );
    }
    
    public void test11() throws UserRepositoryException {
        AddressBookService service = new AddressBookService();
        String out = service.getAttribute( USERNAME, 
                                           AddressBookService.FIRST_NAME );   
        assertEquals( "Paul", out );
    }
    
    public void test12() throws UserRepositoryException {
        AddressBookService service = new AddressBookService();
        String out = service.getAttribute( USERNAME, 
                                           AddressBookService.LAST_NAME ); 
        assertEquals( "McCartney", out );
    }
    
    public void testGetRoleDirectorySession() throws UserRepositoryException{
        AddressBookService service = new AddressBookService();
		RoleDirectorySession dirSess = service.getRoleDirectorySession();
		RoleDirectory roles = dirSess.getAllUserRoles(USERNAME);
        dirSess.close();
        // As the number of roles change quite dinamically,
        // we only want to know if there is any, the exact number is quite irrelevant
        assertTrue( roles.roleCount() > 0);        
    }
    

    // stress test, repeat query many times
    public void testStress() {
        final int repeats = 100;
        AddressBookService service = new AddressBookService();
        for( int i = 0; i < repeats; i++ ) {
            try {
                service.getAttribute( USERNAME, AddressBookService.LAST_NAME );
                Thread.sleep( 100 );     // can't be too fast!
            }
            catch( UserRepositoryException e ) {
                e.printStackTrace();
                fail( "Attempt no. " + i + ": " + e.getMessage() );
            }
            catch( InterruptedException e ) {
                // ignore
            }
        }
    }
}
