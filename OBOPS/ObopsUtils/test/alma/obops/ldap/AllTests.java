/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jan 28, 2010
 * @version $Revision: 1.3 $
 */

// $Id: AllTests.java,v 1.3 2011/03/24 09:49:31 fjulbe Exp $

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.obops.ldap" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestJavaxNamingUtils.class );
        suite.addTestSuite( TestAddressBookService.class );
        suite.addTestSuite( TestAlmaLdapConnection.class );
        //$JUnit-END$
        return suite;
    }

}
