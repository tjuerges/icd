/**
 * TestAlmaLdapConnection.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.naming.CommunicationException;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;

import alma.obops.utils.TestUtilities;

import junit.framework.TestCase;


/**
 * @author amchavan, Mar 10, 2011
 * @version $Revision$
 */

// $Id$

public class TestAlmaLdapConnection extends TestCase {
    
 // the settings below are for an LDAP server running locally
 // within Apache Directory Studio -- useful for testing
 // server restart and our resilience to that
//      protected static final String LDAP_URL = "ldap://localhost:10389";
//      protected static final String PRINCIPAL_DN = "uid=admin,ou=system";
//      protected static final String PASSWORD = "secret";
//      protected static final String ACCOUNTS_OU = "ou=users,ou=system";
//      protected static final String ROLES_OU = "ou=groups,ou=system";

 // Settings for the standard ALMA readUser  
//     protected static final String PRINCIPAL_DN = "uid=readUser,ou=master,dc=alma,dc=info";
//     protected static final String PASSWORD = "oS!iMLDAP!";
     
      protected static final String LDAP_URL = "ldap://ngas01.hq.eso.org:389";
      protected static final String PRINCIPAL_DN = "uid=admin,ou=people,ou=master,dc=alma,dc=info";
      protected static final String PASSWORD = "admin";
      protected static final String ACCOUNTS_OU = "ou=people,ou=master,dc=alma,dc=info";
      protected static final String ROLES_OU = "ou=roles,ou=master,dc=alma,dc=info";


    private AlmaLdapConnection connection;

    /**
     * Utility method, creates a directory entry
     * @return The simple ID of the entry
     * @throws NamingException
     */
    public String createDirectoryEntry() throws NamingException {
        List<Property> properties = new ArrayList<Property>();
        properties.add( new Property( "cn", "value for cn" ));
        properties.add( new Property( "sn", "value for sn" ));
        String uid = TestUtilities.newID( this.getClass().getSimpleName() );
        connection.createEntry( uid, properties );
        return uid;
    }
    
    public void setUp() throws AlmaLdapConnectionException {
        this.connection = new AlmaLdapConnection( LDAP_URL, 
                                                  PRINCIPAL_DN, PASSWORD, 
                                                  ACCOUNTS_OU, ROLES_OU );
    }
    
    public void tearDown() {
        this.connection = null;
        System.gc();
    }
    
    public void testAddRemoveRoles() throws NamingException, AlmaLdapConnectionException {
        String uid = createDirectoryEntry();

        // initially we have no roles for this account
        SortedSet<String> roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 0, roles.size() );
        
        final String role1 = "OBOPS/APRC";
        final String role2 = "OBOPS/ARP";
        boolean retcode;
        
        // try to add a role
        assertFalse( connection.hasRole( role1, uid ));
        assertFalse( connection.hasRole( role2, uid ));
        retcode = connection.addRoleToAccount( role1, uid );
        assertTrue( retcode );
        assertTrue( connection.hasRole( role1, uid ));
        assertFalse( connection.hasRole( role2, uid ));
        
        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 1, roles.size() );
        assertEquals( role1, roles.first() );
        
        // adding again should have no effect
        retcode = connection.addRoleToAccount( role1, uid );
        assertFalse( retcode );
        assertTrue( connection.hasRole( role1, uid ));
        assertFalse( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 1, roles.size() );
        assertEquals( role1, roles.first() );
        
        // try to add a second role
        retcode = connection.addRoleToAccount( role2, uid );
        assertTrue( retcode );
        assertTrue( connection.hasRole( role1, uid ));
        assertTrue( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 2, roles.size() );
        assertEquals( role1, roles.first() );
        roles.remove( roles.first() );
        assertEquals( role2, roles.first() );
        
        // adding again should have no effect
        retcode = connection.addRoleToAccount( role2, uid );
        assertFalse( retcode );
        assertTrue( connection.hasRole( role1, uid ));
        assertTrue( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 2, roles.size() );
        assertEquals( role1, roles.first() );
        roles.remove( roles.first() );
        assertEquals( role2, roles.first() );
        
        // now remove a role
        retcode = connection.removeRoleFromAccount( role1, uid );
        assertTrue( retcode );
        assertFalse( connection.hasRole( role1, uid ));
        assertTrue( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 1, roles.size() );
        assertEquals( role2, roles.first() );
        
        // removing again should have no effect
        retcode = connection.removeRoleFromAccount( role1, uid );
        assertFalse( retcode );
        assertFalse( connection.hasRole( role1, uid ));
        assertTrue( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 1, roles.size() );
        assertEquals( role2, roles.first() );
        
        // now remove the second role
        retcode = connection.removeRoleFromAccount( role2, uid );
        assertTrue( retcode );
        assertFalse( connection.hasRole( role1, uid ));
        assertFalse( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 0, roles.size() );
        
        // removing again should have no effect
        retcode = connection.removeRoleFromAccount( role2, uid );
        assertFalse( retcode );
        assertFalse( connection.hasRole( role1, uid ));
        assertFalse( connection.hasRole( role2, uid ));

        roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 0, roles.size() );
        
        connection.deleteEntry( uid );  // clean up after ourselves...
    }
    
    public void testComputeDifferences() {
        List<Property> oldProperties = new ArrayList<Property>();
        Property op1 = new Property( "to-be-removed-1", "value1" );
        Property op2 = new Property( "to-be-removed-2", "value1" );
        Property op3 = new Property( "to-be-replaced", "value" );
        Property op4 = new Property( "unchanged-1", "value31" );
        Property op5 = new Property( "unchanged-2", "value32" );

        oldProperties.add( op1 );
        oldProperties.add( op2 );
        oldProperties.add( op3 );
        oldProperties.add( op4 );
        oldProperties.add( op5 );
        
        List<Property> newProperties = new ArrayList<Property>();
        Property newProp1 = new Property( "to-be-added-1", "value1" );
        Property newProp2 = new Property( "to-be-added-2", "value2" );
        Property newProp3 = new Property( "to-be-replaced", "new value1" );
        Property newProp5 = new Property( "unchanged-1", "value31" );
        Property newProp6 = new Property( "unchanged-2", "value32" );

        newProperties.add( newProp1 );
        newProperties.add( newProp2 );
        newProperties.add( newProp3 );
        newProperties.add( newProp5 );
        newProperties.add( newProp6 );

        Attributes oldAttributes = AlmaLdapConnection.convertToAttributes( oldProperties );
        Attributes newAttributes = AlmaLdapConnection.convertToAttributes( newProperties );

        ModificationItem[] diffs; 
        
        diffs = AlmaLdapConnection.computeDifferences( oldAttributes, 
                                                       oldAttributes );
        assertEquals( 0, diffs.length );
        
        diffs = AlmaLdapConnection.computeDifferences( newAttributes, 
                                                       newAttributes );
        assertEquals( 0, diffs.length );
        
        diffs = AlmaLdapConnection.computeDifferences( oldAttributes, 
                                                       newAttributes );

        // pretty print
        //--------------------------------------------------
        String[] modOpLabels = { null, "ADD_ATTRIBUTE", "REPLACE_ATTRIBUTE",
                "REMOVE_ATTRIBUTE" };
        for( int i = 0; i < diffs.length; i++ ) {
            ModificationItem diff = diffs[i];
            int modOp = diff.getModificationOp();
            String modAttr = diff.getAttribute().getID();
            System.out.println( ">>> " + modAttr + ": " + modOpLabels[modOp] );
        }

        assertEquals( 5, diffs.length );
        
        // Now make sure that we got exactly the right number of
        // additions, replacements and removals
        //-------------------------------------------------------
        int addCount = 0;
        int replaceCount = 0;
        int removeCount = 0;
        
        for( int i = 0; i < diffs.length; i++ ) {
            ModificationItem diff = diffs[i];
            int modOp = diff.getModificationOp();
            switch( modOp ) {
            case DirContext.ADD_ATTRIBUTE:
                addCount++;
                break;
            case DirContext.REPLACE_ATTRIBUTE:
                replaceCount++;
                break;
            case DirContext.REMOVE_ATTRIBUTE:
                removeCount++;
                break;
           default:
               throw new RuntimeException( "Unknown operation: " + modOp );
            }
        }

        assertEquals( 2, addCount );
        assertEquals( 1, replaceCount );
        assertEquals( 2, removeCount );
    }
    
    /* 
     * Test that we survive an LDAP server restart
     * If this test is not run in a debugger it will always pass, so TO
     * MAKE THIS AN EFECTIVE TEST one should:
     *  1. run this test case in debug mode (within Eclipse)
     *  2. place a breakpoint at the line "connection.refresh()" below
     *  3. when it stops, shut down and restart the LDAP server (see LDAP_URL)
     *  4. resume running this test case
     */
    public void testConnectionReset() throws NamingException, 
                                             InterruptedException {    
        
        try {
            // place a breakpoint at the following line!
            connection.refresh();
        }
        catch( CommunicationException e ) {

            Throwable cause = e.getCause();
            if( cause instanceof java.net.SocketException ) {
                // if we get here it means that the server was restarted
                // but we didn't cope with that
                //-----------------------------------------------------
                e.printStackTrace();
                final String msg = 
                    "Unexpected " + 
                    e.getClass().getSimpleName() + 
                    ", didn't cope with LDAP server restart";
                fail( msg );
            }
            else {
                throw e;
            }
        }
    }
    
    public void testConversionToAttribute() {
        List<Property> properties = new ArrayList<Property>();
        Property p00 = new Property( "name0", null );
        Property p01 = new Property( "name0", "" );
        Property  p1 = new Property( "name1", "value1" );
        Property  p2 = new Property( "name2", "value2" );
        Property p31 = new Property( "name3", "value31" );
        Property p32 = new Property( "name3", "value32" );

        properties.add( p00 );
        properties.add( p01 );
        properties.add( p1 );
        properties.add( p2 );
        properties.add( p31 );
        properties.add( p32 );
        
        // convert to Attributes and back
        Attributes converted1 = AlmaLdapConnection.convertToAttributes( properties );
        List<Property> converted2 = AlmaLdapConnection.convertToPropertyList( converted1 );

        assertEquals( 4 +       // attributes p1, p2, p31, p32 
                      3 +       // "uidNumber", "gidNumber",  "homeDirectory"
                      AlmaLdapConnection.OBJECT_CLASSES.length, // "objectClass"
                      converted2.size() );
        
        assertTrue( converted2.contains( p1 ));
        assertTrue( converted2.contains( p2 ));
        assertTrue( converted2.contains( p31 ));
        assertTrue( converted2.contains( p32 ));
        
        assertFalse( converted2.contains( p00 ));
        assertFalse( converted2.contains( p01 ));
        
        for( int i = 0; i < AlmaLdapConnection.OBJECT_CLASSES.length; i++ ) {
            String objClass = AlmaLdapConnection.OBJECT_CLASSES[i];
            Property objClassProp = new Property( "objectClass", objClass );
            assertTrue( converted2.contains( objClassProp ));
        }
    }

    // conversion of roles from Alma to LDAP notation and back
    public void testConvertToFromAlmaNotation() {

        String roleAlma = "A/B";
        String roldLdap = AlmaLdapConnection.convertToLdapNotation( roleAlma );
        assertEquals( roleAlma, AlmaLdapConnection.convertToAlmaNotation( roldLdap ));
        
        roldLdap = "cn=B,ou=A," + AlmaLdapConnection.getRolesOU();
        roleAlma = "A/B";
        roldLdap = AlmaLdapConnection.convertToLdapNotation( roleAlma );
        assertEquals( roleAlma, AlmaLdapConnection.convertToAlmaNotation( roldLdap ));
    }
    
    public void testCreateEntry() throws NamingException, AlmaLdapConnectionException {
        
        String uid = createDirectoryEntry();
        
        final String uidSpec = "uid=" + uid;
        SortedSet<String> names;
        names = connection.searchNames( uidSpec, SearchControls.ONELEVEL_SCOPE,
                                        null, ACCOUNTS_OU );
        assertNotNull( names );
        assertEquals( 1, names.size() );
        assertTrue( names.first().startsWith( uidSpec ));
        
        connection.deleteEntry( uid );
        names = connection.searchNames( uidSpec, SearchControls.ONELEVEL_SCOPE,
                                        null, ACCOUNTS_OU );
        assertNotNull( names );
        assertEquals( 0, names.size() );
    }
    
    public void testDeleteEntry() {
        final String uid = "This account does not exist, by Jove!";
        try {
            connection.deleteEntry( uid );
        }
        catch( NamingException e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
    }
    
//    public void testFindAllDerivedRoles() throws AlmaLdapConnectionException {
//        SortedSet<String> roles = connection.findAllDerivedRoles();
//        assertNotNull( roles );
//        assertFalse( roles.contains( "OBOPS/ARCA" ));
//        assertTrue( roles.contains( "OBOPS/DSOA" ));
//    }
    
//    public void testFindAllRoleDNsFor() throws AlmaLdapConnectionException {
//        String uid = "obops";
//        String dn = AlmaLdapConnection.convertToDn( uid );
//        SortedSet<String> roles = connection.findAllRoleDNsFor( dn );
//        assertNotNull( roles );
//        assertEquals( 4, roles.size() );
//        assertTrue( roles.contains( "cn=ARCA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
//        assertTrue( roles.contains( "cn=PHT,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
//        assertTrue( roles.contains( "cn=QAA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
//        assertTrue( roles.contains( "cn=DSOA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
//    }
    
    public void testFindAllRoles() throws AlmaLdapConnectionException {
        SortedSet<String> roles = connection.findAllRoles();
        assertNotNull( roles );
        assertTrue( roles.contains( "OBOPS/ARCA" ));
        assertTrue( roles.contains( "MASTER/USER" ));
        assertTrue( roles.contains( "OMC/ASTRONOMER_ON_DUTY" ));
    }
    
    public void testFindRoleDNsFor() throws AlmaLdapConnectionException {
        String uid = "unit-test-user";
        String dn = AlmaLdapConnection.convertToAccountDn( uid );
        SortedSet<String> roles = connection.findDirectRoleDNsFor( dn );
        assertNotNull( roles );
        assertEquals( 3, roles.size() );
        assertTrue( roles.contains( "cn=ARCA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
        assertTrue( roles.contains( "cn=PHT,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
        assertTrue( roles.contains( "cn=QAA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
        assertFalse( roles.contains( "cn=DSOA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info" ));
    }
    
//    public void testFindRolesForDN() throws AlmaLdapConnectionException {
//        String uid = "obops";
//        String dn = AlmaLdapConnection.convertToDn( uid );
//        SortedSet<String> roles = connection.findRolesForDN( dn );
//        assertNotNull( roles );
//        assertEquals( 4, roles.size() );
//        assertTrue( roles.contains( "OBOPS/ARCA" ));
//        assertTrue( roles.contains( "OBOPS/PHT" ));
//        assertTrue( roles.contains( "OBOPS/QAA" ));
//        assertTrue( roles.contains( "OBOPS/DSOA" ));
//    }
    
    public void testFindRolesForUID() throws AlmaLdapConnectionException {
        String uid = "unit-test-user";
        SortedSet<String> roles = connection.findRolesForUID( uid );
        assertNotNull( roles );
        assertEquals( 4, roles.size() );
        assertTrue( roles.contains( "OBOPS/ARCA" ));
        assertTrue( roles.contains( "OBOPS/PHT" ));
        assertTrue( roles.contains( "OBOPS/QAA" ));
        assertTrue( roles.contains( "OBOPS/DSOA" ));
    }
    
    public void testFindSuperRoles() throws AlmaLdapConnectionException {

        Set<String> superRoles = connection.findSuperRoles( "OBOPS/ARCA" );
        assertNotNull( superRoles );
        assertEquals( 1, superRoles.size() );
        assertTrue( superRoles.contains( "OBOPS/DSOA" ) );
        
        superRoles = connection.findSuperRoles( "OBOPS/DSOA" );
        assertNotNull( superRoles );
        assertEquals( 0, superRoles.size() );

        String role2 = "MASTER/ADMINISTRATOR";
        superRoles = connection.findSuperRoles( role2 );
        assertNotNull( superRoles );
        assertEquals( 11, superRoles.size() );
        assertTrue( superRoles.contains( "MASTER/AUDITOR" ));
        assertTrue( superRoles.contains( "MASTER/MANAGER" ));
        assertTrue( superRoles.contains( "MASTER/OPERATOR" ));
        assertTrue( superRoles.contains( "OMC/MASTER_OPERATOR" ));
    }
    
    public void testGetRolesTree() throws AlmaLdapConnectionException  {
        List<List<String>> roleDescriptors = connection.getRolesTree();
        assertNotNull( roleDescriptors );
        assertTrue( roleDescriptors.size() > 0 );
        boolean foundARCA = false;
        boolean foundDSOA = false;
        for( List<String> roleDescriptor : roleDescriptors ) {
            if( roleDescriptor.get( 0 ).equals( "OBOPS/ARCA" )) {
                foundARCA = true;
                assertEquals( 1, roleDescriptor.size() );
            }
            else if( roleDescriptor.get( 0 ).equals( "OBOPS/DSOA" )) {
                foundDSOA = true;
                assertEquals( 4, roleDescriptor.size() );
            }
        }
        
        assertTrue( foundARCA );
        assertTrue( foundDSOA );
    }
    
    public void testGetRoleMembers() throws AlmaLdapConnectionException  {
        List<List<String>> roleDescriptors = connection.getRoleMembers();
        assertNotNull( roleDescriptors );
        assertTrue( roleDescriptors.size() > 0 );
        for( List<String> roleDescriptor : roleDescriptors ) {
            if( roleDescriptor.get( 0 ).equals( "OBOPS/PHT" )) {
                assertTrue( roleDescriptor.size() > 0 );
                assertTrue( roleDescriptor.contains( "obops" ));
            }
        }
    }
    
    public void testReadEntry() throws AlmaLdapConnectionException {
        String uid = "obops";
        List<Property> properties = connection.readEntry( uid );
        assertNotNull( properties );
        assertTrue( properties.size() > 0 );
        for( Property property : properties ) {
            System.out.print( property.name + "=" + property.value + " " );
        }
    }
    
    public void testSearch00() throws AlmaLdapConnectionException {
        
        Set<Object> results = 
            connection.search( "(uid=*)", SearchControls.ONELEVEL_SCOPE, null,
                               ACCOUNTS_OU, 
                               AlmaLdapConnection.EXTRACT_NAME_NAMESPACE );
        assertNotNull( results );
        
        int size = results.size();
        assertTrue( size > 0 );
        
        // verify results are simple names
        for( Object result : results ) {
            assertTrue( result.toString().contains( "," ));
        }
    }
    
    public void testSearch01() throws AlmaLdapConnectionException, NamingException {
        
        Set<Object> results = 
            connection.search( "(uid=obops)", SearchControls.ONELEVEL_SCOPE, null,
                               ACCOUNTS_OU, 
                               AlmaLdapConnection.EXTRACT_ATTRIBUTES );
        assertNotNull( results );
        
        int size = results.size();
        assertTrue( size == 1 );
        
        for( Object tmp : results ) {
            @SuppressWarnings("unchecked")
            List<Property> properties = (List<Property>) tmp;
            for( Property property : properties ) {
                String msg = property.name + "=" + property.value;
                System.out.println( msg );
            }
        }
    }
    
    public void testSearchNames() throws AlmaLdapConnectionException {
        SortedSet<String> result = 
            connection.searchNames( "(uid=*)", SearchControls.ONELEVEL_SCOPE,
                                    null, ACCOUNTS_OU );
        assertNotNull( result );
        
        int size = result.size();
        assertTrue( size > 0 );
        
        // verify results are names with namespaces
        for( String string : result ) {
            assertFalse( string.contains( "," ));
        }
    }

    public void testSearchNamesNamespaces() throws AlmaLdapConnectionException {
        SortedSet<String> result = 
            connection.searchNamesNamespaces( "(uid=*)",
                                              SearchControls.ONELEVEL_SCOPE,
                                              null,
                                              ACCOUNTS_OU );
        assertNotNull( result );
        
        int size = result.size();
        assertTrue( size > 0 );
        
        // verify results are names with namespaces
        for( String string : result ) {
            assertTrue( string.contains( "," ));
        }
    }
    
    public void testFindSubRoles() throws AlmaLdapConnectionException {
        String role0 = "OBOPS/QAA";
        SortedSet<String> subroles0 = connection.findSubRoles( role0 );
        assertNotNull( subroles0 );
        assertEquals( 0, subroles0.size() );

        String role1 = "OBOPS/DSOA";
        SortedSet<String> subroles1 = connection.findSubRoles( role1 );
        assertNotNull( subroles1 );
        assertEquals( 3, subroles1.size() );
        assertTrue( subroles1.contains( role0 ));

        String role2 = "MASTER/AUDITOR";
        SortedSet<String> subroles2 = connection.findSubRoles( role2 );
        assertNotNull( subroles2 );
        assertEquals( 3, subroles2.size() );
        assertTrue( subroles2.contains( "MASTER/ADMINISTRATOR" ));
    }
    
    public void testFindIDsByRole() throws AlmaLdapConnectionException {
        
        String role0 = "OBOPS/QAA";
        SortedSet<String> ids0 = connection.findIDsByRole( role0 );
        assertNotNull( ids0 );
        assertTrue( ids0.contains( "obops" ));
        
        String role1 = "OBOPS/DSOA";
        SortedSet<String> ids1 = connection.findIDsByRole( role1 );
        assertNotNull( ids1 );
        assertTrue( ids1.contains( "obops" ));
        assertTrue( ids1.contains( "ringo" ));
        
        String role2 = "MASTER/AUDITOR";
        SortedSet<String> ids2 = connection.findIDsByRole( role2 );
        assertNotNull( ids2 );
        assertTrue( ids2.contains( "amanning2" ));
        assertTrue( ids2.contains( "amanning" ));
    }
}
