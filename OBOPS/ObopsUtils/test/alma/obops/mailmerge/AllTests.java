/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.mailmerge;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jan 28, 2010
 * @version $Revision$
 */

// $Id$

public class AllTests {

    public static Test suite() {
        
        TestSuite suite = new TestSuite( "Test for alma.obops.mailmerge" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestMailMergerImpl.class );
        //$JUnit-END$
        return suite;
    }

}
