/**
 * TestMailMergerImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.mailmerge;

import static alma.obops.mailmerge.MailMerger.DEFAULT_PLACEHOLDER_END_DELIMITER;
import static alma.obops.mailmerge.MailMerger.DEFAULT_PLACEHOLDER_START_DELIMITER;
import static alma.obops.mailmerge.MailMerger.MERGE_PLACEHOLDER_RE;
import static alma.obops.mailmerge.MailMergerImpl.array2list;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import junit.framework.TestCase;

/**
 * @author amchavan, Aug 16, 2010
 * @version $Revision: 1.6 $
 */

// $Id: TestMailMergerImpl.java,v 1.6 2011/04/15 15:04:51 fjulbe Exp $

public class TestMailMergerImpl extends TestCase {

    public void setUp() {
        // terrible hack!
        if( System.getProperty( "ACS.data" ) == null ) {
            String pwd = System.getProperty( "user.dir" );
            if( pwd.endsWith( "test" )) {
                System.setProperty( "ACS.data", "." );  
            }
            else {
                System.setProperty( "ACS.data", "./test" );  
            }
        }
    }
    
    // very basic test
    public void test00() throws FileNotFoundException, IOException {
        
        MailMerger mm = MailMergerFactory.getMailMerger();        
        String re = DEFAULT_PLACEHOLDER_START_DELIMITER + "(" +
                    MERGE_PLACEHOLDER_RE + ")" +
                    DEFAULT_PLACEHOLDER_END_DELIMITER;
        assertEquals( re, ((MailMergerImpl)mm).placeholderRegExp );
        
        String template = "";
        List<Map<String,String>> params = new ArrayList<Map<String,String>>();
        List<String> ret = mm.merge( template, params );
        assertNotNull( ret );
        assertEquals( 0, ret.size() );
        
        
    }

    // very basic merging
    public void test01() throws FileNotFoundException, IOException {
        
        MailMergerImpl mm = (MailMergerImpl) MailMergerFactory.getMailMerger();        
        String template = "%A%";
        
        String[][] input = {
                {"A=a"},  
                {"A=b"},
                {}
        };
        List<Map<String,String>> params = array2list( input );
        
        List<String> ret = mm.merge( template, params );

        assertEquals( 1, mm.placeholders.size() );
        assertEquals( "A", mm.placeholders.get( 0 ));
        assertEquals( 3, ret.size() );
        assertEquals( "a", ret.get( 0 ));
        assertEquals( "b", ret.get( 1 ));
        assertEquals( "",  ret.get( 2 ));
    }

    // delimiters
    public void test02() throws FileNotFoundException, IOException {
        
        MailMergerImpl mm = (MailMergerImpl) MailMergerFactory.getMailMerger();     
        mm.setDelimiters( "<<", ">>" );
        String template = "<<A>><<A>>";
        
        String[][] input = {
                {"A=a"},  
                {"A=b"},
                {}
        };
        List<Map<String,String>> params = array2list( input );
        
        List<String> ret = mm.merge( template, params );

        assertEquals( 2, mm.placeholders.size() );
        assertEquals( "A", mm.placeholders.get( 0 ));
        assertEquals( 3, ret.size() );
        assertEquals( "aa", ret.get( 0 ));
        assertEquals( "bb", ret.get( 1 ));
        assertEquals( "",   ret.get( 2 ));
    }


    // more ambitious merging -- as per javadoc
    public void test03() throws FileNotFoundException, IOException {
        
        MailMergerImpl mm = (MailMergerImpl) MailMergerFactory.getMailMerger();        
        String template = 
            "Dear Mr. %name%, room %room% was reserved for you from %start% to %end%.";

        String[][] input = {
                { "name=Jones", 
                  "room=103", 
                  "start=June 1st, 2011",
                  "end=July 1st, 2011" },
                { "name=Smith", 
                  "room=462", 
                  "start=April 15, 2011" },
        };
        List<Map<String,String>> params = array2list( input );
        
        List<String> ret = mm.merge( template, params );

        assertEquals( 4, mm.placeholders.size() );
        assertEquals( "name", mm.placeholders.get( 0 ));
        assertEquals( 2, ret.size() );
        assertEquals( "Dear Mr. Jones, room 103 was reserved for you " +
                      "from June 1st, 2011 to July 1st, 2011.", 
                      ret.get( 0 ));
        assertEquals( "Dear Mr. Smith, room 462 was reserved for you " +
                      "from April 15, 2011 to .", 
                      ret.get( 1 ));
    }

    // Check that we catch null parameters
    public void testBad00() throws FileNotFoundException, IOException {
        MailMerger mm = MailMergerFactory.getMailMerger();

        try {
            List<Map<String,String>> params = new ArrayList<Map<String,String>>();
            mm.merge( null, params );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // no-op, expected
        }
        
        try {
            String template = "";
            mm.merge( template, null );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // no-op, expected
        }
    }

    // bad mail merge
    public void testBad01() throws MessagingException, FileNotFoundException, IOException {
        
        MailMerger mm = MailMergerFactory.getMailMerger();        
        String template = "";
    
        String[][] input = {
                {},
        };
        List<Map<String,String>> params = array2list( input );
        
        try {
            mm.mailMerge( template, params, null, null, null );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e )  {
            // no-op, expected
        }
        
        try {
            mm.mailMerge( template, params, null, null, "bogus" );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e )  {
            // no-op, expected
        }
    }
    
    // internal email test for development, commented out
//    public void testMail00() throws MessagingException, FileNotFoundException, IOException {
//        MailMergerImpl mm = (MailMergerImpl) MailMergerFactory.getMailMerger();   
//        //           recipientTO, recipientCC, subject, contentType, contents     
//        mm.sendmail( "amchavan@eso.org", 
//                     "this-address-does-not-exists@eso.org",
//                     "This is a unit test", 
//                     "text/html", 
//                     "<em>Test</em>" );
//    }
    
    // basic email test
    public void testMail01() throws Exception {
        MailMerger mm = MailMergerFactory.getMailMerger();        
        String template = 
            "Subject: [unit test output] Hello %name%\n" +
            "This is an <b>HTML</b> <em>message</em> for %name% (sent by " +
            "<code>" +
            this.getClass().getCanonicalName() + 
            "</code>)";
        
        Map<String,String> map = new HashMap<String,String>();
        map.put( "name", "Maurizio" );
        map.put( "EMAIL", "amchavan@eso.org" );
        
        List<Map<String,String>> params = new ArrayList<Map<String,String>>();
        params.add( map );
                   
        mm.mailMerge( template, // template 
        			params,     // paramSets
        			null,       // recipientCC
        			null,   	// recipientBCC
        			MailMerger.HTML_CONTENT_TYPE // contentType 
        		);
    }
    
    // If you want to spam a few people, un-comment the following test        
    // email test with address override
//    public void testMail02() throws Exception {
//        MailMerger mm = MailMergerFactory.getMailMerger();        
//        String template = 
//            "Subject: [unit test output] Hello %name%\n" +
//            "This is a message for %name% that should actually reach a few people extra (sent by " + 
//            this.getClass().getCanonicalName() + 
//            ")";
//        
//        String[][] input = {
//                { "name=Maurizio", "email=amchavan@eso.org" },
//        };
//        List<Map<String,String>> params = array2list( input );
//  
//        mm.mailMerge( template, params, null, "text/plain", "obops@eso.org" );
//    }
}
