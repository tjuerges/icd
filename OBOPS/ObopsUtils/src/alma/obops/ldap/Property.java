/**
 * Copyright European Southern Observatory 2011
 */

package alma.obops.ldap;


/**
 * A very simple <em>struct</em>-like class to represent a <em>[name,value]</em>
 * pair. We use it to export LDAP attributes to client classes.
 * 
 * Note that we need this class because LDAP attributes can be repeated, so we
 * can't make use of {@link java.util.Properties}
 * 
 * @author amchavan, Mar 11, 2011
 * @version $Revision: 1.2 $
 */

// $Id: Property.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class Property {
    public String name;
    public String value;
    
    public Property( String name, String value ) {
        if( name == null ) {
            throw new IllegalArgumentException( "Arg name cannot be null" );
        }
        this.name = name;
        this.value = value;
    }

    // For testing
    @Override
    public boolean equals( Object obj ) {
        if( obj instanceof Property ) {
            Property other = (Property) obj;
            if( other.name.equals( name ) ) {
                if( other.value == null ) {
                    if( value == null )
                        return true;
                }
                else if( other.value.equals( value ) ) {
                    return true;
                }
            }
        }
        return false;
    }

    // For testing
    @Override
    public String toString() {
        return name + "=" + value;
    }
}