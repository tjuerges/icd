/**
 * LdapUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * A collection of methods and constants to simplify interaction with
 * javax.naming.xxx -- with a special focus on LDAP
 * 
 * @author amchavan, Mar 10, 2011
 * @version $Revision$
 */

// $Id$

public class JavaxNamingUtils {

    public static final String LDAP_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";

    /**
     * @return An InitialDirContext with the same parameters as the input one
     * @throws NamingException
     */
    public static InitialDirContext clone( InitialDirContext context )
            throws NamingException {

        Hashtable<?, ?> env = context.getEnvironment();
        InitialDirContext ret = new InitialDirContext( env );
        return ret;
    }

    // /**
    // * Run a harmless operation on the server, to see if the context is alive
    // * and well. If we get a {@link CommunicationException} caused by a
    // * {@link SocketException}, it means that the server was restarted and we
    // * need to reconnect.
    // */
    // void ensurecontext() throws NamingException {
    // try {
    // context.getAttributes( accountsOU ); // harmless, fast operation
    // }
    // catch( CommunicationException e ) {
    // Throwable cause = e.getCause();
    // if( cause instanceof java.net.SocketException ) {
    // // the server was restarted
    // logger.warning( "LDAP server was restarted, reconnecting" );
    // connectToLDAP();
    // }
    // }
    // }

    /**
     * @param context
     *            LDAP server context
     * 
     * @return The list of Base DNs retrieved from that LDAP server, in
     *         unspecified order; e.g. [
     *         <em>ou=system, dc=example,dc=com, ou=schema</em>]
     * 
     * @throws NamingException
     *             if anything goes wrong
     */
    public static String[] getBaseDNs( InitialDirContext context )
            throws NamingException {

        NamingEnumeration<SearchResult> results = getBaseDNsInternal( context );
        List<String> tmp = new ArrayList<String>();

        // loop over all results
        while( results.hasMore() ) {
            SearchResult result = (SearchResult) results.next();
            Attributes attrs = result.getAttributes();

            // loop over all attributes of this result
            NamingEnumeration<? extends Attribute> attrsEnum = attrs.getAll();
            while( attrsEnum.hasMore() ) {
                Attribute attr = (Attribute) attrsEnum.next();

                // loop over all values of that attribute
                NamingEnumeration<?> attrEnum = attr.getAll();
                while( attrEnum.hasMore() ) {
                    tmp.add( attrEnum.next().toString() );
                }
            }
        }

        return tmp.toArray( new String[tmp.size()] );
    }

    // /**
    // * Run a harmless operation on the server, to see if the context is alive
    // * and well. If we get a {@link CommunicationException} caused by a
    // * {@link SocketException}, it means that the server was restarted and we
    // * need to reconnect.
    // */
    // void ensurecontext() throws NamingException {
    // try {
    // context.getAttributes( accountsOU ); // harmless, fast operation
    // }
    // catch( CommunicationException e ) {
    // Throwable cause = e.getCause();
    // if( cause instanceof java.net.SocketException ) {
    // // the server was restarted
    // logger.warning( "LDAP server was restarted, reconnecting" );
    // connectToLDAP();
    // }
    // }
    // }

    /**
     * Very shallow wrapper around search()
     * 
     * @throws NamingException
     */
    private static NamingEnumeration<SearchResult> getBaseDNsInternal( InitialDirContext context )
            throws NamingException {
        String[] searchAttrs = { "namingContexts" };
        NamingEnumeration<SearchResult> results = search( context,
                                                          "(objectclass=*)",
                                                          SearchControls.OBJECT_SCOPE,
                                                          searchAttrs, "" );
        return results;
    }

    /**
     * Create a new InitialDirContext for performing LDAPv3-style extended
     * operations.
     * 
     * @param providerURL
     *            URL of the LDAP server, e.g.
     *            <em>ldap://ngas01.hq.eso.org:389</em>
     * 
     * @param principal
     *            Distinguished Name of the LDAP account to use; must have
     *            read/write access. E.g.
     *            <em>uid=root,ou=master,dc=alma,dc=info</em>
     * 
     * @param password
     *            Password of the LDAP account to use; e.g. <em>boo!</em>
     * 
     * 
     * @throws NamingException
     */
    public static InitialDirContext getLdapContext( String providerURL,
                                                    String principal,
                                                    String password )
            throws NamingException {

        // Create the LDAP context
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put( Context.INITIAL_CONTEXT_FACTORY, LDAP_CTX_FACTORY );
        env.put( Context.PROVIDER_URL, providerURL );
        env.put( Context.SECURITY_PRINCIPAL, principal );
        env.put( Context.SECURITY_CREDENTIALS, password );
        InitialDirContext ret = new InitialDirContext( env );
        return ret;
    }

    /**
     * @param context
     *            LDAP server connection
     * 
     * @return <code>true</code> if the input context is active and can be used,
     *         <code>false</code> otherwise
     */
    public static boolean isActive( InitialDirContext context ) {

        try {
            // try a generic operation on the input context
            getBaseDNsInternal( context );
        }
        catch( NamingException e ) {
            Throwable cause = e.getCause();
            if( cause instanceof java.net.SocketException || 
                cause instanceof java.net.ConnectException ||
                ( cause instanceof java.io.IOException && 
                  cause.getMessage().contains( "connection closed" ))) {
                return false;
            }
            throw new RuntimeException( "Unexpected exception", cause );
        }
        catch( Exception e ) {
            throw new RuntimeException( "Really unexpected exception", e );
        }

        return true;
    }

    /**
     * Perform a generic directory search
     * 
     * @param context
     *            Where the search should take place
     * 
     * @param filter
     *            What to search for; e.g. <em>uid=john</em>
     * 
     * @param scope
     *            One of {@link SearchControls#OBJECT_SCOPE} (results will
     *            contain one or zero element, with no name),
     *            {@link SearchControls#ONELEVEL_SCOPE} or
     *            {@link SearchControls#SUBTREE_SCOPE}
     * 
     * @param attributes
     *            List of the attributes to be returned; e.g. [
     *            <em>"sn", "mail"</em>]. If <code>null</code>, all attributes
     *            will be returned.
     * 
     * @param base
     *            Base of the search; e.g. <em>dc=alma,dc=info</em>
     * 
     * @return A possibly empty enumeration of search results. The elements of
     *         the enumeration depend on the search criteria.
     * 
     * @throws NamingException
     */
    public static NamingEnumeration<SearchResult> search( InitialDirContext context,
                                                          String filter,
                                                          int scope,
                                                          String[] attributes,
                                                          String base )
            throws NamingException {

        SearchControls controls = new SearchControls();
        controls.setSearchScope( scope );
        if( attributes != null ) {
            controls.setReturningAttributes( attributes );
        }
        NamingEnumeration<SearchResult> ret = context.search( base, filter,
                                                              controls );
        return ret;
    }
}
