package alma.obops.ldap;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import alma.userrepository.addressbook.AddressBookSession;
import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.roledirectory.RoleDirectorySession;
import alma.userrepository.shared.DirectorySessionFactory;
import alma.userrepository.shared.LdapDirectorySessionFactory;
import alma.userrepository.shared.ldap.DnDirectory;
/**
 * An interface to LDAP's AddressBook
 *
 * @author amchavan, Jan 28, 2010
 * @version $Revision$
 */

// $Id$

public class AddressBookService {

    public static final String EMAIL = "mail";
    public static final String FIRST_NAME = "givenName";
    public static final String LAST_NAME = "sn";
    
	protected DirectorySessionFactory sessionFactory;

    public AddressBookService() {
        sessionFactory = LdapDirectorySessionFactory.getInstance(null);
	}

    /**
     * @param username
     *            Identifies the user
     * @param attribute
     *            The attribute to look for; should be one of {@link #EMAIL},
     *            {@link #FIRST_NAME}, {@link #LAST_NAME}
     *            
     * @return The value of such attribute for the input user
     * 
     * @throws UserRepositoryException
     */
    public String getAttribute( String username, String attribute ) 
        throws UserRepositoryException {
        String[] tmp = { attribute };
        String email = getAttributes( username, tmp ).get( attribute );
        return email;
    }

    /**
     * Retrieve a set of attributes for the input user
     * 
     * @param username
     *            Identifies the user
     * @param attributes
     *            A list of attributes to look for; should be one of
     *            {@link #EMAIL}, {@link #FIRST_NAME},
     *            {@link #LAST_NAME}
     *            
     * @return A map of [attribute,value] pairs, where attribute is one of the
     *         input attributes
     *         
     * @throws UserRepositoryException
     */
    public Map<String, String> getAttributes( String username,
                                              String[] attributes )
            throws UserRepositoryException {

        final String addressFilter = "(uid=*)";
        final List<String> attrs = Arrays.asList( attributes );
        final String userLocation = DnDirectory.getUserDn( username );
        
        final AddressBookSession dirSess = getAddressBookSession();
        final List<Map<String, String>> map = 
            dirSess.searchLocationGetAttributes( userLocation, 
                                                 addressFilter,
                                                 attrs );
        dirSess.close();

        return map.get( 0 );
    }

    /**
     * @return A new AddressBookSession
     * @throws UserRepositoryException
     */
    public AddressBookSession getAddressBookSession()
            throws UserRepositoryException {
        return getAddressBookSession( "", "" );
    }

    /**
     * @param userId
     * @param password
     * @return A new AddressBookSession
     * @throws UserRepositoryException
     */
    public AddressBookSession getAddressBookSession( String userId,
                                                     String password )
            throws UserRepositoryException {
        return sessionFactory.newAddressBookSession( userId, password );
    }

    /**
     * Return the RoleDirectorySession for an specific userId
     * 
     * @param userId
     * @param password
     * @return
     * @throws UserRepositoryException
     */
    public RoleDirectorySession getRoleDirectorySession( String userId,
	            String password ) throws UserRepositoryException {
    	return sessionFactory.newRoleDirectorySession(userId , password);
    }	

    /**
     * Return the RoleDirectorySession with anonymous access
     * 
     * @param userId
     * @param password
     * @return
     * @throws UserRepositoryException
     */
    public RoleDirectorySession getRoleDirectorySession() throws UserRepositoryException {
    	return sessionFactory.newRoleDirectorySession("" , "");
    }	
}
