/**
 * AlmaLdapConnectionException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

/**
 * @author amchavan, Mar 10, 2011
 * @version $Revision: 1.2 $
 */

// $Id: AlmaLdapConnectionException.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class AlmaLdapConnectionException extends Exception {

    private static final long serialVersionUID = 8717767724739643021L;

    public AlmaLdapConnectionException() {
    }

    /**
     * @param message
     */
    public AlmaLdapConnectionException( String message ) {
        super( message );
    }

    /**
     * @param message
     * @param cause
     */
    public AlmaLdapConnectionException( String message, Throwable cause ) {
        super( message, cause );
    }

    /**
     * @param cause
     */
    public AlmaLdapConnectionException( Throwable cause ) {
        super( cause );
    }
}
