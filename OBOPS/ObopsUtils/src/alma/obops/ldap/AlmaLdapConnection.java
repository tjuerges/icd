/**
 * AlmaLdapConnection.java
 *
 * Copyright European Southern Observatory 2011
 */

package alma.obops.ldap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import alma.obops.utils.config.ArchiveConfig;
import alma.obops.utils.config.ObopsConfig;

/**
 * An Alma-specific utility class for interfacing with LDAP; uses
 * {@link JavaxNamingUtils} for generic, low-level LDAP queries.
 *
 * @author obops, Mar 10, 2011
 * @version $Revision$
 */

// $Id$

public class AlmaLdapConnection {
    
    private enum RoleQueryOption {
        SUBROLES, ACCOUNTS
    }
    
    /**
     * Extracts from each SearchResult the full name, including namespace, and
     * all of its attributes; returns a {@link List} of {@link Property}
     * instances, in which the first item is the full name
     */
    public static final SearchResultProcessor EXTRACT_FULLNAME_ATTRIBUTES = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            Attributes attrs = result.getAttributes();
            List<Property> ret = AlmaLdapConnection.convertToPropertyList( attrs );
            String name = result.getNameInNamespace();
            Property nameProp = new Property( "name", name );
            ret.add( 0, nameProp );
            return ret;
        }
    };
    
    /**
     * Extracts from each SearchResult all of its attributes, returns a
     * {@link List} of {@link Property} instances
     */
    public static final SearchResultProcessor EXTRACT_ATTRIBUTES = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            Attributes attrs = result.getAttributes();
            List<Property> ret = AlmaLdapConnection.convertToPropertyList( attrs );
            return ret;
        }
    };
    
    /**
     * Extracts from each SearchResult the full name, including namespace;
     * returns it as a String
     */
    public static final SearchResultProcessor EXTRACT_NAME_NAMESPACE = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            return result.getNameInNamespace();
        }
    };
    
    /**
     * Extracts from each SearchResult a simple name, not including namespace;
     * returns it as a String
     */
    public static final SearchResultProcessor EXTRACT_NAME = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            return result.getName();
        }
    };
    
    /**
     * Extracts from each SearchResult the full name including namespace and
     * converts to Alma role notation; returns it as a String
     */
    public static final SearchResultProcessor EXTRACT_ROLE_DN_ALMA_NOTATION = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            final String roleDN = result.getNameInNamespace();
            return convertToAlmaNotation( roleDN );
        }
    };
    
    /**
     * Extracts from the SearchResult the list of all members as a List<String>;
     * that is, the resulting set has a single element that is a List of Strings
     */
    public static final SearchResultProcessor EXTRACT_ROLE_MEMBERS = 
        new SearchResultProcessor() {
        @Override
        public Object process( SearchResult result ) {
            
            Attributes attrs = result.getAttributes();
            NamingEnumeration<?> members;
            try {
                List<String> ret = new ArrayList<String>();
                members = attrs.get( "member" ).getAll();
                while( members.hasMore() ) {
                    String dn = members.next().toString();
                    if( dn.startsWith( "uid=" ) ) {
                        if( dn.contains( "nobody" ) ) {
                            continue; // skip uid=nobody
                        }
                        ret.add( convertFromDn( dn ) );
                    }
                }
                return ret;
            }
            catch( NamingException e ) {
                throw new RuntimeException( e );
            }
        }
    };

    
    protected static String accountsOU = null;      // we need this to be static
    
    protected static String rolesOU = null;         // we need this to be static
    
    /**
     * Computes the set of differences between oldAttributes and newAttributes
     * @param newAttrs
     * @param oldAttrs
     */
    // Code originally taken from 
    // alma.userrepository.addressbook.ldap.AttributeUtilities
    static ModificationItem[] computeDifferences( Attributes oldAttrs, 
                                                  Attributes newAttrs ) {
        List<ModificationItem> modifications = new ArrayList<ModificationItem>();
    
        // for each item in new that is defined differently to that in old
        // make a 'change' modification item
        // ---------------------------------------------------------------
        NamingEnumeration<? extends Attribute> newAttrsEnum = newAttrs.getAll();
        while( newAttrsEnum.hasMoreElements() ) {
            Attribute newAttr = newAttrsEnum.nextElement();
            String attrID = newAttr.getID();
            Attribute oldAttr = oldAttrs.get( attrID );
            
            if( newAttr.equals( oldAttr )) {
                continue;
            }
    
            if( oldAttr == null ) {
                modifications.add( new ModificationItem( DirContext.ADD_ATTRIBUTE, newAttr ));
                continue;
            }
            
            modifications.add( new ModificationItem( DirContext.REPLACE_ATTRIBUTE, newAttr ) );
        }
    
        // for each item in old that is not in new
        // make a 'delete' modification item
        // -------------------------------------------
        NamingEnumeration<? extends Attribute> oldAttrsEnum = oldAttrs.getAll();
        while( oldAttrsEnum.hasMoreElements() ) {
            Attribute attr = oldAttrsEnum.nextElement();
            String attrID = attr.getID();
            Attribute newAttr = newAttrs.get( attrID );
            if( newAttr == null ) {
                modifications.add( new ModificationItem(
                        DirContext.REMOVE_ATTRIBUTE, attr ));
            }
        }
    
        return modifications.toArray( new ModificationItem[0] );
    }
    
    /**
     * @param roleDN
     *            A role DN, e.g.
     *            <em>cn=AOD,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</em>
     * @return That role in the ALMA notation; e.g. <em>OBOPS/AOD</em>
     */
    public static String convertToAlmaNotation( String roleDN ) {
        String[] tmp = roleDN.split( "," );
        StringBuilder sb = new StringBuilder();
        if( tmp.length > 1 ) {
            sb.append( tmp[1].substring( 3 )).append( "/" );
        }
        else {
            final String message = "Not a role DN: '" + roleDN + "'";
            throw new IllegalArgumentException( message );
        }
        sb.append( tmp[0].substring( 3 ));
        String roleAlma = sb.toString();
        return roleAlma;
    }
    
    /**
     * @return The input List of {@link Property} instances converted to an
     *         {@link Attributes} instance. All mandatory attributes of the Alma
     *         LDAP schema for accounts will be automatically included,
     *         including <em>objectClass</em>, <em>gidNumber</em>,
     *         <em>homeDirectory</em>, <em>uidNumber</em>, etc. <br/>
     *         Default values will be assigned to those attributes.
     */
    public static Attributes convertToAttributes( List<Property> propList ) {
        Attributes attributes = new BasicAttributes();
        
        // Add all "objectClass" attributes
        Attribute objClasses = new BasicAttribute( "objectClass" );
        for( int i = 0; i < OBJECT_CLASSES.length; i++ ) {
            objClasses.add( OBJECT_CLASSES[i] );
        }
        attributes.put( objClasses );
    
        // Add all mandatory attributes, with default (meaningless) values
        attributes.put( new BasicAttribute( "uidNumber", "0" ));
        attributes.put( new BasicAttribute( "gidNumber", "0" ));
        attributes.put( new BasicAttribute( "homeDirectory", "/home" ));
        
        for( Property property : propList ) {
            
            // ignore null or empty properties
            if( property.value == null || property.value.length() == 0 ) {
                continue;
            }
            
            // If needed, create a new Attribute, otherwise add a value
            // to an existing Attribute
            final Attribute attribute = attributes.get( property.name );
            if( attribute == null ) {
                attributes.put( new BasicAttribute( property.name, 
                                                    property.value ));        
            }
            else {
                attribute.add( property.value );
            }
        }
        return attributes;
    }
    
    /**
     * @param role
     *            A role in the ALMA notation; e.g. <em>OBOPS/AOD</em>
     * @return That role's DN (LDAP notation), e.g.
     *            <em>cn=AOD,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</em> 
     */
    public static String convertToLdapNotation( String role ) {
        String[] tmp = role.split( "/" );
        StringBuilder sb = new StringBuilder();
        if( tmp.length > 1 ) {
            sb.append( "cn=" )
              .append( tmp[1] )
              .append( ",ou=" )
              .append( tmp[0] )
              .append( "," )
              .append( getRolesOU() );
        }
        else {
            String message = "Not a role in the ALMA notation: '" + role + "'";
            throw new 
                IllegalArgumentException( message );
        }
        String roleDN = sb.toString();
        return roleDN;
    }
    
    /**
     * Convert a set of objects into an ordered set of Strings
     */
    public static SortedSet<String> convertToOrderedStringSet( Set<Object> objects ) {
        SortedSet<String> ret = new TreeSet<String>();
        for( Object result : objects ) {
            ret.add( result.toString() );
        }
        return ret;
    }
    /**
     * @return The input Attributes converted to a List of {@link Property}
     *         instances.
     * @throws RuntimeException
     */
    public static List<Property> convertToPropertyList( Attributes attrs )
            throws RuntimeException {
        List<Property> ret = new ArrayList<Property>();
        NamingEnumeration<? extends Attribute> nEnum = attrs.getAll();
        try {
            while( nEnum.hasMore() ) {
                Attribute attribute = nEnum.next();
                String name = attribute.getID();
                NamingEnumeration<?> values = attribute.getAll();
                while( values.hasMore() ) {
                    String value = values.next().toString();
                    ret.add( new Property( name, value ) );
                }
            }
        }
        catch( NamingException e ) {
            // should never happen
            throw new RuntimeException( e );
        }
        return ret;
    }
    
    /**
     * @return The OU for ALMA portal accounts
     * @throws RuntimeException
     *             if no constructor of this class was invoked before calling
     *             this method -- the OU for ALMA roles is extracted from
     *             configuration files by the constructors
     */
    public static String getAccountsOU() {
        if( accountsOU == null ) {
            throw new RuntimeException( "Accounts OU was not initialized, " +
                                        "please invoke a constructor before " +
                                        "calling this method" );
        }
        return accountsOU;
    }

    /**
     * @return The OU for ALMA roles
     * @throws RuntimeException
     *             if no constructor of this class was invoked before calling
     *             this method -- the OU for ALMA roles is extracted from
     *             configuration files by the constructors
     */
    public static String getRolesOU() {
        if( rolesOU == null ) {
            throw new RuntimeException( "Roles OU was not initialized, " +
            		                    "please invoke a constructor before " +
            		                    "calling this method" );
        }
        return rolesOU;
    }

    protected List<List<String>> rolesTree = null;
    protected String providerUrl;
    protected InitialDirContext context;
    protected String principal;
    protected String password;

    /** When we create an LDAP entry, it will have these object classes */
    public static final String[] OBJECT_CLASSES = {
        "top",
        "shadowAccount",
        "posixAccount",
        "person",
        "organizationalPerson",
        "outlookAttributes",
        "inetOrgPerson",
        "almaUser",
        "almaPreferences",
        "almaAddress",
    };

    /**
     * @param dn
     *            An account DN, e.g.
     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     * @return The corresponding simple UID, e.g. <em>obops</em>
     * 
     */
    public static String convertFromDn( String dn ) {
        int start = 4; // "uid="
        int end = dn.indexOf( "," );
        if( end <= start ) {
            throw new IllegalArgumentException( "Invalid DN: '" + dn + "'" );
        }
        return dn.substring( start, end );
    }

    /**
     * @param uid  A simple UID, e.g. <em>obops</em>
     * @return The corresponding DN for our LDAP server, e.g.
     *         <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     */
    public static String convertToAccountDn( String uid ) {
        StringBuilder sb = new StringBuilder( "uid=" );
        return sb.append( uid ).append( "," ).append( getAccountsOU() ).toString();
    }

    /**
     * @param uid  A role in the ALMA notation, e.g. <em>OBOPS/QAA</em>
     * @return The corresponding DN for our LDAP server, e.g.
     *         <em>cn=QAA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</em>
     */
    public static String convertToRoleDn( String role ) {
        String[] tmp = role.split( "/" );
        StringBuilder sb = new StringBuilder();
        return sb.append( "cn=" ).append( tmp[1] ).append( "," )
                 .append( "ou=" ).append( tmp[0] ).append( "," )
                 .append( getRolesOU() ).toString();
    }

    /**
     * Zero-arg constructor -- takes all connection parameters from
     * configuration files <em>archiveConfig.properties</em> and
     * <em>obopsConfig.properties</em>.
     * 
     * @throws AlmaLdapConnectionException  if anything goes wrong
     */
    public AlmaLdapConnection() throws AlmaLdapConnectionException {
        this( ArchiveConfig.getUserRepositoryProviderUrl(), 
              ObopsConfig.getLdapPrincipalDN(), 
              ObopsConfig.getLdapPrincipalPassword(), 
              ObopsConfig.getLdapAccountsOU(), 
              ObopsConfig.getLdapRolesOU() );
    }

    /**
     * Constructor -- for testing only. Production code should use the zero-arg
     * constructor instead.
     * 
     * @param providerURL
     *            URL of the LDAP server, e.g.
     *            <em>ldap://ngas01.hq.eso.org:389</em>
     * 
     * @param principal
     *            Distinguished Name of the LDAP account to use; must have
     *            read/write access. E.g.
     *            <em>uid=root,ou=master,dc=alma,dc=info</em>
     * 
     * @param password
     *            Password of the LDAP account to use; e.g. <em>boo!</em>
     * 
     * @param accountsOU
     *            Organizational Unit to which accounts belong; e.g.
     *            <em>ou=people,ou=master,dc=alma,dc=info</em>
     * 
     * @param rolesOU
     *            Organizational Unit to which roles belong; e.g.
     *            <em>ou=roles,ou=master,dc=alma,dc=info</em>
     * 
     * @throws AlmaLdapConnectionException
     */
    public AlmaLdapConnection( String providerUrl, String principal, String password,
                        String accountsOU, String rolesOU )
        throws AlmaLdapConnectionException {
        
        try {
            this.context     = JavaxNamingUtils.getLdapContext( providerUrl, 
                                                                principal, 
                                                                password );
            this.providerUrl = providerUrl;
            this.principal   = principal;
            this.password    = password;
            
            // we need these to be static
            AlmaLdapConnection.accountsOU  = accountsOU;
            AlmaLdapConnection.rolesOU = rolesOU;
        }
        catch( NamingException e ) {
            throw new AlmaLdapConnectionException( e );
        }
    }

    /**
     * Depending on the value of param <em>operation</em>, add or remove a
     * member to/from a group.
     * 
     * @param memberDN
     *            DN of a member, e.g.
     *            <em>cn=AOD,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</em>
     * @param groupDN
     *            DN of a group, e.g.
     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     * @param operation
     *            One of {@link DirContext#ADD_ATTRIBUTE} or
     *            {@link DirContext#REMOVE_ATTRIBUTE}
     *            
     * @throws AlmaLdapConnectionException
     */
    void addOrRemoveMemberAttribute( String groupDN, String memberDN, int operation )
            throws AlmaLdapConnectionException {
        try {
            
            if( operation != DirContext.ADD_ATTRIBUTE && 
                operation != DirContext.REMOVE_ATTRIBUTE ) {
                final String msg = "Invalid operation: " + operation;
                throw new IllegalArgumentException( msg );
            }
            
            Attribute attr = new BasicAttribute( "member", memberDN );
            ModificationItem item = 
                new ModificationItem( operation, attr );
            ModificationItem[] modItems = { item };
            context.modifyAttributes( groupDN, modItems );
        }
        catch( NamingException e ) {
            throw new AlmaLdapConnectionException( e );
        }
    }

    /**
     * Add a role to a Portal account. If this account already had the role, the
     * call leaves the account unchanged and returns <code>false</code>.
     * 
     * @param role
     *            A role name in the ALMA notation, e.g. <em>MASTER/USER</em>
     * @param uid
     *            A simple account ID, e.g. <em>obops</em>
     * 
     * @return <code>true</code> if the role was added, <code>false</code>
     *         otherwise
     * @throws AlmaLdapConnectionException
     */
    public boolean addRoleToAccount( String role, String uid ) 
        throws AlmaLdapConnectionException {

        String roleDN = convertToLdapNotation( role );
        String uidDN = convertToAccountDn( uid );
        if( hasRoleInternal( roleDN, uidDN )) {
            // if account already has that role, do nothing, return false
            return false;
        }
        
        addOrRemoveMemberAttribute( roleDN, uidDN, DirContext.ADD_ATTRIBUTE );
        return true;
    }

    /**
     * Close this connection
     */
    public void close() {
        try {
            this.context.close();
        }
        catch( NamingException e ) {
            // ignore, should never happen
        }
    }

    /**
     * Creates an account ("subcontext") in LDAP.
     * <p/>
     * 
     * Note that this method makes no checks of the list of properties that are
     * passed in, which means that <em>invalid entries can be created</em>. To
     * ensure correct creation of entries, use the methods in the PortalAccount
     * DAO instead.
     * 
     * @param uid
     *            Simple UID of the account, like <em>obops</em>
     * @param properties
     *            A set of properties describing the account. By default this
     *            method will also include all mandatory attributes of the Alma
     *            LDAP schema for accounts, including <em>objectClass</em>,
     *            <em>gidNumber</em>, <em>homeDirectory</em>, 
     *            <em>uidNumber</em>, etc. <br/>
     *            Default values will be assigned to those attributes.
     * 
     * @throws NamingException
     */
    public void createEntry( String uid, List<Property> properties ) 
        throws NamingException {
        Attributes attrs = convertToAttributes( properties );
        context.createSubcontext( convertToAccountDn( uid ), attrs );
    }

    /**
     * Deletes an account ("subcontext") in LDAP
     * @param uid           Simple UID of the account, like <em>obops</em>
     * 
     * @throws NamingException 
     */
    public void deleteEntry( String uid ) throws NamingException {
        try {
            context.destroySubcontext( convertToAccountDn( uid ));
        }
        catch( javax.naming.InvalidNameException e ) {
            // entry not found, ignore this
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        context.close();
    }

//    /**
//     * Finds all roles in the ALMA structure that are derived; that is, all
//     * roles that include other roles. For instance, the <em>OBOPS/DSOA</em>
//     * role includes <em>OBOPS/ARCA</em>; so, accounts having the
//     * <em>OBOPS/ARCA</em> role will also have the <em>OBOPS/DSOA</em> role.
//     * 
//     * @return An ordered list of roles in the ALMA notation
//     * 
//     * @throws AlmaLdapConnectionException
//     */
//    public SortedSet<String> findAllDerivedRoles() 
//        throws AlmaLdapConnectionException {
//
//        List<List<String>> roleDescriptors = getRolesTree();
//        
//        SortedSet<String> derived = new TreeSet<String>();
//        for( List<String> roleDescriptor : roleDescriptors ) {
//            if( roleDescriptor.size() > 1 ) {
//                derived.add( roleDescriptor.get( 0 ));
//            }
//        }
//        return derived;
//    }

//    /**
//     * Recursively finds all the roles of which the input DN is a direct or
//     * indirect member
//     * 
//     * @param dn
//     *            DN of an entry
//     * 
//     * @return An ordered set of role DNs
//     * 
//     * @throws AlmaLdapConnectionException
//     */
//    SortedSet<String> findAllRoleDNsFor( String dn ) 
//        throws AlmaLdapConnectionException {
//        SortedSet<String> ret = new TreeSet<String>();
//        SortedSet<String> directRoles = findDirectRoleDNsFor( dn );
//        ret.addAll( directRoles );
//        for( String directRole : directRoles ) {
//            SortedSet<String> tmp = findDirectRoleDNsFor( directRole );
//            ret.addAll( tmp );
//        }
//        return ret;
//    }

    /**
     * Finds all roles
     * 
     * @return The ordered list of all roles, in the ALMA notation
     *            
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> findAllRoles() 
        throws AlmaLdapConnectionException {
        
        List<List<String>> roleDescriptors = getRolesTree();
        
        SortedSet<String> derived = new TreeSet<String>();
        for( List<String> roleDescriptor : roleDescriptors ) {
            derived.add( roleDescriptor.get( 0 ));
        }
        return derived;
    }

    /**
     * Finds all the roles of which the input DN is a direct member
     * 
     * @param dn
     *            DN of an account entry, e.g.
     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     * 
     * @return An ordered set of role DNs
     * 
     * @throws AlmaLdapConnectionException
     */
    SortedSet<String> findDirectRoleDNsFor( String dn ) 
        throws AlmaLdapConnectionException {
        
        final String filter = "(member=" + dn + ")";
        SortedSet<String> ret = searchNamesNamespaces( filter,
                                                       SearchControls.SUBTREE_SCOPE,
                                                       null, getRolesOU() );
        return ret;
    }

    /**
     * Finds all the roles of which the input DN is a direct member
     * 
     * @param dn
     *            DN of an account entry, e.g.
     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     * 
     * @return An ordered set of roles in the ALMA notation
     * 
     * @throws AlmaLdapConnectionException
     */
    SortedSet<String> findDirectRolesFor( String dn ) 
        throws AlmaLdapConnectionException {
        
        SortedSet<String> ret = new TreeSet<String>();
        SortedSet<String> roleDNs = findDirectRoleDNsFor( dn );
        for( String roleDN : roleDNs ) {
            ret.add( convertToAlmaNotation( roleDN ));
        }
        return ret;
    }

//    /**
//     * Finds all the roles of which the input DN is a direct or
//     * indirect member
//     * 
//     * @param dn
//     *            DN of an account entry, e.g.
//     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
//     * 
//     * @return A non-<code>null</code> ordered set of roles in the ALMA
//     *         notation, e.g. <em>MASTER/USER</em>
//     * 
//     * @throws AlmaLdapConnectionException
//     */
//    public SortedSet<String> findRolesForDN( String dn ) 
//        throws AlmaLdapConnectionException {
//        SortedSet<String> roleDNs = findAllRoleDNsFor( dn );
//        SortedSet<String> roles = new TreeSet<String>();
//        for( String roleDN : roleDNs ) {
//            roles.add( convertToAlmaNotation( roleDN ));      
//        }
//        return roles;
//    }

    /**
     * @param role
     *            A role in ALMA notation, e.g. <em>OBOPS/QAA</em>
     * 
     * @return A sorted set of simple user IDs, e.g. <em>jack john peter</em>,
     *         corresponding to all users having that role or any of its
     *         sub-roles, sub-sub-roles, etc.
     *         
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> findIDsByRole( String role ) 
        throws AlmaLdapConnectionException {
        
        SortedSet<String> allRoles = findSubRoles( role );
        allRoles.add( role );
        
        SortedSet<String> ret = new TreeSet<String>();
        String[] attributes = { "member" };
        for( String aRole : allRoles ) {
            // turn QAA/OBOPS into cn=QAA,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info
            String searchBase = convertToRoleDn( aRole );
            Set<Object> results = 
                search( "(member=*)", SearchControls.SUBTREE_SCOPE, 
                        attributes, searchBase, EXTRACT_ROLE_MEMBERS );
            for( Object result : results ) {
                @SuppressWarnings("unchecked")
                List<String> members = (List<String>) result;
                ret.addAll( members );
            }
        }
        return ret;
    }
    
    /**
     * Finds all the roles of which the input DN is direct or indirectly a
     * member
     * 
     * @param uid
     *            UID of an entry, e.g. <em>obops</em>
     * 
     * @return A non-<code>null</code> ordered set of roles in the ALMA
     *         notation, e.g. <em>MASTER/USER</em>
     * 
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> findRolesForUID( String uid ) 
        throws AlmaLdapConnectionException {
        SortedSet<String> ret = new TreeSet<String>();
        SortedSet<String> directRoles = findDirectRolesFor( convertToAccountDn( uid ));
        for( String directRole : directRoles ) {
            ret.addAll( findSuperRoles( directRole ));
        }
        ret.addAll( directRoles );
        return ret;
    }
    
    /**
     * Finds all sub-roles of the input ALMA role; that is, all roles
     * including that role. For instance, the <em>OBOPS/DSOA</em> role includes
     * <em>OBOPS/ARCA</em>; calling this method with <em>OBOPS/DSOA</em> will
     * return a set including <em>OBOPS/ARCA</em>.<br/>
     * The search proceeds recursively down the roles tree; the returned
     * result set includes all sub-sub-roles, sub-sub-sub-roles, etc.
     * 
     * @param role
     *            A role in the ALMA notation
     *            
     * @return An ordered list of super-roles, in the ALMA notation
     * 
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> findSubRoles( String role ) 
        throws AlmaLdapConnectionException {

        List<List<String>> roleDescriptors = getRolesTree();
        
        // Look first for the direct sub-roles
        SortedSet<String> directSubRoles = new TreeSet<String>();
        for( List<String> roleDescriptor : roleDescriptors ) {
            String subRole = roleDescriptor.get( 0 );
            if( subRole.equals( role ) ) {
                directSubRoles.addAll( roleDescriptor );
                directSubRoles.remove( subRole );
            }
        }
        
        // Now recurse up and find any sub-sub-roles
        SortedSet<String> ret = new TreeSet<String>();
        ret.addAll( directSubRoles );
        for( String directSubRole : directSubRoles ) {
            ret.addAll( findSubRoles( directSubRole ));
        }

        return ret;
    }

    /**
     * Finds all super-roles of the input ALMA role; that is, all roles
     * including that role. For instance, the <em>OBOPS/DSOA</em> role includes
     * <em>OBOPS/ARCA</em>; calling this method with <em>OBOPS/ARCA</em> will
     * return a set including <em>OBOPS/DSOA</em>.<br/>
     * The search proceeds recursively up the roles tree; the returned
     * result set includes all super-super-roles, super-super-super-roles, etc.
     * 
     * @param role
     *            A role in the ALMA notation
     *            
     * @return An ordered list of super-roles, in the ALMA notation
     * 
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> findSuperRoles( String role ) 
        throws AlmaLdapConnectionException {

        List<List<String>> roleDescriptors = getRolesTree();
        
        // Look first for the direct super-roles
        SortedSet<String> directSuperRoles = new TreeSet<String>();
        for( List<String> roleDescriptor : roleDescriptors ) {
            String superRole = roleDescriptor.get( 0 );
            if( superRole.equals( role ) ) {
                continue;
            }
            if( roleDescriptor.contains( role ) ) {
                directSuperRoles.add( superRole );
            }
        }
        
        // Now recurse up and find any super-super-roles
        SortedSet<String> ret = new TreeSet<String>();
        ret.addAll( directSuperRoles );
        for( String directSuperRole : directSuperRoles ) {
            ret.addAll( findSuperRoles( directSuperRole ));
        }

        return ret;
    }

    /**
     * Finds all roles and their members in the ALMA structure.
     * 
     * @return A list of lists of Strings; the main list has one member list per
     *         Alma LDAP role. Each sub-list's first element is the role name;
     *         all following elements are that role's members, if any. <br/>
     *         All role names are given in the ALMA notation; e.g.
     * 
     *         <pre>
     *      MASTER/USER jack john mary
     *      MASTER/AUDITOR galileo tyco ann
     *      MASTER/EDITOR luise
     *      ...
     *      OBOPS/AOD obops
     *      OBOPS/APRC marylin constance berliner miller
     *      ...
     *      OBOPS/DSOA 
     *      ...
     * </pre>
     * 
     * @throws AlmaLdapConnectionException
     */
    public List<List<String>> getRoleMembers() 
        throws AlmaLdapConnectionException {
        
        return queryAllRoleInfo( RoleQueryOption.ACCOUNTS );
    }
    
    /**
     * Finds all roles and sub-roles in the ALMA structure.
     * 
     * @return A list of lists of Strings; the main list has one member list per
     *         Alma LDAP role. Each sub-list's first element is the role name;
     *         all following elements are that role's sub-roles, if any. <br/>
     *         All role names are given in the ALMA notation; e.g.
     * 
     *         <pre>
     *      MASTER/USER MASTER/MEMBER
     *      MASTER/AUDITOR MASTER/OPERATOR
     *      MASTER/EDITOR MASTER/OPERATOR
     *      ...
     *      OBOPS/AOD
     *      OBOPS/APRC
     *      ...
     *      OBOPS/DSOA OBOPS/AOD OBOPS/ARCA OBOPS/QAA
     *      ...
     * </pre>
     * 
     * @throws AlmaLdapConnectionException
     */
    public List<List<String>> getRolesTree() 
        throws AlmaLdapConnectionException {
        
        // if we have cached results return those -- we assume that
        // the roles structure will not change at runtime
        if( rolesTree != null ) {
            return rolesTree;
        }
        
        List<List<String>> ret = queryAllRoleInfo( RoleQueryOption.SUBROLES );
        
        rolesTree = ret;  // cache this for next time
        return ret;
    }

    /**
     * Check if a Portal account has a role.
     * 
     * @param role
     *            A role name in the ALMA notation, e.g. <em>MASTER/USER</em>
     * @param uid
     *            A simple account ID, e.g. <em>obops</em>
     * 
     * @return <code>true</code> if the account has that role,
     *         <code>false</code> otherwise
     * @throws AlmaLdapConnectionException 
     */
    public boolean hasRole( String role, String uid ) 
        throws AlmaLdapConnectionException {
        
        String roleDN = convertToLdapNotation( role );
        String uidDN = convertToAccountDn( uid );
        return hasRoleInternal( roleDN, uidDN );
    }

    /**
     * @param roleDN
     *            A role DN, e.g.
     *            <em>cn=AOD,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info</em>
     * @param uidDN
     *            DN of an account entry, e.g.
     *            <em>uid=obops,ou=people,ou=master,dc=alma,dc=info</em>
     *            
     * @return <code>true</code> if the account has that role,
     *         <code>false</code> otherwise
     *         
     * @throws AlmaLdapConnectionException
     */
    boolean hasRoleInternal( String roleDN, String uidDN )
            throws AlmaLdapConnectionException {
        String filter = "(member=" + uidDN + ")";
        SortedSet<String> results = searchNames( filter, 
                                                 SearchControls.SUBTREE_SCOPE, 
                                                 null,
                                                 roleDN );
        if( results.size() == 0 ) {
            return false;
        }
        
        return true;
    }

    /**
     * Perform an LDAP query looking for group members down the roles subtree.
     * 
     * @param option
     *            Depending on whether you
     *            are interested in sub-roles or accounts, respectively.
     *            
     * @return A list of lists of Strings; the main list has one member list per
     *         Alma LDAP role. Each sub-list's first element is the role name;
     *         all following elements are either that role's sub-roles
     *         or that roles's accounts, depending on the value of memberType.
     * 
     * @see #getRolesOU()
     * 
     * @throws AlmaLdapConnectionException
     */
    public List<List<String>> queryAllRoleInfo( RoleQueryOption option )
            throws AlmaLdapConnectionException {
        // the following search will return a set of lists of properties
        String[] attributes = { "member" };
        Set<Object> allRoles = search( "(objectClass=*)", 
                                       SearchControls.SUBTREE_SCOPE, 
                                       attributes,
                                       getRolesOU(), 
                                       EXTRACT_FULLNAME_ATTRIBUTES );
        
        List<List<String>> ret = new ArrayList<List<String>>();
        // for each element of the set...
        for( Object role : allRoles ) {
            
            // each element is a list
            @SuppressWarnings("unchecked")
            List<Property> props = (List<Property>) role;
            
            // the first element of the list is a group DN
            String roleDN = props.get(0).value;
            
            // skip anything that does not look like a role DN, that is,
            // something like:
            //    cn=AOD,ou=OBOPS,ou=roles,ou=master,dc=alma,dc=info
            if( !roleDN.startsWith( "cn=" )) {
                continue;
            }
            String almaNotation = convertToAlmaNotation( roleDN );
            if( almaNotation.equals( "roles/USER" )) {
                continue;   // remove artifact of our LDAP setup 
            }
            
            List<String> roleDesc = new ArrayList<String>();
            roleDesc.add( almaNotation);
            props.remove(0);
            
            // add to the role descriptor all of its sub-roles
            for( Property member : props ) {
                if( option == RoleQueryOption.SUBROLES ) {
                    if( member.value.startsWith( "cn=" )) {
                        roleDesc.add( convertToAlmaNotation( member.value ));
                    }
                }
                else if( option == RoleQueryOption.ACCOUNTS )
                    if( member.value.startsWith( "uid=" ) ) {

                        try {
                            String uid = convertFromDn( member.value );
                            roleDesc.add( uid );
                        }
                        catch( IllegalArgumentException e ) {
                            // ignore malformed DNs
                            continue;
                        }
                    }
                }
        
            ret.add( roleDesc );
        }
        return ret;
    }

    /**
     * Read an account's information from LDAP
     * 
     * @param id
     *            Simple ID of the account, like <em>obops</em>
     * 
     * @return An unordered List of Property instances, each defining a property
     *         of the account; e.g. <em>almaPreference2=eu, almaPreference1=eu, 
     *         givenName=Albert, sn=Einstein, ou=40520, ...</em>; if no account
     *         with the given ID is found, <code>null</code> is returned
     * 
     * @throws AlmaLdapConnectionException
     */
    @SuppressWarnings("unchecked")
    public List<Property> readEntry( String id ) 
        throws AlmaLdapConnectionException {
        
        String filter = "(uid=" + id + ")";
        Set<Object> results = search( filter, SearchControls.ONELEVEL_SCOPE, 
                                      null,
                                      getAccountsOU(), 
                                      AlmaLdapConnection.EXTRACT_ATTRIBUTES );
        if( results.size() == 0 ) {
            return null;
        }
        
        // get hold of the first result
        List<Property> properties = null;
        for( Object object : results ) {
            properties = (List<Property>) object;
            break;
        }
        return properties;
    }
    
    /**
     * Check if our context is active; if not, create a new one
     * @throws NamingException 
     */
    // this is not private to allow unit testing
    void refresh() throws NamingException {
        if( ! JavaxNamingUtils.isActive( context )) {
            context = JavaxNamingUtils.clone( context );
        }
    }
    
    /**
     * Remove a role from a Portal account. If the account did not have the
     * role, the call leaves the account unchanged and returns
     * <code>false</code>.
     * 
     * @param role
     *            A role name in the ALMA notation, e.g. <em>MASTER/USER</em>
     * @param uid
     *            A simple account ID, e.g. <em>obops</em>
     * 
     * @return <code>true</code> if the role was removed, <code>false</code>
     *         otherwise
     * @throws AlmaLdapConnectionException 
     */
    public boolean removeRoleFromAccount( String role, String uid ) 
        throws AlmaLdapConnectionException {
        
        String roleDN = convertToLdapNotation( role );
        String uidDN = convertToAccountDn( uid );
        if( ! hasRoleInternal( roleDN, uidDN )) {
            // if account does not have that role, do nothing, return false
            return false;
        }
        
        addOrRemoveMemberAttribute( roleDN, uidDN, DirContext.REMOVE_ATTRIBUTE );
        return true;
    }

    /**
     * Perform a generic search, process the results, return a list of processed
     * results. <br/>
     * Validity of the connection is ensured by calling {@link #refresh()}
     * before performing the search.
     * 
     * @param filter
     *            What to search for; e.g. <em>uid=john</em>; should not be
     *            <code>null</code>
     * 
     * @param scope
     *            One of {@link SearchControls#OBJECT_SCOPE} (results will
     *            contain one or zero element, with no name),
     *            {@link SearchControls#ONELEVEL_SCOPE} or
     *            {@link SearchControls#SUBTREE_SCOPE}
     * 
     * @param attributes
     *            List of the attributes to be returned, e.g. [ <em>"sn", 
     *            "mail"</em>]; can be <code>null</code>, in which
     *            case all attributes are returned
     * 
     * @param base
     *            Base of the search; e.g. <em>dc=alma,dc=info</em>; should not
     *            be <code>null</code>
     * 
     * @param postProcessor
     *            Every SearchResult returned by the LDAP search will be
     *            processed by this object before being added to the returned
     *            result set; should not be <code>null</code>
     * 
     * @return A possibly empty, unordered set of all items we find, each having
     *         been post-processed.
     * 
     * @throws AlmaLdapConnectionException
     */
    public Set<Object> search( String filter, int scope, String[] attributes,
                               String base, SearchResultProcessor postProcessor )
            throws AlmaLdapConnectionException {
        
        if( filter == null || base == null || postProcessor == null ) {
            throw new RuntimeException( "null arg" );
        }
                    
        Set<Object> ret = new HashSet<Object>();
        try {
            refresh();
            NamingEnumeration<SearchResult> results = 
                JavaxNamingUtils.search( this.context, filter, scope, 
                                         attributes, base );
            while( results.hasMore() ) {
                SearchResult result = (SearchResult) results.next();
                Object processedResult = postProcessor.process( result );
                ret.add( processedResult ); 
            }
        }
        catch( NamingException e ) {
            throw new AlmaLdapConnectionException( e );
        }

        return ret;
    }
    
    /**
     * Perform a generic search, return entry names.
     * 
     * @param filter
     *            What to search for; e.g. <em>ou=example</em>
     * 
     * @param scope
     *            One of {@link SearchControls#OBJECT_SCOPE} (results will
     *            contain one or zero element, with no name),
     *            {@link SearchControls#ONELEVEL_SCOPE} or
     *            {@link SearchControls#SUBTREE_SCOPE}
     * 
     * @param attributes
     *            List of the attributes to be returned; e.g. [
     *            <em>"sn", "mail"</em>]
     * 
     * @param base
     *            Base of the search; e.g. <em>dc=alma,dc=info</em>
     * 
     * @return A possibly empty, ordered set of then names of every item we
     *         find, without namespace information. That is, <em>uid=jack</em>
     *         as opposed to <em>uid=jack,ou=example,ou=com</em>.
     *         
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> searchNames( String filter, 
                                          int scope,
                                          String[] attributes, 
                                          String base )
            throws AlmaLdapConnectionException {

        
        Set<Object> results = 
            search( filter, scope, attributes, base, EXTRACT_NAME );
        SortedSet<String> ret = convertToOrderedStringSet( results );
        return ret;
    }

    /**
     * Perform a generic search, return entry names.
     * 
     * @param filter
     *            What to search for; e.g. <em>ou=example</em>
     * 
     * @param scope
     *            One of {@link SearchControls#OBJECT_SCOPE} (results will
     *            contain one or zero element, with no name),
     *            {@link SearchControls#ONELEVEL_SCOPE} or
     *            {@link SearchControls#SUBTREE_SCOPE}
     * 
     * @param attributes
     *            List of the attributes to be returned; e.g. [
     *            <em>"sn", "mail"</em>]
     * 
     * @param base
     *            Base of the search; e.g. <em>dc=alma,dc=info</em>
     * 
     * @return A possibly empty, ordered set of then names of every item we
     *         find, including namespace information; e.g. 
     *         <em>uid=jack,ou=example,ou=com</em>.
     *         
     * @throws AlmaLdapConnectionException
     */
    public SortedSet<String> searchNamesNamespaces( String filter, 
                                          int scope,
                                          String[] attributes, 
                                          String base )
            throws AlmaLdapConnectionException {
        
        Set<Object> results = 
            search( filter, scope, attributes, base, EXTRACT_NAME_NAMESPACE );
        
        SortedSet<String> ret = convertToOrderedStringSet( results );
        return ret;
    }
    
    /**
     * Updates an account ("subcontext") in LDAP; only attributes that have
     * actually changed will be updated.
     * 
     * @param uid
     *            Simple UID of the account, like <em>obops</em>
     * @param oldProps
     *            The previous set of properties describing the account
     * @param newProps
     *            The new set of properties describing the account
     * 
     * @throws NamingException
     */
    public void updateEntry( String uid, 
                             List<Property> oldProps,
                             List<Property> newProps ) throws NamingException {
        
        Attributes oldAttrs = convertToAttributes( oldProps );
        Attributes newAttrs = convertToAttributes( newProps );
        ModificationItem[] diffs = computeDifferences( oldAttrs, newAttrs );
        context.modifyAttributes( convertToAccountDn( uid ), diffs );
    }
}
