/**
 * StringProcessor.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import javax.naming.directory.SearchResult;

/**
 * A simple interface to allow LDAP search results to be post-processed before
 * they are returned to the caller.
 *
 * @author amchavan, Mar 10, 2011
 * @version $Revision: 1.2 $
 */

// $Id: SearchResultProcessor.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public interface SearchResultProcessor {
    
    /**
     * @return The input SearchResult, processed in some way.
     */
    public Object process( SearchResult result );
}
