/**
 * ObOpsLdapSessionFactory.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.ldap;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Properties;

import alma.userrepository.shared.LdapDirectorySessionFactory;
import alma.userrepository.shared.ldap.ConnectionProperties;

/**
 * A version of LdapDirectorySessionFactory that's configurable with an
 * arbitrary property set.
 * <p/>
 * This class implements the singleton pattern.
 *
 * @author amchavan, Jan 28, 2010
 * @version $Revision$
 */

// $Id$

/*
 * NOTE: It will no longer be necessary as those properties can be set
 * in the LDAP Interface so it is not necessary to customize at this level
 */
@Deprecated()
public class LdapSessionFactory extends LdapDirectorySessionFactory {

    public static final String INITIAL_FACTORY_KEY = "java.naming.factory.initial";
    public static final String STATE_FACTORY_KEY = "java.naming.factory.state";
    public static final String OBJECT_FACTORY_KEY = "java.naming.factory.object";
    public static final String AUTHENTICATION_KEY = "java.naming.security.authentication";
    public static final String PROVIDER_URL_KEY = "java.naming.provider.url";
    public static final String READ_ONLY_USER_DN_KEY = "alma.userrepository.ldap.readonly.user.dn";
    public static final String READ_ONLY_USER_PASSWORD_KEY = "alma.userrepository.ldap.readonly.user.password";

    /** Holds the singleton instance */
    protected static LdapSessionFactory instance;

    /** Private constructor */
    private LdapSessionFactory() {
        // no-op
    	super(null);
    }

    /** @return The singleton instance of this class */
    public static LdapSessionFactory getInstance() {
        if (instance == null) {
            synchronized( LdapSessionFactory.class ) {
                instance = new LdapSessionFactory();
            }
        }
        return instance;
    }

    /**
     * Configure the LDAP session factory.
     * <p/>
     * This method must be called before the session factory is created with the
     * {@link #getInstance()} method, otherwise it will have no effect.
     *
     * @param file
     *            Reads a property list (key and element pairs) from the input
     *            file in a simple line-oriented format. For more info, see
     *            {@link #configure(Properties)}
     *
     * @throws IOException
     */
    public static void configure( File file ) throws IOException {
        Reader reader = new FileReader( file );
        configure( reader );
    }

    /**
     * Configure the LDAP session factory.
     * <p/>
     * This method must be called before the session factory is created with the
     * {@link #getInstance()} method, otherwise it will have no effect.
     *
     * @param reader
     *            Reads a property list (key and element pairs) from the input
     *            character stream in a simple line-oriented format. For more
     *            info, see {@link #configure(Properties)}
     *
     * @throws IOException
     */
    public static void configure( Reader reader ) throws IOException {
        Properties props = new Properties();
        props.load( reader );
        configure( props );
    }

    /**
     * Configure the LDAP session factory.
     * <p/>
     * This method must be called before the session factory is created with the
     * {@link #getInstance()} method, otherwise it will have no effect.
     *
     * @param stream
     *            Reads a property list (key and element pairs) from the input
     *            byte stream. For more info, see {@link #configure(Properties)}
     *
     * @throws IOException
     */
    public static void configure( InputStream stream ) throws IOException {
        Properties props = new Properties();
        props.load( stream );
        configure( props );
    }

    /**
     * Configure the LDAP session factory.
     * <p/>
     * This method must be called before the session factory is created
     * with the {@link #getInstance()} method, otherwise it will have no effect.
     *
     * @param properties
     *            The input priorities may include:
     *            <ul>
     *            <li>java.naming.factory.initial (default value is
     *                <em>com.sun.jndi.ldap.LdapCtxFactory</em>;
     *                should not be changed)
     *            <li>java.naming.factory.state (default value is
     *                <em>alma.userrepository.addressbook.ldap.UserRepositoryStateFactory:com.sun.jndi.ldap.obj.LdapGroupFactory</em>;
     *                should not be changed)
     *            <li>java.naming.factory.object (default value is
     *                <em>alma.userrepository.addressbook.ldap.UserRepositoryObjectFactory:com.sun.jndi.ldap.obj.LdapGroupFactory</em>;
     *                should not be changed)
     *            <li>java.naming.security.authentication (default value is
     *                <em>simple</em>;
     *                should not be changed)
     *            <li>java.naming.provider.url (default value is
     *                <em>ldap://127.0.0.1:1389/</em>)
     *            <li>alma.userrepository.ldap.readonly.user.dn
     *                (default value is
     *                <em>uid=readUser,ou=master,dc=alma,dc=info</em>)
     *            <li>alma.userrepository.ldap.readonly.user.password
     *                (default value is appropriate for the default user)
     *            </ul>
     *            If any of those priorities is included, they will override the
     *            default value.
     *
     */
    public static void configure( Properties properties ) {

        if( connectionProperties == null ) {
            synchronized( LdapSessionFactory.class ) {
                connectionProperties = ConnectionProperties.getInstance();
            }
        }
        Enumeration<Object> keys = properties.keys();
        while( keys.hasMoreElements() ) {

            String key = keys.nextElement().toString();
            /*String val = */properties.getProperty( key );

            /*
            if( key.equals( AUTHENTICATION_KEY )) {
                connectionProperties.setAuthentication( val );
            }
            else if( key.equals( INITIAL_FACTORY_KEY )) {
                connectionProperties.setInitialFactory( val );
            }
            else if( key.equals( STATE_FACTORY_KEY )) {
                connectionProperties.setStateFactory( val );
            }
            else if( key.equals( OBJECT_FACTORY_KEY )) {
                connectionProperties.setObjectFactory( val );
            }
            else if( key.equals( PROVIDER_URL_KEY )) {
                connectionProperties.setProviderUrl( val );
            }
            else if( key.equals( READ_ONLY_USER_DN_KEY )) {
                connectionProperties.setReadOnlyUserName( val );
            }
            else if( key.equals( READ_ONLY_USER_PASSWORD_KEY )) {
                connectionProperties.setReadOnlyUserPassword( val );
            }
            else {
                // just ignore this property
            }
            */
        }
    }

    /**
     * The connection properties used by this session factory
     * @return
     */
    public static ConnectionProperties getConnectionProperties() {
        return connectionProperties;
    }
}
