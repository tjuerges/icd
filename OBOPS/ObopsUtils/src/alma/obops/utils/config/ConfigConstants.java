package alma.obops.utils.config;

/**
 * 
 * This class contains constants for Beans in the Spring application context
 * 
 * @author rkurowsk, Nov 2, 2008
 * @version $Revision$
 */

// $Id$
public class ConfigConstants {

	/**
	 * Name of hibernateProperties attribute on sessionFactory
	 */
	public static final String HIBERNATE_PROPERTIES = "hibernateProperties";
	
	/**
	 * SessionFactory's hibernate property names 
	 */
	public static final String CONNECTION_DRIVER 	= "hibernate.connection.driver_class";
	public static final String CONNECTION_PASSWORD 	= "hibernate.connection.password";
	public static final String CONNECTION_URL 		= "hibernate.connection.url";
	public static final String CONNECTION_USER 		= "hibernate.connection.username";
	public static final String CONNECTION_POOL_SIZE = "hibernate.connection.pool_size";
	public static final String HIBERNATE_DIALECT 	= "hibernate.dialect";
	
	/**
	 * Name of sessionFactory bean in Spring applicationContext
	 */
	public static final String SESSION_FACTORY_BEAN = "sessionFactory";
	
    /** Vendor name: Oracle */
    public static final String VENDOR_ORACLE = "oracle";
    
    /** Vendor name: Hypersonic SQL database */
    public static final String VENDOR_HSQLDB = "hsqldb";
    
    /** Vendor name: the ALMA Archive (not really a vendor...) */
    public static final String VENDOR_ARCHIVE = "archive";
}
