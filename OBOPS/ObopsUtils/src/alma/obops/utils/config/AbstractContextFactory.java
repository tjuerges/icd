/**
 * 
 */
package alma.obops.utils.config;

import static alma.obops.utils.config.ConfigConstants.SESSION_FACTORY_BEAN;
import static alma.obops.utils.config.ConfigConstants.VENDOR_HSQLDB;
import static alma.obops.utils.config.ConfigConstants.VENDOR_ORACLE;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;

import alma.archive.database.helpers.wrappers.AbstractDbConfig;

/**
 * 
 * This class, implemented as a singleton, is responsible for creating a 
 * Spring Application context and configuring its Data Base settings using DbConfig.
 * 
 * @author rkurowsk, Nov 2, 2008
 * @version $Revision: 1.7 $
 */

// $Id: AbstractContextFactory.java,v 1.7 2011/02/04 16:37:36 rkurowsk Exp $
public abstract class AbstractContextFactory {

	Logger logger;
		
	/**
	 * applicationContext holds the Spring application context to be used by all threads in this VM
	 */
	private ConfigurableApplicationContext applicationContext;

	private boolean initialized = false;

	private String connectionDriver;
  	private String connectionPassword;
  	private String connectionUrl;
  	private String connectionUser;	
  	private boolean reInitAllowed;

	/*
	 * This will hold information to identify where the AppContextFactory was initialized.
	 * It will be useful for debug if multiple attempts are made to init the factory. 
	 */
	private StackTraceElement[] threadStackDuringInit;
	
	/**
	 * Returns the spring application context
	 * @return
	 */
	public ApplicationContext getAppContext(){
		checkIfInitialized();
		return this.applicationContext;
	}

	/**
	 * Return boolean initialized state of Factory
	 * @return
	 */
	public boolean isInitialized(){
		return initialized;
	}
	
	/**
	 * Returns a bean from the spring container
	 * @param beanName
	 * @return
	 */
	public Object getBean(String beanName){
		
		checkIfInitialized();
		
		try{
			return getAppContext().getBean(beanName);
		}catch(NoSuchBeanDefinitionException e){
			e.printStackTrace();
			logger.severe("The bean: \"" + beanName + "\" has not been added to the Spring application context xml file.");
			throw e;
			
		}
	}
	
	/**
	 * Init method which accepts a spring application context classpath location 
	 * and a dbConfig object.
	 * If the dbConfig is not null it will apply the dbConfig values to the spring app context.
	 * 
	 * Note: This init() method may only be called once. Subsequent calls will result in an exception.
	 * 
	 * @param appContextPath
	 * @param dbConfig
	 * @param classLoader
	 * @param reInitAllowed (true when running tests)
	 */
	public final synchronized void init(String appContextPath, 
			AbstractDbConfig dbConfig, ClassLoader classLoader){

		if(applicationContext == null || reInitAllowed){
			logger = dbConfig.getLogger();
			
			if(reInitAllowed){
				logger.log(Level.WARNING,  "Duplicate initialization of " 
						+ this.getClass().getName() + " was permitted. This should only occur when running tests." );
			}
			
			// SpringDbConfigPostProcessor will be called by Spring after the application context is loaded,
			// it may override certain application context db properties with values from DbConfig and
			// set the connection... variables on the ContextFactory
			SpringDbConfigPostProcessor.setDbConfig(dbConfig);
			SpringDbConfigPostProcessor.setContextFactory(this);
			
			logger.log(Level.INFO, "Attempting to load: " + appContextPath );
			
			try{
				this.applicationContext = new GenericApplicationContext();
				XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader((GenericApplicationContext)this.applicationContext);
				ClassPathResource cpr = new ClassPathResource(appContextPath, classLoader);
				xmlReader.loadBeanDefinitions(cpr);
				this.applicationContext.refresh();
				
			}catch(Exception e){
				logger.log(Level.SEVERE, e.getMessage());
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			
			this.threadStackDuringInit = Thread.currentThread().getStackTrace();
			initialized = true;
			logger.log(Level.INFO, "AppContextFacory initialzed using appContextPath: " 
					+ appContextPath + " - dbConfig: " + dbConfig);
			
		}else{
			duplicateInitializationError();
		}
	}
	
	/**
	 * Init method which accepts a spring application context classpath location 
	 * and a dbConfig object.
	 * If the dbConfig is not null it will apply the dbConfig values to the spring app context.
	 * 
	 * Note: This init() method may only be called once. Subsequent calls will result in an exception.
	 * 
	 * @param appContextPath
	 * @param dbConfig
	 */
	public synchronized void init(String appContextPath, AbstractDbConfig dbConfig){
		init(appContextPath, dbConfig, null);
	}

	/* 
	 * builds up the original initializer stack as a string for display in the error message
	 */
	private void duplicateInitializationError(){
 
		StringBuilder sb = new StringBuilder();
		if( threadStackDuringInit != null ){
			// start with the second element of the trace
			for(int idx = 1; idx < this.threadStackDuringInit.length; idx++){
				StackTraceElement element = this.threadStackDuringInit[idx];
				sb.append("\n --> ");
				sb.append(element.getClassName());
				sb.append(" line: ");
				sb.append(element.getLineNumber());
			}
		}
		String errorMessage = "\nDuplicate call to " + this.getClass().getName() + ".init(). Initialization was already performed by:";
		
		logger.log(Level.SEVERE, errorMessage + sb.toString());
		
		throw new RuntimeException(errorMessage + sb.toString());	
	}
	
    /**
     * @return the connection driver used by the hibernate session factory 
     */
    public String getConnectionDriver() {
    	checkIfInitialized();
    	return this.connectionDriver;
    }

    /**
     * @return the connection password used by the hibernate session factory
     */
	public String getConnectionPassword() {
		checkIfInitialized();
		return this.connectionPassword;
    }

	/**
	 * @return the connection url used by the hibernate session factory
   	 */
    public String getConnectionUrl() {
    	checkIfInitialized();
    	return this.connectionUrl;    
    }

    /**
     * @return the connection user of the hibernate session factory
     */
    public String getConnectionUser() {
    	checkIfInitialized();
    	return this.connectionUser;
    }
    
    /**
	 * @param connectionDriver the connectionDriver to set
	 */
	public void setConnectionDriver(String connectionDriver) {
		this.connectionDriver = connectionDriver;
	}

	/**
	 * @param connectionPassword the connectionPassword to set
	 */
	public void setConnectionPassword(String connectionPassword) {
		this.connectionPassword = connectionPassword;
	}

	/**
	 * @param connectionUrl the connectionUrl to set
	 */
	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	/**
	 * @param connectionUser the connectionUser to set
	 */
	public void setConnectionUser(String connectionUser) {
		this.connectionUser = connectionUser;
	}

	/**
     * @return <code>true</code> is we are connected to an HSQLDB server and
     *         the database is set to <i>file</i> mode; <code>false</code>
     *         otherwise.
     */
    public boolean connectionIsFileBased() {
    	checkIfInitialized();    	
        return connectionIsHsqldb() && getConnectionUrl().contains( ":file:" );
    }

    /**
     * @return <code>true</code> is we are connected to an HSQLDB server and
     *         the database is set to <i>memory</i> mode; <code>false</code>
     *         otherwise.
     */
    public boolean connectionIsMemoryBased() {
    	checkIfInitialized();
        return connectionIsHsqldb() && getConnectionUrl().contains( ":mem:" );
    }

    /**
     * @return <code>true</code> is we are connected to an HSQLDB server,
     *         <code>false</code> otherwise.
     */
    public boolean connectionIsHsqldb() {
    	checkIfInitialized();
        return getConnectionUrl().contains( ":hsqldb:" );
    }
    
    /**
     * @return <code>true</code> is we are connected to an HSQLDB server,
     *         <code>false</code> otherwise.
     */
    public boolean connectionIsOracle() {
    	checkIfInitialized();
        return getConnectionUrl().contains( ":oracle:" );
    }

    /**
     * @return One of {@value DbUtilities#VENDOR_HSQLDB} or 
     *         {@value DbUtilities#VENDOR_ORACLE}
     */
    public String getConnectionVendor() {
    	checkIfInitialized();
    	return connectionIsHsqldb() ? 
                VENDOR_HSQLDB : VENDOR_ORACLE;
    }
    
  /**
	 * Compute a description of where our database server resides.
	 * 
	 * @return If we are connected to an external server, return the hostname
	 *         and optional port of our database server; for instance
	 *         <i>almadev1.hq.eso.org:1521</i>. Otherwise (that is, if the
	 *         dataser server is a local thread), indicate where the data is
	 *         kept:
	 */
	public String getConnectionLocation() {
		// Example URLs:
		// jdbc:oracle:thin:@almadev1.hq.eso.org:1521:alma1
		// jdbc:hsqldb:mem:obops
		// jdbc:hsqldb:file:/home/someuser/obops
		// jdbc:hsqldb:hsql:almadev5.hq.eso.org:9001/obops
		String[] elements = getConnectionUrl().split(":");
		if (connectionIsOracle()) {
			return elements[3].substring(1);
		} else if (connectionIsHsqldb()) {
			if (connectionIsMemoryBased()) {
				return "memory";
			} else {
				return elements[3];
			}
		} else {
			// what are we connected to?!?
			String msg = "Internal error: don;t know this database vendor";
			throw new RuntimeException(msg);
		}
	}
	
	public SessionFactory getSessionFactory(){
		return (SessionFactory)getBean(SESSION_FACTORY_BEAN);
	}

	protected void checkIfInitialized(){
		if(this.applicationContext == null){
		    String msg = "Field applicationContext has not been initialized";
			throw new RuntimeException( msg );
		}
	}
	
	/**
	 * @return the reInitAllowed
	 */
	public boolean isReInitAllowed() {
		return reInitAllowed;
	}

	/**
	 * @param reInitAllowed the reInitAllowed to set
	 */
	public void setReInitAllowed(boolean reInitAllowed) {
		this.reInitAllowed = reInitAllowed;
	}
}
