/**
 * 
 */
package alma.obops.utils.config;

import static alma.obops.utils.config.ConfigConstants.CONNECTION_DRIVER;
import static alma.obops.utils.config.ConfigConstants.CONNECTION_PASSWORD;
import static alma.obops.utils.config.ConfigConstants.CONNECTION_URL;
import static alma.obops.utils.config.ConfigConstants.CONNECTION_USER;
import static alma.obops.utils.config.ConfigConstants.HIBERNATE_DIALECT;
import static alma.obops.utils.config.ConfigConstants.HIBERNATE_PROPERTIES;
import static alma.obops.utils.config.ConfigConstants.SESSION_FACTORY_BEAN;

import java.util.logging.Logger;

import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.ManagedProperties;

import alma.acs.logging.AcsLogLevel;
import alma.archive.database.helpers.wrappers.AbstractDbConfig;

/**
 * This class is a Spring post processor. It will override any databse settings in the spring application context
 * using values from the DbConfig it has been given.
 * 
 * @author rkurowsk, Nov 2, 2008
 * @version $Revision$
 */

// $Id$

public class SpringDbConfigPostProcessor implements BeanFactoryPostProcessor {
    
	Logger logger;
	
    // dbConfig must be initialized before the Spring applicationContext is loaded. 
	private static AbstractDbConfig dbConfig;
	private static AbstractContextFactory ctxFactory;

	/**
	 * Setter for logger. This logger can be initialized in Spring applicationContext
	 * @param logger The logger to use in this class
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Setter for dbConfig
	 * NB: dbConfig must be initialized before the Spring applicationContext is loaded.
	 * @param dbCfg
	 */
	public static void setDbConfig(AbstractDbConfig dbCfg){
		SpringDbConfigPostProcessor.dbConfig = dbCfg;
	}
	
	/**
	 * NB: dbConfig must be initialized before the Spring applicationContext is loaded.
	 * @param ctxFactory the ctxFactory to set
	 */
	public static void setContextFactory(AbstractContextFactory ctxFactory) {
		 
		SpringDbConfigPostProcessor.ctxFactory = ctxFactory;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
	 */
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
			throws BeansException {

		if(dbConfig == null){
			throw new RuntimeException("SpringDbConfigPostProcessor.dbConfig was not initialized");
		}
		if(ctxFactory == null){
			throw new RuntimeException("SpringDbConfigPostProcessor.ctxFactory was not initialized");
		}

		MutablePropertyValues sessionFactoryProperties = 
			(MutablePropertyValues)beanFactory
				.getBeanDefinition(SESSION_FACTORY_BEAN).getPropertyValues();
		
		ManagedProperties hibernateProperties = 
			(ManagedProperties)sessionFactoryProperties
				.getPropertyValue(HIBERNATE_PROPERTIES).getValue();
		
		if( dbConfig != null ) {
           
        	if( dbConfig.getDriver() != null ) {
        		setNewHibernateProperty(CONNECTION_DRIVER, dbConfig.getDriver(), hibernateProperties);
            }

            if( dbConfig.getPassword() != null ) {
            	setNewHibernateProperty(CONNECTION_PASSWORD, dbConfig.getPassword(), hibernateProperties);
            }

            if( dbConfig.getConnectionUrl() != null ) {
            	setNewHibernateProperty(CONNECTION_URL, dbConfig.getConnectionUrl(), hibernateProperties);
            }

            if( dbConfig.getUsername() != null ) {
            	setNewHibernateProperty(CONNECTION_USER, dbConfig.getUsername(), hibernateProperties);
            }
            
            if( dbConfig.getDialect() != null ) {
            	setNewHibernateProperty(HIBERNATE_DIALECT, dbConfig.getDialect(), hibernateProperties);
            }
        }
        
        // RK: I'm not happy about setting the ctxFactory variables here but couldn't easily extract them from
        // the sessionFactory bean once it's created
		ctxFactory.setConnectionDriver(getHibernateProperty(CONNECTION_DRIVER, hibernateProperties));
		ctxFactory.setConnectionPassword(getHibernateProperty(CONNECTION_PASSWORD, hibernateProperties));
		ctxFactory.setConnectionUrl(getHibernateProperty(CONNECTION_URL, hibernateProperties));
		ctxFactory.setConnectionUser(getHibernateProperty(CONNECTION_USER, hibernateProperties));
	}

	private void logInfo(String propName, String propValue){
		if( logger != null )
			logger.log(AcsLogLevel.DEBUG, "Setting sessionFactory's hibernate property " + propName + " = " + propValue);
	}
	
	private void setNewHibernateProperty(String keyString, String newValueString, ManagedProperties hibernateProperties){
		
		logInfo(keyString, newValueString);
		
		Object key = new TypedStringValue(keyString);
		hibernateProperties.remove(key);
		Object newValue = new TypedStringValue(newValueString);
		hibernateProperties.put(key, newValue);
	}
	
	private String getHibernateProperty(String keyString, ManagedProperties hibernateProperties){
		
		Object key = new TypedStringValue(keyString);
		String returnValue = null;
		TypedStringValue value = (TypedStringValue)hibernateProperties.get(key);
		
		if(value!= null ){
			returnValue = value.getValue();
		}
		
		return returnValue;
		
	}

}