/**
 * Copyright European Southern Observatory 2011
 */
package alma.obops.utils.config;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * 
 * Wrapper class for accessing archiveConfig.properties -- introduced to remove
 * dependency on the Archive.
 * 
 * @author amchavan, 10-Mar-2011, from ObopsConfig.java
 * @version $Revision: 1.2 $
 */

// $Id: ArchiveConfig.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class ArchiveConfig {

    /** Name of the system property equivalent to the ACSDATA env variable */
    private static final String ACSDATA = "ACS.data";
    private static final String ARCHIVE_CONFIG_PATH = "/config/archiveConfig.properties";

    // names of the properties in archiveConfig.properties
    private static final String USERREPOSITORY_PROVIDER_URL = "archive.userrepository.provider.url";

    private static File archiveConfigFile;
    private static Properties archiveConfigProperties;

    static {
        initArchiveConfig();
    }

    /**
     * @return The URL of the LDAP server, keyword
     *         {@value #USERREPOSITORY_PROVIDER_URL}
     */
    public static String getUserRepositoryProviderUrl() {
        return getProperty( USERREPOSITORY_PROVIDER_URL );
    }

    /**
     * @return the contents of archiveConfig.properties as a Properties object
     */
    public static Properties getProperties() {
        return archiveConfigProperties;
    }

    /**
     * @return the property corresponding to the given key
     */
    private static String getProperty( String key ) {

        String propValue = archiveConfigProperties.getProperty( key );
        if( propValue == null ) {
            String msg = "Property " + key + " not found in "
                    + archiveConfigFile.getAbsolutePath();
            throw new RuntimeException( msg );
        }

        return propValue;
    }

    /*
     * Initializes access to archiveConfig.properties.
     */
    private static void initArchiveConfig() {

        try {
            if( archiveConfigProperties == null ) {
                String acsDataPath = System.getProperty( ACSDATA );
                if( acsDataPath == null ) {
                    String msg = "System property " + ACSDATA + " not found";
                    throw new RuntimeException( msg );
                }

                archiveConfigFile = new File( acsDataPath + ARCHIVE_CONFIG_PATH );

                if( !archiveConfigFile.exists() ) {
                    String msg = "File " + archiveConfigFile.getAbsolutePath()
                            + " not found";
                    throw new RuntimeException( msg );
                }

                archiveConfigProperties = new Properties();
                archiveConfigProperties.load( new FileInputStream(
                        archiveConfigFile ) );
            }
        }
        catch( Exception e ) {
            throw new RuntimeException( e );
        }
    }
}
