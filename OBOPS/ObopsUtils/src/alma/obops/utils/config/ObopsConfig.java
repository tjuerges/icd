/**
 * Copyright European Southern Observatory 2011
 */
package alma.obops.utils.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * Wrapper class for accessing obopsConfig.properties
 *
 * @author rkurowsk, Jan 12, 2011
 * @version $Revision: 1.3 $
 */

// $Id: ObopsConfig.java,v 1.3 2011/03/01 08:48:59 fjulbe Exp $

public class ObopsConfig {

    /** Name of the system property equivalent to the ACSDATA env variable */
	private static final String ACSDATA = "ACS.data";
    private static final String OBOPS_CONFIG_PATH = "/config/obopsConfig.properties";
    
    // names of the properties in obopsConfig.properties
    private static final String MAIL_SMTP_SERVER_PROP = "mail.smtp.server";
    private static final String MAIL_FROM_ADDRESS_PROP = "mail.from.address";
    private static final String MAIL_USER_PROP = "mail.user";
    private static final String MAIL_PASSWORD_PROP = "mail.password";
    private static final String USER_PORTAL_URL_PROP = "userportal.url";

    private static final String   LDAP_PRINCIPAL = "obops.ldap.principal.dn";
    private static final String    LDAP_PASSWORD = "obops.ldap.principal.password";
    private static final String LDAP_ACCOUNTS_OU = "obops.ldap.accounts.ou";
    private static final String    LDAP_ROLES_OU = "obops.ldap.roles.ou";
    
    
    
    private static File obopsConfigFile;
    private static Properties obopsConfigProperties;
    
    static{initObopsConfig();}
    
    /**
     * Returns the from email specified in obopsConfig.properties
     * @return
     */
    public static String getEmailFromAddress(){
    	return getProperty(MAIL_FROM_ADDRESS_PROP);
    }
    
    /**
     * Returns the email password specified in obopsConfig.properties
     * @return
     */
    public static String getEmailPassword(){
    	return getProperty(MAIL_PASSWORD_PROP);
    }
    
    /**
     * Returns the email user specified in obopsConfig.properties
     * @return
     */
    public static String getEmailUser(){
    	return getProperty(MAIL_USER_PROP);
    }
    
    /**
     * @return The OU for LDAP-based User Portal accounts; e.g.
     *         <em>ou=people,ou=master,dc=alma,dc=info</em>
     */
    public static String getLdapAccountsOU(){
        return getProperty( LDAP_ACCOUNTS_OU );
    }
    
    /**
     * @return The DN to use for accessing LDAP; should have read+write
     *         privileges, as we need to alter entries; e.g.
     *         <em>uid=admin,ou=people,ou=master,dc=alma,dc=info</em>
     */
    public static String getLdapPrincipalDN(){
        return getProperty( LDAP_PRINCIPAL );
    }
    
    /**
     * @return The password for the DN  used for accessing LDAP
     */
    public static String getLdapPrincipalPassword(){
        return getProperty( LDAP_PASSWORD );
    }
    
    /**
     * @return The OU for LDAP-based User Portal roles; e.g.
     *         <em>ou=roles,ou=master,dc=alma,dc=info</em>
     */
    public static String getLdapRolesOU(){
        return getProperty( LDAP_ROLES_OU );
    }
    
    /**
     * Returns the obopsConfig.properties as a Properties object
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static Properties getProperties() throws FileNotFoundException, IOException{
        return obopsConfigProperties;
    }

    /**
     * Returns the property corresponding to the given propKey
     * @param propKey
     * @return
     */
    private static String getProperty(String propKey){
        
    	String propValue = obopsConfigProperties.getProperty( propKey );
        if( propValue == null ) {
            String msg = "Property " + propKey + 
                         " not found in " + obopsConfigFile.getAbsolutePath();
            throw new RuntimeException( msg );
        }
        
        return propValue;
    }    

    /**
     * Returns the SMTP server specified in obopsConfig.properties
     * @return
     */
    public static String getSmtpServer(){
    	return getProperty(MAIL_SMTP_SERVER_PROP);
    }

    /**
     * Returns the user portal url specified in obopsConfig.properties
     * @return
     */
    public static String getUserPortalUrl(){
    	return getProperty(USER_PORTAL_URL_PROP);
    }    

    /*
     * Initializes access to obopsConfig.properties.
     */
    private static void initObopsConfig(){

    	try{
	        if(obopsConfigProperties == null){
	            String acsDataPath = System.getProperty( ACSDATA );
	            if( acsDataPath == null ) {
	                String msg = "System property " + ACSDATA + " not found";
	                throw new RuntimeException( msg );
	            }
	            
		        obopsConfigFile = new File( acsDataPath + OBOPS_CONFIG_PATH);
	        
		        if(!obopsConfigFile.exists() ) {
		            String msg = "File " + obopsConfigFile.getAbsolutePath() + " not found";
		            throw new RuntimeException( msg );
		        }
		    
		        obopsConfigProperties = new Properties();
				obopsConfigProperties.load( new FileInputStream( obopsConfigFile ));
	        }	
    	}catch(Exception e){
    		throw new RuntimeException(e);
    	}

    }
}
