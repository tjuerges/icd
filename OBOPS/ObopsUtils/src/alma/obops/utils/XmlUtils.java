/**
 * XmlUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;

import java.io.StringReader;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;


/**
 * A collection of XML-related utility methods and constants.
 *
 * @author amchavan, Oct 26, 2010
 * @version $Revision: 1.2 $
 */

// $Id: XmlUtils.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class XmlUtils {

    public static final String SCHEMA_LANGUAGE = "http://www.w3.org/2001/XMLSchema";

    /**
     * Validate an XML document against a schema
     * 
     * @param xml          Text of the XML document
     * @param schema       A URL identifying the schema file
     * @param errorHandler What to do with validation errors
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws DocumentException
     * 
     * @author amchavan, 2010-10-26 -- from http://www.edankert.com/validate.html
     */
    public static void validate( String xml, URL schema, ErrorHandler errorHandler ) 
        throws SAXException,
               ParserConfigurationException, 
               DocumentException {
        
        SAXParserFactory pFactory = SAXParserFactory.newInstance();
        SchemaFactory sFactory = SchemaFactory.newInstance( SCHEMA_LANGUAGE );
        
        StreamSource source = new StreamSource( schema.toExternalForm() );
        Source[] sources = new Source[] { source };
        pFactory.setSchema( sFactory.newSchema( sources ));
    
        SAXParser parser = pFactory.newSAXParser();
        SAXReader reader = new SAXReader( parser.getXMLReader() );
        reader.setValidation( false );
        reader.setErrorHandler( errorHandler );
        reader.read( new StringReader( xml ));
    }
}
