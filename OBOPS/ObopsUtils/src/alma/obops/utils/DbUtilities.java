/*
 * Utils
 * 
 * $Revision$
 *
 * Sep 22, 2006
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A collection of database-related methods and constants.
 * 
 * @version $Revision$
 * @author amchavan, Sep 22, 2006
 */

// $Id$

public class DbUtilities {
    
    /** JDBC driver for HSQLDB */
    public static final String HSQLDB_JDBC_DRIVER = "org.hsqldb.jdbcDriver";
    
    /** JDBC driver for Oracle */
    public static final String ORACLE_JDBC_DRIVER = "oracle.jdbc.OracleDriver";
    
    /** Vendor name: Oracle */
    public static final String VENDOR_ORACLE = "oracle";
    
    /** Vendor name: Hypersonic SQL database */
    public static final String VENDOR_HSQLDB = "hsqldb";
    
    /** Vendor name: the ALMA Archive (not really a vendor...) */
    public static final String VENDOR_ARCHIVE = "archive";

    /** Possible modes for the HSQLDB server */
    public enum HsqldbMode {
    	MEMORY, FILE, SERVER, SERVLET, WEB
    };
    
    /**
     * Execute an SQL script.
     * @param script  A stream opened on the SQL script
     * @param conn    Connection to the DB server
     * @throws SQLException
     * @throws IOException 
     */
    public static void runScript( InputStream script, Connection conn )
            throws SQLException, IOException {
        final String sqlText = FileIoUtils.fileToString( script );
        runScript( sqlText, conn );
    }
        
    /**
     * Execute an SQL script.
     * @param script  The SQL script, as a single string
     * @param conn    Connection to the DB server
     * @throws SQLException
     */
    public static void runScript( String script, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement();
        String[] statements = script.split( ";", -1 );
        for( int i = 0; i < statements.length; i++ ) {
            String statement = statements[i].trim();
            if( statement.length() == 0 ) {
                // skip empty lines
                continue;
            }
//            System.out.println( statement );
            try {
                stmt.execute( statement );
            }
            catch( SQLException e ) {
                // We want to ignore errors when dropping non-existent
                // tables or views
                String msg = e.getMessage().toLowerCase();
                if( ! (statement.toLowerCase().contains( "drop" ) && 
                      (msg.contains( "not exist" ) ||       // ORACLE
                       msg.contains( "not found" )          // HSQLDB 
                       ))) {
                    
                    throw new SQLException( e.getMessage()
                            + " while executing [" + statement + "]", e );
                }
            }
        }
    }
 
    /**
     * Execute an SQL script.
     * @param script  A stream opened on the SQL script
     * @param conn    Connection to the DB server
     * @throws SQLException
     * @throws IOException 
     */
    public static void runSingleScript( InputStream script, Connection conn )
            throws SQLException, IOException {
        final String sqlText = FileIoUtils.fileToString( script );
        runSingleScript( sqlText, conn );
    }
    
    /**
     * Execute a single SQL script, no split, executes it as it is
     * @param script  The SQL script, as a single string
     * @param conn    Connection to the DB server
     * @throws SQLException
     */
    public static void runSingleScript( String script, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement();
            try {
                stmt.execute( script );
            }
            catch( SQLException e ) {
                // We want to ignore errors when dropping non-existent
                // tables or views
                String msg = e.getMessage().toLowerCase();
                if( ! (script.toLowerCase().contains( "drop" ) && 
                      (msg.contains( "not exist" ) ||       // ORACLE
                       msg.contains( "not found" )          // HSQLDB 
                       ))) {
                    
                    throw new SQLException( e.getMessage()
                            + " while executing [" + script + "]", e );
                }
            }
    }    
    
    /**
     * Execute an SQL query.
     * NB: The returned resultset's statement must be closed after use.
     * Otherwise Oracle's <em>too many cursors</em> error will occur.
     * 
     * @param query  An SQL "SELECT" statement
     * @param conn   Connection to the DB server
     * @throws SQLException
     */
    public static ResultSet query( String query, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                               ResultSet.CONCUR_UPDATABLE );
        ResultSet ret = stmt.executeQuery( query );
        
        return ret;
    }

    /**
     * Execute an SQL query.
     * 
     * @param query
     *            An SQL "SELECT" statement
     * @param conn
     *            Connection to the DB server
     *            
     * @return The result set, converted to a {@link ResultTable}
     * @throws SQLException
     */
    public static ResultTable queryAsTable( String query, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery( query );
        ResultTable ret = new ResultTable( rs );
        return ret;
    }
    

    
    /**
     * Create a database.
     * 
     * @param ddl
     *            Resource name of the DDL script to create the database.
     * 
     * @param conn
     *            Connection to the database server.
     *            
     * @throws IOException 
     * @throws SQLException 
     */
     public static void createDatabase( String ddl, Connection conn ) 
         throws IOException, SQLException {
         
        if( ddl == null || conn == null ) {
            throw new IllegalArgumentException( "Null arg" );
        }

        final String ddltext = FileIoUtils.resourceToString( ddl );
        runScript( ddltext, conn );
    }
}
