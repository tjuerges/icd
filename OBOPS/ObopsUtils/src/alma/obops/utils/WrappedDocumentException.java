package alma.obops.utils;

/** 
 * We wrap iText exceptions here, to make the API of PdfUtils simpler 
 *
 * @author amchavan, May 12, 2010
 * @version $Revision$
 */

// $Id$

public class WrappedDocumentException extends Exception {

    private static final long serialVersionUID = 5614801583468278548L;

    public WrappedDocumentException() {
        super();
    }

    public WrappedDocumentException( String message ) {
        super( message );
    }

    public WrappedDocumentException( String message, Throwable cause ) {
        super( message, cause );
    }

    public WrappedDocumentException( Throwable cause ) {
        super( cause );
    }
}