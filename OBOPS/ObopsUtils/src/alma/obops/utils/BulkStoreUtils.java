/**
 * BulkStoreUtils.java
 *
 * Copyright European Southern Observatory 2010
 */

package alma.obops.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import alma.acs.util.AcsLocations;
import alma.bulkdata.BulkStoreHelper;
import alma.bulkdata.BulkStoreOperations;
import alma.bulkdata.BulkStorePackage.BulkStoreException;

/**
 * A collection of utility methods to interact with the Archive's bulk store.
 * <p/>
 * 
 * <strong>Reminder</strong>: in order to connect to the Bulk Store you need in
 * fact to connect to an ACS Manager, which in turn exposes the Bulk Store
 * component. <br/>
 * 
 * While obtaining the component is the responsibility of this class, the caller
 * must make sure that the Manager can be reached; that can be done via setting
 * Java system properties <em>ACS.managerhost</em> (e.g.
 * <code>ite2.hq.eso.org</code>) and <em>ACS.baseport</em> (e.g. <code>0</code>
 * ); as an alternative, property <em>ACS.manager</em> can be set with a full
 * corbaloc, like <code>corbaloc::134.171.27.234:3000/Manager</code>.<br/>
 * See {@link AcsLocations#figureOutManagerLocation()} for more info.
 * <p/>
 * 
 * When using <em>acsStartJava</em> on Linux, setting the environment variable
 * <em>MANAGER_REFERENCE</em> will affect the setting of system property
 * ACS.manager, while ACS.baseport will be set from <em>ACS_INSTANCE</em>.<br/>
 * As of 2011/05/12 there is no way to set ACS.managerhost from an environment
 * variable; see http://jira.alma.cl/browse/COMP-5426
 * 
 * @author amchavan, May 12, 2010
 * @version $Revision$
 */

// $Id$

public class BulkStoreUtils {

	/** MIME type of the PDF files in the Bulk Store */
    // public static final String PDF_MIME_TYPE = "application/pdf";
    public static final String PDF_MIME_TYPE = "application/octet-stream";
    
    /** Our name, when we connect to ACS */
    public static final String OBOPS_CLIENT = "ObOpsBulkStoreRetriever";
    
    /** Bulk store ID */
    public static final String IDL_BULKSTORE_CONNECTION = 
        "IDL:alma/bulkdata/BulkStore:1.0";
    
    private static BulkStoreOperations bulkstoreConnection;
    
    private static Logger logger = GeneralUtils.getAcsLogger( BulkStoreUtils.class.getName() );

    /**
     * @param bulkUid
     * @return
     * @throws WrappedAcsException 
     */
    public static byte[] bulkStoreRetrieve( String bulkUid ) 
        throws WrappedAcsException {
        
        BulkStoreOperations bulkstore = connectToBulkStore();
        try {
            logger.fine( "bulkStoreRetrieve(): starting..." );
            byte[] data = bulkstore.retrieve( bulkUid );
            logger.fine( "bulkStoreRetrieve(): returning " + data.length + " bytes" );
            return data;
        }
        catch( BulkStoreException e ) {
            throw new WrappedAcsException( e );
        }
    }

    /**
     * @return A connection to the bulk store to be found at
     *         {@link #IDL_BULKSTORE_CONNECTION}; the connection will be made
     *         using {@link #OBOPS_CLIENT} as client name.
     * @throws WrappedAcsException
     */
    public static BulkStoreOperations connectToBulkStore()
            throws WrappedAcsException {
        
        String location = null;
        if( bulkstoreConnection == null ) {

            Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

            try {
                location = AcsLocations.figureOutManagerLocation();
                logger.fine( "connectToBulkStore(): corbaloc='" + location
                        + "'" );

                ComponentClient client = new ComponentClient( logger, location,
                        OBOPS_CLIENT ) {
                    @Override
                    protected void initAlarmSystem() throws Exception {
                        final String msg = OBOPS_CLIENT
                                + " client does not initialize its alarm "
                                + "system, as it's not needed, "
                                + "and might cause problems when used from a "
                                + "webapp with a pre-defined classpath";
                        m_logger.info( msg );
                    }
                };
                logger.fine( "connectToBulkStore(): client=" + client );
                
                ContainerServices services = client.getContainerServices();
                org.omg.CORBA.Object bulkStoreRef = 
                    services.getDefaultComponent( IDL_BULKSTORE_CONNECTION );
                final String msg = "connectToBulkStore(): bulkStoreRef("
                        + IDL_BULKSTORE_CONNECTION + ")=" + bulkStoreRef;
                logger.fine( msg );

                bulkstoreConnection = BulkStoreHelper.narrow( bulkStoreRef );
            }
            catch( Exception exc ) {
                final String msg = "Couldn't connect to "
                        + IDL_BULKSTORE_CONNECTION + " at " + location + ": "
                        + exc.getMessage();
                throw new WrappedAcsException( msg, exc );
            }
        }
        
        logger.fine( "connectToBulkStore(): returning bulkstoreConnection=" + 
                     bulkstoreConnection );
        return bulkstoreConnection;
    
    }
    
    /**
     * Retrieve from the Bulk Store a PDF file.
     * 
     * @param out      The retrieved PDF file will be streamed here
     * @param bulkUid  Bulk Store ID of the PDF file
     * 
     * @throws WrappedAcsException 
     * @throws MessagingException 
     * @throws IOException 
     * @throws WrappedDocumentException 
     */
    
 // Hack for IT4
 // TODO: RK - uncomment this if the mime type issue of pdf files in NGAS is resolved post IT4
    
//    public static void pdfRetrieve( OutputStream out, String bulkUid ) 
//        throws WrappedAcsException, MessagingException, 
//               IOException, WrappedDocumentException {
//        
//        byte[] data = BulkStoreUtils.bulkStoreRetrieve( bulkUid );  
//        logger.fine( "getPdf(): retrieved data size=" + data.length );
//        if( data.length == 0 ) {
//            throw new IOException( "Retrieved zero data for bulk ID " + bulkUid );
//        }
//        
//        ByteArrayInputStream in = new ByteArrayInputStream( data );
//        MimeMessage mimeMsg = new MimeMessage( null, in );
//        
////        List<byte[]> pdfFiles = unpack( mimeMsg, PDF_MIME_TYPE, null );
//        Map<String, byte[]> pdfFilesMap = unpack( mimeMsg, PDF_MIME_TYPE);
//		List<byte[]> pdfFiles = (List<byte[]>) pdfFilesMap.values();
//        logger.fine( "getPdf(): unpacked PDF files=" + pdfFiles.size() );
//        
//        PdfUtils.combine( out, pdfFiles );
//    }
    
    /**
     * Retrieve from the Bulk Store a PDF file.
     * 
     * @param bulkUid  Bulk Store ID of the PDF file
     * @param ordering List of the requested order of the attachment types.
     * 
     * @return An input stream opened on the PDF file
     * 
     * @throws WrappedAcsException 
     * @throws MessagingException 
     * @throws IOException 
     * @throws WrappedDocumentException 
     */
    public static InputStream pdfRetrieve( String bulkUid, List<String> ordering) 
        throws MessagingException, 
               IOException, WrappedDocumentException {
        
    	try {
	        byte[] data = BulkStoreUtils.bulkStoreRetrieve( bulkUid );  
	        logger.fine( "getPdf(): retrieved data size=" + data.length );
	        if( data.length == 0 ) {
	            throw new IOException( "Retrieved zero data for bulk ID " + bulkUid );
	        }
	        
	        ByteArrayInputStream in = new ByteArrayInputStream( data );
	        MimeMessage mimeMsg = new MimeMessage( null, in );
	        
	//        List<byte[]> pdfFiles = unpack( mimeMsg, PDF_MIME_TYPE );
	        Map<String, byte[]> pdfFilesMap = unpack( mimeMsg, PDF_MIME_TYPE);
	        logger.fine( "getPdf(): unpacked PDF files=" + pdfFilesMap.size() );
	        
	        if( pdfFilesMap.size() == 0 ) {
	            logger.fine( "getPdf(): returning empty file" );
	            String text = "No PDF file(s) found for bulk ID " + bulkUid;
	            return PdfUtils.create( text );
	        }
			// order the pdfs
	        List<byte[]> pdfFiles = PdfUtils.order(pdfFilesMap, ordering);
	        // should work if we let PdfUtils.combine do it, but doing it here just to be sure
	        if (pdfFiles.size() == 0) {
	            logger.fine( "getPdf(): returning empty file" );
	            String text = "Empty ordering was passed for bulk ID " + bulkUid;
	            return PdfUtils.create( text );
	        }
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        PdfUtils.combine( out, pdfFiles );
	        InputStream ret = new ByteArrayInputStream( out.toByteArray() );
	        
	        return ret;
    	} catch (WrappedAcsException we) {
            logger.fine( "getPdf(): returning empty file" );
            String text = "No data available: no connection to bulk store: "
                    + we.getMessage();
            return PdfUtils.create( text );
    	}
    }

    /**
     * @param The content type string of a MIME part, e.g.
     *            <code>text/plain; charset=ISO-8859-1; format=flowed</code> or
     *            <code>application/pdf; name="in1.pdf"</code>
     * @return The type of the files in the of the <code>"type="</code>
     */
    protected static String extractFileType( String mimeContentType ) {
        String[] tmp = mimeContentType.split( ";" );
        return tmp[0];
    }

    /**
     * Unpack a MIME message, select its parts of the given type, return those
     * parts as byte arrays
     * 
     * @param type
     *            Something like <code>text/xml</code> or
     *            <code>application/pdf</code>; if <code>null</code>, parts of
     *            any type will be returned
     * 
     * @throws MessagingException
     * @throws IOException
     * @throws RuntimeException If input argument <em>message</em> is null
     */
    // Original code provided by Marcus Schilling, 11-May-2010
    // modified to return the elements of the MIME message as byte[]
    public static Map<String, byte[]> unpack( MimeMessage message, String type ) 
        throws IOException, MessagingException {
        
        if( message == null || type == null ) {
            throw new RuntimeException( "Null parameter" );
        }
        
        Map<String, byte[]> ret = new HashMap<String, byte[]>();
        MimeMultipart multipart = (MimeMultipart) message.getContent();

        int nParts = multipart.getCount();
        for( int i = 0; i < nParts; i++ ) {
            MimeBodyPart mbp = (MimeBodyPart) multipart.getBodyPart( i );
            String mimeContentType = mbp.getContentType();
            
            logger.fine( "unpack(" + i + "): mimeContentType='"
                    + mimeContentType + "'" );

            String fileType =  ( mimeContentType );
            
            logger.fine( "unpack(" + i + "): extracted fileType='" + fileType + "'" );

// Hack for IT4
// TODO: RK - uncomment this when the mime type issue of pdf files in NGAS is resolved post IT4
            
//			if (type != null && !type.equals(fileType)) {
//				logger.fine("unpack(" + i + "): skipping '" + fileType
//						+ "' part");
//				continue;
//			}
//			// map the pdfs to their names
//			String key;
//			if (type.equals(PDF_MIME_TYPE)) {
//				key = extractFileType(mbp.getFileName());
//			} else {
//				key = String.valueOf(i);
//			}
            
// TODO: RK - and remove the following lines (see previous todo)        
            if (mbp.getFileName() == null) {
				logger.fine("unpack(" + i + "): skipping part due to null file name");
				continue;
			}
            String key = extractFileType(mbp.getFileName());
            
// End of Hack for IT4            

            ByteArrayOutputStream fos = new ByteArrayOutputStream();
            InputStream is = new BufferedInputStream(
            		mbp.getDataHandler().getDataSource().getInputStream() );
            int b;
            while( (b = is.read()) != -1 ) {
                fos.write( b );
            }
            fos.close();
            logger.fine( "unpack(" + i + "): read '" + fileType + "' part" );

            ret.put(key, fos.toByteArray() );
        }

        return ret;
    }

    // FOR DEBUGGING ONLY
    // Try with uid://A501/X3003/X6 
    public static void main( String[] args ) {
    	if( args.length == 0 ) {
    		System.out.println( "Usage:\t" +
    				BulkStoreUtils.class.getCanonicalName() +
    				" <bulk-store-id>\n\twhere <bulk-store-id> is e.g. uid://A501/X3003/X6" );
    		System.exit( 1 );
    	}
    	
        try {
            InputStream stream = pdfRetrieve( args[0], null );
            byte[] tmp = FileIoUtils.readBytes( stream );
            File out = new File( "test.pdf" );
            FileIoUtils.byteArrayToFile( tmp, out.getAbsolutePath() );
            System.out.println( "Created " + out.getAbsolutePath() );
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * For testing only: retrieve a mock PDF file -- no Bulk Store access.
     * 
     * @param bulkUid  Bulk Store ID of the PDF file -- IGNORED
     * 
     * @return An input stream opened on the PDF file
     * 
     * @throws WrappedAcsException 
     * @throws MessagingException 
     * @throws IOException 
     * @throws WrappedDocumentException 
     */
    public static InputStream pdfRetrieveMOCK( String bulkUid ) 
        throws WrappedAcsException, MessagingException, 
               IOException, WrappedDocumentException {
        String text = "This is the mock PDF file for bulk ID " + bulkUid;
        return PdfUtils.create( text );
    }
}
