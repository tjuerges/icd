/*
 * HsqldbUtilities
 * 
 * $Revision: 1.5 $
 *
 * Sep 13, 2006
 * 
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;


/**
 * Utilities for HSQLDB
 * 
 * @version $Revision: 1.5 $
 * @author amchavan, Sep 13, 2006
 */

// $Id: HsqldbUtilities.java,v 1.5 2010/10/19 11:30:42 rkurowsk Exp $
public class HsqldbUtilities {

    /** Possible HSQLDB modes */ 
    public enum Mode {
        Memory, File, Server
    }
    
    /** Password for connecting to the HSQLDB server */
    public static final String HSQLDB_PASSWORD = "";

    /** Username for connecting to the HSQLDB server */
    public static final String HSQLDB_USER = "sa";

    /** Base URL for an HSQLDB file-based database */
    public static final String HSQLDB_FILE_URL = "jdbc:hsqldb:file:";

    /** Base URL for an HSQLDB in-memory database */
    public static final String HSQLDB_MEMORY_URL = "jdbc:hsqldb:mem:ignored";

    /** Base URL for an HSQLDB server-mode database */
    public static final String HSQLDB_SERVER_URL = "jdbc:hsqldb:hsql://";
    
    /** JDBC driver for HSQLDB */
    public static final String HSQLDB_JDBC_DRIVER = "org.hsqldb.jdbcDriver";

    static {
        try {
            Class.forName( HSQLDB_JDBC_DRIVER );
        }
        catch( ClassNotFoundException e ) {
            throw new ExceptionInInitializerError( e );
        }
    }
    
    /**
     * Create a local HSQLDB database.
     * 
     * @param mode
     *            Mode of the database server
     * 
     * @param name
     *            Name of the database: for instance "shiftlog"; ignored if
     *            parameter <em>conn</em> is not null.
     * 
     * @param location
     *            If parameter <em>mode</em> is {@linkplain Mode#Server}, this
     *            should be a string like
     *            <code>&lt;hostname&gt;</code> for instance
     *            <code>localhost</code>. <br/>
     *            If parameter <em>mode</em> is {@linkplain Mode#File}, this
     *            should be the pathname of the directory where the database
     *            will be created, relative to the current working directory;
     *            for instance <code>hsqldb-files</code>. <br/>
     *            This parameter will be ignored if parameter <em>mode</em> is
     *            {@linkplain Mode#Memory} (for an in-memory database), or if
     *            parameter <em>conn</em> is not null.
     * 
     * @param ddl
     *            Resource name of the DDL script to create the database.
     * 
     * @param conn
     *            Connection to the database server; if <code>null</code>, a new
     *            one will be created (and eventually closed) based on
     *            parameters <em>name</em> and <em>location</em>.
     * @throws Exception 
     */
     public static void createDatabase( Mode mode,
                                        String name, 
                                        String location, 
                                        String ddl,
                                        Connection conn ) throws Exception {
        boolean newConnection = false;

        if( conn == null ) {
            newConnection = true;
            conn = getConnection( mode, name, location );
        }
        final String ddltext = FileIoUtils.resourceToString( ddl );
        DbUtilities.runScript( ddltext, conn );
        if( mode == Mode.File ) {
            DbUtilities.runScript( "SHUTDOWN", conn );
        }else if( newConnection ) {
            conn.close();
        }

    }

    /**
     * Return a valid HSQLDB connection URL based on the input parameters
     * 
     * @param mode
     *            Database server mode
     * @param name
     *            Name of the database; e.g. <em>asdm</em>
     * @param location
     *            If mode is {@link Mode#Server}, this should be the hostname of
     *            the machine where the HSQLDB server is running; if mode is
     *            {@link Mode#File}, this should be the name of the directory
     *            where HSQLDB files are stored. Otherwise this parameter is
     *            ignored.
     * @return
     */
    public static String getConnectionUrl( Mode mode, String name,
                                           String location ) {
        String url;
        if( mode == Mode.Server ) {
            url = HSQLDB_SERVER_URL + location + ":9001/" + name;
        }
        else if( mode == Mode.File ) {
            url = HSQLDB_FILE_URL + location + "/" + name;
        }
        else {
            url = HSQLDB_MEMORY_URL;
        }
        return url;
    }

     
     /**
      * Create a local HSQLDB database.
      * @param url
      *            hsqldb url
      * 
      * @param ddl
      *            Pathname of the DDL script to create the database.
      * 
      * @throws Exception 
      */
     public static void createDatabase(String url, String ddl) throws Exception {
    	 
		try {
			Class.forName(HSQLDB_JDBC_DRIVER);
			Connection conn = getConnection(url);
			final String ddltext = FileIoUtils.resourceToString(ddl);
			try {
				DbUtilities.runScript(ddltext, conn);
			} finally {
				conn.commit();
				conn.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
      
     /**
     * Utility method.
     * @return true if String word is equal() to one of the strings
     *         in words[]; false otherwise.
     */
    private static boolean findWordInList( String word, String[] words ) {
        for( int i = 0; i < words.length; i++ ) {
            if( word.equals( words[i] )) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Shutdown the HSQLDB database server.
     * 
     * @param mode
     *            Mode of the database server
     * 
     * @param name
     *            Name of the database: for instance "shiftlog"; ignored if
     *            parameter <em>conn</em> is not null.
     * 
     * @param location
     *            If parameter <em>mode</em> is {@linkplain Mode#Server}, this
     *            should be a string like
     *            <code>&lt;hostname&gt;</code> for instance
     *            <code>localhost</code>. <br/>
     *            If parameter <em>mode</em> is {@linkplain Mode#File}, this
     *            should be the pathname of the directory where the database
     *            will be created, relative to the current working directory;
     *            for instance <code>hsqldb-files</code>. <br/>
     *            This parameter will be ignored if parameter <em>mode</em> is
     *            {@linkplain Mode#Memory} (for an in-memory database), or if
     *            parameter <em>conn</em> is not null.
     * 
     * @param conn
     *            Connection to the database server; if <code>null</code>, a new
     *            one will be created (and eventually closed) based on
     *            parameters <em>name</em> and <em>location</em>.
     */
     public static void shutdown( Mode mode, 
                                  String name, 
                                  String location,
                                  Connection conn ) {
        try {
        	
            boolean newConnection = false;
            if( conn == null ) {
                newConnection = true;
                conn = getConnection( mode, name, location );
            }
            DbUtilities.runScript( "SHUTDOWN", conn );
            if( newConnection ) {
                conn.close();
            }
    
        }catch( Exception e ) {
            e.printStackTrace();
            return;
        }
    }

    /**
      * Check if a directory contains the data files needed for a valid
      * HSQLDB database. Note: does not check if those files are valid
      * themselves.
      * 
      * @param dirname  The pathname of a directory.
      * @param dbname   Name of the database stored in that directory.
      * 
      * @return  <code>true</code> if the input directory is a
      *          files valid HSQLDB database;
      *          <code>false</code> otherwise.
      */
    public static boolean validDatabaseDirectory( String dirname, 
                                                  String dbname ) {

        File dir = new File( dirname );
        
        // directory must be there
        if( ! dir.exists() )       
            return false;
        
        // directory must contain dbname.properties and dbname.script
        String[] files = dir.list();
        if( files == null )
            return false;               // directory is empty!

        if( ! findWordInList( dbname + ".properties", files ) ||
            ! findWordInList( dbname + ".script", files ) )
            return false;
        
        // all is fine
        return true;
    }
    
    private static Connection getConnection(Mode mode, String name, String location){
		
    	String url = getConnectionUrl( mode, name, location );
		return getConnection(url);
		
    }
    
    private static Connection getConnection(String url){
    	
    	Connection conn = null;
    	
		try {
			Class.forName( HSQLDB_JDBC_DRIVER );
			conn = DriverManager.getConnection(url, HSQLDB_USER, HSQLDB_PASSWORD);
		} catch (Exception e) {
			throw new RuntimeException(
					"Error getting jdbc connection for url: " + url
							+ "\n Error message: " + e.getMessage());
		}
		
		return conn;
    }
}
