/*
 * TestUtilities
 *
 * $Revision$
 *
 * Sep 29, 2006
 *
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


/**
 * A collection of static methods, useful for testing this project.
 *
 * @version $Revision$
 * @author amchavan, Sep 29, 2006
 */

// $Id$

public class TestUtilities {

    /** Where we should launch our tests */
    public static final String CANONICAL_TEST_DIR = "test";

    /**
     * Standard error message if we're not running our unit tests
     * from the canonical test directory.
     */
    public static final String CANONICAL_DIR_MSG =
        "Test must be run from directory '" + CANONICAL_TEST_DIR + "'";

    /**
     * Name of the DB configuration property indicating the location
     * of the server to use.
     */
    static public final String LOCATIONPROP = "archive.oracle.location";

    @SuppressWarnings("unused")
	static private final String[] LOCATIONSPROPS = {
        // Important: first integration, then development, then local
        "integration.oracle.location",
        "development.oracle.location",
        "local.oracle.location",
    };

    // Used to build default UIDs
    private static int id0 = -1;

    private static int id1 = -1;

    private static int id2 = 0;

    /**
     * Utility method: get the value of the child of Element root with the given
     * name; play around with different situations.
     */
    @SuppressWarnings("unchecked")
    public static String getChild( Element root, String name ) {
        Element child = root.getChild( name );
        if( child == null ) {
            throw new RuntimeException( "No child named '" + name + "'" );
        }
        List<Object> contents = child.getContent();
        if( contents.size() == 0 ) {
            return "";
        }
        String ret = child.getContent( 0 ).getValue();
        return ret;
    }


    /**
     * @return <code>true</code> is the current working directory is
     *         {@link #CANONICAL_TEST_DIR}, <code>false</code> otherwise.
     */
    public static boolean isCanonicalTestDir() {
        File fHere = new File( "" );
        String pHere = fHere.getAbsolutePath();
        return pHere.endsWith( CANONICAL_TEST_DIR );
    }
    
    /**
     * Check if a (database) server is available.
     *
     * @param location
     *            A string of the form <em>hostname:port</em>, where
     *            <em>hostname</em> is the name of the host where the server
     *            is running, and <em>port</em> is the port on which we expect
     *            the server to be; e.g. <code>almadev1.hq.eso.org:1521</code>
     *
     * @return <code>true</code> if the server could be successfully reached,
     *         <code>false</code> otherwise
     *
     * @throws IllegalArgumentException
     *             If the input string could not be parsed.
     */
    public static boolean isServerAvalable( String location ) {

        String[] temp = location.split( ":" );
        if( temp.length != 2 )
            throw new IllegalArgumentException( location );
        String hostname = temp[0];
        int port = 0;
        try {
            port = Integer.parseInt( temp[1] );
        }
        catch( NumberFormatException e ) {
            throw new IllegalArgumentException( location );
        }
        return isServerAvalable( hostname, port );
    }

    /**
     * Check if a (database) server is available.
     *
     * @param hostname
     *            Name of the host where the server is running; e.g.
     *            <code>almadev1.hq.eso.org</code>
     * @param port
     *            Port on which we expect the server to be; e.g.
     *            <code>1521</code>
     *
     * @return <code>true</code> if the server could be successfully reached,
     *         <code>false</code> otherwise
     */
    public static boolean isServerAvalable( String hostname, int port ) {
        boolean ok = false;
        Socket s = null;

        try {
            s = new Socket( hostname, port );
            InputStream is = s.getInputStream();
            ok = true;
            is.close();
        }
        catch( UnknownHostException e ) {
            // ignore
        }
        catch( IOException e ) {
            // ignore
        }
        finally {
            try {
                if( s != null )
                    s.close();
            }
            catch( IOException e ) {
                // ignore
            }
        }

        return ok;
    }
    /**
     * @return A new, hopefully unique, Archive UID like
     *         <code>uid://X22/X1d/X7a</code>
     */
    public static String makeNewArchiveUid() {
        if( id0 < 0 ) {
            // need to initialize our random components
            Random r = new Random();
            id0 = r.nextInt( 1000 ) + 1;    // no zeros allowed
            id1 = r.nextInt( 1000 ) + 1;    // no zeros allowed
        }
        StringBuilder sb = new StringBuilder();
        
        sb.append( "uid://X" )
          .append( Integer.toHexString( id0 ))
          .append( "/X" )
          .append( Integer.toHexString( id1 ))
          .append( "/X" )
          .append( Integer.toHexString( ++id2 ));
           
        return sb.toString();
    }
    /** 
     * @return A new, random string ID starting with 0 (zero) and the given name 
     */
    public static String newID( String name ) {
        return "0-" + name + "-" + UUID.randomUUID().toString();
    }

    /**
     * Parse an XML document.
     * @param text  A textual representation of the XML document.
     * @return The root element of the XML document.
     * @throws IOException
     * @throws JDOMException
     */
    public static Element parse( String text ) throws JDOMException, IOException {
        StringReader reader = new StringReader( text );
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build( reader );
        return doc.getRootElement();
    }
}
