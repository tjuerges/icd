package alma.obops.utils;

/** 
 * We wrap ACS exceptions here, to make the API of AcsUtils simpler 
 *
 * @author amchavan, May 12, 2010
 * @version $Revision$
 */

// $Id$

public class WrappedAcsException extends Exception {

    private static final long serialVersionUID = 5614801583468278548L;

    public WrappedAcsException() {
        super();
    }

    public WrappedAcsException( String message ) {
        super( message );
    }

    public WrappedAcsException( String message, Throwable cause ) {
        super( message, cause );
    }

    public WrappedAcsException( Throwable cause ) {
        super( cause );
    }
}