/**
 * PdfUtils.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * A collection of utilities for creating and manipulating PDF files.
 * 
 * Most methods throw wrapped exceptions to avoid clients having to import iText jarfiles.
 * 
 * @author amchavan, Mar 12, 2010
 * @version $Revision$
 */

// $Id$

public class PdfUtils {

	static final String IS_SECURED = 
		"is secured and requires the owner password for further processing";

	/**
	 * Combine multiple PDF files into one
	 * 
	 * @param out
	 *            A stream opened on the output file
	 * @param files
	 *            A list of input files; each <code>byte[]</code> should contain
	 *            a complete PDF file.
	 * 
	 * @throws WrappedDocumentException
	 * @throws RuntimeException
	 *             If any one of the input files is secured and requires the
	 *             owner password for further processing; see
	 *             http://1t3xt.info/tutorials/faq.php?branch=faq.itext&node=password
	 *             
	 * @throws IOException
	 */
    public static void combine( OutputStream out, List<byte[]> files )
            throws WrappedDocumentException, IOException {

        if( out == null || files == null ) {
            throw new IllegalArgumentException( "Null arg(s)" );
        }
        Document document = new Document();

        try {
            PdfCopy copy = new PdfCopy( document, out );
            document.open();
            for( byte[] file : files ) {
                PdfReader reader = new PdfReader( file );
                boolean fullPermissions = reader.isOpenedWithFullPermissions();
                if( ! fullPermissions ) {
                	throw new RuntimeException( "File " + IS_SECURED );
                }
                int pageNum = reader.getNumberOfPages();
                for( int j = 1; j <= pageNum; j++ ) {
                    copy.addPage( copy.getImportedPage( reader, j ) );
                }
            }
        }
        catch( Exception e ) {
            out.close();
            throw new WrappedDocumentException( e );
        }
        document.close();
    }

    /**
     * Combine multiple PDF files into one
     * 
     * @param out
     *            Pathname of the output file 
     * @param files
     *            A list of input files; each <code>byte[]</code> should contain
     *            a complete PDF file.
     *            
     * @throws WrappedDocumentException
	 * @throws RuntimeException
	 *             If any one of the input files is secured and requires the
	 *             owner password for further processing; see
	 *             http://1t3xt.info/tutorials/faq.php?branch=faq.itext&node=password
     * @throws IOException
     */
    public static void combine( String dest, List<byte[]> files )
            throws IOException, WrappedDocumentException {

        if( dest == null || files == null ) {
            throw new IllegalArgumentException( "Null arg(s)" );
        }
        File f = new File( dest );
        OutputStream out = new FileOutputStream( f );
        combine( out, files );
        out.close();
    }

    /**
     * Combine multiple PDF files into one
     * 
     * @param out
     *            A stream opened on the output file
     * @param files
     *            A list of input file pathnames; each <code>String</code>
     *            should be the pathname of a PDF file.
     *            
     * @throws WrappedDocumentException
	 * @throws RuntimeException
	 *             If any one of the input files is secured and requires the
	 *             owner password for further processing; see
	 *             http://1t3xt.info/tutorials/faq.php?branch=faq.itext&node=password
	 *             
     * @throws IOException
     */
    public static void combineByName( OutputStream out, List<String> files )
            throws WrappedDocumentException, IOException {

        if( out == null || files == null ) {
            throw new IllegalArgumentException( "Null arg(s)" );
        }
        Document document = new Document();

        try {
            PdfCopy copy = new PdfCopy( document, out );
            document.open();
            for( String file : files ) {
                PdfReader reader = new PdfReader( file );
                boolean fullPermissions = reader.isOpenedWithFullPermissions();
                if( ! fullPermissions ) {
					throw new RuntimeException( "File '" + file + "' " + IS_SECURED );
                }
                int pageNum = reader.getNumberOfPages();
                for( int j = 1; j <= pageNum; j++ ) {
                    copy.addPage( copy.getImportedPage( reader, j ) );
                }
            }
        }
        catch( Exception e ) {
            throw new WrappedDocumentException( e );
        }
        document.close();
    }

    /**
     * Combine multiple PDF files into one
     * 
     * @param out
     *            Pathname of the output file 
     * @param files
     *            A list of input file pathnames; each <code>String</code>
     *            should be the pathname of a PDF file.
     *            
     * @throws WrappedDocumentException
	 * @throws RuntimeException
	 *             If any one of the input files is secured and requires the
	 *             owner password for further processing; see
	 *             http://1t3xt.info/tutorials/faq.php?branch=faq.itext&node=password
     * @throws IOException
     */
    public static void combineByName( String dest, List<String> files )
            throws IOException, WrappedDocumentException {

        if( dest == null || files == null ) {
            throw new IllegalArgumentException( "Null arg(s)" );
        }
        File f = new File( dest );
        OutputStream out = new FileOutputStream( f );
        combineByName( out, files );
        out.close();
    }
    
    /**
     * Create a PDF document containing one paragraph of text.
     * @param text The text (what else?)
     * @return An input stream opened on the document
     * @throws WrappedDocumentException
     */
    public static InputStream create( String text ) throws WrappedDocumentException {
        // creation of the document with a certain size and certain margins
        Document document = new Document( PageSize.A4, 50, 50, 50, 50 );
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance( document, out );
            document.addAuthor( PdfUtils.class.getName() );
            document.open();
            document.add( new Paragraph( text ));
            document.close();
        }
        catch( DocumentException e ) {
            throw new WrappedDocumentException( e );
        }

        return new ByteArrayInputStream( out.toByteArray() );
    }

	/**
	 * Return the ordered list of pdfs according to the ordering criterion
	 * @param pdfs The unordered pdfs, mapped to the pdfs names
	 * @param ordering The ordered list of requested pdfs names
	 * @return The requested pdfs, ordered in a list.
	 */
	public static List<byte[]> order( Map<String, byte[]> pdfs, List<String> ordering ) {
		
		List<byte[]> ret = new ArrayList<byte[]>();
		
		for (String criterion : ordering) {
			ret.add(pdfs.get(criterion));
		}

		return ret;
	}
}
