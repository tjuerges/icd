/**
 * ResultTable.java
 *
 * Copyright European Southern Observatory 2011
 */

package alma.obops.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple table representation of JDBC result set
 *
 * @author amchavan, May 2, 2011
 * @version $Revision$
 */

// $Id$

public class ResultTable {
    
    /** Where we hold our data */
    protected List<List<String>> table;
    
    /** List of column names */
    protected String[] columns;
    
    public ResultTable() {
        this.table =  new ArrayList<List<String>>();
    }

    /**
     * Creates an instance and populates it with the input {@link ResultSet};
     * the result set is then <em>closed</em>. <br/>
     * Notice that the entire set is read in, regardless of its size.
     * 
     * @throws SQLException
     */
    public ResultTable( ResultSet rs ) throws SQLException {
        this();
        load( rs );
    }

    /**
     * @return The table cell at the input row and column; indexes start at zero
     */
    public String get( int row, int col ) {
        return table.get( row ).get( col );
    }
    
    /**
     * @return The table cell at the input row number and column name; row
     *         indexes start at zero
     */
    public String get( int row, String colName ) {
        colName = colName.toUpperCase();
        int col = -1;
        for( int i = 0; i < columns.length; i++ ) {
            if( columns[i].equals( colName )) {
                col = i;
                break;
            }
        }
        if( col < 0 ) {
            throw new RuntimeException( "Column not found: '" + colName + "'" );
        }
        return get( row, col );
    }

    /** @return The number of rows in this table */
    public int getColumnCount() {
        return columns.length;
        
    }
    
    /** @return The number of rows in this table */
    public int getRowCount() {
        return table.size();
        
    }
    
    /**
     * Load the input {@link ResultSet}; the result set is then <em>closed</em>.<br/>
     * Notice that the entire set is read in, regardless of its size.
     * 
     * @throws SQLException
     */
    public void load( ResultSet resultSet ) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        
        columns = new String[columnCount];
        for( int i = 0; i < columnCount; i++ ) {
            columns[i] = metaData.getColumnName( i+1 ).toUpperCase();
        }
        
        table.clear();
        
        while( resultSet.next() ) {
            List<String> row = new ArrayList<String>( columnCount );
            for( int i = 0; i < columnCount; i++ ) {
                row.add( i, resultSet.getString( i+1 ));
            }
            table.add( row );
        }
        resultSet.close();
    }
}
