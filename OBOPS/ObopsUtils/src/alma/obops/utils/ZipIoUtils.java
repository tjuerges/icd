package alma.obops.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


/**
 * 
 * Class FileIoUtils
 * 
 * Static helper class, used to manipulate files.
 * 
 * @version $Revision$
 * @author amchavan, 10-Sep-2007, from VLT code
 * 
 */

// $Id$

public class ZipIoUtils {

    private final static int DEFAULT_BUF_LEN = 64 * 1024;

    /**
     * This class is an extension of ZipEntry, that includes the actual data of
     * the Zip file entry.
     */
    public static class ZipNtry extends ZipEntry {

        /**
         * Return the content of a Zip file entry as an array of bytes.
         * 
         * @param zipfile
         *                The Zip file to read
         * @param entry
         *                The Zip file entry to read
         * 
         * @return A new array of bytes, representing the entire contents of the
         *         Zip entry.
         * 
         * @throws IOException
         *                 If something went wrong while reading the file.
         */
        public static byte[] getZipEntry( ZipFile zipfile, ZipEntry entry )
            throws IOException {

            // Note: when dealing with ZipFiles, method ZipEntry.getSize()
            // returns meaningful values
            // ------------------------------------------------------------
            final InputStream s = zipfile.getInputStream( entry );
            final byte[] data = FileIoUtils.readEntry( s );
            return data;
        }
        
        private byte data[];
        
        public ZipNtry( ZipEntry e, byte data[] ) {
            super(e);
            this.data = data;
        }

        public byte[] getData() {
            return data;
        }
    }
    
    /**
     * Read all entries from a zip file
     * @param zipfile  The zip file to read
     * @return A list of all the entries read from the input zip file.
     *         The list may be empty but it's never <code>null</code>.
     * @throws IOException 
     * @throws ZipException 
     */
    public static List<ZipNtry> getZipEntries( File zipfile ) 
        throws ZipException, IOException {
        return getZipEntries( new ZipFile( zipfile ));
    }

    /**
     * Read all entries from a zip file
     * @param zipfile  Pathname of the zip file to read
     * @return A list of all the entries read from the input zip file.
     *         The list may be empty but it's never <code>null</code>.
     * @throws IOException 
     * @throws ZipException 
     */
    public static List<ZipNtry> getZipEntries( String zipfile ) 
        throws ZipException, IOException {
        return getZipEntries( new File( zipfile ));
    }

    /**
     * Read all entries from a zip file
     * @param zipURL  The URL of the zip file to read
     * @return A list of all the entries read from the input zip file.
     *         The list may be empty but it's never <code>null</code>.
     * @throws IOException 
     * @throws ZipException 
     */
    public static List<ZipNtry> getZipEntries( URL zipURL ) 
        throws ZipException, IOException {
        return getZipEntries( zipURL.getFile() );
    }

    /**
     * Read all entries from a zip file
     * @param zipfile  The zip file to read
     * @return A list of all the entries read from the input zip file.
     *         The list may be empty but it's never <code>null</code>.
     * @throws IOException 
     */
    public static List<ZipNtry> getZipEntries( ZipFile zipfile )
        throws IOException {

        List<ZipNtry> ret = new ArrayList<ZipNtry>();

        Enumeration<? extends ZipEntry> entries = zipfile.entries();
        while( entries.hasMoreElements() ) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            InputStream stream = zipfile.getInputStream( entry );
            byte[] data = FileIoUtils.readEntry( stream );
            ZipNtry ntry = new ZipNtry( entry, data );
            ret.add( ntry );
        }

        zipfile.close();
        
        return ret;
    }
  
    /**
     * Create a ZIP file from a list of files
     * 
     * @param sourceFiles
     *                ThE files to zip up into a file
     * @param zipFile
     *                The ZIP file to create
     * @throws IOException
     */
    public static void zipFiles( File[] sourceFiles, File zipFile )
        throws IOException {

        zipFiles( sourceFiles, null, zipFile );
    }

    /**
     * Create a ZIP file from a list of input streams
     * 
     * @param sourceFiles
     *                The map of input streams
     *                
     * @param entryName
     *                If not null, entries in the resulting zip file will be
     *                prefixed by this string; if <code>null</code> they will
     *                be prefixed with their relative pathname
     * @param zipFile
     *                The ZIP file to create
     * @throws IOException
     */
    public static void zipFiles( Map<String, InputStream> sourceFiles,
                                 String entryName,
                                 File zipFile ) throws IOException {

        int read = 0;
        InputStream in;
        byte[] data = new byte[DEFAULT_BUF_LEN];

        ZipOutputStream out = 
            new ZipOutputStream( new FileOutputStream( zipFile ) );
        
        // Define the archive mode
        out.setMethod( ZipOutputStream.DEFLATED );
        // Compress files one by one.
        for (Map.Entry<String, InputStream> mEntry : sourceFiles.entrySet()){
            // Define a ZIP entry for each file.
            ZipEntry entry;
            if( entryName == null ) {
                entry = new ZipEntry( mEntry.getKey() );
            }
            else {
                entry = new ZipEntry( entryName + File.separator
                        + mEntry.getKey() );
            }
            in = mEntry.getValue();
            out.putNextEntry( entry );
            // Write file
            while( (read = in.read( data, 0, DEFAULT_BUF_LEN )) != -1 ) {
                out.write( data, 0, read );
            }
            out.closeEntry();
            in.close();
        }
        out.close();
    }
    
    /**
     * Create a ZIP file from a list of files
     * 
     * @param sourceFiles
     *                The files to zip up into a file
     * @param entryName
     *                If not null, entries in the resulting zip file will be
     *                prefixed by this string; if <code>null</code> they will
     *                be prefixed with their relative pathname
     * @param zipFile
     *                The ZIP file to create
     * @throws IOException
     */
    public static void zipFiles( File[] sourceFiles, 
                                 String entryName,
                                 File zipFile ) throws IOException {

        int read = 0;
        FileInputStream in;
        byte[] data = new byte[DEFAULT_BUF_LEN];

        ZipOutputStream out = 
            new ZipOutputStream( new FileOutputStream( zipFile ) );
        
        // Define the archive mode
        out.setMethod( ZipOutputStream.DEFLATED );
        // Compress files one by one.
        for( int i = 0; i < sourceFiles.length; i++ ) {

            // Define a ZIP entry for each file.
            ZipEntry entry;
            if( entryName == null ) {
                entry = new ZipEntry( sourceFiles[i].getPath() );
            }
            else {
                entry = new ZipEntry( entryName + File.separator
                        + sourceFiles[i].getName() );
            }
            in = new FileInputStream( sourceFiles[i] );
            out.putNextEntry( entry );
            // Write file
            while( (read = in.read( data, 0, DEFAULT_BUF_LEN )) != -1 ) {
                out.write( data, 0, read );
            }
            out.closeEntry();
            in.close();
        }
        out.close();
    }
    
    public static void appendToZip( File sourceFile, String entryName, File zipFile) 
    	throws IOException {
    	
        int read = 0;
        byte[] data = new byte[DEFAULT_BUF_LEN];
        
     // get a temp file
		File tempFile = File.createTempFile(zipFile.getName(), null);

		// get the old entries
		List<ZipNtry> entries = getZipEntries(zipFile);
		// and the new one
        ZipEntry newentry;
        if( entryName == null ) {
            newentry = new ZipEntry( sourceFile.getPath() );
        }
        else {
            newentry = new ZipEntry( entryName + File.separator
                    + sourceFile.getName() );
        }
        ZipOutputStream out = 
            new ZipOutputStream( new FileOutputStream( tempFile ) );
        // write the old entries
        for (ZipNtry entry: entries) {
 	 	    out.putNextEntry( entry );
  		    ByteArrayInputStream in = new ByteArrayInputStream(entry.getData());
        	while( (read = in.read( data, 0, DEFAULT_BUF_LEN )) != -1 ) {
            	out.write( data, 0, read );
        	}
        	out.closeEntry();
            in.close();
        }
        // and append the new one
 	    out.putNextEntry( newentry );
        FileInputStream in = new FileInputStream( sourceFile );
        while ( (read = in.read(data, 0, DEFAULT_BUF_LEN)) != -1 ) {
        	out.write(data, 0, read);
        }
        out.closeEntry();
        in.close();
        out.close();
        
        // inefficient, but safer than renameTo for some cases
        FileIoUtils.copyFile(tempFile, zipFile);
		tempFile.delete();
		
    }
    
}
