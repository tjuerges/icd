package alma.obops.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URL;


/**
 * 
 * Class FileIoUtils
 * 
 * Static helper class, used to manipulate files.
 * 
 * @version $Revision$
 * @author amchavan, 10-Sep-2007, from VLT code
 * 
 */

// $Id$

public class FileIoUtils {

	private static final String ALMA_OBOPS_CACHE = "almaObopsCache";
	private final static String OBOPS_TMP_FILE_PREFIX = "obops";

    private final static int DEFAULT_BUF_LEN = 64 * 1024;
    
    static boolean overwriteFlag_ = false;


    /**
     * Copy an array of bytes; copying to destination may be offset. 
     */
    private static void copyBytes( byte[] from, byte[] to,
                                   int num, int toOffset ) {
        for( int i = 0; i < num; i++ ) {
            to[i + toOffset] = from[i];
        }
    }
    
    /**
     * Copy the files under source_directory to dest_directory. Sub-directories
     * are effectively ignored; existing files are overwritten.
     * 
     * @param source_directory
     * @param dest_directory
     * @throws IOException
     */
    public static void copyDir( String source_directory, String dest_directory )
        throws IOException {

        // Create a list of files...
        File source_dir = new File( source_directory );
        String[] source_files = source_dir.list();

        // ..and copy them.
        for( int i = 0; i < source_files.length; i++ ) {
            String sourceFile = source_directory + File.separator
                    + source_files[i];
            // Skip directories
            File f = new File( sourceFile );
            if( f.isDirectory() ) {
                continue;
            }
            String destinationFile = dest_directory + File.separator
                    + source_files[i];
            copyFile( sourceFile, destinationFile );
        }
    }
    
    
    /**
     * Copy a file.
     * 
     * @param source_file
     *                Source file
     * @param destination_file
     *                Destination file
     * @throws IOException
     */
    public static void copyFile( File source_file, File destination_file )
        throws IOException {

        String source_name = source_file.getCanonicalPath();
        String dest_name = destination_file.getCanonicalPath();

        if( source_file.isDirectory() ) {
            String msg = "Source file " + source_name + " is a directory";
            throw new IOException( msg );
        }

        FileInputStream source = null;
        FileOutputStream dest = null;
        try {
            // First make sure the specified source file
            // exists, is a file, and is readable.
            if( !source_file.exists() || !source_file.isFile() )
                throw new IOException( "No such file: " + source_name );
            if( !source_file.canRead() )
                throw new IOException( "File not readable: "
                        + source_name );

            // If the destination exists, make sure it is a writable file.
            // If the destination doesn't exist, make sure the directory
            // exists and is writable.
            if( destination_file.exists() ) {
                if( !destination_file.isFile() ) {
                    throw new IOException( "Not a file: " + dest_name );
                }
                if( !destination_file.canWrite() ) {
                    throw new IOException( "Can't write: " + dest_name );
                }

                File parentdir = parent( destination_file );
                String parentpath = parentdir.getCanonicalPath();
                if( !parentdir.exists() )
                    throw new IOException( "Directory not found: "
                            + parentpath );
                if( !parentdir.canWrite() )
                    throw new IOException( "Can't write: " + parentpath );
            }

            // If we've gotten this far, then everything is okay; we can
            // copy the file.
            source = new FileInputStream( source_file );
            dest = new FileOutputStream( destination_file );
            byte[] buffer = new byte[DEFAULT_BUF_LEN];
            while( true ) {
                int bytes_read = source.read( buffer );
                if( bytes_read == -1 )
                    break;
                dest.write( buffer, 0, bytes_read );
            }
        }
        // No matter what happens, always close any streams we've
        finally {
            if( source != null ) {
                try {
                    source.close();
                }
                catch( IOException e ) {
                    ;
                }
            }
            if( dest != null ) {
                try {
                    dest.close();
                }
                catch( IOException e ) {
                    ;
                }
            }
        }
    }
    
    
    /**
     * Copy a file.
     * 
     * @param source_name
     *                Pathname of the source file
     * @param dest_name
     *                Pathname of the destination file
     * @throws IOException
     */
    public static void copyFile( String source_name, String dest_name )
        throws IOException {

        File source_file = new File( source_name );
        File destination_file = new File( dest_name );
        copyFile( source_file, destination_file );
    }
    
    /**
     * 
     * Deletes all files and subdirectories under dir. Returns true if all
     * deletions were successful. If a deletion fails, the method stops
     * attempting to delete and returns false.
     * 
     * @param dir Root directory
     * @return boolean
     */
    public static boolean deleteDir( File dir ) {
        if( dir.isDirectory() ) {
            String[] children = dir.list();
            for( int i = 0; i < children.length; i++ ) {
                boolean success = deleteDir( new File( dir, children[i] ) );
                if( !success ) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
    
    /**
     * Writes the input data to a file.
     * @param data Binary data do be written
     * @param file Pathname of the file
     * @throws IOException
     */
    public static void byteArrayToFile( byte[] data, String file ) 
        throws IOException {
        
        OutputStream out = 
            new BufferedOutputStream( new FileOutputStream( new File ( file )));
        out.write( data );
        out.close();
    }
    
    /**
     * Read the contents of a file.
     * @param file Pathname of the file
     * @return The contents of the file.
     * @throws IOException
     */
    public static byte[] fileToByteArray( File file ) 
        throws IOException {
        
        int l = (int) file.length();
        byte[] ret = new byte[ l ];
        InputStream is = new BufferedInputStream( new FileInputStream( file ));
        is.read( ret, 0, l );
        is.close();
        return ret;
    }
    
    /**
     * Read a file and return its contents as a String.
     * 
     * @param file
     *                File to read.
     * @throws IOException
     *                 If something went wrong.
     */
    public static String fileToString( File file ) throws IOException {
        Reader r = new FileReader( file );
        return fileToString( r );
    }

    /**
     * Read a file from an InputStream and return its contents as a String.
     * 
     * @throws IOException
     *                 If something went wrong.
     */
    public static String fileToString( InputStream stream ) throws IOException {
        Reader r = new InputStreamReader( stream );
        return fileToString( r );
    }

    /**
     * Read a file from a Reader and return its contents as a String.
     * 
     * @throws IOException
     *                 If something went wrong.
     */
    public static String fileToString( Reader reader ) throws IOException {

        BufferedReader br = new BufferedReader( reader );
        StringBuffer sb = new StringBuffer();
        char[] buff = new char[DEFAULT_BUF_LEN];
        while( br.ready() ) {
            int nread = br.read( buff, 0, buff.length );
            if( nread <= 0 ) {
                break;
            }
            sb.append( buff, 0, nread );
        }
        br.close();
        return sb.toString();
    }

    /**
     * Read a file and return its contents as a String.
     * 
     * @param filename
     *                Pathname of the file to read.
     * 
     * @throws IOException
     *                 If something went wrong.
     */
    public static String fileToString( String filename ) throws IOException {
        File filePath = new File( filename );
        Reader r = new FileReader( filePath );
        return fileToString( r );
    }

    /**
     * Read a file from a URL and return its contents as a String.
     * 
     * @throws IOException If something went wrong.
     */
    public static String fileToString( URL url ) throws IOException {

        InputStream is = url.openStream();
        BufferedReader br = new BufferedReader( new InputStreamReader( is ));
        return fileToString( br );
    }

    /**
     * Returns the canonical path of a file; in case of an exception the
     * absolute path is returned instead.
     * 
     * @param f
     *                Input file
     * @return path
     */
    public static String getPathname( File f ) {
        try {
            return f.getCanonicalPath();
        }
        catch( IOException e ) {
            return f.getAbsolutePath();
        }
    }

    /**
     * Find a resource and return a stream to read it. <BR>
     * The resource will be looked up first it the classpath: if it is not found
     * there, it will be be looked up in the file system.
     * 
     * @param pathname
     *                Pathname of the resource to find.
     * @return An InputStream for the resource, if it could be opened.
     * @throws IOException
     *                 If the stream could not be read.
     */
    public static InputStream getResourceStream( String pathname )
        throws IOException {

        InputStream s = null; // this is the stream we return

        // Look for the resource on the classpath,
        // using the best available class loader
        // ---------------------------------------
        ClassLoader cl = FileIoUtils.class.getClassLoader();
        URL url = cl != null ? cl.getResource( pathname ) : ClassLoader
                .getSystemResource( pathname );

        if( url != null ) {
            s = url.openStream();
            return s;
        }

        // Look for the resource on the file system
        // -----------------------------------------
        File f = new File( pathname );
        s = new FileInputStream( f );

        return s;
    }

    /**
     * @return <code>true</code> if the file called pathname1 exists and is
     *         newer (more recently created or modified) than the file called
     *         pathname2; <code>false</code> otherwise
     */
    public static boolean isNewer( String pathname1, String pathname2 ) {
        return isNewer( new File( pathname1 ), new File( pathname2 ));
    }

    /**
     * @return <code>true</code> if f1 exists and is newer (more recently
     *         created or modified) than f2; <code>false</code> otherwise
     */
    public static boolean isNewer( File f1, File f2 ) {
        if( f1.exists() && 
            f1.canRead() && 
            f1.lastModified() > f2.lastModified() ) {
            return true;
        }
        return false;
    }

    // File.getParent() can return null when the file is specified without
    // a directory or is in the root directory.
    // This method handles those cases.
    private static File parent( File f ) {
        String dirname = f.getParent();
        if( dirname == null ) {
            if( f.isAbsolute() )
                return new File( File.separator );
            else
                return new File( System.getProperty( "user.dir" ) );
        }
        return new File( dirname );
    }

    /**
     * Convert an input stream into an array of bytes. It uses an internal
     * buffer of {@link #DEFAULT_BUF_LEN} bytes.
     * 
     * @param in
     *                The input stream
     * @throws IOException
     */
    public static byte[] readBytes( InputStream in ) throws IOException {
        return readBytes( in, DEFAULT_BUF_LEN );

    }

    /**
     * Convert an input stream into an array of bytes
     * 
     * @param in
     *                The input stream
     * @param bufLen
     *                Length of the internal buffer to use.
     * 
     * @throws IOException
     */
    public static byte[] readBytes( InputStream in, int bufLen )
        throws IOException {
        
        byte[] out = new byte[0];
        // the length of a buffer can vary
        byte[] buf = new byte[bufLen];
        byte[] tmp = null;
        int len = 0;
        while( (len = in.read( buf, 0, bufLen )) != -1 ) {
            // extend array
            tmp = new byte[out.length + len];
            // copy data
            System.arraycopy( out, 0, tmp, 0, out.length );
            System.arraycopy( buf, 0, tmp, out.length, len );
            out = tmp;
            tmp = null;
        }
        return out;
    }

    /**
     * Read some data from an InputStream into a byte array.
     * 
     * @param stream - the input stream from which to read
     * @return the bytes read in.
     * @throws IOException
     */
    protected static byte[] readEntry( InputStream stream ) throws IOException {
        final int BLKSIZE = 10 * 1024;             // 10 kB
        final int BUFSIZE = 1 * ( 1024 * 1024 );   // 1 MB

        int nTotal = 0;
        byte[] smallBuf = new byte[BLKSIZE];
        byte[] bigBuf = new byte[BUFSIZE];

        // Read from the input stream into a buffer
        while( true ) {
            final int n = stream.read( smallBuf, 0, smallBuf.length );
            if( n < 0 ) {
                break;
            }
            try {
                copyBytes( smallBuf, bigBuf, n, nTotal );
            }
            catch( ArrayIndexOutOfBoundsException e ) {
                // We have exceded the size of the big buffer, so grow it.
                // I have chosen a pretty arbitrary factor of 2 by which to
                // grow it (which quietly assumes that bigBuf is at least
                // as long as smallBuf) - dclarke.
                byte[] newBigBuf = new byte[bigBuf.length * 2];
                copyBytes( bigBuf, newBigBuf, nTotal, 0 );
                copyBytes( smallBuf, newBigBuf, n, nTotal );
                bigBuf = newBigBuf;
            }
            nTotal += n;
        }

        // Copy the data we read to the array we return
        final byte[] data = new byte[nTotal];
        copyBytes( bigBuf, data, nTotal, 0 );

        // Done!
        return data;
    }

    /**
     * Read the contents of a text file from a reader.
     * @param reader  Name of the reader to read.
     * @return  The contents of the file, as a single String.
     * @throws IOException
     */
    public static String readerToString( Reader reader ) 
        throws IOException {
    
        StringBuilder sb = new StringBuilder();
        BufferedReader breader = new BufferedReader( reader );
        while( true ) {
            String line = breader.readLine();
            if( line == null ) {
                break;
            }
            sb.append( line ).append( "\n" );
        }
        return sb.toString();
    }

    /** 
     * Read the contents of a text file from the classpath.
     * @param name  Name of the resource to read.
     * @return  The contents of the file, as a single String.
     * @throws IOException 
     */
    public static String resourceToString( String name ) 
        throws IOException {
        
        ClassLoader cloader = FileIoUtils.class.getClassLoader();
        InputStream stream = cloader.getResourceAsStream( name );
        if( stream == null ) {
            throw new IOException( "Resource " + name + " not found" );
        }
        Reader reader = new InputStreamReader( stream );
        return readerToString( reader );
    }

    /**
     * Write a string to a file.
     * 
     * @param s
     *                Input string
     * @param file
     *                Destination file
     * @throws IOException
     */
    public static void stringToFile( String s, File file ) throws IOException {

        BufferedWriter bw = new BufferedWriter( new FileWriter( file ) );
        bw.write( s, 0, s.length() );
        bw.close();

    }
    /**
     * Write a string to a file.
     * 
     * @param s
     *                Input string
     * @param filename
     *                Pathname of the destination file
     * @throws IOException
     */
    public static void stringToFile( String s, String filename )
        throws IOException {
        File f = new File( filename );
        stringToFile( s, f );
    }

    /**
     * Write a byte array to a stream and flush it.
     * 
     * @throws IOException
     */
    public static void writeBytes( byte[] byteArray, OutputStream stream )
        throws IOException {
        
        stream.write( byteArray );
        stream.flush();
    }

    public static File createTempFile() throws IOException 
	{
		return getTempFile(null);
	}

    /**
     * Returns an existing temporary file, with the specified name,
     * which is located under the per-JVM temporary directory.
     * If the file doesn't exist, the it creates it before
     * returning it.
     * 
     * 
     * @param name The name of the file to be retrieved. If null, then
     * a random name is generated, with the "obops-" prefix.
     * @return The temporary file
     * @throws IOException If any error occurs while retrieving the file
     */
    public static File getTempFile(String name) throws IOException 
	{
		File tempDirectory = createGeneralTmpDirIfDoesNotExist();	
		File appTempDirectory = createAppTempDirIfDoesNotExist(tempDirectory);
		File perJvmTempDirectory = createJvmDirIfDoesNotExist(appTempDirectory);
		File retVal = createTmpFileInTmpDir(perJvmTempDirectory, name);
		return retVal;
	}

	public static File createTmpFileInTmpDir(File jvmTempDirectory, String name)
			throws IOException 
	{

		// create the tmp file, in the per-jvm tmp directory
		File retVal = null;
		if( name == null )
			retVal = File.createTempFile(OBOPS_TMP_FILE_PREFIX, null, jvmTempDirectory);
		else
			retVal = new File(jvmTempDirectory, name);
		retVal.deleteOnExit();
		return retVal;
	}

	// synchronized will not be thread safe across multiple jvm instances
	// i.e. multiple copies of shiftlog tool running concurrently
	public static synchronized File createGeneralTmpDirIfDoesNotExist() 
	{
		// create the general tmp dir, if it doesn't already exist
		File tempDirectory = new File(System.getProperty("java.io.tmpdir"));
		if(!tempDirectory.exists()) {
			tempDirectory.mkdir();
			tempDirectory.setReadable(true, false);
			tempDirectory.setWritable(true, false);
			tempDirectory.setExecutable(true, false);
		}
		return tempDirectory;
	}

	// synchronized will not be thread safe across multiple jvm instances
	// i.e. multiple copies of shiftlog tool running concurrently
	public static synchronized File createAppTempDirIfDoesNotExist(File tempDirectory) {
		// create the alma obops tmp dir, if it doesn't already exist
		File appTempDirectory = new File(tempDirectory, ALMA_OBOPS_CACHE);
		if(!appTempDirectory.exists()) {
			appTempDirectory.mkdir();
			appTempDirectory.setReadable(true, false);
			appTempDirectory.setWritable(true, false);
			appTempDirectory.setExecutable(true, false);
		}
		return appTempDirectory;
	}

	// synchronized will not be thread safe across multiple jvm instances,
	// i.e. multiple copies of shiftlog tool running concurrently
	public static synchronized File createJvmDirIfDoesNotExist(File appTempDirectory) 
	{
		// create a unique tmp dir, for each jvm process, if it doesn't already exist
		ManagementFactory.getRuntimeMXBean();
		RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
		String uniqueJvmId = System.getProperty("user.name") + "-" + rt.getStartTime();
		File jvmTempDirectory = new File(appTempDirectory, uniqueJvmId);
		
		if(!jvmTempDirectory.exists()) {
			jvmTempDirectory.mkdir();
			jvmTempDirectory.setReadable(true, true);
			jvmTempDirectory.setWritable(true, true);
			jvmTempDirectory.setExecutable(true, true);
			jvmTempDirectory.deleteOnExit();
		}
		return jvmTempDirectory;
	}

    /**
     * Replace oldSuffix in the input pathname with newSuffix. If pathname does
     * not end with oldSuffix, newSuffix is simply concatenated at the end of
     * pathname.
     */
    public static String replaceSuffix( String pathname, 
                                        String oldSuffix, String newSuffix ) {
        String ret;
        int index = pathname.indexOf( oldSuffix );
        if( index >= 0 ) {
            ret = pathname.substring( 0, index );
        }
        else {
            ret = pathname;
        }
        ret += newSuffix;
        return ret;
    }

}