/*
 * Copyright European Southern Observatory 2006
 */

package alma.obops.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import alma.acs.logging.ClientLogManager;

/**
 * A collection of generally useful utility methods and constants.
 * 
 * @version $Revision$
 * @author amchavan, Sep 22, 2006
 */

// $Id$

public class GeneralUtils {
    
    /**
     * @param logger
     * @return
     * @throws SecurityException
     */
    public static Logger configureLoggerAsIso( Logger logger )
            throws SecurityException {
        logger.setUseParentHandlers( false );
        Handler handler = new ConsoleHandler();
        handler.setFormatter( new IsoDateLogFormatter()  );
        logger.addHandler( handler );
        return logger;
    }

    /**
     * Recursively delete a directory and all its contents.
     * 
     * @param fileOrDir The file or directory to delete.
     * @throws IOException 
     */
    public static void delete( File fileOrDir ) throws IOException {
        if( fileOrDir.exists() ) {
            if( fileOrDir.isDirectory() ) {
                String[] list = fileOrDir.list();
                for( int i = 0; i < list.length; i++ ) {
                    StringBuilder sb = new StringBuilder();
                    sb.append( fileOrDir.getCanonicalPath() )
                      .append( "/" )
                      .append( list[i] );
                    File f = new File( sb.toString() );
                    delete( f );    // recursion
                }
            }
            fileOrDir.delete();
        }
    }

    /**
     * Gets a named AcsLogger. It first checks in the JDK's LogManager if there
     * is such a logger; if not present, it creates a new AcsLogger. The
     * returned object is always treated as a Logger, though, to prevent errors
     * produced by the AcsLogger class being loaded by different ClassLoaders.
     * 
     * @param name
     *            The logger name
     * @return The named logger; if none exists in the LogManager, a new
     *         AcsLogger is created
     */
    public static Logger getAcsLogger(String name) {
    	Logger l = LogManager.getLogManager().getLogger(name);
    	if( l == null )
    		l = ClientLogManager.getAcsLogManager().getLoggerForApplication(name, false);
    	return l;
    }

    /**
     * @return A logger which outputs messages on a single line,
     *         prefixed by a date/time string in ISO format.
     */
    public static Logger getIsoLogger() {
        Logger logger = Logger.getAnonymousLogger();
        return configureLoggerAsIso( logger );
    }

    /**
     * @return A logger which outputs messages on a single line,
     *         prefixed by a date/time string in ISO format.
     */
    public static Logger getIsoLogger( String name ) {
        Logger logger = Logger.getLogger( name );
        return configureLoggerAsIso( logger );
    }

    /**
     * @return The stack trace of the input exception as a multi-line string,
     *         suitable for logging
     */
    public static String getStackTrace( Throwable e ) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter( sw );
        e.printStackTrace( pw );
        pw.flush();    // just in case, should not be needed
        return sw.toString();
    }

    /**
     * @return <code>true</code> if we are running on a system of the MS Windows
     *         family, <code>false</code> otherwise
     */
    public static boolean isWindows() {
        String os = System.getProperty( "os.name" );
        return os.toLowerCase().contains( "windows" );
    }

    /**
     * Read the contents of a text file from a reader.
     * @param reader  Name of the reader to read.
     * @return  The contents of the file, as a single String.
     * @throws IOException
     */
    public static String readerToString( Reader reader ) 
        throws IOException {
    
        StringBuilder sb = new StringBuilder();
        BufferedReader breader = new BufferedReader( reader );
        while( true ) {
            String line = breader.readLine();
            if( line == null ) {
                break;
            }
            sb.append( line ).append( "\n" );
        }
        return sb.toString();
    }

    /** 
     * Read the contents of a text file from the classpath.
     * @param name  Name of the resource to read.
     * @return  The contents of the file, as a single String.
     * @throws IOException 
     */
    public static String resourceToString( String name ) 
        throws IOException {
        
        ClassLoader cloader = GeneralUtils.class.getClassLoader();
        InputStream stream = cloader.getResourceAsStream( name );
        if( stream == null ) {
            throw new IOException( "Resource " + name + " not found" );
        }
        Reader reader = new InputStreamReader( stream );
        return readerToString( reader );
    }

    /** 
     * Configure the logging system used by third-party libraries to output
     * messages of a certain level or higher.
     * 
     * @param level  One of <code>FINEST</code>, <code>FINER</code> ... 
     *               <code>SEVERE</code>.
     */
    public static void setLogLevel( String level ) {
        final String line0 = "handlers=java.util.logging.ConsoleHandler\n";
        final String line1 = ".level=";
        StringBuilder sb = new StringBuilder();
        sb.append( line0 ).append( line1 ).append( level ).append( '\n' );
        byte[] bytes = sb.toString().getBytes();
        InputStream ins = new ByteArrayInputStream( bytes );
        try {
            LogManager.getLogManager().readConfiguration( ins );
        }
        catch( Exception e ) {
            // should never happen
        }
    }
}
