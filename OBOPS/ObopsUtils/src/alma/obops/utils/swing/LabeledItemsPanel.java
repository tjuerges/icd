/**
 * LabelledItemPanel.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.utils.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A JPanel using GridBagLayout with some utility methods.
 * 
 * @author amchavan, Oct 6, 2010; original code found at
 *         http://www.javaworld.com/javaworld/jw-10-2002/jw-1004-dialog.html?page=4
 * @version $Revision: 1.2 $
 */

// $Id: LabeledItemsPanel.java,v 1.2 2011/02/03 10:28:57 rkurowsk Exp $

public class LabeledItemsPanel extends JPanel {

	public enum VerticalAlignment {
		TOP,
		MIDDLE,
		BOTTOM
	};

	public enum HorizonalAlignment {
		LEFT,
		CENTER,
		RIGHT,
		JUSTIFIED
	}

    private static final long serialVersionUID = 3971161055671044002L;
    
    protected static final int RIGHT_PADDING = 10;
    protected static final int LEFT_PADDING = 10;
    protected static final int TOP_PADDING = 5;
    
    /** The row to add the next labeled item to */
    private int myNextItemRow = 0;

    public LabeledItemsPanel() {
        init();
    }

    private void init() {
        setLayout( new GridBagLayout() );

        // Create a blank label to use as a vertical fill so that the
        // label/item pairs are aligned to the top of the panel and are not
        // grouped in the centre if the parent component is taller than
        // the preferred size of the panel.

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 99;
        constraints.insets = new Insets( TOP_PADDING, 
                                         0, 
                                         0, 
                                         0 );
        constraints.weighty = 1.0;
        constraints.fill = GridBagConstraints.VERTICAL;

        JLabel verticalFillLabel = new JLabel();

        add( verticalFillLabel, constraints );
    }

    /**
     * Add a label/component pair
     */
    public void add( String labelText, JComponent item ) {
        add( new JLabel( labelText ), item );
    }

    /**
     * Add a label/component pair
     */
    public void add( JLabel label, JComponent item ) {
    	add(label, item, HorizonalAlignment.CENTER, VerticalAlignment.MIDDLE);
    }

    public void add(String labelText, JComponent item, HorizonalAlignment hAlign, VerticalAlignment vAlign) {
    	add(new JLabel(labelText), item, hAlign, vAlign);
    }

    public void add(JLabel label, JComponent item, HorizonalAlignment hAlign, VerticalAlignment vAlign) {
        GridBagConstraints labelConstraints = new GridBagConstraints();

        labelConstraints.gridx = 0;
        labelConstraints.gridy = myNextItemRow;
        labelConstraints.insets = new Insets( TOP_PADDING, 
                                              LEFT_PADDING, 
                                              0, 
                                              0 );

        if( hAlign.equals(HorizonalAlignment.RIGHT) ||
            hAlign.equals(HorizonalAlignment.CENTER) )
        	if( vAlign.equals(VerticalAlignment.TOP) )
        		labelConstraints.anchor = GridBagConstraints.NORTHEAST;
        	else if( vAlign.equals(VerticalAlignment.MIDDLE) )
        		labelConstraints.anchor = GridBagConstraints.EAST;
        	else if( vAlign.equals(VerticalAlignment.BOTTOM) )
        		labelConstraints.anchor = GridBagConstraints.SOUTHEAST;
        else {
        	if( vAlign.equals(VerticalAlignment.TOP) )
        		labelConstraints.anchor = GridBagConstraints.NORTHWEST;
        	else if( vAlign.equals(VerticalAlignment.MIDDLE) )
        		labelConstraints.anchor = GridBagConstraints.WEST;
        	else if( vAlign.equals(VerticalAlignment.BOTTOM) )
        		labelConstraints.anchor = GridBagConstraints.SOUTHWEST;
        }

        labelConstraints.fill = GridBagConstraints.NONE;

        add( label, labelConstraints );

        // Add the component with its constraints

        GridBagConstraints itemConstraints = new GridBagConstraints();

        itemConstraints.gridx = 1;
        itemConstraints.gridy = myNextItemRow;
        itemConstraints.insets = new Insets( TOP_PADDING, 
                                             LEFT_PADDING, 
                                             0, 
                                             RIGHT_PADDING );
        itemConstraints.weightx = 1.0;

        if( hAlign.equals(HorizonalAlignment.RIGHT) ||
            hAlign.equals(HorizonalAlignment.JUSTIFIED) ) {
        	if( vAlign.equals(VerticalAlignment.TOP) )
        		itemConstraints.anchor = GridBagConstraints.NORTHEAST;
        	else if( vAlign.equals(VerticalAlignment.MIDDLE) )
        		itemConstraints.anchor = GridBagConstraints.EAST;
        	else if( vAlign.equals(VerticalAlignment.BOTTOM) )
        		itemConstraints.anchor = GridBagConstraints.SOUTHEAST;
        }
        else {
        	if( vAlign.equals(VerticalAlignment.TOP) )
        		itemConstraints.anchor = GridBagConstraints.NORTHWEST;
        	else if( vAlign.equals(VerticalAlignment.MIDDLE) )
        		itemConstraints.anchor = GridBagConstraints.WEST;
        	else if( vAlign.equals(VerticalAlignment.BOTTOM) )
        		itemConstraints.anchor = GridBagConstraints.SOUTHWEST;
        }
        
        itemConstraints.fill = GridBagConstraints.HORIZONTAL;

        add( item, itemConstraints );

        myNextItemRow++;
    }
}