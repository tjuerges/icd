package alma.obops.utils;

import java.text.SimpleDateFormat;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/** 
 * A simple formatter for LogRecords that outputs everything on
 * one line, with an ISO format date string 
 * 
 * @version $Revision$
 * @author amchavan, 26-Jan-2007
 */

// $Id$

public class IsoDateLogFormatter extends SimpleFormatter {
    public static final String ISODATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    static SimpleDateFormat sdf = new SimpleDateFormat( ISODATEFORMAT );
    
    public String format( LogRecord logrecord ) {
        
        StringBuilder sb = new StringBuilder();
        sb.append( sdf.format( logrecord.getMillis() ))
          .append( " " )
          .append( logrecord.getLevel().getName() )
          .append( ": " )
          .append( logrecord.getMessage() )
          .append( "\n" );
        return sb.toString();
    }
}