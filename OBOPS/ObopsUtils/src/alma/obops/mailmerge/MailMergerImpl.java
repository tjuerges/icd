/**
 * MailMergerImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.mailmerge;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import alma.obops.utils.config.ObopsConfig;

/**
 * A non-public, basic implementation of the {@link MailMerger} interface.
 *
 * @author amchavan, Aug 16, 2010
 * @version $Revision: 1.6 $
 */

// $Id: MailMergerImpl.java,v 1.6 2011/04/15 15:04:51 fjulbe Exp $

class MailMergerImpl implements MailMerger {

    // FOR TESTING ONLY: convert the input string table to a list of maps
    static List<Map<String, String>> array2list( String[][] array ) {
        List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
        
        for( int i = 0; i < array.length; i++ ) {
            String[] stringMap = array[i];
            Map<String, String> map = new HashMap<String,String>();
            for( int j = 0; j < stringMap.length; j++ ) {
                String[] pair = stringMap[j].split( "=" );
                map.put( pair[0], pair[1] );
            }
            ret.add( map );
        }
        return ret;
    }
    String placeholderRegExp;
    List<String> placeholders;
    String startDelimiter;
    String endDelimiter;

    String emailKeyword;
    private String smtpServer;
    private String fromAddress;
    private String mailUser;
    private String mailPassword;

    /**
     * Constructor -- uses default values for the RE delimiters.
     * @throws IOException 
     * @throws FileNotFoundException 
     * 
     * @see MailMerger#DEFAULT_PLACEHOLDER_START_DELIMITER
     * @see MailMerger#DEFAULT_PLACEHOLDER_END_DELIMITER
     */
    public MailMergerImpl() throws FileNotFoundException, IOException {
        setDelimiters( DEFAULT_PLACEHOLDER_START_DELIMITER,
                       DEFAULT_PLACEHOLDER_END_DELIMITER );
        setEmailKeyword( DEFAULT_EMAIL_KEYWORD );
        
        // get email config properties from ObopsConfig
        this.smtpServer = ObopsConfig.getSmtpServer();
        this.fromAddress = ObopsConfig.getEmailFromAddress();
        this.mailUser = ObopsConfig.getEmailUser();
        this.mailPassword = ObopsConfig.getEmailPassword();
    }

    /**
     * @return The list of placeholders in the input template
     */
    private List<String> findPlaceholders( String template ) {
        Pattern p = Pattern.compile( placeholderRegExp );
        Matcher m = p.matcher(template);
        
        List<String> ret = new ArrayList<String>();
        
        while( m.find() ) {
            String placeholder = template.substring( m.start(1), m.end(1) );
            ret.add( placeholder );
        }
        return ret;
    }

    /**
     * @see alma.obops.mailmerge.MailMerger#mailMerge(java.lang.String, java.util.List, java.lang.String, java.lang.String)
     */
    @Override
    public void mailMerge( String template,
                           List<Map<String, String>> paramSets,
                           String recipientCC,
                           String recipientBCC,
                           String contentType )
            throws IllegalArgumentException, MessagingException {
        mailMerge( template, paramSets, recipientCC, recipientBCC, contentType, null );
    }

    /**
     * @see alma.obops.mailmerge.MailMerger#mailMerge(java.lang.String, java.util.List, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void mailMerge( String template,
                           List<Map<String, String>> paramSets,
                           String recipientCC,
                           String recipientBCC,
                           String contentType,
                           String overrideEmail )
            throws IllegalArgumentException, MessagingException {
        mailMergeInternal( template, 
                           paramSets, 
                           true, 
                           recipientCC, 
                           recipientBCC,
                           contentType, 
                           overrideEmail );
    }
    
    /**
     * Do all the actual merge and mail work. 
     * See {@link MailMerger#mailMerge(String,List,String,String,String)}
     * 
     * @param doMail
     *            If <code>true</code>, mail out each generated message;
     *            otherwise, just merge message-specific information.
     * 
     * @throws IllegalArgumentException
     * @throws MessagingException
     */
    private List<String> mailMergeInternal( String template,
                                            List<Map<String, String>> paramSets,
                                            boolean doMail,
                                            String recipientCC,
                                            String recipientBCC,
                                            String contentsType,
                                            String overrideAddress )
            throws IllegalArgumentException, MessagingException {
        
        // Check input args
        if( template == null ) {
            throw new IllegalArgumentException( "Null template" );
        }
        if( paramSets == null ) {
            throw new IllegalArgumentException( "Null parameter set" );
        }
        if( doMail ) {
            if( contentsType == null ) {
                throw new IllegalArgumentException( "Null contents type" );
                
            }
            if( !( contentsType.equals( TEXT_CONTENT_TYPE ) || 
                   contentsType.equals( HTML_CONTENT_TYPE ))) {
                String msg = "Invalid contents type: should be one of: '"
                        + TEXT_CONTENT_TYPE + "', '" + HTML_CONTENT_TYPE + "'";
                throw new IllegalArgumentException( msg );
            }
        }
        
        placeholders = findPlaceholders( template );
//        for( String placeholder : placeholders ) {
//            System.out.println( ">>> " + placeholder );
//        }
        
        int index = 0;
        List<String> ret = new ArrayList<String>();     // we return this
        for( Map<String,String> paramSet : paramSets ) {    // foreach parameter set
            String message = new String( template );        // create a new message
            for( String placeholder : placeholders ) {      // foreach placeholder
                String replacement = paramSet.get( placeholder );  // get its replacement value
                if( replacement == null ) {                 //  deal with missing replacements
                    replacement = "";
                }
                // now replace all occurrences of that placeholder with
                // the given replacement value
                placeholder = startDelimiter + placeholder + endDelimiter;
                message = message.replaceAll( placeholder, replacement );
            }
            
            ret.add( message );
            if( doMail ) {
                
                // check that we have an email address
                String emailAddress = paramSet.get( emailKeyword ); 
                if( emailAddress == null || emailAddress.length() == 0 ) {
                    String msg = 
                        "Missing or empty email address: index = " +
                        index +
                        ", email address keyword = '" +
                        emailKeyword +
                    	"'";
                    throw new IllegalArgumentException( msg );
                }
                
                // check if we have a subject line
                String subject = DEFAULT_SUBJECT;
                if( message.startsWith( SUBJECT_HEADER )) {
                    int n = message.indexOf( '\n' );
                    subject = message.substring( SUBJECT_HEADER.length(), n ).trim();
                    message = message.substring( n+1 );
                }
                
                // if so requested, override intended recipient
                String recipientTO = 
                    overrideAddress != null ? overrideAddress : emailAddress;
                
                // GO!
                sendmail( recipientTO, recipientCC, recipientBCC, subject, contentsType, message );
            }
            
            index++;
        }
        return ret;
    }

    /**
     * @see alma.obops.mailmerge.MailMerger#merge(java.lang.String, java.util.List)
     */
    @Override
    public List<String> merge( String template,
                               List<Map<String, String>> paramSets ) {
        try {
            return mailMergeInternal( template, paramSets, false, null, null, null, null );
        }
        catch( MessagingException e ) {
            // should never happen
            return null;
        }
    }
    
    /** Send out a message to a recipient */
    void sendmail( String recipientTO, 
                   String recipientCC, 
                   String recipientBCC,
                   String subject, 
                   String contentType, 
                   String contents )
        throws MessagingException {
    	
        boolean debug = false;

        // Setup of the mail service
        Properties props = new Properties();
        props.put( "mail.smtp.host", smtpServer );
        props.put( "mail.user", mailUser );
        props.put( "mail.password", mailPassword );

        // create some properties and get the default Session
        Session session = Session.getDefaultInstance( props, null );
        session.setDebug( debug );

        // create a message
        Message msg = new MimeMessage( session );

        // set the from and to address
        InternetAddress addressFrom = new InternetAddress( fromAddress );
        msg.setFrom( addressFrom );

        String[] recipients = recipientTO.replaceAll(" ", "").split(",");
        InternetAddress[] addressTo = new InternetAddress[recipients.length];
        for (int i = 0; i<recipients.length; i++) {
        	addressTo[i] = new InternetAddress( recipients[i] );
        }
        msg.setRecipients( Message.RecipientType.TO, addressTo );
  
        if( recipientCC != null ) {
            String[] recipientsCC = recipientCC.replaceAll(" ", "").split(",");       	
            InternetAddress[] addressCc = new InternetAddress[recipientsCC.length];
            for (int i = 0; i<recipientsCC.length; i++) {
            	addressCc[i] = new InternetAddress( recipientsCC[i] );
            }  
            msg.setRecipients( Message.RecipientType.CC, addressCc );
        }

        if( recipientBCC != null ) {
            String[] recipientsBCC = recipientBCC.replaceAll(" ", "").split(",");       	
            InternetAddress[] addressBcc = new InternetAddress[recipientsBCC.length];
            for (int i = 0; i < recipientsBCC.length; i++) {
            	addressBcc[i] = new InternetAddress( recipientsBCC[i] );
            }  
            msg.setRecipients( Message.RecipientType.BCC, addressBcc );
        }
        
//        msg.addHeader("Reply-to", "obops@eso.org");

        // Setting the Subject and Content Type
        msg.setSubject( subject );
        msg.setContent( contents, contentType );
        
//        System.out.println(">>>>>>>>>> email content start >>>>>>> ");
//        System.out.println(contents);
//        System.out.println(">>>>>>>>>> email content end >>>>>>>> ");
        
//        System.out.println(">>> sending '" + subject + "' email to: " + recipientTO 
//        		+ " cc: " + recipientCC + " bcc: " + recipientBCC);
        
        try{
        	Transport.send( msg );
        }catch(MessagingException me){
        	
        	// just in case something higher up is swallowing the exception
        	me.printStackTrace();
        	throw me;
        }
        
    }

    /**
     * @see alma.obops.mailmerge.MailMerger#setDelimiters(java.lang.String, java.lang.String)
     */
    @Override
    public void setDelimiters( String start, String end ) {
        this.startDelimiter = start;
        this.endDelimiter = end;
        this.placeholderRegExp = 
            start + "(" + MERGE_PLACEHOLDER_RE + ")" + end;
    }

    /**
     * @see alma.obops.mailmerge.MailMerger#setEmailKeyword(java.lang.String)
     */
    @Override
    public void setEmailKeyword( String emailKeyword ) {
        this.emailKeyword = emailKeyword;
    }

}
