/**
 * MailMerge.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.obops.mailmerge;

import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

/**
 * A simple utility interface for producing and sending out personalized email
 * messages. A message is produced by merging user-specific information into a
 * standard template text.
 * <p/>
 * 
 * Placeholders in the template text are denoted by <em>PLACEHOLDER_START</em>+
 * <em>PLACEHOLDER</em>+<em>PLACEHOLDER_END</em>, where <em>PLACEHOLDER</em> is
 * any string matching the {@link #PLACEHOLDER_RE} regular expression; and
 * <em>PLACEHOLDER_START</em> and <em>PLACEHOLDER_END</em> default to
 * {@link #DEFAULT_PLACEHOLDER_START_DELIMITER} and
 * {@link #DEFAULT_PLACEHOLDER_END_DELIMITER}, respectively. They can be reset
 * with {@link #setDelimiters(String, String)}. <br/>
 * Valid placeholders in the template text are for instance
 * <code>%LastName%</code>, <code>&lt;&lt;PROJECT_CODE&gt;&gt;</code> and
 * <code>#last-submission-date#</code>.
 * <p/>
 * 
 * Placeholders in the user-specific parameter map are denoted by
 * <em>PLACEHOLDER</em> alone; that is, without delimiters. <br/>
 * Valid placeholders in the parameter map are for instance
 * <code>LastName</code>, <code>PROJECT_CODE;</code> and
 * <code>last-submission-date</code>.
 * <p/>
 * 
 * For instance, let's assume the template text is:<br/>
 * <table>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Dear Mr. %name%, room %room% was reserved for you from %start% to %end%.
 * </em></td>
 * </tr>
 * </table>
 * and the input map includes:
 * <table>
 * <tr>
 * <td>name=Jones</td>
 * <td>room=103</td>
 * <td>start=June 1st, 2011</td>
 * <td>end=July 1st, 2011</td>
 * <tr>
 * <td>name=Smith</td>
 * <td>room=462</td>
 * <td>start=April 15, 2011</td>
 * <td></td>
 * </table>
 * (Notice that a value for <em>end</em> is missing from the second map.) The
 * resulting messages will be:
 * <table>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Dear Mr. Jones, room 103 was reserved for you from June 1st, 2011 to July 1st, 2011.<br/>
 * </em></td>
 * </tr>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Dear Mr. Smith, room 462 was reserved for you from April 15, 2011 to .<br/>
 * </em></td>
 * </tr>
 * </table>
 * <p/>
 * 
 * When mailing a merged message, the first line of the template takes on a
 * special meaning. After merging the message-specific information, if that line
 * starts with the string {@link #SUBJECT_HEADER}, then that line will removed
 * from the generated message and anything following {@link #SUBJECT_HEADER}
 * will become the subject of the generated message. <br/>
 * 
 * For instance, if we have the same parameter map but the template text is:<br/>
 * <table>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Subject: Hello Mr %name%!<br/>
 * Dear Mr. %name%, room %room% was reserved for you from %start% to %end%.
 * </em></td>
 * </tr>
 * </table>
 * two email messages will be generated, with subject lines
 * <table>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>Hello Mr Jones!</em></td>
 * </tr>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>Hello Mr Smith!</em></td>
 * </tr>
 * </table>
 * and body text
 * <table>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Dear Mr. Jones, room 103 was reserved for you from June 1st, 2011 to July 1st, 2011.<br/>
 * </em></td>
 * </tr>
 * <tr>
 * <td>&nbsp;&nbsp;&nbsp;</td>
 * <td><em>
 * Dear Mr. Smith, room 462 was reserved for you from April 15, 2011 to .<br/>
 * </tr>
 * </table><p/>
 * 
 * The placeholder delimiters are by default
 * {@link #DEFAULT_PLACEHOLDER_START_DELIMITER} and
 * {@link #DEFAULT_PLACEHOLDER_END_DELIMITER}; they can be changed by calling
 * {@link #setDelimiters()}<p/>
 * 
 * Example usage:
 * <pre>
        MailMerger mm = MailMergerFactory.getMailMerger();        
        String template = 
            "Subject: [unit test output] Hello %name%\n" +
            "This is an <b>HTML</b> <em>message</em> for %name%";
        
        Map<String,String> map = new HashMap<String,String>();
        map.put( "name", "Maurizio" );
        map.put( "EMAIL", "amchavan@eso.org" );
        
        List<Map<String,String>> params = new ArrayList<Map<String,String>>();
        params.add( map );
        
        //            template, paramSets, recipientCC, contentType
        mm.mailMerge( template, params,    null,        MailMerger.HTML_CONTENT_TYPE );
 * </pre> 
 * 
 * 
 * @author amchavan, Jul 19, 2010
 * @version $Revision: 1.4 $
 */

// $Id: MailMerger.java,v 1.4 2011/04/15 15:04:51 fjulbe Exp $

public interface MailMerger {

    /**
     * The regular expression denoting a placeholder: any sequence of upper- and
     * lowercase letters, numbers, dash "-" and underscore "_", but no
     * whitespace
     */
    public static final String MERGE_PLACEHOLDER_RE = "[-_\\w]+";
    
    /** The default string marking the beginning of a placeholder */
    public static final String DEFAULT_PLACEHOLDER_START_DELIMITER = "%";

    /** The default string marking the end of a placeholder */
    public static final String DEFAULT_PLACEHOLDER_END_DELIMITER = "%";

    /** The default string denoting an email address parameter */
    public static final String DEFAULT_EMAIL_KEYWORD = "EMAIL";
    
    /**
     * After merging the message-specific information, if the first message line
     * starts with the string this string, then that line will removed from the
     * generated message and anything following this string will become the
     * subject of the generated message.
     */
    public static final String SUBJECT_HEADER = "Subject:";

    /** Default subject, if none is provided */
    public static final String DEFAULT_SUBJECT = "(no subject)";

    /** Content type, plain text messages */
    public static final String TEXT_CONTENT_TYPE = "text/plain";
    
    /** Content type, HTML messages */
    public static final String HTML_CONTENT_TYPE = "text/html";

    /**
     * Produce a list of messaged from a template and a list of parameter sets
     * defining message-specific information.
     * 
     * @param template
     *            A template text; it may include one or more placeholders.
     *            After merging the message-specific information, if the first
     *            line of the message starts with the string
     *            <code>Subject:</code>, then that line will removed from the
     *            generated message and anything following <code>Subject:</code>
     *            will become the subject of the generated message.
     *            See {@link #SUBJECT_HEADER}.
     * 
     * @param paramSets
     *            A list of parameter sets; each set is implemented as a map of
     *            &lt;placeholder,value&gt; pairs. For each placeholder found in
     *            the template, if a value for that placeholder is found in the
     *            map, that value is substituted in the resulting text. <br/>
     *            If no value is found for a placeholder, an empty string will
     *            be substituted instead.
     * 
     * @return A list of messages, each resulting from the merging of a
     *         parameter set with the template. The list may be empty but is
     *         never <code>null</code>.
     * 
     * @throws IllegalArgumentException
     *             If any of the input args is <code>null</code>
     */
    public List<String> merge( String template, 
                               List<Map<String,String>> paramSets );

    /**
     * Set the placeholder delimiter strings; the change applies to all
     * subsequent merges. Delimiters should be 'simple' strings; that is, they
     * should include none of the special characters used to define a regular
     * expression, like <code>[</code>, <code>*</code>, etc. <br/>
     * 
     * See java.util.regex.Pattern for more info.
     * 
     * @param start   The new placeholder start delimiter.
     * @param end     The new placeholder end delimiter.
     * 
     * @see #DEFAULT_PLACEHOLDER_START_DELIMITER
     * @see #DEFAULT_PLACEHOLDER_END_DELIMITER
     */
    public void setDelimiters( String start, String end );


    /**
     * Set the email address keyword.
     * 
     * @param emailKeyword   The new email address keyword.
     * 
     * @see #DEFAULT_EMAIL_KEYWORD
     */
    public void setEmailKeyword( String emailKeyword );

    /**
     * Mail out a list of messaged constructed from a template and a list of
     * parameter sets defining message-specific information.
     * 
     * @param template
     *            A template text; it may include one or more placeholders.<br/>
     *            After merging the message-specific information, if the first
     *            line of the message starts with the string
     *            <code>Subject:</code>, then that line will removed from the
     *            generated message and anything following <code>Subject:</code>
     *            will become the subject of the generated message.
     *            See {@link #SUBJECT_HEADER}.
     * 
     * @param paramSets
     *            A list of parameter sets; each set is implemented as a map of
     *            &lt;keyword,value&gt; pairs. For each placeholder found in the
     *            template, if a value for that placeholder is found in the map,
     *            that value is substituted in the resulting text. <br/>
     *            If no value is found for a placeholder, an empty string will
     *            be substituted instead.<br/>
     * 
     *            It is expected that one of the keywords in the map matches the
     *            email address keyword, see {@link #DEFAULT_EMAIL_KEYWORD} and
     *            {@link #setEmailKeyword(String)}. If so, each generated
     *            message will be sent to that email address; otherwise, a
     *            runtime exception is thrown
     * 
     * @param recipientCC
     *            An email address; if not <code>null</code>, all messages will 
     *            be sent to this address as well
     *            
     * @param recipientBCC
     *            An email address; if not <code>null</code>, all messages will 
     *            be sent to this address as well     *            
     * 
     * @param contentType
     *            If {@link #TEXT_CONTENT_TYPE}, messages will be sent as plain 
     *            text; if {@link #HTML_CONTENT_TYPE} they will be sent as HTML
     *            
     * @throws MessagingException 
     * @throws IllegalArgumentException
     *             if one of the parameter sets does not contain a value for the
     *             email address keyword.
     */
    public void mailMerge( String template, 
                           List<Map<String,String>> paramSets,
                           String recipientCC,
                           String recipientBCC,
                           String contentType ) 
        throws IllegalArgumentException, MessagingException;

    /**
     * Mail out a list of messaged constructed from a template and a list of
     * parameter sets defining message-specific information.
     * 
     * @param template
     *            A template text; it may include one or more placeholders.<br/>
     *            After merging the message-specific information, if the first
     *            line of the message starts with the string
     *            <code>Subject:</code>, then that line will removed from the
     *            generated message and anything following <code>Subject:</code>
     *            will become the subject of the generated message.
     *            See {@link #SUBJECT_HEADER}.
     * 
     * @param paramSets
     *            A list of parameter sets; each set is implemented as a map of
     *            &lt;keyword,value&gt; pairs. For each placeholder found in the
     *            template, if a value for that placeholder is found in the map,
     *            that value is substituted in the resulting text. <br/>
     *            If no value is found for a placeholder, an empty string will
     *            be substituted instead.<br/>
     * 
     *            It is expected that one of the keywords in the map matches the
     *            email address keyword, see {@link #DEFAULT_EMAIL_KEYWORD} and
     *            {@link #setEmailKeyword(String)}. If so, each generated
     *            message will be sent to that email address; otherwise, a
     *            runtime exception is thrown.
     * 
     * @param recipientCC
     *            An email address; if not <code>null</code>, all messages will 
     *            be sent to this address as well
     *
     * @param recipientBCC
     *            An email address; if not <code>null</code>, all messages will 
     *            be sent to this address as well
     *            
     * @param contentType
     *            If {@link #TEXT_CONTENT_TYPE}, messages will be sent as plain 
     *            text; if {@link #HTML_CONTENT_TYPE} they will be sent as HTML
     * 
     * @param overrideEmail
     *            An email address; if not <code>null</code>, all messages will 
     *            be sent to this address instead of their intended recipient 
     *            (e.g. for test purposes)
     *            
     * @throws MessagingException
     * @throws IllegalArgumentException
     *             if one of the parameter sets does not contain a value for the
     *             email address keyword.
     */
    public void mailMerge( String template, 
                           List<Map<String,String>> paramSets,
                           String recipientCC,
                           String recipientBCC,
                           String contentType,
                           String overrideEmail ) 
        throws IllegalArgumentException, MessagingException;

}
