/**
 * MailMergerFactory.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.mailmerge;

/**
 * A simple factory of MailMerger instances. <p/>
 * 
 * The default implementation requires
 * that file <em>$ACSDATA/obopsConfig.properties</em> is found and it
 * contains the following properties:<UL>
 * <li><em>smtp.server</em>: the SMTP server to use for sending out email 
 *     messages, e.g. <em>smtphost.hq.eso.org</em>
 * <li><em>from.address</em>: what email address should appear on the 
 *     <em>From:</em> line
 * <li><em>mail.user</em>: an account on the <em>smtp.server</em>
 * <li><em>mail.password</em>: password for that account
 * </UL>
 * 
 * @author amchavan, Aug 17, 2010
 * @version $Revision: 1.2 $
 */

// $Id: MailMergerFactory.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class MailMergerFactory {
    
    /** 
     * @return A new implementation of {@link MailMerger}
     * @throws ExceptionInInitializerError if anything goes wrong
     */
    public static MailMerger getMailMerger() {
        try {
            return new MailMergerImpl();
        }
        catch( Exception e ) {
            throw new ExceptionInInitializerError( e );
        }
    }
}
