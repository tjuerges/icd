package alma.obops.sso;

import org.springframework.dao.DataAccessException;

/**
 * @version $Revision: 1.2 $
 */

// $Id: LDAPDataAccessException.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class LDAPDataAccessException extends DataAccessException {

    private static final long serialVersionUID = 5124618032459068575L;

    public LDAPDataAccessException( String errorMessage ) {
        super( errorMessage );
    }

    public LDAPDataAccessException( String errorMessage, Throwable t ) {
        super( errorMessage, t );
    }
}
