package alma.obops.sso;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.providers.cas.CasAuthenticationToken;

/**
 * 
 * It reads the username and credentials from the CAS authentication token
 * object. Is assigns
 * 
 * @author fjulbe, Mar 10, 2010
 * @version $Revision$
 */
public class UserCredentialsProcessor {

	public static final String ROLE_PREFIX = "ROLE_";
	
	private String userName;
	private boolean piRole;
	private List<String> roles = new ArrayList<String>();
	private String piEmail;

	/**
	 * Private constructor to force use of factory method: processCredentials
	 */
	private UserCredentialsProcessor(){}
	
	/**
	 * It receives the Authentication Token and retrieves the username and
	 * roles. If it contains a PI role it automatically assigns this
	 * role to the user.
	 * 
	 * @param credentialsObject
	 */
	public static UserCredentialsProcessor processCredentials(
			CasAuthenticationToken credentialsObject) {

		UserCredentialsProcessor ucp = new UserCredentialsProcessor();
		
		CasAuthenticationToken casToken = (CasAuthenticationToken) credentialsObject;
		AlmaUserDetails userDetails = (AlmaUserDetails) casToken.getPrincipal();
		// Initializing
		
		GrantedAuthority[] auth = userDetails.getAuthorities();
		for (GrantedAuthority authority : auth) {
			if(authority.getAuthority().startsWith(ROLE_PREFIX)){
				String role = authority.getAuthority().substring(ROLE_PREFIX.length()).toLowerCase();
				ucp.roles.add(role);
			}
		}
		ucp.userName = userDetails.getUsername();
		
		//TODO: remove this debug
		System.out.println(">>>> UserCredentialsProcessor - userName in Cas token : " + ucp.userName);
		
		ucp.piEmail = userDetails.getEmail();
		
		return ucp;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the piRole
	 */
	public boolean isPiRole() {
		return piRole;
	}
	
	/**
	 * @return piEmail
	 */
	public String getPiEmail() {
		return piEmail;
	}	
	
	/**
	 * @return
	 */
	public List<String> getRoles() {
		return roles;
	}	
}
