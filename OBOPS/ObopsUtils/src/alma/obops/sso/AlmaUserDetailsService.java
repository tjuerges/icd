package alma.obops.sso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;

import alma.obops.ldap.AddressBookService;
import alma.userrepository.errors.UserRepositoryException;
import alma.userrepository.roledirectory.Role;
import alma.userrepository.roledirectory.RoleDirectory;
import alma.userrepository.roledirectory.RoleDirectorySession;
import alma.userrepository.shared.DirectorySessionFactory;
import alma.userrepository.shared.LdapDirectorySessionFactory;

public class AlmaUserDetailsService implements UserDetailsService {
	
	protected DirectorySessionFactory sessionFactory;
	private String subsystem;
	
	private static Logger logger = Logger
		.getLogger(AlmaUserDetailsService.class.getName());
	
    public AlmaUserDetailsService(){
    	sessionFactory = LdapDirectorySessionFactory.getInstance(null);   	
    }
	
    /**
     * It will be removed as soon as all applications using it are moved
     * to the new implementation.
     * @see alma.obops.sso.AlmaUserDetailsService(java.lang.String)
     * Constructor called by Spring security files. It takes the ALMA LDAP parameters
     * (in case there are any) and configures the LDAP session.
     * 
     * @param factoryState
     * @param factoryObject
     * @param factoryInitial
     * @param securityAuthentication
     * @param providerUrl
     * @param userDN
     * @param userPass
     */
    @Deprecated
    public AlmaUserDetailsService(	String factoryState,
    								String factoryObject,
    								String factoryInitial,
    								String securityAuthentication,
    								String providerUrl,
    								String userDN,
    								String userPass,
    								String subsystem) {   	
    	
        /*
         * If these properties are now taken from the archiveConfig.properties,
         * they should not be present anymore.
    	Properties props = new Properties();
        
    	if(!factoryState.equals("")){
    		logger.info(" LDAP property factoryState " + factoryState);
            props.setProperty( LdapSessionFactory.STATE_FACTORY_KEY, 
            		factoryState);
        }
    	if(!factoryInitial.equals("")) {
    		logger.info(" LDAP property factoryInitial " + factoryInitial);
            props.setProperty( LdapSessionFactory.INITIAL_FACTORY_KEY,
            		factoryInitial);       	
        }
    	if(!securityAuthentication.equals("")) {
    		logger.info(" LDAP property securityAuthentication " + securityAuthentication);
            props.setProperty( LdapSessionFactory.AUTHENTICATION_KEY,
            		securityAuthentication);       	
        }
    	if(!providerUrl.equals("")) {
    		logger.info(" >> LDAP property provider URL " + providerUrl);
            props.setProperty( LdapSessionFactory.PROVIDER_URL_KEY,
            		providerUrl);       	
        }
    	if(!userDN.equals("")) {
    		logger.info(" >> LDAP property user DN " + userDN);
            props.setProperty( LdapSessionFactory.READ_ONLY_USER_DN_KEY,
            		userDN);       	
        }
    	if(!userPass.equals("")) {
    		logger.info(" >> LDAP property userPass read from configuration file");
            props.setProperty( LdapSessionFactory.READ_ONLY_USER_PASSWORD_KEY,
            		userPass);       	
        } 
    	
 	
    	
    	// If there are no properties it means all parameters were empty
    	// so it takes the default ones		
    	if (props.size() > 0) {
    		LdapSessionFactory.configure( props );
    	}*/
    	if(!subsystem.equals("")){
    		logger.info(" Subsystem " + subsystem);
    		this.subsystem = subsystem;
        }       	
        sessionFactory = LdapDirectorySessionFactory.getInstance(null);
	}
    
    
    /**
     * Creates the DirectorySessionFactory and sets the subsystem
     * 
     * @param subsystem
     */
    public AlmaUserDetailsService(String subsystem) { 
    	if(!subsystem.equals("")){
    		logger.info(">>> Initializing AlmaUserDetailsService for subsystem " + subsystem);
    		this.subsystem = subsystem.trim();
        }       	
        sessionFactory = LdapDirectorySessionFactory.getInstance(null);     	
    }
    
	/* (non-Javadoc)
	 * Implementation of the USerDetailsService used by Spring to retrieve credentials and
	 * authorizations.
	 * 
	 * The UserDetails object returned becomes the "principal" object in the session.
	 * 
	 * @see org.springframework.security.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	public UserDetails loadUserByUsername(String username) {

		try {
			// Opening an addressBook service and retrieving the email //
	        AddressBookService service = new AddressBookService();        
	        String attr0 = AddressBookService.EMAIL;
	        String[] attrs = { attr0 };
	        Map<String,String> ret = service.getAttributes( username, attrs );
			
			// Obtaining an LDAP session from the service
			RoleDirectorySession dirSess = service.getRoleDirectorySession();
			RoleDirectory roles = dirSess.getAllUserRoles(username);
            logger.info( ">>> Granted roles for user " + username ); 
            logger.info( ">>> " + roles ); 
	        dirSess.close();
	        
			List<String> appNames = roles.getApplicationNames();
            logger.info( ">>> ApplicationNames for user " + username ); 
            logger.info( ">>> " + appNames ); 
			// Adding authorities to the list.
			ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            boolean piRole = false;
            logger.info( ">>> subsystem='" + subsystem + "'" ); 
			for (String singleApp: appNames) {
				// We only take roles for the specific subsystem
				if(singleApp.equals(subsystem)){
					List<Role> userRoles=roles.getRoles(singleApp);
					for(Role singleRole: userRoles){
						if(singleRole.toString().toUpperCase().equals("PI")) {
							piRole = true;
						}
						authorities.add(new GrantedAuthorityImpl("ROLE_" + singleRole.toString().toUpperCase()));
					}
				}
			}
			
			//TODO: discuss with Maurizio, we need some sort of role for all users of ALMA/OBOPS
			// issue related to Assessor roles and ph1m
//			authorities.add(new GrantedAuthorityImpl("ROLE_ASSESSOR"));
			if(!piRole) {
				authorities.add(new GrantedAuthorityImpl("ROLE_PI"));
			}
			
			// String piEmail = "pi-mail@email.com";
			String email = ret.get(attr0);
			GrantedAuthority[] authArray = 
			    (GrantedAuthority[]) authorities.toArray(new GrantedAuthority[]{});
            logger.info( ">>> Granted authorities for user " 
                         + username
                         + " (email=" 
                         + email 
                         + "):" );
			for( int i = 0; i < authArray.length; i++ ) {
                GrantedAuthority auth = authArray[i];
                logger.info( "   auth=" + auth.toString() );
            }
			UserDetails userDetails = new AlmaUserDetails(
					username,
					"", // This is the password field. So far is empty. We may use it or not.
					email, // PIEmail
					true,
					true,
					true,
					true,
					authArray
					);
			return userDetails;
			
		}
		catch (UserRepositoryException e) {
			// Inherit of abstract DataAccessException
			throw new LDAPDataAccessException(e.getMessage());
		}
	}
}
