/**
 * PortalAccountDetailsService.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.obops.sso;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import alma.obops.ldap.AlmaLdapConnection;
import alma.obops.ldap.AlmaLdapConnectionException;
import alma.obops.ldap.Property;

/**
 * An implementation of UserDetailsService based on the ALMA LDAP architecture.
 * 
 * It has no dependencies outside this module.
 *
 * @author amchavan, Mar 11, 2011
 * @version $Revision: 1.2 $
 */

// $Id: PortalAccountDetailsService.java,v 1.2 2011/03/24 09:49:31 fjulbe Exp $

public class PortalAccountDetailsService implements UserDetailsService {

    private static Logger logger = 
        Logger.getLogger(PortalAccountDetailsService.class.getSimpleName());
    private static AlmaLdapConnection ldapConnection;
    
    private String subsystem;

    /**
     * @param subsystem
     *            The Alma CIPT subsystem for whose roles we're interested; e.g.
     *            <em>OBOPS</em>
     */
    public PortalAccountDetailsService( String subsystem ) {
        super();
        this.subsystem = subsystem.trim().toUpperCase();
    }

    /**
     * @see org.springframework.security.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    @Override
    public UserDetails loadUserByUsername( String username )
            throws UsernameNotFoundException, DataAccessException {

        // Find the LDAP entry corresponding to the username
        List<Property> properties = null;
        try {
            properties = getLdapConnection().readEntry( username );
        }
        catch( AlmaLdapConnectionException e ) {
            throw new LDAPDataAccessException( e.getMessage(), e );
        }
        if( properties == null ) {
            throw new UsernameNotFoundException( username );
        }

        // Find all relevant roles
        //---------------------------------------------------------
        Set<String> roleSet;
        try {
            roleSet = getLdapConnection().findRolesForUID( username );
        }
        catch( AlmaLdapConnectionException e ) {
            throw new LDAPDataAccessException( e.getMessage(), e );
        }
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        String subsystemSpec = subsystem.toUpperCase() + "/";
        int subsystemSpecLength = subsystemSpec.length();
        
        /* TODO remove PI-role hack */  
        boolean foundPI = false;    
        
        for( String role : roleSet ) {
            role = role.trim().toUpperCase();   // something like "OBOPS/ARCA"
            if( role.startsWith( subsystemSpec )) {
                role = role.substring( subsystemSpecLength );   // "ARCA"
                authList.add( new GrantedAuthorityImpl( "ROLE_" + role ));
                
                /* TODO remove PI-role hack */
                if( !foundPI && role.equals( "PI" ) ) {
                    foundPI = true;
                }
            }
        }
        
        /* TODO remove PI-role hack */
        if( !foundPI ) {
            authList.add( new GrantedAuthorityImpl( "ROLE_PI" ));
        }
        
        // Convert roles list to an array of GrantedAuthorities
        GrantedAuthority[] authArray = authList.toArray( new GrantedAuthority[0] );
        
        // Log some stuff to help with problems
        StringBuilder sb = new StringBuilder( "username='" );
        sb.append( username )
          .append( "', subsystem='" )
          .append( subsystem )
          .append( "', roles: " );
        for( int i = 0; i < authArray.length; i++ ) {
            GrantedAuthority auth = authArray[i];
            sb.append("'").append( auth.getAuthority() ).append("' ");
        }
        logger.info( sb.toString() );
        
        String email = getPropertyValue( properties, "mail" );
        UserDetails ret = new AlmaUserDetails( username, 
                                               "", // This is the password field
                                               email, 
                                               true, true, true, true, 
                                               authArray );
        return ret;
    }

    /**
     * @return One of the values of the Property with the given propertyName; or
     *         <code>null</code> if there isn't such a property.
     */
    private String getPropertyValue( List<Property> properties, String propertyName ) {
        for( Property property : properties ) {
            if( property.name.equals( propertyName )) {
                return property.value;
            }
        }
        return null;
    }
    
    private AlmaLdapConnection getLdapConnection() {
        if( ldapConnection == null ) {
            try {
                ldapConnection = new AlmaLdapConnection();
            }
            catch( AlmaLdapConnectionException e ) {
                String message = "Could not create an AlmaLdapConnection";
                throw new RuntimeException( message, e );
            }
        }
        return ldapConnection;
    }
}
