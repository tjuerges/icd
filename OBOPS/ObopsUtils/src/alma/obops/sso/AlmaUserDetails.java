package alma.obops.sso;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.User;


/**
 *
 * Extension of User: Adds email info to the User credentials.
 * Potentially, it will grow significantly. 
 *
 * @author fjulbe, Mar 15, 2010
 * @version $Revision$
 */
public class AlmaUserDetails extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = -11347859277827193L;
	private String email;

	/**
	 * @param username
	 * @param password
	 * @param email
	 * @param enabled
	 * @param accountNonExpired
	 * @param credentialsNonExpired
	 * @param accountNonLocked
	 * @param authorities
	 * @throws IllegalArgumentException
	 */
	public AlmaUserDetails(String username, String password, String email, boolean enabled,
							boolean accountNonExpired, boolean credentialsNonExpired,
							boolean accountNonLocked, GrantedAuthority[] authorities)
							throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		// Adding the email info
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
