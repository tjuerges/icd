package alma.lifecycle.remote.client;

import java.util.ArrayList;
import java.util.List;

import alma.entity.xmlbinding.obsproject.ObsProjectRefT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusChoice;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusRefT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.entity.xmlbinding.schedblock.SchedBlockRefT;
import alma.entity.xmlbinding.valuetypes.StatusT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.SpringTestCase;
import alma.lifecycle.remote.client.RestClient;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

public class RestClientTest extends SpringTestCase {
	private static RestClient client	= new RestClient("http://localhost:8180/StateSystem");

	public RestClientTest() {
		super();
	}
	
	public void setUpData() throws Exception {
		
		// some fake 'domain' entities which will be referenced by the status entities
		// ObsProject domain entity reference
		ObsProjectRefT oprt = new ObsProjectRefT();
		oprt.setEntityId("uid://X00/X4/X2");

		// ObsUnitSet domain entity references 
		// OUSStatusPF uses the obsUnitSetRef.partId to reference the ObsUnitSet
		ObsProjectRefT obsUnitSetRef1 = new ObsProjectRefT();
		obsUnitSetRef1.setEntityId(oprt.getEntityId());
		obsUnitSetRef1.setPartId(oprt.getEntityId() + "-1");
		
		ObsProjectRefT obsUnitSetRef2 = new ObsProjectRefT();
		obsUnitSetRef2.setEntityId(oprt.getEntityId());
		obsUnitSetRef2.setPartId(oprt.getEntityId() + "-2");
		
		// setup status entities
		StatusT status = new StatusT();
		status.setState(StatusTStateType.READY);

		ProjectStatusEntityT pset = new ProjectStatusEntityT();
		pset.setEntityId("uid://X00/X4/X1");
		ProjectStatusRefT psrt = new ProjectStatusRefT();
		psrt.setEntityId(pset.getEntityId());

		OUSStatusEntityT ous1et = new OUSStatusEntityT();
		ous1et.setEntityId("uid://X00/X4/X3");
		OUSStatusRefT ous1rt = new OUSStatusRefT();
		ous1rt.setEntityId(ous1et.getEntityId());

		OUSStatusEntityT ous2et = new OUSStatusEntityT();
		ous2et.setEntityId("uid://X00/X4/X5");
		OUSStatusRefT ous2rt = new OUSStatusRefT();
		ous2rt.setEntityId(ous2et.getEntityId());

		SBStatusEntityT sbet = new SBStatusEntityT();
		sbet.setEntityId("uid://X00/X4/X7");
		SBStatusRefT sbsrt = new SBStatusRefT();
		sbsrt.setEntityId(sbet.getEntityId());

		ProjectStatus ps = new ProjectStatus();
		ps.setProjectStatusEntity(pset);
		ps.setObsProgramStatusRef(ous1rt);
		ps.setStatus(status);
		ps.setName("Test project status structure");
		ps.setPI("PI");
		ps.setObsProjectRef(oprt);
		
		OUSStatus ous1 = new OUSStatus();
		ous1.setOUSStatusEntity(ous1et);
		ous1.setProjectStatusRef(psrt);
		ous1.setStatus(status);
		ous1.setTotalObsUnitSets(2);
		ous1.setTotalSBs(1);
		ous1.setObsUnitSetRef(obsUnitSetRef1);
		
		OUSStatusChoice oussc1 = new OUSStatusChoice();
		oussc1.addOUSStatusRef(ous2rt);
		ous1.setOUSStatusChoice(oussc1);
		
//		ous1.setNumberObsUnitSetsCompleted(2);
//		ous1.setNumberObsUnitSetsFailed(2);
//		ous1.setNumberSBsCompleted(1);
//		ous1.setNumberSBsFailed(1);	
		
		OUSStatus ous2 = new OUSStatus();
		ous2.setOUSStatusEntity(ous2et);
		ous2.setProjectStatusRef(psrt);
		ous2.setContainingObsUnitSetRef(ous1rt);
		ous2.setStatus(status);
		ous2.setTotalSBs(1);
		ous2.setObsUnitSetRef(obsUnitSetRef2);

		ous2.setObsUnitSetMemberType("OUSStatus");
//		ous2.setNumberSBsFailed(1);
//		ous2.setNumberSBsCompleted(1);

		OUSStatusChoice oussc2 = new OUSStatusChoice();
		oussc2.addSBStatusRef(sbsrt);
		ous2.setOUSStatusChoice(oussc2);		
		
		SBStatus sbs = new SBStatus();
		sbs.setSBStatusEntity(sbet);
		sbs.setProjectStatusRef(psrt);
		sbs.setContainingObsUnitSetRef(ous2rt);
		sbs.setStatus(status);
		SchedBlockRefT sbrt = new SchedBlockRefT();
		sbrt.setEntityId("uid://X00/X4/X6");
		sbs.setSchedBlockRef(sbrt);
		
		stateArchive.update(ps);
		stateArchive.update(ous1);
		stateArchive.update(ous2);
		stateArchive.update(sbs);
		commitAndStartNewTransaction();

		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");
		ProjectStatus entity = stateArchive.getProjectStatus(id);
		assertNotNull(entity);
		assertTrue(entity instanceof ProjectStatus);
		assertEquals(id.getEntityId(), entity.getProjectStatusEntity().getEntityId());
		
		entity = client.getProjectStatus(id);
		assertNotNull(entity);
		assertTrue(entity instanceof ProjectStatus);
		assertEquals(id.getEntityId(), entity.getProjectStatusEntity().getEntityId());
	
	}

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		setUpData();
	}

	public void testChangeStateOUSStatus() throws Exception {
		setAllToState(StatusTStateType.FULLYOBSERVED);

		OUSStatusEntityT id = new OUSStatusEntityT();
		id.setEntityId("uid://X00/X4/X5");

		assertTrue(client.changeState(id, StatusTStateType.PIPELINEERROR, "Pipeline", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.FULLYOBSERVED, "ObOps", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.PIPELINEERROR, "Pipeline", RoleProviderMock.ALL_ROLES_USER));

		// cannot set same state twice (= no transition from state X to state X)
		try {
			client.changeState(id, StatusTStateType.PIPELINEERROR, "Pipeline", RoleProviderMock.ALL_ROLES_USER);
			fail("Set state where no transition path exists");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// invalid transition path
		try {
			client.changeState(id, StatusTStateType.READY, "StateEngine", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state as unprivileged role");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// unprivileged role
		try {
			client.changeState(id, StatusTStateType.FULLYOBSERVED, "ObOps", "unprivileged");
			fail("Changed state as unprivileged role");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// incorrect subsystem for transition
		try {
			client.changeState(id, StatusTStateType.FULLYOBSERVED, "Scheduling", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state as unprivileged subsystem");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// null entity
		try {
			OUSStatusEntityT nullId = null;
			client.changeState(nullId, StatusTStateType.FULLYOBSERVED, "ObOps", RoleProviderMock.ALL_ROLES_USER);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("uid://X123/X123/X123");
		try {
			client.changeState(id, StatusTStateType.FULLYOBSERVED, "ObOps", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state of entity that doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testChangeStateProjectStatus() throws Exception {
		setAllToState(StatusTStateType.APPROVED);

		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");

		assertTrue(client.changeState(id, StatusTStateType.PHASE2SUBMITTED, "ObsPrep", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.APPROVED, "ObOps", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.PHASE2SUBMITTED, "ObsPrep", RoleProviderMock.ALL_ROLES_USER));

		// cannot set same state twice (= no transition from state X to state X)
		try {
			client.changeState(id, StatusTStateType.PHASE2SUBMITTED, "ObsPrep", RoleProviderMock.ALL_ROLES_USER);
			fail("Set state where no transition path exists");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// invalid transition path
		try {
			client.changeState(id, StatusTStateType.FULLYOBSERVED, "StateEngine", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state as unprivileged role");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// unprivileged role
		try {
			client.changeState(id, StatusTStateType.APPROVED, "ObOps", "unprivileged");
			fail("Changed state as unprivileged role");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// incorrect subsystem for transition
		try {
			assertTrue(client.changeState(id, StatusTStateType.APPROVED, "ObsPrep", RoleProviderMock.ALL_ROLES_USER));
			fail("Changed state as unprivileged subsystem");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// null entity
		try {
			ProjectStatusEntityT nullId = null;
			client.changeState(nullId, StatusTStateType.APPROVED, "ObOps", RoleProviderMock.ALL_ROLES_USER);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("uid://X123/X123/X123");
		try {
			client.changeState(id, StatusTStateType.APPROVED, "ObOps", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state of entity that doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testChangeStateSBStatus() throws Exception {
		setAllToState(StatusTStateType.READY);

		SBStatusEntityT id = new SBStatusEntityT();
		id.setEntityId("uid://X00/X4/X7");

		assertTrue(client.changeState(id, StatusTStateType.RUNNING, "Scheduling", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.READY, "Scheduling", RoleProviderMock.ALL_ROLES_USER));
		assertTrue(client.changeState(id, StatusTStateType.RUNNING, "Scheduling", RoleProviderMock.ALL_ROLES_USER));

		// cannot set same state twice (= no transition from state X to state X)
		try {
			assertFalse(client.changeState(id, StatusTStateType.RUNNING, "Scheduling", RoleProviderMock.ALL_ROLES_USER));
			fail("Set state where no transition path exists");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// invalid transition path
		try {
			client.changeState(id, StatusTStateType.PHASE1SUBMITTED, "Scheduling", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state as unprivileged role");
		} catch (AcsJNoSuchTransitionEx e) {
		}

		// unprivileged role
		try {
			client.changeState(id, StatusTStateType.READY, "Scheduling", "jimmy");
			fail("Changed state as unprivileged role");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// incorrect subsystem for transition
		try {
			client.changeState(id, StatusTStateType.READY, "Archive", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state as unprivileged subsystem");
		} catch (AcsJNotAuthorizedEx e) {
		}

		// null entity
		try {
			SBStatusEntityT nullId = null;
			client.changeState(nullId, StatusTStateType.READY, "Scheduling", RoleProviderMock.ALL_ROLES_USER);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("uid://X123/X123/X123");
		try {
			client.changeState(id, StatusTStateType.RUNNING, "Scheduling", RoleProviderMock.ALL_ROLES_USER);
			fail("Changed state of entity that doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testGetProjectStatus() throws Exception {
		// retrieve valid entity
		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");
		ProjectStatus entity = client.getProjectStatus(id);
		assertNotNull(entity);
		assertTrue(entity instanceof ProjectStatus);
		assertEquals(id.getEntityId(), entity.getProjectStatusEntity().getEntityId());

		// null entity
		try {
			ProjectStatusEntityT nullId = null;
			entity = client.getProjectStatus(nullId);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("rhubarb");
		try {
			entity = client.getProjectStatus(id);
			fail("No exception thrown when entity UID is invalid");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity of different type
		id.setEntityId("uid://X00/X4/X3");
		try {
			entity = client.getProjectStatus(id);
			fail("No exception thrown when entity is of different type");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entity = client.getProjectStatus(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testGetOUSStatus() throws Exception {
		// retrieve valid entity
		OUSStatusEntityT id = new OUSStatusEntityT();
		id.setEntityId("uid://X00/X4/X3");
		OUSStatus entity = client.getOUSStatus(id);
		assertNotNull(entity);
		assertTrue(entity instanceof OUSStatus);
		assertEquals(id.getEntityId(), entity.getOUSStatusEntity().getEntityId());

		// null entity
		try {
			OUSStatusEntityT nullId = null;
			entity = client.getOUSStatus(nullId);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("rhubarb");
		try {
			entity = client.getOUSStatus(id);
			fail("No exception thrown when entity UID is invalid");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity of different type
		id.setEntityId("uid://X00/X4/X1");
		try {
			entity = client.getOUSStatus(id);
			fail("No exception thrown when entity is of different type");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entity = client.getOUSStatus(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}

	}

	public void testGetOUSStatuses() throws Exception {
		// retrieve valid entity
		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");
		List<OUSStatus> entities = client.getOUSStatuses(id);
		assertNotNull(entities);
		List<String> expectedIds = new ArrayList<String>();
		expectedIds.add("uid://X00/X4/X3");
		expectedIds.add("uid://X00/X4/X5");
		for (OUSStatus o : entities) {
			assertNotNull(o);
			assertTrue(expectedIds.contains(o.getOUSStatusEntity().getEntityId()));
		}

		// bad entity
		id.setEntityId("rhubarb");
		try {
			entities = client.getOUSStatuses(id);
			fail("No exception thrown with bad entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// null entity
		try {
			ProjectStatusEntityT nullId = null;
			entities = client.getOUSStatuses(nullId);
			fail("No exception thrown with null entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entities = client.getOUSStatuses(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testGetSBStatus() throws Exception {
		// retrieve valid entity
		SBStatusEntityT id = new SBStatusEntityT();
		id.setEntityId("uid://X00/X4/X7");
		SBStatus entity = client.getSBStatus(id);
		assertNotNull(entity);
		assertTrue(entity instanceof SBStatus);
		assertEquals(id.getEntityId(), entity.getSBStatusEntity().getEntityId());

		// null entity
		try {
			SBStatusEntityT nullId = null;
			entity = client.getSBStatus(nullId);
			fail("No exception thrown when entityT is null");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity ID
		id.setEntityId("rhubarb");
		try {
			entity = client.getSBStatus(id);
			fail("No exception thrown when entity UID is invalid");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity of different type
		id.setEntityId("uid://X00/X4/X3");
		try {
			entity = client.getSBStatus(id);
			System.out.println(entity);
			fail("No exception thrown when entity is of different type");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entity = client.getSBStatus(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testGetSBStatusesOUSStatusEntityT() throws Exception {
		// retrieve valid entity
		OUSStatusEntityT id = new OUSStatusEntityT();
		id.setEntityId("uid://X00/X4/X5");
		List<SBStatus> entities = client.getSBStatuses(id);
		List<String> expectedIds = new ArrayList<String>();
		expectedIds.add("uid://X00/X4/X7");
		for (SBStatus o : entities) {
			assertNotNull(o);
			assertTrue(expectedIds.contains(o.getSBStatusEntity().getEntityId()));
		}

		// entity exists but no child SBStatuses
		id.setEntityId("uid://X00/X4/X3");
		try {
			entities = client.getSBStatuses(id);
			fail("No exception thrown when no child entities found");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// bad entity
		id.setEntityId("rhubarb");
		try {
			entities = client.getSBStatuses(id);
			fail("No exception thrown with bad entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// null entity
		try {
			ProjectStatusEntityT nullId = null;
			entities = client.getSBStatuses(nullId);
			fail("No exception thrown with null entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entities = client.getSBStatuses(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	public void testGetSBStatusesProjectStatusEntityT() throws Exception {
		// retrieve valid entity
		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");
		List<SBStatus> entities = client.getSBStatuses(id);
		assertNotNull(entities);
		List<String> expectedIds = new ArrayList<String>();
		expectedIds.add("uid://X00/X4/X7");
		for (SBStatus o : entities) {
			assertNotNull(o);
			assertTrue(expectedIds.contains(o.getSBStatusEntity().getEntityId()));
		}

		// bad entity
		id.setEntityId("rhubarb");
		try {
			entities = client.getSBStatuses(id);
			fail("No exception thrown with bad entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// null entity
		try {
			ProjectStatusEntityT nullId = null;
			entities = client.getSBStatuses(nullId);
			fail("No exception thrown with null entity");
		} catch (AcsJNoSuchEntityEx e) {
		}

		// entity doesn't exist
		id.setEntityId("uid://X123/X123/X123");
		try {
			entities = client.getSBStatuses(id);
			fail("No exception thrown when entity doesn't exist");
		} catch (AcsJNoSuchEntityEx e) {
		}
	}

	private void setAllToState(StatusTStateType status) throws Exception {
		SBStatusEntityT sbId = new SBStatusEntityT();
		sbId.setEntityId("uid://X00/X4/X7");
		SBStatus sbs = client.getSBStatus(sbId);
		sbs.getStatus().setState(status);
		client.update(sbs);

		OUSStatusEntityT ousId = new OUSStatusEntityT();
		ousId.setEntityId("uid://X00/X4/X3");
		OUSStatus ouss = client.getOUSStatus(ousId);
		ouss.getStatus().setState(status);
		client.update(ouss);
		ousId.setEntityId("uid://X00/X4/X5");
		ouss = client.getOUSStatus(ousId);
		ouss.getStatus().setState(status);
		client.update(ouss);

		ProjectStatusEntityT psId = new ProjectStatusEntityT();
		psId.setEntityId("uid://X00/X4/X1");
		ProjectStatus ps = client.getProjectStatus(psId);
		ps.getStatus().setState(status);
		client.update(ps);
	}

	public void testUpdateProjectStatus() throws Exception {
		// retrieve test entity with UID://X00/X4/X1
		ProjectStatusEntityT id = new ProjectStatusEntityT();
		id.setEntityId("uid://X00/X4/X1");
		ProjectStatus entity = client.getProjectStatus(id);
		// set state to broken
		entity.getStatus().setState(StatusTStateType.BROKEN);
		client.update(entity);

		// read entity, write with new state
		entity = client.getProjectStatus(id);
		assertTrue(entity.getStatus().getState() != StatusTStateType.REPAIRED);
		entity.getStatus().setState(StatusTStateType.REPAIRED);
		client.update(entity);

		// read entity, verify state matches that expected
		entity = client.getProjectStatus(id);
		assertTrue(entity.getStatus().getState() == StatusTStateType.REPAIRED);

		// try to overwrite OUSStatusEntity at UID://X00/X4/X3
		entity.getProjectStatusEntity().setEntityId("uid://X00/X4/X3");
		client.update(entity);
	}

	public void testUpdateOUSStatus() throws Exception {
		// create
		OUSStatusEntityT id = new OUSStatusEntityT();
		id.setEntityId("uid://X00/X4/X3");
		OUSStatus entity = client.getOUSStatus(id);
		entity.getStatus().setState(StatusTStateType.BROKEN);
		client.update(entity);

		entity = client.getOUSStatus(id);
		assertTrue(entity.getStatus().getState() == StatusTStateType.BROKEN);
		entity.getStatus().setState(StatusTStateType.REPAIRED);
		client.update(entity);

		entity = client.getOUSStatus(id);
		assertTrue(entity.getStatus().getState() == StatusTStateType.REPAIRED);

		entity.getOUSStatusEntity().setEntityId("uid://X00/X4/X1");
		client.update(entity);
	}

	public void testUpdateSBStatus() throws Exception {
		SBStatusEntityT id = new SBStatusEntityT();
		id.setEntityId("uid://X00/X4/X7");
		SBStatus entity = client.getSBStatus(id);
		entity.getStatus().setState(StatusTStateType.BROKEN);
		client.update(entity);

		entity = client.getSBStatus(id);
		assertTrue(entity.getStatus().getState() == StatusTStateType.BROKEN);
		entity.getStatus().setState(StatusTStateType.REPAIRED);
		client.update(entity);

		entity = client.getSBStatus(id);
		assertTrue(entity.getStatus().getState() == StatusTStateType.REPAIRED);

		// update should set if entity doesn't exist
		entity.getSBStatusEntity().setEntityId("uid://X00/X4/X1");
		client.update(entity);
	}

	public void testGetObsProjectStates() {
		String states;
		states = client.getObsProjectStates(null);
		assertEquals("", states);
		
		states = client.getObsProjectStates("");
		assertEquals("", states);

		states = client.getObsProjectStates("rhubarb");
		assertEquals("", states);

		states = client.getObsProjectStates("obops");
		assertNotNull(states);
	}
	
	public void testGetObsUnitSetStates() {
		String states;
		states = client.getObsUnitSetStates(null);
		assertEquals("", states);
		
		states = client.getObsUnitSetStates("");
		assertEquals("", states);

		states = client.getObsUnitSetStates("rhubarb");
		assertEquals("", states);

		states = client.getObsUnitSetStates("obops");
		assertNotNull(states);
	}
	
	public void testSchedBlockStates() {
		String states;
		states = client.getSchedBlockStates(null);
		assertEquals("", states);
		
		states = client.getSchedBlockStates("");
		assertEquals("", states);

		states = client.getSchedBlockStates("rhubarb");
		assertEquals("", states);

		states = client.getSchedBlockStates("obops");
		assertNotNull(states);
	}
	
}
