/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jun 18, 2009
 * @version $Revision$
 */

// $Id$
public class AllTests {

    public static Test suite() {
        TestSuite suite = 
            new TestSuite( "Test for alma.lifecycle" );
        
//        suite.addTest( alma.lifecycle.remote.client.RestClientTest );
        
        return suite;
    }
}
