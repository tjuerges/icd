package alma.lifecycle.remote.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;

import alma.lifecycle.remote.client.RemoteStateSystemClient;


public class AcsClientFactory extends HttpServlet {
	private static final long serialVersionUID = -5127711396882415192L;

	public void init(ServletConfig config) throws ServletException {
        String managerRef = config.getInitParameter("MANAGER_REFERENCE");
        if (managerRef != null) {
        	AcsClientSingleton.setManagerLocation(managerRef);
        }
    	AcsClientSingleton.getInstance();
    }
	
	/**
	 * Creates and returns an instance of RemoteStateSystemClient.
	 * 
	 * @return an instance of RemoteStateSystemClient
	 * @throws UnavailableException
	 *             if the StateSystem ACS component cannot be contacted
	 */
	public static RemoteStateSystemClient createClient() throws UnavailableException {
		return AcsClientSingleton.getInstance().getClient(); 
	}
}

class AcsClientSingleton {
    static String MANAGER_LOC = "corbaloc::127.0.0.1:3000/Manager";
    
    private static AcsClientSingleton instance;
    private RemoteStateSystemClient client;
    
    private AcsClientSingleton() throws UnavailableException {
    	Logger logger = Logger.getLogger(AcsClientSingleton.class.getName());
        try {
            client = new RemoteStateSystemClient(logger, AcsClientSingleton.MANAGER_LOC);
            logger.log(Level.INFO, "Retrieved StateSystem ACS component from " + AcsClientSingleton.MANAGER_LOC);
        } catch (Exception ex) {
            logger.log(Level.SEVERE,
                    "Could not connect to StateSystem ACS component", ex);
            throw new UnavailableException("Could not retrieve StateSystem ACS component from " + AcsClientSingleton.MANAGER_LOC);
        }
    }
    
    
	/**
	 * Return the ACS client manager, contacting a CORBA manager on
	 * localhost.
	 * 
	 * @return the ACS client manager
	 * @throws UnavailableException
	 *             if the StateSystem ACS component cannot be retrieved
	 */
    public static AcsClientSingleton getInstance() throws UnavailableException {
        if (instance == null) {
            synchronized (AcsClientSingleton.class) {
                if (instance == null) {
                    instance = new AcsClientSingleton();
                }
            }
        }
        return instance;
    }

    
	/**
	 * Returns the ACS StateSystem client retrieved from the ACS manager
	 * location given at runtime.
	 * 
	 * @return the ACS StateSystem client
	 * @throws UnavailableException
	 *             if the StateSystem ACS component cannot be retrieved
	 */
    public RemoteStateSystemClient getClient() throws UnavailableException {
        return client;
    }
    
    
    public static void setManagerLocation(String url) {
    	MANAGER_LOC = url;
    }
}

