package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

/**
 * Maps AcsJNotAuthorizedEx to HTML Response #401: Unauthorized.
 * 
 * @author sjw
 */
@Provider
public class AcsJNotAuthorizedExExceptionMapper implements ExceptionMapper<AcsJNotAuthorizedEx> {

	@Override
	public Response toResponse(AcsJNotAuthorizedEx e) {
		Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
        return Response.status(Status.UNAUTHORIZED).build();
	}

}
