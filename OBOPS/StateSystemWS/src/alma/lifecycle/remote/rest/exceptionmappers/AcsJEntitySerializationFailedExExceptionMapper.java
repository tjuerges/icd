package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;

/**
 * Maps AcsJEntitySerializationFailedEx to HTML Response #500: Internal Server
 * Error.
 * 
 * @author sjw
 */
@Provider
public class AcsJEntitySerializationFailedExExceptionMapper implements ExceptionMapper<AcsJEntitySerializationFailedEx> {

	@Override
	public Response toResponse(AcsJEntitySerializationFailedEx e) {
		Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Error deserializing entity", e);
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();		
	}

}
