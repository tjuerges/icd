package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 * Maps AcsJStateIOFailedEx to HTML Response #500: Internal Server Error.
 * 
 * @author sjw
 */
@Provider
public class AcsJStateIOFailedExExceptionMapper implements ExceptionMapper<AcsJStateIOFailedEx> {

	@Override
	public Response toResponse(AcsJStateIOFailedEx e) {
        Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
	
}
