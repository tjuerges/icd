package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;

/**
 * Maps AcsJNoSuchEntityEx to HTML Response #404: Not Found.
 * 
 * @author sjw
 */
@Provider
public class AcsJNoSuchEntityExExceptionMapper implements ExceptionMapper<AcsJNoSuchEntityEx> {

	@Override
	public Response toResponse(AcsJNoSuchEntityEx e) {
		Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
        return Response.status(Status.NOT_FOUND).build();
	}

}
