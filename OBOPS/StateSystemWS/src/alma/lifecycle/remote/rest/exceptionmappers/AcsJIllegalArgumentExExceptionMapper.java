package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;

/**
 * Maps AcsJIllegalArgumentEx to HTML Response #400: Bad Request.
 * 
 * @author sjw
 */
@Provider
public class AcsJIllegalArgumentExExceptionMapper implements ExceptionMapper<AcsJIllegalArgumentEx> {

	@Override
	public Response toResponse(AcsJIllegalArgumentEx e) {
		Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
        return Response.status(Status.BAD_REQUEST).build();
	}

}
