package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;

/**
 * Maps AcsJInappropriateEntityType to HTML Response #500: Internal Server
 * Error.
 * 
 * @author sjw
 */
@Provider
public class AcsJInappropriateEntityTypeExExceptionMapper implements ExceptionMapper<AcsJInappropriateEntityTypeEx> {

	@Override
	public Response toResponse(AcsJInappropriateEntityTypeEx e) {
        // Never happens -always returns NoSuchEntity?
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

}
