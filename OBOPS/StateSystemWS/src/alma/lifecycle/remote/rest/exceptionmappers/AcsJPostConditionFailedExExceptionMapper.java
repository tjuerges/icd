package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;

/**
 * Maps AcsJPostConditionFailedEx to HTML Response #409 Conflict
 * 
 * @author sjw
 */
@Provider
public class AcsJPostConditionFailedExExceptionMapper implements ExceptionMapper<AcsJPostconditionFailedEx> {

	@Override
	public Response toResponse(AcsJPostconditionFailedEx e) {
		Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
        return Response.status(Status.CONFLICT).build();
	}

}
