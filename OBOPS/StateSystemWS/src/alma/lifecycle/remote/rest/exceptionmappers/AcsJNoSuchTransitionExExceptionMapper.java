package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;

/**
 * Maps AcsJNoSuchTransitionEx to HTML Response #409: Bad Request.
 * 
 * @author sjw
 */
@Provider
public class AcsJNoSuchTransitionExExceptionMapper implements ExceptionMapper<AcsJNoSuchTransitionEx> {

	@Override
	public Response toResponse(AcsJNoSuchTransitionEx e) {
		Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
		return Response.status(Status.CONFLICT).build();
	}

}
