package alma.lifecycle.remote.rest.exceptionmappers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;

/**
 * Maps AcsJNullEntityIdEx to HTML Response #500: Internal Server Error.
 * 
 * @author sjw
 */
@Provider
public class AcsJNullEntityExExceptionMapper implements ExceptionMapper<AcsJNullEntityIdEx> {

	@Override
	public Response toResponse(AcsJNullEntityIdEx e) {
		// This should never happen, as translateUid always returns a String
		// (at least 'uid://'), so setEntityId will never be set to null
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
        throw new WebApplicationException(e, Response.status(Status.INTERNAL_SERVER_ERROR).build());
	}

}
