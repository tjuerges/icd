package alma.lifecycle.remote.rest;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.remote.client.ChangeStateRestRequest;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * Provides REST access to a ProjectStatus.
 * <p>
 * When accessed with HTTP GET, this resource returns a representation of the
 * target ProjectStatus entity; the exact data returned depends on whether the
 * client requests XML, HTML or plain text.
 * <p>
 * This resource can also act as an access point for child OUSStatus and
 * SBStatus entities, via the '/OUSStatuses' and '/SBStatuses' resources
 * respectively.
 * 
 * @author Stewart Williams
 */
public class ProjectStatus extends StatusEntityResource {

	private final alma.entity.xmlbinding.projectstatus.ProjectStatus projectStatus;

    /**
	 * Creates REST resource for the ProjectStatus with the given UID.
	 * 
	 * @throws UnavailableException
	 *             if the ACS component is unavailable
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if the entity in the archive with a given EntityId is not of
	 *             the appropriate type
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             unmarshaled
	 * @throws AcsJNullEntityIdEx
	 *             if statusEntityId argument is null
	 */
    public ProjectStatus(ProjectStatusEntityT entityT) throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
    	super(entityT);
    	RemoteStateSystemClient client = AcsClientFactory.createClient();
        projectStatus = client.getProjectStatus(entityT);
    	super.setStatusBase(projectStatus);
    }

    @PUT
    @Consumes("application/xml")
    public Response changeState(JAXBElement<ChangeStateRestRequest> request) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, UnavailableException {
    	ChangeStateRestRequest csr = request.getValue();
    	ProjectStatusEntityT target = projectStatus.getProjectStatusEntity();
    	StatusTStateType destination = csr.getState();
    	String subsystem = csr.getSubsystem();
    	String userId = csr.getUserId();
    	RemoteStateSystemClient client = AcsClientFactory.createClient();
    	client.changeState(target, destination, subsystem, userId);
    	return Response.ok().build();
    }
    
    @Path("OUSStatuses")
    public OUSStatuses getOUSStatuses() {
        return new OUSStatuses(projectStatus.getProjectStatusEntity());
    }

    @Path("SBStatuses")
    public SBStatuses getSBStatuses() {
        return new SBStatuses(projectStatus.getProjectStatusEntity());
    }
}
