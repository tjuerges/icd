package alma.lifecycle.remote.rest;

import javax.servlet.UnavailableException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import alma.lifecycle.remote.client.RemoteStateSystemClient;

import com.sun.jersey.api.NotFoundException;

@Path("/states/{subsystem}/")
public class SubsystemStates {
	@GET
	@Produces("text/plain")
	@Path("ObsProject")
    public String getObsProjectStatesPlain(@PathParam("subsystem") String subsystem) throws UnavailableException {
		RemoteStateSystemClient client = AcsClientFactory.createClient();
		return client.getObsProjectStates(subsystem); 
	}

	@GET
	@Produces("text/html")
	@Path("ObsProject")
    public String getObsProjectHtml(@PathParam("subsystem") String subsystem) throws UnavailableException {
		return "<html>" + format("ObsProject", getObsProjectStatesPlain(subsystem)) + "</html>";
	}
	
	@GET
	@Produces("text/plain")
	@Path("ObsUnitSet")
    public String getObsUnitSetStatesPlain(@PathParam("subsystem") String subsystem) throws UnavailableException {
		RemoteStateSystemClient client = AcsClientFactory.createClient();
		return client.getObsUnitSetStates(subsystem); 
	}

	@GET
	@Produces("text/html")
	@Path("ObsUnitSet")
    public String getObsUnitSetHtml(@PathParam("subsystem") String subsystem) throws UnavailableException {
		return "<html>" + format("ObsUnitSet", getObsUnitSetStatesPlain(subsystem)) + "</html>";
	}
	
	@GET
	@Produces("text/plain")
	@Path("SchedBlock")
    public String getSchedBlockStatesPlain(@PathParam("subsystem") String subsystem) throws UnavailableException {
		RemoteStateSystemClient client = AcsClientFactory.createClient();
		return client.getSchedBlockStates(subsystem); 
	}

	@GET
	@Produces("text/html")
	@Path("SchedBlock")
    public String getSchedBlockStatesHtml(@PathParam("subsystem") String subsystem) throws UnavailableException {
		return "<html>" + format("SchedBlock", getSchedBlockStatesPlain(subsystem)) + "</html>";
	}
	
	@GET
	@Produces("text/html")
	public String getAllStates(@PathParam("subsystem") String subsystem) throws UnavailableException {
		String html = format("ObsProject", getObsProjectStatesPlain(subsystem))
				+ format("ObsUnitSet", getObsUnitSetStatesPlain(subsystem))
				+ format("SchedBlock", getSchedBlockStatesPlain(subsystem));

		if (html.equals("")) {
			throw new NotFoundException();
		} else {
			return "<html><h1>State transitions for " + subsystem + "</h1>" + html + "</html>";
		}

	}

	private String format(String entity, String transitions) {
		String html = "";
		if (transitions != null && !transitions.equals("")) {
			html = "<h2>" + entity + " transitions</h2><ul>";
			for (String transition : transitions.split(";")) {
				html += "<li>" + transition + "</li>";
			}
			html += "</ul>";
		}
		return html;
	}
}
