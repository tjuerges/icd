package alma.lifecycle.remote.rest;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.remote.client.ChangeStateRestRequest;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * Provides REST access to an OUSStatus.
 * <p>
 * When accessed with HTTP GET, this resource returns a representation of the
 * OUSStatus entity it represents; the exact data returned depends on whether
 * the client requests XML, HTML or plain text.
 * <p>
 * This resource can also act as an access point for child SBStatus entities,
 * via the '/SBStatuses' resource.
 * 
 * @author Stewart Williams
 */
public class OUSStatus extends StatusEntityResource {
    private final alma.entity.xmlbinding.ousstatus.OUSStatus ousStatus;

	/**
	 * Creates REST resource for the OUSStatus with the given UID.
	 * 
	 * @throws UnavailableException
	 *             if the StateSystem ACS component is unavailable
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if the entity in the archive with a given EntityId is not of
	 *             the appropriate type
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             unmarshaled
	 * @throws AcsJNullEntityIdEx
	 *             if statusEntityId argument is null
	 */
    public OUSStatus(OUSStatusEntityT entityT) throws UnavailableException, AcsJNullEntityIdEx, AcsJInappropriateEntityTypeEx, AcsJNoSuchEntityEx {
    	super(entityT);
    	RemoteStateSystemClient client = AcsClientFactory.createClient();
        ousStatus = client.getOUSStatus(entityT);
    	super.setStatusBase(ousStatus);
    }

    @Path("SBStatuses")
    public SBStatuses getSBStatuses() {
        return new SBStatuses(ousStatus.getOUSStatusEntity());
    }
    
    @PUT
    @Consumes("application/xml")
    public Response changeState(JAXBElement<ChangeStateRestRequest> request) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, UnavailableException {
    	ChangeStateRestRequest csr = request.getValue();

    	OUSStatusEntityT target = ousStatus.getOUSStatusEntity();
    	StatusTStateType destination = csr.getState();
    	String subsystem = csr.getSubsystem();
    	String userId = csr.getUserId();
    	
    	RemoteStateSystemClient client = AcsClientFactory.createClient();
    	client.changeState(target, destination, subsystem, userId);
    	
    	return Response.ok().build();
    }
    
}
