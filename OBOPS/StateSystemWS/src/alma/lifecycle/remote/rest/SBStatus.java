package alma.lifecycle.remote.rest;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.xml.bind.JAXBElement;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.remote.client.ChangeStateRestRequest;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * Provides REST access to an SBStatus.
 * <p>
 * When accessed with HTTP GET, this resource returns a representation of the
 * SBStatus entity it represents; the exact data returned depends on whether the
 * client requests XML, HTML or plain text.
 * 
 * @author Stewart Williams
 */
public class SBStatus extends StatusEntityResource {
    @Context
    SecurityContext security;	
	
    private final alma.entity.xmlbinding.sbstatus.SBStatus sbStatus;

    /**
	 * Creates REST resource for the ProjectStatus with the given UID.
	 * 
	 * @throws UnavailableException
	 *             if the ACS component is unavailable
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if the entity in the archive with a given EntityId is not of
	 *             the appropriate type
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             unmarshaled
	 * @throws AcsJNullEntityIdEx
	 *             if statusEntityId argument is null
	 */
    public SBStatus(SBStatusEntityT entityT) throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
    	super(entityT);
    	RemoteStateSystemClient client = AcsClientFactory.createClient();
        sbStatus = client.getSBStatus(entityT);
    	super.setStatusBase(sbStatus);
    }

    
    @PUT
    @Consumes("application/xml")
    public Response changeState(JAXBElement<ChangeStateRestRequest> request) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, UnavailableException {
    	ChangeStateRestRequest csr = request.getValue();

    	SBStatusEntityT target = sbStatus.getSBStatusEntity();
    	StatusTStateType destination = csr.getState();
    	String subsystem = csr.getSubsystem();
    	String userId = csr.getUserId();

    	RemoteStateSystemClient client = AcsClientFactory.createClient();
    	client.changeState(target, destination, subsystem, userId);
    	
    	return Response.ok().build();
    }
    
}
