package alma.lifecycle.remote.rest;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.exolab.castor.xml.XMLException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import alma.entity.xmlbinding.projectstatus.StatusBaseT;

/**
 * Provides an easy way to append multiple Castor serialisations to an XML
 * document.
 * 
 * @author Stewart Williasm
 */
public class MultiEntityDocument {
	private static final Logger logger = Logger.getLogger(MultiEntityDocument.class.getName());
    private static final Transformer transformer;
    private static final DocumentBuilder builder;
    private final Document doc;
    private final Element rootNode;

    static {
        TransformerFactory transfac = TransformerFactory.newInstance();
        try {
            transformer = transfac.newTransformer();
        } catch (TransformerConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
        	logger.log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        }
    }

    public MultiEntityDocument(String rootElement) {
        doc = builder.newDocument();
        rootNode = doc.createElement(rootElement);
        doc.appendChild(rootNode);
    }

    public void addAsChildElement(Document other) {
        Node otherNode = other.getFirstChild();
        Node dupNode = this.doc.importNode(otherNode, true);
        rootNode.appendChild(dupNode);
    }

    public void addAsChildElement(StringWriter writer) {
        InputSource is = new InputSource(new StringReader(writer.toString()));
        Document newDoc;
        try {
            newDoc = builder.parse(is);
        } catch (SAXException ex) {
            // Castor marshal() is returning invalid XML
        	logger.log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        } catch (IOException ex) {
            // We should never get an IOException on a StringWriter, but log it anyway
        	logger.log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        }
        addAsChildElement(newDoc);
    }

    public void addAsChildElement(StatusBaseT base) {
        StringWriter writer = new StringWriter();
        try {
            base.marshal(writer);
        } catch (XMLException ex) {
        	logger.log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        }
        addAsChildElement(writer);
    }

    @Override
    public String toString() {
        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        try {
            transformer.transform(source, result);
        } catch (TransformerException ex) {
        	logger.log(Level.SEVERE, "Error converting results to XML", ex);
            throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
        }
        String xmlString = sw.toString();

        return xmlString;
    }
}
