package alma.lifecycle.remote.rest;

import java.io.StringReader;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.exolab.castor.xml.XMLException;

import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 * An SBStatuses object provides REST access to SBStatus entities. Its behaviour
 * depends on the context in which it is used, either as a root resource or as a
 * child resource of a parent ProjectStatus or OUSStatus.
 * <p>
 * When given a UID, SBStatuses returns the REST resource representing the
 * SBStatus with the given UID. If the UID does not correspond to a valid
 * SBStatus, HTML error code 404 (not found) will be returned. If SBStatuses is
 * accessed as a root resource but without giving a UID argument, SBStatuses
 * will return HTML error code 400 (bad request). For example,
 * 
 * <li>/SBStatuses/X00-X4-X7 -> SBStatus REST resource</li>
 * <li>/SBStatuses/X00-Y12-Z32 -> #404 not found</li>
 * <li>/SBStatuses -> #400 bad request</li>
 * <p>
 * When accessed from a ProjectStatus or OUSStatus REST resource, SBStatuses
 * will return an XML representation of all the SBStatus entities contained
 * within the parent entity. If the parent entity contains no SBStatuses, HTML
 * error code #404 (not found) will be returned. For example,
 * 
 * <li>/ProjectStatus/X00-X4-X1/SBStatuses = return all SBStatus entities
 * contained within ProjectStatus uid://X00/X4/X1, or #404 not found if the
 * ProjectStatus has no SBStatus children</li>
 * <li>/OUSStatus/X00-X4-X5/SBStatuses = return all SBStatus entities contained
 * within OUSStatus uid://X00/X4/X5, or #404 not found if the OUSStatus has no
 * SBStatus children</li>
 * <li>/ProjectStatus/X00-X4-X1/OUSStatuses/X00-X4-X5/SBStatuses = return all
 * SBStatus entities that are children of OUSStatus uid://X00/X4/X5, which is a
 * child of ProjectStatus uid://X00/X4/X1.</li>
 * 
 * @author Stewart Williams
 */
@Path("/SBStatuses")
public class SBStatuses {
	@Context UriInfo uriInfo;
	
	/** The name of the XML element containing the SBStatus representations */
    public static final String MULTIPLE_ELEMENT_NAME = "SBStatuses";

    private final ProjectStatusEntityT projEntityT;
    private final OUSStatusEntityT ousEntityT;

	/**
	 * Create a REST resource representing a ProjectStatus' SBStatuses.
	 * 
	 * @param entityT
	 *            the parent ProjectStatus entity
	 */
	public SBStatuses(ProjectStatusEntityT entityT) {
		projEntityT = entityT;
		ousEntityT = null;
	}

	/**
	 * Create a REST resource representing an OUSStatus' SBStatuses.
	 * 
	 * @param entityT
	 *            the parent OUSStatus entity
	 */
    public SBStatuses(OUSStatusEntityT entityT) {
        ousEntityT = entityT;
        projEntityT = null;
    }

    @POST
    @Consumes({"text/plain","application/xml","text/xml"})
    public Response update(String xmlString) throws UnavailableException, AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        RemoteStateSystemClient client = AcsClientFactory.createClient();

        // unmarshal entity from posted input 
        StringReader reader = new StringReader(xmlString);
        alma.entity.xmlbinding.sbstatus.SBStatus sbStatus;
		try {
			sbStatus = alma.entity.xmlbinding.sbstatus.SBStatus.unmarshalSBStatus(reader);
		} catch (XMLException e) {
	        Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
	        Response response = Response.status(Status.BAD_REQUEST).build();
	        return response;
		}
		
		// update StateSystem
		client.update(sbStatus);
			
		// response should say where updated resource can be found
		String almaUid = sbStatus.getSBStatusEntity().getEntityId();
		String restUid = StatusEntityResource.translateAlmaUidToRestUid(almaUid);

		UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
		uriBuilder.path(restUid);
		URI restUri = uriBuilder.build();
		
		return Response.created(restUri).build();		
    }
    
	/**
	 * Return the SBStatus REST resource of the SBStatus with the given UID.
	 * 
	 * @param sbStatusUid
	 *            the UID of the SBStatus to retrieve
	 * @return the SBStatus REST resource
	 * @throws UnavailableException
	 *             if the StateSystem component cannot be retrieved
	 * @throws AcsJInappropriateEntityTypeEx 
	 * @throws AcsJNoSuchEntityEx 
	 * @throws AcsJNullEntityIdEx 
	 */
    @Path("{SBStatusUID}")
    public SBStatus getSBStatus(@PathParam("SBStatusUID") String sbStatusUid) throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
        SBStatusEntityT entityT = new SBStatusEntityT();
        entityT.setEntityId(StatusEntityResource.translateRestUidToAlmaUid(sbStatusUid));

        SBStatus sbStatus = new SBStatus(entityT);

        if (projEntityT != null) {
            Boolean isChild = false;
                RemoteStateSystemClient client = AcsClientFactory.createClient();
                for (alma.entity.xmlbinding.sbstatus.SBStatus sbs : client.getSBStatusList(projEntityT)) {
                    if (sbs.getProjectStatusRef().getEntityId().equals(projEntityT.getEntityId())) {
                        isChild = true;
                    }
                }

            if (isChild == false) {
                throw new WebApplicationException(Response.status(Status.NOT_FOUND).build());
            }
        }

        if (ousEntityT != null) {
            Boolean isChild = false;
                RemoteStateSystemClient client = AcsClientFactory.createClient();
                for (alma.entity.xmlbinding.sbstatus.SBStatus sbs : client.getSBStatusList(ousEntityT)) {
                    if (sbs.getSBStatusEntity().getEntityId().equals(entityT.getEntityId())) {
                        isChild = true;
                    }
                }
            if (isChild == false) {
                throw new WebApplicationException(Response.status(Status.NOT_FOUND).build());
            }
        }

        return sbStatus;        
    }    
    
	/**
	 * Retrieves an XML representation of all the SBStatuses that are child
	 * entities of this resource. Each SBStatus will be given as a child element
	 * of the root element &lt;SBStatuses&gt;.
	 * 
	 * @return a String containing the XML representation
	 * @throws UnavailableException
	 *             if the ACS component is unavailable
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if the entity is not of the appropriate type
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             StatusEntityID argument
	 * @throws AcsJNullEntityIdEx
	 *             if the StatusEntityId argument is null
	 */
    @GET
    @Produces("application/xml")
    public String getXml() throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
        if (projEntityT == null && ousEntityT == null) {
            Logger.getLogger(OUSStatuses.class.getName()).log(Level.FINE,
                    "/SBStatuses called without parent");
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
        }

        if (projEntityT != null) {
            return getProjectXml();
        } else {
            return getOUSXML();
        }
    }

    private String getProjectXml() throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
		MultiEntityDocument me = new MultiEntityDocument(MULTIPLE_ELEMENT_NAME);
		RemoteStateSystemClient client = AcsClientFactory.createClient();
		for (StatusBaseT sbStatus : client.getSBStatusList(projEntityT)) {
			me.addAsChildElement(sbStatus);
		}
        return me.toString();
    }

    private String getOUSXML() throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
        MultiEntityDocument me = new MultiEntityDocument(MULTIPLE_ELEMENT_NAME);
		RemoteStateSystemClient client = AcsClientFactory.createClient();
		for (StatusBaseT sbstatus : client.getSBStatusList(ousEntityT)) {
			me.addAsChildElement(sbstatus);
		}
		return me.toString();
    }
}
