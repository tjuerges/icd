package alma.lifecycle.remote.rest;

import java.io.StringReader;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.exolab.castor.xml.XMLException;

import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 *
 * @author sjw
 */
@Path("ProjectStatuses")
public class ProjectStatuses {
	@Context UriInfo uriInfo;

	@Path("{ProjectStatusUID}")
    public ProjectStatus getProjectStatus(@PathParam("ProjectStatusUID") String projectStatusUid) throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
		ProjectStatusEntityT entityT = new ProjectStatusEntityT();
		entityT.setEntityId(StatusEntityResource.translateRestUidToAlmaUid(projectStatusUid));
		return new ProjectStatus(entityT);
    }
    
    @POST
    @Consumes({"text/plain","application/xml","text/xml"})
    public Response update(String xmlString) throws UnavailableException, AcsJStateIOFailedEx, AcsJNoSuchEntityEx {

        RemoteStateSystemClient client = AcsClientFactory.createClient();

        // unmarshal ProjectStatus from posted input 
        StringReader reader = new StringReader(xmlString);
        alma.entity.xmlbinding.projectstatus.ProjectStatus projStatus;        
		try {
			projStatus = alma.entity.xmlbinding.projectstatus.ProjectStatus.unmarshalProjectStatus(reader);
		} catch (XMLException e) {
	        Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
	        Response response = Response.status(Status.BAD_REQUEST).build();
	        return response;
		}
		
		// update StateSystem
		client.update(projStatus);
			
		// response should say where updated resource can be found
		String almaUid = projStatus.getProjectStatusEntity().getEntityId();
		String restUid = StatusEntityResource.translateAlmaUidToRestUid(almaUid);

		UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
		uriBuilder.path(restUid);
		URI restUri = uriBuilder.build();
		
		return Response.created(restUri).build();		
    }
}
