package alma.lifecycle.remote.rest;

import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.entities.commonentity.EntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

/**
 * Provides functions common to all StatusEntity resources.
 * <p>
 *  
 * @author sjw
 */
public abstract class StatusEntityResource {
	protected final String entityId;
	protected StatusBaseT base;
	protected String entityType;
	
	public StatusEntityResource(EntityT entity) {
        entityId = entity.getEntityId();
	}

	public void setStatusBase(StatusBaseT base) {
		this.base = base;
        entityType = base.getClass().getSimpleName();
	}
	
	
	/**
	 * Retrieves an XML representation of this status entity.
	 * 
	 * @return a String containing the XML representation
	 */
	@GET
	@Produces( { "application/xml", "text/xml" })
	public String getXml() {
		StringWriter writer = new StringWriter();
		try {
			base.marshal(writer);
		} catch (MarshalException e) {
	        Logger.getLogger(getClass().getName()).log(Level.SEVERE, 
	        		"Could not marshal " + entityType + " " + entityId, e);
	        throw new WebApplicationException(e, Response.status(Status.INTERNAL_SERVER_ERROR).build());
		} catch (ValidationException e) {
	        Logger.getLogger(getClass().getName()).log(Level.SEVERE, 
	        		"Could not marshal " + entityType + " " + entityId, e);
	        throw new WebApplicationException(e, Response.status(Status.INTERNAL_SERVER_ERROR).build());
		}
		return writer.toString();		
	}

	
	/**
	 * Retrieves an HTML document containing a brief summary of the status of
	 * this entity.
	 * 
	 * @return a String containing the HTML summary
	 */
    @GET
    @Produces("text/html")
    public String getHtml() {
        return "<html><h1>" + entityType + " " + entityId + "</h1>" +
        	"<h2>Current state is <span style=\"color:red\">" + getText() + 
        	"</span></h2></html>";
    }	
	
    
    /**
     * Returns, as plain text, the status of this entity.
     * 
     * @return the entity status
     */
    @GET
    @Produces("text/plain")
    public String getText() {
    	return base.getStatus().getState().toString();
    }

    
	/**
	 * Converts a UID in REST format to the standard ALMA archive format.
	 * 
	 * @param restUid
	 *            the UID, in REST format
	 * @return the UID, in standard ALMA format
	 */
    public static String translateRestUidToAlmaUid(String restUid) {
        if (restUid == null) {
            return null;
        }
        String newUid = "uid://" + restUid.replace("-", "/");
        return newUid;
    }

    
	/**
	 * Converts a UID in ALMA format to a REST-format UID.
	 * 
	 * @param almaUid
	 *            the UID to transform
	 * @return the UID in REST format
	 */
	public static String translateAlmaUidToRestUid(String almaUid) {
		if (almaUid == null) {
			return null;
		}
		return almaUid.substring(6).replaceAll("/", "-");
	}
	
	
	protected String getUserId(SecurityContext context) throws AcsJNotAuthorizedEx { 
    	String userId = context.getUserPrincipal().getName();
    	if (userId == null || userId.equals("")) {
    		throw new AcsJNotAuthorizedEx();
    	}
    	return userId;
	}    	
	
}
