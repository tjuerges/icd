package alma.lifecycle.remote.rest;

import java.io.StringReader;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.UnavailableException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.exolab.castor.xml.XMLException;

import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 * REST Web Service
 *
 * @author sjw
 */
@Path("/OUSStatuses")
public class OUSStatuses {
	@Context UriInfo uriInfo;

    private final ProjectStatusEntityT projEntityT;
    public static final String MULTIPLE_ELEMENT_NAME = "OUSStatuses";

    /** Creates a new instance of OUSStatuses */
    public OUSStatuses(ProjectStatusEntityT entityT) {
        this.projEntityT = entityT;
    }

	/**
	 * Retrieves an XML representation of the child OUSStatuses of a resource.
	 * 
	 * @return an instance of java.lang.String
	 * @throws UnavailableException
	 *             if the ACS component is unavailable
	 * @throws AcsJEntitySerializationFailedEx
	 *             if there was a problem serialising entities to XML
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if the entity is not of the appropriate type
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             StatusEntityID argument
	 * @throws AcsJNullEntityIdEx
	 *             if the StatusEntityId argument is null
	 */
    @GET
    @Produces("application/xml")
    public String getXml() throws UnavailableException, AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx {
        if (projEntityT == null) {
            Logger.getLogger(OUSStatuses.class.getName()).log(Level.FINE,
                    "/OUSStatuses called without parent Project");
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
        }

        MultiEntityDocument doc = new MultiEntityDocument(MULTIPLE_ELEMENT_NAME);

        RemoteStateSystemClient client = AcsClientFactory.createClient();
        for (StatusBaseT base : client.getProjectStatusList(projEntityT)) {
            if (base.getClass() == alma.entity.xmlbinding.ousstatus.OUSStatus.class) {
                doc.addAsChildElement(base);
            }
        }

        return doc.toString();
    }

    @Path("{OUSStatusUID}")
    public OUSStatus getOUSStatus(@PathParam("OUSStatusUID") String ousStatusUid) throws UnavailableException, AcsJNullEntityIdEx, AcsJInappropriateEntityTypeEx, AcsJNoSuchEntityEx, AcsJEntitySerializationFailedEx {
        OUSStatusEntityT entityT = new OUSStatusEntityT();
        entityT.setEntityId(StatusEntityResource.translateRestUidToAlmaUid(ousStatusUid));
        OUSStatus ousStatus = new OUSStatus(entityT);
			
		// Q: is the OUSStatus a child of this project? 
        if (projEntityT != null) {
            RemoteStateSystemClient client = AcsClientFactory.createClient();
            for (StatusBaseT base : client.getProjectStatusList(projEntityT)) {
                if (base.getClass() == alma.entity.xmlbinding.ousstatus.OUSStatus.class) {
                    alma.entity.xmlbinding.ousstatus.OUSStatus cast = (alma.entity.xmlbinding.ousstatus.OUSStatus) base;
                    if (cast.getProjectStatusRef().getEntityId().equals(projEntityT.getEntityId())) {
                        // A: yes, so return it
                    	return ousStatus;
                    }
                }
            }
            // A: no, so return NOT_FOUND
            throw new WebApplicationException(Response.status(Status.NOT_FOUND).build());
        }

	    return ousStatus;
    }

    @POST
    @Consumes({"text/plain","application/xml","text/xml"})
    public Response update(String xmlString) throws UnavailableException, AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        RemoteStateSystemClient client = AcsClientFactory.createClient();

        // unmarshal entity from posted input 
        StringReader reader = new StringReader(xmlString);
        alma.entity.xmlbinding.ousstatus.OUSStatus ousStatus;
		try {
			ousStatus = alma.entity.xmlbinding.ousstatus.OUSStatus.unmarshalOUSStatus(reader);
		} catch (XMLException e) {
	        Logger.getLogger(getClass().getName()).log(Level.FINEST, null, e);
	        Response response = Response.status(Status.BAD_REQUEST).build();
	        return response;
		}
		
		// update StateSystem
		client.update(ousStatus);
			
		// response should say where updated resource can be found
		String almaUid = ousStatus.getOUSStatusEntity().getEntityId();
		String restUid = StatusEntityResource.translateAlmaUidToRestUid(almaUid);

		UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
		uriBuilder.path(restUid);
		URI restUri = uriBuilder.build();
		
		return Response.created(restUri).build();		
    }

}
