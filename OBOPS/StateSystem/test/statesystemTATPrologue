#!/bin/bash

echo "starting ACS..."  1>> tmp/prologue.out 2>> tmp/prologue.err
export JAVA_OPTIONS="-Darchive.configFile=archiveConfig.properties.remote"

acsutilTATPrologue -l --noloadifr
echo "ACS is now up."  1>> tmp/prologue.out 2>> tmp/prologue.err

export ACS_INSTANCE=`cat tmp/acs_instance`
echo "ACS_INSTANCE is: $ACS_INSTANCE"  1>> tmp/prologue.out 2>> tmp/prologue.err

echo "Loading IDL specified in prologue..." 1>> tmp/prologue.out 2>> tmp/prologue.err
acsstartupLoadIFR projectlifecycle.idl statearchiveexceptions.idl stateengineexceptions.idl \
              1>> ./tmp/prologue.out 2>> ./tmp/prologue.err

# -----------------------------------------------------------------------------------------
# NOTE: the remote tests cannot use an in-memory hsqldb due to the fact that some of the
# processes reside in different JVM instances; in-memory only works for one process/JVM.
#
# Specifically, the setup/teardown process of junit testing framework is used to create/drop
# tables, etc. However, those are executed in the jvm instance of the junit test, which for 
# the case of the tests that utilize the CORBA/ACS component, is a different JVM instance
# than the component runs in, meaning that the tables are *not* dropped/created for the
# component, and we get problems with tables being missing. 
#
# There are multiple ways to solve this problem, one could be for the component to have 
# a test mode in which it creates an in-memory db itself. Another solution, as is done here, 
# is to use a "server" mode for hsqldb. Because many of the other tests don't want to use 
# server mode for hsqldb, we will therefore, configure hsqldb only for the component
# -----------------------------------------------------------------------------------------

# Start hsqldb server using file-based setup
echo "starting hsqldb server for testing..." 1>> tmp/prologue.out 2>> tmp/prologue.err
acsStartJava org.hsqldb.Server -database.0 mem:hsqldb/state-archive -dbname.0 statearchive >& ./tmp/statearchivestart.log &
sleep 5

# create the tables for our file-based hsqldb database
echo "creating tables for hsqldb..." 1>> tmp/prologue.out 2>> tmp/prologue.err
acsStartJava alma.lifecycle.clients.CreateEmptyDatabase Server statearchive localhost sql/hsqldb-ddl.sql >& ./tmp/cleanstatearchive.log

# start the container, which will auto-start the component
echo "Starting java container for OBOPS..." 1>> tmp/prologue.out 2>> tmp/prologue.err
acsutilAwaitContainerStart -java OBOPS/ACC/javaContainer

unset JAVA_OPTIONS

echo "prologue finished." 1>>tmp/prologue.out 2>>tmp/prologue.err
exit 0
