/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jun 18, 2009
 * @version $Revision$
 */

// $Id$
public class AllNonRemoteTests {

    public static Test suite() {
        String name = "Test for alma.lifecycle, no ACS";
        TestSuite suite = new TestSuite( name );
        
        //$JUnit-BEGIN$
        suite.addTestSuite( TestStateSystem.class );
        //$JUnit-END$
        
        suite.addTest( alma.lifecycle.persistence.AllTests.suite() );
        suite.addTest( alma.lifecycle.stateengine.AllTests.suite() );
        suite.addTest( alma.lifecycle.uml.magicdraw.AllTests.suite() );
        suite.addTest( alma.lifecycle.clients.AllTests.suite() );
        suite.addTest( alma.entity.xmlbinding.AllTests.suite() );
        
        return suite;
    }
}
