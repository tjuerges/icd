/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;

/**
 * 
 * TestObsUnitSetLifeCycle tests all the state transitions that are initiated in
 * the ObsUnitSet Life cycle. This test runs using the production UML state machine diagram.
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision$
 */

// $Id$
public class TestObsUnitSetLifeCycle extends TestCase {

	Logger logger = Logger.getLogger(TestObsUnitSetLifeCycle.class.getName());

	public void testFullyObservedToPipelineError() {

		changeObsUnitSetState( StatusTStateType.FULLYOBSERVED, 
		                       StatusTStateType.PIPELINEERROR, 
		                       Location.SCO,
		                       Subsystem.PIPELINE, Role.QAA);

	}

	public void testPipelineErrorToFullyObserved() {

		changeObsUnitSetState( StatusTStateType.FULLYOBSERVED,
		                       StatusTStateType.PIPELINEERROR, 
		                       StatusTStateType.FULLYOBSERVED,
		                       StatusTStateType.PIPELINEERROR, 
		                       StatusTStateType.FULLYOBSERVED, 
		                       Location.SCO, Subsystem.OBOPS, Role.QAA);
		
		changeObsUnitSetState( StatusTStateType.FULLYOBSERVED,
                StatusTStateType.PIPELINEERROR, 
                StatusTStateType.PROCESSED,
                StatusTStateType.PIPELINEERROR, 
                StatusTStateType.FULLYOBSERVED, 
                Location.SCO, Subsystem.OBOPS, Role.QAA);

	}

	public void testFullyObservedToProcessed() {

		changeObsUnitSetState( StatusTStateType.FULLYOBSERVED, 
		                       StatusTStateType.PROCESSED, 
		                       Location.SCO, Subsystem.PIPELINE, Role.QAA);
	}
	
	public void testFullyObservedToProcessed2() {

		changeObsUnitSetState(StatusTStateType.OBSERVINGTIMEDOUT, 
				StatusTStateType.FULLYOBSERVED,
                StatusTStateType.FULLYOBSERVED,
                StatusTStateType.FULLYOBSERVED,
				StatusTStateType.PROCESSED, 
				Location.SCO, 
				Subsystem.PIPELINE, 
				Role.QAA);
	}
	
	public void testObservingTimedOutToFullyObserved() {

		changeObsUnitSetState(StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
                StatusTStateType.FULLYOBSERVED,
                StatusTStateType.OBSERVINGTIMEDOUT,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);
	}
	
	
	public void testObservingTimedOutToCanceled() {

		changeObsUnitSetState(StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
                StatusTStateType.FULLYOBSERVED,
                StatusTStateType.OBSERVINGTIMEDOUT,
				StatusTStateType.CANCELED, 
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);
	}
	
	public void testProcessedToPipelineError() {

		// trickle down
		changeObsUnitSetState( StatusTStateType.PROCESSED, 
		                       StatusTStateType.PIPELINEERROR, 
		                       Location.SCO, Subsystem.OBOPS, Role.QAA );
		
		// 2 should be processed and 2 fullyObserved
		int fullyObservedSbCount = 0;
		for(SBStatus sbStat: Utils.getSBStatuses()){
			if(StatusTStateType.FULLYOBSERVED.equals(sbStat.getStatus().getState())){
				fullyObservedSbCount++;
			}
		}		
		
		assertEquals("There should be 2 fullyobserved SB's after the trickledown of ProcessedToPipelineError", 
				2, fullyObservedSbCount);
		
	}


	public void testProcessedToPipelineErrorTwice() {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
	    System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
	    stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				StatusTStateType.FULLYOBSERVED, 
				StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED);
	
		try {
			OUSStatus level0Ous = Utils.findOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatus level1Ous = Utils.findOUSStatus(level0Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous1 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous2 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(1).getEntityId());
			
			logger.info("project status before change: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
	
			stateEngine.changeState(level2Ous1.getOUSStatusEntity(), 
					StatusTStateType.PIPELINEERROR,	Subsystem.OBOPS, Role.QAA);
	
			logger.info("project status after PIPELINEERROR 1: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			stateEngine.changeState(level2Ous2.getOUSStatusEntity(),
					StatusTStateType.PIPELINEERROR,	Subsystem.OBOPS, Role.QAA);
			
			logger.info("project status after PIPELINEERROR 2: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			SBStatus sb1 = Utils.findSBStatus(level2Ous1.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			sb1.getStatus().setState(StatusTStateType.PROCESSED);
			SBStatus sb2 = Utils.findSBStatus(level2Ous1.getOUSStatusChoice().getSBStatusRef(1).getEntityId());
			sb2.getStatus().setState(StatusTStateType.VERIFIED);
			
			logger.info("project status before FULLYOBSERVED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			stateEngine.changeState(level2Ous1.getOUSStatusEntity(), 
					StatusTStateType.FULLYOBSERVED,	Subsystem.OBOPS, Role.QAA);
			
			logger.info("project status after FULLYOBSERVED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	public void testFullyObservedToProcessed02() {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
	    System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
	    stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.READY);
	
		try {
			OUSStatus level0Ous = Utils.findOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatus level1Ous = Utils.findOUSStatus(level0Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous1 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous2 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(1).getEntityId());
			
			// setup first OUS as partiallyobserved
			level2Ous2.getStatus().setState(StatusTStateType.FULLYOBSERVED);
			SBStatus sb1 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			sb1.getStatus().setState(StatusTStateType.FULLYOBSERVED);
			SBStatus sb2 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(1).getEntityId());
			sb2.getStatus().setState(StatusTStateType.FULLYOBSERVED);
			
			logger.info("project status before change: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
	
			stateEngine.changeState(level2Ous2.getOUSStatusEntity(), 
					StatusTStateType.PROCESSED,	Subsystem.PIPELINE, Role.QAA);
	
			logger.info("project status after PROCESSED 1: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
	
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private ProjectStatus changeObsUnitSetState(StatusTStateType startState,
			StatusTStateType targetState, String location, String subsystem,
			String role) {

		return changeObsUnitSetState(startState, startState, startState,
				startState, targetState, location, subsystem, role);
	}

	
	private ProjectStatus 
	    changeObsUnitSetState( StatusTStateType opStartState,
	                           StatusTStateType allOusStartState,
	                           StatusTStateType allSbStartState,
	                           StatusTStateType testOusStartState,
	                           StatusTStateType ousTargetState, 
	                           String location, 
	                           String subsystem, 
	                           String uid) {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, location );
        stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				opStartState, allOusStartState, allSbStartState);

		try {
			OUSStatus ousProgram = Utils.findOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatusRefT childOusRefT = ousProgram.getOUSStatusChoice().getOUSStatusRef(0);
			
			OUSStatus ousChild1 = Utils.findOUSStatus(childOusRefT.getEntityId());
			
			OUSStatusRefT childOus2RefT = ousChild1.getOUSStatusChoice().getOUSStatusRef(0);			
			OUSStatus ousChild2 = Utils.findOUSStatus(childOus2RefT.getEntityId());
			
			ousChild2.getStatus().setState(testOusStartState);
			
			logger.fine(Utils.getProjectStatusTreeForDisplay(projectStatus));

			stateEngine.changeState(ousChild2.getOUSStatusEntity(), ousTargetState, subsystem, uid);

			assertEquals(ousTargetState, ousChild2.getStatus().getState());

			logger.fine("TestObsUnitSetLifeCycle: ObsProjectStatus after change from: "
							+ allOusStartState + " to: " + ousTargetState);
			logger.fine(Utils.getProjectStatusTreeForDisplay(projectStatus));

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return projectStatus;
	}
}
