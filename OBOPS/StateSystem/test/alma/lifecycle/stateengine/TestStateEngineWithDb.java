/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.SpringTestCase;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

/**
 * 
 * TestStateEngineWithDb puts the state engine impl through its paces using 
 * a basic Uml state machine diagram, Spring based StateArchive and a DB
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision$
 */

// $Id$
public class TestStateEngineWithDb extends SpringTestCase {

	Logger logger = Logger.getLogger(TestStateEngineWithDb.class.getName());
	
	private StateEngine stateEngine;
	
	public StateEngine getStateEngine() {
		return stateEngine;
	}

	public void setStateEngine(StateEngine stateEngine) {
		this.stateEngine = stateEngine;
	}

	///////////////////////////////// ObsProject tests /////////////////////
	
	public void testObsProjectChangeStateSuccess(){ 

		// test successful change from Approved to Phase2Submitted
		try {
			changeStatusTStateType(StatusTStateType.APPROVED, 
			                      StatusTStateType.PHASE2SUBMITTED, 
			                      Subsystem.OBSPREP, Role.PI);	
			
	        List<StateChangeRecord> list = 
	            stateArchive.findStateChangeRecords( null, null, null, 
	                                             null, null, null );
	        
	        assertNotNull(list);
	        assertEquals(1, list.size());
	        assertNotSame(list.get(0).getDomainEntityId(), list.get(0).getStatusEntityId());

	        
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
		
	public void testObsProjectIncorrectRole(){ 

		// test change from Approved to Phase2Submitted with incorrect Role
		// ObsPrep; PI; SCO are the correct values		
		
		boolean threwAuthException = false;
		try {
			try{
				changeStatusTStateType(StatusTStateType.APPROVED, 
				                      StatusTStateType.PHASE2SUBMITTED, 
				                      Subsystem.OBSPREP, Role.AOD);				
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect role", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
	public void testObsProjectIncorrectSubsystem(){ 

		// test change from Approved to Phase2Submitted with incorrect Subsystem
		// ObsPrep; PI; SCO are the correct values
		boolean threwAuthException = false;
		try {
			try{
				changeStatusTStateType(StatusTStateType.APPROVED, 
				                      StatusTStateType.PHASE2SUBMITTED, 
				                      Subsystem.OBOPS, Role.PI);
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect subsystem", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
		
//	public void testObsProjectIncorrectLocation(){ 
//
//		// test change from Approved to Phase2Submitted with incorrect Location
//		// ObsPrep; PI; SCO are the correct values
//		boolean threwAuthException = false;
//		try {
//			try{
//				changeStatusTStateType(Location.OSF, 
//				                      StatusTStateType.APPROVED, 
//				                      StatusTStateType.PHASE2SUBMITTED, 
//				                      Subsystem.OBSPREP, Role.PI);
//			}catch(AcsJNotAuthorizedEx ae){
//				threwAuthException = true;
//			}
//			assertTrue("Did not throw AuthorizationException for incorrect Location", threwAuthException);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//
//	}
		
	public void testObsProjectInvalidTargetState(){ 

		// test change from Approved to Invalid target
		boolean threwNoSuchException = false;
		try {
			try{
				changeStatusTStateType(StatusTStateType.APPROVED, 
				                      StatusTStateType.REPAIRED, 
				                      Subsystem.OBSPREP, Role.PI);
			}catch(AcsJNoSuchTransitionEx ae){
				threwNoSuchException = true;
			}
			assertTrue("Did not throw NoSuchTransitionException for incorrect target state", threwNoSuchException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}	
		
	private void changeStatusTStateType(StatusTStateType startState, 
	                                    StatusTStateType targetState, 
	                                    String subsystem, 
	                                    String uid ) 
		throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, 
			   AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
			   AcsJStateIOFailedEx, AcsJNoSuchEntityEx, AcsJIllegalArgumentEx
	{
		
		Utils.reset();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		ProjectStatus projectStatus = Utils.makeProjectStatus();
		projectStatus.getStatus().setState(startState);
		projectStatus.getObsProjectRef().setEntityId("project domain id");
		
		Collection<OUSStatus> ousList = Utils.getOUSStatuses();
		Collection<SBStatus> sbList = Utils.getSBStatuses();
		
		stateArchive.insert(projectStatus, 
				ousList.toArray(new OUSStatus[0]), 
				sbList.toArray(new SBStatus[0]));
		
		stateEngine.changeState((ProjectStatusEntityT)projectStatus.getProjectStatusEntity(), 
				targetState, subsystem, uid);
		
	}
}
