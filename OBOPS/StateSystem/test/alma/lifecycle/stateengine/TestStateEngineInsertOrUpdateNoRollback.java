/**
 * TestStateArchiveImpl.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.stateengine;

import static alma.lifecycle.config.SpringConstants.TEST_STATE_SYSTEM_SPRING_CONFIG;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.acs.entityutil.EntitySerializer;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.obsproposal.ObsProposal;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.xmlentity.XmlEntityStruct;

/**
 * Basic test for StateEngine insertOrUpdate() with no rollback
 * 
 * @author rkurowsk, Jul 3, 2009
 * @version $Revision: 1.2 $
 */

// $Id: TestStateEngineInsertOrUpdateNoRollback.java,v 1.2 2011/02/03 10:28:55 rkurowsk Exp $

public class TestStateEngineInsertOrUpdateNoRollback extends TestCase {

	public static final String OBOPS_ARCHIVE_ID = "X58";
	protected Logger logger = Logger.getAnonymousLogger();
	protected StateArchive stateArchive;
	private StateEngine stateEngine;
	
	
	public TestStateEngineInsertOrUpdateNoRollback() throws Exception {
		
		if(!StateSystemContextFactory.INSTANCE.isInitialized()){
	    	// initialize the contextFactory using the tmcdbConfiguration file
			StateSystemContextFactory.INSTANCE.init(TEST_STATE_SYSTEM_SPRING_CONFIG, 
					new StateArchiveDbConfig(logger));
			
			stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
	    	stateArchive.initStateArchive(logger);
	    	
	    	stateEngine  = StateSystemContextFactory.INSTANCE.getStateEngine();
	    	System.setProperty( Location.RUNLOCATION_PROP, Location.SCO);
	    	stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		}
	}
	
    public void testInsertOrUpdate() throws Exception {

	    Utils.reset();

	    ProjectStatus ops  = Utils.makeProjectStatus( 1, 2 );
        SBStatus[] sbsList = Utils.getSBStatuses().toArray( new SBStatus[0] );
        OUSStatus[] oussList = Utils.getOUSStatuses().toArray( new OUSStatus[0] );
        
    	EntitySerializer serializer = EntitySerializer.getEntitySerializer( logger );
    	
    	ObsProposal prop = TestStateEngineInsertOrUpdate.makeTestObsProposal();
    	SchedBlock sb0 = TestStateEngineInsertOrUpdate.makeTestSchedBlock();
        SchedBlock sb1 = TestStateEngineInsertOrUpdate.makeTestSchedBlock();
        ObsProject proj = TestStateEngineInsertOrUpdate.makeTestObsProject( prop, sb0, sb1);
        
        XmlEntityStruct[] archiveEntities = new XmlEntityStruct[4];
        archiveEntities[0] = serializer.serializeEntity(proj);
        archiveEntities[1] = serializer.serializeEntity(prop);
        archiveEntities[2] = serializer.serializeEntity(sb0);
        archiveEntities[3] = serializer.serializeEntity(sb1);
        
        // this test requires that the stateArchiveImple or Dao throw a runtime exception
                
        Collection<StateTransitionParam> stp = new ArrayList<StateTransitionParam>();
        
        stp.add(new StateTransitionParam(ops.getProjectStatusEntity(), 
        		StatusTStateType.READY, Subsystem.OBOPS, Role.ARCA.toString()));
        
    	
    	stateEngine.insertOrUpdate(archiveEntities, "testUser", ops, oussList, sbsList, stp);

    }
}
