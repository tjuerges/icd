/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.lifecycle.uml.dto.UmlLifeCycles;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * TestStateEngine puts the state engine impl through its paces using a basic Uml state machine diagram
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision$
 */

// $Id$
public class TestStateEngine extends TestCase {

	Logger logger = Logger.getLogger(TestStateEngine.class.getName());
	
	public void testStateEngineInit(){
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		assertTrue("StateEngine not initialized", stateEngine.isInitialized());
	}
		
	///////////////////////////////// ObsProject tests /////////////////////
	
	public void testObsProjectChangeStateSuccess(){ 

		// test successful change from Approved to Phase2Submitted
		try {
			changeStatusTStateType(Location.SCO, 
			                      StatusTStateType.APPROVED, 
			                      StatusTStateType.PHASE2SUBMITTED, 
			                      Subsystem.OBSPREP, Role.PI);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
		
	public void testObsProjectIncorrectRole(){ 

		// test change from Approved to Phase2Submitted with incorrect Role
		// ObsPrep; PI; SCO are the correct values		
		
		boolean threwAuthException = false;
		try {
			try{
				changeStatusTStateType(Location.SCO, 
				                      StatusTStateType.APPROVED, 
				                      StatusTStateType.PHASE2SUBMITTED, 
				                      Subsystem.OBSPREP, Role.AOD);				
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect role", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
	public void testObsProjectIncorrectSubsystem(){ 

		// test change from Approved to Phase2Submitted with incorrect Subsystem
		// ObsPrep; PI; SCO are the correct values
		boolean threwAuthException = false;
		try {
			try{
				changeStatusTStateType(Location.SCO, 
				                      StatusTStateType.APPROVED, 
				                      StatusTStateType.PHASE2SUBMITTED, 
				                      Subsystem.OBOPS, Role.PI);
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect subsystem", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
		
    // TODO
    //------------------------------------------------------------
    // This test commented out because we do not know where the
    // State System is running -- we need that information
    // somehow.
    // See Jira ticket COMP-3643
    //-----------------------------------------------------------
//	public void testObsProjectIncorrectLocation(){ 
//
//		// test change from Approved to Phase2Submitted with incorrect Location
//		// ObsPrep; PI; SCO are the correct values
//		boolean threwAuthException = false;
//		try {
//			try{
//				changeStatusTStateType(Location.OSF, 
//				                      StatusTStateType.APPROVED, 
//				                      StatusTStateType.PHASE2SUBMITTED, 
//				                      Subsystem.OBSPREP, Role.PI);
//			}catch(AcsJNotAuthorizedEx ae){
//				threwAuthException = true;
//			}
//			assertTrue("Did not throw AuthorizationException for incorrect Location", threwAuthException);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//
//	}
		
	public void testObsProjectInvalidTargetState(){ 

		// test change from Approved to Invalid target
		boolean threwNoSuchException = false;
		try {
			try{
				changeStatusTStateType(Location.SCO, 
				                      StatusTStateType.APPROVED, 
				                      StatusTStateType.REPAIRED, 
				                      Subsystem.OBSPREP, Role.PI);
			}catch(AcsJNoSuchTransitionEx ae){
				threwNoSuchException = true;
			}
			assertTrue("Did not throw NoSuchTransitionException for incorrect target state", threwNoSuchException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}	
	
	public void testSchedBlockIncorrectRole(){ 

		// test change from Ready to Running
		// with incorrect Role
		// Scheduling; Auto; OSF are the correct values		
		boolean threwAuthException = false;
		try {
			try{
				changeSbState( Location.SCO,
				               StatusTStateType.READY, 
				               StatusTStateType.RUNNING, 
				               Subsystem.SCHEDULING, Role.AOD);
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect role", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
	public void testSchedBlockIncorrectSubsystem(){ 

		// test change from Ready to Running
		// with incorrect Subsystem
		// Scheduling; Auto; OSF are the correct values
		boolean threwAuthException = false;
		try {
			try{
				changeSbState(Location.SCO,
				              StatusTStateType.READY, 
				              StatusTStateType.RUNNING, 
				              Subsystem.OBOPS, Role.AOD);
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect subsystem", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
		
    // TODO
    //------------------------------------------------------------
    // This test commented out because we do not know where the
    // State System is running -- we need that information
    // somehow.
    // See Jira ticket COMP-3643
    //-----------------------------------------------------------
//	public void testSchedBlockIncorrectLocation(){ 
//
//		// test change from Ready to Running
//		// with incorrect Location
//		// Scheduling; Auto; OSF are the correct values
//		
//		boolean threwAuthException = false;
//		try {
//			try{
//				changeSbState( Location.SCO,
//				               StatusTStateType.READY, 
//				               StatusTStateType.RUNNING, 
//				               Subsystem.SCHEDULING, Role.AUTO);
//			}catch(AcsJNotAuthorizedEx ae){
//				threwAuthException = true;
//			}
//			assertTrue("Did not throw AuthorizationException for incorrect Location", threwAuthException);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//	}
		
	public void testSchedBlockInvalidTargetState(){ 

		try {
			boolean threwNoSuchException = false;
			try{
				changeSbState( Location.SCO,
				               StatusTStateType.READY, 
				               StatusTStateType.PARTIALLYOBSERVED, 
				               Subsystem.SCHEDULING, Role.AOD );
			}catch(AcsJNoSuchTransitionEx ae){
				threwNoSuchException = true;
			}
			assertTrue("Did not throw NoSuchTransitionException for incorrect target state", threwNoSuchException);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void testGetObsProjectStates(){
		
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO);
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		String statesString = stateEngine.getObsProjectStates(Subsystem.OBOPS);
		assertNotNull(statesString);
		System.out.println("ObsProjectStates string = " + statesString);
		Map<String, Set<String>> statesMap = UmlLifeCycles.convertStatesStringToMap(statesString);
		assertEquals(11, statesMap.size());
		System.out.println("ObsProjectStates map = " + statesMap);

		
	}
	
	public void testGetObsUnitSetStates(){
		
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO);
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		String statesString = stateEngine.getObsUnitSetStates(Subsystem.OBOPS);
		assertNotNull(statesString);
		System.out.println("ObsUnitSetStates = " + statesString);
		Map<String, Set<String>> statesMap = UmlLifeCycles.convertStatesStringToMap(statesString);
		assertEquals(3, statesMap.size());
		System.out.println("ObsUnitSetStates map = " + statesMap);
		
	}
	
	public void testGetSchedBlockStates(){
		
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO);
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		String statesString = stateEngine.getSchedBlockStates(Subsystem.OBOPS);
		assertNotNull(statesString);
		System.out.println("SchedBlockStates = " + statesString);
		Map<String, Set<String>> statesMap = UmlLifeCycles.convertStatesStringToMap(statesString);
		assertEquals(3, statesMap.size());
		System.out.println("SchedBlockStates map = " + statesMap);
		
	}
	
	private void changeSbState( String location, 
	                            StatusTStateType startState, 
	                            StatusTStateType targetState, 
	                            String subsystem, 
	                            String uid ) 
		throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx{
		
		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, location );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		SBStatus sBstatus = Utils.makeSBStatus();
		sBstatus.getStatus().setState(startState);
		
		stateEngine.changeState((SBStatusEntityT)sBstatus.getSBStatusEntity(), 
				targetState, subsystem, uid);

	}
	
	private void changeStatusTStateType( String location, 
	                                    StatusTStateType startState, 
	                                    StatusTStateType targetState, 
	                                    String subsystem, 
	                                    String uid ) 
		throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx{
		
		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		ProjectStatus projectStatus = Utils.makeProjectStatus();
		projectStatus.getStatus().setState(startState);
		
		stateEngine.changeState((ProjectStatusEntityT)projectStatus.getProjectStatusEntity(), 
				targetState, subsystem, uid);
	}
	
}
