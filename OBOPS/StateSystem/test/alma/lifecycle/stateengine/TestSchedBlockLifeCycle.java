/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;

/**
 * 
 * TestSchedBlockLifeCycle tests all the state transitions that are initiated in
 * the SchedBlock Life cycle. This test runs using the production UML state
 * machine diagram.
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision: 1.8 $
 */

// $Id: TestSchedBlockLifeCycle.java,v 1.3.2.4 2009/08/03 15:41:53 rkurowsk Exp
// $
public class TestSchedBlockLifeCycle extends TestCase {

	Logger logger = Logger.getLogger(TestSchedBlockLifeCycle.class.getName());

	public void testReadyToRunning() {

		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(StatusTStateType.READY,
				StatusTStateType.RUNNING, Location.OSF, Subsystem.SCHEDULING,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus()
				.getState());
		
		ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.READY,
				StatusTStateType.RUNNING, 
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());

	}

	public void testPhase2SubmittedToPartiallyObserved() {

		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.REPAIRED, 
				StatusTStateType.PHASE2SUBMITTED, 1, 
				StatusTStateType.FULLYOBSERVED, 2,
				StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.READY, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.ARCA);
		
		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());
		
	}

	public void testBroken2FullyObserved() {

		// test if bubble up changes project status
//		e(StatusTStateType projStartState,
//				StatusTStateType ousStartState, int nrOus,
//				StatusTStateType allSbInitState, int nrSb,
//				StatusTStateType testSbStartState, 
//				StatusTStateType sbTargetState,
//				String location, String subsystem, String uid) {
		ProjectStatus ps = changeSbState(
				StatusTStateType.BROKEN, 
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.RUNNING, 2,
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO, 
				Subsystem.SCHEDULING,
				Role.AOD);
		
		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());		
		
		
		ps = changeSbState(
				StatusTStateType.BROKEN, 
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.RUNNING, 1,
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO, 
				Subsystem.SCHEDULING,
				Role.AOD);
		
		assertEquals(StatusTStateType.FULLYOBSERVED, ps.getStatus().getState());

		ps = changeSbState(
				StatusTStateType.BROKEN, 
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.BROKEN, 2,
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO, 
				Subsystem.SCHEDULING,
				Role.AOD);
		
		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());
		
	}
	
	public void testPhase2SubmittedToBroken() {

		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.REPAIRED, 
				StatusTStateType.PHASE2SUBMITTED, 1, 
				StatusTStateType.PHASE2SUBMITTED, 2,
				StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.BROKEN, 
				Location.OSF, 
				Subsystem.OBOPS,
				Role.AOD);
		
		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());
		
	}
	
	public void testPhase2SubmittedToDeleted() {

		// test if ous guard prevents bubble up to change ous status
		changeSbState(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.DELETED, Location.SCO, Subsystem.OBSPREP,
				Role.PI);

		// find the DELETED sb's parent ous, it should still be PHASE2SUBMITTED
		// as another child sb is still PHASE2SUBMITTED.
		checkParentAfterBubbleUp(StatusTStateType.DELETED,
				StatusTStateType.PHASE2SUBMITTED);

		// test if bubble up changes ous status when all children are deleted
		changeSbState(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.PHASE2SUBMITTED, 2,
				StatusTStateType.PHASE2SUBMITTED, 1,
				StatusTStateType.PHASE2SUBMITTED, 
				StatusTStateType.DELETED,
				Location.SCO, Subsystem.OBSPREP, Role.PI);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.DELETED,
				StatusTStateType.DELETED);
	}		
	
// TODO: this will test if the delete bubbles up to the top OUS, waiting for feedback before introducing it. (RK)
	
//	public void testPhase2SubmittedToDeletedBubbleUpToTopOus() {
//		
//		// test if bubble up changes top ous status when all children are deleted
//		ProjectStatus ps = changeSbState(StatusTStateType.PHASE2SUBMITTED,
//				StatusTStateType.PHASE2SUBMITTED, 1,
//				StatusTStateType.PHASE2SUBMITTED, 1,
//				StatusTStateType.PHASE2SUBMITTED, 
//				StatusTStateType.DELETED,
//				Location.SCO, Subsystem.OBSPREP, Role.PI);
//		
//		OUSStatus ousStatus = Utils.findOUSStatus(ps.getObsProgramStatusRef().getEntityId());
//		assertEquals(StatusTStateType.DELETED, ousStatus.getStatus().getState());
//		
//
//	}

	public void testRunningToSuspended() {

		changeSbState(StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.RUNNING,
				StatusTStateType.SUSPENDED, 
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);

	}

	public void testSuspendedToBroken() {

		ProjectStatus ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.BROKEN, 
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);

		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());
		
		ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.BROKEN, 
				Location.OSF, 
				Subsystem.OBOPS,
				Role.AOD);

		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());

	}

	public void testSuspendedToReady() {

		ProjectStatus ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.READY, 
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());
		
		ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.READY, 
				Location.OSF, 
				Subsystem.OBOPS,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());

	}
	
	public void testSuspendedToFullyObserved() {

		ProjectStatus ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.FULLYOBSERVED, 
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());
		
		ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.SUSPENDED,
				StatusTStateType.FULLYOBSERVED, 
				Location.OSF, 
				Subsystem.OBOPS,
				Role.AOD);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());

		// test if bubble up changes project status to FULLYOBSERVED
		ps = changeSbState(
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 1, 
				StatusTStateType.PARTIALLYOBSERVED, 1,
				StatusTStateType.SUSPENDED, 
				StatusTStateType.FULLYOBSERVED,
				Location.OSF, 
				Subsystem.SCHEDULING,
				Role.AOD);
		
		assertEquals(StatusTStateType.FULLYOBSERVED, ps.getStatus().getState());

	}

	public void testRunningToReady() {

		changeSbState(StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.RUNNING,
				StatusTStateType.READY, 
				Location.OSF,
				Subsystem.SCHEDULING, Role.AOD);

	}

	public void testBrokenToPhase2Submitted() {

		// test if bubble up changes project status, only break one SB
		ProjectStatus ps = changeSbState(
				StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.BROKEN, 1,
				StatusTStateType.BROKEN, 
				StatusTStateType.PHASE2SUBMITTED,
				Location.SCO, 
				Subsystem.OBSPREP, 
				Role.PI);

		// find the PHASE2SUBMITTED sb's parent ous, it should still be
		// PHASE2SUBMITTED
		checkParentAfterBubbleUp(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.PHASE2SUBMITTED);

		assertEquals(StatusTStateType.REPAIRED, ps.getStatus().getState());

		ps = changeSbState(
				StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 2, 
				StatusTStateType.BROKEN, 1,
				StatusTStateType.BROKEN, 
				StatusTStateType.PHASE2SUBMITTED,
				Location.SCO, 
				Subsystem.OBSPREP, 
				Role.PI);

		checkParentAfterBubbleUp(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.PHASE2SUBMITTED);
		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());
	}

	public void testBrokenToFullyObserved() {

		changeSbState(
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO,
				Subsystem.OBSPREP, 
				Role.PI);

		// test if bubble up changes ous status to FULLYOBSERVED if all children
		// are FULLYOBSERVED
		changeSbState(
				StatusTStateType.BROKEN, 
				StatusTStateType.BROKEN, 2, 
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO,
				Subsystem.OBSPREP, 
				Role.PI);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.FULLYOBSERVED,
				StatusTStateType.FULLYOBSERVED);

		// test if bubble up changes ous status to PARTIALLYOBSERVED when one
		// broken child FULLYOBSERVED but other is still PARTIALLYOBSERVED
		ProjectStatus ps = changeSbState(
				StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.RUNNING, 2, 
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO,
				Subsystem.OBSPREP, 
				Role.PI);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED);
		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus().getState());

		// test if bubble up changes ous status to PARTIALLYOBSERVED but leaves
		// Project BROKEN if another OUS is still Broken
		ps = changeSbState(StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 2, 
				StatusTStateType.RUNNING,	2, 
				StatusTStateType.BROKEN,
				StatusTStateType.FULLYOBSERVED, 
				Location.SCO,
				Subsystem.OBSPREP, 
				Role.PI);

		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());

		// test if bubble up changes ous status to FULLYOBSERVED when one
		// broken child FULLYOBSERVED but other is still FULLYOBSERVED
		ps = changeSbState(StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 1, 
				StatusTStateType.FULLYOBSERVED, 2,
				StatusTStateType.BROKEN, 
				StatusTStateType.FULLYOBSERVED,
				Location.SCO, 
				Subsystem.OBSPREP, 
				Role.PI);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.FULLYOBSERVED,
				StatusTStateType.FULLYOBSERVED);
		assertEquals(StatusTStateType.FULLYOBSERVED, ps.getStatus().getState());

		// test if bubble up changes ous status to FULLYOBSERVED but leaves
		// Project Broken
		// if another OUS is still Broken
		ps = changeSbState(StatusTStateType.BROKEN,
				StatusTStateType.BROKEN, 2, 
				StatusTStateType.FULLYOBSERVED, 2,
				StatusTStateType.BROKEN, 
				StatusTStateType.FULLYOBSERVED,
				Location.SCO, 
				Subsystem.OBSPREP, 
				Role.PI);

		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());

	}


	public void testProcessedToReady() {

		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
				StatusTStateType.READY, 
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);

		assertEquals(StatusTStateType.PARTIALLYOBSERVED, ps.getStatus()
				.getState());

	}

	public void testProcessedToBroken() {

		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
				StatusTStateType.BROKEN, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);

		assertEquals(StatusTStateType.BROKEN, ps.getStatus().getState());

	}
	
	public void testCSVReadyToCSVRunning() {
			
		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVREADY,
				2,
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVRUNNING, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);

		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());
		
		// Transition performed by other roles/subsystems/locations
		ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVREADY,
				2,
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVRUNNING, 
				Location.OSF, 
				Subsystem.OBSPREP,
				Role.AOD);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());			
	}
	
	public void testCSVRunningToCSVSuspended() {
		
		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVRUNNING,
				StatusTStateType.CSVSUSPENDED, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());
		// Transition performed by other roles/subsystems/locations
		ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVRUNNING,
				StatusTStateType.CSVSUSPENDED, 
				Location.OSF, 
				Subsystem.OBSPREP,
				Role.AOD);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());		

	}
	
	public void testCSVRunningToCSVReady() {
		
		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVRUNNING,
				StatusTStateType.CSVREADY, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());
		// Transition performed by other roles/subsystems/locations
		ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVRUNNING,
				StatusTStateType.CSVREADY, 
				Location.OSF, 
				Subsystem.OBSPREP,
				Role.AOD);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());		

	}

	public void testCSVSuspendedToCSVReady() {
		
		// test if bubble up changes project status
		ProjectStatus ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVSUSPENDED,
				StatusTStateType.CSVREADY, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());
		// Transition performed by other roles/subsystems/locations
		ps = changeSbState(
				StatusTStateType.CSVREADY,
				StatusTStateType.CSVREADY,
				1,
				StatusTStateType.CSVRUNNING,
				2,
				StatusTStateType.CSVSUSPENDED,
				StatusTStateType.CSVREADY, 
				Location.OSF, 
				Subsystem.OBSPREP,
				Role.AOD);
		
		assertEquals(StatusTStateType.CSVREADY, ps.getStatus().getState());		

	}
	
	public void testProcessedToVerified() {

		// test if guard prevents bubble up
		ProjectStatus ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
				StatusTStateType.VERIFIED, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);

		assertEquals(StatusTStateType.PROCESSED, ps.getStatus().getState());

		// check if bubble up occurs to parent ous only (not project
		ps = changeSbState(StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 2, 
				StatusTStateType.PROCESSED, 1,
				StatusTStateType.PROCESSED, 
				StatusTStateType.VERIFIED,
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.VERIFIED,
				StatusTStateType.VERIFIED);
		assertEquals(StatusTStateType.PROCESSED, ps.getStatus().getState());

		// test if bubble up changes project status
		ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 1, 
				StatusTStateType.PROCESSED, 1,
				StatusTStateType.PROCESSED, 
				StatusTStateType.VERIFIED,
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);

		assertEquals(StatusTStateType.VERIFIED, ps.getStatus().getState());

	}
	
	public void testProcessedToVerificationFailure() {

		// test if guard prevents bubble up
		ProjectStatus ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 
				StatusTStateType.PROCESSED,
				StatusTStateType.VERIFICATIONFAILURE, 
				Location.SCO, 
				Subsystem.OBOPS,
				Role.QAA);

		assertEquals(StatusTStateType.PROCESSED, ps.getStatus().getState());

		// check if bubble up occurs to parent ous only (not project
		ps = changeSbState(StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 2, 
				StatusTStateType.PROCESSED, 1,
				StatusTStateType.PROCESSED, 
				StatusTStateType.VERIFICATIONFAILURE,
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);

		// find a DELETED sb's parent ous
		checkParentAfterBubbleUp(StatusTStateType.VERIFICATIONFAILURE,
				StatusTStateType.VERIFIED);
		assertEquals(StatusTStateType.PROCESSED, ps.getStatus().getState());

		// test if bubble up changes project status
		ps = changeSbState(
				StatusTStateType.PROCESSED,
				StatusTStateType.PROCESSED, 1, 
				StatusTStateType.PROCESSED, 1,
				StatusTStateType.PROCESSED, 
				StatusTStateType.VERIFICATIONFAILURE,
				Location.SCO, 
				Subsystem.OBOPS, 
				Role.QAA);

		assertEquals(StatusTStateType.VERIFIED, ps.getStatus().getState());

	}
	
	@SuppressWarnings("unused")
    public void testProcessedToVerified02() {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
	    System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
	    stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				StatusTStateType.OBSERVINGTIMEDOUT, 
				StatusTStateType.FULLYOBSERVED, 
				StatusTStateType.FULLYOBSERVED);
	
		try {
			OUSStatus level0Ous = Utils.findOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatus level1Ous = Utils.findOUSStatus(level0Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous1 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous2 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(1).getEntityId());
			
			SBStatus sb1 = Utils.findSBStatus(level2Ous1.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			SBStatus sb2 = Utils.findSBStatus(level2Ous1.getOUSStatusChoice().getSBStatusRef(1).getEntityId());
			SBStatus sb3 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			SBStatus sb4 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(1).getEntityId());
			
			level2Ous2.getStatus().setState(StatusTStateType.PROCESSED);
			sb3.getStatus().setState(StatusTStateType.PROCESSED);
			sb4.getStatus().setState(StatusTStateType.VERIFIED);
			
			logger.info("project status before change: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
	
			stateEngine.changeState(sb3.getSBStatusEntity(), 
					StatusTStateType.VERIFIED, Subsystem.OBOPS, Role.QAA);
	
			logger.info("project status after VERIFIED 1: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	// /////////////////////////////// utils ////////////////////////////

	private void checkParentAfterBubbleUp(StatusTStateType childsState,
			StatusTStateType parentsExpectedState) {

		for (SBStatus sb : Utils.getSBStatuses()) {
			if (sb.getStatus().getState().equals(childsState)) {
				OUSStatus ous = Utils.findOUSStatus(sb
						.getContainingObsUnitSetRef().getEntityId());
				assertEquals("Bubble Up operation from SB to OUS failed",
						parentsExpectedState, ous.getStatus().getState());
			}
		}
	}

	private ProjectStatus changeSbState(StatusTStateType startState,
			StatusTStateType targetState, String location, String subsystem,
			String role) {

		return changeSbState(startState, startState, 2, startState, 2,
				startState, targetState, location, subsystem, role);
	}

	private ProjectStatus changeSbState(StatusTStateType projStartState,
			StatusTStateType ousStartState, StatusTStateType allSbInitState,
			StatusTStateType sbTargetState, String location, String subsystem,
			String role) {

		return changeSbState(projStartState, ousStartState, 2, allSbInitState,
				2, allSbInitState, sbTargetState, location, subsystem, role);
	}

	private ProjectStatus changeSbState(StatusTStateType projStartState,
			StatusTStateType ousStartState, int nrOus,
			StatusTStateType allSbInitState, int nrSb,
			StatusTStateType testSbStartState, 
			StatusTStateType sbTargetState,
			String location, String subsystem, String uid) {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, location );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		ProjectStatus projectStatus = Utils.makeProjectStatus(nrOus, nrSb,
				projStartState, ousStartState, allSbInitState);

		try {
			// all the sb's are initialized to the same start state
			// for some tests it is necessary to init the state of
			// the target SB to something different before the change
			SBStatus sbStatus = Utils.getSBStatuses().iterator().next();
			sbStatus.getStatus().setState(testSbStartState);

			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));

			stateEngine.changeState((SBStatusEntityT) sbStatus
					.getSBStatusEntity(), sbTargetState, subsystem, uid);

			assertEquals(sbTargetState, sbStatus.getStatus().getState());

			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return projectStatus;
	}
}
