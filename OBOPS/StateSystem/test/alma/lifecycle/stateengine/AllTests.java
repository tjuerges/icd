/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author rkurowsk, June 08, 2009
 * @version $Revision$
 */

// $Id$

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.lifecycle.stateengine" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestStateEngine.class );
        suite.addTestSuite( TestObsProjectLifeCycle.class );
        suite.addTestSuite( TestObsUnitSetLifeCycle.class );
        suite.addTestSuite( TestSchedBlockLifeCycle.class );
        suite.addTestSuite( TestStateEngineWithDb.class );
        //$JUnit-END$
        return suite;
    }

}
