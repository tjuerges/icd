/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;

/**
 * 
 * TestObsProjectLifeCycle tests all the state transitions that are initiated in
 * the ObsProject Life cycle. This test runs using the production UML state
 * machine diagram.
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision$
 */

// $Id: TestObsProjectLifeCycle.java,v 1.3.2.3 2009/08/03 15:41:53 rkurowsk Exp
// $
public class TestObsProjectLifeCycle extends TestCase {

	Logger logger = Logger.getLogger(TestObsProjectLifeCycle.class.getName());

	public void testPhase1SubmittedToApproved() {
		changeProjectState(StatusTStateType.PHASE1SUBMITTED,
				StatusTStateType.APPROVED, Location.SCO,
				Subsystem.OBOPS, Role.DSOA);
	}
	
	public void testApprovedToPhase1Submitted() {
		changeProjectState(StatusTStateType.APPROVED,
				StatusTStateType.PHASE1SUBMITTED, Location.SCO,
				Subsystem.OBOPS, Role.DSOA);
	}
	
// TODO: uncomment once Rejected added to StatusTStateType

//	public void testPhase1SubmittedToRejected() {
//		changeProjectState(StatusTStateType.PHASE1SUBMITTED,
//				StatusTStateType.REJECTED, Location.SCO,
//				Subsystem.OBOPS, Role.DSOA);
//	}
//	
//	public void testRejectedToPhase1Submitted() {
//		changeProjectState(StatusTStateType.REJECTED,
//				StatusTStateType.PHASE1SUBMITTED_TYPE, Location.SCO,
//				Subsystem.OBOPS, Role.DSOA);
//		
//		changeProjectState(StatusTStateType.REJECTED,
//				StatusTStateType.PHASE1SUBMITTED_TYPE, Location.SCO,
//				Subsystem.OBOPS, Role.ASSESSOR);
//
//	}

	public void testApprovedToPhase2Submitted() {
		changeProjectState(StatusTStateType.APPROVED,
				StatusTStateType.PHASE2SUBMITTED, Location.SCO,
				Subsystem.OBSPREP, Role.PI);
	}
	
	// tests trickle down from ObsProject to all ObsUnitSets and SchedBlocks
	public void testPhase2SubmittedToReady() {

		// test successful change from Phase2Submitted to Ready along with
		// trickle down to ObsUnitSets & SchedBlocks
		changeProjectState(
				StatusTStateType.PHASE2SUBMITTED, StatusTStateType.READY,
				Location.SCO, Subsystem.OBOPS, Role.ARCA);

		SBStatus sbStatus = Utils.getSBStatuses().iterator().next();
		assertEquals(StatusTStateType.READY, sbStatus.getStatus().getState());
		
		Utils.reset();
		
		changeProjectState(
				StatusTStateType.PHASE2SUBMITTED, StatusTStateType.READY,
				Location.OSF, Subsystem.SCHEDULING, Role.AOD);

		sbStatus = Utils.getSBStatuses().iterator().next();
		assertEquals(StatusTStateType.READY, sbStatus.getStatus().getState());
	}

	public void testPhase2SubmittedToApproved() {
		changeProjectState(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.APPROVED, Location.SCO, Subsystem.OBOPS,
				Role.ARCA);
	}

	public void testBrokenToRepaired() {
		changeProjectState(StatusTStateType.BROKEN,
				StatusTStateType.REPAIRED, Location.SCO, Subsystem.OBSPREP,
				Role.PI);
	}

	public void testRepairedToBroken() {
		changeProjectState(StatusTStateType.REPAIRED,
				StatusTStateType.BROKEN, Location.OSF, Subsystem.STATE_ENGINE,
				Role.OBSUNITSET);
	}

//	public void testRepairedToPartiallyObserved01() {
//
//		Utils.reset();
//		StateEngineImpl stateEngine = new StateEngineImpl();
//		StateArchive stateArchive = new StateArchiveMockImpl();
//        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
//		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
//		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
//		ProjectStatus projectStatus = Utils.makeProjectStatus(1, 1,
//				StatusTStateType.REPAIRED, StatusTStateType.PHASE2SUBMITTED,
//				StatusTStateType.PHASE2SUBMITTED);
//
//		try {
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//
//			stateEngine.changeState(projectStatus.getProjectStatusEntity(),
//					StatusTStateType.PARTIALLYOBSERVED, Subsystem.OBOPS, "arca");
//
//			assertEquals(StatusTStateType.PARTIALLYOBSERVED, projectStatus
//					.getStatus().getState());
//
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//			SBStatus sbStatus = Utils.getSBStatuses().iterator().next();
//			assertEquals(StatusTStateType.READY, sbStatus.getStatus().getState());
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//
//	}
	
//	public void testRepairedToPartiallyObserved02() {
//
//		Utils.reset();
//		StateEngineImpl stateEngine = new StateEngineImpl();
//		StateArchive stateArchive = new StateArchiveMockImpl();
//        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO );
//		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
//		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
//		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
//				StatusTStateType.REPAIRED, StatusTStateType.PHASE2SUBMITTED,
//				StatusTStateType.PHASE2SUBMITTED);
//
//		try {
//
//			OUSStatus ousProgram = Utils.findOUSStatus(projectStatus
//					.getObsProgramStatusRef().getEntityId());
//			OUSStatus ousPlan = Utils.findOUSStatus(ousProgram
//					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
//			OUSStatus ousChild1 = Utils.findOUSStatus(ousPlan
//					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
//			ousChild1.getStatus().setState(StatusTStateType.PROCESSED);
//			SBStatus sb1 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
//					.getSBStatusRef(0).getEntityId());
//			sb1.getStatus().setState(StatusTStateType.PROCESSED);
//			SBStatus sb2 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
//					.getSBStatusRef(1).getEntityId());
//			sb2.getStatus().setState(StatusTStateType.PROCESSED);
//
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//			
//			stateEngine.changeState(projectStatus.getProjectStatusEntity(),
//					StatusTStateType.PARTIALLYOBSERVED, Subsystem.OBOPS, "arca");
//
//			assertEquals(StatusTStateType.PARTIALLYOBSERVED, projectStatus
//					.getStatus().getState());
//
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//			assertEquals(StatusTStateType.READY, ousPlan.getStatus().getState());
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//
//	}

	public void testAnyStateToObservingTimedOut() {

		changeProjectState(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.OBSERVINGTIMEDOUT, Location.SCO,
				Subsystem.OBOPS, Role.ARCA);

		changeProjectState(StatusTStateType.APPROVED,
				StatusTStateType.OBSERVINGTIMEDOUT, Location.SCO,
				Subsystem.OBOPS, Role.ARCA);

		changeProjectState(StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.OBSERVINGTIMEDOUT, Location.SCO,
				Subsystem.OBOPS, Role.ARCA);

		// new role DSOA
		changeProjectState(StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.OBSERVINGTIMEDOUT, Location.SCO,
				Subsystem.OBOPS, Role.DSOA);
		
		changeProjectState(null, StatusTStateType.OBSERVINGTIMEDOUT,
				Location.SCO, Subsystem.OBOPS, Role.ARCA);
	}

	public void testObservingTimedOutToProcessed() {

		changeProjectState(StatusTStateType.OBSERVINGTIMEDOUT,
				StatusTStateType.PROCESSED, Location.SCO,
				Subsystem.STATE_ENGINE, Role.OBSUNITSET);

	}

	public void testAnyStateToCancelled() {

		changeProjectState(StatusTStateType.PHASE2SUBMITTED,
				StatusTStateType.CANCELED, Location.SCO, Subsystem.OBOPS,
				Role.ARCA);

		changeProjectState(StatusTStateType.APPROVED,
				StatusTStateType.CANCELED, Location.SCO, Subsystem.OBOPS,
				Role.ARCA);
		
		// new role DSOA
		changeProjectState(StatusTStateType.APPROVED,
				StatusTStateType.CANCELED, Location.SCO, Subsystem.OBOPS,
				Role.DSOA);

		
		changeProjectState(null, StatusTStateType.CANCELED, Location.SCO,
				Subsystem.OBOPS, Role.ARCA);
	}

	private ProjectStatus changeProjectState(StatusTStateType startState,
			StatusTStateType targetState, String location, String subsystem,
			String uid) {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
        System.setProperty( Location.RUNLOCATION_PROP, location );
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());

		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2, startState);

		try {
			
			stateEngine.changeState(projectStatus.getProjectStatusEntity(),
					targetState, subsystem, uid);

			assertEquals(targetState, projectStatus.getStatus().getState());

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return projectStatus;
	}

	public void testMixedStatesToObservingTimedOut() {

		String location = Location.SCO;
		String subsystem = Subsystem.OBOPS;
		String uid = "arca";

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
        System.setProperty( Location.RUNLOCATION_PROP, location );
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());

		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED,
				StatusTStateType.PARTIALLYOBSERVED);

		try {
			OUSStatus ousProgram = Utils.findOUSStatus(projectStatus
					.getObsProgramStatusRef().getEntityId());
			OUSStatus ousPlan = Utils.findOUSStatus(ousProgram
					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());

			OUSStatus ousChild1 = Utils.findOUSStatus(ousPlan
					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			ousChild1.getStatus().setState(StatusTStateType.FULLYOBSERVED);
			SBStatus sb1 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
					.getSBStatusRef(0).getEntityId());
			sb1.getStatus().setState(StatusTStateType.FULLYOBSERVED);
			
			SBStatus sb2 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
					.getSBStatusRef(1).getEntityId());
			sb2.getStatus().setState(StatusTStateType.FULLYOBSERVED);

			OUSStatus ousChild2 = Utils.findOUSStatus(ousPlan
					.getOUSStatusChoice().getOUSStatusRef(1).getEntityId());
			
			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));

			stateEngine.changeState(projectStatus.getProjectStatusEntity(),
					StatusTStateType.OBSERVINGTIMEDOUT, subsystem, uid);

			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			assertEquals(StatusTStateType.FULLYOBSERVED, 
					ousChild1.getStatus().getState());
			assertEquals(StatusTStateType.OBSERVINGTIMEDOUT, 
					ousChild2.getStatus().getState());
			assertEquals(StatusTStateType.OBSERVINGTIMEDOUT, 
					ousProgram.getStatus().getState());
			assertEquals(StatusTStateType.OBSERVINGTIMEDOUT, 
					projectStatus.getStatus().getState());

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
//	public void testRepairedAndPhase2SubToReady() {
//
//		String location = Location.SCO;
//		String subsystem = Subsystem.OBOPS;
//		String uid = "arca";
//
//		Utils.reset();
//		StateEngineImpl stateEngine = new StateEngineImpl();
//		StateArchive stateArchive = new StateArchiveMockImpl();
//		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
//        System.setProperty( Location.RUNLOCATION_PROP, location );
//		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
//
//		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
//				StatusTStateType.REPAIRED,
//				StatusTStateType.PHASE2SUBMITTED,
//				StatusTStateType.PHASE2SUBMITTED);
//
//		try {
//			OUSStatus ousProgram = Utils.findOUSStatus(projectStatus
//					.getObsProgramStatusRef().getEntityId());
//			OUSStatus ousPlan = Utils.findOUSStatus(ousProgram
//					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
//
//			OUSStatus ousChild1 = Utils.findOUSStatus(ousPlan
//					.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
//			ousChild1.getStatus().setState(StatusTStateType.READY);
//			SBStatus sb1 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
//					.getSBStatusRef(0).getEntityId());
//			sb1.getStatus().setState(StatusTStateType.READY);
//			
//			SBStatus sb2 = Utils.findSBStatus(ousChild1.getOUSStatusChoice()
//					.getSBStatusRef(1).getEntityId());
//			sb2.getStatus().setState(StatusTStateType.READY);
//
//			
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//
//			stateEngine.changeState(projectStatus.getProjectStatusEntity(),
//					StatusTStateType.PARTIALLYOBSERVED, subsystem, uid);
//
//			logger.info(Utils.getProjectStatusTreeForDisplay(projectStatus));
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//	}
	
	public void testObservingTimedOutToProcessedViaFullyObserved() {

		Utils.reset();
		StateEngineImpl stateEngine = new StateEngineImpl();
		StateArchive stateArchive = new StateArchiveMockImpl();
		stateEngine.setInputUmlFilePath(PRODUCTION_UML);
	    System.setProperty( Location.RUNLOCATION_PROP, Location.TEST );
	    stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
		
		ProjectStatus projectStatus = Utils.makeProjectStatus(2, 2,
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.PARTIALLYOBSERVED, 
				StatusTStateType.READY);
	
		try {
			OUSStatus level0Ous = Utils.findOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatus level1Ous = Utils.findOUSStatus(level0Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous1 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(0).getEntityId());
			OUSStatus level2Ous2 = Utils.findOUSStatus(level1Ous.getOUSStatusChoice().getOUSStatusRef(1).getEntityId());
			
			// setup first OUS and its children
			SBStatus sb1 = Utils.findSBStatus(level2Ous1.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			sb1.getStatus().setState(StatusTStateType.RUNNING);
			
			// setup second OUS & its child SB's (set all to processed)
			level2Ous2.getStatus().setState(StatusTStateType.PROCESSED);
			SBStatus sb3 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(0).getEntityId());
			sb3.getStatus().setState(StatusTStateType.PROCESSED);
			SBStatus sb4 = Utils.findSBStatus(level2Ous2.getOUSStatusChoice().getSBStatusRef(1).getEntityId());
			sb4.getStatus().setState(StatusTStateType.PROCESSED);
			
			logger.info("project status before OBSERVINGTIMEDOUT: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
	
			stateEngine.changeState(projectStatus.getProjectStatusEntity(), 
					StatusTStateType.OBSERVINGTIMEDOUT,	Subsystem.OBOPS, Role.ARCA);
	
			logger.info("project status after OBSERVINGTIMEDOUT: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			stateEngine.changeState(sb1.getSBStatusEntity(),
					StatusTStateType.SUSPENDED,	Subsystem.SCHEDULING, Role.AOD);
			
			logger.info("project status after SB SUSPENDED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			stateEngine.changeState(sb1.getSBStatusEntity(),
					StatusTStateType.FULLYOBSERVED,	Subsystem.SCHEDULING, Role.AOD);
			
			logger.info("project status after SB FULLYOBSERVED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
			stateEngine.changeState(level2Ous1.getOUSStatusEntity(),
					StatusTStateType.FULLYOBSERVED,	Subsystem.OBOPS, Role.QAA);
			
			logger.info("project status after OUS FULLYOBSERVED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));

			stateEngine.changeState(level1Ous.getOUSStatusEntity(), 
					StatusTStateType.PROCESSED,	Subsystem.PIPELINE, Role.QAA);
			
			logger.info("project status after PROCESSED: \n" + Utils.getProjectStatusTreeForDisplay(projectStatus));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
}
