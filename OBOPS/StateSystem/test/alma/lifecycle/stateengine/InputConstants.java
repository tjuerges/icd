/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

/**
 * StateEngine InputConstants
 * 
 * @author rkurowsk, Aug 03, 2009
 * @version $Revision$
 */

// $Id$

public class InputConstants {
	
	public static final String PRODUCTION_UML = "/obs-project-life-cycle.mdxml";
}
