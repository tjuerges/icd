/**
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.stateengine;

import java.util.ArrayList;
import java.util.Collection;

import alma.acs.container.archive.Range;
import alma.acs.entityutil.EntitySerializer;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.obsproject.ObsProgramT;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.obsproject.ObsUnitSetT;
import alma.entity.xmlbinding.obsproject.ObsUnitSetTChoice;
import alma.entity.xmlbinding.obsproposal.InvestigatorT;
import alma.entity.xmlbinding.obsproposal.ObsProposal;
import alma.entity.xmlbinding.obsproposal.ObsProposalEntityT;
import alma.entity.xmlbinding.obsproposal.ObsProposalRefT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SchedBlockEntityT;
import alma.entity.xmlbinding.schedblock.SchedBlockRefT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.SpringTestCase;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.xmlentity.XmlEntityStruct;

/**
 * Basic test for StateEngine insertOrUpdate()
 * 
 * @author rkurowsk, Jul 3, 2009
 * @version $Revision: 1.2 $
 */

// $Id: TestStateEngineInsertOrUpdate.java,v 1.2 2011/02/03 10:28:55 rkurowsk Exp $

public class TestStateEngineInsertOrUpdate extends SpringTestCase {

	public static final String OBOPS_ARCHIVE_ID = "X58";
	private static final boolean ROLLBACK = true;
	private StateEngine stateEngine;
	
	public StateEngine getStateEngine() {
		return stateEngine;
	}


	public void setStateEngine(StateEngine stateEngine) {
		this.stateEngine = stateEngine;
	}

	@Override
    public void onSetUpBeforeTransaction() {
    	this.setDefaultRollback(ROLLBACK);
    }
    
	@Override
	protected void onSetUpInTransaction() throws Exception {
	    Utils.reset();
		ops  = Utils.makeProjectStatus( 1, 2 );
        sbs  = Utils.findSBStatus( ops );
        ouss = Utils.findOUSStatus( ops );

        sbsList = Utils.getSBStatuses().toArray( new SBStatus[0] );
        oussList = Utils.getOUSStatuses().toArray( new OUSStatus[0] );
        
        System.setProperty( Location.RUNLOCATION_PROP, Location.SCO);
        stateEngine.initStateEngine(logger, getStateArchive(), new RoleProviderMock());
        
	}
	
	@Override
	protected void onTearDownInTransaction() throws Exception {
		// do nothing
	}
	
    public void testInsert() throws Exception {

    	EntitySerializer serializer = EntitySerializer.getEntitySerializer( logger );
    	
    	ObsProposal prop = makeTestObsProposal();
    	SchedBlock sb0 = makeTestSchedBlock();
        SchedBlock sb1 = makeTestSchedBlock();
        ObsProject proj = makeTestObsProject( prop, sb0, sb1);
        
        XmlEntityStruct[] archiveEntities = new XmlEntityStruct[4];
        archiveEntities[0] = serializer.serializeEntity(proj);
        archiveEntities[1] = serializer.serializeEntity(prop);
        archiveEntities[2] = serializer.serializeEntity(sb0);
        archiveEntities[3] = serializer.serializeEntity(sb1);
        
    	stateEngine.insertOrUpdate(archiveEntities, "testUser", ops, oussList, sbsList, null);

    }
    
    public void testInsertUpdate() throws Exception {
    	
    	EntitySerializer serializer = EntitySerializer.getEntitySerializer( logger );
    	
    	ObsProposal prop = makeTestObsProposal();
    	SchedBlock sb0 = makeTestSchedBlock();
        SchedBlock sb1 = makeTestSchedBlock();
        ObsProject proj = makeTestObsProject( prop, sb0, sb1);
        
        XmlEntityStruct[] archiveEntities = new XmlEntityStruct[3];
        archiveEntities[0] = serializer.serializeEntity(proj);
        archiveEntities[1] = serializer.serializeEntity(prop);
        archiveEntities[2] = serializer.serializeEntity(sb0);
        
        stateEngine.insertOrUpdate(archiveEntities, "testUser", ops, oussList, sbsList, null);
        
        Collection<StateTransitionParam> stp = new ArrayList<StateTransitionParam>();
        stp.add(new StateTransitionParam(ops.getProjectStatusEntity(), 
        		StatusTStateType.READY, Subsystem.OBOPS, Role.ARCA.toString()));
        
        archiveEntities = new XmlEntityStruct[1];
        archiveEntities[0] = serializer.serializeEntity(sb1);
        
        stateEngine.insertOrUpdate(archiveEntities, "testUser", null, null, null, stp);
        
    }
    
    /**
     * Utility method
     * @return A bogus ObsProject
     */
    static ObsProject makeTestObsProject() {
        
        String uid = createEntityId();
        ObsProjectEntityT entity = new ObsProjectEntityT();
        entity.setEntityId( uid );
        
        ObsProgramT program = new ObsProgramT();
        program.setObsPlan( new ObsUnitSetT() );
        
        ObsProject prj = new ObsProject();
        prj.setObsProjectEntity( entity );
        prj.setProjectName( "Project " + uid );
        prj.setObsProgram( program );
        
        return prj;
    }
    
    /**
     * Utility method
     * @return A bogus ObsProject
     */
    static ObsProject makeTestObsProject( ObsProposal prop,
                                                    SchedBlock... sbs ) {
        ObsProject prj = makeTestObsProject();
        
        // ObsProposal
        //----------------------
        ObsProposalRefT opr = new ObsProposalRefT();
        opr.setEntityId( prop.getObsProposalEntity().getEntityId() );
        prj.setObsProposalRef( opr );
        
        // The SBs, grouped in the 'top' ObsUnitSet
        //-------------------------------------------------
        ObsUnitSetTChoice choice = new ObsUnitSetTChoice();
        ObsUnitSetT top = new ObsUnitSetT();
        top.setObsUnitSetTChoice( choice );
        top.setEntityPartId( "do-not-care" );
        for( int i = 0; i < sbs.length; i++ ) {
            
            SchedBlock sb = (SchedBlock) sbs[i];
            SchedBlockRefT sbr = new SchedBlockRefT();
            sbr.setEntityId( sb.getSchedBlockEntity().getEntityId() );
            choice.addSchedBlockRef( sbr );
        }

        // Add our 'top' ObsUnitSet to the obs plan
        //-------------------------------------------------
        ObsUnitSetTChoice topc = new ObsUnitSetTChoice();
        topc.addObsUnitSet( top );
        ObsUnitSetT plan = prj.getObsProgram().getObsPlan();
        plan.setObsUnitSetTChoice( topc );
        plan.setEntityPartId( "do-not-care" );
        
        // Build and set the obs. program
        //-------------------------------------------------
        ObsProgramT program = new ObsProgramT();
        program.setObsPlan( plan );
        prj.setObsProgram( program );
        
        return prj;
    }

    /**
     * Utility method
     * @return A bogus ObsProposal
     */
    static ObsProposal makeTestObsProposal() {
        String uid = createEntityId();
        ObsProposalEntityT entity = new ObsProposalEntityT();
        entity.setEntityId( uid );
        ObsProposal proposal = new ObsProposal();
        proposal.setObsProposalEntity( entity );
        proposal.setTitle( "Observing the heavens" );
        InvestigatorT pi = new InvestigatorT();
        pi.setFullName("Tycho Brahe" );
        proposal.setPrincipalInvestigator(pi );
        return proposal;
    }

    /**
     * Utility method
     * @return A bogus SchedBlock
     */
    static SchedBlock makeTestSchedBlock() {
        String uid = createEntityId();
        SchedBlockEntityT entity = new SchedBlockEntityT();
        entity.setEntityId( uid );
        SchedBlock prj = new SchedBlock();
        prj.setSchedBlockEntity( entity );
        return prj;
    }
    
	/**
	 * Utility method
	 * @return A unique entity ID
	 */
	public static String createEntityId() {
	    int   rangeID = (int) (Math.random() * 1000);
	    int   localID = (int) (Math.random() * 1000);
	    String uid = Range.generateUID( OBOPS_ARCHIVE_ID, 
	                                    Integer.toHexString( rangeID ), 
	                                    localID );
	    return uid;
	}
}
