/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle;

import static alma.lifecycle.config.SpringConstants.TEST_STATE_SYSTEM_SPRING_CONFIG;

import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.archive.database.helpers.wrappers.AbstractDbConfig;
import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.lifecycle.stateengine.StateEngine;
import alma.lifecycle.stateengine.StateEngineImpl;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.obops.utils.HsqldbUtilities;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

/**
 * 
 * TestStateSystem
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision: 1.9 $
 */

// $Id: TestStateSystem.java,v 1.9 2011/02/03 10:28:56 rkurowsk Exp $
public class TestStateSystem extends TestCase {

	Logger logger = Logger.getLogger(TestStateSystem.class.getName());
	
    private static final String DDL = "sql/hsqldb-ddl.sql";
    
    private StateSystem stateSystem;

    private StateArchive stateArchive;

    private StateEngine stateEngine;
    
    public void setUp() {

    	if( !StateSystemContextFactory.INSTANCE.isInitialized() ) {
            AbstractDbConfig dbConfig = null;
            try {
                dbConfig = new StateArchiveDbConfig( logger);
            }
            catch( DbConfigException e ) {
                e.printStackTrace();
            }
            StateSystemContextFactory.INSTANCE.init( TEST_STATE_SYSTEM_SPRING_CONFIG, dbConfig );
        }
    	System.setProperty( Location.RUNLOCATION_PROP, Location.TEST );
    	stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
    	stateArchive.initStateArchive(logger);
    	stateEngine  = StateSystemContextFactory.INSTANCE.getStateEngine();
		stateEngine.initStateEngine(logger, stateArchive, new RoleProviderMock());
    	stateSystem  = new StateSystemBaseImpl( stateArchive, stateEngine, logger );
    	
    	// Now on to something really ugly
    	StateEngineImpl.setCanBeReinitialized();
    	
        try {
        	String url = StateSystemContextFactory.INSTANCE.getConnectionUrl();
			HsqldbUtilities.createDatabase(url, DDL);    	
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
    	
    }

    // good case
    public void testInitialization() throws AcsJNotAuthorizedEx {

        System.setProperty( Location.RUNLOCATION_PROP, "sCo" ); // case-insensitive
        stateSystem.initStateArchive( logger );
        stateSystem.initStateEngine( logger, stateArchive, new RoleProviderMock());
        assertEquals( Location.SCO, stateSystem.getRunLocation() );
    }

    // bad case1 : no geo location
    public void testBadInitialization01() throws AcsJNotAuthorizedEx {
        
        // if the Location.RUNLOCATION_VAR env variable is set
        // (as is the case for 'make test') this test is not
        // meaningful -- we cannot unset it
        if( System.getenv( Location.RUNLOCATION_VAR ) != null ) {
            return;
        }
        
        System.clearProperty( Location.RUNLOCATION_PROP );
        stateSystem.initStateArchive( logger );
        try {
            stateSystem.initStateEngine( logger, stateArchive, new RoleProviderMock());
            fail( "Expected RuntimeException" );
        }
        catch( RuntimeException e ) {
            // expected
            assertTrue( e.getMessage().contains( "not set or empty" ));
        }
    }

    // bad case 2: invalid geo location
    public void testBadInitialization02() throws AcsJNotAuthorizedEx {
        
        System.setProperty( Location.RUNLOCATION_PROP, "bogus" );
        stateSystem.initStateArchive( logger );
        try {
            stateSystem.initStateEngine( logger, stateArchive, new RoleProviderMock() );
            fail( "Expected RuntimeException" );
        }
        catch( RuntimeException e ) {
            // expected
            assertTrue( e.getMessage().contains( "Invalid value" ));
        }
    }
    
    public void testChangeProjectStatus(){
    	Utils.reset();
    	ProjectStatus ps = Utils.makeProjectStatus();
    	try {
			stateSystem.insert(ps, 
					Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
			
			stateSystem.changeState(ps.getProjectStatusEntity(), 
					StatusTStateType.READY, Subsystem.OBOPS, "arca");
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("changeState for ProjectStatus failed");
		}
    	
    }
    
    public void testChangeOusStatus(){
    	Utils.reset();
    	ProjectStatus ps = Utils.makeProjectStatus();
    	try {
			stateSystem.insert(ps, 
					Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
			
			OUSStatus ous = Utils.findOUSStatus(ps);
			
			stateSystem.changeState(ous.getOUSStatusEntity(), 
					StatusTStateType.READY, Subsystem.STATE_ENGINE, Role.OBSPROJECT);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("changeState for OUS failed");
		}
    	
    }
    
    
    public void testChangeSbStatus(){
    	Utils.reset();
    	ProjectStatus ps = Utils.makeProjectStatus();
    	try {
			stateSystem.insert(ps, 
					Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
			
			SBStatus sb = Utils.findSBStatus(ps);
			
			stateSystem.changeState(sb.getSBStatusEntity(), 
					StatusTStateType.READY, Subsystem.STATE_ENGINE, Role.OBSUNITSET);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("changeState for OUS failed");
		}
    	
    }
}