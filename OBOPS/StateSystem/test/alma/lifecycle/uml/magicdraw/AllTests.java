/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.uml.magicdraw;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author rkurowsk, June 08, 2009
 * @version $Revision$
 */

// $Id$

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.lifecycle.uml.magicdraw" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestMagicDrawStateDiagramParser.class );

        //$JUnit-END$
        return suite;
    }

}
