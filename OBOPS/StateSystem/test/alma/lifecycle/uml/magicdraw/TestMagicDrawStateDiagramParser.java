/**
 * TestMagicDrawStateDiagramParser.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.uml.magicdraw;

import static alma.lifecycle.stateengine.InputConstants.PRODUCTION_UML;

import java.util.Map;
import java.util.logging.Logger;

import junit.framework.TestCase;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.lifecycle.uml.dto.UmlLifeCycles;
import alma.lifecycle.uml.dto.UmlStateTransition;

/**
 *  
 *  Unit test for MagicDrawStateDiagramParser
 *  
 * @author rkurowsk, March 06, 2009
 * @version $Revision$
 */

// $Id$
public class TestMagicDrawStateDiagramParser extends TestCase {

	//obsProjectLifecycle states
	// AnyStateToCanceled
	private static final String CANCELED_STATE = "Canceled";
	private static final String ANYSTATE_STATE = "AnyState";
	private static final String ANYSTATE_TO_CANCELED = "AnyStateToCanceled";
	private static final String ANYSTATE_TO_CANCELED_SUBSYSTEM = Subsystem.OBOPS;
	private static final String ANYSTATE_TO_CANCELED_ROLE =  Role.ARCA;
	private static final String ANYSTATE_TO_CANCELED_LOCATION = Location.SCO;
	
	// ApprovedToPhase2Submitted
	private static final String PHASE2SUBMITTED_STATE = "Phase2Submitted";
	private static final String APPROVED_STATE = "Approved";
	private static final String APPROVED_TO_PHASE2SUBMITTED = "ApprovedToPhase2Submitted";
	private static final String APPROVED_TO_PHASE2SUBMITTED_SUBSYSTEM = Subsystem.OBSPREP;
	private static final String APPROVED_TO_PHASE2SUBMITTED_ROLE = Role.PI;
	private static final String APPROVED_TO_PHASE2SUBMITTED_LOCATION = Location.SCO;
	
	//Phase2SubmittedToApproved
	private static final String PHASE2SUBMITTED_TO_APPROVED = "Phase2SubmittedToApproved";
	private static final String PHASE2SUBMITTED_TO_APPROVED_SUBSYSTEM = Subsystem.OBOPS;
	private static final String PHASE2SUBMITTED_TO_APPROVED_ROLE = Role.ARCA;
	private static final String PHASE2SUBMITTED_TO_APPROVED_LOCATION = Location.SCO;

	
	public TestMagicDrawStateDiagramParser() {}

	public void testParseProductionUml() {

		MagicDrawStateDiagramParser parser = new MagicDrawStateDiagramParser(Logger.getAnonymousLogger());
		try {
			
			UmlLifeCycles lc = parser.parseInputUml(PRODUCTION_UML);
			
		//	System.out.println(lc.toString());
			
			// check the number of states
			assertEquals("ObsProjectLifecycle size incorrect", 13, lc.getObsProjectLifecycle().values().size());
			assertEquals("ObsUnitSetLifecycle size incorrect", 11, lc.getObsUnitSetLifecycle().values().size());
			assertEquals("SchedBlockLifecycle size incorrect", 10, lc.getSchedBlockLifecycle().values().size());

			// do some ObsProjectLifecycle specific tests
			Map<String, Map<String, UmlStateTransition>> obsProjectLifeCycle = lc.getObsProjectLifecycle();
			
			Map<String, UmlStateTransition> cancelledTargetStateTransitions = obsProjectLifeCycle.get(CANCELED_STATE);
			UmlStateTransition anyStateToCanceled = cancelledTargetStateTransitions.get(ANYSTATE_STATE);
			assertNotNull("Missing ObsProjectLifecycle transition: " + ANYSTATE_TO_CANCELED, anyStateToCanceled);
			assertTrue("Invalid subsystem for " + ANYSTATE_TO_CANCELED, anyStateToCanceled.getPermittedSubSystems().contains(ANYSTATE_TO_CANCELED_SUBSYSTEM));
			assertTrue("Invalid role for " + ANYSTATE_TO_CANCELED, anyStateToCanceled.getPermittedRoles().contains(ANYSTATE_TO_CANCELED_ROLE));
			assertTrue("Invalid location for " + ANYSTATE_TO_CANCELED, anyStateToCanceled.getPermittedLocations().contains(ANYSTATE_TO_CANCELED_LOCATION));
			
			Map<String, UmlStateTransition> phase2SubmittedTargetStateTransitions = obsProjectLifeCycle.get(PHASE2SUBMITTED_STATE);
			UmlStateTransition approvedToPhase2Submitted = phase2SubmittedTargetStateTransitions.get(APPROVED_STATE);
			assertNotNull("Missing ObsProjectLifecycle transition: " + APPROVED_TO_PHASE2SUBMITTED , approvedToPhase2Submitted);
			assertTrue("Invalid subsystem for " + APPROVED_TO_PHASE2SUBMITTED, approvedToPhase2Submitted.getPermittedSubSystems().contains(APPROVED_TO_PHASE2SUBMITTED_SUBSYSTEM));
			assertTrue("Invalid role for " + APPROVED_TO_PHASE2SUBMITTED, approvedToPhase2Submitted.getPermittedRoles().contains(APPROVED_TO_PHASE2SUBMITTED_ROLE));
			assertTrue("Invalid location for " + APPROVED_TO_PHASE2SUBMITTED, approvedToPhase2Submitted.getPermittedLocations().contains(APPROVED_TO_PHASE2SUBMITTED_LOCATION));
			
			Map<String, UmlStateTransition> approvedTargetStateTransitions  = obsProjectLifeCycle.get(APPROVED_STATE);
			UmlStateTransition phase2SubmittedToApproved = approvedTargetStateTransitions.get(PHASE2SUBMITTED_STATE);
			assertNotNull("Missing ObsProjectLifecycle transition: " + PHASE2SUBMITTED_TO_APPROVED , phase2SubmittedToApproved);
			assertTrue("Invalid subsystem for " + PHASE2SUBMITTED_TO_APPROVED, phase2SubmittedToApproved.getPermittedSubSystems().contains(PHASE2SUBMITTED_TO_APPROVED_SUBSYSTEM));
			assertTrue("Invalid role for " + PHASE2SUBMITTED_TO_APPROVED, phase2SubmittedToApproved.getPermittedRoles().contains(PHASE2SUBMITTED_TO_APPROVED_ROLE));
			assertTrue("Invalid location for " + PHASE2SUBMITTED_TO_APPROVED, phase2SubmittedToApproved.getPermittedLocations().contains(PHASE2SUBMITTED_TO_APPROVED_LOCATION));
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}

