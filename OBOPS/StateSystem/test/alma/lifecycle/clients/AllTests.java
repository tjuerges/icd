/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jun 22, 2009
 * @version $Revision$
 */

// $Id$

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.lifecycle.clients" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestStateSystemCLI.class );
        //$JUnit-END$
        return suite;
    }

}
