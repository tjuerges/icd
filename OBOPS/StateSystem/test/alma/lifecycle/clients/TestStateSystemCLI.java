/**
 * TestStateArchiveCLI.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.PersistenceTestCase;
import alma.lifecycle.StateSystem;
import alma.lifecycle.StateSystemBaseImpl;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.StateEngine;
import alma.lifecycle.stateengine.StateEngineMockImpl;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;

/**
 * @author amchavan, Jun 22, 2009
 * @version $Revision$
 */

// $Id$

public class TestStateSystemCLI extends PersistenceTestCase {
    
	Logger logger = Logger.getAnonymousLogger();
    /**
     * To look for XML files representing entities in a directory
     */
    private class XmlEntitiesFF implements FilenameFilter {

        final String pattern = "\\w+-.+\\.xml";
        @Override
        public boolean accept( File dir, String name ) {
            if( name.matches( pattern )) {
                return true;
            }
            return false;
        }
    }
    
    
    /** Where our reference data sits */
    private static final String REFDATA = "./alma/lifecycle/clients/refdata";

    protected StateSystemCLI cli;
    protected ProjectStatus ps;
    protected String psuid;
    
    private void cleanupXmlFiles() {
        String[] files = findXmlFiles();
        for( int i = 0; i < files.length; i++ ) {
            File f = new File( files[i] );
            f.delete();
        }
    }

    private String[] findXmlFiles() {
        final File pwd = new File( "." );
        final FilenameFilter ff = new XmlEntitiesFF();
        String[] files = pwd.list( ff );
        return files;
    }

    public void setUp() {
        
        super.setUp();

        cleanupXmlFiles();
        
        Utils.reset();
        ps = Utils.makeProjectStatus();
        psuid = ps.getProjectStatusEntity().getEntityId();
        
        StateArchive statearc = new StateArchiveMockImpl();
        StateEngine  stateeng = new StateEngineMockImpl();
        StateSystem  statesys = new StateSystemBaseImpl( statearc, stateeng , logger);
        cli = new StateSystemCLI( statesys );
    }
    
    public void tearDown() {
        super.tearDown();
        cleanupXmlFiles();
    }

    //
    public void testDoTreeGeneratePut() throws Exception {
        
        String[] command = {
                StateSystemCLI.Commands.tgput.toString(),
                REFDATA + "/ObsProject0.xml",
        };
        Object[] entities = (Object[]) cli.exec( command );
        assertNotNull( entities );
        assertEquals( 21, entities.length );
        
        assertNotNull( entities[0] );
        assertTrue(    entities[0] instanceof ProjectStatus );
        
        assertNotNull( entities[1] );
        assertTrue(    entities[1] instanceof OUSStatus );  // ObsPlan
        
        assertNotNull( entities[2] );
        assertTrue(    entities[2] instanceof OUSStatus );
        
        assertNotNull( entities[3] );
        assertTrue(    entities[3] instanceof SBStatus );
        
//        System.out.println( StateSystemCLI.USAGE );
    }

    //
    public void testDoTreePut() throws Exception {
        
        String[] command = {
                StateSystemCLI.Commands.tput.toString(),
                REFDATA + "/ProjectStatus0.xml",
                REFDATA + "/OUSStatus0.xml",
                REFDATA + "/SBStatus0.xml",
        };
        Object[] entities = (Object[]) cli.exec( command );
        assertNotNull( entities );
        assertEquals( 3, entities.length );
        
        assertNotNull( entities[0] );
        assertTrue(    entities[0] instanceof ProjectStatus );
        
        assertNotNull( entities[1] );
        assertTrue(    entities[1] instanceof OUSStatus );
        
        assertNotNull( entities[2] );
        assertTrue(    entities[2] instanceof SBStatus );
        
//        System.out.println( StateSystemCLI.USAGE );
    }

    public void testDoUpdate() throws Exception {
        
        // Put an initial tree of objects
        //----------------------------------------
        
        String[] command0 = {
                StateSystemCLI.Commands.tput.toString(),
                REFDATA + "/ProjectStatus0.xml",
                REFDATA + "/OUSStatus0.xml",
                REFDATA + "/SBStatus0.xml",
        };
        cli.exec( command0 );
        
        // Now update each of the entities
        //--------------------------------
        
        String[] command1 = {
                StateSystemCLI.Commands.update.toString(),
                REFDATA + "/ProjectStatus0.xml",
//                REFDATA + "/OUSStatus0.xml",
//                REFDATA + "/SBStatus0.xml",
        };
        Object entity = cli.exec( command1 );
        assertNotNull( entity );
        assertTrue( entity instanceof ProjectStatus );

        String[] command2 = {
                StateSystemCLI.Commands.update.toString(),
//                REFDATA + "/ProjectStatus0.xml",
                REFDATA + "/OUSStatus0.xml",
//                REFDATA + "/SBStatus0.xml",
        };
        entity = cli.exec( command2 );
        assertNotNull( entity );
        assertTrue( entity instanceof OUSStatus );

        String[] command3 = {
                StateSystemCLI.Commands.update.toString(),
//                REFDATA + "/ProjectStatus0.xml",
//                REFDATA + "/OUSStatus0.xml",
                REFDATA + "/SBStatus0.xml",
        };
        entity = cli.exec( command3 );
        assertNotNull( entity );
        assertTrue( entity instanceof SBStatus );
    }

    public void testDoGetProjectStatus() throws Exception {

        // Get a ProjectStatus out
        // --------------------------------
        String[] command1 = { StateSystemCLI.Commands.getps.toString(), psuid };
        Object entity = cli.exec( command1 );
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 1, files.length );
    }

    public void testDoGetOusStatus() throws Exception {

        String[] command1 = { StateSystemCLI.Commands.getouss.toString(),
                              "uid://X58/X27f/Xff0"};
        Object entity = cli.exec( command1 );
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 1, files.length );
    }

    public void testDoGetSbStatus() throws Exception {

        String[] command1 = { StateSystemCLI.Commands.getsbs.toString(),
                              "uid://X58/X27f/Xff0"
                };
        Object entity = cli.exec( command1 );
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 1, files.length );
    }

    public void testDoChangeProjectStatus() {

        // error!
        // --------------------------------
        String[] command0 = { StateSystemCLI.Commands.changeps.toString() };    
        try {
            cli.exec( command0 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
     // error!
        // --------------------------------
        String[] command1 = { 
                StateSystemCLI.Commands.changeps.toString(),
                "uid://bogus",
                "badstate",
                null, 
                null, 
                null };    
        try {
            cli.exec( command1 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
        // Change a ProjectStatus
        // --------------------------------
        String[] goodCMD = { StateSystemCLI.Commands.changeps.toString(),
                              "uid://X58/X27f/Xff0",
                              StatusTStateType.READY.toString(),
                              Subsystem.OBOPS,
                              Role.ARCA
        };
                    
        try {
            cli.exec( goodCMD );
        }
        catch( Exception e ) {
            fail( e.getMessage() );
        }   
    }

    public void testDoChangeOusStatus() {
    
        // error!
        // --------------------------------
        String[] command0 = { StateSystemCLI.Commands.changeouss.toString() };    
        try {
            cli.exec( command0 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
     // error!
        // --------------------------------
        String[] command1 = { 
                StateSystemCLI.Commands.changeouss.toString(),
                "uid://bogus",
                "badstate",
                null, 
                null, 
                null };    
        try {
            cli.exec( command1 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
     // error!
        // --------------------------------
        String[] command2 = { 
                StateSystemCLI.Commands.changeouss.toString(),
                "uid://bogus",
                StatusTStateType.READY.toString(),
                null };    
        try {
            cli.exec( command2 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
        // Change a ProjectStatus
        // --------------------------------
        String[] goodCMD = { StateSystemCLI.Commands.changeouss.toString(),
                              "uid://X58/X27f/Xff0",
                              StatusTStateType.PIPELINEERROR.toString(),
                              Subsystem.PIPELINE,
                              Role.AOD // pass in a role as the UserId, RoleProviderTestMock will return what it is given 
        };
                    
        try {
            cli.exec( goodCMD );
        }
        catch( Exception e ) {
        	e.printStackTrace();
            fail( e.getMessage() );
        }   
    }

    public void testDoChangeSbStatus() {
    
        // error!
        // --------------------------------
        String[] command0 = { StateSystemCLI.Commands.changesbs.toString() };    
        try {
            cli.exec( command0 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
     // error!
        // --------------------------------
        String[] command1 = { 
                StateSystemCLI.Commands.changesbs.toString(),
                "uid://bogus",
                "badstate",
                null, 
                null, 
                null };    
        try {
            cli.exec( command1 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
     // error!
        // --------------------------------
        String[] command2 = { 
                StateSystemCLI.Commands.changesbs.toString(),
                "uid://bogus",
                StatusTStateType.RUNNING.toString(),
                null };    
        try {
            cli.exec( command2 );
            fail( "Expected error" );
        }
        catch( Exception e ) {
            // no-op, expected
        }
        
        // Change a ProjectStatus
        // --------------------------------
        String[] goodCMD = { StateSystemCLI.Commands.changesbs.toString(),
                              "uid://X58/X27f/Xff0",
                              StatusTStateType.RUNNING.toString(),
                              Subsystem.SCHEDULING,
                              Role.AOD // pass in a role as the UserId, RoleProviderTestMock will return what it is given 
        };
                    
        try {
            cli.exec( goodCMD );
        }
        catch( Exception e ) {
            fail( e.getMessage() );
        }   
    }

    public void testDoGetProjectStatusList() throws Exception {
    
        String[] cmd = { StateSystemCLI.Commands.getpsl.toString(), psuid };
        Object entity = cli.exec( cmd );
        
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 9, files.length );
    }
    
    
    public void testDoGetSbStatusListOus() throws Exception {

        List<OUSStatus> oussList = new ArrayList<OUSStatus>();
        List<SBStatus> sbsList = new ArrayList<SBStatus>();
        Utils.walk( ps, oussList, sbsList );
        
        // Take one SBStatus, find out who the parent OUSStatus is
        SBStatus sbs = sbsList.get( 0 );
        String oussUid = sbs.getContainingObsUnitSetRef().getEntityId();
        
        String[] cmd = { StateSystemCLI.Commands.getsbslo.toString(), oussUid };
        Object entity = cli.exec( cmd );
        
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 2, files.length );
    }
    
    
    public void testDoGetSbStatusListPs() throws Exception {
        
        String[] cmd = { StateSystemCLI.Commands.getsbslp.toString(), psuid };
        Object entity = cli.exec( cmd );
        
        assertNotNull( entity );

        String[] files = findXmlFiles();
        assertEquals( 4, files.length );
    }
}
