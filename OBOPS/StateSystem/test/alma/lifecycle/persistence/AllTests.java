/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author amchavan, Jun 18, 2009
 * @version $Revision$
 */

// $Id$
public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.lifecycle.persistence" );
        
        suite.addTest( alma.lifecycle.persistence.domain.AllTests.suite() );
        
        //$JUnit-BEGIN$
        suite.addTestSuite( TestStateArchiveSimpleImpl.class );
        suite.addTestSuite( TestStateArchiveImpl.class );
        //$JUnit-END$

        return suite;
    }
}
