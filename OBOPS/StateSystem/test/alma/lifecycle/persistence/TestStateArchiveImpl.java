/**
 * TestStateArchiveImpl.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import static alma.lifecycle.persistence.StateArchiveTestHelper.BASELINE;
import static alma.lifecycle.persistence.StateArchiveTestHelper.ENTITY_ID0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.ENTITY_ID1;
import static alma.lifecycle.persistence.StateArchiveTestHelper.SLOT;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATE0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATUS_ID0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATUS_ID1;
import static alma.lifecycle.persistence.StateArchiveTestHelper.makeList192;
import static alma.lifecycle.persistence.StateArchiveTestHelper.makeRecords;
import static alma.lifecycle.persistence.StateArchiveTestHelper.marshal;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.SpringTestCase;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 * @author amchavan, Jul 3, 2009
 * @version $Revision$
 */

// $Id$

public class TestStateArchiveImpl extends SpringTestCase {

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getOUSStatus(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetOUSStatus() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        OUSStatusEntityT id = ouss.getOUSStatusEntity();
        OUSStatus out = stateArchive.getOUSStatus( id );
        String expected = marshal( ouss );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getOUSStatusXml(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetOUSStatusXml() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        OUSStatusEntityT id = ouss.getOUSStatusEntity();
        String out = stateArchive.getOUSStatusXml( id );
        String expected = marshal( ouss );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusByProjectStatusId() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        ProjectStatus out = stateArchive.getProjectStatus( id );
        String expected = marshal( ops );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusByObsProjectId() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        ops.getProjectStatusEntity();
        ProjectStatus out = stateArchive.getProjectStatus( ops.getProjectStatusEntity() );
        String expected = marshal( ops );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }
    
    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        StatusBaseT[] out = stateArchive.getProjectStatusList( id );
        assertNotNull( out );
        int numEntities = NUM_LEAF_OUSS * NUM_SBS_PER_OUS + NUM_LEAF_OUSS + 3;
        assertEquals( numEntities, out.length );
        ProjectStatus ps = (ProjectStatus) out[0];
        assertEquals( id.getEntityId(), 
                      ps.getProjectStatusEntity().getEntityId() ); 
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusXml(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusXml() throws Exception {

        stateArchive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        String out = stateArchive.getProjectStatusXml( id );
        String expected = marshal( ops );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusXmlList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        String[] out = stateArchive.getProjectStatusXmlList( id );
        assertNotNull( out );
        int numEntities = NUM_LEAF_OUSS * NUM_SBS_PER_OUS + NUM_LEAF_OUSS + 3;
        assertEquals( numEntities, out.length );
        assertEquals( marshal( ops ), out[0] ); 
//        for( int i = 0; i < out.length; i++ ) {
//            System.out.println( "---------------------------------------" );
//            System.out.println( out[i] );
//        }
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatus(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)}
     * @throws Exception
     */
    public void testGetSBStatus() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        SBStatusEntityT id = sbs.getSBStatusEntity();
        SBStatus out = stateArchive.getSBStatus( id );
        String expected = marshal( sbs );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusOusList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        SBStatus[] sbs = 
            stateArchive.getSBStatusList( ouss.getOUSStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXmlList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusOusXmlList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        String[] sbs = 
            stateArchive.getSBStatusXmlList( ouss.getOUSStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusProjectList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        SBStatus[] sbs = 
            stateArchive.getSBStatusList( ops.getProjectStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS * NUM_LEAF_OUSS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusProjectXmlList() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        String[] sbs = 
            stateArchive.getSBStatusXmlList( ops.getProjectStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS * NUM_LEAF_OUSS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXml(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusXml() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
        SBStatusEntityT id = sbs.getSBStatusEntity();
        String out = stateArchive.getSBStatusXml( id );
        String expected = marshal( sbs );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(ProjectStatus, OUSStatus[], SBStatus[])
     */
    public void testPutOUSStatus() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#update(alma.entity.xmlbinding.projectstatus.ProjectStatus)}.
     * @throws Exception 
     */
    public void testPutProjectStatus() throws Exception {
        stateArchive.insert( ops, oussList, sbsList );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    public void testPutStateChangeRecord00() throws Exception {
        
        // A simple put, no further read
        //------------------------------
        stateArchive.insert( scr );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord01() throws Exception {
      
        // Make a list of one, put it, then read that one back
        //----------------------------------------------------
        List<StateChangeRecord> list = 
            makeRecords( 0, null, null, null, null, null, null, null );
        for( StateChangeRecord record : list ) {
            stateArchive.insert( record );
        }

        List<StateChangeRecord> results = (List<StateChangeRecord>) 
        	this.stateArchiveHibernateDao.findAll(StateChangeRecord.class);
        assertNotNull( results );
        assertEquals( 1, results.size() );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord02() throws Exception {

        // Make a list of two, put them, then read them back
        //----------------------------------------------------
        String[] entityIds = { ENTITY_ID0, ENTITY_ID1 };
        List<StateChangeRecord> list = makeRecords( 0, entityIds, 
                                                    null, null, null, 
                                                    null, null, null );
        for( StateChangeRecord record : list ) {
            stateArchive.insert( record );
        }

        List<StateChangeRecord> results = (List<StateChangeRecord>) 
    		this.stateArchiveHibernateDao.findAll(StateChangeRecord.class);
        assertNotNull( results );
        assertEquals( 2, results.size() );

    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord04() throws Exception {


        // Make a list of four, put them, then read them back
        //----------------------------------------------------
        String[] entityIds = { ENTITY_ID0, ENTITY_ID1 };
        String[] statusIds = { STATUS_ID0, STATUS_ID1 };
        List<StateChangeRecord> list = makeRecords( 0, 
                                                    entityIds, 
                                                    statusIds, 
                                                    null, 
                                                    null, 
                                                    null, 
                                                    null, 
                                                    null );
        for( StateChangeRecord record : list ) {
            stateArchive.insert( record );
        }

        List<StateChangeRecord> results = (List<StateChangeRecord>) 
			this.stateArchiveHibernateDao.findAll(StateChangeRecord.class);
        assertNotNull( results );
        assertEquals( 4, results.size() );
    }
    
    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord192() throws Exception {


        // Make a list of 192, put them, then read them back
        //----------------------------------------------------
        List<StateChangeRecord> list = makeList192( 0 );
        for( StateChangeRecord record : list ) {
            stateArchive.insert( record );
        }

        List<StateChangeRecord> results = (List<StateChangeRecord>) 
			this.stateArchiveHibernateDao.findAll(StateChangeRecord.class);
        assertNotNull( results );
        assertEquals( 192, results.size() );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecordMany() throws Exception {

        final int count = 10;
        
        // Make many lists of 192 each, put them all, then 
        // read everything back
        //----------------------------------------------------
        for( int i = 0; i < count; i++ ) {
            List<StateChangeRecord> list = makeList192( i );
            for( StateChangeRecord record : list ) {
                stateArchive.insert( record );
            }
        }
        
        List<StateChangeRecord> results = (List<StateChangeRecord>) 
			this.stateArchiveHibernateDao.findAll(StateChangeRecord.class);
        assertNotNull( results );
        assertEquals( 192 * count, results.size() );
    }
    
    public void testFindStateChangeRecords() throws Exception {
        
        // create a bunch of records, 384 in total
        //-----------------------------------------
        for( int i = 0; i < 2; i++ ) {
            List<StateChangeRecord> list = makeList192( i );
            for( StateChangeRecord record : list ) {
                stateArchive.insert( record );
            }
        }
        
        // Simplest query, return the lot and
        // make sure rows are sorted by timestamp
        //---------------------------------------
        List<StateChangeRecord> list = 
            stateArchive.findStateChangeRecords( null, null, null, 
                                             null, null, null );
        assertNotNull( list );
        assertEquals( 384, list.size() );
        long lower = 0;
        for( StateChangeRecord record : list ) {
            long current = record.getTimestamp().getTime();
            assertTrue( "not sorted by timestamp", lower <= current );
            lower = current;
        }
        
        // Also very simple, return nothing
        //-----------------------------------
        Date impossible = new Date( BASELINE * 100 );
        list = stateArchive.findStateChangeRecords( impossible, null, 
                                                null, null, null, null );
        assertNotNull( list );
        assertEquals( 0, list.size() );
        
        // Return first half of entries 
        //-----------------------------------
        Date halfway = new Date( BASELINE + SLOT );
        list = stateArchive.findStateChangeRecords( null, halfway, 
                                                null, null, null, null );
        assertNotNull( list );
        assertEquals( 193, list.size() );   // 193!!
        
        // Return one entry only 
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( halfway, halfway, 
                                                null, null, null, null );
        assertNotNull( list );
        assertEquals( 1, list.size() );
        
        // Return second half of entries 
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( halfway, null,
                                                null, null, null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        
        // Return only ENTITY0 state changes
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( null, null, 
                                               ENTITY_ID0, 
                                               null,  null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( ENTITY_ID0, record.getDomainEntityId() );
        }
        
        // Return only STATE0 state changes
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( null, null, 
                                               null, 
                                               STATE0, 
                                               null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( STATE0, record.getDomainEntityState() );
        }
        
        // Return entries of user aod
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( null, null, 
                                               null, null, 
                                               "aod", null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( "aod", record.getUserId() );
        }
        
        // Return only SchedBlock status values
        //-----------------------------------
        list = stateArchive.findStateChangeRecords( null, null, 
                                               null, null, null, 
                                               StateEntityType.SBK );
        assertNotNull( list );
        assertEquals( 128, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( StateEntityType.SBK, record.getEntityType() );
        }
        
        // Return only SchedBlock status values
        // of user arca
        //-------------------------------------
        list = stateArchive.findStateChangeRecords( null, null, 
                                               null, null, 
                                               "arca", 
                                               StateEntityType.SBK );
        assertNotNull( list );
        assertEquals( 64, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( "arca", record.getUserId() );
            assertEquals( StateEntityType.SBK, record.getEntityType() );
        }
        
    }
    

    public void testUpdateProjectStatus() throws Exception {
    		stateArchive.update( ops ); // inserts new proj
        ProjectStatus ps = stateArchive.getProjectStatus(ops.getProjectStatusEntity());
        assertNotNull(ps);
        ops.getStatus().setState(StatusTStateType.CANCELED);
        stateArchive.update( ops ); // updates existing proj
        commitAndStartNewTransaction();
        ps = stateArchive.getProjectStatus(ops.getProjectStatusEntity());
        assertEquals(ps.getStatus().getState(), StatusTStateType.CANCELED);
        
    }
    
    public void testUpdateOUSStatus() throws Exception {

    	stateArchive.insert( ops, oussList, sbsList );
    	OUSStatusRefT ousParentRefT = new OUSStatusRefT();
    	ousParentRefT.setEntityId(oussList[oussList.length - 1].getOUSStatusEntity().getEntityId());
    	OUSStatus newOusStatus = Utils.makeOUSStatus(null, ops, ousParentRefT, StatusTStateType.READY);
    	stateArchive.update(newOusStatus); // insert new ous
    	OUSStatus ous = stateArchive.getOUSStatus(newOusStatus.getOUSStatusEntity());
        assertNotNull(ous);
    	newOusStatus.getStatus().setState(StatusTStateType.CANCELED);
    	stateArchive.update(newOusStatus); // update existing ous
    	ous = stateArchive.getOUSStatus(newOusStatus.getOUSStatusEntity());
    	assertEquals(ous.getStatus().getState(), StatusTStateType.CANCELED);
    	
    	
    }
    
    public void testUpdateSbStatus() throws Exception {
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	SBStatus newSbStatus = Utils.makeSBStatus(ops, oussList[oussList.length - 1], StatusTStateType.READY);
    	stateArchive.update(newSbStatus);
    	SBStatus sb = stateArchive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertNotNull(sb);
    	newSbStatus.getStatus().setState(StatusTStateType.CANCELED);
    	stateArchive.update(newSbStatus);
    	sb = stateArchive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertEquals(sb.getStatus().getState(), StatusTStateType.CANCELED);
    }
    
    // Make sure we catch the case in which somebody tries to insert
    // OUSStatus or SBStatus instances out of sequence
    public void testBadUpdates() {

        // find the top-level OUSStatus
        OUSStatus topLevel = null;
        for( OUSStatus ouss : oussList ) {
            if( ouss.getContainingObsUnitSetRef() == null ) {
                topLevel = ouss;
                break;
            }
        }
        if( topLevel == null ) {
            fail( "Can't find top-level OUSStatus" );
        }

        // find an intermediate OUSStatus
        OUSStatus inter = null;
        for( OUSStatus ouss : oussList ) {
            if( ouss.getContainingObsUnitSetRef() != null ) {
                inter = ouss;
                break;
            }
        }
        if( inter == null ) {
            fail( "Can't find intermediate OUSStatus" );
        }
        
        
        // first case: top-level OUSStatus, missing ProjectStatus
        try {
            stateArchive.update( topLevel );
            fail( "Expected AcsJNoSuchEntityEx" );
        }
        catch( AcsJStateIOFailedEx e ) {
            fail( e.getMessage() );
        }
        catch( AcsJNoSuchEntityEx e ) {
            // this was expected
            String msg = e.getCause().getMessage();
            String expected = "missing root ProjectStatus";
            assertEquals( expected, msg.substring( 0, expected.length() ));
        }
        
        // second case: intermediate OUSStatus, missing parent OUSStatus
        try {
            stateArchive.update( inter );
            fail( "Expected AcsJNoSuchEntityEx" );
        }
        catch( AcsJStateIOFailedEx e ) {
            fail( e.getMessage() );
        }
        catch( AcsJNoSuchEntityEx e ) {
            // this was expected
            String msg = e.getCause().getMessage();
            String expected = "missing parent OUSStatus";
            assertEquals( expected, msg.substring( 0, expected.length() ));
        }
        
        // third case: SBStatus, missing parent OUSStatus
        try {
            stateArchive.update( sbsList[0] );
            fail( "Expected AcsJNoSuchEntityEx" );
        }
        catch( AcsJStateIOFailedEx e ) {
            fail( e.getMessage() );
        }
        catch( AcsJNoSuchEntityEx e ) {
            // this was expected
            String msg = e.getCause().getMessage();
            String expected = "missing parent OUSStatus";
            assertEquals( expected, msg.substring( 0, expected.length() ) );
        }
    }
    
    // Try inserting an entire tree of status entities by using the
    // update() methods.
    // Added by amchavan, 13-Aug-2009, to reproduce a bug reported by
    // Lindsey (it does)
    public void testInsertByUpdates() throws Exception {
        
        try {
            OUSStatus[] oussSequence = Utils.getOusStatusSequence();
            
            int statusEntityCounter = 1;
            // First the project...
            stateArchive.update( ops );
            for( OUSStatus ouss : oussSequence ) {
                stateArchive.update( ouss );
                statusEntityCounter++;
            }
            for( SBStatus sbStatus : sbsList ) {
                stateArchive.update( sbStatus );
                statusEntityCounter++;
            }
            
            List<StateChangeRecord> stateChangeRecords = 
                stateArchive.findStateChangeRecords( null, null, null, 
                                                 null, null, null );
            // check if stateChange records were created
            assertEquals(statusEntityCounter, stateChangeRecords.size());
            
        }
        catch( AcsJNoSuchEntityEx e ) {
            String msg = e.getCause().getMessage();
            fail( "Bug reported by Lindsey: should not happen: '" + msg + "'" );
        }
    }
    
    public void testfindProjectStatusByState() throws Exception {
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	
    	String[] states = {StatusTStateType.PHASE2SUBMITTED.toString(), 
    			StatusTStateType.READY.toString(),
    			StatusTStateType.RUNNING.toString(),
    			StatusTStateType.PARTIALLYOBSERVED.toString(),
    			StatusTStateType.CANCELED.toString()
    	};
		
    	ProjectStatus[] projectsStatuses = stateArchive.findProjectStatusByState(states);
    	assertNotNull(projectsStatuses);
    	assertTrue(projectsStatuses.length > 0);
    	System.out.println("projectsStatuses[0] = " + projectsStatuses[0].getStatus().getState());    	
    }
    
    public void testfindProjectStatusByAnyState() throws Exception {
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	
    	String[] states = {
    			StatusTStateType.ANYSTATE.toString()};
		
    	ProjectStatus[] projectsStatuses = stateArchive.findProjectStatusByState(states);
    	assertNotNull(projectsStatuses);
    	assertTrue(projectsStatuses.length > 0);
    	System.out.println("projectsStatuses[0] = " + projectsStatuses[0].getStatus().getState());
    	
    }
    
    public void testfindSBStatusByState() throws Exception {
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	
    	String[] states = {StatusTStateType.PHASE2SUBMITTED.toString(), 
    			StatusTStateType.READY.toString(),
    			StatusTStateType.RUNNING.toString(),
    			StatusTStateType.PARTIALLYOBSERVED.toString(),
    	};
		
    	SBStatus[] sbStatuses = stateArchive.findSBStatusByState(states);
    	assertNotNull(sbStatuses);
    	assertTrue(sbStatuses.length > 0);
    	System.out.println("sbStatuses[0] = " + sbStatuses[0].getStatus().getState());
    }
    
    public void testfindSBStatusByAnyState() throws Exception {
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	
    	String[] states = {StatusTStateType.ANYSTATE.toString()};
		
    	SBStatus[] sbStatuses = stateArchive.findSBStatusByState(states);
    	assertNotNull(sbStatuses);
    	assertTrue(sbStatuses.length > 0);
    	System.out.println("sbStatuses[0] = " + sbStatuses[0].getStatus().getState());
    }
    
    public void testAddExecStatusEntriesToSbStatus() throws Exception {
    	
    	StringWriter writer = new StringWriter();
        ops.marshal( writer );
    	String projStatusXml = writer.toString();
    	try{
    		ops.validate();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	System.out.println(projStatusXml);
    	
    	stateArchive.insert( ops, oussList, sbsList );
    	SBStatus newSbStatus = Utils.makeSBStatus(ops, oussList[oussList.length - 1], StatusTStateType.READY);
    	
    	for(int execCount = 1; execCount <= 200; execCount++){
    		newSbStatus.addExecStatus(Utils.makeExecStatus(execCount));
    	}
    	
    	stateArchive.update(newSbStatus);
    	
    	SBStatus sb = stateArchive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertNotNull(sb);
    	newSbStatus.getStatus().setState(StatusTStateType.CANCELED);
    	stateArchive.update(newSbStatus);
    	
    	sb = stateArchive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertEquals(sb.getStatus().getState(), StatusTStateType.CANCELED);
    	
    	for(int execCount = 200; execCount < 1000; execCount++){
    		sb.addExecStatus(Utils.makeExecStatus(execCount));
    	}

    	String sbStatusXmlMem = getSBStatusXML(sb);
    	
    	stateArchive.update(sb);
    	commitAndStartNewTransaction();
    	sb = stateArchive.getSBStatus(sb.getSBStatusEntity());
    	assertEquals(1000, sb.getExecStatusCount());
    	
    	String sbStatusXmlDb = getSBStatusXML(sb);
    	assertEquals(sbStatusXmlMem, sbStatusXmlDb);
    	
    	System.out.println(">> SB xml size: " +  (sbStatusXmlDb.getBytes().length / 1024) + " kb");
    }

    private String getSBStatusXML(SBStatus status){
	    
    	StringWriter writer = new StringWriter();
	    try {
	        status.marshal( writer );
	        String t = writer.toString();
	        return t;
	    }
	    catch( Exception e ) {
	        // should never happen
	        e.printStackTrace();
	        throw new RuntimeException( e );
	    }
    }

}
