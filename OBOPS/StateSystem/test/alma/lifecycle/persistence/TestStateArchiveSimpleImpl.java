/**
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import static alma.lifecycle.persistence.StateArchiveTestHelper.BASELINE;
import static alma.lifecycle.persistence.StateArchiveTestHelper.ENTITY_ID0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.ENTITY_ID1;
import static alma.lifecycle.persistence.StateArchiveTestHelper.SLOT;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATE0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATUS_ID0;
import static alma.lifecycle.persistence.StateArchiveTestHelper.STATUS_ID1;
import static alma.lifecycle.persistence.StateArchiveTestHelper.makeList192;
import static alma.lifecycle.persistence.StateArchiveTestHelper.makeRecords;
import static alma.lifecycle.persistence.StateArchiveTestHelper.marshal;

import java.util.Date;
import java.util.List;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.PersistenceTestCase;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;

/**
 * @author amchavan, Jul 3, 2009
 * @version $Revision$
 */

// $Id$

public class TestStateArchiveSimpleImpl extends PersistenceTestCase {
    /**
     * @see alma.lifecycle.PersistenceTestCase#setUp()
     */
    public void setUp() {
        super.setUp();
        archive = new StateArchiveSimpleImpl();
        
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getOUSStatus(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetOUSStatus() throws Exception {
        archive.insert( ops, oussList, sbsList );
        OUSStatusEntityT id = ouss.getOUSStatusEntity();
        OUSStatus out = archive.getOUSStatus( id );
        String expected = marshal( ouss );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getOUSStatusXml(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetOUSStatusXml() throws Exception {
        archive.insert( ops, oussList, sbsList );
        OUSStatusEntityT id = ouss.getOUSStatusEntity();
        String out = archive.getOUSStatusXml( id );
        String expected = marshal( ouss );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusByProjectStatusId() throws Exception {
        archive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        ProjectStatus out = archive.getProjectStatus( id );
        String expected = marshal( ops );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }
    
    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusListByObsProjectId() throws Exception {
        
    	archive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        StatusBaseT[] out = archive.getProjectStatusList( ops.getProjectStatusEntity() );
        assertNotNull( out );
        int numEntities = NUM_LEAF_OUSS * NUM_SBS_PER_OUS + NUM_LEAF_OUSS + 3;
        assertEquals( numEntities, out.length );
        ProjectStatus ps = (ProjectStatus) out[0];
        assertEquals( id.getEntityId(), 
                      ps.getProjectStatusEntity().getEntityId() ); 
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        StatusBaseT[] out = archive.getProjectStatusList( id );
        assertNotNull( out );
        int numEntities = NUM_LEAF_OUSS * NUM_SBS_PER_OUS + NUM_LEAF_OUSS + 3;
        assertEquals( numEntities, out.length );
        ProjectStatus ps = (ProjectStatus) out[0];
        assertEquals( id.getEntityId(), 
                      ps.getProjectStatusEntity().getEntityId() ); 
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusXml(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusXml() throws Exception {

        archive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        String out = archive.getProjectStatusXml( id );
        String expected = marshal( ops );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getProjectStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetProjectStatusXmlList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        ProjectStatusEntityT id = ops.getProjectStatusEntity();
        String[] out = archive.getProjectStatusXmlList( id );
        assertNotNull( out );
        int numEntities = NUM_LEAF_OUSS * NUM_SBS_PER_OUS + NUM_LEAF_OUSS + 3;
        assertEquals( numEntities, out.length );
        assertEquals( marshal( ops ), out[0] ); 
//        for( int i = 0; i < out.length; i++ ) {
//            System.out.println( "---------------------------------------" );
//            System.out.println( out[i] );
//        }
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatus(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)}
     * @throws Exception
     */
    public void testGetSBStatus() throws Exception {
        archive.insert( ops, oussList, sbsList );
        SBStatusEntityT id = sbs.getSBStatusEntity();
        SBStatus out = archive.getSBStatus( id );
        String expected = marshal( sbs );
        String outXml = marshal( out );
        assertEquals( expected, outXml );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusOusList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        SBStatus[] sbs = 
            archive.getSBStatusList( ouss.getOUSStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXmlList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusOusXmlList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        String[] sbs = 
            archive.getSBStatusXmlList( ouss.getOUSStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusProjectList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        SBStatus[] sbs = 
            archive.getSBStatusList( ops.getProjectStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS * NUM_LEAF_OUSS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusProjectXmlList() throws Exception {
        archive.insert( ops, oussList, sbsList );
        String[] sbs = 
            archive.getSBStatusXmlList( ops.getProjectStatusEntity() );
        assertNotNull( sbs );
        assertEquals( NUM_SBS_PER_OUS * NUM_LEAF_OUSS, sbs.length );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#getSBStatusXml(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)}.
     * @throws Exception 
     */
    public void testGetSBStatusXml() throws Exception {
        archive.insert( ops, oussList, sbsList );
        SBStatusEntityT id = sbs.getSBStatusEntity();
        String out = archive.getSBStatusXml( id );
        String expected = marshal( sbs );
        assertEquals( expected, out );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#update(alma.entity.xmlbinding.ousstatus.OUSStatus)}.
     */
    public void testPutOUSStatus() throws Exception {
        archive.insert( ops, oussList, sbsList );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#update(alma.entity.xmlbinding.projectstatus.ProjectStatus)}.
     * @throws Exception 
     */
    public void testPutProjectStatus() throws Exception {
        archive.insert( ops, oussList, sbsList );
    }


    public void testUpdateProjectStatus() throws Exception {
    	archive.update( ops ); // inserts new proj
        ProjectStatus ps = archive.getProjectStatus(ops.getProjectStatusEntity());
        assertNotNull(ps);
        ops.getStatus().setState(StatusTStateType.CANCELED);
        archive.update( ops ); // updates existing proj
        ps = archive.getProjectStatus(ops.getProjectStatusEntity());
        assertEquals(ps.getStatus().getState(), StatusTStateType.CANCELED);
        
    }
    
    public void testUpdateOUSStatus() throws Exception {

    	archive.insert( ops, oussList, sbsList );
    	OUSStatusRefT ousParentRefT = new OUSStatusRefT();
    	ousParentRefT.setEntityId(oussList[oussList.length - 1].getOUSStatusEntity().getEntityId());
    	OUSStatus newOusStatus = Utils.makeOUSStatus(null, ops, ousParentRefT, StatusTStateType.READY);
    	archive.update(newOusStatus); // insert new ous
    	OUSStatus ous = archive.getOUSStatus(newOusStatus.getOUSStatusEntity());
        assertNotNull(ous);
    	newOusStatus.getStatus().setState(StatusTStateType.CANCELED);
    	archive.update(newOusStatus); // update existing ous
    	ous = archive.getOUSStatus(newOusStatus.getOUSStatusEntity());
    	assertEquals(ous.getStatus().getState(), StatusTStateType.CANCELED);
    	
    	
    }
    
    public void testUpdateSbStatus() throws Exception {
    	
    	archive.insert( ops, oussList, sbsList );
    	SBStatus newSbStatus = Utils.makeSBStatus(ops, oussList[oussList.length - 1], StatusTStateType.READY);
    	archive.update(newSbStatus);
    	SBStatus sb = archive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertNotNull(sb);
    	newSbStatus.getStatus().setState(StatusTStateType.CANCELED);
    	archive.update(newSbStatus);
    	sb = archive.getSBStatus(newSbStatus.getSBStatusEntity());
    	assertEquals(sb.getStatus().getState(), StatusTStateType.CANCELED);
    }
    
    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    public void testPutStateChangeRecord00() throws Exception {
        
        // A simple put, no further read
        //------------------------------
        archive.insert( scr );
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord01() throws Exception {
      
        // Make a list of one, put it, then read that one back
        //----------------------------------------------------
        List<StateChangeRecord> list = 
            makeRecords( 0, null, null, null, null, null, null, null );
        for( StateChangeRecord record : list ) {
            archive.insert( record );
        }

        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 1, results.size() );
        commitTransaction();
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord02() throws Exception {

        // Make a list of two, put them, then read them back
        //----------------------------------------------------
        String[] entityIds = { ENTITY_ID0, ENTITY_ID1 };
        List<StateChangeRecord> list = makeRecords( 0, entityIds, 
                                                    null, null, null, 
                                                    null, null, null );
        for( StateChangeRecord record : list ) {
            archive.insert( record );
        }

        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 2, results.size() );
        commitTransaction();

    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord04() throws Exception {


        // Make a list of four, put them, then read them back
        //----------------------------------------------------
        String[] entityIds = { ENTITY_ID0, ENTITY_ID1 };
        String[] statusIds = { STATUS_ID0, STATUS_ID1 };
        List<StateChangeRecord> list = makeRecords( 0, 
                                                    entityIds, 
                                                    statusIds, 
                                                    null, 
                                                    null, 
                                                    null, 
                                                    null, 
                                                    null );
        for( StateChangeRecord record : list ) {
            archive.insert( record );
        }

        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 4, results.size() );
        commitTransaction();
    }
    
    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecord192() throws Exception {


        // Make a list of 192, put them, then read them back
        //----------------------------------------------------
        List<StateChangeRecord> list = makeList192( 0 );
        for( StateChangeRecord record : list ) {
            archive.insert( record );
        }

        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 192, results.size() );
        commitTransaction();
    }

    /**
     * Test method for {@link alma.lifecycle.persistence.StateArchiveSimpleImpl#insert(alma.lifecycle.persistence.domain.StateChangeRecord)}.
     */
    @SuppressWarnings("unchecked")
    public void testPutStateChangeRecordMany() throws Exception {

        final int count = 10;
        
        // Make many lists of 192 each, put them all, then 
        // read everything back
        //----------------------------------------------------
        for( int i = 0; i < count; i++ ) {
            List<StateChangeRecord> list = makeList192( i );
            for( StateChangeRecord record : list ) {
                archive.insert( record );
            }
        }
        
        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 192 * count, results.size() );
        commitTransaction();
    }
    
    public void testFindStateChangeRecords() throws Exception {
        
        // create a bunch of records, 384 in total
        //-----------------------------------------
        for( int i = 0; i < 2; i++ ) {
            List<StateChangeRecord> list = makeList192( i );
            for( StateChangeRecord record : list ) {
                archive.insert( record );
            }
        }
        
        // Simplest query, return the lot and
        // make sure rows are sorted by timestamp
        //---------------------------------------
        List<StateChangeRecord> list = 
            archive.findStateChangeRecords( null, null, null, 
                                            null, null, null );
        assertNotNull( list );
        assertEquals( 384, list.size() );
        long lower = 0;
        for( StateChangeRecord record : list ) {
            long current = record.getTimestamp().getTime();
            assertTrue( "not sorted by timestamp", lower <= current );
            lower = current;
        }
        
        // Also very simple, return nothing
        //-----------------------------------
        Date impossible = new Date( BASELINE * 100 );
        list = archive.findStateChangeRecords( impossible, null, 
                                               null, null, null, null );
        assertNotNull( list );
        assertEquals( 0, list.size() );
        
        // Return first half of entries 
        //-----------------------------------
        Date halfway = new Date( BASELINE + SLOT );
        list = archive.findStateChangeRecords( null, halfway, 
                                               null, null, null, null );
        assertNotNull( list );
        assertEquals( 193, list.size() );   // 193!!
        
        // Return one entry only 
        //-----------------------------------
        list = archive.findStateChangeRecords( halfway, halfway, 
                                               null, null, null, null );
        assertNotNull( list );
        assertEquals( 1, list.size() );
        
        // Return second half of entries 
        //-----------------------------------
        list = archive.findStateChangeRecords( halfway, null,
                                               null, null, null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        
        // Return only ENTITY0 state changes
        //-----------------------------------
        list = archive.findStateChangeRecords( null, null, 
                                               ENTITY_ID0, 
                                               null, null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( ENTITY_ID0, record.getDomainEntityId() );
        }
        
        // Return only STATE0 state changes
        //-----------------------------------
        list = archive.findStateChangeRecords( null, null, 
                                               null, 
                                               STATE0, 
                                               null, null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( STATE0, record.getDomainEntityState() );
        }
        
        // Return entries of user arca
        //-----------------------------------
        list = archive.findStateChangeRecords( null, null, 
                                               null, null, 
                                               "arca",
                                               null );
        assertNotNull( list );
        assertEquals( 192, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( "arca", record.getUserId() );
        }
        
        // Return only SchedBlock status values
        //-----------------------------------
        list = archive.findStateChangeRecords( null, null, 
                                               null, null, null, 
                                               StateEntityType.SBK );
        assertNotNull( list );
        assertEquals( 128, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( StateEntityType.SBK , record.getEntityType() );
        }
        
        // Return only SchedBlock status values
        // of user 1
        //-------------------------------------
        list = archive.findStateChangeRecords( null, null, 
                                               null, null, 
                                              "arca", 
                                              StateEntityType.SBK  );
        assertNotNull( list );
        assertEquals( 64, list.size() );
        for( StateChangeRecord record : list ) {
            assertEquals( "arca", record.getUserId() );
            assertEquals( StateEntityType.SBK, record.getEntityType() );
        }
        

    }

}
