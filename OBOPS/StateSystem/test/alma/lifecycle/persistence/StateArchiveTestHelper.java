/**
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import java.io.StringWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;

/**
 * Helper class for TestStateArchiveImpl & TestStateArchiveSimpleImpl
 * 
 * @author rkurowsk, Jul 31, 2009
 * @version $Revision$
 */

// $Id$
public class StateArchiveTestHelper {

	static final String LOCATION0 = "LC0";
	static final String LOCATION1 = "LC1";
	static final String ENTITY_ID0 = "domain0";
	static final String ENTITY_ID1 = "domain1";
	static final String STATUS_ID0 = "status0";
	static final String STATUS_ID1 = "status1";
	static final String STATE0 = "state0";
	static final String STATE1 = "state1";
	static final String ROLE0 = "role0";
	static final String ROLE1 = "role1";
	static final String UID0 = "uid0";
	static final String UID1 = "uid1";
	
	static final int BASELINE = 1000000; // baseline time (1000 secs)
	static final int SLOT = 1000; // num msec per slot

	/**
	 * @param slot
	 *            Identifier of the record set
	 * @return a List of 192 individual records
	 */
	static List<StateChangeRecord> makeList192(int slot) {
		String[] entityIds = { ENTITY_ID0, ENTITY_ID1 };
		String[] statusIds = { STATUS_ID0, STATUS_ID1 };
		String[] states = { STATE0, STATE1 };
		String[] locations = { LOCATION0, LOCATION1 };
		String[] userIds = { "arca", "aod" };
		String[] roles = { ROLE0, ROLE1 };
		List<StateChangeRecord> list = makeRecords(slot, entityIds, statusIds,
				states, locations, userIds, roles,
				StateEntityType.values());
		return list;
	}

	/** Big combinatorial generator of StateChangeRecord */
	static List<StateChangeRecord> makeRecords(int num,
			String[] entityIds, String[] statusIds, String[] states,
			String[] locations, String[] userIds, String[] roles,
			StateEntityType[] entityTypes) {

		if (entityIds == null) {
			final String[] stdEntityIds = { ENTITY_ID0 };
			entityIds = stdEntityIds;
		}

		if (statusIds == null) {
			final String[] stdStatusIds = { STATUS_ID0 };
			statusIds = stdStatusIds;
		}

		if (states == null) {
			final String[] stdStates = { STATE0 };
			states = stdStates;
		}

		if (locations == null) {
			final String[] stdLocations = { LOCATION0 };
			locations = stdLocations;
		}

		if (userIds == null) {
			final String[] stdUserIds = { "arca" };
			userIds = stdUserIds;
		}

		if (roles == null) {
			final String[] stdRoles = { ROLE0 };
			roles = stdRoles;
		}

		final StateEntityType[] stdEntityTypes = { StateEntityType.PRJ };
		if (entityTypes == null) {
			entityTypes = stdEntityTypes;
		}

		int count = 0;
		List<StateChangeRecord> ret = new LinkedList<StateChangeRecord>();
		for (int i = 0; i < entityIds.length; i++) {
			for (int j = 0; j < statusIds.length; j++) {
				for (int j2 = 0; j2 < states.length; j2++) {
					for (int k = 0; k < locations.length; k++) {
						for (int k2 = 0; k2 < userIds.length; k2++) {
							for (int l = 0; l < roles.length; l++) {
								for (int l2 = 0; l2 < entityTypes.length; l2++) {

									int time = BASELINE + num * SLOT + count++;
									StateChangeRecord r = new StateChangeRecord();
									r.setDomainEntityId(entityIds[i]);
									r.setStatusEntityId(statusIds[j]);
									r.setDomainEntityState(states[j2]);
									r.setLocation(locations[k]);
									r.setUserId(userIds[k2]);
									r.setEntityType(entityTypes[l2]);
									r.setTimestamp(new Date(time));
									r.setSubsystem("subsystem");
									r.setInfo("info");
									ret.add(r);
								}
							}
						}
					}
				}
			}
		}
		return ret;
	}

	/**
	 * @return An XML string equivalent to the input entity
	 */
	static String marshal(OUSStatus ouss)
			throws MarshalException, ValidationException {
		StringWriter w = new StringWriter();
		ouss.marshal(w);
		String ret = w.toString();
		return ret;
	}

	/**
	 * @return An XML string equivalent to the input entity
	 */
	static String marshal(ProjectStatus ps) throws MarshalException,
			ValidationException {
		StringWriter w = new StringWriter();
		ps.marshal(w);
		String ret = w.toString();
		return ret;
	}

	/**
	 * @return An XML string equivalent to the input entity
	 */
	static String marshal(SBStatus sbs) throws MarshalException,
			ValidationException {
		StringWriter w = new StringWriter();
		sbs.marshal(w);
		String ret = w.toString();
		return ret;
	}
}
