/**
 * TestOUSStatusPF.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.lifecycle.PersistenceTestCase;
import alma.obops.utils.DbUtilities;

/**
 * @author amchavan, Jun 8, 2009
 * @version $Revision$
 */

// $Id$

public class TestOUSStatusPF extends PersistenceTestCase {

    private OUSStatusPF facade;

    public void setUp() {
        super.setUp();
        ProjectStatus ops = Utils.makeProjectStatus();
        OUSStatus program = Utils.findOUSStatus(ops.getObsProgramStatusRef().getEntityId());
        OUSStatus childOus = Utils.findOUSStatus(
        		program.getOUSStatusChoice().getOUSStatusRef()[0].getEntityId());
        facade = new OUSStatusPF( childOus, 
                                         ops.getObsProjectRef().getEntityId() );
    }

    //  Make sure the constructor initializes all relevant fields
    public void testConstructor() {
    	assertNotNull( facade.getDomainEntityId() );      // partId of ous
        assertNotNull( facade.getObsProjectId() );
        assertNotNull( facade.getProjectStatusId() );
        assertNotNull( facade.getParentOusStatusId() );
        assertNotNull( facade.getDomainEntityState() );
        assertNotNull( facade.getStatusEntity() );
        assertNotNull( facade.getStatusEntityId() );
        assertNotNull( facade.getXml() );
    }

    // Simple test: write our facade object and check that it was indeed written
    public void testWrite() throws HibernateException, SQLException {
        Session session = getCurrentSession();
        session.beginTransaction();
        session.save( facade );
        session.getTransaction().commit();
        session = getCurrentSession();
        session.beginTransaction();
        ResultSet rs = DbUtilities.query( "select xml from obs_unit_set_status", 
                                          session.connection() );
        rs.beforeFirst();
        assertTrue( rs.next() );
        final String ref = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<OUSStatus xmlns=\"Alma/Scheduling/OUSStatus\"\n"+
            "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
        assertTrue( rs.getString( 1 ).startsWith( ref ));
        session.getTransaction().commit();
    }
    
    // Simple test: write our facade object and read it back
    @SuppressWarnings("unchecked")
    public void testWriteRead() throws HibernateException, SQLException {

        beginTransaction();
        getCurrentSession().save( facade );
        commitTransaction();
        
        beginTransaction();
        String hql = "from OUSStatusPF";
        List<OUSStatusPF> facades = 
            (List<OUSStatusPF>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( facades );
        assertEquals( 1, facades.size() );
        OUSStatusPF f = (OUSStatusPF) facades.get( 0 );
        assertEquals( facade.getXml(), f.getXml() );
        commitTransaction();
    }
}
