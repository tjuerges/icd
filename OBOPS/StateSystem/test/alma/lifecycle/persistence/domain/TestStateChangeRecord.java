/**
 * TestStateChangRecord.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import alma.lifecycle.PersistenceTestCase;

/**
 * @author amchavan, Jul 7, 2009
 * @version $Revision$
 */

// $Id$

public class TestStateChangeRecord extends PersistenceTestCase {
    
    public void setUp() {
        super.setUp();
    }
    
    public void testConstructor() {

        try {
            new StateChangeRecord( "", "", "", "", new Date(), "", "0", "", "", 
                                   null );
        }
        catch( Exception e ) {
            // no-op, expected
        }
    }
    
    // Simple test: write our facade object and read it back
    @SuppressWarnings("unchecked")
    public void testWriteRead() throws HibernateException, SQLException {

        StateChangeRecord record = 
            new StateChangeRecord( "", "", "", "", new Date(), "", "0", "", "", 
            			StateEntityType.PRJ );
        
        beginTransaction();
        getCurrentSession().save( record );
        commitTransaction();
        
        beginTransaction();
        String hql = "from StateChangeRecord";
        List<StateChangeRecord> results = 
            (List<StateChangeRecord>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( results );
        assertEquals( 1, results.size() );
        StateChangeRecord f = (StateChangeRecord) results.get( 0 );
        assertEquals( record.getTimestamp(), f.getTimestamp() );
        assertEquals( record.getId(), f.getId() );
        commitTransaction();
    }
}
