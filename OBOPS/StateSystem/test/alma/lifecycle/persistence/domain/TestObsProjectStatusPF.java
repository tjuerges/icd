/**
 * TestProjectStatusPF.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.hibernate.HibernateException;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.lifecycle.PersistenceTestCase;
import alma.obops.utils.DbUtilities;

/**
 * @author amchavan, Jun 8, 2009
 * @version $Revision$
 */

// $Id$

public class TestObsProjectStatusPF extends PersistenceTestCase {

    private ObsProjectStatusPF facade;

    public void setUp() {
        super.setUp();
        ProjectStatus ops = Utils.makeProjectStatus();
        try {
			facade = new ObsProjectStatusPF( ops );
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }

    //  Make sure the constructor initializes all relevant fields
    public void testConstructor() {
        assertNotNull( facade.getDomainEntityId() );
        assertNotNull( facade.getObsProgramStatusId() );
        assertNotNull( facade.getObsProjectId() );
        assertNotNull( facade.getProjectStatusId() );
        assertNotNull( facade.getDomainEntityState() );
        assertNotNull( facade.getStatusEntity() );
        assertNotNull( facade.getStatusEntityId() );
        assertNotNull( facade.getXml() );
    }

    // Simple test: write our facade object and check that it was indeed 
    // written with an independent query
    public void testWrite() throws HibernateException, SQLException {
        beginTransaction();
        getCurrentSession().save( facade );
        commitTransaction();
        
        getCurrentSession().beginTransaction();
        ResultSet rs = DbUtilities.query( "select xml from obs_project_status", 
                                          getConnection() );
        rs.beforeFirst();
        assertTrue( rs.next() );
        final String ref = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<ProjectStatus xmlns=\"Alma/Scheduling/ProjectStatus\"\n"+
            "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
        assertTrue( rs.getString( 1 ).startsWith( ref ));
        commitTransaction();
    }
    
    // Simple test: write our facade object and read it back
    @SuppressWarnings("unchecked")
    public void testWriteRead() throws HibernateException, SQLException {

        beginTransaction();
        getCurrentSession().save( facade );
        commitTransaction();
        
        beginTransaction();
        String hql = "from ObsProjectStatusPF";
        List<ObsProjectStatusPF> facades = 
            (List<ObsProjectStatusPF>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( facades );
        assertEquals( 1, facades.size() );
        ObsProjectStatusPF f = (ObsProjectStatusPF) facades.get( 0 );
        assertEquals( facade.getXml(), f.getXml() );
        commitTransaction();
    }
}
