/**
 * TestSchedBlockStatusPF.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.lifecycle.PersistenceTestCase;
import alma.obops.utils.DbUtilities;

/**
 * @author amchavan, Jun 8, 2009
 * @version $Revision$
 */

// $Id$

public class TestSchedBlockStatusPF extends PersistenceTestCase {

    protected SchedBlockStatusPF facade;

    public void setUp() {
        super.setUp();

        ProjectStatus ops = Utils.makeProjectStatus();
        SBStatus sbs = Utils.findSBStatus( ops );
        facade = new SchedBlockStatusPF( sbs, ops.getObsProjectRef().getEntityId() );
    }

    //  Make sure the constructor initializes all relevant fields
    public void testConstructor() {
        assertNotNull( facade.getDomainEntityId() );
        assertNotNull( facade.getObsProjectId() );
        assertNotNull( facade.getProjectStatusId() );
        assertNotNull( facade.getParentOusStatusId() );
        assertNotNull( facade.getDomainEntityState() );
        assertNotNull( facade.getStatusEntity() );
        assertNotNull( facade.getStatusEntityId() );
        assertNotNull( facade.getXml() );
    }

    // Simple test: write our facade object and check that it was indeed written
    public void testWrite() throws HibernateException, SQLException {
        Session session = getCurrentSession();
        session.beginTransaction();
        session.save( facade );
        session.getTransaction().commit();
        session = getCurrentSession();
        session.beginTransaction();
        ResultSet rs = DbUtilities.query( "select xml from sched_block_status", 
                                          session.connection() );
        rs.beforeFirst();
        assertTrue( rs.next() );

        final String ref = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
            "<SBStatus xmlns=\"Alma/Scheduling/SBStatus\"\n" + 
            "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
        assertTrue( rs.getString( 1 ).startsWith( ref ) );
        session.getTransaction().commit();
    }
    
    // Simple test: write our facade object and read it back
    @SuppressWarnings("unchecked")
    public void testWriteRead() throws HibernateException, SQLException {

        beginTransaction();
        getCurrentSession().save( facade );
        commitTransaction();
        
        beginTransaction();
        String hql = "from SchedBlockStatusPF";
        List<SchedBlockStatusPF> facades = 
            (List<SchedBlockStatusPF>) 
            getCurrentSession().createQuery( hql ).list();
        assertNotNull( facades );
        assertEquals( 1, facades.size() );
        SchedBlockStatusPF f = (SchedBlockStatusPF) facades.get( 0 );
        assertEquals( facade.getXml(), f.getXml() );
        commitTransaction();
    }
}
