/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * TODO
 *
 * @author amchavan, Jun 8, 2009
 * @version $Revision$
 */

// $Id$
public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite(
                "Test for alma.lifecycle.persistence.domain" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestSchedBlockStatusPF.class );
        suite.addTestSuite( TestStateChangeRecord.class );
        suite.addTestSuite( TestObsProjectStatusPF.class );
        suite.addTestSuite( TestOUSStatusPF.class );
        //$JUnit-END$
        return suite;
    }

}
