/*
 * Copyright European Southern Observatory 2006
 */

package alma.lifecycle;


import static alma.lifecycle.config.SpringConstants.TEST_STATE_SYSTEM_SPRING_CONFIG;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.AbstractTransactionalSpringContextTests;

import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.lifecycle.config.SpringConstants;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.dao.DataAccessObject;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.obops.utils.HsqldbUtilities;
import alma.obops.utils.config.ConfigConstants;

/**
 * Superclass of all test cases in the Data Access Module, sets up a generic
 * test environment based on Hibernate, Spring and an in-memory HSQLDB database.<br/>
 * 
 * Configuration takes place by way of the Spring configuration file
 * <em>only</em> ({@value SpringConstants#TEST_SPRING_CONFIG}); that is, no archiveConfig.properties is
 * involved.<br/>
 * 
 * 
 * @version $Revision: 1.13 $
 * @author amchavan, Sep 11, 2007
 */

// $Id: SpringTestCase.java,v 1.13 2011/02/03 10:28:56 rkurowsk Exp $

public abstract class SpringTestCase extends AbstractTransactionalSpringContextTests {

    protected Logger logger = Logger.getAnonymousLogger();
    
    protected static final int NUM_SBS_PER_OUS = 2;
    protected static final int NUM_LEAF_OUSS = 2;
    
    private static final String DDL = "sql/hsqldb-ddl.sql";
    
    /* these two are injected by Spring */
    protected StateArchive stateArchive;
	protected DataAccessObject stateArchiveHibernateDao;

    protected ProjectStatus ops;
    protected OUSStatus ouss;
    protected SBStatus sbs;
    protected OUSStatus[] oussList;
    protected SBStatus[] sbsList;
    protected StateChangeRecord scr;
    
	public SpringTestCase(){
		super();
    	// prevent each transaction from rolling back when it is ended
        // we do our own table cleanup manually in onTearDownInTransaction()
        this.setDefaultRollback(false);
        setAutowireMode(AUTOWIRE_BY_NAME);
    }
    
    public DataAccessObject getStateArchiveHibernateDao() {
		return stateArchiveHibernateDao;
	}

	public void setStateArchiveHibernateDao(
			DataAccessObject stateArchiveHibernateDao) {
		this.stateArchiveHibernateDao = stateArchiveHibernateDao;
	}

	public StateArchive getStateArchive() {
		return stateArchive;
	}

	public void setStateArchive(StateArchive stateArchive) {
		this.stateArchive = stateArchive;
	}

	/* (non-Javadoc)
	 * @see org.springframework.test.AbstractSingleSpringContextTests#loadContext(java.lang.Object)
	 */
	@Override
	protected ConfigurableApplicationContext loadContext(Object key)
			throws Exception {
		
		if(!StateSystemContextFactory.INSTANCE.isInitialized()){
	    	// initialize the contextFactory using the tmcdbConfiguration file
			StateSystemContextFactory.INSTANCE.init(TEST_STATE_SYSTEM_SPRING_CONFIG, 
					new StateArchiveDbConfig(logger));
			
			stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
	    	stateArchive.initStateArchive(logger);
	    	
        	String vendor = StateSystemContextFactory.INSTANCE.getConnectionVendor();
        	logger.info("DB Vendor = " + vendor);
		}		
		
		return (ConfigurableApplicationContext)StateSystemContextFactory.INSTANCE.getAppContext();
		
	}
	
    /*(non-Javadoc)
	 * This tells AbstractTransactionalSpringContextTestss where to find the Spring application context
	 * It uses this for dependency injection into the Testcases 
	 * @see
	 * org.springframework.test.AbstractSingleSpringContextTests#getConfigLocations
	 * ()
	 */
	protected String[] getConfigLocations() {
		return new String[] { TEST_STATE_SYSTEM_SPRING_CONFIG };
	}

    /**
     * Sets up a generic test environment based on Hibernate and some database
     * backend -- as per {@link SpringConstants#TEST_SPRING_CONFIG}.
     * <p/>
     * 
     * @see junit.framework.TestCase#setUp()
     */
    public void onSetUpBeforeTransaction() {
    	
    	this.setDefaultRollback(false);
//    	
//        if( !TestUtilities.isCanonicalTestDir() ) {
//            fail( TestUtilities.CANONICAL_DIR_MSG );
//        }
        
    }

    /**
     * Create a valid empty database, but only if it is an HSQLDB server
     * 
     * @param dropScript  A SQL script to drop all tables in the database
     * @param createScript  A SQL script to create all tables in the database
     */
    protected void createAllTAbles(String createScript ) {
        try {
        	
        	String vendor = StateSystemContextFactory.INSTANCE.getConnectionVendor();
        	if(ConfigConstants.VENDOR_HSQLDB.equalsIgnoreCase(vendor)){
        		String url = StateSystemContextFactory.INSTANCE.getConnectionUrl();
        		HsqldbUtilities.createDatabase(url, DDL);
        	}else{
        		// do Oracle cleanup
        	}
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }

    }
    
	/**
	 * Convenience method to commit existing transaction and start a new one.
	 */
	protected void commitAndStartNewTransaction(){
	
		try {
		   this.endTransaction();
		   this.startNewTransaction();
		}
		catch(Throwable th) {
			logger.log(Level.WARNING, "DamTestCase.commitAndStartNewTransaction caught an unexpected throwable: " + th);
			th.printStackTrace();
		}
	}
	
	/*
	 * Use this end transaction method after a test that would have thrown an exception and
	 * marked the current transaction for rollback.
	 */
	protected void endTransactionAndIgnoreExceptions(){
        try{
        	endTransaction();
        }catch (Exception ex) {
        	// Ignore transaction rollback exceptions
		}
	}


	/**
	 * helper method to get property files using classloader
	 * @param clazz
	 * @param resource
	 * @return
	 */
	protected InputStream getInputStream(String resource) {
		
		InputStream stream = this.getClass().getResourceAsStream(resource);

		if (stream == null) {
			fail("Resource " + resource + " not found");
		}

		URL res = this.getClass().getResource(resource);
		logger.log(Level.WARNING, "'" +  resource + "' loaded from: " + res.getPath());
		
		return stream;
	}
	
	
	@Override
	protected void onSetUp() throws Exception {
		super.onSetUp();
        Utils.reset();
        
        ops  = Utils.makeProjectStatus( NUM_LEAF_OUSS, NUM_SBS_PER_OUS );
        sbs  = Utils.findSBStatus( ops );
        ouss = Utils.findOUSStatus( ops );
        scr  = new StateChangeRecord( "1", "1", "1", "1", new Date(), "1", "0", "1", "1",
        		StateEntityType.SBK);

        sbsList = Utils.getSBStatuses().toArray( new SBStatus[0] );
        oussList = Utils.getOUSStatuses().toArray( new OUSStatus[0] );
	}

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		createAllTAbles(DDL);
	}
//
//	@Override
//	protected void onTearDownAfterTransaction() throws Exception {
//		super.onTearDownAfterTransaction();
//	}
//
	@Override
	protected void onTearDownInTransaction() throws Exception {
		super.onTearDownInTransaction();
		String dbVendor = StateSystemContextFactory.INSTANCE.getConnectionVendor();
    	if(ConfigConstants.VENDOR_ORACLE.equalsIgnoreCase(dbVendor)){
    		cleanStateTables();
    	}
	}

    @SuppressWarnings("unchecked")
	private void cleanStateTables(){
    	
    	List<SchedBlockStatusPF> sbStatusPfList = (List<SchedBlockStatusPF>)
    		stateArchiveHibernateDao.findAll(SchedBlockStatusPF.class);
    	for(SchedBlockStatusPF sbStatusPf : sbStatusPfList){
    		stateArchiveHibernateDao.delete(sbStatusPf);
    		for(SBStatus testSbStatus : sbsList){
    			if(testSbStatus.getSBStatusEntity().getEntityId()
    					.equals(sbStatusPf.getStatusEntityId())){
    				stateArchiveHibernateDao.delete(sbStatusPf);
    				break;
    			}
    		}
    	}
    	
    	List<OUSStatusPF> ousStatusPFList = (List<OUSStatusPF>)
    		stateArchiveHibernateDao.findAll(OUSStatusPF.class);
    	for(OUSStatusPF ousStatusPf : ousStatusPFList){
    		for(OUSStatus testOusStatus : oussList){
    			if(testOusStatus.getOUSStatusEntity().getEntityId()
    					.equals(ousStatusPf.getStatusEntityId())){
    				stateArchiveHibernateDao.delete(ousStatusPf);
    				break;
    			}
    		}
    	}
    	
    	List<ObsProjectStatusPF> obsProjectStatusPFList = (List<ObsProjectStatusPF>)
    		stateArchiveHibernateDao.findAll(ObsProjectStatusPF.class);
    	for(ObsProjectStatusPF obsProjectStatusPf : obsProjectStatusPFList){
    		if(ops.getObsProgramStatusRef().getEntityId()
    				.equals(obsProjectStatusPf.getStatusEntityId())){
    			stateArchiveHibernateDao.delete(obsProjectStatusPf);
    			break;
    		}
    	}
    }
    
}
