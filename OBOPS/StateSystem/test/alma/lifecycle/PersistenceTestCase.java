/**
 * TestPersistenceFacade.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle;

import java.sql.Connection;
import java.util.Date;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.utils.HibernateUtils;
import alma.obops.utils.HsqldbUtilities;
import alma.obops.utils.HsqldbUtilities.Mode;

/**
 * Common abstract superclass of all tests in this package
 *
 * @author amchavan, Jun 18, 2009
 * @version $Revision$
 */

// $Id$

public abstract class PersistenceTestCase extends TestCase {

    protected static final int NUM_SBS_PER_OUS = 2;
    protected static final int NUM_LEAF_OUSS = 2;
    
    private SessionFactory factory;
    
    protected StateArchive archive;
    protected ProjectStatus ops;
    protected OUSStatus ouss;
    protected SBStatus sbs;
    protected OUSStatus[] oussList;
    protected SBStatus[] sbsList;
    protected StateChangeRecord scr;


    public void setUp() {
        factory = HibernateUtils.getSessionFactory();
        beginTransaction();
        try {
            HsqldbUtilities.createDatabase( Mode.Memory,
                                            "statusdb", 
                                            null,   // in-memory
                                            "sql/hsqldb-ddl.sql", 
                                            getConnection() );
        }
        catch( Exception e ) {
            e.printStackTrace();
            fail( e.getMessage() );
        }
        commitTransaction();
        
        Utils.reset();
        
        ops  = Utils.makeProjectStatus( NUM_LEAF_OUSS, NUM_SBS_PER_OUS );
        sbs  = Utils.findSBStatus( ops );
        ouss = Utils.findOUSStatus( ops );
        scr  = new StateChangeRecord( "", "", "", "", new Date(), "", "0", "", "",
        		StateEntityType.SBK );

        sbsList  = Utils.getSBStatuses().toArray( new SBStatus[0] );
        oussList = Utils.getOusStatusSequence();
    }

    protected void beginTransaction() {
        Session session = getCurrentSession();
        session.beginTransaction();
    }
    
    protected void commitTransaction() {
        Session session = getCurrentSession();
        session.getTransaction().commit();
    }

    public void tearDown() {
        Session session = getCurrentSession();
        session.close();
        factory.close();
    }
    
    /** @return The current Hibernate session */
    protected Session getCurrentSession() {
        return factory.getCurrentSession();
    }
    
    /** @return The current JDBC connection */
    protected Connection getConnection() {
        return getCurrentSession().connection();
    }
}
