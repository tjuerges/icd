package alma.lifecycle.remote;

import java.util.Collection;
import java.util.logging.Logger;

import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.entityutil.EntitySerializer;
import alma.asdmIDLTypes.IDLArrayTime;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.projectlifecycle.StateSystem;
import alma.projectlifecycle.StateSystemHelper;
import alma.projectlifecycle.StateSystemJ;
import alma.projectlifecycle.StateSystemOperations;
import alma.statearchiveexceptions.NoSuchEntityEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * JUnit test client for the StateSystem. 
 * 
 * @author sharring
 */
public class RemoteStateSystemMockTestCase extends ComponentClientTestCase
{
   private static final String OBOPS_LIFECYCLE_STATE_SYSTEM = "OBOPS_MOCK_LIFECYCLE_STATE_SYSTEM";

   // standard CORBA interface
   private StateSystem m_stateSystemComp;
   
   // transparent-XML interface
   private StateSystemJ m_stateSystemCompJ;
   
   private ProjectStatus projectStatus;
   
   public RemoteStateSystemMockTestCase() throws Exception 
   {
      super("RemoteStateSystemMockTestCase");
   }
   
   private synchronized void releaseComponents(ContainerServices services, Logger logger)
   {
	   services.releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
   }
   
   private synchronized void getComponents(ContainerServices services, Logger logger)
   {
      try 
      {
         org.omg.CORBA.Object compObj = null;
      
         compObj = services.getComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);

         assertNotNull(compObj);
      
         m_stateSystemComp = StateSystemHelper.narrow(compObj);
         assertNotNull(m_stateSystemComp);
      
         projectStatus = Utils.makeProjectStatus(2, 2, StatusTStateType.APPROVED);
         XmlEntityStruct entityStruct = EntitySerializer.getEntitySerializer(logger).serializeEntity(projectStatus);
         m_stateSystemComp.updateProjectStatus(entityStruct);

// 		 Previous implementation. We leave it here in case the replacement is not that one
//       m_stateSystemCompJ = services.getTransparentXmlComponent(
//                 StateSystemJ.class, m_stateSystemComp, StateSystemOperations.class);
         
         m_stateSystemCompJ = services.getTransparentXmlWrapper(
               StateSystemJ.class, m_stateSystemComp, StateSystemOperations.class);
         assertNotNull(m_stateSystemCompJ);
      }
      catch(Exception ex)
      {
         ex.printStackTrace();
         fail(ex.getMessage());
      }
   }

   /**
    * Tests that the component name method a) succeeds, and b) returns the expected value.
    * 
    * @throws Exception
    */
   public void testComponentName() throws Exception {
	   try {
		   getComponents(this.getContainerServices(), this.m_logger);
		   assertEquals(OBOPS_LIFECYCLE_STATE_SYSTEM, m_stateSystemComp.name());
	   } catch(Throwable th) {
		   fail(th.getMessage());
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the getOUSStatus method call.
    * 
    * @throws Exception
    */
   public void testGetOUSStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
         String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getOUSStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getOUSStatusXml method call.
    * 
    * @throws Exception
    */
   public void testGetOUSStatusXml() throws Exception 
   {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
         String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getOUSStatusXml(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getProjectStatus method call.
    * 
    * @throws Exception
    */
   public void testGetProjectStatus() throws Exception 
   {  
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
         String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getProjectStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getProjectStatusList method call.
    * 
    * @throws Exception
    */
   public void testGetProjectStatusList() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getProjectStatusList(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getProjectStatusXml method call.
    * 
    * @throws Exception
    */
   public void testGetProjectStatusXml() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getProjectStatusXml(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getProjectStatusXmlList method call.
    * 
    * @throws Exception
    */
   public void testGetProjectStatusXmlList() throws Exception {
      try {
         getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getProjectStatusXmlList(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatus method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
         String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatusListForObsUnitSet method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatusListForObsUnitSet() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatusListForOUSStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatusListForProjectStatus method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatusListForProjectStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatusListForProjectStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatusXml method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatusXml() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatusXml(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatusXmlListForOUSStatus method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatusXmlListForOUSStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatusXmlListForOUSStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getSBStatusXmlListForProjectStatus method call.
    * 
    * @throws Exception
    */
   public void testGetSBStatusXmlListForProjectStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 String uid = Utils.makeNewEntityId();
         m_stateSystemComp.getSBStatusXmlListForProjectStatus(uid);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the getObsProjectStates method
    * @throws Exception
    */
   public void testGetObsProjectStates() throws Exception {
	   String subsystem = Subsystem.OBOPS;
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   m_stateSystemComp.getObsProjectStates(subsystem);
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }

   /**
    * Tests the getObsProjectStates method
    * @throws Exception
    */
   public void testGetObsUnitSetStates() throws Exception {
	   String subsystem = Subsystem.OBOPS;
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   m_stateSystemComp.getObsUnitSetStates(subsystem);
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }

   /**
    * Tests the getObsProjectStates method
    * @throws Exception
    */
   public void testGetSchedBlockStates() throws Exception {
	   String subsystem = Subsystem.OBOPS;
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   m_stateSystemComp.getSchedBlockStates(subsystem);
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the findStateChangeRecords method call.
    */
   public void testFindStateChangeRecords() {
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   IDLArrayTime start = new IDLArrayTime();
		   IDLArrayTime end = new IDLArrayTime();
		   String domainEntityId = "1";
		   String state = "2";
		   String userId = "3";
		   String type = StateEntityType.PRJ.toString();
		   m_stateSystemComp.findStateChangeRecords(start, end, domainEntityId, state, userId, type);
	   } 
	   catch(Throwable th) {
		   fail(th.getMessage());
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the findProjectStatusByState method call.
    */
   public void testFindProjectStatusByState()
   {
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   String[] states = { StatusTStateType.APPROVED.toString(), StatusTStateType.CANCELED.toString() };
		   m_stateSystemComp.findProjectStatusByState(states);
	   } 
	   catch(Throwable th) {
		   fail(th.getMessage());
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the findSBStatusByState method call.
    */
   public void testFindSBStatusByState()
   {
	   try {
		   getComponents(this.getContainerServices(), this.m_logger); 
		   String[] states = { StatusTStateType.APPROVED.toString(), StatusTStateType.CANCELED.toString() };
		   m_stateSystemComp.findSBStatusByState(states);
	   } 
	   catch(Throwable th) {
		   fail(th.getMessage());
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the putOUSStatus method call.
    * 
    * @throws Exception
    */
   public void testPutOUSStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
    	 Collection<OUSStatus> obsUnitSetStatuses = Utils.getOUSStatuses();
                  
         assert(1 <= obsUnitSetStatuses.size());
         
         OUSStatus ous = obsUnitSetStatuses.iterator().next();
         XmlEntityStruct xmlEntity = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(ous);
         
         m_stateSystemComp.updateOUSStatus(xmlEntity);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
		   fail(th.getMessage());
	   } finally {
		   releaseComponents(this.getContainerServices(), this.m_logger);
	   }
   }
   
   /**
    * Tests the putProjectObsUnitSetAndSBStatuses method call.
    * 
    * @throws Exception
    */
   public void testPutProjectObsUnitSetAndSBStatuses() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger);
    	 XmlEntityStruct projXmlEntity = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(projectStatus);
         
         OUSStatus[] ousStatuses = Utils.getOUSStatuses().toArray(new OUSStatus[0]);
         assert(1 <= ousStatuses.length);
         
         XmlEntityStruct[] ousStatusStructs = new XmlEntityStruct[ousStatuses.length];
         int count = 0;
         for(OUSStatus status: ousStatuses) {
            ousStatusStructs[count++] = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(status);
         }

         SBStatus[] sbStatuses = Utils.getSBStatuses().toArray(new SBStatus[0]);
         assert(1 <= sbStatuses.length);
         
         XmlEntityStruct[] sbStatusStructs = new XmlEntityStruct[sbStatuses.length];
         count = 0;
         for(SBStatus status: sbStatuses) {
            sbStatusStructs[count++] = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(status);
         }
         
         m_stateSystemComp.insert(projXmlEntity, ousStatusStructs, sbStatusStructs);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
		   fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the putProjectStatus method call.
    * 
    * @throws Exception
    */
   public void testPutProjectStatus() throws Exception { 
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 	
         XmlEntityStruct xmlEntity = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(projectStatus);
         m_stateSystemComp.updateProjectStatus(xmlEntity);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the putSBStatus method call.
    * 
    * @throws Exception
    */
   public void testPutSBStatus() throws Exception {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 SBStatus sbStatus = Utils.makeSBStatus();
         XmlEntityStruct xmlEntity = EntitySerializer.getEntitySerializer(m_logger).serializeEntity(sbStatus);
         m_stateSystemComp.updateSBStatus(xmlEntity);
      } 
      catch (NoSuchEntityEx e) {
         // expected exception
      }
      catch(Throwable th) {
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }

   /**
    * Tests the changeProjectState method call.
    * 
    * @throws Exception
    */
   public void testChangeProjectState() {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
    	 m_stateSystemComp.changeProjectStatus(projectStatus.getProjectStatusEntity().getEntityId(), StatusTStateType.CANCELED.toString(), Subsystem.OBOPS, "0");
      }
      catch(Throwable th) {
    	  th.printStackTrace();
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the changeProjectState method call.
    * 
    * @throws Exception
    */
   public void testChangeObsUnitSetState() { 
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 	
         m_stateSystemComp.changeOUSStatus(projectStatus.getObsProgramStatusRef().getEntityId(), StatusTStateType.CANCELED.toString(), Subsystem.OBOPS, "0");
      } catch(Throwable th) {
    	  th.printStackTrace();
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
   
   /**
    * Tests the changeProjectState method call.
    * 
    * @throws Exception
    */
   public void testChangeSchedBlockState() {
      try {
    	 getComponents(this.getContainerServices(), this.m_logger); 
         SBStatus sbStatus = Utils.makeSBStatus();
         m_stateSystemComp.changeSBStatus(sbStatus.getSBStatusEntity().getEntityId(), StatusTStateType.CANCELED.toString(), Subsystem.OBOPS, "0");
      } catch(Throwable th) {
    	  th.printStackTrace();
    	  fail(th.getMessage());
      } finally {
    	  releaseComponents(this.getContainerServices(), this.m_logger);
      }
   }
}

