package alma.lifecycle.remote.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.PersistenceTestCase;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

public class RemoteStateSystemClientTestCase extends PersistenceTestCase 
{
	private static RemoteStateSystemClient client;
	private static String managerLoc;
	
	private static synchronized void instantiateClientIfNeeded() throws Exception 
	{
		if(null == managerLoc) {
			managerLoc = getManagerLoc();
		}
		
		assertNotNull(managerLoc);
		
		if(null == client) {
			client = new RemoteStateSystemClient(Logger
					.getLogger("RemoteStateSystemClientTestCase"), managerLoc);
		}
	}
	
	public void testInstantiation() throws Exception {
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
	}
	
	public void testFindSBStatusByState() throws Exception
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		String[] states = { StatusTStateType.APPROVED.toString(), StatusTStateType.CANCELED.toString() };
		SBStatus[] statuses = client.findSBStatusByState(states);
	}
	
	public void testFindProjectStatusByState() throws Exception
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		String[] states = { StatusTStateType.APPROVED.toString(), StatusTStateType.CANCELED.toString() };
		ProjectStatus[] statuses = client.findProjectStatusByState(states);
	}

	
	public void testGetObsProjectStates() throws Exception
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		String subsystem = Subsystem.OBOPS;
		String states = client.getObsProjectStates(subsystem);
		assertNotNull(states);
	}
	
	public void testGetObsUnitSetStates() throws Exception
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		String subsystem = Subsystem.OBOPS;
		String states = client.getObsUnitSetStates(subsystem);
		assertNotNull(states);
	}

	public void testGetSchedBlockStates() throws Exception
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		String subsystem = Subsystem.OBOPS;
		String states = client.getSchedBlockStates(subsystem);
		assertNotNull(states);
	}

	public void testGetProjectStatusList() throws Exception {
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		
		// first create and insert a test project
		Utils.reset();
		ProjectStatus target = Utils.makeProjectStatus();
		try {
			client.insert(target, Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
		} catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		// now try to get it using the getProjectStatusList method
		StatusBaseT[] statuses = client.getProjectStatusList(target.getProjectStatusEntity());
		assertNotNull(statuses);
		
		boolean foundProjStatus = false;		
		for(StatusBaseT status : statuses) {
			if(status instanceof ProjectStatus) {
				ProjectStatus prjStatus = (ProjectStatus) status;
				if(prjStatus.getProjectStatusEntity().getEntityId().equals(target.getProjectStatusEntity().getEntityId())) {
					foundProjStatus = true;
					break;
				}
			}
		}
		assertTrue(foundProjStatus);

		Collection<SBStatus> sbStatusCollection = Utils.getSBStatuses();
		if(sbStatusCollection.size() > 0) 
		{
			boolean[] foundSbStatus = new boolean[sbStatusCollection.size()];
			int i = 0;
			for(SBStatus status : sbStatusCollection) 
			{
				foundSbStatus[i] = false;
				for(StatusBaseT statusOut : statuses) 
				{
					if(statusOut instanceof SBStatus) 
					{
						SBStatus sbStatus = (SBStatus) statusOut;
						if(sbStatus.getSBStatusEntity().getEntityId().equals(status.getSBStatusEntity().getEntityId()) ) 
						{
							foundSbStatus[i] = true;
						}
					}
				}
				i++;
			}

			for(int j = 0; j < foundSbStatus.length; j++) {
				assertTrue(foundSbStatus[j]);
			}
		}
		
		Collection<OUSStatus> ousStatusCollection = Utils.getOUSStatuses();
		if(ousStatusCollection.size() > 0) 
		{
			boolean[] foundOusStatus = new boolean[ousStatusCollection.size()];
			int i = 0;
			for(OUSStatus status : ousStatusCollection) 
			{
				foundOusStatus[i] = false;
				for(StatusBaseT statusOut : statuses) 
				{
					if(statusOut instanceof OUSStatus) 
					{
						OUSStatus ousStatus = (OUSStatus) statusOut;
						if(ousStatus.getOUSStatusEntity().getEntityId().equals(status.getOUSStatusEntity().getEntityId()) ) 
						{
							foundOusStatus[i] = true;
						}
					}
				}
				i++;
			}

			for(int j = 0; j < foundOusStatus.length; j++) {
				assertTrue(foundOusStatus[j]);
			}
		}
		

	}
	
	public void testChangeProjectStatus() throws Exception 
	{
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);

		// first create and insert a test project
		Utils.reset();
		ProjectStatus target = Utils.makeProjectStatus();
		try {
			client.insert(target, Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		// now try to change its state
		StatusTStateType destination = StatusTStateType.READY;
		String subsystem = Subsystem.OBOPS;
		String userId = RoleProviderMock.ALL_ROLES_USER;

		boolean success = false;
		try {
			success = client.changeState(target.getProjectStatusEntity(),
					destination, subsystem, userId);
		} catch (AcsJNoSuchTransitionEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJNotAuthorizedEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJPreconditionFailedEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJPostconditionFailedEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AcsJIllegalArgumentEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(success);
	}

	public void testChangeProjectStatusWithWrongSubsystem() throws Exception 
	{	
		RemoteStateSystemClientTestCase.instantiateClientIfNeeded();
		assertNotNull(client);
		
		// first create and insert a test project
		Utils.reset();
		ProjectStatus target = Utils.makeProjectStatus();
		try {
			client.insert(target, Utils.getOUSStatuses().toArray(new OUSStatus[0]), 
					Utils.getSBStatuses().toArray(new SBStatus[0]));
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		StatusTStateType destination = StatusTStateType.READY;
		String subsystem = Subsystem.OBSPREP; // wrong subsystem
		String userId = "0";
		boolean threwAuthException = false;
		try {
			try{
				client.changeState(target.getProjectStatusEntity(),
						destination, subsystem, userId);	
				
			}catch(AcsJNotAuthorizedEx ae){
				threwAuthException = true;
			}
			assertTrue("Did not throw AuthorizationException for incorrect subsystem", threwAuthException);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static synchronized String getManagerLoc() throws UnknownHostException,
			FileNotFoundException, IOException 
	{
		if(managerLoc == null) {
			// first, try the property
			managerLoc = System.getProperty("ACS.manager");

			// if property not set, try the env variable
			if (managerLoc == null) {
				managerLoc = System.getenv("MANAGER_REFERENCE");
			}

			// if env variable (and property) not set, use the acs_instance
			// file to determine the port, and then use the localhost to
			// construct the manager reference, by hand
			if (managerLoc == null) {
				String ipaddr = InetAddress.getLocalHost().getHostAddress();
				assertNotNull(ipaddr);

				File acsInstanceFile = new File("./tmp/acs_instance");
				FileReader reader = new FileReader(acsInstanceFile);
				BufferedReader bufReader = new BufferedReader(reader);
				String instanceString = bufReader.readLine();
				assertNotNull(instanceString);

				int acsInstance = Integer.valueOf(instanceString);
				int portInt = 3000 + acsInstance * 100;
				String port = Integer.toString(portInt);
				assertNotNull(port);

				managerLoc = "corbaloc::" + ipaddr + ":" + port + "/Manager";
			}
		}
		return managerLoc;
	}
}
