package alma.lifecycle.remote.client;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests 
{
	 public static Test suite() 
	    {
	        TestSuite suite = new TestSuite( "Test for alma.lifecycle.remote.client" );
	        //$JUnit-BEGIN$
	        suite.addTestSuite( RemoteStateSystemClientTestCase.class );
	        //$JUnit-END$
	        return suite;
	    }
}
