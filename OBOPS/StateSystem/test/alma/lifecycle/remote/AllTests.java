/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.remote;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author Steve Harrington
 */

public class AllTests 
{
    public static Test suite() 
    {
        TestSuite suite = new TestSuite( "Test for alma.lifecycle.remote" );
        //$JUnit-BEGIN$
        suite.addTestSuite( RemoteStateSystemMockTestCase.class );
        suite.addTestSuite( RemoteStateSystemTestCase.class );
        suite.addTest( alma.lifecycle.remote.client.AllTests.suite() );
        //$JUnit-END$
        return suite;
    }
}
