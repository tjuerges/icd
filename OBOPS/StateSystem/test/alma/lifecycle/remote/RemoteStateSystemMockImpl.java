package alma.lifecycle.remote;

import alma.lifecycle.StateSystemBaseImpl;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.StateArchiveMockImpl;
import alma.lifecycle.stateengine.StateEngine;
import alma.lifecycle.stateengine.StateEngineMockImpl;

public class RemoteStateSystemMockImpl extends RemoteStateSystemImpl 
{
	protected void createStateSystem() {
		logger.info(this.name() + " instantiating mock state system");
		StateArchive stateArchive = new StateArchiveMockImpl();
		StateEngine stateEngine = new StateEngineMockImpl();
		stateSystem = new StateSystemBaseImpl( stateArchive, stateEngine, logger );
	}
}

