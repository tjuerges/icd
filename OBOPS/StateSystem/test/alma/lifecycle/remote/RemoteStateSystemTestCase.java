package alma.lifecycle.remote;

import alma.ACSErrTypeCommon.IllegalArgumentEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.projectlifecycle.StateSystem;
import alma.projectlifecycle.StateSystemHelper;

/**
 * JUnit test client for the StateSystem. 
 * 
 * @author sharring
 */
public class RemoteStateSystemTestCase extends ComponentClientTestCase
{
	private static final String OBOPS_LIFECYCLE_STATE_SYSTEM = "OBOPS_LIFECYCLE_STATE_SYSTEM";

	// standard CORBA interface
	private StateSystem m_stateSystemComp;
	
	public RemoteStateSystemTestCase() throws Exception 
	{
		super("RemoteStateSystemTestCase");
	}

	private synchronized StateSystem getStateSystemComponent(ContainerServices services)
	{
		if(m_stateSystemComp == null) {
			org.omg.CORBA.Object compObj = null;
			
			try 
			{
				compObj = services.getComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
			} 
			catch (AcsJContainerServicesEx e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(e.getMessage());
			}
			assertNotNull(compObj);

			m_stateSystemComp = StateSystemHelper.narrow(compObj);
			assertNotNull(m_stateSystemComp);
		}
		return m_stateSystemComp;
	}
			
	/**
	 * Tests that the component name method a) succeeds, and b) returns the expected value.
	 * 
	 * @throws Exception
	 */
	public void testComponentName() throws Exception {
		try {
			m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
			assertEquals(OBOPS_LIFECYCLE_STATE_SYSTEM, m_stateSystemComp.name());
		} catch(Throwable th) {
			th.printStackTrace();
			fail(th.getMessage());
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}
	
	public void testChangeProjectStatusInvalidStateName() 
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		try {
			m_stateSystemComp.changeProjectStatus("ignored target", "should throw IllegalArgumentxception (bad state name)", "ignored subsys", "0");
		} catch(IllegalArgumentEx e) {
			// expected exception
		} catch(Throwable th) {
			th.printStackTrace();
			fail(th.getMessage());
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}
	
	public void testChangeOUSStatusInvalidStateName() 
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		try {
			m_stateSystemComp.changeOUSStatus("ignored target", "should throw IllegalArgumentxception (bad state name)", "ignored subsys", "0");
		} catch(IllegalArgumentEx e) {
			// expected exception
		} catch(Throwable th) {
			th.printStackTrace();
			fail(th.getMessage());
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}
	
	public void testChangeSBStatusInvalidStateName() 
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		try {
			m_stateSystemComp.changeSBStatus("ignored target", "should throw IllegalArgumentxception (bad state name)", "ignored subsys", "0");
		} catch(IllegalArgumentEx e) {
			// expected exception
		}  catch(Throwable th) {
			th.printStackTrace();
			fail(th.getMessage());
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}
	
	public void testGetObsProjectStates()
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		String subsystem = Subsystem.OBOPS;
		try {
			String states = m_stateSystemComp.getObsProjectStates(subsystem);
			assertNotNull(states);
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}

	public void testGetObsUnitSetStates()
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		String subsystem = Subsystem.OBOPS;
		try {
			String states = m_stateSystemComp.getObsUnitSetStates(subsystem);
			assertNotNull(states);
		} finally {
			this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}

	public void testGetSchedBlockStates()
	{
		m_stateSystemComp = getStateSystemComponent(this.getContainerServices());
		String subsystem = Subsystem.OBOPS;
		try {
			String states = m_stateSystemComp.getSchedBlockStates(subsystem);
			assertNotNull(states);
		} finally {
		   this.getContainerServices().releaseComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		}
	}
}

