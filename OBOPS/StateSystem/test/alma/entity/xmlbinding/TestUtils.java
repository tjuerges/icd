/**
 * TestUtils.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusRefT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;

/**
 * @author amchavan, Jul 1, 2009
 * @version $Revision$
 */

// $Id$

public class TestUtils extends TestCase {

    public void testMakeSBStatus() {
        SBStatus sbs = Utils.makeSBStatus();
        assertNotNull( sbs );

        // Check parent OUSStatus
        OUSStatusRefT rParent = sbs.getContainingObsUnitSetRef();
        assertNotNull( rParent );
        OUSStatus parent = 
            Utils.findOUSStatus( rParent.getEntityId() );
        assertNotNull( parent );

        // Check root OUSStatus
        OUSStatusRefT rRoot = parent.getContainingObsUnitSetRef();
        assertNotNull( rRoot );
        OUSStatus root = 
            Utils.findOUSStatus( rRoot.getEntityId() );
        assertNotNull( root );

        // Check ObsProgramStatus
        OUSStatusRefT rProgram = root.getContainingObsUnitSetRef();
        assertNotNull( rProgram );
        OUSStatus program =
                Utils.findOUSStatus( rProgram .getEntityId() );
        assertNotNull( program );

        // Check ProjectStatus
        ProjectStatusRefT rProject = program.getProjectStatusRef();
        assertNotNull( rProject );
        ProjectStatus project = 
            Utils.findProjectStatus( rProject .getEntityId() );
        assertNotNull( project );
        
        try {
            Writer w = new StringWriter();
            project.marshal( w );
            program.marshal( w );
            root.marshal( w );
            parent.marshal( w );
            sbs.marshal( w );
       //     System.out.println( w.toString() );
        }
        catch( Exception e ) {
            fail( e.getClass().getSimpleName() + ": " + e.getMessage() );
        }
    }
    
    public void testMakeProjectStatus() {
        ProjectStatus ps = Utils.makeProjectStatus();

        // Check ObsProgramStatus
        OUSStatusRefT rProgram = ps.getObsProgramStatusRef();
        assertNotNull( rProgram );
        OUSStatus program = 
            Utils.findOUSStatus( rProgram.getEntityId() );
        assertNotNull( program );

        // Check secondary root
        OUSStatusRefT rSecRoot = 
            program.getOUSStatusChoice().getOUSStatusRef( 0 );
        assertNotNull( rSecRoot );
        OUSStatus secRoot = 
            Utils.findOUSStatus( rSecRoot.getEntityId() );
        assertNotNull( secRoot );

        // Secondary root should have two children
        OUSStatusRefT rChild0 = 
            secRoot.getOUSStatusChoice().getOUSStatusRef( 0 );
        assertNotNull( rChild0 );
        OUSStatus child0 = 
            Utils.findOUSStatus( rChild0.getEntityId() );
        assertNotNull( child0 );

        OUSStatusRefT rChild1 = 
            secRoot.getOUSStatusChoice().getOUSStatusRef( 1 );
        assertNotNull( rChild1 );
        OUSStatus child1 = 
            Utils.findOUSStatus( rChild1.getEntityId() );
        assertNotNull( child1 );
        
        // Each child should have two child SBStatus entities
        SBStatusRefT rSBS0 = 
            child0.getOUSStatusChoice().getSBStatusRef( 0 );
        assertNotNull( rSBS0 );
        SBStatus sbs0 = Utils.findSBStatus( rSBS0.getEntityId() );
        assertNotNull( sbs0 );

        SBStatusRefT rSBS1 = 
            child0.getOUSStatusChoice().getSBStatusRef( 1 );
        assertNotNull( rSBS1 );
        SBStatus sbs1 = Utils.findSBStatus( rSBS1.getEntityId() );
        assertNotNull( sbs1 );

        SBStatusRefT rSBS2 = 
            child1.getOUSStatusChoice().getSBStatusRef( 0 );
        assertNotNull( rSBS2 );
        SBStatus sbs2 = Utils.findSBStatus( rSBS2.getEntityId() );
        assertNotNull( sbs2 );

        SBStatusRefT rSBS3 = 
            child1.getOUSStatusChoice().getSBStatusRef( 1 );
        assertNotNull( rSBS3 );
        SBStatus sbs3 = Utils.findSBStatus( rSBS3.getEntityId() );
        assertNotNull( sbs3 );
    }
    
    public void testWalk() {
        ProjectStatus ops = Utils.makeProjectStatus( 1, 1 );
        List<OUSStatus> ouss = new ArrayList<OUSStatus>();
        List<SBStatus> sbs = new ArrayList<SBStatus>();
        Utils.walk( ops, ouss, sbs );

        assertEquals( 3, ouss.size() );
        assertEquals( 1, sbs.size() );
        
        ops = Utils.makeProjectStatus( 2, 2 );
        ouss.clear();
        sbs.clear();
        Utils.walk( ops, ouss, sbs );

        assertEquals( 4, ouss.size() );
        assertEquals( 4, sbs.size() );
    }
    
    /**
     * This is not a test: create a tree of status entities and save it as a set
     * of XML files -- written for Paola S.
     * amchavan, 14-08-2009
     */
    public void createEntityFiles() throws Exception {
        Utils.reset();
        ProjectStatus ops  = Utils.makeProjectStatus( 2, 2 );

        String uid = ops.getProjectStatusEntity().getEntityId();
        Utils.writeEntityToFile( uid, Utils.marshall( ops ) );

        OUSStatus[] oussSequence = Utils.getOusStatusSequence();
        int i = 0;
        for( OUSStatus ouss : oussSequence ) {
            uid = String.valueOf( i++ ) + 
                  "-" + 
                  ouss.getOUSStatusEntity().getEntityId();
            Utils.writeEntityToFile( uid, Utils.marshall( ouss ) );
        }
        
        SBStatus[] sbsList = Utils.getSBStatuses().toArray( new SBStatus[0] );
        i = 0;
        for( SBStatus sbStatus : sbsList ) {
            uid = String.valueOf( i++ ) + 
                  "-" + 
                  sbStatus.getSBStatusEntity().getEntityId();
            Utils.writeEntityToFile( uid, Utils.marshall( sbStatus ) );
        }
    }
}
