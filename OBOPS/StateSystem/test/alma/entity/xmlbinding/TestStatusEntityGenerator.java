/**
 * TestStatusEntityGenerator.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import junit.framework.TestCase;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusChoice;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;

/**
 * @author amchavan, Aug 17, 2009
 * @version $Revision$
 */

// $Id$
public class TestStatusEntityGenerator extends TestCase {

    private StatusEntityGenerator generator;
    private MockUidProvider provider;

    /** Reads an ObsProject from an XML file */
    private ObsProject readObsProject( String pathname ) throws Exception {
        URL url = this.getClass().getResource( pathname );
        InputStream s = new BufferedInputStream( url.openStream() );
        InputStreamReader reader = new InputStreamReader( s );
        ObsProject ret = ObsProject.unmarshalObsProject( reader );
        return ret;
    }

    public void setUp() {
        provider = new MockUidProvider();
        generator = new StatusEntityGenerator( provider );
    }

    public void testGenerateTree0() throws Exception {
        
        ObsProjectEntityT pID = new ObsProjectEntityT();
        pID.setEntityId( provider.getNewId() );
        ObsProject p = readObsProject( "refdata/ObsProject0.xml" );
        
        List<StatusBaseT> tree = generator.generateTree( p );
        assertNotNull( tree );
        assertTrue( tree.size() > 0 );
        assertTrue( tree.get(0) instanceof ProjectStatus );
        assertTrue( tree.get(1) instanceof OUSStatus );     // ObsProgramStatus
        
        // 1 + 6
        assertTrue( tree.get(2) instanceof OUSStatus );
        assertTrue( tree.get(3) instanceof SBStatus );
        assertTrue( tree.get(4) instanceof SBStatus );
        assertTrue( tree.get(5) instanceof SBStatus );
        assertTrue( tree.get(6) instanceof SBStatus );
        assertTrue( tree.get(7) instanceof SBStatus );
        assertTrue( tree.get(8) instanceof SBStatus );
        
        // 1 + 6
        assertTrue( tree.get( 9) instanceof OUSStatus );
        assertTrue( tree.get(10) instanceof SBStatus );
        assertTrue( tree.get(11) instanceof SBStatus );
        assertTrue( tree.get(12) instanceof SBStatus );
        assertTrue( tree.get(13) instanceof SBStatus );
        assertTrue( tree.get(14) instanceof SBStatus );
        assertTrue( tree.get(15) instanceof SBStatus );
        
        // 1 + 4
        assertTrue( tree.get(16) instanceof OUSStatus );
        assertTrue( tree.get(17) instanceof SBStatus );
        assertTrue( tree.get(18) instanceof SBStatus );
        assertTrue( tree.get(19) instanceof SBStatus );
        assertTrue( tree.get(20) instanceof SBStatus );
    }
    
    public void testGenerateTree1() throws Exception {
        
        ObsProjectEntityT pID = new ObsProjectEntityT();
        pID.setEntityId( provider.getNewId() );
        ObsProject p = readObsProject( "refdata/ObsProject1.xml" );
        
        List<StatusBaseT> tree = generator.generateTree( p );
        assertNotNull( tree );
        assertTrue( tree.size() > 0 );
        assertTrue( tree.get(0) instanceof ProjectStatus );
        assertTrue( tree.get(1) instanceof OUSStatus );     // ObsProgramStatus

        // 1 + 1
        assertTrue( tree.get(2) instanceof OUSStatus );
        assertTrue( tree.get(3) instanceof SBStatus );
        
        // 1 + 1
        assertTrue( tree.get(4) instanceof OUSStatus );
        assertTrue( tree.get(5) instanceof SBStatus );
        
        // 1 + 1
        assertTrue( tree.get(6) instanceof OUSStatus );
        assertTrue( tree.get(7) instanceof SBStatus );
        
        // 1 + 1
        assertTrue( tree.get(8) instanceof OUSStatus );
        assertTrue( tree.get(9) instanceof SBStatus );
        
        // 1 + 1 + 1
        assertTrue( tree.get(10) instanceof OUSStatus );
        assertTrue( tree.get(11) instanceof OUSStatus );
        assertTrue( tree.get(12) instanceof SBStatus );
        
        // Check for an initialization bug
        ProjectStatus ps = (ProjectStatus) tree.get(0);
        assertNotNull( ps.getObsProgramStatusRef() );
        assertNotNull( ps.getObsProgramStatusRef().getEntityId() );
    }

    // Bad calls
    public void testGenerateTreeBad() {

        try {
            generator = new StatusEntityGenerator( null );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // expected
        }

        generator = new StatusEntityGenerator( provider );
        try {
            generator.generateTree( null );
            fail( "Expected IllegalArgumentException" );
        }
        catch( IllegalArgumentException e ) {
            // expected
        }
    }

    public void testBugs() throws Exception {
        
        ObsProjectEntityT pID = new ObsProjectEntityT();
        pID.setEntityId( provider.getNewId() );
        ObsProject p = readObsProject( "refdata/ObsProject1.xml" );
        
        List<StatusBaseT> tree = generator.generateTree( p );
        
        // Check for an initialization bug
        ProjectStatus ps = (ProjectStatus) tree.get(0);
        assertNotNull( ps.getObsProgramStatusRef() );
        assertNotNull( ps.getObsProgramStatusRef().getEntityId() );
        
        // Check for another initialization bug
        for( StatusBaseT entity : tree ) {
            if( ! (entity instanceof OUSStatus) ) {
                continue;
            }
            OUSStatus ouss = (OUSStatus) entity;
            OUSStatusChoice choice = ouss.getOUSStatusChoice();
            assertNotNull( choice );
            assertFalse( choice.getOUSStatusRefCount() == 0 && 
                         choice.getSBStatusRefCount() == 0 );
        }
    }
}
