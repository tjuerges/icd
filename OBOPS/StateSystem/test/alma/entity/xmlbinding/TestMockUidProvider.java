/**
 * TestMockUidProvider.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import junit.framework.TestCase;

/**
 * @author amchavan, Aug 17, 2009
 * @version $Revision$
 */

// $Id$

public class TestMockUidProvider extends TestCase {
    MockUidProvider provider;
    
    public void setUp() {
        provider = new MockUidProvider();
    }

    public void testGetNewId() {
        String uid = provider.getNewId();
        assertNotNull( uid );
        assertFalse( uid.length() == 0 );
        assertFalse( uid.contains( " " ));
    }

    // verify whether it really loops over
    public void testGetManyNewIds() {
        String uid0 = provider.getNewId();
        for( int i = 1; i < 0xFFF; i++ ) {
            provider.getNewId();
        }
        String uid1 = provider.getNewId();
        assertEquals( uid0, uid1 );
    }
}
