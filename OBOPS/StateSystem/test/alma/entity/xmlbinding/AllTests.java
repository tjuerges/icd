/**
 * AllTests.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * TODO
 *
 * @author amchavan, Jul 1, 2009
 * @version $Revision$
 */

// $Id$
public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite( "Test for alma.entity.xmlbinding" );
        //$JUnit-BEGIN$
        suite.addTestSuite( TestUtils.class );
        suite.addTestSuite( TestStatusEntityGenerator.class );
        suite.addTestSuite( TestMockUidProvider.class );
        //$JUnit-END$
        return suite;
    }

}
