
-- PROJECTS
-- get project statuses not in archive
select OBS_PROJECT_ID from OBS_PROJECT_STATUS t1 where t1.OBS_PROJECT_ID not in 
(select ARCHIVE_UID from XML_OBSPROJECT_ENTITIES);

-- get archive projects not in StateArchive
select ARCHIVE_UID from XML_OBSPROJECT_ENTITIES t1 where t1.ARCHIVE_UID not in 
(select OBS_PROJECT_ID from OBS_PROJECT_STATUS);

-- OUSs
-- get ous statuses not in archive
select OBS_PROJECT_ID, DOMAIN_ENTITY_ID from OBS_UNIT_SET_STATUS t1 where t1.OBS_PROJECT_ID not in 
(select ARCHIVE_UID from XML_OBSPROJECT_ENTITIES);

-- get archive project's OUSs not in StateArchive
select ARCHIVE_UID from XML_OBSPROJECT_ENTITIES t1 where t1.ARCHIVE_UID not in 
(select OBS_PROJECT_ID from OBS_UNIT_SET_STATUS);

-- SBs
-- get SB statuses not in archive
select STATUS_ENTITY_ID, DOMAIN_ENTITY_ID, OBS_PROJECT_ID from SCHED_BLOCK_STATUS t1 where t1.DOMAIN_ENTITY_ID not in 
(select ARCHIVE_UID from XML_SCHEDBLOCK_ENTITIES);

-- get archive schedBlocks not in StateArchive
select ARCHIVE_UID from XML_SCHEDBLOCK_ENTITIES t1 where t1.ARCHIVE_UID not in 
(select DOMAIN_ENTITY_ID from SCHED_BLOCK_STATUS);