/*
 * Alter script for the State Archive -- Oracle SQL version
 * rkruowsk, 21-Jan-2010, creation
 *
 * $Id$ 
 */

-- Alter StateArchive Clobs to XMLTYPE

-- OBS_PROJECT_STATUS
-- add new temporary XMLTYPE column
alter table obs_project_status
add(
	temp xmltype
)xmltype temp store as clob;

-- copy existing clob columns data into temp xmltype
update obs_project_status t1
	set t1.temp = (select xmltype(t2.xml) from obs_project_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop old clob xml column		
alter table obs_project_status
drop(
	xml
);

-- re-create xml colum but as xmltype
alter table obs_project_status
add(
	xml xmltype
) xmltype xml store as clob;

-- copy temp data into new xml column
update obs_project_status t1
	set t1.xml = (select t2.temp from obs_project_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop temp column 
alter table obs_project_status
drop(
	temp
);


-- OBS_UNIT_SET_STATUS
-- add new temporary xmltype column
alter table obs_unit_set_status
add(
	temp xmltype
)xmltype temp store as clob;

-- copy existing clob columns data into temp xmltype
update obs_unit_set_status t1
	set t1.temp = (select xmltype(t2.xml) from obs_unit_set_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop old clob xml column		
alter table obs_unit_set_status
drop(
	xml
);

-- re-create xml colum but as xmltype
alter table obs_unit_set_status
add(
	xml xmltype
) xmltype xml store as clob;

-- copy temp data into new xml column
update obs_unit_set_status t1
	set t1.xml = (select t2.temp from obs_unit_set_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop temp column 
alter table obs_unit_set_status
drop(
	temp
);

-- SCHED_BLOCK_STATUS
-- add new temporary xmltype column
alter table sched_block_status
add(
	temp xmltype
) xmltype temp store as clob;

-- copy existing clob columns data into temp xmltype
update sched_block_status t1
	set t1.temp = (select xmltype(t2.xml) from sched_block_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop old clob xml column		
alter table sched_block_status
drop(
	xml
);

-- re-create xml colum but as xmltype
alter table sched_block_status
add(
	xml xmltype
) xmltype xml store as clob;

-- copy temp data into new xml column
update sched_block_status t1
	set t1.xml = (select t2.temp from sched_block_status t2 
		where t1.status_entity_id = t2.status_entity_id);

-- drop temp column 
alter table sched_block_status
drop(
	temp
);