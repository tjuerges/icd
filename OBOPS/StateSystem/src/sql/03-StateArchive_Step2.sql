set serveroutput on
spool StateArchive2.log

-- Alter StateArchive Clobs to XMLTYPE

declare
	stmt 	varchar2(256);
	coltype	varchar2(10);
	xmlmalformed EXCEPTION;
	PRAGMA EXCEPTION_INIT(xmlmalformed, -31011);
begin
	select DATA_TYPE into coltype from user_tab_columns where table_name='OBS_PROJECT_STATUS' and column_name='XML';
	if coltype = 'CLOB'
	then
		dbms_output.put_line('--->>> Changing OBS_PROJECT_STATUS.XML column type from CLOB to XMLType <<<---');
		stmt := 'alter table obs_project_status add(temp xmltype) xmltype temp store as clob';
		execute immediate stmt;
		stmt := 'update obs_project_status t1 set t1.temp = (select xmltype(t2.xml) from obs_project_status t2 where t1.status_entity_id = t2.status_entity_id)';
		execute immediate stmt;
		stmt := 'alter table obs_project_status drop(xml)';
		execute immediate stmt;
		stmt := 'alter table obs_project_status rename column temp to xml';
		execute immediate stmt;
	end if;
exception
	when xmlmalformed then
		null;
end;
/


declare
        stmt    varchar2(256);
        coltype varchar2(10);
	xmlmalformed EXCEPTION;
	PRAGMA EXCEPTION_INIT(xmlmalformed, -31011);
begin
        select DATA_TYPE into coltype from user_tab_columns where table_name='OBS_UNIT_SET_STATUS' and column_name='XML';
        if coltype = 'CLOB'
        then
                dbms_output.put_line('--->>> Changing OBS_UNIT_SET_STATUS.XML column type from CLOB to XMLType <<<---');
                stmt := 'alter table obs_unit_set_status add(temp xmltype)xmltype temp store as clob';
                execute immediate stmt;
                stmt := 'update obs_unit_set_status t1 set t1.temp = (select xmltype(t2.xml) from obs_unit_set_status t2 where t1.status_entity_id = t2.status_entity_id)';
                execute immediate stmt;
                stmt := 'alter table obs_unit_set_status drop(xml)';
                execute immediate stmt;
                stmt := 'alter table obs_unit_set_status rename column temp to xml';
                execute immediate stmt;
        end if;
exception
	when xmlmalformed then
		null;
end;
/


declare
        stmt    varchar2(256);
        coltype varchar2(10);
begin
        select DATA_TYPE into coltype from user_tab_columns where table_name='SCHED_BLOCK_STATUS' and column_name='XML';
        if coltype = 'CLOB'
        then
                dbms_output.put_line('--->>> Changing SCHED_BLOCK_STATUS.XML column type from CLOB to XMLType <<<---');
                stmt := 'alter table sched_block_status add(temp xmltype) xmltype temp store as clob';
                execute immediate stmt;
                stmt := 'update sched_block_status t1 set t1.temp = (select xmltype(t2.xml) from sched_block_status t2 where t1.status_entity_id = t2.status_entity_id)';
                execute immediate stmt;
                stmt := 'alter table sched_block_status drop(xml)';
                execute immediate stmt;
                stmt := 'alter table sched_block_status rename column temp to xml';
                execute immediate stmt;
        end if;
end;
/

spool off
quit
