set serveroutput on
spool StateArchive1.log

declare
	cnt number(1);
	stmt varchar2(100);
begin
	select count(COLUMN_NAME) into cnt from user_tab_columns where TABLE_NAME='STATE_CHANGES' and COLUMN_NAME='DOMAIN_PART_ID';
	if cnt = 0
	then
		dbms_output.put_line('--->>> Adding column DOMAIN_PART_ID to table STATE_CHANGES <<<---');
		stmt := 'alter table state_changes add domain_part_id varchar2(64) null';
		-- dbms_output.put_line(stmt);
		execute immediate stmt;
	else
		dbms_output.put_line('Column DOMAIN_PART_ID of table STATE_CHANGES already exists.');
	end if;
end;
/

spool off
quit
