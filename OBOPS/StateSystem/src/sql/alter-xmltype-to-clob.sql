-- Alter StateArchive XMLTYPE to Clob

-- obs_project_status

alter table obs_project_status
add(
	temp clob
);

update obs_project_status t1
	set t1.temp = (select t2.xml.getClobVal() from obs_project_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table obs_project_status
drop(
	xml
);

alter table obs_project_status
add(
	xml clob
);

update obs_project_status t1
	set t1.xml = (select t2.temp from obs_project_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table obs_project_status
drop(
	temp
);


-- obs_unit_set_status

alter table obs_unit_set_status
add(
	temp clob
);

update obs_unit_set_status t1
	set t1.temp = (select t2.xml.getClobVal() from obs_unit_set_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table obs_unit_set_status
drop(
	xml
);

alter table obs_unit_set_status
add(
	xml clob
);

update obs_unit_set_status t1
	set t1.xml = (select t2.temp from obs_unit_set_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table obs_unit_set_status
drop(
	temp
);

-- sched_block_status

alter table sched_block_status
add(
	temp clob
);

update sched_block_status t1
	set t1.temp = (select t2.xml.getClobVal() from sched_block_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table sched_block_status
drop(
	xml
);

alter table sched_block_status
add(
	xml clob
);

update sched_block_status t1
	set t1.xml = (select t2.temp from sched_block_status t2 
		where t1.status_entity_id = t2.status_entity_id);

alter table sched_block_status
drop(
	temp
);