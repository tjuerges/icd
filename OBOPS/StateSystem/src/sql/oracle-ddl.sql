/*
 * DDL for the State Archive -- Oracle SQL version
 * rkruowsk, 20-Aug-2009, creation
 *
 * $Id$ 
 */
 
drop table obs_project_status  cascade constraints purge;
drop table obs_unit_set_status cascade constraints purge;
drop table sched_block_status cascade constraints purge;
drop table state_changes cascade constraints purge;

-- Schema registration
/*
 
A directory containing all the required xsd files must exist on the Oracle server.
	e.g. /diska/alma/ACS-8.1/ACSSW/idl  

The oracle user the performs the following commands must have permissions to access this dir.
 
The physical dir above must be mapped to an Oracle logical dir using this command: 

	create directory idl_dir as '/diska/alma/ACS-8.1/ACSSW/idl '; 

(This logical dir is used by bfilename in dbms_xmlschema.registerschema.)

Now the schemas can be imported. 

*/

-- import schemas
/*
begin
dbms_xmlschema.registerschema(
	schemaurl => 'CommonEntity.xsd',
	schemadoc => bfilename ('IDL_DIR', 'CommonEntity.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ValueTypes.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ValueTypes.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ObsReview.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ObsReview.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ObsAttachment.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ObsAttachment.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ObsProposal.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ObsProposal.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ObsProject.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ObsProject.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'SchedBlock.xsd',
	schemadoc => bfilename ('IDL_DIR', 'SchedBlock.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'ProjectStatus.xsd',
	schemadoc => bfilename ('IDL_DIR', 'ProjectStatus.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'OUSStatus.xsd',
	schemadoc => bfilename ('IDL_DIR', 'OUSStatus.xsd'),
	force => true
);
end;	
/

begin
dbms_xmlschema.registerschema(
	schemaurl => 'SBStatus.xsd',
	schemadoc => bfilename ('IDL_DIR', 'SBStatus.xsd'),
	force => true
);
end;	
/

*/

create table obs_project_status (
    status_entity_id varchar2(64)              not null,
    domain_entity_id varchar2(64)              not null, -- contains the obsProject archiveUID (not required, may be removed in future)
    domain_entity_state varchar2(32)           not null,
    obs_project_status_id varchar2(64)         not null,
    obs_program_status_id varchar2(64)         not null,
    obs_project_id varchar2(64)                not null, -- contains the obsProject archiveUID
    project_was_timed_out timestamp                null,
    xml xmltype                                not null,
    constraint obs_project_status_pk primary key(status_entity_id)
)xmltype xml store as clob;

--xmltype xml store as clob xmlschema "ProjectStatus.xsd" element "ProjectStatus";

create table obs_unit_set_status (
    status_entity_id varchar2(64)              not null,
    domain_entity_id varchar2(64)                  null,  -- contains the ous partId
    domain_entity_state varchar2(32)           not null,
    parent_obs_unit_set_status_id varchar2(64)     null,  -- may be null for root
    obs_project_status_id varchar2(64)         not null,
    obs_project_id varchar2(64)                not null,  -- contains the obsProject archiveUID
    total_required_time_in_sec integer         not null,
    total_used_time_in_sec integer             not null,     
    xml xmltype                                not null,
    constraint obs_unit_set_status_pk primary key(status_entity_id)
)xmltype xml store as clob;

-- xmltype xml store as clob xmlschema "OUSStatus.xsd" element "OUSStatus";

create table sched_block_status (
    status_entity_id varchar2(64)              not null,
    domain_entity_id varchar2(64)              not null, -- contains the schedBlock archiveUID
    domain_entity_state varchar2(32)           not null,
    parent_obs_unit_set_status_id varchar2(64) not null,
    obs_project_status_id varchar2(64)         not null,
    obs_project_id varchar2(64)                not null, -- contains the obsProject archiveUID
    total_required_time_in_sec integer         not null,
    total_used_time_in_sec integer             not null,     
    xml xmltype                                   not null,
    constraint sched_block_status_pk primary key(status_entity_id)
)xmltype xml store as clob;

--xmltype xml store as clob xmlschema "SBStatus.xsd" element "SBStatus";

create table state_changes (
    state_changes_id number(32),
    status_entity_id varchar2(64)              not null,
    domain_entity_id varchar2(64)              not null,
    domain_part_id varchar2(64)                null,
    domain_entity_state varchar2(32)           not null,
    timestamp timestamp                        not null,
    location varchar2(3)                       not null,
    user_id varchar2(64)                       not null,
    subsystem varchar2(32)                     not null,
    info varchar2(54)                          null,
    entity_type varchar2(3)                    not null,
 constraint state_changes_pk primary key(state_changes_id)
);

alter table state_changes
add(
    domain_part_id varchar2(64) null
);

-- Foreign key constraints
-- obs_project_status joins to xml_obsproject_entities on obs_project_id
alter table obs_project_status
add constraint fk_obsproject_entity
  foreign key (obs_project_id)
  references xml_obsproject_entities(archive_uid);
  
-- obs_unit_set_status joins to xml_obsproject_entities on obs_project_id
-- unfortunately there is no way I know of to enforce FK on the partId (RK)
alter table obs_unit_set_status
add constraint fk_ous_obsproject_entity
  foreign key (obs_project_id)
  references xml_obsproject_entities(archive_uid);

-- sched_block_status joins to xml_obsproject_entities on obs_project_id
-- and to xml_schedblock_entities on domain_entity_id 
alter table sched_block_status
add constraint fk_sb_obsproject_entity
  foreign key (obs_project_id)
  references xml_obsproject_entities(archive_uid);  

alter table sched_block_status
add constraint fk_sb_schedblock_entity
  foreign key (domain_entity_id)
  references xml_schedblock_entities(archive_uid);  

-- Unique constraints
alter table obs_project_status
add constraint obs_project_status_unique 
	unique (obs_project_id);

alter table obs_unit_set_status
add constraint obs_unit_set_status_unique 
	unique (obs_project_id, domain_entity_id);

alter table sched_block_status
add constraint sched_block_status_unique 
	unique (obs_project_id, domain_entity_id);

drop sequence hibernate_sequence;
create sequence hibernate_sequence;

-- TODO: add indexes to state_changes status_entity_id & entity_type

commit;

-- Clean up script for removing orphaned status entries 
-- run this if foreign keys couldn't be created due to: 
--		ORA-02298: cannot validate ...- parent keys not found
/* 		
		
delete from obs_unit_set_status ous 
	where ous.obs_project_id not in 
		(select archive_uid from xml_obsproject_entities);
		
delete from obs_project_status ops 
	where ops.obs_project_id not in 
		(select archive_uid from xml_obsproject_entities);
		
delete from sched_block_status sbs 
	where sbs.obs_project_id not in 
		(select archive_uid from xml_obsproject_entities);
		
delete from sched_block_status sbs 
	where sbs.domain_entity_id not in 
		(select archive_uid from xml_schedblock_entities);
		
*/	