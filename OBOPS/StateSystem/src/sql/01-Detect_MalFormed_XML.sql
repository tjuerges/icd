set serveroutput on
set verify off

accept table_name prompt 'Type Table Name: '

declare
        xmlval xmltype;
	condition varchar2(2048) := '';
        xmlmalformed EXCEPTION;
        PRAGMA EXCEPTION_INIT(xmlmalformed, -31011);
begin
	for rec_cur in (select status_entity_id, xml from &table_name)
	loop
		begin
		-- dbms_output.put_line(rec_cur.status_entity_id);
		select xmltype(rec_cur.xml) into xmlval from dual;
		exception when xmlmalformed then
                	dbms_output.put_line('Status entity ID ' || chr(9) || rec_cur.status_entity_id || chr(9) || ' : Malformed XML in CLOB');
			if condition is null
			then
				condition := '''' || rec_cur.status_entity_id || '''';
			else
				condition := '''' || rec_cur.status_entity_id || '''' || ',' || condition;
			end if;
		end;
	end loop;
	dbms_output.put_line('Database Cleanup command:');
	dbms_output.put_line('delete from &table_name where status_entity_id in ('|| condition || ');');

end;
/
