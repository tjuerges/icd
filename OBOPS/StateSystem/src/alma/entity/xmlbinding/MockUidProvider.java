/**
 * TestUidProvided.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import java.util.Calendar;
import java.util.Random;

/**
 * A provider of mocked entity IDs: uses no Archive services, IDs are generated
 * with random numbers.
 * <p/>
 * 
 * An ID like <em>uid://X&lt;part1&gt;/X&lt;part2&gt;/X&lt;part3&gt;</em> (e.g.
 * uid://X58/X27f/Xff0) will be thus generated:
 * <ol>
 * <li><em>part1</em> is a randomly generated number between 0-0xFFF</li>
 * <li><em>part2</em> is also a random number in the same range</li>
 * <li><em>part2</em> is a non-negative number, which gets increased with every
 * call to the generator; starts at 0</li>
 * </ol>
 * 
 * A maximum of 0x1000 (1024) unique IDs can be generated with an instance of
 * this generator.
 * 
 * @author amchavan, Aug 17, 2009
 * @version $Revision$
 */

// $Id$

public class MockUidProvider implements UidProvider {
    
    private int part1;         
    private int part2;         
    private int part3;
    
    private static final String FORMAT = "uid://X%x/X%x/X%x";
    
    public MockUidProvider() {
        Random random = new Random(Calendar.getInstance().getTime().getTime());
        part1 = Math.abs( random.nextInt() ) % 0xFFF;
        part2 = Math.abs( random.nextInt() ) % 0xFFF;
        part3 = 0;
    }

    /**
     * @see alma.entity.xmlbinding.UidProvider#getNewId()
     */
    @Override
    public String getNewId() {
        return String.format( FORMAT, part1, part2, (part3++) % 0xFFF );
    }
}
