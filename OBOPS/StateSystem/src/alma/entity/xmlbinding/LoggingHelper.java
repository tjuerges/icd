/**
 * Copyright European Southern Observatory 2009
 */
package alma.entity.xmlbinding;

import java.util.HashMap;
import java.util.Map;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;

/**
 *
 * Helper class for logging
 *
 * @author rkurowsk, Aug 21, 2009
 * @version $Revision$
 */

// $Id$
public class LoggingHelper {

    /**
     * Returns a formatted String showing the whole project status tree.
     * For each entity type the id and status are shown.
     * 
     * @param projectTreeList
     * @return A formatted string showing the states of the whole project status tree 
     */
    public static String getProjectStatusTreeForDisplay(StatusBaseT[] projectTreeList){
    
    	String display = "";
    	
	    if(projectTreeList != null && projectTreeList.length > 0){
	    	// extract list into maps keyed by entityId
	        Map<String,OUSStatus> ousStatuses = new HashMap<String,OUSStatus>();
	        Map<String,SBStatus> sbStatuses = new HashMap<String,SBStatus>();
	        ProjectStatus projectStatus = null;
	        
	        for( StatusBaseT statusEntity : projectTreeList ) {
	            if( statusEntity instanceof ProjectStatus ) {
	            	projectStatus = (ProjectStatus)statusEntity;
	            }else if( statusEntity instanceof OUSStatus ) {
	            	OUSStatus ousStatus = (OUSStatus)statusEntity;
	            	ousStatuses.put(ousStatus.getOUSStatusEntity().getEntityId(), ousStatus);
	            }else if( statusEntity instanceof SBStatus ) {
	            	SBStatus sbStatus = (SBStatus)statusEntity;
	            	sbStatuses.put(sbStatus.getSBStatusEntity().getEntityId(), sbStatus);
	            }else {
	                String msg = "Internal error, don't know how to deal " +
	                    "with instances of: " + statusEntity.getClass().getCanonicalName();
	                throw new RuntimeException( msg );
	            }
	        }
	        
	    	display = getProjectStatusTreeForDisplay(projectStatus, ousStatuses, sbStatuses);
	    }else{
	    	display = "projectTreeList is empty";
	    }
	    
	    return display;
    }
    
    /**
     * Returns a formatted String showing the whole project status tree.
     * For each entity type the id and status are shown.
     * 
     * @param projectStatus
     * @param obsUnitSetStatuses
     * @param schedBlockStatuses
     * @return A formatted string showing the states of the whole project status tree 
     */
    public static String getProjectStatusTreeForDisplay(ProjectStatus projectStatus, 
    		Map<String,OUSStatus> obsUnitSetStatuses,
    		Map<String,SBStatus> schedBlockStatuses){

    	StringBuilder sb = new StringBuilder();
    	sb.append("ProjectStatus(");
    	sb.append(projectStatus.getProjectStatusEntity().getEntityId());
    	sb.append(") state: ");
    	sb.append(projectStatus.getStatus().getState().toString());
    	sb.append(" ( ");
    	String obsProgramId = projectStatus.getObsProgramStatusRef().getEntityId();
    	OUSStatus obsProgram = obsUnitSetStatuses.get(obsProgramId);
    	sb.append(getObsUnitSetChildrenForDisplay(obsProgram, 
    			obsUnitSetStatuses, schedBlockStatuses, 1));
    	sb.append("\n)");
    	
    	return sb.toString();

    }
    
    private static String getObsUnitSetChildrenForDisplay(OUSStatus parentOUSStatus, 
    		Map<String,OUSStatus> obsUnitSetStatuses,
    		Map<String,SBStatus> schedBlockStatuses,
    		int nrTabs){
    	
    	String tab = "";
    	for(int idx = 0; idx < nrTabs; idx++){
    		tab = tab + "   ";
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	sb.append("\n");
    	sb.append(tab);
    	sb.append("ous(");
    	sb.append(parentOUSStatus.getOUSStatusEntity().getEntityId());
    	sb.append(") state: ");
    	sb.append(parentOUSStatus.getStatus().getState().toString());
    	sb.append(" (");
		// check if the given OUSStatus has children
		if(parentOUSStatus.getOUSStatusChoice() != null){

			// check if they are OUSStatus children
			if(parentOUSStatus.getOUSStatusChoice().getOUSStatusRefCount() > 0){
				OUSStatusRefT[] childOUSStatusRefs = 
					parentOUSStatus.getOUSStatusChoice().getOUSStatusRef();

				for(OUSStatusRefT ousStatusRef: childOUSStatusRefs){
					OUSStatus childOUSStatus = obsUnitSetStatuses.get(ousStatusRef.getEntityId());
					sb.append(getObsUnitSetChildrenForDisplay(childOUSStatus, 
							obsUnitSetStatuses, schedBlockStatuses, nrTabs + 1));
				}
			// else check if they are SchedBlockStatus children
			}else if(parentOUSStatus.getOUSStatusChoice().getSBStatusRefCount() > 0){
				SBStatusRefT[] childSchedBlockStatusRefs =
					parentOUSStatus.getOUSStatusChoice().getSBStatusRef();
				for(SBStatusRefT childSBStatusRef: childSchedBlockStatusRefs){
					SBStatus sbStatus = schedBlockStatuses.get(childSBStatusRef.getEntityId());
					sb.append("\n");
					sb.append(tab);
					sb.append("   SB(");
					sb.append(childSBStatusRef.getEntityId());
					sb.append(") state: ");
					sb.append(sbStatus.getStatus().getState().toString());
				}
			}
		}else{
			sb.append("ObsUnitSet with entityId: ");
			sb.append( parentOUSStatus.getOUSStatusEntity().getEntityId());
			sb.append( " has no children");
		}
		sb.append("\n");
		sb.append(tab);
		sb.append(")");
		
		return sb.toString();
		
    }
}
