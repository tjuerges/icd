/**
 * UidProvider.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

/**
 * Provides fresh, unique entity IDs.
 *
 * @author amchavan, Aug 17, 2009
 * @version $Revision$
 */

// $Id$

public interface UidProvider {

    /**
     * Generate a new unique entity ID string, compatible with any and all 
     * Archive conventions.
     *  
     * @return A unique entity ID string
     */
    public String getNewId();
}
