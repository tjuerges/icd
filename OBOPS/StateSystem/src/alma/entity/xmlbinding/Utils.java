/**
 * Utils.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.obsproject.ObsProjectRefT;
import alma.entity.xmlbinding.obsproposal.ObsProposalRefT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusChoice;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusRefT;
import alma.entity.xmlbinding.sbstatus.ExecStatusT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SchedBlockEntityT;
import alma.entity.xmlbinding.schedblock.SchedBlockRefT;
import alma.entity.xmlbinding.valuetypes.ExecBlockRefT;
import alma.entity.xmlbinding.valuetypes.StatusT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.obops.utils.DatetimeUtils;

/**
 * A collection of utility methods to help with the mocking, including
 * factory methods for status entities.
 *
 * @author amchavan, Jun 10, 2009
 * @version $Revision: 1.6 $
 */

// $Id: Utils.java,v 1.6 2011/02/03 10:28:55 rkurowsk Exp $

public class Utils {

    /** Keeps track of the OUSStatus entities we created */
    private static Map<String,OUSStatus> obsUnitSetStatuses =
        new LinkedHashMap<String,OUSStatus>();
    
    /**
     * Keeps track of the OUSStatus entities we created, in the order they
     * should be inserted in the database
     */
    private static OUSStatus[] oussSequence;

    /** Keeps track of the SBStatus entities we created */
    private static Map<String,SBStatus> schedBlockStatuses =
        new HashMap<String,SBStatus>();
    
    /** Keeps track of the ProjectStatus entities we created */
    private static Map<String,ProjectStatus> obsProjectStatuses =
        new HashMap<String,ProjectStatus>();

    /** Random number generator, for making unique IDs */
    private static Random random = null;
    
    /**
     * <strong>Note</strong>: this method assumes that the input ProjectStatus
     * was created using {@linkplain #makeProjectStatus()}.
     * 
     * @return An arbitrary OUSStatus among those associated to the input
     *         ProjectStatus; state is <em>Phase2Submitted<em/>
     */
    public static OUSStatus findOUSStatus( ProjectStatus ps ) {
        
        // Find the ObsProgram
        OUSStatusRefT ref = ps.getObsProgramStatusRef();
        String id = ref.getEntityId();
        OUSStatus program = Utils.findOUSStatus( id );
        
        // Find the root OUS
        ref = program.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus root = Utils.findOUSStatus( id );
        
        // Find the first child of the root OUS
        ref = root.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus ret = Utils.findOUSStatus( id );
        
        return ret;
    }

    /**
     * @return The OUSStatus entity with the given ID 
     */
    public static OUSStatus findOUSStatus( String id ) {
        return obsUnitSetStatuses.get( id );
    }

    /**
     * @return The ProjectStatus entity with the given ID 
     */
    public static ProjectStatus findProjectStatus( String id ) {
        return obsProjectStatuses.get( id );
    }

    public static ProjectStatus findProjectStatus(ObsProjectEntityT obsProjectId){

    	ProjectStatus foundPs = null;
    	for(ProjectStatus ps: obsProjectStatuses.values()){
    		ps.getObsProjectRef().getEntityId().equals(obsProjectId.getEntityId());
    		foundPs = ps;
    		break;
    	}
    	
    	return foundPs;
    }
    
    
    /**
     * <strong>Note</strong>: this method assumes that the input ProjectStatus
     * was created using {@linkplain #makeProjectStatus()}.
     * 
     * @return An arbitrary SBStatus among those associated to the input
     *         ProjectStatus; state is <em>Phase2Submitted<em/>
     */
    public static SBStatus findSBStatus( ProjectStatus ps ) {
        
        // Find the ObsProgram
        OUSStatusRefT ref = ps.getObsProgramStatusRef();
        String id = ref.getEntityId();
        OUSStatus program = Utils.findOUSStatus( id );
        
        // Find the root OUS
        ref = program.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus root = Utils.findOUSStatus( id );
        
        // Find the first child of the root OUS
        ref = root.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus child0 = Utils.findOUSStatus( id );
        
        // Find the first SBStatus of that child
        SBStatusRefT sbRef = 
            child0.getOUSStatusChoice().getSBStatusRef(0);
        id = sbRef.getEntityId();
        SBStatus ret = Utils.findSBStatus( id );
        ret.getStatus().setState( StatusTStateType.PHASE2SUBMITTED );
        
        return ret;
    }
    
    /**
     * @return The SBStatus entity with the given ID 
     */
    public static SBStatus findSBStatus( String id ) {
        return schedBlockStatuses.get( id );
    }
    

    /**
     * @return The type of the input XML document: ProjectStatus, SBStatus, etc.
     * @param reader
     *            A reader open on an XML document
     * @throws DocumentException
     */
    public static String getDocumentName( Reader reader ) throws DocumentException {
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read( reader );
        Element root = document.getRootElement();
        String name = root.getName();
        return name;
    }
    
    /**
     * Parse the input file and find out what type of status entity it
     * represents.
     * 
     * @param pathname
     *            Pathname of the input file
     * 
     * @return The status entity type.
     */
    public static String getEntityType( String pathname ) throws Exception {
    
        File f = new File( pathname );
        if( !(f.exists() && f.canRead()) ) {
            String msg = "File " + pathname + " not found or not readable"
                    + "\npwd=" + (new File( "." )).getAbsolutePath();
            throw new RuntimeException( msg );
        }
    
        FileReader reader = new FileReader( f );
        String name = getDocumentName( reader );
        reader.close();
        return name;
    }

    /**
     * @return collection of ObsUnitSets
     */
    public static Collection<OUSStatus> getOUSStatuses(){
    	return obsUnitSetStatuses.values();
    }

    /**
     * @return The OUSStatus entities we created, in the order they should be
     *         inserted in the database
     */
    public static OUSStatus[] getOusStatusSequence() {
        return oussSequence;
    }

    /**
     * @param projectStatus
     * @return A formatted string showing the states of the whole project status tree 
     */
    public static String getProjectStatusTreeForDisplay(ProjectStatus projectStatus){

    	return LoggingHelper.getProjectStatusTreeForDisplay(projectStatus, 
    			obsUnitSetStatuses, schedBlockStatuses);

    }
  
    /**
     * @return collection of sbstatuses
     */
    public static Collection<SBStatus> getSBStatuses(){
    	return schedBlockStatuses.values();
    }

    /**
     * @return A new and (hopefully) unique entity ID.
     */
    public static String makeNewEntityId() {
        if( random == null ) {
            // Initialize the random generator with the current time,
            // so that we get every time a different sequence of
            // generated random numbers -- we need that to allow unique 
            // persisted keys.
            random = new Random( Calendar.getInstance().getTime().getTime() );
        }
        return "uid://X1/X2/X" + Integer.toString( Math.abs( random.nextInt() ));
    }

    private static ObsProjectEntityT makeObsProjectEntityId() {
        String id = makeNewEntityId();
        ObsProjectEntityT ret = new ObsProjectEntityT();
        ret.setEntityIdEncrypted(id);
        ret.setEntityId( id );
        return ret;
    }
    
    private static OUSStatusEntityT makeObsUnitSetEntityId( ) {
        String id = makeNewEntityId();
        OUSStatusEntityT ret = new OUSStatusEntityT();
        ret.setEntityId( id );
        return ret;
    }

    /**
     * @return A new OUSStatus, status is <em>Phase2Submitted</em>
     */
    public static OUSStatus 
        makeOUSStatus( ObsProjectEntityT obsProjectEntity,
                       ProjectStatus projectStatus,
                       OUSStatusRefT parent,
                       StatusTStateType ousState ) {

        ProjectStatusRefT obsProjectStatusRef = makeRefT( projectStatus );

        OUSStatus ret = new OUSStatus();
        ret.setContainingObsUnitSetRef( parent );
        
        ObsProjectRefT obsUnitSetRef = new ObsProjectRefT();
        obsUnitSetRef.setEntityId("projectEntityId");
        obsUnitSetRef.setPartId("ousPartId_" + Math.random());
        ret.setObsUnitSetRef(obsUnitSetRef);
        
        ret.setProjectStatusRef( obsProjectStatusRef );
        ret.setOUSStatusEntity( makeObsUnitSetEntityId() );
        ret.setStatus( new StatusT() );
        ret.getStatus().setState(ousState);
        ret.setOUSStatusChoice( new OUSStatusChoice() );
        ret.setTotalRequiredTimeInSec(1000);
        ret.setTotalUsedTimeInSec(500);
        obsUnitSetStatuses.put( ret.getOUSStatusEntity().getEntityId(),
                                ret );

        return ret;
    }

//    /**
//     * @return A new OUSStatus, status is
//     *         {@linkplain StatusTStateType#Phase2Submitted}
//     */
//    public static OUSStatus makeOUSStatus() {
//        OUSStatus ret = new OUSStatus();
//        ret.setOUSStatusEntity( makeObsUnitSetEntityId( ret ) );
//        ret.setState( StatusTStateType.Phase2Submitted );
//        obsUnitSetStatuses.put( ret.getOUSStatusEntity().getEntityId(), 
//                                ret );
//        return ret;
//    }

    /**
     * @return A new ProjectStatus, referencing a non-existing ObsProject; state
     *         is <em>Phase2Submitted</em>.<p/>
     *         
     *         The returned structure has two obsUnitSetStatus entities,
     *         each containing two SBStatus entities:
     *         <pre>
     *         ProjectStatus
     *             +-- ObsProgramStatus (OUSStatus)
     *                    +-- OUSStatus (the secondary root)
     *                            +-- OUSStatus
     *                                   +-- SBStatus 
     *                                   +-- SBStatus 
     *                            +-- OUSStatus
     *                                   +-- SBStatus 
     *                                   +-- SBStatus 
     *         </pre>
     *         
     *         
     */
    public static ProjectStatus makeProjectStatus() {
        return makeProjectStatus( 2, 2 );
    }

    /**
     * @see alma.entity.xmlbinding.Utils#makeProjectStatus(int, int, StatusTStateType, StatusTStateType, StatusTStateType)
     */
    public static ProjectStatus makeProjectStatus( int numOUSS, int numSBS ) {
        return makeProjectStatus( numOUSS, 
                                  numSBS, 
                                  StatusTStateType.PHASE2SUBMITTED, 
                                  StatusTStateType.PHASE2SUBMITTED, 
                                  StatusTStateType.PHASE2SUBMITTED );
    }

    /**
     * @see alma.entity.xmlbinding.Utils#makeProjectStatus(int, int, StatusTStateType, StatusTStateType, StatusTStateType)
     * 
     * @param numOUSS
     * @param numSBS
     * @param startState
     */
    public static ProjectStatus makeProjectStatus( int numOUSS, 
                                                   int numSBS, 
                                                   StatusTStateType startState ) {
        return makeProjectStatus(numOUSS, numSBS, startState, startState, startState);
    }

    /**
     * @return A new ProjectStatus, referencing a non-existing ObsProject with the states set to the given ones
     *         
     *         The returned structure has numOUSS obsUnitSetStatus entities,
     *         each containing numSBS SBStatus entities:
     *         <pre>
     *         ProjectStatus
     *             +-- ObsProgramStatus (OUSStatus)
     *                    +-- OUSStatus (the secondary root)
     *                            +-- OUSStatus
     *                                   +-- SBStatus
     *                                   +-- ... numSBS in total
     *                                   +-- SBStatus 
     *                            +-- OUSStatus
     *                            +-- ... numOUSS in total
     *                            +-- OUSStatus
     *         </pre>
     *         
     *         
     * @param numOUSS
     * @param numSBS
     * @param prjState
     * @param ousState
     * @param sbState
     */
    public static ProjectStatus makeProjectStatus( int numOUSS, 
                                                   int numSBS, 
                                                   StatusTStateType prjState, 
                                                   StatusTStateType ousState, 
                                                   StatusTStateType sbState ) {
        
    	// create ProjectStatus
    	ObsProjectEntityT obsProjectEntity = makeObsProjectEntityId();
    	ObsProjectRefT obsProjectRef = makeRefT( obsProjectEntity );
    	ProjectStatusEntityT projectStatusEntity = makeProjectStatusEntityId();
    	ObsProposalRefT obsProposalRefT = makeObsProposalRefT();
    	
        ProjectStatus ret = new ProjectStatus();
        ret.setName("Why a name?");
        ret.setPI("Why a PI");
        ret.setBreakpointTime("");
        ret.setTimeOfUpdate(DatetimeUtils.formatAsIso(new Date()));
        ret.setProjectWasTimedOut(DatetimeUtils.formatAsIso(new Date()));
        ret.setProjectStatusEntity( projectStatusEntity );
        ret.setStatus( new StatusT() );
        ret.getStatus().setState(prjState );
        ret.getStatus().setReadyTime(DatetimeUtils.formatAsIso(new Date()));
        ret.getStatus().setStartTime("");
        ret.getStatus().setEndTime("");
        ret.setObsProjectRef( obsProjectRef );
        
        ret.setObsProposalRef(obsProposalRefT);
        
        oussSequence = new OUSStatus[numOUSS+2];
        
        // create obsProgram (root OUSStatus)
        OUSStatus program = 
            makeOUSStatus( obsProjectEntity, ret, null, ousState );
        OUSStatusRefT programRef = makeRefT( program );
        ret.setObsProgramStatusRef( programRef );
        oussSequence[0] = program;
        
        // Create OUSStatus, child of program, secondary root
        OUSStatus root2 = 
            makeOUSStatus( obsProjectEntity, ret, programRef, ousState );
        OUSStatusRefT root2Ref = makeRefT( root2 );
        program.getOUSStatusChoice().addOUSStatusRef( root2Ref );
        oussSequence[1] = root2;

        // Create a number of OUSStatus, children of secondary root
        for( int i = 0; i < numOUSS; i++ ) {
            OUSStatus ouss = 
                makeOUSStatus( obsProjectEntity, ret, root2Ref, ousState );
            OUSStatusRefT oussRef = makeRefT( ouss );
            root2.getOUSStatusChoice().addOUSStatusRef( oussRef );
            oussSequence[i+2] = ouss;
            
            // Add a number of SBStatus entities to this OUSStatus
            for( int j = 0; j < numSBS; j++ ) {
                SBStatus sbs = makeSBStatus( ret, ouss, sbState);
                sbs.setContainingObsUnitSetRef( oussRef );
            }
        }
        
        obsProjectStatuses.put( projectStatusEntity.getEntityId(), ret );
        return ret;
    }
    
    private static ObsProposalRefT makeObsProposalRefT(){
    	
	    ObsProposalRefT opr = new ObsProposalRefT();
	    opr.setEntityId(makeNewEntityId());
	    opr.setDocumentVersion("");
	    return opr;
    }
    
    private static ProjectStatusEntityT makeProjectStatusEntityId() {
        String id = makeNewEntityId();
        ProjectStatusEntityT ret = new ProjectStatusEntityT();
        ret.setEntityId( id );
        ret.setEntityIdEncrypted(id);
        return ret;
    }
    
    private static ObsProjectRefT makeRefT( ObsProjectEntityT entity ) {
        ObsProjectRefT ret = new ObsProjectRefT();
        ret.setEntityId( entity.getEntityId() );
        ret.setDocumentVersion("");
        return ret;
    }
    
    private static OUSStatusRefT makeRefT( OUSStatus s ) {
        OUSStatusRefT ret = new OUSStatusRefT();
        ret.setEntityId( s.getOUSStatusEntity().getEntityId() );
        ret.setDocumentVersion("");
        return ret;
    }
    
    private static ProjectStatusRefT makeRefT( ProjectStatus s ) {
        ProjectStatusRefT ret = new ProjectStatusRefT();
        ret.setEntityId( s.getProjectStatusEntity().getEntityId() );
        return ret;
    }

    private static SBStatusRefT makeRefT( SBStatus s ) {
        SBStatusRefT ret = new SBStatusRefT();
        ret.setEntityId( s.getSBStatusEntity().getEntityId() );
        return ret;
    }
    
    private static SchedBlockRefT makeRefT( SchedBlockEntityT entity ) {
        SchedBlockRefT ret = new SchedBlockRefT();
        ret.setEntityId( entity.getEntityId() );
        return ret;
    }
    
    /**
     * @return A new SBStatus; state is <em>Phase2Submitted</em>. The
     *         returned entity will be associated to a new, fully-populated
     *         ProjectStatus
     *     
     * @see #makeProjectStatus()
     */
    public static SBStatus makeSBStatus() {
        // Create a full ProjectStatus
        ProjectStatus ps = Utils.makeProjectStatus();
        
        // Find the ObsProgram
        OUSStatusRefT ref = ps.getObsProgramStatusRef();
        String id = ref.getEntityId();
        OUSStatus program = Utils.findOUSStatus( id );
        
        // Find the root OUS
        ref = program.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus root = Utils.findOUSStatus( id );
        
        // Find the first child of the root OUS
        ref = root.getOUSStatusChoice().getOUSStatusRef(0);
        id = ref.getEntityId();
        OUSStatus child0 = Utils.findOUSStatus( id );
        
        // Find the first SBStatus of that child
        SBStatusRefT sbRef = child0.getOUSStatusChoice().getSBStatusRef(0);
        id = sbRef.getEntityId();
        SBStatus ret = Utils.findSBStatus( id );
        
        return ret;
    }
    
    /**
     * @param ouss 
     * @return A new SBStatus, associated to the input ProjectStatus; state is
     *         <em>Phase2Submitted<em/>
     */
    public static SBStatus makeSBStatus( ProjectStatus ps, 
                                         OUSStatus ouss,
                                         StatusTStateType sbState) {

        SchedBlock sb = new SchedBlock();
        sb.setSchedBlockEntity( makeSchedBlockEntityId() );
        SchedBlockRefT sbRef = makeRefT( sb.getSchedBlockEntity() );
        ProjectStatusRefT prjRef = makeRefT( ps );
        OUSStatusRefT containingOusRef = new OUSStatusRefT();
        containingOusRef.setEntityId(ouss.getOUSStatusEntity().getEntityId());
        SBStatus ret = new SBStatus();
        ret.setSBStatusEntity( makeSBStatusEntityId() );
        ret.setSchedBlockRef( sbRef );
        ret.setProjectStatusRef( prjRef );
        ret.setContainingObsUnitSetRef(containingOusRef);
        ret.setTotalRequiredTimeInSec(1000);
        ret.setTotalUsedTimeInSec(500);
        
//        ret.setObsProjectRef( ps.getObsProjectRef() );
        ret.setStatus( new StatusT() );
        ret.getStatus().setState( sbState );

        ouss.getOUSStatusChoice().addSBStatusRef( makeRefT( ret ));
        
        schedBlockStatuses.put( ret.getSBStatusEntity().getEntityId(), ret );
        return ret;
    }
    
    /**
     * Creates a fake execStatusT
     * 
     * @param id
     * @return
     */
    public static ExecStatusT makeExecStatus(int appendToId){
    
		StatusT execStatusState = new StatusT();
		execStatusState.setState(StatusTStateType.FULLYOBSERVED);
		execStatusState.setStartTime(DatetimeUtils.formatAsIso(new Date()));
		execStatusState.setEndTime(DatetimeUtils.formatAsIso(new Date()));
		
		ExecBlockRefT execBlockRef = new ExecBlockRefT();
		execBlockRef.setExecBlockId("uid://X02/X244/X" + appendToId);
		
		ExecStatusT execStatus = new ExecStatusT();
		execStatus.setStatus(execStatusState);
		execStatus.setTimeOfCreation(DatetimeUtils.formatAsIso(new Date()));
		execStatus.setArrayName("Array001");
		execStatus.setExecBlockRef(execBlockRef);
		
		return execStatus;
		
    }
    
    private static SBStatusEntityT makeSBStatusEntityId( ) {
        String sb = makeNewEntityId();
        SBStatusEntityT ret = new SBStatusEntityT();
        ret.setEntityId( sb );
        return ret;
    }

    private static SchedBlockEntityT makeSchedBlockEntityId() {
        String sb = makeNewEntityId();
        SchedBlockEntityT ret = new SchedBlockEntityT();
        ret.setEntityId( sb );
        return ret;
    }

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws ValidationException 
     * @throws MarshalException 
     */
    public static String marshall( OUSStatus entity ) 
        throws MarshalException, ValidationException {
        StringWriter w = new StringWriter();
        entity.marshal( w );
        return w.toString();
    }

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws ValidationException 
     * @throws MarshalException 
     */
    public static String marshall( ProjectStatus entity ) 
        throws MarshalException, ValidationException {
        StringWriter w = new StringWriter();
        entity.marshal( w );
        return w.toString();
    }

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws ValidationException 
     * @throws MarshalException 
     */
    public static String marshall( SBStatus entity ) 
        throws MarshalException, ValidationException {
        StringWriter w = new StringWriter();
        entity.marshal( w );
        return w.toString();
    }

    public static void reset(){

    	obsUnitSetStatuses = new LinkedHashMap<String,OUSStatus>();
    	schedBlockStatuses = new HashMap<String,SBStatus>();
    	obsProjectStatuses = new HashMap<String,ProjectStatus>();
    	
    }

    /**
     * Parse the input file and create a status entity.
     * 
     * @param pathname
     *            Pathname of the input file
     * 
     * @return The status entity that was created.
     */
    public static Object unmarshal( String pathname ) throws Exception {
    
        File f = new File( pathname );
        if( !(f.exists() && f.canRead()) ) {
            String msg = "File " + pathname + " not found or not readable"
                    + "\npwd=" + (new File( "." )).getAbsolutePath();
            throw new RuntimeException( msg );
        }
    
        Reader reader = new FileReader( f );
        String name = getDocumentName( reader );
        reader.close();
    
        Object ret;
        reader = new FileReader( f );
        if( name.equals( "ProjectStatus" ) ) {
            ret = ProjectStatus.unmarshalProjectStatus( reader );
        }
        else if( name.equals( "OUSStatus" ) ) {
            ret = OUSStatus.unmarshalOUSStatus( reader );
        }
        else if( name.equals( "SBStatus" ) ) {
            ret = SBStatus.unmarshalSBStatus( reader );
        }
        else if( name.equals( "ObsProject" ) ) {
            ret = ObsProject.unmarshalObsProject( reader );
        }
        else {
            String msg = "Unsupported entity type found in file " + pathname;
            throw new RuntimeException( msg );
        }
        reader.close();
        return ret;
    }

    /**
     * Walk down the input OUSStatus, collecting all child elements along
     * the way.
     * 
     * @param ouss
     *            The OUSStatus to visit; it is assumed that it was
     *            created with by of the methods of this class.
     * @param oussList
     *            This list will be populated with all OUSStatus entities
     *            directly or indirectly connected to the input ProjectStatus
     * @param sbsList
     *            This list will be populated with all SBStatus entities
     *            eventually connected to the input ProjectStatus
     */
    public static void walk( OUSStatus ouss, 
                             List<OUSStatus> oussList,
                             List<SBStatus> sbsList ) {

        OUSStatusChoice choice = ouss.getOUSStatusChoice();
        OUSStatusRefT[] ousStatuses = choice.getOUSStatusRef();
        
        if( ousStatuses.length > 0 ) {
            for( int i = 0; i < ousStatuses.length; i++ ) {
                OUSStatusRefT ref = ousStatuses[i];
                OUSStatus child = 
                    findOUSStatus( ref.getEntityId() );
                oussList.add( child );
                walk( child, oussList, sbsList );
            }
            return;
        }
        
        SBStatusRefT[] sbStatuses = choice.getSBStatusRef();
        for( int i = 0; i < sbStatuses.length; i++ ) {
            SBStatusRefT ref = sbStatuses[i];
            SBStatus child = findSBStatus( ref.getEntityId() );
            sbsList.add( child );
        }
    }

    /**
     * Walk down the input ProjectStatus, collecting all child elements along
     * the way.
     * 
     * @param ps
     *            The ProjectStatus to visit; it is assumed that it was created
     *            with {@linkplain #makeProjectStatus()} or
     *            {@linkplain #makeProjectStatus(int, int)}.
     * @param oussList
     *            This list will be populated with all OUSStatus entities
     *            directly or indirectly connected to the input ProjectStatus
     * @param sbsList
     *            This list will be populated with all SBStatus entities
     *            eventually connected to the input ProjectStatus
     */
    public static void walk( ProjectStatus ps, 
                             List<OUSStatus> oussList,
                             List<SBStatus> sbsList ) {

        OUSStatusRefT progRef = ps.getObsProgramStatusRef();
        OUSStatus prog = findOUSStatus( progRef.getEntityId() );
        oussList.add( prog );
        walk( prog, oussList, sbsList );
    }

    /**
     * Write the input entity to a file in the current working directory.
     * 
     * The file will be called
     * <em>&lt;entityType&gt-&lt;simplifiedEntityId&gt;.xml</em>, where
     * entityType is <em>ProjectStatus</em>, <em>OUSStatus</em>, etc.; and
     * simplifiedEntityId is obtained from entityID by stripping the leading
     * <em>uid://</em> sequence and replacing slash chars (' <code>/</code>')
     * with dashes.<br/>
     * For instance, if the entityID of the input ProjectStatus is
     * <em>uid://X58/X27f/Xff0</em> the resulting filename will be
     * <em>ProjectStatus-X58-X27f-Xff0.xml</em>
     * 
     * @param uid
     *            ID of the required ProjectStatus
     * 
     * @param xml
     *            The XML text of the entity
     * 
     * @throws IOException
     */
    public static void writeEntityToFile( String uid, String xml ) throws Exception {
    
        // Convert uid://X58/X27f/Xff0 to X58-X27f-Xff0
        String simpleEntityID = uid;
        if( simpleEntityID.startsWith( "uid://" ) ) {
            simpleEntityID = simpleEntityID.substring( 6 );
        }
        simpleEntityID = simpleEntityID.replace( "/", "-" );
    
        // Recover entity type
        String type = getDocumentName( new StringReader( xml ));
    
        // Build the final pathname
        String filename = type + "-" + simpleEntityID + ".xml";
    
        // Write the entity out to that file
        File f = new File( filename );
        FileWriter fw = new FileWriter( f );
        BufferedWriter bw = new BufferedWriter( fw );
        bw.append( xml );
        bw.close();
    }
}
