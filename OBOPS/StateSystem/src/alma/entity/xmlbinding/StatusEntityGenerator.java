/**
 * Generator.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.entity.xmlbinding;

import java.util.LinkedList;
import java.util.List;

import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.obsproject.ObsProjectRefT;
import alma.entity.xmlbinding.obsproject.ObsUnitSetT;
import alma.entity.xmlbinding.obsproject.ObsUnitSetTChoice;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusChoice;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusRefT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.entity.xmlbinding.schedblock.SchedBlockRefT;
import alma.entity.xmlbinding.valuetypes.StatusT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;

/**
 * Utility class to generate hierarchies of status entities
 * ({@linkplain StatusBaseT} entities).
 * 
 * @author amchavan, Aug 17, 2009
 * @version $Revision$
 */

// $Id$

public class StatusEntityGenerator {
    
    /** To give a valid ID to newly created entities */
    private UidProvider uidProvider;

    // syntactic sugar
    public static final StatusTStateType PHASE2SUB = 
        StatusTStateType.PHASE2SUBMITTED;
    
    /**
     * Constructor
     * @param uidProvider  To give a valid ID to newly created entities.
     */
    public StatusEntityGenerator( UidProvider uidProvider ) {
        if( uidProvider == null ) {
            throw new IllegalArgumentException( "Null arg 'uidProvider'" );
        }
        this.uidProvider = uidProvider;
    }

    /**
     * Build a tree of status entities.
     * 
     * @param root
     *            The status entities we create correspond to to the domain
     *            entities referenced by in the input ObsProject.
     * 
     * @return A list of status entities, ordered so that they can be written to
     *         the State Archive:
     *         <pre>
     *         ProjectStatus
     *             +-- ObsProgramStatus (OUSStatus)
     *                    +-- OUSStatus (the secondary root)
     *                            +-- OUSStatus
     *                                   +-- SBStatus 
     *                                   +-- SBStatus 
     *                            +-- OUSStatus
     *                                   +-- SBStatus 
     *                                   +-- SBStatus 
     *         </pre>
     *         The list may be empty but it's never <code>null</code>
     */
    public List<StatusBaseT> generateTree( ObsProject root ) {
        
        if( root == null ) {
            throw new IllegalArgumentException( "Null arg 'root'" );
        }
        
        List<StatusBaseT> ret = new LinkedList<StatusBaseT>();  // returned list
        
        // Create ProjectStatus
        //-------------------------------------------------------
        ProjectStatusEntityT projStatusID = new ProjectStatusEntityT();
        projStatusID.setEntityId( uidProvider.getNewId() );
        
        ObsProjectRefT obsProjectRef = new ObsProjectRefT();
        obsProjectRef.setEntityId( root.getObsProjectEntity().getEntityId() );
      // obsProjectRef.setPartId( null );   // TODO: is that right?
        
        ProjectStatus projStatus = new ProjectStatus();
        projStatus.setProjectStatusEntity( projStatusID );
        projStatus.setObsProjectRef( obsProjectRef );
        projStatus.setStatus( getNewStatus( PHASE2SUB ));
        ret.add( projStatus );
        
        // Create the program status (it's an OUSStatus instance)
        //-------------------------------------------------------
        
        ObsUnitSetT obsPlan = root.getObsProgram().getObsPlan();
        
        ProjectStatusRefT projStatusRef = new ProjectStatusRefT();
        projStatusRef.setEntityId( projStatusID.getEntityId() );
        obsProjectRef.setPartId( obsPlan.getEntityPartId() );
        OUSStatus progStatus = makeOusStatus( obsProjectRef, 
                                              null,  // no parent OUSStatus
                                              projStatusRef );
        ret.add( progStatus );
        
        OUSStatusRefT programRef = new OUSStatusRefT();
        programRef.setEntityId( progStatus.getOUSStatusEntity().getEntityId() );
        projStatus.setObsProgramStatusRef( programRef );
        
        // Now recurse down the tree of ObsUnitSets and create
        // the rest of this tree
        //-----------------------------------------------------
        recurse( obsProjectRef, 
                 root.getObsProgram().getObsPlan(), 
                 progStatus,
                 projStatusRef,
                 ret );
        
        // Done
        return ret;
    }

    /**
     * @param rootRef
     * @param obsProgram
     * @param progStatus
     * @param projStatusRef
     * @param ret 
     */
    private void recurse( ObsProjectRefT obsProjectRef, 
                          ObsUnitSetT rootOUS,
                          OUSStatus rootOUSStatus,
                          ProjectStatusRefT projStatusRef, 
                          List<StatusBaseT> tree ) {

        OUSStatusRefT rootOUSStatusRef = new OUSStatusRefT();
        rootOUSStatusRef.setEntityId( rootOUSStatus.getOUSStatusEntity().getEntityId() );
        rootOUSStatusRef.setPartId( "do-not-care" );
        
        ObsUnitSetTChoice choice = rootOUS.getObsUnitSetTChoice();
        if( choice.getObsUnitSetCount() > 0 ) {
            
            // Our ObsUnitSet contains other ObsUnitSets 
            // We loop over the children, add each one to the tree,
            // and recurse
            //----------------------------------------------------
            ObsUnitSetT[] ouss = choice.getObsUnitSet();
            for( int i = 0; i < ouss.length; i++ ) {
                
                ObsUnitSetT ous = ouss[i];

                ObsProjectRefT obsUnitSetRef = new ObsProjectRefT();
                obsUnitSetRef.setEntityId(obsProjectRef.getEntityId());
                obsUnitSetRef.setPartId( ous.getEntityPartId() ); 
                
                OUSStatus ousStatus = makeOusStatus( obsUnitSetRef, 
                                                     rootOUSStatusRef, 
                                                     projStatusRef );
                
                OUSStatusRefT newOUSStatusRef = new OUSStatusRefT();
                newOUSStatusRef.setEntityId( ousStatus.getOUSStatusEntity().getEntityId() );
                rootOUSStatus.getOUSStatusChoice().addOUSStatusRef( newOUSStatusRef );
 
                tree.add( ousStatus );
                recurse( obsProjectRef, ous, ousStatus, projStatusRef, tree );
            }
        }
        else {
            
            // Our ObsUnitSet contains SchedBlocks 
            // We loop over the children add each one to the tree
            //----------------------------------------------------
            SchedBlockRefT[] sbrefs = choice.getSchedBlockRef();
            for( int i = 0; i < sbrefs.length; i++ ) {
                SBStatus sbs = makeSbStatus( sbrefs[i], 
                                             rootOUSStatus,
                                             rootOUSStatusRef,
                                             projStatusRef );
                tree.add( sbs );
            }
        }
    }

    private OUSStatus makeOusStatus( ObsProjectRefT obsUnitSetRef,
                                     OUSStatusRefT parentOussRef,
                                     ProjectStatusRefT projStatusRef ) {
        
        OUSStatusEntityT id = new OUSStatusEntityT();
        id.setEntityId( uidProvider.getNewId() );
        
        OUSStatus ouss = new OUSStatus();
        ouss.setContainingObsUnitSetRef( parentOussRef );
        ouss.setObsUnitSetRef( obsUnitSetRef );
        ouss.setOUSStatusEntity( id );
        ouss.setProjectStatusRef( projStatusRef );
        ouss.setStatus( getNewStatus( PHASE2SUB ));
        ouss.setOUSStatusChoice( new OUSStatusChoice() );
        
        return ouss;
    }
    
    
    private StatusT getNewStatus( StatusTStateType state ) {
        StatusT ret = new StatusT();
        ret.setState( state );
        return ret;
    }
    
    private SBStatus makeSbStatus( SchedBlockRefT sbRef,
                                   OUSStatus parentOUSStatus, 
                                   OUSStatusRefT parentOussRef,
                                   ProjectStatusRefT projStatusRef ) {
        
        SBStatusEntityT id = new SBStatusEntityT();
        id.setEntityId( uidProvider.getNewId() );
        
        SBStatus sbs = new SBStatus();

        sbs.setSBStatusEntity( id );
        sbs.setSchedBlockRef( sbRef );
        sbs.setProjectStatusRef( projStatusRef );
        sbs.setContainingObsUnitSetRef( parentOussRef );
        sbs.setStatus( getNewStatus( PHASE2SUB ));
        
        // add a reference to this new SBStatus entity in 
        // the parent OUSStatus
        SBStatusRefT sbsRef = new SBStatusRefT();
        sbsRef.setEntityId( id.getEntityId() );
        parentOUSStatus.getOUSStatusChoice().addSBStatusRef( sbsRef );
        
        return sbs;
    }
}
