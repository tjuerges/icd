/**
 * MagicDrawContentHandler.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.uml.magicdraw;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import alma.lifecycle.uml.dto.UmlLifeCycles;
import alma.lifecycle.uml.dto.UmlState;
import alma.lifecycle.uml.dto.UmlStateTransition;

/**
 * 
 * This class converts the Magic Draw UML state machine diagram into Java classes for use by the engine.
 * It is compatible with MagicDraw 15 onwards. 
 * It handles the standard .mdxml file and the xmi 2.1 .uml file format.
 *  
 * @author rkurowsk, March 06, 2009
 * @version $Revision$
 */

// $Id$
public class MagicDrawContentHandler extends DefaultHandler {

	private Logger logger;
	
	private static final String OBSPROJECT_LIFE_CYCLE = "ObsProject life-cycle";
	private static final String OBSUNITSET_LIFE_CYCLE = "ObsUnitSet life-cycle";
	private static final String SCHEDBLOCK_LIFE_CYCLE = "SchedBlock life-cycle";
	
	private static final String PACKAGE_ELEMENT = "packagedElement";
	private static final String STATE_ELEMENT = "subvertex"; 
	
	private static final String TRANSITION_ELEMENT = "transition";
	private static final String TRANSITION_SOURCE_ID_ATTR = "source";
	private static final String TRANSITION_TARGET_ID_ATTR = "target";
	private static final String TRANSITION_GUARD_ID_ATTR = "guard";

	private static final String GUARD_SPECIFICATION_ELEMENT = "specification";
	private static final String GUARD_BODY_ATTR = "body";
	
	private static final String TYPE_ATTR = "xmi:type";
	private static final String STATE_MACHINE_TYPE_ATTR_VALUE = "uml:StateMachine";
	
	private static final String ID_ATTR = "xmi:id";
	private static final String NAME_ATTR = "name";
	
	private static final String PROFILE_INFO = "MagicDraw_Profile:Info";
	private static final String VERSION_ATTR = "version";
	
	private Locator locator;
	private UmlLifeCycles umlLifeCycles = null;

	// all these maps are Keyed by id
	private Map<String, UmlState> obsProjStates = new HashMap<String, UmlState>();
	private Map<String, UmlStateTransition> obsProjTransitions = new HashMap<String, UmlStateTransition>();

	private Map<String, UmlState> sbStates = new HashMap<String, UmlState>();
	private Map<String, UmlStateTransition> sbTransitions = new HashMap<String, UmlStateTransition>();

	private Map<String, UmlState> ousStates = new HashMap<String, UmlState>();
	private Map<String, UmlStateTransition> ousTransitions = new HashMap<String, UmlStateTransition>();
	
	private Map<String, UmlState> currentStates;
	private Map<String, UmlStateTransition> currentTransitions;
	
	private UmlStateTransition currentTransition = null;
	
	/**
	 * Public constructor that accepts a logger
	 * 
	 * @param logger
	 */
	public MagicDrawContentHandler(Logger logger){
		this.logger = logger;
		umlLifeCycles = new UmlLifeCycles(logger);
	}
	
	@Override
	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}
	
	@Override
	public void processingInstruction(String target, String data)
			throws SAXException {
		logger.fine("processingInstruction  Target:" + target + " and Data:" + data);
	}

	@Override
	public void startDocument() throws SAXException {
		logger.fine("startDocument() parsing...");
	}

	@Override
	public void startElement(String namespaceURI, String localName,
			String name, Attributes attributes) throws SAXException {
		if(name.equals(PROFILE_INFO)){
			String version = getMandatoryValue(VERSION_ATTR, attributes);
			logger.info("ObsProject Life-Cycle uml diagram " + version.replaceAll("$", ""));
			
		} else if(name.equals(PACKAGE_ELEMENT) // is this a stateMachine element? (one of the 3 diagrams)
				&& attributes.getValue(TYPE_ATTR) != null
				&& attributes.getValue(TYPE_ATTR).equals(STATE_MACHINE_TYPE_ATTR_VALUE)){
			
			String stateMachineName = getMandatoryValue(NAME_ATTR, attributes);
			logger.fine("=========================\nFound UML StateMachine: " + stateMachineName);
			if(stateMachineName != null && stateMachineName.equals(OBSPROJECT_LIFE_CYCLE)){
				this.currentStates = this.obsProjStates;
				this.currentTransitions = this.obsProjTransitions;
			}else if(stateMachineName != null && stateMachineName.equals(SCHEDBLOCK_LIFE_CYCLE)){
				this.currentStates = this.sbStates;
				this.currentTransitions = this.sbTransitions;
			}else if(stateMachineName != null && stateMachineName.equals(OBSUNITSET_LIFE_CYCLE)){
				this.currentStates = this.ousStates;
				this.currentTransitions = this.ousTransitions;
			}
			
		// is this a State element?
		} else if(name.equals(STATE_ELEMENT) ){
			
			String stateId = getMandatoryValue(ID_ATTR, attributes);
			String stateName = getMandatoryValue(NAME_ATTR, attributes);
			
			// stateName may be null if mistakes were made in UML diagram, states without names will be ignored
			if(stateId != null && stateName != null){
				UmlState st = getOrCreateState(stateId);
				st.setName(stateName);
				logger.fine("Found UML state: " + st);				
			}
		
		// is this a transition element			
		}else if(name.equals(TRANSITION_ELEMENT)){
			
			String transId = getMandatoryValue(ID_ATTR, attributes);
			String sourceStateId = getMandatoryValue(TRANSITION_SOURCE_ID_ATTR, attributes);
			String targetStateId = getMandatoryValue(TRANSITION_TARGET_ID_ATTR, attributes);
			String guardId = getMandatoryValue(TRANSITION_GUARD_ID_ATTR, attributes);
			UmlState sourceState = getOrCreateState(sourceStateId);
			UmlState targetState = getOrCreateState(targetStateId);

			UmlStateTransition trans = new UmlStateTransition(transId, sourceState, targetState, guardId);
			sourceState.getOutgoing().put(transId, trans);
			targetState.getIncoming().put(transId, trans);
			this.currentTransitions.put(transId, trans); 
			this.currentTransition = trans;
			logger.fine("Found UML transition: " + trans);
		// is this an ownedRule/Specification element
		}else if(this.currentTransition != null && name.equals(GUARD_SPECIFICATION_ELEMENT)){
			
			String guardBody = getMandatoryValue(GUARD_BODY_ATTR, attributes);
			if(guardBody != null){

				// the guard body should contain a semicolon delimited list of three comma delimited lists
				// e.g: sybsystem1, subsystem2;role1, role2;
				String[] guards = guardBody.trim().split(";");
				if(guards.length == 3){

					Set<String> subSystemSet = trimStrArrayToSet(guards[0].trim().split(","));
					this.currentTransition.setPermittedSubSystems(subSystemSet);

					Set<String> roleSet = trimStrArrayToSet(guards[1].trim().split(","));
					this.currentTransition.setPermittedRoles(roleSet);
					
					Set<String> locationSet = trimStrArrayToSet(guards[2].trim().split(","));
					this.currentTransition.setPermittedLocations(locationSet);
				}else{
					String msg = "Invalid Guard on transition:"
						+ this.currentTransition.getId() + " Guard: " + guardBody;
					logger.severe(msg);
					throw new SAXException(msg);
				}
				logger.fine("Found UML guard: " + guardBody + " for transition: " + this.currentTransition);
			}
		}
	}

	private Set<String> trimStrArrayToSet(String[] strs){
		
		Set<String> strSet = new LinkedHashSet<String>();
		for(String st: strs){
			strSet.add(st.trim().toLowerCase());
		}
		return strSet;
	}
	
	private UmlState getOrCreateState(String stateId){

		// check if state already exists in map ( an empty one may already have been put there by a transition)
		UmlState state = null;
		if(this.currentStates.containsKey(stateId)){
			state = this.currentStates.get(stateId);
		}else{
			state = new UmlState(stateId);
			this.currentStates.put(stateId, state);
		}
		return state;
	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {

		if(name.equals(TRANSITION_ELEMENT)){
			this.currentTransition = null;
		}
	}

	@Override
	public void endDocument() throws SAXException {
		
		logger.fine("endDocument() parsing...");
		
		// put the transition maps into the UmlLifeCycles object
		umlLifeCycles.setObsProjectLifecycle(buildTransitionMaps(obsProjTransitions));
		
		umlLifeCycles.setObsUnitSetLifecycle(buildTransitionMaps(ousTransitions));
		
		umlLifeCycles.setSchedBlockLifecycle(buildTransitionMaps(sbTransitions));
		
	}

	// builds a map of maps. 
	// the first map's key is the target state name, it's value is a map
	// the second map's key is the source state name, it's value is a umlStateTransition
	private Map<String, Map<String, UmlStateTransition>> buildTransitionMaps(Map<String, UmlStateTransition> transitions){
		
		Map<String, Map<String, UmlStateTransition>> lifecycleTransitionMap = new HashMap<String, Map<String, UmlStateTransition>>();
		
		for(UmlStateTransition trans: transitions.values()){
			String targetStateName = trans.getTargetState().getName();
			Map<String, UmlStateTransition> targetStateTransitions = null;
			
			// check if this target state already exists in the lifecycle map, otherwise create a new map for it.
			if(lifecycleTransitionMap.containsKey(targetStateName)){
				targetStateTransitions = lifecycleTransitionMap.get(targetStateName);
			}else{
				targetStateTransitions = new HashMap<String, UmlStateTransition>();
				lifecycleTransitionMap.put(targetStateName, targetStateTransitions);
			}
			targetStateTransitions.put(trans.getSourceState().getName(), trans);
			
		}
		
		return lifecycleTransitionMap;
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
	}

	@Override
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
	}

	@Override
	public void skippedEntity(String name) throws SAXException {
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
	}

	/**
	 * @return the stateDiagramsDto
	 */
	public UmlLifeCycles getStateMachines() {
		return umlLifeCycles;
	}
	
	private String getMandatoryValue(String attributeName, Attributes attributes){
		
		String value = attributes.getValue(attributeName);
		
		if(value == null || value.equals("")){
			String msg = "Mandatory attribute: '" 
				+ attributeName + "' has no value. Line: " 
				+ this.locator.getLineNumber();
			
			logger.warning(msg);
			
		}
		
		return value;
	}

}

