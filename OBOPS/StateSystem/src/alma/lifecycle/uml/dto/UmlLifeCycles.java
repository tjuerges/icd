/**
 * UmlLifeCycles.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.uml.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.LifeCycle;

/**  
 * Convenience class for grouping the set of lifeCycles
 *  
 * @author rkurowsk, March 06, 2009
 * @version $Revision$
 */

// $Id$
public class UmlLifeCycles {

	// all the lifeCycles are keyed on targetState name then sourceState name 
	// e.g. Map<targetStateName, Map<sourceStateName, UmlStateTransition>>
	private Map<String, Map<String, UmlStateTransition>> obsProjectLifecycle;
	private Map<String, Map<String, UmlStateTransition>> obsUnitSetLifecycle;
	private Map<String, Map<String, UmlStateTransition>> schedBlockLifecycle;
	
	// These 3 maps are an optimization, the state lists are populated 
	// on demand and stored in these maps keyed on subsystem 
	// key: subsystem, value: delimited string e.g. sourceState1:targetState1,targetState2;sourceState2:targetState3,targetState4
	private Map<String, String> obsProjectStatesBySubsystem = new HashMap<String, String>();
	private Map<String, String> obsUnitSetStatesBySubsystem = new HashMap<String, String>();
	private Map<String, String> schedBlockStatesBySubsystem = new HashMap<String, String>();

    /**
     * Default constructor. 
     * It loads stateNameMappings from a properties file
     * 
     * @param logger
     */
    public UmlLifeCycles(Logger logger){}
    
	/**
	 * Returns the obsProjectLifecycle map of transitions
	 * @return obsProjectLifecycle
	 */
	public Map<String, Map<String, UmlStateTransition>> getObsProjectLifecycle() {
		return obsProjectLifecycle;
	}

	/**
	 * Sets the obsProjectLifecycle map of transitions
	 * @param obsProjectLifecycle map
	 */
	public void setObsProjectLifecycle(Map<String, Map<String, UmlStateTransition>> obsProjectLifecycle) {
		this.obsProjectLifecycle = obsProjectLifecycle;
	}

	/**
	 * Returns the obsUnitSetLifecycle map of transitions
	 * @return obsUnitSetLifecycle
	 */
	public Map<String, Map<String, UmlStateTransition>> getObsUnitSetLifecycle() {
		return obsUnitSetLifecycle;
	}

	/**
	 * Sets obsUnitSetLifecycle
	 * @param obsUnitSetLifecycle map
	 */
	public void setObsUnitSetLifecycle(Map<String, Map<String, UmlStateTransition>> obsUnitSetLifecycle) {
		this.obsUnitSetLifecycle = obsUnitSetLifecycle;
	}

	/**
	 * Returns the schedBlockLifecycle transitions map 
	 * @return schedBlockLifecycle map
	 */
	public Map<String, Map<String, UmlStateTransition>> getSchedBlockLifecycle() {
		return schedBlockLifecycle;
	}

	/**
	 * Setter for schedBlockLifecycle
	 * @param schedBlockLifecycle
	 */
	public void setSchedBlockLifecycle(Map<String, Map<String, UmlStateTransition>> schedBlockLifecycle) {
		this.schedBlockLifecycle = schedBlockLifecycle;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString(){
	    
	    String retValue = "";
	    
	    retValue = "UmlLifeCycles ( "
	        + "\n\nobsProjectLifecycle {" + toStringLifeCycle(this.obsProjectLifecycle) 
	        + "\n}\n\nobsUnitSetLifecycle {" + toStringLifeCycle(this.obsUnitSetLifecycle)
	        + "\n}\n\nschedBlockLifecycle { " + toStringLifeCycle(this.schedBlockLifecycle)
	        + "\n})";
	
	    return retValue;
	}
	
	/**
	 * Gets a delimited String of source state names and user friendly target state names 
	 * from the obsProject life-cycle for the given subsystem.
	 * It also extracts the 'AnyState' source and adds it to all the source states.
	 * 
	 * @param subsystem
	 * @return sourceState1:targetState1=targetState1FriendlyName;sourceState2:targetState3,targetState4
	 */
	public synchronized String getObsProjectStates(String subsystem){
		
		return getStatesForSubsystem(subsystem, obsProjectStatesBySubsystem, 
				obsProjectLifecycle, LifeCycle.OBSPROJECT);
		
	}
	
	/** 
	 * Gets a delimited String of source state names and user friendly target state names
	 * from the obsUnitSet life-cycle for the given subsystem.
	 * It also extracts the 'AnyState' source and adds it to all the source states.
	 *   
	 * @param subsystem
	 * @param lifecycle 
	 * @return sourceState1:targetState1=targetState1FriendlyName;sourceState2:targetState3,targetState4
	 */	
	public synchronized String getObsUnitSetStates(String subsystem){
		
		return getStatesForSubsystem(subsystem, obsUnitSetStatesBySubsystem, 
				obsUnitSetLifecycle, LifeCycle.OBSUNITSET);
	}
	
	
	/**
	 * Gets a delimited String of source state names and user friendly target state names
	 * from the SchedBlock life-cycle for the given subsystem.
	 * It also extracts the 'AnyState' source and adds it to all the source states.
	 *   
	 * @param subsystem
	 * @param lifecycle
	 * @return sourceState1:targetState1=targetState1FriendlyName;sourceState2:targetState3,targetState4
	 */	
	public synchronized String getSchedBlockStates(String subsystem){
		
		return getStatesForSubsystem(subsystem, schedBlockStatesBySubsystem, 
				schedBlockLifecycle, LifeCycle.SCHEDBLOCK);
	}
	
	/**
	 * Takes a string of source and target states in the format:
	 * sourceState1:targetState1,targetState2;sourceState2:targetState3,targetState4
	 * and converts it into a Map of Lists of Strings keyed on source state name
	 * 
	 * @param statesString
	 * @return map of states keyed on sourceState name
	 */
	public static Map<String, Set<String>> convertStatesStringToMap(String statesString){
		Map<String, Set<String>> states = new HashMap<String, Set<String>>();
		if(statesString != null && !statesString.equals("")){
			String[] stateLists = statesString.split(";");
			for(String stateList: stateLists){
				String[] sourceAndTargetStates = stateList.split(":");				
				Set<String> targetStatesList = new LinkedHashSet<String>();
				for(String targetState: sourceAndTargetStates[1].split(",")){
					targetStatesList.add(targetState);
				}
				states.put(sourceAndTargetStates[0], targetStatesList);
			}
		}		
		
		return states;
	}
	
	private String toStringLifeCycle(Map<String, Map<String, UmlStateTransition>> lifeCycle){
		
		StringBuilder sb = new StringBuilder();
		for(String targetStateKey: lifeCycle.keySet()){
			Map<String, UmlStateTransition> targetStateTransitions = lifeCycle.get(targetStateKey);
			for(String sourceStateKey: targetStateTransitions.keySet()){
				UmlStateTransition transition = targetStateTransitions.get(sourceStateKey);
				sb.append("\n\t" + transition.toString());
			}
		}
		
//		for(String targetStateKey: lifeCycle.keySet()){
//			Map<String, UmlStateTransition> targetStateTransitions = lifeCycle.get(targetStateKey);
//			sb.append("\nTargetState: " + targetStateKey.toString());
//			for(String sourceStateKey: targetStateTransitions.keySet()){
//				UmlStateTransition transition = targetStateTransitions.get(sourceStateKey);
//				sb.append("\n\tSourceState: " + sourceStateKey.toString());
//				sb.append("\n\t\t" + transition.toString());
//			}
//		}
		return sb.toString();
	}
	
	private Map<String, Set<String>> populateSubsystemStates(String subsystem, 
			Map<String, Map<String, UmlStateTransition>> lifecycle){
		
		// NOTE: "AnyState targets" refers to those target states that can be
		// changed to from AnyState in the life-cycle diagram (AnyState is a reserved word in this context)
		// e.g. In the ObsProject life-cycle "Canceled" is an AnyState target.
		
		// The Key is the source state, the value is a Set of target state names
		Map<String, Set<String>> statesForSubsystem = new HashMap<String, Set<String>>();
		Set<String> anyStateTargets = new HashSet<String>();
		
		// first check if there is an "AnyState" state in this life-cycle.
		// add all its targets allowed for the given subsystem to anyStateTargets
		for(Map<String, UmlStateTransition> sourceStates: lifecycle.values()){
			UmlStateTransition anyStateTransition = sourceStates.get(StatusTStateType.ANYSTATE.toString());
			if(anyStateTransition != null 
					&& anyStateTransition.getPermittedSubSystems().contains(subsystem)){
				anyStateTargets.add(anyStateTransition.getTargetState().getName());
			}
		}

		// iterate through all the sourceStates in the lifecycle extracting all the 
		// transitions allowed for the given subsystem
		for(Map<String, UmlStateTransition> sourceStates: lifecycle.values()){
			for(UmlStateTransition transition: sourceStates.values()){
				String sourceStateName = transition.getSourceState().getName();
				
				// ignore AnyState sources, they have already been extracted.
				// only add the target state if the transition is allowed for the subsystem 
				// or if there is an AnyState transition for this subsystem
				if(!sourceStateName.equals(StatusTStateType.ANYSTATE.toString()) 
						&& (anyStateTargets.size() > 0
								|| transition.getPermittedSubSystems().contains(subsystem))){

					// add a new entry for this sourceStateName if required
					if(!statesForSubsystem.containsKey(sourceStateName)){
						statesForSubsystem.put(sourceStateName, new HashSet<String>());
					}
					
					// add this target state if the transition is permitted for the subsystem
					if(transition.getPermittedSubSystems().contains(subsystem)){
						String targetStateName = transition.getTargetState().getName();
						statesForSubsystem.get(sourceStateName).add(targetStateName);
					}
					
					// add the AnyState targets to this source state 
					// check that the AnyState target is not the source state 
					// e.g. can't change from Canceled to Canceled.
					if(anyStateTargets.size() > 0){
						for(String anyStateTarget: anyStateTargets){
							if(!anyStateTarget.equals(sourceStateName)){
								statesForSubsystem.get(sourceStateName).add(anyStateTarget);
							}
						}
					}
				}
			}
		}

		return statesForSubsystem;
	}
	
	/*
	 * Gets the targets states for the given subsystem, life-cycle etc.
	 */
	private String getStatesForSubsystem(String subsystem, 
			Map<String, String> statesBySubsystem, 
			Map<String, Map<String, UmlStateTransition>> lifeCycle,
			String lifeCycleName){
		
		String statesString = null;
		
		if(!statesBySubsystem.containsKey(subsystem)){
			Map<String, Set<String>> states = null;
			states = populateSubsystemStates(subsystem, lifeCycle);
			statesString = convertStatesMapToString(states, lifeCycleName);			
			statesBySubsystem.put(subsystem, statesString);
		}else{
			statesString = statesBySubsystem.get(subsystem);
		}
		
		return statesString;
	}
	
	/* 
	 * To build the states string, we add a friendly name - coming from a
	 * properties file - to each state adding a '=' sign to it. 
	 */
	private String convertStatesMapToString(Map<String, Set<String>> states, 
			String lifeCycleName){
		
		StringBuilder sb = new StringBuilder();
		for(String sourceState: states.keySet()){
			sb.append(sourceState);
			sb.append(":");
			
			for(String targetState: states.get(sourceState)){			
				sb.append(targetState);
				sb.append(",");
			}
			sb.append(";");
		}
		
		return sb.toString();
		
	}	

	/**
	 * Returns a comma delimited String, in alphabetical order, 
	 * with all a lifeCycle's states (but excluding AnyState)
	 * 
	 * @param lifeCycle
	 * @return state1,state2
	 */
	public String getAllLifeCycleStates(Map<String, Map<String, UmlStateTransition>> lifeCycle){
		
		Set<String> statesSet = new TreeSet<String>();
		// add all target states 
		statesSet.addAll(lifeCycle.keySet());
		
		for(String targetStateKey: lifeCycle.keySet()){
			Map<String, UmlStateTransition> targetStateTransitions = lifeCycle.get(targetStateKey);
			// add all sourceStates
			statesSet.addAll(targetStateTransitions.keySet());
		}
		
		// remove AnyState
		statesSet.remove(StatusTStateType.ANYSTATE.toString());
		
		StringBuilder sb = new StringBuilder();
		
		for(String stateName: statesSet){
			sb.append(stateName);
			sb.append(",");
		}
		
		return sb.toString();
	}
}
