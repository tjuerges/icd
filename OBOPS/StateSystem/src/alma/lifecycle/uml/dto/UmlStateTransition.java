/**
 * UmlStateTransition.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.uml.dto;

import java.util.Set;

import alma.lifecycle.stateengine.StateTransition;


/**
 * 
 * Contains a UML state transition's information
 *  
 * @author rkurowsk, Mar 6, 2009
 * @version $Revision$
 */

// $Id$
public class UmlStateTransition {

	private static final String TRANSITION_NAME_DIVIDER = "To";
	
	private String id;
	private String guardId;

	private UmlState sourceState;
	private UmlState targetState;
	private Set<String> permittedSubSystems;
	private Set<String> permittedRoles;
	private Set<String> permittedLocations;
	
	// this is the state transition action Class that is populated using reflection
	private StateTransition stateTransitionAction;

	public UmlStateTransition(String id, UmlState sourceState, UmlState targetState, String guardId) {
		super();
		this.id = id;
		this.sourceState = sourceState;
		this.targetState = targetState;
		this.guardId = guardId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UmlStateTransition other = (UmlStateTransition) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UmlState getSourceState() {
		return sourceState;
	}

	public void setSourceState(UmlState sourceState) {
		this.sourceState = sourceState;
	}

	public UmlState getTargetState() {
		return targetState;
	}

	public void setTargetState(UmlState targetState) {
		this.targetState = targetState;
	}

	public String getGuardId() {
		return guardId;
	}

	public void setGuardId(String guardId) {
		this.guardId = guardId;
	}

	public StateTransition getStateTransitionAction() {
		return stateTransitionAction;
	}

	public void setStateTransitionAction(StateTransition stateTransitionAction) {
		this.stateTransitionAction = stateTransitionAction;
	}

	
	public Set<String> getPermittedSubSystems() {
		return permittedSubSystems;
	}

	public void setPermittedSubSystems(Set<String> permittedSubSystems) {
		this.permittedSubSystems = permittedSubSystems;
	}

	public Set<String> getPermittedRoles() {
		return permittedRoles;
	}

	public void setPermittedRoles(Set<String> permittedRoles) {
		this.permittedRoles = permittedRoles;
	}

	public Set<String> getPermittedLocations() {
		return permittedLocations;
	}

	public void setPermittedLocations(Set<String> permittedLocations) {
		this.permittedLocations = permittedLocations;
	}
	
	public String getName(){
		String name = TRANSITION_NAME_DIVIDER;
		if(this.sourceState != null){
			name = this.sourceState.getName() + name;
		}
		if(this.targetState != null){
			name = name + this.getTargetState().getName();
		}
		return name;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "  ";
	    String retValue = "";
	    retValue = "UmlStateTransition "
	    	+ this.sourceState + "To" + this.targetState + " ("
	        + "sourceState = " + this.sourceState + TAB
	        + "targetState = " + this.targetState + TAB
	        + "permittedSubSystems = " + this.permittedSubSystems + TAB
	        + "permittedRoles = " + this.permittedRoles + TAB
	        + "permittedLocations = " + this.permittedLocations + TAB
//	        + "id = " + this.id + TAB
//	        + "guardId = " + this.guardId + TAB
	        + "stateTransitionAction = " + this.stateTransitionAction
	        + ")";
	
	    return retValue;
	}
}
