/**
 * UmlState.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.uml.dto;

import java.util.HashMap;
import java.util.Map;

/**
 *  
 *  Contains UML state's information
 *  
 * @author rkurowsk, Mar 6, 2009
 * @version $Revision$
 */

// $Id$
public class UmlState {

	private String id;
	private String name;

	// both maps are keyed on transition id
	private Map<String, UmlStateTransition> incoming = new HashMap<String, UmlStateTransition>();
	private Map<String, UmlStateTransition> outgoing = new HashMap<String, UmlStateTransition>();
		
	public UmlState(String id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the incoming
	 */
	public Map<String, UmlStateTransition> getIncoming() {
		return incoming;
	}

	/**
	 * @param incoming the incoming to set
	 */
	public void setIncoming(Map<String, UmlStateTransition> incoming) {
		this.incoming = incoming;
	}

	/**
	 * @return the outgoing
	 */
	public Map<String, UmlStateTransition> getOutgoing() {
		return outgoing;
	}

	/**
	 * @param outgoing the outgoing to set
	 */
	public void setOutgoing(Map<String, UmlStateTransition> outgoing) {
		this.outgoing = outgoing;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	
	public String toString(){
	    return this.name;
	}
	
//	public String toString(){
//	    final String TAB = "  ";
//	    String retValue = "";
//	    retValue = "UmlState ( "
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "incoming = " + this.incoming + TAB
//	        + "outgoing = " + this.outgoing + TAB
//	        + " )";
//	
//	    return retValue;
//	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UmlState other = (UmlState) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
