/**
 * Copyright European Southern Observatory 2008
 */
package alma.lifecycle.config;

import static alma.lifecycle.config.SpringConstants.STATE_ARCHIVE_BEAN;
import static alma.lifecycle.config.SpringConstants.STATE_ENGINE_BEAN;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.StateEngine;
import alma.obops.utils.config.AbstractContextFactory;

/**
 * 
 * This class, implemented as an enum singleton, is responsible for creating a 
 * Spring Application context and configuring it's DataBase settings using DbConfig.
 * 
 * @author rkurowsk, Nov 2, 2008
 * @version $Revision$
 */

// $Id$
public class StateSystemContextFactory extends AbstractContextFactory {

	/*
	 * The singleton INSTANCE.
	 */
	public static StateSystemContextFactory INSTANCE = new StateSystemContextFactory();
	
	/*
	 * private constructor to enforce Singleton
	 */
	private StateSystemContextFactory(){
	}
	
	/**
	 * @return an instance of a StateArchive
	 */
	public StateArchive getStateArchive(){
		checkIfInitialized();
		return (StateArchive)getBean(STATE_ARCHIVE_BEAN);
	}
	
	/**
	 * @return an instance of a StateEngine
	 */
	public StateEngine getStateEngine(){
		checkIfInitialized();
		return (StateEngine)getBean(STATE_ENGINE_BEAN);
	}
	
}
