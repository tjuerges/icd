package  alma.lifecycle.config;

/**
 * 
 * This class contains constants for Beans in the Spring application context
 * 
 * @author rkurowsk, Nov 2, 2008
 * @version $Revision: 1.4 $
 */

// $Id: SpringConstants.java,v 1.4 2011/02/03 10:28:55 rkurowsk Exp $
public class SpringConstants {

	/** 
	 * Default spring applicatoin context
	 */
    public static final String STATE_SYSTEM_SPRING_CONFIG = "/stateSystemContext.xml";
    
    public static final String TEST_STATE_SYSTEM_SPRING_CONFIG = "/testStateSystemContext.xml";
    
	/**
	 * Name of hibernateProperties attribute on sessionFactory
	 */
	public static final String HIBERNATE_PROPERTIES = "hibernateProperties";
	
	/**
	 * Name of sessionFactory bean in Spring applicationContext
	 */
	public static final String SESSION_FACTORY_BEAN = "sessionFactory";
	
	/**
	 * Name of hibernateUtils bean name
	 */
	public static final String HIBERNATE_UTILS_BEAN = "hibernateUtils";
	
	/**
	 * SessionFactory's hibernate property names 
	 */
	public static final String CONNECTION_DRIVER = "hibernate.connection.driver_class";
	public static final String CONNECTION_PASSWORD = "hibernate.connection.password";
	public static final String CONNECTION_URL = "hibernate.connection.url";
	public static final String CONNECTION_USER = "hibernate.connection.username";
	public static final String CONNECTION_POOL_SIZE = "hibernate.connection.pool_size";
	public static final String HIBERNATE_DIALECT = "hibernate.dialect";

	public static final String STATE_ARCHIVE_BEAN = "stateArchive";
	public static final String STATE_ENGINE_BEAN = "stateEngine";
}
