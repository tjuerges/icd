/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import java.util.Set;

import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

/**
 * Interface for Role providers. 
 *
 * @author rkurowsk, Aug 27, 2009
 * @version $Revision$
 */

// $Id$

public interface RoleProvider {
    
    /**
     * Retrieves a set of roles for a given user id
     */
    public Set<String> getRoles(String userId) throws AcsJNotAuthorizedEx;
    
}
