/**
 * StateEngine.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine;

import java.util.Collection;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.constants.Location;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * State engines implement this interface, requesting state transitions for
 * SchedBlocks, ObsProjects and ObsUnitSets, the <em>target objects</em>.
 * <p/>
 * 
 * State transitions are defined in the life-cycle diagrams and
 * described by <em>state transition classes</em>; that
 * is, classes implementing the {@linkplain StateTransition} interface. The
 * association between a state transition and its class is performed by way of a
 * naming convention (see the interface's documentation).
 * <p/>
 * 
 * All <code>changeState()</code> methods in this interface implement the same
 * algorithm:
 * <p/>
 * 
 * <ol>
 * <li>
 * The method initially checks whether:
 *    <ul>
 *    <li>there exists a transition from the <em>origin state</em> of the target
 *        object to its <em>destination state</em>
 *    <li>that transition can be effected by the given subsystem and role
 *    <li>that transition can take place at the system's current
 *        location
 *    </ul>
 * <p/>
 * 
 * <li>The method then looks for the <em>state transition 
 * class</em> for the requested state transition; if no such class can be found,
 * a {@linkplain ClassNotFoundException} exception is thrown. (Implementors are
 * encouraged to perform this check once and for all state transitions at system
 * startup.)
 * <p/>
 * 
 * The state transition class is instanciated; that objects's
 * {@linkplain StateTransition#guard(SBStatus, OUSStatus, ProjectStatus, String, String, java.util.Set, StatusTStateType)}
 * method is called. (Again, instanciation should be performed once at system
 * startup.)
 * <p/>
 * 
 * <li>The target object is set to the destination state; the state change is
 * persisted and recorded in the history of that object.
 * <p/>
 * 
 * <li>The state transition objects's
 * {@linkplain StateTransition#effect(SBStatus, OUSStatus, ProjectStatus, String, String, java.util.Set)}
 * method is called.
 * </ol>
 * 
 * @author amchavan, May 29, 2009
 * @version $Revision: 1.3 $
 */

// $Id: StateEngine.java,v 1.3 2011/02/03 10:28:55 rkurowsk Exp $

public interface StateEngine {

	/**
	 * Request the State Engine to change the state of the target ObsProject.
	 * 
	 * @param targetEntity
	 *            Reference to the target ObsProject, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeState( ProjectStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId) 
	throws AcsJNoSuchTransitionEx, 
	       AcsJNotAuthorizedEx,
	       AcsJPreconditionFailedEx,
	       AcsJPostconditionFailedEx,
	       AcsJIllegalArgumentEx,
	       AcsJNoSuchEntityEx;

	/**
	 * Request the State Engine to change the state of the target ObsUnitSet.
	 * 
	 * @param targetEntity
	 *            The target ObsUnitSet, whose state should be changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeState( OUSStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId) 
	   throws AcsJNoSuchTransitionEx, 
              AcsJNotAuthorizedEx,
              AcsJPreconditionFailedEx,
              AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;
	
	/**
	 * Request the State Engine to change the state of the target SchedBlock.
	 * 
	 * @param targetEntity
	 *            Reference to the target SchedBlock, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeState( SBStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId ) 
	   throws AcsJNoSuchTransitionEx, 
	          AcsJNotAuthorizedEx,
	          AcsJPreconditionFailedEx,
	          AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;

	/**
	 * Calls the changeState method required by the statusEntityT type 
	 * passed in the stateTransitionParam. 
	 * 
	 * @param stateTransitionParam
	 * @return
	 * @throws AcsJNoSuchTransitionEx
	 * @throws AcsJNotAuthorizedEx
	 * @throws AcsJPreconditionFailedEx
	 * @throws AcsJPostconditionFailedEx
	 * @throws AcsJIllegalArgumentEx
	 * @throws AcsJNoSuchEntityEx
	 */
	public boolean changeState( StateTransitionParam stateTransitionParam) 
		throws AcsJNoSuchTransitionEx, 
		AcsJNotAuthorizedEx,
		AcsJPreconditionFailedEx,
		AcsJPostconditionFailedEx,
		AcsJIllegalArgumentEx,
		AcsJNoSuchEntityEx;
			
    /**
     * @return The location at which this StateEngine is running; that is, one
     *         of the values of the {@link Location} pseudo-enumeration.
     */
	public String getRunLocation();

	/**
	 * Sets the location at which this StateEngine is running (SCO or OSF)
	 * @deprecated  This method should not be called
	 * @param runLocation
	 */
	public void setRunLocation(String runLocation);

    /**
     * @return the path to the input State Engine Machine UML diagram that
     *         describes the ObsProject life cycle.
     */
	public String getInputUmlFilePath();

    /**
     * Sets the path to the input State Engine Machine UML diagram that
     * describes the ObsProject life cycle.
     * 
     * @param inputUmlFilePath
     */
	public void setInputUmlFilePath(String inputUmlFilePath);

    /**
     * Initialize the StateEngine
     * 
     * @param logr
     * @param stateArchive
     * 
     */
	public void initStateEngine( Logger logr, StateArchive stateArchive, RoleProvider roleProvider );
	
	/**
	 * Returns a delimited string of states in the obsProject 
	 * life-cycle for the given subsystem
	 * 
	 * @param subsystem
	 * @return sourceState1:targetState1,targetState2;sourceState2:targetState3,targetState4
	 *   
	 */
	public String getObsProjectStates(String subsystem);
	
	/**
	 * Returns a delimited string of states in the obsUnitSet 
	 * life-cycle for the given subsystem
	 * 
	 * @param subsystem
	 * @return sourceState1:targetState1,targetState2;sourceState2:targetState3,targetState4
	 */
	public String getObsUnitSetStates(String subsystem);
	
	/**
	 * Returns a delimited string of states in the SchedBlock 
	 * life-cycle for the given subsystem
	 * 
	 * @param subsystem
	 * @return sourceState1:targetState1,targetState2;sourceState2:targetState3,targetState4
	 */
	public String getSchedBlockStates(String subsystem);
	
    /**
     * Special insert/update method to solve COMP-4376 (StateArchive & Archive partial insert prevention)
     * Performs the Archive inserts and updates then the StateArchive inserts and updates 
     * and finally the StateTransitions, all in one 'transaction'.
     * <p/>
     * 
     * <b>Note</b>: it is assumed that all references internal to the entity
     * tree are consistent; that is, all references between ProjectStatus and
     * OUSStatuses, and between OUSStatuses and
     * OUSStatuses.<br/>
     * Inconsistent entity trees will most likely cause the transaction(s) to
     * fail.
     * <p/>
     * 
	 * @param archiveEntities
	 *      Array of archive entities that will be inserted/updated in the XML Archive
     * 			i.e. ObsProject, ObsPropsal, SchedBlock etc. 
     * 		May be null for updates.
     *  
	 * @param archiveUser
	 * 		user id for Archive insert. 
	 * 		May be null for updates if archiveEntities is null.
     * 
	 * @param opStatus
	 * 		The obsProject status entity for insertion.
	 * 		May be null for updates.
	 * 
	 * @param ousStatuses
	 * 		An array of all OUSStatus entities for insertion and or update. 
	 * 		These must be ordered with parents before children in the array.  
	 * 		May be null for updates.
     *      
	 * @param sbStatuses
     * 		Array of SBStatus entities for insertion and or update. 
     * 		May be null for updates.
     * 
	 * @param stateTransitions
	 * 		Collection of state transition parameters. 
	 * 		May be null for inserts. 
	 * 
	 * @throws AcsJIllegalArgumentEx
	 * @throws AcsJStateIOFailedEx
	 * @throws AcsJNoSuchEntityEx
	 */
	public void insertOrUpdate(
			XmlEntityStruct[] archiveEntities,
			String archiveUser,
			ProjectStatus opStatus,
			OUSStatus[] ousStatuses, 
			SBStatus[] sbStatuses,
			Collection<StateTransitionParam> stateTransitions)
		throws AcsJIllegalArgumentEx, 
				AcsJStateIOFailedEx,
				AcsJNoSuchEntityEx, 
				AcsJNoSuchTransitionEx, 
				AcsJNotAuthorizedEx, 
				AcsJPreconditionFailedEx, 
				AcsJPostconditionFailedEx;
}
