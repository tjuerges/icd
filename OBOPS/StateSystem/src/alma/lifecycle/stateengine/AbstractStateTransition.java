/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * Abstract implementation of a state transition action class.
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id: AbstractStateTransition.java,v 1.0 Jun 5, 2009 11:04:14 AM rkurowsk Exp
// $
public class AbstractStateTransition implements StateTransition {

	protected Logger logger = null;

	private StateArchive stateArchive;
	private StateEngineInternal stateEngine;
	
	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
		throws AcsJPreconditionFailedEx {
		
		return true;

	}

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles) throws AcsJPostconditionFailedEx {
	}

	public StateArchive getStateArchive() {
		return stateArchive;
	}

	public void setStateArchive(StateArchive stateArchive) {
		this.stateArchive = stateArchive;
	}

	public StateEngineInternal getStateEngine() {
		return stateEngine;
	}

	public void setStateEngine(StateEngineInternal stateEngine) {
		this.stateEngine = stateEngine;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

    /**
     * This method calculates what an ObsUnitSet's status should be based on its
     * children's states
     * 
     * <pre>
     *  Rules to determine the correct ObsUnitSet state in priority order.
     *  For an ObsUnitSet which is a parent of ObsUnitSets:
     *  
     * - Broken if ANY child is Broken (ignore PipelineError children)
     *  else 
     *  - Phase2Submitted if ANY child is Phase2Submitted (ignore PipelineError children)
     *  else
     * - ObservingTimedOut if ANY children are ObservingTimedOut (ignore PipelineError children)
     *  else
     * - Deleted if ALL children are Deleted (ignore PipelineError children)
     *  else
     * - Verified if ALL children are Verified  
     * 		(ignore PipelineError and Deleted children)
     *  else
     * - Processed if ALL children are Processed  
     * 		(ignore PipelineError, Deleted and Verified children)
     *  else
     * - FullyObserved if ALL children are FullyObserved 
     * 		(ignore PipelineError, Deleted, Verified and Processed children)
     *  otherwise 
     * - PartiallyObserved
     * 
     * 
     *  For an ObsUnitSet which is a parent of SchedBlocks: 
     *  
     * - Broken if ANY child is Broken
     *  else 
     *  - Phase2Submitted if ANY child is Phase2Submitted
     *  else
     * - Deleted if ALL children are Deleted
     *  else
     * - Verified if ALL children are Verified and/or VerificationFailure
     * 		(ignore Deleted children)
     *  else
     * - Processed if ALL children are Processed  
     * 		(ignore Deleted and Verified children)
     *  else
     * - FullyObserved if ALL children are FullyObserved  
     * 		(ignore Deleted, Verified and Processed children)
     *  otherwise 
     * - PartiallyObserved
     * 
     * </pre>
     * 
     * @param parentOUSStatus
     * @return what an ObsUnitSet's status should be based on its children's
     *         states
     * @throws AcsJNullEntityIdEx
     * @throws AcsJNoSuchEntityEx
     * @throws AcsJInappropriateEntityTypeEx
     */
	protected StatusTStateType calculateDerivedState(OUSStatus parentOUSStatus) 
		throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {
		
	    StatusTStateType correctState = null;
		
		if(parentOUSStatus.getOUSStatusChoice() != null){

			Set<StatusTStateType> childStates = new HashSet<StatusTStateType>();
			
			// if the children are ObsUnitSets
			if(parentOUSStatus.getOUSStatusChoice().getOUSStatusRefCount() > 0){
				OUSStatusRefT[] childOUSStatusRefs = 
					parentOUSStatus.getOUSStatusChoice().getOUSStatusRef();
				for(OUSStatusRefT ousStatusRef: childOUSStatusRefs){
					OUSStatusEntityT childOusStatusEntityT = new OUSStatusEntityT();
					childOusStatusEntityT.setEntityId(ousStatusRef.getEntityId());
					// retrieve the OUSStatus from the archive
					OUSStatus childOUSStatus = getStateArchive().getOUSStatus(childOusStatusEntityT);
					
					childStates.add(childOUSStatus.getStatus().getState());
				}
				
				correctState = deriveStateFromOusChildren(childStates);				
				
			// if the children are SchedBlocks
			}else if(parentOUSStatus.getOUSStatusChoice().getSBStatusRefCount() > 0){
				SBStatusRefT[] childSchedBlockStatusRefs =
					parentOUSStatus.getOUSStatusChoice().getSBStatusRef();
				for(SBStatusRefT childSBStatusRef: childSchedBlockStatusRefs){
					SBStatusEntityT childSBStatusEntityT = new SBStatusEntityT();
					childSBStatusEntityT.setEntityId(childSBStatusRef.getEntityId());
					// retrieve the SBStatus from the archive
					SBStatus childSBStatus = getStateArchive().getSBStatus(childSBStatusEntityT);
					
					childStates.add(childSBStatus.getStatus().getState());
				}
				
				correctState = deriveStateFromSbChildren(childStates);
			}
			
			logger.fine("correctState is: " + correctState);
			
		}else{
			logger.severe("ObsUnitSet with entityId: " 
					+ parentOUSStatus.getOUSStatusEntity().getEntityId() 
					+ " has no children. How this can happen during a Bubble Up operation is indeed a mystery");
		}
		
		return correctState;
	}
	
	private StatusTStateType deriveStateFromOusChildren(Set<StatusTStateType> childOusStates){
		
	    logger.fine("childOusStates = " + childOusStates.toString());
		
	    StatusTStateType derivedState = null;
	    
		// remove PIPELINEERROR state so it is ignored in further evaluation
		childOusStates.remove(StatusTStateType.PIPELINEERROR);
		
		if(childOusStates.contains(StatusTStateType.BROKEN)){
			
			// BROKEN if ANY child is BROKEN
			derivedState = StatusTStateType.BROKEN;
			
		}else if(childOusStates.contains(StatusTStateType.PHASE2SUBMITTED)){
			
			// PHASE2SUBMITTED if ANY child is PHASE2SUBMITTED
			derivedState = StatusTStateType.PHASE2SUBMITTED;

		} else if(childOusStates.contains(StatusTStateType.OBSERVINGTIMEDOUT)){
				
			// OBSERVINGTIMEDOUT if ANY children are OBSERVINGTIMEDOUT
			derivedState = StatusTStateType.OBSERVINGTIMEDOUT;
				
		}else if(childOusStates.size() == 1 
				&& childOusStates.contains(StatusTStateType.DELETED)){
			
			// DELETED if ALL children are DELETED
			derivedState = StatusTStateType.DELETED;
			
		}else{
			// remove DELETED state so it is ignored in further evaluation
			childOusStates.remove(StatusTStateType.DELETED);
			
			if(childOusStates.size() == 1 
					&& childOusStates.contains(StatusTStateType.VERIFIED)){
					
				// VERIFIED if ALL children are VERIFIED
				derivedState = StatusTStateType.VERIFIED;
					
			}else{
				// remove VERIFIED state so it is ignored in further evaluation
				childOusStates.remove(StatusTStateType.VERIFIED);
				
				if(childOusStates.size() == 1 
							&& childOusStates.contains(StatusTStateType.PROCESSED)){
					
					// PROCESSED if ALL children are PROCESSED
					derivedState = StatusTStateType.PROCESSED;
					
				}else{
					// remove PROCESSED state so it is ignored in further evaluation
					childOusStates.remove(StatusTStateType.PROCESSED); 
					
					if(childOusStates.size() == 1 
							&& childOusStates.contains(StatusTStateType.FULLYOBSERVED)){
						
						// FULLYOBSERVED if ALL children are FULLYOBSERVED 
						derivedState = StatusTStateType.FULLYOBSERVED;
						
					}else{
						// All other states have been eliminated only PARTIALLYOBSERVED remains
						derivedState = StatusTStateType.PARTIALLYOBSERVED;
					}
				}
			}
		}
		
		return derivedState;
	}
	
	private StatusTStateType deriveStateFromSbChildren(Set<StatusTStateType> childSbStates){
		
	    logger.fine("childSbStates = " + childSbStates.toString());

	    StatusTStateType derivedState = null;
	    
		if(childSbStates.contains(StatusTStateType.BROKEN)){
			
			// BROKEN if ANY child is BROKEN
			derivedState = StatusTStateType.BROKEN;
			
		}else if(childSbStates.contains(StatusTStateType.PHASE2SUBMITTED)){
			
			// PHASE2SUBMITTED if ANY child is PHASE2SUBMITTED
			derivedState = StatusTStateType.PHASE2SUBMITTED;
			
		}else if(childSbStates.size() == 1 
				&& childSbStates.contains(StatusTStateType.DELETED)){
			
			// DELETED if ALL children are DELETED
			derivedState = StatusTStateType.DELETED;
			
		}else{
			// remove DELETED state so it is ignored in further evaluation
			childSbStates.remove(StatusTStateType.DELETED);
			
			// Replace OBSERVINGTERMINATED with FULLYOBSERVED as 
			// ObsUnitSets become FULLYOBSERVED for either 
			if(childSbStates.contains(StatusTStateType.OBSERVINGTERMINATED)){
				childSbStates.remove(StatusTStateType.OBSERVINGTERMINATED);
				childSbStates.add(StatusTStateType.FULLYOBSERVED);
			}
			
			if((childSbStates.size() == 1 
						&& (childSbStates.contains(StatusTStateType.VERIFIED) 
								|| childSbStates.contains(StatusTStateType.VERIFICATIONFAILURE))
				|| (childSbStates.size() == 2 
						&& childSbStates.contains(StatusTStateType.VERIFIED) 
						&& childSbStates.contains(StatusTStateType.VERIFICATIONFAILURE))
				)){
					
				// VERIFIED if ALL children are VERIFIED or VERIFICATIONFAILURE
				derivedState = StatusTStateType.VERIFIED;
					
			}else{
				// remove VERIFIED state so it is ignored in further evaluation
				childSbStates.remove(StatusTStateType.VERIFIED);
				
				if(childSbStates.size() == 1 
							&& childSbStates.contains(StatusTStateType.PROCESSED)){
					
					// PROCESSED if ALL children are PROCESSED
					derivedState = StatusTStateType.PROCESSED;
					
				}else{
					// remove PROCESSED state so it is ignored in further evaluation
					childSbStates.remove(StatusTStateType.PROCESSED); 
					
					if(childSbStates.size() == 1 
							&& childSbStates.contains(StatusTStateType.FULLYOBSERVED)){
						
						// FULLYOBSERVED if ALL children are FULLYOBSERVED 
						derivedState = StatusTStateType.FULLYOBSERVED;
						
					}else{
						// All other states have been eliminated only PARTIALLYOBSERVED remains
						derivedState = StatusTStateType.PARTIALLYOBSERVED;
					}
				}
			}
		}
		
		return derivedState;
	}
	
}
