/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

import alma.lifecycle.stateengine.StateTransition;
import alma.lifecycle.stateengine.constants.LifeCycle;
import alma.lifecycle.uml.dto.UmlLifeCycles;
import alma.lifecycle.uml.dto.UmlStateTransition;
import alma.lifecycle.uml.magicdraw.MagicDrawStateDiagramParser;

/**
 *
 * This class generates StateTransition stub classes for a given MagicDraw UML state diagram.
 *
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class StateTransitionGenerator {

	private Logger logger = Logger.getLogger(StateTransitionGenerator.class.getName());
	private static String TEMPLATE;
	private static final String LIFE_CYCLE_EXP = "<lifecycle>";
	private static final String STATE_TRANSITION_NAME_EXP = "<StateTransitionName>";
	
	/**
	 * Loads a uml state diagram and generates stub classes using the StateTransitionTemplate.txt
	 * 
	 * @param inputFilePath
	 * @param outputPath
	 */
	public void generate(String inputFilePath, String outputPath) {

		MagicDrawStateDiagramParser parser = new MagicDrawStateDiagramParser(logger);

		try {
			logger.info("parsing input file: " + inputFilePath);
			
			// load the lifeCycles from the specified uml file
			UmlLifeCycles lifeCycles = parser.parseInputUml(inputFilePath);
			
			// load the action classes into the lifeCycles using reflection
			String packagePrefix = StateTransition.class.getPackage().getName() + ".";

			logger.info("generating action classes for obsProjectLifecycle");
			generateTransitionClasses(lifeCycles.getObsProjectLifecycle().values(), 
					outputPath, packagePrefix, LifeCycle.OBSPROJECT.toLowerCase());

			logger.info("generating action classes for obsUnitSetLifecycle");
			generateTransitionClasses(lifeCycles.getObsUnitSetLifecycle().values(), 
					outputPath, packagePrefix, LifeCycle.OBSUNITSET.toLowerCase());

			logger.info("generating action classes for schedBlockLifecycle");
			generateTransitionClasses(lifeCycles.getSchedBlockLifecycle().values(), 
					outputPath, packagePrefix, LifeCycle.SCHEDBLOCK.toLowerCase());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private void generateTransitionClasses (Collection<Map<String, UmlStateTransition>> lifecycleTransitions, 
			String outputDir, String packagePrefix, String lifeCycle ) throws IOException {
		
		String packagePath = packagePrefix.replaceAll("\\.", "/") + lifeCycle;
		// create output path for this lifecycle
		File packageOutputDir = new File(outputDir + "/" + packagePath);

		// cleanup existing output dir
		if(packageOutputDir.exists()){
			logger.info("Output dir: " + packageOutputDir + " already exists");
		}else{
			boolean createdDir = packageOutputDir.mkdirs();
			if(createdDir){
				logger.info("Success creating new output dir: " + packageOutputDir);
			}else{
				String msg = "Failure creating new output dir: " + packageOutputDir;
				logger.severe(msg);
				throw new RuntimeException(msg);
			}
		}

		for(Map <String, UmlStateTransition> targetStateTransitions: lifecycleTransitions){
			for (UmlStateTransition umlStateTrans : targetStateTransitions.values()) {
				String className = umlStateTrans.getName();
				
				logger.info("Generating transition class: " + packagePath + "/" + className);
				
				String classCode = TEMPLATE.replaceAll(LIFE_CYCLE_EXP, lifeCycle)
					.replaceAll(STATE_TRANSITION_NAME_EXP, className);
	
				logger.finer("\n" + classCode);
				
				File classFile = new File(packageOutputDir.getAbsolutePath() + "/" + className + ".java");
				classFile.createNewFile();
	
				FileWriter fw = null;
				try{
					fw = new FileWriter(classFile);
					fw.write(classCode);
					fw.flush();
				}finally{
					if(fw != null){
						fw.close();
					}
				}
			}
		}
	}

	private void initTemplate(String templateUri) throws IOException{
		
		// load template file into TEMPLATE
		logger.info("Loading template: " + templateUri);
		InputStream templateInputStream = StateTransitionGenerator.class.getResourceAsStream(templateUri);
		logger.info("templateInputStream: " + templateInputStream);
		BufferedReader br =  new BufferedReader(new InputStreamReader(templateInputStream));
		String line = br.readLine();
		StringBuilder sb = new StringBuilder();
		while(line != null){
			sb.append(line);
			sb.append("\n");
			line = br.readLine();
		}
		TEMPLATE = sb.toString();
//		System.out.println(TEMPLATE);
	}
	
	/**
	 * Run the StateTransitionGenerator passing two arguments
	 * 1. Input: the file path to the uml state machine diagram.
	 * 2. Output: the path where the generated files should be written.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {	
			if (args != null && args.length == 2) {
				System.out.println("Starting StateTransitionGenerator with input: " + args[0] + " output:" + args[1]);
				String inputFile = args[0].trim();
				String outputPath = args[1].trim();

				StateTransitionGenerator stg = new StateTransitionGenerator();
				stg.initTemplate("StateTransitionTemplate.txt");
				stg.generate(inputFile, outputPath);
	
			} else {
				System.err.println("Could not start the StateTransitionGenerator due to missing args. " +
						"\n\targ0: uml input file path. \n\targ1: output directory for generated files ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
