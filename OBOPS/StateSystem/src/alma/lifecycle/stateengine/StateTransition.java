/**
 * StateTransition.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * Classes implementing of this interface define the conditions allowing a state
 * transition (its <em>guard</em>) as well as the transition's side effects (its
 * <em>effect</em>).
 * <p/>
 * 
 * State transitions are defined in the life-cycle diagrams and described by
 * implementors of this interface, called
 * <em>state transition classes</em>. Transitions move from the
 * <em>origin state</em> of the target object (the object's current state) to
 * its <em>destination state</em>.<p/>
 * 
 * The association between a state transition and its class is
 * performed by way of a naming convention:<br/>
 * <code>&nbsp;&nbsp;&nbsp;alma.lifecycle.&lt;lifecycle&gt;.&lt;origin&gt;To&lt;destination&gt;</code>
 * <br/>
 * where &lt;lifecycle&gt; is one of <code>obsproject</code>,
 * <code>obsunitset</code> or <code>schedblock</code>. For instance:<br/>
 * <code>&nbsp;&nbsp;&nbsp;alma.lifecycle.schedblock.ReadyToRunning</code>
 * <p/>
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$
public interface StateTransition {

    /**
     * Determine if a state transition can take place. If not, an exception is
     * thrown.
     * 
     * @param schedBlockStatus
     *            SBStatus whose state should be changed; can be
     *            <code>null</code>
     * 
     * @param obsUnitSetStatus
     *            OUSStatus whose state should be changed; can be
     *            <code>null</code>
     * 
     * @param obsProjectStatus
     *            ProjectStatus whose state should be changed; can be
     *            <code>null</code>
     * 
     * @param subsystem
     *            The subsystem requesting the state change
     * 
     * @param userId
     *            ID of the user requesting the state change, to be written into
     *            the state history for the target object. It will not be
     *            verified: it is the responsibility of the caller to
     *            authenticate the user.
     * 
     * @param usersRoles
     *            Roles of the user requesting the state change.
     *  
     * @param targetState
     * 			  
     * @throws AcsJPreconditionFailedEx if the state transition is refused.
     *         This exception's {@link Exception#getCause()} and
     *         {@link Exception#getMessage()} methods can be used to infer why
     *         the guard failed.
     */
    public boolean guard( SBStatus schedBlockStatus, 
                          OUSStatus obsUnitSetStatus,
                          ProjectStatus obsProjectStatus, 
                          String subsystem, 
                          String userId,
                          Set<String> usersRoles,
                          StatusTStateType targetState ) throws AcsJPreconditionFailedEx;

    /**
     * Defines the action to be performed after a state transition was
     * successfully completed.
     * 
     * @param schedBlockStatus
     *            SBStatus whose state was changed; can be <code>null</code>
     * 
     * @param obsUnitSetStatus
     *            OUSStatus whose state was changed; can be <code>null</code>
     * 
     * @param obsProjectStatus
     *            ProjectStatus to which the input SchedBlock belongs; can be
     *            <code>null</code>
     * 
     * @param subsystem
     *            The subsystem requesting the state change
     * 
     * @param userId
     *            ID of the user requesting the state change, to be written into
     *            the state history for the target object. It will not be
     *            verified: it is the responsibility of the caller to
     *            authenticate the user.
     * 
     * @param usersRoles
     *            Roles of the user requesting the state change. 
     * 
     * @throws AcsJPostconditionFailedEx
     *             If anything goes wrong.
     */
    public void effect( SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
                        ProjectStatus obsProjectStatus, String subsystem, String userId,
                        Set<String> usersRoles ) throws AcsJPostconditionFailedEx;

}
