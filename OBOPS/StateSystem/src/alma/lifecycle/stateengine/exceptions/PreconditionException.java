/**
 * PreconditionException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.exceptions;

import alma.lifecycle.stateengine.StateTransition;

/**
 * This exception gets thrown when the <code>guard()</code> method of
 * a {@link StateTransition} resolves that a state transition cannot take place.
 * Callers should use this exception's {@linkplain Throwable#getCause()} or
 * {@linkplain Throwable#getMessage()} methods to infer why the guard failed.
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$

public class PreconditionException extends StateTransitionException {

    private static final long serialVersionUID = -1077514362557073710L;

    public PreconditionException() {
        super();
    }

    public PreconditionException( String message, Throwable cause ) {
        super( message, cause );
    }

    public PreconditionException( String message ) {
        super( message );
    }

    public PreconditionException( Throwable cause ) {
        super( cause );
    }
    
    
}
