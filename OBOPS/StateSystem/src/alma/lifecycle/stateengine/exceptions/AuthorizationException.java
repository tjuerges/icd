/**
 * AuthorizationException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.exceptions;

/**
 * This exception gets thrown when some state transition is not authorized,
 * either because of the invoking subsystem or the provided role.
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$

public class AuthorizationException extends StateTransitionException {

    private static final long serialVersionUID = 4497482805135875572L;

    /**
     * Constructs a new exception with the specified detail message.
     * 
     * @param message
     *            The detail message; it should indicate whether the invoking
     *            subsystem or role are not allowed to perform the requested
     *            state transition.
     */
    public AuthorizationException( String message ) {
        super( message );
    }
}
