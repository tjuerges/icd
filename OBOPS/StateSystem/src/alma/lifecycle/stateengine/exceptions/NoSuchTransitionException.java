/**
 * NoSuchTransition.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.exceptions;

/**
 * This exception gets thrown when some state transition does not exist in the
 * state diagram for a life-cycle.
 * 
 * The {@linkplain #getCurrentState()} method of this exception can be used to
 * retrieve the current state of the state transition's target object. (The
 * initiator of a failed state transition may not be aware of the target
 * object's state at the time of the attempt.)
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$

public class NoSuchTransitionException extends StateTransitionException {

    private static final long serialVersionUID = 7514044463908009637L;

    private Object currentState;
    
    /**
     * @return The currentState of the state transition's target object.
     */
    public Object getCurrentState() {
        return currentState;
    }

    /**
     * Constructor.
     * 
     * @param currentState
     *            The currentState of the target object, to be provided as
     *            information relative to the failed state transition. The
     *            initiator of the state transition may not be aware of the
     *            target object's state at the time of the attempt.
     */
    public NoSuchTransitionException( Object currentState ) {
        super();
        this.currentState = currentState;
    }
}
