/**
 * PreconditionException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.exceptions;

import alma.lifecycle.stateengine.StateTransition;

/**
 * This exception gets thrown if execution of the
 * <code>effect()</code> method of a {@linkplain StateTransition} fails.
 * 
 * Callers should use this exception's {@linkplain Throwable#getCause()} or
 * {@linkplain Throwable#getMessage()} methods to infer why the side-effect method
 * failed.
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$

public class PostconditionException extends StateTransitionException {

    private static final long serialVersionUID = -1077514362557073710L;

    public PostconditionException() {
        super();
    }

    public PostconditionException( String message, Throwable cause ) {
        super( message, cause );
    }

    public PostconditionException( String message ) {
        super( message );
    }

    public PostconditionException( Throwable cause ) {
        super( cause );
    }
}
