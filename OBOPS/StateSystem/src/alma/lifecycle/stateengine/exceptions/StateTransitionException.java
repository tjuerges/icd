/**
 * StateTransitionException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.exceptions;

/**
 * Abstract superclass of all exceptions thrown by the State Engine.
 * 
 * It is provided as a convenience for the callers of the State Engine API, 
 * who can simply catch this exception if they are not interested in the 
 * details of the issue raised by the engine.
 * 
 * @author amchavan, Jun 3, 2009
 * @version $Revision$
 */

// $Id$
public abstract class StateTransitionException extends Exception {

    private static final long serialVersionUID = 8723056236058907280L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public StateTransitionException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with <code>cause</code> is
     * <i>not</i> automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message (which is saved for later retrieval by the
     *            {@link #getMessage()} method).
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link #getCause()} method). (A <tt>null</tt> value is
     *            permitted, and indicates that the cause is nonexistent or
     *            unknown.)
     */
    public StateTransitionException( String message, Throwable cause ) {
        super( message, cause );
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for 
     *          later retrieval by the {@link #getMessage()} method.
     */
    public StateTransitionException( String message ) {
        super( message );
    }

    /**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     * This constructor is useful for exceptions that are little more than
     * wrappers for other throwables.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public StateTransitionException( Throwable cause ) {
        super( cause );
    }
}
