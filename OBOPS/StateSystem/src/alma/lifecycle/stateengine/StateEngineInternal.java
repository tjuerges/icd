/**
 * StateEngine.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine;

import java.util.Set;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * Internal interface for the StateEngine.
 * 
 * @author rkurowsk, August 27, 2009
 * @version $Revision$
 */

// $Id$

public interface StateEngineInternal extends StateEngine {

	/**
	 * Request the State Engine to change the state of the target ObsProject.
	 * 
	 * @param targetEntity
	 *            Reference to the target ObsProject, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param usersRoles
	 * 			  Set of roles assigned to the user
	 * 
	 * @param lockProjectStatus
	 *    		  boolean indicating if the projectStatus should be locked in the DB
	 *    
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( ProjectStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            Set<String> usersRoles,
	                            boolean lockProjectStatus) 
	throws AcsJNoSuchTransitionEx, 
	       AcsJNotAuthorizedEx,
	       AcsJPreconditionFailedEx,
	       AcsJPostconditionFailedEx,
	       AcsJIllegalArgumentEx,
	       AcsJNoSuchEntityEx;

	/**
	 * Request the State Engine to change the state of the target ObsUnitSet.
	 * 
	 * @param targetEntity
	 *            The target ObsUnitSet, whose state should be changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param usersRoles
	 * 			  Set of roles assigned to the user
	 * 
	 * @param lockProjectStatus
	 *    		  boolean indicating if the projectStatus should be locked in the DB
	 *    
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( OUSStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            Set<String> usersRoles,
	                            boolean lockProjectStatus) 
	   throws AcsJNoSuchTransitionEx, 
              AcsJNotAuthorizedEx,
              AcsJPreconditionFailedEx,
              AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;
	
	/**
	 * Request the State Engine to change the state of the target SchedBlock.
	 * 
	 * @param targetEntity
	 *            Reference to the target SchedBlock, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param usersRoles
	 * 			  Set of roles assigned to the user
	 * 
	 * @param lockProjectStatus
	 *    		  boolean indicating if the projectStatus should be locked in the DB
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( SBStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            Set<String> usersRoles,
	                            boolean lockProjectStatus)  
	   throws AcsJNoSuchTransitionEx, 
	          AcsJNotAuthorizedEx,
	          AcsJPreconditionFailedEx,
	          AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;

	/**
	 * Request the State Engine to change the state of the target ObsProject.
	 * 
	 * @param targetEntity
	 *            Reference to the target ObsProject, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( ProjectStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            String role) 
	throws AcsJNoSuchTransitionEx, 
	       AcsJNotAuthorizedEx,
	       AcsJPreconditionFailedEx,
	       AcsJPostconditionFailedEx,
	       AcsJIllegalArgumentEx,
	       AcsJNoSuchEntityEx;

	/**
	 * Request the State Engine to change the state of the target ObsUnitSet.
	 * 
	 * @param targetEntity
	 *            The target ObsUnitSet, whose state should be changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param role
	 * 			  Role assigned to the user
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( OUSStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            String role) 
	   throws AcsJNoSuchTransitionEx, 
              AcsJNotAuthorizedEx,
              AcsJPreconditionFailedEx,
              AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;
	
	/**
	 * Request the State Engine to change the state of the target SchedBlock.
	 * 
	 * @param targetEntity
	 *            Reference to the target SchedBlock, whose state should be
	 *            changed
	 * 
	 * @param targetState
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param role
	 * 			  role for performing this change state
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeStateInternal( SBStatusEntityT targetEntity,
	                            StatusTStateType targetState,
	                            String subsystem, 
	                            String userId, 
	                            String role)  
	   throws AcsJNoSuchTransitionEx, 
	          AcsJNotAuthorizedEx,
	          AcsJPreconditionFailedEx,
	          AcsJPostconditionFailedEx,
              AcsJIllegalArgumentEx,
              AcsJNoSuchEntityEx;
	
	
	/**
	 * Returns a comma delimited string of all the state names in the ObsProject life-cycle
	 * in alphabetical order and excluding AnyState
	 * 
	 * @return e.g. state1,state2,state3
	 */
	public String getAllObsProjectStates();
	
	/**
	 * Returns a comma delimited string of all the state names in the ObsUnitSet life-cycle
	 * in alphabetical order and excluding AnyState
	 * 
	 * @return e.g. state1,state2,state3
	 */
	public String getAllObsUnitSetStates();
	
	/**
	 * Returns a comma delimited string of all the state names in the SchedBlock life-cycle
	 * in alphabetical order and excluding AnyState
	 * 
	 * @return e.g. state1,state2,state3
	 */
	public String getAllSchedBlockStates();
}
