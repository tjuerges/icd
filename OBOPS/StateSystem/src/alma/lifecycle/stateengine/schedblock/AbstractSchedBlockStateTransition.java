/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.AbstractStateTransition;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;

/**
 *
 * Abstract super class for SchedBlock StateTransitions 
 *
 * @author rkurowsk, Jul 8, 2009
 * @version $Revision$
 */

// $Id$
public abstract class AbstractSchedBlockStateTransition extends AbstractStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles) throws AcsJPostconditionFailedEx {
		
		try {
			OUSStatusEntityT obsUnitSetStatusEntityT = new OUSStatusEntityT();
			obsUnitSetStatusEntityT.setEntityId(schedBlockStatus.getContainingObsUnitSetRef().getEntityId());
			OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(obsUnitSetStatusEntityT);
			StatusTStateType parentTargetState = getParentTargetStateForEffect();
			
			// only call change state if the parent target state is different from the current state
			if(!parentTargetState.equals(parentOUSStatus.getStatus().getState())){
				getStateEngine().changeStateInternal(obsUnitSetStatusEntityT, parentTargetState, 
						Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
			}
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}		
	}
	
	protected abstract StatusTStateType getParentTargetStateForEffect();
}
