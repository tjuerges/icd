/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;

/**
 * 
 * This class implements the BrokenToPhase2Submitted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class BrokenToPhase2Submitted extends AbstractSchedBlockStateTransition {

	protected StatusTStateType getParentTargetStateForEffect(){
		return StatusTStateType.PHASE2SUBMITTED;
	}
}
