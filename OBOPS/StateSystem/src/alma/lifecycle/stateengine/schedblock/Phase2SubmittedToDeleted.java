/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;

/**
 * 
 * This class implements the Phase2SubmittedToDeleted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToDeleted extends AbstractSchedBlockStateTransition {

	protected StatusTStateType getParentTargetStateForEffect(){
		return StatusTStateType.DELETED;
	}
	
}
