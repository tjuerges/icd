/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the SuspendedToReady state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class SuspendedToReady extends AbstractStateTransition {

	// no effect
}
