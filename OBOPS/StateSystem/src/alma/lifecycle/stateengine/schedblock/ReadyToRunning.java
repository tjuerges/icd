/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;

/**
 * 
 * This class implements the ReadyToRunning state transition
 * which bubbles up the change to the parent ObsUnitSet 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ReadyToRunning extends AbstractSchedBlockStateTransition {

	protected StatusTStateType getParentTargetStateForEffect(){
		return StatusTStateType.PARTIALLYOBSERVED;
	}
	
}
