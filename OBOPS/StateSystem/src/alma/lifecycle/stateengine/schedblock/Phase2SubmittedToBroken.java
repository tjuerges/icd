/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.AbstractStateTransition;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;

/**
 * 
 * This class implements the Phase2SubmittedToBroken state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author fjulbe, Jul 12, 2010
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToBroken extends AbstractStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		try {
			OUSStatusEntityT obsUnitSetStatusEntityT = new OUSStatusEntityT();
			obsUnitSetStatusEntityT.setEntityId(schedBlockStatus.getContainingObsUnitSetRef().getEntityId());
			OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(obsUnitSetStatusEntityT);
			
			// only call change state if the parent target state is not BROKEN
			if(!StatusTStateType.BROKEN.equals(parentOUSStatus.getStatus().getState())){
				
				getStateEngine().changeStateInternal(obsUnitSetStatusEntityT, 
						StatusTStateType.BROKEN, 
						Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
			}

		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}
	
}
