/**
State transitions of the SchedBlock life-cycle.

@version $Revision$
 */
package alma.lifecycle.stateengine.schedblock;