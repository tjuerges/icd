/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.schedblock;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.AbstractStateTransition;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;



/**
 * 
 * This class implements the BrokenToFullyObserved state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class BrokenToFullyObserved extends AbstractStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles) throws AcsJPostconditionFailedEx {
		
		try {
			
			OUSStatusEntityT obsUnitSetStatusEntityT = new OUSStatusEntityT();
			obsUnitSetStatusEntityT.setEntityId(schedBlockStatus.getContainingObsUnitSetRef().getEntityId());
			
			OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(obsUnitSetStatusEntityT);
			StatusTStateType parentOusTargetState = calculateDerivedState(parentOUSStatus);
			
			logger.info("SchedBlocks parent ObsUnitSet target state = " + parentOusTargetState);
			
			if(!parentOusTargetState.equals(parentOUSStatus.getStatus().getState())){
				if(parentOusTargetState.equals(StatusTStateType.PARTIALLYOBSERVED)
								|| parentOusTargetState.equals(StatusTStateType.FULLYOBSERVED)){
					
					getStateEngine().changeStateInternal(obsUnitSetStatusEntityT, parentOusTargetState,
							Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
				}else{
					logger.warning("ObsUnitSet id: " + obsUnitSetStatusEntityT.getEntityId() 
							+ " has the state: " + parentOUSStatus.getStatus().getState() 
							+ ". This is out of synch with its children. It's correct state is: " + parentOusTargetState);
				}
			}
			
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}		
	}
	
}
