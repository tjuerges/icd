/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.AbstractStateTransition;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * Abstract super class for StatusTStateTypeTransitions 
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public abstract class AbstractObsUnitSetStateTransition extends AbstractStateTransition {
	
	/**
	 * Generic guard implementation
	 */
	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
			throws AcsJPreconditionFailedEx {

		try {
			// check status of all child ous or sb's and see if requested change is allowed according to the rules			
			return checkBubbleUpRules(obsUnitSetStatus, targetState, true);
			
		} catch (Exception e) {
			throw new AcsJPreconditionFailedEx(e);
		}
	}
	

	/**
	 * Helper method for effect() implementations, it uses the most common roles.
	 *
	 * @throws AcsJPostconditionFailedEx
	 */
	protected void doEffect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus, 
			ProjectStatus obsProjectStatus,	String userId, 
			StatusTStateType parentOusTargetState, 
			StatusTStateType projectTargetState)
		throws AcsJPostconditionFailedEx {

		try {
			if(obsUnitSetStatus.getContainingObsUnitSetRef() != null && parentOusTargetState != null){
				OUSStatusEntityT parentOusEntity = getParentOusEntityT(obsUnitSetStatus);
				OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(parentOusEntity);
				// only call changeState if target state is not parents current state
				if(!parentOUSStatus.getStatus().getState().equals(parentOusTargetState)){
					getStateEngine().changeStateInternal(parentOusEntity, parentOusTargetState, 
							Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
				}
			}else{
				ProjectStatusEntityT projectStatusEntityT = getProjectsEntityT(obsUnitSetStatus);
				ProjectStatus projectStatus = getStateArchive().getProjectStatus(projectStatusEntityT);
				// only call changeState if target state is not projects current state
				
				if(!projectStatus.getStatus().getState().equals(projectTargetState)){
					getStateEngine().changeStateInternal(projectStatusEntityT, projectTargetState,	 
							Subsystem.STATE_ENGINE, userId, Role.OBSUNITSET);
				}
			}
		}catch (AcsJNoSuchTransitionEx e){
			// if one of the bubble up effects tries to call an invalid transition just ignore it
			logger.warning("Bubble up halted. " + e.getCause().getMessage());
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}
	
	/**
	 * Performs trickle down of state transition for the 
	 * given ObsUnitSet's child ous & sb's
	 * 
	 * @param obsUnitSetStatus
	 * @param ousTargetState
	 * @param ousRole
	 * @param sbTargetState
	 * @param sbRole
	 * @param userId
	 * @param currentStateRequired 
	 * 		may be null in which case it is ignored, but if provided currentStateRequired 
	 * 		determines what state the OUS or SB must currently be in for the trickle down to change 
	 * 		it to the ousTargetState.
	 * 		e.g. if ousTargetState is "Ready" and currentStateRequired is "Phase2Submitted"
	 * 		then only OUS's that are "Phase2Submitted" will be changed to "Ready". 
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 * @throws AcsJNotAuthorizedEx
	 * @throws AcsJPreconditionFailedEx
	 * @throws AcsJPostconditionFailedEx
	 * @throws AcsJIllegalArgumentEx 
	 * @throws AcsJNoSuchEntityEx 
	 * @throws AcsJInappropriateEntityTypeEx 
	 * @throws AcsJNullEntityIdEx 
	 */
	protected void trickleDown( OUSStatus obsUnitSetStatus, 
	                            StatusTStateType ousTargetState,  
	                            String ousRole,
	                            StatusTStateType sbTargetState, 
	                            String sbRole, 
	                            String subsystem, 
	                            String userId,
	                            StatusTStateType currentStateRequired) 
		throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx,
			AcsJNullEntityIdEx, AcsJInappropriateEntityTypeEx {

		logger.fine("entered trickleDownStateChange for OUSStatus id: " 
				+ obsUnitSetStatus.getOUSStatusEntity().getEntityId());

		// check if the given OUSStatus has children
		if(obsUnitSetStatus.getOUSStatusChoice() != null){

			// check if they are OUSStatus children
			if(obsUnitSetStatus.getOUSStatusChoice().getOUSStatusRefCount() > 0){
				
				OUSStatusRefT[] childOUSStatusRefs = 
					obsUnitSetStatus.getOUSStatusChoice().getOUSStatusRef();

				for(OUSStatusRefT ousStatusRef: childOUSStatusRefs){
					OUSStatusEntityT childOusStatusEntity = new OUSStatusEntityT();
					childOusStatusEntity.setEntityId(ousStatusRef.getEntityId());
					
					OUSStatus childOusOUSStatus = getStateArchive().getOUSStatus(childOusStatusEntity);
					StatusTStateType childOusState = childOusOUSStatus.getStatus().getState();
					
					// only call change state if the child state is different from the target state
					// and if trickleDownTargetState null or trickleDownTargetState is the ous current state
					if(!ousTargetState.equals(childOusState) &&
							(currentStateRequired == null || currentStateRequired.equals(childOusState))){
						// try to change the state of each OUSStatus child
						getStateEngine().changeStateInternal(childOusStatusEntity, ousTargetState, 
								subsystem, userId, ousRole);
					}
				}
				
			// else check if they are SchedBlockStatus children
			}else if(obsUnitSetStatus.getOUSStatusChoice().getSBStatusRefCount() > 0){
				
				SBStatusRefT[] childSchedBlockStatusRefs =
					obsUnitSetStatus.getOUSStatusChoice().getSBStatusRef();
				
				for(SBStatusRefT childSBStatusRef: childSchedBlockStatusRefs){
					SBStatusEntityT childSBStatusEntityT = new SBStatusEntityT();
					childSBStatusEntityT.setEntityId(childSBStatusRef.getEntityId());

					SBStatus childSbStatus = getStateArchive().getSBStatus(childSBStatusEntityT);
					StatusTStateType childSbState = childSbStatus.getStatus().getState();
					
					// only call change state if the child state is different from the target state
					// and if trickleDownTargetState null or trickleDownTargetState is the sb current state
					if(!sbTargetState.equals(childSbState)&&
							(currentStateRequired == null || currentStateRequired.equals(childSbState))){
							
						// try to change the state of each SchedBlockStatus child
						getStateEngine().changeStateInternal(childSBStatusEntityT, sbTargetState, 
								subsystem, userId, sbRole);
					}
				}
			}
		}else{
			logger.warning("ObsUnitSet with entityId: " 
					+ obsUnitSetStatus.getOUSStatusEntity().getEntityId() 
					+ " has no children");
		}
		
	}

	/**
	 * Checks the Bubble Up rules to determine if the given OUSStatus should change state
	 * as a result of its child SchedBlockStatus or OUSStatus changing state.
	 * 
	 * <pre>
	 * 
	 * Bubble Up rules in pseudo code. 
	 * Variables:
	 *  
	 * 	currentState = OUSStatus as it is.
	 * 	correctState = OUSStatus as it should be (taking into account that one of its children has changed state) 
	 * 	targetState  = OUSStatus desired (what the child's 'suggests' to the parent its state should be)
	 *
	 *  if currentState != correctState & correctState == targetState : return True
	 *  	(OUSStatus changes and bubble up continues to its parents)
	 *  
	 *  if currentState != correctState & correctState != targetState : Log an error and return False. 
	 *  	(this will only happen if the OUSStatus is out synch with its children)
	 *  
	 *  if currentState == correctState : return False. 
	 *  	(OUSStatus state is already what it should be)
	 *
	 *   </pre>
	 * @param obsUnitSetStatus
	 * @param targetState
	 * @param raiseAlarmWhenOutOfSynch
	 * 
	 * @return flag that indicates if OUSStatus should change
	 */
	protected boolean checkBubbleUpRules(OUSStatus obsUnitSetStatus, 
			StatusTStateType targetState, boolean raiseAlarmWhenOutOfSynch) 
		throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx {

		logger.fine("entered checkBubbleUpRules for OUSStatus id: " 
				+ obsUnitSetStatus.getOUSStatusEntity().getEntityId());

		boolean result = false;
		if(obsUnitSetStatus != null){
		    StatusTStateType currentState = obsUnitSetStatus.getStatus().getState();
		    StatusTStateType correctState = calculateDerivedState(obsUnitSetStatus);
			
			 if(!currentState.equals(correctState) && correctState.equals(targetState)){
				 result = true;
			 }else if(!currentState.equals(correctState) && !correctState.equals(targetState)){
				 
				 if(raiseAlarmWhenOutOfSynch){
					 //TODO: integrate with alarm system so someone actually sees this message

					 String msg = "OUSStatus id: " + obsUnitSetStatus.getOUSStatusEntity().getEntityId() 
					 + " is out of synch with the states of its children. It is: " + currentState 
					 + " but it should be: " + correctState 
					 + ". \nThis prevented a Bubble Up operation when a child requested a change to: " + targetState;
					 logger.severe(msg);
				 }
				 result = false;
				 
			 }else if(currentState.equals(correctState)){
				 result = false; 
			 }
		}

		return result;
	}
	
	protected OUSStatusEntityT getParentOusEntityT(OUSStatus obsUnitSetStatus){
		
		OUSStatusEntityT parentOusEntityT = new OUSStatusEntityT();
		parentOusEntityT.setEntityId(obsUnitSetStatus.getContainingObsUnitSetRef().getEntityId());
		
		return parentOusEntityT;
	}
	
	protected ProjectStatusEntityT getProjectsEntityT(OUSStatus obsUnitSetStatus){
		
		ProjectStatusEntityT projectEntityT = new ProjectStatusEntityT();
		projectEntityT.setEntityId(obsUnitSetStatus.getProjectStatusRef().getEntityId());
		
		return projectEntityT;
	}
}
