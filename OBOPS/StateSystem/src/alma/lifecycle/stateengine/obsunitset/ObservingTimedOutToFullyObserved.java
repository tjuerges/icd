/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;


/**
 * 
 * This class implements the ObservingTimedOutToFullyObserved state transition
 *  
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ObservingTimedOutToFullyObserved extends AbstractObsUnitSetStateTransition {

	/**
	 * If an OUS is ObservingTimedOut do not check the bubble up rules if the 
	 * ObservingTimedOutToFullyObserved transition is caused by OBOPS, just allow it.
	 * 
	 */
	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
			throws AcsJPreconditionFailedEx {

		boolean result = false;
		try{
			if (Subsystem.OBOPS.equalsIgnoreCase(subsystem)){
				result = true;
			}else if(Subsystem.STATE_ENGINE.equalsIgnoreCase(subsystem)){
				result = checkBubbleUpRules(obsUnitSetStatus, targetState, false);
			}
		} catch (Exception e) {
			throw new AcsJPreconditionFailedEx(e);
		}
		
		return result;
	}
	
	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		try {
			// If this is the original call by OBOPS or the result of
			// a bubble up (role SchedBlock) then bubble up to the parent
			if (Subsystem.OBOPS.equalsIgnoreCase(subsystem) 
				|| Subsystem.STATE_ENGINE.equalsIgnoreCase(subsystem)) {

				// tell parent OUSStatus that this OUSStatus is FullyObserved
				if(obsUnitSetStatus.getContainingObsUnitSetRef() != null){
					getStateEngine().changeStateInternal(getParentOusEntityT(obsUnitSetStatus),	
							StatusTStateType.FULLYOBSERVED,	Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
				}
			}
		}catch (AcsJNoSuchTransitionEx e){
			// if one of the bubble up effects tries to call an invalid transition just ignore it
			logger.warning("Bubble up halted. " + e.getCause().getMessage());
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}
}
