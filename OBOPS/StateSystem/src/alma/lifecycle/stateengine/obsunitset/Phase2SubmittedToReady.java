/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * This class implements the Phase2SubmittedToReady state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToReady extends AbstractObsUnitSetStateTransition {

	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
		throws AcsJPreconditionFailedEx {

		// do nothing and allow trickle down to occur
		return true;
	}
	
	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles) throws AcsJPostconditionFailedEx {

		logger.fine("entered ObsUnitSet Phase2SubmittedToReady effect()");

		try{
			// change child ous & sb's to Ready state 
			// use different roles for ObsUnitSets and SchedBlocks
			// ObsUnitSet status is changed by the role ObsProject (even when it's a recursive ous to ous change)
			// SchedBlock status is changed by the role ObsUnitSet
            trickleDown( obsUnitSetStatus, 
						StatusTStateType.READY, Role.OBSPROJECT, 
						StatusTStateType.READY, Role.OBSUNITSET, 
						Subsystem.STATE_ENGINE, userId, 
						StatusTStateType.PHASE2SUBMITTED);
			
    	} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
		
	}


}
