/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;

/**
 * 
 * This class implements the BrokenToFullyObserved state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class BrokenToFullyObserved extends AbstractObsUnitSetStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		// tell parent OUSStatus or ObsProjectStatus that
		// this OUSStatus is FullyObserved
		try {
			
			OUSStatusEntityT obsUnitSetStatusEntityT = new OUSStatusEntityT();
			StatusTStateType ousParentTargetState = null;
			
			// if there is a ContainingObsUnitSetRef that means that there is an OUS parent 
			if(obsUnitSetStatus.getContainingObsUnitSetRef() != null) {
				obsUnitSetStatusEntityT.setEntityId(obsUnitSetStatus.getContainingObsUnitSetRef().getEntityId());
				OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(obsUnitSetStatusEntityT);
				ousParentTargetState = calculateDerivedState(parentOUSStatus);
				logger.info("ObsUnitset: parent ObsUnitSet target state = " + ousParentTargetState);
			}
			
			if(ousParentTargetState == null ||
				(ousParentTargetState.equals(StatusTStateType.PARTIALLYOBSERVED)
					|| ousParentTargetState.equals(StatusTStateType.FULLYOBSERVED))){
			
				doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
						ousParentTargetState, StatusTStateType.FULLYOBSERVED);
			}
			
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}
}
