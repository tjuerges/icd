/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;


/**
 * 
 * This class implements the BrokenToPhase2Submitted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class BrokenToPhase2Submitted extends AbstractObsUnitSetStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {
			
			doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
					StatusTStateType.PHASE2SUBMITTED, StatusTStateType.REPAIRED);
	}
}
