/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;


/**
 * 
 * This class implements the BrokenToPhase2Broken state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author fjulbe, Jul 12, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToBroken extends AbstractObsUnitSetStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {
			
			doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
					StatusTStateType.BROKEN, StatusTStateType.BROKEN);
	}
}
