/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import alma.lifecycle.stateengine.AbstractStateTransition;


/**
 * 
 * This class implements the ObservingTimedOutToCanceled state transition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ObservingTimedOutToCanceled extends AbstractStateTransition {

	// do nothing
	
}
