/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import alma.lifecycle.stateengine.AbstractStateTransition;


/**
 * 
 * This class implements the FullyObservedToPipelineError state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class FullyObservedToPipelineError extends AbstractStateTransition {

// no effect

}
