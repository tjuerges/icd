/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;



/**
 * 
 * This class implements the Phase2SubmittedToDeleted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToDeleted extends AbstractObsUnitSetStateTransition {

	// TODO: this will bubble up delete to parent OUS's, waiting for feedback before introducing it.

//	@Override
//	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
//            ProjectStatus obsProjectStatus, String subsystem, String userId,
//            Set<String> usersRoles) throws AcsJPostconditionFailedEx {
//
//		logger.fine("entered ObsUnitSet Phase2SubmittedToDeleted effect()");
//
//		try{
//			if(obsUnitSetStatus.getContainingObsUnitSetRef() != null){
//				OUSStatusEntityT parentOusEntity = getParentOusEntityT(obsUnitSetStatus);
//				OUSStatus parentOUSStatus = getStateArchive().getOUSStatus(parentOusEntity);
//				// only call changeState if target state is not parents current state
//				if(!parentOUSStatus.getStatus().getState().equals(StatusTStateType.DELETED)){
//					getStateEngine().changeStateInternal(parentOusEntity, StatusTStateType.DELETED, 
//							Subsystem.STATE_ENGINE, userId, Role.SCHEDBLOCK);
//				}
//			}
//			
//    	} catch (Exception e) {
//			throw new AcsJPostconditionFailedEx(e);
//		}
//	}
	
}
