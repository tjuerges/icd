/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.Role;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * This class implements the PartiallyObservedToObservingTimedOut state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class PartiallyObservedToObservingTimedOut extends AbstractObsUnitSetStateTransition {

	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
		throws AcsJPreconditionFailedEx {

		if(obsUnitSetStatus == null){
			throw new AcsJPreconditionFailedEx(new NullPointerException("obsUnitSetStatus is null"));
		}
		
		// always allow the transition
		return true;
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.AbstractStateTransition#effect(alma.entity.xmlbinding.sbstatus.SBStatus, alma.entity.xmlbinding.ousstatus.OUSStatus, alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.lifecycle.stateengine.enums.Subsystem, int, alma.lifecycle.stateengine.enums.Role)
	 */
	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles) throws AcsJPostconditionFailedEx {

		logger.fine("entered ObsUnitSet AnyStateToObservingTimedOut effect()");

		try{
			// change child ObsUnitSets that are PartiallyObserved to ObservingTimedOut state 
			// ObsUnitSet status is changed by the role ObsProject
			
			// check if the given OUSStatus has OUSStatus children
			if(obsUnitSetStatus.getOUSStatusChoice() != null 
					&& obsUnitSetStatus.getOUSStatusChoice().getOUSStatusRefCount() > 0){
					
				OUSStatusRefT[] childOUSStatusRefs = 
					obsUnitSetStatus.getOUSStatusChoice().getOUSStatusRef();

				for(OUSStatusRefT ousStatusRef: childOUSStatusRefs){
					OUSStatusEntityT childOusStatusEntity = new OUSStatusEntityT();
					childOusStatusEntity.setEntityId(ousStatusRef.getEntityId());
					
					OUSStatus childOusStatus =  getStateArchive().getOUSStatus(childOusStatusEntity);
					
					// try to change the state of each PARTIALLYOBSERVED OUSStatus child to OBSERVINGTIMEDOUT
					if(StatusTStateType.PARTIALLYOBSERVED.equals(childOusStatus.getStatus().getState())){
						getStateEngine().changeStateInternal(childOusStatusEntity, 
								StatusTStateType.OBSERVINGTIMEDOUT, 
								subsystem, userId, Role.OBSPROJECT);
					}
				}
			}
		}catch (AcsJNoSuchTransitionEx e){
			// if one of the bubble up effects tries to call an invalid transition just ignore it
			logger.warning("Bubble up halted. " + e.getCause().getMessage());
			
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
		
	}


}
