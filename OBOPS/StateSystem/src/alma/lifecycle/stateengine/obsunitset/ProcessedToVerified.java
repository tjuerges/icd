/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;


/**
 * 
 * This class implements the ProcessedToVerified state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ProcessedToVerified extends AbstractObsUnitSetStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		// tell parent OUSStatus or ObsProjectStatus that 
		// this OUSStatus is VERIFIED
		doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
				StatusTStateType.VERIFIED, StatusTStateType.VERIFIED);
	}
}
