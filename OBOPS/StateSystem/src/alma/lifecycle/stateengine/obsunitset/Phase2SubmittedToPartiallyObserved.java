/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;


/**
 * 
 * This class implements the Phase2SubmittedToPartiallyObserved state transition
 * which bubbles up to the parent ObsUnitSet or Project
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToPartiallyObserved extends AbstractObsUnitSetStateTransition {

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		// tell parent OUSStatus or ObsProjectStatus that 
		// this OUSStatus is PartiallyObserved
		doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
				StatusTStateType.PARTIALLYOBSERVED, StatusTStateType.PARTIALLYOBSERVED);
	}
	
}
