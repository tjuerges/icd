/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * This class implements the FullyObservedToProcessed state transition
 * 
 * There is an issue with how to implement the trickle down & bubble up
 * transitions initiated at the ObsUnitSet life-cycle. If the ObsUnitSet has
 * parent ObsUnitSets then the bubble up will cause the parent ObsUnitSet to do
 * a trickle down. This results in an invalid transition exception as original
 * ObsUnitSet has already changed state and the parent tries to change it to the
 * same state again. To prevent this a different roles are passed to the trickle
 * down & bubble up operations.
 * 
 * 1. The original change state is called by role: Auto (subsystem Pipeline) 
 * 2. The trickle down will be performed as role: ObsUnitSet (subsystem StateEngine) 
 * 3. The bubble up will be performed as role: SchedBlock (subsystem StateEngine)
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class FullyObservedToProcessed extends AbstractObsUnitSetStateTransition {

	@Override
	public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles,	StatusTStateType targetState) 
			throws AcsJPreconditionFailedEx {

		try {

			// If this is the original call by Pipeline or the result
			// of a trickle down from a parent then trickle down further
			if ((Subsystem.PIPELINE.equalsIgnoreCase(subsystem) 
					&& usersRoles.contains(Role.QAA))
				|| (Subsystem.STATE_ENGINE.equalsIgnoreCase(subsystem) 
					&& usersRoles.contains(Role.OBSUNITSET))) {
				
				trickleDown(obsUnitSetStatus, StatusTStateType.PROCESSED,
						Role.OBSUNITSET, StatusTStateType.PROCESSED,
						Role.OBSUNITSET, Subsystem.STATE_ENGINE, userId, 
						StatusTStateType.FULLYOBSERVED);
			}

			return true;

		} catch (Exception e) {
			throw new AcsJPreconditionFailedEx(e);
		}
	}

	@Override
	public void effect(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
            ProjectStatus obsProjectStatus, String subsystem, String userId,
            Set<String> usersRoles)
			throws AcsJPostconditionFailedEx {

		try {
			// If this is the original call by Pipeline or the result of
			// a bubble up (role SchedBlock) then bubble up to the parent
			if ((Subsystem.PIPELINE.equalsIgnoreCase(subsystem) 
					&& usersRoles.contains(Role.QAA))
				|| (Subsystem.STATE_ENGINE.equalsIgnoreCase(subsystem) 
					&& usersRoles.contains(Role.SCHEDBLOCK))) {

				// tell parent OUSStatus or ObsProjectStatus that 
				// this OUSStatus is PROCESSED
				doEffect(schedBlockStatus, obsUnitSetStatus, obsProjectStatus, userId, 
						StatusTStateType.PROCESSED, StatusTStateType.PROCESSED);

			}
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}
}
