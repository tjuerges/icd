/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsunitset;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

/**
 * 
 * This class implements the ProcessedToPipelineError state transition
 *
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ProcessedToPipelineError extends AbstractObsUnitSetStateTransition {

		@Override
		public boolean guard(SBStatus schedBlockStatus, OUSStatus obsUnitSetStatus,
	            ProjectStatus obsProjectStatus, String subsystem, String userId,
	            Set<String> usersRoles,	StatusTStateType targetState) 
				throws AcsJPreconditionFailedEx {

			try {

				// If this is the original call by OboOps or the result
				// of a trickle down from a parent then trickle down further
				if ((Subsystem.OBOPS.equalsIgnoreCase(subsystem) 
						&& usersRoles.contains(Role.QAA))
					|| (Subsystem.STATE_ENGINE.equalsIgnoreCase(subsystem) 
						&& usersRoles.contains(Role.OBSUNITSET))) {
					
					trickleDown(obsUnitSetStatus, StatusTStateType.PIPELINEERROR,
							Role.OBSUNITSET, StatusTStateType.FULLYOBSERVED,
							Role.OBSUNITSET, Subsystem.STATE_ENGINE, userId, 
							StatusTStateType.PROCESSED);
				}

				return true;

			} catch (Exception e) {
				throw new AcsJPreconditionFailedEx(e);
			}
		}
		
}
