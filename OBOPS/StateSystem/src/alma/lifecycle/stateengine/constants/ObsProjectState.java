/**
 * ObsProjectStates.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

/**
 * The possible states of an ObsProject
 *
 * @author amchavan, May 29, 2009
 * @version $Revision: 1.2 $
 */

// $Id: ObsProjectState.java,v 1.2 2011/02/03 10:28:56 rkurowsk Exp $

public class ObsProjectState {
	
	public static final String APPROVED = "Approved";
	public static final String PHASE2SUBMITTED = "Phase2Submitted";
	public static final String PHASE1SUBMITTED = "Phase1Submitted";	
	public static final String BROKEN = "Broken";
	public static final String READY = "Ready";
	public static final String PARTIALLYOBSERVED = "PartiallyObserved";
	public static final String FULLYOBSERVED = "FullyObserved";
	public static final String CANCELED = "Canceled";
	public static final String REPAIRED = "Repaired";
	public static final String REJECTED = "Rejected";
	public static final String PROCESSED = "Processed";
	public static final String VERIFIED = "Verified";
	public static final String OBSERVING_TIMED_OUT = "ObservingTimedOut";	
	// CSV States
	public static final String CSV_READY = "CSVReady";		
}
