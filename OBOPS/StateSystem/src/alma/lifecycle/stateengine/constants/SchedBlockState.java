/**
 * StatusTStateTypes.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;


/**
 * The possible states of a SchedBlock
 *
 * @author amchavan"; May 29"; 2009
 * @version $Revision: 1.3 $
 */

// $Id: SchedBlockState.java,v 1.3 2011/02/03 10:28:56 rkurowsk Exp $

public class SchedBlockState {
	
	public static final String PHASE2SUBMITTED = "Phase2Submitted";
	public static final String BROKEN = "Broken";
	public static final String READY = "Ready";
	public static final String RUNNING = "Running";
	public static final String FULLYOBSERVED = "FullyObserved";
	public static final String PROCESSED = "Processed";
	public static final String VERIFIED = "Verified";
	public static final String DELETED = "Deleted";
	public static final String SUSPENDED = "Suspended";	
	public static final String VERIFICATIONFAILURE = "VerificationFailure";
	/// CSV States
	public static final String CSV_READY = "CSVReady";	
	public static final String CSV_RUNNING = "CSVRunning";	
	public static final String CSV_SUSPENDED = "CSVSuspended";	
}
