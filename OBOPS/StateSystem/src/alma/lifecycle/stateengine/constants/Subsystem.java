/**
 * StatusTStateTypes.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

/**
 * The subsystems known to the State Engine
 *
 * @author amchavan, May 29, 2009
 * @version $Revision$
 */

// $Id$

public class Subsystem {
	
	public static final String OBOPS = "obops";
	public static final String OBSPREP = "obsprep";
	public static final String SCHEDULING = "scheduling";
	public static final String PIPELINE = "pipeline";
	public static final String STATE_ENGINE = "stateengine";
	
	public static final String[] ALL_SUBSYSTEMS = {OBOPS, 
			OBSPREP, SCHEDULING, PIPELINE, STATE_ENGINE};
	
}
