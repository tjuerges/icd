/**
 * ObsProjectStates.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;


/**
 * The possible states of an ObsUnitSet. Note that the state of an ObsUnitSet
 * depends on the states of the contained SchedBlocks; this 
 * enumeration overlaps largely with {@link StatusTStateType}.
 * 
 * @author amchavan, May 29, 2009
 * @version $Revision: 1.4 $
 */

// $Id: ObsUnitSetState.java,v 1.4 2011/02/03 10:28:56 rkurowsk Exp $
public class ObsUnitSetState {
    
	
	public static final String PHASE2SUBMITTED = "Phase2Submitted";
	public static final String BROKEN = "Broken";
	public static final String READY = "Ready";
	public static final String PARTIALLYOBSERVED = "PartiallyObserved";
	public static final String FULLYOBSERVED = "FullyObserved";
	public static final String PIPELINEERROR = "PipelineError";
	public static final String PROCESSED = "Processed";
	public static final String DELETED = "Deleted";
	public static final String CANCELED = "Canceled";	
	public static final String VERIFIED = "Verified";
	public static final String OBSERVINGTIMEDOUT = "ObservingTimedOut";
	// CSV State
	public static final String CSV_READY = "CSVReady";	
}
