/**
 * LifeCycles.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

/**
 * Names of the life cycles currently supported by the state engine.
 * These names are also used to build up the package names for the 
 * transition classes. 
 *
 * @author rkurowsk, June 05, 2009
 * @version $Revision$
 */

// $Id$

public class LifeCycle {
	public static final String OBSPROJECT = "obsproject";
	public static final String OBSUNITSET = "obsunitset";
	public static final String SCHEDBLOCK = "schedblock";
}
