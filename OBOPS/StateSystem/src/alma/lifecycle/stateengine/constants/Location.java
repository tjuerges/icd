/**
 * StatusTStateTypes.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

/**
 * Geographical locations where the State Engine may run (a pseudo-enumeration)
 *
 * @author amchavan, May 29, 2009
 * @version $Revision$
 */

// $Id$

public class Location {

    /**
     * Name of the system property telling where we are geographically located.
     * The property's value should be on of the items in the {@link #values}
     * list.
     */
    public static final String RUNLOCATION_PROP = "alma.run.location";

    /**
     * Name of the environment variable telling where we are geographically
     * located. The variable value should be on of the items in the
     * {@link #values} list.
     */
    public static final String RUNLOCATION_VAR = "RUNLOCATION";
    
    /** Location: OSF */
	public static final String OSF = "osf";
	
    /** Location: SCO */
    public static final String SCO = "sco";
    
    /** Location: any ARC */
    public static final String ARC = "arc";
    
    /** A virtual location, where all location checks are suspended */
    public static final String TEST = "tst";
    
    /** The list of all locations */
    public static final String[] values = {
        ARC, OSF, SCO, TEST
    };
}
