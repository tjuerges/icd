/**
 * StatusTStateTypes.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.stateengine.constants;

/**
 * The user roles known to the State Engine
 *
 * @author amchavan, May 29, 2009
 * @version $Revision$
 */

// $Id$

public class Role {

	//public static final String AUTO = "auto";
	public static final String ARCA = "arca";  // ARC Astronomer
	public static final String AOD = "aod";    // Astronomer on Duty
	public static final String PI = "pi";      // PI :-)
	public static final String QAA = "qaa";    // QA Astronomer
	public static final String DSOA = "dsoa";  // DSO Astronomer
	public static final String SAS = "sas";    // Science Assessor
	public static final String TAS = "tas";    // Technical Assessor
	public static final String PCH = "pch";    // Panel Chair
	public static final String PHT = "pht";    // Program Handling Team

	// internal roles used by StateEngine only
	public static final String OBSPROJECT = "obsproject";
	public static final String OBSUNITSET = "obsunitset";
	public static final String SCHEDBLOCK = "schedblock";
	
	public static final String[] ALL_ROLES = {
		ARCA, AOD, PI, QAA, DSOA, SAS, TAS, PHT, PCH, 
		OBSPROJECT, OBSUNITSET, SCHEDBLOCK};
	
}
