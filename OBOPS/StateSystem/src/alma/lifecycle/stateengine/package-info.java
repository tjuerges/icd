/**
Classes for the core State Engine, including the definition of all state
transitions.

@version $Revision$
 */
package alma.lifecycle.stateengine;