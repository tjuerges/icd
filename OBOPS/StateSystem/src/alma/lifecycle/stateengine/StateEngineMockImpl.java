/**
 * StateEngineMockImpl.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.stateengine;

import java.util.Collection;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * For testing only
 *
 * @author amchavan, Jul 14, 2009
 * @version $Revision: 1.3 $
 */

// $Id: StateEngineMockImpl.java,v 1.3 2011/02/03 10:28:55 rkurowsk Exp $

public class StateEngineMockImpl implements StateEngine {

    /**
     * @see alma.lifecycle.stateengine.StateEngine#changeState(ProjectStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( ProjectStatusEntityT target,
                                StatusTStateType destination, String subsystem,
                                String userId )
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx {
        // no-op -- mock implementation
        return false;
    }

    /**
     * @see alma.lifecycle.stateengine.StateEngine#changeState(OUSStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( OUSStatusEntityT target,
                                StatusTStateType destination, String subsystem,
                                String userId )
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx {
        // no-op -- mock implementation
        return false;
    }

    /**
     * @see alma.lifecycle.stateengine.StateEngine#changeState(SBStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( SBStatusEntityT target,
                                StatusTStateType destination, String subsystem,
                                String userId)
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx 
   {
        // no-op -- mock implementation
        return false;
    }

	@Override
	public void initStateEngine(Logger logr, StateArchive stateArchive, RoleProvider roleProvider) {
	}

	@Override
	public String getInputUmlFilePath() {
		return null;
	}

	@Override
	public String getRunLocation() {
		return null;
	}

	@Override
	public void setInputUmlFilePath(String inputUmlFilePath) {
	}

	@Override
	public void setRunLocation(String runLocation) {
	}


	@Override
	public String getObsProjectStates(String subsystem) {
		return "";
	}

	@Override
	public String getObsUnitSetStates(String subsystem) {
		return "";
	}

	@Override
	public String getSchedBlockStates(String subsystem) {
		return "";
	}

	@Override
	public boolean changeState(StateTransitionParam stateTransitionParam)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
		return false;
	}

	@Override
	public void insertOrUpdate(XmlEntityStruct[] archiveEntities,
			String user, ProjectStatus opStatus, OUSStatus[] ousStatuses,
			SBStatus[] sbStatuses, Collection<StateTransitionParam> stateTransitions)
			throws AcsJIllegalArgumentEx, AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx, AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx {
		// TODO Auto-generated method stub
		
	}
}
