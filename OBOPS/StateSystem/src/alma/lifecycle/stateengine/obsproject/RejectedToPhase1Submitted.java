/**
 * Copyright European Southern Observatory 2010
 */
package alma.lifecycle.stateengine.obsproject;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the RejectedToPhase1Submitted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, March 11, 2010
 * @version $Revision$
 */

// $Id$
public class RejectedToPhase1Submitted extends AbstractStateTransition {

	// no effect
	
}
