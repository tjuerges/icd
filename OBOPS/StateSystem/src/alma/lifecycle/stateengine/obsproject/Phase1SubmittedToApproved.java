/**
 * Copyright European Southern Observatory 2010
 */
package alma.lifecycle.stateengine.obsproject;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the Phase1SubmittedToApproved state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, March 11, 2010
 * @version $Revision$
 */

// $Id$
public class Phase1SubmittedToApproved extends AbstractStateTransition {

	// no effect
	
}
