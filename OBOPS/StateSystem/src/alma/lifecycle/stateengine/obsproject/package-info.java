/**
State transitions of the ObsProject life-cycle.

@version $Revision$
 */
package alma.lifecycle.stateengine.obsproject;