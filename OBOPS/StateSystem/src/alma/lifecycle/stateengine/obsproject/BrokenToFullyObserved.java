/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsproject;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the BrokenToFullyObserved state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author fjulbe, Jun 12, 2010
 * @version $Revision$
 */
// $Id$

public class BrokenToFullyObserved extends AbstractStateTransition{

}
