/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsproject;

import java.util.Set;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.stateengine.AbstractStateTransition;
import alma.lifecycle.stateengine.constants.Role;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;

/**
 * 
 * This class implements the Phase2SubmittedToReady state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class Phase2SubmittedToReady extends AbstractStateTransition {
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.AbstractStateTransition#effect(alma.entity.xmlbinding.sbstatus.SBStatus, alma.entity.xmlbinding.ousstatus.OUSStatus, alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.lifecycle.stateengine.enums.Subsystem, int, alma.lifecycle.stateengine.enums.Role)
	 */
	@Override
	public void effect(SBStatus schedBlockStatus,
			OUSStatus obsUnitSetStatus, ProjectStatus obsProjectStatus, 
			String subsystem, String userID, Set<String> usersRoles )
			throws AcsJPostconditionFailedEx {

		try {
			// perform trickle down effect 
			// get obsProgram ObsUnitSet and try change its state to ready (p2s to ready)
			OUSStatusEntityT obsUnitSetStatusEntityT = new OUSStatusEntityT();
			obsUnitSetStatusEntityT.setEntityId(obsProjectStatus.getObsProgramStatusRef().getEntityId());
			OUSStatus ousStatus = getStateArchive().getOUSStatus(obsUnitSetStatusEntityT);
			logger.fine("Retrieved ObsProgram: " + ousStatus.toString());
			
			// call change state on StateEngine
			getStateEngine().changeStateInternal(ousStatus.getOUSStatusEntity(), 
					StatusTStateType.READY, Subsystem.STATE_ENGINE, userID, Role.OBSPROJECT);
			
		} catch (Exception e) {
			throw new AcsJPostconditionFailedEx(e);
		}
	}

}
