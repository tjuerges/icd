/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsproject;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the ApprovedToPhase2Submitted state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class ApprovedToPhase2Submitted extends AbstractStateTransition {

	// no effect
	
}
