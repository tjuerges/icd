/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine.obsproject;

import alma.lifecycle.stateengine.AbstractStateTransition;

/**
 * 
 * This class implements the RepairedToBroken state transition
 * 
 * @see alma.lifecycle.stateengine.StateTransition
 * 
 * @author rkurowsk, Jun 5, 2009
 * @version $Revision$
 */

// $Id$
public class RepairedToBroken extends AbstractStateTransition {

	// no effect
	
}
