/**
 * StateEngineImpl.java
 *
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.transaction.annotation.Transactional;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.acs.exceptions.AcsJException;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.stateengine.constants.LifeCycle;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.uml.dto.UmlLifeCycles;
import alma.lifecycle.uml.dto.UmlStateTransition;
import alma.lifecycle.uml.magicdraw.MagicDrawStateDiagramParser;
import alma.obops.utils.DatetimeUtils;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;


/**
 * 
 * This class loads and executes UML state machine diagrams. 
 * 
 * See {@link #determineRunLocation()} for info on how the geographical location
 * of the state engine is determined.
 * 
 * @author rkurowsk, June 04, 2009
 * @version $Revision: 1.7 $
 */

// $Id: StateEngineImpl.java,v 1.7 2011/02/03 10:28:55 rkurowsk Exp $

@Transactional(rollbackFor=AcsJException.class)
public class StateEngineImpl implements StateEngineInternal {

    /** 
     * FOR TESTING ONLY!!!
     * Flag: if true, allow the state engine to be initialized more than once 
     */
	private static boolean canBeReinitialized = false;
	private static final String NOT_INITIALIZED_MSG = "The StateEngine was not initialized correctly. (Check archiveConfig.properties, inputUmlFilePath and RUNLOCATION)";
	
    private Integer mutex = 0;
	private UmlLifeCycles lifeCycles;
	private String runLocation; // one of the values in Location
	private Logger logger = null;
	private StateArchive stateArchive; 
	private String inputUmlFilePath;
	private RoleProvider roleProvider;
	
	private static final String OBSPROJECT_LIFE_CYCLE = "ObsProject life-cycle";
	private static final String OBSUNITSET_LIFE_CYCLE = "ObsUnitSet life-cycle";
	private static final String SCHEDBLOCK_LIFE_CYCLE = "SchedBlock life-cycle";
    
    /**
	 * Default, zero-arg constructor
	 */
	public StateEngineImpl(){
	}
	
	public StateArchive getStateArchive() {
		return stateArchive;
	}

	public void setStateArchive(StateArchive stateArchive) {
		this.stateArchive = stateArchive;
	}

	public String getInputUmlFilePath() {
		return inputUmlFilePath;
	}

	public void setInputUmlFilePath(String inputUmlFilePath) {
		this.inputUmlFilePath = inputUmlFilePath;
	}
	
	/**
	 * @see alma.lifecycle.stateengine.StateEngine#getRunLocation()
	 */
	public String getRunLocation() {
		return runLocation.toLowerCase();
	}
	
	/**
	 * @throws RuntimeException Always
	 * @see alma.lifecycle.stateengine.StateEngine#setRunLocation(java.lang.String)
	 */
	public void setRunLocation(String runLocation) {
		throw new RuntimeException( "Deprecated" );
	}

	/**
	 * Init method that loads a uml state diagram and validates that all the
	 * action classes required by the lifeCycles exist in the classpath
	 */
	public void initStateEngine(Logger logr, StateArchive stateArchive, RoleProvider roleProvider) {

		if( canBeReinitialized || !isInitialized()){
			synchronized(this.mutex){
				this.logger = logr;
				this.stateArchive = stateArchive;
				this.roleProvider = roleProvider;
				
				MagicDrawStateDiagramParser parser = new MagicDrawStateDiagramParser(logr);
				try {
					logger.info("Initializing StateEngine...");
					
					// set the geo location we run at, if we can
                    determineRunLocation();
					
					// load the lifeCycles from the specified uml file
					this.lifeCycles = parser.parseInputUml(inputUmlFilePath);
		
					logger.fine("loading action classes into lifeCycles");
					// load the action classes into the lifeCycles using reflection
					String packagePrefix = StateTransition.class.getPackage().getName() + ".";
		
					injectTransitionActionsIntoLifeCycle(this.lifeCycles.getObsProjectLifecycle().values(), 
							packagePrefix + LifeCycle.OBSPROJECT.toLowerCase());
		
					injectTransitionActionsIntoLifeCycle(this.lifeCycles.getObsUnitSetLifecycle().values(), 
							packagePrefix + LifeCycle.OBSUNITSET.toLowerCase());
		
					injectTransitionActionsIntoLifeCycle(this.lifeCycles.getSchedBlockLifecycle().values(), 
							packagePrefix + LifeCycle.SCHEDBLOCK.toLowerCase());
					
					this.mutex = 1;
					
					logger.fine("Success loading input file: " + inputUmlFilePath);
					logger.info("StateEngine is initialized...");
					if(logger.isLoggable(Level.FINEST)){
						logger.finest(this.lifeCycles.toString());
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}else{
			logger.warning("StateEngineImpl was already initilized. Cannot re-initialize it.");
		}
	}

    /**
     * Checks to see if the state engine has been initialized with a UML diagram
     * 
     * @return <code>true</code> if the state engine has been initialized,
     *         <code>false</code> otherwise.
     */
    public synchronized boolean isInitialized(){
        synchronized(this.mutex){
            return this.mutex != 0;
        }
    }
    
    /**
     * FOR TESTING ONLY! 
     * 
     * Allow the state engine to be initialized more than once
     */
    public static void setCanBeReinitialized(){
        canBeReinitialized = true;
    }

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#changeState(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT, alma.lifecycle.stateengine.enums.StatusTStateType, alma.lifecycle.stateengine.enums.Subsystem, int, alma.lifecycle.stateengine.enums.Role)
	 */
	@Override
	public boolean changeState( ProjectStatusEntityT targetEntity,
	                            StatusTStateType targetState, 
	                            String subsystem, 
	                            String userId) 
		throws AcsJNoSuchTransitionEx, 
	       AcsJNotAuthorizedEx,
	       AcsJPreconditionFailedEx,
	       AcsJPostconditionFailedEx,
	       AcsJIllegalArgumentEx,
	       AcsJNoSuchEntityEx {

		if(!isInitialized()){
			logger.severe(NOT_INITIALIZED_MSG);
			throw new AcsJIllegalArgumentEx(new IllegalStateException(NOT_INITIALIZED_MSG));
		}
		
		logger.finer("changeState called with ObsProjectRefT: " +  targetEntity 
				+ " targetState: " +  targetState 
				+ " Subsystem: " + subsystem
				+ " userId: " +  userId);
		
		Set<String> usersRoles = roleProvider.getRoles(userId);
		
		// call changeStateInternal & tell it to lock the project status
		// project lock only performed when changeState called by external client
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, true);

		
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean changeStateInternal(ProjectStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			String role) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
		
		Set<String> usersRoles = new HashSet<String>();
		usersRoles.add(role);
		
		// do not lock the project as this is an internal call from a state transition action
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, false);
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.util.Set)
	 */
	@Override
	public boolean changeStateInternal(ProjectStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			Set<String> usersRoles, boolean lockProjectStatus) 
	throws AcsJNoSuchTransitionEx,
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx,
			AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx,
			AcsJNoSuchEntityEx {

		boolean stateChanged = false;
		
		// get targetState from lifecycle
		Map<String, UmlStateTransition> targetStatesTransitions = this.lifeCycles.getObsProjectLifecycle().get(targetState.toString());
		
		// check if the target state is valid for this lifecycle (this could happen if the StatusTStateType enums are not kept in sync with UML diagram)
		if(targetStatesTransitions == null){
			throw new AcsJNoSuchTransitionEx(new RuntimeException("Target state: " + targetState + " does not exist in ObsProjectLifecycle"));
		}
		
		try{
			
			// get projectStatus from DB
			ProjectStatus projectStatus = stateArchive.getProjectStatus(targetEntity);

			if(lockProjectStatus){
				stateArchive.lockProjectStatus(targetEntity);
			}
			
			// try to get "anyState" source state transition 
			UmlStateTransition transition = targetStatesTransitions.get(StatusTStateType.ANYSTATE.toString());
			// if "anyState" not found try get current state's transition
			if(transition == null){
				logger.fine("Getting transition for project source state: " + projectStatus.getStatus().getState());
				transition = targetStatesTransitions.get(projectStatus.getStatus().getState().toString());
			}
			
			if(transition == null){
				String msg = "There is no ObsProjectStatus transition from: " + projectStatus.getStatus().getState() 
				+ " to: " + targetState + ". ObsProjectStatus id: " + targetEntity.getEntityId();
				logger.warning(msg);
				throw new AcsJNoSuchTransitionEx(new RuntimeException(msg));
			}
			
			if(transition != null && checkPermissions(transition, subsystem, usersRoles, OBSPROJECT_LIFE_CYCLE)){
				StateTransition stateTransitionAction = transition.getStateTransitionAction();

				//perform precondition
				if(stateTransitionAction.guard(null, null, projectStatus, subsystem, userId, usersRoles, targetState)){

					//change state of project & persist it
					projectStatus.getStatus().setState(targetState);
					
					// if the targetState is ObservingTimedOut then set ProjectWasTimedOut on the ProjectStatus
					if(StatusTStateType.OBSERVINGTIMEDOUT.equals(targetState)){
						projectStatus.setProjectWasTimedOut(DatetimeUtils.formatAsIso(new Date()));
					}
					
					stateArchive.update(projectStatus);
					
					// insert state change history record
					StateChangeRecord scr = new StateChangeRecord(projectStatus,
							new Date(), getRunLocation(), userId, subsystem, "no info");
					stateArchive.insert(scr);
					
					//perform post condition 
					stateTransitionAction.effect(null, null, projectStatus, subsystem, userId, usersRoles);
					
					stateChanged = true;
				}else{
					logger.info("Guard prevented transition of obsProjectStatus: " 
							+ projectStatus.getProjectStatusEntity().getEntityId()
							+ " to state: " + targetState);
				}
			}
		}catch(AcsJStateIOFailedEx e){
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJNullEntityIdEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJInappropriateEntityTypeEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		}
		
		return stateChanged;
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#changeState(alma.entity.xmlbinding.projectstatus.OUSStatusEntityT, alma.lifecycle.stateengine.enums.StatusTStateType, alma.lifecycle.stateengine.enums.Subsystem, int, alma.lifecycle.stateengine.enums.Role)
	 */
	@Override
	public boolean changeState(OUSStatusEntityT targetEntity, 
								StatusTStateType targetState,
								String subsystem, 
								String userId)
		throws  AcsJNoSuchTransitionEx,
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx,
			AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx,
			AcsJNoSuchEntityEx {
		
		if(!isInitialized()){
			logger.severe(NOT_INITIALIZED_MSG);
			throw new AcsJIllegalArgumentEx(new IllegalStateException(NOT_INITIALIZED_MSG));
		}
		
		logger.finer("changeState called with OUSStatusEntityT: " +  targetEntity 
				+ " targetState: " +  targetState 
				+ " Subsystem: " + subsystem
				+ " userId: " +  userId);
		
		Set<String> usersRoles = roleProvider.getRoles(userId);

		// call changeStateInternal & tell it to lock the project status
		// project lock only performed when changeState called by external client
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, true);
		
	}


	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean changeStateInternal(OUSStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			String role) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {

		Set<String> usersRoles = new HashSet<String>();
		usersRoles.add(role);
		
		// do not lock the project as this is an internal call from a state transition action
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, false);
		
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.util.Set)
	 */
	@Override
	public boolean changeStateInternal(OUSStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			Set<String> usersRoles, boolean lockProjectStatus) 
		throws AcsJNoSuchTransitionEx,
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx,
			AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx,
			AcsJNoSuchEntityEx {


		boolean stateChanged = false;
		
		// get targetState from lifecycle
		Map<String, UmlStateTransition> targetStatesTransitions = this.lifeCycles.getObsUnitSetLifecycle().get(targetState.toString());
		
		// check if the target state is valid for this lifecycle (this could happen if the ObsUnitSet enums are not kept in sync with UML diagram)
		if(targetStatesTransitions == null){
			throw new AcsJNoSuchTransitionEx(new RuntimeException("Target state: " + targetState + " does not exist in ObsUnitSetLifecycle"));
		}
		
		try{
			
			// get obsUnitSetStatus from DB
			OUSStatus obsUnitSetStatus = stateArchive.getOUSStatus(targetEntity);

			if(lockProjectStatus){
				ProjectStatusEntityT projectStatusId = new ProjectStatusEntityT();
				projectStatusId.setEntityId(obsUnitSetStatus.getProjectStatusRef().getEntityId());
				stateArchive.lockProjectStatus(projectStatusId);
			}
			
			// try to get "anyState" source state transition 
			UmlStateTransition transition = targetStatesTransitions.get(StatusTStateType.ANYSTATE.toString());
			// if "anyState" not found try get current state's transition
			if(transition == null){
				logger.fine("Getting transition for obsUnitSet source state: " + obsUnitSetStatus.getStatus().getState());
				transition = targetStatesTransitions.get(obsUnitSetStatus.getStatus().getState().toString());
			}
			
			if(transition == null){
				String msg = "There is no OUSStatus transition from: " 
					+ obsUnitSetStatus.getStatus().getState()	+ " to: " + targetState
					+ ". OUSStatus id: " + targetEntity.getEntityId();
				logger.warning(msg);
				throw new AcsJNoSuchTransitionEx(new RuntimeException(msg));
			}
			
			if(transition != null && checkPermissions(transition, subsystem, usersRoles, OBSUNITSET_LIFE_CYCLE)){
				StateTransition stateTransitionAction = transition.getStateTransitionAction();
				
				//perform precondition
				if(stateTransitionAction.guard(null, obsUnitSetStatus, null, subsystem, userId, usersRoles, targetState)){

					//change state of project & persist it
					obsUnitSetStatus.getStatus().setState(targetState);
					stateArchive.update(obsUnitSetStatus);
					
					// insert state change history record
					StateChangeRecord scr = new StateChangeRecord(obsUnitSetStatus,
							new Date(), getRunLocation(), userId, subsystem, "no info");
					stateArchive.insert(scr);
					
					//perform post condition 
					stateTransitionAction.effect(null, obsUnitSetStatus, null, subsystem, userId, usersRoles);
					
					stateChanged = true;
				}else{
					logger.info("Guard prevented transition of obsUnitSetStatus: " 
							+ obsUnitSetStatus.getOUSStatusEntity().getEntityId()
							+ " to state: " + targetState);
				}
			}
		}catch(AcsJStateIOFailedEx e){
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJNullEntityIdEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJInappropriateEntityTypeEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		}
		
		return stateChanged;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#changeState(alma.entity.xmlbinding.projectstatus.SBStatusEntityT, alma.lifecycle.stateengine.enums.StatusTStateType, alma.lifecycle.stateengine.enums.Subsystem, int, alma.lifecycle.stateengine.enums.Role)
	 */
	@Override
	public boolean changeState(SBStatusEntityT targetEntity, 
			StatusTStateType targetState, 
			String subsystem, String userId) 
		throws AcsJNoSuchTransitionEx,
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx,
			AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx, 
			AcsJNoSuchEntityEx {
		
		if(!isInitialized()){
			logger.severe(NOT_INITIALIZED_MSG);
			throw new AcsJIllegalArgumentEx(new IllegalStateException(NOT_INITIALIZED_MSG));
		}
		
		logger.finer("changeState called with ObsProjectRefT: " +  targetEntity 
				+ " targetState: " +  targetState 
				+ " Subsystem: " + subsystem
				+ " userId: " +  userId);
		
		Set<String> usersRoles = roleProvider.getRoles(userId);
	
		// call changeStateInternal & tell it to lock the project status
		// project lock only performed when changeState called by external client
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, true);
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.sbstatus.SBStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean changeStateInternal(SBStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			String role) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {

		Set<String> usersRoles = new HashSet<String>();
		usersRoles.add(role);
		
		// do not lock the project as this is an internal call from a state transition action
		return changeStateInternal(targetEntity, targetState, subsystem, userId, usersRoles, false);
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngineInternal#changeState(alma.entity.xmlbinding.sbstatus.SBStatusEntityT, alma.entity.xmlbinding.valuetypes.types.StatusTStateType, java.lang.String, java.lang.String, java.util.Set)
	 */
	@Override
	public boolean changeStateInternal(SBStatusEntityT targetEntity,
			StatusTStateType targetState, String subsystem, String userId,
			Set<String> usersRoles, boolean lockProjectStatus) 
	throws AcsJNoSuchTransitionEx,
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx,
			AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx,
			AcsJNoSuchEntityEx {


		boolean stateChanged = false;
		
		// get targetState from lifecycle
		Map<String, UmlStateTransition> targetStatesTransitions = this.lifeCycles.getSchedBlockLifecycle().get(targetState.toString());
		
		// check if the target state is valid for this lifecycle (this could happen if the StatusTStateType enums are not kept in sync with UML diagram)
		if(targetStatesTransitions == null){
			throw new AcsJNoSuchTransitionEx(new RuntimeException("Target state: " + targetState + " does not exist in SchedBlockLifecycle"));
		}
		
		try{
			// get SBStatus from DB
			SBStatus schedBlockStatus = stateArchive.getSBStatus(targetEntity);

			if(lockProjectStatus){
				ProjectStatusEntityT projectStatusId = new ProjectStatusEntityT();
				projectStatusId.setEntityId(schedBlockStatus.getProjectStatusRef().getEntityId());
				stateArchive.lockProjectStatus(projectStatusId);
			}
			
			// try to get "anyState" source state transition 
			UmlStateTransition transition = targetStatesTransitions.get(StatusTStateType.ANYSTATE.toString());
			// if "anyState" not found try get current state's transition
			if(transition == null){
				logger.fine("Getting transition for project source state: " + schedBlockStatus.getStatus().getState());
				transition = targetStatesTransitions.get(schedBlockStatus.getStatus().getState().toString());
			}
			
			if(transition == null){
				String msg = "There is no SBStatus transition from: " 
					+ schedBlockStatus.getStatus().getState() + " to: " + targetState
					+ ". schedBlockStatus id: " + targetEntity.getEntityId();
				logger.warning(msg);
				throw new AcsJNoSuchTransitionEx(new RuntimeException(msg));
			}
			
			if(transition != null && checkPermissions(transition, subsystem, usersRoles, SCHEDBLOCK_LIFE_CYCLE)){
				StateTransition stateTransitionAction = transition.getStateTransitionAction();

				//perform precondition
				if(stateTransitionAction.guard(schedBlockStatus, null, null, subsystem, userId, usersRoles, targetState)){

					//change state of project & persist it
					schedBlockStatus.getStatus().setState(targetState);
					stateArchive.update(schedBlockStatus);
					
					// insert state change history record
					StateChangeRecord scr = new StateChangeRecord(schedBlockStatus,
							new Date(), getRunLocation(), userId, subsystem, "no info");
					stateArchive.insert(scr);
					
					//perform post condition 
					stateTransitionAction.effect(schedBlockStatus, null, null, subsystem, userId, usersRoles);
					
					stateChanged = true;
				}else{
					logger.info("Guard prevented transition of schedBlockStatus: " 
							+ schedBlockStatus.getSBStatusEntity().getEntityId()
							+ " to state: " + targetState);
				}
			}
		}catch(AcsJStateIOFailedEx e){
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJNullEntityIdEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} catch (AcsJInappropriateEntityTypeEx e) {
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		}
		
		return stateChanged;
	}

    /**
     * Check if a transition can be authorized.
     * 
     * @return <code>true</code>, always
     * 
     * @throws AcsJNotAuthorizedEx
     *             If any of subsystem, role or {@link #runLocation} do not
     *             match what is required by the state transition.
     */
	private boolean checkPermissions(UmlStateTransition transition,
			String subsystem, Set<String> usersRoles, String lifeCycleName)
		throws AcsJNotAuthorizedEx {

		boolean permitted = false;
		String notPermitted = lifeCycleName + " transition: "
				+ transition.getName() + " is not permitted ";

		if (transition.getPermittedSubSystems().contains(subsystem.toLowerCase())) {
			
			boolean hasRightRole = false;
			
			for(String userRole: usersRoles){
				if(transition.getPermittedRoles().contains(userRole.toLowerCase())){
					hasRightRole = true;
					break;
				}
			}
			
			if (hasRightRole) {
				// see if the geo location is valid -- 'test' is a special case
				// allowing any transition
				if (runLocation == Location.TEST
						|| transition.getPermittedLocations().contains(
								runLocation)) {
					permitted = true;
				} else {
					String msg = notPermitted + "at location: " + runLocation;
					logger.warning(msg);
					throw new AcsJNotAuthorizedEx(new RuntimeException(msg));
				}
			} else {
				String msg = notPermitted + "for roles: " + usersRoles;
				logger.warning(msg);
				throw new AcsJNotAuthorizedEx(new RuntimeException(msg));
			}
		} else {
			String msg = notPermitted + "for subsystem: " + subsystem;
			logger.warning(msg);
			logger.warning(transition.toString());
			throw new AcsJNotAuthorizedEx(new RuntimeException(msg));
		}

		return permitted;
	}
	
	private void injectTransitionActionsIntoLifeCycle (
			Collection<Map<String, UmlStateTransition>> lifecycleTransitions, 
			String packageName)
		throws ClassNotFoundException {

		for (Map<String, UmlStateTransition> targetStateTransitions : lifecycleTransitions) {
			for (UmlStateTransition umlStateTrans : targetStateTransitions.values()) {
				String fullClassName = packageName + "." + umlStateTrans.getName();
				AbstractStateTransition stateTransitionAction = null;
				try{
					Class<?> stateTransitionClass = Class.forName(fullClassName);
					stateTransitionAction = (AbstractStateTransition)stateTransitionClass.newInstance();
					
					// inject dependencies into each transition action
					stateTransitionAction.setStateArchive(this.stateArchive);
					stateTransitionAction.setStateEngine(this);
					stateTransitionAction.setLogger(this.logger);
					
				}catch(ClassNotFoundException e){
					logger.severe("Class not found: " + e.getMessage());
					throw e;
					
				}catch(IllegalAccessException e){
					logger.severe(e.getMessage());
					throw new RuntimeException(e);
	
				} catch (InstantiationException e) {
					logger.severe(e.getMessage());
					throw new RuntimeException(e);
				}
				umlStateTrans.setStateTransitionAction(stateTransitionAction);
			}
		}
	}

    /**
     * Find out where the State Engine is running (its geographical location).
     * <p/>
     * 
     * This method will initially look up the value of system property
     * {@link Location#RUNLOCATION_PROP}; if that value is not set, it will
     * lookup the value of the environment variable
     * {@link Location#RUNLOCATION_VAR}. (Use of system properties is intended
     * for development and unit testing.)
     * <p/>
     * 
     * If no such value is found, or it is an empty string, an exception is
     * thrown. Otherwise the value is checked against the {@link Location}
     * pseudo-enumeration: if it's not equal to any of those values, an
     * exception is thrown. Comparison is case-insensitive.
     * <p/>
     * 
     * Finally, the {@link #runLocation} field is set with one of the values in
     * {@link Location}, so that location tests can be performed with both
     * <code>==</code> and <code>equals()</code>.
     * <p/>
     * 
     * <strong>Note</strong>: if the location is determined to be the special
     * value {@link Location#TEST}, no location checks will be performed by the
     * State Engine. This is meant <em>for development only</em> and should
     * never be used in production installations.
     * 
     * @see #getRunLocation()
     * 
     * @throws RuntimeException
     *             If the geographical location cannot be determined.
     */
	protected void determineRunLocation() throws RuntimeException {
	    
	    // Prepare standard error message
	    String stdmsg = 
	            ": please check the value of the '" +
	            Location.RUNLOCATION_VAR +
	    		"' env variable (or the '" +
	    		Location.RUNLOCATION_PROP +
	    		"' system property), should be one of: ";
	    for( String value : Location.values ) {
            stdmsg += value + " ";
        }
	    
	    // Retrieve the geo location: system property
	    // overrides environment variable
	    String tmp = System.getProperty( Location.RUNLOCATION_PROP );
	    if( tmp == null ) {
	        tmp = System.getenv( Location.RUNLOCATION_VAR );
	    }
	    
	    // Check that geo location is set
	    if( tmp == null || tmp.length() == 0 ) {
	        String msg = "Location is not set or empty" + stdmsg;
	        throw new RuntimeException( msg );
	    }
	    
	    // Loop over valid values
	    for( String value : Location.values ) {
	        // Do we have a valid value?
            if( tmp.toLowerCase().equals( value )) {
                runLocation = value;    // YES, we're done
                return;
            }
        }
	    
	    // Given value is invalid
	    String msg = "Invalid value for location: '" + tmp + "'" + stdmsg;
        throw new RuntimeException( msg );
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getObsProjectStates(java.lang.String)
	 */
	@Override
	public String getObsProjectStates(String subsystem) {
		return lifeCycles.getObsProjectStates(subsystem);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getObsUnitSetStates(java.lang.String)
	 */
	@Override
	public String getObsUnitSetStates(String subsystem) {
		return lifeCycles.getObsUnitSetStates(subsystem);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getSchedBlockStates(java.lang.String)
	 */
	@Override
	public String getSchedBlockStates(String subsystem) {
		return lifeCycles.getSchedBlockStates(subsystem);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getAllObsProjectStates()
	 */
	@Override
	public String getAllObsProjectStates() {
		return lifeCycles.getAllLifeCycleStates(lifeCycles.getObsProjectLifecycle());
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getAllObsUnitSetStates()
	 */
	@Override
	public String getAllObsUnitSetStates() {
		return lifeCycles.getAllLifeCycleStates(lifeCycles.getObsUnitSetLifecycle());
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getAllSchedBlockStates()
	 */
	@Override
	public String getAllSchedBlockStates() {
		return lifeCycles.getAllLifeCycleStates(lifeCycles.getSchedBlockLifecycle());
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#changeState(alma.lifecycle.stateengine.StateTransitionParam)
	 */
	@Override
	public boolean changeState(StateTransitionParam stp)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {

		if(stp.getTargetStatusEntity() instanceof ProjectStatusEntityT){
			
			return changeState((ProjectStatusEntityT)stp.getTargetStatusEntity(), 
				stp.getTargetState(), stp.getSubsystem(), stp.getUserId());
			
		}else if(stp.getTargetStatusEntity() instanceof OUSStatusEntityT){
			
			return changeState((OUSStatusEntityT)stp.getTargetStatusEntity(),
					stp.getTargetState(), stp.getSubsystem(), stp.getUserId());
			
		}else if(stp.getTargetStatusEntity() instanceof SBStatusEntityT){
			
			return changeState((SBStatusEntityT)stp.getTargetStatusEntity(), 
					stp.getTargetState(), stp.getSubsystem(), stp.getUserId());
			
		}else{
			throw new AcsJIllegalArgumentEx(
					new IllegalArgumentException("Un-supported Entity type: " 
							+ stp.getTargetStatusEntity().getClass().getName()));
		}
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#insertOrUpdate(alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.ousstatus.OUSStatus[], alma.entity.xmlbinding.sbstatus.SBStatus[], alma.xmlentity.XmlEntityStruct[], java.lang.String, java.util.Collection)
	 */
	@Override
	public void insertOrUpdate(
			XmlEntityStruct[] archiveEntities, 
			String archiveUser,
			ProjectStatus opStatus,
			OUSStatus[] ousStatuses, 
			SBStatus[] sbStatuses,
			Collection<StateTransitionParam> stateTransitions)
		throws AcsJIllegalArgumentEx, 
			AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx, 
			AcsJNoSuchTransitionEx, 
			AcsJNotAuthorizedEx, 
			AcsJPreconditionFailedEx, 
			AcsJPostconditionFailedEx {
		
		// insert/update archive
		if(archiveEntities != null && archiveEntities.length > 0){
			stateArchive.storeArchiveEntities(archiveEntities, archiveUser, logger);
		}
		
		// update opStatus entity
		if(opStatus != null){
			stateArchive.update(opStatus);
		}
		
		// insert/update ousStatus entities
		if(ousStatuses != null){
			for(OUSStatus ousStatus : ousStatuses){
				stateArchive.update(ousStatus);
			}
		}

		// insert/update sbStatus entities
		if(sbStatuses != null){
			for(SBStatus sbStatus : sbStatuses){
				stateArchive.update(sbStatus);
			}
		}
		
		// perform state transitions
		if(stateTransitions != null){
			for(StateTransitionParam stp : stateTransitions){
				changeState(stp);
			}
		}
	}

}

