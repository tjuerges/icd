/**
 * Copyright European Southern Observatory 2010
 */

package alma.lifecycle.stateengine;

import alma.entities.commonentity.EntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;


/**
 * Value object for state transition parameters passed to the StateEngine for a specific transition.
 * 
 * @author rkurowsk, Nov 08, 2010
 * @version $Revision: 1.2 $
 */

// $Id: StateTransitionParam.java,v 1.2 2011/02/03 10:28:55 rkurowsk Exp $
public class StateTransitionParam {

	private EntityT targetStatusEntity;
	private StatusTStateType targetState;
	private String subsystem; 
	private String userId;
	
	/**
	 * Default constructor
	 * 
	 * @param targetStatusEntity
	 * @param targetState
	 * @param subsystem
	 * @param userId
	 */
	public StateTransitionParam(EntityT targetStatusEntity,
			StatusTStateType targetState, 
			String subsystem, 
			String userId) {
		this.targetStatusEntity = targetStatusEntity;
		this.targetState = targetState;
		this.subsystem = subsystem;
		this.userId = userId;
	}

	public EntityT getTargetStatusEntity() {
		return targetStatusEntity;
	}

	public StatusTStateType getTargetState() {
		return targetState;
	}

	public String getSubsystem() {
		return subsystem;
	}

	public String getUserId() {
		return userId;
	}

	
	
}
