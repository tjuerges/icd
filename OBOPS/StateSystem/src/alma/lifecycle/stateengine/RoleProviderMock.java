/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.stateengine;

import java.util.HashSet;
import java.util.Set;

import alma.lifecycle.stateengine.constants.Role;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;

/**
 *
 * Mock impl of RoleProvider
 *
 * @author rkurowsk, Aug 27, 2009
 * @version $Revision$
 */

// $Id$
public class RoleProviderMock implements RoleProvider {
	public static final String ALL_ROLES_USER = "user_with_all_roles";
	
	private static final Set<String> allRoles = new HashSet<String>();
	
	static{
		allRoles.add(Role.AOD);
		allRoles.add(Role.ARCA);
		allRoles.add(Role.OBSPROJECT);
		allRoles.add(Role.OBSUNITSET);
		allRoles.add(Role.PI);
		allRoles.add(Role.QAA);
		allRoles.add(Role.SCHEDBLOCK);
		allRoles.add(Role.DSOA);
		allRoles.add(Role.SAS);
		allRoles.add(Role.TAS);
		allRoles.add(Role.PHT);
		allRoles.add(Role.PCH);
	}
	
	@Override
	public Set<String> getRoles(String userId) throws AcsJNotAuthorizedEx {
		
		Set<String> roles = new HashSet<String>();

		if (userId.equals(ALL_ROLES_USER)) {
			roles.addAll(allRoles);
		}else if(allRoles.contains(userId.toLowerCase())){
			roles.add(userId.toLowerCase());
		}
		
		return roles;
	}

}
