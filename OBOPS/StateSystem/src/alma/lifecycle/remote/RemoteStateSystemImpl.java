package alma.lifecycle.remote;

import static alma.lifecycle.config.SpringConstants.STATE_SYSTEM_SPRING_CONFIG;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.ACSErrTypeCommon.IllegalArgumentEx;
import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.acs.entityutil.EntityException;
import alma.acs.entityutil.EntitySerializer;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.asdmIDLTypes.IDLArrayTime;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.StateSystem;
import alma.lifecycle.StateSystemBaseImpl;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.stateengine.RoleProvider;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.lifecycle.stateengine.StateEngine;
import alma.projectlifecycle.StateChangeData;
import alma.projectlifecycle.StateSystemJ;
import alma.statearchiveexceptions.EntitySerializationFailedEx;
import alma.statearchiveexceptions.InappropriateEntityTypeEx;
import alma.statearchiveexceptions.NoSuchEntityEx;
import alma.statearchiveexceptions.NullEntityIdEx;
import alma.statearchiveexceptions.StateIOFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.NoSuchTransitionEx;
import alma.stateengineexceptions.NotAuthorizedEx;
import alma.stateengineexceptions.PostconditionFailedEx;
import alma.stateengineexceptions.PreconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;
/**
 * Class to expose the state system (comprised of the state archive and state engine) 
 * to the "outside world" (i.e. other subsystems outside of OBOPS) via an ACS/CORBA 
 * component.
 * 
 * @author Steve Harrington
 */
public class RemoteStateSystemImpl implements ComponentLifecycle, StateSystemJ
{
	protected ContainerServices containerServices;
	protected Logger logger;
	protected StateSystem stateSystem;

	/////////////////////////////////////////////////////////////
	// Implementation of StateSystemJ interface
	/////////////////////////////////////////////////////////////


	@Override
	public String getObsProjectStates(String subsystem) 
	{
		String retVal = "";
		retVal = stateSystem.getObsProjectStates(subsystem);
		return retVal;
	}

	@Override
	public String getObsUnitSetStates(String subsystem) 
	{
		String retVal = "";
		retVal = stateSystem.getObsUnitSetStates(subsystem);		
		return retVal;
	}

	@Override
	public String getSchedBlockStates(String subsystem) 
	{
		String retVal = "";
		retVal = stateSystem.getSchedBlockStates(subsystem);
		return retVal;
	}
	
	@Override
	public OUSStatus getOUSStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		OUSStatus retVal = null;
		try {
			OUSStatusEntityT entityT = new OUSStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getOUSStatus(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
		
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}
	
	@Override
	public String getOUSStatusXml(String id) throws NullEntityIdEx,
			InappropriateEntityTypeEx, NoSuchEntityEx 
	{
		String retVal = null;

		try {
			OUSStatusEntityT entityT = new OUSStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getOUSStatusXml(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}

		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public ProjectStatus getProjectStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		ProjectStatus retVal = null;
		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getProjectStatus(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public XmlEntityStruct[] getProjectStatusList(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx, EntitySerializationFailedEx
	{
		XmlEntityStruct[] retVal = new XmlEntityStruct[0];

		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
			StatusBaseT[] statusArray = stateSystem.getProjectStatusList(entityT);
			if(null != statusArray) {
				retVal = new XmlEntityStruct[statusArray.length];
				int count = 0;
				for(StatusBaseT status : statusArray) {
					try {
						XmlEntityStruct entityStruct = EntitySerializer.getEntitySerializer(logger).serializeEntity(status);
						retVal[count++] = entityStruct;
					} catch (EntityException e) {
						throw new AcsJEntitySerializationFailedEx(e).toEntitySerializationFailedEx();
					}
				}
			} else {
				throw new AcsJNoSuchEntityEx(new RuntimeException("No statuses found for id: " + id)).toNoSuchEntityEx();
			}
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		} catch (AcsJEntitySerializationFailedEx e) {
			throw e.toEntitySerializationFailedEx();
		}
		
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public String getProjectStatusXml(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		String retVal = null;

		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getProjectStatusXml(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}

		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public String[] getProjectStatusXmlList(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		String[] retVal = new String[0];
		
		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
		   retVal = stateSystem.getProjectStatusXmlList(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        		
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public SBStatus getSBStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		SBStatus retVal = null;
		
		try {
			SBStatusEntityT entityT = new SBStatusEntityT();
			entityT.setEntityId(id);
		   retVal = stateSystem.getSBStatus(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        	
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public SBStatus[] getSBStatusListForOUSStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		SBStatus[] retVal = new SBStatus[0];
		
		try {
			OUSStatusEntityT entityT = new OUSStatusEntityT();
			entityT.setEntityId(id);
		   retVal = stateSystem.getSBStatusList(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        
		if(retVal == null) {
			retVal = new SBStatus[0];
		}
		return retVal;
	}

	@Override
	public SBStatus[] getSBStatusListForProjectStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		SBStatus[] retVal = new SBStatus[0];
		
		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getSBStatusList(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        
		if(retVal == null) {
			retVal = new SBStatus[0];
		}
		return retVal;
	}

	@Override
	public String getSBStatusXml(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		String retVal = null;
		
		try {
		   SBStatusEntityT entityT = new SBStatusEntityT();
		   entityT.setEntityId(id);
		   retVal = stateSystem.getSBStatusXml(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        
		if(null == retVal) {
			throw new AcsJNoSuchEntityEx().toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public String[] getSBStatusXmlListForOUSStatus(String id) 
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		String[] retVal = new String[0];
		
		try {
			OUSStatusEntityT entityT = new OUSStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getSBStatusXmlList(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        
		if(retVal == null) {
			retVal = new String[0];
		}
		return retVal;
	}

	@Override
	public String[] getSBStatusXmlListForProjectStatus(String id)
	   throws NullEntityIdEx, InappropriateEntityTypeEx, NoSuchEntityEx
	{
		String[] retVal = new String[0];
		
		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(id);
			retVal = stateSystem.getSBStatusXmlList(entityT);
		} catch (AcsJNullEntityIdEx e) {
			throw e.toNullEntityIdEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		}
        
		if(retVal == null) {
			retVal = new String[0];
		}
		return retVal;
	}
	

	@Override
	public StateChangeData[] 
	   findStateChangeRecords(IDLArrayTime start,
			                  IDLArrayTime end, 
			                  String domainEntityId, 
			                  String state, 
			                  String userId, 
			                  String type) 
        throws StateIOFailedEx 
	{
		StateChangeData[] retVal = new StateChangeData[0];

		// It is not possible to pass nulls in a CORBA function. However,
		// a convention could be used: IDLArrayTime.value needs to be greater than
		// zero and the Strings need to have a non zero length to be passed as non null
		// parameters in the query.
		Date intStartDate = null;
		if (start.value > 0)
			intStartDate = new Date(start.value);
		Date intEndDate = null;
		if (end.value > 0)
		    intEndDate = new Date(end.value);
		String intDomainEntityId = null;
		if (domainEntityId.length() > 0)
		    intDomainEntityId = domainEntityId;
		String intState = null;
		if (state.length() > 0)
		    intState = state;
        String intUserId = null;
        if (userId.length() > 0)
            intUserId = state;
        StateEntityType intType = null;
        if (type.length() > 0)
            intType = StateEntityType.valueOf(type);
        
		try {
			List<StateChangeRecord> records = 
			    stateSystem.findStateChangeRecords(intStartDate, intEndDate, intDomainEntityId,
			            intState, intUserId, intType);
			if(null != records) 
			{
				retVal = new StateChangeData[records.size()];
				int count = 0;
				for(StateChangeRecord record : records) 
				{
					StateChangeData data = new StateChangeData();
					data.id = record.getId();
					if (record.getDomainEntityId() != null) {
						data.domainEntityId = record.getDomainEntityId();
					}
					if (record.getDomainPartId() != null) {
						data.domainPartId = record.getDomainPartId();
					}
					if (record.getDomainEntityState() != null) {
						data.domainEntityState = record.getDomainEntityState();
					}
					if (record.getEntityType() != null) {
						data.entityType = record.getEntityType().toString();
					}
					if (record.getLocation() != null) {
						data.location = record.getLocation();
					}
					if (record.getInfo() != null) {
						data.info = record.getInfo(); // TODO: ask Maurizio if this is correct!
					}
					if (record.getStatusEntityId() != null) {
						data.statusEntityId = record.getStatusEntityId();
					}
					data.timestamp = new IDLArrayTime();
					if (record.getTimestamp() != null) {
						data.timestamp.value = record.getTimestamp().getTime();
					}
					if (record.getUserId() != null) {
						data.userId = record.getUserId();
					}
					retVal[count++] = data;
				}
			} 
		} catch (AcsJStateIOFailedEx e) {
			throw e.toStateIOFailedEx();
		}
		return retVal;
	}
	
	
	// put methods...
	@Override
	public void updateOUSStatus(OUSStatus entity)
	   throws alma.statearchiveexceptions.StateIOFailedEx, alma.statearchiveexceptions.NoSuchEntityEx
	{
		try {
			stateSystem.update(entity);
		} catch (AcsJStateIOFailedEx e) {
			throw e.toStateIOFailedEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		}
	}

	@Override
	public void insert(ProjectStatus opStatus,
			OUSStatus[] ousStatus, SBStatus[] sbStatus)
	   throws StateIOFailedEx, NoSuchEntityEx
	{	
		try {
		   stateSystem.insert(opStatus, ousStatus, sbStatus);
		} catch (AcsJStateIOFailedEx e) {
			throw e.toStateIOFailedEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		}  
	}

	@Override
	public void updateProjectStatus(ProjectStatus entity)
	   throws StateIOFailedEx, NoSuchEntityEx
	{
		try {
		   stateSystem.update(entity);
		} catch (AcsJStateIOFailedEx e) {
			throw e.toStateIOFailedEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} 
	}

	@Override
	public void updateSBStatus(SBStatus entity) 
	   throws StateIOFailedEx, NoSuchEntityEx
	{
		try {
		   stateSystem.update(entity);
		} catch (AcsJStateIOFailedEx e) {
			throw e.toStateIOFailedEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		} 	  
	}
	
	@Override
	public boolean changeSBStatus(String sbStatusId, String destinationState, String subsys, String userId)
	throws NoSuchTransitionEx, NotAuthorizedEx, PreconditionFailedEx, PostconditionFailedEx, IllegalArgumentEx, NoSuchEntityEx
	{
		boolean retVal = false;
		try {
			SBStatusEntityT entityT = new SBStatusEntityT();
			entityT.setEntityId(sbStatusId);
			
			StatusTStateType newState = validateInputArgumentsForStatusChange(sbStatusId, destinationState, subsys, userId);
			
			retVal = stateSystem.changeState(entityT, newState, subsys, userId);
		} catch (AcsJNoSuchTransitionEx e) {
			throw e.toNoSuchTransitionEx();
		} catch (AcsJNotAuthorizedEx e) {
			throw e.toNotAuthorizedEx();
		} catch (AcsJPreconditionFailedEx e) {
			throw e.toPreconditionFailedEx();
		} catch (AcsJPostconditionFailedEx e) {
			throw e.toPostconditionFailedEx();
		} catch (AcsJIllegalArgumentEx e) {
			throw e.toIllegalArgumentEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public boolean changeOUSStatus(String OUSStatusId, 
			String destinationState, String subsys, 
			String userId)
	throws NoSuchTransitionEx, NotAuthorizedEx, PreconditionFailedEx, PostconditionFailedEx, IllegalArgumentEx, NoSuchEntityEx
	{
		boolean retVal = false;

		try {
			OUSStatusEntityT entityT = new OUSStatusEntityT();
			entityT.setEntityId(OUSStatusId);
			StatusTStateType newState = validateInputArgumentsForStatusChange(OUSStatusId, destinationState, subsys, userId);
			retVal = stateSystem.changeState(entityT, newState, subsys, userId);
		} catch (AcsJNoSuchTransitionEx e) {
			throw e.toNoSuchTransitionEx();
		} catch (AcsJNotAuthorizedEx e) {
			throw e.toNotAuthorizedEx();
		} catch (AcsJPreconditionFailedEx e) {
			throw e.toPreconditionFailedEx();
		} catch (AcsJPostconditionFailedEx e) {
			throw e.toPostconditionFailedEx();
		} catch (AcsJIllegalArgumentEx e) {
			throw e.toIllegalArgumentEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		}
		return retVal;
	}

	@Override
	public boolean changeProjectStatus(String projStatusId, String destinationState, String subsys, String userId)
	throws NoSuchTransitionEx, NotAuthorizedEx, PreconditionFailedEx, PostconditionFailedEx, IllegalArgumentEx, NoSuchEntityEx
	{
		boolean retVal = false;
		try {
			ProjectStatusEntityT entityT = new ProjectStatusEntityT();
			entityT.setEntityId(projStatusId);
			StatusTStateType newState = validateInputArgumentsForStatusChange(projStatusId, destinationState, subsys, userId);
			retVal = stateSystem.changeState(entityT, newState, subsys, userId);
		} catch (AcsJNoSuchTransitionEx e) {
			throw e.toNoSuchTransitionEx();
		} catch (AcsJNotAuthorizedEx e) {
			throw e.toNotAuthorizedEx();
		} catch (AcsJPreconditionFailedEx e) {
			throw e.toPreconditionFailedEx();
		} catch (AcsJPostconditionFailedEx e) {
			throw e.toPostconditionFailedEx();
		} catch (AcsJIllegalArgumentEx e) {
			throw e.toIllegalArgumentEx();
		} catch (AcsJNoSuchEntityEx e) {
			throw e.toNoSuchEntityEx();
		}
		return retVal;
	}
	
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states) throws InappropriateEntityTypeEx, IllegalArgumentEx 
	{
		ProjectStatus[] retVal = new ProjectStatus[0];
		
		try {
			retVal = stateSystem.findProjectStatusByState(states);
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		} catch (AcsJIllegalArgumentEx e) {
			throw e.toIllegalArgumentEx();
		}
        
		if(retVal == null) {
			retVal = new ProjectStatus[0];
		}
		return retVal;
	}

	@Override
	public SBStatus[] findSBStatusByState(String[] states) throws InappropriateEntityTypeEx, IllegalArgumentEx  
	{
		SBStatus[] retVal = new SBStatus[0];
		
		try {
			retVal = stateSystem.findSBStatusByState(states);
		} catch (AcsJInappropriateEntityTypeEx e) {
			throw e.toInappropriateEntityTypeEx();
		} catch (AcsJIllegalArgumentEx e) {
			throw e.toIllegalArgumentEx();
		}
        
		if(retVal == null) {
			retVal = new SBStatus[0];
		}
		return retVal;
	}

        public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
            OUSStatus[] retVal = new OUSStatus[0];
        retVal = stateSystem.getOUSStatusList(ousStatusIds);
            if(retVal == null) {
                retVal = new OUSStatus[0];
            }
            return retVal;
        }

	/////////////////////////////////////////////////////////////
	// Implementation of ComponentLifecycle
	/////////////////////////////////////////////////////////////

	public void initialize(ContainerServices containerServices) throws ComponentLifecycleException
	{
		this.containerServices = containerServices;
		logger = containerServices.getLogger();
		
		createStateSystem();
	
		logger.info("RemoteStateArchive component initialized.");
	}

	public void execute() 
	{
		logger.info("RemoteStateArchive component executing.");
	}

	public void cleanUp() 
	{
		logger.info("RemoteStateArchive component cleaned up.");
	}

	public void aboutToAbort() 
	{
		logger.info("RemoteStateArchive component about to abort.");
		cleanUp();
	}

	/////////////////////////////////////////////////////////////
	// Implementation of ACSComponent
	/////////////////////////////////////////////////////////////

	public ComponentStates componentState() 
	{
		return containerServices.getComponentStateManager().getCurrentState();
	}

	public String name() 
	{
		return containerServices.getName();
	}
	
	 // SLH: commenting out these conversion routines (which convert from a corba enumeration
	 //      to a java enum) in case we later revert to enumerated types. If that doesn't happen,
	 //      then this code can (and should) be deleted.
	/* private alma.lifecycle.stateengine.enums.Subsystem convertSubsystemEnum(
			Subsystem subsys) 
	{
		alma.lifecycle.stateengine.enums.Subsystem enumSubsys = null;
		if(subsys.equals(Subsystem.SUBSYSTEM_OBOPS)) {
			enumSubsys = alma.lifecycle.stateengine.enums.Subsystem.ObOps;
		}
		else if(subsys.equals(Subsystem.SUBSYSTEM_OBSPREP)) {
			enumSubsys = alma.lifecycle.stateengine.enums.Subsystem.ObsPrep;
		}
		else if(subsys.equals(Subsystem.SUBSYSTEM_PIPELINE)) {
			enumSubsys = alma.lifecycle.stateengine.enums.Subsystem.Pipeline;
		}
		else if(subsys.equals(Subsystem.SUBSYSTEM_SCHEDULING)) {
			enumSubsys = alma.lifecycle.stateengine.enums.Subsystem.Scheduling;
		}
		return enumSubsys;
	}

	private alma.lifecycle.stateengine.enums.Role convertRoleEnum(Role role) 
	{
		alma.lifecycle.stateengine.enums.Role enumRole = null;
		if(role.equals(Role.ROLE_AOD)) {
			enumRole = alma.lifecycle.stateengine.enums.Role.AoD;
		}
		else if(role.equals(Role.ROLE_ARCA)) {
			enumRole = alma.lifecycle.stateengine.enums.Role.ArcA;
		}
		else if(role.equals(Role.ROLE_AUTO)) {
			enumRole = alma.lifecycle.stateengine.enums.Role.Auto;
		}
		else if(role.equals(Role.ROLE_PI)) {
			enumRole = alma.lifecycle.stateengine.enums.Role.PI;
		}
		else if(role.equals(Role.ROLE_QAA)) {
			enumRole = alma.lifecycle.stateengine.enums.Role.QAA;
		}
		return enumRole;
	}

	private ObsProjectState convertProjectStateEnum(ProjectState projectState) 
	{
		ObsProjectState obsProjState = null;
		if(projectState.equals(ProjectState.OBS_PROJECT_STATE_APPROVED)) {
			obsProjState = ObsProjectState.Approved;
		}
		else if(projectState.equals(ProjectState.OBS_PROJECT_STATE_CANCELED)) {
			obsProjState = ObsProjectState.Canceled;
		}
		else if(projectState.equals(ProjectState.OBS_PROJECT_STATE_PHASE2SUBMITTED)) {
			obsProjState = ObsProjectState.Phase2Submitted;
		}
		else if(projectState.equals(ProjectState.OBS_PROJECT_STATE_READY)) {
			obsProjState = ObsProjectState.Ready;
		}
		else if(projectState.equals(ProjectState.OBS_PROJECT_STATE_REPAIRED)) {
			obsProjState = ObsProjectState.Repaired;
		}
		else if(projectState.equals(ProjectState.OBS_PROJECT_STATE_TOBEREPAIRED)) {
			obsProjState = ObsProjectState.ToBeRepaired;
		}
		return obsProjState;
	}
	
	private alma.lifecycle.stateengine.enums.SchedBlockState convertSchedBlockStateEnum(
			SchedBlockState sbState) 
	{
		alma.lifecycle.stateengine.enums.SchedBlockState enumSbState = null;
		if(sbState.equals(SchedBlockState.SCHED_BLOCK_STATE_BROKEN)) {
			enumSbState = alma.lifecycle.stateengine.enums.SchedBlockState.Broken;
		}
		else if(sbState.equals(SchedBlockState.SCHED_BLOCK_STATE_PARTIALLYOBSERVED)) {
			enumSbState = alma.lifecycle.stateengine.enums.SchedBlockState.PartiallyObserved;
		}
		else if(sbState.equals(SchedBlockState.SCHED_BLOCK_STATE_PHASE2SUBMITTED)) {
			enumSbState = alma.lifecycle.stateengine.enums.SchedBlockState.Phase2Submitted;
		}
		else if(sbState.equals(SchedBlockState.SCHED_BLOCK_STATE_READY)) {
			enumSbState = alma.lifecycle.stateengine.enums.SchedBlockState.Ready;
		}
		else if(sbState.equals(SchedBlockState.SCHED_BLOCK_STATE_RUNNING)) {
			enumSbState = alma.lifecycle.stateengine.enums.SchedBlockState.Running;
		}
		return enumSbState;
	}

	private alma.lifecycle.stateengine.enums.ObsUnitSetState convertObsUnitSetStateEnum(ObsUnitSetState obsUnitSetState) 
	{
		alma.lifecycle.stateengine.enums.ObsUnitSetState obsUnitSetStateEnum = null;
		if(obsUnitSetState.equals(ObsUnitSetState.OBS_UNIT_SET_STATE_BROKEN)) {
			obsUnitSetStateEnum = alma.lifecycle.stateengine.enums.ObsUnitSetState.Broken;
		}
		else if(obsUnitSetState.equals(ObsUnitSetState.OBS_UNIT_SET_STATE_PARTIALLYOBSERVED)) {
			obsUnitSetStateEnum = alma.lifecycle.stateengine.enums.ObsUnitSetState.PartiallyObserved;
		}
		else if(obsUnitSetState.equals(ObsUnitSetState.OBS_UNIT_SET_STATE_PHASE2SUBMITTED)) {
			obsUnitSetStateEnum = alma.lifecycle.stateengine.enums.ObsUnitSetState.Phase2Submitted;
		}
		else if(obsUnitSetState.equals(ObsUnitSetState.OBS_UNIT_SET_STATE_READY)) {
			obsUnitSetStateEnum = alma.lifecycle.stateengine.enums.ObsUnitSetState.Ready;
		}
		else if(obsUnitSetState.equals(ObsUnitSetState.OBS_UNIT_SET_STATE_RUNNING)) {
			obsUnitSetStateEnum = alma.lifecycle.stateengine.enums.ObsUnitSetState.Running;
		}
		return obsUnitSetStateEnum;
	}*/
	
	protected void createStateSystem() throws ComponentLifecycleException
	{		logger.info(this.name() + " instantiating production (not mocked) state archive & engine");
	    if( !StateSystemContextFactory.INSTANCE.isInitialized() ) {
	    	StateArchiveDbConfig dbConfig = null;
            try {
                dbConfig = 
                    new StateArchiveDbConfig(logger );
            }
            catch( Exception e ) {
                e.printStackTrace();
                String msg = this.getClass().getSimpleName()
                        + " is unable to read DB configuration; error was: "
                        + e.getMessage();
                logger.severe( msg );
                this.containerServices.getComponentStateManager()
                        .setState( ComponentStates.COMPSTATE_ERROR );
            }

	        StateSystemContextFactory.INSTANCE.init( STATE_SYSTEM_SPRING_CONFIG, dbConfig );
	    }

	    StateArchive stateArchive =
	        StateSystemContextFactory.INSTANCE .getStateArchive();
	    stateArchive.initStateArchive( logger );
	    StateEngine stateEngine =
	        StateSystemContextFactory.INSTANCE.getStateEngine();
	    RoleProvider roleProvider = new RoleProviderMock(); // TODO: get the "real" role provider, not the mock, when available
	    try {
            stateEngine.initStateEngine( logger, stateArchive, roleProvider );
        }
        catch( Exception e ) {
            logger.severe( "State Engine initialization failed: " + e.getCause().getMessage() );
            this.containerServices
                .getComponentStateManager()
                .setState( ComponentStates.COMPSTATE_ERROR );
        }

	    logger.info( "Revision: $Revision$" );
	    stateSystem = new StateSystemBaseImpl( stateArchive,stateEngine, logger ); 
	}
	
	private StatusTStateType validateInputArgumentsForStatusChange(String statusId, 
			String destinationState, String subsys, String userId)
	   throws IllegalArgumentEx 
	{
		StatusTStateType newState = null;
		try {
			newState = StatusTStateType.valueOf(destinationState);
		} catch(IllegalArgumentException e) {
			AcsJIllegalArgumentEx toThrow = new AcsJIllegalArgumentEx(e);
			toThrow.setVariable("destinationState");
			toThrow.setValue(destinationState);
			toThrow.setErrorDesc("Valid states are: " + StatusTStateType.enumerate());
			throw toThrow.toIllegalArgumentEx();
		}
		// TODO: validate any of the other arguments?
		
		return newState;
	}
}
