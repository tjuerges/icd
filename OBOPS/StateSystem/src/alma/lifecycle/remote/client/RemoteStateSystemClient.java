package alma.lifecycle.remote.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.IllegalArgumentEx;
import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.client.ComponentClient;
import alma.acs.entityutil.EntityDeserializer;
import alma.acs.entityutil.EntityException;
import alma.asdmIDLTypes.IDLArrayTime;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.StateSystem;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.stateengine.RoleProvider;
import alma.lifecycle.stateengine.StateTransitionParam;
import alma.projectlifecycle.StateChangeData;
import alma.projectlifecycle.StateSystemHelper;
import alma.projectlifecycle.StateSystemJ;
import alma.projectlifecycle.StateSystemOperations;
import alma.statearchiveexceptions.EntitySerializationFailedEx;
import alma.statearchiveexceptions.InappropriateEntityTypeEx;
import alma.statearchiveexceptions.NoSuchEntityEx;
import alma.statearchiveexceptions.NullEntityIdEx;
import alma.statearchiveexceptions.StateIOFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.NoSuchTransitionEx;
import alma.stateengineexceptions.NotAuthorizedEx;
import alma.stateengineexceptions.PostconditionFailedEx;
import alma.stateengineexceptions.PreconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * This class is an ACS client which 'talks' with the ACS Component (IDL
 * interface) for the state system. It implements the alma.lifecycle.StateSystem
 * interface for convenience in 'plugging & playing' into generic modules, such
 * as CLI's and so forth.
 * 
 * @author sharring
 * @version $Revision: 1.8 $
 */

// $Id: RemoteStateSystemClient.java,v 1.8 2011/03/09 09:16:02 hsommer Exp $

public class RemoteStateSystemClient 
	extends ComponentClient 
	implements StateSystem{
	
	public static final String NOT_AVAILABLE = "This method is not available";
    private static final String OBOPS_LIFECYCLE_STATE_SYSTEM = "OBOPS_LIFECYCLE_STATE_SYSTEM";
	private static final String STATE_SYSTEM_CLIENT = "StateSystemClient";
	private static final String projectStatusClassName = getShortNameForClass(ProjectStatus.class);
	private static final String ousStatusClassName = getShortNameForClass(OUSStatus.class);
	private static final String sbStatusClassName = getShortNameForClass(SBStatus.class);
	
	private StateSystemJ stateSystemComp;

	/**
     * Constructor.
     * 
     * @param logger
     *            the logger to use in logging informational, debug, error,
     *            warning, and other messages.
     * @param managerLoc
     *            the location of the ACS manager. 
     *            NOTE: you can get this, often, from env vars.
     * @throws Exception
     */
    public RemoteStateSystemClient( Logger logger, String managerLoc )
            throws Exception {
        super( logger, managerLoc, STATE_SYSTEM_CLIENT );
        initStateSystemClient();
    }

	@Override
	public List<StateChangeRecord> findStateChangeRecords(Date start, Date end,
			String domainEntityId, String state, String userId,
			StateEntityType type) throws AcsJStateIOFailedEx 
	{
		List<StateChangeRecord> retVal = new ArrayList<StateChangeRecord>();
		
		IDLArrayTime startArrayTime = new IDLArrayTime(start.getTime());
		IDLArrayTime endArrayTime = new IDLArrayTime(end.getTime());
		
		StateChangeData[] records = null;
		try {
			records = stateSystemComp.findStateChangeRecords(startArrayTime, endArrayTime, domainEntityId, state, userId, type.toString()); 
		} catch (StateIOFailedEx e) {
			throw AcsJStateIOFailedEx.fromStateIOFailedEx(e);
		}
		
		if(null != records) 
		{
			for(StateChangeData record : records) 
			{
				// TODO: verify conversion from/to IDLArrayTime & Date
				StateChangeRecord recordToAdd = 
					new StateChangeRecord(record.domainEntityId,
						record.domainPartId,
                        record.statusEntityId, 
                        record.domainEntityState, 
                        new Date(record.timestamp.value), 
                        record.location,
                        record.userId,
                        record.subsystem,
                        record.info,
                        StateEntityType.valueOf(record.entityType));
				
				retVal.add(recordToAdd);
			}
		}
		
		return retVal;
	}

	@Override
	public OUSStatus getOUSStatus(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		OUSStatus retVal = null;
		
		try {
			retVal = stateSystemComp.getOUSStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public String getOUSStatusXml(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		String retVal = null;
		
		try {
			retVal = stateSystemComp.getOUSStatusXml(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public ProjectStatus getProjectStatus(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		ProjectStatus retVal = null;
		
		try {
			retVal = stateSystemComp.getProjectStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public StatusBaseT[] getProjectStatusList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx 
	{
		StatusBaseT[] retVal = new StatusBaseT[0];
		
		try {
			XmlEntityStruct[] entityStructs = stateSystemComp.getProjectStatusList(id.getEntityId());
			retVal = new StatusBaseT[entityStructs.length];
			int count = 0;
			for(XmlEntityStruct entityStruct : entityStructs) {
				try {
					StatusBaseT status = null;
					
					// BEGIN HACK: the EntityDeserializer is not able to handle inheritance (base classes, child classes); as such, we have to explicitly
					// deserialize for each type of concrete child class of the StatusBaseT base class. This is a bit ugly & fragile, frankly.
					// ideally, all of this code would be a single line such as:
					// status = (StatusBaseT) EntityDeserializer.getEntityDeserializer(m_logger).deserializeEntity(entityStruct, StatusBaseT.class);
					// but alas...
					if(entityStruct.entityTypeName.equals(projectStatusClassName)) {
						status = (StatusBaseT) EntityDeserializer.getEntityDeserializer(m_logger).deserializeEntity(entityStruct, ProjectStatus.class);
					} else if(entityStruct.entityTypeName.equals(sbStatusClassName)) {
						status = (StatusBaseT) EntityDeserializer.getEntityDeserializer(m_logger).deserializeEntity(entityStruct, SBStatus.class);
					} else if(entityStruct.entityTypeName.equals(ousStatusClassName)) {
						status = (StatusBaseT) EntityDeserializer.getEntityDeserializer(m_logger).deserializeEntity(entityStruct, OUSStatus.class);
					} 
					// END HACK
					
					if(null == status) {
						throw new AcsJEntitySerializationFailedEx(new RuntimeException("Could not deserialize status entity")).toEntitySerializationFailedEx();
					}
					retVal[count++] = status;
				} catch (EntityException e) {
					throw new AcsJEntitySerializationFailedEx(e);
				} catch(ClassCastException e) {
					throw new AcsJInappropriateEntityTypeEx(e);
				}
			}

		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		} catch (EntitySerializationFailedEx e) {
			throw AcsJEntitySerializationFailedEx.fromEntitySerializationFailedEx(e);
		}
		
		return retVal;
	}

	@Override
	public String getProjectStatusXml(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		String retVal = null;
		
		try {
			retVal = stateSystemComp.getProjectStatusXml(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public String[] getProjectStatusXmlList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		String[] retVal = null;
		
		try {
			retVal = stateSystemComp.getProjectStatusXmlList(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public SBStatus getSBStatus(SBStatusEntityT id) throws AcsJNullEntityIdEx,
			AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx 
	{
		SBStatus retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public SBStatus[] getSBStatusList(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		SBStatus[] retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatusListForOUSStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public SBStatus[] getSBStatusList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		SBStatus[] retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatusListForProjectStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public String getSBStatusXml(SBStatusEntityT id) throws AcsJNullEntityIdEx,
			AcsJNoSuchEntityEx, AcsJInappropriateEntityTypeEx 
	{
		String retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatusXml(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public String[] getSBStatusXmlList(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		String[] retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatusXmlListForOUSStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public String[] getSBStatusXmlList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx 
	{
		String[] retVal = null;
		
		try {
			retVal = stateSystemComp.getSBStatusXmlListForProjectStatus(id.getEntityId());
		} catch (NullEntityIdEx e) {
			throw AcsJNullEntityIdEx.fromNullEntityIdEx(e);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public void insert(ProjectStatus opStatus, OUSStatus[] ousStatus,
			SBStatus[] sbStatus) throws AcsJStateIOFailedEx
	{
		try {
			stateSystemComp.insert(opStatus, ousStatus, sbStatus);
		} catch (StateIOFailedEx e) {
			throw AcsJStateIOFailedEx.fromStateIOFailedEx(e);
		} catch (NoSuchEntityEx e) {
		}
	}
	
	@Override
	public void update(OUSStatus entity) throws AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx 
	{
		try {
			stateSystemComp.updateOUSStatus(entity);
		} catch (StateIOFailedEx e) {
			throw AcsJStateIOFailedEx.fromStateIOFailedEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
	}

	@Override
	public void update(ProjectStatus entity) throws AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx 
	{
		try {
			stateSystemComp.updateProjectStatus(entity);
		} catch (StateIOFailedEx e) {
			throw AcsJStateIOFailedEx.fromStateIOFailedEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
	}

	@Override
	public void update(SBStatus entity) throws AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx 
	{
		try {
			stateSystemComp.updateSBStatus(entity);
		} catch (StateIOFailedEx e) {
			throw AcsJStateIOFailedEx.fromStateIOFailedEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
	}

	@Override
	public boolean changeState(ProjectStatusEntityT target,
			StatusTStateType destination, String subsystem, String userId) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx
	{
		boolean retVal = false;
		
		try {
			retVal = stateSystemComp.changeProjectStatus(target.getEntityId(), destination.toString(), subsystem, userId);
		} catch (NoSuchTransitionEx e) {
			throw AcsJNoSuchTransitionEx.fromNoSuchTransitionEx(e);
		} catch (NotAuthorizedEx e) {
			throw AcsJNotAuthorizedEx.fromNotAuthorizedEx(e);
		} catch (PreconditionFailedEx e) {
			throw AcsJPreconditionFailedEx.fromPreconditionFailedEx(e);
		} catch (PostconditionFailedEx e) {
			throw AcsJPostconditionFailedEx.fromPostconditionFailedEx(e);
		} catch (IllegalArgumentEx e) {
			throw AcsJIllegalArgumentEx.fromIllegalArgumentEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public boolean changeState(OUSStatusEntityT target,
			StatusTStateType destination, String subsystem, String userId) 
	   throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx
	{
		boolean retVal = false;
		
		try {
			stateSystemComp.changeOUSStatus(target.getEntityId(), destination.toString(), subsystem, userId);
		} catch (NoSuchTransitionEx e) {
			throw AcsJNoSuchTransitionEx.fromNoSuchTransitionEx(e);
		} catch (NotAuthorizedEx e) {
			throw AcsJNotAuthorizedEx.fromNotAuthorizedEx(e);
		} catch (PreconditionFailedEx e) {
			throw AcsJPreconditionFailedEx.fromPreconditionFailedEx(e);
		} catch (PostconditionFailedEx e) {
			throw AcsJPostconditionFailedEx.fromPostconditionFailedEx(e);
		} catch (IllegalArgumentEx e) {
			throw AcsJIllegalArgumentEx.fromIllegalArgumentEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}

	@Override
	public boolean changeState(SBStatusEntityT target,
			StatusTStateType destination, String subsystem, String userId) throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, AcsJIllegalArgumentEx, AcsJNoSuchEntityEx
	{
		boolean retVal = false;
		
		try {
			retVal = stateSystemComp.changeSBStatus(target.getEntityId(), destination.toString(), subsystem, userId);
		} catch (NoSuchTransitionEx e) {
			throw AcsJNoSuchTransitionEx.fromNoSuchTransitionEx(e);
		} catch (NotAuthorizedEx e) {
			throw AcsJNotAuthorizedEx.fromNotAuthorizedEx(e);
		} catch (PreconditionFailedEx e) {
			throw AcsJPreconditionFailedEx.fromPreconditionFailedEx(e);
		} catch (PostconditionFailedEx e) {
			throw AcsJPostconditionFailedEx.fromPostconditionFailedEx(e);
		} catch (IllegalArgumentEx e) {
			throw AcsJIllegalArgumentEx.fromIllegalArgumentEx(e);
		} catch (NoSuchEntityEx e) {
			throw AcsJNoSuchEntityEx.fromNoSuchEntityEx(e);
		}
		
		return retVal;
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@Override
	public StatusBaseT[] getProjectStatusList(ObsProjectEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx 
	{
        StatusBaseT[] retVal = null;
	    // TODO: implement 
		return retVal;	
	}
	

	@Override
	public String getObsProjectStates(String subsystem) {
		String retVal = "";
		retVal = stateSystemComp.getObsProjectStates(subsystem);
		return retVal;
	}

	@Override
	public String getObsUnitSetStates(String subsystem) {
		String retVal = "";
		retVal = stateSystemComp.getObsUnitSetStates(subsystem);
		return retVal;
	}

	@Override
	public String getSchedBlockStates(String subsystem) {
		String retVal = "";
		retVal = stateSystemComp.getSchedBlockStates(subsystem);
		return retVal;
	}
	

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findProjectStatusByState(java.lang.String[])
	 */
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states) 
	   throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx 
	{
		ProjectStatus[] retVal = null;
		
		try {
			retVal = stateSystemComp.findProjectStatusByState(states);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (IllegalArgumentEx e) {
			throw AcsJIllegalArgumentEx.fromIllegalArgumentEx(e);
		}
		
		return retVal;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findSBStatusByState(java.lang.String[])
	 */
	@Override
	public SBStatus[] findSBStatusByState(String[] states) 
	   throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx 
	{
		SBStatus[] retVal = null;
		
		try {
			retVal = stateSystemComp.findSBStatusByState(states);
		} catch (InappropriateEntityTypeEx e) {
			throw AcsJInappropriateEntityTypeEx.fromInappropriateEntityTypeEx(e);
		} catch (IllegalArgumentEx e) {
			throw AcsJIllegalArgumentEx.fromIllegalArgumentEx(e);
		}
		
		return retVal;
	}
	
	/////////////////////////////////////////////////////////////////
	// Methods which are in the java (pojo) interface, but not in the IDL (corba) interface follow...
	/////////////////////////////////////////////////////////////////

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public String getInputUmlFilePath() {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public String getRunLocation() {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public void setInputUmlFilePath( String inputUmlFilePath ) {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public void setRunLocation( String runLocation ) {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public void initStateArchive( Logger logger ) {
        throw new RuntimeException( NOT_AVAILABLE );
    }

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
    @Override
    public void initStateEngine( Logger logr, StateArchive stateArchive, RoleProvider roleProvider ) {
        throw new RuntimeException( NOT_AVAILABLE );
    }
    

    /**
     * @throws RuntimeException
     *             Always: this method is not exposed to remote clients
     */
	@Override
	public void lockProjectStatus(ProjectStatusEntityT projectStatusId)
			throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {

		throw new RuntimeException( NOT_AVAILABLE );
		
	}
	
	/////////////////////////////////////////////////////////////////
	// methods private to this class, not exposed publicly, follow...
	/////////////////////////////////////////////////////////////////
	
	private void initStateSystemClient() throws AcsJContainerServicesEx
	{
		org.omg.CORBA.Object compObj = null; 
		try 
		{
		   // get the component as a 'plain' acs component (corba object)
		   compObj = getContainerServices().getComponent(OBOPS_LIFECYCLE_STATE_SYSTEM);
		   
		   // narrow the component to the proper type
		   alma.projectlifecycle.StateSystem stateSystemComponent = StateSystemHelper.narrow(compObj);

		   // now acquire the 'transparent xml interface' for convenience
		   this.stateSystemComp = getContainerServices().getTransparentXmlWrapper(StateSystemJ.class, stateSystemComponent, StateSystemOperations.class);
		} 
		catch (AcsJContainerServicesEx e) 
		{
			m_logger.severe("RemoteStateSystemClient is unable to acquire state system component");
			throw(e);
		}
	}
	
	private static String getShortNameForClass(Class<?> clazz)
	{
		String retVal = null;
		
		String[] splitString = clazz.getName().split("\\.");
		if(splitString != null && splitString.length > 0) {
			retVal = splitString[splitString.length - 1];
		}
		
		return retVal;
	}

    @Override
    public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
        return new OUSStatus[0];
    }

	@Override
	public void storeArchiveEntities(XmlEntityStruct[] archiveEntities,
			String user, Logger logr) {
		
		throw new RuntimeException( NOT_AVAILABLE );		
	}

	@Override
	public boolean changeState(StateTransitionParam stateTransitionParam)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {

		// TODO 
		throw new RuntimeException( NOT_AVAILABLE );
	}

	@Override
	public void insertOrUpdate(XmlEntityStruct[] archiveEntities,
			String user, ProjectStatus opStatus, OUSStatus[] ousStatuses,
			SBStatus[] sbStatuses, Collection<StateTransitionParam> stateTransitions)
			throws AcsJIllegalArgumentEx, AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx, AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx {

		throw new RuntimeException( NOT_AVAILABLE );	
		
	}


}
