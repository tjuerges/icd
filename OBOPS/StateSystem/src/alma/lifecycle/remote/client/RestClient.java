package alma.lifecycle.remote.client;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response.Status;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.exolab.castor.xml.XMLException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class RestClient {
	private final Client client;

	// hold REST resource locations given our host URL 
	private final String ousStatusUrl;
	private final String projectStatusUrl;
	private final String sbStatusUrl;
	private final String statesUrl;
	
	// REST root resource locations
	private static final String RESOURCES    = "/resources";
	private static final String PROJSTATUSES = "/ProjectStatuses";
	private static final String OUSSTATUSES  = "/OUSStatuses";
	private static final String SBSTATUSES   = "/SBStatuses";
	private static final String STATES       = "/states";

	private static final Logger logger = Logger.getLogger(RestClient.class.getName());	
	private static final Transformer transformer;
    private static final DocumentBuilder builder;
    
    static {
    	// create shared XML transformer
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException ex) {
            logger.log(Level.SEVERE, "Could not create Transformer", ex);
            throw new RuntimeException(ex);
        }

        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
        	logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
	
	/**
	 * Create a new StateSystem web service client. The client will target a
	 * REST service at the given URL.
	 * 
	 * @param restUrl
	 *            the URL of the target web service
	 */
	public RestClient(String restUrl) {
		ClientConfig clientConfig = new DefaultClientConfig();
		client = Client.create(clientConfig);
	
		projectStatusUrl = restUrl + RESOURCES + PROJSTATUSES + "/";
		ousStatusUrl     = restUrl + RESOURCES + OUSSTATUSES  + "/";
		sbStatusUrl      = restUrl + RESOURCES + SBSTATUSES   + "/";
		statesUrl        = restUrl + RESOURCES + STATES       + "/";
	}

	/**
	 * Request the State Engine to change the state of the target ObsProject.
	 * 
	 * @param target
	 *            Reference to the target ObsProject, whose state should be
	 *            changed
	 * 
	 * @param destination
	 *            The state to be set, if possible
	 * 
	 * @param subsystem
	 *            The subsystem requesting the state change
	 * 
	 * @param userId
	 *            ID of the user requesting the state change, to be written into
	 *            the state history for the target object. It will not be
	 *            verified: it is the responsibility of the caller to
	 *            authenticate the user.
	 * 
	 * @param role
	 *            Role of the user requesting the state change; it will not be
	 *            verified. It is the responsibility of the caller to make sure
	 *            the user has this role.
	 * @return True if the state change succeeded
	 * 
	 * @throws AcsJNoSuchTransitionEx
	 *             There is no transition from the target's current state to the
	 *             destination state.
	 * 
	 * @throws AcsJNotAuthorizedEx
	 *             State transition is not authorized, because either the
	 *             invoking subsystem or the provided role are not valid.
	 * 
	 * @throws AcsJPreconditionFailedEx
	 *             The transition's guard failed.
	 * 
	 * @throws AcsJPostconditionFailedEx
	 *             If the transition's effect failed.
	 */
	public boolean changeState(ProjectStatusEntityT target, StatusTStateType destination, String subsystem, String userId) 
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, Exception {
		if (target == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null target"));
		}

		ChangeStateRestRequest request = getChangeStateRequest(destination, subsystem, userId);
		String url = projectStatusUrl + transformEntityId(target.getEntityId());
		return doPut(request, url);
	}
	
	
	public boolean changeState(OUSStatusEntityT target, StatusTStateType destination, String subsystem, String userId)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, Exception {
		if (target == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null target"));
		}

		ChangeStateRestRequest request = getChangeStateRequest(destination, subsystem, userId);
		String url = ousStatusUrl + transformEntityId(target.getEntityId());
		return doPut(request, url);
	}


	public boolean changeState(SBStatusEntityT target, StatusTStateType destination, String subsystem, String userId)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx, AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx, Exception {
		if (target == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null target"));
		}

		ChangeStateRestRequest request = getChangeStateRequest(destination, subsystem, userId);
		String url = sbStatusUrl + transformEntityId(target.getEntityId());
		return doPut(request, url);
	}

	
	private boolean doPut(ChangeStateRestRequest request, String url) throws AcsJNoSuchTransitionEx, 
					AcsJNoSuchEntityEx, AcsJNotAuthorizedEx, Exception {
//		// create new client as don't want to attach authentication headers to
//		// shared instance
//		ClientConfig clientConfig = new DefaultClientConfig();
//		Client authClient = Client.create(clientConfig);
//		HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(userId, "password");
//		authClient.addFilter(authFilter);

		WebResource wr = client.resource(url);
		ClientResponse response = wr.put(ClientResponse.class, request);

		if (response.getStatus() == Status.OK.getStatusCode()) {
			return true;
		}
		if (response.getStatus() == Status.CONFLICT.getStatusCode()) {
			throw new AcsJNoSuchTransitionEx();
		}
		if (response.getStatus() == Status.NOT_FOUND.getStatusCode()) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Entity could not be found"));
		}		
		if (response.getStatus() == Status.UNAUTHORIZED.getStatusCode()) {
			throw new AcsJNotAuthorizedEx();
		}
		if (response.getStatus() == Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
			//  ACS may not be running or container not initialized.
			throw new Exception("Internal server error");
		}		
		if (response.getStatus() == Status.BAD_REQUEST.getStatusCode()) {
			return false;
		}
		
		return false;
	}
	
	
	private ChangeStateRestRequest getChangeStateRequest(
			StatusTStateType destination, String subsystem, String userId) {
		ChangeStateRestRequest request = new ChangeStateRestRequest();
		request.setState(destination);
		request.setSubsystem(subsystem);
		request.setUserId(userId);
		return request;		
	}

	
	/**
	 * Retrieve the ProjectStatus entity with the given ID.
	 * 
	 * @param id
	 *            the id of the ProjectStatus entity to look for
	 * @return the required ProjectStatus, if one was found
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             un-marshaled
	 */
	public ProjectStatus getProjectStatus(ProjectStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		return getProjectStatus(transformEntityId(id.getEntityId()));
	}

	
	/**
	 * Retrieve the OUSStatus entity with the given ID.
	 * 
	 * @param id
	 *            the id of the OUSStatus entity to look for
	 * @return the required OUSStatus, if one was found
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             un-marshaled
	 */
	public OUSStatus getOUSStatus(OUSStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		return getOUSStatus(transformEntityId(id.getEntityId()));		
	}

	/**
	 * Look for all the leaf OUSStatus entities of a ProjectStatus; that is, all
	 * the OUSStatus entities that are either directly referenced by the
	 * ProjectStatus entity or by one of its descendant OUSStatus entities.
	 * 
	 * @param id
	 *            The entity ID of the root ProjectStatus entity.
	 * @return The list of the required OUSStatus entities, if any were found.
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the search
	 *             criteria,or the XML text in the Archive could not be
	 *             un-marshaled
	 */
	public List<OUSStatus> getOUSStatuses(ProjectStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		String resourceUrl = transformEntityId(id.getEntityId());
		String url = projectStatusUrl + resourceUrl + OUSSTATUSES;		
		
		ClientResponse response = doGet(url);
		Document doc = getDocumentFromResponse(response);
        NodeList nodes = doc.getElementsByTagName("OUSStatus");

		List<OUSStatus> result = new ArrayList<OUSStatus>();
        for (int i = 0; i<nodes.getLength(); i++) {
        	Node node = nodes.item(i);
        	OUSStatus ousStatus;
			try {
				ousStatus = unmarshalOUSStatus(node);
			} catch (XMLException e) {
				throw new AcsJNoSuchEntityEx(e);
			}
        	result.add(ousStatus);
        }
        
        return result;
	}	

	
	/**
	 * Look for the SBStatus entity with the given ID.
	 * 
	 * @param id
	 *            the id of the entity to look for
	 * @return the required SBStatus, if one was found
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the supplied
	 *             argument, or the XML text in the Archive could not be
	 *             un-marshaled
	 */
	public SBStatus getSBStatus(SBStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		return getSBStatus(transformEntityId(id.getEntityId()));
	}

	
	/**
	 * Retrieve all leaf SBStatuses of an OUSStatus; that is, all the SBStatus
	 * entities that are either directly referenced by the OUSStatus entity or
	 * by one of its descendant OUSStatus entities.
	 * 
	 * @param id
	 *            The entity ID of the root OUSStatus entity.
	 * @return The list of the required status entities, if any were found.
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the search
	 *             criteria,or the XML text in the Archive could not be
	 *             un-marshaled
	 */
	public List<SBStatus> getSBStatuses(OUSStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		String resourceUrl = transformEntityId(id.getEntityId());
		String url = ousStatusUrl + resourceUrl + SBSTATUSES;		
		List<SBStatus> result = new ArrayList<SBStatus>();
		
		ClientResponse response = doGet(url);
		
		Document doc = getDocumentFromResponse(response);
        NodeList nodes = doc.getElementsByTagName("SBStatus");

        for (int i = 0; i<nodes.getLength(); i++) {
        	Node node = nodes.item(i);
        	SBStatus sbStatus;
			try {
				sbStatus = unmarshalSBStatus(node);
			} catch (XMLException e) {
				throw new AcsJNoSuchEntityEx(e);
			}
        	result.add(sbStatus);
        }
        
        return result;
	}

	
	/**
	 * Retrieve all leaf SBStatuses of a ProjectStatus; that is, all the
	 * SBStatus entities that are either directly referenced by one of the
	 * descendant OUSStatus entities.
	 * 
	 * @param id
	 *            The entity ID of the root ProjectStatus entity.
	 * @return The list of child SBStatus entities, if any were found.
	 * @throws AcsJNoSuchEntityEx
	 *             if there is no entity in the archive matching the search
	 *             criteria
	 */
	public List<SBStatus> getSBStatuses(ProjectStatusEntityT id) throws AcsJNoSuchEntityEx {
		if (id == null) {
			throw new AcsJNoSuchEntityEx(new IllegalArgumentException("Null entity"));
		}
		String resourceUrl = transformEntityId(id.getEntityId());
		String url = projectStatusUrl + resourceUrl + SBSTATUSES;		
		
		ClientResponse response = doGet(url);
		Document doc = getDocumentFromResponse(response);
        NodeList nodes = doc.getElementsByTagName("SBStatus");

		List<SBStatus> result = new ArrayList<SBStatus>();
        for (int i = 0; i<nodes.getLength(); i++) {
        	Node node = nodes.item(i);

        	String xmlString = getStringFromNode(node);
            StringReader reader = new StringReader(xmlString);
        	try {
        		SBStatus sbStatus = SBStatus.unmarshalSBStatus(reader);
        		result.add(sbStatus);
			} catch (XMLException e) {
				e.printStackTrace();
			}
        }
        
        return result;
	}

	
    /**
     * Store the input ProjectStatus entity into the Archive. The entity will be
     * updated; it should be already present.
     * 
     * @param projectStatus
     *            The ProjectStatus entity to be stored.
     * @throws AcsJStateIOFailedEx
     *             If anything went wrong while storing the entity
     * @throws AcsJNoSuchEntityEx
     *             If the input entity was not found in the State Archive
     */
	public void update(ProjectStatus projectStatus) throws AcsJNoSuchEntityEx, AcsJStateIOFailedEx {
		update(projectStatus, projectStatusUrl);
	}
	
	
	/**
	 * Store the input OUSStatus entity into the Archive. The entity will be
	 * updated; it should be already present.
	 * 
	 * @param ousStatus
	 *            The OUSStatus entity to be stored.
	 * @throws AcsJStateIOFailedEx
	 *             If anything went wrong while storing the entity
	 * @throws AcsJNoSuchEntityEx
	 *             If the input entity was not found in the State Archive
	 */
	public void update(OUSStatus ousStatus) throws AcsJNoSuchEntityEx, AcsJStateIOFailedEx {
		update(ousStatus, ousStatusUrl);
	}

	/**
	 * Store the input SBStatus entity into the Archive. The entity will be
	 * updated; it should be already present.
	 * 
	 * @param sbStatus
	 *            The SBStatus entity to be stored.
	 * 
	 * @throws AcsJStateIOFailedEx
	 *             If anything went wrong while storing the entity
	 * 
	 * @throws AcsJNoSuchEntityEx
	 *             If the input entity was not found in the State Archive
	 */
	public void update(SBStatus sbStatus) throws AcsJNoSuchEntityEx, AcsJStateIOFailedEx {
		update(sbStatus, sbStatusUrl);
	}

	
	/**
	 * POSTs a marshalled entity to the given URL
	 * 
	 * @param entity
	 *            the entity to unmarshall
	 * @param url
	 *            the URL to post to
	 * @throws AcsJNoSuchEntityEx
	 *             if the entity does no exist
	 * @throws AcsJStateIOFailedEx
	 *             if the entity could not be committed
	 */
	private void update(StatusBaseT entity, String url) throws AcsJNoSuchEntityEx, AcsJStateIOFailedEx {
		String xmlString = getStringFromEntity(entity);
		WebResource wr = client.resource(url);
		ClientResponse response = wr.post(ClientResponse.class, xmlString);	

		int statusCode = response.getStatus();
		
		if (statusCode == Status.CREATED.getStatusCode()) {
			return;
		} else if (statusCode == Status.NOT_FOUND.getStatusCode()) {
			throw new AcsJNoSuchEntityEx();
		} else if (statusCode == Status.CONFLICT.getStatusCode()) {
			throw new AcsJStateIOFailedEx();
		} else if (statusCode == Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
			throw new AcsJStateIOFailedEx();
		} else {
			throw new RuntimeException("Unknown completion state; server error suspected");
		}
	}

	
	/**
	 * Returns a String containing the XML representation of the given entity.
	 * 
	 * @param entity
	 *            the entity to unmarshal
	 * @return the String/XML representation of the given entity
	 */
	private String getStringFromEntity(StatusBaseT entity) {
		StringWriter writer = new StringWriter();
		try {
			entity.marshal(writer);
		} catch (XMLException e) {
			logger.log(Level.SEVERE, "Could not marshal entity");
			throw new RuntimeException("Could not marshal entity", e);
		}
		return writer.toString();
	}
	
	
	/**
	 * Construct a Document from the given web service response.
	 * 
	 * @param response
	 *            the web service response
	 * @return a Document extracted from the given response
	 * @throws AcsJNoSuchEntityEx 
	 *             if an error occurs while unmarshalling the XML
	 */
	private Document getDocumentFromResponse(ClientResponse response) throws AcsJNoSuchEntityEx {
        InputSource is = new InputSource(new StringReader(response.getEntity(String.class)));
        Document doc;
		try {
			doc = builder.parse(is);
		} catch (IOException e) {
			// We shouldn't get IO errors on a StringReader, so this would be a
			// fatal exception
			logger.log(Level.SEVERE, "IO errors on StringReader", e);
			throw new RuntimeException(e);
		} catch (SAXException e) {
			logger.log(Level.WARNING, "Web service is returning invalid XML", e);
			throw new AcsJNoSuchEntityEx(e);
		}
        return doc;
	}

	
	/**
	 * Return a String representation of the given Node
	 * 
	 * @param node
	 *            the Node to transform
	 * @return the String representation
	 */
	private String getStringFromNode(Node node) {
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(node);
        try {
            transformer.transform(source, result);
        } catch (TransformerException ex) {
        	logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
		return writer.toString();
	}

	
	/**
	 * Performs a HTTP GET of the given REST resource.
	 * 
	 * @param url
	 *            the URL to request
	 * @return the ClientResponse found at the given URL
	 * @throws AcsJNoSuchEntityEx
	 *             if the REST resource cannot be found or an error occurred
	 *             while retrieving the resource
	 */
	private ClientResponse doGet(String url) throws AcsJNoSuchEntityEx {
		WebResource wr = client.resource(url);
		ClientResponse response = wr.accept("application/xml").get(ClientResponse.class);		

		if (response.getResponseStatus() == Status.OK) {
			return response;
		} else if (response.getResponseStatus() == Status.NOT_FOUND) {
			throw new AcsJNoSuchEntityEx(url + " not found");
		} else {
			throw new AcsJNoSuchEntityEx("Unexpected error occurred while retrieving " + url);
		}
	}
	
	
	/**
	 * Unmarshals a ProjectStatus entity from a String containing XML.
	 * 
	 * @param xmlString
	 *            the marshaled ProjectStatus representation
	 * @return the unmarshaled ProjectStatus entity
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private ProjectStatus unmarshalProjectStatus(String xmlString) throws MarshalException, ValidationException {
        StringReader reader = new StringReader(xmlString);
        return ProjectStatus.unmarshalProjectStatus(reader);
	}

	
	/**
	 * Unmarshals an SBStatus entity from a String containing XML.
	 * 
	 * @param xmlString
	 *            the marshaled SBStatus representation
	 * @return the unmarshaled SBStatus entity
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private SBStatus unmarshalSBStatus(String xmlString) throws MarshalException, ValidationException {
        StringReader reader = new StringReader(xmlString);
        return SBStatus.unmarshalSBStatus(reader);
	}
	
	
	/**
	 * Unmarshals an OUSStatus entity from a String containing XML.
	 * 
	 * @param xmlString
	 *            the marshaled OUSStatus representation
	 * @return the unmarshaled OUSStatus entity
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private OUSStatus unmarshalOUSStatus(String xmlString) throws MarshalException, ValidationException {
        StringReader reader = new StringReader(xmlString);        
    	return OUSStatus.unmarshalOUSStatus(reader);
	}

	
	/**
	 * Unmarshals an OUSStatus entity from an XML Node.
	 * @param node the Node containing the marshaled OUSStatus
	 * @return the unmarshaled ProjectStatus entity
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private OUSStatus unmarshalOUSStatus(Node node) throws MarshalException, ValidationException {
		String xmlString = getStringFromNode(node);
		return unmarshalOUSStatus(xmlString);
	}

	
	/**
	 * Unmarshals an SBStatus entity from an XML Node.
	 * @param node the Node containing the marshaled SBStatus
	 * @return the unmarshaled SBStatus entity
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	private SBStatus unmarshalSBStatus(Node node) throws MarshalException, ValidationException {
		String xmlString = getStringFromNode(node);
		return unmarshalSBStatus(xmlString);		
	}

	
	/**
	 * Returns the ProjectStatus from the REST resource with the given UID
	 * 
	 * @param uid
	 *            the resource UID of the entity to retrieve
	 * @return the ProjectStatus with the given resource UID
	 * @throws AcsJNoSuchEntityEx
	 *             if no ProjectStatus could be found with the given REST UID or
	 *             an error occurred while unmarshalling the web service
	 *             response.
	 */
	private ProjectStatus getProjectStatus(String uid) throws AcsJNoSuchEntityEx {
		String url = projectStatusUrl + uid;				
		ClientResponse response = doGet(url);
		
		String textEntity = response.getEntity(String.class);
		try {
			return unmarshalProjectStatus(textEntity);
		} catch (XMLException e) {
			throw new AcsJNoSuchEntityEx(e);
		}
	}

	
	/**
	 * Returns the OUSStatus from the REST resource with the given UID
	 * 
	 * @param uid
	 *            the resource UID of the entity to retrieve
	 * @return the OUSStatus with the given resource UID
	 * @throws AcsJNoSuchEntityEx
	 *             if no OUSStatus could be found with the given REST UID or an
	 *             error occurred while unmarshalling the web service response.
	 */
	private OUSStatus getOUSStatus(String uid) throws AcsJNoSuchEntityEx {
		String url = ousStatusUrl + uid;
		ClientResponse response = doGet(url);
		
		String textEntity = response.getEntity(String.class);
		try {
			return unmarshalOUSStatus(textEntity);
		} catch (XMLException e) {
			throw new AcsJNoSuchEntityEx(e);
		}
	}
	

	/**
	 * Returns the SBStatus from the REST resource with the given UID
	 * 
	 * @param uid
	 *            the resource UID of the entity to retrieve
	 * @return the SBStatus with the given resource UID
	 * @throws AcsJNoSuchEntityEx
	 *             if no SBStatus could be found with the given REST UID or an
	 *             error occurred while unmarshalling the web service response.
	 */
	private SBStatus getSBStatus(String uid) throws AcsJNoSuchEntityEx {
		String url = sbStatusUrl + uid;		
		ClientResponse response = doGet(url);
		
		String textEntity = response.getEntity(String.class);
		try {
			return unmarshalSBStatus(textEntity);
		} catch (XMLException e) {
			throw new AcsJNoSuchEntityEx(e);
		}
	}

	
	/**
	 * Returns the REST UID corresponding to the given ALMA UID.
	 * 
	 * @param id
	 *            the ALMA-format UID
	 * @return the REST resource UID
	 */
	private String transformEntityId(String id) {
		return id.substring(6).replaceAll("/", "-");
	}
	
	
	/**
	 * Returns a delimited string of states in the obsProject life-cycle for the
	 * given subsystem
	 * 
	 * e.g. sourceState1:targetState1,targetState2;sourceState2:targetState3,
	 * targetState4
	 * 
	 * @param subsystem
	 * @return the states available for the given subsystem
	 * 
	 */
	public String getObsProjectStates(String subsystem) {
		if (subsystem == null) {
			return "";
		}
		String url = statesUrl + subsystem + "/ObsProject";
		return getState(url);
	}

	/**
	 * Returns a delimited string of states in the obsUnitSet life-cycle for the
	 * given subsystem e.g.
	 * sourceState1:targetState1,targetState2;sourceState2:targetState3
	 * ,targetState4
	 * 
	 * @param subsystem
	 * @return the states available for the given subsystem
	 */
	public String getObsUnitSetStates(String subsystem) {
		if (subsystem == null) {
			return "";
		}
		String url = statesUrl + subsystem + "/ObsUnitSet";
		return getState(url);
	}

	
	/**
	 * Returns a delimited string of states in the SchedBlock life-cycle for the
	 * given subsystem e.g.
	 * sourceState1:targetState1,targetState2;sourceState2:targetState3
	 * ,targetState4
	 * 
	 * @param subsystem
	 * @return the states available for the given subsystem
	 */
	public String getSchedBlockStates(String subsystem) {
		if (subsystem == null) {
			return "";
		}
		String url = statesUrl + subsystem + "/SchedBlock";
		return getState(url);
	}

	
	private String getState(String url) {
		WebResource wr = client.resource(url);
		ClientResponse response = wr.accept("text/plain").get(ClientResponse.class);

		if (response.getResponseStatus() != Status.OK) {
			return "";
		}

		return response.getEntity(String.class);
	}
	
	public static void main(String[] args) throws Exception {
		RestClient rc = new RestClient("http://localhost:8180/StateSystem");
		System.out.println(rc.getObsProjectStates("obops"));
		System.out.println(rc.getObsProjectStates("rhubarb"));
		
//		System.out.println(rc.getOUSStatus("X00-X4-X5"));
//		System.out.println(rc.getOUSStatus("X00-X4-X1"));
		
//		ProjectStatusEntityT pset = new ProjectStatusEntityT();
//		pset.setEntityId("uid://X00/X4/X1");
//		System.out.println(rc.getSBStatuses(pset));
//		System.out.println(rc.getOUSStatuses(pset));
//
//		for (OUSStatus ousStatus : rc.getOUSStatuses(pset)) {
//			System.out.println(rc.getSBStatuses(ousStatus.getOUSStatusEntity()));
//		}
		
	}
}
