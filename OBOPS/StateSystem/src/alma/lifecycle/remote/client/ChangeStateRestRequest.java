package alma.lifecycle.remote.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ChangeStateRestRequest")
public class ChangeStateRestRequest {
	private String userId;
	private String state;
	private String subsystem;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public StatusTStateType getState() {
		return StatusTStateType.valueOf(state);
	}

	public void setState(StatusTStateType state) {
		this.state = state.toString();
	}

	public String getSubsystem() {
		return subsystem;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}
}
