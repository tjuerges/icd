/**
 * StateSystemBaseImpl.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entities.commonentity.EntityT;
import alma.entity.xmlbinding.LoggingHelper;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.stateengine.RoleProvider;
import alma.lifecycle.stateengine.StateEngine;
import alma.lifecycle.stateengine.StateTransitionParam;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * A basic implementation of the State System, delegating actual work
 * to other implementing classes.
 *
 * @author amchavan, Jul 9, 2009
 * @version $Revision: 1.7 $
 */

// $Id: StateSystemBaseImpl.java,v 1.7 2011/02/03 10:28:56 rkurowsk Exp $

public class StateSystemBaseImpl implements StateSystem {

	Logger logger;
	private static final String BEFORE = "BEFORE";
	private static final String AFTER = "AFTER";
	
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getOUSStatus(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    public OUSStatus getOUSStatus( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getOUSStatus( id );
    }

    /** We delegate all State Archive operations to this object */
    private StateArchive stateArchive;

    /** We delegate all State Engine operations to this object */
    private StateEngine stateEngine;

    /**
     * Constructor; we need classes to delegate real work to.
     */
    public StateSystemBaseImpl( StateArchive sarchive, StateEngine sengine, Logger logger ) {
        this.stateArchive = sarchive;
        this.stateEngine = sengine;
        this.logger = logger;
    }
    
    /**
     * {@inheritDoc}
     * @throws AcsJIllegalArgumentEx 
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.stateengine.StateEngine#changeState(OUSStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( OUSStatusEntityT target,
                                StatusTStateType destination, String subsystem,
                                String userId)
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
            AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
    	
    	logParams(target, destination, subsystem, userId);
    	
    	logFullProjectStatusTree(target, BEFORE);
    	
        boolean result = stateEngine.changeState( target, destination, subsystem, userId );
        
        logFullProjectStatusTree(target, AFTER);
        
        return result;
    }

    
    /**
     * {@inheritDoc}
     * @throws AcsJIllegalArgumentEx 
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.stateengine.StateEngine#changeState(ProjectStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( ProjectStatusEntityT target,
                                StatusTStateType destination, String subsystem,
                                String userId)
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
            AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
    	
    	logParams(target, destination, subsystem, userId);
    	
    	logFullProjectStatusTree(target, BEFORE);
    	
        boolean result = stateEngine.changeState( target, destination, subsystem, userId );
        
        logFullProjectStatusTree(target, AFTER);
    	
        return result;
        
    }

    /**
     * {@inheritDoc}
     * @throws AcsJIllegalArgumentEx 
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.stateengine.StateEngine#changeState(SBStatusEntityT, StatusTStateType, String, String)
     */
    @Override
    public boolean changeState( SBStatusEntityT target, StatusTStateType destination,
                                String subsystem, String userId)
            throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
            AcsJPreconditionFailedEx, AcsJPostconditionFailedEx, 
            AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
    	
    	logParams(target, destination, subsystem, userId);
    	
    	logFullProjectStatusTree(target, BEFORE);
    	
        boolean result = stateEngine.changeState( target, destination, subsystem, userId );
        
        logFullProjectStatusTree(target, AFTER);
    	
    	return result;
    }
    
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#findStateChangeRecords(java.util.Date, java.util.Date, java.lang.String, java.lang.String, java.lang.String, alma.lifecycle.persistence.domain.StateEntityType)
     */
    public List<StateChangeRecord> findStateChangeRecords( Date start,
                                                           Date end,
                                                           String domainEntityId,
                                                           String state,
                                                           String userId,
                                                           StateEntityType type )
            throws AcsJStateIOFailedEx {
        return stateArchive.findStateChangeRecords( start, end, domainEntityId,
                                                    state, userId, type );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getOUSStatusXml(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public String getOUSStatusXml( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getOUSStatusXml( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public ProjectStatus getProjectStatus( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getProjectStatus( id );
    }

    /**
     * @throws AcsJEntitySerializationFailedEx 
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public StatusBaseT[] getProjectStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx 
    {
        return stateArchive.getProjectStatusList( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXml(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String getProjectStatusXml( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getProjectStatusXml( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String[] getProjectStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getProjectStatusXmlList( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatus(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)
     */
    @Override
    public SBStatus getSBStatus( SBStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatus( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public SBStatus[] getSBStatusList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatusList( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public SBStatus[] getSBStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatusList( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXml(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)
     */
    @Override
    public String getSBStatusXml( SBStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatusXml( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public String[] getSBStatusXmlList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatusXmlList( id );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String[] getSBStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        return stateArchive.getSBStatusXmlList( id );
    }

    /**
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.ousstatus.OUSStatus)
     */
    @Override
    public void update( OUSStatus entity ) throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        stateArchive.update( entity );
    }

    /**
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.projectstatus.ProjectStatus)
     */
    @Override
    public void update( ProjectStatus entity ) throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        stateArchive.update( entity );
    }

    /**
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.ousstatus.OUSStatus[], alma.entity.xmlbinding.sbstatus.SBStatus[])
     */
    @Override
    public void insert( ProjectStatus opStatus, OUSStatus[] ousStatus,
                     SBStatus[] sbStatus ) throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        stateArchive.insert( opStatus, ousStatus, sbStatus );
    }

    /**
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.sbstatus.SBStatus)
     */
    @Override
    public void update( SBStatus entity ) throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {
        stateArchive.update( entity );
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.lifecycle.persistence.domain.StateChangeRecord)
     */
    @Override
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx {
        stateArchive.insert( record );
    }

	/**
	 * @see StateEngine#initStateEngine(Logger, StateArchive, RoleProvider)
	 */
	@Override
	public void initStateEngine(Logger logr, StateArchive stateArchive, RoleProvider roleProvider) {
		
		stateEngine.initStateEngine(logr, stateArchive, roleProvider);
		
	}

	/**
	 * @see StateArchive#initStateArchive(Logger)
	 */
	@Override
	public void initStateArchive(Logger logger) {
		stateArchive.initStateArchive(logger);
	}

	/**
	 * @see alma.lifecycle.stateengine.StateEngine#getInputUmlFilePath()
	 */
	@Override
	public String getInputUmlFilePath() {
		return this.stateEngine.getInputUmlFilePath();
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#getRunLocation()
	 */
	@Override
	public String getRunLocation() {
		return this.stateEngine.getRunLocation();
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#setInputUmlFilePath(java.lang.String)
	 */
	@Override
	public void setInputUmlFilePath(String inputUmlFilePath) {
		this.stateEngine.setInputUmlFilePath(inputUmlFilePath);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#setRunLocation(java.lang.String)
	 */
	@Override
	public void setRunLocation(String runLocation) {
		this.stateEngine.setRunLocation(runLocation);
	}
	
	/*
	 * This method is only used for logging convenience, hence the catch all
	 */
	private StatusBaseT[] getProjectStatusTree(EntityT target){
		
		StatusBaseT[] projectStatusList = null;
		ProjectStatusEntityT psEntity = null;
		
		try{
	        if( target instanceof ProjectStatusEntityT ) {
	        	
	        	psEntity = (ProjectStatusEntityT)target;
	        	
	        }else if( target instanceof OUSStatusEntityT ) {
	        	
	        	OUSStatusEntityT ousStatusEntityT = (OUSStatusEntityT)target;
				OUSStatus ous = getOUSStatus(ousStatusEntityT);
				if(ous != null){
					psEntity = new ProjectStatusEntityT();
					psEntity.setEntityId(ous.getProjectStatusRef().getEntityId());
				}
				
	        }else if( target instanceof SBStatusEntityT ) {
	        	
	        	SBStatusEntityT sbStatusEntityT = (SBStatusEntityT)target;
				SBStatus sbs = getSBStatus(sbStatusEntityT);
				if(sbs != null){
					psEntity = new ProjectStatusEntityT();
					psEntity.setEntityId(sbs.getProjectStatusRef().getEntityId());
				}
				
	        }else {
	            throw new RuntimeException( "Invalid entity type: " 
	            		+ target.getClass().getCanonicalName());
	        }
			
	        if(psEntity != null){
	        	projectStatusList = getProjectStatusList(psEntity);
	        }
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return projectStatusList;
	}
	

    /*
     * Output the changeState params
     */
    private void logParams(EntityT target,
            StatusTStateType destination, String subsystem,
            String userId){
    	
    	//TODO: change to fine once out of testing
    	if(logger.isLoggable(Level.INFO)){
	    	logger.info("ChangeState Params [" +  target.getEntityTypeName() + ": " + target.getEntityId() + "]"
					+ "[targetState:"  + destination.toString() + "]"
					+ "[subsystem:" + subsystem + "]"
					+ "[userId:" + userId + "]");
    	}
    }
    
    /*
     * Output the changeState params and the whole project status tree
     */
    private void logFullProjectStatusTree(EntityT target, String when){

    	//TODO: change to fine once out of testing
    	if(logger.isLoggable(Level.INFO)){
			logger.info("ProjectStatus tree " + when + " changeState executed \n\n"
					+ LoggingHelper.getProjectStatusTreeForDisplay(
							getProjectStatusTree(target)));
    	}
    }

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@Override
	public StatusBaseT[] getProjectStatusList(ObsProjectEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx {
		
		return stateArchive.getProjectStatusList(id);
		
	}


	@Override
	public String getObsProjectStates(String subsystem) {
		return stateEngine.getObsProjectStates(subsystem);
	}

	@Override
	public String getObsUnitSetStates(String subsystem) {
		return stateEngine.getObsUnitSetStates(subsystem);
	}

	@Override
	public String getSchedBlockStates(String subsystem) {
		return stateEngine.getSchedBlockStates(subsystem);
	}

	@Override
	public void lockProjectStatus(ProjectStatusEntityT projectStatusId)
			throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
		stateArchive.lockProjectStatus(projectStatusId);
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findProjectStatusByState(java.lang.String[])
	 */
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states)
		throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		return stateArchive.findProjectStatusByState(states);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findSBStatusByState(java.lang.String[])
	 */
	@Override
	public SBStatus[] findSBStatusByState(String[] states)
		throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		return stateArchive.findSBStatusByState(states);
	}
    
    @Override
    public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
        return stateArchive.getOUSStatusList(ousStatusIds);
    }

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#changeState(alma.lifecycle.stateengine.StateTransitionParam)
	 */
	@Override
	public boolean changeState(StateTransitionParam stp)
			throws AcsJNoSuchTransitionEx, AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, AcsJPostconditionFailedEx,
			AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
		
    	logParams(stp.getTargetStatusEntity(), stp.getTargetState(), stp.getSubsystem(), stp.getUserId());
    	
    	logFullProjectStatusTree(stp.getTargetStatusEntity(), BEFORE);
    	
        boolean result = stateEngine.changeState(stp);
        
        logFullProjectStatusTree(stp.getTargetStatusEntity(), AFTER);
    	
        return result;		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#storeArchiveEntities(alma.xmlentity.XmlEntityStruct[], java.lang.String, java.util.logging.Logger)
	 */
	@Override
	public void storeArchiveEntities(XmlEntityStruct[] archiveEntities,
			String user, Logger logr) {
		
		stateArchive.storeArchiveEntities(archiveEntities, user, logger);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.stateengine.StateEngine#insertOrUpdate(java.util.Collection, alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.ousstatus.OUSStatus[], alma.entity.xmlbinding.sbstatus.SBStatus[], alma.xmlentity.XmlEntityStruct[], java.lang.String)
	 */
	@Override
	public void insertOrUpdate(
			XmlEntityStruct[] archiveEntities,
			String archiveUser, 
			ProjectStatus opStatus, 
			OUSStatus[] ousStatuses,
			SBStatus[] sbStatuses,
			Collection<StateTransitionParam> stateTransitions)
			throws AcsJIllegalArgumentEx, 
			AcsJStateIOFailedEx,
			AcsJNoSuchEntityEx, 
			AcsJNoSuchTransitionEx, 
			AcsJNotAuthorizedEx,
			AcsJPreconditionFailedEx, 
			AcsJPostconditionFailedEx {
		
		stateEngine.insertOrUpdate(archiveEntities, archiveUser, 
				opStatus, ousStatuses, 
				sbStatuses, stateTransitions);
		
	}
}
