/**
 * ObsProjectPatchMar2010.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.constants.Location;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;

/**
 * A utility to change the state of an ObsProject
 *
 * @author amchavan, Mar 5, 2010
 * @version $Revision$
 */

// $Id$

public class ChangeObsProjectState {

    public static final String PATCH_SPRING_CONFIG = "/oracleApplicationContext.xml";

    private Logger logger;
    private StateArchive stateArchive;
    
    public ChangeObsProjectState() throws FileNotFoundException,
            DbConfigException {

        Class<?> clasz = this.getClass();
        logger = Logger.getLogger( clasz.getName() );

//        File f = new File( DB_CONFIG );
//        if( !f.exists() || ! f.canRead() ) {
//            throw new RuntimeException( "Can't read: " + DB_CONFIG );
//        }
//        InputStream dbConfStream = new FileInputStream( f );
        StateArchiveDbConfig dbConfig = 
            new StateArchiveDbConfig(logger );
        StateSystemContextFactory.INSTANCE.init( PATCH_SPRING_CONFIG, dbConfig );

        System.setProperty( Location.RUNLOCATION_PROP, Location.TEST );
        stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
        stateArchive.initStateArchive( logger );
    }

    /**
     * Change the state of a set of {@link ObsProject} entities.
     * 
     * @param args
     *            <ul>
     *            <li><strong>state</strong> The new state
     * 
     *            <li><strong>uid [uid ...]</strong> A list of
     *            {@link ProjectStatus} Archive UIDs (<strong>note</strong>: not
     *            ObsProject UIDs): all {@link ProjectStatus} entities
     *            referenced on the list will be changed to the new status,
     *            which effectively changes the state of the corresponding
     *            ObsProjects.
     *            </ul>
     *            Example: <br/>
     *            &nbsp;&nbsp;&nbsp;
     *            <em>ChangeObsProjectState Phase2Submitted uid://X22/X2a/X38c uid://X22/X2a/X3aa</em>
     *            <p/>
     *            To retrieve the {@link ProjectStatus} UID of an ObsProject
     *            one can run the following SQL query:<br/>
     *            &nbsp;&nbsp;&nbsp;
     *            <em>select status_entity_id from obs_project_status where obs_project_id = '&lt;uid&gt;'</em>
     *            <br/>
     *            where <em>&lt;uid&gt;</em> id the Archive UID of the 
     *            ObsProject; e.g. <em>uid://X22/X28/X14</em>.
     *            <p/>
     *            Note that error checking is very limited.
     */
    public static void main( String[] args ) {
        ChangeObsProjectState patch;
        try {
            patch = new ChangeObsProjectState();
            patch.run( args );
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Loop over the input list, and change those states
     * @param args
     * @throws Exception
     */
    private void run( String[] args ) throws Exception {
        
        String state = args[0];
        StatusTStateType tstate = StatusTStateType.valueOf( state );
        
        for( int i = 1; i < args.length; i++ ) {
            
            String uid = args[i];
            ProjectStatusEntityT e = new ProjectStatusEntityT();
            e.setEntityId( uid );
            
            ProjectStatus progStatus;
            try {
                progStatus = stateArchive.getProjectStatus( e );
            }
            catch( AcsJNoSuchEntityEx e1 ) {
                String msg = "Could not find ProjectStatus with uid: " + uid;
                logger.log( Level.WARNING, msg );
                continue;
            }
            progStatus.getStatus().setState( tstate );
            stateArchive.update( progStatus );
            System.out.println( "Updated " + progStatus.getProjectStatusEntity().getEntityId() );
        }
        
// Find all projects in the Phase1Submitted state
//        String[] states = { "Phase1Submitted" };
//        ProjectStatus[] ps = stateArchive.findProjectStatusByState( states );
//        for( ProjectStatus projectStatus : ps ) {
//            projectStatus.getStatus().setState( StatusTStateType.PHASE2SUBMITTED );
//            stateArchive.update( projectStatus );
//            System.out.println( "Updated " + projectStatus.getProjectStatusEntity().getEntityId() );
//        }
    }
}
