/**
 * CreateEmptyDatabase.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import alma.obops.utils.HsqldbUtilities;
import alma.obops.utils.HsqldbUtilities.Mode;

/**
 * Utility class: creates an empty status database for HSQLDB.<p/>

 * Usage:<br/>
 * <code>&nbsp;&nbsp;CreateEmptyDatabase mode name location ddl</code><br/>
 * for instance:<br/>
 * <code>&nbsp;&nbsp;CreateEmptyDatabase Server statusdb //localhost:9001 sql/hsqldb-ddl.sql</code><br/>
 * <code>&nbsp;&nbsp;CreateEmptyDatabase File statusdb hsqldb-files sql/hsqldb-ddl.sql</code><p/>
 * 
 * This is a wrapper around 
 * {@linkplain HsqldbUtilities#createDatabase(Mode, String, String, String, java.sql.Connection)}
 *
 * @author amchavan, Jun 22, 2009
 * @version $Revision$
 */

// $Id$

public class CreateEmptyDatabase {

    public static void main( String[] args ) throws Exception {
        Mode       mode = Mode.valueOf( args[0] );
        String     name = args[1];
        String location = args[2];
        String      ddl = args[3];
        HsqldbUtilities.createDatabase( mode, name, location, ddl, null );
    }
}
