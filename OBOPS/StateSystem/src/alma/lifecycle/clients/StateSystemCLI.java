/**
 * StateSystemCLI.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import static alma.lifecycle.config.SpringConstants.STATE_SYSTEM_SPRING_CONFIG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import alma.acs.entityutil.EntitySerializer;
import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.MockUidProvider;
import alma.entity.xmlbinding.StatusEntityGenerator;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.StateSystem;
import alma.lifecycle.StateSystemBaseImpl;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.remote.client.RemoteStateSystemClient;
import alma.lifecycle.stateengine.RoleProvider;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.lifecycle.stateengine.StateEngine;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * Command-line interface to the State System.
 * <p/>
 * Usage: see {@linkplain #USAGE}
 * 
 * @author amchavan, Jun 22, 2009
 * @version $Revision$
 */

// $Id$
public class StateSystemCLI {

    /**
     * Commands we recognize
     */
    enum Commands {
        interactive, // run an interactive interpreter
        tgput, // tree generate & put
        tput, // tree put
        update, // update
        getouss, // get OUS status
        getps, // get project status
        getpsl, // get project status list
        getsbs, // get SB status
        getsbslo, // get all SB status of OUS
        getsbslp, // get all SB status of project
        changeouss, // change OUS state
        changeps, // change project status state
        changesbs, // change OUS state
    }

    /**
     * Interfaces to the State System we can use
     */
    enum Interfaces {
        direct, // direct access to the database
        idl, // access via a ComponentClient
        web
        // access via a Web service
    }

    private static final String IDL_INTERFACE_SPEC = "idl=corbaloc::<ip>:<port>/Manager";

    private static final String ACS_MANAGER = "MANAGER_REFERENCE";

    private static final String PROMPT = ">>> ";

    private static final Object QUIT_CMD = "exit";

    private static final String COMMENT_PREFIX = "#";

    /** Usage string */
    protected static String USAGE;

    /**
     * Prints a standard error message on the console, then exit with an exit
     * code of 1
     * 
     * @param errorMessage
     *            Message to display
     */
    private static void error( String errorMessage ) {
        error( errorMessage, false );
    }

    /**
     * Prints a standard error message on the console, then exit with an exit
     * code of 1
     * 
     * @param errorMessage
     *            Message to display
     * @param usage
     *            if <code>true</code>, a standard "usage" message will be
     *            output as well.
     */
    private static void error( String errorMessage, boolean usage ) {
        StringBuilder sb = new StringBuilder();
        sb.append( "Error: " ).append( errorMessage );
        if( usage ) {
            sb.append( "\n" ).append( USAGE );
        }
        System.out.println( sb.toString() );
        System.exit( 1 );
    };

    /**
     * @param args
     *            The command-line arguments; see
     *            {@linkplain #buildUsageString()}.
     */
    public static void main( String[] args ) {

        buildUsageString();

        // Check command line
        // ------------------------------------------
        if( args.length < 2 ) {
            error( "insufficient command-line args", true );
        }
        String iface = args[0];
        StateSystem statesys = makeStateSystem( iface );
        StateSystemCLI cli = new StateSystemCLI( statesys );

        String[] cmd = Arrays.copyOfRange( args, 1, args.length );
        exec( cli, cmd );
    }

    /**
     * @param cli
     * @param cmd
     */
    private static void exec( StateSystemCLI cli, String[] cmd ) {
        try {
            cli.exec( cmd );
        }
        catch( IllegalArgumentException e ) {
            error( e.getMessage(), true );
        }
        catch( AcsJNoSuchEntityEx e ) {
            e.printStackTrace();
            String msg = e.getClass().getSimpleName() + ": " + 
                         e.getCause().getMessage();
            error( msg, false );
        }
        catch( Exception e ) {
            e.printStackTrace();
            String msg = e.getClass().getSimpleName() + ": " + e.getMessage();
            error( msg, false );
        }
    }

    /**
     * Builds a State System based on the input interface name.
     * 
     * @param iface
     *            One of <code>direct</code>, <code>web</code>, <code>idl</code>
     *            or
     *            <code>idl=corbaloc::<em>&lt;ip&gt;</em>:<em>&lt;port&gt;</em>/Manager</code>
     * <br/>
     *            For instance
     *            <code>idl=corbaloc::134.171.18.52:3000/Manager</code>
     * 
     * @return A StateSystem implementation.
     */
    private static StateSystem makeStateSystem( String iface ) {

        // 'direct' interface: create a Spring application context
        // and use it to retrieve the standard implementations of
        // StateSystem and StateArchive
        // --------------------------------------------------------
        if( iface.equals( Interfaces.direct.name() ) ) {
            StateSystem ret = makeDirectStateSystem();
            return ret;
        }

        // 'idl' interface: decode the parameter, then
        // use the container services to obtain a
        // reference to the OBOPS_LIFECYCLE_STATE_SYSTEM component,
        // and retrieve the StateSystem implementation from there
        // ---------------------------------------------------------
        if( iface.startsWith( Interfaces.idl.name() ) ) {
            StateSystem ret = makeIdlStateSystem( iface );
            return ret;
        }

        // 'web' interface: TODO
        // -----------------------------------------------------------------
        if( iface.equals( Interfaces.web.name() ) ) {
            throw new RuntimeException( "Interface '" + Interfaces.web.name()
                    + "' is not yet supported." );
        }

        // Error!
        // ---------------------------------------------------------------
        error( "unrecognized interface: " + iface, true );
        return null; // this is never executed
    }

    /** The State System we use for state transitions */
    protected StateSystem stateSystem;

    /** We pass this on to our StateSystem implementations */
    protected static Logger logger = Logger.getAnonymousLogger();

    /** Builds a user friendly usage string */
    private static void buildUsageString() {

        StringBuilder sb = new StringBuilder();
        sb.append( "Usage:\n" )
                .append( "    " )
                .append( StateSystemCLI.class.getSimpleName() )
                .append( " interface command arg1 [arg2 ...]\n" )
                .append( "\n" ).append( "* \"interface\" should be one of " );

        Interfaces[] ifaces = Interfaces.values();
        for( int i = 0; i < ifaces.length; i++ ) {
            sb.append( "\"" ).append( ifaces[i] ).append( "\"" );
            if( i < ifaces.length - 1 ) {
                sb.append( ", " );
            }
        }

        // explain idl=corbaloc...
        sb.append( "\n  If interface is \"" )
          .append( Interfaces.idl.name() )
          .append( "\", it can be complemented by the corbaloc of the ACS\n" )
          .append( "  Manager:\n" )
          .append( "     " )
          .append( IDL_INTERFACE_SPEC )
          .append( "\n" )
          .append( "  For instance\n" )
          .append( "     idl=corbaloc::134.171.18.52:3000/Manager\n" )
          .append( "  Otherwise the value of that corbaloc is taken from the\n" )
          .append( "  MANAGER_REFERENCE environment variable" )
          .append( "\n\n" );

        sb.append( "* \"command\" should be one of " );
        Commands[] cmds = Commands.values();
        for( int i = 0; i < cmds.length; i++ ) {
            sb.append( "\"" ).append( cmds[i] ).append( "\"" );
            if( i < cmds.length - 1 ) {
                sb.append( ", " );
            }
        }
        sb.append( "\n\n" )
        .append( "* \"arg1 [arg2 ...]\" are a list of command-specific arguments\n\n" )
        .append( "Commands:\n" );

        // interactive
        sb.append( "    " + Commands.interactive.name() + "\n" )
          .append( "\tStart an intereactive interpreter reading from stdin.\n" )
          .append( "\n" );

        // update
        sb.append( "    " + Commands.update.name() + " pathname\n" )
        .append( "\tWrite into the State Archive the status entity represented by\n" )
        .append( "\tthe input XML file.\n" )
        .append( "\tA \"status entity\" is an instance of StatusBaseT.\n" )
        .append( "\n" );

        // tput
        sb .append( "    " + Commands.tput.name() + " pathname1 [pathname2 ...]\n" )
           .append( "\tWrite into the State Archive the tree of status entities represented by\n" )
           .append( "\tthe input XML files -- it is assumed that they represent a valid tree,\n" )
           .append( "\tno checks are performed.\n" )
           .append( "\n" );

        // tgput
        sb .append( "    " + Commands.tgput.name() + " pathname\n" )
           .append( "\tGenerate a complete tree of entities from an ObsProject, and store\n" )
           .append( "\tthem into the State Archive.\n" )
           .append( "\tThe input XML files is assumed to represent an ObsProject.\n" )
           .append( "\n" );

        // getpsl
        sb.append( "    " + Commands.getpsl.name() + " uid\n" )
          .append( "\tLook for the ProjectStatus entity with the given uid, find all entities of\n" )
          .append( "\tits status tree and save them as XML files in the current directory.\n" )
          .append( "\tIf the entity with the given uid is not found, no file is written and\n" )
          .append( "\ta warning message is generated on stdout.\n" )
          .append( "\n" );

        // getsbslp,
        sb.append( "    " + Commands.getsbslp.name() + " uid\n" )
          .append( "    " + Commands.getsbslo.name() + " uid\n" )
          .append( "\tFind all SBStatus entities that are children of the entity with the\n" )
          .append( "\tgiven uid; either a ProjectStatus ("
                                 + Commands.getsbslp.name()
                                 + ") or an OUSStatus ("
                                 + Commands.getsbslo.name() + ")\n" )
          .append( "\tSave all SBStatus entities as XML files in the current directory.\n" )
          .append( "\tIf the parent entity is not found, no file is written and a warning\n" )
          .append( "\tmessage is generated on stdout.\n" )
          .append( "\n" );

        // getps, getous, getsbs
        sb.append( "    " + Commands.getouss.name() + " uid\n" )
          .append( "    " + Commands.getps.name() + " uid\n" )
          .append( "    " + Commands.getsbs.name() + " uid\n" )
          .append( "\tRetrieve an OUSStatus, ProjectStatus or SBStatus entity from the\n" )
          .append( "\tState Archive and save it as an XML file in the current directory.\n" )
          .append( "\tIf the entity with the given uid is not found, no file is written and\n" )
          .append( "\ta warning message is generated on stdout.\n" )
          .append( "\n" );

        // changeps, changeous, changesbs
        final String stdParams = " uid newstate subsys user \n";
        sb.append( "    " + Commands.changeps.name() + stdParams )
          .append( "    " + Commands.changeouss.name() + stdParams )
          .append( "    " + Commands.changesbs.name() + stdParams )
          .append( "\tChange the state of ProjectStatus entity 'uid' to be 'newstate'.\n" )
          .append( "\tParameters 'subsys' and 'user' identify the user and\n" )
          .append( "\twill be used to authorize the transaction and record it in the\n" )
          .append( "\tentity's history.\n" )
          .append( "\n" );

        // help
        sb.append( "Everything else will produce this help message" );

        USAGE = sb.toString();
    }

    /**
     * Creates a Spring application context and uses it to retrieve the standard
     * implementations of StateSystem and StateArchive
     */
    private static StateSystem makeDirectStateSystem() {

        if( !StateSystemContextFactory.INSTANCE.isInitialized() ) {
        	StateArchiveDbConfig dbConfig = null;
            try {
                dbConfig = new StateArchiveDbConfig( logger );
            }
            catch( DbConfigException e ) {
                e.printStackTrace();
            }
            StateSystemContextFactory.INSTANCE.init( STATE_SYSTEM_SPRING_CONFIG, dbConfig);
        }

        StateArchive stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
        stateArchive.initStateArchive( logger );
        StateEngine stateEngine = StateSystemContextFactory.INSTANCE.getStateEngine();
        // TODO: replace RoleProviderMock with real LDAP one
        RoleProvider RoleProvider = new RoleProviderMock();
        
        try {
            stateEngine.initStateEngine( logger, stateArchive, RoleProvider);
        }
        catch( Exception e ) {
            e.printStackTrace();
            error( "State Engine initialization failed: " + 
                   e.getCause().getMessage() );
        }

        StateSystem ret = new StateSystemBaseImpl( stateArchive, stateEngine, logger );
        return ret;
    }

    /**
     * Use the container services to obtain a reference to the
     * OBOPS_LIFECYCLE_STATE_SYSTEM component, and retrieve the StateSystem
     * implementation from there
     * 
     * @param iface
     *            The interface specification: it can be <code>idl</code> or
     * 
     *            <code>idl=corbaloc::<em>&lt;ip&gt;</em>:<em>&lt;port&gt;</em>/Manager</code>
     * <br/>
     *            For instance:
     *            <code>idl=corbaloc::134.171.18.52:3000/Manager</code>
     */
    private static StateSystem makeIdlStateSystem( String iface ) {

        // Compute the corbaloc of the ACS Manager
        // ----------------------------------------
        String acsManager = null;

        if( iface.equals( Interfaces.idl.name() ) ) {
            // Interface specification does not include the corbaloc.
            // Try and recover that from an environment variable
            acsManager = System.getenv( ACS_MANAGER );
            if( acsManager == null ) {
                error( "Environment variable '" + ACS_MANAGER
                        + "' is not defined.", true );
            }
        }
        else {
            // Interface specification seems to include the corbaloc.
            // Check if it really does
            String ip = "\\d+\\.\\d+\\.\\d+\\.\\d+"; // RE for IP addresses
            String re = "idl=corbaloc::" + ip + ":\\d+/Manager";
            if( !iface.matches( re ) ) {
                error( "Invalid format for interface spec, should be '"
                        + IDL_INTERFACE_SPEC + "'", true );
            }
            String[] tmp = iface.split( "=" );
            acsManager = tmp[1];
        }

        // Now try to build the IDL client
        // -----------------------------------------------------
        try {
            StateSystem ret = new RemoteStateSystemClient( logger, acsManager );
            return ret;
        }
        catch( Exception e ) {
            e.printStackTrace();
            error( e.getMessage() );
            return null; // this is never executed
        }
    }

    /**
     * Constructor.
     * 
     * @param stateSystem
     *            The implementation of the State System we provide a
     *            command-line interface for.
     */
    public StateSystemCLI( StateSystem stateSystem ) {
        this.stateSystem = stateSystem;
    }

    /**
     * Change the state of an OUSStatus entity; if the entity is not found, a
     * warning message is generated on stdout.<br/>
     * 
     * @param args
     *            Should be a sequence of strings:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid newstate subsys user</em><br/>
     *            where:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid</em> is the ID of the entity<br/>
     *            <em>&nbsp;&nbsp;&nbsp;newstate</em> is the required new state<br/>
     *            <em>&nbsp;&nbsp;&nbsp;subsys</em> is the subsystem effecting
     *            the change<br/>
     *            <em>&nbsp;&nbsp;&nbsp;user</em> is the user ID<br/>
     *            <p/>
     *            For instance:
     *            <em>uid://X58/X27f/Xff0 PipelineError Pipeline 0</em>
     * 
     * @return <code>true</code> if the state change could take place,
     *         <code>false</code> otherwise.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected Boolean doChangeOusStatus( String[] args ) throws Exception {

        if( args.length != 4 ) {
            String msg = "Invalid args list";
            throw new IllegalArgumentException( msg );
        }

        // Convert params to proper types
        OUSStatusEntityT id = new OUSStatusEntityT();
        id.setEntityId( args[0] );

        StatusTStateType state = StatusTStateType.valueOf( args[1] );

        String subsys = args[2];
        String user = args[3];

        Boolean ret = stateSystem.changeState( id, state, subsys, user );
        return ret;
    }

    /**
     * Change the state of a ProjectStatus entity; if the entity is not found, a
     * warning message is generated on stdout.<br/>
     * 
     * @param args
     *            Should be a sequence of strings:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid newstate subsys user</em><br/>
     *            where:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid</em> is the ID of the entity<br/>
     *            <em>&nbsp;&nbsp;&nbsp;newstate</em> is the required new state<br/>
     *            <em>&nbsp;&nbsp;&nbsp;subsys</em> is the subsystem effecting
     *            the change<br/>
     *            <em>&nbsp;&nbsp;&nbsp;user</em> is the user ID<br/>
     *            <p/>
     *            For instance: <em>uid://X58/X27f/Xff0 Ready ObOps 32342
     * 
     * @return <code>true</code> if the state change could take place,
     *         <code>false</code> otherwise.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected Boolean doChangeProjectStatus( String[] args ) throws Exception {

        if( args.length != 4 ) {
            String msg = "Invalid args list";
            throw new IllegalArgumentException( msg );
        }

        // Convert params to proper types
        ProjectStatusEntityT id = new ProjectStatusEntityT();
        id.setEntityId( args[0] );

        StatusTStateType state = StatusTStateType.valueOf( args[1] );

        String subsys = args[2];
        String user = args[3];

        Boolean ret = stateSystem.changeState( id, state, subsys, user );
        return ret;
    }

    /**
     * Change the state of an SBStatus entity; if the entity is not found, a
     * warning message is generated on stdout.<br/>
     * 
     * @param args
     *            Should be a sequence of strings:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid newstate subsys user</em><br/>
     *            where:<br/>
     *            <em>&nbsp;&nbsp;&nbsp;uid</em> is the ID of the entity<br/>
     *            <em>&nbsp;&nbsp;&nbsp;newstate</em> is the required new state<br/>
     *            <em>&nbsp;&nbsp;&nbsp;subsys</em> is the subsystem effecting
     *            the change<br/>
     *            <em>&nbsp;&nbsp;&nbsp;user</em> is the user ID<br/>
     *            <p/>
     *            For instance:
     *            <em>uid://X58/X27f/Xff0 Running Scheduling 78321
     * 
     * @return <code>true</code> if the state change could take place,
     *         <code>false</code> otherwise.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected Boolean doChangeSbStatus( String[] args ) throws Exception {

        if( args.length != 4 ) {
            String msg = "Invalid args list";
            throw new IllegalArgumentException( msg );
        }

        // Convert params to proper types
        SBStatusEntityT id = new SBStatusEntityT();
        id.setEntityId( args[0] );

        StatusTStateType state = StatusTStateType.valueOf( args[1] );

        String subsys = args[2];
        String user = args[3];

        Boolean ret = stateSystem.changeState( id, state, subsys, user );
        return ret;
    }

    /**
     * Get an OUSStatus entity and save it as an XML file in the current
     * directory.
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathname.
     * 
     * @param uid
     *            ID of the required OUSStatus
     * 
     * @return The XML text of the entity, or <code>null</code> if no such
     *         entity was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected String doGetOusStatus( String uid ) throws Exception {

        String xml = null;
        try {
            // Build an OUSStatusEntityT for the query
            OUSStatusEntityT id = new OUSStatusEntityT();
            id.setEntityId( uid );
            xml = stateSystem.getOUSStatusXml( id );

            Utils.writeEntityToFile( uid, xml );
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }

        return xml;
    }

    /**
     * Get a ProjectStatus entity and save it as an XML file in the current
     * directory.
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathname.
     * 
     * @param uid
     *            ID of the required ProjectStatus
     * 
     * @return The XML text of the entity, or <code>null</code> if no such
     *         entity was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected String doGetProjectStatus( String uid ) throws Exception {

        String xml = null;
        try {
            // Build a ProjectStatusEntityT for the query
            ProjectStatusEntityT id = new ProjectStatusEntityT();
            id.setEntityId( uid );
            xml = stateSystem.getProjectStatusXml( id );

            Utils.writeEntityToFile( uid, xml );
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }

        return xml;
    }

    /**
     * Get a ProjectStatus entity tree and save all entities as files in the
     * current directory
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathnames.
     * 
     * @param uid
     *            ID of the required ProjectStatus
     * 
     * @return The list of XML entities, or <code>null</code> if no such project
     *         was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected StatusBaseT[] doGetProjectStatusList( String uid )
            throws Exception {

        StatusBaseT[] statusEntities = null;
        try {
            // Build a ProjectStatusEntityT for the query
            ProjectStatusEntityT id = new ProjectStatusEntityT();
            id.setEntityId( uid );
            statusEntities = stateSystem.getProjectStatusList( id );

            EntitySerializer serializer = 
                EntitySerializer.getEntitySerializer( logger );
            for( int i = 0; i < statusEntities.length; i++ ) {
                StatusBaseT statusEntity = statusEntities[i];
                XmlEntityStruct x = serializer.serializeEntity( statusEntity );
                Utils.writeEntityToFile( x.entityId, x.xmlString );
            }
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }

        return statusEntities;
    }

    /**
     * Get an SBStatus entity and save it as an XML file in the current
     * directory.
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathname.
     * 
     * @param uid
     *            ID of the required SBStatus
     * 
     * @return The XML text of the entity, or <code>null</code> if no such
     *         entity was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected String doGetSbStatus( String uid ) throws Exception {

        String xml = null;
        try {
            // Build an SBStatusEntityT for the query
            SBStatusEntityT id = new SBStatusEntityT();
            id.setEntityId( uid );
            xml = stateSystem.getSBStatusXml( id );

            Utils.writeEntityToFile( uid, xml );
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }

        return xml;
    }

    /**
     * Get all SBStatus entities that are children of an OUSStatus; save all
     * such entities as files in the current directory
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathnames.
     * 
     * @param uid
     *            ID of the parent OUSStatus
     * 
     * @return The list of XML entities (represented as XML text), or
     *         <code>null</code> if no such project was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected SBStatus[] doGetSbStatusListOus( String uid ) throws Exception {
    
        SBStatus[] sbList = null;
        try {
            // Build a ProjectStatusEntityT for the query
            OUSStatusEntityT id = new OUSStatusEntityT();
            id.setEntityId( uid );
            sbList = stateSystem.getSBStatusList( id );
    
            for( int i = 0; i < sbList.length; i++ ) {
                String eid = sbList[i].getSBStatusEntity().getEntityId();
                String xml = Utils.marshall( sbList[i] );
                Utils.writeEntityToFile( eid, xml );
            }
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }
    
        return sbList;
    }

    /**
     * Get all SBStatus entities that are children of a ProjectStatus; save all
     * such entities as files in the current directory
     * 
     * If the entity is not found, no file is written and a warning message is
     * generated on stdout.<br/>
     * 
     * See {@link Utils#writeEntityToFile(String, String)} for a description of the
     * output file pathnames.
     * 
     * @param uid
     *            ID of the parent ProjectStatus
     * 
     * @return The list of XML entities (represented as XML text), or
     *         <code>null</code> if no such project was found.
     * 
     * @throws Exception
     *             If anything goes wrong
     */
    protected SBStatus[] doGetSbStatusListPs( String uid ) throws Exception {

        SBStatus[] sbList = null;
        try {
            // Build a ProjectStatusEntityT for the query
            ProjectStatusEntityT id = new ProjectStatusEntityT();
            id.setEntityId( uid );
            sbList = stateSystem.getSBStatusList( id );

            for( int i = 0; i < sbList.length; i++ ) {
                String eid = sbList[i].getSBStatusEntity().getEntityId();
                String xml = Utils.marshall( sbList[i] );
                Utils.writeEntityToFile( eid, xml );
            }
        }
        catch( AcsJNoSuchEntityEx e ) {
            warning( "No entity found for uid '" + uid + "'" );
        }

        return sbList;
    }

    /**
     * Start an interactive session, reading individual commands from standard
     * input.
     * 
     * @param cli  The command-line interpreter
     * 
     * @return Always <code>null</code>
     * 
     * @throws IOException
     */
    protected Object doInteractive( StateSystemCLI cli ) throws Exception {

        BufferedReader in = 
            new BufferedReader( new InputStreamReader( System.in ));

        // interpreter loop
        // -------------------------------------
        while( true ) {
            System.out.print( PROMPT );
            System.out.flush();
            String line = in.readLine();

            // See if we're done
            if( line == null ) {
                break;
            }
            line = line.trim();
            if( line.equals( QUIT_CMD )) {
                break;
            }

            // Ignore empty lines and comment lines
            if( line.length() == 0 || line.startsWith( COMMENT_PREFIX ) ) {
                continue;
            }

            // 'interactive' is a no-no in interactive mode!
            String iactiveCmd = Commands.interactive.name();
            if( line.equals( iactiveCmd ) ) {
                String msg = "'" + iactiveCmd
                        + "' is not allowed in interactive mode";
                System.out.println( msg );
                continue;
            }

            // Break down input line in words, and exec resulting command
            String[] cmd = line.split( "\\s+" );
            exec( cli, cmd );
        }
        return null;
    }

    /**
     * Generate a complete tree of entities from the input ObsProject, and store
     * into the State Archive
     * 
     * @param pathname
     *            Pathnames of an XML file representing an ObsProject
     * 
     * @return The list of the status entities that were created and persisted.
     *         Elements of this list are in the same order as they were written
     *         into the State Archive.
     * 
     * @throws Exception
     *             if anything goes wrong
     */
    protected Object[] doTreeGeneratePut( String pathname ) throws Exception {

        ObsProject project = (ObsProject) Utils.unmarshal( pathname );
        MockUidProvider provider = new MockUidProvider();
        StatusEntityGenerator generator = new StatusEntityGenerator( provider );
        List<StatusBaseT> entities = generator.generateTree( project );
        for( StatusBaseT entity : entities ) {
            if( entity instanceof SBStatus ) {
                stateSystem.update( (SBStatus) entity );
            }
            else if( entity instanceof OUSStatus ) {
                stateSystem.update( (OUSStatus) entity );
            }
            else if( entity instanceof ProjectStatus ) {
                stateSystem.update( (ProjectStatus) entity );
            }
            else {
                String msg = 
                    "Internal error, don't know how to deal " +
                    "with instances of: " +
                    entity.getClass().getCanonicalName();
                throw new RuntimeException( msg );
            }
        }
        return entities.toArray( new StatusBaseT[0] );
    }

    /**
     * Update the status entity represented by the input file pathname into the
     * State Archive
     * 
     * @param pathname
     *            Pathname of the input file
     * 
     * @return The status entity that was updated.
     * @throws Exception
     */
    protected Object doUpdate( String pathname ) throws Exception {

        Object ret = Utils.unmarshal( pathname );
        String entityType = Utils.getEntityType( pathname );
        if( entityType.equals( "ProjectStatus" ) ) {
            stateSystem.update( (ProjectStatus) ret );
        }
        else if( entityType.equals( "OUSStatus" ) ) {
            stateSystem.update( (OUSStatus) ret );
        }
        else if( entityType.equals( "SBStatus" ) ) {
            stateSystem.update( (SBStatus) ret );
        }
        else {
            String msg = "Unsupported entity type '" + entityType
                    + "' found in file " + pathname;
            throw new RuntimeException( msg );
        }
        return ret;
    }

    /**
     * Interpret the input command (a sequence of words) and execute the implied
     * actions.
     * 
     * For a description of what commands are accepted, see ({@linkplain #USAGE}
     * .
     * 
     * @return Whatever the command execution returned.
     * 
     * @throws Exception
     *             If anything happens
     */
    public Object exec( String[] cmd ) throws Exception {

        // Separate command name and command arguments
        String cmdName = cmd[0];
        String[] cmdArgs = Arrays.copyOfRange( cmd, 1, cmd.length );

        Commands command = null;
        try {
            command = Commands.valueOf( cmdName );
        }
        catch( IllegalArgumentException e ) {
            // give a better explanation
            String msg = "Unrecognized command '" + cmdName + "'";
            throw new IllegalArgumentException( msg );
        }

        if( command.equals( Commands.interactive ) ) {
            // Put a complete tree of files into the State Archive
            return doInteractive( this );
        }

        if( command.equals( Commands.tput ) ) {
            // Put a complete tree of files into the State Archive
            return doTreePut( cmdArgs );
        }

        if( command.equals( Commands.tgput ) ) {
            // Generate a complete tree of entities from the input
            // ObsProject, and store into the State Archive
            return doTreeGeneratePut( cmdArgs[0] );
        }

        if( command.equals( Commands.update ) ) {
            // Put a complete tree of files into the State Archive
            return doUpdate( cmdArgs[0] );
        }

        if( command.equals( Commands.getouss ) ) {
            // Get an OUSStatus entity and save it as a file
            // in the current directory
            return doGetOusStatus( cmdArgs[0] );
        }

        if( command.equals( Commands.getps ) ) {
            // Get a ProjectStatus entity and save it as a file
            // in the current directory
            return doGetProjectStatus( cmdArgs[0] );
        }

        if( command.equals( Commands.getpsl ) ) {
            // Get a ProjectStatus entity tree and save all entities as
            // files in the current directory
            return doGetProjectStatusList( cmdArgs[0] );
        }

        if( command.equals( Commands.getsbs ) ) {
            // Get an SBStatus entity and save it as a file
            // in the current directory
            return doGetSbStatus( cmdArgs[0] );
        }

        if( command.equals( Commands.changeouss ) ) {
            // Get a OUSStatus entity and save it as a file
            // in the current directory
            return doChangeOusStatus( cmdArgs );
        }

        if( command.equals( Commands.changeps ) ) {
            // Get a ProjectStatus entity and save it as a file
            // in the current directory
            return doChangeProjectStatus( cmdArgs );
        }

        if( command.equals( Commands.changesbs ) ) {
            // Get a SBStatus entity and save it as a file
            // in the current directory
            return doChangeSbStatus( cmdArgs );
        }

        if( command.equals( Commands.getsbslo ) ) {
            // Get all SBStatus entities that are children of a OUSStatus;
            // save all such entities as files in the current directory
            return doGetSbStatusListOus( cmdArgs[0] );
        }

        if( command.equals( Commands.getsbslp ) ) {
            // Get all SBStatus entities that are children of a ProjectStatus;
            // save all such entities as files in the current directory
            return doGetSbStatusListPs( cmdArgs[0] );
        }

        String msg = "Internal error: unsupported command '" + command + "'";
        throw new IllegalArgumentException( msg );
    }

    /**
     * Prints a standard warning message on the console
     * 
     * @param warningMessage
     *            Message to display
     * @param usage
     *            if <code>true</code>, a standard "usage" message will be
     *            output as well.
     */
    private void warning( String warningMessage ) {
        StringBuilder sb = new StringBuilder();
        sb.append( "Warning: " ).append( warningMessage );
        System.out.println( sb.toString() );
    }

    /**
     * Put the status entity tree represented by the input file list into the
     * State Archive
     * 
     * @param pathnames
     *            Pathnames of the input files
     * 
     * @return The list of the status entity that were created and persisted.
     *         Elements of this list are in the same order as the corresponding
     *         input files.
     * 
     * @throws Exception
     *             if anything goes wrong
     */
    protected Object[] doTreePut( String[] pathnames ) throws Exception {
    
        // Convert list of pathnames to list of status entities
        Object[] ret = new Object[pathnames.length];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = Utils.unmarshal( pathnames[i] );
        }
    
        // Now create sublists, one per class
        ProjectStatus ops = null;
        Vector<OUSStatus> ous = new Vector<OUSStatus>();
        Vector<SBStatus> sbs = new Vector<SBStatus>();
        for( Object object : ret ) {
            if( object instanceof ProjectStatus ) {
                ops = (ProjectStatus) object;
            }
            else if( object instanceof OUSStatus ) {
                ous.add( (OUSStatus) object );
            }
            else if( object instanceof SBStatus ) {
                sbs.add( (SBStatus) object );
            }
            else {
                String msg = "Unsupported entity type: "
                        + object.getClass().getSimpleName();
                throw new RuntimeException( msg );
            }
        }
        OUSStatus[] ousa = ous.toArray( new OUSStatus[0] );
        SBStatus[] sbsa = sbs.toArray( new SBStatus[0] );
    
        // All right, now we can try to save the lot
        stateSystem.insert( ops, ousa, sbsa );
        return ret;
    }
}
