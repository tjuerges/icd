/**
 * ObsProjectPatchMar2010.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.constants.Location;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;

/**
 * A utility to change the state of an ObsProject
 *
 * @author amchavan, Mar 5, 2010
 * @version $Revision$
 */

// $Id$

public class ChangeObsUnitSetState {

    public static final String PATCH_SPRING_CONFIG = "/oracleApplicationContext.xml";

    private Logger logger;
    private StateArchive stateArchive;
    
    public ChangeObsUnitSetState() throws FileNotFoundException,
            DbConfigException {

        Class<?> clasz = this.getClass();
        logger = Logger.getLogger( clasz.getName() );

//        File f = new File( DB_CONFIG );
//        if( !f.exists() || ! f.canRead() ) {
//            throw new RuntimeException( "Can't read: " + DB_CONFIG );
//        }
//        InputStream dbConfStream = new FileInputStream( f );
        StateArchiveDbConfig dbConfig = 
            new StateArchiveDbConfig(logger );
        StateSystemContextFactory.INSTANCE.init( PATCH_SPRING_CONFIG, dbConfig );

        System.setProperty( Location.RUNLOCATION_PROP, Location.TEST );
        stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
        stateArchive.initStateArchive( logger );
    }

    /**
     * Change the state of a set of {@link ObsProject} entities.
     * 
     * @param args
     *            <ul>
     *            <li><strong>state</strong> The new state
     * 
     *            <li><strong>uid [uid ...]</strong> A list of
     *            {@link ProjectStatus} Archive UIDs (<strong>note</strong>: not
     *            ObsProject UIDs): all {@link ProjectStatus} entities
     *            referenced on the list will be changed to the new status,
     *            which effectively changes the state of the corresponding
     *            ObsProjects.
     *            </ul>
     *            Example: <br/>
     *            &nbsp;&nbsp;&nbsp;
     *            <em>ChangeObsProjectState Phase2Submitted uid://X22/X2a/X38c uid://X22/X2a/X3aa</em>
     *            <p/>
     *            To retrieve the {@link ProjectStatus} UID of an ObsProject
     *            one can run the following SQL query:<br/>
     *            &nbsp;&nbsp;&nbsp;
     *            <em>select status_entity_id from obs_project_status where obs_project_id = '&lt;uid&gt;'</em>
     *            <br/>
     *            where <em>&lt;uid&gt;</em> id the Archive UID of the 
     *            ObsProject; e.g. <em>uid://X22/X28/X14</em>.
     *            <p/>
     *            Note that error checking is very limited.
     */
    public static void main( String[] args ) {
    	ChangeObsUnitSetState patch;
        try {
            patch = new ChangeObsUnitSetState();
            patch.run( args );
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Loop over the input list, and change those states
     * @param args
     * @throws Exception
     */
    private void run( String[] args ) throws Exception {
        
        String state = args[0];
        StatusTStateType tstate = StatusTStateType.valueOf( state );
        if (args.length < 2 || args.length > 2) {
        	throw new Exception("Not enough arguments");
        }
        
        //for( int i = 1; i < args.length; i++ ) {
            
            String projectUid = args[1];
            //String ousUid = args[1];            
            ProjectStatusEntityT e = new ProjectStatusEntityT();
            OUSStatusEntityT ousStatusEntity = new OUSStatusEntityT();
            e.setEntityId( projectUid );
            
            ProjectStatus progStatus;
            OUSStatus ousStatus;
            try {
                progStatus = stateArchive.getProjectStatus( e );
                OUSStatusRefT ousStatusRef = progStatus.getObsProgramStatusRef();
                String ousId = ousStatusRef.getEntityId();                
                ousStatusEntity.setEntityId( ousId ); 
                ousStatus = stateArchive.getOUSStatus(ousStatusEntity);
                
            }
            catch( AcsJNoSuchEntityEx e1 ) {
                String msg = "Could not find ProjectStatus with uid: " + projectUid;
                logger.log( Level.WARNING, msg );
                throw e1;                
            }
            ousStatus.getStatus().setState( tstate );
            stateArchive.update( ousStatus );
            System.out.println( "Updated OUS status - " + ousStatus.getOUSStatusEntity().getEntityId() );
    }
}
