/**
 * CreateEmptyDatabase.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.clients;

import alma.obops.utils.HsqldbUtilities;
import alma.obops.utils.HsqldbUtilities.Mode;

/**
 * Utility class: shut down the HSQLDB server.
 * <p/>
 * 
 * Usage:<br/>
 * <code>&nbsp;&nbsp;ShutdownDbServer mode name location</code><br/>
 * for instance:<br/>
 * <code>&nbsp;&nbsp;ShutdownDbServer Server statusdb //localhost:9001</code><br/>
 * <code>&nbsp;&nbsp;ShutdownDbServer File statusdb hsqldb-files</code><p/>
 * 
 * This is a wrapper around 
 * {@linkplain HsqldbUtilities#shutdown(Mode, String, String, java.sql.Connection)}
 *
 * @author amchavan, Jun 22, 2009
 * @version $Revision$
 */

// $Id$

public class ShutdownDbServer {

    public static void main( String[] args ) {
        Mode mode = Mode.valueOf( args[0] );
        String name = args[1];
        String location = args[2];
        HsqldbUtilities.shutdown( mode, name, location, null );
    }
}
