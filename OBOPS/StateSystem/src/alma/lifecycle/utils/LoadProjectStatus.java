/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.utils;

import static alma.lifecycle.config.SpringConstants.STATE_SYSTEM_SPRING_CONFIG;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.archive.database.helpers.wrappers.StateArchiveDbConfig;
import alma.entity.xmlbinding.MockUidProvider;
import alma.entity.xmlbinding.StatusEntityGenerator;
import alma.entity.xmlbinding.UidProvider;
import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.ExecStatusT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.ExecBlockRefT;
import alma.entity.xmlbinding.valuetypes.StatusT;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.config.StateSystemContextFactory;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.RoleProviderMock;
import alma.lifecycle.stateengine.StateEngine;
import alma.lifecycle.stateengine.constants.Location;
import alma.obops.utils.DatetimeUtils;
import alma.obops.utils.HsqldbUtilities;
import alma.obops.utils.ZipIoUtils;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.stateengineexceptions.wrappers.AcsJNoSuchTransitionEx;
import alma.stateengineexceptions.wrappers.AcsJNotAuthorizedEx;
import alma.stateengineexceptions.wrappers.AcsJPostconditionFailedEx;
import alma.stateengineexceptions.wrappers.AcsJPreconditionFailedEx;
/**
 * 
 * A utility to load sample project data into the StateArchive
 * 
 * @author rkurowsk, Jul 10, 2009
 * @version $Revision$
 */

// $Id$
public class LoadProjectStatus {

	/** The projects we want to load */
	private static final String[] PROJECTS = {
//		"projects/SD_SPECTRA.aot",
		"projects/1dot3dot1SpecLineSurvInHiZSysVers2Sand-V9.aot",
    	"projects/01_projectStatusTest.aot",
    	"projects/02_projectStatusTest.aot",
    	"projects/03_projectStatusTest.aot",
		"projects/obsProjectGalaxies.aot",    	
	};
	 
	private static final String DDL = "sql/hsqldb-ddl.sql";
	
	private static final String PATHPROJ = "ObsProject";
	
	private StateArchive stateArchive;
	private StateEngine stateEngine;
	
	
	/**
	 * @param createTables
	 */
	public LoadProjectStatus(boolean createTables) {
        try {
			if(!StateSystemContextFactory.INSTANCE.isInitialized()){
				StateArchiveDbConfig dbConfig = 
					new StateArchiveDbConfig( Logger.getAnonymousLogger());
				StateSystemContextFactory.INSTANCE.init(STATE_SYSTEM_SPRING_CONFIG, dbConfig);
			}
			System.setProperty( Location.RUNLOCATION_PROP, Location.TEST );
			this.stateArchive = StateSystemContextFactory.INSTANCE.getStateArchive();
			this.stateEngine = StateSystemContextFactory.INSTANCE.getStateEngine();
			this.stateEngine.initStateEngine(Logger.getAnonymousLogger(), this.stateArchive, new RoleProviderMock());
			
			if(createTables){
				HsqldbUtilities.createDatabase(StateSystemContextFactory.INSTANCE.getConnectionUrl(), DDL);    	
			}
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
	}
	
	/**
	 * Create state archive entries for given project & schedBlocks
	 * 
 	 * @param project
	 * @param genExecutions
	 * 
	 * @throws MarshalException
	 * @throws ValidationException
	 * @throws IOException
	 * @throws AcsJStateIOFailedEx
	 * @throws AcsJNoSuchEntityEx 
	 * @throws AcsJIllegalArgumentEx 
	 * @throws AcsJPostconditionFailedEx 
	 * @throws AcsJPreconditionFailedEx 
	 * @throws AcsJNotAuthorizedEx 
	 * @throws AcsJNoSuchTransitionEx 
	 */
	public void createStatusEntites(ObsProject project, boolean genExecutions) 
		throws Exception {
			
		UidProvider uidProv = new MockUidProvider();
		StatusEntityGenerator seg = new StatusEntityGenerator(uidProv);
		List<StatusBaseT> statusEntities = seg.generateTree(project);
		
		List<OUSStatus> ousStatusList = new ArrayList<OUSStatus>();
		List<SBStatus> sbStatusList = new ArrayList<SBStatus>();
		ProjectStatus projectStatus = null;

		for(StatusBaseT statusBaseT: statusEntities){
			if(statusBaseT instanceof ProjectStatus){
				projectStatus = (ProjectStatus) statusBaseT;
			}if(statusBaseT instanceof OUSStatus){
				ousStatusList.add((OUSStatus)statusBaseT);
			}if(statusBaseT instanceof SBStatus){
				sbStatusList.add((SBStatus)statusBaseT);
			}
		}
		stateArchive.insert(projectStatus, ousStatusList.toArray(new OUSStatus[0]),
				sbStatusList.toArray(new SBStatus[0]));
		genExecutions = false;		
        // generate some fake execution data
		
        if(genExecutions){
        	ProjectStatus ps = null;
        	SBStatus sb = null;
        	Calendar cal = Calendar.getInstance();
        	cal.set(Calendar.MINUTE, 10);
			String date1 = DatetimeUtils.formatAsIso(cal.getTime());
			cal.set(Calendar.MINUTE, 20);
			String date2 = DatetimeUtils.formatAsIso(cal.getTime());
			cal.set(Calendar.MINUTE, 30);
			String date3 = DatetimeUtils.formatAsIso(cal.getTime());
        	
			for(StatusBaseT statusBaseT: statusEntities){
				if(statusBaseT instanceof ProjectStatus){
					ps = (ProjectStatus) statusBaseT;
				}if(statusBaseT instanceof SBStatus){
					
					// create a fake exec status entry for each SB
					sb = (SBStatus)statusBaseT;
					ExecStatusT execStatus1 = new ExecStatusT();
					
					ExecBlockRefT execRef = new ExecBlockRefT();
					execRef.setExecBlockId("123455");
					execStatus1.setExecBlockRef(execRef);
					
					execStatus1.setEntityPartId("123455");
					execStatus1.setTimeOfCreation(date1);
					
					StatusT status = new StatusT();
					status.setStartTime(date2);
					status.setEndTime(date3);
					status.setState(StatusTStateType.FULLYOBSERVED);
					execStatus1.setStatus(status);
					sb.setExecStatus(new ExecStatusT[]{execStatus1});
					stateArchive.update(sb);
					
				}
			}
			
//			if(ps != null){
//				// change the project to ready
//				stateEngine.changeState(ps.getProjectStatusEntity(), 
//						StatusTStateType.READY, Subsystem.OBOPS, Role.ARCA);
//			}
//			
//			if(sb != null){
//				// change state of an SB from P2S to running
//				stateEngine.changeState(sb.getSBStatusEntity(), 
//						StatusTStateType.RUNNING, Subsystem.SCHEDULING, Role.AOD);
//			}
        }
	}
	
	/**
	 * Import all known reference programs.
	 * 
	 * @return The list of all known project files.
	 */
	private File[] getProjects(String[] fileNames) {
		File[] files = new File[fileNames.length];

		for (int i = 0; i < fileNames.length; i++) {
			String project = fileNames[i];
			URL url = LoadProjectStatus.class.getResource(project);
			if (url == null) {
				String msg = "Can't find resource " + project + " in package "
						+ LoadProjectStatus.class.getPackage().getName();
				throw new RuntimeException(msg);
			}
			File projectFile = new File(url.getFile());
			files[i] = projectFile;
		}
		return files;
	}

	private void saveProject(File zipfile, boolean genExecutions) throws Exception{

		System.out.println("Importing " + zipfile.getName() + "... ");

		List<ZipIoUtils.ZipNtry> entries = ZipIoUtils.getZipEntries(zipfile);

		ObsProject project = null;

		for (ZipIoUtils.ZipNtry entry : entries) {

			// Decode the entry and build a reader from the data field
			// --------------------------------------------------------
			String name = entry.getName();
			byte[] data = entry.getData();
			String s = new String(data);
			StringReader r = new StringReader(s);

			// Unmarshal individual Business Objects
			// ------------------------------------------
			if (name.startsWith(PATHPROJ)) {
				project = ObsProject.unmarshalObsProject(r);
			} else {
				String msg = "Ignoring entry in Zip file: " + name;
				System.err.println(msg);
			}
		}
		
		createStatusEntites(project, false);

	}

	/** Import given projects and save them into the StateArchive */
	private void loadAndSaveProjects(String[] fileNames) {
		File[] projects = getProjects(fileNames);
		loadAndSaveProjects(projects);
	}

	/** Import given projects and save them into the StateArchive */
	private void loadAndSaveProjects(File[] projects) {
		for (File projectFile : projects) {
			try {
				saveProject(projectFile, false);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
	
	/**
	 * Import all known reference projects and save them into the StateArchive.
	 * 
	 * Reference programs are looked up in in subdirectory <em>projects</em> of
	 * this package;
	 * 
	 * @param args
	 *            Ignored
	 */
	public static void main(String[] args) {

		try {
			System.out.println("Starting LoadProjects...");
			LoadProjectStatus lp = new LoadProjectStatus(true);
			lp.loadAndSaveProjects(PROJECTS);
			System.out.println("Finished LoadProjects");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
