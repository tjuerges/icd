/**
 * HibernateUtil.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * Helper class which takes care of startup and makes accessing a
 * SessionFactory convenient (from the Hibernate docs).
 * 
 * @author amchavan, Jun 10, 2009
 * @version $Revision$
 */

// $Id$
public class HibernateUtils {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if( sessionFactory == null ) {
            try {
                // Create the SessionFactory from annotations in the domain classes
                AnnotationConfiguration ac = new AnnotationConfiguration();
                sessionFactory = ac.configure().buildSessionFactory();
            }
            catch( Throwable ex ) {
                // Make sure you log the exception, as it might be swallowed
                System.err.println( "SessionFactory creation failed." + ex );
                throw new RuntimeException( ex );
            }
        }
        return sessionFactory;
    }
}