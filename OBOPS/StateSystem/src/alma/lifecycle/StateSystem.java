/**
 * StateSystem.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle;

import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.stateengine.StateEngine;

/**
 * The public interface to the ALMA life-cycle state system.
 * 
 * It combines the {@linkplain StateEngine} and {@linkplain StateArchive} 
 * interfaces.
 *
 * @author amchavan, Jul 8, 2009
 * @version $Revision$
 */

// $Id$

public interface StateSystem extends StateArchive, StateEngine {
    // no other methods for the time being
}
