/**
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.springframework.transaction.annotation.Transactional;

import alma.acs.entityutil.EntityException;
import alma.acs.exceptions.AcsJException;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusChoice;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatusRefT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatusRefT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatusRefT;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;

/**
 * Abstract implementation of the {@linkplain StateArchive} interface 
 *
 * @author amchavan, Jun 22, 2009
 * @author rkurowsk, July 30, 2009
 * @version $Revision$
 */

// $Id$

@Transactional(rollbackFor=AcsJException.class)
public abstract class StateArchiveAbstractImpl implements StateArchive {

    static final String NULL_ENTITY_ARG = "Input arg 'entity' is null";
    
    protected Logger logger;
    
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#initStateArchive(java.util.logging.Logger)
	 */
	@Override
	public void initStateArchive(Logger logger) {
		this.logger = logger;
	}
	
	/**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getOUSStatus(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public OUSStatus getOUSStatus( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	logger.severe("Null entity Id");
            throw new AcsJNullEntityIdEx();
        }
        
        OUSStatus ret = getOUSStatus( id.getEntityId() );
        return ret;
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.projectstatus.ProjectStatus)
     */
    @Override
    public void update( ProjectStatus projectStatusForUpdate ) throws AcsJStateIOFailedEx {

        if( projectStatusForUpdate == null ) {
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("projectStatusForUpdate is null"));
        }
        ProjectStatusEntityT projectStatusEntityT = projectStatusForUpdate.getProjectStatusEntity();
        if(projectStatusEntityT == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("projectStatusForUpdate.getProjectStatusEntity() is null" ));
        }
        String projectEntityId = projectStatusEntityT.getEntityId();
        if(projectEntityId == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("projectStatusForUpdate.getProjectStatusEntity().getEntityId() is null" ));
        }
        
        ObsProjectStatusPF projectStatusPF;
		try {
			projectStatusPF = new ObsProjectStatusPF( projectStatusForUpdate );
		} catch (ParseException e) {
			throw new AcsJStateIOFailedEx(e);
		}
        
        ObsProjectStatusPF existingObsProjectStatus = getObsProjectStatusPF(projectEntityId );
        if( existingObsProjectStatus == null ) {
        	insert(projectStatusPF);
        }else{
            update(projectStatusPF);
        }
    }
    
    /**
     * {@inheritDoc}
     * @throws AcsJNoSuchEntityEx 
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.ousstatus.OUSStatus)
     */
    @Override
    public void update( OUSStatus ouss ) 
        throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {

        if( ouss == null ) {
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("ouss is null"));
        }
        OUSStatusEntityT ousStatusEntityT = ouss.getOUSStatusEntity();
        if(ousStatusEntityT == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("ouss.getOUSStatusEntity() is null" ));
        }
        String ousEntityId = ousStatusEntityT.getEntityId();
        if(ousEntityId == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("ouss.getOUSStatusEntity().getEntityId() is null" ));
        }
                
        OUSStatusPF oussPF = getOUSStatusPF(ousEntityId);
        if( oussPF == null ) {
            
            // We need to insert a new OUSStatus into the Archive
            // Let's do some consistency checks first
            //--------------------------------------------------
            
            // Does this OUSStatus have a parent OUSStatus? 
            OUSStatusRefT parentRefT = ouss.getContainingObsUnitSetRef();
            String rootEntityId = ouss.getProjectStatusRef().getEntityId();
            ObsProjectStatusPF ps = getObsProjectStatusPF(rootEntityId);
            
            if( parentRefT == null || 
                parentRefT.getEntityId() == null ||
                parentRefT.getEntityId().length() == 0 ) {
                
                // NO, this OUSStatus does not have a parent OUSStatus, so it is
                // the top-level OUSStatus
                // Check if the root ProjectStatus is there

                if(ps == null){
                    String msg = "missing root ProjectStatus with entityId: " +
                                 rootEntityId;
                    logger.severe(msg);
                    throw new AcsJNoSuchEntityEx(
                            new IllegalArgumentException( msg ));
                }
            }
            else {
                // YES, this OUSStatus does have a parent OUSStatus
                // Check if the parent is there
                
                String parentEntityId = parentRefT.getEntityId();
                OUSStatusPF parent = getOUSStatusPF( parentEntityId );
                if( parent == null ) {
                    String msg = "missing parent OUSStatus with entityId: "
                            + parentEntityId;
                    logger.severe(msg);
                    throw new AcsJNoSuchEntityEx( new IllegalArgumentException(
                            msg ) );
                }
            }
        	
        	// Everything is OK, let's insert
        	OUSStatusPF ousStatusPF = 
        	    new OUSStatusPF( ouss, ps.getObsProjectId() );
        	insert(ousStatusPF);
        }
        else {
            // oussPF != null
            // The OUSStatus already exists in the Archive
            // We just update
            //--------------------------------------------------
        	OUSStatusPF ousStatusPF = 
        	    new OUSStatusPF( ouss, oussPF.getObsProjectId() );
            update(ousStatusPF);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#update(alma.entity.xmlbinding.sbstatus.SBStatus)
     */
    @Override
    public void update( SBStatus sbStatusForUpdate ) throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx {

        if( sbStatusForUpdate == null ) {
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("sbStatusForUpdate is null"));
        }
        SBStatusEntityT SBStatusEntityT = sbStatusForUpdate.getSBStatusEntity();
        if(SBStatusEntityT == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("sbStatusForUpdate.getSBStatusEntity() is null" ));
        }
        String SBEntityId = SBStatusEntityT.getEntityId();
        if(SBEntityId == null){
            throw new AcsJStateIOFailedEx(
            		new IllegalArgumentException("sbStatusForUpdate.getSBStatusEntity().getEntityId() is null" ));
        }
                
        SchedBlockStatusPF existingSBStatus = getSchedBlockStatusPF(SBEntityId);
        if( existingSBStatus == null ) {
        	String parentOusStatusEntityId = sbStatusForUpdate.getContainingObsUnitSetRef().getEntityId();
        	OUSStatusPF parentOusStatus = getOUSStatusPF(parentOusStatusEntityId);
        	if(parentOusStatus == null){
        		String msg = "missing parent OUSStatus with entityId: "	+ parentOusStatusEntityId;
        		logger.severe(msg);
        		throw new AcsJNoSuchEntityEx(new IllegalArgumentException(msg));
        	}
        	String obsProjectStatusEntityId = sbStatusForUpdate.getProjectStatusRef().getEntityId();
        	ObsProjectStatusPF obsProjectStatus = getObsProjectStatusPF(obsProjectStatusEntityId);
        	if(obsProjectStatus == null){
        		String msg = "missing root ProjectStatus with entityId: " + obsProjectStatusEntityId;
        		logger.severe(msg);
        		throw new AcsJNoSuchEntityEx(new IllegalArgumentException(msg));
        	}
        	
        	SchedBlockStatusPF sbStatusPF = new SchedBlockStatusPF( sbStatusForUpdate, obsProjectStatus.getObsProjectId() );
        	insert(sbStatusPF);
        }else{
        	SchedBlockStatusPF sbStatusPF = new SchedBlockStatusPF( sbStatusForUpdate, existingSBStatus.getObsProjectId() );
            update(sbStatusPF);
        }
    }
    /**
     * TODO -- make this a public method?
     */
    protected OUSStatus getOUSStatus( OUSStatusRefT ref )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        OUSStatus ret = getOUSStatus( ref.getEntityId() );
        return ret;
    }

    /**
     * Query the database.
     * 
     * @param id
     *            EntityId of an OUSStatus entity
     *            
     * @return The OUSStatus whose EntityId is equal to the input key, if
     *         any was found.
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     * 
     */
    protected OUSStatus getOUSStatus( String id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        String xml = getOUSStatusXml( id );
        OUSStatus ret = unmarshalOUSStatus( xml );
        return ret;
    }
    
    
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getOUSStatusXml(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public String getOUSStatusXml( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getOUSStatusXml( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected String getOUSStatusXml( OUSStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getOUSStatusXml( id.getEntityId() );
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of an OUSStatus entity
     * @return An XML representation of the OUSStatus whose EntityId is
     *         equal to the input key.
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * 
     */
    protected String getOUSStatusXml( String key ) 
        throws AcsJNoSuchEntityEx, AcsJNullEntityIdEx {

        OUSStatusPF entity = getOUSStatusPF( key );
        String ret = null;
        if( entity != null && entity.getXml() != null ) {
            ret = new String( entity.getXml() );
        }else{
    		String msg = "Could not find OUSStatus entity with id: " + key;
    		logger.severe(msg);
            throw new AcsJNoSuchEntityEx(new NoSuchEntityException(msg));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public ProjectStatus getProjectStatus( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx,
                   AcsJNoSuchEntityEx, 
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity Id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        ProjectStatus ret = getProjectStatus( id.getEntityId() );
        return ret;
    }

    /**
     * TODO -- make this public? 
     */
    protected ProjectStatus getProjectStatus( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx,
                   AcsJNoSuchEntityEx, 
                   AcsJInappropriateEntityTypeEx {
        
        ProjectStatus ret = getProjectStatus( id.getEntityId() );
        return ret;
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of a ProjectStatus entity
     * 
     * @return The ProjectStatus whose EntityId is equal to the input key.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected ProjectStatus getProjectStatus( String key )
            throws AcsJNullEntityIdEx,
                   AcsJNoSuchEntityEx, 
                   AcsJInappropriateEntityTypeEx {

    	String xml = getProjectStatusXml( key );
        ProjectStatus ret = unmarshalProjectStatus( xml );
        return ret;
    }

    /**
     * {@inheritDoc}
     * @throws AcsJEntitySerializationFailedEx 
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public StatusBaseT[] getProjectStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx, 
                   AcsJEntitySerializationFailedEx 
    {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
         
        StatusBaseT[] retVal;
		try {
			retVal = getProjectStatusListStatusBaseT( id.getEntityId() );
		} catch (EntityException e) {
			logger.severe(e.getMessage());
			throw new AcsJEntitySerializationFailedEx(e);
		}
        
        return retVal;
    }

    /**
     * TODO -- make this public?
     */
    public Object[] getProjectStatusList( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getProjectStatusList( id.getEntityId() );
    }

    /**
     * Look for the ProjectStatus entity with the given ID, return all
     * entities of the tree.
     * 
     * @param key
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return An array of status entities composing the entity tree whose root
     *         is the ProjectStatus, if it was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    protected Object[] getProjectStatusList( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        ProjectStatus project = getProjectStatus( key );
        List<OUSStatus> ouss = new ArrayList<OUSStatus>();
        List<SBStatus> sbs = new ArrayList<SBStatus>();
        walk( project, ouss, sbs );
        int numOUSs = ouss.size();
        int numSBs = sbs.size();
        int numStatusEntities = 1 + numOUSs + numSBs;
        Object[] ret = new Object[ numStatusEntities ];
        ret[0] = project;
        int index = 1;
        for( int i = 0; i < numOUSs; i++ ) {
            ret[index++] = ouss.get( i );
        }
        for( int i = 0; i < numSBs; i++ ) {
            ret[index++] = sbs.get( i );
        }
        return ret;
    }
    
    /**
     * Look for the ProjectStatus entity with the given ID, return all
     * entities of the tree.
     * 
     * @param key
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return An array of status entities composing the entity tree whose root
     *         is the ProjectStatus, if it was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     * @throws EntityException 
     */
    private StatusBaseT[] getProjectStatusListStatusBaseT( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx,
                   EntityException {
        
        ProjectStatus projectStatus = getProjectStatus( key );
        
        return getProjectStatusListStatusBaseT(projectStatus);
    }

    private StatusBaseT[] getProjectStatusListStatusBaseT(ProjectStatus projectStatus)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx, EntityException {

		List<OUSStatus> ouss = new ArrayList<OUSStatus>();
		List<SBStatus> sbs = new ArrayList<SBStatus>();
		walk(projectStatus, ouss, sbs);
		int numOUSs = ouss.size();
		int numSBs = sbs.size();
		int numStatusEntities = 1 + numOUSs + numSBs;

		StatusBaseT[] ret = new StatusBaseT[numStatusEntities];
		ret[0] = projectStatus;
		int index = 1;
		for (int i = 0; i < numOUSs; i++) {
			ret[index++] = ouss.get(i);
		}
		for (int i = 0; i < numSBs; i++) {
			ret[index++] = sbs.get(i);
		}
		return ret;
	}
    
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXml(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String getProjectStatusXml( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getProjectStatusXml( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected String getProjectStatusXml( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getProjectStatusXml( id.getEntityId() );
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of a ProjectStatus entity
     *            
     * @return An XML representation of the ProjectStatus whose EntityId is
     *         equal to the input key.
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     */
    protected String getProjectStatusXml( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        ObsProjectStatusPF entity = getObsProjectStatusPF( key );

        String ret = null;
        if( entity != null && entity.getXml() != null ) {
            ret = new String( entity.getXml() );
        }else{
    		String msg ="Could not find ProjectStatus entity with id: " + key;
    		logger.severe(msg);
            throw new AcsJNoSuchEntityEx(new NoSuchEntityException(msg));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String[] getProjectStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getProjectStatusXmlList( id.getEntityId() );
    }

    /**
     * TODO -- should this be public?
     */
    public String[] getProjectStatusXmlList( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getProjectStatusXmlList( id.getEntityId() );
    }

    /**
     * TODO
     */
    protected String[] getProjectStatusXmlList( String key )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx {
        
        String projectXml = getProjectStatusXml( key );
        ProjectStatus project = unmarshalProjectStatus( projectXml );
        List<String> entities = new ArrayList<String>();
        walk( project, entities );
        int numEntities = entities.size();
        int numStatusEntities = 1 + numEntities;
        String[] ret = new String[ numStatusEntities ];
        ret[0] = projectXml;
        int index = 1;
        for( int i = 0; i < numEntities; i++ ) {
            ret[index++] = entities.get( i );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatus(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)
     */
    @Override
    public SBStatus getSBStatus( SBStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        SBStatus ret = getSBStatus( id.getEntityId() );
        return ret;
    }

    /**
     * TODO -- make this public?
     */
    protected SBStatus getSBStatus( SBStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        SBStatus ret = getSBStatus( id.getEntityId() );
        return ret;
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of an SBStatus entity
     *            
     * @return The SBStatus whose EntityId is equal to the input key, if
     *         any was found.
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     * 
     */
    protected SBStatus getSBStatus( String key ) 
        throws AcsJNoSuchEntityEx, 
               AcsJNullEntityIdEx, 
               AcsJInappropriateEntityTypeEx {
        
        String xml = getSBStatusXml( key );
        SBStatus ret = unmarshalSBStatus( xml );
        return ret;
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public SBStatus[] getSBStatusList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getSBStatusOusList( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected SBStatus[] getSBStatusList( OUSStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getSBStatusOusList( id.getEntityId() );
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public SBStatus[] getSBStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getSBStatusProjectList( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected SBStatus[] getSBStatusList( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getSBStatusProjectList( id.getEntityId() );
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of an OUSStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ObsUnitSet whose
     *         EntityId is equal to the input key.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected SBStatus[] getSBStatusOusList( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        String[] xml = getSBStatusOusXmlList( key );
		List<SBStatus> sbStatusTmpList = new ArrayList<SBStatus>();
		for(String sbStatusXml: xml){
			try{
				sbStatusTmpList.add(unmarshalSBStatus(sbStatusXml));
			}catch(AcsJInappropriateEntityTypeEx ex){
                ex.printStackTrace();
                logger.severe("Error unmarshalling SBStatus XML: " + sbStatusXml);
			}
		}
        
        return sbStatusTmpList.toArray(new SBStatus[0]);
    }

    /**
     * Query the database.
     * 
     * @param ousEntityId
     *            EntityId of an OUSStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ObsUnitSet whose
     *         EntityId is equal to the input key. The entities are represented
     *         as XML text.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected String[] getSBStatusOusXmlList( String ousEntityId )
            throws AcsJNullEntityIdEx, 
                AcsJNoSuchEntityEx,
                AcsJInappropriateEntityTypeEx {

        List<SchedBlockStatusPF> results = getSchedBlockStatusPFList(ousEntityId);

        if( results == null || results.size() == 0 ) {
            throw new AcsJNoSuchEntityEx();
        }
        
        String[] ret = new String[results.size()];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = new String( results.get( i ).getXml() );
        }

        return ret;
    }
    
    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of a ProjectStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ProjectStatus
     *         whose EntityId is equal to the input key.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected SBStatus[] getSBStatusProjectList( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        Object[] temp = getProjectStatusList( key );
        List<SBStatus> ret = new ArrayList<SBStatus>();
        for( int i = 0; i < temp.length; i++ ) {
            if( temp[i] instanceof SBStatus ) {
                ret.add( (SBStatus) temp[i] );
            }
        }
        
        return ret.toArray( new SBStatus[0] );
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of a ProjectStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ProjectStatus
     *         whose EntityId is equal to the input key.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected String[] getSBStatusProjectXmlList( String key )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        String[] temp = getProjectStatusXmlList( key );
        List<String> ret = new ArrayList<String>();
        for( int i = 0; i < temp.length; i++ ) {
            // pretty rudimentary: if the XML text ends with an "</SBStatus>"
            // element we assume the text represents an SBStatus -- need to make
            // this much more robust. 
            // Should we simply un-marshal the string?
            // Yes, but what do un-marshal it as?
            // AMC, 06-Jul-2009
            String line = temp[i];
            if( line.trim().endsWith( "</SBStatus>" ) ) {
                ret.add( (String) temp[i] );
            }
        }
        
        return ret.toArray( new String[0] );
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXml(alma.entity.xmlbinding.sbstatus.SBStatusEntityT)
     */
    @Override
    public String getSBStatusXml( SBStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getSBStatusXml( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected String getSBStatusXml( SBStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getSBStatusXml( id.getEntityId() );
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of an SBStatus entity
     *            
     * @return An XML representation of the SBStatus whose EntityId is equal to
     *         the input key.
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     */
    protected String getSBStatusXml( String key ) 
        throws AcsJNoSuchEntityEx, AcsJNullEntityIdEx {
        
        SchedBlockStatusPF entity  = getSchedBlockStatusPF( key );
        
        String ret = null;
        if( entity != null && entity.getXml() != null ) {
            ret = new String( entity.getXml() );
        }else{
    		String msg ="Could not find SBStatus entity with id: " + key;
    		logger.severe(msg);
            throw new AcsJNoSuchEntityEx(new NoSuchEntityException(msg));
        }
        return ret;
    }

    
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.ousstatus.OUSStatusEntityT)
     */
    @Override
    public String[] getSBStatusXmlList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {

        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }

        return getSBStatusOusXmlList( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected String[] getSBStatusXmlList( OUSStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {

        return getSBStatusOusXmlList( id.getEntityId() );
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
     */
    @Override
    public String[] getSBStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        if( id == null ) {
        	String msg = "Null entity id";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        return getSBStatusProjectXmlList( id.getEntityId() );
    }

    /**
     * TODO -- make this public?
     */
    protected String[] getSBStatusXmlList( ProjectStatusRefT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
        
        return getSBStatusProjectXmlList( id.getEntityId() );
    }

    /**
     * @throws AcsJInappropriateEntityTypeEx if un-marshaling failed
     */
    protected OUSStatus unmarshalOUSStatus( String xml )
            throws AcsJInappropriateEntityTypeEx {
        StringReader reader = new StringReader( xml );
        
        try {
            OUSStatus ret = 
                OUSStatus.unmarshalOUSStatus( reader );
            return ret;
        }
        catch( MarshalException e ) {
            throw new AcsJInappropriateEntityTypeEx( e );
        }
        catch( ValidationException e ) {
            throw new AcsJInappropriateEntityTypeEx( e );
        }
    }

    /**
     * @throws AcsJInappropriateEntityTypeEx if un-marshaling failed
     */
    protected ProjectStatus unmarshalProjectStatus( String xml )
            throws AcsJInappropriateEntityTypeEx {
        StringReader reader = new StringReader( xml );
        try {
            ProjectStatus ret = ProjectStatus.unmarshalProjectStatus( reader );
            return ret;
        }
        catch( MarshalException e ) {
            throw new AcsJInappropriateEntityTypeEx( e );
        }
        catch( ValidationException e ) {
            throw new AcsJInappropriateEntityTypeEx( e );
        }
    }

    /**
     * @throws AcsJInappropriateEntityTypeEx if un-marshaling failed
     */
    protected SBStatus unmarshalSBStatus( String xml )
            throws AcsJInappropriateEntityTypeEx {
        StringReader reader = new StringReader( xml );
        try {
            SBStatus ret = SBStatus.unmarshalSBStatus( reader );
            return ret;
        }
        catch( MarshalException e ) {
            throw new AcsJInappropriateEntityTypeEx( e );
        }
        catch( ValidationException e ) {
            throw new AcsJInappropriateEntityTypeEx();
        }
    }

    /**
     * Walk down the input OUSStatus, collecting all child elements along
     * the way.
     * 
     * @param ouss
     *            The OUSStatus to visit.
     * @param oussList
     *            This list will be populated with all OUSStatus entities
     *            directly or indirectly connected to the input ProjectStatus
     * @param sbsList
     *            This list will be populated with all SBStatus entities
     *            eventually connected to the input ProjectStatus
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg ouss is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected void walk( OUSStatus ouss,
                         List<OUSStatus> oussList, 
                         List<SBStatus> sbsList ) 
        throws AcsJNullEntityIdEx, 
               AcsJNoSuchEntityEx, 
               AcsJInappropriateEntityTypeEx {
        
        OUSStatusChoice choice = ouss.getOUSStatusChoice();
        if(choice!=null){
	        OUSStatusRefT[] ousStatuses = choice.getOUSStatusRef();
	        
	        if( ousStatuses.length > 0 ) {
	            for( int i = 0; i < ousStatuses.length; i++ ) {
	                OUSStatusRefT ref = ousStatuses[i];
	                OUSStatus child = getOUSStatus( ref );
	                oussList.add( child );
	                walk( child, oussList, sbsList );
	            }
	            return;
	        }
	        
	        SBStatusRefT[] sbStatuses = choice.getSBStatusRef();
	        for( int i = 0; i < sbStatuses.length; i++ ) {
	            SBStatusRefT ref = sbStatuses[i];
	            try{
	            	SBStatus child = getSBStatus( ref );
	            	sbsList.add( child );
	            }
	            catch(AcsJNoSuchEntityEx ex) {
	            	// We log and continue to look for SB status entities
	            	logger.warning("Error when retrieving SB status entity from the archive");
	            	logger.warning(ex.getMessage());	            	
	            }	
	        }
        }
    }

    /**
     * Walk down the input OUSStatus, collecting all child elements along
     * the way.
     * 
     * @param ouss
     *            The OUSStatus to visit
     * @param entities
     *            This list will be populated with all OUSStatus and
     *            SBStatus entities directly or indirectly connected to the
     *            input ProjectStatus; entities will be represented as XML text
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg ouss is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected void walk( OUSStatus ouss, List<String> entities ) 
        throws AcsJNullEntityIdEx, 
               AcsJNoSuchEntityEx, 
               AcsJInappropriateEntityTypeEx {
        
        OUSStatusChoice choice = ouss.getOUSStatusChoice();
        OUSStatusRefT[] ousStatuses = choice.getOUSStatusRef();
        
        if( ousStatuses.length > 0 ) {
            for( int i = 0; i < ousStatuses.length; i++ ) {
                OUSStatusRefT ref = ousStatuses[i];
                String childXml = getOUSStatusXml( ref );
                OUSStatus child = unmarshalOUSStatus( childXml );
                entities.add( childXml );
                walk( child, entities );
            }
            return;
        }
        
        SBStatusRefT[] sbStatuses = choice.getSBStatusRef();
        for( int i = 0; i < sbStatuses.length; i++ ) {
            SBStatusRefT ref = sbStatuses[i];
            String childXml = getSBStatusXml( ref );
            entities.add( childXml );
        }
    }

    /**
     * Walk down the input ProjectStatus, collecting all child elements along
     * the way.
     * 
     * @param ps
     *            The ProjectStatus to visit.
     * @param oussList
     *            This list will be populated with all OUSStatus entities
     *            directly or indirectly connected to the input ProjectStatus
     * @param sbsList
     *            This list will be populated with all SBStatus entities
     *            eventually connected to the input ProjectStatus
     *         
     * @throws AcsJNullEntityIdEx
     *             if input arg ouss is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected void walk( ProjectStatus ps, 
                         List<OUSStatus> oussList,
                         List<SBStatus> sbsList ) 
        throws AcsJNullEntityIdEx, 
               AcsJNoSuchEntityEx, 
               AcsJInappropriateEntityTypeEx {
        
        OUSStatusRefT progRef = ps.getObsProgramStatusRef();
        OUSStatus prog = getOUSStatus( progRef.getEntityId() );
        oussList.add( prog );
        walk( prog, oussList, sbsList );
    }

    /**
     * Walk down the input ProjectStatus, collecting all child elements along
     * the way.
     * 
     * @param ps
     *            The ProjectStatus to visit
     * @param entities
     *            This list will be populated with all OUSStatus and
     *            SBStatus entities directly or indirectly connected to the
     *            input ProjectStatus; entities will be represented as XML text
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg ouss is null
     * @throws AcsJNoSuchEntityEx
     *             if no entity with that key was found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    protected void walk( ProjectStatus ps, List<String> entities ) 
        throws AcsJNullEntityIdEx, 
               AcsJNoSuchEntityEx, 
               AcsJInappropriateEntityTypeEx {
        
        OUSStatusRefT progRef = ps.getObsProgramStatusRef();
        String progXml = getOUSStatusXml( progRef );
        OUSStatus prog = unmarshalOUSStatus( progXml );
        entities.add( progXml );
        walk( prog, entities );
    }
   
    /**
     * @param statusEntityId
     * @return an OUSStatusPF for the given obsUnitSetStatusPf statusEntityId
     */
    protected abstract OUSStatusPF getOUSStatusPF( String statusEntityId );
    
    /**
     * @param statusEntityId
     * @return an ObsProjectStatusPF for the given ObsProjectStatusPF statusEntityId 
     */
    protected abstract ObsProjectStatusPF getObsProjectStatusPF( String statusEntityId );

    /**
     * @param statusEntityId
     * @return an SchedBlockStatusPF for the given SchedBlockStatusPF statusEntityId
     */
    protected abstract SchedBlockStatusPF getSchedBlockStatusPF( String statusEntityId );

    /**
     * @param parentOusEntityId
     * @return a list of SchedBlockStatusPF for the given parentOusEntityId
     */
    protected abstract List<SchedBlockStatusPF> getSchedBlockStatusPFList( String parentOusEntityId);

    /**
     * Updates an OUSStatusPF
     * @param updatedOusStatusPf
     */
    protected abstract void update(OUSStatusPF updatedOusStatusPf) throws AcsJStateIOFailedEx;
    
    /**
     * Updates an ObsProjectStatusPF
     * @param updatedObsProjectStatusPf
     */
    protected abstract void update(ObsProjectStatusPF updatedObsProjectStatusPf) throws AcsJStateIOFailedEx;
    
    /**
     * Updates an SchedBlockStatusPF
     * @param schedBlockStatusPF
     */
    protected abstract void update(SchedBlockStatusPF schedBlockStatusPF) throws AcsJStateIOFailedEx;
    
    /**
     * Inserts an OUSStatusPF
     * @param updatedOusStatusPf
     */
    protected abstract void insert(OUSStatusPF updatedOusStatusPf) throws AcsJStateIOFailedEx;
    
    /**
     * Inserts an ObsProjectStatusPF
     * @param updatedObsProjectStatusPf
     */
    protected abstract void insert(ObsProjectStatusPF updatedObsProjectStatusPf) throws AcsJStateIOFailedEx;
    
    /**
     * Inserts an SchedBlockStatusPF
     * @param schedBlockStatusPF
     */
    protected abstract void insert(SchedBlockStatusPF schedBlockStatusPF) throws AcsJStateIOFailedEx;

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@Override
	public StatusBaseT[] getProjectStatusList(ObsProjectEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx {

        if( id == null || id.getEntityId() == null ) {
        	String msg = "Null ObsProjectEntityT";
            logger.severe(msg);
            throw new AcsJNullEntityIdEx( new IllegalArgumentException(msg));
        }
        
        StatusBaseT[] projectStatusList = null;

		// TODO: optimize this code so it doesn't do 2 queries to get a ProjectStatus 
        
		ObsProjectStatusPF obsProjectStatusPF = getProjectStatusPF(id);

		// Some legacy projects may not have status entities
		if(obsProjectStatusPF != null){
			
			try {
				projectStatusList = getProjectStatusListStatusBaseT(obsProjectStatusPF.getProjectStatusId());
			} catch (EntityException e) {
				throw new AcsJEntitySerializationFailedEx(e);
			}
		}
		
		return projectStatusList;
	}
	
	/**
	 * Gets a project status given its obsProject entity id
	 */
	protected abstract ObsProjectStatusPF getProjectStatusPF(ObsProjectEntityT obsProjectId);
}
