/**
 * Copyright European Southern Observatory 2009
 */
package alma.lifecycle.persistence;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.Utils;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * An in-memory mock-up implementation of the StateArchive interface, for
 * test purposes. 
 * 
 * References the entities created by {@link Utils}
 *
 * @author rkurowsk, Jun 10, 2009
 * @version $Revision: 1.7 $
 */

// $Id: StateArchiveMockImpl.java,v 1.7 2011/02/03 10:28:56 rkurowsk Exp $

public class StateArchiveMockImpl implements StateArchive {

    protected Logger logger;
    
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#initStateArchive(java.util.logging.Logger)
	 */
	@Override
	public void initStateArchive(Logger logger) {
		this.logger = logger;
		
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatus(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public ProjectStatus getProjectStatus(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
		return Utils.findProjectStatus(id.getEntityId());
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public StatusBaseT[] getProjectStatusList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {

        ProjectStatus ps = Utils.findProjectStatus( id.getEntityId() );
        if( ps == null ) {
            return null;
        }

        List<OUSStatus> oussList = new ArrayList<OUSStatus>();
        List<SBStatus> sbsList = new ArrayList<SBStatus>();
        Utils.walk( ps, oussList, sbsList );
        
        int num = oussList.size() + sbsList.size() + 1;
        
//        EntitySerializer es = EntitySerializer.getEntitySerializer( logger );
        StatusBaseT[] ret = new StatusBaseT[num];

        ret[0] = ps;

        int i = 1;
        for( OUSStatus status : oussList ) {
            ret[i++] = status;
        }
        for( SBStatus status : sbsList ) {
            ret[i++] = status;
        }
        
        
        return ret;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXml(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public String getProjectStatusXml(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
	    
	    ProjectStatus tmp = getProjectStatus( id );
	    if( tmp == null ) {
	        return null;
	    }
	    
	    return marshall( tmp );
	}

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws RuntimeException if anything goes wrong
     */
    protected String marshall( ProjectStatus entity ) {
        StringWriter w = new StringWriter();
        try {
            entity.marshal( w );
        }
        catch( Exception e ) {
            // ugly error handling, yes, but this is a mock implementation
            e.printStackTrace();
            throw new RuntimeException( e );
        }
        return w.toString();
    }

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws RuntimeException if anything goes wrong
     */
    protected String marshall( OUSStatus entity ) {
        StringWriter w = new StringWriter();
        try {
            entity.marshal( w );
        }
        catch( Exception e ) {
            // ugly error handling, yes, but this is a mock implementation
            e.printStackTrace();
            throw new RuntimeException( e );
        }
        return w.toString();
    }

    /**
     * Wrapper around the usual marshall() method. 
     * @return The input entity converted to XML
     * @throws RuntimeException if anything goes wrong
     */
    protected String marshall( SBStatus entity ) {
        StringWriter w = new StringWriter();
        try {
            entity.marshal( w );
        }
        catch( Exception e ) {
            // ugly error handling, yes, but this is a mock implementation
            e.printStackTrace();
            throw new RuntimeException( e );
        }
        return w.toString();
    }

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getProjectStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public String[] getProjectStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {
	    
	    ProjectStatus ps = Utils.findProjectStatus( id.getEntityId() );
	    if( ps == null ) {
	        return null;
	    }

        List<OUSStatus> oussList = new ArrayList<OUSStatus>();
        List<SBStatus> sbsList = new ArrayList<SBStatus>();
        Utils.walk( ps, oussList, sbsList );
        
        int num = oussList.size() + sbsList.size() + 1;

        String[] ret = new String[num];
        ret[0] = marshall( ps );
	    
	    int i = 1;
	    for( OUSStatus status : oussList ) {
            ret[i++] = marshall( status ); 
        }
	    for( SBStatus status : sbsList ) {
            ret[i++] = marshall( status ); 
        }
	    
		return ret;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getOUSStatus(alma.entity.xmlbinding.projectstatus.OUSStatusEntityT)
	 */
	@Override
	public OUSStatus getOUSStatus(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {

		return Utils.findOUSStatus(id.getEntityId());
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getOUSStatusXml(alma.entity.xmlbinding.projectstatus.OUSStatusEntityT)
	 */
	@Override
	public String getOUSStatusXml(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OUSStatus/>";
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatus(alma.entity.xmlbinding.projectstatus.SBStatusEntityT)
	 */
	@Override
	public SBStatus getSBStatus(SBStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {

		return Utils.findSBStatus(id.getEntityId());
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
    public SBStatus[] getSBStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {

        ProjectStatus ps = Utils.findProjectStatus( id.getEntityId() );
        if( ps == null ) {
            return null;
        }

        List<OUSStatus> oussList = new ArrayList<OUSStatus>();
        List<SBStatus> sbsList = new ArrayList<SBStatus>();
        Utils.walk( ps, oussList, sbsList );
        
        int num = sbsList.size();
        SBStatus[] ret = new SBStatus[num];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = sbsList.get( i );
        }
        
        return ret;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatusList(alma.entity.xmlbinding.projectstatus.OUSStatusEntityT)
	 */
	@Override
    public SBStatus[] getSBStatusList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx {

	    OUSStatus ouss = Utils.findOUSStatus( id.getEntityId() );
        if( ouss == null ) {
            return null;
        }

        List<OUSStatus> oussList = new ArrayList<OUSStatus>();
        List<SBStatus> sbsList = new ArrayList<SBStatus>();
        Utils.walk( ouss, oussList, sbsList );
        
        int num = sbsList.size();
        SBStatus[] ret = new SBStatus[num];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = sbsList.get( i );
        }
        
        return ret;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatusXml(alma.entity.xmlbinding.projectstatus.SBStatusEntityT)
	 */
	@Override
	public String getSBStatusXml(SBStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SBStatus/>";
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public String[] getSBStatusXmlList(ProjectStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
		return null;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#getSBStatusXmlList(alma.entity.xmlbinding.projectstatus.OUSStatusEntityT)
	 */
	@Override
	public String[] getSBStatusXmlList(OUSStatusEntityT id)
			throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
			AcsJInappropriateEntityTypeEx {
		return null;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#put(alma.entity.xmlbinding.projectstatus.ProjectStatus)
	 */
	@Override
	public void update(ProjectStatus entity) throws AcsJStateIOFailedEx {
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#put(alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.projectstatus.OUSStatus[], alma.entity.xmlbinding.projectstatus.SBStatus[])
	 */
	@Override
	public void insert(ProjectStatus obsProject,
			OUSStatus[] obsUnitSets, SBStatus[] schedBlocks)
			throws AcsJStateIOFailedEx {
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#put(alma.entity.xmlbinding.projectstatus.OUSStatus)
	 */
	@Override
	public void update(OUSStatus entity) throws AcsJStateIOFailedEx {
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#put(alma.entity.xmlbinding.projectstatus.SBStatus)
	 */
	@Override
	public void update(SBStatus entity) throws AcsJStateIOFailedEx {
	}

    /**
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.lifecycle.persistence.domain.StateChangeRecord)
     */
    @Override
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx {
        // no-op
    }

    /**
     * @see StateArchive#findStateChangeRecords(Date, Date, String, String, String, String)
     */
    @Override
    public List<StateChangeRecord> findStateChangeRecords(
           Date start,
           Date end,
           String domainEntityId,
           String state,
           String userId,
           StateEntityType type )
            throws AcsJStateIOFailedEx {
        return new LinkedList<StateChangeRecord>();
    }


    
	public StatusBaseT[] getProjectStatusList(ObsProjectEntityT id)
		throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
		AcsJInappropriateEntityTypeEx, AcsJEntitySerializationFailedEx {
		
        ProjectStatus ps = Utils.findProjectStatus( id);
       
        return getProjectStatusList(ps.getProjectStatusEntity());
	}

	@Override
	public void lockProjectStatus(ProjectStatusEntityT projectStatusId)
			throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx{
			//no opp
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findProjectStatusByState(java.lang.String[])
	 */
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states)
			throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findSBStatusByState(java.lang.String[])
	 */
	@Override
	public SBStatus[] findSBStatusByState(String[] states)
			throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public void storeArchiveEntities(XmlEntityStruct[] archiveEntities,
			String user, Logger logr) {
		// TODO Auto-generated method stub
		
	}

}
