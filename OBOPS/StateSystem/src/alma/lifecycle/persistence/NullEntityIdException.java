/**
 * NullEntityIdException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence;

/**
 * Thrown if an EntityId argument is null.
 *
 * @author dclarke, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

public class NullEntityIdException extends Exception {

    private static final long serialVersionUID = -7719136847572841309L;

    public NullEntityIdException() {
        super();
    }

    public NullEntityIdException( String message, Throwable cause ) {
        super( message, cause );
    }

    public NullEntityIdException( String message ) {
        super( message );
    }

    public NullEntityIdException( Throwable cause ) {
        super( cause );
    }
}
