/**
 * NullEntityIdException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence;

/**
 * Thrown if anything want wrong during a State Archive read/write operation.
 * 
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

public class StateIOException extends Exception {

    private static final long serialVersionUID = -9131780366387989163L;

    public StateIOException() {
        super();
    }

    public StateIOException( String message, Throwable cause ) {
        super( message, cause );
    }

    public StateIOException( String message ) {
        super( message );
    }

    public StateIOException( Throwable cause ) {
        super( cause );
    }
}
