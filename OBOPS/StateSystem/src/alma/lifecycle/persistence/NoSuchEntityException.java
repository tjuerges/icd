/**
 * NullEntityIdException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence;

/**
 * Thrown if there is no entity in the Archive matching a supplied EntityId
 * argument.
 * 
 * @author dclarke, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

public class NoSuchEntityException extends Exception {

    private static final long serialVersionUID = 2801265896944097181L;

    public NoSuchEntityException() {
        super();
    }

    public NoSuchEntityException( String message, Throwable cause ) {
        super( message, cause );
    }

    public NoSuchEntityException( String message ) {
        super( message );
    }

    public NoSuchEntityException( Throwable cause ) {
        super( cause );
    }
}
