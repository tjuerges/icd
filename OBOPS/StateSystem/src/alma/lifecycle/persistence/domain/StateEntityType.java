/**
 * Copyright European Southern Observatory 2010
 */

package alma.lifecycle.persistence.domain;

/**
 * StateEntityType enum
 * 
 * @author rkurowski, March 12, 2010
 * @version $Revision$
 */

// $Id$

public enum StateEntityType {
	PRJ, OUT, SBK;
}