/**
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Abstract superclass to represent classes that extend ObsUnitStatusT i.e. OUSStatus & SBStatus
 *
 * @author rkurowsk, Aug 28, 2009
 * @version $Revision$
 */

// $Id$

@MappedSuperclass
public abstract class AbstractOusStatusPF extends AbstractStatusPF {

    /**
	 * Total required time in seconds
	 */
	@Column(name = "total_required_time_in_sec")
	protected int totalRequiredTimeInSec;

	/**
	 * Total used time in seconds
	 */
	@Column(name = "total_used_time_in_sec")
	protected int totalUsedTimeInSec;
     
  
	public int getTotalRequiredTimeInSec() {
		return totalRequiredTimeInSec;
	}

	public void setTotalRequiredTimeInSec(int totalRequiredTimeInSec) {
		this.totalRequiredTimeInSec = totalRequiredTimeInSec;
	}

	public int getTotalUsedTimeInSec() {
		return totalUsedTimeInSec;
	}

	public void setTotalUsedTimeInSec(int totalUsedTimeInSec) {
		this.totalUsedTimeInSec = totalUsedTimeInSec;
	}

}
