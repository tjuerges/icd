/**
 * StateChangeRecord.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;

/**
 * A record of this class gets created whenever a domain entity (e.g. a
 * SchedBlock) changes state.
 * 
 * State changes are timestamped.
 * 
 * @author amchavan, Jul 7, 2009
 * @version $Revision$
 */

// $Id$

@Entity
@Table(name="state_changes")
public class StateChangeRecord {
	
    /**
     * This class' natural ID
     */
    @Id
    @Column(name="state_changes_id")
    @Generated(value="native")
    protected Long id;

    /**
     * Reference to the status entity corresponding to the domain entity for
     * which we record a state change; e.g. an SBStatus
     */
    @Column(name="status_entity_id")
    protected String statusEntityId;
    
    /**
     * Reference to the domain entity for which we record a state change; e.g.
     * a SchedBlock
     */
    @Column(name="domain_entity_id")
    protected String domainEntityId;
    
    /**
     * Reference to the domain entity part id for which we record a state change.
     * Only applicable for ObsUnitSets
     */
    @Column(name="domain_part_id")
    protected String domainPartId;

    /**
     * The state of the domain entity.
     * 
     * It's a value from either the StatusTStateType, StatusTStateType or
     * StatusTStateType enumerations.
     */
    @Column(name="domain_entity_state")
    protected String domainEntityState;
    
    /** Time and date of the state change -- UTC */
    @Column(name="timestamp")
    protected Date timestamp;

    /**
     * Location where the state change was originally recorded; e.g. "OSF" 
     */
    @Column(name="location")
    protected String location;

    /**
     * ID of the user that initiated the state change. 
     */
    @Column(name="user_id")
    protected String userId;

    /**
     * Subsystem that initiated the state change. 
     */
    @Column(name="subsystem")
    protected String subsystem;
    
    /**
     * Role of the user that initiated the state change. 
     */
    @Column(name="info")
    protected String info;

    /**
     * Type of the domain entity; should be one of the types in
     * {@linkplain #ENTITY_TYPES}
     */
    @Column(name="entity_type")
    @Enumerated(EnumType.STRING)
    protected StateEntityType entityType;
    
    /** Default constructor */
    public StateChangeRecord() {
        // TODO -- fix this hack
        //         should be using the generated values
        Double t = (Math.random() * 10000000000L);
        this.id = t.longValue();
    }

    /**
     * Constructor.
     * 
     * @param statusEntityId
     *            Reference to the status entity corresponding to the domain
     *            entity for which we record a state change; e.g. an SBStatus
     * 
     * @param domainEntityId
     *            Reference to the domain entity for which we record a state
     *            change; e.g. a SchedBlock
     * 
     * @param domainEntityState
     *            The state of the domain entity. It's a value from either the
     *            StatusTStateType, StatusTStateType or StatusTStateType
     *            enumerations
     * 
     * @param timestamp
     *            Time and date of the state change -- UTC
     * 
     * @param location
     *            Location where the state change was originally recorded; e.g.
     *            "OSF"
     * 
     * @param userId
     *            ID of the user that initiated the state change
     * 
     * @param entityType
     *            Type of the domain entity; should be one of the types in
     *            {@linkplain #ENTITY_TYPES}
     * 
     * @throws IllegalArgumentException
     *             if some input argument is illegal
     */
    public StateChangeRecord( String domainEntityId,
    			      String domainPartId,
                              String statusEntityId, 
                              String domainEntityState, 
                              Date timestamp, 
                              String location,
                              String userId,
                              String subsystem,
                              String info, 
                              StateEntityType entityType ) {
        this();
        this.statusEntityId = statusEntityId;
        this.domainEntityId = domainEntityId;
        this.domainPartId = domainPartId;
        this.domainEntityState = domainEntityState;
        this.timestamp = timestamp;
        this.location = location;
        this.userId = userId;
        this.info = info;
        this.subsystem = subsystem;
        this.entityType = entityType;
    }

    /**
     * Same constructor as above but takes: AbstractStatusPF
     * 
     * @param statusPf
     * @param timestamp
     * @param location
     * @param userId
     * @param subsystem
     * @param info
     * @param entityType
     */
    public StateChangeRecord(AbstractStatusPF statusPf, 
            Date timestamp, 
            String location,
            String userId,
            String subsystem,
            String info, 
            StateEntityType entityType ) {
    	
    	this(statusPf.getDomainEntityId(),
    			null,
    			statusPf.statusEntityId, 
    			statusPf.domainEntityState, 
    			timestamp, location, userId, 
    			subsystem, info, entityType);
    	
    	// OUSStatus hack required as the OUS's part_id is stored in the 
    	// OUSStatusPF domain_entity_id.
    	if(statusPf instanceof OUSStatusPF){
    		OUSStatusPF ousStatuPf = (OUSStatusPF)statusPf;
    		this.domainEntityId = ousStatuPf.getObsProjectId();
    		this.domainPartId = ousStatuPf.getDomainEntityId();
    	}
    
    }
    
    /**
     * Constructor that takes ProjectStatus castor class.
     * 
     * @param projectStatus
     * @param timestamp
     * @param location
     * @param userId
     * @param subsystem
     * @param info
     */
    public StateChangeRecord(ProjectStatus projectStatus, 
            Date timestamp, 
            String location,
            String userId,
            String subsystem,
            String info) {
    	
    	this(projectStatus.getObsProjectRef().getEntityId(), null,
    			projectStatus.getProjectStatusEntity().getEntityId(), 
    			projectStatus.getStatus().getState().toString(), 
    			timestamp, location, userId, 
    			subsystem, info, StateEntityType.PRJ);
    }
    
    /**
     * Constructor that takes OUSStatus castor class.
     * 
     * @param sbStatus
     * @param timestamp
     * @param location
     * @param userId
     * @param subsystem
     * @param info
     */
    public StateChangeRecord(OUSStatus ousStatus, 
            Date timestamp, 
            String location,
            String userId,
            String subsystem,
            String info) {
    	
    	this(ousStatus.getObsUnitSetRef().getEntityId(), 
    			ousStatus.getObsUnitSetRef().getPartId(), 
    			ousStatus.getOUSStatusEntity().getEntityId(), 
    			ousStatus.getStatus().getState().toString(), 
    			timestamp, location, userId, 
    			subsystem, info, StateEntityType.OUT);
    }
    
    /**
     * Constructor that takes SBStatus castor class.
     * 
     * @param sbStatus
     * @param timestamp
     * @param location
     * @param userId
     * @param subsystem
     * @param info
     */
    public StateChangeRecord(SBStatus sbStatus, 
            Date timestamp, 
            String location,
            String userId,
            String subsystem,
            String info) {
    	
    	this(sbStatus.getSchedBlockRef().getEntityId(), null,
    			sbStatus.getSBStatusEntity().getEntityId(), 
    			sbStatus.getStatus().getState().toString(), 
    			timestamp, location, userId, 
    			subsystem, info, StateEntityType.SBK);
    
    }
    
    /**
     * @return Reference to the domain entity for which we record a state change; e.g.
     * a SchedBlock
     */
    public String getDomainEntityId() {
        return domainEntityId;
    }

    /**
     * @return The state of the domain entity. It's a value from either the
     *         StatusTStateType, StatusTStateType or StatusTStateType enumerations.
     */
    public String getDomainEntityState() {
        return domainEntityState;
    }

    /**
     * @return Type of the domain entity
     */
    public StateEntityType getEntityType() {
        return entityType;
    }

    /**
     * @return This class' natural ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @return Location where the state change was originally recorded; e.g.
     *         "OSF"
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return Info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @return Reference to the status entity corresponding to the domain entity
     *         for which we record a state change; e.g. a SBStatus
     */
    public String getStatusEntityId() {
        return statusEntityId;
    }

    /**
     * @return Time and date of the state change -- UTC
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @return ID of the user that initiated the state change.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param domainEntityId
     *            Reference to the domain entity for which we record a state
     *            change; e.g. a SchedBlock
     */
    public void setDomainEntityId( String domainEntityId ) {
        this.domainEntityId = domainEntityId;
    }

    /**
     * @param domainEntityState
     *            The state of the domain entity.
     *            It's a value from either the StatusTStateType, StatusTStateType
     *            or StatusTStateType enumerations.
     */
    public void setDomainEntityState( String domainEntityState ) {
        this.domainEntityState = domainEntityState;
    }

    /**
     * @param entityType
     *            Type of the domain entity
     */
    public void setEntityType( StateEntityType entityType ) {
        this.entityType = entityType;
    }

    /**
     * @param id
     *            This class' natural ID
     */
    public void setId( Long id ) {
        this.id = id;
    }

    /**
     * @param location
     *            Location where the state change was originally recorded; e.g.
     *            "OSF"
     */
    public void setLocation( String location ) {
        this.location = location;
    }

    /**
     * @param info
     */
    public void setInfo( String info ) {
        this.info = info;
    }

    /**
     * @param statusEntityId
     *            Reference to the status entity corresponding to the domain
     *            entity for which we record a state change; e.g. a SBStatus
     */
    public void setStatusEntityId( String statusEntityId ) {
        this.statusEntityId = statusEntityId;
    }

    /**
     * @param timestamp
     *            Time and date of the state change -- UTC
     */
    public void setTimestamp( Date timestamp ) {
        this.timestamp = timestamp;
    }

    /**
     * @param userId
     *            ID of the user that initiated the state change.
     */
    public void setUserId( String userId ) {
        this.userId = userId;
    }

	public String getSubsystem() {
		return subsystem;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	/**
	 * @return the domainPartId
	 */
	public String getDomainPartId() {
		return domainPartId;
	}

	/**
	 * @param domainPartId the domainPartId to set
	 */
	public void setDomainPartId(String domainPartId) {
		this.domainPartId = domainPartId;
	}
	
}
