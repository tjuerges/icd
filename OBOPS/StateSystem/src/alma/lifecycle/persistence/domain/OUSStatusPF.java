/**
 * OUSStatusPF.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import java.io.StringReader;
import java.io.StringWriter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;

import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.hibernate.util.HibernateXmlType;

/**
 * Implementation of the {@linkplain StatusPersistenceFacade} interface to
 * provide a facade for {@linkplain OUSStatus} instances.
 * 
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

@Entity
@Table(name="obs_unit_set_status")
@TypeDef(name = "xmltype", typeClass = HibernateXmlType.class)
public class OUSStatusPF extends AbstractOusStatusPF {

    /** Reference to the parent OUSStatus instance */
    @Column(name="parent_obs_unit_set_status_id")
    protected String parentOusStatusId = null;

    /** Default constructor */
    public OUSStatusPF() {
        // no-op
    }

    /**
     * Constructor.
     * 
     * @param status
     *            The status entity to provide a facade for
     * 
     * @param obsProjectId
     *            A reference to the original ObsProject domain entity
     * 
     * @throws IllegalArgumentException
     *             If input arg status is <code>null</code>
     */
    public OUSStatusPF( OUSStatus status, String obsProjectId )
        throws IllegalArgumentException {
        
        // Check input args
        if( status == null ) {
            throw new IllegalArgumentException( "Input OUSStatus is null" );
        }
       
        if(status.getObsUnitSetRef() == null){
        	throw new IllegalArgumentException( "Input OUSStatus.getObsUnitSetRef() is null" );
        }
        	
        if(status.getObsUnitSetRef().getPartId() == null){
        	throw new IllegalArgumentException( "Input OUSStatus.getObsUnitSetRef().getPartId() is null" );
        }        	
        
        if(status.getProjectStatusRef().getEntityId() == null){
        	throw new IllegalArgumentException( "Input OUSStatus.getProjectStatusRef().getEntityId() is null" );
        }        	
        
        // Populate the fields of this instance
        setXml( marshal( status ));
        setStatusEntityId( status.getOUSStatusEntity().getEntityId() );

        // for OUSStatusPF were putting the OUS partId into the DomainEntityId field. 
        setDomainEntityId(status.getObsUnitSetRef().getPartId());
        setDomainEntityState( status.getStatus().getState().toString() );
        setObsProjectId( obsProjectId );
        setProjectStatusId(status.getProjectStatusRef().getEntityId());
        if(status.getContainingObsUnitSetRef() != null){
        	this.parentOusStatusId = status.getContainingObsUnitSetRef().getEntityId();
        }
        
        setTotalRequiredTimeInSec(status.getTotalRequiredTimeInSec());
        setTotalUsedTimeInSec(status.getTotalUsedTimeInSec());
        
    }

    /**
     * @return A reference to the parent OUSStatus
     */
    public String getParentOusStatusId() {
        return this.parentOusStatusId;
    }

    /**
     * @see alma.lifecycle.persistence.domain.StatusPersistenceFacade#getStatusEntity()
     * @return An instance of {@linkplain OUSStatus}
     */
    @Override
    public Object getStatusEntity() {
        StringReader reader = new StringReader( getXml() );
        OUSStatus ret;
        try {
            ret = OUSStatus.unmarshalOUSStatus( reader );
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new RuntimeException( e );    // wrap in a runtime exception
        }
        return ret;
    }

    /**
     * Marshal our status object to XML
     * @param status   The status object
     * @return The XML document representing the status object
     */
    protected String marshal( OUSStatus status ) {
        StringWriter writer = new StringWriter();
        try {
            status.marshal( writer );
            String t = writer.toString();
            return t;
        }
        catch( Exception e ) {
            // should never happen
            e.printStackTrace();
            throw new RuntimeException( e );
        } 
    }

}
