/**
 * ProjectStatusPF.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence.domain;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;

import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.obops.utils.DatetimeUtils;
import alma.hibernate.util.HibernateXmlType;

/**
 * Implementation of the {@linkplain StatusPersistenceFacade} interface to
 * provide a facade for {@linkplain ProjectStatus} instances.
 * 
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

@Entity
@Table(name="obs_project_status")
@TypeDef(name = "xmltype", typeClass = HibernateXmlType.class)
public class ObsProjectStatusPF extends AbstractStatusPF {

    /** Reference to the ObsProgram status instance */
    @Column(name="obs_program_status_id")
    private String obsProgramStatusRef = null;
    
    /** Timestamp indicating when a project timed out */
    @Column(name="project_was_timed_out")
    private Date projectWasTimedOut = null;
    
    
    /** Default constructor */
    public ObsProjectStatusPF() {
        // no-op
    }
    
    /**
     * Constructor.
     * 
     * @param status
     *            The status entity to provide a facade for.
     * 
     * @throws IllegalArgumentException If input arg status is <code>null</code>
     * @throws ParseException 
     */
    public ObsProjectStatusPF( ProjectStatus status )
        throws IllegalArgumentException, ParseException {
        
        // Check input args
        if( status == null ) {
            throw new IllegalArgumentException( "Input arg is null" );
        }
        
        // Populate the fields of this instance
        setXml( marshal( status ));
        setStatusEntityId( status.getProjectStatusEntity().getEntityId() );
        setDomainEntityId( status.getObsProjectRef().getEntityId() );
        setDomainEntityState( status.getStatus().getState().toString() );
        setObsProjectId( getDomainEntityId() );  // domain ent. is an ObsProject
        setProjectStatusId( getStatusEntityId() );   // status ent. is an...
                                                        // ...ProjectStatus
        this.obsProgramStatusRef = 
            status.getObsProgramStatusRef().getEntityId();
       
        if(status.getProjectWasTimedOut() != null 
        		&& !status.getProjectWasTimedOut().equals("")){
        	this.projectWasTimedOut = DatetimeUtils.getDate(status.getProjectWasTimedOut());
        }
    }

    /**
     * @return A reference to the ObsProgramStatus, the top-level
     *         OUSStatus
     */
    public String getObsProgramStatusId() {
        return this.obsProgramStatusRef;
    }

    /**
     * Marshal our status object to XML
     * @param status   The status object
     * @return The XML document representing the status object
     */
    protected String marshal( ProjectStatus status ) {
        StringWriter writer = new StringWriter();
        try {
            status.marshal( writer );
            String t = writer.toString();
            return t;
        }
        catch( Exception e ) {
            // should never happen
            e.printStackTrace();
            throw new RuntimeException( e );
        } 
    }

    /**
     * @see alma.lifecycle.persistence.domain.StatusPersistenceFacade#getStatusEntity()
     * @return An instance of {@linkplain ProjectStatus}
     */
    @Override
    public Object getStatusEntity() {
        StringReader reader = new StringReader( getXml() );
        ProjectStatus ret;
        try {
            ret = ProjectStatus.unmarshalProjectStatus( reader );
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new RuntimeException( e );    // wrap in a runtime exception
        }
        return ret;
    }

}
