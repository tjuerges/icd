/**
 * SBStatusPF.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import java.io.StringReader;
import java.io.StringWriter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;

import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.hibernate.util.HibernateXmlType;

/**
 * Implementation of the {@linkplain StatusPersistenceFacade} interface to
 * provide a facade for {@linkplain SBStatus} instances.
 * 
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

@Entity
@Table(name="sched_block_status")
@TypeDef(name = "xmltype", typeClass = HibernateXmlType.class)
public class SchedBlockStatusPF extends AbstractOusStatusPF {

    /** Reference to the parent OUSStatus instance */
    @Column(name="parent_obs_unit_set_status_id")
    private String parentOusStatusId = null;

    /** Default constructor */
    public SchedBlockStatusPF() {
        // no-op
    }
    
    /**
     * Constructor.
     * 
     * @param status
     *            The status entity to provide a facade for
     * 
     * @param obsProjectId
     *            A reference to the original ObsProject domain entity
     * 
     * @throws IllegalArgumentException
     *             If input arg status is <code>null</code>
     */
    public SchedBlockStatusPF( SBStatus status, String obsProjectId )
        throws IllegalArgumentException {
        
        // Check input args
        if( status == null ) {
            throw new IllegalArgumentException( "Input arg is null" );
        }
        
        // Populate the fields of this instance
        setXml( marshal( status ));
        setStatusEntityId( status.getSBStatusEntity().getEntityId() );
        setDomainEntityId( status.getSchedBlockRef().getEntityId() );
        setDomainEntityState( status.getStatus().getState().toString() );
        setObsProjectId( obsProjectId );
        setProjectStatusId(status.getProjectStatusRef().getEntityId());
        this.parentOusStatusId = status.getContainingObsUnitSetRef().getEntityId();
        setTotalRequiredTimeInSec(status.getTotalRequiredTimeInSec());
        setTotalUsedTimeInSec(status.getTotalUsedTimeInSec());
    }

    /**
     * @return A reference to the parent OUSStatus
     */
    public String getParentOusStatusId() {
        return this.parentOusStatusId;
    }

    /**
     * @see alma.lifecycle.persistence.domain.StatusPersistenceFacade#getStatusEntity()
     * @return An instance of {@linkplain SBStatus}
     */
    @Override
    public Object getStatusEntity() {
        StringReader reader = new StringReader( getXml() );
        SBStatus ret;
        try {
            ret = SBStatus.unmarshalSBStatus( reader );
        }
        catch( Exception e ) {
            // should never happen
            e.printStackTrace();
            throw new RuntimeException( e );    // wrap in a runtime exception
        }
        return ret;
    }

    /**
     * Marshal our status object to XML
     * @param status   The status object
     * @return The XML document representing the status object
     */
    protected String marshal( SBStatus status ) {
        StringWriter writer = new StringWriter();
        try {
            status.marshal( writer );
            String t = writer.toString();
            return t;
        }
        catch( Exception e ) {
            // should never happen
            e.printStackTrace();
            throw new RuntimeException( e );
        } 
    }

}
