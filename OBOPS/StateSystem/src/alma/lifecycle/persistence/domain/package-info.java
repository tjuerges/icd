/**
Classes in this package provide a facade for status entities (such as 
ProjectStatus, SBStatus, etc.) suitable 
for storage in the Archive and for efficient querying.

@see alma.entity.xmlbinding.projectstatus.ProjectStatus
@see alma.entity.xmlbinding.sbstatus.SBStatus
@see alma.entity.xmlbinding.ousstatus.OUSStatus
@version $Revision$
 */
package alma.lifecycle.persistence.domain;



