/**
 * StatusSummary.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.lifecycle.persistence.EntityTypeException;

/**
 * Implementors of this interface provide persistence-oriented "facade" access
 * to a status entity: {@linkplain ProjectStatus},
 * {@linkplain OUSStatus} or {@linkplain SBStatus}.<p/>
 * 
 * Implementors of this class should be POJOs, suitable for mapping to
 * a relational database; e.g. using Hibernate.
 * 
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

public interface StatusPersistenceFacade {

    /**
     * Get a reference to the original domain entity, for which the status
     * entity provides status information; e.g. a {@linkplain SchedBlock}
     * instance.
     * 
     * @return A pointer to a domain entity.
     * 
     * @throws EntityTypeException
     *             If the status entity has no equivalent domain entity; e.g. if
     *             the status entity is an {@linkplain OUSStatus} entity.
     */
    public String getDomainEntityId() throws EntityTypeException;

    /**
     * We provide a facade for a status entity; status entities reference domain
     * entities; domain entities in turn belong to a hierarchy headed by an
     * {@linkplain ObsProject}.
     * 
     * @return A reference to that {@linkplain ObsProject}.
     */
    public String getObsProjectId();

    /**
     * We provide a facade for a status entity; status entities belong to a
     * hierarchy headed by an {@linkplain ProjectStatus}.
     * 
     * @return A reference to that {@linkplain ProjectStatus}.
     */
    public String getProjectStatusId();

    /**
     * Get the current state of the status entity.
     * 
     * @return A value from either the StatusTStateType, StatusTStateType or
     *         StatusTStateType enumerations.
     */
    public String getDomainEntityState();

    /**
     * @return The status entity we provide a facade for; e.g. a
     *         {@linkplain SBStatus} instance.
     */
    public Object getStatusEntity();

    /**
     * Get a reference to the status entity we provide a facade for; e.g.
     * a {@linkplain SBStatus} instance.
     * @return A pointer to the status entity we summarize.
     */
    public String getStatusEntityId();

    /**
     * Get the XML text that can be used to un-marshall the status entity
     * 
     * @return XML text
     */
    public String getXml();

    /**
     * Set the reference to the original domain entity, for which the status
     * entity provides status information; e.g. a {@linkplain SchedBlock}
     * instance.
     * 
     * @param domainEntityRef A pointer to a domain entity.
     * 
     * @throws EntityTypeException
     *             If the status entity has no equivalent domain entity; e.g. if
     *             the status entity is an {@linkplain OUSStatus} entity.
     */
    public void setDomainEntityId( String domainEntityRef );

    /**
     * @param id A reference to the {@linkplain ObsProject}.
     * @see #getObsProjectId()
     */
    public void setObsProjectId( String id );

    /**
     * @param id A reference to the {@linkplain ProjectStatus}.
     * @see #getProjectStatusId()
     */
    public void setProjectStatusId( String id );

    /**
     * Set the state of the status entity.
     * 
     * @param state
     *            A value from either the StatusTStateType, StatusTStateType or
     *            StatusTStateType enumerations, depending on the implementing
     *            class.
     */
    public void setDomainEntityState( String state );

    /**
     * Sets the reference to the status entity we provide a facade for.
     * 
     * @param statusEntityID
     *            A reference to the status entity we provide a facade for.
     */
    public void setStatusEntityId( String statusEntityID );

    /**
     * Set the XML text representing the status entity
     * @param xml The XML text representing the status entity
     */
    public void setXml( String xml );
}
