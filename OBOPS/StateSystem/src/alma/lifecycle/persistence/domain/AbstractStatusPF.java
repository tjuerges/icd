/**
 * StatusSummaryAdapter.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;

import alma.entity.xmlbinding.obsproject.ObsProject;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;

/**
 * Basic implementation of the {@linkplain StatusPersistenceFacade} interface.
 *
 * @author amchavan, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

@MappedSuperclass
public abstract class AbstractStatusPF implements StatusPersistenceFacade {

    /** The XML text representing the status entity */
	@Type(type = "xmltype")
    @Column
    protected String xml;
    
    /**
     * A reference to the status entity we provide a facade for; e.g. a
     * SBStatus
     */
    @Id
    @Column(name="status_entity_id")
    protected String statusEntityId;
    
    /** A reference to original domain entity; e.g. a SchedBlock */
    @Column(name="domain_entity_id")
    protected String domainEntityId;

    /**
     * Status entities reference domain entities; domain entities in turn belong
     * to a hierarchy headed by an {@linkplain ObsProject}; this is a reference
     * to that {@linkplain ObsProject}.
     */
    @Column(name="obs_project_id")
    protected String obsProjectId;

    /**
     * Status entities belong to a hierarchy headed by an
     * {@linkplain ProjectStatus}; this is a a reference to that
     * {@linkplain ProjectStatus}.
     */
    @Column(name="obs_project_status_id")
    protected String obsProjectStatusId;

    /**
     * The state of the domain entity.
     * 
     * It's a value from either the
     * StatusTStateType,
     * StatusTStateType or
     * StatusTStateType
     * enumerations.
     */
    @Column(name="domain_entity_state")
    protected String domainEntityState;

    /**
     * @see StatusPersistenceFacade#getDomainEntityId()
     */
    @Override
    public String getDomainEntityId() {
        return domainEntityId;
    }

    /**
     * @return A reference to the {@linkplain ObsProject}.
     * @see StatusPersistenceFacade#getObsProjectId()
     */
    public String getObsProjectId() {
        return obsProjectId;
    }

    /**
     * @return A reference to the {@linkplain ProjectStatus}.
     * @see StatusPersistenceFacade#getProjectStatusId()
     */
    public String getProjectStatusId() {
        return obsProjectStatusId;
    }

    /**
     * @see StatusPersistenceFacade#getDomainEntityState()
     */
    @Override
    public String getDomainEntityState() {
        return domainEntityState;
    }

    /**
     * @see StatusPersistenceFacade#setStatusEntityId(String)
     */
    public String getStatusEntityId() {
        return statusEntityId;
    }

    /**
     * @see StatusPersistenceFacade#getXml()
     */
    @Override
    public String getXml() {
        return xml;
    }

    /**
     * @see StatusPersistenceFacade#setDomainEntityId(String)
     * @param domainEntityID
     *            A reference to original domain entity; e.g. a SchedBlock
     */
    @Override
    public void setDomainEntityId( String domainEntityID ) {
        this.domainEntityId = domainEntityID;
    }

    /**
     * @param id A reference to the {@linkplain ObsProject}.
     * @see StatusPersistenceFacade#setObsProjectId(String)
     */
    public void setObsProjectId( String id ) {
        this.obsProjectId = id;
    }

    
    /**
     * @param id A reference to the {@linkplain ProjectStatus}.
     * @see StatusPersistenceFacade#setProjectStatusId(String)
     */
    public void setProjectStatusId( String id ) {
        this.obsProjectStatusId = id;
    }

    /**
     * Sets a reference to the status entity we summarize.
     * 
     * @param statusEntityID  A reference to the status entity we summarize.
     * 
     * @see StatusPersistenceFacade#setStatusEntityId(String)
     */
    @Override
    public void setStatusEntityId( String statusEntityID ) {
        this.statusEntityId = statusEntityID;
    }
    
    /**
     * @param xml The XML text representing the status entity
     * 
     * @see StatusPersistenceFacade#setXml(String)
     */
    @Override
    public void setXml( String xml ) {
        this.xml = xml;
    }
    
    /**
     * Set the state of the status entity.
     * 
     * @param state
     *            The state of the status entity. Must be a value from the
     *            StatusTStateType enumeration.
     * 
     * @see StatusPersistenceFacade#setDomainEntityState(String)
     */
    @Override
    public void setDomainEntityState( String state ) {
        this.domainEntityState = state;
    }
}
