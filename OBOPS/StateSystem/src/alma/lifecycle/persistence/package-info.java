/**
Support for persisting status entities (such as 
ProjectStatus, SBStatus, etc.) to the Archive.

@see alma.entity.xmlbinding.projectstatus.ProjectStatus
@see alma.entity.xmlbinding.sbstatus.SBStatus
@version $Revision$
 */
package alma.lifecycle.persistence;