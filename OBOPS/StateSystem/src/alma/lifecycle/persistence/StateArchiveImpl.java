/**
 * StateArchiveImpl.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.transaction.annotation.Transactional;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.acs.exceptions.AcsJException;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.valuetypes.types.StatusTStateType;
import alma.lifecycle.persistence.dao.DataAccessObject;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.stateengine.constants.Location;
import alma.lifecycle.stateengine.constants.Subsystem;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * Implementation of the {@linkplain StateArchive} service.
 *
 * @author rkurowsk, July 29, 2009
 * @version $Revision: 1.8 $
 */

// $Id: StateArchiveImpl.java,v 1.8 2011/02/03 10:28:56 rkurowsk Exp $

@Transactional(rollbackFor=AcsJException.class)
public class StateArchiveImpl extends StateArchiveAbstractImpl implements StateArchive {

    private DataAccessObject stateArchiveDao;
    
    /** Public zero-arg constructor */
    public StateArchiveImpl() {

    }
    
    public DataAccessObject getStateArchiveHibernateDao() {
		return stateArchiveDao;
	}

	public void setStateArchiveHibernateDao(
			DataAccessObject stateArchiveHibernateDao) {
		this.stateArchiveDao = stateArchiveHibernateDao;
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchiveAbstractImpl#getObsProjectStatusPF(java.lang.String)
	 */
	@Override
    public ObsProjectStatusPF getObsProjectStatusPF( String key ) {
		
		ObsProjectStatusPF opsPf = (ObsProjectStatusPF)stateArchiveDao.read(key, ObsProjectStatusPF.class);
		
		if(opsPf != null && opsPf.getXml() == null){
			throw new RuntimeException ("ObsProjectStatusPF xml is null");
		}
		
		return opsPf;
			
    }

    /**
     * Query the database.
     * 
     * @param key
     *            EntityId of an OUSStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ObsUnitSet whose
     *         EntityId is equal to the input key. The entities are represented
     *         as XML text.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    @Override
    protected String[] getSBStatusOusXmlList( String key )
            throws AcsJNullEntityIdEx, 
                AcsJNoSuchEntityEx,
                AcsJInappropriateEntityTypeEx {

        List<SchedBlockStatusPF> results = stateArchiveDao.getSchedBlockStatusPFList(key);

        if( results.size() == 0 ) {
            throw new AcsJNoSuchEntityEx();
        }
        
        String[] ret = new String[results.size()];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = new String( results.get( i ).getXml() );
        }

        return ret;
    }

    /* (non-Javadoc)
     * @see alma.lifecycle.persistence.StateArchiveAbstractImpl#getSchedBlockStatusPF(java.lang.String)
     */
    @Override
    protected SchedBlockStatusPF getSchedBlockStatusPF( String key ) {
    	
    	SchedBlockStatusPF sbsPf = (SchedBlockStatusPF)stateArchiveDao.read(key, SchedBlockStatusPF.class);
		
    	if(sbsPf != null && sbsPf.getXml() == null){
			throw new RuntimeException ("SchedBlockStatusPF xml is null.");
		}
		
        return sbsPf;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * alma.lifecycle.persistence.StateArchiveAbstractImpl#update(alma.lifecycle
	 * .persistence.domain.OUSStatusPF)
	 */
	@Override
	protected void update(OUSStatusPF ousStatusPf)
			throws AcsJStateIOFailedEx {
		try {
			stateArchiveDao.update(ousStatusPf);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * alma.lifecycle.persistence.StateArchiveAbstractImpl#update(alma.lifecycle
	 * .persistence.domain.ObsProjectStatusPF)
	 */
	@Override
	public void update(ObsProjectStatusPF obsProjectStatusPF)
			throws AcsJStateIOFailedEx {
		try {
			stateArchiveDao.update(obsProjectStatusPF);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * alma.lifecycle.persistence.StateArchiveAbstractImpl#update(alma.lifecycle
	 * .persistence.domain.SchedBlockStatusPF)
	 */
	@Override
	protected void update(SchedBlockStatusPF sbStatusPF)
			throws AcsJStateIOFailedEx {
		try {
			stateArchiveDao.update(sbStatusPF);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}
	}
	
    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.ousstatus.OUSStatus[], alma.entity.xmlbinding.sbstatus.SBStatus[])
     */
    @Override
    public void insert( ProjectStatus opStatus,
                        OUSStatus[] ousStatuses,
                        SBStatus[] sbStatuses ) throws AcsJStateIOFailedEx {

        if( opStatus == null || ousStatuses == null || sbStatuses == null ) {
            String msg = "Null arg";
            throw new AcsJStateIOFailedEx(new IllegalArgumentException( msg ) );
        }
        
        try{
	        String obsProjectId = opStatus.getObsProjectRef().getEntityId();
	        ObsProjectStatusPF opsPF = new ObsProjectStatusPF( opStatus );
	        stateArchiveDao.create(opsPF);
	        
	        for( int i = 0; i < ousStatuses.length; i++ ) {
	            OUSStatus entity = ousStatuses[i];
	            OUSStatusPF facade = 
	            	new OUSStatusPF( entity, obsProjectId );
	            stateArchiveDao.create(facade);
	        }
	        
	        for( int i = 0; i < sbStatuses.length; i++ ) {
	            SBStatus entity = sbStatuses[i];
	            SchedBlockStatusPF facade = 
	            	new SchedBlockStatusPF( entity, obsProjectId );
	            stateArchiveDao.create(facade);
	        }
	        
        }catch (Exception e) {
        	e.printStackTrace();
        	logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}
    }

    /**
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.lifecycle.persistence.domain.StateChangeRecord)
     */
    @Override
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx {
       
    	try{
        	stateArchiveDao.create(record);
        }catch (Exception e) {
        	logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}
    }

    /**
     * @see StateArchive#findStateChangeRecords(Date, Date, String, String, String, String)
	 */
	@Override
	public List<StateChangeRecord> findStateChangeRecords(Date start, Date end,
			String domainEntityId, String state, String userId,
			StateEntityType type) throws AcsJStateIOFailedEx {

		try {
			return stateArchiveDao.findStateChangeRecords(start, end,
					domainEntityId, state, userId, type);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new AcsJStateIOFailedEx(e);
		}

	}
	
	@Override
    protected OUSStatusPF getOUSStatusPF( String key ) {
		
		OUSStatusPF oussPf = (OUSStatusPF)stateArchiveDao.read(key, OUSStatusPF.class);
		
    	if(oussPf != null && oussPf.getXml() == null){
			throw new RuntimeException ("OUSStatusPF xml is null");
		}
		
        return oussPf;
        
    }

	@Override
	protected List<SchedBlockStatusPF> getSchedBlockStatusPFList(String parentOusEntityId) {
        return stateArchiveDao.getSchedBlockStatusPFList(parentOusEntityId);
	}

	@Override
	protected void insert(ObsProjectStatusPF obsProjectStatusPf)
			throws AcsJStateIOFailedEx {
		
		stateArchiveDao.create(obsProjectStatusPf);

		// create an initial state change record for the ObsProjectStatusPF 
		
		//TODO: API should change to accept a userId so we can stop blaming god.
		StateChangeRecord scr = new StateChangeRecord(
				obsProjectStatusPf, new Date(), Location.SCO, 
				"god", Subsystem.STATE_ENGINE, 
				"creation", StateEntityType.PRJ);
		
		insert(scr);
	}

	@Override
	protected void insert(OUSStatusPF ousStatusPf)
			throws AcsJStateIOFailedEx {
		
		stateArchiveDao.create(ousStatusPf);
		
		// create an initial state change record for the OUSStatus 
		StateChangeRecord scr = new StateChangeRecord(
				ousStatusPf, new Date(), Location.SCO, 
				"god", Subsystem.STATE_ENGINE, 
				"creation", StateEntityType.OUT);
		
		insert(scr);
	}

	@Override
	protected void insert(SchedBlockStatusPF schedBlockStatusPF)
			throws AcsJStateIOFailedEx {
		stateArchiveDao.create(schedBlockStatusPF);
		
		// create an initial state change record for the OUSStatus 
		StateChangeRecord scr = new StateChangeRecord(
				schedBlockStatusPF, new Date(),	Location.SCO, 
				"god", Subsystem.STATE_ENGINE, 
				"creation", StateEntityType.SBK);
		
		insert(scr);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchiveAbstractImpl#getProjectStatusPF(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@Override
	protected ObsProjectStatusPF getProjectStatusPF(
			ObsProjectEntityT obsProjectId){
		return stateArchiveDao.getProjectStatusPF(obsProjectId);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#lockProject(alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT)
	 */
	@Override
	public void lockProjectStatus(ProjectStatusEntityT projectStatusId ) 
		throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
        
		if( projectStatusId == null ) {
            String msg = "Null arg";
            throw new AcsJIllegalArgumentEx(new IllegalArgumentException( msg ) );
        }
		
		ObsProjectStatusPF obsProjectStatusPF = getObsProjectStatusPF(projectStatusId.getEntityId());
		
		if(obsProjectStatusPF == null){
			throw new AcsJNoSuchEntityEx();
		}
		
		stateArchiveDao.lock(obsProjectStatusPF);
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findProjectStatusByState(java.lang.String[])
	 */
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states)
			throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		
		if( states == null || states.length < 1 ) {
            String msg = "Null arg";
            throw new AcsJIllegalArgumentEx( new IllegalArgumentException( msg ) );
        }
		// check if the array of states contains ANYSTATE, if so pass null to the DAO and it will return everything
		for(String state: states){
			if(state.equals(StatusTStateType.ANYSTATE.toString())){
				states = null;
			}
		}
		
		List<ObsProjectStatusPF> obsProjectStatusPFList = stateArchiveDao.findProjectStatusByState(states);
		List<ProjectStatus> projStatusTmpList = new ArrayList<ProjectStatus>();
		for(ObsProjectStatusPF obsProjectStatusPF: obsProjectStatusPFList){
			try{
				projStatusTmpList.add(unmarshalProjectStatus( obsProjectStatusPF.getXml()));
			}catch(AcsJInappropriateEntityTypeEx ex){
                ex.printStackTrace();
                logger.severe("error unmarshalling ProjectStatus XML: " + obsProjectStatusPF.getXml());
			}
		}
		
		return projStatusTmpList.toArray(new ProjectStatus[0]);
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findSBStatusByState(java.lang.String[])
	 */
	@Override
	public SBStatus[] findSBStatusByState(String[] states)
			throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		
		if( states == null || states.length < 1 ) {
            String msg = "Null arg";
            throw new AcsJIllegalArgumentEx( new IllegalArgumentException( msg ) );
        }
		// check if the array of states contains ANYSTATE, if so pass null to the DAO and it will return everything
		for(String state: states){
			if(state.equals(StatusTStateType.ANYSTATE.toString())){
				states = null;
			}
		}
		
		List<SchedBlockStatusPF> sbStatusPFList = stateArchiveDao.findSBStatusByState(states);
		
		List<SBStatus> sbStatusTmpList = new ArrayList<SBStatus>();
        for(SchedBlockStatusPF sbStatusPF: sbStatusPFList){
            try {
                sbStatusTmpList.add(unmarshalSBStatus(sbStatusPF.getXml()));
            } catch (AcsJInappropriateEntityTypeEx ex) {
                ex.printStackTrace();
                logger.severe("error unmarshalling SBStatus XML: " + sbStatusPF.getXml());
            }
        }
		return sbStatusTmpList.toArray(new SBStatus[0]);
	}
	
	@Override
	public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
		List<OUSStatus> ousStatusTmpList = new ArrayList<OUSStatus>();
		for (String ousStatusId : ousStatusIds) {
			OUSStatusPF ousStatusPF = stateArchiveDao
					.getOUSStatusPF(ousStatusId);
			if (ousStatusPF == null)
				continue;
			try {
				ousStatusTmpList.add(unmarshalOUSStatus(ousStatusPF.getXml()));
				addChildrenOUSStatus(ousStatusPF, ousStatusTmpList);
			} catch (AcsJInappropriateEntityTypeEx ex) {
				continue;
			}
		}
		return ousStatusTmpList.toArray(new OUSStatus[0]);
	}

	private void addChildrenOUSStatus(OUSStatusPF parent,
			List<OUSStatus> OUSStatusList) throws AcsJInappropriateEntityTypeEx {
		List<OUSStatusPF> childOUSStatusPFList = stateArchiveDao
				.getChildOUSStatusPF(parent.getStatusEntityId());
		for (OUSStatusPF childOUSStatusPF : childOUSStatusPFList) {
			OUSStatusList.add(unmarshalOUSStatus(childOUSStatusPF.getXml()));
			addChildrenOUSStatus(childOUSStatusPF, OUSStatusList);
		}
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#storeArchiveEntities(alma.xmlentity.XmlEntityStruct[], java.lang.String, java.util.logging.Logger)
	 */
	@Override
	public void storeArchiveEntities(XmlEntityStruct[] archiveEntities,
			String user, Logger logr) {
		
		stateArchiveDao.storeArchiveEntities(archiveEntities, user, logger);
		
	}

}
