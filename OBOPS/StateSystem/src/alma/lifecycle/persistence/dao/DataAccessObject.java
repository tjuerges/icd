/*
 * Copyright European Southern Observatory 2006
 */

package alma.lifecycle.persistence.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.lifecycle.persistence.StateArchive;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.xmlentity.XmlEntityStruct;

/**
 * The root of all DAO interfaces.
 * 
 * @version $Revision: 1.5 $
 * @author rkurowsk, July 29, 2009
 */

// $Id: DataAccessObject.java,v 1.5 2011/02/03 10:28:57 rkurowsk Exp $

public interface DataAccessObject {

	/**
	 * Persist the input domain object; that is, create a representation of it
	 * in the underlying database
	 * 
	 * @return The generated identifier
	 */
	public Serializable create(Object domainObject);

	/**
	 * Make the input domain object transient; that is, remove its
	 * representation from the underlying database
	 */
	public void delete(Object domainObject);

	/**
	 * Retrieve a domain object from the database.
	 * 
	 * @param domainClass
	 *            to read
	 * 
	 * @param id
	 *            identification for the object to retrieve
	 * 
	 * @return An instance of the input class, whose ID is the the input number,
	 *         if one exists; <code>null</code> otherwise.
	 */
	public Object read(Serializable id, Class<?> domainClass);

	/**
	 * Synchronize the input domain object with the database; that is, update
	 * its representation in the underlying database
	 */
	public void update(Object domainObject);

	/**
	 * Re-attache the domain object to the DB
	 * 
	 * @param domainObject
	 */
	public void reAttach(Object domainObject);

	/**
	 * @param domainClass
	 * 
	 * @return All known entries
	 */
	public List<?> findAll(Class<?> domainClass);

	/**
	 * Lookup entries based on given search criteria.
	 * 
	 * @param searchCriteria
	 * @param orderCriteria
	 * @param domainClass
	 */
	public List<?> find(final List<Object> searchCriteria, 
			final List<Object> orderCriteria, 
			final Class<?> domainClass);
	
	
	/**
     * Query the database.
     * 
	 * @param parentOusStatusId
     *            EntityId of an OUSStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ObsUnitSet whose
     *         EntityId is equal to the input key. The entities are represented
     *         as XML text.
	 */
	public List<SchedBlockStatusPF> getSchedBlockStatusPFList(String parentOusStatusId);
		

    /**
	 * @see StateArchive#findStateChangeRecords(Date, Date, String, String, String, StateEntityType)
	 */
	public List<StateChangeRecord> findStateChangeRecords(Date start, Date end,
			String domainEntityId, String state, String userId,
			StateEntityType type);

	/**
	 * Find ObsProjectStatusPF by obsProject
	 */
	public ObsProjectStatusPF getProjectStatusPF(ObsProjectEntityT obsProjectId);
	
    
    /**
     * Locks the given object in the DB, lock is released when transaction commits or rolls back
     * 
     * @param domainObject
     */
    public void lock(Object domainObject );
    
    
	/**
	 * Find ObsProjectStatusPF matching any of the given states
	 * 
	 * @param states
	 * @return
	 */
	public List<ObsProjectStatusPF> findProjectStatusByState( String[] states );
    	
	/**
	 * Find SchedBlockStatusPF matching any of the given states
	 * @param states
	 * @return
	 */
	public List<SchedBlockStatusPF> findSBStatusByState( String[] states );
    
	public OUSStatusPF getOUSStatusPF(String ousStatusId);
	
	public List<OUSStatusPF> getChildOUSStatusPF(String parentOUSStatusId);
	
	/**
	 * Stores (inserts or updates) XML archive entities using current hibernate session & transaction. 
	 * 
	 * @param archiveEntities
	 * @param user
	 * @param logr
	 */
	public void storeArchiveEntities(final XmlEntityStruct[] archiveEntities, 
			final String user, final Logger logr);
		
}
