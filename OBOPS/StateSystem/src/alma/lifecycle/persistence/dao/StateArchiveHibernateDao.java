/**
 * HibernateDao.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence.dao;

import java.io.Serializable;
import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import oracle.jdbc.OracleConnection;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.Work;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import alma.archive.database.interfaces.InternalIF;
import alma.archive.database.interfaces.InternalIFFactory;
import alma.archive.database.oracle.InternalIfImpl;
import alma.archive.wrappers.Permissions;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.hibernate.util.JdbcNativeExtractor;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.xmlentity.XmlEntityStruct;


/**
 * Root class of all Hibernate-based Data Access Objects This class now
 * extends Spring's HibernateDaoSupport to take advantage of
 * getHibernateTemplate() and delarative transactions.
 * 
 * @version $Revision$
 * @author rkurowsk, July 29, 2009
 * 
 */

// $Id$
@Transactional
public class StateArchiveHibernateDao extends HibernateDaoSupport 
	implements DataAccessObject {

	/**
	 * Public constructor
	 */
	public StateArchiveHibernateDao() {
		// no-op
	}
	/**
	 * Persist the input domain object; that is, create a representation of it
	 * in the underlying database
	 * 
	 * @see DataAccessObject#create(Object)
	 */
	public Serializable create(Object domainObject) {
		return getHibernateTemplate().save(domainObject);
	}

	/**
	 * Make the input domain object transient; that is, remove its
	 * representation from the underlying database
	 * 
	 * @see DataAccessObject#delete(Object)
	 */
	public void delete(Object domainObject) {
		getHibernateTemplate().delete(domainObject);
	}

	/**
	 * Retrieve a domain object from the database.
	 * 
	 * @param domainClass
	 * 
	 * @param id
	 *            Numerical identification for the object to retrieve
	 * 
	 * @return An instance of the domain class, whose ID is the the input number;
	 *         or <code>null</code> if none was found.
	 */
	public Object read(final Serializable id, Class<?> domainClass) {
		return getHibernateTemplate().get(domainClass, id);
	}

	/**
	 * Synchronize the input domain object with the database; that is, update
	 * its representation in the underlying database
	 * 
	 * @see DataAccessObject#update(Object)
	 */
	public void update(Object domainObject) {
		getHibernateTemplate().merge(domainObject);
	}

	/**
	 * Lookup entries based on given search criteria.
	 * 
	 * @param searchCriteria
	 * @param orderCriteria
	 * @param domainClass
	 */
	public List<?> find(final List<Object> searchCriteria, final List<Object> orderCriteria, final Class<?> domainClass) {

		return (List<?>) getHibernateTemplate().execute(
			new HibernateCallback() {
				public Object doInHibernate(Session session) {
					Criteria query = session.createCriteria(domainClass);
					Criterion clause;
					if (searchCriteria != null) {
						for (Object object : searchCriteria) {
							clause = (Criterion) object;
							if (clause != null) {
								query.add(clause);
							}
						}
					}
					Order sortClause;
					if (orderCriteria != null) {
						for (Object object : orderCriteria) {
							sortClause = (Order) object;
							if (sortClause != null) {
								query.addOrder(sortClause);
							}
						}
					}
					return query.list();
				}
			});
	}

	/**
	 * Reads a domain object using the given query
	 * @param hql
	 * @return the domain object found
	 */
    protected Object read(final String hql) {

    	return getHibernateTemplate().execute(new HibernateCallback(){
    		public Object doInHibernate(Session session){
    	        Query query = session.createQuery( hql );
   	            return query.uniqueResult();
    		}
    	});
    	
    }
    
    /**
     * Re-attache the domain object to the DB
     * @param domainObject
     */
    public void reAttach(Object domainObject){
    	getHibernateTemplate().lock(domainObject, LockMode.NONE);
    }

	/* (non-Javadoc)
	 * @see alma.obops.dam.DataAccessObject#findAll(java.lang.Class)
	 */
	@Override
	public List<?> findAll(Class<?> domainClass) {
		return find(null, null, domainClass);
	}
	
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.dao.DataAccessObject#getSchedBlockStatusPFList(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<SchedBlockStatusPF> getSchedBlockStatusPFList(String parentOusStatusId){
        
		List<Object> searchCriteria = new ArrayList<Object>();
		searchCriteria.add(Restrictions.eq("parentOusStatusId", parentOusStatusId));

		List<Object> sortCriteria = new ArrayList<Object>();
		sortCriteria.add(Order.asc("statusEntityId"));

		return (List<SchedBlockStatusPF>)find(searchCriteria, sortCriteria, SchedBlockStatusPF.class);
		
	}

    /* (non-Javadoc)
     * @see alma.lifecycle.persistence.dao.DataAccessObject#findStateChangeRecords(java.util.Date, java.util.Date, java.lang.String, java.lang.String, java.lang.Integer, java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public List<StateChangeRecord> findStateChangeRecords(Date start, Date end,
			String domainEntityId, String state, String userId, StateEntityType type) {

    	List<Object> searchCriteria = new ArrayList<Object>();

		if (start != null) {
			searchCriteria.add(Restrictions.ge("timestamp", start));
		}
		if (end != null) {
			searchCriteria.add(Restrictions.le("timestamp", end));
		}
		if (domainEntityId != null) {
			searchCriteria.add(Restrictions.eq("domainEntityId", domainEntityId));
		}
		if (state != null) {
			searchCriteria.add(Restrictions.eq("domainEntityState", state));
		}
		if (userId != null) {
			searchCriteria.add(Restrictions.eq("userId", userId));
		}
		if (type != null) {
			searchCriteria.add(Restrictions.eq("entityType", type));
		}

		// order by timestamp
		List<Object> sortCriteria = new ArrayList<Object>();
		sortCriteria.add(Order.asc("timestamp"));

		return (List<StateChangeRecord>)find(searchCriteria, sortCriteria, StateChangeRecord.class);
	}
    
    
	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.dao.DataAccessObject#getProjectStatusPFList(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@SuppressWarnings("unchecked")
	public ObsProjectStatusPF getProjectStatusPF(ObsProjectEntityT obsProjectId){
		
		List<Object> searchCriteria = new ArrayList<Object>();
		searchCriteria.add(Restrictions.eq("domainEntityId", obsProjectId.getEntityId()));

		List<Object> sortCriteria = new ArrayList<Object>();
		sortCriteria.add(Order.asc("statusEntityId"));
		
		List<ObsProjectStatusPF> projStatusList = (List<ObsProjectStatusPF>)
			find(searchCriteria, sortCriteria, ObsProjectStatusPF.class);
			
		ObsProjectStatusPF obsProjectStatusPF = null;
		
		if( projStatusList.size() == 1){
			obsProjectStatusPF = projStatusList.get(0);
		}else if(projStatusList.size() > 1){
			throw new RuntimeException("Multiple project status entries exist for project entity:" + obsProjectId.getEntityId());
		}
		
		return obsProjectStatusPF;
		
	}
    
    /**
     * Locks the given object in the DB
     * 
     * @param domainObject
     */
    public void lock(Object domainObject ){
    	getHibernateTemplate().lock(domainObject, LockMode.UPGRADE);
    }

    /* (non-Javadoc)
     * @see alma.lifecycle.persistence.dao.DataAccessObject#findProjectStatusByState(java.lang.String[])
     */
    @SuppressWarnings("unchecked")
	public List<ObsProjectStatusPF> findProjectStatusByState( String[] states ){
    	
		List<Object> searchCriteria = new ArrayList<Object>();
		
		if(states != null){
			searchCriteria.add(Restrictions.in("domainEntityState", states));
		}

		List<Object> sortCriteria = new ArrayList<Object>();
		sortCriteria.add(Order.asc("statusEntityId"));

		return (List<ObsProjectStatusPF>)find(searchCriteria, sortCriteria, ObsProjectStatusPF.class);
    }
    
    /* (non-Javadoc)
     * @see alma.lifecycle.persistence.dao.DataAccessObject#findSBStatusByState(java.lang.String[])
     */
    @SuppressWarnings("unchecked")
	public List<SchedBlockStatusPF> findSBStatusByState( String[] states ){
    	
		List<Object> searchCriteria = new ArrayList<Object>();
		if(states != null){
			searchCriteria.add(Restrictions.in("domainEntityState", states));
		}

		List<Object> sortCriteria = new ArrayList<Object>();
		sortCriteria.add(Order.asc("statusEntityId"));

		return (List<SchedBlockStatusPF>)find(searchCriteria, sortCriteria, SchedBlockStatusPF.class);
    }

    public OUSStatusPF getOUSStatusPF(String ousStatusId){
        return (OUSStatusPF) getHibernateTemplate().get(OUSStatusPF.class, ousStatusId);
    }
    
    @SuppressWarnings("unchecked")
    public List<OUSStatusPF> getChildOUSStatusPF(String parentOUSStatusId) {
        
        List<Object> searchCriteria = new ArrayList<Object>();
        if(parentOUSStatusId != null){
            searchCriteria.add(Restrictions.eq("parentOusStatusId", parentOUSStatusId));
        }

        List<Object> sortCriteria = new ArrayList<Object>();
        sortCriteria.add(Order.asc("statusEntityId"));

        return (List<OUSStatusPF>)find(searchCriteria, sortCriteria, OUSStatusPF.class);        
    }

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.dao.DataAccessObject#storeArchiveEntities(alma.xmlentity.XmlEntityStruct[], java.lang.String, java.util.logging.Logger)
	 */
	@Override
	public void storeArchiveEntities(final XmlEntityStruct[] archiveEntities,
			final String user, final Logger logr) {

		getSession().doWork(new Work() {

			@Override
			public void execute(Connection hibernateProxyConn)
					throws SQLException {
				try {
					
					InternalIF archiveInternal = InternalIFFactory.getInternalIF(logr);
					archiveInternal.init();
					Permissions permissions = new Permissions();

					OracleConnection conn = null;
					if(archiveInternal instanceof InternalIfImpl){
						conn = getOracleConnection(hibernateProxyConn);
					}
					
					for (XmlEntityStruct archiveEntity : archiveEntities) {
						URI uid = new URI(archiveEntity.entityId);
						archiveInternal.store(uid, archiveEntity.xmlString,
								archiveEntity.entityTypeName, user,
								permissions, user, true, conn);
					}

				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		});

	}

	private OracleConnection getOracleConnection(Connection proxyConn) {

		try {
			
			JdbcNativeExtractor extractor = new JdbcNativeExtractor();

			// if c3p0 is enabled then proxyConn is a hibernate proxy to a c3p0 proxy
			// JdbcNativeExtractor needs to be passed the c3p0 proxy from which
			// it will extract the native Oracle connection

			if(proxyConn.getMetaData().getConnection() != null){
				Connection proxyConnLevel2 = proxyConn.getMetaData().getConnection();
				if (proxyConnLevel2.getClass().getName().startsWith("com.mchange.v2.c3p0")) {
					proxyConn = proxyConn.getMetaData().getConnection();
				}
			}
			
			Connection conn = extractor.getNativeConnection(proxyConn);

			if (conn instanceof oracle.jdbc.OracleConnection) {
				return (OracleConnection) conn;
			} else {
				throw new RuntimeException("Current connection is not an OracleConnection. It is: "
								+ conn.getClass().getName());
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

}
