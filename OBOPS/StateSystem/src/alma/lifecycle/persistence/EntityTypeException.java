/**
 * NullEntityIdException.java
 *
 * Copyright European Southern Observatory 2008
 */

package alma.lifecycle.persistence;

/**
 * Thrown if the entity in the Archive with a given EntityId is not of the
 * appropriate type, or if an operation was attempted on an entity of invalid
 * type.
 * 
 * @author dclarke, Jun 4, 2009
 * @version $Revision$
 */

// $Id$

public class EntityTypeException extends Exception {

    private static final long serialVersionUID = -9131780366387989163L;

    public EntityTypeException() {
        super();
    }

    public EntityTypeException( String message, Throwable cause ) {
        super( message, cause );
    }

    public EntityTypeException( String message ) {
        super( message );
    }

    public EntityTypeException( Throwable cause ) {
        super( cause );
    }
}
