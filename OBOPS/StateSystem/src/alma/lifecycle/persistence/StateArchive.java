/**
 * StateArchive.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.ousstatus.OUSStatusEntityT;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.projectstatus.StatusBaseT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.entity.xmlbinding.sbstatus.SBStatusEntityT;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.statearchiveexceptions.wrappers.AcsJEntitySerializationFailedEx;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * Implementors of this interface provide a way to read, write and update
 * <em>status entities</em> into the Archive, including
 * {@linkplain ProjectStatus}, {@linkplain OUSStatus} and
 * {@linkplain SBStatus}.
 * 
 * @author amchavan, Jun 8, 2009
 * @version $Revision: 1.7 $
 */

// $Id: StateArchive.java,v 1.7 2011/02/03 10:28:56 rkurowsk Exp $

public interface StateArchive {

    /**
	 * Query the State Archive for ProjectStatus records, based on their state.
	 * 
	 * @param states
	 *            An array of state values; it may not be <code>null</code> nor
	 *            empty (zero elements).
	 *            If the array contains ANYSTATE 
	 *            (i.e. StatusTStateType.ANYSTATE.toString())
	 *            then all ProjectStatus records will be returned.
	 * 
	 * @return An array of ProjectStatus entities, including all entities in the
	 *         archive whose state matches one of the states in the input array.
	 *         The returned array is not sorted in any particular order. <br/>
	 * 
	 *         If for instance the <em>states</em> array includes states 'A',
	 *         'B' and 'C', the returned array will include all known
	 *         ProjectStatus entities whose state is 'A' or 'B' or 'C'. <br/>
	 * 
	 *         The returned array may be empty but is never <code>null</code>.
	 *         <p/>
	 * 
	 *         <strong>Note</strong>: care should be exercised when calling this
	 *         method, as it may start a very long-running query, returning a
	 *         large number of entries.
	 * 
	 * @throws AcsJIllegalArgumentEx
	 *             If arg <em>states</em> is <code>null</code> or empty (zero
	 *             elements).
	 * 
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if some entity found in the archive is not of the appropriate
	 *             type (this should never happen unless the data in the db is
	 *             corrupted)
	 */
    public ProjectStatus[] findProjectStatusByState( String[] states )
            throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx;

    /**
	 * Query the State Archive for SBStatus records, based on their state.
	 * 
	 * @param states
	 *            An array of state values; it may not be <code>null</code> nor
	 *            empty (zero elements).
	 *            If the array contains ANYSTATE 
	 *            (i.e. StatusTStateType.ANYSTATE.toString())
	 *            then all SBStatus records will be returned.            
	 * 
	 * @return An array of SBStatus entities, including all entities in the
	 *         archive whose state matches one of the states in the input array.
	 *         The returned array is not sorted in any particular order. <br/>
	 * 
	 *         If for instance the <em>states</em> array includes states 'A',
	 *         'B' and 'C', the returned array will include all known
	 *         ProjectStatus entities whose state is 'A' or 'B' or 'C'. <br/>
	 * 
	 *         The returned array may be empty but is never <code>null</code>.
	 *         <p/>
	 * 
	 *         <strong>Note</strong>: care should be exercised when calling this
	 *         method, as it may start a very long-running query, returning a
	 *         large number of entries.
	 * 
	 * @throws AcsJIllegalArgumentEx
	 *             If arg <em>states</em> is <code>null</code> or empty (zero
	 *             elements).
	 *             
	 * @throws AcsJInappropriateEntityTypeEx
	 *             if some entity found in the archive is not of the appropriate
	 *             type (this should never happen unless the data in the db is
	 *             corrupted)
	 */
    public SBStatus[] findSBStatusByState( String[] states )
            throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx;

    /**
     * Query the State Archive for state change records.
     * <p/>
     * Parameters specify query criteria: if a parameter is <code>null</code> it
     * will be ignored (that is, the corresponding query criteria will not be
     * included). So, for instance, calling this method with all
     * <code>null</code> parameters will result in returning all records in the
     * archive -- not to attempt in production.
     * <p/>
     * Query criteria corresponding to non-null parameters will AND-ed; that is,
     * only records satisfying all query criteria will be returned.
     * 
     * 
     * @param start
     *            Start timestamp: look for records with timestamp later or
     *            equal to this time
     * @param end
     *            End timestamp: look for records with timestamp earlier or
     *            equal to this time
     * @param domainEntityId
     *            ID of the <em>domain</em> entity whose state change records we
     *            should look for
     * @param state
     *            Restrict the returned records to this state
     * @param userId
     *            Restrict the returned records to this user ID
     * @param type
     * 
     * @return A list of records satisfying all query criteria, possibly empty
     *         but never <code>null</code>; the list list ordered by timestamp
     *         (ascending)
     */
    public List<StateChangeRecord> 
        findStateChangeRecords( Date start, 
                                Date end,
                                String domainEntityId,
                                String state,
                                String userId,
                                StateEntityType type ) 
       throws AcsJStateIOFailedEx;

    /**
     * Look for the OUSStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the OUSStatus to look for.
     * 
     * @return The required status entity, if one was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public OUSStatus getOUSStatus( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for the ProjectStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the OUSStatus to look for.
     * 
     * @return The XML representation of the required status entity, if one was
     *         found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String getOUSStatusXml( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for the ProjectStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return The required status entity, if one was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             argument, or the XML text in the Archive could not be
     *             un-marshaled  
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public ProjectStatus getProjectStatus( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for the ProjectStatus entity with the given ID, return all entities
     * of the tree.
     * 
     * @param id
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return An array of status entities, composing the entity tree whose root
     *         is the ProjectStatus, if it was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     * 
     * @throws AcsJEntitySerializationFailedEx
     *             if there was a problem serializing entities to XML
     */
    public StatusBaseT[] getProjectStatusList( ObsProjectEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx,
                   AcsJEntitySerializationFailedEx;

    /**
     * Look for the ProjectStatus entity with the given ID, return all entities
     * of the tree.
     * 
     * @param id
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return An array of status entities, composing the entity tree whose root
     *         is the ProjectStatus, if it was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     * 
     * @throws AcsJEntitySerializationFailedEx
     *             if there was a problem serializing entities to XML
     */
    public StatusBaseT[] getProjectStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx,
                   AcsJEntitySerializationFailedEx;

    /**
     * Look for the ProjectStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return The XML representation of the required status entity, if one was
     *         found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String getProjectStatusXml( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for the ProjectStatus entity with the given ID, return all
     * entities of the tree.
     * 
     * @param id
     *            The entity ID of the ProjectStatus to look for.
     * 
     * @return Each string in the array represents (as XML text) one of the
     *         status entities composing the entity tree whose root is the
     *         ProjectStatus, if it was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String[] getProjectStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for the SBStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the SBStatus to look for.
     * 
     * @return The required status entity, if one was found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public SBStatus getSBStatus( SBStatusEntityT id )
            throws AcsJNullEntityIdEx,
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for all the leaf children of an OUSStatus; that is, all the
     * SBStatus entities that are either directly referenced by that
     * entity or by one of its descendant OUSStatus entities.
     * 
     * @param id
     *            The entity ID of the root OUSStatus entity.
     * 
     * @return The list of the required status entities, if any were found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the search
     *             criteria 
     *             
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public SBStatus[] getSBStatusList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx;

    /**
     * Look for all the SBStatus children of an ProjectStatus.
     * 
     * @param id
     *            The entity ID of the ProjectStatus entity.
     * 
     * @return The list of the required status entities, if any were found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the search
     *             criteria 
     *             
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public SBStatus[] getSBStatusList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx;

    /**
     * Look for the SBStatus entity with the given ID.
     * 
     * @param id
     *            The entity ID of the SBStatus to look for.
     * 
     * @return The XML representation of the required status entity, if one was
     *         found.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the supplied
     *             statusEntityId argument
     * 
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String getSBStatusXml( SBStatusEntityT id )
            throws AcsJNullEntityIdEx, 
                   AcsJNoSuchEntityEx,
                   AcsJInappropriateEntityTypeEx;

    /**
     * Look for all the leaf children of an OUSStatus; that is, all the
     * SBStatus entities that are either directly referenced by that
     * entity or by one of its descendant OUSStatus entities.
     * 
     * @param id
     *            The entity ID of the root OUSStatus entity.
     * 
     * @return The list of the required status entities, if any were found,
     *         represented as XML text.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the search
     *             criteria 
     *             
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String[] getSBStatusXmlList( OUSStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx;

    /**
     * Look for all the SBStatus children of an ProjectStatus.
     * 
     * @param id
     *            The entity ID of the ProjectStatus entity.
     * 
     * @return The list of the required status entities, if any were found,
     *         represented as XML text.
     * 
     * @throws AcsJInappropriateEntityTypeEx
     *             if the entity in the archive with a given EntityId is not of
     *             the appropriate type
     * 
     * @throws AcsJNoSuchEntityEx
     *             if there is no entity in the archive matching the search
     *             criteria 
     *             
     * @throws AcsJNullEntityIdEx
     *             if statusEntityId argument is null
     */
    public String[] getSBStatusXmlList( ProjectStatusEntityT id )
            throws AcsJNullEntityIdEx, AcsJNoSuchEntityEx,
            AcsJInappropriateEntityTypeEx;

    /**
     * Initializes the StateArchive
     * 
     * @param logger
     */
    public void initStateArchive(Logger logger);

    /**
     * Store the input ProjectStatus entity into the Archive,
     * as well as all dependent OUSStatus and SBStatus entities.
     * All entities will be created.
     * <p/>
     * 
     * <b>Note</b>: it is assumed that all references internal to the entity
     * tree are consistent; that is, all references between ProjectStatus and
     * OUSStatuses, and between OUSStatuses and
     * OUSStatuses.<br/>
     * Inconsistent entity trees will most likely cause the transaction(s) to
     * fail.
     * <p/>
     * 
     * <b>Note</b>: given the potential size of the entity tree, it is not
     * guaranteed that all entities will be written/updated in a single database
     * transaction.
     * 
     * @param opStatus
     *            The ProjectStatus entity to be stored together with its
     *            dependent entities.
     * 
     * @param ousStatus
     *            The list of all OUSStatus entities directly or
     *            indirectly referenced by the ProjectStatus (via its
     *            ObsProgram status).
     * 
     * @param sbStatus
     *            The list of all SBStatus entities referenced by the
     *            OUSStatus entities.
     * 
     * @throws AcsJStateIOFailedEx
     *             If anything went wrong while storing the entity tree; for 
     *             instance, one of the input entities was already present.
     *             
     * @throws AcsJNoSuchEntityEx Never
     */
    public void insert( ProjectStatus opStatus,
                        OUSStatus[] ousStatus,
                        SBStatus[] sbStatus ) 
        throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx;
    
    /**
     * Store the input StateChangeRecord into the Archive. The record will
     * be created; in general StateChangeRecord are never updated.
     * 
     * @param record The SBStatus entity to be stored.
     * 
     * @throws AcsJStateI
     *              If anything went wrong while storing the entity.
     */
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx;
    
    /**
     * Locks the given project status in the DB
     * 
     * @param projectStatusId
     * 
     * @throws AcsJIllegalArgumentEx
     * @throws AcsJNoSuchEntityEx
     */
    public void lockProjectStatus( ProjectStatusEntityT projectStatusId  )
    		throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx;

    /**
     * Store the input OUSStatus entity into the Archive.
     * 
     * If the entity was already present in the Archive, it will be updated;
     * otherwise it will be inserted. Note that the insert will fail if the
     * containing OUSStatus entity is not found; if the input OUSStatus is the
     * top-level one (hence has no parent), the insert will fail if the root
     * ProjectStatus entity is not found.
     * 
     * @param entity
     *            The OUSStatus entity to be stored.
     * 
     * @throws AcsJStateIOFailedEx
     *             If anything went wrong while storing the entity
     * 
     * @throws AcsJNoSuchEntityEx
     *             If the containing OUSStatus entity or the root ProjectStatus
     *             entity were not found in the State Archive
     */
    public void update( OUSStatus entity ) 
        throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx;
    
    /**
     * Store the input ProjectStatus entity into the Archive.
     * 
     * If the entity was already present in the Archive, it will be updated;
     * otherwise it will be inserted. 
     * 
     * @param entity
     *            The ProjectStatus entity to be stored.
     * 
     * @throws AcsJStateIOFailedEx
     *             If anything went wrong while storing the entity
     * 
     * @throws AcsJNoSuchEntityEx
     *             Never
     */
    public void update( ProjectStatus entity ) 
        throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx;

    /**
     * Store the input SBStatus entity into the Archive.
     * 
     * If the entity was already present in the Archive, it will be updated;
     * otherwise it will be inserted. Note that the insert will fail if the
     * containing OUSStatus entity is not found.
     * 
     * @param entity
     *            The SBStatus entity to be stored.
     * 
     * @throws AcsJStateIOFailedEx
     *             If anything went wrong while storing the entity
     * 
     * @throws AcsJNoSuchEntityEx
     *             If the containing OUSStatus entity was not found in the State
     *             Archive
     */
    public void update( SBStatus entity )
        throws AcsJStateIOFailedEx, AcsJNoSuchEntityEx;
    
    public OUSStatus[] getOUSStatusList(String[] ousStatusIds);

	/**
	 * Stores (inserts or updates) XML archive entities using current hibernate session & transaction.
	 * 
	 * @param archiveEntities
	 * @param user
	 * @param logr
	 */
	public void storeArchiveEntities(final XmlEntityStruct[] archiveEntities, 
			final String user, final Logger logr);
}
