/**
 * StateArchiveImpl.java
 *
 * Copyright European Southern Observatory 2009
 */

package alma.lifecycle.persistence;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import alma.ACSErrTypeCommon.wrappers.AcsJIllegalArgumentEx;
import alma.entity.xmlbinding.obsproject.ObsProjectEntityT;
import alma.entity.xmlbinding.ousstatus.OUSStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatus;
import alma.entity.xmlbinding.projectstatus.ProjectStatusEntityT;
import alma.entity.xmlbinding.sbstatus.SBStatus;
import alma.lifecycle.persistence.domain.OUSStatusPF;
import alma.lifecycle.persistence.domain.ObsProjectStatusPF;
import alma.lifecycle.persistence.domain.SchedBlockStatusPF;
import alma.lifecycle.persistence.domain.StateChangeRecord;
import alma.lifecycle.persistence.domain.StateEntityType;
import alma.lifecycle.utils.HibernateUtils;
import alma.statearchiveexceptions.wrappers.AcsJInappropriateEntityTypeEx;
import alma.statearchiveexceptions.wrappers.AcsJNoSuchEntityEx;
import alma.statearchiveexceptions.wrappers.AcsJNullEntityIdEx;
import alma.statearchiveexceptions.wrappers.AcsJStateIOFailedEx;
import alma.xmlentity.XmlEntityStruct;

/**
 * Implementation of the {@linkplain StateArchive} interface based on plain
 * Hibernate mappings -- no DAOs, no Services.
 *
 * @author amchavan, Jun 22, 2009
 * @version $Revision: 1.6 $
 */

// $Id: StateArchiveSimpleImpl.java,v 1.6 2011/02/03 10:28:56 rkurowsk Exp $

public class StateArchiveSimpleImpl extends StateArchiveAbstractImpl implements StateArchive {

    /** What we use for our Hibernate interaction */
    private SessionFactory factory;
    
    /** Public zero-arg constructor */
    public StateArchiveSimpleImpl() {
        setUp();
    }

    /**
     * {@inheritDoc}
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.entity.xmlbinding.projectstatus.ProjectStatus, alma.entity.xmlbinding.ousstatus.OUSStatus[], alma.entity.xmlbinding.sbstatus.SBStatus[])
     */
    @Override
    public void insert( ProjectStatus opStatus,
                        OUSStatus[] ousStatuses,
                        SBStatus[] sbStatuses ) throws AcsJStateIOFailedEx {

        if( opStatus == null || ousStatuses == null || sbStatuses == null ) {
            String msg = "Null arg";
            IllegalArgumentException e = new IllegalArgumentException( msg );
            throw new AcsJStateIOFailedEx( e );
        }
        
        Session session = factory.getCurrentSession();
        beginTransaction();
        
        String obsProjectId = opStatus.getObsProjectRef().getEntityId();
        ObsProjectStatusPF opsPF = null;
        
		try {
			opsPF = new ObsProjectStatusPF( opStatus );
		} catch (ParseException e) {
			throw new AcsJStateIOFailedEx(e);
		}
		
        session.save( opsPF );

        for( int i = 0; i < ousStatuses.length; i++ ) {
            OUSStatus entity = ousStatuses[i];
            OUSStatusPF facade = new OUSStatusPF( entity, 
                                                                obsProjectId );
            session.save( facade );
        }
        
        for( int i = 0; i < sbStatuses.length; i++ ) {
            SBStatus entity = sbStatuses[i];
            SchedBlockStatusPF facade = new SchedBlockStatusPF( entity,
                                                                obsProjectId );
            session.save( facade );
        }
        commitTransaction();
    }

    /**
     * {@inheritDoc}
	 * @see  StateArchive#findStateChangeRecords(Date, Date, String, String, String, String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<StateChangeRecord> findStateChangeRecords(Date start, Date end,
			String domainEntityId, String state, String userId,
			StateEntityType type) throws AcsJStateIOFailedEx {

		Session session = factory.getCurrentSession();
		beginTransaction();
		Criteria crit = session.createCriteria(StateChangeRecord.class);

		if (start != null) {
			crit.add(Restrictions.ge("timestamp", start));
		}
		if (end != null) {
			crit.add(Restrictions.le("timestamp", end));
		}
		if (domainEntityId != null) {
			crit.add(Restrictions.eq("domainEntityId", domainEntityId));
		}
		if (state != null) {
			crit.add(Restrictions.eq("domainEntityState", state));
		}
		if (userId != null) {
			crit.add(Restrictions.eq("userId", userId));
		}
		if (type != null) {
			crit.add(Restrictions.eq("entityType", type));
		}

		// order by timestamp
		crit.addOrder(Order.asc("timestamp"));

		List<StateChangeRecord> results = (List<StateChangeRecord>) crit.list();
		return results;
	}
    
    /**
     * @see alma.lifecycle.persistence.StateArchive#insert(alma.lifecycle.persistence.domain.StateChangeRecord)
     */
    @Override
    public void insert( StateChangeRecord record ) throws AcsJStateIOFailedEx {
        put( (Object) record );
    }
	
    @SuppressWarnings("unchecked")
    @Override
    protected SchedBlockStatusPF getSchedBlockStatusPF( String statusEntityId ) {
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from SchedBlockStatusPF sbspf where sbspf.statusEntityId = '" +
            statusEntityId +
            "'";
        Query query = session.createQuery( hql );
        List<SchedBlockStatusPF> results = 
            (List<SchedBlockStatusPF>) query.list();
        commitTransaction();
        
        SchedBlockStatusPF ret = null;
        if(results != null && results.size() > 0){
        	ret = results.get(0);
        }
        return ret;
    }
    

    @SuppressWarnings("unchecked")
    @Override
    protected OUSStatusPF getOUSStatusPF( String statusEntityId ) {
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from OUSStatusPF ousspf where ousspf.statusEntityId = '" +
            statusEntityId +
            "'";
        Query query = session.createQuery( hql );
        List<OUSStatusPF> results = 
            (List<OUSStatusPF>) query.list();
        commitTransaction();
        OUSStatusPF ret = null;
        if(results != null && results.size() > 0){
        	ret = results.get(0);
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ObsProjectStatusPF getObsProjectStatusPF( String statusEntityId ) {
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from ObsProjectStatusPF opspf where opspf.statusEntityId = '" +
            statusEntityId +
        	"'";
        Query query = session.createQuery( hql );
        List<ObsProjectStatusPF> results = 
            (List<ObsProjectStatusPF>) query.list();
        commitTransaction();
        
        ObsProjectStatusPF ret = null;
        if(results != null && results.size() > 0){
        	ret = results.get(0);
        }
        return ret;

    }
    
    /**
     * Query the database.
     * 
     * @param parentOusStatusId
     *            EntityId of an OUSStatus entity
     * 
     * @return A list of SBStatus entities, the children of the ObsUnitSet whose
     *         EntityId is equal to the input key. The entities are represented
     *         as XML text.
     * 
     * @throws AcsJNullEntityIdEx
     *             if input arg key is null
     * @throws AcsJNoSuchEntityEx
     *             if no such SBStatus entities were found
     * @throws AcsJInappropriateEntityTypeEx
     *             if some internal error occurred
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String[] getSBStatusOusXmlList( String parentOusStatusId )
            throws AcsJNullEntityIdEx, 
                AcsJNoSuchEntityEx,
                AcsJInappropriateEntityTypeEx {

        if( parentOusStatusId == null ) {
            throw new AcsJNullEntityIdEx();
        }

        // String entityID = id.getEntityId();
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from SchedBlockStatusPF sbspf where sbspf.parentOusStatusId = '" +
            parentOusStatusId + 
            "'";
        Query query = session.createQuery( hql );
        List<SchedBlockStatusPF> results = 
            (List<SchedBlockStatusPF>) query.list();

        if( results.size() == 0 ) {
            throw new AcsJNoSuchEntityEx();
        }
        
        String[] ret = new String[results.size()];
        for( int i = 0; i < ret.length; i++ ) {
            ret[i] = new String( results.get( i ).getXml() );
        }
        commitTransaction();

        return ret;
    }
    
	@SuppressWarnings("unchecked")
	@Override
	protected List<SchedBlockStatusPF> getSchedBlockStatusPFList(
			String parentOusEntityId) {
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from SchedBlockStatusPF sbspf where sbspf.parentOusStatusId = '" +
            parentOusEntityId + 
            "'";
        Query query = session.createQuery( hql );
        List<SchedBlockStatusPF> results = (List<SchedBlockStatusPF>) query.list();

        commitTransaction();

        return results;
    }

    /**
     * Save the input object into the database
     */
    private void put( Object facade ) {
        Session session = factory.getCurrentSession();
        beginTransaction();
        session.saveOrUpdate( facade );
        session.flush();
        commitTransaction();
    }

    /** Set up the connection to the database */
    private void setUp() {
        factory = HibernateUtils.getSessionFactory();
    }

    private void beginTransaction() {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
    }
    
    private void commitTransaction() {
        Session session = factory.getCurrentSession();
        session.getTransaction().commit();
    }

	@Override
	protected void update(OUSStatusPF updatedOusStatusPf) 
		throws AcsJStateIOFailedEx {
		
		put(updatedOusStatusPf);
	}

	@Override
	protected void update(ObsProjectStatusPF updatedObsProjectStatusPF)
			throws AcsJStateIOFailedEx {
		
		put(updatedObsProjectStatusPF);
		
	}

	@Override
	protected void update(SchedBlockStatusPF schedBlockStatusPF)
			throws AcsJStateIOFailedEx {
		
		put(schedBlockStatusPF);
		
	}

	@Override
	protected void insert(ObsProjectStatusPF updatedObsProjectStatusPf)
			throws AcsJStateIOFailedEx {
		put(updatedObsProjectStatusPf);
		
	}

	@Override
	protected void insert(OUSStatusPF updatedOusStatusPf)
			throws AcsJStateIOFailedEx {
		put(updatedOusStatusPf);
		
	}

	@Override
	protected void insert(SchedBlockStatusPF schedBlockStatusPF)
			throws AcsJStateIOFailedEx {
		put(schedBlockStatusPF);
		
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchiveAbstractImpl#getProjectStatusPF(alma.entity.xmlbinding.obsproject.ObsProjectEntityT)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected ObsProjectStatusPF getProjectStatusPF(
			ObsProjectEntityT obsProjectId) {
		
        Session session = factory.getCurrentSession();
        beginTransaction();
        String hql = 
            "from ObsProjectStatusPF opspf where opspf.domainEntityId = '" +
            obsProjectId.getEntityId() +  	"'";
        Query query = session.createQuery( hql );
        List<ObsProjectStatusPF> results = (List<ObsProjectStatusPF>) query.list();
        commitTransaction();
        
        ObsProjectStatusPF ret = null;
        if(results != null && results.size() > 0){
        	ret = results.get(0);
        }
        return ret;

    }

	@Override
	public void lockProjectStatus(ProjectStatusEntityT projectStatusId)
			throws AcsJIllegalArgumentEx, AcsJNoSuchEntityEx {
			// no opp
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findProjectStatusByState(java.lang.String[])
	 */
	@Override
	public ProjectStatus[] findProjectStatusByState(String[] states)
		throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see alma.lifecycle.persistence.StateArchive#findSBStatusByState(java.lang.String[])
	 */
	@Override
	public SBStatus[] findSBStatusByState(String[] states)
		throws AcsJIllegalArgumentEx, AcsJInappropriateEntityTypeEx {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public OUSStatus[] getOUSStatusList(String[] ousStatusIds) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public void storeArchiveEntities(XmlEntityStruct[] archiveEntities,
			String user, Logger logr) {
		// TODO Auto-generated method stub
		
	}

}
