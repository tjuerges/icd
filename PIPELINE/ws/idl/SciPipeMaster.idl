#ifndef _SCIPIPEMASTER_IDL_
#define _SCIPIPEMASTER_IDL_

#include <mastercomp_if.idl>

#pragma prefix "alma"

module pipelinescience {

    /**
     * This IDL file contains the Science Pipeline master component interface.
     * This interface is part of the PIPELINE subsystem pipelinescience module.
     * <P>
     * The science pipeline master component starts up and shuts down the
     * Science Pipeline and maintains knowledge its internal state. It also
     * provides a standard interface to the Executive.
     * <P>
     * The Science Pipeline master component implements the ComponentLifecycle
     * interface methods defined in ACS as part of the ACS component container
     * model. The Science Pipeline master component inherits the ACS
     * MasterComponent interface, which provides the doTransition method for
     * transitioning from one master component state to the another. The science
     * pipeline master component implements the AlmaSubsystemActions interface
     * methods, defined in ACS as part of the master component state model.
     * These methods implement the state transitions. 
     */

    interface SciPipeMaster : ACS::MasterComponent {

        /**
	 * Say hello to the science pipeline master component !
	 * This is a sanity check method.
	 * @param None
	 * @return string A string which contains a greeting message.
	 */

	string helloSciPipeMaster();

    };


};
#endif
