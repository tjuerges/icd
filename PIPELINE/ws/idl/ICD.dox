/*! \mainpage 
 
 \htmlinclude header.html

<!---------------------------------------------------------------------------->
<!-- Version ----------------------------------------------------------------->
<!---------------------------------------------------------------------------->
 <H2>COMP-70.45.00.00-70.45.00.00-003-I-ICD</H2> 
 <H2>Version: I</H2>
 <H2>2007-05-01</H2>
 <H2>Draft</H2>

<!---------------------------------------------------------------------------->
<!-- Index ------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
 \n
 <H2> INDEX </H2>\n
 \n
 <ul>
 <li> \ref one 
 <li> \ref two
 <li> \ref three
     <ul>
     <li> \ref three1
         <ul>
         <li> \ref three1A
         <li> \ref three1B
         <li> \ref three1C
         <li> \ref three1D
         <li> \ref three1E
         </ul>
     <li> \ref three2
         <ul>
         <li> \ref three2A
         <li> \ref three2B
         <li> \ref three2C
         <li> \ref three2D
         <li> \ref three2E
         </ul>
     </ul>
 <li> \ref four
     <ul>
     <li> \ref four1
         <ul>
         <li> \ref four1A
	 </ul>
     <li> \ref four2
         <ul>
         <li> \ref four2A
	 </ul>
     </ul>
 <li> \ref five
     <ul>
     <li> \ref five1
     <li> \ref five2
     </ul>
 <li> \ref six
     <ul>
     <li> \ref six1
     <li> \ref six2
     </ul>
 <li> \ref seven
     <ul>
     <li> \ref seven1
     <li> \ref seven2
     </ul>
 <li> \ref eight
     <ul>
     <li> \ref eight1
     <li> \ref eight2
     </ul>
 <li> \ref nine
     <ul>
     <li> \ref nine1
     <li> \ref nine2
     </ul>
 <li> \ref ten
 <li> \ref eleven
     <ul>
     <li> \ref eleven1
     <li> \ref eleven2
     </ul>
 <li> \ref twelve
     <ul>
     <li> \ref twelve1
     <li> \ref twelve2
     <li> \ref twelve3
     <li> \ref twelve4
     <li> \ref twelve5
     <li> \ref twelve6
     </ul>
 </ul>

<!---------------------------------------------------------------------------->
<!-- Change Record ----------------------------------------------------------->
<!---------------------------------------------------------------------------->
 \n
 <H2> CHANGE RECORD </H2>\n
 \n
<p>
<ul>
<li><em>January 24, 2005</em>
This is the CDR2 version, ALMA-70.45.00.00-003-G-ICD, version G, dated 
2004-06-01 converted to the Doxygen-based system.

<li><em>February 15, 2005</em>
Updated to include changes thru February 15, 2005.

<li><em>May 1, 2007</em>
Updated for CDR5 to include changes thru May 1, 2007.
</ul>
 \n

<!---------------------------------------------------------------------------->
<!-- Section 1. Purpose ------------------------------------------------------>
<!---------------------------------------------------------------------------->
 \section one 1. Purpose

  The Pipeline Subsystem is divided into two major systems: the Quicklook
  Display system and the Science Pipeline system.

  The primary purpose of the Quicklook Display system is to provide rapid
  feedback to the astronomer on duty and to the telescope operator on the 
  scientific quality of the data taken during the current observing session.

  The Quicklook Display system must:

  <ul>
  <li> monitor and display selected calibration results over the current 
  observing session
  <li> monitor and display the status of the array over the current observing
  session
  <li> quickly reduce and display data accumulated over the current observing
  session
  <li> support customization of the displays by the astronomer on duty or
  operator
  <li> alert the astronomer on duty and telescope operator that the quality of
  the data is deteriorating
  <li> store the quicklook results temporarily in the archive
  </ul>

  The Quicklook Display system must operate in near real-time at the 
  Operations Support Facility (OSF).

  The primary purpose of the Science Pipeline system is to produce reference
  images of scientific quality for storage in the archive and distribution to 
  the PI(s).

  The Science Pipeline system must:

  <ul>
  <li> automatically process data taken in the standard observing modes
  <li> compute quality assessment metrics and store them in the archive
  <li> store flag tables, calibration solutions, and reference images in
       the archive
  <li> store the processing summary log in the archive
  </ul>

  The Science Pipeline must operate at the Santiago Central Office (SCO) (and
  initially at the OSF) and at the ALMA Regional Centers (ARCs),


<!---------------------------------------------------------------------------->
<!-- Section 2. Namespace ---------------------------------------------------->
<!---------------------------------------------------------------------------->
 \section two 2. Namespace

  The Quicklook Display IDL interface namespace is
  alma.pipelineql.\<interface\>. The corresponding XML namespace is
  Alma/pipelineql.

  The Science Pipeline IDL interface namespace is
  alma.pipelinescience.\<interface\>. The corresponding XML namespace
  is Alma/pipelinescience.

<!---------------------------------------------------------------------------->
<!-- Section 3. Interfaces --------------------------------------------------->
<!---------------------------------------------------------------------------->
 \section three 3. Interfaces

<!---------------------------------------------------------------------------->
<!-- Section 3.1 Quicklook Display Interfaces -------------------------------->
<!---------------------------------------------------------------------------->
 \subsection three1 3.1 Quicklook Display Interfaces

 <p>
 The following interfaces are described here:
 <ul>
 <li> the quicklook display master component interface, see \ref three1A
 <li> the quicklook display  manager interface, see \ref three1B
 <li> the quicklook display executive interface, see \ref three1C
 <li> the quicklook display operator interface, see \ref three1D
 <li> the quicklook display scheduler interface, see \ref three1E
 </ul>
 <p>
 The Quicklook Display system master component activates the Quicklook Display system
 and starts the Quicklook Display system manager component. The Quicklook Display
 system manager component must be started before the start of observing.
 <p>
 The Quicklook Display system consists of 3 parts: the telescope
 calibration monitor, the array monitor, and the data processor.
 Once activated each of the 3 Quicklook Display parts runs automatically.
 The operator may enable or disable each part and / or interact with the
 associated GUIs but is not permitted to interact with the data handling
 or the data processing procedures.
 <p>
 The Quicklook Display system manager component interfaces to the Scheduling
 (via methods and events) and Offline (via DataCapture methods and events)
 subsystems.  To date implements only the telescope calibration monitor functions
 are implemented.

 <p>
 The following quantities are defined in the IDL file: 
 <ul>
 <li>typedef string pipelineql::QlEntityId
 <i>The quicklook display system entity ids</i>
 <li>typedef string pipelineql::QlPartId
 <i>The quicklook display system part ids</i>
 <li>typedef string pipelineql:SessionTitle
 <i>The quicklook display system session title</i>
 <li>typedef sequence<QdEntityId> pipelineql::QlEntityIdSeq
 <i>A sequence of quicklook system entity ids</i>
 <li>typedef asdmIDLTypes::IDLEntityRef pipelineql::SessionRef
 <i>The entity reference for a session</i>
 <li>typedef asdmIDLTypes::IDLEntityRef pipelineql::SchedblockRef
 <i>A scheduling block entity reference</i>
 </ul>
 <p>
 The following exceptions are defined in the IDL file.
 <ul>
 <li>pipelineql::InvalidStateError
 <i>The Quicklook Display system state is invalid for the given request.</i>
 </ul>

 <p>
 The following enumerations are defined in the IDL file.
 <ul>
 <li> pipelineql::QlDisplayStatus { QL_STARTED, QL_STOPPED }
 <i> The current status of the Quicklook Display system </i>
 <li> pipelineql::QlDisplayOpState { QL_DISABLED, QL_ENABLED }
 <i> The current operational state of the Quicklook Display system.</i>
 <li> pipelineql::QlSessionstate { QLSESSION_DISABLED, QLSESSION_ENABLED }
 <i> The current operational state of an individual session.</i>
 <li> pipelineql::QlObservingState { QLOBSERVING_STARTED, QLOBSERVING_DONE,
      QLOBSERVING_DISCONNECTED }
 <i> The current observing status of an individual session.</i>
 <li> pipelineql::QlProcessingCompletionStatus { QLPROCESSING_COMPLETE, QLPROCESSING_INCOMPLETE }
 <i> The Quicklook Display system processing completion status. </i>
 </ul>

<!---------------------------------------------------------------------------->
<!-- Section 3.1.A The Quicklook Display Master Component Interface ---------->
<!---------------------------------------------------------------------------->
 \subsection three1A 3.1A The Quicklook Display Master Component Interface

 <p>
 The Quicklook Display system master component activates and deactivates
 the Quicklook Display system and manages the state of the subsystem. The
 master component interface is used by the Executive Subsystem to start and
 stop the Quicklook Display System. It implements the ACS master component
 interface.
 <p>
 See the pipelineql::QlDisplayMaster documentation.
 <p>
 For the source code see \ref twelve1. \n

<!---------------------------------------------------------------------------->
<!-- Section 3.1.B The Quicklook Display Manager Interface --------------------------->
<!---------------------------------------------------------------------------->
 \subsection three1B 3.1.B The Quicklook Display Manager Interface

 <p>
 The Quicklook Display manager system component activates and deactivates the
 telescope monitoring, array monitoring, and data processing operations.
 The Quicklook Display manager interface or its sub interfaces are used by the
 Executive Subsystem, the Scheduler, or the Operator to enable or disable the above
 operations.
 <p>
 See the pipelineql::QlDisplayManager documentation.
 <p>
 For the source code see \ref twelve2. \n


<!---------------------------------------------------------------------------->
<!-- Section 3.1.C The QuickLook Display Executive Interface ------------------>
<!---------------------------------------------------------------------------->
 \subsection three1C 3.1.C The QuickLook Display Executive Interface

 <p>
 This interface is used by the Executive Subsystem to start and stop
 the Quicklook Display system.
 <p>
 See the pipelineql::QlDisplayExecutive documentation.
 <p>
 For the source code see \ref twelve2. \n


<!---------------------------------------------------------------------------->
<!-- Section 3.1.D The Quicklook Display Operator Interface ------------------->
<!---------------------------------------------------------------------------->
 \subsection three1D 3.1.D The Quicklook Display Operator Interface

 <p>
 This interface is used by the telescope operator to monitor the state
 of and interact with the Quicklook Display system. It is internal
 to the QuickLook Display system.
 <p>
 See the pipelineql::QlDisplayOperator documentation.
 <p>
 For the source code see \ref twelve2. \n


<!--------------------------------------------------------------------------->
<!-- Section 3.1.E The Quicklook Display Scheduler Interface ----------------->
<!--------------------------------------------------------------------------->
 \subsection three1E 3.1.E The Quicklook Display Scheduler Interface

 <p>
 This interface is used by the Scheduling Subsystem to initialize quicklook 
 display operations for an individual observing session.
 <p>
 See the pipelineql::QlDisplayScheduler documentation.
 <p>
 For the source code see \ref twelve2. \n


<!--------------------------------------------------------------------------->
<!-- Section 3.2 Science Pipeline Interfaces -------------------------------->
<!--------------------------------------------------------------------------->
 \subsection three2 3.2 The Science Pipeline Interfaces

 <p>
 The following interfaces are described here:
 <ul>
 <li> the master component interface, see \ref three2A
 <li> the science pipeline manager interface, see \ref three2B
 <li> the science pipeline executive interface, see \ref three2C
 <li> the science pipeline operator interface, see \ref three2D
 <li> the science pipeline Scheduler interface, see \ref three2E
 </ul>
 <p>
 The Science Pipeline system master component activates the Science Pipeline
 manager component. The Science Pipeline manager component must be activated
 before pipeline processing requests can be received. The executive, operator,
 and scheduler interfaces provide different views of the science pipeline
 manager component.

 <p>
 The following quantities are defined in the IDL file: 
 <ul>
 <li>typedef string pipelinescience::ProcessingRequest 
 <i>The pipeline processing request XML string issued by the scheduler.</i>
 <li>typedef string pipelinescience::ProjectStatusEntityId
 <i>The id of the project status entity.</i>
 <li>typedef string pipelinescience::ObsUnitSetPartId
 <i>The part id of the observing unit set which is defined inside the project
 status entity.</i>
 <li>typedef string pipelinescience::PprPartId
 <i>The part id of the pipeline processing request which is defined inside the
 project status entity.</i>
 <li>typedef string pipelinescience::ProcessId
 <i>The pipeline process id.</i>
 </ul>

 <p>
 The following exceptions are defined in the IDL file: 
 <ul>
 <li>pipelinescience::InvalidStateError
 <i>The science pipeline is in an invalid state.</i>
 <li>pipelinescience::QueueOperationsError
 <i>A queue operations error occurred.</i>
 <li>pipelinescience::AuthorizationFailureError
 <i>The requested operation does not have the proper authorization</i>
 <li>pipelinescience::InvalidProcessingRequestError
 <i>The pipeline processing request is invalid.</i>
 <li>pipelinescience::ProcessingError
 <i>A processing error occurred.</i>
 <li>pipelinescience::NotInQueueError
 <i>The processing request is not in the queue.</i>
 </ul>

 <p>
 The following enumerations are defined in the IDL interfaces.
 <ul>
 <li>pipelinescience::OperatingMode { OM_AUTOMATED, OM_INTERACTIVE, OM_SIMULATION }
 <i>The pipeline operations mode.</i>
 <li>pipelinescience::QueueStatus { QS_ENABLED, QS_DISABLED }
 <i>The pipeline processing queue status.</i>
 <li>pipelinescience::ExecutionStatus { ES_SUBMITTED, ES_QUEUED, ES_RUNNING, ES_COMPLETED }
 <i>The execution status of a pipeline processing request. </i>
 <li>pipelinescience::CompletionStatus { CS_REJECTED, CS_FAILED, CS_COMPLETE_FAILED, 
     CS_COMPLETE_SUCCEEDED }
 <i>The completion status of a pipeline processing request. </i>
 </ul>
 

<!---------------------------------------------------------------------------->
<!-- Section 3.2.A The Science Pipeline Master Component Interface ----------->
<!---------------------------------------------------------------------------->
 \subsection three2A 3.2.A The Science PIpeline Master Component Interface

 <p>
 The Science Pipeline system master component activates and deactivates
 the Science Pipeline system and manages the state of the subsystem. The
 master component interface is used by the Executive Subsystem to start
 and stop the Science Pipeline system. It implements the ACS master component
 interface.
 <p>
 See the pipelinescience::SciPipeMaster documentation.
 <p>
 For the source code see \ref twelve3. \n

<!---------------------------------------------------------------------------->
<!-- Section 3.2.B The Science Pipeline Manager Interface -------------------->
<!---------------------------------------------------------------------------->
 \subsection three2B 3.2.B The Science Pipeline Manager Interface

 <p>
 The Science Pipeline manager component manages the pipeline processing queue.
 It implements the executive, operator, and scheduler interfaces.
 <p>
 See the pipelinescience::SciPipeManager documentation.
 <p>
 For the source code see \ref twelve4. \n


<!---------------------------------------------------------------------------->
<!-- Section 3.2.C The Science Pipeline Executive Interface ------------------>
<!---------------------------------------------------------------------------->
 \subsection three2C 3.2.C The Science Pipeline Executive Interface

 <p>
 This interface is used by the Executive Subsystem to start and stop
 the queuing of pipeline processing requests and to examine the state
 of the pipeline processing queue.
 <p>
 See the pipelinescience::SciPipeExecutive documentation.
 <p>
 For the source code see \ref twelve4. \n

<!---------------------------------------------------------------------------->
<!-- Section 3.2.D The Science Pipeline Operator Interface ------------------->
<!---------------------------------------------------------------------------->
 \subsection three2D 3.2.D The Science Pipeline Operator Interface

 <p>
 This interface is used by the Pipeline operator to monitor the state
 of and interact with the pipeline processing queue. It is internal
 to the Science Pipeline subsystem.
 <p>
 See the pipelinescience::SciPipeOperator documentation.
 <p>
 For the source code see \ref twelve4. \n


<!--------------------------------------------------------------------------->
<!-- Section 3.2.E The Science Pipeline Scheduler Interface ----------------->
<!--------------------------------------------------------------------------->
 \subsection three2E 3.2.E The Science Pipeline Scheduler Interface

 <p>
 This interface is used by the Scheduling Subsystem to submit pipeline
 processing requests to the pipeline processing queue and to monitor
 their progress.
 <p>
 See the pipelinescience::SciPipeScheduler documentation.
 <p>
 For the source code see \ref twelve4. \n


<!--------------------------------------------------------------------------->
<!-- Section 4. Data Types and Entities -------------------------------------->
<!--------------------------------------------------------------------------->
 \section four 4. Data Types and Entities

<!--------------------------------------------------------------------------->
<!-- Section 4.1 Quicklook Display Data Types and Entities ------------------>
<!--------------------------------------------------------------------------->
 \subsection four1 4.1 Quicklook Display Data Types and Entities
 <p>
 The Quicklook Display system currently creates the following entities.
 <ul>
 <li>QuickLookDisplay
 <i>Only a skeleton schema is currently available, see \ref four1A </i> 
 </ul>
 <p>
 The quicklook results are not stored in the science archive. They are stored
 temporarily on disk and later in a quicklook display area in the archive
 maintained by operations for that purpose. The quick look results will
 eventually include telescope calibration, array monitoring, and data processing
 results the form of reports, tables, and plots.
 <p>
 The quick look results will be stored in XML with the following specific
 formats. Note that the actual data will stored in binary in Offline / AIPS++
 compatible format to facilitate rapid access to the Offline / AIPS++ data
 reduction facilities. These results are for internal ALMA usage not for
 export. References to the quicklook results will be maintained in a yet TBD
 manner inside the project status entity.
 <ul>
 <li> summary reports and logs in ascii text
 <li> monitoring results tables in XML tables
 <li> vector plots in SVG or derived variant
 <li> image plots in PNG / JPEG or derived variant
 </ul>
 <p>
 We expect the format of this entity to continue to evolve rapidly in response
 to further comments from commissioning and operations.

<!--------------------------------------------------------------------------->
<!-- Section 4.1.A The Quicklook Display Schema ----------------------------->
<!--------------------------------------------------------------------------->
 \subsection four1A 4.1.A The QuickLookDisplay Entity Schema
 <p>
 A graphical representation of the current skeleton schema is shown here.
 <p>
 Include a picture here at some point !
 <p>
 See \ref twelve5 for the actual skeleton schema.
 <p>
 A graphical representation of the original concept schema is shown here.
 <p>
 Include a picture here at some point !

<!--------------------------------------------------------------------------->
<!-- Section 4.2 Science Pipeline Data Types and Entities ------------------->
<!--------------------------------------------------------------------------->
 \subsection four2 4.2 Science Pipeline Data Types and Entities

 <p>
 The Science Pipeline System currently creates the following entities.
 <ul>
 <li>SciPipeResult
 <i>Only a skeleton schema is currently available, see \ref four2A </i> 
 </ul>
 <p>
 The science pipeline results are stored in the science archive. The science
 pipeline results will include reports, flagging tables, calibration tables,
 images, and plots.
 <p>
 The science pipeline results will be stored in XML with the following specific
 formats. Note only calibration solutions are stored. The calibrated
 visibilities are not stored. References to the science pipeline results will
 be maintained in the project status entity, which also maintains a reference
 to the processing parameters and processing script.
 <p>
 <ul>
 <li> summary reports in ascii text
 <li> flagging tables in ASDM compatible format XML
 <li> calibration tables in in ASDM compatible format XML 
 <li> images in FITS
 <li> plots in SVG or derived variant
 <li> plots in PNG / JPEG or derived variant
 </ul>

<!---------------------------------------------------------------------------->
<!-- Section 4.2.A The Science Pipeline Results Schema ----------------------->
<!---------------------------------------------------------------------------->
 \subsection four2A 4.2.A The Science Pipeline Results Entity Schema

 <p>
 A graphical representation of the current skeleton schema is shown here.
 <p>
 Include a picture here at some point !
 <p>
 See \ref twelve6 for the actual skeleton schema.
 <p>
 A graphical representation of the original concept schema is shown here.
 <p>
 Include a picture here at some point !

<!---------------------------------------------------------------------------->
<!-- Section 5 Published Events  --------------------------------------------->
<!---------------------------------------------------------------------------->
 \section five 5. Published Events

<!---------------------------------------------------------------------------->
<!-- Section 5.1 Quicklook Display Published Events  ------------------------->
<!---------------------------------------------------------------------------->
 \subsection five1 5.1 Quicklook Display Published Events

 <p> The Quicklook Display Subsystem publishes events on the following
 notification channel:
 <ul>
 <li>pipelineql::CHANNELNAME_QLDISPLAYMANAGER
 </ul>
 <p>
 The Quicklook Display Subsystem currently publishes the following events:
 <ul>
 <li>pipelineql::QlSessionProcessingDoneEvent
 <i>This event is issued when the quicklook processing for an observing
 session is finished.</i>
 </ul>
 <p>
 See \ref twelve2 for details. \n

<!---------------------------------------------------------------------------->
<!-- Section 5.2 Science Pipeline Published Events --------------------------->
<!---------------------------------------------------------------------------->
 \subsection five2 5.2 Science Pipeline Published Events

 <p>
 The Science Pipeline Subsystem publishes events on the following notification
 channel
 <ul>
 <li>pipelinescience::CHANNELNAME_SCIPIPEMANAGER
 </ul>
 <p>
 The Science Pipeline Subsystem currently publishes the following events:
 <ul>
 <li>pipelinescience::ScienceProcessingDoneEvent
 <i>This event is issued when a science pipeline run is terminated.</i>
 </ul>
 <p>
 See \ref twelve4 for details. \n

<!---------------------------------------------------------------------------->
<!-- Section 6. Received Events ---------------------------------------------->
<!---------------------------------------------------------------------------->
 \section six 6. Received Events

<!---------------------------------------------------------------------------->
<!-- Section 6.1 Quicklook Display Received Events --------------------------->
<!---------------------------------------------------------------------------->
 \subsection six1 6.1 Quicklook Display Received Events

 <p>
 The Quicklook Display System currently listens for the following events from
 the Scheduling Subsystem:
 <ul>
 <li>StartSessionEvent
 <li>EndSessionEvent
 </ul>
 <p> It also listens for the following events from the Data Capturer 
 component:
 <ul>
 <li>DataCaptureStartedEvent
 <li>ScanReducedEvent
 </ul>

<!---------------------------------------------------------------------------->
<!-- Section 6.2 Science Pipeline Received Events ---------------------------->
<!---------------------------------------------------------------------------->
 \subsection six2 6.2 Science Pipeline Received Events
 <p>
 The Science Pipeline Subsystem does not receive any events.

<!---------------------------------------------------------------------------->
<!-- Section 7 Published Streams --------------------------------------------->
<!---------------------------------------------------------------------------->
 \section seven 7. Published Streams

<!---------------------------------------------------------------------------->
<!-- Section 7.1 Quicklook Display Published Streams ------------------------->
<!---------------------------------------------------------------------------->
 \subsection seven1 7.1 Quicklook Display Published Streams
 <p>
 The Quicklook Display system does not publish any data streams.
 <p>
 The QuickLook Display system data volume to the archive is somewhat
 uncertain. However assuming that the measurement set data rate is
 approximately the same as the average visibility rate, and that the
 reports, calibration tables, images, and plots add at most a further
 10%, then the expected data rate is ~570 GB per day if all the intermediate
 products are stored. If only quicklook result summaries are stored the
 rate is much less, <~ 0.5 GB per day assuming a 0.1% data rate. The maximum
 storage required will be the archive lifetime (still TBD) times this.

<!---------------------------------------------------------------------------->
<!-- Section 7.2 Science Pipeline Published Streams -------------------------->
<!---------------------------------------------------------------------------->
 \subsection seven2 7.2 Science Pipeline Published Streams
 <p>
 The Science Pipeline System does not publish any data streams.
 <p>
 The Science Pipeline system data volume is also uncertain. Current estimates
 of the average image data rate are running ~3% of the average visibility rate.
 Flagging and calibration tables add at most another few percent. Assuming a
 results data rate equal to 6% of the average visibility rate of ~31 GB per day.

<!---------------------------------------------------------------------------->
<!-- Section 8 Consumed Streams ------------------------------------------->
<!---------------------------------------------------------------------------->
 \section eight 8. Consumed Streams

<!---------------------------------------------------------------------------->
<!-- Section 8.1 Quicklook Display Consumed Streams -------------------------->
<!---------------------------------------------------------------------------->
 \subsection eight1 8.1 Quicklook Display Consumed Streams

 <p>
 The QuickLook Display System consumes the Correlator System data streams 
 (Spectral, Channel Averaged, and Total Power). This will be handled via
 the Offline real time filler.
 See the Correlator ICD for details.

<!---------------------------------------------------------------------------->
<!-- Section 8.2 Science Pipeline Consumed Streams --------------------------->
<!---------------------------------------------------------------------------->
 \subsection eight2 8.2 Science Pipeline Consumed Streams

 <p>
 The Science Pipeline Display system does not consume any data streams. 

<!---------------------------------------------------------------------------->
<!-- Section 9 Dependencies on External Data Types --------------------------->
<!---------------------------------------------------------------------------->
 \section nine 9. Dependencies on External Data Types

<!---------------------------------------------------------------------------->
<!-- Section 9.1 Quicklook Dependencies on External Data Types ------------ -->
<!---------------------------------------------------------------------------->
 \subsection nine1 9.1 Quicklook Dependencies on External Data Types

 <p>
 The Quicklook Display system is dependent on the completeness and correctness
 of ASDM data published by the Control and Telcal Subsystems and sent to the
 Control / Offline data capture component, and on the completeness and
 correctness of the ASDM Corba structs returned by the data capture component.
 It also depends on the completeness and correctness of the correlator data
 stream published by the Correlator Subsystem. See the Offline and Correlator
 ICDs for details.
 <p>
 The QuickLook Display System also depends on the completeness and correctness
 of the measurements sets produced by the Offline filler component and used
 by the Offline data visualization and reduction engines.

<!---------------------------------------------------------------------------->
<!-- Section 9.2 Science Pipeline Dependencies on External Data Types -------->
<!---------------------------------------------------------------------------->
 \subsection nine2 9.2 Science Pipeline Dependencies on External Data Types

 <p>
 The Science Pipeline is dependent on the APDM via the ProjectStatus entity
 maintained by the Scheduling Subsystem, and on the PipelineProcessingRequest
 and ObsUnitSet data structures maintained therein. It is also dependent
 on the ASDM tables stored in the archive and calibration data stored in
 the calibration database.
 <p>
 The Science Pipeline System also depends on the completeness and correctness
 of the measurements sets produced by the Offline filler component and used
 by the Offline data visualization and reduction engines. It also depends
 on the completeness and correctness of the calibration tables produced by
 the Offline calibration engine(s).
 

<!---------------------------------------------------------------------------->
<!-- Section 10 ACS Dependencies --------------------------------------------->
<!---------------------------------------------------------------------------->
 \section ten 10. ACS Dependencies

 Both the Quicklook Display and Science Pipeline System use the following
 ACS facilities and services:
 <ul>
 <li>the ORBS (C++, Java, Python)
 <li>the CDB facility
 <li>the container / component (master, ACS) model service
 <li>the naming service
 <li>the logging service
 <li>the error service,
 <li>the notification channel service
 <li>the audio visual streaming service
 </ul>
 

<!---------------------------------------------------------------------------->
<!-- Section 11 Required Subsystem and Interfaces ---------------------------->
<!---------------------------------------------------------------------------->
 \section eleven 11. Required Subsystems and Interfaces

<!---------------------------------------------------------------------------->
<!-- Section 11.1 Quicklook Display Required Subsystems and Interfaces ------->
<!---------------------------------------------------------------------------->
 \subsection eleven1 11.1 Quicklook Display Required Subsystems and Interfaces

 <p>
 The Quicklook Display System depends on:
 <ul>
 <li>offline::DataCapturer
 <i>The Control / Offline data capture interface. Used to capture Control and
 Telcal Subsystem auxiliary / meta data.</i>

 <li>bulkstream::BulkStreamListener 
 <i>The Archive Subsystem bulk stream listener interface. Used to capture the
 correlator data stream.</i>

 <li>alma::xmlstore
 <i>The Archive Subsystem XML storage interface. Used to store the quicklook
 display results entity.</i>

 <li>offline:filler
 <i>The Offline ASDM to measurement set conversion interface. Used to convert
 the Control / Telcal auxiliary / meta data and the Correlator visibility
 data to a measurement set. </i>

 <li>BULK_DATA_SENDER::BulkDataSender
 <i>The Archive Subsystem bulk stream sender interface. May be used to temporarily
 archive measurement sets, calibration tables, and images.</i>

 <li>offline:Casapy
 <i>The Casapy data reduction engines belong to Offline</i>
 </ul>

<!---------------------------------------------------------------------------->
<!-- Section 11.2 Science Pipeline Required Subsystems and Interfaces -------->
<!---------------------------------------------------------------------------->
 \subsection eleven2 11.2 Science Pipeline Required Subsystems and Interfaces

 <p>
 The Science Pipeline system depends on:
 <ul>
 <li>alma::xmlstore
 <i>The Archive Subsystem XML entity retrieval and storage interface. Used to
 retrieve all or parts of the ProjectStatus entity and to store the
 PipelineResults entity.</i>

 <li>offline::filler
 <i>The Offline Subsystem ASDM to measurement set conversion interface. Used
 to retrieve an ASDM from the archive and convert it to a measurement set on
 disk.</i>

 <li>BULK_DATA_SENDER::BulkDataSender
 <i>The Archive Subsystem bulk data sender interface. Used to store the
 reduced images and other bulk data in the archive.</i>

 <li>offline:Casapy
 <i>The Casapy data reduction engines belong to Offline</i>
 </ul>

<!---------------------------------------------------------------------------->
<!-- Section 12. References to the Actual Schema and IDL Files --------------->
<!---------------------------------------------------------------------------->

 \section twelve 12. References to the Actual Schema and IDL files

 \subsection twelve1 The Quicklook Display System Master Component IDL file

 \include QlDisplayMaster.idl

 \subsection twelve2 The Quicklook Display System Component IDL file

 \include QlDisplayManager.idl

 \subsection twelve3 The Science Pipeline System Master Component IDL file

 \include SciPipeMaster.idl

 \subsection twelve4 The Science Pipeline Manager Component IDL file

 \include SciPipeManager.idl

 \subsection twelve5 The QuickLookDisplay Schema

 \include QuickLookDisplay.xsd

 \subsection twelve6 The SciPipeResults Schema

 \include SciPipeResults.xsd

*/
