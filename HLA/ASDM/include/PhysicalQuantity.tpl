/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File @PhysicalQuantity@.h
 */

#ifndef @PhysicalQuantity@_CLASS
#define @PhysicalQuantity@_CLASS

#include <vector>
#include <iostream>
#include <string>
using namespace std;

#ifndef WITHOUT_ACS
#include <asdmIDLTypesC.h>
using asdmIDLTypes::IDL@PhysicalQuantity@;
#endif

#include <StringTokenizer.h>
#include <NumberFormatException.h>
using asdm::StringTokenizer;
using asdm::NumberFormatException;

#include "EndianStream.h"
using asdm::EndianOSStream;
using asdm::EndianISStream;


namespace asdm {

class @PhysicalQuantity@;
@PhysicalQuantity@ operator * ( double , const @PhysicalQuantity@ & );
ostream & operator << ( ostream &, const @PhysicalQuantity@ & );
istream & operator >> ( istream &, @PhysicalQuantity@ &);

/**
 * @PhysicalQuantityDefinition@.
 * 
 * @version 1.00 Jan. 7, 2005
 * @author Allen Farris
 * 
 * @version 1.1 Aug 8, 2006
 * @author Michel Caillat 
 * added toBin/fromBin methods.
 */
class @PhysicalQuantity@ {
  /**
   * Overloading of multiplication operator.
   * @param d a value in double precision .
   * @param x a const reference to a @PhysicalQuantity@ .
   * @return a @PhysicalQuantity@ 
   */
  friend @PhysicalQuantity@ operator * ( double d, const @PhysicalQuantity@ & x );

  /**
   * Overloading of << to output the value an @PhysicalQuantity@ on an ostream.
   * @param os a reference to the ostream to be written on.
   * @param x a const reference to a @PhysicalQuantity@.
   */
  friend ostream & operator << ( ostream & os, const @PhysicalQuantity@ & x);

  /**
   * Overloading of >> to read an @PhysicalQuantity@ from an istream.
   */
  friend istream & operator >> ( istream & is, @PhysicalQuantity@ & x);

public:
	/**
	 * The nullary constructor (default).
	 */
	@PhysicalQuantity@();

	/**
	 * The copy constructor.
	 */
	@PhysicalQuantity@(const @PhysicalQuantity@ &);

	/**
	 * A constructor from a string representation.
	 * The string passed in argument must be parsable into a double precision
	 * number to express the value in radian of the angle.
	 *
	 * @param s a string.
	 */
	@PhysicalQuantity@(const string &s);
#ifndef WITHOUT_ACS
	/**
	 *
	 * A constructor from a CORBA/IDL representation.
	 * 
	 * @param idl@PhysicalQuantity@ a cons ref to an IDL@PhysicalQuantity@.
	 */
	@PhysicalQuantity@(const IDL@PhysicalQuantity@ & idl@PhysicalQuantity@);
#endif

	/**
	 * A constructor from a value in double precision.
	 * The value passed in argument defines the value of the @PhysicalQuantity@ in radian.
	 */
	@PhysicalQuantity@(double value);

	/**
	 * The destructor.
	 */
	virtual ~@PhysicalQuantity@();

	/**
	 * A static method equivalent to the constructor from a string.
	 * @param s a string?.
	 */
	static double fromString(const string& s);

	/**
	 * Conversion into string.
	 * The resulting string contains the representation of the value of this @PhysicalQuantity@.
	 *
	 * @return string
	 */
	static string toString(double);

	/**
	 * Parse the next (string) token of a StringTokenizer into an angle.
	 * @param st a reference to a StringTokenizer.
	 * @return an @PhysicalQuantity@.
	 */
	static @PhysicalQuantity@ get@PhysicalQuantity@(StringTokenizer &st) throw(NumberFormatException);
			
	/**
	 * Write the binary representation of this to an EndianOSStream .
	 * @param eoss a reference to an EndianOSStream .
	 */		
	void toBin(EndianOSStream& eoss);

	/**
	 * Write the binary representation of a vector of @PhysicalQuantity@ to a EndianOSStream.
	 * @param angle the vector of @PhysicalQuantity@ to be written
	 * @param eoss the EndianOSStream to be written to
	 */
	static void toBin(const vector<@PhysicalQuantity@>& angle,  EndianOSStream& eoss);
	
	/**
	 * Write the binary representation of a vector of vector of @PhysicalQuantity@ to a EndianOSStream.
	 * @param angle the vector of vector of @PhysicalQuantity@ to be written
	 * @param eoss the EndianOSStream to be written to
	 */	
	static void toBin(const vector<vector<@PhysicalQuantity@> >& angle,  EndianOSStream& eoss);
	
	/**
	 * Write the binary representation of a vector of vector of vector of @PhysicalQuantity@ to a EndianOSStream.
	 * @param angle the vector of vector of vector of @PhysicalQuantity@ to be written
	 * @param eoss the EndianOSStream to be written to
	 */
	static void toBin(const vector<vector<vector<@PhysicalQuantity@> > >& angle,  EndianOSStream& eoss);

	/**
	 * Read the binary representation of an @PhysicalQuantity@ from a EndianISStream
	 * and use the read value to set an  @PhysicalQuantity@.
	 * @param eiss the EndianStream to be read
	 * @return an @PhysicalQuantity@
	 */
	static @PhysicalQuantity@ fromBin(EndianISStream& eiss);
	
	/**
	 * Read the binary representation of  a vector of  @PhysicalQuantity@ from an EndianISStream
	 * and use the read value to set a vector of  @PhysicalQuantity@.
	 * @param eiss a reference to the EndianISStream to be read
	 * @return a vector of @PhysicalQuantity@
	 */	 
	 static vector<@PhysicalQuantity@> from1DBin(EndianISStream & eiss);
	 
	/**
	 * Read the binary representation of  a vector of vector of @PhysicalQuantity@ from an EndianISStream
	 * and use the read value to set a vector of  vector of @PhysicalQuantity@.
	 * @param eiss the EndianISStream to be read
	 * @return a vector of vector of @PhysicalQuantity@
	 */	 
	 static vector<vector<@PhysicalQuantity@> > from2DBin(EndianISStream & eiss);
	 
	/**
	 * Read the binary representation of  a vector of vector of vector of @PhysicalQuantity@ from an EndianISStream
	 * and use the read value to set a vector of  vector of vector of @PhysicalQuantity@.
	 * @param eiss the EndianISStream to be read
	 * @return a vector of vector of vector of @PhysicalQuantity@
	 */	 
	 static vector<vector<vector<@PhysicalQuantity@> > > from3DBin(EndianISStream & eiss);	 
	 
	 /**
	  * An assignment operator @PhysicalQuantity@ = @PhysicalQuantity@.
	  * @param x a const reference to an @PhysicalQuantity@.
	  */
	 @PhysicalQuantity@ & operator = (const @PhysicalQuantity@ & x);
	 
	 /**
	  * An assignment operator @PhysicalQuantity@ = double.
	  * @param d a value in double precision.
	  */
	 @PhysicalQuantity@ & operator = (const double d);

	 /**
	  * Operator increment and assign.
	  * @param x a const reference to an @PhysicalQuantity@.
	  */
	@PhysicalQuantity@ & operator += (const @PhysicalQuantity@ & x);

	/**
	 * Operator decrement and assign.
	 * @param x a const reference to an @PhysicalQuantity@.
	 */
	@PhysicalQuantity@ & operator -= (const @PhysicalQuantity@ & x);

	/**
	 * Operator multiply and assign.
	 * @param x a value in double precision.
	 */
	@PhysicalQuantity@ & operator *= (const double x);

	/**
	 * Operator divide and assign.
	 * @param x a valye in double precision.
	 */
	@PhysicalQuantity@ & operator /= (const double x);

	/**
	 * Addition operator.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	@PhysicalQuantity@ operator + (const @PhysicalQuantity@ & x) const;

	/**
	 * Substraction operator.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	@PhysicalQuantity@ operator - (const @PhysicalQuantity@ & x) const;

	/**
	 * Multiplication operator.
	 * @param x a value in double precision.
	 */
	@PhysicalQuantity@ operator * (const double x) const;

	/**
	 * Division operator.
	 * @param d a value in double precision.
	 */
	@PhysicalQuantity@ operator / (const double x) const;

	/**
	 * Comparison operator. Less-than.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool operator < (const @PhysicalQuantity@ & x) const;

	/**
	 * Comparison operator. Greater-than.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool operator > (const @PhysicalQuantity@ & x) const;

	/**
	 * Comparison operator. Less-than or equal.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */	
	bool operator <= (const @PhysicalQuantity@ & x) const;

	/**
	 * Comparison operator. Greater-than or equal.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool operator >= (const @PhysicalQuantity@ & x) const;

	/**
	 * Comparision operator. Equal-to.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool operator == (const @PhysicalQuantity@ & x) const;

	/** 
	 * Comparison method. Equality.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool equals(const @PhysicalQuantity@ & x) const;

	/**
	 * Comparison operator. Not-equal.
	 * @param x a const reference to a @PhysicalQuantity@.
	 */
	bool operator != (const @PhysicalQuantity@ & x) const;

	/**
	 * Comparison method. Test nullity.
	 * @return a bool.
	 */
	bool isZero() const;

	/**
	 * Unary operator. Opposite.
	 */
	@PhysicalQuantity@ operator - () const;

	/**
	 * Unary operator. Unary plus.
	 */
	@PhysicalQuantity@ operator + () const;

	/**
	 * Converts into a string.
	 * @return a string containing the representation of a the value in double precision.
	 */
	string toString() const;

	/** 
	 * Idem toString.
	 */
	string toStringI() const;

	/**
	 * Conversion operator.
	 * Converts into a string.
	 */
	operator string () const;

	/**
	 * Return the double precision value of the @PhysicalQuantity@.
	 * @return double
	 */
	double get() const;

#ifndef WITHOUT_ACS
	/**
	 * Return the IDL@PhysicalQuantity@ representation of the @PhysicalQuantity@.
	 * @return IDL@PhysicalQuantity@ 
	 */
	IDL@PhysicalQuantity@ toIDL@PhysicalQuantity@() const;
#endif
	/**
	 * Returns the abbreviated name of the unit implicitely associated to any @PhysicalQuantity@.
	 * @return string
	 */
	static string unit();

private:
	double value;

};

// @PhysicalQuantity@ constructors
inline @PhysicalQuantity@::@PhysicalQuantity@() : value(0.0) {
}

inline @PhysicalQuantity@::@PhysicalQuantity@(const @PhysicalQuantity@ &t) : value(t.value) {
}

#ifndef WITHOUT_ACS
inline @PhysicalQuantity@::@PhysicalQuantity@(const IDL@PhysicalQuantity@ &l) : value(l.value) {
}
#endif

inline @PhysicalQuantity@::@PhysicalQuantity@(const string &s) : value(fromString(s)) {
}

inline @PhysicalQuantity@::@PhysicalQuantity@(double v) : value(v) {
}

// @PhysicalQuantity@ destructor
inline @PhysicalQuantity@::~@PhysicalQuantity@() { }

// assignment operator
inline @PhysicalQuantity@ & @PhysicalQuantity@::operator = ( const @PhysicalQuantity@ &t ) {
	value = t.value;
	return *this;
}

// assignment operator
inline @PhysicalQuantity@ & @PhysicalQuantity@::operator = ( const double v ) {
	value = v;
	return *this;
}

// assignment with arithmetic operators
inline @PhysicalQuantity@ & @PhysicalQuantity@::operator += ( const @PhysicalQuantity@ & t) {
	value += t.value;
	return *this;
}

inline @PhysicalQuantity@ & @PhysicalQuantity@::operator -= ( const @PhysicalQuantity@ & t) {
	value -= t.value;
	return *this;
}

inline @PhysicalQuantity@ & @PhysicalQuantity@::operator *= ( const double n) {
	value *= n;
	return *this;
}

inline @PhysicalQuantity@ & @PhysicalQuantity@::operator /= ( const double n) {
	value /= n;
	return *this;
}

// arithmetic functions
inline @PhysicalQuantity@ @PhysicalQuantity@::operator + ( const @PhysicalQuantity@ &t2 ) const {
	@PhysicalQuantity@ tmp;
	tmp.value = value + t2.value;
	return tmp;
}

inline @PhysicalQuantity@ @PhysicalQuantity@::operator - ( const @PhysicalQuantity@ &t2 ) const {
	@PhysicalQuantity@ tmp;
	tmp.value = value - t2.value;
	return tmp;
}
inline @PhysicalQuantity@ @PhysicalQuantity@::operator * ( const double n) const {
	@PhysicalQuantity@ tmp;
	tmp.value = value * n;
	return tmp;
}

inline @PhysicalQuantity@ @PhysicalQuantity@::operator / ( const double n) const {
	@PhysicalQuantity@ tmp;
	tmp.value = value / n;
	return tmp;
}

// comparison operators
inline bool @PhysicalQuantity@::operator < (const @PhysicalQuantity@ & x) const {
	return (value < x.value);
}

inline bool @PhysicalQuantity@::operator > (const @PhysicalQuantity@ & x) const {
	return (value > x.value);
}

inline bool @PhysicalQuantity@::operator <= (const @PhysicalQuantity@ & x) const {
	return (value <= x.value);
}

inline bool @PhysicalQuantity@::operator >= (const @PhysicalQuantity@ & x) const {
	return (value >= x.value);
}

inline bool @PhysicalQuantity@::equals(const @PhysicalQuantity@ & x) const {
	return (value == x.value);
}


inline bool @PhysicalQuantity@::operator == (const @PhysicalQuantity@ & x) const {
	return (value == x.value);
}

inline bool @PhysicalQuantity@::operator != (const @PhysicalQuantity@ & x) const {
	return (value != x.value);
}

// unary - and + operators
inline @PhysicalQuantity@ @PhysicalQuantity@::operator - () const {
	@PhysicalQuantity@ tmp;
        tmp.value = -value;
	return tmp;
}

inline @PhysicalQuantity@ @PhysicalQuantity@::operator + () const {
	@PhysicalQuantity@ tmp;
    tmp.value = value;
	return tmp;
}

// Conversion functions
inline @PhysicalQuantity@::operator string () const {
	return toString();
}

inline string @PhysicalQuantity@::toString() const {
	return toString(value);
}

inline string @PhysicalQuantity@::toStringI() const {
	return toString(value);
}

inline double @PhysicalQuantity@::get() const {
	return value;
}

#ifndef WITHOUT_ACS
inline IDL@PhysicalQuantity@ @PhysicalQuantity@::toIDL@PhysicalQuantity@() const {
	IDL@PhysicalQuantity@ tmp;
	tmp.value = value;
	return tmp;
}
#endif

// Friend functions

inline @PhysicalQuantity@ operator * ( double n, const @PhysicalQuantity@ &x) {
	@PhysicalQuantity@ tmp;
	tmp.value = x.value * n;
	return tmp;
}

inline ostream & operator << ( ostream &o, const @PhysicalQuantity@ &x ) {
	o << x.value;
	return o;
}

inline istream & operator >> ( istream &i, @PhysicalQuantity@ &x ) {
	i >> x.value;
	return i;
}

inline string @PhysicalQuantity@::unit() {
	return string ("@PhysicalQuantityUnit@");
}

} // End namespace asdm

#endif /* @PhysicalQuantity@_CLASS */
