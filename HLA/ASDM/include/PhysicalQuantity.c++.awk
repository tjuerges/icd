#! /usr/bin/awk -f
#
# This awk script is used to generate the c++ include files
# containing the definitions of the physical quantities used
# in the ASDM c++ implementation. 
#
# It is meant to be used in collaboration with a template file 'PhysicalQuantity.tpl'
# and a text file 'PhysicalQuantity.txt' in the following command :
#
#  gawk -f PhysicalQuantity.c++.awk "x=init" PhysicalQuantity.txt "x=process" PhysicalQuantity.tpl
#
#

#
# Function definitions.
#
function ltrim(s) {
    sub(/^ */, "", s);
    return s
}

function rtrim(s) {
    sub(/ *$/, "", s);
    return s
}

function trim(s) {
    return ltrim(rtrim(s));
}

function arraySize(array) {
    size_ = 0;
    for (item in array)
	size_++;
    return size_;
}

#
# Preamble.
# 
BEGIN {
    RS = ""; FS="\n";
    ir = 0;
}

{
#
# The init pass.
# Collect all the information in each block.
#
    if ( x == "init") {
	name[ir] = $1; print name[ir];
	def[ir]  = $2;
	unit[ir] = $3;
	ofilename = name[ir] ".h";
	print "//" ofilename " generated on '" strftime() "'. Edit at your own risk." > ofilename;
	ir++;
    }
    
#
# The process pass.
# Generate the include file for each physical quantity. 
#
    if ( x == "process") {
	for (iname = 0; iname < arraySize(name); iname++) {
	    ofilename = name[iname] ".h";
	    block = $0;
	    gsub("@PhysicalQuantity@", name[iname], block);
	    gsub("@PhysicalQuantityDefinition@", def[iname], block);
	    gsub("@PhysicalQuantityUnit@", unit[iname], block);
	    print block >> ofilename;
	}
    }
}
