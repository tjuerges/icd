#if     !defined(_SDMBDO1_H)

#include <iostream>
#include <vector>

#include <ASDMEntities.h>
#include <xercesc/dom/DOM.hpp>

#include <Tag.h>
#include <ArrayTime.h>
#include <ArrayTimeInterval.h>

#include <IllegalAccessException.h>

using namespace asdm;

#include "BaselinesSet.h"
/*
#include "ASDMBinary.h"
*/
#include "ASDMBinaries.h"
#include "MSData.h"
#include "VMSData.h"

class SdmBdo1
{
 public:

  SdmBdo1( ASDM* datasetPtr, MainRow* mainRowPtr, 
	   string projectExecBlockDir);
  
  SdmBdo1( ASDM* datasetPtr, ConfigDescriptionRow* cdPtr, 
	   vector<vector<int> > vv_bitSize, 
	   vector<vector<Interval> > vv_exposure,
	   string projectExecBlockDir);

  ~SdmBdo1();


  void build( ASDM* datasetPtr, ConfigDescriptionRow* cdPtr, 
	      vector<vector<int> > vv_bitSize, 
	      vector<vector<Interval> > vv_exposure,
	      string projectExecBlockDir);

  int                       update( MainRow* mainRowPtr);


  BaselinesSet*             getBaselinesSet(){return baselinesSet_;}
  unsigned long             getAutoCorrContainerSize(){ return autoCorrContainerSize_;}
  unsigned long             getCrossCorrContainerSize(){ return crossCorrContainerSize_;}
  ASDMBinaryBlock*          getASDMBinaryBlock(){return blockPtr_;}


  ConfigDescriptionRow*     getConfigDescriptionRow();
  unsigned long             dataCellsize();
  char*                     getAutoCorrContainer();
  char*                     getCrossCorrContainer();

  double                    getFillingFactor();
  int                       numTree(){return v_treeHierarchy_.size();}

  int                       numBitSize(){return vv_bitSize_.size();}
  int                       numExposure(){return vv_exposure_.size();}

  vector<vector<int> >      getvvBitSize();
  vector<vector<Interval> > getvvExposure();

  void                      fillActualTime(ArrayTime time);
  void                      fillActualDuration(Interval duration);
  void                      fillActualDuration(vector<vector<Interval> > vv_exposure);
  void                      fillBaselineFlag();
  void                      fillDummyAutoData();   // for testing using a pattern
  void                      fillDummyCrossData();  // for testing using a pattern

  int                       saveTreeHierarchy(ConfigDescriptionRow* cdPtr, 
					      vector<vector<int> > vv_bitSize, 
					      vector<vector<Interval> > vv_exposure_);
  void                      exportDataObject(string uid);
  int                       importDataObject(ArrayTime time, Tag fieldId);
  int                       importDataObject(MainRow* mainRowPtr);
  int                       importDataObject();

  MSData*        getData( int na1, int na2, int nfe,
			  int ndd, int nbin,
			  int apcCode,
			  vector< vector<float> > vv_scaleFactor);

  MSData*        getData( int na, int nfe, 
			  int ndd, int nbin,
			  vector< vector<float> > vv_scaleFactor);

  MSData*        getData( Tag antId1, int feedId1, 
			  Tag antId2, int feedId2,
			  Tag dataDescId, int apcCode, int  binNum,
			  vector< vector<float> > vv_scaleFactor);

  MSData*        getData( Tag antId, int feedId, 
			  Tag dataDescId, int  binNum,
			  vector< vector<float> > vv_scaleFactor);

  vector<MSData*> getData( int apcCode, int corrMode, 
			   vector< vector<float> > vv_scaleFactor);

  VMSData*        getDataCols( int apcCode, int corrMode,
			       vector< vector<float> > vv_scaleFactor);

 private:
  static ASDM*              datasetPtr_;
  static MainRow*           mrPtr_;
  static string             projectExecBlockDir_;    // export or import dataset directory
  static SdmBdo1*           sdmBdo1Ptr_;
  static vector<SdmBdo1*>   v_treeHierarchy_;


  static vector<vector<Length> >   vv_uvw_;
  static vector<Tag>               v_stateId_;

  ConfigDescriptionRow*     cdPtr_;
  vector<vector<int> >      vv_bitSize_;             // numBaseband vectors of Ncm items
  vector<vector<Interval> > vv_exposure_;            // numAnt vectors of numBaseband items

  ASDMBinaryBlock*          blockPtr_;               // MUST be at the end of the list of attribute AND as a pointer
  BaselinesSet*             baselinesSet_;

  static MSData*            msDataPtr_;              // one MS-MAIN row given
  static vector<MSData*>    v_msDataPtr_;            //
  VMSData*                  vmsDataPtr_;

  ArrayTime                 time_;

  long long*                arrayTime_;
  long long*                interval_;
  unsigned int*             flag_;
  unsigned long             autoCorrContainerSize_;  // size (bytes) for the container of auto-correlations
  unsigned long             crossCorrContainerSize_; // size (bytes) for the container of cross-correlations
  //unsigned long             dataCellSize_;           // size (number of bytes) for the Binary Data Cell
  unsigned long             ndone_;                  // number of bytes currenly written in the data-cell
  int                       iostat_;
  int                       MSDataSize_;
  char*                     autoCorrContainer_;
  char*                     crossCorrContainer_;

  int                       importDataObject(string uid);
  int                       importDataObject(EntityId uid);
  int                       importDataObject(EntityRef uid);


};

#define _SDMBDO1_H 
#endif
