<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="/home/caillat/docbook-xsl-1.69.1/html/chunk.xsl"/>
<!-- xsl:include href="mytitlepages.xsl"/ -->
	<xsl:param name="generate.toc">appendix  toc,title
article/appendix  nop
article   toc,title
book      title,toc,figure,example,equation
part      toc,title
preface   toc,title
qandadiv  toc
qandaset  toc
reference toc,title
sect1     toc
sect2     toc
sect3     toc
sect4     toc
sect5     toc
section   toc
set       toc,title</xsl:param>
</xsl:stylesheet>
