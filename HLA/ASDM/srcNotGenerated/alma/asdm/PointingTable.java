/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File PointingTable.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;
import alma.xmlstore.ArchiveConnectionPackage.*;
import alma.xmlstore.IdentifierPackage.*; // import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
// import alma.xmlstore.IdentifierPackage.NotAvailable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collection;
import java.io.IOException;

/**
 * The PointingTable class is an Alma table. <TABLE BORDER="1"> <CAPTION>
 * Attributes of Pointing </CAPTION>
 * <TR BGCOLOR="#AAAAAA">
 * <TH> Name </TH>
 * <TH> Type </TH>
 * <TH> Comment </TH>
 * </TR>
 * 
 * <TR>
 * <TH BGCOLOR="#CCCCCC" colspan="3" align="center"> Key </TD>
 * </TR>
 * 
 * 
 * <TR>
 * <TD> antennaId </TD>
 * <TD> Tag </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * 
 * 
 * <TR>
 * <TD> timeInterval </TD>
 * <TD> ArrayTimeInterval </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * 
 * 
 * 
 * <TR>
 * <TH BGCOLOR="#CCCCCC"  colspan="3" valign="center"> Value <br>
 * (Mandarory) </TH>
 * </TR>
 * 
 * <TR>
 * <TD> pointingModelId </TD>
 * <TD> int </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * <TR>
 * <TD> numPoly </TD>
 * <TD> int </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * <TR>
 * <TD> timeOrigin </TD>
 * <TD> ArrayTime </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * <TR>
 * <TD> pointingDirection </TD>
 * <TD> Angle[][] </TD>
 * <TD> numPoly+1, 2 </TD>
 * </TR>
 * 
 * <TR>
 * <TD> target </TD>
 * <TD> Angle[][] </TD>
 * <TD> numPoly+1, 2 </TD>
 * </TR>
 * 
 * <TR>
 * <TD> offset </TD>
 * <TD> Angle[][] </TD>
 * <TD> numPoly+1, 2 </TD>
 * </TR>
 * 
 * <TR>
 * <TD> encoder </TD>
 * <TD> Angle[] </TD>
 * <TD> 2 </TD>
 * </TR>
 * 
 * <TR>
 * <TD> pointingTracking </TD>
 * <TD> boolean </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * 
 * 
 * <TR>
 * <TH BGCOLOR="#CCCCCC"  colspan="3" valign="center"> Value <br>
 * (Optional) </TH>
 * </TR>
 * 
 * <TR>
 * <TD> name </TD>
 * <TD> String </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * <TR>
 * <TD> sourceOffset </TD>
 * <TD> Angle[][] </TD>
 * <TD> numPoly+1, 2 </TD>
 * </TR>
 * 
 * <TR>
 * <TD> phaseTracking </TD>
 * <TD> boolean </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * <TR>
 * <TD> overTheTop </TD>
 * <TD> boolean </TD>
 * <TD> &nbsp; </TD>
 * </TR>
 * 
 * 
 * </TABLE>
 */
public class PointingTable extends ASDMTable implements Representable {

	/**
	 * The name of this table.
	 */
	private static String tableName = "Pointing";

	/**
	 * The list of field names that make up key key.
	 */
	private static String[] key = { "antennaId", "timeInterval" };

	/**
	 * Return the list of field names that make up key key as an array of
	 * strings.
	 */
	public static String[] getKeyName() {
		return key;
	}

	private ASDM container;

	// Archive binary
	private boolean archiveAsBin = true;

	// File binary
	private boolean fileAsBin = true;

	/**
	 * Returns a string built by concatenating the ascii representation of the
	 * parameters values suffixed with a "_" character.
	 */
	String Key(Tag antennaId) {
		StringBuffer result = new StringBuffer();

		result.append(antennaId.toString()).append("_");

		return result.toString();
	}

	// A data structure to store the PointingRow s.

	// In all cases we maintain a private ArrayList of PointingRow s.
	private ArrayList<PointingRow> privateRows;

	/**
	 * The context's key is a string resulting from a call to the method Key and
	 * the value must be a Vector of PointingRow.
	 */
	private Hashtable<String, Vector<PointingRow>> context;

	private Entity entity;

	/**
	 * Insert a PointingRow in a vector of PointingRow so that it's ordered by
	 * ascending start time.
	 * 
	 * @param PointingRow .
	 *            The row to be inserted.
	 * @param Vector
	 *            row. The vector where to insert x.
	 * 
	 */
	private PointingRow insertByStartTime(PointingRow x, Vector<PointingRow> row)
			throws DuplicateKey {

		ArrayTime start = x.timeInterval.getStart();

		// Is the row vector empty ?
		if (row.size() == 0) {
			row.add(x);
			privateRows.add(x);
			x.isAdded();
			return x;
		}

		// Optimization for the case of insertion by ascending time.
		PointingRow last = row.get(row.size() - 1);

		if (start.get() > last.timeInterval.getStart().get()) {
			//
			// Modify the duration of last if and only if the start time of x
			// is located strictly before the end time of last.
			//    
			if (start.get() < (last.timeInterval.getStart().get() + last.timeInterval
					.getDuration().get()))
				last.timeInterval.setDuration(start.get()
						- last.timeInterval.getStart().get());
			row.add(x);
			privateRows.add(x);
			x.isAdded();
			return x;
		}

		// Optimization for the case of insertion by descending time.
		PointingRow first = row.get(0);

		if (start.get() < first.timeInterval.getStart().get()) {
			//
			// Modify the duration of x if and only if the start time of first
			// is located strictly before the end time of x.
			//
			if (first.timeInterval.getStart().get() < (start.get() + x.timeInterval
					.getDuration().get()))
				x.timeInterval.setDuration(first.timeInterval.getStart().get()
						- start.get());
			row.insertElementAt(x, 0);
			privateRows.add(x);
			x.isAdded();
			return x;
		}

		// Case where x has to be inserted inside row; let's use a dichotomy
		// method to find the insertion index.

		int k0 = 0;
		int k1 = row.size() - 1;

		while (k0 != (k1 - 1)) {
			if (start.get() == (row.get(k0)).timeInterval.getStart().get()) {
				if ((row.get(k0)).equalByRequiredValue(x))
					return (row.get(k0));
				else
					throw new DuplicateKey("DuplicateKey exception in ",
							"PointingTable");
			} else if (start.get() == (row.get(k1)).timeInterval.getStart()
					.get()) {
				if ((row.get(k1)).equalByRequiredValue(x))
					return (row.get(k1));
				else
					throw new DuplicateKey("DuplicateKey exception in ",
							"PointingTable");
			} else {
				if (start.get() <= (row.get((k0 + k1) / 2)).timeInterval
						.getStart().get())
					k1 = (k0 + k1) / 2;
				else
					k0 = (k0 + k1) / 2;
			}
		}
		(row.get(k0)).timeInterval.setDuration(start.get()
				- (row.get(k0)).timeInterval.getStart().get());
		x.timeInterval.setDuration((row.get(k0 + 1)).timeInterval.getStart()
				.get()
				- start.get());
		row.insertElementAt(x, k1);
		privateRows.add(x);
		x.isAdded();
		return x;
	}

	private final String[] attributesNames = {

	"antennaId", "timeInterval"

	,

	"pointingModelId", "numPoly", "timeOrigin", "pointingDirection", "target",
			"offset", "encoder", "pointingTracking"

			,

			"name", "sourceOffset", "phaseTracking", "overTheTop"

	};

	public String[] getAttributesNames() {
		return attributesNames;
	}

	private final String[] keyAttributesNames = {

	"antennaId", "timeInterval"

	};

	boolean inKey(String s) {
		for (int i = 0; i < keyAttributesNames.length; i++)
			if (keyAttributesNames[i].equals(s))
				return true;
		return false;
	}

	// public String[] getKeyAttributesNames() { return keyAttributesNames; }

	private final String[] requiredValueAttributesNames = {

	"pointingModelId", "numPoly", "timeOrigin", "pointingDirection", "target",
			"offset", "encoder", "pointingTracking"

	};

	// public String[] getRequiredValueAttributesNames() { return
	// requiredValueAttributesNames;}

	private final String[] optionalValueAttributesNames = {

	"name", "sourceOffset", "phaseTracking", "overTheTop"

	};

	// public String[] getOptionalValueAttributesNames() { return
	// optionalValueAttributesNames; }

	/**
	 * Create a PointingTable.
	 * <p>
	 * This constructor has package access because only the container can create
	 * tables. All tables must know the container to which they belong.
	 * 
	 * @param container
	 *            The container to which this table belongs.
	 */
	PointingTable(ASDM container) {
		this.container = container;

		privateRows = new ArrayList<PointingRow>();

		context = new Hashtable<String, Vector<PointingRow>>();

		entity = new Entity();
		entity.setEntityId(new EntityId("uid://X0/X0/X0"));
		entity.setEntityIdEncrypted("na");
		entity.setEntityTypeName("PointingTable");
		entity.setEntityVersion("1");
		entity.setInstanceVersion("1");

	}

	/**
	 * Return the container to which this table belongs.
	 * 
	 * @return a ASDM.
	 */
	public ASDM getContainer() {
		return container;
	}

	/**
	 * Return the number of rows in the table.
	 */

	public int size() {
		int result = 0;
		Collection v = context.values();
		Iterator iter = v.iterator();
		while (iter.hasNext()) {
			result += ((Vector) iter.next()).size();
		}
		return result;
	}

	/**
	 * Return the name of this table.
	 */
	public String getName() {
		return tableName;
	}

	/**
	 * Returns "PointingTable" followed by the current size of the table between
	 * parenthesis. Example : SpectralWindowTable(12)
	 * 
	 * @return a String.
	 */
	public String toString() {
		return "PointingTable(" + size() + ")";
	}

	//
	// ====> Row creation.
	//

	/**
	 * Create a new row with default values .
	 * 
	 * @return a PointingRow
	 */
	public PointingRow newRow() {
		return new PointingRow(this);
	}

	//
	// Append a row to its table.
	//

	/**
	 * Add a row.
	 * 
	 * @param x
	 *            a pointer to the PointingRow to be added.
	 * 
	 * @return a PointingRow. If the table contains a PointingRow whose
	 *         attributes (key and mandatory values) are equal to x ones then
	 *         returns a pointer on that PointingRow, otherwise returns x.
	 * 
	 * @throw DuplicateKey { thrown when the table contains a PointingRow with a
	 *        key equal to the x one but having and a value section different
	 *        from x one }
	 * 
	 * 
	 * @note The row is inserted in the table in such a way that all the rows
	 *       having the same value of ( antennaId ) are stored by ascending
	 *       time.
	 * @see method getByContext.
	 * 
	 */
	public PointingRow add(PointingRow x) throws DuplicateKey {
		/*
		 * Is there already a context for this combination of not temporal
		 * attributes ?
		 */
		String k = Key(x.getAntennaId());

		Vector<PointingRow> v = null;

		if ((v = (Vector<PointingRow>) context.get(k)) == null) {
			// There is not yet a context ...
			// Create and initialize an entry in the context map for this
			// combination....
			v = new Vector<PointingRow>();
			context.put(k, v);
		}

		PointingRow result = null;
		try {
			result = insertByStartTime(x, v);
		} catch (DuplicateKey e) {
			throw e; // Simply rethrow it
		}
		return result;
	}

	/**
	 * Create a new row initialized to the specified values.
	 * (the autoincrementable attribute if any is not in the parameter list)
	 * @return the new initialized row.
	
	 * @param antennaId. 
	
	 * @param timeInterval. 
	
	 * @param pointingModelId. 
	
	 * @param numPoly. 
	
	 * @param timeOrigin. 
	
	 * @param pointingDirection. 
	
	 * @param target. 
	
	 * @param offset. 
	
	 * @param encoder. 
	
	 * @param pointingTracking. 
	
	 */
	public PointingRow newRow(Tag antennaId, ArrayTimeInterval timeInterval,
			int pointingModelId, int numPoly, ArrayTime timeOrigin,
			Angle[][] pointingDirection, Angle[][] target, Angle[][] offset,
			Angle[] encoder, boolean pointingTracking) {
		PointingRow row = new PointingRow(this);
		try {

			row.setAntennaId(antennaId);

			row.setTimeInterval(timeInterval);

			row.setPointingModelId(pointingModelId);

			row.setNumPoly(numPoly);

			row.setTimeOrigin(timeOrigin);

			row.setPointingDirection(pointingDirection);

			row.setTarget(target);

			row.setOffset(offset);

			row.setEncoder(encoder);

			row.setPointingTracking(pointingTracking);

		} catch (Exception e) {
			// Never happens
		}

		return row;
	}

	/**
	 * Create a new row using a copy constructor mechanism.
	 * 
	 * The method creates a new PointingRow owned by this. Each attribute of the
	 * created row is a (deep) copy of the corresponding attribute of row. The
	 * method does not add the created row to this, its simply parents it to
	 * this, a call to the add method has to be done in order to get the row
	 * added (very likely after having modified some of its attributes). If row
	 * is null then the method returns a new PointingRow with default values for
	 * its attributes.
	 * 
	 * @param row
	 *            the row which is to be copied.
	 */
	public PointingRow newRow(PointingRow row) {
		return new PointingRow(this, row);
	}

	// ====> Append a row to its table.

	// 
	// A private method to append a row to its table, used by input conversion
	// methods.
	//

	private PointingRow checkAndAdd(PointingRow x) throws DuplicateKey {
		String k = Key(x.getAntennaId());

		Vector<PointingRow> v = null;
		Object o = context.get(k);
		if (o == null) {
			v = new Vector<PointingRow>();
			context.put(k, v);
		}
		return insertByStartTime(x, context.get(k));
	}

	//
	// ====> Methods returning rows.
	//	

	/**
	 * Get all rows.
	 * 
	 * @return Alls rows as an array of PointingRow
	 */
	public PointingRow[] get() {
		PointingRow[] result = new PointingRow[privateRows.size()];
		result = (PointingRow[]) privateRows.toArray(result);
		return result;

		/*
		 * PointingRow[] result = new PointingRow[size()]; Iterator iter =
		 * context.values().iterator(); int i = 0; while (iter.hasNext()) {
		 * Vector v = (Vector) iter.next(); for (int k = 0; k < v.size(); k++)
		 * result[i++] = v.get(k); } return result;
		 */
	}

	/**
	 * Returns all the rows sorted by ascending startTime for a given context.
	 * The context is defined by a value of ( antennaId ).
	 * 
	 * @return an array of PointingRow. A null returned value means that the
	 *         table contains no PointingRow for the given ( antennaId ).
	 */
	public PointingRow[] getByContext(Tag antennaId) {

		String k = Key(antennaId);
		Vector<PointingRow> v = null;

		PointingRow[] result = null;
		if ((v = context.get(k)) == null)
			return null;
		else {
			result = new PointingRow[v.size()];
			return (PointingRow[]) v.toArray(result);
		}
	}

	/**
	 * Returns a PointingRow given a key.
	 * 
	 * @return a pointer to the row having the key whose values are passed as
	 *         parameters, or 0 if no row exists for that key.
	 * 
	 */
	public PointingRow getRowByKey(Tag antennaId, ArrayTimeInterval timeInterval) {
		String keystr = Key(antennaId);

		Vector<PointingRow> row = null;
		if ((row = context.get(keystr)) == null)
			return null;

		// Is the vector empty...impossible in principle !
		if (row.size() == 0)
			return null;

		// Only one element in the vector
		if (row.size() == 1) {
			PointingRow r = row.get(0);
			if (r.getTimeInterval().contains(timeInterval.getStart()))
				return r;
			else
				return null;
		}
		// Optimizations
		PointingRow last = row.get(row.size() - 1);
		if (timeInterval.getStart().get() >= (last.getTimeInterval().getStart()
				.get() + last.getTimeInterval().getDuration().get()))
			return null;
		PointingRow first = row.get(0);
		if (timeInterval.getStart().get() < first.getTimeInterval().getStart()
				.get())
			return null;

		// More than one row
		// let's use dichotomy method for the general case..
		int k0 = 0;
		int k1 = row.size() - 1;
		PointingRow r = null;
		while (k0 != k1) {
			
			// Is the start time contained in the time interval of row #k0 ?
			r = row.get(k0);
			if (r.getTimeInterval().contains(timeInterval.getStart()))
				return r;

			// Is the start time contained in the time interval of row #k1 ?
			r = row.get(k1);
			if (r.getTimeInterval().contains(timeInterval.getStart()))
				return r;

			// Are the rows #k0 and #k1 consecutive
			// Then we know for sure that there is no row containing the start of timeInterval.
			if (k1 == (k0 + 1) ) return null;
			
			r = row.get((k0 + k1) / 2);
			if (timeInterval.getStart().get() <= r.getTimeInterval().getStart()
					.get())
				k1 = (k0 + k1) / 2;
			else
				k0 = (k0 + k1) / 2;
		}
		return null;
	}

	public ASDMRow[] getRows() {
		return (ASDMRow[]) get();
	}

	// 
	// ====> Conversion Methods
	//
	/**
	 * Convert this table into a PointingTableIDL CORBA structure.
	 * 
	 * @return a PointingTableIDL
	 */
	public PointingTableIDL toIDL() {
		PointingTableIDL x = new PointingTableIDL();
		PointingRow[] v = get();
		x.row = new PointingRowIDL[v.length];
		for (int i = 0; i < v.length; ++i) {
			x.row[i] = v[i].toIDL();
		}
		return x;
	}

	/**
	 * Populate this table from the content of a PointingTableIDL Corba
	 * structure.
	 * 
	 * @throws DuplicateKey
	 *             Thrown if the method tries to add a row having a key that is
	 *             already in the table.
	 * @throws ConversionException
	 */
	public void fromIDL(PointingTableIDL x) throws DuplicateKey,
			ConversionException {
		PointingRow tmp = null;
		for (int i = 0; i < x.row.length; ++i) {
			tmp = newRow();
			tmp.setFromIDL(x.row[i]);
			// checkAndAdd(tmp);
			add(tmp);
		}
	}

	/**
	 * To be implemented
	 */
	public byte[] toFITS() throws ConversionException {
		// TODO
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromFITS(byte[] fits) throws ConversionException {
		// TODO
	}

	/**
	 * To be implemented
	 */
	public String toVOTable() throws ConversionException {
		// TODO
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromVOTable(String vo) throws ConversionException {
		// TODO
	}

	/**
	 * Translate this table to an XML representation conform to the schema
	 * defined for Pointing (PointingTable.xsd).
	 * 
	 * @returns a string containing the XML representation.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> ");
		buf
				.append("<?xml-stylesheet type=\"text/xsl\" href=\"../asdm2html/table2html.xsl\"?> ");
		buf
				.append("<PointingTable xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../idl/PointingTable.xsd\"> ");
		// buf.append("<PointingTable>");
		buf.append(entity.toXML());
		String s = container.getEntity().toXML();
		// Change the "Entity" tag to "ContainerEntity".
		buf.append("<Container" + s.substring(1));

		for (PointingRow row : privateRows) {
			buf.append(row.toXML());
			buf.append(" ");
		}
		buf.append("</PointingTable>");
		return buf.toString();
	}

	/**
	 * Populate this table from the content of a XML document that is required
	 * to be conform to the XML schema defined for a Pointing
	 * (PointingTable.xsd).
	 * 
	 * @throws ConversionException
	 */
	public void fromXML(String xmlDoc) throws ConversionException {
		Parser xml = new Parser(xmlDoc);
		if (!xml.isStr("<PointingTable"))
			error();
		String s = xml.getElement("<Entity", "/>");
		if (s == null)
			error();
		Entity e = new Entity();
		e.setFromXML(s);
		if (!e.getEntityTypeName().equals("PointingTable"))
			error();
		setEntity(e);
		// Skip the container's entity; but, it has to be there.
		s = xml.getElement("<ContainerEntity", "/>");
		if (s == null)
			error();

		// Get each row in the table.
		s = xml.getElementContent("<row>", "</row>");
		PointingRow row = null;
		while (s != null) {
			row = newRow();
			row.setFromXML(s);
			try {
				checkAndAdd(row);
			} catch (DuplicateKey e1) {
				throw new ConversionException(e1.toString(), "PointingTable");
			}
			s = xml.getElementContent("<row>", "</row>");
		}
		if (!xml.isStr("</PointingTable>"))
			error();
	}

	/**
	 * 
	 * @throws ConversionException
	 */

	private void error() throws ConversionException {
		throw new ConversionException("Invalid xml document", "Pointing");
	}

	/**
	 * Serialize this into a stream of bytes and encapsulates that stream into a
	 * MIME message.
	 * 
	 * @returns an array of bytes containing the MIME message.
	 * 
	 */
	public byte[] toMIME() throws ConversionException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);

		String UID = this.getEntity().getEntityId().toString();
		String execBlockUID = this.getContainer().getEntity().getEntityId()
				.toString();
		try {
			// The MIME header.
			dos.writeBytes("MIME-Version: 1.0");
			dos.writeBytes("\n");
			dos
					.writeBytes("Content-Type: Multipart/Related; boundary='MIME_boundary'; type='text/xml'; start= '<header.xml>'");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Description: Correlator");
			dos.writeBytes("\n");
			dos.writeBytes("alma-uid:" + UID);
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The MIME XML part header.
			dos.writeBytes("--MIME_boundary");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Type: text/xml; charset='ISO-8859-1'");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Transfer-Encoding: 8bit");
			dos.writeBytes("\n");
			dos.writeBytes("Content-ID: <header.xml>");
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The MIME XML part content.
			dos.writeBytes("<?xml version='1.0'  encoding='ISO-8859-1'?>");
			dos.writeBytes("\n");
			dos
					.writeBytes("<ASDMBinaryTable  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'  xsi:noNamespaceSchemaLocation='ASDMBinaryTable.xsd' ID='None'  version='1.0'>\n");
			dos.writeBytes("<ExecBlockUID>\n");
			dos.writeBytes(execBlockUID + "\n");
			dos.writeBytes("</ExecBlockUID>\n");
			dos.writeBytes("</ASDMBinaryTable>\n");

			// The MIME binary part header
			dos.writeBytes("--MIME_boundary");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Type: binary/octet-stream");
			dos.writeBytes("\n");
			dos.writeBytes("Content-ID: <content.bin>");
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The MIME binary part content
			entity.toBin(dos);
			container.getEntity().toBin(dos);
			dos.writeInt(size());
			for (PointingRow row : privateRows)
				row.toBin(dos);

			// The closing MIME boundary
			dos.writeBytes("\n--MIME_boundary--");
			dos.writeBytes("\n");
		} catch (IOException e) {
			throw new ConversionException(
					"Error while writing binary data , the message was "
							+ e.getMessage(), "Pointing");
		}
		return bos.toByteArray();
	}

	static private boolean binaryPartFound(DataInputStream dis, String s,
			int pos) throws IOException {
		int posl = pos;
		int count = 0;
		dis.mark(1000000);
		try {
			while (dis.readByte() != s.charAt(posl)) {
				count++;
			}
		} catch (EOFException e) {
			return false;
		}

		if (posl == (s.length() - 1))
			return true;

		if (pos == 0) {
			posl++;
			return binaryPartFound(dis, s, posl);
		} else {
			if (count > 0) {
				dis.reset();
				return binaryPartFound(dis, s, 0);
			} else {
				posl++;
				return binaryPartFound(dis, s, posl);
			}
		}
	}

	/**
	 * Extracts the binary part of a MIME message and deserialize its content to
	 * fill this with the result of the deserialization.
	 * 
	 * @param mimeMsg
	 *            the array of bytes containing the MIME message.
	 * @throws ConversionException
	 */
	public void setFromMIME(byte[] mimeMsg) throws ConversionException {
		ByteArrayInputStream bis = new ByteArrayInputStream(mimeMsg);
		DataInputStream dis = new DataInputStream(bis);

		String terminator = "Content-Type: binary/octet-stream\nContent-ID: <content.bin>\n\n";

		entity = null;
		// Entity containerEntity = null;
		try {
			if (binaryPartFound(dis, terminator, 0) == false) {
				throw new ConversionException(
						"Failed to detect the beginning of the binary part",
						"Pointing");
			}

			entity = Entity.fromBin(dis);
			Entity containerEntity = Entity.fromBin(dis);

			int numRows = dis.readInt();
			for (int i = 0; i < numRows; i++) {
				PointingRow aRow = PointingRow.fromBin(dis, this);
				this.checkAndAdd(aRow);
			}
		} catch (IOException e) {
			throw new ConversionException(
					"Error while reading binary data , the message was "
							+ e.getMessage(), "Pointing");
		} catch (TagFormatException e) {
			throw new ConversionException(
					"Error while reading binary data , the message was "
							+ e.getMessage(), "Pointing");
		} catch (DuplicateKey e) {
			throw new ConversionException(
					"Error while writing binary data , the message was "
							+ e.getMessage(), "Pointing");
		}
	}

	/**
	 * Stores a representation (binary or XML) of this table into a file.
	 * 
	 * Depending on the boolean value of its private field fileAsBin a binary
	 * serialization of this (fileAsBin==true) will be saved in a file
	 * "Pointing.bin" or an XML representation (fileAsBin==false) will be saved
	 * in a file "Pointing.xml". The file is always written in a directory whose
	 * name is passed as a parameter.
	 * 
	 * @param directory
	 *            The name of directory where the file containing the table's
	 *            representation will be saved.
	 * @throws ConversionException
	 */
	public void toFile(String directory) throws ConversionException {
		// Check if the directory exists
		File directoryFile = new File(directory);
		if (directoryFile.exists() && !directoryFile.isDirectory())
			throw new ConversionException("Cannot write into directory "
					+ directoryFile.getAbsolutePath()
					+ ". This file already exists and is not a directory",
					"Pointing");

		// if not let's create it.
		if (!directoryFile.exists()) {
			if (!directoryFile.mkdir())
				throw new ConversionException("Could not create directory "
						+ directoryFile.getAbsolutePath(), "Pointing");
		}

		String fileName = null;
		BufferedWriter out = null;
		if (fileAsBin) {
			// write the bin serialized.
			fileName = directory + "/Pointing.bin";
		} else {
			// write the XML
			fileName = directory + "/Pointing.xml";
		}

		File file = new File(fileName);

		// Delete the file if it already exists
		// We assume here that 'file'  does not denote  a directory. The probability
		// for that is very low; a perfect code would take care of this possibility.
		if (file.exists()) {
			if (!file.delete())
				throw new ConversionException(
						"Problem while trying to delete a previous version of '"
								+ file.toString() + "'", "Pointing");
		}

		if (fileAsBin) {
			OutputStream os;
			try {
				os = new FileOutputStream(file);
				os.write(toMIME());
				os.close();
			} catch (FileNotFoundException e) {
				throw new ConversionException(
						"Problem while writing the binary representation, the message was : "
								+ e.getMessage(), "Pointing");
			} catch (IOException e) {
				throw new ConversionException(
						"Problem while writing the binary representation, the message was : "
								+ e.getMessage(), "Pointing");
			}
		} else {
			try {
				out = new BufferedWriter(new FileWriter(file));
				out.write(toXML());
				out.close();
			} catch (IOException e) {
				throw new ConversionException(
						"Problem while writing the XML representation, the message was : "
								+ e.getMessage(), "Pointing");
			}
		}
	}

	/**
	 * Reads and parses a file containing a representation of a PointingTable as
	 * those produced by the toFile method. This table is populated with the
	 * result of the parsing.
	 * 
	 * @param directory
	 *            The name of the directory containing the file te be read and
	 *            parsed.
	 * @throws ConversionException
	 *             If any error occurs while reading the files in the directory
	 *             or parsing them.
	 * 
	 */
	public void setFromFile(String directory) throws ConversionException {
		File directoryFile = new File(directory);

		if (!directoryFile.isDirectory())
			throw new ConversionException("Directory " + directory
					+ " does not exist.", "Pointing");

		String fileName = null;

		if (fileAsBin)
			fileName = directory + "/Pointing.bin";
		else
			fileName = directory + "/Pointing.xml";

		File file = new File(fileName);

		if (!file.exists())
			throw new ConversionException("File " + fileName
					+ " does not exists", "Pointing");

		if (fileAsBin) {
			byte[] bytes = null;
			try {
				InputStream is = new FileInputStream(file);
				long length = file.length();
				if (length > Integer.MAX_VALUE)
					throw new ConversionException("File " + fileName
							+ " is too large", "Pointing");

				bytes = new byte[(int) length];
				int offset = 0;
				int numRead = 0;

				while (offset < bytes.length
						&& (numRead = is.read(bytes, offset, bytes.length
								- offset)) >= 0) {
					offset += numRead;
				}

				if (offset < bytes.length) {
					throw new ConversionException(
							"Could not completely read file " + file.getName(),
							"Pointing");
				}
				is.close();
			} catch (IOException e) {
				throw new ConversionException("Error while reading "
						+ file.getName() + ". The message was "
						+ e.getMessage(), "Pointing");
			}

			setFromMIME(bytes);
		} else {
			try {
				BufferedReader in = new BufferedReader(new FileReader(file));
				StringBuffer xmlDoc = new StringBuffer();
				String line = in.readLine();
				while (line != null) {
					xmlDoc.append(line);
					line = in.readLine();
				}
				in.close();
				fromXML(xmlDoc.toString());
			} catch (IOException e) {
				throw new ConversionException(e.getMessage(), "Pointing");
			}
		}
	}

	/**
	 * Returns the table's entity.
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * Set the table's entity
	 * 
	 * @param e
	 *            An entity.
	 */
	public void setEntity(Entity e) {
		this.entity = e;
	}

	/**
	 * Store this table into the archive.
	 * 
	 * @param ar
	 *            the archiver in charge of the archiving.
	 * @returns the UID assigned to the archived table.
	 */
	String toArchive(Archiver ar) throws ConversionException, ArchiverException {
		String retUID = null;
		Entity ent = null;

		try {
			retUID = ar.getID();
			ent = this.getEntity();
			ent.setEntityId(new EntityId(retUID));
			this.setEntity(ent);

			if (archiveAsBin) {
				byte[] asBin = this.toMIME();
				ar.binStore(retUID, asBin);
			} else {
				String xmlTable = this.toXML();
				ar.store(xmlTable, "PointingTable", retUID);
			}
		} catch (ArchiveException e) {
			throw new ArchiverException(
					"Archiver exception while archiving a PointingTable. Message was "
							+ e.getMessage());
		} catch (NotAvailable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUID;
	}

	/**
	 * Retrieve this table from the archive.
	 * 
	 * @param ar
	 *            the archiver in charge of the archiving.
	 */
	public void fromArchive(Archiver ar, String UID)
			throws ConversionException, ArchiverException {
		try {
			if (archiveAsBin) {
				this.setFromMIME(ar.binRetrieve(UID));
			} else {
				this.fromXML(ar.retrieve(UID));
			}
		} catch (ArchiveException e) {
			throw new ArchiverException(
					"Archiver exception while retrieving a PointingTable with UID "
							+ UID + ". The message was " + e.getMessage());
		}
	}
}
