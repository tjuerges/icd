
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File DataDescriptionTable.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;
import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
import alma.xmlstore.IdentifierPackage.NotAvailable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collection;

/**
 * The DataDescriptionTable class is an Alma table.
 * <TABLE BORDER="1">
 * <CAPTION> Attributes of DataDescription </CAPTION>
 * <TR BGCOLOR="#AAAAAA"> <TH> Name </TH> <TH> Type </TH> <TH> Comment </TH></TR>
 
 * <TR> <TH BGCOLOR="#CCCCCC" colspan="3" align="center"> Key </TD></TR>
	
		
 * <TR>
 * <TD><I> dataDescriptionId </I></TD> 
 * <TD><U> Tag</U></TD>
 * <TD> &nbsp; </TD>
 * </TR>
 			
	


 * <TR> <TH BGCOLOR="#CCCCCC"  colspan="3" valign="center"> Value <br> (Mandarory) </TH></TR>
	
 * <TR>
 * <TD> polOrHoloId </TD> 
 * <TD> Tag </TD>
 * <TD>  &nbsp; </TD>
 * </TR>
	
 * <TR>
 * <TD> spectralWindowId </TD> 
 * <TD> Tag </TD>
 * <TD>  &nbsp; </TD>
 * </TR>
	
 * </TABLE>
 */
public class DataDescriptionTable  extends ASDMTable implements Representable{

	/**
	 * The name of this table.
	 */
	private static String tableName = "DataDescription";
	

	/**
	 * The list of field names that make up key key.
	 */
	private static String[] key = { "dataDescriptionId" };
	
	/**
	 * Return the list of field names that make up key key
	 * as an array of strings.
	 */	
	public static String[] getKeyName() {
		return key;
	}


	private ASDM container;
	
	// Archive binary
	private boolean archiveAsBin = false;
	
	// File binary
	private boolean fileAsBin = false;
	

	
	// A data structure to store the DataDescriptionRow s.
	
	// In all cases we maintain a private ArrayList of DataDescriptionRow s.
	private ArrayList privateRows;
	

		
	private ArrayList row;
	

	private Entity entity;
	

	// A Hashtable for the autoincrementation algorithm
	private Hashtable noAutoIncIds;
	void autoIncrement(String key, DataDescriptionRow x) {
		try {
			Integer N = (Integer) noAutoIncIds.get(key);
			if (N == null) {
				// There is not yet a combination of the non autoinc attributes values in the hashtable
	
				// Initialize  dataDescriptionId to Tag(0).
				x.setDataDescriptionId(new Tag(0, TagType.DataDescription));
	
				// Record it in the hashtable.
				noAutoIncIds.put(key, new Integer(0));					
		} 
		else {
				// There is already a combination of the non autoinc attributes values in the hashtable
				int n = N.intValue()+1; 
	
				// Initialize  dataDescriptionId to Tag(n).
				x.setDataDescriptionId(new Tag(n, TagType.DataDescription));
	
				// Record it in the hashtable.
				noAutoIncIds.put(key, new Integer(n));
			}				
		} catch (IllegalAccessException e) {
			// never happens.
		}		
	}





	private final String[] attributesNames = {
	
		"dataDescriptionId"
	
	
	,
	
	
		"polOrHoloId"
	,
		"spectralWindowId"
		
	};
	
	public String[] getAttributesNames() {
		return attributesNames;
	}

	private String[] attributesTypes = {
			"Tag",
			"Tag",
			"boolean"
	};
	
	public String[] getAttributesTypes() { return attributesTypes; }
	
	private final String[] keyAttributesNames = {
	
		"dataDescriptionId"
	
	};
	
	boolean inKey(String s) {
		for (int i = 0; i < keyAttributesNames.length; i++) 
			if ( keyAttributesNames[i].equals(s) ) return true;
		return false;
	}
	
	//public String[] getKeyAttributesNames() { return keyAttributesNames; }
	
	private final String[] requiredValueAttributesNames = {
	
		"polOrHolo"
	,
		"spectralWindowId"
		
	};
	//public String[] getRequiredValueAttributesNames() { return requiredValueAttributesNames;}
	
	private final String[] optionalValueAttributesNames = {
			"flagRow"
	};
	//public String[] getOptionalValueAttributesNames() { return optionalValueAttributesNames; }	

		
	/**
	 * Create a DataDescriptionTable.
	 * <p>
	 * This constructor has package access because only the
	 * container can create tables.  All tables must know the container
	 * to which they belong.
	 * @param container The container to which this table belongs.
	 */ 
	DataDescriptionTable (ASDM container) {
		this.container = container;
		
		privateRows = new ArrayList();
		
		 
		row = new ArrayList(); 
		
		entity = new Entity();
		entity.setEntityId(new EntityId("uid://X0/X0/X0"));
		entity.setEntityIdEncrypted("na");
		entity.setEntityTypeName("DataDescriptionTable");
		entity.setEntityVersion("1");
		entity.setInstanceVersion("1");
		

		noAutoIncIds = new Hashtable();

	}
	
	/**
	 * Return the container to which this table belongs.
	 * @return a ASDM.
	 */
	public ASDM getContainer() {
		return container;
	}
	
	/**
	 * Return the number of rows in the table.
	 */

	public int size() {
		return row.size();
	}		


	/**
	 * Return the name of this table.
	 */
	public String getName() {
		return tableName;
	}
	
	/**
	 * Returns "DataDescriptionTable" followed by the current size of the table 
	 * between parenthesis. 
	 * Example : SpectralWindowTable(12)
	 * @return a String.
	 */
	 public String toString() {
	 	return "DataDescriptionTable("+size()+")";
	 }
	 
	//
	// ====> Row creation.
	//

	/**
	 * Create a new row with default values .
	 * @return a DataDescriptionRow
	 */
	public DataDescriptionRow newRow() {
		return new DataDescriptionRow(this);
	}
	//
	// Append a row to its table.
	//

	
	 
	
	/** 
 	 * Look up the table for a row whose noautoincrementable attributes are matching their
 	 * homologues in x.  If a row is found  this row else autoincrement  x.dataDescriptionId, 
 	 * add x to its table and returns x.
 	 *  
 	 * @returns a DataDescriptionRow.
 	 * @param x. A row to be added.
 	 */ 
 		 
		
	public DataDescriptionRow add(DataDescriptionRow x) {
				
			 
		DataDescriptionRow aRow = lookup(
				
			x.getPolOrHoloId()
				,
			x.getSpectralWindowId()
				
		);
		if (aRow != null) return aRow;
			
		try {		
			
			// Autoincrement dataDescriptionId
			x.setDataDescriptionId(new Tag(size(), TagType.DataDescription));
			
		} catch (IllegalAccessException e) {
			// never happens.
		}			
		row.add(x);
		privateRows.add(x);
		x.isAdded();
		return x;
	}
		
		



	
	/**
	 * Create a new row initialized to the specified values.
	 * (the autoincrementable attribute if any is not in the parameter list)
	 * @return the new initialized row.
	
 	 * @param polOrHolo. 
	
 	 * @param spectralWindowId. 

	 */
	public DataDescriptionRow newRow(Tag polOrHolo, Tag spectralWindowId) {
		DataDescriptionRow row = new DataDescriptionRow(this);
		try {
			
		row.setPolOrHoloId(polOrHolo);
			
		row.setSpectralWindowId(spectralWindowId);
	
		}
		catch (Exception e) {
			// Never happens
		}		

	    return row;
	}


	/**
	 * Create a new row using a copy constructor mechanism.
	 * 
	 * The method creates a new DataDescriptionRow owned by this. Each attribute of the created row 
	 * is a (deep) copy of the corresponding attribute of row. The method does not add 
	 * the created row to this, its simply parents it to this, a call to the add method
	 * has to be done in order to get the row added (very likely after having modified
	 * some of its attributes).
	 * If row is null then the method returns a new DataDescriptionRow with default values for its attributes. 
	 *
	 * @param row the row which is to be copied.
	 */
	 public DataDescriptionRow newRow(DataDescriptionRow row) {
	 	return new DataDescriptionRow(this, row);
	 }

//
// ====> Append a row to its table.
//

	// 
	// A private method to append a row to its table, used by input conversion
	// methods.
	//

	
	/**
	 * If this table has an autoincrementable attribute then check if *x verifies the rule of uniqueness and throw exception if not.
	 * Check if *x verifies the key uniqueness rule and throw an exception if not.
	 * Append x to its table.
	 * @param x a pointer on the row to be appended.
	 * @returns a pointer on x.
	 */
	private DataDescriptionRow  checkAndAdd(DataDescriptionRow x) throws DuplicateKey, UniquenessViolationException {
 

	 
	
		if (lookup(
		
			x.getPolOrHoloId()
		,
			x.getSpectralWindowId()
		
		) != null) throw new UniquenessViolationException("Uniqueness violation exception in table DataDescriptionTable");
	
	
		
		if (getRowByKey(
		
			x.getDataDescriptionId()
				
		) != null) throw new DuplicateKey("Duplicate key exception in ", "DataDescriptionTable");
		
		row.add(x);
		privateRows.add(x);
		x.isAdded();
		return x;
	}	



	

	//
	// ====> Methods returning rows.
	//	



	
	/**
	 * Get all rows.
	 * @return Alls rows as an array of DataDescriptionRow
	 */
	public DataDescriptionRow[] get() {
		DataDescriptionRow[] x = new DataDescriptionRow [row.size()];
		x = (DataDescriptionRow[])row.toArray(x);
		return x;
	}
	


	 
	/**
 	 * Returns a DataDescriptionRow given a key.
 	 * @return the row having the key whose values are passed as parameters, or null if
 	 * no row exists for that key.
		
 	 * @param dataDescriptionId. 
		
 	 *
 	 */
	public DataDescriptionRow getRowByKey(Tag dataDescriptionId)  {
 		Iterator iter = row.iterator();
 		DataDescriptionRow x = null;
 		while (iter.hasNext()) {
 			x = (DataDescriptionRow) iter.next();
		
			
			if (!x.getDataDescriptionId().equals(dataDescriptionId)) continue;
			
		
			return x;
		}
		return null;		
	}
	

	
	/**
 	 * Look up the table for a row whose all attributes  except the autoincrementable one 
 	 * are equal to the corresponding parameters of the method.
 	 * @return this row if any, null otherwise.
 	 *
		
 	 * @param polOrHolo.
 	 	
 	 * @param spectralWindowId.
 	 		 	 
 	 */
	public DataDescriptionRow lookup(Tag polOrHolo, Tag spectralWindowId) {
		for (int i = 0; i < size(); i++) 
		if (((DataDescriptionRow)row.get(i)).compareNoAutoInc(polOrHolo, spectralWindowId))
					return (DataDescriptionRow) row.get(i);
					
			return null;
	} 	 
	
 	 
	

	

public ASDMRow[] getRows() {
		return (ASDMRow[]) get();
}


	
	// 
	// ====> Conversion Methods
	//
	/**
	 * Convert this table into a DataDescriptionTableIDL CORBA structure.
	 *
	 * @return a DataDescriptionTableIDL
	 */
	public DataDescriptionTableIDL toIDL() {
		DataDescriptionTableIDL x = new DataDescriptionTableIDL ();
		DataDescriptionRow[] v = get();
		x.row = new DataDescriptionRowIDL[v.length];
		for (int i = 0; i < v.length; ++i) {
			x.row[i] = v[i].toIDL();
		}
		return x;
	}
	
	/**
	 * Populate this table from the content of a DataDescriptionTableIDL Corba structure.
	 *
	 * @throws DuplicateKey Thrown if the method tries to add a row having a key that is already in the table.
	 * @throws ConversionException
	 */
	public void fromIDL(DataDescriptionTableIDL x) throws DuplicateKey,ConversionException, UniquenessViolationException  {
		DataDescriptionRow tmp = null;
		for (int i = 0; i < x.row.length; ++i) {
			tmp = newRow();
			tmp.setFromIDL(x.row[i]);
			// checkAndAdd(tmp);
			add(tmp);
		}
	}

	/**
	 * To be implemented
	 */
	public byte[] toFITS() throws ConversionException {
		// TODO 
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromFITS(byte[] fits) throws ConversionException {
		// TODO 
	}

	/**
	 * To be implemented
	 */
	public String toVOTable() throws ConversionException {
		// TODO 
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromVOTable(String vo) throws ConversionException {
		// TODO 
	}


	/**
	 * Translate this table to an XML representation conform
	 * to the schema defined for DataDescription (DataDescriptionTable.xsd).
	 *
	 * @returns a string containing the XML representation.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> ");
		buf.append("<DataDescriptionTable xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dtdsc=\"http://Alma/XASDM/DataDescriptionTable\" xsi:schemaLocation=\"http://Alma/XASDM/DataDescriptionTable http://almaobservatory.org/XML/XASDM/1/DataDescriptionTable.xsd\"> ");

		//buf.append("<DataDescriptionTable>");
		buf.append(entity.toXML());
		String s = container.getEntity().toXML();
		// Change the "Entity" tag to "ContainerEntity".
		buf.append("<Container" + s.substring(1));
		DataDescriptionRow[] v = get();
		for (int i = 0; i < size(); ++i) {
			buf.append(v[i].toXML());
			buf.append(" ");
		}		
		buf.append("</DataDescriptionTable>");
		return buf.toString();
	}

	/**
	 * Populate this table from the content of a XML document that is required to
	 * be conform to the XML schema defined for a DataDescription (DataDescriptionTable.xsd).
	 * @throws ConversionException
	 */
	public void fromXML(String xmlDoc) throws  ConversionException{
		Parser xml = new Parser(xmlDoc);
		if (!xml.isStr("<DataDescriptionTable")) 
			error();
		String s = xml.getElement("<Entity",">");
		if (s == null) 
			error();
		Entity e = new Entity();
		e.setFromXML(s);
		if (!e.getEntityTypeName().equals("DataDescriptionTable"))
			error();
		setEntity(e);
		// Skip the container's entity; but, it has to be there.
		s = xml.getElement("<ContainerEntity",">");
		if (s == null) 
			error();

		// Get each row in the table.
		s = xml.getElementContent("<row>","</row>");
		DataDescriptionRow row = null;
		while (s != null) {
			row = newRow();
			row.setFromXML(s);
			try {
				checkAndAdd(row);
			} catch (DuplicateKey e1) {
				throw new ConversionException(e1.toString(),"DataDescriptionTable");
			} catch(UniquenessViolationException e2) {
				throw new ConversionException(e2.toString(),"DataDescriptionTable");
			}
		       			
			s = xml.getElementContent("<row>","</row>");
		}
		if (!xml.isStr("</DataDescriptionTable>")) 
			error();
	}
	private void error() throws ConversionException {
		throw new ConversionException("Invalid xml document","DataDescription");
	}

  	/**
	  * Serialize this into a stream of bytes and encapsulates that stream into a MIME message.
	  * @returns an  array of bytes containing the MIME message.
	  * 
	  */
	public byte[] toMIME() throws ConversionException {
		throw new ConversionException("The toMIME method is not implemented", "DataDescription");
	}

	static private boolean binaryPartFound(DataInputStream dis, String s, int pos) throws IOException {
		int posl = pos;
		int count = 0;
		dis.mark(1000000);
		try {
			while ( dis.readByte() != s.charAt(posl)){
				count ++;
			}
		}
		catch (EOFException e) {
			return false;
		}
		
		if (posl == (s.length() - 1)) return true;

		if (pos == 0) {
			posl++;
			return binaryPartFound(dis, s, posl);
		}
		else {
			if (count > 0) { dis.reset();  return binaryPartFound(dis, s, 0) ; }
			else {
				posl++;
				return binaryPartFound(dis, s, posl);
			}
		}
	}
  /** 
    * Extracts the binary part of a MIME message and deserialize its content
	 * to fill this with the result of the deserialization. 
	 * @param mimeMsg the array of bytes containing the MIME message.
	 * @throws ConversionException
	 */
	 public void setFromMIME(byte[]  mimeMsg) throws ConversionException {
		throw new ConversionException("The setFromMIME method is not implemented", "DataDescription");
	}
	
	/**
	  * Stores a representation (binary or XML) of this table into a file.
	  *
	  * Depending on the boolean value of its private field fileAsBin a binary serialization  of this (fileAsBin==true)  
	  * will be saved in a file "DataDescription.bin" or an XML representation (fileAsBin==false) will be saved in a file "DataDescription.xml".
	  * The file is always written in a directory whose name is passed as a parameter.
	  * @param directory The name of directory  where the file containing the table's representation will be saved.
	  * @throws ConversionException
	  */
	  public void toFile(String directory) throws ConversionException {
	  	// Check if the directory exists
	  	File directoryFile = new File(directory);
	  	if (directoryFile.exists() && !directoryFile.isDirectory())
	  		throw new ConversionException("Cannot write into directory " 
	  			+  directoryFile.getAbsolutePath() 
	  			+ ". This file already exists and is not a directory", "DataDescription");
	  			
	  	//if not let's create it.		
	  	if (!directoryFile.exists()) {
	  		if (!directoryFile.mkdir())
	  			throw new ConversionException("Could not create directory " + directoryFile.getAbsolutePath(), "DataDescription");
	  	}
	  		
	  	String fileName = null;
	  	BufferedWriter out = null;
	  	if (fileAsBin) {
	  		// write the bin serialized.
	  		fileName = directory+"/DataDescription.bin";
	  	}
	  	else {
	  		// write the XML
	  		fileName = directory+"/DataDescription.xml";
	  	}
	  	
	  	File file = new File(fileName);
	  	
	  	// Delete the file if it already exists
	  	// We assume here that 'file'  does not denote  a directory. The probability
	  	// for that is very low; a perfect code would take care of this possibility.
	  	if (file.exists()) {
	  			if (!file.delete())
	  				throw new ConversionException("Problem while trying to delete a previous version of '"+file.toString()+"'", "DataDescription"); 
	  	}	  	
	  	
	  	if (fileAsBin) {
	  		OutputStream os;
			try {
				os = new FileOutputStream(file);
				os.write(toMIME());
				os.close();
			} catch (FileNotFoundException e) {
	  			throw new ConversionException("Problem while writing the binary representation, the message was : " + e.getMessage(), "DataDescription");
			} catch (IOException e) {
	  			throw new ConversionException("Problem while writing the binary representation, the message was : " + e.getMessage(), "DataDescription");
			}	
	  	}
	  	else {
	  		try {
	  			out = new BufferedWriter(new FileWriter(file));
	  			out.write(toXML());
	  			out.close();
	  		}
	  		catch (IOException e) {
	  			throw new ConversionException("Problem while writing the XML representation, the message was : " + e.getMessage(), "DataDescription");
	  		}
	  	}
	  }
	  
	/**
	 * Reads and parses a file containing a representation of a DataDescriptionTable as those produced  by the toFile method.
	 * This table is populated with the result of the parsing.
	 * @param directory The name of the directory containing the file te be read and parsed.
	 * @throws ConversionException If any error occurs while reading the 
	 * files in the directory or parsing them.
	 *
	 */
	 public void setFromFile(String directory) throws ConversionException {
	 	 	File directoryFile = new File(directory);

			if (!directoryFile.isDirectory())
				throw new ConversionException ("Directory " + directory + " does not exist.", "DataDescription");
				
			String fileName = null;
			
			if ( fileAsBin) 
				fileName = directory + "/DataDescription.bin";
			else 
				fileName  = directory + "/DataDescription.xml";
			
			File file = new File(fileName);
				
			if (!file.exists())
				throw new ConversionException ("File " + fileName + " does not exists", "DataDescription");
				
			if (fileAsBin) {
				byte[] bytes = null;
       			try {
					InputStream is = new FileInputStream(file);
					long length = file.length();
					if (length > Integer.MAX_VALUE) 
						throw new ConversionException ("File " + fileName + " is too large", "DataDescription");
				
					bytes = new byte[(int)length];
					int offset = 0;
        			int numRead = 0;
 
        			while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            			offset += numRead;
        			}
        		
        			if (offset < bytes.length) {
            			throw new ConversionException("Could not completely read file "+file.getName(), "DataDescription");
            		}
            		is.close();
            	}
        		catch (IOException e) {
        			throw new ConversionException("Error while reading "+file.getName()+". The message was " + e.getMessage(),
        																	  "DataDescription");
        		}	
        		
            	setFromMIME(bytes);
			}
			else {
				try {
 					BufferedReader in = new BufferedReader(new FileReader(file));	
 					StringBuffer xmlDoc = new StringBuffer();
 					String line = in.readLine();
					while (line != null) {
						xmlDoc.append(line);
						line = in.readLine();
					}
					in.close();
					fromXML(xmlDoc.toString());
				}
				 catch (IOException e) {
					throw new ConversionException(e.getMessage(), "DataDescription");
				}	
			}  	
	 }
	/**
	 * Returns the table's entity.
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * Set the table's entity
	 *
	 * @param e An entity. 
	 */
	public void setEntity(Entity e) {
		this.entity = e; 
	}
	
	/**
     * Store this table into the archive.
     * @param ar the archiver in charge of the archiving.
     * @returns the UID assigned to the archived table.
     */
     String toArchive(Archiver ar) throws ConversionException, ArchiverException{

     
     	String retUID = null;
     	Entity ent = null;
      	try {
      		retUID = ar.getID();
      		ent = this.getEntity();
      		ent.setEntityId(new EntityId(retUID));
      		this.setEntity(ent);
     		String xmlTable = this.toXML();
     		ar.store(xmlTable, "DataDescriptionTable", retUID);
     	}
     	catch (ArchiveException e) {
      		throw new ArchiverException("Archiver exception while archiving a DataDescriptionTable under UID " + retUID+". The message was " + e.getMessage()); 
     	}
     	catch (NotAvailable e) {
  			throw new ArchiverException("Archiver exception while archiving a DataDescriptionTable under UID " + retUID+". The message was " + e.getMessage()); 
        }
     	return retUID;
     }
     
     /*
     ** Retrieves a table in the Archive given its UID.
     ** @param UID the Archive UID assigned to the archived table.
     ** @throws ArchiverException
     ** @throws ConversionException
     */
     public void fromArchive(Archiver ar, String UID) throws ConversionException, ArchiverException {
        try {
        	String xml = ar.retrieve(UID);
     		this.fromXML(xml);
     	}
     	catch (ArchiveException e) {
      		throw new ArchiverException("Archiver exception while retrieving a DataDescriptionTable with UID " + UID +". The message was " + e.getMessage());
     	}    	
     }
}



