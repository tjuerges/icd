
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File DataDescriptionRow.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
/**
 * The DataDescriptionRow class is a row of a DataDescriptionTable.
 */
public class DataDescriptionRow extends ASDMRow {
	/** 
	 * This piece of code is not clean at all...should be improved in the future...
	 */
	private Tag wrap(Tag t) {
		return t;
	}

	private Tag unwrap(Tag t) {
		return t;
	}

	private Integer wrap(int i) {
		return new Integer(i);
	}

	private Float wrap(float f) {
		return new Float(f);
	}

	private Double wrap(double d) {
		return new Double(d);
	}

	private Boolean wrap(boolean b) {
		return new Boolean(b);
	}	

	private int unwrap(Integer I) {
		return I.intValue();
	}
	/**
	 * End of poor piece of code...
	 */


	/**
	 * Create a DataDescriptionRow.
	 * <p>
	 * This constructor has package access because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 */ 
	DataDescriptionRow (DataDescriptionTable table) {
		this.table = table;
		this.hasBeenAdded = false;
	} 

	/**
	 * Creates a DataDescriptionRow using a copy constructor mechanism.
	 * <p>
	 * Given a DataDescriptionRow row and a DataDescriptionTable table, the method creates a new
	 * DataDescriptionRow owned by table. Each attribute of the created row is a copy (deep)
	 * of the corresponding attribute of row. The method does not add the created
	 * row to its table, its simply parents it to table, a call to the add method
	 * has to be done in order to get the row added (very likely after having modified
	 * some of its attributes).
	 * If row is null then the method returns a row with default values for its attributes. 
	 *
	 * This constructor has package access because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 * @param row  The row which is to be copied.
	 */ 
	DataDescriptionRow (DataDescriptionTable table, DataDescriptionRow row) {
		this.table = table;
		this.hasBeenAdded = false;

		if (row == null) return;






		// dataDescriptionId is a Tag, a new Tag is required.
		this.dataDescriptionId = new Tag(row.dataDescriptionId); 









		// polOrHoloId is a Tag, a new Tag is required.
		this.polOrHoloId = new Tag(row.polOrHoloId); 






		// spectralWindowId is a Tag, a new Tag is required.
		this.spectralWindowId = new Tag(row.spectralWindowId); 


	} 

	/**
	 * The table to which this row belongs.
	 */
	private DataDescriptionTable table;

	/**
	 * Whether this row has been added to the table or not.
	 */
	private boolean hasBeenAdded;

	// This method has package access and is used by the Table class
	// when it is added to the table.
	void isAdded() {
		hasBeenAdded = true;
	}

	/**
	 * Return the table to which this row belongs.
	 */
	public DataDescriptionTable getTable () {
		return table;
	}

	/**
	 * Return this row in the form of an IDL struct.
	 * @return The values of this row as a DataDescriptionRowIDL struct.
	 */
	public DataDescriptionRowIDL toIDL() {
		DataDescriptionRowIDL x = new DataDescriptionRowIDL ();

		// Fill the IDL structure.







		x.dataDescriptionId = getDataDescriptionId().toIDLTag();



		x.polOrHoloId = getPolOrHoloId().toIDLTag();










		x.spectralWindowId = getSpectralWindowId().toIDLTag();












		return x;
	}

	/**
	 * Fill the values of this row from the IDL struct DataDescriptionRowIDL.
	 * @param x The IDL struct containing the values used to fill this row.
	 */
	public void setFromIDL (DataDescriptionRowIDL x) throws ConversionException {
		try {
			// Fill the values from x.









			setDataDescriptionId(new Tag(x.dataDescriptionId));				












			setPolOrHoloId(new Tag (x.polOrHoloId));













			setSpectralWindowId(new Tag (x.spectralWindowId));











		} catch (IllegalAccessException err) {
			throw new ConversionException (err.toString(),"DataDescription");
		}
	}

	/**
	 * Return this row in the form of an XML string.
	 * @return The values of this row as an XML string.
	 * @throws ConversionException.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<row> ");








		Parser.toXML(dataDescriptionId, "dataDescriptionId", buf);	








		Parser.toXML(polOrHoloId, "polOrHoloId", buf);	










		Parser.toXML(spectralWindowId, "spectralWindowId", buf);	











		buf.append("</row>");
		return buf.toString();
	}

	/**
	 * Fill the values of this row from an XML string 
	 * that was produced by the toXML() method.
	 * @param x The XML string being used to set the values of this row.
	 * @throws ConversionException.
	 */
	public void setFromXML (String rowDoc) throws ConversionException {
		Parser row = new Parser(rowDoc);
		String s = null;
		try {





			setDataDescriptionId(Parser.getTag("dataDescriptionId","DataDescription",rowDoc));



			setPolOrHoloId(Parser.getTag("polOrHoloId","DataDescription",rowDoc));







			setSpectralWindowId(Parser.getTag("spectralWindowId","DataDescription",rowDoc));










		} catch (IllegalAccessException err) {
			throw new ConversionException (err.toString(),"DataDescription");
		}
	}	

	////////////////////////////////
	// Intrinsic Table Attributes //
	////////////////////////////////



	// ===> Attribute dataDescriptionId





	private  Tag dataDescriptionId; 





	/**
	 * Get dataDescriptionId.
	 * @return dataDescriptionId as Tag
	 */
	public Tag getDataDescriptionId (){




		return new Tag(dataDescriptionId);


	}


	/**
	 * Set dataDescriptionId with the specified Tag value.
	 * @param dataDescriptionId The Tag value to which dataDescriptionId is to be set.

	 * @throw IllegalAccessException If an attempt is made to change this field after is has been added to the table.
	 */

	protected  void setDataDescriptionId (Tag dataDescriptionId) throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException("Attempt to change the dataDescriptionId field, that is part of a key, after this row has been added to this table.");
		}



		this.dataDescriptionId = new Tag(dataDescriptionId);




	}






	////////////////////////////////
	// Extrinsic Table Attributes //
	////////////////////////////////


	// ===> Attribute polOrHoloId





	private  Tag polOrHoloId; 





	/**
	 * Get polOrHoloId.
	 * @return polOrHoloId as Tag
	 */
	public Tag getPolOrHoloId (){




		return new Tag(polOrHoloId);


	}


	/**
	 * Set polOrHoloId with the specified Tag value.
	 * @param polOrHoloId The Tag value to which polOrHoloId is to be set.

	 */
	public void setPolOrHoloId (Tag polOrHoloId) {



		this.polOrHoloId = new Tag(polOrHoloId);




	}






	// ===> Attribute spectralWindowId





	private  Tag spectralWindowId; 





	/**
	 * Get spectralWindowId.
	 * @return spectralWindowId as Tag
	 */
	public Tag getSpectralWindowId (){




		return new Tag(spectralWindowId);


	}


	/**
	 * Set spectralWindowId with the specified Tag value.
	 * @param spectralWindowId The Tag value to which spectralWindowId is to be set.

	 */
	public void setSpectralWindowId (Tag spectralWindowId) {



		this.spectralWindowId = new Tag(spectralWindowId);




	}





	///////////
	// Links //
	///////////






	/**
	 * Returns the row in the Polarization table having Polarization.poIarizationId == polOrHoloId
	 * @return a PolarizationRow or null if polOrHoloId is an Holography Tag.
	 * 
	 */
	public PolarizationRow getPolarizationUsingPolOrHoloId() {
		if (this.polOrHoloId.getTagType() == TagType.Holography) 
			return null;
		else
			return table.getContainer().getPolarization().getRowByKey(polOrHoloId);
	}

	/**
	 * Returns the row in the Holography table having Holography.holographyId == polOrHoloId
	 * @return a PolarizationRow or null if polOrHoloId is an Polarization Tag.
	 * 
	 */
	public HolographyRow getHolographyUsingPolOrHoloId() {
		if (this.polOrHoloId.getTagType() == TagType.Polarization) 
			return null;
		else
			return table.getContainer().getHolography().getRowByKey(polOrHoloId);
	}







	/**
	 * Returns the pointer to the row in the SpectralWindow table having SpectralWindow.spectralWindowId == spectralWindowId
	 * @return a SpectralWindowRow
	 * 

	 */
	public SpectralWindowRow getSpectralWindowUsingSpectralWindowId() {

		return table.getContainer().getSpectralWindow().getRowByKey(spectralWindowId);
	}






	/**
	 * Compare each attribute except the autoincrementable one of this DataDescriptionRow with 
	 * the corresponding parameters and return true if there is a match and false otherwise.
	 */ 
	public boolean compareNoAutoInc(Tag polOrHoloId, Tag spectralWindowId) {




		// polOrHoloId is a Tag, compare using the equals method.
		if (!(this.polOrHoloId.equals(polOrHoloId))) return false; 






		// spectralWindowId is a Tag, compare using the equals method.
		if (!(this.spectralWindowId.equals(spectralWindowId))) return false; 






		return true;
	}	



	/**
	 * Return true if all required attributes of the value part are equal to their homologues
	 * in x and false otherwise.
	 *
	 * @param x the DataDescriptionRow whose required attributes of the value part 
	 * will be compared with those of this.
	 * @return a boolean.
	 */
	public boolean equalByRequiredValue(DataDescriptionRow x) {

		return compareRequiredValue(

				x.getPolOrHoloId()
				,
				x.getSpectralWindowId()
		);

	}

	public boolean compareRequiredValue(Tag polOrHoloId, Tag spectralWindowId) {




		// polOrHoloId is a Tag, compare using the equals method.
		if (!(this.polOrHoloId.equals(polOrHoloId))) return false; 






		// spectralWindowId is a Tag, compare using the equals method.
		if (!(this.spectralWindowId.equals(spectralWindowId))) return false; 






		return true;	
	}	

	////////////////////////////////////////// 
	//Attributes values as array of Objects.//
	//////////////////////////////////////////


	// Row display related stuff
	Object[] getAttributesValues() {
		Object[] r = new Object[getTable().getAttributesNames().length];
		int i = 0;


		r[i++] = getDataDescriptionId();





		r[i++] = getPolOrHoloId();



		r[i++] = getSpectralWindowId();


	 		 
		return r;	

	}

}



