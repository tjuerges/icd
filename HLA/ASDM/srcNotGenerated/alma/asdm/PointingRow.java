/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File PointingRow.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * The PointingRow class is a row of a PointingTable.
 */
public class PointingRow extends ASDMRow {
	/**
	 * This piece of code is not clean at all...should be improved in the
	 * future...
	 */
	private Tag wrap(Tag t) {
		return t;
	}

	private Tag unwrap(Tag t) {
		return t;
	}

	private Integer wrap(int i) {
		return new Integer(i);
	}

	private Float wrap(float f) {
		return new Float(f);
	}

	private Double wrap(double d) {
		return new Double(d);
	}

	private Boolean wrap(boolean b) {
		return new Boolean(b);
	}

	private int unwrap(Integer I) {
		return I.intValue();
	}

	/**
	 * End of poor piece of code...
	 */

	/**
	 * Create a PointingRow.
	 * <p>
	 * This constructor has package access because only the table can create
	 * rows. All rows know the table to which they belong.
	 * 
	 * @param table
	 *            The table to which this row belongs.
	 */
	PointingRow(PointingTable table) {
		this.table = table;
		this.hasBeenAdded = false;

		this.nameExists = false;
		this.sourceOffsetExists = false;
		this.phaseTrackingExists = false;
		this.overTheTopExists = false;

	}

	/**
	 * Creates a PointingRow using a copy constructor mechanism.
	 * <p>
	 * Given a PointingRow row and a PointingTable table, the method creates a
	 * new PointingRow owned by table. Each attribute of the created row is a
	 * copy (deep) of the corresponding attribute of row. The method does not
	 * add the created row to its table, its simply parents it to table, a call
	 * to the add method has to be done in order to get the row added (very
	 * likely after having modified some of its attributes). If row is null then
	 * the method returns a row with default values for its attributes.
	 * 
	 * This constructor has package access because only the table can create
	 * rows. All rows know the table to which they belong.
	 * 
	 * @param table
	 *            The table to which this row belongs.
	 * @param row
	 *            The row which is to be copied.
	 */
	PointingRow(PointingTable table, PointingRow row) {
		this.table = table;
		this.hasBeenAdded = false;

		if (row == null)
			return;

		// antennaId is a Tag, a new Tag is required.
		this.antennaId = new Tag(row.antennaId);

		// timeInterval is a ArrayTimeInterval, a new ArrayTimeInterval is
		// required.
		this.timeInterval = new ArrayTimeInterval(row.timeInterval);

		// pointingModelId is a int, a simple assignment (=) is ok.
		this.pointingModelId = row.pointingModelId;


		// numPoly is a int, a simple assignment (=) is ok.
		this.numPoly = row.numPoly;

		// timeOrigin is a ArrayTime, a new ArrayTime is required.
		this.timeOrigin = new ArrayTime(row.timeOrigin);

		// pointingDirection is an array , let's use the copyPointingDirection
		// method.
		this.pointingDirection = copyPointingDirection(row.pointingDirection);

		// target is an array , let's use the copyTarget method.
		this.target = copyTarget(row.target);

		// encoder is an array , let's use the copyEncoder method.
		this.encoder = copyEncoder(row.encoder);

		// pointingTracking is a boolean, a simple assignment (=) is ok.
		this.pointingTracking = row.pointingTracking;


		this.offset = copyOffset(row.offset);
		
		if (row.nameExists) {
			this.name = new String(row.name);
			this.nameExists = true;
		}
		else
			this.nameExists = false;

		if (row.sourceOffsetExists) {

			// sourceOffset is an array , let's use the copySourceOffset method.
			this.sourceOffset = copySourceOffset(row.sourceOffset);

			this.sourceOffsetExists = true;
		}

		if (row.phaseTrackingExists) {
			this.phaseTracking = row.phaseTracking;
			this.phaseTrackingExists = true;
		}
		else 
			this.phaseTrackingExists = false;
		
		if (row.overTheTopExists) {

			// overTheTop is a boolean, a simple assignment (=) is ok.
			this.overTheTop = row.overTheTop;

			this.overTheTopExists = true;
		}

	}

	/**
	 * The table to which this row belongs.
	 */
	private PointingTable table;

	/**
	 * Whether this row has been added to the table or not.
	 */
	private boolean hasBeenAdded;

	// This method has package access and is used by the Table class
	// when it is added to the table.
	void isAdded() {
		hasBeenAdded = true;
	}

	/**
	 * Return the table to which this row belongs.
	 */
	public PointingTable getTable() {
		return table;
	}

	/**
	 * Return this row in the form of an IDL struct.
	 * 
	 * @return The values of this row as a PointingRowIDL struct.
	 */
	public PointingRowIDL toIDL() {
		PointingRowIDL x = new PointingRowIDL();

		// Fill the IDL structure.

		x.timeInterval = getTimeInterval().toIDLArrayTimeInterval();

		x.nameExists = nameExists;
		if (nameExists) {
			try {

				x.name = getName();

			} catch (IllegalAccessException e) {
			}
		} else {

			// name is not an array.

		}

		x.numPoly = getNumPoly();

		x.timeOrigin = getTimeOrigin().toIDLArrayTime();

		Angle[][] tmpPointingDirection = getPointingDirection();
		x.pointingDirection = new IDLAngle[tmpPointingDirection.length][tmpPointingDirection[0].length];
		for (int i = 0; i < x.pointingDirection.length; ++i)
			for (int j = 0; j < x.pointingDirection[i].length; ++j)
				x.pointingDirection[i][j] = tmpPointingDirection[i][j]
						.toIDLAngle();

		Angle[][] tmpTarget = getTarget();
		x.target = new IDLAngle[tmpTarget.length][tmpTarget[0].length];
		for (int i = 0; i < x.target.length; ++i)
			for (int j = 0; j < x.target[i].length; ++j)
				x.target[i][j] = tmpTarget[i][j].toIDLAngle();

		Angle[][] tmpOffset = getOffset();
		x.offset = new IDLAngle[tmpOffset.length][tmpOffset[0].length];
		for (int i = 0; i < x.offset.length; ++i)
			for (int j = 0; j < x.offset[i].length; ++j)
				x.offset[i][j] = tmpOffset[i][j].toIDLAngle();
		x.sourceOffsetExists = sourceOffsetExists;
		if (sourceOffsetExists) {
			try {

				Angle[][] tmpSourceOffset = getSourceOffset();
				x.sourceOffset = new IDLAngle[tmpSourceOffset.length][tmpSourceOffset[0].length];
				for (int i = 0; i < x.sourceOffset.length; ++i)
					for (int j = 0; j < x.sourceOffset[i].length; ++j)
						x.sourceOffset[i][j] = tmpSourceOffset[i][j]
								.toIDLAngle();

			} catch (IllegalAccessException e) {
			}
		} else {

			x.sourceOffset = new IDLAngle[0][0];

		}

		Angle[] tmpEncoder = getEncoder();
		x.encoder = new IDLAngle[tmpEncoder.length];
		for (int i = 0; i < x.encoder.length; ++i)
			x.encoder[i] = tmpEncoder[i].toIDLAngle();

		x.pointingTracking = getPointingTracking();

		x.phaseTrackingExists = phaseTrackingExists;
		if (phaseTrackingExists) {
			try {

				x.phaseTracking = getPhaseTracking();

			} catch (IllegalAccessException e) {
			}
		} else {

			// phaseTracking is not an array.

		}

		x.overTheTopExists = overTheTopExists;
		if (overTheTopExists) {
			try {

				x.overTheTop = getOverTheTop();

			} catch (IllegalAccessException e) {
			}
		} else {

			// overTheTop is not an array.

		}

		x.antennaId = getAntennaId().toIDLTag();

		x.pointingModelId = getPointingModelId();

		return x;
	}

	/**
	 * Fill the values of this row from the IDL struct PointingRowIDL.
	 * 
	 * @param x
	 *            The IDL struct containing the values used to fill this row.
	 */
	public void setFromIDL(PointingRowIDL x) throws ConversionException {
		try {
			// Fill the values from x.

			setTimeInterval(new ArrayTimeInterval(x.timeInterval));

			if (x.nameExists) {

				setName(x.name);

			}

			setNumPoly(x.numPoly);

			setTimeOrigin(new ArrayTime(x.timeOrigin));

			Angle[][] tmpPointingDirection = new Angle[x.pointingDirection.length][x.pointingDirection[0].length];
			for (int i = 0; i < x.pointingDirection.length; ++i)
				for (int j = 0; j < x.pointingDirection[i].length; ++j)
					tmpPointingDirection[i][j] = new Angle(
							x.pointingDirection[i][j]);
			setPointingDirection(tmpPointingDirection);

			Angle[][] tmpTarget = new Angle[x.target.length][x.target[0].length];
			for (int i = 0; i < x.target.length; ++i)
				for (int j = 0; j < x.target[i].length; ++j)
					tmpTarget[i][j] = new Angle(x.target[i][j]);
			setTarget(tmpTarget);

			Angle[][] tmpOffset = new Angle[x.offset.length][x.offset[0].length];
			for (int i = 0; i < x.offset.length; ++i)
				for (int j = 0; j < x.offset[i].length; ++j)
					tmpOffset[i][j] = new Angle(x.offset[i][j]);
			setOffset(tmpOffset);

			if (x.sourceOffsetExists) {

				Angle[][] tmpSourceOffset = new Angle[x.sourceOffset.length][x.sourceOffset[0].length];
				for (int i = 0; i < x.sourceOffset.length; ++i)
					for (int j = 0; j < x.sourceOffset[i].length; ++j)
						tmpSourceOffset[i][j] = new Angle(x.sourceOffset[i][j]);
				setSourceOffset(tmpSourceOffset);

			}

			Angle[] tmpEncoder = new Angle[x.encoder.length];
			for (int i = 0; i < x.encoder.length; ++i)
				tmpEncoder[i] = new Angle(x.encoder[i]);
			setEncoder(tmpEncoder);

			setPointingTracking(x.pointingTracking);

			if (x.phaseTrackingExists) {

				setPhaseTracking(x.phaseTracking);

			}

			if (x.overTheTopExists) {

				setOverTheTop(x.overTheTop);

			}

			setAntennaId(new Tag(x.antennaId));

			setPointingModelId(x.pointingModelId);

		} catch (IllegalAccessException err) {
			throw new ConversionException(err.toString(), "Pointing");
		}
	}

	/**
	 * Return this row in the form of an XML string.
	 * 
	 * @return The values of this row as an XML string.
	 * @throws ConversionException.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<row> ");

		Parser.toXML(timeInterval, "timeInterval", buf);

		if (nameExists) {

			Parser.toXML(name, "name", buf);

		}

		Parser.toXML(numPoly, "numPoly", buf);

		Parser.toXML(timeOrigin, "timeOrigin", buf);

		Parser.toXML(pointingDirection, "pointingDirection", buf);

		Parser.toXML(target, "target", buf);

		Parser.toXML(offset, "offset", buf);

		if (sourceOffsetExists) {

			Parser.toXML(sourceOffset, "sourceOffset", buf);

		}

		Parser.toXML(encoder, "encoder", buf);

		Parser.toXML(pointingTracking, "pointingTracking", buf);

		if (phaseTrackingExists) {

			Parser.toXML(phaseTracking, "phaseTracking", buf);

		}

		if (overTheTopExists) {

			Parser.toXML(overTheTop, "overTheTop", buf);

		}

		Parser.toXML(antennaId, "antennaId", buf);

		Parser.toXML(pointingModelId, "pointingModelId", buf);

		buf.append("</row>");
		return buf.toString();
	}

	/**
	 * Fill the values of this row from an XML string that was produced by the
	 * toXML() method.
	 * 
	 * @param x
	 *            The XML string being used to set the values of this row.
	 * @throws ConversionException.
	 */
	public void setFromXML(String rowDoc) throws ConversionException {
		Parser row = new Parser(rowDoc);
		String s = null;
		try {

			setTimeInterval(Parser.getArrayTimeInterval("timeInterval",
					"Pointing", rowDoc));

			if (row.isStr("<name>")) {

				setName(Parser.getString("name", "Pointing", rowDoc));

			}

			setNumPoly(Parser.getInteger("numPoly", "Pointing", rowDoc));

			setTimeOrigin(Parser.getArrayTime("timeOrigin", "Pointing", rowDoc));

			setPointingDirection(Parser.get2DAngle("pointingDirection",
					"Pointing", rowDoc));

			setTarget(Parser.get2DAngle("target", "Pointing", rowDoc));

			setOffset(Parser.get2DAngle("offset", "Pointing", rowDoc));

			if (row.isStr("<sourceOffset>")) {

				setSourceOffset(Parser.get2DAngle("sourceOffset", "Pointing",
						rowDoc));

			}

			setEncoder(Parser.get1DAngle("encoder", "Pointing", rowDoc));

			setPointingTracking(Parser.getBoolean("pointingTracking",
					"Pointing", rowDoc));

			if (row.isStr("<phaseTracking>")) {

				setPhaseTracking(Parser.getBoolean("phaseTracking", "Pointing",
						rowDoc));

			}

			if (row.isStr("<overTheTop>")) {

				setOverTheTop(Parser.getBoolean("overTheTop", "Pointing",
						rowDoc));

			}

			setAntennaId(Parser.getTag("antennaId", "Antenna", rowDoc));

			setPointingModelId(Parser.getInteger("pointingModelId", "Pointing",
					rowDoc));

		} catch (IllegalAccessException err) {
			throw new ConversionException(err.toString(), "Pointing");
		}
	}

	/**
	 * Serialize this into a stream of bytes written to a DataOutputStream
	 * 
	 * @param dos
	 *            the DataOutStream to be written to.
	 * @throws IOException
	 */
	public void toBin(DataOutputStream dos) throws IOException {

		timeInterval.toBin(dos);

		dos.writeBoolean(this.nameExists);
		if (nameExists) {
			dos.writeInt(name.length());
			dos.writeBytes(name);
		}
		
		dos.writeInt(numPoly);
		timeOrigin.toBin(dos);
		Angle.toBin(pointingDirection, dos);
		Angle.toBin(target, dos);

		Angle.toBin(offset, dos);

		dos.writeBoolean(sourceOffsetExists);
		if (sourceOffsetExists) {
			Angle.toBin(sourceOffset, dos);
		}
		Angle.toBin(encoder, dos);
		dos.writeBoolean(pointingTracking);
		dos.writeBoolean(phaseTrackingExists);
		if (phaseTrackingExists) {
			dos.writeBoolean(phaseTracking);
		}
		dos.writeBoolean(overTheTop);
		if (overTheTopExists) {
			dos.writeBoolean(overTheTop);
		}
		antennaId.toBin(dos);
		dos.writeInt(pointingModelId);
	}

	/**
	 * Deserialize a stream of bytes read from a DataInputStream to build a
	 * PointingRow.
	 * 
	 * @param dis
	 *            the DataInputStream to be read.
	 * @table the PointingTable to which the row built by deserialization will
	 *        be parented.
	 */
	public static PointingRow fromBin(DataInputStream dis, PointingTable table)
			throws IOException, TagFormatException {
		PointingRow row = new PointingRow(table);
		row.timeInterval = ArrayTimeInterval.fromBin(dis);

		row.nameExists = dis.readBoolean();
		if (row.nameExists) {
			int numChar = dis.readInt();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < numChar; i++)
				sb.append((char) dis.readByte());
			row.name = sb.toString();
		}
		
		row.numPoly = dis.readInt();
		row.timeOrigin = ArrayTime.fromBin(dis);
		row.pointingDirection = Angle.from2DBin(dis);
		row.target = Angle.from2DBin(dis);
		row.offset = Angle.from2DBin(dis);
		row.sourceOffsetExists = dis.readBoolean();
		if (row.sourceOffsetExists)
			row.sourceOffset = Angle.from2DBin(dis);
		row.encoder = Angle.from1DBin(dis);
		row.pointingTracking = dis.readBoolean();
		row.phaseTrackingExists = dis.readBoolean();
		if (row.phaseTracking) {
			row.phaseTracking = dis.readBoolean();
		}
		row.overTheTopExists = dis.readBoolean();
		if (row.overTheTopExists)
			row.overTheTop = dis.readBoolean();
		row.antennaId = Tag.fromBin(dis);
		row.pointingModelId = dis.readInt();
		return row;
	}

	// //////////////////////////////
	// Intrinsic Table Attributes //
	// //////////////////////////////

	// ===> Attribute timeInterval

	ArrayTimeInterval timeInterval;

	/**
	 * Get timeInterval.
	 * 
	 * @return timeInterval as ArrayTimeInterval
	 */
	public ArrayTimeInterval getTimeInterval() {

		return new ArrayTimeInterval(timeInterval);

	}

	/**
	 * Set timeInterval with the specified ArrayTimeInterval value.
	 * 
	 * @param timeInterval
	 *            The ArrayTimeInterval value to which timeInterval is to be
	 *            set.
	 * 
	 * @throw IllegalAccessException If an attempt is made to change this field
	 *        after is has been added to the table.
	 */

	public void setTimeInterval(ArrayTimeInterval timeInterval)
			throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException(
					"Attempt to change the timeInterval field, that is part of a key, after this row has been added to this table.");
		}

		this.timeInterval = new ArrayTimeInterval(timeInterval);

	}

	// ===> Attribute name, which is optional

	private boolean nameExists = false;

	private String name;

	/**
	 * The attribute name is optional. Return true if this attribute exists.
	 * 
	 * @return true if and only if the name attribute exists.
	 */
	public boolean isNameExists() {
		return nameExists;
	}

	/**
	 * Get name, which is optional.
	 * 
	 * @return name as String
	 * @throws IllegalAccessException
	 *             If name does not exist.
	 */
	public String getName() throws IllegalAccessException {
		if (!nameExists) {
			throw new IllegalAccessException(
					"Attempt to access a non-existent attribute.  The " + name
							+ " attribute in table Pointing does not exist!");
		}

		return new String(name);

	}

	/**
	 * Set name with the specified String value.
	 * 
	 * @param name
	 *            The String value to which name is to be set.
	 * 
	 */
	public void setName(String name) {

		this.name = name;

		nameExists = true;

	}

	/**
	 * Mark name, which is an optional field, as non-existent.
	 */
	public void clearName() {
		nameExists = false;
	}

	// ===> Attribute numPoly

	private int numPoly;

	/**
	 * Get numPoly.
	 * 
	 * @return numPoly as int
	 */
	public int getNumPoly() {

		return numPoly;

	}

	/**
	 * Set numPoly with the specified int value.
	 * 
	 * @param numPoly
	 *            The int value to which numPoly is to be set.
	 * 
	 */
	public void setNumPoly(int numPoly) {

		this.numPoly = numPoly;

	}

	// ===> Attribute timeOrigin

	private ArrayTime timeOrigin;

	/**
	 * Get timeOrigin.
	 * 
	 * @return timeOrigin as ArrayTime
	 */
	public ArrayTime getTimeOrigin() {

		return new ArrayTime(timeOrigin);

	}

	/**
	 * Set timeOrigin with the specified ArrayTime value.
	 * 
	 * @param timeOrigin
	 *            The ArrayTime value to which timeOrigin is to be set.
	 * 
	 */
	public void setTimeOrigin(ArrayTime timeOrigin) {

		this.timeOrigin = new ArrayTime(timeOrigin);

	}

	// ===> Attribute pointingDirection

	private Angle[][] pointingDirection;

	private Angle[][] copyPointingDirection(Angle[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Angle[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Angle[0][0];

		Angle[][] result = new Angle[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Angle(a[i][j]);

		return result;
	}

	/**
	 * Get pointingDirection.
	 * 
	 * @return pointingDirection as Angle[][]
	 */
	public Angle[][] getPointingDirection() {

		return copyPointingDirection(pointingDirection);

	}

	/**
	 * Set pointingDirection with the specified Angle[][] value.
	 * 
	 * @param pointingDirection
	 *            The Angle[][] value to which pointingDirection is to be set.
	 * 
	 */
	public void setPointingDirection(Angle[][] pointingDirection) {

		this.pointingDirection = copyPointingDirection(pointingDirection);

	}

	// ===> Attribute target

	private Angle[][] target;

	private Angle[][] copyTarget(Angle[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Angle[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Angle[0][0];

		Angle[][] result = new Angle[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Angle(a[i][j]);

		return result;
	}

	/**
	 * Get target.
	 * 
	 * @return target as Angle[][]
	 */
	public Angle[][] getTarget() {

		return copyTarget(target);

	}

	/**
	 * Set target with the specified Angle[][] value.
	 * 
	 * @param target
	 *            The Angle[][] value to which target is to be set.
	 * 
	 */
	public void setTarget(Angle[][] target) {

		this.target = copyTarget(target);

	}

	// ===> Attribute offset

	private Angle[][] offset;

	private Angle[][] copyOffset(Angle[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Angle[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Angle[0][0];

		Angle[][] result = new Angle[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Angle(a[i][j]);

		return result;
	}

	/**
	 * Get offset.
	 * 
	 * @return offset as Angle[][]
	 */
	public Angle[][] getOffset() {

		return copyOffset(offset);

	}

	/**
	 * Set offset with the specified Angle[][] value.
	 * 
	 * @param offset
	 *            The Angle[][] value to which offset is to be set.
	 * 
	 */
	public void setOffset(Angle[][] offset) {

		this.offset = copyOffset(offset);

	}

	// ===> Attribute sourceOffset, which is optional

	private boolean sourceOffsetExists = false;

	private Angle[][] sourceOffset;

	private Angle[][] copySourceOffset(Angle[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Angle[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Angle[0][0];

		Angle[][] result = new Angle[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Angle(a[i][j]);

		return result;
	}

	/**
	 * The attribute sourceOffset is optional. Return true if this attribute
	 * exists.
	 * 
	 * @return true if and only if the sourceOffset attribute exists.
	 */
	public boolean isSourceOffsetExists() {
		return sourceOffsetExists;
	}

	/**
	 * Get sourceOffset, which is optional.
	 * 
	 * @return sourceOffset as Angle[][]
	 * @throws IllegalAccessException
	 *             If sourceOffset does not exist.
	 */
	public Angle[][] getSourceOffset() throws IllegalAccessException {
		if (!sourceOffsetExists) {
			throw new IllegalAccessException(
					"Attempt to access a non-existent attribute.  The "
							+ sourceOffset
							+ " attribute in table Pointing does not exist!");
		}

		return copySourceOffset(sourceOffset);

	}

	/**
	 * Set sourceOffset with the specified Angle[][] value.
	 * 
	 * @param sourceOffset
	 *            The Angle[][] value to which sourceOffset is to be set.
	 * 
	 */
	public void setSourceOffset(Angle[][] sourceOffset) {

		this.sourceOffset = copySourceOffset(sourceOffset);

		sourceOffsetExists = true;

	}

	/**
	 * Mark sourceOffset, which is an optional field, as non-existent.
	 */
	public void clearSourceOffset() {
		sourceOffsetExists = false;
	}

	// ===> Attribute encoder

	private Angle[] encoder;

	private Angle[] copyEncoder(Angle[] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Angle[0];

		Angle[] result = new Angle[dim1];
		for (int i = 0; i < result.length; i++)

			result[i] = new Angle(a[i]);

		return result;
	}

	/**
	 * Get encoder.
	 * 
	 * @return encoder as Angle[]
	 */
	public Angle[] getEncoder() {

		return copyEncoder(encoder);

	}

	/**
	 * Set encoder with the specified Angle[] value.
	 * 
	 * @param encoder
	 *            The Angle[] value to which encoder is to be set.
	 * 
	 */
	public void setEncoder(Angle[] encoder) {

		this.encoder = copyEncoder(encoder);

	}

	// ===> Attribute pointingTracking

	private boolean pointingTracking;

	/**
	 * Get pointingTracking.
	 * 
	 * @return pointingTracking as boolean
	 */
	public boolean getPointingTracking() {

		return pointingTracking;

	}

	/**
	 * Set pointingTracking with the specified boolean value.
	 * 
	 * @param pointingTracking
	 *            The boolean value to which pointingTracking is to be set.
	 * 
	 */
	public void setPointingTracking(boolean pointingTracking) {

		this.pointingTracking = pointingTracking;

	}

	// ===> Attribute phaseTracking, which is optional

	private boolean phaseTrackingExists = false;

	private boolean phaseTracking;

	/**
	 * The attribute phaseTracking is optional. Return true if this attribute
	 * exists.
	 * 
	 * @return true if and only if the phaseTracking attribute exists.
	 */
	public boolean isPhaseTrackingExists() {
		return phaseTrackingExists;
	}

	/**
	 * Get phaseTracking, which is optional.
	 * 
	 * @return phaseTracking as boolean
	 * @throws IllegalAccessException
	 *             If phaseTracking does not exist.
	 */
	public boolean getPhaseTracking() throws IllegalAccessException {
		if (!phaseTrackingExists) {
			throw new IllegalAccessException(
					"Attempt to access a non-existent attribute.  The "
							+ phaseTracking
							+ " attribute in table Pointing does not exist!");
		}

		return phaseTracking;

	}

	/**
	 * Set phaseTracking with the specified boolean value.
	 * 
	 * @param phaseTracking
	 *            The boolean value to which phaseTracking is to be set.
	 * 
	 */
	public void setPhaseTracking(boolean phaseTracking) {

		this.phaseTracking = phaseTracking;

		phaseTrackingExists = true;

	}

	/**
	 * Mark phaseTracking, which is an optional field, as non-existent.
	 */
	public void clearPhaseTracking() {
		phaseTrackingExists = false;
	}

	// ===> Attribute overTheTop, which is optional

	private boolean overTheTopExists = false;

	private boolean overTheTop;

	/**
	 * The attribute overTheTop is optional. Return true if this attribute
	 * exists.
	 * 
	 * @return true if and only if the overTheTop attribute exists.
	 */
	public boolean isOverTheTopExists() {
		return overTheTopExists;
	}

	/**
	 * Get overTheTop, which is optional.
	 * 
	 * @return overTheTop as boolean
	 * @throws IllegalAccessException
	 *             If overTheTop does not exist.
	 */
	public boolean getOverTheTop() throws IllegalAccessException {
		if (!overTheTopExists) {
			throw new IllegalAccessException(
					"Attempt to access a non-existent attribute.  The "
							+ overTheTop
							+ " attribute in table Pointing does not exist!");
		}

		return overTheTop;

	}

	/**
	 * Set overTheTop with the specified boolean value.
	 * 
	 * @param overTheTop
	 *            The boolean value to which overTheTop is to be set.
	 * 
	 */
	public void setOverTheTop(boolean overTheTop) {

		this.overTheTop = overTheTop;

		overTheTopExists = true;

	}

	/**
	 * Mark overTheTop, which is an optional field, as non-existent.
	 */
	public void clearOverTheTop() {
		overTheTopExists = false;
	}

	// //////////////////////////////
	// Extrinsic Table Attributes //
	// //////////////////////////////

	// ===> Attribute antennaId

	private Tag antennaId;

	/**
	 * Get antennaId.
	 * 
	 * @return antennaId as Tag
	 */
	public Tag getAntennaId() {

		return new Tag(antennaId);

	}

	/**
	 * Set antennaId with the specified Tag value.
	 * 
	 * @param antennaId
	 *            The Tag value to which antennaId is to be set.
	 * 
	 * @throw IllegalAccessException If an attempt is made to change this field
	 *        after is has been added to the table.
	 */

	public void setAntennaId(Tag antennaId) throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException(
					"Attempt to change the antennaId field, that is part of a key, after this row has been added to this table.");
		}

		this.antennaId = new Tag(antennaId);

	}

	// ===> Attribute pointingModelId

	private int pointingModelId;

	/**
	 * Get pointingModelId.
	 * 
	 * @return pointingModelId as int
	 */
	public int getPointingModelId() {

		return pointingModelId;

	}

	/**
	 * Set pointingModelId with the specified int value.
	 * 
	 * @param pointingModelId
	 *            The int value to which pointingModelId is to be set.
	 * 
	 */
	public void setPointingModelId(int pointingModelId) {

		this.pointingModelId = pointingModelId;

	}

	// /////////
	// Links //
	// /////////

	/**
	 * Returns the pointer to the row in the Antenna table having
	 * Antenna.antennaId == antennaId
	 * 
	 * @return a AntennaRow
	 * 
	 * 
	 */
	public AntennaRow getAntennaUsingAntennaId() {

		return table.getContainer().getAntenna().getRowByKey(antennaId);
	}

	/**
	 * Compare each attribute except the autoincrementable one of this
	 * PointingRow with the corresponding parameters and return true if there is
	 * a match and false otherwise.
	 */
	public boolean compareNoAutoInc(Tag antennaId,
			ArrayTimeInterval timeInterval, int pointingModelId, int numPoly,
			ArrayTime timeOrigin, Angle[][] pointingDirection,
			Angle[][] target, Angle[][] offset, Angle[] encoder,
			boolean pointingTracking) {

		// antennaId is a Tag, compare using the equals method.
		if (!(this.antennaId.equals(antennaId)))
			return false;

		// timeInterval is a ArrayTimeInterval, compare using the equals method.
		if (!(this.timeInterval.equals(timeInterval)))
			return false;

		// pointingModelId is a int, compare using the == operator.
		if (!(this.pointingModelId == pointingModelId))
			return false;

		// numPoly is a int, compare using the == operator.
		if (!(this.numPoly == numPoly))
			return false;

		// timeOrigin is a ArrayTime, compare using the equals method.
		if (!(this.timeOrigin.equals(timeOrigin)))
			return false;

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1pointingDirection = this.pointingDirection.length;
		if (dim1pointingDirection != pointingDirection.length)
			return false;
		if (dim1pointingDirection > 0) {
			int dim2pointingDirection = this.pointingDirection[0].length;
			if (dim2pointingDirection != pointingDirection[0].length)
				return false;
			for (int i = 0; i < dim1pointingDirection; i++)
				for (int j = 0; j < dim2pointingDirection; j++) {

					// pointingDirection is an array of Angle, compare using
					// equals method.
					if (!(this.pointingDirection[i][j]
							.equals(pointingDirection[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1target = this.target.length;
		if (dim1target != target.length)
			return false;
		if (dim1target > 0) {
			int dim2target = this.target[0].length;
			if (dim2target != target[0].length)
				return false;
			for (int i = 0; i < dim1target; i++)
				for (int j = 0; j < dim2target; j++) {

					// target is an array of Angle, compare using equals method.
					if (!(this.target[i][j].equals(target[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1offset = this.offset.length;
		if (dim1offset != offset.length)
			return false;
		if (dim1offset > 0) {
			int dim2offset = this.offset[0].length;
			if (dim2offset != offset[0].length)
				return false;
			for (int i = 0; i < dim1offset; i++)
				for (int j = 0; j < dim2offset; j++) {

					// offset is an array of Angle, compare using equals method.
					if (!(this.offset[i][j].equals(offset[i][j])))
						return false;

				}
		}

		// We compare two 1D arrays.
		// Compare firstly their dimensions and then their values.
		if (this.encoder.length != encoder.length)
			return false;
		for (int i = 0; i < encoder.length; i++) {

			// encoder is an array of Angle, compare using equals method.
			if (!(this.encoder[i].equals(encoder[i])))
				return false;

		}

		// pointingTracking is a boolean, compare using the == operator.
		if (!(this.pointingTracking == pointingTracking))
			return false;

		return true;
	}

	/**
	 * Return true if all required attributes of the value part are equal to
	 * their homologues in x and false otherwise.
	 * 
	 * @param x
	 *            the PointingRow whose required attributes of the value part
	 *            will be compared with those of this.
	 * @return a boolean.
	 */
	public boolean equalByRequiredValue(PointingRow x) {

		return compareRequiredValue(

		x.getPointingModelId(), x.getNumPoly(), x.getTimeOrigin(), x
				.getPointingDirection(), x.getTarget(), x.getOffset(), x
				.getEncoder(), x.getPointingTracking()

		);

	}

	public boolean compareRequiredValue(int pointingModelId, int numPoly,
			ArrayTime timeOrigin, Angle[][] pointingDirection,
			Angle[][] target, Angle[][] offset, Angle[] encoder,
			boolean pointingTracking) {

		// pointingModelId is a int, compare using the == operator.
		if (!(this.pointingModelId == pointingModelId))
			return false;

		// numPoly is a int, compare using the == operator.
		if (!(this.numPoly == numPoly))
			return false;

		// timeOrigin is a ArrayTime, compare using the equals method.
		if (!(this.timeOrigin.equals(timeOrigin)))
			return false;

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1pointingDirection = this.pointingDirection.length;
		if (dim1pointingDirection != pointingDirection.length)
			return false;
		if (dim1pointingDirection > 0) {
			int dim2pointingDirection = this.pointingDirection[0].length;
			if (dim2pointingDirection != pointingDirection[0].length)
				return false;
			for (int i = 0; i < dim1pointingDirection; i++)
				for (int j = 0; j < dim2pointingDirection; j++) {

					// pointingDirection is an array of Angle, compare using
					// equals method.
					if (!(this.pointingDirection[i][j]
							.equals(pointingDirection[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1target = this.target.length;
		if (dim1target != target.length)
			return false;
		if (dim1target > 0) {
			int dim2target = this.target[0].length;
			if (dim2target != target[0].length)
				return false;
			for (int i = 0; i < dim1target; i++)
				for (int j = 0; j < dim2target; j++) {

					// target is an array of Angle, compare using equals method.
					if (!(this.target[i][j].equals(target[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1offset = this.offset.length;
		if (dim1offset != offset.length)
			return false;
		if (dim1offset > 0) {
			int dim2offset = this.offset[0].length;
			if (dim2offset != offset[0].length)
				return false;
			for (int i = 0; i < dim1offset; i++)
				for (int j = 0; j < dim2offset; j++) {

					// offset is an array of Angle, compare using equals method.
					if (!(this.offset[i][j].equals(offset[i][j])))
						return false;

				}
		}

		// We compare two 1D arrays.
		// Compare firstly their dimensions and then their values.
		if (this.encoder.length != encoder.length)
			return false;
		for (int i = 0; i < encoder.length; i++) {

			// encoder is an array of Angle, compare using equals method.
			if (!(this.encoder[i].equals(encoder[i])))
				return false;

		}

		// pointingTracking is a boolean, compare using the == operator.
		if (!(this.pointingTracking == pointingTracking))
			return false;

		return true;
	}

	// ////////////////////////////////////////
	// Attributes values as array of Objects.//
	// ////////////////////////////////////////

	// Row display related stuff
	Object[] getAttributesValues() {
		Object[] r = new Object[getTable().getAttributesNames().length];
		int i = 0;

		r[i++] = getAntennaId();

		r[i++] = getTimeInterval();

		r[i++] = wrap(getPointingModelId());

		r[i++] = wrap(getNumPoly());

		r[i++] = getTimeOrigin();

		;
		r[i++] = getPointingDirection();

		;
		r[i++] = getTarget();

		;
		r[i++] = getOffset();

		;
		r[i++] = getEncoder();

		r[i++] = wrap(getPointingTracking());

		if (isNameExists()) {
			try {

				r[i++] = getName();

			} catch (IllegalAccessException e) {
				;
			}
		} else {
			r[i++] = null;
		}

		if (isSourceOffsetExists()) {
			try {

				;
				r[i++] = getSourceOffset();

			} catch (IllegalAccessException e) {
				;
			}
		} else {
			r[i++] = null;
		}

		if (isPhaseTrackingExists()) {
			try {

				r[i++] = wrap(getPhaseTracking());

			} catch (IllegalAccessException e) {
				;
			}
		} else {
			r[i++] = null;
		}

		if (isOverTheTopExists()) {
			try {

				r[i++] = wrap(getOverTheTop());

			} catch (IllegalAccessException e) {
				;
			}
		} else {
			r[i++] = null;
		}

		return r;

	}

}
