
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File TotalPowerTable.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;
import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
import alma.xmlstore.IdentifierPackage.NotAvailable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collection;

/**
 * The TotalPowerTable class is an Alma table.
 * <TABLE BORDER="1">
 * <CAPTION> Attributes of TotalPower </CAPTION>
 * <TR BGCOLOR="#AAAAAA"> <TH> Name </TH> <TH> Type </TH> <TH> Comment </TH></TR>

 * <TR> <TH BGCOLOR="#CCCCCC" colspan="3" align="center"> Key </TD></TR>


 * <TR>
 * <TD> configDescriptionId </TD> 
 * <TD> Tag </TD>
 * <TD> &nbsp; </TD>
 * </TR>



 * <TR>
 * <TD> fieldId </TD> 
 * <TD> Tag </TD>
 * <TD> &nbsp; </TD>
 * </TR>



 * <TR>
 * <TD> time </TD> 
 * <TD> ArrayTime </TD>
 * <TD> &nbsp; </TD>
 * </TR>




 * <TR> <TH BGCOLOR="#CCCCCC"  colspan="3" valign="center"> Value <br> (Mandarory) </TH></TR>

 * <TR>
 * <TD> execBlockId </TD> 
 * <TD> Tag </TD>
 * <TD>  &nbsp; </TD>
 * </TR>

 * <TR>
 * <TD> stateId </TD> 
 * <TD> Tag[] </TD>
 * <TD>  ConfigDescription.numAntenna  </TD>
 * </TR>

 * <TR>
 * <TD> scanNumber </TD> 
 * <TD> int </TD>
 * <TD>  &nbsp; </TD>
 * </TR>

 * <TR>
 * <TD> subscanNumber </TD> 
 * <TD> int </TD>
 * <TD>  &nbsp; </TD>
 * </TR>

 * <TR>
 * <TD> integrationNumber </TD> 
 * <TD> int </TD>
 * <TD>  &nbsp; </TD>
 * </TR>

 * <TR>
 * <TD> uvw </TD> 
 * <TD> Length[][] </TD>
 * <TD>  ConfigDescription.numAntenna, 3  </TD>
 * </TR>

 * <TR>
 * <TD> exposure </TD> 
 * <TD> Interval[][] </TD>
 * <TD>  ConfigDescription.numAntenna, AlmaCorrelatorMode.numBaseband  </TD>
 * </TR>

 * <TR>
 * <TD> timeCentroid </TD> 
 * <TD> ArrayTime[][] </TD>
 * <TD>  ConfigDescription.numAntenna, AlmaCorrelatorMode.numBaseband  </TD>
 * </TR>

 * <TR>
 * <TD> floatData </TD> 
 * <TD> float[][][] </TD>
 * <TD>  , ,   </TD>
 * </TR>

 * <TR>
 * <TD> flagAnt </TD> 
 * <TD> int[] </TD>
 * <TD>  ConfigDescription.numAntenna  </TD>
 * </TR>

 * <TR>
 * <TD> flagPol </TD> 
 * <TD> int[][] </TD>
 * <TD>  ,   </TD>
 * </TR>

 * <TR>
 * <TD> flagRow </TD> 
 * <TD> boolean </TD>
 * <TD>  &nbsp; </TD>
 * </TR>

 * <TR>
 * <TD> interval </TD> 
 * <TD> Interval </TD>
 * <TD>  &nbsp; </TD>
 * </TR>



 * <TR> <TH BGCOLOR="#CCCCCC"  colspan="3" valign="center"> Value <br> (Optional) </TH></TR>

 * <TR>
 * <TD> subintegrationNumber </TD> 
 * <TD> int </TD>
 * <TD>  &nbsp; </TD> 
 * </TR>


 * </TABLE>
 */
public class TotalPowerTable  extends ASDMTable implements Representable{

	/**
	 * The name of this table.
	 */
	private static String tableName = "TotalPower";


	/**
	 * The list of field names that make up key key.
	 */
	private static String[] key = { "time", "configDescriptionId", "fieldId" };

	/**
	 * Return the list of field names that make up key key
	 * as an array of strings.
	 */	
	public static String[] getKeyName() {
		return key;
	}


	private ASDM container;

	// Archive binary
	private boolean archiveAsBin = true;
	
	// File binary
	private boolean fileAsBin = true;

	/** 
	 * Returns a string built by concatenating the ascii representation of the
	 * parameters values suffixed with a "_" character.
	 */
	String Key(Tag configDescriptionId, Tag fieldId) {
		StringBuffer result = new StringBuffer();


		result.append(configDescriptionId.toString()).append("_");



		result.append(fieldId.toString()).append("_");




		return result.toString();
	}		

	/** 
	 * Returns a string built by concatenating the ascii representation of the
	 * parameters values suffixed with a "_" character.
	 */
	String Key(Tag configDescriptionId, Tag fieldId, ArrayTime time) {
		StringBuffer result = new StringBuffer();


		result.append(configDescriptionId.toString()).append("_");



		result.append(fieldId.toString()).append("_");




		return result.toString();
	}			


	// A data structure to store the TotalPowerRow s.

	// In all cases we maintain a private ArrayList of TotalPowerRow s.
	private ArrayList<TotalPowerRow> privateRows;



	private ArrayList<TotalPowerRow> row;

	/**
	 * The context's key is a string resulting from a call to the method Key and the value
	 * must be a Vector of TotalPowerRow.
	 */
	private Hashtable<String, Vector<TotalPowerRow> > context;

	private Entity entity;






	private final String[] attributesNames = {

			"configDescriptionId"
			,
			"fieldId"
			,
			"time"


			,


			"execBlockId"
			,
			"stateId"
			,
			"scanNumber"
			,
			"subscanNumber"
			,
			"integrationNumber"
			,
			"uvw"
			,
			"exposure"
			,
			"timeCentroid"
			,
			"floatData"
			,
			"flagAnt"
			,
			"flagPol"
			,
			"flagRow"
			,
			"interval"


			,


			"subintegrationNumber"

	};

	public String[] getAttributesNames() {
		return attributesNames;
	}

	private final String[] keyAttributesNames = {

			"configDescriptionId"
			,
			"fieldId"
			,
			"time"

	};

	boolean inKey(String s) {
		for (int i = 0; i < keyAttributesNames.length; i++) 
			if ( keyAttributesNames[i].equals(s) ) return true;
		return false;
	}

	//public String[] getKeyAttributesNames() { return keyAttributesNames; }

	private final String[] requiredValueAttributesNames = {

			"execBlockId"
			,
			"stateId"
			,
			"scanNumber"
			,
			"subscanNumber"
			,
			"integrationNumber"
			,
			"uvw"
			,
			"exposure"
			,
			"timeCentroid"
			,
			"floatData"
			,
			"flagAnt"
			,
			"flagPol"
			,
			"flagRow"
			,
			"interval"

	};
	//public String[] getRequiredValueAttributesNames() { return requiredValueAttributesNames;}

	private final String[] optionalValueAttributesNames = {

			"subintegrationNumber"

	};
	//public String[] getOptionalValueAttributesNames() { return optionalValueAttributesNames; }	


	/**
	 * Create a TotalPowerTable.
	 * <p>
	 * This constructor has package access because only the
	 * container can create tables.  All tables must know the container
	 * to which they belong.
	 * @param container The container to which this table belongs.
	 */ 
	TotalPowerTable (ASDM container) {
		this.container = container;

		privateRows = new ArrayList<TotalPowerRow>();


		row = new ArrayList<TotalPowerRow>(); 


		context = new Hashtable<String, Vector<TotalPowerRow>>();

		entity = new Entity();
		entity.setEntityId(new EntityId("uid://X0/X0/X0"));
		entity.setEntityIdEncrypted("na");
		entity.setEntityTypeName("TotalPowerTable");
		entity.setEntityVersion("1");
		entity.setInstanceVersion("1");


	}

	/**
	 * Return the container to which this table belongs.
	 * @return a ASDM.
	 */
	public ASDM getContainer() {
		return container;
	}

	/**
	 * Return the number of rows in the table.
	 */

	public int size() {
		return this.privateRows.size();
	}		


	/**
	 * Return the name of this table.
	 */
	public String getName() {
		return tableName;
	}

	/**
	 * Returns "TotalPowerTable" followed by the current size of the table 
	 * between parenthesis. 
	 * Example : SpectralWindowTable(12)
	 * @return a String.
	 */
	public String toString() {
		return "TotalPowerTable("+size()+")";
	}

	//
	// ====> Row creation.
	//

	/**
	 * Create a new row with default values .
	 * @return a TotalPowerRow
	 */
	public TotalPowerRow newRow() {
		return new TotalPowerRow(this);
	}

	/**
	 * Add a row.
	 * @param x the TotalPowerRow to be added.
	 *
	 * @return a TotalPowerRow. If the table contains a TotalPowerRow whose attributes (key and mandatory values) are equal to x ones
	 * then returns a pointer on that TotalPowerRow, otherwise returns x.
	 *
	 * @throw DuplicateKey { thrown when the table contains a TotalPowerRow with a key equal to the x one but having
	 * and a value section different from x one }
	 *

	 * @note The row is inserted in the table in such a way that all the rows having the same value of
	 * ( configDescriptionId, fieldId ) are stored by ascending time.
	 */
	public TotalPowerRow add(TotalPowerRow x) throws DuplicateKey {		 
		/*
		 * Is there already a context for this combination of not temporal 
		 * attributes ?
		 */
		String k = Key(
				x.getConfigDescriptionId()
				,
				x.getFieldId()
		);

		Vector<TotalPowerRow> v = null;
		if ((v = context.get(k)) == null) {
			// There is not yet a context ...
			// Create and initialize an entry in the context map for this combination....
			v = new Vector<TotalPowerRow>();
			context.put(k, v);	
		}

		TotalPowerRow result = null;
		try {
			result = insertByTime(x, v);
		}
		catch (DuplicateKey e) {
			throw e; // Simply rethrow it
		}
		return result;
	}	

	/**
	 * Add an array of rows.
	 * @throws DuplicateKey  Thrown if any new row has a key that is already in the table.
	 * @param x The array of rows to be added.
	 */
	public void add(TotalPowerRow[] x) throws DuplicateKey {
		for (int i = 0; i < x.length; ++i) {
			add(x[i]);
		}
	}





	/**
	 * Create a new row initialized to the specified values.
	 * (the autoincrementable attribute if any is not in the parameter list)
	 * @return the new initialized row.

	 * @param configDescriptionId. 

	 * @param fieldId. 

	 * @param time. 

	 * @param execBlockId. 

	 * @param stateId. 

	 * @param scanNumber. 

	 * @param subscanNumber. 

	 * @param integrationNumber. 

	 * @param uvw. 

	 * @param exposure. 

	 * @param timeCentroid. 

	 * @param floatData. 

	 * @param flagAnt. 

	 * @param flagPol. 

	 * @param flagRow. 

	 * @param interval. 

	 */
	public TotalPowerRow newRow(Tag configDescriptionId, Tag fieldId, ArrayTime time, Tag execBlockId, Tag[] stateId, int scanNumber, int subscanNumber, int integrationNumber, Length[][] uvw, Interval[][] exposure, ArrayTime[][] timeCentroid, float[][][] floatData, int[] flagAnt, int[][] flagPol, boolean flagRow, Interval interval) {
		TotalPowerRow row = new TotalPowerRow(this);
		try {

			row.setConfigDescriptionId(configDescriptionId);

			row.setFieldId(fieldId);

			row.setTime(time);

			row.setExecBlockId(execBlockId);

			row.setStateId(stateId);

			row.setScanNumber(scanNumber);

			row.setSubscanNumber(subscanNumber);

			row.setIntegrationNumber(integrationNumber);

			row.setUvw(uvw);

			row.setExposure(exposure);

			row.setTimeCentroid(timeCentroid);

			row.setFloatData(floatData);

			row.setFlagAnt(flagAnt);

			row.setFlagPol(flagPol);

			row.setFlagRow(flagRow);

			row.setInterval(interval);

		}
		catch (Exception e) {
			// Never happens
		}		

		return row;
	}


	/**
	 * Create a new row using a copy constructor mechanism.
	 * 
	 * The method creates a new TotalPowerRow owned by this. Each attribute of the created row 
	 * is a (deep) copy of the corresponding attribute of row. The method does not add 
	 * the created row to this, its simply parents it to this, a call to the add method
	 * has to be done in order to get the row added (very likely after having modified
	 * some of its attributes).
	 * If row is null then the method returns a new TotalPowerRow with default values for its attributes. 
	 *
	 * @param row the row which is to be copied.
	 */
	public TotalPowerRow newRow(TotalPowerRow row) {
		return new TotalPowerRow(this, row);
	}


//	====> Append a row to its table.


	// 
	// A private method to append a row to its table, used by input conversion
	// methods.
	//


	/**
	 * If this table has an autoincrementable attribute then check if *x verifies the rule of uniqueness and throw exception if not.
	 * Check if *x verifies the key uniqueness rule and throw an exception if not.
	 * Append x to its table.
	 * @param x a pointer on the row to be appended.
	 * @returns a pointer on x.
	 */
	// 
	// A private method to append a row to its table, used by input conversion
	// methods.
	//






	private TotalPowerRow checkAndAdd(TotalPowerRow x) throws DuplicateKey {
		String k = Key( 
				x.getConfigDescriptionId() 
				, 
				x.getFieldId() 
		);

		Vector<TotalPowerRow> v = null;
		Object o = context.get(k);
		if (o == null) {
			v = new Vector<TotalPowerRow>();
			context.put(k, v);
		}			
		return insertByTime(x,  context.get(k));
	}







	//
	// ====> Methods returning rows.
	//	
	/**
	 * Get all rows.
	 * @return Alls rows as an array of TotalPowerRow
	 */
	public TotalPowerRow[] get() {
		TotalPowerRow[] result = new TotalPowerRow[privateRows.size()];
		result = (TotalPowerRow[]) privateRows.toArray(result);
		return result;
	}




	/**
	 * Returns a TotalPowerRow given a key.
	 * @return the row having the key whose values are passed as parameters, or null if
	 * no row exists for that key.

	 * @param configDescriptionId. 

	 * @param fieldId. 

	 * @param time. 

	 *
	 */
	public TotalPowerRow getRowByKey(Tag configDescriptionId, Tag fieldId, ArrayTime time)  {
		String keystr = Key(configDescriptionId, fieldId);
		
		Vector<TotalPowerRow> row = null;
		if ((row =  context.get(keystr)) == null ) return null;
		
		// Is the vector empty ...impossible in principle !
		if (row.size() == 0) return null;
		
		// Only one element in the vector
		if (row.size() == 1) {
			TotalPowerRow r = row.get(0);
			if ( time.get() ==r.getTime().get())
				return r;
			else
				return null;
		}
		
		// Optimizations
		TotalPowerRow last =  row.get(row.size()-1);
		if (time.get() > last.getTime().get())return null;
		TotalPowerRow first =  row.get(0);
		if (time.get() < first.getTime().get()) return null;
		
		// More than one row
		// let's use a dichotomy method for the general case..
		int k0 = 0;
		int k1 = row.size() - 1;
		TotalPowerRow r = null;
		while (k0 != k1) {
			r =  row.get(k0);
			if (time.get() == r.getTime().get()) return r;
			
			r =  row.get(k1);
			if (time.get() == r.getTime().get()) return r;
			
			r =  row.get((k0+k1)/2);
			if (time.get() <= r.getTime().get())
				k1 = (k0 + k1) / 2;
			else
				k0 = (k0 + k1) / 2;
		}
		return null;
	}

	/**
	 * Returns all the rows sorted by ascending time for a given context. 
	 * The context is defined by a value of ( configDescriptionId, fieldId ).
	 *
	 * @return an array of TotalPowerRow. A null returned value means that the table contains
	 * no TotalPowerRow for the given ( configDescriptionId, fieldId ).
	 */
	public TotalPowerRow[] getByContext(Tag configDescriptionId, Tag fieldId) {

		String k = Key(configDescriptionId, fieldId);
		Vector<TotalPowerRow> v = null;

		TotalPowerRow[] result = null;
		if ((v =  context.get(k)) == null) return null;
		else {
			result = new TotalPowerRow[v.size()];
			return (TotalPowerRow[]) v.toArray(result);
		}
	}	

	/**
	 * Insert a TotalPowerRow in a vector of TotalPowerRow so that it's ordered by ascending  time.
	 *
	 * @param TotalPowerRow  . The row to be inserted.
	 * @param Vector row. The vector where to insert x.
	 *
	 */
	private TotalPowerRow insertByTime(TotalPowerRow x, Vector<TotalPowerRow> row) throws DuplicateKey {		

		ArrayTime start = x.getTime();

		// Is the row vector empty ?
		if (row.size() == 0) {
			row.add(x);
			privateRows.add(x);
			x.isAdded();
			return x;
		}

		// Optimization for the case of insertion by ascending time.
		TotalPowerRow last =  row.get(row.size()-1);

		if ( start.get() > last.getTime().get() ) {
			row.add(x);
			privateRows.add(x);
			x.isAdded();
			return x;
		}

		// Optimization for the case of insertion by descending time.
		TotalPowerRow first = row.get(0);

		if ( start.get() < first.getTime().get() ) {
			row.insertElementAt(x, 0);
			privateRows.add(x);
			x.isAdded();
			return x;
		} 

		// Case where x has to be inserted inside row; let's use a dichotomy
		// method to find the insertion index.

		int k0 = 0;
		int k1 = row.size() - 1;    	    	

		while (k0 != (k1 - 1)) {
			if (start.get() == (row.get(k0)).getTime().get()) {
				if ((row.get(k0)).equalByRequiredValue(x))
					return (row.get(k0));
				else
					throw new DuplicateKey("DuplicateKey exception in ", "TotalPowerTable");	
			}
			else if (start.get() == (row.get(k1)).getTime().get()) {
				if ((row.get(k1)).equalByRequiredValue(x))
					return (row.get(k1));
				else
					throw new DuplicateKey("DuplicateKey exception in ", "TotalPowerTable");	
			}
			else {
				if (start.get() <= (row.get((k0+k1)/2)).getTime().get())
					k1 = (k0 + k1) / 2;
				else
					k0 = (k0 + k1) / 2;				
			} 	
		}
		row.insertElementAt(x, k1);
		privateRows.add(x);
		x.isAdded();
		return x;   			
	}

	/**
	 * Look up the table for a row whose all attributes 
	 * are equal to the corresponding parameters of the method.
	 * @return this row if any, null otherwise.
	 *

	 * @param configDescriptionId.

	 * @param fieldId.

	 * @param time.

	 * @param execBlockId.

	 * @param stateId.

	 * @param scanNumber.

	 * @param subscanNumber.

	 * @param integrationNumber.

	 * @param uvw.

	 * @param exposure.

	 * @param timeCentroid.

	 * @param floatData.

	 * @param flagAnt.

	 * @param flagPol.

	 * @param flagRow.

	 * @param interval.

	 */
	public TotalPowerRow lookup(Tag configDescriptionId, Tag fieldId, ArrayTime time, Tag execBlockId, Tag[] stateId, int scanNumber, int subscanNumber, int integrationNumber, Length[][] uvw, Interval[][] exposure, ArrayTime[][] timeCentroid, float[][][] floatData, int[] flagAnt, int[][] flagPol, boolean flagRow, Interval interval) {
		for (int i = 0; i < size(); i++) 
			if (((TotalPowerRow)row.get(i)).compareNoAutoInc(configDescriptionId, fieldId, time, execBlockId, stateId, scanNumber, subscanNumber, integrationNumber, uvw, exposure, timeCentroid, floatData, flagAnt, flagPol, flagRow, interval))
				return (TotalPowerRow) row.get(i);

		return null;
	} 	 






	public ASDMRow[] getRows() {
		return (ASDMRow[]) get();
	}



	// 
	// ====> Conversion Methods
	//
	/**
	 * Convert this table into a TotalPowerTableIDL CORBA structure.
	 *
	 * @return a TotalPowerTableIDL
	 */
	public TotalPowerTableIDL toIDL() {
		TotalPowerTableIDL x = new TotalPowerTableIDL ();
		TotalPowerRow[] v = get();
		x.row = new TotalPowerRowIDL[v.length];
		for (int i = 0; i < v.length; ++i) {
			x.row[i] = v[i].toIDL();
		}
		return x;
	}

	/**
	 * Populate this table from the content of a TotalPowerTableIDL Corba structure.
	 *
	 * @throws DuplicateKey Thrown if the method tries to add a row having a key that is already in the table.
	 * @throws ConversionException
	 */
	public void fromIDL(TotalPowerTableIDL x) throws DuplicateKey,ConversionException  {
		TotalPowerRow tmp = null;
		for (int i = 0; i < x.row.length; ++i) {
			tmp = newRow();
			tmp.setFromIDL(x.row[i]);
			// checkAndAdd(tmp);
			add(tmp);
		}
	}

	/**
	 * To be implemented
	 */
	public byte[] toFITS() throws ConversionException {
		// TODO 
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromFITS(byte[] fits) throws ConversionException {
		// TODO 
	}

	/**
	 * To be implemented
	 */
	public String toVOTable() throws ConversionException {
		// TODO 
		return null;
	}

	/**
	 * To be implemented
	 */
	public void fromVOTable(String vo) throws ConversionException {
		// TODO 
	}


	/**
	 * Translate this table to an XML representation conform
	 * to the schema defined for TotalPower (TotalPowerTable.xsd).
	 *
	 * @returns a string containing the XML representation.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> ");
		buf.append("<?xml-stylesheet type=\"text/xsl\" href=\"../asdm2html/table2html.xsl\"?> ");
		buf.append("<TotalPowerTable xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../idl/TotalPowerTable.xsd\"> ");
		//buf.append("<TotalPowerTable>");
		buf.append(entity.toXML());
		String s = container.getEntity().toXML();
		// Change the "Entity" tag to "ContainerEntity".
		buf.append("<Container" + s.substring(1));

		for (TotalPowerRow row : privateRows) {
			buf.append(row.toXML());
			buf.append(" ");
		}
		buf.append("</TotalPowerTable>");
		return buf.toString();
	}

	/**
	 * Populate this table from the content of a XML document that is required to
	 * be conform to the XML schema defined for a TotalPower (TotalPowerTable.xsd).
	 * @throws ConversionException
	 */
	public void fromXML(String xmlDoc) throws  ConversionException{
		Parser xml = new Parser(xmlDoc);
		if (!xml.isStr("<TotalPowerTable")) 
			error();
		String s = xml.getElement("<Entity","/>");
		if (s == null) 
			error();
		Entity e = new Entity();
		e.setFromXML(s);
		if (!e.getEntityTypeName().equals("TotalPowerTable"))
			error();
		setEntity(e);
		// Skip the container's entity; but, it has to be there.
		s = xml.getElement("<ContainerEntity","/>");
		if (s == null) 
			error();

		// Get each row in the table.
		s = xml.getElementContent("<row>","</row>");
		TotalPowerRow row = null;
		while (s != null) {
			row = newRow();
			row.setFromXML(s);
			try {
				checkAndAdd(row);
			} catch (DuplicateKey e1) {
				throw new ConversionException(e1.toString(),"TotalPowerTable");
			}  			
			s = xml.getElementContent("<row>","</row>");
		}
		if (!xml.isStr("</TotalPowerTable>")) 
			error();
	}

	private void error() throws ConversionException {
		throw new ConversionException("Invalid xml document","TotalPower");
	}
 	/**
	  * Serialize this into a stream of bytes and encapsulates that stream into a MIME message.
	  * @returns an  array of bytes containing the MIME message.
	  * 
	  */
	public byte[] toMIME() throws ConversionException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);

		String UID = this.getEntity().getEntityId().toString();
		String execBlockUID = this.getContainer().getEntity().getEntityId().toString();
		try {
			// The XML Header part.
			dos.writeBytes("MIME-Version: 1.0");
			dos.writeBytes("\n");
			dos
			.writeBytes("Content-Type: Multipart/Related; boundary='MIME_boundary'; type='text/xml'; start= '<header.xml>'");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Description: Correlator");
			dos.writeBytes("\n");
			dos.writeBytes("alma-uid:" + UID);
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The MIME XML part header.
			dos.writeBytes("--MIME_boundary");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Type: text/xml; charset='ISO-8859-1'");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Transfer-Encoding: 8bit");
			dos.writeBytes("\n");
			dos.writeBytes("Content-ID: <header.xml>");
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The MIME XML part content.
			dos.writeBytes("<?xml version='1.0'  encoding='ISO-8859-1'?>");
			dos.writeBytes("\n");
			dos
			.writeBytes("<ASDMBinaryTable  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'  xsi:noNamespaceSchemaLocation='ASDMBinaryTable.xsd' ID='None'  version='1.0'>\n");
			dos.writeBytes("<ExecBlockUID>\n");
			dos.writeBytes(execBlockUID + "\n");
			dos.writeBytes("</ExecBlockUID>\n");
			dos.writeBytes("</ASDMBinaryTable>\n");

			// The MIME binary part header
			dos.writeBytes("--MIME_boundary");
			dos.writeBytes("\n");
			dos.writeBytes("Content-Type: binary/octet-stream");
			dos.writeBytes("\n");
			dos.writeBytes("Content-ID: <content.bin>");
			dos.writeBytes("\n");
			dos.writeBytes("\n");

			// The binary part.
			entity.toBin(dos);
			container.getEntity().toBin(dos);
			dos.writeInt(size());
			for (TotalPowerRow row: privateRows) row.toBin(dos);
			
			// The closing MIME boundary
			dos.writeBytes("\n--MIME_boundary--");
			dos.writeBytes("\n");
		} catch (IOException e) {
			throw new ConversionException(
					"Error while reading binary data , the message was "
					+ e.getMessage(), "TotalPower");
		}
		return bos.toByteArray();		
	}




	static private boolean binaryPartFound(DataInputStream dis, String s, int pos) throws IOException {
		int posl = pos;
		int count = 0;
		dis.mark(1000000);
		try {
			while (dis.readByte() != s.charAt(posl)){
				count ++;
			}
		}
		catch (EOFException e) {
			return false;
		}

		if (posl == (s.length() - 1)) return true;

		if (pos == 0) {
			posl++;
			return binaryPartFound(dis, s, posl);
		}
		else {
			if (count > 0) { dis.reset();  return binaryPartFound(dis, s, 0) ; }
			else {
				posl++;
				return binaryPartFound(dis, s, posl);
			}
		}
	}

	 /** 
	   * Extracts the binary part of a MIME message and deserialize its content
		 * to fill this with the result of the deserialization. 
		 * @param mimeMsg the array of bytes containing the MIME message.
		 * @throws ConversionException
		 */
	public void setFromMIME(byte[] data) throws ConversionException {
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		DataInputStream dis = new DataInputStream(bis);

		String terminator = "Content-Type: binary/octet-stream\nContent-ID: <content.bin>\n\n";

		entity = null;
		try {
			if (binaryPartFound(dis, terminator, 0) == false) {
				throw new ConversionException ("Failed to detect the beginning of the binary part", "TotalPower");
			}
			
			entity = Entity.fromBin(dis);
			Entity containerEntity = Entity.fromBin(dis);

			int numRows = dis.readInt();
			for (int i = 0; i < numRows; i++) {
				this.checkAndAdd(TotalPowerRow.fromBin(dis, this));
			}
		} catch (IOException e) {
			throw new ConversionException(
					"Error while reading binary data , the message was "
					+ e.getMessage(), "TotalPower");
		} catch (TagFormatException e) {
			throw new ConversionException( "Error while reading binary data , the message was "
					+ e.getMessage(), "TotalPower");
		}catch (DuplicateKey e) {
			throw new ConversionException(
					"Error while reading binary data , the message was "
					+ e.getMessage(), "TotalPower");
		}
	}


	
	/**
	  * Stores a representation (binary or XML) of this table into a file.
	  *
	  * Depending on the boolean value of its private field fileAsBin a binary serialization  of this (fileAsBin==true)  
	  * will be saved in a file "TotalPowerTable.bin" or an XML representation (fileAsBin==false) will be saved in a file "TotalPowerTable.xml".
	  * The file is always written in a directory whose name is passed as a parameter.
	  * @param directory The name of directory  where the file containing the table's representation will be saved.
	  * @throws ConversionException
	  */
	  public void toFile(String directory) throws ConversionException {
	  	// Check if the directory exists
	  	File directoryFile = new File(directory);
	  	if (directoryFile.exists() && !directoryFile.isDirectory())
	  		throw new ConversionException("Cannot write into directory " 
	  			+  directoryFile.getAbsolutePath() 
	  			+ ". This file already exists and is not a directory", "TotalPowerTable");
	  			
	  	//if not let's create it.		
	  	if (!directoryFile.exists()) {
	  		if (!directoryFile.mkdir())
	  			throw new ConversionException("Could not create directory " + directoryFile.getAbsolutePath(), "TotalPowerTable");
	  	}
	  		
	  	String fileName = null;
	  	BufferedWriter out = null;
	  	if (fileAsBin) {
	  		// write the bin serialized.
	  		fileName = directory+"/TotalPower.bin";
	  	}
	  	else {
	  		// write the XML
	  		fileName = directory+"/TotalPower.xml";
	  	}
	  	
	  	File file = new File(fileName);
	  	
	  	// Delete the file if it already exists
	  	// We assume here that 'file'  does not denote  a directory. The probability
	  	// for that is very low; a perfect code would take care of this possibility.
	  	if (file.exists()) {
	  			if (!file.delete())
	  				throw new ConversionException("Problem while trying to delete a previous version of '"+file.toString()+"'", "TotalPower"); 
	  	}	  	
	  	
	  	if (fileAsBin) {
	  		OutputStream os;
			try {
				os = new FileOutputStream(file);
				os.write(toMIME());
				os.close();
			} catch (FileNotFoundException e) {
	  			throw new ConversionException("Problem while writing the binary representation, the message was : " + e.getMessage(), "TotalPower");
			} catch (IOException e) {
	  			throw new ConversionException("Problem while writing the binary representation, the message was : " + e.getMessage(), "TotalPower");
			}	
	  	}
	  	else {
	  		try {
	  			out = new BufferedWriter(new FileWriter(file));
	  			out.write(toXML());
	  			out.close();
	  		}
	  		catch (IOException e) {
	  			throw new ConversionException("Problem while writing the XML representation, the message was : " + e.getMessage(), "TotalPowerTable");
	  		}
	  	}
	  }
	  
	/**
	 * Reads and parses a file containing a representation of a TotalPowerTable as those produced  by the toFile method.
	 * This table is populated with the result of the parsing.
	 * @param directory The name of the directory containing the file te be read and parsed.
	 * @throws ConversionException If any error occurs while reading the 
	 * files in the directory or parsing them.
	 *
	 */
	 public void setFromFile(String directory) throws ConversionException {
	 	 	File directoryFile = new File(directory);

			if (!directoryFile.isDirectory())
				throw new ConversionException ("Directory " + directory + " does not exist.", "TotalPower");
				
			String fileName = null;
			
			if ( fileAsBin) 
				fileName = directory + "/TotalPower.bin";
			else 
				fileName  = directory + "/TotalPower.xml";
			
			File file = new File(fileName);
				
			if (!file.exists())
				throw new ConversionException ("File " + fileName + " does not exists", "TotalPower");
				
			if (fileAsBin) {
				byte[] bytes = null;
       			try {
					InputStream is = new FileInputStream(file);
					long length = file.length();
					if (length > Integer.MAX_VALUE) 
						throw new ConversionException ("File " + fileName + " is too large", "TotalPower");
				
					bytes = new byte[(int)length];
					int offset = 0;
        			int numRead = 0;
 
        			while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            			offset += numRead;
        			}
        		
        			if (offset < bytes.length) {
            			throw new ConversionException("Could not completely read file "+file.getName(), "TotalPower");
            		}
            		is.close();
            	}
        		catch (IOException e) {
        			throw new ConversionException("Error while reading "+file.getName()+". The message was " + e.getMessage(),
        																	  "TotalPower");
        		}	
        		
            	setFromMIME(bytes);
			}
			else {
				try {
 					BufferedReader in = new BufferedReader(new FileReader(file));	
 					StringBuffer xmlDoc = new StringBuffer();
 					String line = in.readLine();
					while (line != null) {
						xmlDoc.append(line);
						line = in.readLine();
					}
					in.close();
					fromXML(xmlDoc.toString());
				}
				 catch (IOException e) {
					throw new ConversionException(e.getMessage(), "TotalPower");
				}	
			}  	
	 }
	 
	/**
	 * Returns the table's entity.
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * Set the table's entity
	 *
	 * @param e An entity. 
	 */
	public void setEntity(Entity e) {
		this.entity = e; 
	}

	/**
	 * Store this table into the archive.
	 * @param ar the archiver in charge of the archiving.
	 * @returns the UID assigned to the archived table.
	 */
	String toArchive(Archiver ar) throws ConversionException, ArchiverException{
		String retUID = null;
		Entity ent = null;

		try {
			retUID = ar.getID();
			ent = this.getEntity();
			ent.setEntityId(new EntityId(retUID));
			this.setEntity(ent);

			if (archiveAsBin) {
				byte[] asBin = this.toMIME();
				ar.binStore(retUID, asBin);
			} else {
				String xmlTable = this.toXML();
				ar.store(xmlTable, "TotalPowerTable", retUID);
			}
		} catch (ArchiveException e) {
			throw new ArchiverException(
					"Archiver exception while archiving a TotalPowerTable. Message was "
					+ e.getMessage());
		} catch (NotAvailable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUID;
	}

	/**
	 * Retrieve this table from the archive.
	 * @param ar the archiver in charge of the archiving.
	 */
	public void fromArchive(Archiver ar, String UID) throws ConversionException, ArchiverException {
		try {
			if (archiveAsBin) {
				this.setFromMIME(ar.binRetrieve(UID));
			}
			else {
				this.fromXML(ar.retrieve(UID));
			}
		}
		catch (ArchiveException e) {
			throw new ArchiverException("Archiver exception while retrieving a TotalPowerTable with UID " + UID +". The message was " + e.getMessage());
		}
	}
}



