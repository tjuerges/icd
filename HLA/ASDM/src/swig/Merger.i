%module Merger
%import "ASDM.entity.i"

%{
#include "Merger.h"
%}

namespace asdm {
  class Merger {
  public:
    Merger();
    virtual ~Merger();
    void merge(asdm::ASDM* ds1, asdm::ASDM* ds2);
  };
}
