#!/bin/env python
# -*- coding: UTF-8 -*-
import getopt, math, StringIO

from asdm import *
from pylab import *

def usage():
    print """plotAntenna.py path-to-ASDM
    Plot the positions of the antennas recorded in the Antenna table of an ASDM.
    Parameter:
    path-to-ASDM : the path to the ASDM exported on disk.
    """


def toDMS(r) :
    secondsOfArc = 180 * 3600 * r / math.pi
    s = math.fmod(secondsOfArc, 60)
    minutesOfArc = math.floor((secondsOfArc - s)/60)
    m = math.fmod(minutesOfArc, 60)
    d = math.floor((minutesOfArc-m)/60)
    return [d, m, s]

def PHI(X, Y, Z, e, a, phi) :
	return math.atan((Z + e * e * math.sin(phi) * a / math.sqrt(1 - e * e * math.sin(phi) * math.sin(phi)))/math.sqrt(X*X + Y*Y))
    
def geocentricToGeographic(position) :
    X, Y, Z = map(Length.get, position)
    e = 0.00335281317
    a = 6378136.46

    epsilon = 1.e-10
    nitermax = 10
    phi0 = 0.
    phi1 = PHI(X, Y, Z, e, a, phi0)
    niter = 0
    while (math.fabs(phi1 - phi0) > epsilon) and (niter < nitermax) :
        phi0 = phi1
        phi1 = PHI(X, Y, Z, e, a, phi0)
        niter = niter + 1

    if niter > nitermax :
        print "phi could not be calculated in less than ", nitermax, " iterations."

    return (phi1, math.atan2(Y, X))


def latlongText(latlongInRadian) :
    lat , lon = latlongInRadian
    if lat < 0:
        latOrientation = "-"
        lat = -lat
    else :
        latOrientation = "+"
        
    output = StringIO.StringIO()
    d, m, s = toDMS(lat)
    print >> output, "%s%02.0f°%02.0f'%5.2f\"" % (latOrientation, d, m, s),
    latitude = output.getvalue()
    output.close()

    if lon < 0 :
        longOrientation = "-"
        lon = -lon
    else:
        longOrientation = "+"

    output = StringIO.StringIO()
    d, m, s = toDMS(lon)
    print >> output, "%s%03.0f°%02.0f'%5.2f\"" % (longOrientation, d, m, s),
    longitude = output.getvalue()
    output.close()

    return latitude + "," + longitude
    
def stationXCoords(stations) :
    return map(Length.get, map(lambda x: x[0], map(StationRow.position, stations)))


def stationYCoords(stations) :
    return map(Length.get, map(lambda x: x[1], map(StationRow.position, stations)))


def stationNames(stations) :
    """ Return the names of the stations
    """
    return map(StationRow.name, stations)


def plotStations(stations, dsPath):
    #
    # prepare the graphical environment
    #
    fig = figure()
    myPlot = fig.add_subplot(111)
    myPlot.grid(True)
    title('Stations positions of \n' + dsPath)
    xlabel('xPosition (m)')
    ylabel('yPosition (m)')

    #
    # plot ANTENNA_PADs.
    #
    antennaPads = filter(lambda row: row.type() == StationType.ANTENNA_PAD(), stations)
    myPlot.plot(stationXCoords(antennaPads), stationYCoords(antennaPads), 'o')
    
    map(lambda name, x, y: annotate(name, xy=(x,y), xytext=(0, 10), textcoords='offset points', horizontalalignment='center'),
        stationNames(antennaPads),
        stationXCoords(antennaPads),
        stationYCoords(antennaPads))

    #
    # plot WEATHER STATIONs.
    #
    weatherStationPads = filter(lambda row: row.type() == StationType.WEATHER_STATION(), stations)
    myPlot.plot(stationXCoords(weatherStationPads), stationYCoords(weatherStationPads), 'v')

    map(lambda name, x, y: annotate(name, xy=(x,y),  xytext=(0, 10), textcoords='offset points', horizontalalignment='center'),
        stationNames(weatherStationPads),
        stationXCoords(weatherStationPads),
        stationYCoords(weatherStationPads))

    #
    #
    #
    show()

def geographicPositions(antennaPads) :
    return map (geocentricToGeographic, map (StationRow.position, antennaPads))


def pairs(l) :
    if len(l) < 2:
        return []
    else :
        return (map (lambda e : [l[0], e], l[1:])) + pairs(l[1:])

def distance(pairOfLatLong) :
    latLong1 , latLong2 = pairOfLatLong
    lat1, long1 = latLong1
    lat2, long2 = latLong2

    return math.acos(math.cos(lat1)*math.cos(long1)*math.cos(lat2)*math.cos(long2) + math.cos(lat1)*math.sin(long1)*math.cos(lat2)*math.sin(long2) + math.sin(lat1)*math.sin(lat2))/180 * math.pi * (6378137.0 + 3000.0)
    
def main():
    """ The main function
    """
    #
    # The path to the ASDM is expected in argv[1]
    #
    try:
        options, arg = getopt.getopt(sys.argv[1:], ":akh:")
    except getopt.GetoptError, err:
            usage();
            sys.exit(2)

    if len(arg) == 0 :
        usage()
        sys.exit(2)

    dsPath = arg[0]
    ds = ASDM()

    try:
        print 'Loading ', dsPath
        ds.setFromFile(dsPath)
    except ConversionException, e:  # Note that  ConversionException class is known
        print e.getMessage()
        sys.exit (1)


    antennaPads = filter(lambda row: row.type() == StationType.ANTENNA_PAD(), ds.stationTable().get())
    listOfGeographicPositions = geographicPositions(antennaPads)
    for name, geocentric, geographic in map (lambda name, geocentric, geographic : [name, geocentric, geographic], map(StationRow.name, antennaPads), map(StationRow.position, antennaPads), map (latlongText, listOfGeographicPositions)):
        print name , " ->", geocentric , " -> ", geographic

    # for pairOfLatLong in pairs(listOfGeographicPositions):
    #     print "The distance from " , latlongText(pairOfLatLong[0]), " to ", latlongText(pairOfLatLong[1]) , " is " , distance(pairOfLatLong), "m."

    plotStations(antennaPads, dsPath)
    
#
# The main entry point.
#
if __name__ == "__main__":
    main()
