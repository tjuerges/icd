%include "std_vector.i"
%include "std_string.i"
%include "std_complex.i"

%{
#include "Angle.h"
#include "AngularRate.h"
#include "ArrayTime.h"
#include "ArrayTimeInterval.h"
#include "Interval.h"
#include "Boolean.h"
#include "Byte.h"
#include "Character.h"
#include "Complex.h"
#include "Double.h"
#include "Entity.h"
#include "EntityRef.h"
#include "Float.h"
#include "Flux.h"
#include "Frequency.h"
#include "Humidity.h"
#include "Integer.h"
#include "Length.h"
#include "Long.h"
#include "Pressure.h"
#include "Short.h"
#include "Speed.h"
#include "TagType.h"
#include "Tag.h"
#include "Temperature.h"

#include "ConversionException.h"
#include "IllegalAccessException.h"
#include "InvalidArgumentException.h"
#include "InvalidDataException.h"
#include "NumberFormatException.h"
#include "OutOfBoundsException.h"
#include "TagFormatException.h"
#include "UniquenessViolationException.h"

#include <stdint.h>
%}


//
// This will help SWIG to understand what is int64_t
//
%include "stdint.i"

%define EXCEPTION(name)
namespace asdm {
  class name {
  public:
	name();
	virtual ~name();
	std::string getMessage() const;    
  };
}
%enddef

EXCEPTION(ConversionException);
EXCEPTION(IllegalAccessException);
EXCEPTION(InvalidArgumentException);
EXCEPTION(InvalidDataException);
EXCEPTION(NumberFormatException);
EXCEPTION(OutOfBoundsException);
EXCEPTION(TagFormatException);
EXCEPTION(UniquenessViolationException);
 
using namespace std;

%define PHYTYPE(typeName)
  class typeName {
  public:
    typeName();
    typeName(const typeName& a);
    typeName(double value);

    virtual ~typeName();
    double get() const;
    string unit() const;
    
    %extend{
	char *__str__() {
		static char temp[256];
		sprintf(temp,"%e%s", $self->get(), $self->unit().c_str());
		return &temp[0];
	}
	
	char *__repr__() {
		static char temp[256];
		sprintf(temp,"%e%s", $self->get(), $self->unit().c_str());
		return &temp[0];
	}
    }
  };
%enddef

namespace asdm {	
  class Interval {
  public:
 	Interval();                              		// default constructor
	Interval(const Interval &);   
	virtual ~Interval();
	Interval(int64_t value);
	int64_t get() const;
    %extend {
	  char *__str__() {
	    static char temp[128];
	    sprintf(temp, "%fs", $self->get()/1000000000.);
	    return &temp[0];
	  }

	  char *__repr__() {
	    static char temp[128];
	    sprintf(temp, "%fs", $self->get()/1000000000.);
	    return &temp[0];
	  }
    }
  };

  class ArrayTime : public Interval {
  public:
    // Useful constants
//     const static int numberSigDigitsInASecond 					= 9;
//     const static int64_t unitsInASecond 						= 1000000000LL;
//     const static int64_t unitsInADayL 						= 86400000000000LL;
//     const static double unitsInADay ;	
//     const static double unitsInADayDiv100 ;		
//     const static double julianDayOfBase ;	
//     const static int64_t julianDayOfBaseInUnitsInADayDiv100 	= 2073600432000000000LL;  
    
    
    ArrayTime(); 
    ArrayTime(const ArrayTime &t);   

    ArrayTime(int year, int month, double day); 
    ArrayTime(int year, int month, int day, int hour, int minute, double second);
    ArrayTime(double modifiedJulianDay);
    ArrayTime(int modifiedJulianDay, double secondsInADay);
    ArrayTime(int64_t nanoseconds); 
    
    double getJD() const; 
    double getMJD() const; 
 
    std::string toFITS() const;
    %extend {
      char *__str__() {
	static char temp[256];
	sprintf(temp, "%s", $self->toFITS().c_str());
	return &temp[0];
      }

      char *__repr__() {
	static char temp[256];
	sprintf(temp, "%s", $self->toFITS().c_str());
	return &temp[0];
      }
    }  
    
  };

  class ArrayTimeInterval {
  public:
    ArrayTimeInterval();
    ArrayTimeInterval(ArrayTime  start,
		      Interval  duration);

    ArrayTimeInterval(int64_t startInNanoSeconds,
		      int64_t durationInNanoSeconds);		  				  
    
    ArrayTimeInterval(ArrayTime start);
    ArrayTimeInterval(double startInMJD);
    ArrayTimeInterval(int64_t startInNanoSeconds);

    %extend {
      ArrayTime start() { return $self->getStart(); }
      Interval duration() { return $self->getDuration(); }
      char* __str__() {
	static char temp[256];
	sprintf(temp, "start=%s, duration=%f", $self->getStart().toFITS().c_str(), $self->getDuration().get()/1000000000.); 
	return &temp[0];
      }

      char* __repr__() {
	static char temp[256];
	sprintf(temp, "start=%s, duration=%f", $self->getStart().toFITS().c_str(), $self->getDuration().get()/1000000000.); 
	return &temp[0];
      }
    }
  };


  class Boolean {
  public:
    static bool parseBoolean(const std::string& s);
    static std::string toString(bool b);
  };
  
  class Byte {
    
  public:
    static char parseByte(const std::string &s);
    static std::string toString(char c);
    static const char MIN_VALUE;
    static const char MAX_VALUE;
  };

  class Character {    
  public:
    static unsigned char parseCharacter(const std::string &s);
    static std::string toString(unsigned char c);
  };

  class Complex : public std::complex<double> {
  public:
    static Complex fromString(const std::string&);
    static Complex getComplex(StringTokenizer &t);
    
    Complex();                              		
    Complex(const Complex &);				
    Complex(const std::string &s);

    Complex(double re, double im);
    
    double getReal() const;
    double getImg() const;
    void setReal(double re);
    void setImg(double im);
    
    bool Complex::isZero() const;
    bool equals(const Complex &) const;  

    std::string toString() const;

    %extend {
      char *__str__() {
 	ostringstream oss;
 	oss << $self->getReal() << "+i" << $self->getImg();
 	return (char *) oss.str().c_str();
      }
      char *__repr__() {
 	ostringstream oss;
 	oss << $self->getReal() << "+i" << $self->getImg();
 	return (char *) oss.str().c_str();
      }
    }
  };	

  class Double {
  public:
    static double parseDouble(const std::string& s) ;
    static std::string toString(double);
    static const double MAX_VALUE;
    static const double MIN_VALUE;
  };


  %nodefaultctor;
  class Entity {
  public:
    string toString() const;
    %extend {
      char *__str__() {
	static char temp[64];
	sprintf(temp, "%s", $self->getEntityId().toString().c_str());
	return &temp[0];
      }
    }
  };
%clearnodefaultctor; 

%nodefaultctor;
  class EntityRef {
  public:
    string toString() const;
    virtual ~EntityRef();
    %extend {
      char *__str__() {
	static char temp[64];
	sprintf(temp, "%s", $self->getEntityId().toString().c_str());
	return &temp[0];
      }
    }
  };
%clearnodefaultctor; 

  class Float {
  public:
    static float parseFloat(const std::string& s) ;
    static std::string toString(float);
    static const float MAX_VALUE;
    static const float MIN_VALUE;
  };

  class Integer {
  public:
    static double parseInt(const std::string& s) ;
    static std::string toString(int);
    static const double MAX_VALUE;
    static const double MIN_VALUE;
  };



  class Long {
  public:
    static double parseLong(const std::string& s) ;
    static std::string toString(long);
    static const double MAX_VALUE;
    static const double MIN_VALUE;
  };




  class Short {
  public:
    static double parseShort(const std::string& s) ;
    static std::string toString(short);
    static const double MAX_VALUE;
    static const double MIN_VALUE;
  };


%define TAGTYPE(_t_) 
%extend {
  static const asdm::TagType* _t_ () {
    return (asdm::TagType:: ## _t_);
  }
};
%enddef

%{
#include "TagType.h"
%}

%nodefaultctor;
%nodefaultdtor;
  class TagType {
  public:
    virtual std::string toString() const;
    static const TagType* getTagType(std::string name);
   
TAGTYPE(NoType)
TAGTYPE(AlmaRadiometer)
TAGTYPE(Antenna)
TAGTYPE(CalData)
TAGTYPE(CalReduction)
TAGTYPE(ConfigDescription)
TAGTYPE(CorrelatorMode)
TAGTYPE(DataDescription)
TAGTYPE(Ephemeris)
TAGTYPE(ExecBlock)
TAGTYPE(Field)
TAGTYPE(FocusModel)
TAGTYPE(Holography)
TAGTYPE(Observation)
TAGTYPE(Polarization)
TAGTYPE(Processor)
TAGTYPE(Receiver)
TAGTYPE(Scale)
TAGTYPE(SpectralWindow)
TAGTYPE(SquareLawDetector)
TAGTYPE(State)
TAGTYPE(Station)
TAGTYPE(SwitchCycle)

    %extend {
      void dummy() { ; }
      char *__str__() {
	static char temp[32];
	sprintf(temp, "%s", $self->toString().c_str());
	return &temp[0];
      }

      char *__repr__() {
	static char temp[32];
	sprintf(temp, "%s", $self->toString().c_str());
	return &temp[0];
      }

      bool __eq__ (TagType* tt) {
	return $self == tt;
      }
    }
  };
%clearnodefaultctor; 
%clearnodefaultdtor;

%{
#include "Tag.h"
%}

  class Tag {
  public:
    Tag();
    Tag(unsigned int i);
    Tag(unsigned int i, const TagType* t) ;
    Tag(const Tag &t);
    static Tag parseTag (string s);
    std::string toString() const;
    virtual ~Tag();
    //    TagType* getTagType(); 
    //    unsigned int getTagValue();
    bool equals(const Tag & t) const;
    bool operator == (const Tag& t) const;
    bool operator != (const Tag& t) const;
    bool isNull() const;
    %extend {
      char *__str__() {
	static char temp[32];
	sprintf(temp, "%s", $self->toString().c_str());
	return &temp[0];
      }

      char *__repr__() {
	static char temp[32];
	sprintf(temp, "%s", $self->toString().c_str());
	return &temp[0];
      }

      int __hash__() {
	static char temp[32];
	sprintf(temp, "%s", $self->toString().c_str());	
	int result = 0;
	for (int i = 0; temp[i] != 0; i++)
	  result += temp[i];
	return result;
      }

      TagType* type() {
	return $self->getTagType();
      }

      unsigned int get() {
	return $self->getTagValue();
      }

    }
  };

PHYTYPE(Angle);
PHYTYPE(AngularRate);
PHYTYPE(Flux);
PHYTYPE(Frequency);
PHYTYPE(Humidity);
PHYTYPE(Length);
PHYTYPE(Pressure);
PHYTYPE(Speed);
PHYTYPE(Temperature);


}	

%define ARR(from, to)
%template(Arr ## to) std::vector<from>;
%template(Arr2 ## to) std::vector< std::vector<from> >;
%template(Arr3 ## to) std::vector<std::vector<std::vector<from> > >;
%template(Arr4 ## to) std::vector<std::vector<std::vector<std::vector<from> > > >;
%enddef


// %define PHYARR(from, to)
// %template(Arr ## to) std::vector<from>;
// %extend std::vector<from>{
//   char *__str__() {
//     static ostringstream oss;
//     oss << "[" ;
//     for (unsigned int i = 0; i < $self->size(); i++)
//       oss << $self->at(i).get() << $self->at(i).unit() << " ";
//     oss << "]";
//     return (char*) oss.str().c_str();
//   }
//   char *__repr__() {
//     static ostringstream oss;
//     oss << "[" ;
//     for (unsigned int i = 0; i < $self->size(); i++)
//       oss << $self->at(i).get() << $self->at(i).unit() << " ";
//     oss << "]";
//     return (char*) oss.str().c_str();
//   }	
// }

// %enddef

ARR(int, Int);
ARR(float, Float);
ARR(double, Double);
ARR(bool, Bool);
ARR(asdm::Complex, Complex);
ARR(std::string, String);
ARR(asdm::Angle, Angle);
ARR(asdm::AngularRate, AngularRate);
ARR(asdm::Interval, Interval);
ARR(asdm::ArrayTime, ArrayTime);
ARR(asdm::ArrayTimeInterval, ArrayTimeInterval);
ARR(asdm::Flux, Flux);
ARR(asdm::Frequency, Frequency);
ARR(asdm::Length, Length);
ARR(asdm::Pressure, Pressure);
ARR(asdm::Speed, Speed);
ARR(asdm::Temperature, Temperature);

ARR(asdm::Tag, Tag);

