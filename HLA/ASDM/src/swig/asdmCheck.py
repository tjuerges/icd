#!/usr/bin/env python
#
# ALMA - Atacama Large Millimeter Array
# (c) European Southern Observatory, 2002
# (c) Associated Universities Inc., 2002
# Copyright by ESO (in the framework of the ALMA collaboration),
# Copyright by AUI (in the framework of the ALMA collaboration),
# All rights reserved.
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# $Id: asdmCheck.py,v 1.16 2011/04/21 09:39:31 mcaillat Exp $
#/
import os, sys, traceback, email, errno, mimetypes, xml.dom.minidom, StringIO, getopt, re, exceptions, operator
from string import *
from tokenize import *
from token import *

    
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# import the module containing the python/C++ wrapper for the asdm classes.
from asdm import *

class ASDMCheckException(exceptions.Exception) :
    def __init__(self, message) :
        self.message = message

    def __str__(self) :
        return self.message

class ASDMCheckIllegalAccessException(exceptions.Exception) :
    def __init__(self, message) :
        self.message = message

    def __str__(self) :
        return self.message


def uniq(seq):
    """ A simple function which returns a collection of the elements without any duplicate. Does not preserve the order.
    """
    set = {}
    map (set.__setitem__, seq, [])
    return set.keys()

def flatten(x):
    """flatten(sequence) -> list

    Returns a single, flat list which contains all elements retrieved
    from the sequence and all recursively contained sub-sequences
    (iterables).

    Examples:
    >>> [1, 2, [3,4], (5,6)]
    [1, 2, [3, 4], (5, 6)]
    >>> flatten([[[1,2,3], (42,None)], [4,5], [6], 7, MyVector(8,9,10)])
    [1, 2, 3, 42, None, 4, 5, 6, 7, 8, 9, 10]"""

    result = []
    for el in x:
        #if isinstance(el, (list, tuple)):
        if hasattr(el, "__iter__") and not isinstance(el, basestring):
            result.extend(flatten(el))
        else:
            result.append(el)
    return result
    
def getTable(name, ds):
    """ Return the table with name passed in argument 'name' (string) of the ASDM dataset passed in argument 'ds'
    """
    try:
        return ds.__class__.__dict__.get(name.strip()) (ds)
    except exceptions.TypeError:
        raise ASDMCheckException("There is no table with the name '"+name+"'")
    
        

def getAttribute(name, row):
    """ Return the attribute of 'row' (expectedly 'row' is a row of an ASDM table) given its 'name'.
    """
    try:
        return row.__class__.__dict__.get(name.strip()) (row)
    except IllegalAccessException, e:
        raise ASDMCheckIllegalAccessException(e.getMessage());
    except  exceptions.TypeError:
        raise ASDMCheckException("There is no attribute with the name '" + name +"'") 

def getForeignRow(ds, row, fkName):
    """ Return  the foreign row to which the 'row' 's attribute with name 'fkName' points to.
    """
    fk = getAttribute(fkName, row)
    fkTypeName = repr(fk.type())
    fkTypeName = fkTypeName[0].lower() + fkTypeName[1:]
    foreignTable = getTable(fkTypeName+"Table", ds)
    return foreignTable.getRowByKey(fk)
#
# simpleFK : a dictionary to define the simple foreign keys.
#
# 
#
# The dictionary is actually a dictionary of dictionaries. The top level
# dictionary (simpleFK) is the 'tables' dictionary and each of its elements
# is the dictionary of the attributes which are foreign key associated with
# the name of the tables they refer to.
#
# * The key of simpleFK is expected to be a string whose value has to be the name of an
# ASDM table expressed with a lower case character in 1st position and without the 'Table' suffix
#
# * The value associated to a key of simpleFK (i.e. to a table's name) is a dictionary structured as follows :
#       + The key is expected to be the name of an attribute of this table, which is a 'foreign key.  
#       + The value associated a key is expected to be a tuple of string. Each string of that tuple
#         is expected to be the name of a table referenced by the 'foreign key', in most of the cases
#         those tuples will be singletons. The only one known case with more than one referenced table is
#         'polOrHoloId' in 'dataDescription' which refers to 'polarization' or 'holography'.
#
simpleFK = {
    'antenna' : {'stationId': ('station',),
                 'assocAntennaId' : ('antenna',)
                 },

    'calAmpli': {'calDataId': ('calData',),
                 'calReductionId' : ('calReduction',)
                 },

    'calAtmosphere': {'calDataId' : ('calData',),
                      'calReductionId' : ('calReduction',)
                      },

    'calBandpass': {'calDataId' : ('calData',),
                    'calReductionId' : ('calReduction',)
                    },

    'calCurve':  {'calDataId' : ('calData',),
                  'calReductionId' : ('calReduction',)
                  },

    'calData': {'assocCalDataId' : ('calData',)
                },

    'calDelay': {'calDataId' : ('calData',),
                 'calReductionId' : ('calReduction',)
                 },

    'calDevice': {'antennaId' : ('antenna',),
                  'spectralWindowId' : ('spectralWindow',)
                  },

    'calFlux': {'calDataId' : ('calData',),
                'calReductionId' : ('calReduction',)
                },

    'calFocusModel': {'calDataId' : ('calData',),
                      'calReductionId' : ('calReduction',)
                      },

    'calFocus': {'calDataId' : ('calData',),
                 'calReductionId' : ('calReduction',)
                 },

    'calGain': {'calDataId' : ('calData',),
                'calReductionId' : ('calReduction',)
                },

    'calHolography': {'calDataId' : ('calData',),
                      'calReductionId' : ('calReduction',)
                      },

    'calPhase': {'calDataId' :  ('calData',),
                 'calReductionId' : ('calReduction',)
                 },

    'calPointingModel': {'calDataId' : ('calData',),
                         'calReductionId' : ('calReduction',)
                         },

    'calPointing': {'calDataId' : ('calData',),
                    'calReductionId' : ('calReduction',)
                    },

    'calPosition': {'calDataId' : ('calData',),
                    'calReductionId' : ('calReduction',)
                    },

    'calPrimaryBeam': {'calDataId' : ('calData',),
                       'calReductionId' : ('calReduction',)
                       },

    'calSeeing': {'calDataId' : ('calData',),
                  'calReductionId' : ('calReduction',)
                  },

    'calWVR': {'calDataId' : ('calData',),
               'calReductionId' : ('calReduction',)
               },

    'configDescription':  {'antennaId' : ('antenna',),
                           'dataDescriptionId' : ('dataDescription',),
                           'processorId' : ('processor',),
                           'switchCycleId' : ('switchCycle',),
                           'assocConfigDescriptionId' : ('configDescription',)
                           },

    'dataDescription' : {'spectralWindowId' :  ('spectralWindow',),
                         'polOrHoloId' : ('polarization', 'holography')
                         },

    'execBlock' : {'antennaId' : ('antenna',),
                   'sBSummaryId' : ('sBSummary',)
                   },

    'feed' : {'antennaId' :  ('antenna',),
              'spectralWindowId' : ('spectralWindow',)
              },

    'field' : {'assocFieldId' : ('field',),
               'ephemerisId' : ('ephemeris',)
               },

    'focus' : {'antennaId' : ('antenna',)
               },

    'freqOffset' : {'antennaId' : ('antenna',),
                    'spectralWindowId' : ('spectralWindow',)
                    },

    'gainTracking' : {'antennaId' : ('antenna',),
                      'spectralWindowId' : ('spectralWindow',)
                      },

    'history' : {'execBlockId' :('execBlock',)
                 },

    'main' : {'configDescriptionId' : ('configDescription',),
              'fieldId' :  ('field',),
              'execBlockId' :('execBlock',),
              'stateId' : ('state',)
              },

    'pointingModel' : {'antennaId' :  ('antenna',)
                       },

    'pointing' : {'antennaId' : ('antenna',)
                  },

    'receiver' : {'spectralWindowId' : ('spectralWindow',)
                  },

    'scan' : {'execBlockId' : ('execBlock',)
              },

    'source' : {'spectralWindowId' : ('spectralWindow',)
                },

    'spectralWindow' : {'imageSpectralWindowId' : ('spectralWindow',),
                        'assocSpectralWindowId' : ('spectralWindow',)
                        },

    'subscan' : {'execBlockId' : ('execBlock',)
                 },

    'sysCal' : {'antennaId' : ('antenna',),
                'spectralWindowId' : ('spectralWindow',)
                },

    'totalPower': {'configDescriptionId' : ('configDescription',),
                   'fieldId' : ('field',),
                   'execBlockId' : ('execBlock',)
                   },

    'weather' : {'stationId' : ('station',)
                 },

    'wVMCal' : {'antennaId' : ('antenna', ),
                'spectralWindowId' : ('spectralWindow',)
                }
    }
        
def checkSimpleFK(referencedTable, fk) :
    """Check that the foreign key(s) 'fk' is (are) resolved in 'referencedTable'
    1) Check that 'fk' is actually a Tag with a type (e.g. Antenna, SpectralWindow...) which corresponds to the type of 'referencedTable'
    2) Then check that 'fk' actually refers to a row in 'referencedTable'

    'fk' is expected to contain wether a Tag or a tuple of Tags.

    """

    #
    # Is fk a tuple ? In such a case, process each of its elements.
    #
    if repr(type(fk)) == "<type 'tuple'>" :
        for tag in fk:
            if not checkSimpleFK(referencedTable, tag) :
                return False
        return True

    #
    # fk is not a tuple.
    #
    else:
        #
        # is fk a Tag ?
        #
        if repr(type(fk)) != "<class 'asdmTypes.Tag'>" :
            print 'checkSimpleFK : the argument "fk" of type "%s" is not a Tag' % type(fk)
            return False

        #
        # What is its TagType ?
        #
        tagType = repr(fk.type())
        m = re.search('\<class \'(\w+)\.(\w+)Table\'\>', repr(type(referencedTable)))
        if m == None :
            print 'checkSimpleFK : the argument "referenced table" of type "%s" is not an ASDM table' % type(referencedTable)
            return False
        #
        # Does its TagType match the table type ?
        #
        if not (tagType == m.group(1)) :
            return False

        #
        # Does fk refer to a row in referencedTable ?
        #
        return referencedTable.getRowByKey(fk) != None

def checkSimpleFKs(ds) :
    """Check that all the simple foreign keys are resolved.
    For all the attributes of the ASDM which are references or arrays of references to some ASDM tables rows
    verify that those referenced rows exist.
    """

    for referencing, fks in simpleFK.iteritems():
        referencingTable = ds.__class__.__dict__.get(referencing+'Table') (ds)
        if referencingTable.size() > 0:
            print 'Checking foreign keys in "%sTable"' % referencing

            referencingRows = referencingTable.get()
            for fk, references in fks.iteritems() :
            
                evalString = 'False'
                for reference in references :
                    evalString += ' or checkSimpleFK(ds.' + reference + 'Table() , ' + 'r.'+fk+'())'

                for i in range( len(referencingRows) ) :
                      r = referencingRows[i]
                      try:
                          if (eval(evalString) == False) :
                              print '\tRow #' + repr(i) +': unresolved reference from ' + referencing + ' to ' + ' or '.join(references) + ' via ' + fk
                      except IllegalAccessException,e: 
                          pass
                      except AttributeError,e:
                          print e
                          
def getSDMDataHeader(bdfPath):
    """
    Returns the global header of the binary data contained in the file referred to by 'bdfPath' in a DOM element.
    """
    fp = open(bdfPath)
    msg = email.message_from_file(fp)
    fp.close()

    partCounter = 0
    payLoad = None
    parts = msg.walk()

    for part in msg.walk():
        if partCounter == 0:
            partCounter += 1
            continue # ignore the part #0
        if partCounter == 1:
            payLoad = part.get_payload()
            break

    # payLoad is expected to contain the SDMDataHeader (in its XML representation).
    # make a DOM out of it
    dom = xml.dom.minidom.parseString(payLoad)

    # extract the SDMDataHeader
    sdmDataHeader = dom.getElementsByTagName("sdmDataHeader")[0]

    return sdmDataHeader

def timeIntervalIntersectsAScan(row , scans):
    """
    Assumes that 'row' represents one row of one of the ASDM tables which have a timeInterval attribute,
    and that 'scans' contains a tuple or rows of the Scan table.
    Returns True if there is at least one element of scans whose time range [startTime, endTime] has a non null
    intersection with [start, start+duration] (row) and False otherwise.
    """
    for scan in scans:
        currentScanStartTime = scan.startTime().get()
        currentScanEndTime   = scan.endTime().get()
        rowStartTime         = row.timeInterval().start().get()
        rowEndTime           = rowStartTime + row.timeInterval().duration().get()
        if ( max(currentScanStartTime, rowStartTime) < min(currentScanEndTime, rowEndTime) ):
            return True
    return False

def checkOthers(ds, bdfRoot):
    """
    Performs some other particular checks.
    """
    print "In table ConfigDescription, checking if 'dataDescriptionId' and 'switchCycleId' have the same length." 
    configDescriptionRs = ds.configDescriptionTable().get()
    for i in range(len(configDescriptionRs)):
        if len(configDescriptionRs[i].dataDescriptionId()) != len(configDescriptionRs[i].switchCycleId()):
           print "WARNING. In table ConfigDescription at row #%d the 1D array attribute 'dataDescription' and 'switchCycle' do not have the same size" % i

    print "In table ConfigDescription, checking if 'feedId' has its size equal to 'numAntenna*numFeed'."
    for i in range(len(configDescriptionRs)):
        if len(configDescriptionRs[i].feedId()) != configDescriptionRs[i].numAntenna() * configDescriptionRs[i].numFeed() :
            print "WARNING. In table ConfigDescription at row #%d the 1D array attribute 'feediId' has a size of '%d', it should be equal to %d ('numAntenna*numFeed')" % (i, len(configDescriptionRs[i].feedId()) , configDescriptionRs[i].numAntenna()*configDescriptionRs[i].numFeed()) 
 
    print "In table ConfigDescription, checking whether 'assocConfigDescriptionId' and 'assocNature' are both present or both absent."    
    for i in range(len(configDescriptionRs)):
        A = configDescriptionRs[i].isAssocConfigDescriptionIdExists()
        B = configDescriptionRs[i].isAssocNatureExists()
        if (A and not B) or (not A and B) :
            print "WARNING. In table ConfigDescription at row #%d the attributes 'assocConfigDescriptionId' and 'assocNature' must be both present or absent !" % i

    print "In tabke ConfigDescription, checking if all the elements of 'dataDescriptionId' are distinct."
    for i in range(len(configDescriptionRs)):
        dds = configDescriptionRs[i].dataDescriptionId()
        if len(dds) != len(set(dds)):
            print "WARNING. In table ConfigDescription at row #%d the attribute 'dataDescription' which is an array has duplicate elements : '%s'" % (i ,dds)

    spectralWindowRs = ds.spectralWindowTable().get()
    print "In table SpectralWindow, checking whether 'assocSpectralWindowId' and 'assocNature' are both present or both absent !"
    for i in range(len(spectralWindowRs)):
        A = spectralWindowRs[i].isAssocSpectralWindowIdExists()
        B = spectralWindowRs[i].isAssocNatureExists()
        if (A and not B) or (not A  and B) :
            print "WARNING. In table SpectralWindow at row #%d the attributes 'assocSpectralWindowId' and 'assocNature' must be both present or absent !" % i

    print "In table SpectralWindow, checking whether the pair ('chanFreqStart', 'chanFreqStep') or 'chanFreqArray' is present."
    for i in range (len(spectralWindowRs)):
        A = spectralWindowRs[i].isChanFreqStartExists()
        B = spectralWindowRs[i].isChanFreqStepExists()
        C = spectralWindowRs[i].isChanFreqArrayExists()
        if not (A and B) and not C:
            print "WARNING. In table SpectralWindow at row #%d pair of attributes ('chanFreqStart', 'chanFreqStep') and the attribute 'chanFreqArray' are not present." % i
            
    print "In table SpectralWindow, checking whether 'chanWidth' or 'chanWidthArray' is present."
    for i in range (len(spectralWindowRs)):
        A = spectralWindowRs[i].isChanWidthExists()
        B = spectralWindowRs[i].isChanWidthArrayExists()
        if not ( A or B ):
            print "WARNING. In table SpectralWindow at row #%d the attributes 'chanWidth' and 'chanWidthArray' are both absent !" % i

    print "In table SpectralWindow, checking whether 'effectiveBw' or 'effectiveBwArray' is present."
    for i in range(len(spectralWindowRs)):
        A = spectralWindowRs[i].isEffectiveBwExists()
        B = spectralWindowRs[i].isEffectiveBwArrayExists()
        if not ( A or B ):
            print "In table SpectralWindow at row #%d the attributes 'effectiveBw' and 'effectiveBwArray' are both absent !" % i

    print "Checking State and CalDevice table."
    numOffSkyStateRows = len(filter(lambda r: r.onSky() == False, ds.stateTable().get()))
    if ( ds.calDeviceTable().size() == 0 and (numOffSkyStateRows != 0)) :
        print "WARNING. The CalDevice table is empty while there are rows in the State table with onSky == False !"
        
    print """Checking that the polarizations announced in the Feed table are compatible with what is expected in the Polarization table. 
The check is done only for processorType == CORRELATOR and numFeed == 1."""
    cfgDescs = [cfgDesc for cfgDesc in ds.configDescriptionTable().get() if cfgDesc.processorType()==ProcessorType.CORRELATOR() and cfgDesc.numFeed() == 1]
    for cfgDesc in cfgDescs:
        feedId = cfgDesc.feedId()[0]
        for dataDescription in map (ds.dataDescriptionTable().getRowByKey, cfgDesc.dataDescriptionId()):
            polarizationTypes = uniq(flatten(ds.polarizationTable().getRowByKey(dataDescription.polOrHoloId()).corrProduct()))
            for antennaId in cfgDesc.antennaId():
                #
                # Ok get all the feeds declared with a given triple of spectralWindowId, antennaId and feedId
                #
                feeds = [feed for feed in ds.feedTable().get() if dataDescription.spectralWindowId()==feed.spectralWindowId() and antennaId==feed.antennaId() and feedId==feed.feedId()]
                #
                # Check for each of them that their polarizationTypes attribute contain what is expected by the relevant row in the table Polarization.
                #
                for feed in feeds :
                    if not reduce(operator.and_ , map(lambda x: x in feed.polarizationTypes(), polarizationTypes), True):
                        print "WARNING. One polarization ('%s') has a corrProduct attribute which contains a polarization '%s' which not announced in the Feed table '%s' " % (dataDescription.polOrHoloId(), ds.polarizationTable().getRowByKey(dataDescription.polOrHoloId()).corrProduct(), feed.polarizationTypes())

    scans = ds.scanTable().get()
    print "In table Feeds, counting how many rows have a timeInterval with a non null intersection with the time ranges defined in the Scan table"
    feedsIntersectingScans = [feed for feed in ds.feedTable().get() if timeIntervalIntersectsAScan(feed, scans) ]
    if len(feedsIntersectingScans) < ds.feedTable().size():
        print "WARNING. The Feed table has %d rows. %d of them have a time interval with a non null intersection with the time ranges defined by the Scan table." % (ds.feedTable().size(), len(feedsIntersectingScans))
    else:
        print "INFO. The Feed table has %d rows. %d of them have a time interval with a non null intersection with the time ranges defined by the Scan table." % (ds.feedTable().size(), len(feedsIntersectingScans))

    print "In table Main, checking if the binary data referred to by the attribute 'dataOid' are present plus some other consistency checks."
    mainRs = ds.mainTable().get()
    for i in range (len(mainRs)) :
        #
        # Checking in the file containing the binary data exists.
        #
        bdfPath = bdfRoot+"/"+ str(mainRs[i].dataUID()).replace("/","_").replace(":","_")
        print "Checking ", bdfPath

        if not os.path.exists(bdfPath):
            print "WARNING. In main table at row #"+str(i)+" the binary data referred to by '"+str(mainRs[i].dataOid())+"' could not be found"
        else:
            try:
                #
                # checking if the size of the BDF is equal (or close) to the value announced by dataSize in the Main table.
                #
                dataSize = mainRs[i].dataSize() 
                bdfSize  = os.path.getsize(bdfPath)
                if dataSize != bdfSize:
                    print "WARNING. The size of '%s' which is '%d' is not equal to the value '%d' announced by the attribute 'dataSize'" % (bdfPath, bdfSize, dataSize)
                    

                sdmDataHeader = getSDMDataHeader(bdfPath)
                #
                # Checking if the ASDM and the binary data announce the same Processor.
                #
                s_asdmProcessor = str(ds.configDescriptionTable().getRowByKey(mainRs[i].configDescriptionId()).processorType())
                s_bdfProcessor = sdmDataHeader.getElementsByTagName("processorType")[0].childNodes[0].data
                if not (s_asdmProcessor == s_bdfProcessor):
                    print "WARNING. In main table at row #"+str(i)+", the associated config description declares a processor type " + s_asdmProcessor + " which is different from the one appearing in the binary data global header " + s_bdfProcessor

                #
                # Checking if the ASDM and the binary data announce the same Atmospheric Phase Correction.
                #
                l_asdmAPC = map(str, ds.configDescriptionTable().getRowByKey(mainRs[i].configDescriptionId()).atmPhaseCorrection())
                l_bdfAPC = sdmDataHeader.getElementsByTagName("dataStruct")[0].getAttribute("apc").split()
                
                #
                # Ignore the case when APC is announced as AP_UNCORRECTED in the ASDM while it's not set in
                # the BDF header. It's an inconsistency that we decide to live with :-(.
                #
                if s_asdmProcessor == 'RADIOMETER' and len(l_asdmAPC) == 1 and l_asdmAPC[0] == 'AP_UNCORRECTED' and len(l_bdfAPC) == 0 :
                    continue

                if not (l_asdmAPC == l_bdfAPC) :
                    print "WARNING. In main table at row #"+str(i)+", the associated config description declares an atmospheric phase correction " + repr(l_asdmAPC)+" which is different from the one appearing in the binary data global header " + repr(l_bdfAPC)
                                                                
            except Exception, e:
                print "An exception was raised during the processing of '%s'" % bdfPath
                print "Here is the message of the exception '%s'" % e
                print "and the stack trace"
                print traceback.print_tb(sys.exc_traceback)
            
#
# A dictionary to define the expected shapes for attributes which are arrays.
# SHAPES is a dictionary of dictionaries:
#  - the key is expected to contain an ASDM table name,
#  - the value is a dictionary whose key is expected to contain the name of an array attribute of this table
#  and the value a tuple representing the expected dimensions.
#
#
SHAPES = {
    'almaRadiometer' : { 'spectralWindowId' : ('numAntenna',)
                         },
    
    'antenna'  : { 'position' : ('3',),
                   'offset' : ('3',)
                   },
    
    'calAmpli' : { 'polarizationTypes': ('numReceptor',),
                   'frequencyRange': ('2',),
                   'apertureEfficiency': ('numReceptor'),
                   'apertureEfficiencyError': ('numReceptor')
                   },

    'calAtmosphere' : { 'forwardEffSpectrum':('numReceptor', 'numFreq'),
                        'frequencyRange':('2',),
                        'frequencySpectrum':('numFreq',),
                        'polarizationTypes':('numReceptor',),
                        'powerSkySpectrum' : ('numReceptor', 'numFreq'),
                        'powerLoadSpectrum' : ('numLoad', 'numReceptor', 'numFreq'),
                        'tAtmSpectrum':('numReceptor', 'numFreq'),
                        'tRecSpectrum':('numReceptor', 'numFreq'),
                        'tSysSpectrum':('numReceptor', 'numFreq'),
                        'tauSpectrum':('numReceptor', 'numFreq'),
                        'sbGainSpectrum':('numReceptor', 'numFreq'),
                        'tAtm':('numReceptor',),
                        'tRec':('numReceptor',),
                        'tSys':('numReceptor',),
                        'tau':('numReceptor',),
                        'water':('numReceptor',),
                        'waterError':('numReceptor',),
                        'alphaSpectrum' : ('numReceptor', 'numFreq'),
                        'forwardEfficiency':('numReceptor',),
                        'forwardEfficiencyError':('numReceptor',),
                        'sbGain':('numReceptor',),
                        'sbGainSpectrum' : ('numReceptor', 'numFreq')
                        },

    'calBandpass' : { 'antennaNames' : ('numAntenna',),
                      'freqLimits' : ('2',),
                      'polarizationTypes' : ('numReceptor',),
                      'curve' : ('numAntenna', 'numReceptor', 'numPoly'),
                      'reducedChiSquare' : ('numReceptor',),
                      'rms' : ('numReceptor','numBaseline')
                      },

    'calCurve' : { 'frequencyRange' : ('2',),
                   'antennaNames': ('numAntenna',),
                   'polarizationTypes' : ('numReceptor',),                   
                   'curve' : ('numAntenna', 'numReceptor', 'numPoly'),
                   'reducedChiSquared' : ('numReceptor',),
                   'rms' : ('numReceptor','numBaseline')
                   },
    
    'calData' : { 'scanSet' : ('numScan',),
                  'fieldName' : ('numScan',),
                  'sourceName' : ('numScan',),
                  'sourceCode' : ('numScan',),
                  'scanIntent' : ('numScan',)
                  },
    
    'calDelay' : {'delayError' : ('numReceptor',),
                  'delayOffset' : ('numReceptor',),
                  'polarizationTypes': ('numReceptor',),
                  'delayOffset' : ('numReceptor',),
                  'reducedChiSquared' : ('numReceptor',),
                  'refFreq' : ('numSideband',),
                  'refFreqPhase' : ('numSideband',),
                  'sidebands' : ('numSideband',)
                  },

    'calDevice' : {'calLoadNames' : ('numCalload',),
                   'calEff' : ('numReceptor', 'numCalload'),
                   'noiseCal' : ('numCalload',),
                   'temperatureLoad' : ('numCalload',)
                   },

    'calFlux' : {'frequencyRange' : ('numFrequencyRanges', '2'),
                 'flux' : ('numStokes', 'numFrequencyRanges'),
                 'fluxError' : ('numStokes', 'numFrequencyRanges'),
                 'stokes' : ('numStokes',),
                 'direction' : ('2',),
                 'PA' : ('numStokes', 'numFrequencyRanges'),
                 'PAError' :  ('numStokes', 'numFrequencyRanges'),
                 'size' : ('numStokes', 'numFrequencyRanges', '2'),
                 'sizeError' : ('numStokes', 'numFrequencyRanges', '2')
                 },

    'calFocusModel' : {'coeffName' : ('numCoeff',),
                       'coeffFormula' : ('numCoeff',),
                       'coeffValue' : ('numCoeff',),
                       'coeffError' : ('numCoeff',),
                       'coeffFixed' : ('numCoeff',),
                       'focusRMS' : ('3',),
                       },

    'calFocus' : {'frequencyRange' : ('2',),
                  'pointingDirection' : ('2',),
                  'offset' : ('numReceptor','3'),
                  'offsetError' : ('numReceptor','3'),
                  'offsetWasTied' : ('numReceptor','3'),
                  'reducedChiSquared' : ('numReceptor','3'),
                  'focusCurveWidth' :  ('numReceptor','3'),
                  'focusCurveWidthError' :  ('numReceptor','3'),
                  'focusCurveWasFixed' : ('3',),
                  'offIntensity' :  ('numReceptor',),
                  'offIntensityError' :  ('numReceptor',),
                  'peakIntensity' :  ('numReceptor',),
                  'peakIntensityError' :  ('numReceptor',)
                  },

    'calHolography' : {'focusPosition' : ('3',),
                       'frequencyRange' : ('2',),
                       'polarizationTypes' : ('numReceptor',),
                       'direction' : ('2',),
                       'screwName' : ('numScrew', ),
                       'screwMotion' : ('numScrew',),
                       'screwMotionError' : ('numScrew',),
                       'gravOptRange' : ('2', ),
                       'tempOptRange' : ('2', )
                       },
    
    'calPhase' : {'ampli': ( 'numReceptor', 'numBaseline'),
                  'antennaNames' : ('numBaseline', '2'),
                  'baselineLengths' : ('numBaseline',),
                  'decorrelationFactor' :('numReceptor', 'numBaseline'),
                  'direction' : ('2',),
                  'frequencyRange' : ('2',),
                  'phase': ('numReceptor', 'numBaseline'),
                  'polarizationTypes' : ('numReceptor',),
                  'phaseRMS' : ('numReceptor', 'numBaseline'),
                  'statPhaseRMS' : ('numReceptor', 'numBaseline'),
                  'correctionValidity' : ('numBaseline',)
                  },

    'calPointingModel' : {'coeffName' : ('numCoeff',),
                          'coeffVal' :  ('numCoeff',),
                          'coeffError' :  ('numCoeff',),
                          'coeffFixed' :  ('numCoeff',),
                          'coeffFormula' :  ('numCoeff',)
                          },

    'calPointing' : {'direction' : ('2',),
                     'frequencyRange' : ('2'),
                     'polarizationTypes' : ('numReceptor',),
                     'collOffsetRelative' : ('numReceptor', '2'),
                     'collOffsetAbsolute' : ('numReceptor', '2',),
                     'collError' : ('numReceptor', '2',),
                     'collOffsetTied' : ('numReceptor', '2',),
                     'reducedChiSquared' : ('numReceptor',),
                     'beamPA': ('numReceptor',),
                     'beamPAError' : ('numReceptor',),
                     'beamWidth' : ('numReceptor', '2'),
                     'beamWidthError' : ('numReceptor', '2'),
                     'beamWidthWasFixed' : ('2',),
                     'offIntensity' : ('numReceptor',),
                     'offIntensityError' : ('numReceptor',),
                     'peakIntensity' : ('numReceptor', ),
                     'peakIntensityError' : ('numReceptor',)
                     },

    'calPosition' : {'antennaPosition' : ('3',),
                     'stationPosition' : ('3',),
                     'refAntennaNames' : ('numAntenna',),
                     'positionOffset': ('3',),
                     'positionErr' :  ('3',)
                     },
    
    'calPrimaryBeam' : {'frequencyRange' : ('2', ),
                        'polarizationTypes' : ('numReceptor',),
                        'mainBeamEfficiency' : ('numReceptor',)
                        },

    'calReduction' : {'appliedCalibrations' : ('numApplied',),
                      'paramSet' : ('numParam',),
                      'invalidConditions' : ('numInvalidConditions',)
                      },

    'calSeeing' : {'frequencyRange' : ('2',),
                   'baselineLengths' : ('numBaseLengths',),
                   'phaseRMS' : ('numBaseLengths',)
                   },

    'calWVR' : {'inputAntennaNames' : ('numInputAntennas',),
                'chanFreq' : ('numChan',),
                'chanWidth' : ('numChan',),
                'refTemp' : ('numInputAntennas', 'numChan'),
                'pathCoeff' : ('numInputAntennas', 'numChan', 'numPoly'),
                'polyFreqLimits' : ('2',),
                'wetPath':('numPoly',),
                'dryPath':('numPoly' ,)
                },

    'configDescription' : {'atmPhaseCorrection' : ('numAtmPhaseCorrection',),
                           'antennaId' : ('numAntenna',),
                           'feedId' : ('numAntenna * numFeed',),
                           'switchCycleId' : ('numDataDescription',),
                           'dataDescriptionId' : ('numDataDescription',),
                           'phasedArrayList' : ('numAntenna',),
                           'assocNature' : ('numAssocValues',),
                           'assocConfigDescriptionId' : ('numAssocValues',)
                           },

    'correlatorMode' : { 'basebandNames' : ('numBaseband',),
                         'basebandConfig' : ('numBaseband',),
                         'axesOrderArray' : ('numAxes', ),
                         'filterMode' : ('numBaseband',)
                         }, 

    'delayModel' : { 'atmDryDelay' : ('numPoly',),
                     'atmWetDelay' : ('numPoly',),
                     'clockDelay'  : ('numPoly',),
                     'geomDelay'   : ('numPoly',),
                     'dispDelay'   : ('numPoly',),
                     'groupDelay'  : ('numPoly',),
                     'phaseDelay'  : ('numPoly',)
                     },

    'execBlock' : { 'antennaId' : ('numAntenna',)
                    },

    'feed' : { 'beamOffset' : ('numReceptor', '2'),
               'focusReference' : ('numReceptor', '3'),
               'polarizationTypes' : ('numReceptor',),
               'polResponse' : ('numReceptor', 'numReceptor'),
               'receptorAngle' : ('numReceptor',),
               'receiverId' : ('numReceptor',),
               'illumOffset': ('2', ),
               'position' : ('3', )
               },

    'field' : { 'delayDir' : ('numPoly', '2'),
                'phaseDir' : ('numPoly', '2'),
                'referenceDir' : ('numPoly', '2')
                },

    'focusModel' : {'coeffName' : ('numCoeff',),
                    'coeffFormula' : ('numCoeff',),
                    'coeffVal' : ('numCoeff',)
                    },

    'focus' : {'focusOffset' : ('3',),
               'measuredFocusPosition' : ('3',)
               },

    'gainTracking' : {'cableDelay' : ('numReceptor',),
                      'loPropagationDelay' : ('numLO',),
                      'polarizationTypes' : ('numReceptor',),
                      'receiverDelay' : ('numReceptor',),
                      'freqOffset' : ('numLO',),
                      'phaseOffset' : ('numLO',),
                      'attFreq' : ('numAttFreq',),
                      'attSpectrum' : ('numAttFreq',)
                      },
    
    'holography' : { 'type' : ('numCorr',)
                     },

    'main' : { 'stateId' : ('numAntenna',) },

    'pointingModel' : { 'coeffName' : ('numCoeff',),
                        'coeffVal' : ('numCoeff',),
                        'coeffFormula' : ('numFormula',)
                        },

    'pointing' : { 'encoder' : ('numSample', '2'),
                   'pointingDirection' : ('numSample', '2'),
                   'target' :  ('numSample', '2'),
                   'offset' :  ('numSample', '2'),
                   'sourceOffset' :  ('numSample', '2'),
                   'sampledTimeInterval' : ('numSample',)
                   },

    'polarization' : { 'corrType' : ('numCorr',),
                       'corrProduct' : ('numCorr', '2')
                       },

    'receiver' : { 'freqLO' : ('numLO',),
                   'sidebandLO' : ('numLO',)
                   },
    
    'sBSummary' : { 'centerDirection' : ('2',),
                    'observingMode' : ('numObservingMode',),
                    'scienceGoal' : ('numScienceGoal',),
                    'weatherConstraint' : ('numWeatherConstraint',)
                   },

    'scan' : { 'scanIntent' : ('numIntent',),
               'calDataType' : ('numIntent',),
               'calibrationOnLine' : ('numIntent',),
               'calibrationFunction' : ('numIntent',),
               'calibrationSet' : ('numIntent',),
               'calPattern' : ('numIntent',),
               'fieldName' : ('numField',)
               },

    'seeing' : { 'baseLength' : ('numBaseLength',),
                 'phaseRms' : ('numBaseLength',)
                 },
    
    'source' : { 'direction' : ('2',),
                 'properMotion': ('2',),
                 'position' : ('3',),
                 'transition' : ('numLines',),
                 'restFrequency' : ('numLines',),
                 'sysVel' : ('numLines',),
                 'rangeVel' : ('2',),
                 'frequency' : ('numFreq',),
                 'frequencyInterval' : ('numFreq',),
                 'stokesParameter' : ('numStokes',),
                 'flux' : ('numFreq', 'numStokes'),
                 'fluxErr' : ('numFreq', 'numStokes'),
                 'positionAngle' : ('numFreq',),
                 'positionAngleErr' : ('numFreq',),
                 'size' : ('numFreq', '2'),
                 'sizeErr': ('numFreq', '2')
                 },

    'spectralWindow' : { 'chanFreqArray' : ('numChan',),
                         'chanWidthArray' : ('numChan',),
                         'effectiveBwArray' : ('numChan',),
                         'lineArray' : ('numChan',),
                         'resolutionArray' : ('numChan',),
                         'assocNature' : ('numAssocValues',),
                         'assocSpectralWindowId' : ('numAssocValues',)
                         },

    'station' : { 'position' : ('3',)
                  },

    'subscan' : { 'numSubintegration' : ('numIntegration',)
                  },

    'switchCycle' : { 'weightArray' : ('numStep',),
                      'dirOffsetArray' : ('numStep', '2'),
                      'freqOffsetArray' : ('numStep',),
                      'stepDurationArray' : ('numStep',)
                      },
    
    'sysCal' : { 'tcalSpectrum' : ('numReceptor', 'numChan'),
                 'trxSpectrum' : ('numReceptor', 'numChan'),
                 'tskySpectrum' : ('numReceptor', 'numChan'),
                 'tsysSpectrum' : ('numReceptor', 'numChan'),
                 'tantSpectrum' : ('numReceptor', 'numChan'),
                 'tantTsysSpectrum' : ('numReceptor', 'numChan'),
                 'phaseDiffSpectrum' : ('numReceptor', 'numChan')                                 
                 },

    'wVMCal' : { 'polyFreqLimits' : ('2',),
                 'pathCoeff' : ('numChan', 'numPoly'),
                 'refTemp'   : ('numChan',)
                 }
    }
                      
#
# A collection of functions to check the array sizes constraints
#
def check1D(array ,size) :
    """ Check that the length of 'array', assumed to be a sequence is equal to 'size'

    Returns True if and only if the equality condition is verified.
    """
    
    return len(array) == size


def check2D(array , size1, size2) :
    """ Check that the length of 'array', assumed to be a sequence of sequence is equal to 'size1'
    and that each of its elements is a sequence of length 'size2'

    Returns True if and only if all the equality conditions are verified.
    """
    if len(array) != size1 :
        return False

    for slice in array :
        if not check1D(slice, size2) :
            return False

    return True

def check3D (array, size1, size2, size3) :
    """ Check that the length of 'array', assumed to be a sequence of sequence is equal to 'size1'
    and that each of its elements passes successfully the check2D test with the pair of integer
    (size2, size3).

    Returns True if and only if all the equality conditions are verified.
    """

    if len(array) != size1 :
        return False

    for slice in array :
        if not check2D(slice, size2, size3) :
            return False

    return True
    
def check4D (array, size1, size2, size3, size4) :
    """ Check that the length of 'array', assumed to be a sequence of sequence is equal to 'size1'
    and that each of its elements passes successfully the check3D test with the triple of integer
    (size2, size3, size4).

    Returns True if and only if all the equality conditions are verified.
    """

    if len(array) != size1 :
        return False

    for slice in array :
        if not check3D(slice, size2, size3, size4) :
            return False

    return True

def toPython(s, methodName, rowVar):
    result = []
    g = generate_tokens(StringIO.StringIO(s).readline)
    for toknum, tokval,_,_,_ in g:
        if toknum == NAME:
            result.extend([(STRING, methodName),
                           (OP, '('),
                           (STRING, repr(tokval)),
                           (OP, ','),
                           (STRING, rowVar),
                           (OP, ')')
                           ])
        else:
            result.append((toknum, tokval))
    return untokenize(result)


def shapeElement2Int(ds, r, shapeElement):
    if repr(type(shapeElement)) == "<type 'tuple'>" and len(shapeElement) == 2:
        foreignKey = shapeElement[0]
        foreignAttribute = shapeElement[1]
        foreignRow = getForeignRow(ds, r, foreignKey)
        return getAttribute(foreignAttribute, foreignRow)
    
    elif repr(type(shapeElement)) == "<type 'str'>":
        if shapeElement.isdigit():
            return int(shapeElement)
        else:
            try :
                evalString = toPython(shapeElement, "getAttribute", "r")
                return eval(evalString)
            except ASDMCheckIllegalAccessException,e :
                raise ASDMCheckException("The value of the attribute '"+shapeElement+"' is a part of a shape being checked, but this attribute was absent !")

    else:
        raise ASDMCheckException("Invalid shape element '"+repr(shapeElement)+"'")
        
def checkArrays(ds):
    """

    Returns True if and only if the attributes whose names are defined as keys in each dictionary of
    the dictionary SHAPES have their shapes equal to the values associated to those keys.
    """

    intab = '()'
    outtab = '[]'
    transtab = maketrans(intab, outtab)
    
    for tablePrefix, shapes in SHAPES.iteritems():
        tableName = tablePrefix + 'Table'
        try:
            table = getTable(tableName, ds)
        except Exception, err:
            print 'Unknown table "' + tableName + '"'
            break
        
        if (table.size() > 0):
            print 'Checking arrays sizes in "' + tableName + '"'
            rows = table.get()
            for i in range(len(rows)):
                r = rows[i]

                for attribute, shape in shapes.iteritems():
                    try:
                        if len(shape) == 1:
                            result = check1D(getAttribute(attribute, r),
                                             shapeElement2Int(ds, r, shape[0]))
                        elif len(shape) == 2:
                            result = check2D(getAttribute(attribute, r),
                                             shapeElement2Int(ds, r, shape[0]),
                                             shapeElement2Int(ds, r, shape[1]))
                        elif len(shape) == 3:
                            result = check3D(getAttribute(attribute, r),
                                             shapeElement2Int(ds, r, shape[0]),
                                             shapeElement2Int(ds, r, shape[1]),
                                             shapeElement2Int(ds, r, shape[2]))
                        elif len(shape) == 4:
                            result = check4D(getAttribute(attribute, r),
                                             shapeElement2Int(ds, r, shape[0]),
                                             shapeElement2Int(ds, r, shape[1]),
                                             shapeElement2Int(ds, r, shape[2]),
                                             shapeElement2Int(ds, r, shape[3]))
                        if not result :
                            print "\tWARNING. Row #%d : attribute '%s' does not have the expected shape '%s'" % (i, attribute, reduce (lambda x, y :  x +  y ,
                                                                                                                              map (lambda x : '[' + x + ']' ,shape))
                                                                                                        )
                                                        
                    except ASDMCheckIllegalAccessException,e :
                        continue #break
                    except ASDMCheckException, e0:
                        print "\t%s at row #%d while checking shape of '%s'" % (e0, i, attribute)
                        continue #break
                    except AttributeError,e1 :
                        print "\t%s at row #%d" % (e1, i)
                        continue #break

def usage():
    print """asdmCheck.py [-h] [-a] [-k] [-o] path-to-ASDM
    This application performs a number of checks on ASDM datasets exported on disk:
    - it verifies that all attributes which play the role of foreign keys,i.e. which are Tags, have the good type and refer to existing rows.
    - it verifies that all attributes which are arrays (possibly multidimensional) have a shape equal to the one which is expected in
    the model definition.
    - it makes a number of other checks like the simultaneous presence or absence of some attributes.

    Parameter:
    path-to-ASDM : the path to the ASDM exported on disk.
    
    Options:
    -h : display this message
    -a : check shapes of array attributes
    -k : check foreign keys (Tag) resolution
    -o : check other usage rules
    -r : display the revision information about this application

    Without any option, array shape, foreign keys and other rules are checked.
    """
    
def main():
    """ The main function
    """
    #
    # The path to the ASDM is expected in argv[1]
    #
    try:
        options, arg = getopt.getopt(sys.argv[1:], ":akhro:", ["revision"])
    except getopt.GetoptError, err:
            usage();
            sys.exit(2)
        
    optCheckArrays = False
    optCheckFKs = False
    optCheckOthers = False
    
    for o, a in options:
        if o in ("-r", "--revision"):
            print "$Id: asdmCheck.py,v 1.16 2011/04/21 09:39:31 mcaillat Exp $"
            rOptPresent = True
        elif o == '-h':
            usage()
            sys.exit(0)
        elif o == '-a':
            optCheckArrays = True
        elif o == '-k':
            optCheckFKs = True
        elif o == '-o':
            optCheckOthers = True
        else:
            assert False, "unhandled option"
    
    if len(arg) == 0:
        if rOptPresent:
            sys.exit()
        else:
            usage()
            sys.exit(2)
        
    dsPath = arg[0].rstrip("/")
    ds = ASDM()

    try:
        print 'Loading ', dsPath
        ds.setFromFile(dsPath)
    except ConversionException, e:  # Note that  ConversionException class is known
        print e.getMessage()
        sys.exit (1)

    if optCheckArrays == False and optCheckFKs == False and optCheckOthers == False:
        optCheckArrays = True
        optCheckFKs = True
        optCheckOthers = True

    if optCheckArrays:
        checkArrays(ds)

    if optCheckFKs:
        checkSimpleFKs(ds)

    if optCheckOthers:
        checkOthers(ds, dsPath+"/ASDMBinary/")        
#
# The main entry point.
#
if __name__ == "__main__":
    main()

                
