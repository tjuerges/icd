%module asdm

%include "std_vector.i"
%include "std_string.i"

%include "eenumerations.swg"

%include "ASDMTypes.i"
%include "ContainerDef.i"
%include "RowDef.i"
%include "TableDef.i"
