import asdm
import re
import StringIO
import new


#
# A way to print a row of a table.
#
def attributes(adict):
    result=[]
    for k in adict.iterkeys():
        if re.match("__", k) == None :
            result.append(k)
    result.sort()
    return result

def attrP(attr):
    result = ""
    if hasattr(attr, '__iter__') == True:
        result += "["
        for attrItem in attr:
            result += attrP(attrItem) + ", "
        result += "]"
    else:
        result += attr.__str__()
    return result

def __strRow__(self):
    output = StringIO.StringIO()
    for attrName in attributes(self.__class__.__dict__):
        try:
            attr = self.__class__.__dict__.get(attrName)(self)
#            print >>output, attrName , " = " , attrP(attr)
            print >>output, attrName , " = ", attr
        except asdm.IllegalAccessException:
            print >>output, attrName , " = <not defined>"
    return output.getvalue()
        
for k in asdm.__dict__.iterkeys():
    if re.match(".+Row$", k) != None :
        thing = asdm.__dict__.get(k)
        asdm.__dict__.get(k).__str__ = __strRow__

#
# A way to print an ASDM dataset
#
def __strASDM__(self):
    tables=[]
    for k in asdm.ASDM.__dict__.iterkeys():
        if re.match(".+Table", k) != None :
            tables.append(k)
    tables.sort()
    
    hr = "|";
    for i in range(47):
        hr = hr + "-"
    hr = hr + "|"

    output = StringIO.StringIO()
    print >>output, hr
    print >>output, "|%20s : %16s        |" % ("Dataset entity id", self.entity())
    print >>output, hr
    for k in tables:
        print >>output, "|%20s : %16s row(s) |" %(asdm.ASDM.__dict__.get(k)(self).getName(), asdm.ASDM.__dict__.get(k)(self).size())
    print >>output, hr
    return output.getvalue()

asdm.ASDM.__str__ = __strASDM__

def __ASDM__tables__(self):
    tables=[]
    for k in asdm.ASDM.__dict__.iterkeys():
        if re.match(".+Table", k) != None :
            tables.append(k)
    tables.sort()
    return tables

asdm.ASDM.tables = __ASDM__tables__

