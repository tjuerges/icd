#! /usr/bin/env/python
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# import the module containing the python/C++ wrapper for the asdm classes.
from asdm import *

#
#
#
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# About the ASDM and its tables and rows and exceptions.
#
# how to create an ASDM in memory from its disk representation.
#

ds = ASDM();
name = "./testWrapper.sdm"

try :
    ds.setFromFile(name)
except ConversionException, e:  # Note that ConversionException class is known
    print e.getMessage()
    exit


#
# Please note that the exceptions defined in the C++ version of the ASDM have their Python counterparts.
#
 
#
# How to print a report about a dataset
#
print ds


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# How to access a table of the dataset, e.g. its Antenna table.
#  
antennaT=ds.antennaTable()
#
# Please note a first difference between the naming of methods in C++ and Python. The method which returns a reference
# to a table of a dataset wears the name the table with it's first character in lowercase, 'antennaTable' in the above
# example.

print "The Antenna table has " , antennaT.size(), "antenna(s)."

#
# A description of the Antenna table.
#
print
print "The attributes of the Antenna table are the following :"
print antennaT.descr()

#
# Please note this method 'descr' which is specific to the Python version of the ASDM. 
#
#
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# How to get the collection of rows of the Antenna table in a python list.
# Proceed the same way than in C++.
#
antennas = antennaT.get()
print "The Antenna table still has ", len(antennas), " antenna(s)"

#
# How to print the content of a row given its index
# The print method has been defined for a row of a table of the ASDM.
#
##if antennaT.size() > 0 :
##    print antennas[0]   # it's also correct to write antennaT.get()[0]
##                        # or even ds.antennaTable().get()[0]
##else:
##    exit

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
#
# How to access a specific attribute.
#
print
print "antennas[0].time = " , antennas[0].time(), " ( which is a ", type(antennas[0].time()), ")"
print "antennas[0].position = ", antennas[0].position(), " ( which is a ", map(type, antennas[0].position()), ")"
#
# Please note a second difference in the methods naming convention between the C++ and the Python version. In Python
# the method which returns the value of an attribute wears the name of the attribute with it first character in lower case.
#
# How to get the numerical value of an attribute..
#
# Proceed the same way than in C++.
print 
print "antennas[0].time numerical value is ", antennas[0].time().get(), "( which is a ", type(antennas[0].time().get()), ")"
print "antennas[0].position numerical value is ", map(Length.get, antennas[0].position()), "( which is a ", map(type, map(Length.get, antennas[0].position())), ")"

#
#
# Optional attributes are accessed exactly like in C++
#
spwT = ds.spectralWindowTable()
print
if spwT.size() > 0:
    spwR = spwT.get()[0]

    #
    # use the existence attribute
    #
    if spwR.isRefChanExists():
        print spwR.refChan()
    else:
        print "refChan is not defined in the row #0 of " , spwT.getName()

    #
    # or play with IllegalAccessException
    #
    try:
        print "About to generate an exception"
    except IllegalAccessException, e:
        print e.getMessage()
print
#
# Let's print a row of a table whose some attributes are arrays, e.g the Feed table.
#
if ds.feedTable().size() > 0 :
    print
    print "Here is a row of the Feed table whose some attributes are arrays:"
    print ds.feedTable().get()[0]
    
#
# How to get a row given its key
# Here we retrieve all the Antenna row listed in the antennaId attribute
# of the 1st row of the table ConfigDescription
#
cfgT = ds.configDescriptionTable()
if cfgT.size() > 0 :
    print
    print "Retrieving antennas listed by their keys in the 1st row of the table ConfigDescription"
    print
    antennaIds = cfgT.get()[0].antennaId()
    cfgT = ds.configDescriptionTable()

    antennaIds = cfgT.get()[0].antennaId()
    for i in range(len(antennaIds)):
        print ds.antennaTable().getRowByKey(antennaIds[i])

#
# How to get a tuple of rows knowing their "context"
#
focusT = ds.focusTable()
if focusT.size() > 0 :
    print
    print "Retrieving focuses for a given couple (antennaId, feedId)"
    print
    antennaId = antennas[0].antennaId()
    feedId = 0

    focusRows = focusT.getByContext(antennaId)
    print "Found " , len(focusRows) , " focus rows."
    for i in range(len(focusRows)):
        print
        print focusRows[i]

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# About types defined for the ASDM.
#
stmts = ('TagType.Antenna()',
         'Tag(3, TagType.ExecBlock())')

for stmt in stmts :
    print stmt + " -> " + repr(eval(stmt))
print

physQuans = (Angle, AngularRate, Flux, Frequency, Humidity, Length, Pressure, Temperature)

for physQuan in physQuans :
    print physQuan.__name__ + '(1.) -> ' + repr(physQuan(1.)) + '  (type = ' + repr(physQuan) + ')'
    print physQuan.__name__ + '(1.).get() -> ' + str(physQuan(1.).get()) + ' (type = ' + str(type(physQuan(1.).get())) + ')'
print

stmts = ('ArrayTime(2009, 8, 23, 14, 2, 37.)',
         'ArrayTimeInterval(ArrayTime(2009, 8, 23, 14, 2, 37.), Interval(86400000000))')
for stmt in stmts :
    print stmt + " -> " + repr(eval(stmt))
print



#
#
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
#
# About Enumerations defined in ICD/HLA/Enumerations.
#
# * Each enumeration is represented by a Python class whose name is the enumeration's name, e.g. the ALMA enumeration
# AntennaType is implemented in a Python class AntennaMake.
#
# * Each python class representing an ALMA enumerations contains one method for each literal wearing the name of that literal
# and returning this literal, e.g. the python class AntennaType contains three methods GROUND_BASED(), SPACE_BASED(), TRACKING_STN()
# returning respectively the literals GROUND_BASED, SPACE_BASED and TRACKING_STN.
print AntennaType.GROUND_BASED()
print AntennaType.SPACE_BASED()
print AntennaType.TRACKING_STN()

#
# * Each python class representing an ALMA enumerations contains a method list which returns a (python) list containing all its literals,
# e.g. AntennaType.list() returns (GROUND_BASED, SPACE_BASED, TRACKING_STN)
print AntennaType.list()

#
# * The function allAlmaEnumerations() (defined in the module almaEnumerations) returns the list of all python classes defined to represent
# the ALMA enumerations.
#
#
# 
for almaEnumeration in allAlmaEnumerations() :
    print almaEnumeration.__name__ + " = " + repr(almaEnumeration.list())
#



