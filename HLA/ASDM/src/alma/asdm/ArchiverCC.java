/*
 * Created on 22 sept. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package alma.asdm;

import java.util.Hashtable;
import java.util.logging.Logger;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;

import alma.xmlstore.ArchiveInternalError;
import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;



/**
 * @author caillat
 *
 * The ArchiverCC is provided as a facility class to ease the development of an application that needs
 * an Archiver. These applications always need a ContainerServices instance required
 * by the Archiver's constructor. A ContainerServices object comes automatically for example when the
 * application defines a class derived from ComponentClient, but when this is not the case the class ArchiverCC
 * gives an easy way to create a ComponentClient and an Archiver, all in one.<br>
 * The class ArchiverCC provides methods with the same names and signatures than the ones defined in the Archiver class to access the Archive,
 * their executions are simply performed by delegation to the Archiver instance that is automatically created at Archiver's instantiation.
 * 
 */
public class ArchiverCC extends ComponentClient {
	private ContainerServices containerServices = null;
	private Archiver ar = null;


    /**
     * Creates an ArchiverCC object i.e. basically an ComponentClient with an embedded Archiver.
     * 
     * <ul> 
     * <li> A logger,
     * <li> The ACS CORBA manager location.
     * </ul>
     *  @param logger
     *  @param managerLoc
     *  @throws Exception
     */
	public ArchiverCC(Logger logger, String managerLoc) throws Exception {
		super(logger, managerLoc, "ArchiverCC");
		containerServices = getContainerServices();
		
		ar = new Archiver(containerServices);
	}
	
    /**
     * Returns a reference to the Archiver created at instantiation.
     */
	public Archiver getArchiver() {
		return ar;
	}
	
    /**
     * Returns an array of all the Archive uids of stored document having a given schema and verifying a given XPath expression.
     * @param query an XPath expression 
     * @param schema the name of a schema
     * @return an array of strings containing Archive uids.
     * @throws ArchiveException
     */
    public String[] getUIDs(String query, String schema) throws ArchiveException {
        return ar.getUIDs(query, schema);
    }
    
    /**
     * Returns a string containing  pieces of XML code verifying an XPath expression in the documents stored in the Archive with a given schema.
     * The maximum of number of results is defined by a parameter.
     *  
     * @param aQuery an XPath expression 
     * @param schema a schema's name
     * @param maxRes the maximum number of results to be returned 
     * @return a string obtained by concatenating all the results.
     */
	public String query(String aQuery, String schema, int maxRes) {
		return ar.query(aQuery, schema, maxRes);
	}
	
    /**
     *  Returns an XML document stored in the archive given its Archive uid.
     * 
     * @param uid  the Archive uid of the document to retrieve,
     * @return the XML document.
     * 
     * @throws ArchiveException
     */
	public String retrieve(String uid) throws ArchiveException {
		return ar.retrieve(uid);
	}
	

	/**
     * Stores an XML document in the archive. The XML document is assumed to be conform with the schema whose name is passed as a parameter.
     * Returns the Archive uid assigned to the archived document.
     * 
     * @param xmlString the XML document to store
     * @return a string containing the Archive uid of the stored document.
     * 
     * @throws ArchiveException
     */
    public String store(String xmlString, String schema) throws ArchiveException {
		return ar.store(xmlString, schema);
	}
	
    /**
     * Stores an XML document in the archive. The XML document is assumed to be conform with the 
     * schema whose name is passed in argument. 
     * 
     * @param xmlString the XML document to store
     * @param schema the name of the schema 
     * @param archive ID assigned to this document
     * 
     * @throws ArchiveException
     */ 
	public void store(String xmlString, String schema, String uid) throws ArchiveException {
        ar.store(xmlString, schema,  uid);
    }
	
	
	

	
	public void update(String uid, String xmlString) throws ArchiveException {
		ar.update(uid, xmlString);
	}
    
    public void done() {
        try {
            ar.done();
        } catch (ArchiveInternalError e) {
            e.printStackTrace();
        }
    }
    
    public void finalize() {
        try {
            ar.finalize();
        } catch (ArchiveInternalError e) {
            e.printStackTrace();
        }
    }
}
