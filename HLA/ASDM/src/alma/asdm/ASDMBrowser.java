/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Nov 22, 2005
 * File ASDMBrowser.java
 */

package alma.asdm;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import alma.asdm.ASDMTable;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Interval;
import alma.hla.runtime.asdm.types.ArrayTimeInterval;

import java.io.File;
import java.lang.reflect.Method;
import java.util.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.event.*;
import java.lang.reflect.Array;

import java.awt.datatransfer.StringSelection;

/**
 * ASDMBrowser : an application to browse the tables of an ASDM exported in XML
 * on disk.
 * 
 * Command :
 * 
 * <br>
 * <code>acsStartJava alma.asdm.ASDMBrowser [directory]</code>
 * 
 * where <code>directory</code> is a path to the directory containing the XML
 * representation of the ASDM. Note that this path can also be set using the
 * graphical user interface of the application once it is started.
 * 
 * @author caillat
 */
public class ASDMBrowser extends JPanel {

	static private ASDM dataset = null;

	static private String datasetName = null;

	static JFrame topFrame = null;

	JEditorPane titlePane = null;

	JMenuBar menuBar = null;

	JMenu fileMenu = null;

	MenuActionListener mAL = null;

	JList list = null;

	ListSelectionModel listSelectionModel = null;

	JTable jTable = null;

	JPanel rightHalf = null;

	JPanel leftHalf = null;

	JPanel tableContainer = null;

	JSplitPane vSplitPane = null;

	JSplitPane hSplitPane = null;
	JPanel tablePanel = null;
	JScrollPane tablePane = null;

	JPanel valuePanel = null;
	JScrollPane valuePane = null;
	
	JTextPane textPane = null;
	JPopupMenu textPaneMenu = new JPopupMenu();
	JMenuItem i1 = new JMenuItem("copy");
	JMenuItem i2 = new JMenuItem("clear all");
	JMenuItem i3 = new JMenuItem("select all");

	JPopupMenu cellMenu = new JPopupMenu();
	JMenuItem i01 = new JMenuItem("copy");
	
	JScrollPane scrollTextPane = null;

	int lastSelectedRow = -1;

	int lastSelectedColumn = -1;

	JFrame arrayContent = null;
	
	Clipboard systemClipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
	StringSelection stsel = null;
	
	static public class LineNumberTable extends JTable {
		private JTable mainTable;
	 
		public LineNumberTable(JTable table)
		{
			super();
			mainTable = table;
			setAutoCreateColumnsFromModel( false );
			setModel( mainTable.getModel() );
			setSelectionModel( mainTable.getSelectionModel() );
			setAutoscrolls( false );

			addColumn( new TableColumn() );
			getColumnModel().getColumn(0).setCellRenderer(
				mainTable.getTableHeader().getDefaultRenderer() );
			getColumnModel().getColumn(0).setPreferredWidth(50);
			setPreferredScrollableViewportSize(getPreferredSize());
		}
	 
		public boolean isCellEditable(int row, int column)
		{
			return false;
		}
	 
		public Object getValueAt(int row, int column)
		{
			return new Integer(row);
		}
	 
		public int getRowHeight(int row)
		{
			return mainTable.getRowHeight();
		}
	}

	private String dimensions(Object o) {
		if (!o.getClass().getName().startsWith("["))
			return "";

		if (Array.getLength(o) == 0)
			return "";

		if (Array.get(o, 0) == null)
			return "??null??";

		if (!Array.get(o, 0).getClass().getName().startsWith("["))
			return "[" + Array.getLength(o) + "]";
		else
			return "[" + Array.getLength(o) + "]" + dimensions(Array.get(o, 0));
	}

	private String content(Object o) {
		StringBuffer result = new StringBuffer();
		result.append('[');
		if (!Array.get(o, 0).getClass().getName().startsWith("[")) {
			for (int i = 0; i < Array.getLength(o) - 1; i++) {
				result.append(Array.get(o, i).toString());
				result.append(", ");
			}
			result.append(Array.get(o, Array.getLength(o) - 1));

		} else {
			for (int i = 0; i < Array.getLength(o) - 1; i++) {
				result.append(content(Array.get(o, i)));
				result.append(", ");
			}
			result.append(content(Array.get(o, Array.getLength(o) - 1)));
		}
		result.append(']');
		return result.toString();
	}

	private void showArray(ASDMTable table, ASDMRow[] rows, int colIndex,
			int rowIndex) {
		StyledDocument doc = textPane.getStyledDocument();
		try {
			doc.insertString(doc.getLength(), table.getName() + "."
					+ table.getAttributesNames()[colIndex] + "#" + rowIndex
					+ "="
					+ content(rows[rowIndex].getAttributesValues()[colIndex])
					+ "\n", null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	private void showTextPane() {
		if (textPane == null) {
			textPane = new JTextPane();

			
			textPane.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON3) {
						textPaneMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
			);
			
			textPaneMenu.add(i1);
			textPaneMenu.add(i2);
			textPaneMenu.add(i3);
			i1.addActionListener(new ActionListener() {
				public void actionPerformed (ActionEvent h) {
					textPane.copy();
				}
			});
			
			i2.addActionListener(new ActionListener() {
				public void actionPerformed (ActionEvent h) {
					textPane.selectAll();
					textPane.cut();
				}
			});

			i3.addActionListener(new ActionListener() {
				public void actionPerformed (ActionEvent h) {
					textPane.selectAll();
				}
			});
			
			textPane.setBackground(Color.WHITE);
			//textPane.setPreferredSize(new Dimension(800, 300));
			scrollTextPane = new JScrollPane(textPane);
			scrollTextPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			scrollTextPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			hSplitPane.add(scrollTextPane);
		}
	}		
	
	private void showTable(final ASDMTable table) {
		// Build the tables content area.

		if (tablePanel == null) {
			tablePanel = new JPanel();
			tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.LINE_AXIS));
			tableContainer = new JPanel(new GridLayout(1, 1));					
		}		

		tableContainer.setBorder(BorderFactory.createTitledBorder(table
				.getName()));

		String[] columnNames = table.getAttributesNames();
		Object[][] columnValues = new Object[table.size()][];

		final ASDMRow[] spwR = table.getRows();
		for (int i = 0; i < spwR.length; i++) {
			columnValues[i] = spwR[i].getAttributesValues();
		}

		// Create a JTable out of the attributes names and values of table.
		jTable = new JTable(columnValues, columnNames);
		jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jTable.setPreferredScrollableViewportSize(new Dimension(500, 500));

		// Define the table's cells renderer.
		jTable.setDefaultRenderer(Object.class, new ASDMTableCellRenderer());

		// Define the table's selection mechanism.
		jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jTable.setCellSelectionEnabled(true);
		jTable.setColumnSelectionAllowed(true);
		ListSelectionModel colSM = jTable.getColumnModel().getSelectionModel();

		cellMenu.add(i01);
		i01.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent h) {
				StringBuffer sbf = new StringBuffer();
				Object value = jTable.getValueAt(lastSelectedRow, lastSelectedColumn);
				if (value == null)
					sbf.append("");
				else if (value.getClass().getName().startsWith("[")) {
					sbf.append(dimensions(value));
				} else {
					if (value.getClass().getName().equals(
							"alma.hla.runtime.asdm.types.ArrayTime"))
						sbf.append(((ArrayTime) value).toFITS());
					else if (value.getClass().getName().equals(
							"alma.hla.runtime.asdm.types.Interval"))
						sbf.append(Double.toString(((Interval) value)
								.getAsDouble(Interval.SECOND))
								+ "s");
					else if (value.getClass().getName().equals(
							"alma.hla.runtime.asdm.types.ArrayTimeInterval")) {
						ArrayTime start = ((ArrayTimeInterval) value).getStart();
						Interval duration = ((ArrayTimeInterval) value)
								.getDuration();
						sbf.append("start="
								+ start.toFITS()
								+ ", duration="
								+ Double.toString(duration
										.getAsDouble(Interval.SECOND)) + "s");
					} else
						sbf.append(value.toString());
				}

				stsel = new StringSelection(sbf.toString());
				systemClipBoard.setContents(stsel,stsel);
			}
		});
		
		jTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					if (lastSelectedRow != -1 && lastSelectedColumn != -1) {
						Rectangle cellPosition = jTable.getCellRect(lastSelectedRow, lastSelectedColumn, true);
						cellMenu.show(e.getComponent(), cellPosition.x + cellPosition.width/2, cellPosition.y + cellPosition.height/2);
					}
					return;
				}
			}
		});
		
		// A Selection listener when one clicks somewhere in a new column.
		colSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {				
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;
				int rowIndex = jTable.getSelectedRow();
				if (rowIndex < 0)
					return;

				int colIndex = jTable.getSelectedColumn();
				if ((rowIndex == lastSelectedRow)
						&& (colIndex == lastSelectedColumn))
					return;

				lastSelectedRow = rowIndex;
				lastSelectedColumn = colIndex;
				System.out.println(lastSelectedRow + "," + lastSelectedColumn);

				if (spwR[rowIndex].getAttributesValues()[colIndex].getClass()
						.getName().startsWith("["))
					showArray(table, spwR, colIndex, rowIndex);

			}
		});

		// A Selection listener when one clicks somewhere in a new row.
		ListSelectionModel rowSM = jTable.getSelectionModel();
		rowSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				int rowIndex = jTable.getSelectedRow();
				if (rowIndex < 0)
					return;

				int colIndex = jTable.getSelectedColumn();
				if ((rowIndex == lastSelectedRow)
						&& (colIndex == lastSelectedColumn))
					return;

				lastSelectedRow = rowIndex;
				lastSelectedColumn = colIndex;
				System.out.println(lastSelectedRow + "," + lastSelectedColumn);

				if (spwR[rowIndex].getAttributesValues()[colIndex].getClass()
						.getName().startsWith("["))
					showArray(table, spwR, colIndex, rowIndex);
			}
		});

		
		//
		// Create a scroll pane for the table content area and add the table
		// to it.
		if (tablePane == null) {
			tablePane = new JScrollPane(jTable);
			tableContainer.add(tablePane);
			tablePanel.add(tableContainer, BorderLayout.PAGE_START);
			tablePanel.setPreferredSize(new Dimension(800, 500));
			hSplitPane.add(tablePanel);		
		}
		else {
			tableContainer.remove(tablePane);
			tablePane = new JScrollPane(jTable);
			tableContainer.add(tablePane);
			tableContainer.setVisible(false);			
			tableContainer.setVisible(true);
		}
		
		JTable lineTable = new LineNumberTable( jTable );
		tablePane.setRowHeaderView( lineTable );
		
		showTextPane();
	}

	
	/**
	 * Creates the interface (list + table) to display the selected ASDM. Tables
	 * names are listed on the left part in a JList. Table contents are
	 * displayed on the right part in a JTable. When a the content of a cell is
	 * an array only its dimensions are diplayed in that cell; clicking on it
	 * displays the array's content in a popup text window.
	 */
	private void showASDM() {
		Object[] listData = dataset.getTables();

		// Build the tables list area.
		list = new JList(listData);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		listSelectionModel = list.getSelectionModel();
		listSelectionModel
				.addListSelectionListener(new SharedListSelectionHandler());
		JScrollPane listPane = new JScrollPane(list);

		// Do the layout.
		// A split pane to contain the two vertical parts.
		vSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		// add(vSplitPane, BorderLayout.CENTER);
		add(vSplitPane);

		// The left part.
		leftHalf = new JPanel();
		leftHalf.setLayout(new BoxLayout(leftHalf, BoxLayout.LINE_AXIS));
		JPanel listContainer = new JPanel(new GridLayout(1, 1));
		String[] dummy = datasetName.split("/");
		listContainer.setBorder(BorderFactory
				.createTitledBorder(dummy[dummy.length - 1]));
		listContainer.add(listPane);

		leftHalf.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
		leftHalf.add(listContainer);

		leftHalf.setMinimumSize(new Dimension(250, 50));
		// leftHalf.setMaximumSize(new Dimension(250, 50));
		leftHalf.setPreferredSize(new Dimension(250, 200));
		vSplitPane.add(leftHalf);

		rightHalf = new JPanel();

		// A split pane to contain the two horizontal parts.
		hSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		hSplitPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		rightHalf.add(hSplitPane);

		//vSplitPane.add(rightHalf);
		vSplitPane.add(hSplitPane);

		// The right part initially displaying the main table.
		this.showTable(dataset.getMain());

		// topFrame.validate();
		topFrame.setVisible(false);
		topFrame.pack();
		topFrame.setVisible(true);
		topFrame.validate();
	}

	/**
	 * Creates and displays a simple window containing some generic information
	 * about this application. It's displayed by the application as long as no
	 * ASDM dataset has been selected (using the File->Open menu item).
	 * 
	 */
	private void showTitle() {
		String title = "<HTML><BODY><TABLE WIDTH=\"100%\"><TR><TD ALIGN=\"LEFT\" STYLE=\"color:#ffffff; background-color: #222262; font-family:Arial; font-size:36px\">An ASDM</TD></TR><TR><TD ALIGN=\"LEFT\"  STYLE=\"background-color: #eeeeee; color: #222262; font-family:Arial; font-size:36px\">Browser</TD></TR></TABLE></BODY></HTML>";
		titlePane = new JEditorPane("text/html", title);
		add(titlePane);
	}

	/**
	 * Creates the toplevel part of the application's interface, i.e. menu +
	 * content.
	 * 
	 */
	public ASDMBrowser() {
		super(new BorderLayout());

		// A File menu and its items.
		// Create and set up the menu bar.
		// Prepare an ActionListener for the menu.
		mAL = new MenuActionListener();
		fileMenu = new JMenu("File");
		JMenuItem menuItemO = new JMenuItem("Open");
		menuItemO.addActionListener(mAL);
		fileMenu.add(menuItemO);
		JMenuItem menuItemQ = new JMenuItem("Quit");
		menuItemQ.addActionListener(mAL);
		fileMenu.add(menuItemQ);

		menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		topFrame.setJMenuBar(menuBar);

		if (dataset == null) {
			showTitle();
		} else {
			showASDM();
		}
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		// Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);

		// Create and set up the window.
		topFrame = new JFrame("ASDMBrowser");
		topFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		ASDMBrowser demo = new ASDMBrowser();
		demo.setOpaque(true);
		topFrame.setContentPane(demo);

		// Display the window.
		topFrame.pack();
		topFrame.setVisible(true);
	}

	/**
	 * The main method
	 * 
	 * @param args
	 *            one argument at maximum is expected that is the path to an
	 *            ASDM to browse as an alternative to selecting an ASDM using
	 *            the Filechooser of the application's interface.
	 * 
	 */
	public static void main(String[] args) {

		if (args.length >= 1) {
			datasetName = args[0];
			dataset = new ASDM();

			try {
				dataset.setFromFile(datasetName);
				System.out.println("Dataset read successfully.");
			} catch (ConversionException e1) {
				e1.printStackTrace();
				System.out.println("Aborting");
				System.exit(1);
			}
		}
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager
							.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
				} catch (Exception e) {
					UIManager.getCrossPlatformLookAndFeelClassName();
				}
				createAndShowGUI();
			}
		});
	}

	/**
	 * A class to listen at clicks in the tables list. Clicking on a table's
	 * name should display its content in the right part of the interface.
	 */
	class SharedListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {

			ListSelectionModel lsm = (ListSelectionModel) e.getSource();

			/*
			 * System.out.println("A table has been selected, index = " +
			 * list.getSelectedIndex());
			 */
			if (list.getSelectedIndex() == -1) {
				// No selection, do nothing.
				;
			} else {
				// Selection, display the selected table.
				showTable((ASDMTable) dataset.getTables()[list
						.getSelectedIndex()]);
			}
		}
	}

	/**
	 * A class to trigger action based on what item has been selected into one
	 * of the menus of the application's menubar.
	 * 
	 */
	class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("Quit".equals(e.getActionCommand()))
				System.exit(0);
			else if ("Open".equals(e.getActionCommand())) {
				JFileChooser fc = null;
				File currentDirectory;

				if (datasetName == null)
					currentDirectory = null;
				else {
					String dummy = new File(datasetName).getParent();
					if (dummy == null)
						currentDirectory = null;
					else
						currentDirectory = new File(dummy);
				}

				if (currentDirectory == null)
					fc = new JFileChooser();
				else
					fc = new JFileChooser(currentDirectory);

				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fc.showOpenDialog(ASDMBrowser.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					// Remove the current content (under the menu).
					if (datasetName == null) {
						ASDMBrowser.this.remove(titlePane);
					} else if (vSplitPane != null) {
						ASDMBrowser.this.remove(vSplitPane);
					}

					datasetName = fc.getSelectedFile().getAbsolutePath();
					try {
						dataset = new ASDM();
						dataset.setFromFile(datasetName);
						/* System.out.println("Dataset read successfully."); */
						showASDM();
					} catch (ConversionException e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage(),
								"Conversion error", JOptionPane.ERROR_MESSAGE);
					}

				} else {
					; // System.out.println("Cancelled directory selection.");
				}
			}

		}
	}

	/**
	 * A class in charge of rendering the cells of a table displayed in the
	 * right part of the interface :
	 * <ul>
	 * <li> a selected cell is displayed with a gray background
	 * <li> a unselected cell is displayed with a white or light blue background
	 * depending on the parity of its row number.
	 * </ul>
	 * 
	 */
	class ASDMTableCellRenderer extends JLabel implements TableCellRenderer {
		public ASDMTableCellRenderer() {
			this.setOpaque(true);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {

			if (isSelected) {
				setBackground(Color.GRAY);
			} else if (row % 2 == 1)
				setBackground(Color.decode("#cecee8"));
			else
				setBackground(Color.WHITE);

			if (value == null)
				setText("");
			else if (value.getClass().getName().startsWith("[")) {
				setText(dimensions(value));
			} else {
				if (value.getClass().getName().equals(
						"alma.hla.runtime.asdm.types.ArrayTime"))
					setText(((ArrayTime) value).toFITS());
				else if (value.getClass().getName().equals(
						"alma.hla.runtime.asdm.types.Interval"))
					setText(Double.toString(((Interval) value)
							.getAsDouble(Interval.SECOND))
							+ "s");
				else if (value.getClass().getName().equals(
						"alma.hla.runtime.asdm.types.ArrayTimeInterval")) {
					ArrayTime start = ((ArrayTimeInterval) value).getStart();
					Interval duration = ((ArrayTimeInterval) value)
							.getDuration();
					setText("start="
							+ start.toFITS()
							+ ", duration="
							+ Double.toString(duration
									.getAsDouble(Interval.SECOND)) + "s");
				} else
					setText(value.toString());
			}
			return this;
		}
	}

}
