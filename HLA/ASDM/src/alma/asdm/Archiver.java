package alma.asdm;

import alma.acs.container.ContainerServices;
import alma.bulkdata.BulkStoreHelper;
import alma.bulkdata.BulkStorePackage.BulkStoreException;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.Administrative;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.ArchiveInternalError;
import alma.xmlstore.Cursor;
import alma.xmlstore.Identifier;
import alma.xmlstore.Operational;
import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
import alma.xmlstore.CursorPackage.QueryResult;
import alma.xmlstore.IdentifierPackage.NotAvailable;
import alma.xmlstore.OperationalPackage.DirtyEntity;
import alma.xmlstore.OperationalPackage.IllegalEntity;
import alma.xmlstore.OperationalPackage.MalformedURI;
import alma.xmlstore.OperationalPackage.NotFound;

/**
 * 
 * @author caillat
 * 
 */
public class Archiver {
	private ContainerServices containerServices;

	private ArchiveConnection m_ConnArchiveComp;

	private Identifier m_identArchiveComp;

	private Operational m_operArchiveComp;

	Administrative m_admin;

	private alma.bulkdata.BulkStore ACSbulkstore;

	/**
	 * 
	 * Creates an Archiver, i.e. a connection to a database and a collection of
	 * methods to store, retrieve, update, list XML documents stored in this
	 * databse.
	 * 
	 * @param containerServices
	 *            the services of the container providing the components related
	 *            to the Archive.
	 * @throws Exception
	 */
	public Archiver(ContainerServices containerServices) throws Exception {

		this.containerServices = containerServices;
		ACSbulkstore = BulkStoreHelper.narrow(containerServices.getComponent("ARCHIVE_BULKSTORE"));

		m_ConnArchiveComp = alma.xmlstore.ArchiveConnectionHelper
				.narrow(containerServices.getComponent("ARCHIVE_CONNECTION"));
		m_operArchiveComp = m_ConnArchiveComp.getOperational("user");
		m_admin = m_ConnArchiveComp.getAdministrative("user");
		//m_admin.init();

		m_identArchiveComp = alma.xmlstore.IdentifierHelper
				.narrow(containerServices.getComponent("ARCHIVE_IDENTIFIER")
				);
	}

	/**
	 * Returns an Archive uid.
	 * 
	 * @return a string containing an Archive uid.
	 * @throws NotAvailable
	 */
	public String getID() throws NotAvailable {
		return m_identArchiveComp.getIdNamespace();
	}

	/**
	 * Returns an array of all the Archive uids of stored document having a
	 * given schema and verifying a given XPath expression.
	 * 
	 * @param query
	 *            an XPath expression
	 * @param schema
	 *            the name of a schema
	 * @return an array of strings containing Archive uids.
	 * @throws ArchiveException
	 */
	public String[] getUIDs(String query, String schema)
			throws ArchiveException {
		String[] result = null;

		try {
			result = m_operArchiveComp.queryUIDs(query, schema);
		} catch (ArchiveInternalError e) {
			throw new ArchiveException(
					"ArchiveException : could not get UIDs for query " + query
							+ " with schema " + schema + " - " + e.getMessage());
		}
		return result;
	}

	/**
	 * Returns a string containing pieces of XML code verifying an XPath
	 * expression in the documents stored in the Archive with a given schema.
	 * The maximum of number of results is defined by a parameter.
	 * 
	 * @param aQuery
	 *            an XPath expression
	 * @param schema
	 *            a schema's name
	 * @param maxRes
	 *            the maximum number of results to be returned
	 * @return a string obtained by concatenating all the results.
	 */
	public String query(String aQuery, String schema, int maxRes) {
		int nRes = 0;
		String result = "";

		try {
			Cursor cursor = m_operArchiveComp.query(aQuery, schema);

			while (cursor.hasNext() && (nRes < maxRes)) {
				QueryResult res = cursor.next();
				result += res.xml.replaceFirst(
						"<\\?xml version=\"1.0\" encoding=\"UTF\\-8\"\\?>", "");
				nRes += 1;
			}
		} catch (ArchiveInternalError e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean exists(String uid) throws ArchiveException {
		try {
			return m_operArchiveComp.exists(uid);
		}
		catch (MalformedURI e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} 
		catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} 
		catch (DirtyEntity e) {
			throw new ArchiveException("ArchiveException :" + e.getMessage());
		}
		catch (NotFound e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} 
	}
	
	/**
	 * Returns an XML document stored in the archive given its Archive uid.
	 * 
	 * @param uid
	 *            the Archive uid of the document to retrieve,
	 * @return the XML document.
	 * 
	 * @throws ArchiveException
	 */
	public String retrieve(String uid) throws ArchiveException {
		XmlEntityStruct struct = null;

		try {
			// System.out.println("About to retrieve UID "+uid);
			struct = m_operArchiveComp.retrieve(uid.trim());

		} catch (MalformedURI e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (NotFound e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (DirtyEntity e) {
			throw new ArchiveException("ArchiveException :" + e.getMessage());
		}
		return struct.xmlString;
	}

	/**
	 * Stores an XML document in the archive. The XML document is assumed to be
	 * conform with the schema whose name is passed as a parameter. Returns the
	 * Archive uid assigned to the archived document.
	 * 
	 * @param xmlString
	 *            the XML document to store
	 * @return a string containing the Archive uid of the stored document.
	 * 
	 * @throws ArchiveException
	 */

	public String store(String xmlString, String schema)
			throws ArchiveException {
		String id = null;
		XmlEntityStruct struct = new XmlEntityStruct();

		try {
			id = getID();
			struct.entityTypeName = schema;
			struct.schemaVersion = "1.0";
			struct.xmlString = xmlString;
			m_operArchiveComp.store(struct);
		} catch (IllegalEntity e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (NotAvailable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return id;
	}

	/**
	 * Stores an XML document in the archive. The XML document is assumed to be
	 * conform with the schema whose name is passed in argument.
	 * 
	 * @param xmlString
	 *            the XML document to store
	 * @param schema
	 *            the name of the schema
	 * @param archive
	 *            ID assigned to this document
	 * 
	 * @throws ArchiveException
	 */

	public void store(String xmlString, String schema, String uid)
			throws ArchiveException {
		XmlEntityStruct struct = new XmlEntityStruct();

		try {
			struct.entityId = uid;
			struct.entityTypeName = schema;
			struct.schemaVersion = "1.0";
			struct.xmlString = xmlString;
			m_operArchiveComp.store(struct);
		} catch (IllegalEntity e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		}
	}

	private void update(XmlEntityStruct struct) throws IllegalEntity,
			ArchiveInternalError {
		m_operArchiveComp.update(struct);
	}

	/**
	 * Updates a XML document archived with a given Archive uid.
	 * 
	 * @param uid
	 *            the Archive uid of the document to update,
	 * @param xmlString
	 *            the new content of the document.
	 * @throws ArchiveException
	 */
	public void update(String uid, String xmlString) throws ArchiveException {
		XmlEntityStruct struct;

		try {
			struct = this.m_operArchiveComp.retrieve(uid);
		} catch (MalformedURI e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (NotFound e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (DirtyEntity e) {
			throw new ArchiveException("ArchiveException :" + e.getMessage());
		}

		struct.xmlString = xmlString;

		try {
			this.m_operArchiveComp.update(struct);
		} catch (IllegalEntity e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		} catch (ArchiveInternalError e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		}
	}

	public void binStore (String uid, byte[] data) throws ArchiveException {
		try {
			ACSbulkstore.storeData(uid, "ASDMBinaryTable",data.length, data);
		}
		catch (BulkStoreException e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		}
		
	}
	
	public void binStore (String uid, String schemaName, byte[] data) throws ArchiveException {
		try {
			//ACSbulkstore.store(uid, data.length, data);
			ACSbulkstore.storeData(uid, schemaName, data.length, data);
		} catch (BulkStoreException e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		}
	}
	
	public byte[] binRetrieve(String uid) throws ArchiveException {
		byte[] result = null;
		try {
			result = ACSbulkstore.retrieve(uid);
		}
		catch (BulkStoreException e) {
			throw new ArchiveException("ArchiveException : " + e.getMessage());
		}
		return result;
	}
	
	public void done() throws ArchiveInternalError {
		//m_operArchiveComp.close();
		containerServices.releaseComponent("ARCHIVE_CONNECTION");
	}

	public void finalize() throws ArchiveInternalError {
		//m_operArchiveComp.close();
		containerServices.releaseComponent("ARCHIVE_CONNECTION");
	}
}