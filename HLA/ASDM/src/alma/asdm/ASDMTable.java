/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Nov 23, 2005
 * File ASDMTable.java
 */

package alma.asdm;

/**
 * A super (abstract) class for the tables of the ASDM.
 * 
 * A very simple abstract class to define methods common to all ASDM tables, 
 * mostly used by the ASDMDisplay application.
 * 
 * @author caillat
 *
 */
public abstract class ASDMTable {
    /**
     * Returns the rows of an ASDMTable as an array of ASDMRow.
     * @return an array of ASDMRow.
     */
    abstract ASDMRow[] getRows();
    
    /**
     * Returns the number of rows of an ASDMTable.
     * @return the number of rows of this table.
     */
    abstract int size();
    
    /**
     * Returns the names of the attributes of the rows stored in that table.
     * @return the attributes names as an array of String.
     */
    abstract String[] getAttributesNames();
    
    /**
     * Returns the name of that table.
     * @return the name of that table as a String.
     */
    abstract String getName();
}
