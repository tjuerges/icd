package alma.hla.runtime.enumerations;

/**
 * This Exception is thrown when the content of an XML fragment 
 * cannot be parsed into an object of one of the classes defined in alma.hla.runtime.enumerations.
 *  
 * @author caillat
 *
 */
public class XMLConversionException extends Exception {

	public XMLConversionException() {
		// TODO Auto-generated constructor stub
	}

	public XMLConversionException(String message) {
		super("XMLConversionException : cannot convert from IDL. "  + message);
		// TODO Auto-generated constructor stub
	}

	public XMLConversionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public XMLConversionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
