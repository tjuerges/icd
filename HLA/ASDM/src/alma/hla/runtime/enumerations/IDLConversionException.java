package alma.hla.runtime.enumerations;

/**
 * This Exception is thrown when the content of an IDL structure implemented in alma.hla.runtime.enumerations.enumerationsIDL
 * cannot be mapped onto an object of one of the classes defined in alma.hla.runtime.enumerations.
 *  
 * @author caillat
 *
 */
public class IDLConversionException extends Exception {

	public IDLConversionException() {
		// TODO Auto-generated constructor stub
	}

	public IDLConversionException(String message) {
		super("IDLConversionException : cannot convert from IDL. "  + message);
		// TODO Auto-generated constructor stub
	}

	public IDLConversionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IDLConversionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
