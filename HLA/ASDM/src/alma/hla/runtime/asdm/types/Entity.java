/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Entity.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLEntity;

/**
 * The Entity class is an identification of a persistant
 * entity in the ALMA archive.  It easily maps onto an EntityT
 * object in ACS system entities.
 * 
 * @version 1.00 Nov 22, 2004
 * @author Allen Farris
 */
public class Entity implements java.io.Serializable {

	static public Entity getEntity(StringTokenizer t) {
		try {
			String s = t.nextToken("<>");
			if (s.equals(" "))
				s = t.nextToken();
			Entity e = new Entity ();
			e.setFromXML(s);
			return e;
		} catch (NoSuchElementException e) {
		}
		return null;
	}

	// The internal representation of an Entity.
	private EntityId entityId;
	private String entityIdEncrypted;
	private String entityTypeName;
	private String entityVersion;
	private String instanceVersion;
	
	/**
	 * Create a default Entity whose internal parameters are null.
	 */
	public Entity() {
		entityId = null;
		entityIdEncrypted = null;
		entityTypeName = null;
		entityVersion = null;
		instanceVersion = null;
	}

	/**
	 * Create an Entity from the specified string (which is assumed
	 * to be produced by the toString method).
	 */
	public Entity(String s) {
		setFromXML(s);	
	}

	/**
	 * Create a Entity from an IDLEntity object.
	 * @param e The IDLEntity object.
	 */
	public Entity (IDLEntity e) {
		this.entityId = new EntityId(e.entityId);
		this.entityIdEncrypted = e.entityIdEncrypted;
		this.entityTypeName = e.entityTypeName;
		this.entityVersion = e.entityVersion;
		this.instanceVersion = e.instanceVersion;
	}
	
	/**
	 * Create an Entity whose initial value is the specified parameters.
	 */
	public Entity (String entityId, String entityIdEncrypted, String entityTypeName,
			String entityVersion, String instanceVersion) {
		this.entityId = new EntityId(entityId);
		this.entityIdEncrypted = entityIdEncrypted;
		this.entityTypeName = entityTypeName;
		this.entityVersion = entityVersion;
		this.instanceVersion = instanceVersion;
	}
	
	/**
	 * Return the entityId as a String.
	 * @return The entityId as a String.
	 */
	public String toString() {
		return toXML();
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		this.entityId.toBin(dos);
		dos.writeInt(this.entityIdEncrypted.length());
		dos.writeBytes(this.entityIdEncrypted);
		dos.writeInt(this.entityTypeName.length());
		dos.writeBytes(this.entityTypeName);
		dos.writeInt(this.entityVersion.length());
		dos.writeBytes(this.entityVersion);
		dos.writeInt(this.instanceVersion.length());
		dos.writeBytes(this.instanceVersion);
		
	}
	
	/**
	 * Read the binary representation of an Entity from a BODataInputStream
	 * and use the read value to set an Entity.
	 * @param dis the BODataInputStream to be read
	 * @return an Entity
	 * @throws IOException 
	 */
	public static Entity fromBin(BODataInputStream dis) throws IOException{
		StringBuffer sb = new StringBuffer();
		int len = 0;
		
		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());
		String entityId = null;
		if (len > 0) entityId = sb.toString(); 
		
		sb.delete(0, len);
		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());
		String entityIdEncrypted = null;
		if (len > 0)  entityIdEncrypted = sb.toString();
		
		sb.delete(0, len);
		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());
		String entityTypeName = null ;
		if (len > 0) entityTypeName = sb.toString();
		
		sb.delete(0, len);
		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());
		String entityVersion = null;
		if ( len > 0) entityVersion = sb.toString();
		
		sb.delete(0, len);
		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());
		String instanceVersion = null;
		if (len > 0) instanceVersion = sb.toString();
		
		return new Entity(entityId, entityIdEncrypted, entityTypeName, entityVersion, instanceVersion);
	}
	
	/**
	 * Return the values of this Entity as an XML-formated string.
	 * As an example, for the Main table in the ASDM, the toXML 
	 * method would give:
	 * <ul>
	 * <li>	&lt;Entity 
	 * <li>		entityId="uid://X79/X1/X2" 
	 * <li>		entityIdEncrypted="none" 
	 * <li>		entityTypeName="Main" 
	 * <li>		schemaVersion="1" 
	 * <li>		documentVersion="1"/&gt;
	 * </ul>
	 * 
	 * @return The values of this Entity as an XML-formated string.
	 * @throws IllegalStateException if the values of this Entity do not conform to the proper XML format.
	 */
	public String toXML() {
		String msg = validXML();
		if (msg != null) 
			throw new java.lang.IllegalStateException(msg);
		String s = "<Entity entityId=\"" + entityId +
			"\" entityIdEncrypted=\"" + entityIdEncrypted +
			"\" entityTypeName=\"" + entityTypeName + 
			"\" schemaVersion=\"" + entityVersion +
			"\" documentVersion=\"" + instanceVersion + "\"/>";
		return s;
	}
	
	/**
	 * Set the values of this Entity from an XML-formatted as described
	 * in the "toXML" method.
	 * Example:
	 * <ul>
	 * <li>	&lt;Entity 
	 * <li>		entityId="uid://X79/X0/X0" 
	 * <li>		entityIdEncrypted="none" 
	 * <li>		entityTypeName="Main" 
	 * <li>		schemaVersion="1" 
	 * <li>		documentVersion="1"/&gt;
	 * </ul>
	 * 
	 * @param xml An XML-formatted string as described in the "toXML" method.
	 * @throws IllegalArgumentException If the XML string is not a valid format.
	 */
	public void setFromXML(String xml) {
		String se = getXMLValue(xml,"entityId");
		entityId = new EntityId(se);
		//entityId = new EntityId(getXMLValue(xml,"entityId"));
		entityIdEncrypted = getXMLValue(xml,"entityIdEncrypted");
		entityTypeName = getXMLValue(xml,"entityTypeName");
		entityVersion = getXMLValue(xml,"schemaVersion");
		instanceVersion = getXMLValue(xml,"documentVersion");
		if (entityIdEncrypted == null ||
			entityTypeName == null ||
			entityVersion == null ||
			instanceVersion == null)
			throw new java.lang.IllegalArgumentException("Null values detected in Entity " + entityId);
	}
	
	private String getXMLValue(String xml, String parm) {
		int n = xml.indexOf(parm+"=");
		if (n == -1)
			return null;
		int beg = xml.indexOf('\"',n + parm.length()) + 1;
		if (beg == 0)
			return null;
		int end = xml.indexOf('\"',beg);
		if (end == -1)
			return null;
		return xml.substring(beg,end);
	}

	/**
	 * Check for a valid XML values.
	 * @return A non-null string if there are errors in the values
	 * of this Entity; a null string is returned if the Entity is valid.
	 */
	private String validXML() {
		// Check for any null values.
		if (entityId == null ||
			entityIdEncrypted == null ||
			entityTypeName == null ||
			entityVersion == null ||
			instanceVersion == null)
			return "Null values detected in Entity " + entityId;
		String msg = "Invalid format for EntityId: " + entityId;
		// Check the entityId for the correct format.
		return EntityId.validate(entityId.toString());
	}
	
	/**
	 * Return an IDL Entity object.
	 * @return An IDL Entity object.
	 */
	public IDLEntity toIDLEntity() {
		IDLEntity e = new IDLEntity ();
		e.entityId = this.entityId.toString();
		e.entityIdEncrypted = this.entityIdEncrypted;
		e.entityTypeName = this.entityTypeName;
		e.entityVersion = this.entityVersion;
		e.instanceVersion = this.instanceVersion;
		return e;
	}
	
	/**
	 * Return true if and only if the specified object o is an
	 * Entity and its value is equal to this Entity.
	 * @param o The object to which this Entity is being compared.
	 * @return true if and only if the specified object o is an
	 * Entity and its value is equal to this Entity.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Entity))
			return false;
		Entity e = (Entity)o;
		return 	this.entityId == e.entityId &&
				this.entityIdEncrypted == e.entityIdEncrypted &&
				this.entityTypeName == e.entityTypeName &&
				this.entityVersion == e.entityVersion &&
				this.instanceVersion == e.instanceVersion;
	}
	
	/**
	 * Return true if and only if the entityId is null.
	 * @return True if and only if the entityId is null.
	 */
	public boolean isNull() {
		return entityId == null;
	}
	
	// Getters and setters.
	
	/**
	 * @return Returns the entityId.
	 */
	public EntityId getEntityId() {
		return entityId;
	}

	/**
	 * @param entityId The entityId to set.
	 */
	public void setEntityId(EntityId entityId) {
		this.entityId = new EntityId(entityId.toString());
	}

	/**
	 * @return Returns the entityIdEncrypted.
	 */
	public String getEntityIdEncrypted() {
		return entityIdEncrypted;
	}

	/**
	 * @param entityIdEncrypted The entityIdEncrypted to set.
	 */
	public void setEntityIdEncrypted(String entityIdEncrypted) {
		this.entityIdEncrypted = entityIdEncrypted;
	}

	/**
	 * @return Returns the entityTypeName.
	 */
	public String getEntityTypeName() {
		return entityTypeName;
	}

	/**
	 * @param entityTypeName The entityTypeName to set.
	 */
	public void setEntityTypeName(String entityTypeName) {
		this.entityTypeName = entityTypeName;
	}

	/**
	 * @return Returns the entityVersion.
	 */
	public String getEntityVersion() {
		return entityVersion;
	}

	/**
	 * @param entityVersion The entityVersion to set.
	 */
	public void setEntityVersion(String entityVersion) {
		this.entityVersion = entityVersion;
	}

	/**
	 * @return Returns the instanceVersion.
	 */
	public String getInstanceVersion() {
		return instanceVersion;
	}

	/**
	 * @param instanceVersion The instanceVersion to set.
	 */
	public void setInstanceVersion(String instanceVersion) {
		this.instanceVersion = instanceVersion;
	}

}
