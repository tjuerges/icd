/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AngularRate.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLAngularRate;

/**
 * The AngularRate class implements a concept of a angular speed in radians per second.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class AngularRate implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new AngularRate that is the sum of the 
	 * specified AngularRates, a +  b.
	 * @param a The first AngularRate of the pair.
	 * @param b The second AngularRate of the pair.
	 * @return A new AngularRate that is the sum of the 
	 * specified AngularRate, a +  b.
	 */
	static public AngularRate add (AngularRate a, AngularRate b) {
		return new AngularRate (a.rate + b.rate);
	}
	
	/**
	 * Return a new AngularRate that is the difference of the 
	 * specified AngularRates, a - b.
	 * @param a The first AngularRate of the pair.
	 * @param b The second AngularRate of the pair.
	 * @return A new AngularRate that is the difference of the 
	 * specified AngularRates, a - b.
	 */
	static public AngularRate sub (AngularRate a, AngularRate b) {
		return new AngularRate (a.rate - b.rate);
	}
	
	/**
	 * Return a new AngularRate that is the product of a specified 
	 * angular rate and some specified factor, a * factor.
	 * @param a The base angular rate
	 * @param factor The factor that is used to multiply the value.
	 * @return A new AngularRate that is the product of a specified 
	 * angular rate and some specified factor, a * factor.
	 */
	static public AngularRate mult (AngularRate a, double factor) {
		return new AngularRate (a.rate * factor);
	}
	
	/**
	 * Return a new AngularRate that is a specified angular rate divided by a 
	 * specified factor, a / factor.
	 * @param a The base angular rate
	 * @param factor The factor that is used to divide the value.
	 * @return A new AngularRate that is the product of a specified 
	 * angular rate and some specified factor, a / factor.
	 */
	static public AngularRate div (AngularRate a, double factor) {
		return new AngularRate (a.rate / factor);
	}
	
	static public AngularRate getAngularRate(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new AngularRate (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The angular rate in meters per second.
	 */
	private double rate;
	
	/**
	 * Create a AngularRate of zero meters per second.
	 */
	public AngularRate() {
	}

	/**
	 * Create a AngularRate whose initial value is a string designating the angular rate in meters per second
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the angular rate in meters per second
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public AngularRate (String s) {
		rate = Double.parseDouble(s);
	}
	
	/**
	 * Create a AngularRate whose initial value is the same as the specified angular rate.
	 * @param x The specified angular rate that is used as the initial value of this angular rate.
	 */
	public AngularRate (AngularRate x) {
		this.rate = x.rate;
	}
	
	/**
	 * Create a AngularRate from an IDL angular rate object.
	 * @param x The IDL angular rate object.
	 */
	public AngularRate (IDLAngularRate x) {
		this.rate = x.value;
	}
	
	/**
	 * Create a AngularRate whose initial value is the specified angular rate in meters per second.
	 * @param rate The angular rate in meters per second.
	 */
	public AngularRate (double rate) {
		this.rate = rate;
	}
	
	/**
	 * Return the value of this angular rate in meters per second.
	 * @return The value of this angular rate in meters per second.
	 */
	public double get() {
		return rate;
	}
	
	/**
	 * Set the value of this angular rate to the specified angular rate in meters per second.
	 * @param rate The specified angular rate in meters per second.
	 */
	public void set(double rate) {
		this.rate = rate;
	}
	
	/**
	 * Return the value of this as a String in units of meters per second.
	 * @return The value of this as a String in units of meters per second.
	 * 
	 */
	public String toString() {
		return Double.toString(rate);
	}
	
	/**
	 * Return an array of double containing the values of the array of AngularRate objects passed in argument.
	 */
	static public double[] values(AngularRate[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(rate);
	}

	/**
	 * Write the binary representation of a 1D array of angularRate into a DataOutputStream.
	 * @param angularRate the array of AngularRate to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(AngularRate[] angularRate, DataOutputStream dos) throws IOException {
		dos.writeInt(angularRate.length);
		for (int i = 0; i < angularRate.length; i++)
			angularRate[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of AngularRate into a DataOutputStream.
	 * @param angularRate the 2D array of AngularRate to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(AngularRate[][] angularRate, DataOutputStream dos) throws IOException {
		dos.writeInt(angularRate.length);
		dos.writeInt(angularRate[0].length);
		for (int i = 0; i < angularRate.length; i++)
			for (int j=0; j < angularRate[0].length; j++)
				angularRate[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of AngularRate into a DataOutputStream.
	 * @param angularRate the 3D array of AngularRate to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(AngularRate[][][] angularRate, DataOutputStream dos) throws IOException {
		dos.writeInt(angularRate.length);
		dos.writeInt(angularRate[0].length);
		dos.writeInt(angularRate[0][0].length);
		for (int i = 0; i < angularRate.length; i++)
			for (int j=0; j < angularRate[0].length; j++)
				for (int k = 0; k < angularRate[0][0].length; k++)
					angularRate[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an AngularRate from a BODataInputStream
	 * and use the read value to set an  AngularRate.
	 * @param dis the BODataInputStream to be read
	 * @return an AngularRate
	 * @throws IOException
	 */
	public static AngularRate fromBin(BODataInputStream dis) throws IOException {
		AngularRate angularRate = new AngularRate();
		angularRate.set(dis.readDouble());
		return angularRate;
	}
	
	/**
	 * Read the binary representation of a 1D array of  AngularRate from a BODataInputStream
	 * and use the read value to set a 1D array of  AngularRate.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of AngularRate
	 * @throws IOException
	 */
	public static AngularRate[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		AngularRate angularRate[] = new AngularRate[dim1];
		for (int i = 0; i < dim1; i++) 
			angularRate[i] = AngularRate.fromBin(dis);
		return angularRate;
	}
	
	/**
	 * Read the binary representation of a 2D array of  AngularRate from a BODataInputStream
	 * and use the read value to set a 2D array of  AngularRate.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of AngularRate
	 * @throws IOException
	 */
	public static AngularRate[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		AngularRate[][] angularRate = new AngularRate[dim1][dim2];
		angularRate = new AngularRate[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				angularRate[i][j] = AngularRate.fromBin(dis);
		return angularRate;
	}
	
	/**
	 * Read the binary representation of a 3D array of  AngularRate from a BODataInputStream
	 * and use the read value to set a 3D array of  AngularRate.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  AngularRate
	 * @throws IOException
	 */
	public static AngularRate[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		AngularRate[][][] angularRate = new AngularRate[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					angularRate[i][j][k] = AngularRate.fromBin(dis);
		return angularRate;
	}	
	
	/**
	 * Return an IDL AngularRate object.
	 * @return An IDL AngularRate object.
	 */
	public IDLAngularRate toIDLAngularRate() {
		return new IDLAngularRate(rate);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * AngularRate and its value is equal to this angular rate.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * AngularRate and its value is equal to this angular rate.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof AngularRate))
			return false;
		return this.rate == ((AngularRate)o).rate;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this angular rate is zero.
	 * @return True if and only if this angular rate is zero.
	 */
	public boolean isZero() {
		return rate == 0.0;
	}
	
	/**
	 * Compare this AngularRate to the specified Object, which must be
	 * a AngularRate, returning -1, 0, or +1 if this angular rate is less 
	 * than, equal to, or greater than the specified AngularRate.
	 * @param o The Object to which this AngularRate is being compared.
	 * @return -1, 0, or +1 if this angular rate is less than, equal to, or 
	 * greater than the specified object, which must be a AngularRate.
	 * @throws IllegalArgumentException If the object being compared is not a AngularRate.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof AngularRate))
			throw new IllegalArgumentException("Attempt to compare a AngularRate to a non-AngularRate.");
		if (rate < ((AngularRate)o).rate)
			return -1;
		if (rate > ((AngularRate)o).rate)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this AngularRate is equal to the specified 
	 * AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is equal to the specified AngularRate.
	 */
	public boolean eq(AngularRate t) {
		return rate == t.rate; 
	}
	
	/**
	 * Return true if and only if this AngularRate is not equal to the specified 
	 * AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is not equal to the specified AngularRate.
	 */
	public boolean ne(AngularRate t) {
		return rate != t.rate; 
	}
	
	/**
	 * Return true if and only if this AngularRate is less than the specified 
	 * AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is less than the specified AngularRate.
	 */
	public boolean lt(AngularRate t) {
		return rate < t.rate; 
	}
	
	/**
	 * Return true if and only if this AngularRate is less than or equal to
	 * the specified AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is less than or equal to the 
	 * specified AngularRate.
	 */
	public boolean le(AngularRate t) {
		return rate <= t.rate; 
	}
	
	/**
	 * Return true if and only if this AngularRate is greater than the specified 
	 * AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is greater than the specified AngularRate.
	 */
	public boolean gt(AngularRate t) {
		return rate > t.rate; 
	}
	
	/**
	 * Return true if and only if this AngularRate is greater than or equal to
	 * the specified AngularRate.
	 * @param t The AngularRate to which this AngularRate is being compared.
	 * @return True, if and only if this AngularRate is greater than or equal to the 
	 * specified AngularRate.
	 */
	public boolean ge(AngularRate t) {
		return rate >= t.rate; 
	}
	
	/**
	 * Add a specified AngularRate to this angular rate, (this + x).
	 * @param x The AngularRate to be added to this angular rate.
	 * @return This angular rate after the addition.
	 */
	public AngularRate add (AngularRate x) {
		rate += x.rate;
		return this;
	}
	
	/**
	 * Subtract a specified AngularRate from this angular rate, (this - x).
	 * @param x The AngularRate to be subtracted to this angular rate.
	 * @return This angular rate after the subtraction.
	 */
	public AngularRate sub (AngularRate x) {
		rate -= x.rate;
		return this;
	}
	
	/**
	 * Multiply this angular rate by some factor, (this * factor).
	 * @param factor The factor by which this angular rate is to be multiplied.
	 * @return This angular rate after the multiplication.
	 */
	public AngularRate mult (double factor) {
		rate *= factor;
		return this;
	}
	
	/**
	 * Divide this angular rate by some factor, (this / factor).
	 * @param factor The factor by which this angular rate is to be divided.
	 * @return This angular rate after the division.
	 */
	public AngularRate div (double factor) {
		rate /= factor;
		return this;
	}
	
}
