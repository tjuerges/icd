/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Frequency.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLFrequency;

/**
 * The Frequency class implements a concept of a frequency in hertz.
 *  
 * @version 1.00 Nov 17, 2004
 * @author Allen Farris
 */
public class Frequency implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Hertz, Hz, an allowed unit of frequency.
	 */
	static public final String HERTZ 		= "Hz";
	
	/**
	 * Kilohertz, kHz, an allowed unit of frequency.
	 */
	static public final String KILOHERTZ 	= "kHz";
	
	/**
	 * Megahertz, MHz, an allowed unit of frequency.
	 */
	static public final String MEGAHERTZ 	= "MHz";
	
	/**
	 * Gigahertz, GHz, an allowed unit of frequency.
	 */
	static public final String GIGAHERTZ 	= "GHz";
	
	/**
	 * An array of strings containing the allowable units of frequency.
	 */
	static public final String[] AllowedUnits = { HERTZ, KILOHERTZ, MEGAHERTZ, GIGAHERTZ }; 
	
	/**
	 * Return the canonical unit associated with this Frequency, hertz.
	 * @return The unit associated with this Frequency.
	 */
	static public String unit() {
		return HERTZ;
	}
	
	/**
	 * Return a new Frequency that is the sum of the 
	 * specified Frequencies, f1 +  f2.
	 * @param f1 The first Frequency of the pair.
	 * @param f2 The second Frequency of the pair.
	 * @return A new Frequency that is the sum of the 
	 * specified Frequencies, f1 +  f2.
	 */
	static public Frequency add (Frequency f1, Frequency f2) {
		return new Frequency (f1.frequency + f2.frequency);
	}
	
	/**
	 * Return a new Frequency that is the difference of the 
	 * specified frequencies, f1 - f2.
	 * @param f1 The first Frequency of the pair.
	 * @param f2 The second Frequency of the pair.
	 * @return A new Frequency that is the difference of the 
	 * specified frequencies, f1 - f2.
	 */
	static public Frequency sub (Frequency f1, Frequency f2) {
		return new Frequency (f1.frequency - f2.frequency);
	}
	
	/**
	 * Return a new Frequency that is the product of a specified 
	 * frequency and some specified factor, f * factor.
	 * @param f The base frequency
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Frequency that is the product of a specified 
	 * frequency and some specified factor, f * factor.
	 */
	static public Frequency mult (Frequency f, double factor) {
		return new Frequency (f.frequency * factor);
	}
	
	/**
	 * Return a new Frequency that is a specified frequency divided by a 
	 * specified factor, f / factor.
	 * @param f The base frequency
	 * @param factor The factor that is used to divide the value.
	 * @return A new Frequency that is the product of a specified 
	 * frequency and some specified factor, f / factor.
	 */
	static public Frequency div (Frequency f, double factor) {
		return new Frequency (f.frequency / factor);
	}
	
	static public Frequency getFrequency(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Frequency (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The frequency in hertz.
	 */
	private double frequency;
	
	/**
	 * Create a Frequency of zero hertz.
	 */
	public Frequency() {
	}

	/**
	 * Create a Frequency whose initial value is a string designating the frequency in hertz
	 * as a double precision number in the standard Java notation (for example, "31500000000.0").
	 * @param s A string designating the frequency in hertz
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Frequency (String s) {
		frequency = Double.parseDouble(s);
	}
	
	/**
	 * Create a Frequency whose initial value is a string in the specified units.
	 * @param value The initial value of the frequency in the specified units.
	 * @param units A string specifying the unit of frequency, which must be one
	 * of the allowable units of frequency.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Frequency(String value, String units) {
		set(value,units);	
	}
	
	/**
	 * Create a Frequency whose initial value is the same as the specified frequency.
	 * @param f The specified frequency that is used as the initial value of this frequency.
	 */
	public Frequency (Frequency f) {
		this.frequency = f.frequency;
	}
	
	/**
	 * Create a Frequency from an IDL frequency object.
	 * @param f The IDL frequency object.
	 */
	public Frequency (IDLFrequency f) {
		this.frequency = f.value;
	}
	
	/**
	 * Create a Frequency whose initial value is the specified frequency in hertz,
	 * for example 31500000000.0.
	 * @param frequency The frequency in hertz.
	 */
	public Frequency (double frequency) {
		this.frequency = frequency;
	}
	
	/**
	 * Create a Frequency whose initial value is a double in the specified units.
	 * @param value The initial value of the frequency in the specified units.
	 * @param units A string specifying the unit of frequency, which must be one
	 * of the allowable units of frequency.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Frequency(double value, String units) {
		set(value,units);
	}
	
	/**
	 * Return the value of this frequency in hertz.
	 * @return The value of this frequency in hertz.
	 */
	public double get() {
		return frequency;
	}
	
	/**
	 * Return the value of this frequency as a double in the specified units. 
	 * @param units The units in which the value of this frequency is to be returned.
	 * @return The value of this frequency as a double in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public double get(String units) {
		if (units.equals(HERTZ))
			return frequency;
		else if (units.equals(KILOHERTZ))
			return frequency / 1000.0;
		else if (units.equals(MEGAHERTZ))
			return frequency / 1000000.0;
		else if (units.equals(GIGAHERTZ))
			return frequency / 1000000000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Set the value of this frequency to the specified frequency in hertz.
	 * @param frequency The specified frequency in hertz.
	 */
	public void set(double frequency) {
		this.frequency = frequency;
	}
	
	/**
	 * Set the value of this frequency to the specified string in the specified units.
	 * @param value The value to which this frequency is to be set. 
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(String value, String units) {
		if (units.equals(HERTZ))
			frequency = Double.parseDouble(value);
		else if (units.equals(KILOHERTZ))
			frequency = Double.parseDouble(value) * 1000.0;
		else if (units.equals(MEGAHERTZ))
			frequency = Double.parseDouble(value) * 1000000.0;
		else if (units.equals(GIGAHERTZ))
			frequency = Double.parseDouble(value) * 1000000000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");	
	}
	
	/**
	 * Set the value of this frequency to the specified double in the specified units.
	 * @param frequency The value to which this frequency is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(double frequency, String units) {
		if (units.equals(HERTZ))
			this.frequency = frequency;
		else if (units.equals(KILOHERTZ))
			this.frequency = frequency * 1000.0;
		else if (units.equals(MEGAHERTZ))
			this.frequency = frequency * 1000000.0;
		else if (units.equals(GIGAHERTZ))
			this.frequency = frequency * 1000000000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Return the value of this Frequency as a String in units of hertz,
	 * for example, "31500000000.0".
	 */
	public String toString() {
		return Double.toString(frequency);
	}
	
	/**
	 * Return the value of this frequency as a String in the specified units. 
	 * @param units The units in which the value of this frequency is to be displayed.
	 * @return The value of this frequency as a String in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public String toString(String units) {
		if (units.equals(HERTZ))
			return Double.toString(frequency);
		else if (units.equals(KILOHERTZ))
			return Double.toString(frequency / 1000.0);
		else if (units.equals(MEGAHERTZ))
			return Double.toString(frequency / 1000000.0);
		else if (units.equals(GIGAHERTZ))
			return Double.toString(frequency / 1000000000.0);
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Return an array of double containing the values of the array of Frequency objects passed in argument.
	 */
	static public double[] values(Frequency[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}

	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(frequency);
	}
	
	/**
	 * Write the binary representation of a 1D array of frequency into a DataOutputStream.
	 * @param frequency the array of Frequency to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Frequency[] frequency, DataOutputStream dos) throws IOException {
		dos.writeInt(frequency.length);
		for (int i = 0; i < frequency.length; i++)
			frequency[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Frequency into a DataOutputStream.
	 * @param frequency the 2D array of Frequency to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Frequency[][] frequency, DataOutputStream dos) throws IOException {
		dos.writeInt(frequency.length);
		dos.writeInt(frequency[0].length);
		for (int i = 0; i < frequency.length; i++)
			for (int j=0; j < frequency[0].length; j++)
				frequency[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Frequency into a DataOutputStream.
	 * @param frequency the 3D array of Frequency to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Frequency[][][] frequency, DataOutputStream dos) throws IOException {
		dos.writeInt(frequency.length);
		dos.writeInt(frequency[0].length);
		dos.writeInt(frequency[0][0].length);
		for (int i = 0; i < frequency.length; i++)
			for (int j=0; j < frequency[0].length; j++)
				for (int k = 0; k < frequency[0][0].length; k++)
					frequency[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Frequency from a BODataInputStream
	 * and use the read value to set an  Frequency.
	 * @param dis the BODataInputStream to be read
	 * @return an Frequency
	 * @throws IOException
	 */
	public static Frequency fromBin(BODataInputStream dis) throws IOException {
		Frequency frequency = new Frequency();
		frequency.set(dis.readDouble());
		return frequency;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Frequency from a BODataInputStream
	 * and use the read value to set a 1D array of  Frequency.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Frequency
	 * @throws IOException
	 */
	public static Frequency[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Frequency frequency[] = new Frequency[dim1];
		for (int i = 0; i < dim1; i++) 
			frequency[i] = Frequency.fromBin(dis);
		return frequency;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Frequency from a BODataInputStream
	 * and use the read value to set a 2D array of  Frequency.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Frequency
	 * @throws IOException
	 */
	public static Frequency[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Frequency[][] frequency = new Frequency[dim1][dim2];
		frequency = new Frequency[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				frequency[i][j] = Frequency.fromBin(dis);
		return frequency;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Frequency from a BODataInputStream
	 * and use the read value to set a 3D array of  Frequency.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Frequency
	 * @throws IOException
	 */
	public static Frequency[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Frequency[][][] frequency = new Frequency[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					frequency[i][j][k] = Frequency.fromBin(dis);
		return frequency;
	}
	
	/**
	 * Return an IDL Frequency object.
	 * @return An IDL Frequency object.
	 */
	public IDLFrequency toIDLFrequency() {
		return new IDLFrequency(frequency);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Frequency and its value is equal to this frequency.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Frequency and its value is equal to this frequency.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Frequency))
			return false;
		return this.frequency == ((Frequency)o).frequency;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this frequency is zero.
	 * @return True if and only if this frequency is zero.
	 */
	public boolean isZero() {
		return frequency == 0.0;
	}
	
	/**
	 * Compare this Frequency to the specified Object, which must be
	 * a Frequency, returning -1, 0, or +1 if this frequency is less 
	 * than, equal to, or greater than the specified Frequency.
	 * @param o The Object to which this Frequency is being compared.
	 * @return -1, 0, or +1 if this frequency is less than, equal to, or 
	 * greater than the specified object, which must be a Frequency.
	 * @throws IllegalArgumentException If the object being compared is not a Frequency.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Frequency))
			throw new IllegalArgumentException("Attempt to compare a Frequency to a non-Frequency.");
		if (frequency < ((Frequency)o).frequency)
			return -1;
		if (frequency > ((Frequency)o).frequency)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Frequency is equal to the specified 
	 * Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is equal to the specified Frequency.
	 */
	public boolean eq(Frequency t) {
		return frequency == t.frequency; 
	}
	
	/**
	 * Return true if and only if this Frequency is not equal to the specified 
	 * Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is not equal to the specified Frequency.
	 */
	public boolean ne(Frequency t) {
		return frequency != t.frequency; 
	}
	
	/**
	 * Return true if and only if this Frequency is less than the specified 
	 * Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is less than the specified Frequency.
	 */
	public boolean lt(Frequency t) {
		return frequency < t.frequency; 
	}
	
	/**
	 * Return true if and only if this Frequency is less than or equal to
	 * the specified Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is less than or equal to the 
	 * specified Frequency.
	 */
	public boolean le(Frequency t) {
		return frequency <= t.frequency; 
	}
	
	/**
	 * Return true if and only if this Frequency is greater than the specified 
	 * Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is greater than the specified Frequency.
	 */
	public boolean gt(Frequency t) {
		return frequency > t.frequency; 
	}
	
	/**
	 * Return true if and only if this Frequency is greater than or equal to
	 * the specified Frequency.
	 * @param t The Frequency to which this Frequency is being compared.
	 * @return True, if and only if this Frequency is greater than or equal to the 
	 * specified Frequency.
	 */
	public boolean ge(Frequency t) {
		return frequency >= t.frequency; 
	}
	
	/**
	 * Add a specified Frequency to this frequency, (this + x).
	 * @param x The Frequency to be added to this frequency.
	 * @return This frequency after the addition.
	 */
	public Frequency add (Frequency x) {
		frequency += x.frequency;
		return this;
	}
	
	/**
	 * Subtract a specified Frequency from this frequency, (this - x).
	 * @param x The Frequency to be subtracted to this frequency.
	 * @return This frequency after the subtraction.
	 */
	public Frequency sub (Frequency x) {
		frequency -= x.frequency;
		return this;
	}
	
	/**
	 * Multiply this frequency by some factor, (this * factor).
	 * @param factor The factor by which this frequency is to be multiplied.
	 * @return This frequency after the multiplication.
	 */
	public Frequency mult (double factor) {
		frequency *= factor;
		return this;
	}
	
	/**
	 * Divide this frequency by some factor, (this / factor).
	 * @param factor The factor by which this frequency is to be divided.
	 * @return This frequency after the division.
	 */
	public Frequency div (double factor) {
		frequency /= factor;
		return this;
	}
		
}
