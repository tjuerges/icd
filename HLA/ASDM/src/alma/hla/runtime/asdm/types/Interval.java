/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Interval.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLInterval;

/**
 * The Interval class implements a concept of an interval
 * of time in units of nanoseconds.
 * 
 * @version 1.10 Dec 14, 2004
 * @author Allen Farris
 */
public class Interval implements Cloneable, Comparable, java.io.Serializable {
	
	/**
	 * Day, day, an allowed unit of an interval of time.
	 */
	static public final String DAY 					 = "day";
	
	/**
	 * Hout, hour, an allowed unit of an interval of time.
	 */
	static public final String HOUR 				 = "hour";
	
	/**
	 * MInute, min, an allowed unit of an interval of time.
	 */
	static public final String MINUTE 				 = "min";
	
	/**
	 * Second, sec, an allowed unit of an interval of time.
	 */
	static public final String SECOND 				 = "sec";
	
	/**
	 * Millisecond, millisec, an allowed unit of an interval of time.
	 */
	static public final String MILLISECOND 			 = "millisec";
	
	/**
	 * Microsecond, microsec, an allowed unit of an interval of time.
	 */
	static public final String MICROSECOND			 = "microsec";
	
	/**
	 * Nanoseconds, nanosec, an allowed unit of an interval of time.
	 */
	static public final String NANOSECOND 			= "nanosec";
	
	/**
	 * An array of strings containing the allowable units of an interval of time.
	 */
	static public final String[] AllowedUnits = { DAY, HOUR, MINUTE, SECOND, MILLISECOND, 
			MICROSECOND, NANOSECOND}; 
	
	/**
	 * Return the canonical unit associated with this Interval of time, 
	 * nanoseconds.
	 * @return The units associated with this Interval of time.
	 */
	static public String unit() {
		return NANOSECOND;
	}
	
	/**
	 * Return a new Interval of time whose value is the sum of
	 * the two specified intervals, "t1 + t2". 
	 * @param t1 The first Interval of the pair to be added.
	 * @param t2 The second Interval of the pair to be added.
	 * @return a new Interval of time whose value is the sum of
	 * the two specified intervals, "t1 + t2".
	 */
	static public Interval add (Interval t1, Interval t2) {
		return new Interval (t1.time + t2.time);
	}

	/**
	 * Return a new Interval of time whose value is the difference of
	 * the two specified intervals, "t1 - t2". 
	 * @param t1 The first Interval of the pair to be subtracted.
	 * @param t2 The second Interval of the pair to be subtracted.
	 * @return a new Interval of time whose value is the difference of
	 * the two specified intervals, "t1 - t2".
	 */
	static public Interval sub (Interval t1, Interval t2) {
		return new Interval (t1.time - t2.time);
	}
	
	/**
	 * Return a new Interval of time that is the product of a specified 
	 * interval and some specified factor, t * factor.
	 * @param t The base interval
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Interval of time that is the product of a specified 
	 * interval and some specified factor, t * factor.
	 */
	static public Interval mult (Interval t, long factor) {
		return new Interval (t.time * factor);
	}
	
	/**
	 * Return a new Interval of time that is a specified 
	 * interval divided by a specified factor, t / factor.
	 * @param t The base interval
	 * @param factor The factor that is used to divide the value.
	 * @return A new Interval of time that is a specified 
	 * interval divided by a specified factor, t / factor.
	 */
	static public Interval div (Interval t, long factor) {
		return new Interval (t.time / factor);
	}
	
	static public Interval getInterval(StringTokenizer t) {
		try {
			long value = Long.parseLong(t.nextToken());
			return new Interval (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The length of this interval of time, in units of nanoseconds.
	 */
	private long time;

	/**
	 * Create an Interval of time initialized to zero.
	 */
	public Interval() {
		time = 0L;
	}
	
	/**
	 * Create an Interval of time from the specified String, which is assumed
	 * to be a long in units of nanoseconds.
	 * @param s The length of the interval, a long in units of nanoseconds.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable long.
	 */
	public Interval (String s) {
		time = Long.parseLong(s);
	}

	/**
	 * Create an Interval of time whose initial value is a string in the specified units.
	 * The intial value may be expressed either as an interger or a double, but if it is
	 * an integer, it must be a parseable long, for example "4609756965670000000".
	 * @param value The initial value of the interval of time in the specified units.
	 * @param units A string specifying the unit of the interval of time, which must be one
	 * of the allowable units.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * either a parseable long or double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Interval(String value, String units) {
		set(value,units);
	}

	/**
	 * Create an Interval of time initialized to the specified Interval
	 * @param t The Interval used to specify the initial value of this Interval.
	 */
	public Interval (Interval t) {
		time = t.time;
	}
	
	/**
	 * Create an Interval from an IDL time object.
	 * @param t The IDL time object.
	 */
	public Interval (IDLInterval t) {
		this.time = t.value;
	}
	
	/**
	 * Create an Interval of time initialized to the spcified number 
	 * of nanoseconds, for example 4609756965670000000L.
	 * @param nanoseconds The intitial value of this interval in 
	 * units of nanoseconds.
	 */
	public Interval (long nanoseconds) {
		time = nanoseconds;
	}
	
	/**
	 * Create an Interval of time whose initial value is a double in the specified units.
	 * @param value The initial value of the interval of time in the specified units.
	 * @param units A string specifying the unit of the interval of time, which must be one
	 * of the allowable units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Interval(double value, String units) {
		set(value,units);
	}
	
	/**
	 * Create an Interval of time whose initial value is a long in the specified units.
	 * @param value The initial value of the interval of time in the specified units.
	 * @param units A string specifying the unit of the interval of time, which must be one
	 * of the allowable units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Interval(long value, String units) {
		set(value,units);
	}
	
	/**
	 * Return the value of this interval of time in units of
	 * nanoseconds.
	 * @return the value of this interval of time in units of
	 * nanoseconds.
	 */
	public long get() {
		return time;
	}
	
	/**
	 * Return the value of this interval of time as a long in the specified units.
	 * Note that this value is truncated and not rounded, i.e. what is returned is the
	 * number of these units (days, for example) that are in this interval. 
	 * @param units The units in which the value of this interval of time is to be returned.
	 * @return The value of this interval of time as a long in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public long getAsLong(String units) {
		if (units.equals(DAY))
			return time / 86400000000000L;
		else if (units.equals(HOUR))
			return time / 3600000000000L;
		else if (units.equals(MINUTE))
			return time / 60000000000L;
		else if (units.equals(SECOND))
			return time / 1000000000L;
		else if (units.equals(MILLISECOND))
			return time / 1000000L;
		else if (units.equals(MICROSECOND))
			return time / 1000L;
		else if (units.equals(NANOSECOND))
			return time;
		else 
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}

	/**
	 * Return the value of this interval of time as a double in the specified units. 
	 * Note that some precision may be lost in expressing this interval of time as a double.
	 * @param units The units in which the value of this interval of time is to be returned.
	 * @return The value of this interval of time as a double in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public double getAsDouble(String units) {
		if (units.equals(DAY))
			return time / 86400000000000.0;
		else if (units.equals(HOUR))
			return time / 3600000000000.0;
		else if (units.equals(MINUTE))
			return time / 60000000000.0;
		else if (units.equals(SECOND))
			return time / 1000000000.0;
		else if (units.equals(MILLISECOND))
			return time / 1000000.0;
		else if (units.equals(MICROSECOND))
			return time / 1000.0;
		else if (units.equals(NANOSECOND))
			return time;
		else 
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Set this Interval of time to the spcified number of 
	 * nanoseconds.
	 * @param time The value of to which this interval is
	 * to be set in units of nanoseconds.
	 */
	public void set(long time) {
		this.time = time;
	}
	
	/**
	 * Set the value of this interval of time to the specified string in the specified units.
	 * The string must be either a parseable long or double.
	 * @param value The value to which this interval of time is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(String value, String units) {
		// Check to see whether to parse as a Double or Lomg.
		if (value.indexOf('.') != -1 || 
			value.indexOf('E') != -1 || value.indexOf('e') != -1 ||
			value.indexOf('D') != -1 || value.indexOf('d') != -1 ||
			value.indexOf('F') != -1 || value.indexOf('f') != -1) {
			// Parse as double
			if (units.equals(DAY))
				time = (long)(Double.parseDouble(value) * 86400000000000.0);
			else if (units.equals(HOUR))
				time = (long)(Double.parseDouble(value) * 3600000000000.0);
			else if (units.equals(MINUTE))
				time = (long)(Double.parseDouble(value) * 60000000000.0);
			else if (units.equals(SECOND))
				time = (long)(Double.parseDouble(value) * 1000000000.0);
			else if (units.equals(MILLISECOND))
				time = (long)(Double.parseDouble(value) * 1000000.0);
			else if (units.equals(MICROSECOND))
				time = (long)(Double.parseDouble(value) * 1000.0);
			else if (units.equals(NANOSECOND))
				time = (long)Double.parseDouble(value);
			else 
				throw new IllegalArgumentException(units + " is not an alllowable unit.");
		} else {
			// Parse as long
			if (units.equals(DAY))
				time = Long.parseLong(value) * 86400000000000L;
			else if (units.equals(HOUR))
				time = Long.parseLong(value) * 3600000000000L;
			else if (units.equals(MINUTE))
				time = Long.parseLong(value) * 60000000000L;
			else if (units.equals(SECOND))
				time = Long.parseLong(value) * 1000000000L;
			else if (units.equals(MILLISECOND))
				time = Long.parseLong(value) * 1000000L;
			else if (units.equals(MICROSECOND))
				time = Long.parseLong(value) * 1000L;
			else if (units.equals(NANOSECOND))
				time = Long.parseLong(value);
			else 
				throw new IllegalArgumentException(units + " is not an alllowable unit.");
		}
	}
	
	/**
	 * Set the value of this interval of time to the specified double in the specified units.
	 * @param value The value to which this interval of time is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(double value, String units) {	
		if (units.equals(DAY))
			time = (long)(value * 86400000000000.0);
		else if (units.equals(HOUR))
			time = (long)(value * 3600000000000.0);
		else if (units.equals(MINUTE))
			time = (long)(value * 60000000000.0);
		else if (units.equals(SECOND))
			time = (long)(value * 1000000000.0);
		else if (units.equals(MILLISECOND))
			time = (long)(value * 1000000.0);
		else if (units.equals(MICROSECOND))
			time = (long)(value * 1000.0);
		else if (units.equals(NANOSECOND))
			time = (long)value;
		else 
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Set the value of this interval of time to the specified long in the specified units.
	 * @param value The value to which this interval of time is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(long value, String units) {	
		if (units.equals(DAY))
			time = value * 86400000000000L;
		else if (units.equals(HOUR))
			time = value * 3600000000000L;
		else if (units.equals(MINUTE))
			time = value * 60000000000L;
		else if (units.equals(SECOND))
			time = value * 1000000000L;
		else if (units.equals(MILLISECOND))
			time = value * 1000000L;
		else if (units.equals(MICROSECOND))
			time = value * 1000L;
		else if (units.equals(NANOSECOND))
			time = value;
		else 
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Return the value of this interval of time as a String in units
	 * of nanoseconds.
	 */
	public String toString() {
		return Long.toString(time);
	}

	/**
	 * Return the value of this interval of time as a String in the specified units.
	 * The string returned contains an integer and its value is truncated and not rounded, 
	 * i.e. what is returned is the number of these units (days, for example) that are in 
	 * this interval. 
	 * @param units The units in which the value of this interval of time is to be displayed.
	 * @return The value of this interval of time as a String in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public String toStringAsLong(String units) {
		return Long.toString(getAsLong(units));
	}
	
	/**
	 * Return the value of this interval of time as a String in the specified units.
	 * The string returned contains a double and some precision may be lost in 
	 * expressing this interval of time as a double. 
	 * @param units The units in which the value of this interval of time is to be displayed.
	 * @return The value of this interval of time as a String in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public String toStringAsDouble(String units) {
		return Double.toString(getAsDouble(units));
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeLong(time);
	}
	
	/**
	 * Write the binary representation of a 1D array of Interval into a DataOutputStream.
	 * @param interval
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(Interval[] interval, DataOutputStream dos) throws IOException {
		dos.writeInt(interval.length);
		for (int i = 0; i < interval.length; i++)
			interval[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of interval into a DataOutputStream.
	 * @param interval
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(Interval[][] interval, DataOutputStream dos) throws IOException {
		dos.writeInt(interval.length);
		dos.writeInt(interval[0].length);
		for (int i = 0; i < interval.length; i++)
			for (int j=0; j < interval[0].length; j++)
				interval[i][j].toBin(dos);
	}
	
	/**
	 * Read the binary representation of an Interval from a BODataInputStream
	 * and use the read value to set an  Interval.
	 * @param dis the BODataInputStream to be read
	 * @return an Interval
	 * @throws IOException
	 */
	public static Interval fromBin(BODataInputStream dis) throws IOException {
		Interval interval = new Interval();
		interval.set(dis.readLong());
		return interval;
	}
	
	/**
	 * Read the binary representation of a 1D array of Interval from a BODataInputStream
	 * and use the read value to set a 1D array of Interval.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array ofInterval
	 * @throws IOException
	 */
	public static Interval[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Interval interval[] = new Interval[dim1];
		for (int i = 0; i < dim1; i++) 
			interval[i] = Interval.fromBin(dis);
		return interval;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Interval from a BODataInputStream
	 * and use the read value to set a 2D array of  Interval.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Interval
	 * @throws IOException
	 */
	public static Interval[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Interval[][] interval = new Interval[dim1][dim2];
		interval = new Interval[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				interval[i][j] = Interval.fromBin(dis);
		return interval;
	}

	/**
	 * Return an IDL Interval object.
	 * @return An IDL Interval object.
	 */
	public IDLInterval toIDLInterval() {
		return new IDLInterval(time);
	}

	/**
	 * Return true if and only if the specified object o is an
	 * Interval and its value is equal to this interval.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Inteval and its value is equal to this interval.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Interval))
			return false;
		return this.time == ((Interval)o).time;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this Interval of time is zero.
	 * @return true if and only if this Interval of time is zero.
	 */
	public boolean isZero() {
		return time == 0L;
	}

	/**
	 * Compare this Interval to the specified Object, which must be
	 * an Interval, returning -1, 0, or +1 if this interval is less 
	 * than, equal to, or greater than the specified Interval.
	 * @param o The Object to which this Interval is being compared.
	 * @return -1, 0, or +1 if this interval is less than, equal to, or 
	 * greater than the specified object, which must be an Interval.
	 * @throws IllegalArgumentException If the object being compared is not an Interval.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Interval))
			throw new IllegalArgumentException("Attempt to compare an Interval to a non-Interval.");
		if (time < ((Interval)o).time)
			return -1;
		if (time > ((Interval)o).time)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Interval of time is equal to the specified 
	 * Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is equal to the specified Interval.
	 */
	public boolean eq(Interval t) {
		return time == t.time; 
	}
	
	/**
	 * Return true if and only if this Interval of time is not equal to the specified 
	 * Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is not equal to the specified Interval.
	 */
	public boolean ne(Interval t) {
		return time != t.time; 
	}
	
	/**
	 * Return true if and only if this Interval of time is less than the specified 
	 * Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is less than the specified Interval.
	 */
	public boolean lt(Interval t) {
		return time < t.time; 
	}
	
	/**
	 * Return true if and only if this Interval of time is less than or equal to
	 * the specified Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is less than or equal to the 
	 * specified Interval.
	 */
	public boolean le(Interval t) {
		return time <= t.time; 
	}
	
	/**
	 * Return true if and only if this Interval of time is greater than the specified 
	 * Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is greater than the specified Interval.
	 */
	public boolean gt(Interval t) {
		return time > t.time; 
	}
	
	/**
	 * Return true if and only if this Interval of time is greater than or equal to
	 * the specified Interval.
	 * @param t The Interval to which this Interval is being compared.
	 * @return True, if and only if this Interval is greater than or equal to the 
	 * specified Interval.
	 */
	public boolean ge(Interval t) {
		return time >= t.time; 
	}
	
	/**
	 * Add the specified Interval of time to this Interval of time.
	 * @param t The Interval being added to this Interval
	 * @return This interval of time. 
	 */
	public Interval add(Interval t) {
		time += t.time;
		return this;
	}
	
	/**
	 * Subtract the specified Interval of time from this Interval of time.
	 * @param t The Interval being substracted from this Interval 
	 * @return This interval of time. 
	 */
	public Interval sub(Interval t) {
		time -= t.time;
		return this;
	}
	
	/**
	 * Multiple this interval of time by a specifed factor, this * factor.
	 * @param factor The factor that is used to multiply this interval of time.
	 * @return This interval of time. 
	 */
	public Interval mult (long factor) {
		time *= factor;
		return this;
	}
	
	/**
     * Divide this interval of time by a specifed factor, this / factor.
	 * @param factor The factor that is used to divide this interval of time.
	 * @return This interval of time. 
	 */
	public Interval div (long factor) {
		time /= factor;
		return this;
	}

	
	
}

