/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Tag.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.TagFormatException;
import alma.hla.runtime.asdm.types.TagType;
import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLTag;

/**
 * The Tag class is an implementation of a unique index identifying 
 * a row of an ALMA table.
 * 
 * Basically a Tag is defined by  :
 * 
 * <ul>
 * <li>a <i>value</i> which is an integer</li>
 * <li>a <i>type</i> which is a pointer to a TagType.</li>
 *  </ul>
 *  
 * @version 1.00 Nov 22, 2004
 * @author Allen Farris
 * 
 *  @version 2.00 Aug. 3, 2006
 *  @author caillat
 */
public class Tag implements java.io.Serializable, Comparable {

//	static public Tag getTag(StringTokenizer t) throws TagFormatException {
//	try {
//	String value = t.nextToken();
//	return Tag.parseTag(t.nextToken());
//	} catch (NoSuchElementException e) {
//	}
//	return null;
//	}

	private String tag;
	private TagType type;

	/**
	 * Create a default Tag that is null.
	 */
	public Tag() {
		tag = null;
		type = null;
	}

	/** 
	 * Create a Tag from an integer value.
	 * @param i an int defining the tag value.
	 * 
	 * @note The created Tag has a TagType::NoType.
	 */
	public Tag(int i){
		tag = Integer.toString(i);
		type = TagType.NoType;
	}

	/**
	 * Create a Tag from an int and a TagType
	 * @param i an integer value to define the Tag value
	 * @param t a TagType to define the type of the Tag
	 * 
	 * @note called with t == null this method creates a null Tag.
	 */
	public Tag(int i, TagType t) {
		this.tag = Integer.toString(i);
		this.type = t;
	}

	/** 
	 * Create a Tag from a Tag (copy constructor).
	 * @param t the Tag to be copied.
	 */
	public Tag(Tag t) {
		if (t.isNull()) {
			tag = null;
			type = null;
		}
		else {
			tag = new String(t.tag);
			type = t.type;
		}
	}

	/**
	 * Create a Tag from an IDLTag object.
	 * @param t The IDLTag object.
	 * 	
	 */
	public Tag (IDLTag t) {
		Tag x = null;
		try {
			x = parseTag(t.type_value);
		} catch (TagFormatException e) {
			// Let's decide to ignore this Exception.
			;
		}	
		this.tag = x.tag;
		this.type = x.type;
	}

	/**
	 * Create a Tag whose initial value is the specified string.
	 */
	public static Tag parseTag (String s) throws TagFormatException {
		if (s.length() == 0) return new Tag();

		String typeValue[] = s.split("_");
		if (typeValue.length != 2)
			throw new TagFormatException("Error:   \"" + 
					s + "\" cannot be parsed into a Tag ");
		
		// If the type part == "null" then return a null Tag.
		if (typeValue[0].compareTo("null") == 0) return new Tag();

		// Determine the Tag type
		TagType type = TagType.getTagType(typeValue[0]);
		if (type == null) 
			throw new TagFormatException("Error:   \"" + 
					s + "\" has an  invalid Tag type part \"" + typeValue[0] + "\"");	

		// Determine the Tag value
		int value = 0;
		try {
			value = Integer.parseInt(typeValue[1]);
		}
		catch (NumberFormatException e) {
			throw new TagFormatException("Error:  \"" + 
					s + "\" has an  invalid Tag value part \"" + typeValue[1] + "\"");
		}			
		return new Tag(value, type);		
	}

	/**
	 * Return the Tag as a String.
	 * The resulting string consists in the string representation of the type followed by an underscore
	 *  followed by the String representation of the value. Examples : "Antenna_12", "SpectralWindow_0".
	 * @return The Tag as a String.
	 */
	public String toString() {
		if (this.isNull()) return "null_0";
		return type+"_"+tag;
	}

	/**
	 * Return an array of int containing the values of the array of Tag objects passed in argument.
	 *  It must be noted that the information related to the type of each Tag in the items array is lost !
	 *  This method is useful only for anyone interested in the (int) values of Tag objects stored in array.
	 */
	static public int[] values(Tag[] items) {
		int array[] = new int[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].getTagValue();
		return array;
	}
	
	/**
	 * Write the binary representation of a Tag  into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		String s = this.toString();
		dos.writeInt(s.length());
		dos.writeBytes(s);
	}

	/**
	 * Write the binary representation of an ArrayList of  Tag  into a DataOutput stream
	 * @throws IOException 
	 */
	public static void  toBin(ArrayList<Tag>tags, DataOutputStream dos) throws IOException {
		dos.writeInt(tags.size());
		for (Tag tag : tags)
			tag.toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 1D array of Tag into a DataOutputStream.
	 * @param tag
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(Tag[] tags, DataOutputStream dos) throws IOException {
		dos.writeInt(tags.length);
		for (Tag tag : tags)
			tag.toBin(dos);
	}

	/**
	 * Read the binary representation of a Tag  from a BODataInputStream
	 * and use the read value to set a Tag.
	 * @param dis the BODataInputStream to be read
	 * @return a Tag
	 * @throws IOException
	 * @throws TagFormatException 
	 */
	public static Tag fromBin(BODataInputStream dis) throws IOException, TagFormatException {
		StringBuffer sb = new StringBuffer();
		int len = 0;

		len = dis.readInt();
		for (int i = 0; i < len; i++) sb.append((char)dis.readByte());

		return Tag.parseTag(sb.toString());
	}

	/**
	 * Read the binary representation of a 1D array of  Tag from a BODataInputStream
	 * and use the read value to set a 1D array of  Tag.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Tag
	 * @throws IOException
	 * @throws TagFormatException 
	 */
	public static Tag[] from1DBin(BODataInputStream dis) throws IOException, TagFormatException {
		int dim1 = dis.readInt();
		Tag tag[] = new Tag[dim1];
		for (int i = 0; i < dim1; i++) 
			tag[i] = Tag.fromBin(dis);
		return tag;
	}

	/**
	 * Return an IDLTag object.
	 * @return An IDLTag object.
	 */
	public IDLTag toIDLTag() {
		IDLTag e = new IDLTag ();
		e.value = this.tag;
		e.type_value = this.toString();
		return e;
	}
	/**
	 * Return this Tag value as a string.
	 * @return This Tag.
	 * @deprecated
	 */
	public String getTag() {
		return tag;
	}


	/**
	 * Returns the type of this Tag. 
	 * A null returned value indicates that the Tag is null (i.e. this.isNull() == true)/
	 * 
	 * @return the type of this Tag as a TagType.
	 */
	public TagType getTagType() {
		return this.type;
	}


	/**
	 * Returns the value of this Tag.
	 * The returned value has no meaning if the Tag is null.
	 * @return the value of this Tag as an int.
	 */
	public int getTagValue()  {
		if (this.isNull()) return 0;
		return Integer.parseInt(this.tag);
	}

//	/**
//	 * Set the value of this Tag from the specified string.
//	 * @param tag The string value to which to set this Tag.
//	 */
//	private void setTag(String tag) {
//		this.tag = tag;
//	}

	/**
	  * @param o The object to which this is being compared.
	  * @return true if and only if the specified object o is a
	  * Tag and if this and (Tag)o have values and types respectively equal.
	  */
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if((o == null) || (o.getClass() != this.getClass()))
			return false;
		
		// object must be Tag at this point
		Tag t = (Tag)o;
		return (tag.equals(t.tag)) && (type == t.type);
	}
	
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + tag.hashCode();
		hash = 31 * hash + (null == type ? 0 : type.hashCode());
		return hash;
	}

	/**
	 * @param t The Tag to which this is being compared
	 * @return true  this and t have values and types respectively equal.
	 */
	public boolean eq(Tag t) {
		return t.tag.equals(tag) && t.type == type;        
	}

	/**
	 * Return true if and only if this Tag is null, i.e. if its TagType field is null.
	 * @return True if and only if this Tag is null.
	 */
	public boolean isNull() {
		return type == null;
	}

	/**
	 * A Comparator.
	 * The comparison is performed by calling compareTo on the tag fields.
	 * 
	 */
	public int compareTo(Object o) {
		return tag.compareTo(((Tag) o).tag);
	}
}
