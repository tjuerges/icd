/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Speed.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLSpeed;

/**
 * The Speed class implements a concept of a speed in meters per second.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class Speed implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new Speed that is the sum of the 
	 * specified Speeds, a +  b.
	 * @param a The first Speed of the pair.
	 * @param b The second Speed of the pair.
	 * @return A new Speed that is the sum of the 
	 * specified Speed, a +  b.
	 */
	static public Speed add (Speed a, Speed b) {
		return new Speed (a.speed + b.speed);
	}
	
	/**
	 * Return a new Speed that is the difference of the 
	 * specified Speeds, a - b.
	 * @param a The first Speed of the pair.
	 * @param b The second Speed of the pair.
	 * @return A new Speed that is the difference of the 
	 * specified Speeds, a - b.
	 */
	static public Speed sub (Speed a, Speed b) {
		return new Speed (a.speed - b.speed);
	}
	
	/**
	 * Return a new Speed that is the product of a specified 
	 * speed and some specified factor, a * factor.
	 * @param a The base speed
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Speed that is the product of a specified 
	 * speed and some specified factor, a * factor.
	 */
	static public Speed mult (Speed a, double factor) {
		return new Speed (a.speed * factor);
	}
	
	/**
	 * Return a new Speed that is a specified speed divided by a 
	 * specified factor, a / factor.
	 * @param a The base speed
	 * @param factor The factor that is used to divide the value.
	 * @return A new Speed that is the product of a specified 
	 * speed and some specified factor, a / factor.
	 */
	static public Speed div (Speed a, double factor) {
		return new Speed (a.speed / factor);
	}
	
	static public Speed getSpeed(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Speed (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The speed in meters per second.
	 */
	private double speed;
	
	/**
	 * Create a Speed of zero meters per second.
	 */
	public Speed() {
	}

	/**
	 * Create a Speed whose initial value is a string designating the speed in meters per second
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the speed in meters per second
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Speed (String s) {
		speed = Double.parseDouble(s);
	}
	
	/**
	 * Create a Speed whose initial value is the same as the specified speed.
	 * @param x The specified speed that is used as the initial value of this speed.
	 */
	public Speed (Speed x) {
		this.speed = x.speed;
	}
	
	/**
	 * Create a Speed from an IDL speed object.
	 * @param x The IDL speed object.
	 */
	public Speed (IDLSpeed x) {
		this.speed = x.value;
	}
	
	/**
	 * Create a Speed whose initial value is the specified speed in meters per second.
	 * @param speed The speed in meters per second.
	 */
	public Speed (double speed) {
		this.speed = speed;
	}
	
	/**
	 * Return the value of this speed in meters per second.
	 * @return The value of this speed in meters per second.
	 */
	public double get() {
		return speed;
	}
	
	/**
	 * Set the value of this speed to the specified speed in meters per second.
	 * @param speed The specified speed in meters per second.
	 */
	public void set(double speed) {
		this.speed = speed;
	}
	
	/**
	 * Return the value of this as a String in units of meters per second.
	 * @return The value of this as a String in units of meters per second.
	 * 
	 */
	public String toString() {
		return Double.toString(speed);
	}

	/**
	 * Return an array of double containing the values of the array of Speed objects passed in argument.
	 */
	static public double[] values(Speed[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(speed);
	}
	/**
	 * Write the binary representation of a 1D array of speed into a DataOutputStream.
	 * @param speed the array of Speed to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Speed[] speed, DataOutputStream dos) throws IOException {
		dos.writeInt(speed.length);
		for (int i = 0; i < speed.length; i++)
			speed[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Speed into a DataOutputStream.
	 * @param speed the 2D array of Speed to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Speed[][] speed, DataOutputStream dos) throws IOException {
		dos.writeInt(speed.length);
		dos.writeInt(speed[0].length);
		for (int i = 0; i < speed.length; i++)
			for (int j=0; j < speed[0].length; j++)
				speed[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Speed into a DataOutputStream.
	 * @param speed the 3D array of Speed to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Speed[][][] speed, DataOutputStream dos) throws IOException {
		dos.writeInt(speed.length);
		dos.writeInt(speed[0].length);
		dos.writeInt(speed[0][0].length);
		for (int i = 0; i < speed.length; i++)
			for (int j=0; j < speed[0].length; j++)
				for (int k = 0; k < speed[0][0].length; k++)
					speed[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Speed from a BODataInputStream
	 * and use the read value to set an  Speed.
	 * @param dis the BODataInputStream to be read
	 * @return an Speed
	 * @throws IOException
	 */
	public static Speed fromBin(BODataInputStream dis) throws IOException {
		Speed speed = new Speed();
		speed.set(dis.readDouble());
		return speed;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Speed from a BODataInputStream
	 * and use the read value to set a 1D array of  Speed.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Speed
	 * @throws IOException
	 */
	public static Speed[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Speed speed[] = new Speed[dim1];
		for (int i = 0; i < dim1; i++) 
			speed[i] = Speed.fromBin(dis);
		return speed;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Speed from a BODataInputStream
	 * and use the read value to set a 2D array of  Speed.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Speed
	 * @throws IOException
	 */
	public static Speed[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Speed[][] speed = new Speed[dim1][dim2];
		speed = new Speed[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				speed[i][j] = Speed.fromBin(dis);
		return speed;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Speed from a BODataInputStream
	 * and use the read value to set a 3D array of  Speed.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Speed
	 * @throws IOException
	 */
	public static Speed[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Speed[][][] speed = new Speed[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					speed[i][j][k] = Speed.fromBin(dis);
		return speed;
	}	
	/**
	 * Return an IDL Speed object.
	 * @return An IDL Speed object.
	 */
	public IDLSpeed toIDLSpeed() {
		return new IDLSpeed(speed);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Speed and its value is equal to this speed.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Speed and its value is equal to this speed.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Speed))
			return false;
		return this.speed == ((Speed)o).speed;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this speed is zero.
	 * @return True if and only if this speed is zero.
	 */
	public boolean isZero() {
		return speed == 0.0;
	}
	
	/**
	 * Compare this Speed to the specified Object, which must be
	 * a Speed, returning -1, 0, or +1 if this speed is less 
	 * than, equal to, or greater than the specified Speed.
	 * @param o The Object to which this Speed is being compared.
	 * @return -1, 0, or +1 if this speed is less than, equal to, or 
	 * greater than the specified object, which must be a Speed.
	 * @throws IllegalArgumentException If the object being compared is not a Speed.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Speed))
			throw new IllegalArgumentException("Attempt to compare a Speed to a non-Speed.");
		if (speed < ((Speed)o).speed)
			return -1;
		if (speed > ((Speed)o).speed)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Speed is equal to the specified 
	 * Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is equal to the specified Speed.
	 */
	public boolean eq(Speed t) {
		return speed == t.speed; 
	}
	
	/**
	 * Return true if and only if this Speed is not equal to the specified 
	 * Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is not equal to the specified Speed.
	 */
	public boolean ne(Speed t) {
		return speed != t.speed; 
	}
	
	/**
	 * Return true if and only if this Speed is less than the specified 
	 * Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is less than the specified Speed.
	 */
	public boolean lt(Speed t) {
		return speed < t.speed; 
	}
	
	/**
	 * Return true if and only if this Speed is less than or equal to
	 * the specified Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is less than or equal to the 
	 * specified Speed.
	 */
	public boolean le(Speed t) {
		return speed <= t.speed; 
	}
	
	/**
	 * Return true if and only if this Speed is greater than the specified 
	 * Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is greater than the specified Speed.
	 */
	public boolean gt(Speed t) {
		return speed > t.speed; 
	}
	
	/**
	 * Return true if and only if this Speed is greater than or equal to
	 * the specified Speed.
	 * @param t The Speed to which this Speed is being compared.
	 * @return True, if and only if this Speed is greater than or equal to the 
	 * specified Speed.
	 */
	public boolean ge(Speed t) {
		return speed >= t.speed; 
	}
	
	/**
	 * Add a specified Speed to this speed, (this + x).
	 * @param x The Speed to be added to this speed.
	 * @return This speed after the addition.
	 */
	public Speed add (Speed x) {
		speed += x.speed;
		return this;
	}
	
	/**
	 * Subtract a specified Speed from this speed, (this - x).
	 * @param x The Speed to be subtracted to this speed.
	 * @return This speed after the subtraction.
	 */
	public Speed sub (Speed x) {
		speed -= x.speed;
		return this;
	}
	
	/**
	 * Multiply this speed by some factor, (this * factor).
	 * @param factor The factor by which this speed is to be multiplied.
	 * @return This speed after the multiplication.
	 */
	public Speed mult (double factor) {
		speed *= factor;
		return this;
	}
	
	/**
	 * Divide this speed by some factor, (this / factor).
	 * @param factor The factor by which this speed is to be divided.
	 * @return This speed after the division.
	 */
	public Speed div (double factor) {
		speed /= factor;
		return this;
	}
	
}
