/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayTime.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLArrayTime;

/**
 * The ArrayTime class implements the concept of a point in time, implemented
 * as an Interval of time since 17 November 1858 00:00:00 UTC, the beginning of the 
 * modified Julian Day.
 * <p>
 * All dates are assumed to be in the Gregorian calendar, including those 
 * prior to October 15, 1582.  So, if you are interested in very old dates, 
 * this isn't the most convenient class to use. 
 * <p>
 * Internally the time is kept in units of nanoseconds (10<sup>-9</sup> seconds).
 * The base time is 17 November 1858 00:00:00 UTC, and the maximum time is to the
 * year 2151 (2151-02-25T23:47:16.854775807).  This differs from the OMG Time service 
 * The OMG time is in units of 100 nanoseconds using the beginning of the Gregorian
 * calandar,15 October 1582 00:00:00 UTC, as the base time.
 * The reason for this increased accuracy is that the Control system is capable of 
 * measuring time to an accuracy of 40 nanoseconds.  Therefore, by adhering to the 
 * representation of time used in the OMG Time Serivce we would be losing precision.
 * <p>
 * The Time class is an extension of the Interval class, since all times
 * are intervals since 17 November 1858 00:00:00 UTC.
 * <p>
 * All times in this class are assumed to be International 
 * Atomic Time (TAI).  A specific TAI time differs from the corresponding 
 * UTC time by an offset that is an integral number of seconds.  
 * <p>
 * In the methods that give various quantities associated with
 * calendar times, this class does not apply any UTC corrections. 
 * Therefore, if you use these methods to produce calendar times, the 
 * results will differ from civil time by a few seconds.  The classes 
 * UTCTime and LocalTime take the UTC and timezone corrections into 
 * account.
 * <p>
 * The main reference used in crafting these methods is 
 * Astronomical Algorithms by Jean Meeus, second edition,
 * 2000, Willmann-Bell, Inc., ISBN 0-943396-61-1.  See
 * chapter 7, "Julian day", and chapter 12, "Sidereal Time".
 * 
 * @version 1.0 November 10, 2004
 * @author Allen Farris
 */
public class ArrayTime extends Interval {

	/**
	 * Return true if the specified year is a leap year.
	 * @param year the year in the Gregorian calendar.
	 * @return true if the specified year is a leap year.
	 */
	static public boolean isLeapYear(int year) {
		if (year % 4 != 0)
			return false;
		if (year % 100 == 0 && year % 400 != 0)
			return false;
		return true;
	}

	/**
	 * Return the Modified Julian day, given the Julian day.
	 * @param jd The Julian day
	 * @return The Modified Julian day
	 */
	static public double getMJD(double jd) {
		return jd - 2400000.5;
	}

	/**
	 * Return the Julian day, given the Modified Julian day.
	 * @param mjd The modified Julian day
	 * @return the Julian day
	 */
	static public double getJD(double mjd) {
		return mjd + 2400000.5;
	}

	/**
	 * Generate a new Time by adding an Interval
	 * to the specified Time.
	 * @param time The base Time 
	 * @param interval The interval to be added to the base time.
	 * @return A new Time formed by adding an Interval
	 * to the specified Time. 
	 */
	static public ArrayTime add(ArrayTime time, Interval interval) {
		ArrayTime t = new ArrayTime(time);
		t.add(interval);
		return t;
	}

	/**
	 * Generate a new Time by subtracting an Interval
	 * from the specified Time.
	 * @param time The base Time 
	 * @param interval The interval to be subtracted from the base time.
	 * @return A new Time formed by subtracting an Interval
	 * from the specified Time. 
	 */
	static public ArrayTime sub(ArrayTime time, Interval interval) {
		ArrayTime t = new ArrayTime(time);
		t.sub(interval);
		return t;
	}

	static public ArrayTime getArrayTime(StringTokenizer t) {
		try {
			long value = Long.parseLong(t.nextToken());
			return new ArrayTime (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * Create a default Time, initialized to a value of zero.
	 *
	 */
	public ArrayTime() {
		super();
	}
	
	/**
	 * Create a Time from a string, which can be in one of three
	 * formats:
	 * <ul>
	 * <li>A FITS formatted string,
	 * <li>A modified Julian day, or, 
	 * <li>An integer representing the number of  
	 * nanoseconds since 15 October 1582 00:00:00 UTC.
	 * </ul>
	 * <p>
	 * If the format is a FITS formatted string, its format must be 
	 * of the following form:	
	 * 			"YYYY-MM-DDThh:mm:ss.ssss"
	 * Leading zeros are required if months, days, hours, minutes, or seconds 
	 * are single digits.  The value for months ranges from "01" to "12".  
	 * The "T" separting the data and time values is optional (which is a 
	 * relaxation of the strict FITS standard).  If the "T" is 
	 * not present, then a space MUST be present.
	 * <p>
	 * If the format is a modified Julian day, then the string value 
	 * must be in the form of a double which MUST include a decimal point.
	 * <p>
	 * If the format is an interger, then it MUST represent the number of 
	 * nanoseconds since 17 November 1858 00:00:00 UTC, the beginning of the 
	 * modified Julian Day. 
	 * 
	 * @param s The string containing the initial value.
	 */
	public ArrayTime (String s) {
		long u = 0L;
		if (s.indexOf(':') != -1)
			u = FITSString(s);
		else {
			if (s.indexOf('.') != -1) {
				u = mjdToUnit(Double.parseDouble(s));
			} else {
				u = Long.parseLong(s);
			}
		}
		set(u);
	}
	
	/**
	 * Create a Time that is initialized to a specified Time.
	 * @param t
	 */
	public ArrayTime(ArrayTime t) {
		super(t);
	}

	/**
	 * Create a Time from an IDL time object.
	 * @param t The IDL time object.
	 */
	public ArrayTime (IDLArrayTime t) {
		set(t.value);
	}
	
	/**
	 * Create a Time by specifying the year, month, and day plus the fraction of a day.
	 * @param year The yeay
	 * @param month The month 
	 * @param day the day (and time)
	 */
	public ArrayTime(int year, int month, double day) {
		super(init(year,month,day));
	}

	/**
	 * Create a Time by specifying the calendar date and the time.
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 */
	public ArrayTime(int year, int month, int day, int hour, int minute, double second) {
		if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0.0 || second >= 60.0) {
			throw new IllegalArgumentException("Invalid time: " + hour + ":" + minute + ":" + second);
		}
		set(init(year,month,(double)(day + (((((second / 60.0) + minute) / 60.0) + hour) / 24.0))));
	}
	
	/**
	 * Create a Time by specifying the modified Julian day.
	 * @param modifiedJulianDay the modified Julian day, including fractions thereof.
	 */
	public ArrayTime(double modifiedJulianDay) {
		super(mjdToUnit(modifiedJulianDay));
	}
	
	/**
	 * Create a Time by specifying the modified Julian day plus an additional factor
	 * that designates the number of seconds and fractions in a day. 
	 * @param modifiedJulianDay the Modified Julian day expressed as an interger
	 * @param secondsInADay The number of seconds (with fractions) in this day.
	 */
	public ArrayTime(int modifiedJulianDay, double secondsInADay) {
		super(modifiedJulianDay * 8640000000000L + (long)(secondsInADay * 100000000.0));
	}
	
	/**
	 * Create a Time by specifying the number of  
	 * nanoseconds since 17 November 1858 00:00:00 UTC, the beginning of the 
	 * modified Julian Day.
	 * @param tensOfNanoseconds  The number of  
	 * nanoseconds since 17 November 1858 00:00:00 UTC, the beginning of the 
	 * modified Julian Day.
	 */
	public ArrayTime(long nanoseconds) {
		super(nanoseconds);
	}
	
	/**
	 * Return the Julian day.
	 * @return The Julian day as a double.
	 */
	public double getJD() {
		return unitToJD(get());
	}
		
	/**
	 * Return the Modified Julian day.
	 * @return The Modified Julian day as a double.
	 */
	public double getMJD() {
		return unitToMJD(get());
	}
	
	/**
	 * Return an IDL Time object.
	 * @return An IDL Time object.
	 */
	public IDLArrayTime toIDLArrayTime() {
		return new IDLArrayTime(get());
	}

	/**
	 * Return this Time as a FITS formatted string, which is of the
	 * form "YYYY-MM-DDThh:mm:ss.ssss".
	 * @return This Time as a FITS formatted string.
	 */
	public String toFITS() {
		int[] unit = getDateTime();
		int yy = unit[0];
		int mm = unit[1];
		int dd = unit[2];
		int hh = unit[3];
		int min = unit[4];
		int sec = unit[5];
		StringBuffer s = new StringBuffer(unit[0] + "-");
		if (unit[1] < 10) s.append('0'); s.append(unit[1]); s.append('-');
		if (unit[2] < 10) s.append('0'); s.append(unit[2]); s.append('T');
		if (unit[3] < 10) s.append('0'); s.append(unit[3]); s.append(':');
		if (unit[4] < 10) s.append('0'); s.append(unit[4]);  s.append(':');
		if (unit[5] < 10.0) s.append('0'); s.append(sec);
		// apply fractions of a second
		String frac = Integer.toString(unit[6]);
		s.append('.');
		// The statement below is sensitive to the number of significant
		// digits in a fraction.  If units are nanoseconds,
		// then we need we will have 9 significant digits in a fraction 
		// string.
		s.append("0000000000000000".substring(0,numberSigDigitsInASecond - frac.length()));
		s.append(frac);
		return s.toString();
		
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeLong(this.get());
	} 
	
	/**
	 * Write the binary representation of a 1D array of ArrayTime into a DataOutputStream.
	 * @param ArrayTime
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(ArrayTime[] arrayTime, DataOutputStream dos) throws IOException {
		dos.writeInt(arrayTime.length);
		for (int i = 0; i < arrayTime.length; i++)
			arrayTime[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of ArrayTime into a DataOutputStream.
	 * @param ArrayTime
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(ArrayTime[][] arrayTime, DataOutputStream dos) throws IOException {
		dos.writeInt(arrayTime.length);
		dos.writeInt(arrayTime[0].length);
		for (int i = 0; i < arrayTime.length; i++)
			for (int j=0; j < arrayTime[0].length; j++)
				arrayTime[i][j].toBin(dos);
	}
	
	
	/**
	 * Read the binary representation of an ArrayTime from a BODataInputStream
	 * and use the read value to set an  ArrayTime.
	 * @param dis the BODataInputStream to be read
	 * @return an ArrayTime
	 * @throws IOException
	 */
	public static ArrayTime fromBin(BODataInputStream dis) throws IOException {
		ArrayTime angle = new ArrayTime();
		angle.set(dis.readLong());
		return angle;
	}
	
	/**
	 * Read the binary representation of a 1D array of  ArrayTime from a BODataInputStream
	 * and use the read value to set a 1D array of  ArrayTime.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of ArrayTime
	 * @throws IOException
	 */
	public static ArrayTime[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		ArrayTime angle[] = new ArrayTime[dim1];
		for (int i = 0; i < dim1; i++) 
			angle[i] = ArrayTime.fromBin(dis);
		return angle;
	}
	
	/**
	 * Read the binary representation of a 2D array of  ArrayTime from a BODataInputStream
	 * and use the read value to set a 2D array of  ArrayTime.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of ArrayTime
	 * @throws IOException
	 */
	public static ArrayTime[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		ArrayTime[][] angle = new ArrayTime[dim1][dim2];
		angle = new ArrayTime[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				angle[i][j] = ArrayTime.fromBin(dis);
		return angle;
	}
	
	/**
	 * Read the binary representation of a 3D array of  ArrayTime from a BODataInputStream
	 * and use the read value to set a 3D array of  ArrayTime.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  ArrayTime
	 * @throws IOException
	 */
	public static ArrayTime[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		ArrayTime[][][] angle = new ArrayTime[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					angle[i][j][k] = ArrayTime.fromBin(dis);
		return angle;
	}
	
	/**
	 * Return this time as an array of integers denoting the following:
	 * <ul>
	 * <li>year,
	 * <li>month (varies from 1 to 12),
	 * <li>day (varies from 1 to 28, 29, 30, or 31),
	 * <li>hour (varies from 0 to 23),
	 * <li>minute (varies from 0 to 59),
	 * <li>second (varies from 0 to 59), and
	 * <li>the number of nanoseconds that remain in this fraction of a second.
	 * </ul>
	 * @return This time as an array of integers denoting year, month, day, hour, minute
	 * second, and fraction of a second.
	 */
	public int[] getDateTime() {
		int[] n = new int [7];
		long fractionOfADay = get() % unitsInADayL;
		if (fractionOfADay < 0)
			fractionOfADay = unitsInADayL - fractionOfADay;
		long nsec = fractionOfADay / unitsInASecond;
		n[6] = (int)(fractionOfADay - (nsec * 1000000000L));
		long nmin = nsec / 60L;
		n[5] = (int)(nsec - nmin * 60L);
		long nhr = nmin / 60L;
		n[4] = (int)(nmin - nhr * 60L);
		n[3] = (int)nhr;
		
		double jd = unitToJD(get());
		
		// For this algorithm see Meeus, chapter 7, p. 63.
		double x = jd + 0.5; // Make the 12h UT adjustment.
		int Z = (int)x;
		double F = x - Z;
		int A = Z;
		int alpha = 0;
		if (Z >= 2299161) {
			alpha = (int)((Z - 1867216.25) / 36524.25);
			A = Z + 1 + alpha - (int)(alpha / 4);
		}
		int B = A + 1524;
		int C = (int)((B - 122.1) / 365.25);
		int D = (int)(365.25 * C);
		int E = (int)((B - D) / 30.6001);
		double day = B - D - (int)(30.6001 * E) + F;
		int month = (E < 14) ? E - 1 : E - 13;
		int year = (month > 2) ? C - 4716 : C - 4715;
		
		n[2] = (int)day;
		n[1] = month;
		n[0] = year;
		
		return n;
	}
	
	/**
	 * Return the time of day in hours and fractions thereof. 
	 * @return The time of day in hours.
	 */
	public double getTimeOfDay() {
		double x = unitToJD(get()) + 0.5;
		return (x - (int)x) * 24.0;
	}
	
	/**
	 * Return the day number of the week of this Time.
	 * The day numbers are 0-Sunday, 1-Monday, 2-Tuesday,
	 * 3-Wednesday, 4-Thursday, 5-Friday, and 6-Saturday.
	 * @return The day number of the week of this Time.
	 */
	public int getDayOfWeek() {
		return ((int)(unitToJD(get()) + 1.5)) % 7;
	}
	
	/**
	 * Return the day number of the year of this Time.
	 * @return The day number of the year of this Time.
	 */
	public int getDayOfYear() {
		int[] n = getDateTime();
		int year = n[0];
		int month = n[1];
		int day = n[2];
		return ((275 * month) / 9) - 
				((isLeapYear(year) ? 1 : 2) * ((month + 9) / 12)) + 
				day - 30;
	}
	
	/**
	 * Return the time of day as a string, in the form
	 * "hh:mm:ss".
	 * @return The time of day as a string.
	 */
	public String timeOfDayToString() {
		int[] n = getDateTime();
		int hh = n[3];
		int min = n[4];
		int sec = n[5];
		StringBuffer s = new StringBuffer();
		if (hh < 10) s.append('0'); s.append(hh); s.append(':');
		if (min < 10) s.append('0'); s.append(min);  s.append(':');
		if (sec < 10.0) s.append('0'); s.append(sec);
		return s.toString();
	}
	
	/**
	 * Return the local sidereal time for this Time
	 * in hours and fractions of an hour at the specified longitude.
	 * @param longitudeInHours The desired longitude in hours.
	 * @return The local sidereal time in hours.
	 */
	public double getLocalSiderealTime(double longitudeInHours) {
		return getGreenwichMeanSiderealTime() - longitudeInHours;
	}
	
	/**
	 * Return the Greenwich mean sidereal time for this Time
	 * in hours and fractions of an hour.
	 * @return The Greenwich mean sidereal time in hours.
	 */
	public double getGreenwichMeanSiderealTime() {
		double jd = unitToJD(get());
		double t0 = jd - 2451545.0;
		double t = t0 / 36525.0;
		double tt = t * t;
		double x = (280.46061837 + 
				360.98564736629 * t0 + 
				tt * (0.000387933 - (t / 38710000.0))) / 15.0 ;
		double y = Math.IEEEremainder(x,24.0);
		if (y < 0)
			y = 24.0 + y;	   
		return y;
	}
	
	static private long init(int year, int month, double day) {
		// For this algorithm see Meeus, chapter 7, p. 61.
		int iday = (int)day;
		if (month < 1 || month > 12)
			throw new IllegalArgumentException ("Illegal value of month: " 
					+ year + "-" + month + "-" + day);
		if ( (iday < 1 || iday > 31) ||
				((month == 4 || month == 6 || month == 9 || month == 11) && iday > 30) ||
				(month == 2 && (iday > ((isLeapYear(year) ? 29 : 28)))) )
			throw new IllegalArgumentException ("Illegal value of day: "
					+ year + "-" + month + "-" + day);
		if (month <= 2) {
			--year;
			month += 12;
		}
		int A = year / 100;
		int B = 2 - A + (A / 4);
		double jd = (int)(365.25 * (year + 4716)) + (int)(30.6001 * (month + 1)) + iday + B - 1524.5;
		long u = jdToUnit(jd);
		// Now add the fraction of a day.
		u += (long)((day - iday) * unitsInADay + 0.5);
		return u;
	}
	
	static private long init(int year, int month, int day, int hour, int minute, double second) {
		if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0.0 || second >= 60.0) {
			throw new IllegalArgumentException("Invalid time: " + hour + ":" + minute + ":" + second);
		}
		return init(year,month,(double)(day + (((((second / 60.0) + minute) / 60.0) + hour) / 24.0)));
	}

	/**
	 * Return a unit of time, as a long, from a FITS-formatted string that 
	 * specifies the time.  The format must be of the form:	
	 * 			YYYY-MM-DDThh:mm:ss.ssss
	 * Leading zeros are required if months, days, hours, minutes, or seconds 
	 * are single digits.  The value for months ranges from "01" to "12".  
	 * The "T" separting the data and time values is optional.  If the "T" is 
	 * not present, then a space MUST be present.
	 * 
	 * An IllegalArgumentException is thrown is the string is not a valid 
	 * time.
	 */
	private long FITSString(String t) {
		if (t.length() < 19 || t.charAt(4) != '-' || t.charAt(7) != '-' || 
				(t.charAt(10) != 'T' && t.charAt(10) != ' ') || 
				t.charAt(13) != ':' || t.charAt(16) != ':')
			throw new IllegalArgumentException("Invalid time format: " + t);
		int yyyy = 0;
		int mm = 0;
		int dd = 0;
		int hh = 0;
		int min = 0;
		double sec = 0.0;
		try {
			yyyy = Integer.parseInt(t.substring(0,4));
			mm   = Integer.parseInt(t.substring(5,7));
			dd   = Integer.parseInt(t.substring(8,10));
			hh   = Integer.parseInt(t.substring(11,13));
			min  = Integer.parseInt(t.substring(14,16));
			sec  = Double.parseDouble(t.substring(17));
		} catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid time format: " + t);
		}
		return init(yyyy,mm,dd,hh,min,sec);
	}
	
	// Constants used in conversion.
	
	/*
	 * The following represents a base time of MJD
	 * and units of 1 nanosecond.
	 */
	static public final int    numberSigDigitsInASecond = 9;
	static public final long   unitsInASecond 		= 1000000000L;
	static public final long   unitsInADayL 			= 86400000000000L;
	static public final double unitsInADay 		= 86400000000000.0;
	static public final double unitsInADayDiv100 	= 864000000000.0;
	static public final double julianDayOfBase 	= 2400000.5;
	static public final long   julianDayOfBaseInUnitsInADayDiv100 = 2073600432000000000L;

	
	/**
	 * Convert a unit of time in units since the base time to a Julian day.
	 * @param unit The unit to be converted.
	 * @return The Julian day corresponding to the specified unit of time.
	 */
	static public double unitToJD(long unit) {
		return (double)(unit) / unitsInADay + julianDayOfBase;
	}

	/**
	 * Convert a unit of time in units since the base time to a Modified Julian day.
	 * @param unit The unit to be converted.
	 * @return The Modified Julian day corresponding to the specified unit of time.
	 */
	static public double unitToMJD(long unit) {
		return (double)(unit) / unitsInADay;
	}
	
	/**
	 * Convert a Julian day to a unit of time in tens of nanoseconds 
	 * since 15 October 1582 00:00:00 UTC.
	 * @param jd The Julian day to be converted.
	 * @return The unit corresponding to the specified Julian day.
	 */
	static public long jdToUnit(double jd) {
		return ((long)(jd * unitsInADayDiv100) - julianDayOfBaseInUnitsInADayDiv100) * 100L;
	}
	
	/**
	 * Convert a Modified Julian day to units since the base time.
	 * @param mjd The Modified Julian day to be converted.
	 * @return The unit corresponding to the specified Modified Julian day.
	 */
	static public long mjdToUnit(double mjd) {
		return (long)(mjd * unitsInADay);
	}

	/**
	 * Include the UTC correction table.
	 */
	static private UTCCorrection[] UTCCorrectionTable = {
			// 					JD		   TAI-UTC
			new UTCCorrection(2438395.5,   3.2401300),
			new UTCCorrection(2438486.5,   3.3401300),
			new UTCCorrection(2438639.5,   3.4401300),
			new UTCCorrection(2438761.5,   3.5401300),
			new UTCCorrection(2438820.5,   3.6401300),
			new UTCCorrection(2438942.5,   3.7401300),
			new UTCCorrection(2439004.5,   3.8401300),
			new UTCCorrection(2439126.5,   4.3131700),
			new UTCCorrection(2439887.5,   4.2131700),
			new UTCCorrection(2441317.5,  10.0),
			new UTCCorrection(2441499.5,  11.0),
			new UTCCorrection(2441683.5,  12.0),
			new UTCCorrection(2442048.5,  13.0),
			new UTCCorrection(2442413.5,  14.0),
			new UTCCorrection(2442778.5,  15.0),
			new UTCCorrection(2443144.5,  16.0),
			new UTCCorrection(2443509.5,  17.0),
			new UTCCorrection(2443874.5,  18.0),
			new UTCCorrection(2444239.5,  19.0),
			new UTCCorrection(2444786.5,  20.0),
			new UTCCorrection(2445151.5,  21.0),
			new UTCCorrection(2445516.5,  22.0),
			new UTCCorrection(2446247.5,  23.0),
			new UTCCorrection(2447161.5,  24.0),
			new UTCCorrection(2447892.5,  25.0),
			new UTCCorrection(2448257.5,  26.0),
			new UTCCorrection(2448804.5,  27.0),
			new UTCCorrection(2449169.5,  28.0),
			new UTCCorrection(2449534.5,  29.0),
			new UTCCorrection(2450083.5,  30.0),
			new UTCCorrection(2450630.5,  31.0),
			new UTCCorrection(2451179.5,  32.0)
	};
	static private UTCCorrection UTCLast = UTCCorrectionTable [UTCCorrectionTable.length - 1];
	
	/**
	 * Return the TAI to UTC correction in units of seconds that must be applied
	 * to a specified Julian Day, in other words return TAI time minus UCT time.
	 * @param jd The Julian day for the TAI time.
	 * @return The number of seconds that must be subtracted from the TAI time to get the 
	 * correct UTC time.
	 */
	static public double utcCorrection(double jd) {
		if (jd > UTCLast.getJD())
			return UTCLast.getTAIMinusUTC();
		int i = UTCCorrectionTable.length - 1;
		for (; i >= 0; --i) {
			if (jd <= UTCCorrectionTable[i].getJD())
				break;
		}
		return UTCCorrectionTable[i].getTAIMinusUTC();
	}
	
	/**
	 * Get the UTC time that corresponds to this TAI time.
	 * @return The UTC time that corresponds to this TAI time.
	 */
	public UTCTime getUTCTime() {
		double jd = this.getJD();
		long value = get();
		value -= (long)(utcCorrection(jd) * 1000000000.0);
		return new UTCTime(value);
	}
}
