/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Temperature.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLTemperature;

/**
 * The Temperature class implements a concept of a temperature in degrees Centigrade.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class Temperature implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new Temperature that is the sum of the 
	 * specified temperatures, a +  b.
	 * @param a The first Temperature of the pair.
	 * @param b The second Temperature of the pair.
	 * @return A new Temperature that is the sum of the 
	 * specified temperatures, a +  b.
	 */
	static public Temperature add (Temperature a, Temperature b) {
		return new Temperature (a.temperature + b.temperature);
	}
	
	/**
	 * Return a new Temperature that is the difference of the 
	 * specified temperatures, a - b.
	 * @param a The first Temperature of the pair.
	 * @param b The second Temperature of the pair.
	 * @return A new Temperature that is the difference of the 
	 * specified temperatures, a - b.
	 */
	static public Temperature sub (Temperature a, Temperature b) {
		return new Temperature (a.temperature - b.temperature);
	}
	
	/**
	 * Return a new Temperature that is the product of a specified 
	 * temperature and some specified factor, a * factor.
	 * @param a The base temperature
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Temperature that is the product of a specified 
	 * temperature and some specified factor, a * factor.
	 */
	static public Temperature mult (Temperature a, double factor) {
		return new Temperature (a.temperature * factor);
	}
	
	/**
	 * Return a new Temperature that is a specified temperature divided by a 
	 * specified factor, a / factor.
	 * @param a The base temperature
	 * @param factor The factor that is used to divide the value.
	 * @return A new Temperature that is the product of a specified 
	 * temperature and some specified factor, a / factor.
	 */
	static public Temperature div (Temperature a, double factor) {
		return new Temperature (a.temperature / factor);
	}
	
	static public Temperature getTemperature(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Temperature (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The temperature in degrees Centigrade.
	 */
	private double temperature;
	
	/**
	 * Create a Temperature of zero degrees Centigrade.
	 */
	public Temperature() {
	}

	/**
	 * Create a Temperature whose initial value is a string designating the temperature in degrees Centigrade
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the temperature in degrees Centigrade
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Temperature (String s) {
		temperature = Double.parseDouble(s);
	}
	
	/**
	 * Create a Temperature whose initial value is the same as the specified temperature.
	 * @param l The specified temperature that is used as the initial value of this temperature.
	 */
	public Temperature (Temperature l) {
		this.temperature = l.temperature;
	}
	
	/**
	 * Create a Temperature from an IDL temperature object.
	 * @param l The IDL temperature object.
	 */
	public Temperature (IDLTemperature l) {
		this.temperature = l.value;
	}
	
	/**
	 * Create a Temperature whose initial value is the specified temperature in degrees Centigrade.
	 * @param temperature The temperature in degrees Centigrade.
	 */
	public Temperature (double temperature) {
		this.temperature = temperature;
	}
	
	/**
	 * Return the value of this temperature in degrees Centigrade.
	 * @return The value of this temperature in degrees Centigrade.
	 */
	public double get() {
		return temperature;
	}
	
	/**
	 * Set the value of this temperature to the specified temperature in degrees Centigrade.
	 * @param temperature The specified temperature in degrees Centigrade.
	 */
	public void set(double temperature) {
		this.temperature = temperature;
	}
	
	/**
	 * Return the value of this as a String in units of degrees Centigrade.
	 * @return The value of this as a String in units of degrees Centigrade.
	 * 
	 */
	public String toString() {
		return Double.toString(temperature);
	}

	/**
	 * Return an array of double containing the values of the array of Temperature objects passed in argument.
	 */
	static public double[] values(Temperature[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(temperature);
	}

	/**
	 * Write the binary representation of a 1D array of temperature into a DataOutputStream.
	 * @param temperature the array of Temperature to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Temperature[] temperature, DataOutputStream dos) throws IOException {
		dos.writeInt(temperature.length);
		for (int i = 0; i < temperature.length; i++)
			temperature[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Temperature into a DataOutputStream.
	 * @param temperature the 2D array of Temperature to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Temperature[][] temperature, DataOutputStream dos) throws IOException {
		dos.writeInt(temperature.length);
		dos.writeInt(temperature[0].length);
		for (int i = 0; i < temperature.length; i++)
			for (int j=0; j < temperature[0].length; j++)
				temperature[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Temperature into a DataOutputStream.
	 * @param temperature the 3D array of Temperature to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Temperature[][][] temperature, DataOutputStream dos) throws IOException {
		dos.writeInt(temperature.length);
		dos.writeInt(temperature[0].length);
		dos.writeInt(temperature[0][0].length);
		for (int i = 0; i < temperature.length; i++)
			for (int j=0; j < temperature[0].length; j++)
				for (int k = 0; k < temperature[0][0].length; k++)
					temperature[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Temperature from a BODataInputStream
	 * and use the read value to set an  Temperature.
	 * @param dis the BODataInputStream to be read
	 * @return an Temperature
	 * @throws IOException
	 */
	public static Temperature fromBin(BODataInputStream dis) throws IOException {
		Temperature temperature = new Temperature();
		temperature.set(dis.readDouble());
		return temperature;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Temperature from a BODataInputStream
	 * and use the read value to set a 1D array of  Temperature.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Temperature
	 * @throws IOException
	 */
	public static Temperature[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Temperature temperature[] = new Temperature[dim1];
		for (int i = 0; i < dim1; i++) 
			temperature[i] = Temperature.fromBin(dis);
		return temperature;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Temperature from a BODataInputStream
	 * and use the read value to set a 2D array of  Temperature.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Temperature
	 * @throws IOException
	 */
	public static Temperature[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Temperature[][] temperature = new Temperature[dim1][dim2];
		temperature = new Temperature[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				temperature[i][j] = Temperature.fromBin(dis);
		return temperature;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Temperature from a BODataInputStream
	 * and use the read value to set a 3D array of  Temperature.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Temperature
	 * @throws IOException
	 */
	public static Temperature[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Temperature[][][] temperature = new Temperature[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					temperature[i][j][k] = Temperature.fromBin(dis);
		return temperature;
	}	
	
	/**
	 * Return an IDL Temperature object.
	 * @return An IDL Temperature object.
	 */
	public IDLTemperature toIDLTemperature() {
		return new IDLTemperature(temperature);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Temperature and its value is equal to this temperature.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Temperature and its value is equal to this temperature.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Temperature))
			return false;
		return this.temperature == ((Temperature)o).temperature;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this temperature is zero.
	 * @return True if and only if this temperature is zero.
	 */
	public boolean isZero() {
		return temperature == 0.0;
	}
	
	/**
	 * Compare this Temperature to the specified Object, which must be
	 * a Temperature, returning -1, 0, or +1 if this temperature is less 
	 * than, equal to, or greater than the specified Temperature.
	 * @param o The Object to which this Temperature is being compared.
	 * @return -1, 0, or +1 if this temperature is less than, equal to, or 
	 * greater than the specified object, which must be a Temperature.
	 * @throws IllegalArgumentException If the object being compared is not a Temperature.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Temperature))
			throw new IllegalArgumentException("Attempt to compare a Temperature to a non-Temperature.");
		if (temperature < ((Temperature)o).temperature)
			return -1;
		if (temperature > ((Temperature)o).temperature)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Temperature is equal to the specified 
	 * Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is equal to the specified Temperature.
	 */
	public boolean eq(Temperature t) {
		return temperature == t.temperature; 
	}
	
	/**
	 * Return true if and only if this Temperature is not equal to the specified 
	 * Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is not equal to the specified Temperature.
	 */
	public boolean ne(Temperature t) {
		return temperature != t.temperature; 
	}
	
	/**
	 * Return true if and only if this Temperature is less than the specified 
	 * Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is less than the specified Temperature.
	 */
	public boolean lt(Temperature t) {
		return temperature < t.temperature; 
	}
	
	/**
	 * Return true if and only if this Temperature is less than or equal to
	 * the specified Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is less than or equal to the 
	 * specified Temperature.
	 */
	public boolean le(Temperature t) {
		return temperature <= t.temperature; 
	}
	
	/**
	 * Return true if and only if this Temperature is greater than the specified 
	 * Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is greater than the specified Temperature.
	 */
	public boolean gt(Temperature t) {
		return temperature > t.temperature; 
	}
	
	/**
	 * Return true if and only if this Temperature is greater than or equal to
	 * the specified Temperature.
	 * @param t The Temperature to which this Temperature is being compared.
	 * @return True, if and only if this Temperature is greater than or equal to the 
	 * specified Temperature.
	 */
	public boolean ge(Temperature t) {
		return temperature >= t.temperature; 
	}
	
	/**
	 * Add a specified Temperature to this temperature, (this + x).
	 * @param x The Temperature to be added to this temperature.
	 * @return This temperature after the addition.
	 */
	public Temperature add (Temperature x) {
		temperature += x.temperature;
		return this;
	}
	
	/**
	 * Subtract a specified Temperature from this temperature, (this - x).
	 * @param x The Temperature to be subtracted to this temperature.
	 * @return This temperature after the subtraction.
	 */
	public Temperature sub (Temperature x) {
		temperature -= x.temperature;
		return this;
	}
	
	/**
	 * Multiply this temperature by some factor, (this * factor).
	 * @param factor The factor by which this temperature is to be multiplied.
	 * @return This temperature after the multiplication.
	 */
	public Temperature mult (double factor) {
		temperature *= factor;
		return this;
	}
	
	/**
	 * Divide this temperature by some factor, (this / factor).
	 * @param factor The factor by which this temperature is to be divided.
	 * @return This temperature after the division.
	 */
	public Temperature div (double factor) {
		temperature /= factor;
		return this;
	}
	
}
