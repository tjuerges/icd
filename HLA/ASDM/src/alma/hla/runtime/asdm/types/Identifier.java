/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on May 17, 2005
 * File Identifier.java
 */

package alma.hla.runtime.asdm.types;

import java.util.Hashtable;

/**
 * @author caillat
 *
 */
public class Identifier {
    private final String id;
    static private Hashtable ids = null;
    private Identifier(int i) {id = Integer.toString(i);}
    
    static public Identifier newInstance(int i) throws IllegalArgumentException{        
        if (i < 0 ) 
            throw new IllegalArgumentException("'"+i + "' is an invalid value for an identifier");
        if (ids == null) ids = new Hashtable();
        Identifier ident = null;
        if ((ident = (Identifier) ids.get(Integer.toString(i))) != null) {
            return ident;
        }
        else {
            ident = new Identifier(i);
            ids.put(ident.id, ident);
        }
        return ident;
    }
    
    public Identifier succ() {
        return newInstance(Integer.getInteger(id).intValue()+1);
    }
    
    public String toString() {
        return id;
    }
}
