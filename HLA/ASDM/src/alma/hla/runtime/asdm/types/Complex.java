/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Complex.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLComplex;

/**
 * The Complex class implements a complex number.
 * 
 * @version 1.00 Nov 8, 2004
 * @author Allen Farris
 */
public class Complex implements Cloneable, Comparable, java.io.Serializable {
	
	/**
	 * Return a new complex number that is the sum of the two specified 
	 * complex numbers, "a + b". 
	 * @param a The first complex number of the pair.
	 * @param b The second complex number of the pair.
	 * @return A new Complex number that is the sum of the two specified 
	 * complex numbers, "a + b".
	 */
	static public Complex add(Complex a, Complex b) {
		Complex x = new Complex(a);
		return x.add(b);
	}

	/**
	 * Return a new complex number that is the difference of the two specified 
	 * complex numbers, "a - b".
	 * @param a The first complex number of the pair.
	 * @param b The second complex number of the pair.
	 * @return A new Complex number that is the difference of the two specified 
	 * complex numbers, "a - b".
	 */
	static public Complex sub(Complex a, Complex b) {
		Complex x = new Complex(a);
		return x.sub(b);
	}

	/**
	 * Return a new complex number that is the product of the two specified 
	 * complex numbers, "a * b".
	 * @param a The first complex number of the pair.
	 * @param b The second complex number of the pair.
	 * @return A new Complex number that is the product of the two specified 
	 * complex numbers, "a * b".
	 */
	static public Complex mult(Complex a, Complex b) {
		Complex x = new Complex(a);
		return x.mult(b);
	}

	/**
	 * Return a new complex number that is "a / b".
	 * @param a The first complex number of the pair.
	 * @param b The second complex number of the pair.
	 * @return A new Complex number that is "a / b".
	 */
	static public Complex div(Complex a, Complex b) {
		Complex x = new Complex(a);
		return x.div(b);
	}

	/**
	 * Return a new complex number that is the complex conjugate of the 
	 * specified complex number.
	 * @param a The complex number used to form its conjugate.
	 * @return A new complex number that is the complex conjugate of a.
	 */
	static public Complex conjugate(Complex a) {
		Complex x = new Complex(a);
		return x.conjugate();
	}	
	
	static public Complex getComplex(StringTokenizer t) {
		try {
			double valueRe = Double.parseDouble(t.nextToken());
			double valueIm = Double.parseDouble(t.nextToken());
			return new Complex (valueRe,valueIm);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The real part of the complex number;
	 */
	private double re;
	
	/**
	 * The imaginary part of the complex number;
	 */
	private double im;

	/**
	 * Create a complex number initialized to zero.
	 *
	 */
	public Complex() {
		re = 0.0;
		im = 0.0;
	}

	/**
	 * Create a complex number from a string of the form "x y", where x and y are
	 * strings representing double precision numbers in the standard Java notation 
	 * (for example, "56.78 12.34").
	 * @param s The string that servies as the initial value of this complex number.
	 */
	public Complex(String s) {
		String x = s.trim();
		int n = x.indexOf(' ');
		re = Double.parseDouble(x.substring(0,n));
		im = Double.parseDouble(x.substring(n+1));
	}
	
	/**
	 * Create a complex number whose value is initialized to the specified
	 * complex number. 
	 * @param c The complex number that forms the initial value of this complex number.
	 */
	public Complex (Complex c) {
		this.re = c.re;
		this.im = c.im;
	
	}

	/**
	 * Create a complex number from an IDL Complex object.
	 * @param c The IDL Complex object.
	 */
	public Complex (IDLComplex c) {
		this.re = c.re;
		this.im = c.im;
	}
	
	/**
	 * Create a complex number initialized to the specified real
	 * and imaginary parts.
	 * @param re The real part of the complex number.
	 * @param im The imaginary part of the complex number.
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	/**
	 * Return the real part of the ocmplex number.
	 * @return The real part of the ocmplex number.
	 */
	public double getReal() {
		return re;
	}

	/**
	 * Return the imaginary part of the ocmplex number.
	 * @return The imaginary part of the ocmplex number.
	 */
	public double getImg() {
		return im;
	}

	/**
	 * Set the real part of the complex number.
	 * @param re The real part of the complex number.
	 */
	public void setReal(double re) {
		this.re = re;
	}

	/**
	 * Set the imaginary part of the complex number.
	 * @param im The imaginary part of the complex number.
	 */
	public void setImg(double im) {
		this.im = im;
	}
	
	/**
	 * Return an array of doubles equal to the real parts of the array of Complex passed in argument.
	 */
	static public double[] rValues(Complex arr[]) {
		double r[] = new double[arr.length];
		for (int i = 0; i < arr.length; i++) 
			r[i] = arr[i].getReal();
		return r;
	}
	
	/**
	 * Return an array of array of doubles equal to the imaginary parts of the array of array of Complex passed in argument.
	 */
	static public double[][] rValues(Complex arr[][]) {
		double r[][] = new double[arr.length][];
		for (int j = 0; j < arr.length; j++) {
			r[j] = new double[arr[j].length];
			for (int k = 0; k < arr[j].length; k++)
				r[j][k] = arr[j][k].getReal();
		}
		return r;
	}
	/**
	 * Return an array of doubles equal to the imaginary parts of the array of Complex passed in argument.
	 */
	static public double[] iValues(Complex arr[]) {
		double i[] = new double[arr.length];
		for (int j = 0; j < arr.length; j++) 
			i[j] = arr[j].getReal();
		return i;
	}
	
	/**
	 * Return an array of array of doubles equal to the imaginary parts of the array of array of Complex passed in argument.
	 */
	static public double[][] iValues(Complex arr[][]) {
		double i[][] = new double[arr.length][];
		for (int j = 0; j < arr.length; j++) {
			i[j] = new double[arr[j].length];
			for (int k = 0; k < arr[j].length; k++)
				i[j][k] = arr[j][k].getReal();
		}
		return i;
	}
	
	/**
	 * Return the value of this complex number as a string of the form "x y",
	 * where x and y are strings representing double precision numbers in the 
	 * standard Java notation (for example, "56.78 12.34").	 
	 * @return The value of this complex number as a string of the form "x y".
	 */
	public String toString() {
		return Double.toString(re) + " " + Double.toString(im);
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(this.re);
		dos.writeDouble(this.im);
	}
	
	public static void toBin(Complex[] values1D, DataOutputStream dos) throws IOException {
		dos.writeInt(values1D.length);
		for (Complex value : values1D)
			value.toBin(dos);
	}
	
	public static void toBin(Complex[][] values2D, DataOutputStream dos) throws IOException {
		dos.writeInt(values2D.length);
		dos.writeInt(values2D[0].length);
		for (Complex[] values1D : values2D)
			for (Complex value : values1D)
				value.toBin(dos);
	}	

	public static void toBin(Complex[][][] values3D, DataOutputStream dos) throws IOException {
		dos.writeInt(values3D.length);
		dos.writeInt(values3D[0].length);
		dos.writeInt(values3D[0][0].length);
		for (Complex[][] values2D : values3D)
			for (Complex[] values1D : values2D)
				for (Complex value : values1D)
					value.toBin(dos);
	}
	
	/**
	 * Read the binary representation of a Complex from a BODataInputStream
	 * and use the read value to set a  Complex.
	 * @param dis the BODataInputStream to be read
	 * @return a Complex
	 * @throws IOException
	 */
	public static Complex fromBin(BODataInputStream dis) throws IOException {
		return new Complex(dis.readDouble(), dis.readDouble());
	}
	
	public static Complex[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Complex complex[] = new Complex[dim1];
		for (int i = 0; i < dim1; i++)
			complex[i] = Complex.fromBin(dis);
		return complex;
	}
	
	public static Complex[][] from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Complex complex[][] = new Complex[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim1; j++)
				complex[i][j] = Complex.fromBin(dis);
		return complex;
	}
	
	public static Complex[][][] from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Complex complex[][][] = new Complex[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim1; j++)
				for (int k = 0; k< dim3; k++)
					complex[i][j][k] = Complex.fromBin(dis);
		return complex;
	}	
	
	/**
	 * Return an IDL Complex object.
	 * @return An IDL Complex object.
	 */
	public IDLComplex toIDLComplex() {
		return new IDLComplex(re,im);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Complex and its value is equal to this complex number.
	 * @param o The object to which this complex number is being compared.
	 * @return true if and only if the specified object o is a
	 * Complex and its value is equal to this complex number.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Complex))
			return false;
		return this.re == ((Complex)o).re && this.im == ((Complex)o).im;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this complex number is zero.
	 * @return True if and only if this complex number is zero.
	 */
	public boolean isZero() {
		return re == 0.0 && im == 0.0;
	}
	
	/**
	 * Compare this Complex to the specified Object, which must be
	 * a Complex, returning -1, 0, or +1 if the magnitude of this 
	 * complex number is less than, equal to, or greater than the 
	 * specified complex number.
	 * @param o The Object to which this Complex is being compared.
	 * @return -1, 0, or +1 if this complex number is less than, equal to, or 
	 * greater than the specified object, which must be a Complex.
	 * @throws IllegalArgumentException If the object being compared is not a Complex.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Interval))
			throw new IllegalArgumentException("Attempt to compare a Complex to a non-Complex.");
		Complex c = (Complex)o;
		double magThis = Math.sqrt(re * re + im * im);
		double magO = Math.sqrt(c.re * c.re + c.im * c.im);
		if (magThis < magO)
			return -1;
		if (magThis > magO)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this complex number is equal to the specified 
	 * Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is equal to the specified Complex.
	 */
	public boolean eq(Complex t) {
		return re == t.re && im == t.im; 
	}
	
	/**
	 * Return true if and only if this complex number is not equal to the specified 
	 * Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is not equal to the specified Complex.
	 */
	public boolean ne(Complex t) {
		return re != t.re || im != t.im; 
	}
	
	/**
	 * Return true if and only if this complex number is less than the specified 
	 * Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is less than the specified Complex.
	 */
	public boolean lt(Complex t) {
		return compareTo(t) == -1; 
	}
	
	/**
	 * Return true if and only if this complex number is less than or equal to
	 * the specified Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is less than or equal to
	 * the specified Complex.
	 */
	public boolean le(Complex t) {
		int n = compareTo(t);
		return n == -1 || n == 0; 
	}
	
	/**
	 * Return true if and only if this complex number is greater than the specified 
	 * Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is greater than the specified Complex.
	 */
	public boolean gt(Complex t) {
		return compareTo(t) == 1; 
	}
	
	/**
	 * Return true if and only if this complex number is greater than or equal to
	 * the specified Complex.
	 * @param t The Complex to which this Complex is being compared.
	 * @return True, if and only if this complex number is greater than or equal to
	 * the specified Complex.
	 */
	public boolean ge(Complex t) {
		int n = compareTo(t);
		return n == 1 || n == 0; 
	}
	
	/**
	 * Add a specified complex number, x, to this complex number (this + x). 
	 * @param x The complex number to be added to this complex number.
	 * @return This complex number after the addition.
	 */
	public Complex add(Complex x) {
		this.re += x.re;
		this.im += x.im;
		return this;
	}

	/**
	 * Subtract a specified complex number, x, from this complex number (this - x).
	 * @param x The complex number to be subtracted from this complex number
	 * @return This complex number after the subtraction.
	 */
	public Complex sub(Complex x) {
		this.re -= x.re;
		this.im -= x.im;
		return this;
	}

	/**
	 * Multiply this complex number by a specified a complex number (this * x).
	 * @param x The complex number to be multiplied.
	 * @return This complex number after the multiplication.
	 */
	public Complex mult(Complex x) {
		this.re = this.re * x.re - this.im * x.im;
		this.im = this.re * x.im + this.im * x.re;
		return this;
	}

	/**
	 * Divide this complex number by a specified a complex number (this / x).
	 * @param x The complex number by which this number is divided.
	 * @return This complex number after the division.
	 */
	public Complex div(Complex x) {
		double dem = x.re * x.re + x.im * x.im;
		this.re = (this.re * x.re + this.im * x.im) / dem;
		this.im = (this.im * x.re - this.re * x.im) / dem;
		return this;
	}

	/**
	 * Replace this complex number by its complex conjugate.
	 * @return This complex number.
	 */
	public Complex conjugate() {
		im = -im;
		return this;
	}	
	
}
