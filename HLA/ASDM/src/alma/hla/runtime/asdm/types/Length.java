/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Length.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLLength;

/**
 * The Length class implements a concept of a length in meters.
 * 
 * @version 1.00 Nov 10, 2004
 * @author Allen Farris
 */
public class Length implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Kilometer, km, an allowed unit of length.
	 */
	static public final String KILOMETER 	= "km";

	/**
	 * Meter, m, an allowed unit of length.
	 */
	static public final String METER 		= "m";
	
	/**
	 * Centimeter, cm, an allowed unit of length.
	 */
	static public final String CENTIMETER 	= "cm";
	
	/**
	 * Millimeter, mm, an allowed unit of length.
	 */
	static public final String MILLIMETER 	= "mm";
	
	/**
	 * An array of strings containing the allowable units of Length.
	 */
	static public final String[] AllowedUnits = {KILOMETER, METER, CENTIMETER, MILLIMETER }; 
	
	/**
	 * Return the canonical unit associated with this Length, meter.
	 * @return The unit associated with this Length.
	 */
	static public String unit() {
		return METER;
	}
	
	/**
	 * Return a new Length that is the sum of the 
	 * specified lengths, l1 +  l2.
	 * @param l1 The first Length of the pair.
	 * @param l2 The second Length of the pair.
	 * @return A new Length that is the sum of the 
	 * specified lengths, l1 +  l2.
	 */
	static public Length add (Length l1, Length l2) {
		return new Length (l1.length + l2.length);
	}
	
	/**
	 * Return a new Length that is the difference of the 
	 * specified lengths, l1 - l2.
	 * @param l1 The first Length of the pair.
	 * @param l2 The second Length of the pair.
	 * @return A new Length that is the difference of the 
	 * specified lengths, l1 - l2.
	 */
	static public Length sub (Length l1, Length l2) {
		return new Length (l1.length - l2.length);
	}
	
	/**
	 * Return a new Length that is the product of a specified 
	 * length and some specified factor, l * factor.
	 * @param l The base length
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Length that is the product of a specified 
	 * length and some specified factor, l * factor.
	 */
	static public Length mult (Length l, double factor) {
		return new Length (l.length * factor);
	}
	
	/**
	 * Return a new Length that is a specified length divided by a 
	 * specified factor, l / factor.
	 * @param l The base length
	 * @param factor The factor that is used to divide the value.
	 * @return A new Length that is the product of a specified 
	 * length and some specified factor, l / factor.
	 */
	static public Length div (Length l, double factor) {
		return new Length (l.length / factor);
	}
	
	static public Length getLength(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Length (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The length in meters.
	 */
	private double length;
	
	/**
	 * Create a Length of zero meters.
	 */
	public Length() {
	}

	/**
	 * Create a Length whose initial value is a string designating the length in meters
	 * as a double precision number in the standard Java notation (for example, "2124.0").
	 * @param s A string designating the length in meters
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Length (String s) {
		length = Double.parseDouble(s);
	}
	
	/**
	 * Create a Length whose initial value is a string in the specified units.
	 * @param value The initial value of the length in the specified units.
	 * @param units A string specifying the unit of length, which must be one
	 * of the allowable units of Length.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Length(String value, String units) {
		set(value,units);
	}
	
	/**
	 * Create a Length whose initial value is the same as the specified length.
	 * @param l The specified length that is used as the initial value of this length.
	 */
	public Length (Length l) {
		this.length = l.length;
	}
	
	/**
	 * Create a Length from an IDL length object.
	 * @param l The IDL length object.
	 */
	public Length (IDLLength l) {
		this.length = l.value;
	}
	
	/**
	 * Create a Length whose initial value is the specified length in meters,
	 * for example 2124.0.
	 * @param length The length in meters.
	 */
	public Length (double length) {
		this.length = length;
	}
	
	/**
	 * Create a Length whose initial value is a double in the specified units.
	 * @param value The initial value of the length in the specified units.
	 * @param units A string specifying the unit of length, which must be one
	 * of the allowable units of Length.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Length(double value, String units) {
		set(value,units);
	}
	
	/**
	 * Return the value of this length in meters.
	 * @return The value of this length in meters.
	 */
	public double get() {
		return length;
	}
	
	/**
	 * Return the value of this length as a double in the specified units. 
	 * @param units The units in which the value of this Length is to be returned.
	 * @return The value of this length as a double in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public double get(String units) {
		if (units.equals(KILOMETER))
			return length / 1000.0;
		else if (units.equals(METER))
			return length;
		else if (units.equals(CENTIMETER))
			return length * 100.0;
		else if (units.equals(MILLIMETER))
			return length * 1000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Set the value of this length to the specified length in meters.
	 * @param length The specified length in meters.
	 */
	public void set(double length) {
		this.length = length;
	}
	
	/**
	 * Set the value of this length to the specified string in the specified units.
	 * @param value The value to which this length is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(String value, String units) {
		if (units.equals(KILOMETER))
			length = Double.parseDouble(value)* 1000.0;
		else if (units.equals(METER))
			length = Double.parseDouble(value);
		else if (units.equals(CENTIMETER))
			length = Double.parseDouble(value) / 100.0;
		else if (units.equals(MILLIMETER))
			length = Double.parseDouble(value) / 1000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");	
	}
	
	/**
	 * Set the value of this length to the specified double in the specified units.
	 * @param length The value to which this length is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(double length, String units) {
		if (units.equals(KILOMETER))
			this.length = length * 1000.0;
		else if (units.equals(METER))
			this.length = length;
		else if (units.equals(CENTIMETER))
			this.length = length / 100.0;
		else if (units.equals(MILLIMETER))
			this.length = length / 1000.0;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Return the value of this Length as a String in units of meters,
	 * for example, "2124.0".
	 */
	public String toString() {
		return Double.toString(length);
	}
	
	/**
	 * Return the value of this length as a String in the specified units. 
	 * @param units The units in which the value of this Length is to be displayed.
	 * @return The value of this length as a String in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public String toString(String units) {
		if (units.equals(KILOMETER))
			return Double.toString(length / 1000.0);
		else if (units.equals(METER))
			return Double.toString(length);
		else if (units.equals(CENTIMETER))
			return Double.toString(length * 100.0);
		else if (units.equals(MILLIMETER))
			return Double.toString(length *1000.0);
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}

	/**
	 * Return an array of double containing the values of the array of Length objects passed in argument.
	 */
	static public double[] values(Length[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Return an array of of array double containing the values of the array of array of Length objects passed in argument.
	 */
	static public double[][] values(Length[][] items) {
		double array[][] = new double[items.length][];
		for (int i = 0; i < items.length; i++ ) {
			array[i] = new double[items[i].length];
			for (int j = 0; j < items[i].length; j++)
				array[i][j] = items[i][j].get();
		}
		return array;
	}	
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(length);
	}
	
	/**
	 * Write the binary representation of a 1D array of Length into a DataOutputStream.
	 * @param length
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(Length[] length, DataOutputStream dos) throws IOException {
		dos.writeInt(length.length);
		for (int i = 0; i < length.length; i++)
			length[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of length into a DataOutputStream.
	 * @param length
	 * @param dos
	 * @throws IOException 
	 */
	public static void toBin(Length[][] length, DataOutputStream dos) throws IOException {
		dos.writeInt(length.length);
		dos.writeInt(length[0].length);
		for (int i = 0; i < length.length; i++)
			for (int j=0; j < length[0].length; j++)
				length[i][j].toBin(dos);
	}
	
	/**
	 * Read the binary representation of an Length from a BODataInputStream
	 * and use the read value to set an  Length.
	 * @param dis the BODataInputStream to be read
	 * @return an Length
	 * @throws IOException
	 */
	public static Length fromBin(BODataInputStream dis) throws IOException {
		Length length = new Length();
		length.set(dis.readDouble());
		return length;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Length from a BODataInputStream
	 * and use the read value to set a 1D array of  Length.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Length
	 * @throws IOException
	 */
	public static Length[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Length length[] = new Length[dim1];
		for (int i = 0; i < dim1; i++) 
			length[i] = Length.fromBin(dis);
		return length;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Length from a BODataInputStream
	 * and use the read value to set a 2D array of  Length.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Length
	 * @throws IOException
	 */
	public static Length[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Length[][] length = new Length[dim1][dim2];
		length = new Length[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				length[i][j] = Length.fromBin(dis);
		return length;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Length from a BODataInputStream
	 * and use the read value to set a 3D array of  Length.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Length
	 * @throws IOException
	 */
	public static Length[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Length[][][] length = new Length[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					length[i][j][k] = Length.fromBin(dis);
		return length;
	}
	
	/**
	 * Return an IDL Length object.
	 * @return An IDL Length object.
	 */
	public IDLLength toIDLLength() {
		return new IDLLength(length);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Length and its value is equal to this length.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Length and its value is equal to this length.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Length))
			return false;
		return this.length == ((Length)o).length;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this length is zero.
	 * @return True if and only if this length is zero.
	 */
	public boolean isZero() {
		return length == 0.0;
	}
	
	/**
	 * Compare this Length to the specified Object, which must be
	 * a Length, returning -1, 0, or +1 if this length is less 
	 * than, equal to, or greater than the specified Length.
	 * @param o The Object to which this Length is being compared.
	 * @return -1, 0, or +1 if this length is less than, equal to, or 
	 * greater than the specified object, which must be a Length.
	 * @throws IllegalArgumentException If the object being compared is not a Length.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Length))
			throw new IllegalArgumentException("Attempt to compare a Length to a non-Length.");
		if (length < ((Length)o).length)
			return -1;
		if (length > ((Length)o).length)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Length is equal to the specified 
	 * Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is equal to the specified Length.
	 */
	public boolean eq(Length t) {
		return length == t.length; 
	}
	
	/**
	 * Return true if and only if this Length is not equal to the specified 
	 * Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is not equal to the specified Length.
	 */
	public boolean ne(Length t) {
		return length != t.length; 
	}
	
	/**
	 * Return true if and only if this Length is less than the specified 
	 * Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is less than the specified Length.
	 */
	public boolean lt(Length t) {
		return length < t.length; 
	}
	
	/**
	 * Return true if and only if this Length is less than or equal to
	 * the specified Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is less than or equal to the 
	 * specified Length.
	 */
	public boolean le(Length t) {
		return length <= t.length; 
	}
	
	/**
	 * Return true if and only if this Length is greater than the specified 
	 * Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is greater than the specified Length.
	 */
	public boolean gt(Length t) {
		return length > t.length; 
	}
	
	/**
	 * Return true if and only if this Length is greater than or equal to
	 * the specified Length.
	 * @param t The Length to which this Length is being compared.
	 * @return True, if and only if this Length is greater than or equal to the 
	 * specified Length.
	 */
	public boolean ge(Length t) {
		return length >= t.length; 
	}
	
	/**
	 * Add a specified Length to this length, (this + x).
	 * @param x The Length to be added to this length.
	 * @return This length after the addition.
	 */
	public Length add (Length x) {
		length += x.length;
		return this;
	}
	
	/**
	 * Subtract a specified Length from this length, (this - x).
	 * @param x The Length to be subtracted to this length.
	 * @return This length after the subtraction.
	 */
	public Length sub (Length x) {
		length -= x.length;
		return this;
	}
	
	/**
	 * Multiply this length by some factor, (this * factor).
	 * @param factor The factor by which this length is to be multiplied.
	 * @return This length after the multiplication.
	 */
	public Length mult (double factor) {
		length *= factor;
		return this;
	}
	
	/**
	 * Divide this length by some factor, (this / factor).
	 * @param factor The factor by which this length is to be divided.
	 * @return This length after the division.
	 */
	public Length div (double factor) {
		length /= factor;
		return this;
	}
	
}
