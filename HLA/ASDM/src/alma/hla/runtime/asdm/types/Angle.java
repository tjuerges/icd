/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Angle.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLAngle;

/**
 * The Angle class implements a concept of an angle in radians.
 * 
 * @version 1.00 Nov 17, 2004
 * @author Allen Farris
 */
public class Angle implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * The factor by which to multiple an angle in radians to convert it to degrees. 
	 */
	static public final double radToDeg = 180.0 / Math.PI;
	
	/**
	 * The factor by which to multiple an angle in radians to convert it to hours. 
	 */
	static public final double radToHour =  12.0 / Math.PI;
	
	/**
	 * The factor by which to multiple an angle in radians to convert it to arcseconds. 
	 */
	static public final double radToArcsecond = (3600.0 * 180.0) / Math.PI;
	
	/**
	 * The factor by which to multiple an angle in degrees to convert it to radians. 
	 */
	static public final double degToRad = Math.PI / 180.0;
	
	/**
	 * The factor by which to multiple an angle in hours to convert it to radians. 
	 */
	static public final double hourToRad =  Math.PI / 12.0;
	
	/**
	 * The factor by which to multiple an angle in arcseconds to convert it to radians. 
	 */
	static public final double arcsecondToRad = Math.PI / (3600.0 * 180.0);
	
	/**
	 * Radian, rad, an allowed unit of angle.
	 */
	static public final String RADIAN		= "rad";
	
	/**
	 * Degree, deg, an allowed unit of angle.
	 */
	static public final String DEGREE 		= "deg";
	
	/**
	 * Hour, hr, an allowed unit of angle.
	 */
	static public final String HOUR 		= "hr";
	
	/**
	 * Arcsecond, arcsec, an allowed unit of angle.
	 */
	static public final String ARCSECOND 	= "arcsec";
	
	/**
	 * An array of strings containing the allowable units of Angle.
	 */
	static public final String[] AllowedUnits = { RADIAN, DEGREE, HOUR, ARCSECOND }; 
	
	/**
	 * Return the canonical unit associated with this Angle, radian.
	 * @return The unit associated with this Angle.
	 */
	static public String unit() {
		return RADIAN;
	}
	
	/**
	 * Return a new Angle that is the sum of the 
	 * specified angles, a1 +  a2.
	 * @param a1 The first Angle of the pair.
	 * @param a2 The second Angle of the pair.
	 * @return A new Angle that is the sum of the 
	 * specified angles, a1 +  a2.
	 */
	static public Angle add (Angle a1, Angle a2) {
		return new Angle (a1.angle + a2.angle);
	}
	
	/**
	 * Return a new Angle that is the difference of the 
	 * specified angles, a1 - a2.
	 * @param a1 The first Angle of the pair.
	 * @param a2 The second Angle of the pair.
	 * @return A new Angle that is the difference of the 
	 * specified angles, a1 - a2.
	 */
	static public Angle sub (Angle a1, Angle a2) {
		return new Angle (a1.angle - a2.angle);
	}
	
	/**
	 * Return a new Angle that is the product of a specified 
	 * angle and some specified factor, a * factor.
	 * @param a The base angle
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Angle that is the product of a specified 
	 * angle and some specified factor, a * factor.
	 */
	static public Angle mult (Angle a, double factor) {
		return new Angle (a.angle * factor);
	}
	
	/**
	 * Return a new Angle that is a specified angle divided by a 
	 * specified factor, a / factor.
	 * @param a The base angle
	 * @param factor The factor that is used to divide the value.
	 * @return A new Angle that is the product of a specified 
	 * angle and some specified factor, a / factor.
	 */
	static public Angle div (Angle a, double factor) {
		return new Angle (a.angle / factor);
	}
	
	static public Angle getAngle(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Angle (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The angle in radians.
	 */
	private double angle;
	
	/**
	 * Create an Angle of zero radians.
	 */
	public Angle() {
	}

	/**
	 * Create an Angle whose initial value is a string designating the angle in radians
	 * as a double precision number in the standard Java notation (for example, "0.7853975").
	 * @param s A string designating the angle in radians
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Angle (String s) {
		angle = Double.parseDouble(s);
	}
	
	/**
	 * Create an Angle whose initial value is a string in the specified units.
	 * @param value The initial value of the angle in the specified units.
	 * @param units A string specifying the unit of angle, which must be one
	 * of the allowable units of Angle.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Angle(String value, String units) {
		set(value,units);
		/*
		if (units.equals(RADIAN))
			angle = Double.parseDouble(value);
		else if (units.equals(DEGREE))
			angle = Double.parseDouble(value) * degToRad;
		else if (units.equals(HOUR))
			angle = Double.parseDouble(value) * hourToRad;
		else if (units.equals(ARCSECOND))
			angle = Double.parseDouble(value) * arcsecondToRad;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
		*/
	}
	
	/**
	 * Create an Angle whose initial value is the same as the specified angle.
	 * @param a The specified angle that is used as the initial value of this angle.
	 */
	public Angle (Angle a) {
		this.angle = a.angle;
	}
	
	/**
	 * Create an Angle from an IDL Angle object.
	 * @param a The IDL angle object.
	 */
	public Angle (IDLAngle a) {
		this.angle = a.value;
	}
	
	/**
	 * Create an Angle whose initial value is the specified angle in radians,
	 * for example 0.7853975.
	 * @param angle The angle in radians.
	 */
	public Angle (double angle) {
		this.angle = angle;
	}
	
	/**
	 * Create an Angle whose initial value is a double in the specified units.
	 * @param value The initial value of the angle in the specified units.
	 * @param units A string specifying the unit of angle, which must be one
	 * of the allowable units of Angle
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */
	public Angle(double value, String units) {
		set(value,units);
		/*
		if (units.equals(RADIAN))
			angle = value;
		else if (units.equals(DEGREE))
			angle = value * degToRad;
		else if (units.equals(HOUR))
			angle = value * hourToRad;
		else if (units.equals(ARCSECOND))
			angle = value * arcsecondToRad;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
		*/
	}
	
	/**
	 * Return the value of this angle in radians.
	 * @return The value of this angle in radians.
	 */
	public double get() {
		return angle;
	}
	
	/**
	 * Return the value of this angle as a double in the specified units. 
	 * @param units The units in which the value of this Angle is to be returned.
	 * @return The value of this angle as a double in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public double get(String units) {
		if (units.equals(RADIAN))
			return angle;
		else if (units.equals(DEGREE))
			return angle * radToDeg;
		else if (units.equals(HOUR))
			return angle * radToHour;
		else if (units.equals(ARCSECOND))
			return angle * radToArcsecond;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Set the value of this angle to the specified angle in radians.
	 * @param angle The specified angle in radians.
	 */
	public void set(double angle) {
		this.angle = angle;
	}
	
	/**
	 * Set the value of this angle to the specified string in the specified units.
	 * @param value The value to which this angle is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(String value, String units) {
		if (units.equals(RADIAN))
			angle = Double.parseDouble(value);
		else if (units.equals(DEGREE))
			angle = Double.parseDouble(value) * degToRad;
		else if (units.equals(HOUR))
			angle = Double.parseDouble(value) * hourToRad;
		else if (units.equals(ARCSECOND))
			angle = Double.parseDouble(value) * arcsecondToRad;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");	
	}
	
	/**
	 * Set the value of this angle to the specified string in the specified units.
	 * @param value The value to which this angle is to be set. 
	 * @param units The units in which this specified value is expressed.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public void set(double value, String units) {
		if (units.equals(RADIAN))
			this.angle = value;
		else if (units.equals(DEGREE))
			this.angle = value * degToRad;
		else if (units.equals(HOUR))
			this.angle = value * hourToRad;
		else if (units.equals(ARCSECOND))
			this.angle = value * arcsecondToRad;
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}
	
	/**
	 * Return the value of this Angle as a String in units of radians,
	 * for example, "0.7853975".
	 */
	public String toString() {
		return Double.toString(angle);
	}
	
	/**
	 * Return an array of double containing the values of the array of Angle objects passed in argument.
	 */
	static public double[] values(Angle[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Return an array of of array double containing the values of the array of array of Angle objects passed in argument.
	 */
	static public double[][] values(Angle[][] items) {
		double array[][] = new double[items.length][];
		for (int i = 0; i < items.length; i++ ) {
			array[i] = new double[items[i].length];
			for (int j = 0; j < items[i].length; j++)
				array[i][j] = items[i][j].get();
		}
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(angle);
	}
	
	/**
	 * Write the binary representation of a 1D array of angle into a DataOutputStream.
	 * @param angle the array of Angle to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Angle[] angle, DataOutputStream dos) throws IOException {
		dos.writeInt(angle.length);
		for (int i = 0; i < angle.length; i++)
			angle[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Angle into a DataOutputStream.
	 * @param angle the 2D array of Angle to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Angle[][] angle, DataOutputStream dos) throws IOException {
		dos.writeInt(angle.length);
		dos.writeInt(angle[0].length);
		for (int i = 0; i < angle.length; i++)
			for (int j=0; j < angle[0].length; j++)
				angle[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Angle into a DataOutputStream.
	 * @param angle the 3D array of Angle to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Angle[][][] angle, DataOutputStream dos) throws IOException {
		dos.writeInt(angle.length);
		dos.writeInt(angle[0].length);
		dos.writeInt(angle[0][0].length);
		for (int i = 0; i < angle.length; i++)
			for (int j=0; j < angle[0].length; j++)
				for (int k = 0; k < angle[0][0].length; k++)
					angle[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Angle from a BODataInputStream
	 * and use the read value to set an  Angle.
	 * @param dis the BODataInputStream to be read
	 * @return an Angle
	 * @throws IOException
	 */
	public static Angle fromBin(BODataInputStream dis) throws IOException {
		Angle angle = new Angle();
		angle.set(dis.readDouble());
		return angle;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Angle from a BODataInputStream
	 * and use the read value to set a 1D array of  Angle.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Angle
	 * @throws IOException
	 */
	public static Angle[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Angle angle[] = new Angle[dim1];
		for (int i = 0; i < dim1; i++) 
			angle[i] = Angle.fromBin(dis);
		return angle;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Angle from a BODataInputStream
	 * and use the read value to set a 2D array of  Angle.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Angle
	 * @throws IOException
	 */
	public static Angle[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Angle[][] angle = new Angle[dim1][dim2];
		angle = new Angle[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				angle[i][j] = Angle.fromBin(dis);
		return angle;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Angle from a BODataInputStream
	 * and use the read value to set a 3D array of  Angle.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Angle
	 * @throws IOException
	 */
	public static Angle[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Angle[][][] angle = new Angle[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					angle[i][j][k] = Angle.fromBin(dis);
		return angle;
	}
	
	/**
	 * Return the value of this angle as a String in the specified units. 
	 * @param units The units in which the value of this angle is to be displayed.
	 * @return The value of this angle as a String in the specified units.
	 * @throws IllegalArgumentException If the specified unit is not an allowable unit. 
	 */	
	public String toString(String units) {
		if (units.equals(RADIAN))
			return Double.toString(angle);
		else if (units.equals(DEGREE))
			return Double.toString(angle * radToDeg);
		else if (units.equals(HOUR))
			return Double.toString(angle * radToHour);
		else if (units.equals(ARCSECOND))
			return Double.toString(angle * radToArcsecond);
		else
			throw new IllegalArgumentException(units + " is not an alllowable unit.");
	}

	/**
	 * Return an IDL Angle object.
	 * @return An IDL Angle object.
	 */
	public IDLAngle toIDLAngle() {
		return new IDLAngle(angle);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Angle and its value is equal to this angle.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Angle and its value is equal to this angle.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Angle))
			return false;
		return this.angle == ((Angle)o).angle;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this angle is zero.
	 * @return True if and only if this angle is zero.
	 */
	public boolean isZero() {
		return angle == 0.0;
	}
	
	/**
	 * Compare this Angle to the specified Object, which must be
	 * an Angle, returning -1, 0, or +1 if this angle is less 
	 * than, equal to, or greater than the specified Angle.
	 * @param o The Object to which this Angle is being compared.
	 * @return -1, 0, or +1 if this angle is less than, equal to, or 
	 * greater than the specified object, which must be an Angle.
	 * @throws IllegalArgumentException If the object being compared is not an Angle.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Angle))
			throw new IllegalArgumentException("Attempt to compare an Angle to a non-Angle.");
		if (angle < ((Angle)o).angle)
			return -1;
		if (angle > ((Angle)o).angle)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Angle is equal to the specified 
	 * Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is equal to the specified Angle.
	 */
	public boolean eq(Angle t) {
		return angle == t.angle; 
	}
	
	/**
	 * Return true if and only if this Angle is not equal to the specified 
	 * Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is not equal to the specified Angle.
	 */
	public boolean ne(Angle t) {
		return angle != t.angle; 
	}
	
	/**
	 * Return true if and only if this Angle is less than the specified 
	 * Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is less than the specified Angle.
	 */
	public boolean lt(Angle t) {
		return angle < t.angle; 
	}
	
	/**
	 * Return true if and only if this Angle is less than or equal to
	 * the specified Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is less than or equal to the 
	 * specified Angle.
	 */
	public boolean le(Angle t) {
		return angle <= t.angle; 
	}
	
	/**
	 * Return true if and only if this Angle is greater than the specified 
	 * Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is greater than the specified Angle.
	 */
	public boolean gt(Angle t) {
		return angle > t.angle; 
	}
	
	/**
	 * Return true if and only if this Angle is greater than or equal to
	 * the specified Angle.
	 * @param t The Angle to which this Angle is being compared.
	 * @return True, if and only if this Angle is greater than or equal to the 
	 * specified Angle.
	 */
	public boolean ge(Angle t) {
		return angle >= t.angle; 
	}
	
	/**
	 * Add a specified Angle to this angle, (this + x).
	 * @param x The Angle to be added to this angle.
	 * @return This angle after the addition.
	 */
	public Angle add (Angle x) {
		angle += x.angle;
		return this;
	}
	
	/**
	 * Subtract a specified Angle from this angle, (this - x).
	 * @param x The Angle to be subtracted to this angle.
	 * @return This angle after the subtraction.
	 */
	public Angle sub (Angle x) {
		angle -= x.angle;
		return this;
	}
	
	/**
	 * Multiply this angle by some factor, (this * factor).
	 * @param factor The factor by which this angle is to be multiplied.
	 * @return This angle after the multiplication.
	 */
	public Angle mult (double factor) {
		angle *= factor;
		return this;
	}
	
	/**
	 * Divide this angle by some factor, (this / factor).
	 * @param factor The factor by which this angle is to be divided.
	 * @return This angle after the division.
	 */
	public Angle div (double factor) {
		angle /= factor;
		return this;
	}
	
	/**
	 * Return the trigonometric sine of this angle.
	 * @return The trigonometric sine of this angle.
	 */
	public double sin() {
		return Math.sin(angle);
	}
	
	/**
	 * Return the trigonometric cosine of this angle.
	 * @return The trigonometric cosine of this angle.
	 */
	public double cos() {
		return Math.cos(angle);
	}
	
	/**
	 * Return the trigonometric tangent of this angle.
	 * @return The trigonometric tangent of this angle.
	 */
	public double tan() {
		return Math.tan(angle);
	}
	
	/**
	 * Return the arc sine of this angle, in the range of -<i>pi</i>/2 through <i>pi</i>/2.
	 * @return The arc sine of this angle, in the range of -<i>pi</i>/2 through <i>pi</i>/2.
	 */
	public double asin() {
		return Math.asin(angle);
	}
	
	/**
	 * Return the arc cosine of this angle, in the range of 0.0 through <i>pi</i>.
	 * @return The arc cosine of this angle, in the range of 0.0 through <i>pi</i>.
	 */
	public double acos() {
		return Math.acos(angle);
	}
	
	/**
	 * Return the arc tangent of this angle, in the range of -<i>pi</i>/2 through <i>pi</i>/2.
	 * @return The arc tangent of this angle, in the range of -<i>pi</i>/2 through <i>pi</i>/2.
	 */
	public double atan() {
		return Math.atan(angle);
	}

}
