/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestWriteXML.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import alma.asdmIDLTypes.IDLArrayTimeInterval;

/**
 * @author caillat
 *
 * 
 */
public class ArrayTimeInterval {

	private ArrayTime start;
	private Interval duration;
	
	static private boolean readStartTimeDurationInXML_ = false;
	static private boolean readStartTimeDurationInBin_ = false;

	// Constructors
	/**
	 * Construct an ArrayTimeInterval out of nothing.
	 * start and duration are set to 0. Not really useful !
	 */
	public ArrayTimeInterval() {
		start = new ArrayTime(0);
		duration = new Interval(0);
	}
    
	/**
	 * Copy constructor.
	 * @param ait the instance of ArrayTimeInterval to be copied from.
	 */
    public ArrayTimeInterval(ArrayTimeInterval ait) {
        start = new ArrayTime(ait.getStart());
        duration = new Interval(ait.getDuration());
    }
	
    /**
     * Construct an ArrayTimeInterval out of a pair (ArrayTime, Interval).
     * @param start_ the ArrayTime defining the start time.
     * @param duration_ the Interval defining the duration.
     * 
	 * @note the duration is clipped to the highest possible value if necessary.
     */
	public ArrayTimeInterval( ArrayTime  start_,
					  		  Interval  duration_) {
		start = new ArrayTime(start_);
		//duration = new Interval(duration_); // Fixed in Socorro the 28th of Mars 2009.
		duration = new Interval(Math.min(Long.MAX_VALUE - start.get(), duration_.get()));
	}
	
	/**
	 * Construct an ArrayTimeInterval out of a (double, double) pair.
	 *
	 * @param startInMJD a date in Modified Julian Date defining the start time.
	 * @param durationInDays a number of days defining the duration.
	 * 
	 * @note the duration is clipped to the highest possible value if necessary.
	 */					  
	public ArrayTimeInterval(double startInMJD,
			  				 double durationInDays) {
		start = new ArrayTime(startInMJD); 
		duration = new Interval(Math.min(Long.MAX_VALUE,(long) (ArrayTime.unitsInADay * durationInDays)));
	}
			  
	/**
	 * Construct a ArrayTimeInterval out of a (long, long) pair.
	 * @param startInNanoSeconds a number of nanoseconds defining the start time.
	 * @param durationInNanoSeconds a number of nanoseconds defining the duration.
	 * 
	 * @note the duration is clipped to the highest possible value if necessary.
	 */
	public ArrayTimeInterval(long startInNanoSeconds,
			  				 long durationInNanoSeconds){
		start = new ArrayTime(startInNanoSeconds);
		duration = new Interval(Math.min(Long.MAX_VALUE - start.get(),durationInNanoSeconds));		
	}		  				  
		
	/**
	 * Construct an ArrayTimeInterval out of a single ArrayTime value.
	 * @param start_ an ArrayTime defining the start time.
	 * 
	 * @note the duration is set to the highest possible value given the start time.
	 */
	public ArrayTimeInterval(ArrayTime start_){
		start = new ArrayTime(start_) ;
		duration = new Interval(Long.MAX_VALUE - start.get());
	}
	

	/**
	 * Construct an ArrayTimeInterval out of a (ArrayTime, ArrayTime) pair.
	 * 
	 * @param start_ the ArrayTime defining the start of this ArrayTimeInterval
	 * @param end_ an ArrayTime from which the duration of this ArrayTimeInterval
	 * will be deduced by a simple substraction.
	 * 
	 * @note it's expected that end_ is posterior to start_. If that's not the case
	 * then end_ is taken equal to _start, i.e. the duration of the ArrayTimeInterval is set 
	 * to 0.
	 */
	public ArrayTimeInterval(ArrayTime start_, ArrayTime end_) {
		this.start = new ArrayTime(start_);
		this.duration = new Interval();
		if (end_.ge(start_))
			this.duration.set(end_.get() - start_.get());
		else
			this.duration.set(0);
	}
	
	/**
	 * Construct an ArrayTimeInterval out of a single double value.
	 * @param startInMJD a date in Modified Julian Date format defining the start time.
	 * 
	 * @note the duration is set to the highest possible value given the start time.
	 */
	public ArrayTimeInterval(double startInMJD) {
		start = new ArrayTime(startInMJD); 
		duration = new Interval(Long.MAX_VALUE - start.get());
	}
	
	/**
	 * Construct an ArrayTimeInterval out of a single long value.
	 * @param startInNanoSeconds a (positive !) number of nanoseconds defining the start time.
	 * 
	 * @note the duration is set to the highest possible value given the start time.
	 */
	public ArrayTimeInterval(long startInNanoSeconds){
		start = new ArrayTime(startInNanoSeconds); 
		duration = new Interval(Long.MAX_VALUE - start.get());
	}
		
	/**
	 * Construct an ArrayTimeInterval out of its CORBA/IDL representation.
	 * @param idlATI the CORBA/IDL representation.
	 * 
	 * @note the duration is clipped to the highest possible value if necessary.
	 */
    public ArrayTimeInterval(IDLArrayTimeInterval idlATI) {
        this.start = new ArrayTime(idlATI.start);
        this.duration = new Interval(Math.min(idlATI.duration, Long.MAX_VALUE - start.get()));
    }
    
	// Setters
    /**
     * set the start value of an ArrayTimeInterval.
     * @param start an ArrayTime defining the start value.
     * 
     * @note the duration is left unchanged except that if necessary it's clipped to the highest possible 
     * value given the new start time.
     */
	public void setStart(ArrayTime start) {
		this.duration.set(Math.min(this.duration.get(), Long.MAX_VALUE - start.get()));
		this.start = new ArrayTime(start);
	}

    /**
     * set the start value of an ArrayTimeInterval.
     * @param start a Modified Julian Date defining the start value.
     * 
     * @note the duration is left unchanged except that if necessary it's clipped to the highest possible 
     * value given the new start time.
     */	
    public void setStart(double start) {
		this.duration.set(Math.min(this.duration.get(), Long.MAX_VALUE - (long) (ArrayTime.unitsInADay * start)));    	
		this.start.set((long) (ArrayTime.unitsInADay * start));
	}

    /**
     * Set the start value of an ArrayTimeInterval.
     * @param start a long defining the start value as a number of nanoseconds.
     * 
     * @note the duration is left unchanged except that if necessary it's clipped to the highest possible 
     * value given the new start time.
     */	 
    public void setStart(long start) {
		this.duration.set(Math.min(this.duration.get(), Long.MAX_VALUE - start));
		this.start.set(start); 
	}

    /**
     * Set the duration of an ArrayTimeInterval.
     * @param duration an Interval defining the new duration.
     * 
     * @note if necessary the duration is clipped to the highest possible value given the start time
     * of this ArrayTimeInterval.
     */
    public void setDuration(Interval duration) {
		this.duration.set(Math.min(duration.get(), Long.MAX_VALUE - start.get()));
	}


    /**
     * Set the duration of an ArrayTimeInterval.
     * @param duration a number of days defining the new duration.
     * 
     * @note if necessary the duration is clipped to the highest possible value given the start time
     * of this ArrayTimeInterval.
     */    
    public void setDuration(double durationInDays) {
    	long durationsInDaysL = (long)(ArrayTime.unitsInADay * durationInDays);
    	this.duration.set(Math.min(durationsInDaysL, Long.MAX_VALUE - this.start.get()));
	}

 
    /**
     * Set the duration of an ArrayTimeInterval.
     * @param duration a number of nanoseconds defining the new duration.
     * 
     * @note if necessary the duration is clipped to the highest possible value given the start time
     * of this ArrayTimeInterval.
     */      
    public void setDuration(long durationInNanoSeconds) {
    	this.duration.set(Math.min(durationInNanoSeconds, Long.MAX_VALUE - this.start.get()));
	}
	
	// Getters
    /**
     * @return the start time of this ArrayTimeInterval as an ArrayTime.
     */
    public final ArrayTime getStart() {
		return start;	
	}

    /**
     * @return the midpoint of this ArrayTimeInterval as an ArrayTime.
     * 
     * The midpoint is defined as start + duration / 2
     */
    public final ArrayTime getMidPoint() {
		return new ArrayTime(start.get() + duration.get() / 2);	
	}   
    
    /**
     * @return the start time of this ArrayTimeInterval as an MJD (double).
     */
    public double getStartInMJD() {
		return start.getMJD();	
	}

    /**
     * 
     * @return the start time of this ArrayTimeInterval as a number of nanoseconds (long).
     */
    public long getStartInNanoSeconds() {
		return start.get();	
	}

    /**
     * 
     * @return the duration of this ArrayTimeInterval as an Interval.
     */
    public final Interval getDuration() {
		return duration;	
	}

    /**
     * 
     * @return the duration of this ArrayTimeInterval as a number of days (double).
     */
    public double getDurationInDays() {
		return (((double) duration.get()) / ArrayTime.unitsInADay);	
	}
    
    /**
     * 
     * @return the duration of this ArrayTimeInterval as a number of nanoseconds (long)
     */
    public long getDurationInNanoSeconds() {
		return duration.get();	
	}

    /**
     * 
     * @return the CORBA/IDL representation of this ArrayTimeInterval.
     */
    public IDLArrayTimeInterval toIDLArrayTimeInterval() {
        return new IDLArrayTimeInterval(getStartInNanoSeconds(), getDurationInNanoSeconds());
    };
    
	// Checkers
    /**
     * @return true if and only if o is an instance of ArrayTimeInterval and has the same
     * start and duration values than this ArrayTimeInterval.
     */
    public boolean equals(Object o) {
        if (!(o instanceof ArrayTimeInterval))
            return false;
        return ((ArrayTimeInterval) o).getStart().get() == start.get() && 
        ((ArrayTimeInterval) o).getDuration().get() == duration.get();     
    }
    
    /**
     * Checks if this ArrayTimeInterval overlaps the one passed as a parameter.
     * @param ati the ArrayTimeInterval to be checked for overlapping.
     * @return true if and only if there is overlapping.
     */
    public boolean overlaps(ArrayTimeInterval ati) {
		long start1 = start.get();
		long end1 = start1 + duration.get();
		
		
		long start2 = ati.getStart().get();
		long end2   = start2 + ati.getDuration().get();
		
		return (start2 <= start1 && end2 >= start1) ||
		       (start2 >= start1 && start2 <= end1);
	}
	
    
    /**
     * Checks if this ArrayTimeInterval "contains" the one passed as a parameter.
     * 
     * @param ati the ArrayTimeInterval to be checked for the "contains" relationship.
     * @return true if and only if this contains ati.
     */
    public boolean contains(ArrayTimeInterval ati) {
		long start1 = start.get();;
		long end1 = start1 + duration.get();
		
		long start2 = ati.getStart().get();
		long end2   = start2 + ati.getDuration().get();
		return ((start2>=start1) && (end2 <= end1));
	}

    /**
     * Checks if this ArrayTimeInterval "contains" the ArrayTime passed as a parameter.
     * 
     * @param at the ArrayTime to be checked for the "contains" relationship.
     * @return true if and only if this contains at.
     */
    public boolean contains(ArrayTime at) {
		long start1 = start.get();
		long end1 = start1 + duration.get();
		
		long time = at.get();
		return (time >= start1 && time < end1);
	}
    
    // Formatter
    /**
     * Override the default toString.
     */
	public String toString() {
		return (new StringBuffer("(start=")
			.append(this.getStart())
			.append(",duration=")
			.append(this.getDuration())
			.append(")")).toString();
	}
	/**
	 * Write the binary representation of this to a DataOutputStream.
	 * @param dos the DataOutputStream to write to,
	 * It successively writes midpoint and duration.
	 * @throws IOException.
	 */	
	public void toBin(DataOutputStream dos) throws IOException {
	      dos.writeLong(getMidPoint().get());
	      dos.writeLong(getDuration().get()); 			
	}
	
	
	/**
	 * Write the binary representation of a 1D array of ArrayTimeInterval into a DataOutputStream.
	 * @param angle the array of ArrayTimeInterval to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(ArrayTimeInterval[] ati, DataOutputStream dos) throws IOException {
		dos.writeInt(ati.length);
		for (int i = 0; i < ati.length; i++)
			ati[i].toBin(dos);
	}	
	
    /**
     * Defines how the representation of an ArrayTimeInterval found in subsequent reads of
     * a document containing table exported in binary  must be interpreted. The interpretation depends on the value of the argument b :
     * b == true means that it must be interpreted as (startTime, duration)
     * b == false means that it must be interpreted as (midPoint, duration)
     *
     * @param b a boolean value.
     */
    static public void readStartTimeDurationInBin(boolean b) {
    	readStartTimeDurationInBin_ = b;
    }
    
    /**
     * Returns a boolean value whose meaning is defined as follows:
     * true <=> the representation of ArrayTimeInterval object found in any binary table will be considered as (startTime, duration).
     * false <=> the representation of ArrayTimeInterval object found in any binary table will be considered as (midPoint, duration).
     *
     */
    static public boolean readStartTimeDurationInBin() {
    	return readStartTimeDurationInBin_;
    }

    /**
     * Defines how the representation of an ArrayTimeInterval found in subsequent reads of
     * a document containing table exported in XML  must be interpreted. The interpretation depends on the value of the argument b :
     * b == true means that it must be interpreted as (startTime, duration)
     * b == false means that it must be interpreted as (midPoint, duration)
     *
     * @param b a boolean value.
     */
    static public void readStartTimeDurationInXML(boolean b) {
    	readStartTimeDurationInXML_ = b;
    }

    /**
     * Returns a boolean value whose meaning is defined as follows:
     * true <=> the representation of ArrayTimeInterval object found in any binary table will be considered as (startTime, duration).
     * false <=> the representation of ArrayTimeInterval object found in any binary table will be considered as (midPoint, duration).
     *
     */
    static public boolean readStartTimeDurationInXML() {
    	return readStartTimeDurationInXML_;
    }

	/**
	 * Read the binary representation of an ArrayTimeInterval from a BODataInputStream
	 * and use the read value to set an  ArrayTimeInterval.
	 * 
	 * @param dis the BODataInputStream to be read
	 * @return an ArrayTimeInterval
	 * @throws IOException
	 * 
	 * Remember that it reads two long integers (64 bits). The first one is considered as the midpoint of the interval
	 * and the second one ist duration.
	 */
	public static ArrayTimeInterval fromBin(BODataInputStream dis) throws IOException {
		long l1 = dis.readLong();
		long l2 = dis.readLong();
		if (readStartTimeDurationInBin_) {
			return new ArrayTimeInterval(l1, l2);
		}
		else {
			return new ArrayTimeInterval(l1 - l2 / 2, l2);
		}
	}
	
	/**
	 * Read the binary representation of a 1D array of  ArrayTimeInterval from a BODataInputStream
	 * and use the read value to set a 1D array of  ArrayTimeInterval.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of ArrayTimeInterval
	 * @throws IOException
	 */
	public static ArrayTimeInterval[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		ArrayTimeInterval ati[] = new ArrayTimeInterval[dim1];
		for (int i = 0; i < dim1; i++) 
			ati[i] = ArrayTimeInterval.fromBin(dis);
		return ati;
	}
}
