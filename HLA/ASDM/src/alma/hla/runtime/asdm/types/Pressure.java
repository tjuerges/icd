/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Pressure.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLPressure;

/**
 * The Pressure class implements a concept of an atmospheric pressure in hectopascals.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class Pressure implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new Pressure that is the sum of the 
	 * specified hectopascals, a +  b.
	 * @param a The first Pressure of the pair.
	 * @param b The second Pressure of the pair.
	 * @return A new Pressure that is the sum of the 
	 * specified hectopascals, a +  b.
	 */
	static public Pressure add (Pressure a, Pressure b) {
		return new Pressure (a.pressure + b.pressure);
	}
	
	/**
	 * Return a new Pressure that is the difference of the 
	 * specified hectopascals, a - b.
	 * @param a The first Pressure of the pair.
	 * @param b The second Pressure of the pair.
	 * @return A new Pressure that is the difference of the 
	 * specified hectopascals, a - b.
	 */
	static public Pressure sub (Pressure a, Pressure b) {
		return new Pressure (a.pressure - b.pressure);
	}
	
	/**
	 * Return a new Pressure that is the product of a specified 
	 * pressure and some specified factor, a * factor.
	 * @param a The base pressure
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Pressure that is the product of a specified 
	 * pressure and some specified factor, a * factor.
	 */
	static public Pressure mult (Pressure a, double factor) {
		return new Pressure (a.pressure * factor);
	}
	
	/**
	 * Return a new Pressure that is a specified pressure divided by a 
	 * specified factor, a / factor.
	 * @param a The base pressure
	 * @param factor The factor that is used to divide the value.
	 * @return A new Pressure that is the product of a specified 
	 * pressure and some specified factor, a / factor.
	 */
	static public Pressure div (Pressure a, double factor) {
		return new Pressure (a.pressure / factor);
	}
	
	static public Pressure getPressure(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Pressure (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The pressure in hectopascals.
	 */
	private double pressure;
	
	/**
	 * Create a Pressure of zero hectopascals.
	 */
	public Pressure() {
	}

	/**
	 * Create a Pressure whose initial value is a string designating the pressure in hectopascals
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the pressure in hectopascals
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Pressure (String s) {
		pressure = Double.parseDouble(s);
	}
	
	/**
	 * Create a Pressure whose initial value is the same as the specified pressure.
	 * @param l The specified pressure that is used as the initial value of this pressure.
	 */
	public Pressure (Pressure l) {
		this.pressure = l.pressure;
	}
	
	/**
	 * Create a Pressure from an IDL pressure object.
	 * @param l The IDL pressure object.
	 */
	public Pressure (IDLPressure l) {
		this.pressure = l.value;
	}
	
	/**
	 * Create a Pressure whose initial value is the specified pressure in hectopascals.
	 * @param pressure The pressure in hectopascals.
	 */
	public Pressure (double pressure) {
		this.pressure = pressure;
	}
	
	/**
	 * Return the value of this pressure in hectopascals.
	 * @return The value of this pressure in hectopascals.
	 */
	public double get() {
		return pressure;
	}
	
	/**
	 * Set the value of this pressure to the specified pressure in hectopascals.
	 * @param pressure The specified pressure in hectopascals.
	 */
	public void set(double pressure) {
		this.pressure = pressure;
	}
	
	/**
	 * Return the value of this as a String in units of hectopascals.
	 * @return The value of this as a String in units of hectopascals.
	 * 
	 */
	public String toString() {
		return Double.toString(pressure);
	}

	/**
	 * Return an array of double containing the values of the array of Pressure objects passed in argument.
	 */
	static public double[] values(Pressure[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(pressure);
	}
	
	/**
	 * Write the binary representation of a 1D array of pressure into a DataOutputStream.
	 * @param pressure the array of Pressure to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Pressure[] pressure, DataOutputStream dos) throws IOException {
		dos.writeInt(pressure.length);
		for (int i = 0; i < pressure.length; i++)
			pressure[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Pressure into a DataOutputStream.
	 * @param pressure the 2D array of Pressure to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Pressure[][] pressure, DataOutputStream dos) throws IOException {
		dos.writeInt(pressure.length);
		dos.writeInt(pressure[0].length);
		for (int i = 0; i < pressure.length; i++)
			for (int j=0; j < pressure[0].length; j++)
				pressure[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Pressure into a DataOutputStream.
	 * @param pressure the 3D array of Pressure to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Pressure[][][] pressure, DataOutputStream dos) throws IOException {
		dos.writeInt(pressure.length);
		dos.writeInt(pressure[0].length);
		dos.writeInt(pressure[0][0].length);
		for (int i = 0; i < pressure.length; i++)
			for (int j=0; j < pressure[0].length; j++)
				for (int k = 0; k < pressure[0][0].length; k++)
					pressure[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Pressure from a BODataInputStream
	 * and use the read value to set an  Pressure.
	 * @param dis the BODataInputStream to be read
	 * @return an Pressure
	 * @throws IOException
	 */
	public static Pressure fromBin(BODataInputStream dis) throws IOException {
		Pressure pressure = new Pressure();
		pressure.set(dis.readDouble());
		return pressure;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Pressure from a BODataInputStream
	 * and use the read value to set a 1D array of  Pressure.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Pressure
	 * @throws IOException
	 */
	public static Pressure[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Pressure pressure[] = new Pressure[dim1];
		for (int i = 0; i < dim1; i++) 
			pressure[i] = Pressure.fromBin(dis);
		return pressure;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Pressure from a BODataInputStream
	 * and use the read value to set a 2D array of  Pressure.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Pressure
	 * @throws IOException
	 */
	public static Pressure[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Pressure[][] pressure = new Pressure[dim1][dim2];
		pressure = new Pressure[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				pressure[i][j] = Pressure.fromBin(dis);
		return pressure;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Pressure from a BODataInputStream
	 * and use the read value to set a 3D array of  Pressure.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Pressure
	 * @throws IOException
	 */
	public static Pressure[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Pressure[][][] pressure = new Pressure[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					pressure[i][j][k] = Pressure.fromBin(dis);
		return pressure;
	}
	/**
	 * Return an IDL Pressure object.
	 * @return An IDL Pressure object.
	 */
	public IDLPressure toIDLPressure() {
		return new IDLPressure(pressure);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Pressure and its value is equal to this pressure.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Pressure and its value is equal to this pressure.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Pressure))
			return false;
		return this.pressure == ((Pressure)o).pressure;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this pressure is zero.
	 * @return True if and only if this pressure is zero.
	 */
	public boolean isZero() {
		return pressure == 0.0;
	}
	
	/**
	 * Compare this Pressure to the specified Object, which must be
	 * a Pressure, returning -1, 0, or +1 if this pressure is less 
	 * than, equal to, or greater than the specified Pressure.
	 * @param o The Object to which this Pressure is being compared.
	 * @return -1, 0, or +1 if this pressure is less than, equal to, or 
	 * greater than the specified object, which must be a Pressure.
	 * @throws IllegalArgumentException If the object being compared is not a Pressure.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Pressure))
			throw new IllegalArgumentException("Attempt to compare a Pressure to a non-Pressure.");
		if (pressure < ((Pressure)o).pressure)
			return -1;
		if (pressure > ((Pressure)o).pressure)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Pressure is equal to the specified 
	 * Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is equal to the specified Pressure.
	 */
	public boolean eq(Pressure t) {
		return pressure == t.pressure; 
	}
	
	/**
	 * Return true if and only if this Pressure is not equal to the specified 
	 * Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is not equal to the specified Pressure.
	 */
	public boolean ne(Pressure t) {
		return pressure != t.pressure; 
	}
	
	/**
	 * Return true if and only if this Pressure is less than the specified 
	 * Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is less than the specified Pressure.
	 */
	public boolean lt(Pressure t) {
		return pressure < t.pressure; 
	}
	
	/**
	 * Return true if and only if this Pressure is less than or equal to
	 * the specified Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is less than or equal to the 
	 * specified Pressure.
	 */
	public boolean le(Pressure t) {
		return pressure <= t.pressure; 
	}
	
	/**
	 * Return true if and only if this Pressure is greater than the specified 
	 * Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is greater than the specified Pressure.
	 */
	public boolean gt(Pressure t) {
		return pressure > t.pressure; 
	}
	
	/**
	 * Return true if and only if this Pressure is greater than or equal to
	 * the specified Pressure.
	 * @param t The Pressure to which this Pressure is being compared.
	 * @return True, if and only if this Pressure is greater than or equal to the 
	 * specified Pressure.
	 */
	public boolean ge(Pressure t) {
		return pressure >= t.pressure; 
	}
	
	/**
	 * Add a specified Pressure to this pressure, (this + x).
	 * @param x The Pressure to be added to this pressure.
	 * @return This pressure after the addition.
	 */
	public Pressure add (Pressure x) {
		pressure += x.pressure;
		return this;
	}
	
	/**
	 * Subtract a specified Pressure from this pressure, (this - x).
	 * @param x The Pressure to be subtracted to this pressure.
	 * @return This pressure after the subtraction.
	 */
	public Pressure sub (Pressure x) {
		pressure -= x.pressure;
		return this;
	}
	
	/**
	 * Multiply this pressure by some factor, (this * factor).
	 * @param factor The factor by which this pressure is to be multiplied.
	 * @return This pressure after the multiplication.
	 */
	public Pressure mult (double factor) {
		pressure *= factor;
		return this;
	}
	
	/**
	 * Divide this pressure by some factor, (this / factor).
	 * @param factor The factor by which this pressure is to be divided.
	 * @return This pressure after the division.
	 */
	public Pressure div (double factor) {
		pressure /= factor;
		return this;
	}
	
}
