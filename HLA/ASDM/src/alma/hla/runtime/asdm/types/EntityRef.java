/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File EntityRef.java
 */
package alma.hla.runtime.asdm.types;

import java.io.DataOutputStream;
import alma.hla.runtime.util.BODataInputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;



import alma.asdmIDLTypes.IDLEntityRef;

/**
 * The EntityRef class is an identification of a persistant
 * entity in the ALMA archive.  It easily maps onto an EntityRefT
 * object in ACS system entities.
 * 
 * @version 1.00 Nov 22, 2004
 * @author Allen Farris
 */
public class EntityRef implements java.io.Serializable {

	static public EntityRef getEntityRef(StringTokenizer t) {
		try {
			String s = t.nextToken("<>");
			if (s.equals(" "))
				s = t.nextToken();
			EntityRef e = new EntityRef ();
			e.setFromXML(s);
			return e;
		} catch (NoSuchElementException e) {
		}
		return null;
	}

	// The internal representation of an EntityRef.
	private EntityId entityId;
	private PartId partId;
	private String entityTypeName;
	private String instanceVersion;
	
	/**
	 * Create a default EntityRef whose internal parameters are null.
	 */
	public EntityRef() {
		entityId = null;
		partId = null;
		entityTypeName = null;
		instanceVersion = null;
	}

    /**
     * A copy constructor.
     *  @param e the EntityRef to be copied.
     *  
     */
    public EntityRef(EntityRef e) {
        this.entityId = new EntityId(e.entityId);
        if (e.partId != null)
        	this.partId = new PartId(e.partId);
        else
        	this.partId = null;
        this.entityTypeName = new String(e.entityTypeName);
        this.instanceVersion = new String(e.instanceVersion);
    }
    
	/**
	 * Create an EntityRef from the specified string (which is assumed
	 * to be produced by the toString method).
	 */
	public EntityRef(String s) {
		setFromXML(s);	
	}

	/**
	 * Create a EntityRef from an IDLEntityRef object.
	 * @param e The IDLEntityRef object.
	 */
	public EntityRef (IDLEntityRef e) {
		this.entityId = new EntityId(e.entityId);
		this.partId = new PartId(e.partId);
		this.entityTypeName = e.entityTypeName;
		this.instanceVersion = e.instanceVersion;
	}
	
	/**
	 * Create an EntityRef whose initial value is the specified parameters.
	 */
	public EntityRef (String entityId, String partId, String entityTypeName,
			String instanceVersion) {
		this.entityId = new EntityId(entityId);
		if (partId == null)
			this.partId = null;
		else
			this.partId = new PartId(partId);
		this.entityTypeName = entityTypeName;
		this.instanceVersion = instanceVersion;
	}
	
	/**
	 * Return the entityId as a String.
	 * @return The entityId as a String.
	 */
	public String toString() {
		return toXML();
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		this.entityId.toBin(dos);
		this.partId.toBin(dos);
		dos.writeInt(this.entityTypeName.length());
		dos.writeBytes(this.entityTypeName);
		dos.writeInt(this.instanceVersion.length());
		dos.writeBytes(this.instanceVersion);
		
	}
	
	/**
	 * Write the binary representation of a 1D array of EntityRef into a DataOutputStream.
	 * @param entityRefs
	 * @param dos
	 * @throws IOException 
	 */	
	public static void toBin(EntityRef[] entityRefs, DataOutputStream dos) throws IOException {
		dos.writeInt(entityRefs.length);
		for (EntityRef entityRef : entityRefs)
			entityRef.toBin(dos);
	}
	
	static private String readString(BODataInputStream dis) throws IOException {
		int numChar = dis.readInt();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < numChar; i++)
			sb.append((char) dis.readByte());
		return sb.toString();
	}
		
	/**
	 * Returns an EntityRef built from its bin-serialized representation
	 * read on a BODataInputStream
	 * @throws IOException 
	 */
	static public EntityRef fromBin(BODataInputStream dis) throws IOException {
		return new EntityRef(readString(dis), readString(dis), readString(dis), readString(dis));
	}
	
	/**
	 * Read the binary representation of a 1D array of EntityRef from a BODataInputStream
	 * and use the read value to set a 1D array of EntityRef.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of EntityRef
	 * @throws IOException
	 */
	public static EntityRef[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		EntityRef entityRef[] = new EntityRef[dim1];
		for (int i = 0; i < dim1; i++) 
			entityRef[i] = EntityRef.fromBin(dis);
		return entityRef;
	}
	
	/**
	 * Return the values of this EntityRef as an XML-formated string.
	 * As an example, for the Main table in the ASDM, the toXML 
	 * method would give:
	 * <ul>
	 * <li>	&lt;EntityRef 
	 * <li>		entityId="uid://X79/X1/X2" 
	 * <li>		partId="X00000002" 
	 * <li>		entityTypeName="Main" 
	 * <li>		documentVersion="1"/&gt;
	 * </ul>
	 * 
	 * @return The values of this EntityRef as an XML-formated string.
	 * @throws IllegalStateException if the values of this EntityRef do not conform to the proper XML format.
	 */
	public String toXML() {
		String msg = validXML();
		if (msg != null) 
			throw new java.lang.IllegalStateException(msg);
		String s = "<EntityRef entityId=\"" + entityId;
		if (partId != null)
			s += "\" partId=\"" + partId;
		s += "\" entityTypeName=\"" + entityTypeName + 
			 "\" documentVersion=\"" + instanceVersion + "\"/>";
		return s;
	}
	
	/**
	 * Set the values of this EntityRef from an XML-formatted as described
	 * in the "toXML" method.
	 * Example:
	 * <ul>
	 * <li>	&lt;EntityRef 
	 * <li>		entityId="uid://X79/X0/X0" 
	 * <li>		partId="X00000002" 
	 * <li>		entityTypeName="Main" 
	 * <li>		documentVersion="1"/&gt;
	 * </ul>
	 * 
	 * @param xml An XML-formatted string as described in the "toXML" method.
	 * @throws IllegalArgumentException If the XML string is not a valid format.
	 */
	public void setFromXML(String xml) {
		entityId = new EntityId(getXMLValue(xml,"entityId"));
		String s = getXMLValue(xml,"partId");
		if (s == null)
			partId = null;
		else
			partId = new PartId(s);
		entityTypeName = getXMLValue(xml,"entityTypeName");
		instanceVersion = getXMLValue(xml,"documentVersion");
		String msg = validXML();
		if (msg != null) 
			throw new java.lang.IllegalArgumentException(msg);
	}
	
	private String getXMLValue(String xml, String parm) {
		int n = xml.indexOf(parm);
		if (n == -1)
			return null;
		int beg = xml.indexOf('\"',n + parm.length()) + 1;
		if (n == 0)
			return null;
		int end = xml.indexOf('\"',beg);
		if (n == -1)
			return null;
		return xml.substring(beg,end);
	}

	/**
	 * Check for a valid XML values.
	 * @return A non-null string if there are errors in the values
	 * of this EntityRef; a null string is returned if the EntityRef is valid.
	 */
	private String validXML() {
		// Check for any null values. PartId may be null.
		if (entityId == null ||
			entityTypeName == null ||
			instanceVersion == null)
			return "Null values detected in EntityRef " + entityId;
		// Check the entityId for the correct format.
		String msg = EntityId.validate(entityId.toString());
		if (msg != null)
			return msg;
		// Check partId to the correct format.
		if (partId != null)
			return PartId.validate(partId.toString());
		return null;
	}
	
	/**
	 * Return an IDL EntityRef object.
	 * @return An IDL EntityRef object.
	 */
	public IDLEntityRef toIDLEntityRef() {
		IDLEntityRef e = new IDLEntityRef ();
		e.entityId = this.entityId.toString();
		e.partId = this.partId.toString();
		e.entityTypeName = this.entityTypeName;
		e.instanceVersion = this.instanceVersion;
		return e;
	}
	
	/**
	 * Return true if and only if the specified object o is an
	 * EntityRef and its value is equal to this EntityRef.
	 * @param o The object to which this EntityRef is being compared.
	 * @return true if and only if the specified object o is an
	 * EntityRef and its value is equal to this EntityRef.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof EntityRef))
			return false;
		EntityRef e = (EntityRef)o;
		return 	this.entityId == e.entityId &&
				this.partId == e.partId &&
				this.entityTypeName == e.entityTypeName &&
				this.instanceVersion == e.instanceVersion;
	}
	
	/**
	 * Return true if and only if the entityId is null.
	 * @return True if and only if the entityId is null.
	 */
	public boolean isNull() {
		return entityId == null;
	}
	
	// Getters and setters.
	
	/**
	 * @return Returns the entityId.
	 */
	public String getEntityId() {
		return entityId.toString();
	}

	/**
	 * @param entityId The entityId to set.
	 */
	public void setEntityId(String entityId) {
		this.entityId = new EntityId(entityId);
	}

	/**
	 * @return Returns the partId.
	 */
	public String getPartId() {
		if (partId == null)
			return null;
		return partId.toString();
	}

	/**
	 * @param partId The partId to set.
	 */
	public void setPartId(String partId) {
		this.partId = new PartId(partId);
	}

	/**
	 * @return Returns the entityTypeName.
	 */
	public String getEntityTypeName() {
		return entityTypeName;
	}

	/**
	 * @param entityTypeName The entityTypeName to set.
	 */
	public void setEntityTypeName(String entityTypeName) {
		this.entityTypeName = entityTypeName;
	}

	/**
	 * @return Returns the instanceVersion.
	 */
	public String getInstanceVersion() {
		return instanceVersion;
	}

	/**
	 * @param instanceVersion The instanceVersion to set.
	 */
	public void setInstanceVersion(String instanceVersion) {
		this.instanceVersion = instanceVersion;
	}

}
