/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PartId.java
 */
package alma.hla.runtime.asdm.types;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * description
 * 
 * @version 1.00 Nov 24, 2004
 * @author Allen Farris
 */
public class PartId {

	static public String validate(String x) {
		String msg = "Invalid format for partId: " + x;
		if (x == null || x.length() == 0)
			return null;
		if (x.length() != 9 || x.charAt(0) != 'X')
			return msg;
		for (int i = 1; i < 9; ++i) {
			if (!((x.charAt(i) >= '0' && x.charAt(i) <= '9') ||
					(x.charAt(i) >= 'a' && x.charAt(i) <= 'f')))
				return msg;
		}
		return null;
	}
	
	private String id;
	
	/**
	 * 
	 */
	public PartId(String id) {
		String msg = validate(id);
		if (msg != null)
			throw new java.lang.IllegalArgumentException(msg);
		this.id = id;
	}
    
    /**
     * A copy constructor for PartId
     *  @param p the PartId to be copied.
     */
    public PartId(PartId p) {
        this.id = new String(p.id);
    }

	public String toString() {
		return id;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeInt(this.id.length());
		dos.writeBytes(this.id);
	}
    
    /**
     * Return true if and only if the specified object o is an
     * PartId and its value is equal to this PartId.
     * @param o The object to which this interval is being compared.
     * @return true if and only if the specified object o is a
     * PartId and its value is equal to the one of this PartId.
     */
    public boolean equals(Object o) {
        if (!(o instanceof PartId))
            return false;
        return this.id.equals(((PartId)o).id);
    }
	
}
