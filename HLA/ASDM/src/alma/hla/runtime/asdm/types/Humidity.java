/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Humidity.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLHumidity;

/**
 * The Humidity class implements a concept of a relative humidity in percent.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class Humidity implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new Humidity that is the sum of the 
	 * specified percents, a +  b.
	 * @param a The first Humidity of the pair.
	 * @param b The second Humidity of the pair.
	 * @return A new Humidity that is the sum of the 
	 * specified percents, a +  b.
	 */
	static public Humidity add (Humidity a, Humidity b) {
		return new Humidity (a.percent + b.percent);
	}
	
	/**
	 * Return a new Humidity that is the difference of the 
	 * specified percents, a - b.
	 * @param a The first Humidity of the pair.
	 * @param b The second Humidity of the pair.
	 * @return A new Humidity that is the difference of the 
	 * specified percents, a - b.
	 */
	static public Humidity sub (Humidity a, Humidity b) {
		return new Humidity (a.percent - b.percent);
	}
	
	/**
	 * Return a new Humidity that is the product of a specified 
	 * percent and some specified factor, a * factor.
	 * @param a The base percent
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Humidity that is the product of a specified 
	 * percent and some specified factor, a * factor.
	 */
	static public Humidity mult (Humidity a, double factor) {
		return new Humidity (a.percent * factor);
	}
	
	/**
	 * Return a new Humidity that is a specified percent divided by a 
	 * specified factor, a / factor.
	 * @param a The base percent
	 * @param factor The factor that is used to divide the value.
	 * @return A new Humidity that is the product of a specified 
	 * percent and some specified factor, a / factor.
	 */
	static public Humidity div (Humidity a, double factor) {
		return new Humidity (a.percent / factor);
	}
	
	static public Humidity getHumidity(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Humidity (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The percent in percents.
	 */
	private double percent;
	
	/**
	 * Create a Humidity of zero percents.
	 */
	public Humidity() {
	}

	/**
	 * Create a Humidity whose initial value is a string designating the percent in percents
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the percent in percents
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Humidity (String s) {
		percent = Double.parseDouble(s);
	}
	
	/**
	 * Create a Humidity whose initial value is the same as the specified percent.
	 * @param l The specified percent that is used as the initial value of this percent.
	 */
	public Humidity (Humidity l) {
		this.percent = l.percent;
	}
	
	/**
	 * Create a Humidity from an IDL percent object.
	 * @param l The IDL percent object.
	 */
	public Humidity (IDLHumidity l) {
		this.percent = l.value;
	}
	
	/**
	 * Create a Humidity whose initial value is the specified percent in percents.
	 * @param percent The percent in percents.
	 */
	public Humidity (double percent) {
		this.percent = percent;
	}
	
	/**
	 * Return the value of this percent in percents.
	 * @return The value of this percent in percents.
	 */
	public double get() {
		return percent;
	}
	
	/**
	 * Set the value of this percent to the specified percent in percents.
	 * @param percent The specified percent in percents.
	 */
	public void set(double percent) {
		this.percent = percent;
	}
	
	/**
	 * Return the value of this as a String in units of percents.
	 * @return The value of this as a String in units of percents.
	 * 
	 */
	public String toString() {
		return Double.toString(percent);
	}
	/**
	 * Return an array of double containing the values of the array of Humidity objects passed in argument.
	 */
	static public double[] values(Humidity[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(percent);
	}

	/**
	 * Write the binary representation of a 1D array of humidity into a DataOutputStream.
	 * @param humidity the array of Humidity to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Humidity[] humidity, DataOutputStream dos) throws IOException {
		dos.writeInt(humidity.length);
		for (int i = 0; i < humidity.length; i++)
			humidity[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Humidity into a DataOutputStream.
	 * @param humidity the 2D array of Humidity to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Humidity[][] humidity, DataOutputStream dos) throws IOException {
		dos.writeInt(humidity.length);
		dos.writeInt(humidity[0].length);
		for (int i = 0; i < humidity.length; i++)
			for (int j=0; j < humidity[0].length; j++)
				humidity[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Humidity into a DataOutputStream.
	 * @param humidity the 3D array of Humidity to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Humidity[][][] humidity, DataOutputStream dos) throws IOException {
		dos.writeInt(humidity.length);
		dos.writeInt(humidity[0].length);
		dos.writeInt(humidity[0][0].length);
		for (int i = 0; i < humidity.length; i++)
			for (int j=0; j < humidity[0].length; j++)
				for (int k = 0; k < humidity[0][0].length; k++)
					humidity[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Humidity from a BODataInputStream
	 * and use the read value to set an  Humidity.
	 * @param dis the BODataInputStream to be read
	 * @return an Humidity
	 * @throws IOException
	 */
	public static Humidity fromBin(BODataInputStream dis) throws IOException {
		Humidity humidity = new Humidity();
		humidity.set(dis.readDouble());
		return humidity;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Humidity from a BODataInputStream
	 * and use the read value to set a 1D array of  Humidity.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Humidity
	 * @throws IOException
	 */
	public static Humidity[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Humidity humidity[] = new Humidity[dim1];
		for (int i = 0; i < dim1; i++) 
			humidity[i] = Humidity.fromBin(dis);
		return humidity;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Humidity from a BODataInputStream
	 * and use the read value to set a 2D array of  Humidity.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Humidity
	 * @throws IOException
	 */
	public static Humidity[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Humidity[][] humidity = new Humidity[dim1][dim2];
		humidity = new Humidity[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				humidity[i][j] = Humidity.fromBin(dis);
		return humidity;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Humidity from a BODataInputStream
	 * and use the read value to set a 3D array of  Humidity.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Humidity
	 * @throws IOException
	 */
	public static Humidity[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Humidity[][][] humidity = new Humidity[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					humidity[i][j][k] = Humidity.fromBin(dis);
		return humidity;
	}
	/**
	 * Return an IDL Humidity object.
	 * @return An IDL Humidity object.
	 */
	public IDLHumidity toIDLHumidity() {
		return new IDLHumidity(percent);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Humidity and its value is equal to this percent.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Humidity and its value is equal to this percent.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Humidity))
			return false;
		return this.percent == ((Humidity)o).percent;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this percent is zero.
	 * @return True if and only if this percent is zero.
	 */
	public boolean isZero() {
		return percent == 0.0;
	}
	
	/**
	 * Compare this Humidity to the specified Object, which must be
	 * a Humidity, returning -1, 0, or +1 if this percent is less 
	 * than, equal to, or greater than the specified Humidity.
	 * @param o The Object to which this Humidity is being compared.
	 * @return -1, 0, or +1 if this percent is less than, equal to, or 
	 * greater than the specified object, which must be a Humidity.
	 * @throws IllegalArgumentException If the object being compared is not a Humidity.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Humidity))
			throw new IllegalArgumentException("Attempt to compare a Humidity to a non-Humidity.");
		if (percent < ((Humidity)o).percent)
			return -1;
		if (percent > ((Humidity)o).percent)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Humidity is equal to the specified 
	 * Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is equal to the specified Humidity.
	 */
	public boolean eq(Humidity t) {
		return percent == t.percent; 
	}
	
	/**
	 * Return true if and only if this Humidity is not equal to the specified 
	 * Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is not equal to the specified Humidity.
	 */
	public boolean ne(Humidity t) {
		return percent != t.percent; 
	}
	
	/**
	 * Return true if and only if this Humidity is less than the specified 
	 * Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is less than the specified Humidity.
	 */
	public boolean lt(Humidity t) {
		return percent < t.percent; 
	}
	
	/**
	 * Return true if and only if this Humidity is less than or equal to
	 * the specified Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is less than or equal to the 
	 * specified Humidity.
	 */
	public boolean le(Humidity t) {
		return percent <= t.percent; 
	}
	
	/**
	 * Return true if and only if this Humidity is greater than the specified 
	 * Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is greater than the specified Humidity.
	 */
	public boolean gt(Humidity t) {
		return percent > t.percent; 
	}
	
	/**
	 * Return true if and only if this Humidity is greater than or equal to
	 * the specified Humidity.
	 * @param t The Humidity to which this Humidity is being compared.
	 * @return True, if and only if this Humidity is greater than or equal to the 
	 * specified Humidity.
	 */
	public boolean ge(Humidity t) {
		return percent >= t.percent; 
	}
	
	/**
	 * Add a specified Humidity to this percent, (this + x).
	 * @param x The Humidity to be added to this percent.
	 * @return This percent after the addition.
	 */
	public Humidity add (Humidity x) {
		percent += x.percent;
		return this;
	}
	
	/**
	 * Subtract a specified Humidity from this percent, (this - x).
	 * @param x The Humidity to be subtracted to this percent.
	 * @return This percent after the subtraction.
	 */
	public Humidity sub (Humidity x) {
		percent -= x.percent;
		return this;
	}
	
	/**
	 * Multiply this percent by some factor, (this * factor).
	 * @param factor The factor by which this percent is to be multiplied.
	 * @return This percent after the multiplication.
	 */
	public Humidity mult (double factor) {
		percent *= factor;
		return this;
	}
	
	/**
	 * Divide this percent by some factor, (this / factor).
	 * @param factor The factor by which this percent is to be divided.
	 * @return This percent after the division.
	 */
	public Humidity div (double factor) {
		percent /= factor;
		return this;
	}
	
}
