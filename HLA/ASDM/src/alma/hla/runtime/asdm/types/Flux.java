/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Flux.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLFlux;

/**
 * The Flux class implements a concept of a flux in Janskys.
 * 
 * @version 1.00 Nov 21, 2004
 * @author Allen Farris
 */
public class Flux implements Cloneable, Comparable, java.io.Serializable {

	/**
	 * Return a new Flux that is the sum of the 
	 * specified janskys, a +  b.
	 * @param a The first Flux of the pair.
	 * @param b The second Flux of the pair.
	 * @return A new Flux that is the sum of the 
	 * specified janskys, a +  b.
	 */
	static public Flux add (Flux a, Flux b) {
		return new Flux (a.flux + b.flux);
	}
	
	/**
	 * Return a new Flux that is the difference of the 
	 * specified janskys, a - b.
	 * @param a The first Flux of the pair.
	 * @param b The second Flux of the pair.
	 * @return A new Flux that is the difference of the 
	 * specified janskys, a - b.
	 */
	static public Flux sub (Flux a, Flux b) {
		return new Flux (a.flux - b.flux);
	}
	
	/**
	 * Return a new Flux that is the product of a specified 
	 * flux and some specified factor, a * factor.
	 * @param a The base flux
	 * @param factor The factor that is used to multiply the value.
	 * @return A new Flux that is the product of a specified 
	 * flux and some specified factor, a * factor.
	 */
	static public Flux mult (Flux a, double factor) {
		return new Flux (a.flux * factor);
	}
	
	/**
	 * Return a new Flux that is a specified flux divided by a 
	 * specified factor, a / factor.
	 * @param a The base flux
	 * @param factor The factor that is used to divide the value.
	 * @return A new Flux that is the product of a specified 
	 * flux and some specified factor, a / factor.
	 */
	static public Flux div (Flux a, double factor) {
		return new Flux (a.flux / factor);
	}
	
	static public Flux getFlux(StringTokenizer t) {
		try {
			double value = Double.parseDouble(t.nextToken());
			return new Flux (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	/**
	 * The flux in janskys.
	 */
	private double flux;
	
	/**
	 * Create a Flux of zero janskys.
	 */
	public Flux() {
	}

	/**
	 * Create a Flux whose initial value is a string designating the flux in janskys
	 * as a double precision number in the standard Java notation.
	 * @param s A string designating the flux in janskys
	 * @throws NumberFormatException If the specified initial value does not contain
	 * a parseable double.
	 */
	public Flux (String s) {
		flux = Double.parseDouble(s);
	}
	
	/**
	 * Create a Flux whose initial value is the same as the specified flux.
	 * @param l The specified flux that is used as the initial value of this flux.
	 */
	public Flux (Flux l) {
		this.flux = l.flux;
	}
	
	/**
	 * Create a Flux from an IDL flux object.
	 * @param l The IDL flux object.
	 */
	public Flux (IDLFlux l) {
		this.flux = l.value;
	}
	
	/**
	 * Create a Flux whose initial value is the specified flux in janskys.
	 * @param flux The flux in janskys.
	 */
	public Flux (double flux) {
		this.flux = flux;
	}
	
	/**
	 * Return the value of this flux in janskys.
	 * @return The value of this flux in janskys.
	 */
	public double get() {
		return flux;
	}
	
	/**
	 * Set the value of this flux to the specified flux in janskys.
	 * @param flux The specified flux in janskys.
	 */
	public void set(double flux) {
		this.flux = flux;
	}
	
	/**
	 * Return the value of this as a String in units of janskys.
	 * @return The value of this as a String in units of janskys.
	 * 
	 */
	public String toString() {
		return Double.toString(flux);
	}

	/**
	 * Return an array of double containing the values of the array of Flux objects passed in argument.
	 */
	static public double[] values(Flux[] items) {
		double array[] = new double[items.length];
		for (int i = 0; i < items.length; i++ )
			array[i] = items[i].get();
		return array;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void  toBin(DataOutputStream dos) throws IOException {
		dos.writeDouble(flux);
	}

	/**
	 * Write the binary representation of a 1D array of flux into a DataOutputStream.
	 * @param flux the array of Flux to be written
	 * @param dos the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Flux[] flux, DataOutputStream dos) throws IOException {
		dos.writeInt(flux.length);
		for (int i = 0; i < flux.length; i++)
			flux[i].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 2D array of Flux into a DataOutputStream.
	 * @param flux the 2D array of Flux to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Flux[][] flux, DataOutputStream dos) throws IOException {
		dos.writeInt(flux.length);
		dos.writeInt(flux[0].length);
		for (int i = 0; i < flux.length; i++)
			for (int j=0; j < flux[0].length; j++)
				flux[i][j].toBin(dos);
	}
	
	/**
	 * Write the binary representation of a 3D array of Flux into a DataOutputStream.
	 * @param flux the 3D array of Flux to be written
	 * @param dos  the DataOutputStream to be written to
	 * @throws IOException 
	 */
	public static void toBin(Flux[][][] flux, DataOutputStream dos) throws IOException {
		dos.writeInt(flux.length);
		dos.writeInt(flux[0].length);
		dos.writeInt(flux[0][0].length);
		for (int i = 0; i < flux.length; i++)
			for (int j=0; j < flux[0].length; j++)
				for (int k = 0; k < flux[0][0].length; k++)
					flux[i][j][k].toBin(dos);
	}	
	
	/**
	 * Read the binary representation of an Flux from a BODataInputStream
	 * and use the read value to set an  Flux.
	 * @param dis the BODataInputStream to be read
	 * @return an Flux
	 * @throws IOException
	 */
	public static Flux fromBin(BODataInputStream dis) throws IOException {
		Flux flux = new Flux();
		flux.set(dis.readDouble());
		return flux;
	}
	
	/**
	 * Read the binary representation of a 1D array of  Flux from a BODataInputStream
	 * and use the read value to set a 1D array of  Flux.
	 * @param dis the BODataInputStream to be read
	 * @return a 1D array of Flux
	 * @throws IOException
	 */
	public static Flux[] from1DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		Flux flux[] = new Flux[dim1];
		for (int i = 0; i < dim1; i++) 
			flux[i] = Flux.fromBin(dis);
		return flux;
	}
	
	/**
	 * Read the binary representation of a 2D array of  Flux from a BODataInputStream
	 * and use the read value to set a 2D array of  Flux.
	 * @param dis the BODataInputStream to be read
	 * @return a 2D array of Flux
	 * @throws IOException
	 */
	public static Flux[][]  from2DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		Flux[][] flux = new Flux[dim1][dim2];
		flux = new Flux[dim1][dim2];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				flux[i][j] = Flux.fromBin(dis);
		return flux;
	}
	
	/**
	 * Read the binary representation of a 3D array of  Flux from a BODataInputStream
	 * and use the read value to set a 3D array of  Flux.
	 * @param dos the BODataInputStream to be read
	 * @return  a 3D array of  Flux
	 * @throws IOException
	 */
	public static Flux[][][]  from3DBin(BODataInputStream dis) throws IOException {
		int dim1 = dis.readInt();
		int dim2 = dis.readInt();
		int dim3 = dis.readInt();
		Flux[][][] flux = new Flux[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					flux[i][j][k] = Flux.fromBin(dis);
		return flux;
	}	
	/**
	 * Return an IDL Flux object.
	 * @return An IDL Flux object.
	 */
	public IDLFlux toIDLFlux() {
		return new IDLFlux(flux);
	}

	/**
	 * Return true if and only if the specified object o is a
	 * Flux and its value is equal to this flux.
	 * @param o The object to which this interval is being compared.
	 * @return true if and only if the specified object o is an
	 * Flux and its value is equal to this flux.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Flux))
			return false;
		return this.flux == ((Flux)o).flux;
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this flux is zero.
	 * @return True if and only if this flux is zero.
	 */
	public boolean isZero() {
		return flux == 0.0;
	}
	
	/**
	 * Compare this Flux to the specified Object, which must be
	 * a Flux, returning -1, 0, or +1 if this flux is less 
	 * than, equal to, or greater than the specified Flux.
	 * @param o The Object to which this Flux is being compared.
	 * @return -1, 0, or +1 if this flux is less than, equal to, or 
	 * greater than the specified object, which must be a Flux.
	 * @throws IllegalArgumentException If the object being compared is not a Flux.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Flux))
			throw new IllegalArgumentException("Attempt to compare a Flux to a non-Flux.");
		if (flux < ((Flux)o).flux)
			return -1;
		if (flux > ((Flux)o).flux)
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this Flux is equal to the specified 
	 * Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is equal to the specified Flux.
	 */
	public boolean eq(Flux t) {
		return flux == t.flux; 
	}
	
	/**
	 * Return true if and only if this Flux is not equal to the specified 
	 * Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is not equal to the specified Flux.
	 */
	public boolean ne(Flux t) {
		return flux != t.flux; 
	}
	
	/**
	 * Return true if and only if this Flux is less than the specified 
	 * Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is less than the specified Flux.
	 */
	public boolean lt(Flux t) {
		return flux < t.flux; 
	}
	
	/**
	 * Return true if and only if this Flux is less than or equal to
	 * the specified Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is less than or equal to the 
	 * specified Flux.
	 */
	public boolean le(Flux t) {
		return flux <= t.flux; 
	}
	
	/**
	 * Return true if and only if this Flux is greater than the specified 
	 * Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is greater than the specified Flux.
	 */
	public boolean gt(Flux t) {
		return flux > t.flux; 
	}
	
	/**
	 * Return true if and only if this Flux is greater than or equal to
	 * the specified Flux.
	 * @param t The Flux to which this Flux is being compared.
	 * @return True, if and only if this Flux is greater than or equal to the 
	 * specified Flux.
	 */
	public boolean ge(Flux t) {
		return flux >= t.flux; 
	}
	
	/**
	 * Add a specified Flux to this flux, (this + x).
	 * @param x The Flux to be added to this flux.
	 * @return This flux after the addition.
	 */
	public Flux add (Flux x) {
		flux += x.flux;
		return this;
	}
	
	/**
	 * Subtract a specified Flux from this flux, (this - x).
	 * @param x The Flux to be subtracted to this flux.
	 * @return This flux after the subtraction.
	 */
	public Flux sub (Flux x) {
		flux -= x.flux;
		return this;
	}
	
	/**
	 * Multiply this flux by some factor, (this * factor).
	 * @param factor The factor by which this flux is to be multiplied.
	 * @return This flux after the multiplication.
	 */
	public Flux mult (double factor) {
		flux *= factor;
		return this;
	}
	
	/**
	 * Divide this flux by some factor, (this / factor).
	 * @param factor The factor by which this flux is to be divided.
	 * @return This flux after the division.
	 */
	public Flux div (double factor) {
		flux /= factor;
		return this;
	}
	
}
