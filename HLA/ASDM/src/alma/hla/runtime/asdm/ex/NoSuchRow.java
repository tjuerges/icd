
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File NoSuchRow.java
 */
package alma.hla.runtime.asdm.ex;

/**
 * Generate an exception when an expected row cannot be found. 
 */
public class NoSuchRow extends Exception {
	
	/**
	 * Create an exception when an expected row cannot be found.
	 * @param rowNumber The row number that cannot be found.
	 * @param tableName The table being searched.
	 */
	public NoSuchRow(int rowNumber, String tableName) {
		super("No such row as number " + rowNumber + " in table " + tableName);
	}

	/**
	 * Create an exception when an expected row cannot be found.
	 * @param key The key of the row that cannot be found.
	 * @param tableName The table being searched.
	 */
	public NoSuchRow(String key, String tableName) {
		super("No such row with key " + key + " in table " + tableName);
	}
	
	/**
	 * Create an exception when an expected link cannot be found.
	 * @param N The link number that cannot be found.
	 * @param toTableName The table to which the link is directed.
	 * @param fromTableName The table from which the link is directed.
	 */
	public NoSuchRow(int N, String toTableName, String fromTableName) {
		super("No such link as number " + N + " to table " + 
				toTableName + " in this row of table " + fromTableName);
	}

	/**
	 * Create an exception when an expected link cannot be found.
	 * @param key The key of the link that cannot be found.
	 * @param toTableName The table to which the link is directed.
	 * @param fromTableName The table from which the link is directed.
	 */
	public NoSuchRow(String key, String toTableName, String fromTableName) {
		super("No such link with key " + key + " to table " + 
				toTableName + " in this row of table " + fromTableName);
	}
	
	/**
	 * Create an exception when an optional link does not exist.
	 * @param toTableName The table to which the link is directed.
	 * @param fromTableName The table from which the link is directed.
	 * @param option Is not really used.
	 */
	public NoSuchRow(String toTableName, String fromTableName, boolean option) {
		super("The optional link to table " + toTableName + " in this row of table " + 
		fromTableName + " does not exist! ");
	}


}
