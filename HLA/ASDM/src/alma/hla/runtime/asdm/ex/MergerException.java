package alma.hla.runtime.asdm.ex;
	
	/**
	 * The MergerException occurs under unexpected conditions during a merge of
	 * two ASDM datasets.
	 * 
	 * @author caillat
	 */
	public class MergerException extends Exception {
		/**
		 * Create a MergerException.
		 * 
		 * @param m
		 *            A message indicating the cause of the exception.
		 */
		public MergerException(String msg) {
			super(msg);
		}
		
	}

