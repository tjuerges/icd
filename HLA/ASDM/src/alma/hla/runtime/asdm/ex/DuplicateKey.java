
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File DuplicateKey.java
 */
package alma.hla.runtime.asdm.ex;

/**
 * Generate an exception when a new row cannot be inserted 
 * because it contains a duplicate key. 
 */
public class DuplicateKey extends Exception {
	
	/**
	 * Create an exception when a new row cannot be inserted 
	 * because it contains a duplicate key.
	 * @param key The duplicate key
	 * @param tableName The table to which the row is being added.
	 */
	public DuplicateKey(String key, String tableName) {
		super("Cannot insert row with key " + key + " into table " + 
			tableName + ".  Key already exists.");
	}

}
