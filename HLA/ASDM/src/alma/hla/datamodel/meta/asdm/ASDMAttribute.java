/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaAttribute.java
 */
package alma.hla.datamodel.meta.asdm;

// import de.bmiag.genfw.meta.classifier.Attribute;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.meta.uml.classifier.Attribute;


import alma.hla.datamodel.util.MMUtil;
import alma.hla.datamodel.enumerations.meta.AlmaEnumeration;

/**
 * description
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class ASDMAttribute extends Attribute {
	
	public String UpperCaseName() {
		return MMUtil.UpperCaseName(NameS());
	}
	
	public String JavaType() {
		//if (EEnumeration.names.contains(Type().getMetaClass())) return Type().NameS();
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS();
		}
		
		return MMUtil.JavaType(Type());
	}
    
    public String XMLJavaType() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS();
		}
		//if (EEnumeration.names.contains(Type().NameS())) return Type().NameS();
        return MMUtil.JavaType(Type());
    }
    
    public String CppClass() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS();
		}
		//if (EEnumeration.names.contains(Type().NameS())) return Type().NameS();	
		return MMUtil.CppType(Type());    	
    }
    
	public String CppType() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS()+"Mod::"+Type().NameS();
		}
		//if (EEnumeration.names.contains(Type().NameS())) return Type().NameS()+"*";		
		return MMUtil.CppType(Type());
	}	
	
	public String SwigType() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return "Enum<"+this.SimpleCppType()+"Mod::"+this.SimpleCppType()+">";
		}
		//if (EEnumeration.names.contains(Type().NameS())) return Type().NameS()+"*";		
		return MMUtil.CppType(Type());
	}
	
    public String XMLCppType() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS();
		}
        return MMUtil.CppType(Type()).replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
/*    
    public String TypeSet() {
            return MMUtil.UpperCaseName(JavaType())+"Set";        
    }
*/    
    public String TypeSet() {
        return MMUtil.UpperCaseName(JavaType())+"Set";        
    }
        
	public boolean isStringType() {
		return MMUtil.JavaType(Type()).equals("String");
	}
    
	public boolean isCharType() {
		return MMUtil.JavaType(Type()).equals("char");
	}
    
    public boolean isArrayTimeIntervalType() {
        return MMUtil.JavaType(Type()).equals("ArrayTimeInterval");
    }
    
    public boolean isArrayTimeType() {
        return MMUtil.JavaType(Type()).equals("ArrayTime");
    }
    
    public boolean isTemporal() {
    	return (MMUtil.JavaType(Type()).equals("ArrayTime") || MMUtil.JavaType(Type()).equals("ArrayTimeInterval"));
    }
	
	public String IDLType() {
		if (Type() instanceof AlmaEnumeration) {
			return Type().NameS()+"Mod::"+Type().NameS();
		}
		//if (EEnumeration.names.contains(Type().NameS())) return Type().NameS()+"IDL";
		
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		BasicType x = basicType[i];
		if (x.isExtended())
			return "asdmIDLTypes::" + x.getIDLName();
		return basicType[i].getIDLName();
	}
	
	public boolean isEnumeration() {
		//return EEnumeration.names.contains(Type().NameS());
		return Type() instanceof AlmaEnumeration;
	}
	
	public boolean isExtendedType() {
		if (isEnumeration()) return false;

		
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		return basicType[i].isExtended();
	}
	
	public String WrapperType() {
		if (Type() instanceof AlmaEnumeration) {
			//System.out.println(Type().NameS());
			return Type().NameS();
		}
		
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		return basicType[i].getWrapperName();
	}
	
	public String DataOutputType() {
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		return basicType[i].getOutputType();
	}
	
	
	public String ParserType() {
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		if (basicType[i].getJavaName().equals("int"))
			return "parseInt";
		else if (basicType[i].getJavaName().equals("boolean"))
			return "parseBoolean";
		return "parse" + basicType[i].getWrapperName();
	}
		
	// isOptional -- Is this attribute the one that is
	// optional ? 
	public boolean isOptional() {
		if (MultiplicityMin() == null || MultiplicityMax() == null)
			return false;
		if (MultiplicityMin().equals("0") && MultiplicityMax().equals("1"))
			return true;
		if (MultiplicityMin().equals("0") && MultiplicityMax().equals("-1"))
			return true;
		return false;
	}
	
	//isArray
	protected boolean array = false;
	public boolean isArray() {
		return array;
	}
	public String ArrayDimensions() {
		return "0";
	}
	
	public String numberOfDimensions() {
		if (isArray()) 
			return Integer.toString(Size.split("x").length);
		else
			return "0";
	}
	
	public boolean isOneD() {
		if (isArray())
			return Size.split("x").length == 1;
		return false;
	}
	public boolean isTwoD() {
		if (isArray())
			return Size.split("x").length == 2;
		return false;
	}
	public boolean isThreeD() {
		if (isArray())
			return Size.split("x").length == 3;		
		return false;
	}
	public boolean isFourD() {
		if (isArray())
			return Size.split("x").length == 4;		
		return false;
	}
	
    //isASet
    protected boolean isaset = false;
    public boolean isASet() {
        return isaset;
    }
    
    //isEnumLiteral
    protected boolean isenumliteral = false;
    public boolean isEnumLiteral () {
 //   	return EEnumeration.names.contains(this.JavaType());
		return Type() instanceof AlmaEnumeration;
    }
    
	// Is this attribute part of a key?
	private boolean keyPart = false;
	public boolean isKeyPart() {
	    return keyPart;
	}
	public void setKeyPart() {
	    keyPart = true;
	}
	
	// Size
	public String Size = "";
	public void setSize(String Size) {
		this.Size = Size;
	}
	
    // Shape 
    protected String Shape = null;
    public void setShape(String Shape) {
        this.Shape = Shape;
    }
    
    // ExportEncoding
    protected ExportEncoding exportEncoding = ExportEncoding.ASCII;
    public void setExportEncoding(String exportEncoding) {
    	this.exportEncoding = ExportEncoding.get(exportEncoding);
    	if (this.exportEncoding == null)
    		Checks.error(this, "'" + exportEncoding +"' is not a valid export encoding name for" + this.NameS());
    }
    
    public ExportEncoding getExportEncoding() {
    	return exportEncoding;
    }
    
    protected String[] Shape_ = null;
    public void setShape_(String[] s) {
        Shape_ = new String[s.length];
        for (int i=0; i<Shape_.length;i++)
            Shape_[i] = s[i];
    }
    
	// isTableKey -- Does this attribute specify a key of a table? 
	// (i.e. this attribute has the "akey" stereotype).
	protected boolean tableKey = false;
	public boolean isTableKey() {
		return tableKey;
	}
	
	// isAutoIncrementable -- Is this attribute the one that is
	// autoincrementable ? 
	protected boolean autoIncrementable = false;
	public boolean isAutoIncrementable() {
		return autoIncrementable;
	}
		
	// Decide that this attribute is autoIncrementable.
	//
	public void setAutoIncrementable() {
	    this.autoIncrementable = true;
	}
	
	//isExtrinsic -- Does this attribute specify an extrinsic attribute? 
	protected boolean extrinsic = false;
	public boolean isExtrinsic() {
		return extrinsic;
	}

	public String SimpleUpperCaseName() {
		return MMUtil.UpperCaseName(NameS());
	}
	
	public String SimpleJavaType() {
		return MMUtil.JavaType(Type());
	}

	public String SimpleIDLType() {
		BasicType[] basicType = BasicType.getBasicType();
		String s = MMUtil.JavaType(Type());
		int i = 0;
		for (; i < basicType.length; ++i)
			if (basicType[i].getJavaName().equals(s))
				break;
		if (i == basicType.length)
			throw new IllegalArgumentException("Unrecognized Java type: " + s);
		return basicType[i].getIDLName();
	}

	public String SimpleCppType() {
		return MMUtil.CppType(Type());
	}

	public String SimpleName() {
		return NameS();
	}

	// Category
	public String Category = "";
	public void setCategory(String Category) {
		this.Category = Category;
	}
	
	public String JavaTypeDescription() {
        
        if (Shape_ == null) return new String("");
        
        
        StringBuffer result = new StringBuffer("");
        for (int i=0; i<Shape_.length; i++) {
             if (i > 0)
                result.append(", ");
             if (!Shape_[i].trim().equals("")){
                 result.append(Shape_[i]);
             }
        }
        return result.toString();
    }
    
     public String XMLJavaTypeDescription() {
            return this.JavaTypeDescription();
     }
    
    public String CppTypeDescription() {
        
        if (Shape_ == null) return new String("");
        StringBuffer result = new StringBuffer("");
        for (int i=0; i<Shape_.length; i++) {
             if (i > 0)
                result.append(", ");
             if (!Shape_[i].trim().equals("")){
                 result.append(Shape_[i]);
             }
        }
        return result.toString();
    }
    
    public String XMLCppTypeDescription() {
        return this.CppTypeDescription().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }

    //------------- Methods used for latex documentation  -------------------
    public String LatexLongDoc() {
	if (this.hasDocumentation()) {
	    String str = this.Documentation()+"| ";
	    int sep = str.indexOf("|");
	    str = str.substring(sep+1);
	    sep = str.indexOf("|");
	    // if (sep > 0) return str.substring(0,sep).replaceAll("_", "\\\\_");
	    if (sep > 0) return str.substring(0,sep);
	    return "{\\red long doc missing}";
	}
	return new String("{\\red missing}");
    }
    public String LatexShortDoc() {
	if (this.hasDocumentation()) {
	    String str = this.Documentation()+"| ";
	    int sep = str.indexOf("|");
	    // if (sep > 0) return str.substring(0, sep).replaceAll("_", "\\\\_");
	    if (sep > 0) return str.substring(0, sep);
	    return "{\\red short doc missing}";
	}
	return new String("{\\red doc missing}");
    }
    
    public String ShortDocumentation() {
    	if (!hasDocumentation()) return "";
    	String doc = Documentation();
    	int index = doc.indexOf("|");
    	if (index == -1) return doc;
    	return doc.substring(0, index);
    }
    
    public String LatexTypeDescription() {
	String s = this.SimpleCppType().replace("_", "\\_");
	if (Shape_ == null) return s;
        for (int i=0; i<Shape_.length; i++) {
             if (!Shape_[i].trim().equals("")){
		 if (Shape_[i].toString().indexOf("num")==0) {
		     s = s+ " [\\"+ Shape_[i].toString()+"] ";
		 } else {
		     s = s+ " ["+ Shape_[i].toString()+"] ";
		 }
             }
        }
        return s;
    }

    public String LatexSymbol() {
	String str = this.NameS();
	if (this.isDimensionPar()) {
	    if (str.length() > 7) { 
		str = "$N_{" + str.substring(3,7) + "}$";
	    }else{
		str = "$N_{" + str.substring(3) + "}$";
	    }		
	}
	return str;
    }
    //------------- Methods used for C interface of ASDM -------------------

    public String CType() {
	String s = this.SimpleCppType();
	if (s.equals("Angle"))
	    s = "double";
	else if (s.equals("AngularRate"))
	    s = "double";
	else if (s.equals("ArrayTime"))
	    s = "int64_t";
	else if (s.equals("ArrayTimeInterval"))
	    s = "int64_t";
	else if (s.equals("EntityRef"))
	    s = "char";
	else if (s.equals("Flux"))
	    s = "double";
	else if (s.equals("Frequency"))
	    s = "double";
	else if (s.equals("Humidity"))
	    s = "double";
	else if (s.equals("Interval"))
	    s = "int64_t";
	else if (s.equals("Length"))
	    s = "double";
	else if (s.equals("Pressure"))
	    s = "double";
	else if (s.equals("Speed"))
	    s = "double";
	else if (s.equals("string"))
	    s = "char";
	else if (s.equals("Tag"))
	    s = "int";
	else if (s.equals("Temperature"))
	    s = "double";
	return s;
    }

    public boolean  isCppStandard() {
	boolean b = true;
	String s = this.SimpleCppType();
	if (s.equals("Angle") ||
	    s.equals("AngularRate") ||
	    s.equals("ArrayTime") ||
	    s.equals("ArrayTimeInterval") ||
	    s.equals("EntityRef") ||
	    s.equals("Flux") ||
	    s.equals("Frequency") ||
	    s.equals("Humidity") ||
	    s.equals("Interval") ||
	    s.equals("Length") ||
	    s.equals("Pressure") ||
	    s.equals("Speed") ||
	    s.equals("string") ||
	    s.equals("Tag") ||
	    s.equals("Temperature")) {
	    b = false;
	}
	return b;
    }


    /**
     * Return the name of this attribute in  lower cases.
     */
    public String ToLowerCase() {
	String s =  this.NameS();
	return s.toLowerCase();
    }

    /**
     * Return a C type list for an attribute
     * Add dimensions after the attribute name
     * Example : int * assocFieldId, int * assocFieldIdDimOne
     */
    public String CTypeAttributeDimList() {

	String s="";
	//	String[]  dimOrder = {"DimOne","DimTwo","DimThird"};

	if (!this.isArray() && !this.isStringType()) {
	    s=s+this.CType()+" * "+this.NameS();
	} else {    // array
	    s=s+this.CType()+" * "+this.NameS()+", int * "+this.NameS()+"Dim";

	}
	return s;
    }
    public String FType() {
	String s = this.SimpleCppType();
	if (isEnumeration())
	    s = "integer";
	else if (s.equals("int"))
	    s = "integer";
	else if (s.equals("int32_t"))
	    s = "integer";
	else if (s.equals("int64_t"))
	    s = "integer*8";
	else if (s.equals("long"))
	    s = "integer";
	else if (s.equals("long long"))
	    s = "integer*8";
	else if (s.equals("bool"))
	    s = "logical*1";
	else if (s.equals("float"))
	    s = "real";
	else if (s.equals("double"))
	    s = "real*8";
	else if (s.equals("Angle"))
	    s = "real*8";
	else if (s.equals("AngularRate"))
	    s = "real*8";
	else if (s.equals("ArrayTime"))
	    s = "integer*8";
	else if (s.equals("ArrayTimeInterval"))
	    s = "type(ArrayTimeInterval)";
	else if (s.equals("EntityRef"))
	    s = "character*256";
	else if (s.equals("Flux"))
	    s = "real*8";
	else if (s.equals("Frequency"))
	    s = "real*8";
	else if (s.equals("Humidity"))
	    s = "real*8";
	else if (s.equals("Interval"))
	    s = "integer*8";
	else if (s.equals("Length"))
	    s = "real*8";
	else if (s.equals("Pressure"))
	    s = "real*8";
	else if (s.equals("Speed"))
	    s = "real*8";
	else if (s.equals("string"))
	    s = "character*256";
	else if (s.equals("Tag"))
	    s = "integer";
	else if (s.equals("Temperature"))
	    s = "real*8";
	else if (s.equals("Complex"))
	    s = "complex*16";
	return s;
    }
    /**
     * Full Fortran specification
     */
    public String  FFullType() {
	String s = this.FType();
	if (this.isArray())
	    s = s +", allocatable";
	s = s + " :: "+this.NameS().toString();
	if (this.isArray()) {
	    if (this.isOneD() || this.isExtrinsic())
		s = s +"(:)";
	    else if (this.isTwoD())
		s = s +"(:,:)";
	    else if (this.isThreeD())
		s = s +"(:,:,:)";
	    else if (this.isFourD())
		s = s +"(:,:,:,:)";
	}
	return s;
    }
    /**
     * Return name preceded by fortran structure name
     */
    public String FFullName() {
	String s=" " ;
	if (this.isKeyPart())
	    s = "key%";
	else
	    s = "row%";
	return s+this.NameS().toString();
    }
    /**
     * Return a Fortran  type list for an attribute
     * Add dimensions after the attribute name
     * Example : int * assocFieldId, int * assocFieldIdDimOne
     */
    public String FTypeAttributeDimList() {
	
	String s="";
	//      String[]  dimOrder = {"DimOne","DimTwo","DimThird"};
	
	if (!this.isArray() && !this.isStringType()) {
	    s=s+this.NameS();
	} else {    // array
	    s=s+this.NameS()+", "+this.NameS()+"Dim";	    
	}
	return s;
    }
    public boolean isDimensionPar() {
	boolean b = false;
	String str = this.NameS();
	if (str.indexOf("num") == 0) {
	    b=true;
	}
	return b;
    }
    public String RowFortranTypeDescription() {        
        if (Shape_ == null) return new String("");
        StringBuffer result = new StringBuffer("");
        for (int i=Shape_.length; i>0; i--) {
             if (i < Shape_.length)
                result.append(", ");
             if (!Shape_[i-1].trim().equals("")){
                 result.append("row%"+Shape_[i-1]);
             }
        }
        return result.toString();
    }
    public String OptFortranTypeDescription() {        
        if (Shape_ == null) return new String("");
        StringBuffer result = new StringBuffer("");
        for (int i=Shape_.length; i>0; i--) {
             if (i < Shape_.length)
                result.append(", ");
             if (!Shape_[i-1].trim().equals("")){
                 result.append("opt%"+Shape_[i-1]);
             }
        }
        return result.toString();
    }
    public String[] getShape() {  
	return Shape_;
    }
    
    /**
     * Return the type of a Tag in the C++ form "TagType::Something" (e.g. TagType::Holography, TagType::SpectralWindow...).
     * 
     * It returns an empty string if the attribute is not a Tag.
     * The algorithm to determine the type of a Tag is based on analyzing its name which always follows 
     *  naming conventions.
     *  It also considers the case of "modeId" which should have been "almaCorrelatorMode" !!!
     *  This should definitely be replaced by a more complete and coherent approach where Tag attributes would be defined with their types in the UML model.
     */
    public String CppTagType () {
    	String result = "";
    	if ( this.SimpleCppType().equals("Tag")) {
    		if (this.NameS().equals("pairedAntennaId"))
    			return "TagType::Antenna";
    		// Is it this "modeId" ???
    		if (this.NameS().equals("almaCorrelatorModeId"))
    				return "TagType::CorrelatorMode";
    		if (this.NameS().equals("modeId"))
    				return "TagType::AlmaRadiometer";
    		
    		result = this.NameS();
    		// Does the name starts with assoc ?
    		if (result.indexOf("assoc") == 0) {
    			// Then extract assoc
    			result = result.substring(5);
    		}
    		// Does the name starts with ref ?
    		else if (result.indexOf("ref") == 0) {
    			// Then extract assoc
    			result =result.substring(3);
    		}
    		// I know ... this is infamous !
      		else if (result.equals("imageSpectralWindowId")) {
    			result = result.substring(5);
    		}
    		// Now look for the terminating Id.
			result = result.substring(0, result.indexOf("Id"));
			result = "TagType::"+result.substring(0, 1).toUpperCase() + result.substring(1);	
    	}
    	return result;
    }
    
    /**
     * Return the type of a Tag in the form "Something" (e.g. Holography, SpectralWindow...).
     * 
     * It returns an empty string if the attribute is not a Tag.
     * The algorithm to determine the type of a Tag is based on analyzing its name which always follows 
     *  naming conventions.
     *  It also considers the case of "modeId" which should have been "almaCorrelatorMode" !!!
     *  This should definitely be replaced by a more complete and coherent approach where Tag attributes would be defined with their types in the UML model.
     */
    public String TagType () {
    	String result = "";
    	if ( this.SimpleCppType().equals("Tag")) {
    		// Is it this "modeId" ???
    		if (this.NameS().equals("modeId"))
    				return "AlmaCorrelatorMode";
    		
    		result = this.NameS();
    		// Does the name starts with assoc ?
    		if (result.indexOf("assoc") == 0) {
    			// Then extract assoc
    			result = result.substring(5);
    		}
    		// Does the name starts with ref ?
    		else if (result.indexOf("ref") == 0) {
    			// Then extract assoc
    			result =result.substring(3);
    		}
      		else if (result.equals("imageSpectralWindowId")) {
    			result = result.substring(5);
    		}
    		
    		// Now look for the terminating Id.
			result = result.substring(0, result.indexOf("Id"));
			result = result.substring(0, 1).toUpperCase() + result.substring(1);	
    	}
    	return result;
    }
    
    public String casaColumnDesc() {
    	return VerbatimCASAFiller.casaColumnDesc(this);
    }
    
    public String casaColumn() {
    	return VerbatimCASAFiller.casaColumn(this);
    }
    
    public String casaScalarType() {
    	return VerbatimCASAFiller.casaScalarType(this);
    }
    
    public String casaScalarValue(String rowContext)  {
    	return VerbatimCASAFiller.casaScalarValue(this, rowContext);
    }
    
    public String casaArrayValue(String rowContext)  {
    	return VerbatimCASAFiller.casaArrayValue(this, rowContext);
    }
}
