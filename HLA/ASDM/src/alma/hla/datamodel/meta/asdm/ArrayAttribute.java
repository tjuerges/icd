/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayAttribute.java
 */
package alma.hla.datamodel.meta.asdm;


/**
 * description
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class ArrayAttribute extends ASDMAttribute {
	
	public ArrayAttribute () {
		array = true;
	}

	public String JavaType() {
		String ret = super.JavaType();
		String[] x = Size.split("x");
		for (int i = 0; i < x.length; ++i)
			ret += "[]";
		return ret;
	}
    
    public String XMLJavaType() {
        return this.JavaType();
    }

	public String JavaBaseType() {
		String ret = super.JavaType();
		return ret;
	}

	public String IDLType() {
		String ret = super.IDLType();
		String[] x = Size.split("x");
		for (int i = 0; i < x.length; ++i)
			ret = "sequence < " + ret + " >";
		return ret;
	}
	
	public String IDLBaseType() {
		String ret = super.IDLType();
		return ret;
	}

	public String CppType() {
		String ret = super.CppType();
		String[] x = Size.split("x");
		for (int i = 0; i < x.length; ++i)
			ret = "vector<" + ret + " >";
		return ret;
	}
	
	public String SwigType() {
		String ret = super.SwigType();
		String[] x = Size.split("x");
		for (int i = 0; i < x.length; ++i)
			ret = "vector<" + ret + " > ";
		return ret;
	}
	
    public String XMLCppType() {
        return this.CppType().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
    
	public String CppBaseType() {
		String ret = super.CppType();
		return ret;
	}
    
    

	public String ArrayDimensions() {
		String[] x = Size.split("x");
		return Integer.toString(x.length);
	}
	public boolean isOneD() {
		String[] x = Size.split("x");
		return x.length == 1 ? true : false;
	}
	public boolean isTwoD() {
		String[] x = Size.split("x");
		return x.length == 2 ? true : false;
	}
	public boolean isThreeD() {
		String[] x = Size.split("x");
		return x.length == 3 ? true : false;
	}
	public boolean isFourD() {
		String[] x = Size.split("x");
		return x.length == 4 ? true : false;
	}
	
	public String numberOfDimensions() {
		return Integer.toString(Size.split("x").length);
	}
 /*   
    public String JavaTypeDescription() {
        
        if (Shape_ == null) return new String("");
        
        StringBuffer result = new StringBuffer("Dimensions : ");
        for (int i=0; i<Shape_.length; i++) {
             if (i > 0)
                result.append(",");
             if (!Shape_[i].trim().equals("")){
                 result.append(Shape_[i]);
             }
        }
        return result.toString();
    }
 */   
    public String XMLJavaTypeDescription() {
        return this.JavaTypeDescription();
    }
}
