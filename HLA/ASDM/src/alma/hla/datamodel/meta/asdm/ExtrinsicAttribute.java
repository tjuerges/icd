/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ExtrinsicAttribute.java
 */
package alma.hla.datamodel.meta.asdm;

import org.openarchitectureware.meta.uml.classifier.Class;
import org.openarchitectureware.meta.uml.classifier.PrimitiveType;

import alma.hla.datamodel.util.MMUtil;

/**
 * Table attributes are divided into intrinsic and extrinsic.
 * Intrinsic attributes are those that exist as a part of the table
 * regardless of whether it is assoicated with any other tables
 * or not. An extrinsic attribute is an attribute of a table that 
 * exists in virtue of an external association with another table.
 * An extrinsic attribute takes its properties from an intrinsic
 * attribute of some table.
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class ExtrinsicAttribute extends ASDMAttribute {

	private ASDMAttribute base;
	
	/**
	 * Create an ExtrinsicAttribute of a table.
	 */
	public ExtrinsicAttribute() {
		extrinsic = true;
	}
	
	/**
	 * Create an ExtrinsicAttribute of a table.
	 * @param name The name of this attribute.
	 * @param base The base attribute from which this attribute is derived.
	 * @param table The table to which this attribute belongs.
	 */
	public ExtrinsicAttribute(String name, ASDMAttribute base, Class table) {
		extrinsic = true;
		setBase(base);
		setName(name);
		setType();
		this.setClass(table);
	}
	
	public void setBase(ASDMAttribute base) {
		this.base = base;
		setMetaEnvironment(base.getMetaEnvironment());
	}
	
	public void setType() {
		// Set the type of the link attribute.
		PrimitiveType atype = new PrimitiveType ();
//		Identifier atypename = new Identifier ();
//		atypename.Name = base.Type().Name().toString();
		atype.setName(base.Type().Name().toString());
		super.setType(atype);
	}

// HSO: fully commented out this already no-op method
// because oAW4.1.1 will have setName as a final method
//	public void setName(String name) {
//		Identifier aname = new Identifier ();
//		aname.Name = name;
//		super.setName(name);
//	}
	
	public void setOptional() {
		setMultiplicityMin("0");
		setMultiplicityMax("1");
	}
	
	public void setOptionalOneToMany() {
		setMultiplicityMin("0");
		setMultiplicityMax("-1");	
	}
	
	public void setArray() {
		array = true;
		Size = "*";
	}
    
    public void setSet() {
        isaset = true;
    }

	public void setClass(Class c) {
		super.setClass(c);
	}

	public ASDMAttribute Base() {
		return base;
	}

	public String JavaType() {
		String ret = super.JavaType();
		if (array)
			ret += "[]";
        else 
            if (this.isaset)
                ret = TypeSet();
		return ret;
	}
    
    public String XMLJavaType() {
        return this.JavaType();
    }
	
	public String JavaBaseType() {
		String ret = super.JavaType();
		return ret;
	}
	
    public String TypeSet() {
        return MMUtil.UpperCaseName(super.JavaType())+"Set";        
    }
    
	public String CppType() {
		String ret = super.CppType();
		if (array)
		    ret="vector<" + ret + "> ";
        else 
            if (this.isaset) ret = "set<"+ret+" >";
		return ret;
	}
	
	public String SwigType() {
		return CppType();
	}
    
    public String XMLCppType() {
        return this.CppType().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
	
	public String CppBaseType() {
		String ret = super.CppType();
		return ret;
	}
	
	public String IDLType() {
		String ret = super.IDLType();
		if (this.array || this.isaset)
			return "sequence< " + ret + " >";
		return ret;
	}
	
	public String IDLBaseType() {
		String ret = super.IDLType();
		return ret;
	}
	
	public String SimpleUpperCaseName() {
		return MMUtil.UpperCaseName(SimpleName());
	}
	
	public String SimpleJavaType() {
		return MMUtil.JavaType(Type());
	}

	public String SimpleName() {
		return MMUtil.simpleName(NameS());
	}
	
	private String target = "";
	
	/*
	 * Set the name of the Table this extrinsic attribute refers to.
	 */
	public void targetTable(String target) {
		this.target = target;
	}
	
	/*
	 * Get the name of the Table this extrinsic attribute refers to.
	 */
	public String targetTable() {
		return target;
	}
	
	boolean is_id = false;
	
	/*
	 * Set is_id to  true if this extrinsic attribute corresponds to a slice or slices association, otherwise
	 * is_id is left to false..
	 */
	public void isId(boolean is_id) {
		this.is_id = is_id;
	}
	
	/*
	 * Returns true if this extrinsic attribute corresponds to a slice or slices association and false otherwise.
	 */
	public boolean isId() {
		return this.is_id;
	}
	
	boolean is_tag = false;
	/*
	 * Set is_tag to  true if this extrinsic attribute corresponds to a hasa or hasmany association, otherwise
	 * is_tag is left to false..
	 */
	public void isTag(boolean is_tag) {
		this.is_tag = is_tag;
	}
	
	/*
	 * Returns true if this extrinsic attribute corresponds to a slice or slices association and false otherwise.
	 */
	public boolean isTag() {
		return this.is_tag;
	}
	
}
