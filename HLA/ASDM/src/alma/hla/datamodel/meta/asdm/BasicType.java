/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File BasicType.java
 */
package alma.hla.datamodel.meta.asdm;

/**
 * description
 * 
 * @version 1.00 Dec 30, 2004
 * @author Allen Farris
 */
public class BasicType {
	
	static public BasicType[] getBasicType() {
		return basicType;
	}

	static private final BasicType[] basicType = {
			//             		JavaName        				WrapperName			OutputType					IDLName          				 isExtended
			new BasicType ( "int", 							"Integer",					"Int",								"long", 							false),
			new BasicType ( "short", 						"Short",						"Short",							"short", 						false),
			new BasicType ( "long",	 						"Long",						"Long",							"long long",					false),
			new BasicType ( "byte",	 						"Byte",						"Byte",							"octet", 							false),
			new BasicType ( "float",							"Float",						"Float",							"float", 							false),
			new BasicType ( "double",						"Double",					"Double",						"double", 						false),
			new BasicType ( "char",	 						"Character",				"Char",							"char", 							false),
			new BasicType ( "boolean",					"Boolean",					"Boolean",						"boolean", 					false),
			new BasicType ( "String",						"String",					null,								"string", 						false),
			new BasicType ( "Angle",	 					"Angle",					"Double",						"IDLAngle", 					true),
			new BasicType ( "AngularRate",				"AngularRate",			"Double",						"IDLAngularRate", 			true),
			new BasicType ( "ArrayTime",				"ArrayTime",				"Long",							"IDLArrayTime", 			true),
            new BasicType ( "ArrayTimeInterval",    	"ArrayTimeInterval", 	null,								"IDLArrayTimeInterval",	true),            
			new BasicType ( "Complex",	 				"Complex",				null,								"IDLComplex", 				true),
			new BasicType ( "Entity",	 					"Entity",						null,								"IDLEntity", 					true),
			new BasicType ( "EntityId",	 					"EntityId",					null,								"IDLEntityId",					true),
			new BasicType ( "EntityRef",					"EntityRef",				null,								"IDLEntityRef", 				true),
			new BasicType ( "Flux",	 						"Flux",						"Double",						"IDLFlux", 						true),
			new BasicType ( "Frequency",				"Frequency",				"Double",						"IDLFrequency", 			true),
			new BasicType ( "Humidity",	 				"Humidity",				"Double",						"IDLHumidity", 				true),
			new BasicType ( "Interval",	 					"Interval",					"Long",							"IDLInterval", 				true),
			new BasicType ( "Length",	 					"Length",					"Double",						"IDLLength", 					true),
			new BasicType ( "Pressure",	 				"Pressure",					"Double",						"IDLPressure", 				true),
			new BasicType ( "Speed",	 					"Speed",					"Double",						"IDLSpeed", 					true),
			new BasicType ( "Tag",	 						"Tag",						null,								"IDLTag", 						true),
			new BasicType ( "Temperature",			"Temperature",			"Double",						"IDLTemperature", 		true)
			//, new BasicType ( "AntennaType",        "AntennaType",    null,                                 "AntennaTypeIDL",    true)
	};
	
	static public final BasicType getBasicType(String javaName) {
		for (int i = 0; i < basicType.length; ++i) {
			if (basicType[i].JavaName.equals(javaName))
				return basicType[i];
		}
		return null;
	}
	
	private String JavaName;
	private String WrapperName;
	private String OutputType;
	private String IDLName;
	private boolean extended;
	
	BasicType(String j, String w, String on, String i, boolean e) {
		JavaName = j;
		WrapperName = w;
		OutputType = on;
		IDLName = i;
		extended = e;
	}
	
	/**
	 * @return Returns the extended.
	 */
	public boolean isExtended() {
		return extended;
	}

	/**
	 * @return Returns the iDLName.
	 */
	public String getIDLName() {
		return IDLName;
	}

	/**
	 * @return Returns the javaName.
	 */
	public String getJavaName() {
		return JavaName;
	}

	/**
	 * @return Returns the wrapperName.
	 */
	public String getWrapperName() {
		return WrapperName;
	}
	
	/**
	 * @return Returns the OutputName.
	 */
	public String getOutputType() {
		return OutputType;
	}

}
