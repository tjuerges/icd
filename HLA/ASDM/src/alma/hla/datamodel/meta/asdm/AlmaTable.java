/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaTable.java
 */
package alma.hla.datamodel.meta.asdm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Class;
import org.openarchitectureware.meta.uml.Type;

import alma.hla.datamodel.meta.asdm.shapeparser.ShapeParser;
import alma.hla.datamodel.util.ConsistentOrderingComparator;
import alma.hla.datamodel.util.MMUtil;

/**
 * The AlmaTable class contains all information about an ALMA table
 * in the ALMA model.
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class AlmaTable extends Class {
	
	/**
	 *  The set of attributes which are not static, i.e. those attributes
	 *  which define the content of each row of the table.
	 */
	private ElementSet rowattributes;
	
	/**
	 * The set of attributes which are static, i.e. those attributes
	 * which are specific to the table and do not define the content
	 * of each row.
	 */
	private ElementSet tableattributes;
	
	/**
	 * The list of attributes intrinsic to this table.
	 */
	private ElementSet intrinsic;
	
	/**
	 * The list of attributes needed to manage links.
	 */
	private ElementSet extrinsic;
	
	/**
	 * The keys of this table.
	 */
	private ElementSet key;
	
	/**
	 * Links to other tables.
	 */
	private ElementSet link;
	
	/**
	 *  The ASDM attributes
	 */
	private ElementSet asdmattributes;
	
	/**
	 * The autoIncrementable attributes
	 */
	private ElementSet autoincrementable;
	
    /** 
     * The required attributes
     */
    private ElementSet requiredattributes;
    
    /**
     * The required extrinsic attributes
     */
    private ElementSet requiredextrinsicattributes;
    
    /** 
     * The required noautoincrementable attributes.
     */
    private ElementSet requirednoautoincrementableattributes;
    
	/**
	 * The required value-part attributes
	 */
	private ElementSet requiredvalueattributes;
    
    /**
     * The optional value-part attributes
     */
    private ElementSet optionalvalueattributes;
    
    /**
     * The optional extrinsic attributes
     */
    private ElementSet optionalextrinsicattributes;
    
	/**
	 * The key-part attribute
	 */
	private ElementSet keyattributes;
	
	/**
	 * The key-part noautoinc attributes
	 */
	private ElementSet keynoautoincrementableattributes;
    
    /**
     * The key-part noautoinc notimeinterval attributes
     */
    private ElementSet keynoautoincnotimeattributes;
    
    /**
     * The value-part mandatory notimeinterval attributes
     */
    private ElementSet requiredvaluenotimeattributes;
    
    /**
     * The attributes whose type is an enumeration.
     */
    private ElementSet enumeratorattributes;
    
    /**
     * The set of the types used in the attributes declarations
     */
    private ElementSet usedTypes;
    
    /**
     * The set of the types used in the attributes declarations
     * which are also extended types.
     */
    private ElementSet usedExtendedTypes;
        
    /**
     * The temporal attribute. Supposedly unique , part of the key and of
     * type ArrayTimeInterval.
     */
    ASDMAttribute temporalAttribute;
	
	/**
	 * The container to which this table belongs.
	 */
	private AlmaTableContainer container;
	
	/**
	 * Create an AlmaTable.
	 */
	public AlmaTable() {
		intrinsic = new ElementSet ();
		extrinsic = new ElementSet ();
		autoincrementable = new ElementSet();
		key = new ElementSet ();
		link = new ElementSet ();
	}
	
	/**
	 * Returns the CVS revision number of the UML modelling file (ASDM.xml).
	 */
	public String CVSRevision()  {
		return this.container.CVSRevision();
	}
	
	/**
	 * Returns the CVS branch name of the UML modelling file (ASDM.xml).
	 */
	public String CVSBranch() {
		return this.container.CVSBranch();
	}
	
	/**
	 * Set the container to which this table belongs.
	 * @param container
	 */
	public void setContainer(AlmaTableContainer container) {
		this.container = container;
	}
	
	/**
	 * Defines is the table will be exported on disk as a "binary" file
	 * (default no).
	 */
	private boolean fileAsBin = false;
	public void setFileAsBin(String fileAsBin) {
		this.fileAsBin = Boolean.valueOf(fileAsBin);
	}
	
	public boolean fileAsBin() {
		return fileAsBin;
	}
	
	/**
	 * Defines is the table will be archived as a "binary"
	 * MIME document.
	 * (default no).
	 */
	private boolean archiveAsBin = false;
	public void setArchiveAsBin(String archiveAsBin) {
		this.archiveAsBin = Boolean.valueOf(archiveAsBin);
	}
	
	public boolean archiveAsBin() {
		return archiveAsBin;
	}
	
	// Initialization is a mult-step process.	
	/**
	 * Initialization step 1:
	 *  - go through all the ASDMAttributes
	 * that belong to this table and store those that are intrinisic
	 * to this table in its set of ASDMAttributes.
	 */
	public void InitializeOne() {
		Iterator iter = Attribute().iterator();
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof ASDMAttribute) {
				if (!((ASDMAttribute)o).isTableKey() && !((ASDMAttribute)o).isStatic())
					intrinsic.add(o);
			}
		}
		////System.out.println(this.Name+" - intrinsic="+intrinsic);
	}
	
	/**
	 * Initialization step 2: Go through the ASDMAttributes
	 * that belong to this table and store the keys in the
	 * key set.  Those keys are initialized in the process, 
	 * which may cause some link attributes to be created. 
	 */
	public void InitializeTwo() {
		Iterator iter = Attribute().iterator();
		TableKey k = null;
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof TableKey) {
				k = (TableKey)o;
				k.initialize();
				key.add(k);
			}
		}
		if (key.size() == 0)
			Checks.error(this, "Table " + NameS()+ " has no key.");
	}
	
	/**
	 * Initialization step 3: Analyze all the AssociationEnds that are
	 * connected to this table and create and store the TableLinks.  These
	 * are the links to other tables that belong to this table.  This process
	 * also completes the set of LinkAttributes that belong to this table.
	 */
	public void InitializeThree() {
		// OK, intrinsic and key are set.
		// Get all the navigable link ends for this table and put them into link.
		TableLink x = null;
		Iterator iter = AssociationEnd().iterator();
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof TableLink) {
				x = (TableLink)o;
				if (x.Opposite().isNavigable()) {
					x.initialize();
					link.add(x);
				}
			}
		}
		//diag();
	}
	
	/**
	 * Initialization step 4: Go through the ASDMAttributes
	 * that belong to this table and store the autoincrementable ones in the
	 * autoincrementable set.  
	 */
	public void InitializeFour() {
		Iterator iter = Attribute().iterator();
		AutoIncrementableAttribute k = null;
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof AutoIncrementableAttribute) {
				k = (AutoIncrementableAttribute)o;
				//System.out.println(k.NameS() + " is added to the set of autoinc attributes");
				autoincrementable.add(k);
			}
		}
	}
	
    /**
     * temporal is true if there is one scalar attribute of type TimeInterval
     * in the key section or in the required value section.
     */
    boolean temporal = false;
    
	/**
	 * Initialization step 5: go through the ASDMAttributes
	 * that belong to this table:
	 * 	   - to populate rowattributes and tableattributes. 
	 *    - to populate asdmattributes, requiredvalueattributes,
	 * keyattributes and keynoautoincrementableattributes, requredextrinsicattributes 
	 * and optionalextrinsicattributes element sets.
	 */
	public void InitializeFive() {
		Iterator iter = null;

		tableattributes = new ElementSet();
		rowattributes = new ElementSet();
		iter = Attribute().iterator();
		
		while (iter.hasNext()){
			Object o = iter.next();
			if (((ASDMAttribute) o).isStatic())
				tableattributes.add(o);
			else
				rowattributes.add(o);
		}
        // The asdmattributes is actually equal to rowattributes.
		asdmattributes = rowattributes;
	    //System.out.println("Table " + this.Name + " has attributes :" + asdmattributes);	
		
//        asdmattributes = new ElementSet();
//		iter = rowattributes.iterator();
//		while (iter.hasNext()) {
//			Object o = iter.next();
//			if (o instanceof ASDMAttribute) {
//					asdmattributes.add(o);
//			}
//		}
	
	    // We sort alphabetically the set of extrinsic attributes.
        extrinsic = extrinsic.sort(ConsistentOrderingComparator.getInstance());
 
        // The key attributes.
        // Start with the autoincrementable operator if any.    
        /**
         * A new strategy to populate keyattributes based on the order specified in the "key" attribute
         * of the UML specification.
         */
        keyattributes = ((TableKey)key.get(0)).Field();
        
        // The keynoautoincrementableattributes.        
        keynoautoincrementableattributes = new ElementSet();

        for (Object o : keyattributes)
        	if (!((ASDMAttribute) o).autoIncrementable)
        		keynoautoincrementableattributes.add(o);
 

        // The requiredvalueattributes.
        requiredvalueattributes = new ElementSet();

        for (Object o : asdmattributes ) {
        	ASDMAttribute aa = (ASDMAttribute) o;
        	if (!(aa.isKeyPart() || aa.isOptional()) && (extrinsic.contains(o) || intrinsic.contains(o)))
        		requiredvalueattributes.add(o);
        }
        	
		
        // The optionalvalueattributes.
        optionalvalueattributes = new ElementSet();
 
        for (Object o : asdmattributes) 
        	if (((ASDMAttribute)o).isOptional())
        		optionalvalueattributes.add(o);
        

        // The required attributes
        requiredattributes = new ElementSet();

        for (Object o: keyattributes)
        	requiredattributes.add(o);
        
        for (Object o: requiredvalueattributes)
        	requiredattributes.add(o);      

        //System.out.println("Table " + this.Name + " has required attributes :" + RequiredAttributesSet());
        
        // The requirednoautoincrementable attributes.
        if (this.autoincrementable.isEmpty())
            requirednoautoincrementableattributes = requiredattributes;
        else {
        	requirednoautoincrementableattributes = new ElementSet();
        	for (Object o : requiredattributes)
        		if (!((ASDMAttribute)o).autoIncrementable)
        			requirednoautoincrementableattributes.add(o);
        }
        
        //System.out.println("Table " + this.Name + " has required no autoinc attributes :" + RequiredNoAutoIncrementableAttributesList());
        
        // Fills  requiredextrinsicattributes and optionalextrinsicattributes sets.
        optionalextrinsicattributes = new ElementSet();
        requiredextrinsicattributes = new ElementSet();

       for (Object o: extrinsic) {
    	   ASDMAttribute a = (ASDMAttribute) o;
    	   if (a.isOptional())
    		   optionalextrinsicattributes.add(o);
    	   else
    		   requiredextrinsicattributes.add(o);
       }
       
        // The element set of key-part no autoincrementable and not temporal attributes.
        keynoautoincnotimeattributes = new ElementSet();
        temporalAttribute = null;

        for (Object o: keyattributes) {
        	ASDMAttribute aa = (ASDMAttribute) o;
        	if (!(aa.autoIncrementable))
        			if (!aa.isTemporal())
        				keynoautoincnotimeattributes.add(o);
        			else  {
        				if (temporalAttribute == null) {
        					temporal = true;
        					temporalAttribute = aa;
        				}
        				else
                            Checks.error(this, "There can't be more than one attribute of type "+aa.JavaType());
        			}
        }
        
//        System.out.println("The key noautoinc no temporal attributes of " + this.Name + " = " + keynoautoincnotimeattributes);
//        System.out.println("and the corresponding list is " + KeyNoAutoIncNoTimeAttributesList());
        
        // The element set of value-part required and no temporal attributes.
        requiredvaluenotimeattributes = new ElementSet();

        for (Object o: requiredvalueattributes) {
        	ASDMAttribute aa = (ASDMAttribute) o;
        	if (!(aa.isTemporal()))
        		requiredvaluenotimeattributes.add(o);
        }
        
        // The element set of attributes whose type is an enumeration.
        enumeratorattributes = new ElementSet();
		iter = asdmattributes.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute) iter.next();
			if (a.isEnumLiteral())
					enumeratorattributes.add(a);
		}  
		
		HashMap<String, Type> hashMap = new HashMap<String, Type>();
		usedTypes = new ElementSet();
		iter = asdmattributes.iterator();		
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			hashMap.put(a.Type().NameS(), a.Type());
		}		
		usedTypes.addAll(hashMap.values());
		
		hashMap.clear(); 
		usedExtendedTypes = new ElementSet();
		iter = extrinsic.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			if (a.isExtendedType())	{
				hashMap.put(a.Type().NameS(), a.Type());
			}
		}
			
		iter = intrinsic.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			if (a.isExtendedType())	{
				hashMap.put(a.Type().NameS(), a.Type());
			}			
		}		
		usedExtendedTypes.addAll(hashMap.values());		
	}

    /**
     * Initialization step 6: go through the ASDMAttributes
     * that belong to this table and are arrays. For each of them
     * 1) check if a shape has been defined in the UML model, parse it 
     * and define its shape property.
     * 
     *  2) Check if they can be exported in the encoding format announced in the stereotype.
     */
    public void InitializeSix(ElementSet allTables) {
        //System.out.println("Processing shapes and checking export format in table "+this.NameS());
        ASDMAttribute a = null;
        String[] s= null;
        Iterator iter = intrinsic.iterator();
        while (iter.hasNext())  {
            a = (ASDMAttribute)iter.next(); 
            if (a.isArray() && (a.Shape != null)) {
            	// Processing shape.
                try {
                    s = ShapeParser.INSTANCE.parse(a.Shape,this,allTables);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    Checks.error(this,"Error while parsing the shape of "+a.NameS()+"in "+NameS()+". The shape was "+a.Shape);
                }                
                a.setShape_(s);
            }
  
            // Checking Encoding.
            if (a.isArray()) {
                // Only attributes having a non null OutputType field in their type can be exported
                // in Base64.
                if (a.exportEncoding == ExportEncoding.Base64) {
                	if (a.DataOutputType() == null)
                		Checks.error(this, a.NameS() + " cannot be exported in " + a.exportEncoding.toString() +"in "+this.NameS()+". Correct the stereotype.");
                }
            }
        }    
        
        iter = extrinsic.iterator();
        while (iter.hasNext())  {
            a = (ASDMAttribute)iter.next(); 
            if (a.isArray() && (a.Shape != null)) {
                try {
                    s = ShapeParser.INSTANCE.parse(a.Shape,this,allTables);
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    Checks.error(this,"Error while parsing the shape of "+a.NameS()+"in "+NameS()+". The shape was "+a.Shape);
                }                
                a.setShape_(s);
            }
            
            // Check encoding. Only ASCII allowed.
            if (a.isArray() && a.getExportEncoding() != ExportEncoding.ASCII)
            	Checks.error(this, a.NameS()+" can be exported only in " + ExportEncoding.ASCII.toString() + "in " + this.NameS()+". Correct the stereotype.");
        }            
    }
    
 
	/**
	 * This diagnostic method prints the names of all attributes,
	 * keys, and links that are associated with this table.
	 */
	private void diag() {
		//System.out.println("Table " + NameS());
		//System.out.println("Table Attributes");
		Iterator iter = intrinsic.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			//System.out.println(a.NameS());
		}
		//System.out.println("Extrinsic Attributes");
		iter = extrinsic.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			//System.out.println(a.NameS());
		}
		//System.out.println("Key Attributes");
		iter = key.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			//System.out.println(a.NameS());
		}
		//System.out.println("Autoincrementable Attributes");
		iter = autoincrementable.iterator();
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			//System.out.println(a.NameS());
		}		
		//System.out.println("Links to");
		iter = link.iterator();
		while (iter.hasNext()) {
			TableLink a = (TableLink)iter.next();
			//System.out.println(a.Opposite().Class());
		}
	}
	
	// Various get methods.
	
	/**
	 * Return the name of this table with a lower case first letter.
	 */
	public String LowerCase() {
		return MMUtil.LowerCaseName(this.NameS());
		
	}
	
	/**
	 * Return the set of intrinsic attributes as an element set.
	 */
	public ElementSet IntrinsicAttribute () {
		return intrinsic;
	}
	
	/**
	 * Return true if and only if there are no intrinsic attributes.
	 */
	public boolean NoIntrinsicAttribute() {
		return intrinsic.size() == 0;
	}
	
	/**
	 * Return the set of extrinsic attributes as an element set.
	 */
	public ElementSet ExtrinsicAttribute () {
		return extrinsic;
	}
	
	/** 
	 * @return the set of autoincrementable attributes (actually this set 
	 * should contain at most 1 element).
	 */
	public ElementSet AutoIncrementableAttribute () {
		return this.autoincrementable;
	}	
	
	/** 
	 * @return the (likely only one) autoincrementable attribute (actually this set 
	 * should contain at most 1 element).
	 */
	public AutoIncrementableAttribute TheAutoIncrementableAttribute () {
		return (AutoIncrementableAttribute) this.autoincrementable.get(0);
	}	

	/**
	 * Return true if and only if there are no autoincrementable attributes.
	 */
	public boolean NoAutoIncrementableAttribute() {
		////System.out.println (this.NameS() + "autoincrementable size = " + this.autoincrementable.size());
		return this.autoincrementable.size() == 0;
	}
    
    /**
     * Return true if there is ONE temporal attribute (i.e. of type TimeInterval).
     */
    public boolean isTemporal() {
        return temporal;
    }
    
    /**
     * Returns a comma separated list of Java declarations of the list of
     * ASDM attributes passed in the ElementSet s.
     * @return a string containing the Java declarations.
     */
    private String JavaDeclaration(ElementSet s) {
        StringBuffer tmp = new StringBuffer ();
        Iterator iter = s.iterator();
        boolean first = true;
        while (iter.hasNext()) {
            ASDMAttribute a = (ASDMAttribute)iter.next();
            if (!first) tmp.append(", ");
            tmp.append(a.JavaType()  + " " + a.Name());
            if (first) first = false;
        }
        return tmp.toString();        
    }
    
    /**
     * Returns a comma separated list of C++ declarations of the list of
     * ASDM attributes passed in the ElementSet s.
     * @return a string containing the Java declarations.
     */
    private String CppDeclaration(ElementSet s) {
        StringBuffer tmp = new StringBuffer ();
        Iterator iter = s.iterator();
        boolean first = true;
        while (iter.hasNext()) {
            ASDMAttribute a = (ASDMAttribute)iter.next();
            if (!first) tmp.append(", ");
            tmp.append(a.CppType()  + " " + a.Name());
            if (first) first = false;
        }
        return tmp.toString();        
    }
    
    /**
     * Returns a comma separated list of C++ declarations of the list of
     * ASDM attributes passed in the ElementSet s to be presented to a SWIG %extent construct
     * @return a string containing the Cpp declarations.
     */
    private String CppSwigExtentDeclaration(ElementSet s) {
        StringBuffer tmp = new StringBuffer ();
        Iterator iter = s.iterator();
        boolean first = true;
        while (iter.hasNext()) {
            ASDMAttribute a = (ASDMAttribute)iter.next();
            if (!first) tmp.append(", ");
            if (a.isEnumLiteral()) // Enumerations are viewed through the Enum embedding class.
            	tmp.append("Enum<" + a.CppType()  + " > " + a.Name());
            else
            	tmp.append(a.CppType()  + " " + a.Name());
            if (first) first = false;
        }
        return tmp.toString();        
    }    
    
    /**
     * Returns a comma separated list of the attributes names belonging to 
     * the ElementSet s.
     * @return a string containing the list of names.
     */
    private String SetList(ElementSet s){
        StringBuffer tmp = new StringBuffer ();
        Iterator iter = s.iterator();
        boolean first = true;
        while (iter.hasNext()) {
            ASDMAttribute a = (ASDMAttribute)iter.next();
            if (!first) tmp.append(", ");
            tmp.append(a.Name());
            if (first) first = false;
        }
        return tmp.toString();   
    }
	
    /**
     * Return the list of comma separated Java declarations of all mandatory attributes.
     * @return a string containing the java declarations.
     */
	public String RequiredJava() {
        return this.JavaDeclaration(this.requiredattributes);
	}
	
    /**
     * Returns the list of comma separated Java declarations of all the mandatory attributes
     * which are part of the value (complement of the key).
     * @return a string containing the java declarations.
     */
	public String RequiredValueJava() {
        return JavaDeclaration(this.requiredvalueattributes);
	}
    
    /**
     * Returns the list of comma separated Java declarations of all the mandatory attributes
     * which are part of the value (complement of the key) and not temporal.
     * @return a string containing the java declarations.
     */
    public String RequiredValueNoTimeJava() {
        return JavaDeclaration(this.requiredvaluenotimeattributes);
    }
    
    /**
     * Returns the list of comma separated Java declarations of all the mandatory attributes
     * which are not autoincrementable (complement of the key).
     * @return a string containing the java declarations.
     */
    public String RequiredNoAutoIncrementableJava() {
        if (this.autoincrementable.isEmpty()) return this.RequiredJava();
        else return this.JavaDeclaration(this.requirednoautoincrementableattributes);
    }
    
    /**
     * Returns the temporal attribute i.e. the one satisfying 
     * 1) part of key 
     * 2) type ArrayTimeInterval
     */
    public ASDMAttribute TemporalAttribute() {
        return temporalAttribute;
    }
	
	/**
     * Returns the comma separated list of java declarations of all
     * the attributes which are part of the Key.
     * @return a string containing the java declarations.
	 */
	public String KeyJava() {
        return JavaDeclaration(this.keyattributes);
	}
	
	/**
	 * Returns the comma separated list of java declarations of
	 * all attributes that are part of the Key NOT including the autoincrementable
	 * attribute if any.
     * @return a string containing the java declarations.
	 */
	public String KeyNoAutoIncJava() {
		if (this.autoincrementable.size()==0) return KeyJava();
        return JavaDeclaration(this.keynoautoincrementableattributes);
	}
    
    /**
     * Returns the comma separated list of java declarations of
     * all attributes that are part of the Key NOT including the autoincrementable
     * attribute if any nor the temporal one.
     * @return a string containing the java declarations.
     */
    public String KeyNoAutoIncNoTimeJava() {
        return JavaDeclaration(this.keynoautoincnotimeattributes);
    }
    
    

    /**
     * Return the list of comma separated Cpp declarations of all mandatory attributes.
     * @return a string containing the Cpp declarations.
     */
    public String RequiredCpp() {
        return this.CppDeclaration(this.requiredattributes);
    }
    
    /**
     * Return the list of comma separated Cpp declarations of all mandatory attributes.
     * @return a string containing the Cpp declarations.
     */
    public String RequiredSWIGextentCpp() {
        return this.CppSwigExtentDeclaration(this.requiredattributes);
    }
    
    /**
     * Returns the list of comma separated Cpp declarations of all the mandatory attributes
     * which are part of the value (complement of the key).
     * @return a string containing the Cpp declarations.
     */
    public String RequiredValueCpp() {
        return CppDeclaration(this.requiredvalueattributes);
    }

    /**
     * Returns the list of comma separated Cpp declarations of all the mandatory attributes
     * which are part of the value (complement of the key) and NOT temporal
     * @return a string containing the Cpp declarations.
     */
    public String RequiredValueNoTimeCpp() {
        return CppDeclaration(this.requiredvaluenotimeattributes);
    }

    /**
     * Returns the list of comma separated Cpp declarations of all the mandatory attributes
     * which are not autoincrementable (complement of the key).
     * @return a string containing the java declarations.
     */
    public String RequiredNoAutoIncrementableCpp() {
        if (this.autoincrementable.isEmpty()) return this.RequiredCpp();
        else return this.CppDeclaration(this.requirednoautoincrementableattributes);
    }
 
    /**
     * Returns the list of comma separated cpp declarations of all the mandatory attributes
     * which are not autoincrementable (complement of the key) to be used in the context
     * of a SWIG %extent construct.
     * @return a string containing the cpp declarations.
     */
    public String RequiredNoAutoIncrementableSWIGextentCpp() {
        if (this.autoincrementable.isEmpty()) return this.RequiredSWIGextentCpp();
        else return this.CppSwigExtentDeclaration(this.requirednoautoincrementableattributes);
    }    
    
    /**
     * Returns the comma separated list of Cpp declarations of all
     * the attributes which are part of the Key.
     * @return a string containing the Cpp declarations.
     */
    public String KeyCpp() {
        return CppDeclaration(this.keyattributes);
    }
    
    /**
     * Returns the comma separated list of Cpp declarations of
     * all attributes that are part of the Key NOT including the autoincrementable
     * attribute if any.
     * @return a string containing the cpp declarations.
     */
    public String KeyNoAutoIncCpp() {
        if (this.autoincrementable.size()==0) return KeyCpp();
        return CppDeclaration(this.keynoautoincrementableattributes);
    }   

    /**
     * Returns the comma separated list of Cpp declarations of
     * all attributes that are part of the Key NOT including the autoincrementable
     * attribute if any nor the temporal one.
     * @return a string containing the java declarations.
     */
    public String KeyNoAutoIncNoTimeCpp() {
        return CppDeclaration(this.keynoautoincnotimeattributes);
    }    
    
	/**
	 * @return a string containing the names of all attributes which are mandatory part of the Value. 
	 * Names are separated by commas.
	 */
	public String RequiredValueAttributesList() {
        return SetList(this.requiredvalueattributes);
    }
    
    /**
     * @return a string containing the names of all attributes which are mandatory part of 
     * the Value and NOT temporal. 
     * Names are separated by commas.
     */
    public String RequiredValueNoTimeAttributesList() {
        return SetList(this.requiredvaluenotimeattributes);
    }
    
    /**
     * @return a string containing the names of all attributes which are optional part of the Value. 
     * Names are separated by commas.
     */
    public String OptionalValueAttributesList() {
        return SetList(this.optionalvalueattributes);
    }    

    /**
     * @return a string containing the names of all attributes which are mandatory and not
     * autoincrementable.Names are separated by commas.
     */
	public String RequiredNoAutoIncrementableAttributesList() {
	    return SetList(this.requirednoautoincrementableattributes);
    }
    
	/**
	 * @return the set of ASDM attributes
	 */
	public ElementSet ASDMAttributesSet() {
		return asdmattributes;
	}
	
	/**
     * @return the number of attributes, as a String.
     */
    public String numberOfAttributes() {
    	return String.valueOf(keyattributes.size() + requiredvalueattributes.size() + optionalvalueattributes.size());
    }
    
    /**
     * @return the set of required attributes (key and value).
     */
    public ElementSet RequiredAttributesSet() {
        return requiredattributes;
    }
        
    /**
     * @return the set of required attributes (key and value).
     */
    public ElementSet RequiredExtrinsicAttributesSet() {
        return requiredextrinsicattributes;
    }
    
    /**
     * @return the set of required no autoincrementable attributes
     * 
     */
    public ElementSet RequiredNoAutoIncrementableAttributesSet() {
        return this.requirednoautoincrementableattributes;
    }
    
	/**
	 * @return the set of required value-part mandatory attributes.
	 * 
	 */
	public ElementSet RequiredValueAttributesSet() {
		return requiredvalueattributes;
	}
    
    /**
     * @return the number of required attributes, as a String.
     */
    public String numberOfRequiredAttributes() {
    	return String.valueOf(requiredvalueattributes.size());
    }
    
    /**
     * @return the set of required value-part mandatory and NOT temporal attributes.
     * 
     */
    public ElementSet RequiredValueNoTimeAttributesSet() {
        return requiredvaluenotimeattributes;
    }    
    
    /**
     * @return the set of optional value-part attributes.
     * 
     */
    public ElementSet OptionalValueAttributesSet() {
        return optionalvalueattributes;
    } 
    
    /**
     * @return the number of required attributes, as a String.
     */
    public String numberOfOptionalAttributes() {
    	return String.valueOf(optionalvalueattributes.size());
    }
    
    /**
     * @return the set of optional extrinsic attributes.
     */
    public ElementSet OptionalExtrinsicAttributesSet() {
        return optionalextrinsicattributes;
    }
	
    /**
     * @return the number of required value-part attributes.
     * 
     */
    public int RequiredValueAttributesSize() {
        return requiredvalueattributes.size();
    }
    
	/**
	 * @return the set of key-part attributes.
	 * 
	 */
	public ElementSet KeyAttributesSet() {
		return keyattributes;
	}
    
	/**
	 * @return the number of key attributes in a String
	 */
	public String numberOfKeyAttributes() {
		return String.valueOf(keyattributes.size());
	}
	
    /**
     * @return the number of key-part attributes.
     * 
     */
    public int KeyAttributesSize() {
        return keyattributes.size();
    }
	
    /**
     * @return the set of attributes whose type is an enumeration.
     */
    public ElementSet EnumeratorAttributes() {
    	return enumeratorattributes;
    }
    
    /*
     * @return the set of Type used in the declaration of the ASDMAttributes.
     */
    public ElementSet UsedTypes() {
    	return usedTypes;
    }
    
    /*
     * @return the set of Type used in the declarations of the ASDAttributes
     * and which are extended types (like Angle, Entity....)
     * 
     */
    public ElementSet UsedExtendedTypes() {
    	return usedExtendedTypes;
    }
    
    /*
     * @return true (resp.) if at least one (resp. none) of the attributes is
     * of type long.
     */
    public boolean hasLongAttribute() {
    	for (Object o : UsedTypes())
    		if (((Type) o).Name.equals("long"))
    			return true;
    	return false;
    }
    
	/**
	 * @return the set of key-part non autoincrementable attributes.
	 * 
	 */
	public ElementSet KeyNoAutoIncrementableAttributesSet() {
		return keynoautoincrementableattributes;
	}
    
    /**
     * @return the set of key-part non autoincrementable and no temporal attributes.
     * 
     */
    public ElementSet KeyNoAutoIncNoTimeAttributesSet() {
        return keynoautoincnotimeattributes;
    }

    
	/**
	 * @return Returns the comma separated list all attributes that are part of the Key NOT 
	 * including the autoincrementable attribute if any. 
	 */
	public String KeyNoAutoIncrementableAttributesList() {
        if (this.autoincrementable.size()==0) return SetList(this.keyattributes);
        return SetList(this.keynoautoincrementableattributes);
    }
    
    
    /**
     * @return Returns the comma separated list all attributes that are part of the Key NOT 
     * including the autoincrementable attribute if any and NOT including the temporal ones. 
     */
    public String KeyNoAutoIncNoTimeAttributesList() {
        return SetList(this.keynoautoincnotimeattributes);
    }
    
    /**
     * @return Returns the number of attributes that are part of the Key NOT 
     * including the autoincrementable attribute if any. 
     */
    public int KeyNoAutoIncrementableAttributesSize() {
        return this.autoincrementable.size(); 
    }
    
    /**
     * @return Returns the comma separated list all attributes that are part of the Key,
     * including the autoincrementable attribute if any. 
     */
    public String KeyAttributesList() {
        return SetList(this.keyattributes);
    }    
 	
	/**
	 * Return true if and only if there are extrinsic attributes.
	 */
	public boolean NoExtrinsicAttribute() {
		return extrinsic.size() == 0;
	}
	
	/**
	 * Return the set of keys as an element set.
	 */
	public ElementSet Key () {
		return key;
	}

	/**
	 * Return the number of keys.
	 */
	public int numberKey() {
		return key.size();
	}
	
	/**
	 * Return the set of links as an element set.
	 */
	public ElementSet Link () {
		return link;
	}

	/**
	 * Return true if and only if there are links.
	 */
	public boolean NoLink() {
		return link.size() == 0;
	}
	
	/**
	 * Return the container to which this table belongs as an element.
	 */
	public Element Container() {
		return container;
	}
	
	/**
	 * Return the ASDMAttribute whose name is the specified name.
	 * If there is no such attribute, null is returned.
	 * @param name The name of the attribute to be returned.
	 * @return The ASDMAttribute with the specifed name or null 
	 * if there is no such attribute.
	 */
	public ASDMAttribute getAttribute(String name) {
		Iterator iter = intrinsic.iterator();
		ASDMAttribute x = null;
		while (iter.hasNext()) {
			x = (ASDMAttribute)iter.next();
			if (x.NameS().equals(name))
				return x;
		}
		iter = extrinsic.iterator();
		while (iter.hasNext()) {
			x = (ASDMAttribute)iter.next();
			if (x.NameS().equals(name))
				return x;
		}
		return null;
	}

	/**
	 * Return the ASDMAttribute by matching the simple forms of the specified name.
	 * If there is no such attribute, null is returned.
	 * @param name The name of the attribute to be returned.
	 * @return The ASDMAttribute with the specifed name or null 
	 * if there is no such attribute.
	 */
	public ASDMAttribute getAttributeSimple(String name) {
		String sname = MMUtil.simpleName(name);
		
		
		Iterator iter = intrinsic.iterator();
		ASDMAttribute x = null;
		while (iter.hasNext()) {
			x = (ASDMAttribute)iter.next();
			if (MMUtil.simpleName(x.NameS()).equals(sname))
				return x;
		}
		iter = extrinsic.iterator();
		while (iter.hasNext()) {
			x = (ASDMAttribute)iter.next();
			if (MMUtil.simpleName(x.NameS()).equals(sname))
				return x;
		}
		return null;
	}

	/**
	 * Add the specified extrinsic attribute to this table list of
	 * extrinsic attributes.
	 * @param a The extrinsic attribute to be added.
	 */
         void addExtrinsicAttribute(ExtrinsicAttribute a) {
	     extrinsic.add(a);
	 }


         /**
          * Look for an attribute in tableattributes (i.e. static  attribute)
          * whose name is 'xmlns' and return its value , which is supposed to 
          * be the xmlns prefix for this table.
          * If the attribute is not found or if it does not have a value, then exit.
          * 
          */
         public String xmlnsPrefix() {
        	 Iterator iter = tableattributes.iterator();
        	 while (iter.hasNext()){
        		 ASDMAttribute a = (ASDMAttribute) iter.next();
        		 if (a.NameS().equals("xmlnsPrefix")) {
        			 if (a.hasInitValue()) {
        				 String prefix = a.InitValue();
        				 if (prefix.matches("[a-zA-Z]+"))
        					 return prefix;
        				 else 
        					 Checks.error(this, "'"+prefix+"'  is not a valid prefix for an xmlns definition (table '"+this.NameS()+"')");
        			 }
        			 else {
        				 Checks.error(this, "A static attribute 'xmlns' was found in the table '"+this.NameS()+"' but without an initial value; it must have one !");
        			 }
        		 }
        	 }
        	 Checks.error(this, "Could not find a static attribute 'xmlns' in table '"+this.NameS()+"')");
        	 return "";
         } 

    //------------- Methods used for C interface of ASDM -------------------

    /**
     * Return the name of this table in  lower cases.
     */
    public String ToLowerCase() {
	String s =  this.Name().toString();
	return s.toLowerCase();
    }


    /**
     *
     *  Build a list of C parameters for definition of sdm_add()xxxxRow() :
     *  Take all the required attributes, except autoIncrementable
     *   example : (int * stationId, double * pressure,...)
     */ 
    public String RequiredAddCList() {
	Iterator iter = null;
	String s="";
	boolean first = true;

	if (!requirednoautoincrementableattributes.isEmpty()) {
	    iter = requirednoautoincrementableattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		//		if (!a.isAutoIncrementable() && !a.isOptional()) {
		    if (first)
			first=false;
		    else
			s= s+", ";
		    if (!a.isArray() && !a.isStringType() && !a.CppType().equals("EntityRef")) {
			s=s+a.CType()+" * "+a.Name();
		    } else {    // array or string
			s=s+a.CType()+" * "+a.Name() + ", int * "+a.Name()+"Dim";
		    }
		    //		}
	    }
	}
	return s;
    }
    /**
     *
     *  Build a list of Fortran  parameters for use of sdm_add()xxxxRow() :
     *  Take all the required attributes, except autoIncrementable
     *   example : (int * stationId, double * pressure,...)
     */
    public String RequiredAddFList() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	
	if (!requirednoautoincrementableattributes.isEmpty()) {
	    iter = requirednoautoincrementableattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s= s+", ";
		if (!a.isKeyPart())
		    s=s+"row%";
		else
		    s=s+"key%";
		if (!a.isArray() && !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+a.Name();
		} else {    // array or string
		    s=s+a.Name()+", "+a.Name()+"Dim";
		}
	    }
	}
	return s;
    }
    

    /**
     *
     *  Build a list of C parameters for definition of sdm_getxxxxRow()
     *  Take all the required attributes (including autoIncrementable)
     *   example : (int * stationId, double * pressure,...)
     */ 
    public String RequiredGetCList() {
	Iterator iter = null;
	String s="";
	boolean first = true;

	if (!requiredattributes.isEmpty()) {
	    iter = requiredattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s = s+", ";
		if (!a.isArray() && !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+a.CType()+" * "+a.Name();
		} else {    // array or string 
		    s=s+a.CType()+" * "+a.Name() + ", int * "+a.Name()+"Dim";

		}
	    }
	}
	return s;
    }
     /**
      *
      *  Build a list of Fortran  parameters for definition of sdm_getxxxxRow()
      *  Take all the required attributes (including autoIncrementable)
      *   example : (int * stationId, double * pressure,...)
      */
    public String RequiredGetFList() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	
	if (!requiredattributes.isEmpty()) {
	    iter = requiredattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s = s+", ";
		if (a.isKeyPart())
		    s=s+"key%";
		else
		    s=s+"row%";
		if (!a.isArray() && !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+a.Name();
		} else {    // array or string
		    s=s+a.Name()+", "+a.Name()+"Dim";
		    
		}
	    }
	}
	return s;
    }
    

    /**
     *
     *  Build a list of parameters for newRow()
     *   example : ( Tag(*station), ArrayTimeInterval(*timeInterval, *(timeInterval+1)), Pressure(*pressure),...)
     *
     */ 
    public String RequiredValueCppFromCList() {
	Iterator iter = null;
	String s="";
	if (!requirednoautoincrementableattributes.isEmpty()) {
	    iter = requirednoautoincrementableattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		String t =  a.CppType();
		if (t.equals("Tag")) {
			s=s+a.CppType()+"(*"+a.NameS()+","+a.CppTagType()+")";
		}
		else if (t.equals("Angle") ||
		    t.equals("AngularRate") ||
		    t.equals("Flux") ||
		    t.equals("Frequency") ||
		    t.equals("Humidity") ||
		    t.equals("Interval") ||
		    t.equals("Length") || 
		    t.equals("Pressure") ||
		    t.equals("Speed") ||
		    //t.equals("Tag") || 
		    t.equals("Temperature") )
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTime")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTimeInterval")) 
		    s=s+a.CppType()+"(*"+a.Name()+", *("+a.Name()+"+1))";
		else if (t.equals("EntityRef") )
		    s=s+a.CppType()+"(string("+a.Name()+"))";
		else if (t.equals("string"))
		    s=s+a.CppType()+"("+a.Name()+")";
		else if (a.isArray())
		    s=s+a.Name()+"Vec";
		else
		    s=s+"*"+a.Name();
		if (iter.hasNext())
		    s= s+", ";
	    }
	}
	return s;
    }

    /**
     *
     *  Build a list of parameters for newRow()
     *  Same than previous routine, except that  ArrayTimeInterval() contains only a start time (interval<0) 
     *  example : ( Tag(*station), ArrayTimeInterval(*timeInterval), Pressure(*pressure),...)
     *
     */ 
    public String RequiredValueCppFromCListWithoutInterval() {
	Iterator iter = null;
	String s="";
	if (!requirednoautoincrementableattributes.isEmpty()) {
	    iter = requirednoautoincrementableattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		String t =  a.CppType();
		if (t.equals("Tag"))  {
				s=s+a.CppType()+"(*"+a.NameS()+","+a.CppTagType()+")";
		}
		else if (t.equals("Angle") ||
		    t.equals("AngularRate") ||
		    t.equals("Flux") ||
		    t.equals("Frequency") ||
		    t.equals("Humidity") ||
		    t.equals("Interval") ||
		    t.equals("Length") || 
		    t.equals("Pressure") ||
		    t.equals("Speed") ||
		    //t.equals("Tag") || 
		    t.equals("Temperature") )
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTime")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTimeInterval")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("EntityRef") )
		    s=s+a.CppType()+"(string("+a.Name()+"))";
		else if (t.equals("string"))
		    s=s+a.CppType()+"("+a.Name()+")";
		else if (a.isArray())
		    s=s+a.Name()+"Vec";
		else
		    s=s+"*"+a.Name();
		if (iter.hasNext())
		    s= s+", ";
	    }
	}
	return s;
    }

    /**
     *
     *  Build a list of parameters for getRowByKey
     *   example : (Tag(*station),ArrayTimeInterval(*timeInterval, *(timeInterval+1)))
     *
     */ 
    public String KeyCppFromCList() {
	Iterator iter = null;
	String s=" ";
	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		String t =  a.CppType();
		if (t.equals("Tag")) {
//			String tagType = a.NameS().substring(0, a.NameS().indexOf("Id"));
//			tagType = tagType.substring(0, 1).toUpperCase() + tagType.substring(1);
			s = s+ "Tag(*" + a.Name() + ", " + a.CppTagType() +")";
		}
		else if  (t.equals("Angle") ||
		    t.equals("AngularRate") ||
		    t.equals("Flux") || 
		    t.equals("Frequency") || 
		    t.equals("Humidity") ||
		    t.equals("Interval") ||
		    t.equals("Length") || 
		    t.equals("Pressure") ||
		    t.equals("Speed") ||
		    //t.equals("Tag") || 
		    t.equals("Temperature") )
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTime")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTimeInterval")) 
		    s=s+a.CppType()+"(*"+a.Name()+", *("+a.Name()+"+1))";
		else if (t.equals("EntityRef"))
		    s=s+a.CppType()+"(string("+a.Name()+"))";
		else if (t.equals("string"))
		    s=s+a.CppType()+"("+a.Name()+")";
		else 
		    s=s+"*"+a.Name();
		if (iter.hasNext())
		    s= s+", ";
	    }
	}
	return s;
    }

    /**
     *
     *  Build a list of parameters for getRowByKey
     *  Same than previous routine, except that  ArrayTimeInterval() contains only a start time (interval<0) 
     *   example : (Tag(*station),ArrayTimeInterval(*timeInterval, *(timeInterval+1)))
     *
     */ 
    public String KeyCppFromCListWithoutInterval() {
	Iterator iter = null;
	String s=" ";
	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		String t =  a.CppType();
		if (t.equals("Tag")) {
//			String tagType = a.NameS().substring(0, a.NameS().indexOf("Id"));
//			tagType = tagType.substring(0, 1).toUpperCase() + tagType.substring(1);
			s = s+ "Tag(*" + a.Name() + ", " + a.CppTagType() +")";
		}
		else if (t.equals("Angle") ||
		    t.equals("AngularRate") ||
		    t.equals("Flux") || 
		    t.equals("Frequency") || 
		    t.equals("Humidity") ||
		    t.equals("Interval") ||
		    t.equals("Length") || 
		    t.equals("Pressure") ||
		    t.equals("Speed") ||
		    // t.equals("Tag") || 
		    t.equals("Temperature") )
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTime")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("ArrayTimeInterval")) 
		    s=s+a.CppType()+"(*"+a.Name()+")";
		else if (t.equals("EntityRef"))
		    s=s+a.CppType()+"(string("+a.Name()+"))";
		else if (t.equals("string"))
		    s=s+a.CppType()+"("+a.Name()+")";
		else 
		    s=s+"*"+a.Name();
		if (iter.hasNext())
		    s= s+", ";
	    }
	}
	return s;
    }

    /**
     *
     *  Build a list of C type parameters for definition of sdm_add_tttt_xxxx, where tttt is the table name and xxxx the name of an optional parameter
     *  Take all the key attributes
     *   example : (int * stationId, double * pressure,...)
     */ 
    public String KeyCTypeList() {
	Iterator iter = null;
	String s="";
	boolean first = true;

	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s= s+", ";
		if (!a.isArray()&& !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+a.CType()+" * "+a.Name();
		} else {    // array
		    s=s+a.CType()+" * "+a.Name()+", int * "+a.Name()+"Dim";
		}
	    }
	}
	return s;
    }
    /**
     *
     *  Build a list of Fortran type parameters for use of
     *  getTableKeys. Take all the key attributes example : (stationIdList,
     *  pressureList,...)
     */
    public String KeyFTypeList() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	
	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s= s+", ";
		if (!a.isArray()&& !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+a.Name()+"List";
		} else {    // array
		    s=s+a.Name()+"List"+", "+a.Name()+"ListDim";
		}
	    }
	}
	return s;
    }
    /**
     *
     *  Build a list of Fortran parameters for use of sdmAddTableParameter
     *  where Parameter is the name of an optional parameter; Take all the
     *  key attributes
     */
    public String KeyFType() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	
	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (first)
		    first=false;
		else
		    s= s+", ";
		if (!a.isArray()&& !a.isStringType() && !a.CppType().equals("EntityRef")) {
		    s=s+"key%"+a.Name();
		} else {    // array
		    s=s+"key%"+a.Name()+", "+a.Name()+"Dim";
		}
	    }
	}
	return s;
    }
    
    public boolean isArrayTimeIntervalKey() {
	Iterator iter = null;
	boolean found = false;
	if (!keyattributes.isEmpty()) {
	    iter = keyattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (a.CppType().equals("ArrayTimeInterval")) {
		    found = true;
		}
	    }
	} 
	return found;
	
    }


    public ElementSet AutoIncrementableSet () {
	return autoincrementable;
    }

    /**
     *  Check for optional arrays 
     */ 
    public boolean  hasOptionalArrays() {
	Iterator iter = null;
	if (!optionalvalueattributes.isEmpty()) {
	    iter = optionalvalueattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (a.isArray()) { 
		    return true;
		}
	    }
	}
	return false;
    }
    /**
     *  Check argument list for allocation
     */ 
    private String FShape(ASDMAttribute a) {
	String s = "";
	String[] shape=a.getShape();
	if (shape != null) {
	    for (int i=shape.length; i>0; i--) {
		if (i < shape.length) s=s+", ";
		if (!shape[i-1].trim().equals("")){
		    Iterator iter = null;
		    iter = requiredattributes.iterator();
		    boolean found = false;
		    while (iter.hasNext() && !found) {
			ASDMAttribute x = (ASDMAttribute)iter.next();
			if (x.NameS().equals(shape[i-1])) {
			    found = true;
			    s = s + "row%"+x.NameS();
			}
		    }				    
		    iter = optionalvalueattributes.iterator();
		    while (iter.hasNext() && !found) {
			ASDMAttribute x = (ASDMAttribute)iter.next();
			if (x.NameS().equals(shape[i-1])) {
			    found = true;
			    s = s + "opt%"+x.NameS();
			}
		    }				    
		    if (! found) {
			// ugly !
			if (shape[i-1].equals("numAntenna*numFeed")) {
			    s = s + "row%numAntenna*row%numFeed";
			} else {
			    s = s + shape[i-1];
			}
		    }
		}
	    }
	}
	return s;
    }
    public String RowAllocateFList() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	if (!requiredattributes.isEmpty()) {
	    iter = requiredattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (a.isArray()) {
		    if (first)
			first=false;
		    else
			s = s+", ";
		    s=s+" row%"+a.Name()+"("+FShape(a)+")";
		}
	    }
	}
	return s;
    }
 
    /**
     *  Check for required arrays 
     */ 
    public boolean  hasRequiredArrays() {
	Iterator iter = null;
	if (!requiredattributes.isEmpty()) {
	    iter = requiredattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (a.isArray()) { 
		    return true;
		}
	    }
	}
	return false;
    }
 

    public String OptAllocateFList() {
	Iterator iter = null;
	String s="";
	boolean first = true;
	if (!optionalvalueattributes.isEmpty()) {
	    iter = optionalvalueattributes.iterator();
	    while (iter.hasNext()) {
		ASDMAttribute a = (ASDMAttribute)iter.next();
		if (a.isArray()) {
		    if (first)
			first=false;
		    else
			s = s+", ";
		    s=s+" opt%"+a.Name()+"("+FShape(a)+")";
		}
	    }
	}
	return s;
    }
	
}
