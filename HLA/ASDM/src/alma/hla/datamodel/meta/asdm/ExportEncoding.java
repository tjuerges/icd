/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * ExportEncoding.java
 * caillat
 * Jun 14, 2006
 */
package alma.hla.datamodel.meta.asdm;

import java.util.Hashtable;

import org.openarchitectureware.meta.uml.classifier.Class;

/**
 * @author caillat
 * 
 */
public class ExportEncoding extends Class {
	private final String name;

	private ExportEncoding(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	public static final ExportEncoding ASCII = new ExportEncoding("ASCII");
	public static ExportEncoding EE_ASCII() {return ASCII;}
	
	public static final ExportEncoding Base64 = new ExportEncoding("Base64");
	public static ExportEncoding EE_Base64() {return Base64;}
	public static ExportEncoding get(String s) {
		return (ExportEncoding) exportEncodingHashTable.get(s);
	}

	private static Hashtable initializeExportEncodingHashTable() {
		Hashtable result = new Hashtable();
		result.put("ASCII", ASCII);
		result.put("Base64", Base64);
		return result;
	}

	private static Hashtable exportEncodingHashTable = initializeExportEncodingHashTable();
}
