/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaTableContainer.java
 */
package alma.hla.datamodel.meta.asdm;

//import de.bmiag.genfw.meta.classifier.Class;
//import de.bmiag.genfw.meta.ElementSet;
//import de.bmiag.genfw.meta.core.Identifier;
//import de.bmiag.genfw.meta.core.PrimitiveType;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Class;
import org.openarchitectureware.meta.uml.classifier.PrimitiveType;

/**
 * The AlmaTableContainer class contains the information about the
 * AlmaTableContainer in the model.  There must be one and only one
 * AlmaTableContainer in the model.
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class AlmaTableContainer extends Class {
	
	/*
	 * The version number
	 */
	private String version = null;
		
	/*
	 * Set the version's value.
	 * There must be an attribute version with an InitValue set to some integer value.
	 */
	protected void setVersion() {
		for (Object o : ASDMAttributes()) {
			ASDMAttribute a = (ASDMAttribute) o;
			if (a.NameS().equals("version") && a.hasInitValue()) {
				try {
					version = Integer.toString(Integer.parseInt(a.InitValue()));
				}
				catch (NumberFormatException e) {
					System.out.print("Could not parse the value of the metaclass '"+this.NameS()+"'  attribute 'version' into an integer");
					System.out.println("(I read version="+a.InitValue()+"). Abort");
					System.exit(-1);
				}
				return;
			}
		}
		System.out.println("Could not find an attribute 'version' in the metaclass '"+this.NameS()+"'. Abort");
		System.exit(-1);
	}

	/*
	 * The xmlns prefix
	 */
	private String xmlnsPrefix = null;
	
    /**
     * Look for an attribute in tableattributes (i.e. static  attribute)
     * whose name is 'xmlns' and return its value , which is supposed to 
     * be the xmlns prefix for this table.
     * If the attribute is not found or if it does not have a value, then exit.
     * 
     */
    public void setXMLnsPrefix() {
   	 Iterator iter = ASDMAttributes().iterator();
   	 while (iter.hasNext()){
   		 ASDMAttribute a = (ASDMAttribute) iter.next();
   		 if (a.NameS().equals("xmlnsPrefix")) {
   			 if (a.hasInitValue()) {
   				 String prefix = a.InitValue();
   				 if (prefix.matches("[a-zA-Z]+")) {
   					 xmlnsPrefix = prefix;
   				 	return;
   				 }
   				 else 
   					 Checks.error(this, "'"+prefix+"'  is not a valid prefix for an xmlns definition (table '"+this.NameS()+"')");
   			 }
   			 else {
   				 Checks.error(this, "A static attribute 'xmlns' was found in the table '"+this.NameS()+"' but without an initial value; it must have one !");
   			 }
   		 }
   	 }
   	 Checks.error(this, "Could not find a static attribute 'xmlnsPrefix' in '"+this.NameS()+"')");
    } 	
    
	/**
	 * Return the version's value as a String
	 */
	public String version() {
		return version;
	}
	
	/*
	 * The CVS revision number
	 */
	private String cvsRevision;

	/*
	 * The CVS branch name
	 */
	private String cvsBranch;
	
	/*
	 * The XML ns prefix
	 */
	public String xmlnsPrefix() {
		return xmlnsPrefix;
	}
	
	/**
	 * The AlmaTables that belong to this container.
	 */
	private ElementSet table; 
	
	/**
	 * Create an AlmaTableContainer.
	 *
	 */
	public AlmaTableContainer() {
		cvsRevision = System.getProperty("alma.asdm.cvs.revision", "-1");
		cvsBranch = System.getProperty("alma.asdm.cvs.branch", "");
	}
	
	/**
	 * Set the tables that belong to this container.
	 * @param table
	 */
	public void setTable(ElementSet table) {
		this.table = table;
	}
	
	/**
	 * Return the tables thet belong to this container as an ElementSet.
	 * @return
	 */
	public ElementSet AlmaTable() {
		return table;
	}

	/**
	 * Generate the prefix for the package statement in the generated classes.
	 * @return The string that should be prefixed to the package statement.
	 */ 
	public String Path() {
		return "alma.asdm";
	}

	/**
	 * Generate the prefix for the import statement in the generated classes
	 * that imports the extended types.
	 * @return The string that should be prefixed to the package statement.
	 */ 
	public String ExtendedTypes() {
		return "alma.hla.runtime.asdm.types";
	}

	public String ExtendedIDLTypes() {
		return "alma.asdmIDLTypes";
	}

	/**
	 * Return the directory path that corresponds to the path statement.
	 * @return
	 */
	public String DirPath() {
		return Path().replace('.','/');
	}

	/**
	 * Return the ASDMAttributes that belong to this container as an ElementSet
	 * @return
	 */
	
	/**
	 * This member variable is a workaround for the problem that if the ASDMAttributes
	 * method is called more than once, all calls after the first one will
	 * include the attributes defined by the BasicType() method.
	 */
	
	private ElementSet asdmAttributes = null; 
	
	public ElementSet ASDMAttributes() {
		// Go through the Attributes and return all ASDMAttributes.
		if (asdmAttributes != null) return asdmAttributes;
		
		ElementSet x = new ElementSet ();
		Iterator iter = Attribute().iterator();
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof ASDMAttribute) {
				x.add(o);
			}
		}
		asdmAttributes = x;
		return x;
	}
	
	public ElementSet BasicType() {
			ElementSet x = new ElementSet ();
			BasicType[] type = BasicType.getBasicType();
			ASDMAttribute attr = null;
			for (int i = 0; i < type.length; ++i) {
				attr = new ASDMAttribute ();
				attr.setMetaEnvironment(this.getMetaEnvironment());
				attr.setName("dummy");
//				Name aname = new Name ();
//				aname.Name = "dummy";
				attr.Name = "dummy";
				PrimitiveType atype = new PrimitiveType ();
//				Identifier atypename = new Identifier ();
//				atypename.Name = type[i].getJavaName();
				atype.Name = type[i].getJavaName();;
				attr.setType(atype);
                attr.setClass(this);
				x.add(attr);
			}
		return x;
	}
	
	/**
	 * Returns the CVS revision number of the UML modelling file (ASDM.xml).
	 */
	public String CVSRevision()  {
		return cvsRevision;
	}
	
	/**
	 * Returns the CVS branch name of the UML modelling file (ASDM.xml).
	 */
	public String CVSBranch() {
		return cvsBranch;
	}
}
