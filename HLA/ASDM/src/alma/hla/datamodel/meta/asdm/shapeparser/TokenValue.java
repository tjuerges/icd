/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jun 15, 2005
 * File TokenValue.java
 */

package alma.hla.datamodel.meta.asdm.shapeparser;

/**
 * @author caillat
 *
 */
public class TokenValue {
    private final String name;
    
    private TokenValue(String name) {this.name = name;}
    
    public String toString() {return this.name;}
    
    public static final TokenValue END        = new TokenValue("END");
    public static final TokenValue NAME       = new TokenValue("NAME");
    public static final TokenValue NUMBER     = new TokenValue("NUMBER");
    public static final TokenValue PLUS       = new TokenValue("+");
    public static final TokenValue MINUS      = new TokenValue("-");
    public static final TokenValue TIMES      = new TokenValue("*");
    public static final TokenValue DIVIDED_BY = new TokenValue("/");
    public static final TokenValue LPAR       = new TokenValue("(");
    public static final TokenValue RPAR       = new TokenValue(")");
    public static final TokenValue LBRACK     = new TokenValue("[");
    public static final TokenValue RBRACK     = new TokenValue("]");    
}
