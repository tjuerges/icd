/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaLink.java
 */
package alma.hla.datamodel.meta.asdm;

//import de.bmiag.genfw.meta.classifier.Association;
import org.openarchitectureware.meta.uml.classifier.Association;
/**
 * description
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class AlmaLink extends Association {

	// Key
	public String Key = "";
	public void setKey(String Key) {
		this.Key = Key;
	}

	// Option -- syntax is "option=get|noextrinsic"
	/*public String Option = "";
	public void setOption(String Option) {
		this.Option = Option;
		if (Option.trim().length() == 0)
			return;
		System.out.println(">>>" + Option); System.out.flush();
		String[] s = Option.split("|");
		System.out.println(">>>" + s.length);
		for (int i = 0; i < s.length; ++i) {
			System.out.println(">>>" + s[i]); System.out.flush();
			if (s[i].trim().equals("get"))
				isGetOnly = true;
			else if (s[i].trim().equals("noextrinsic"))
				isNoExtrinsic = true;
			//else
			//	Checks.error(this,"Invalid opetion (" + Option + ") on alink.");
		}
	}*/

	public String Get = "";
	public void setGet(String Get) {
		this.Get = Get;
		isGetOnly = true;
	}
	private boolean isGetOnly = false;
	public boolean GetOnly() {
		return isGetOnly;
	}
	
	public String Noextrinsic = "";
	public void setNoextrinsic(String Noextrinsic) {
		this.Noextrinsic = Noextrinsic;
		isNoExtrinsic = true;
	}
	private boolean isNoExtrinsic = false;
	public boolean NoExtrinsic() {
		return isNoExtrinsic;
	}
    
    /**
     * The shape is a string giving the size of the extrinsic attribute 
     * when it's an array.
     */
    protected String Shape=null;
    public void setShape(String Shape){
        //System.out.println("AlmaLink shape ="+Shape);
        this.Shape = Shape;
    }
    
    public String getShape(){
        return Shape;
    }
}
