package alma.hla.datamodel.meta.asdm.shapeparser;

import java.io.IOException;
import java.io.PushbackInputStream;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.openarchitectureware.core.meta.core.ElementSet;

import alma.hla.datamodel.meta.asdm.ASDMAttribute;
import alma.hla.datamodel.meta.asdm.AlmaTable;

/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jun 15, 2005
 * File ShapeParser.java
 */

/**
 * The class ShapeParser provides a way to parse array shapes. It is designed as a singleton
 * and the actual parsing work is done by its method 'parse'.
 *
 * @author caillat
 * 
 */
public class ShapeParser {

    private PushbackInputStream pb = null;

    private StringBuffer numberString = new StringBuffer();

    private StringBuffer symbolName = new StringBuffer();

    private TokenValue currentToken = null;
    
    private AlmaTable almaTable = null;
    
    private ElementSet almaTables = null;

    /**
     * The ShapeParser is used as a singleton through the public static final field INSTANCE.
     */
    public static final ShapeParser INSTANCE = new ShapeParser();
    
    private ShapeParser() {;}
    
    /**
     * 
     * @param s
     *            a string containing the shape descrition to be parsed.
     * 
     * @return an array with its length equal to the number of dimensions and
     *         each element equal to the size of the corresponding dimension.
     *         Example : [numReceptors][2] should return an array with two
     *         elements "numReceptors" and "2". A size can be expressed as
     *         an arithmetic expression written in infixed notation and built
     *         upon integer constants and names of integer attributes 
     *         in the table t.
     */
    public String[] parse(String s, AlmaTable t, ElementSet allTables) throws Exception {
        if (t == null) throw new Exception("ShapeParser error : cannot parse against a null AlmaTable");        
        if ((s == null) || (s.length()==0)) throw new Exception("ShapeParser error : cannot parse a null or empty string");
 
        almaTable = t;
        almaTables = allTables;

        pb = new PushbackInputStream(new StringBufferInputStream(s));
        String[] result = null;
        result = shapes(); 
        return result;
    }
    
    /**
     * 
     * @param tables A set whose elements are all supposed to be AlmaTables.
     * @param name The name of the table that is looked for into tables.
     * @return
     */
    AlmaTable lookup(ElementSet tables, String name){
        Iterator iter = tables.iterator();
        AlmaTable result = null;
        while (iter.hasNext()) {
            result = (AlmaTable)iter.next();
            if (result.NameS().equals(name)) return result;
        }
        return null;
    }

    /**
     * @return a TokenValue depending on the recognized token.
     */
    TokenValue readToken() throws IOException {
        char chr = 0;
        do {
            chr = (char) pb.read();
            if ((short) chr == -1)
                return currentToken=TokenValue.END;
        } while (Character.isWhitespace(chr));

        //System.out.print(chr);
        switch (chr) {
        case '*':
            return currentToken = TokenValue.TIMES;
        case '/':
            return currentToken = TokenValue.DIVIDED_BY;
        case '+':
            return currentToken = TokenValue.PLUS;
        case '-':
            return currentToken = TokenValue.MINUS;
        case '(':
            return currentToken = TokenValue.LPAR;
        case ')':
            return currentToken = TokenValue.RPAR;
        case '[':
            return currentToken = TokenValue.LBRACK;
        case ']':
            return currentToken = TokenValue.RBRACK;

        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            numberString.delete(0, numberString.length());
            numberString.append(chr);

            while ((((int) (chr = (char) pb.read())) != -1)
                    && Character.isDigit(chr)) {
                //System.out.print(chr);
                numberString.append(chr);
            }
            pb.unread((int) chr);
            return currentToken = TokenValue.NUMBER;

        default:
            if (Character.isJavaIdentifierStart(chr)) {
                symbolName.delete(0, symbolName.length());
                symbolName.append(chr);
                while ((((int) (chr = (char) pb.read())) != -1)
                        && (Character.isJavaIdentifierPart(chr) ||
                                chr == '.')) {
                    //System.out.print(chr);
                    symbolName.append(chr);
                }
                pb.unread((int) chr);                
                return currentToken = TokenValue.NAME;
            }
        }
        return null;
    }

    String[] shapes() throws Exception {
        ArrayList<String> dummyArray = new ArrayList<String>();
        StringBuffer sb = null;
        readToken();
        while ((sb = shape()) != null) {
            dummyArray.add(sb.toString());
        }
        return dummyArray.toArray(new String[dummyArray.size()]);
    }
    
    StringBuffer shape() throws Exception {
        if (currentToken.equals(TokenValue.END)) return null;
        
        if (!currentToken.equals(TokenValue.LBRACK))
            throw new Exception("ShapeParser error: "
                    + TokenValue.LBRACK.toString() + " expected.");

        readToken();
        if (currentToken.equals(TokenValue.RBRACK)){
            readToken();
            return (new StringBuffer()).append("");
        }
            
        StringBuffer result = expression();

        if (!currentToken.equals(TokenValue.RBRACK))
            throw new Exception("ShapeParser error: "
                    + TokenValue.RBRACK.toString() + " expected.");

        readToken();
        return result;
    }

    StringBuffer expression() throws Exception {

        StringBuffer result = terme();

        for (;;) {
            //System.out.println("expression : result = "+result);
            if (currentToken.equals(TokenValue.PLUS)) {
                readToken();
                result.append(TokenValue.PLUS.toString()).append(terme());
            } else if (currentToken.equals(TokenValue.MINUS)) {
                readToken();
                result.append(TokenValue.MINUS.toString()).append(terme());
            } else  {
                return result;
            }
        }
    }

    StringBuffer terme() throws Exception {

        StringBuffer result = primary();
        for (;;) {
            if (currentToken.equals(TokenValue.TIMES)) {
                readToken();
                result.append(TokenValue.TIMES.toString()).append(primary());
            } else if (currentToken.equals(TokenValue.DIVIDED_BY)) {
                readToken();
                result.append(TokenValue.DIVIDED_BY.toString()).append(primary());
            } else
                return result;
        }
    }

    StringBuffer primary() throws Exception {
        AlmaTable at = almaTable;
    
        if (currentToken.equals(TokenValue.NUMBER)) {
            readToken();
            return new StringBuffer(numberString.toString());
        }
        else if (currentToken.equals(TokenValue.NAME)) {
            // Is there a dot into the symbol Name ?
            ASDMAttribute a = null;
            String[] symbolNameParts = symbolName.toString().split("\\.");
            
            if (symbolNameParts.length == 1) {
            // Check that this symbol is the name of an attribute in table t.
                a = at.getAttribute(symbolNameParts[0]);
            }
            else if (symbolNameParts.length == 2) {
                at = this.lookup(almaTables, symbolNameParts[0]);
                if (at == null)
                    throw new Exception ("ShapeParser : no table " + symbolNameParts[0] + " in the model.");
                a = at.getAttribute(symbolNameParts[1]);
            }
            if (a == null)
                throw new Exception("ShapeParser : no attribute "+symbolName.toString()+" in the table "+at.NameS());
            
            // Check that this attribute is an integer scalar.
            if (a.isArray() || !a.JavaType().equals("int"))
                throw new Exception("ShapeParser : only integer scalar attribute can appear in a size. " + a + "is a "+a.JavaType()+"!");                
            readToken();
            return new StringBuffer(symbolName.toString());
        }
        else if (currentToken.equals(TokenValue.MINUS)) {
            readToken();
            return (new StringBuffer()).append("-").append(primary());
        }
        else if (currentToken.equals(TokenValue.LPAR)) {
            readToken();
            StringBuffer result = expression();
 
            if (!currentToken.equals(TokenValue.RPAR)) throw new Exception("ShapeParser : "+TokenValue.RPAR.toString()+" expected.");
            readToken();
            return (new StringBuffer())
            .append(TokenValue.LPAR.toString())
            .append(result)
            .append(TokenValue.RPAR.toString());          
        }
        else
            throw new Exception ("Syntax error while parsing a shape definition. (current token was : "+currentToken.toString()+")");
    }
}
