package alma.hla.datamodel.meta.enumeration;

import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;
import org.openarchitectureware.core.constraint.Checks;

public class EEnumerationLiteral extends EnumerationLiteral {
	private String name = null;
	private String value = null;
	private boolean isInt_ = false;
	private boolean isString_ = !isInt_;
	
	private static boolean isJavaIdentifier(String s) {
        if (s.length() == 0 || !Character.isJavaIdentifierStart(s.charAt(0))) {
            return false;
        }
        for (int i=1; i<s.length(); i++) {
            if (!Character.isJavaIdentifierPart(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
	
	protected void initialize() {
		/*
		 * If the litteral does not contain an '=' sign, then it must respect the syntax of a Java and/or C++ identifier,
		 * then it will be considered as a String litteral.
		 */
		if (this.NameS().indexOf('=') == -1) {
			if( !EEnumerationLiteral.isJavaIdentifier(this.NameS())) {
				Checks.error(this, "This litteral '" + this.NameS() +"' does not hace the syntax of an identifier");
			}
			name = this.NameS();
			value = this.NameS();
			isString_ = true;
			isInt_ = false;
		}
		else {
			String nameValue[] = this.NameS().split("=");
			name = nameValue[0].trim();
			if(!EEnumerationLiteral.isJavaIdentifier(name)) {
				Checks.error(this, "The name part of this litteral '" + name + "' does not have the syntax of an identifier" );
			}
			value = nameValue[1].trim();
			if (value.length() == 0) {
				Checks.error(this, "The value part of this literal '" + name + "' cannot be empty"); 
			}
			
			try {
				Integer.decode(value);
				isInt_  = true;
				isString_ = false;
			} catch (NumberFormatException e) {
				isInt_ = false;
				isString_ = false;
			}
		}
	}
	
	public boolean isInt() { return isInt_; }	
	public boolean isString() { return isString_; }
	
	public String getName() { return name; }
	public String getValue() { return value; }
}
