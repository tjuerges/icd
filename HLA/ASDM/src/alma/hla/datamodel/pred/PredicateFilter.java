/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.pred;

import java.util.Iterator;

import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.ModelElement;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PredicateFilter
{
	
	private ElementSet set;
	
	public PredicateFilter( ElementSet set ) {
		this.set = set;
	}

	public ElementSet filterBy( Predicate pred ) {
		ElementSet res = new ElementSet();
		Iterator it = set.iterator();
		while (it.hasNext()) {
			ModelElement element = (ModelElement) it.next();
			if ( pred.filter( element )) res.add( pred.mapElement(element) );
		}
		return res;
	}

}
