#!/usr/bin/env python
#
# split long lines in f90 code.
#
import sys
from string import *

#if len(sys.argv) >1:
#    input = open(sys.argv[1])

maxline = 120

#lines = input.readlines()
lines = sys.stdin.readlines()
for line in lines:
    line = line.replace('//','',1)
    line = line.replace('//','  ')
    if (len(line.rstrip())==0):
        pass
    elif (len(line) >= maxline):
        outline = ' '
        m = line.split()
        indent = find(line,m[0])
        outline = line[0:indent-1]
        for word in m:
            if len(outline)+len(word)+1 >= maxline:
                print outline+'&'
                outline = line[0:indent-1]+'     & '+word
            else:
                outline = outline+' '+word
        print outline
    else:
        print line.rstrip()
        
    
