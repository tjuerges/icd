#include <iostream>
#include <string>
#include <ASDMAll.h>
#include "SDMBinaryExceptions.h"
#include "SDMBinaryBlock.h"
using namespace asdmBinary;
#include "SDMBinaryData.h"
using namespace asdm;

int main (int argc, char* argv[]) {

  if (argc<3) {
    cerr << "Usage : testReadBin ASDMBinaryRootDir dataOID [EB_Directory]" << endl;
    return -1;
  }

  string asdmDir;
  //ASDM*  datasetPtr    = 0;
  ASDM   dataset;
  string binaryRootDir = string(argv[1]);
  string dataOID       = string(argv[2]);
  if(argc==4){
    asdmDir = string(argv[3]);
    try{
      dataset.setFromFile(asdmDir);
    }
    catch (ConversionException e) {
      cerr << "ERROR " << e.getMessage() << endl;
      return -1; 
    }
    catch (...) {
      cerr << "Unexpected exception " << endl;
      return -1;
    }    
  }else{
    try{
      dataset.setFromFile(binaryRootDir);
      asdmDir=binaryRootDir;
    }
    catch (ConversionException e) {
      cerr << "ERROR " << e.getMessage() << endl;
      return -1; 
    }
    catch (...) {
      cerr << "no sdm dataset in directory " << binaryRootDir
	   << ". ==> no sdm identification" << endl;
      asdmDir="";
    }    
  }
  if(asdmDir.length())cout<<"The data can be tagged with the SDM identifiers. "<<endl;

 
  
  SDMBinaryData* sdmBinaryDataPtr=0;
  try{
    sdmBinaryDataPtr = new SDMBinaryData(binaryRootDir,dataOID);
  }
  catch (ConversionException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }
  catch (FromFileException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }  
  catch (...) {
    cerr << "Unexpected exception " << endl;
    return -1;
  }

  cout << "Ready to get the data tree(s)" << endl;

  /*

  if(sdmBinaryDataPtr->correlationMode()!=1){              cout<<"Get the data tree for the cross correlations"<<endl;
    vector<vector<vector<vector<vector<vector<float> > > > > > 
      crossData_Tree = sdmBinaryDataPtr->crossDataTree();

    if(datasetPtr){                                        cout<<"Connect the SDM root identifiers to these data"<<endl;
      vector<vector<vector<vector<int> > > >
	crossId_Tree = sdmBinaryDataPtr->crossIdTree( datasetPtr, dataOID );
    }
  }
  if(sdmBinaryDataPtr->correlationMode()>0){               cout<<"Get the data tree for the auto correlations"<<endl;
    vector<vector<vector<vector<vector<float> > > > >
      autoData_Tree = sdmBinaryDataPtr->autoDataTree();
    if(datasetPtr){                                        cout<<"Connect the SDM root identifiers to these data"<<endl;
      vector<vector<vector<int> > >
	autoId_Tree = sdmBinaryDataPtr->autoIdTree( datasetPtr, dataOID );
    }
  }

  */
  
  delete sdmBinaryDataPtr;

  cout << "Successful test completion" << endl;

  return 0;
}
