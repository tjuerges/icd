/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestWriteXML.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <TransitionTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <TransitionRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

#include <Frequency.h>

using namespace asdm;

	int main(char *arg) {
		ASDM dataset;
		
		/*
		Entity ee;
		ee.setEntityId(*(new EntityId("uid://X0000000000000101/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("ASDM");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		dataset.setEntity(ee);
		dataset.setTimeOfCreation(*(new ArrayTime(2004,12,22,14,14,0.0)));
		*/

		SpectralWindowTable &spectralWindow = dataset.getSpectralWindow();
		// Build the Spectral Window Table
		int numChan = 16;
		vector<Frequency> *chanFreqPtr    = 0;
		vector<Frequency> *chanWidthPtr   = 0;
		vector<Frequency> *effectiveBwPtr = 0;
		vector<Frequency> *resolutionPtr  = 0;
		vector<string>    *assocNaturePtr = 0;
		
		// This table will contain nrows rows. 
		int nrows = 4;
		
		
		for (int j = 0; j < nrows; j++) {
			// Instantiate vectors for some of the table attributes.
			chanFreqPtr    = new vector <Frequency> ();
			chanWidthPtr   = new vector <Frequency> ();
			effectiveBwPtr = new vector <Frequency> ();
			resolutionPtr  = new vector <Frequency> ();
			assocNaturePtr = new vector <string> ();
				
			// Populate these vectors with completely dummy values.	
			for (int i = 0; i < numChan; i++) {
				chanFreqPtr->push_back(*(new Frequency(1000. * i + j)));
				chanWidthPtr->push_back(*(new Frequency(100. + j)));
				effectiveBwPtr->push_back(*(new Frequency(500.+1./(j+i+1.))));
				resolutionPtr->push_back(*(new Frequency(60.+j)));
			}
			
			for (int i = 0; i < 3; i++) {
				char* assocNatureChr = new char[40];
				sprintf(assocNatureChr, "assocNature_%d_%d", i, j);
				assocNaturePtr->push_back(string(assocNatureChr));		
			}
			
			string* name = new string("Spectral Window #");
			char dumstr[3];
			sprintf(dumstr, "%d", j);
			name->append(dumstr);
			
			char freqGroupName[40];
			sprintf(freqGroupName, "Frequency group name  %d", j );
			 
			Frequency refFreq(2000.0*j);
			Frequency totBandWidth(2220.0*j);

			SpectralWindowRow* aRow = spectralWindow.newRow(numChan,
									*name,
									refFreq,
									*chanFreqPtr,
									*chanWidthPtr,
									2200*j,
									*effectiveBwPtr,
									*resolutionPtr,
									totBandWidth,
									1100*j,
									3300*j,
									j,
									string(freqGroupName),
									*assocNaturePtr,
									false);
			
			// Add the row to the SpectralWindow table using this Identifier.					
			SpectralWindowRow* retRow = spectralWindow.add(aRow);
			
					
			// Check the Identifier automatically assigned to this added row
			cout << "Last added SpectralWindowRow was assigned id " << 
				retRow->getSpectralWindowId().toString() << endl;
		}
		
		// Define a entity for the SpectralWindow table.		
		/*
		ee.setEntityId(*(new EntityId("uid://X0000000000000079/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("SpectralWindowTable");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		spectralWindow.setEntity(ee);	
		*/
		
		 // Build a polarization table with two rows.
		 PolarizationTable &polarization = dataset.getPolarization();	

		
		 vector<int>          *corrTypePtr = 0;
		 vector<vector<int> > *corrProductPtr = 0;
		 vector<int>          *tmpVecPtr = 0;
		 
		 for (int i = 0; i < 2; i++) {
		 	
		 	int numCorr=i+1;
		 	
		 	// Instantiate and populate a vector for the corrType attribute
		 	corrTypePtr = new vector<int>();
		 	for (int j = 0; j < numCorr; j++)
		 		corrTypePtr->push_back(i);
		 		
		 		
		 	// Instantiate and populate a vector for the corrProduct attribute
		 	// Given that corrProduct is a vector of vector...this is
		 	// a two step process.	
		 	corrProductPtr = new vector<vector<int> >();
		 	
		 	// 1st vector<int> of corrProduct
		 	tmpVecPtr = new vector<int>;
		 	for (int j = 0; j < numCorr; j++)
		 		tmpVecPtr->push_back(i+j);
		 	corrProductPtr->push_back(*tmpVecPtr);
		 	
		 	// 2nd vector<int> of corrProduct 
		 	tmpVecPtr = new vector<int>;
		 	for (int j = 0; j < numCorr; j++)
		 		tmpVecPtr->push_back(i+j);
		 	corrProductPtr->push_back(*tmpVecPtr);
		 
		 	// Create a new Polarization row.
		 	PolarizationRow* aRow = polarization.newRow(numCorr,
		 	                                            *corrTypePtr,
		 	                                            *corrProductPtr,
		 	                                            false);	
			
			// Add the newly created row to the Polarization table							
			PolarizationRow* retRow = polarization.add(aRow);
			
			// Check the Identifier automatically assigned to this added row
			cout << "Last added PolarizationRow was assigned id " << 
				retRow->getPolarizationId().toString() << endl;                                            	
		 }
		 	

		// Define an entity for the Polarization table.
		 /*
		ee.setEntityId(*(new EntityId("uid://X0000000000000080/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("PolarizationTable");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		polarization.setEntity(ee);	
		 */
		//
		// Build a DataDescription table with three rows. 
		// 
		DataDescriptionTable &dataDescription = dataset.getDataDescription();
		
		DataDescriptionRow* aDDRow = dataDescription.newRow(false,
								    spectralWindow.get(0)->getSpectralWindowId(),
								    polarization.get(0)->getPolarizationId());

		DataDescriptionRow* retRow = dataDescription.add(aDDRow); 

				         		
		aDDRow = dataDescription.newRow(false,
						spectralWindow.get(0)->getSpectralWindowId(),
						polarization.get(1)->getPolarizationId());
		retRow = dataDescription.add(aDDRow); 

						
		aDDRow = dataDescription.newRow(false,
						spectralWindow.get(2)->getSpectralWindowId(),
						polarization.get(0)->getPolarizationId());
		retRow = dataDescription.add(aDDRow); 


		// Define an entity for the Polarization table.
		/*
		ee.setEntityId(*(new EntityId("uid://X0000000000000081/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("DataDescriptionTable");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		dataDescription.setEntity(ee);	 
		*/
		// Write out the dataset.
		try {
			char testdir[40];
			srand((unsigned) clock());
			sprintf(testdir,"asdm_%d", rand());
			dataset.toXML(string(testdir));
		} catch (ConversionException e) {
			cout << "Error! " << e.getMessage() << endl;
		}
		return 0;	
	}
