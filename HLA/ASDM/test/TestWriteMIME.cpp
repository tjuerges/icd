#include "SDMDataHeaderLexer.hpp"
#include "SDMDataHeaderParser.hpp"
#include "SDMBinaryBlock.h"
#include "SDMBinaryExceptions.h"
#include <exception>
#include <mimetic/mimetic.h>
#include <iostream>
#include <sstream>
#include <string>

#include <stdio.h>

using namespace asdmBinary;
using namespace std;

int main ( int argc, char * argv[] ) {
  ANTLR_USING_NAMESPACE(std) ;
  ANTLR_USING_NAMESPACE(antlr) ;
  ANTLR_USING_NAMESPACE(asdmBinary) ;

  if (argc < 4) {
  	cout << "Usage : testWriteMIME xmlfile numAntenna directory" << endl;
  	exit(-1);
  }
  
  SDMBinaryBlock sdmbbOut; 
  SDMDataHeader  sdmdh; 

  cout << "----> About to parse " << argv[1] << endl;
  std::ifstream inXMLFile(argv[1]);

  int numAntenna = atoi(argv[2]); 
  cout << "----> and to produce data for " << numAntenna << " antennas " ;

  string baseDir(argv[3]);
  cout << " into the directory " << baseDir << endl;
  
  // Get an SDMDataHeader from an XML file read on standard input.
  try {
    SDMDataHeaderLexer lexer(inXMLFile);
    SDMDataHeaderParser parser(lexer);
    parser.document();
    
    sdmdh = parser.getSDMDataHeader();
  }
  catch ( SemanticException& e ) {
    cerr << "exception: " << e.getMessage() << endl;
    return -1;		
  }
  catch( ANTLRException& e )
    {
      cerr << "exception: " << e.getMessage() << endl;
      return -1;
    }
  catch(exception& e)
    {
      cerr << "exception: " << e.what() << endl;
      return -1;
    }
   
  //
  // Now let's build a new SDMDataHeader reusing some parts of the SDMDataHeader read above.
  //
  string dataOID = SDMBinaryBlock::dummyUID();
  cout << "----> Creating a new SDMDataHeader reusing the one just read  with "
       << numAntenna
       << " antennas (scheduled and used) and dataOID ="
       << dataOID
       << endl;

  SDMDataHeader sdmdhOut = SDMDataHeader(
  		   dataOID, 
		   sdmdh.getExecBlockId(), 
		   sdmdh.getTime(),
		   sdmdh.getScanNum(),
		   sdmdh.getSubscanNum(), 
		   sdmdh.getIntegrationNum(),
		   sdmdh.getSubintegrationNumExists(), 
		   sdmdh.getSubintegrationNum(),
		   numAntenna,
		   numAntenna,
		   sdmdh.getCorrelationMode());
  
  cout << "----> Here are the sizes of the attachments corresponding to this new SDMDataHeader " << endl;
  cout << "ActualTimes size = " << sdmdhOut.getActualTimes().getSize() << endl;
  cout << "ActualDurations size = " << sdmdhOut.getActualDurations().getSize() << endl;
  cout << "ZeroLagss size = " << sdmdhOut.getZeroLags().getSize() << endl;
  cout << "BaseLine flags = " <<  sdmdhOut.getBaselineFlags().getSize() << endl;
   
  switch (sdmdhOut.getCorrelationMode()->getCode()) {
  	case CorrelationMode::CORRELATIONMODE0 :
  		cout << "CrossData size = " << ((CorrelationMode0 *)sdmdhOut.getCorrelationMode())->getCrossData()->getSize() << endl;
  		break;
   	case CorrelationMode::CORRELATIONMODE1 :
   	  	cout << "AutoData size = " << ((CorrelationMode1 *)sdmdhOut.getCorrelationMode())->getAutoData().getSize() << endl;
   	  	break;
   	case CorrelationMode::CORRELATIONMODE2 : 
 		cout << "CrossData size = " << ((CorrelationMode2 *)sdmdhOut.getCorrelationMode())->getCrossData()->getSize() << endl;
 		cout << "AutoData size = " << ((CorrelationMode2 *)sdmdhOut.getCorrelationMode())->getAutoData().getSize() << endl;
 		break;
  }
  
  cout << "ActualTimes size = " << sdmdhOut.getActualTimes().getSize() << endl; 
  cout << "----> Now creating an ASDMBinary block out of this SDMDataHeader and some fake binary data." << endl; 
  
  //
  // Get memory for some fake binary data.
  // 
  long long int * actualTimes     = new long long int [sdmdhOut.getActualTimes().getSize()];
  cout << "ActualTimes : allocated space for " << sdmdhOut.getActualTimes().getSize() << " " << ActualTimes::TYPENAME << endl;
  for (unsigned int i = 0; i < sdmdhOut.getActualTimes().getSize(); i++)
  	actualTimes[i] = i;
  	
  long long int * actualDurations = new long long int [sdmdhOut.getActualDurations().getSize()];
  cout << "ActualDurations : allocated space for " << sdmdhOut.getActualDurations().getSize() << " " << ActualDurations::TYPENAME << endl;
  for (unsigned int i = 0; i < sdmdhOut.getActualDurations().getSize(); i++)
  	actualDurations[i] = i;
  	
  float * zeroLags = new float[sdmdhOut.getZeroLags().getSize()];
  cout << "ZeroLags : allocated space for " << sdmdhOut.getZeroLags().getSize() << " " << ZeroLags::TYPENAME << endl;
  for (unsigned int i = 0; i < sdmdhOut.getZeroLags().getSize(); i++)
  	zeroLags[i] = (float )i;
  	
  unsigned long int * baselineFlags = new unsigned long int[sdmdhOut.getBaselineFlags().getSize()];
  cout << "BaseLineFlags : allocated space for " << sdmdhOut.getBaselineFlags().getSize() << " " << BaselineFlags::TYPENAME << endl;
  for (unsigned int i = 0; i < sdmdhOut.getBaselineFlags().getSize(); i++)
  	baselineFlags[i] = i;
  	
  short int * crossDataShort;
  long int * crossDataLong;
  float * autoData;
  void * ptr;

  CorrelationMode* cm = sdmdhOut.getCorrelationMode();
  CorrelationMode0 * cm0 = 0;
  CorrelationMode1 * cm1 = 0;
  CorrelationMode2 * cm2 = 0;

  string uid = SDMBinaryBlock::dummyUID();	 
  if ((cm0 = cm->isCorrelationMode0())) {
  	CrossDataLong  * cdl = 0;
  	CrossDataShort * cds = 0;
    if ((cdl = cm0->getCrossData()->isCrossDataLong())) {
      ptr =  (void *) (crossDataLong = new long int[cdl->getSize()]);
      cout << "CrossDataLong : allocated space for " << cdl->getSize() << " " << CrossDataLong::TYPENAME << endl; 
      for (unsigned int i = 0; i < cdl->getSize(); i++) crossDataLong[i] = i % 100;     
    }
    else if ((cds = cm0->getCrossData()->isCrossDataShort())) {
      ptr =  (void *) (crossDataShort = new short int[cds->getSize()]);
      cout << "CrossDataShort : allocated space for " << cds->getSize() <<  " " << CrossDataShort::TYPENAME << endl;      
      for (unsigned int i = 0; i < cds->getSize(); i++) crossDataShort[i] = i % 100;  
    }
    else {
      cout << "I could not determine the type of CrossData." << endl;
      exit (-1);
    }

    sdmbbOut = SDMBinaryBlock(
    			sdmdhOut,
			&actualTimes[0],
			&actualDurations[0],
			&zeroLags[0],
			&baselineFlags[0],
			ptr);
  } 
  else if ( (cm1 = cm->isCorrelationMode1()) ) { 
    autoData =  new float[cm1->getAutoData().getSize()];
    cout << "AutoData : allocated space for " << cm1->getAutoData().getSize() << " " << AutoData::TYPENAME << endl;
    for (unsigned int i = 0; i < cm1->getAutoData().getSize(); i++) autoData[i] = (float )i;          
    sdmbbOut = SDMBinaryBlock(
               sdmdhOut,
	       &actualTimes[0],
	       &actualDurations[0],
	       &zeroLags[0],
	       &baselineFlags[0],
	       (void *) &autoData[0]);
  }
  else if ( (cm2 = cm->isCorrelationMode2()) ) {
   CrossDataLong  * cdl = 0;
   CrossDataShort * cds = 0;
   if ( (cdl = cm2->getCrossData()->isCrossDataLong()) ) {
     ptr =  (void *) (crossDataLong = new long int[cdl->getSize()]);
     cout << "CrossDataLong : allocated space for " << cdl->getSize() << " " << CrossDataLong::TYPENAME << endl; 
     for (unsigned int i = 0; i < cdl->getSize(); i++) crossDataLong[i] = i % 100;     
   }
   else if ( (cds = cm2->getCrossData()->isCrossDataShort()) ) {
   	 ptr =  (void *) (crossDataShort = new short int[cds->getSize()]);
     cout << "CrossDataShort : allocated space for " << cds->getSize() << " " << CrossDataShort::TYPENAME << endl;  
     for (unsigned int i = 0; i < cds->getSize(); i++) crossDataShort[i] = i % 100;  
   }
   else {
     cout << "I could not determine the type of CrossData." << endl;
     exit (-1);
   }
   
   autoData =  new float[cm2->getAutoData().getSize()];
   for (unsigned int i = 0; i < cm2->getAutoData().getSize(); i++) autoData[i] = (float )i;   
   cout << "AutoData : allocated space for " << cm2->getAutoData().getSize() << " " << AutoData::TYPENAME << endl;   
   sdmbbOut = SDMBinaryBlock(
              sdmdhOut,
	      &actualTimes[0],
	      &actualDurations[0],
	      &zeroLags[0],
	      &baselineFlags[0],
	      ptr,
	      autoData);
  }
  else {
     cout << "I could not determine the type of CorrelationMode." << endl;
     exit (-1);
  }
  
  // Now save sdmbbOut in a file.
  // The SDMBinaryBlock will be saved in /tmp/ASDMBinary.

  try {
    sdmbbOut.toFile(baseDir);
    cout << "The SDMBinaryBlock has been exported in " << SDMBinaryBlock::uid2FullFilename(baseDir, uid) << endl; 
  }
  catch(ToFileException& e) {
    cerr << e.getMessage() << endl;
  }
  catch(exception& e) {
    cerr << "exception: " << e.what() << endl;
    return -1;
  } 
 }
