#include <ArrayTime.h>
#include <Interval.h>
#include <ArrayTimeInterval.h>
#include <time.h>

int main (int argc, char* argv[]) {

  using asdm::ArrayTimeInterval;

 
  // Test the overlapping
  // yesterday noon + 12 and today 0 + 12 should overlap
  ArrayTimeInterval ait1(ArrayTime(2005, 4, 26, 12, 0, 0).getMJD(),
			 0.5);

  cout << "ait1 = (start=" << ait1.getStart() << ",end=" << ait1.getStart() + ait1.getDuration() << ")" << endl; 


  // Test the overlapping
  // yesterday noon + 12h and today 0 + 12 should overlap
  ArrayTimeInterval ait0(ArrayTime(2005, 4, 27, 0, 0, 0).getMJD(),
			 0.5);
  cout << "ait0 = (start=" << ait0.getStart() << ",end=" << ait0.getStart() + ait0.getDuration() << ")" << endl; 
  

  if (ait0.overlaps(ait1)) 
    cout << ait0 << " overlaps " << ait1 << endl;
  else 
    cout <<  ait0 << " does not overlap " << ait1 << endl;


  // Yesterday noon + 1/4 of day and today 0h + 12h should not overlap.
  ArrayTimeInterval ait2(ArrayTime(2005, 4, 26, 12, 0, 0).getMJD(),
			 0.25);
  cout << "ait2 = (start=" << ait2.getStart() << ",end=" << ait2.getStart() + ait2.getDuration() << ")" << endl; 

  if (ait0.overlaps(ait2)) 
    cout << ait0 << " overlaps " << ait2 << endl;
  else 
    cout <<  ait0 << " does not overlap " << ait2 << endl;

  // Today at 4 AM is in ait0
  ArrayTime thisMorning(2005, 4, 27, 4, 0, 0);
  if (ait0.contains(thisMorning))
    cout << "Time ="
	 << thisMorning.get()
	 << " is in "
	 << ait0 << endl;
  else
    cout << "Time ="
	 << thisMorning.get()
	 << " is not in "
	 << ait0 << endl;

  // Today at 2 PM is not in ait0
  ArrayTime thisAfternoon(2005, 4, 27, 14, 0, 0);
  if (ait0.contains(thisAfternoon))
    cout << "Time ="
	 << thisAfternoon.get()
	 << " is in "
	 << ait0 << endl;
  else
    cout << "Time ="
	 << thisAfternoon.get()
	 << " is not in "
	 << ait0 << endl;
}
