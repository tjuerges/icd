/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestFeed.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDMAll.h>

using namespace asdm;
int main(char *arg) {
  ASDM dataset;
  
  ReceiverTable&  receiver = dataset.getReceiver();

  // Prepare one dummy receiver
  ArrayTimeInterval timeInterval(10.0);
  Tag spectralWindowId(0);
  int               numLO         = 2;
  string            receiverName  = "RX_03_94";
  string            dewarName     = "DEWAR15";
  string            receiverBand  = "3";
  double            freqLO[2]; freqLO[0]=456789000000.000000;  freqLO[1]=-4000000000.000000;
  vector<Frequency> v_freqLO; v_freqLO.push_back(Frequency(freqLO[0])); v_freqLO.push_back(Frequency(freqLO[1]));
  vector<int>       v_sidebandLO; v_sidebandLO.push_back(1); v_sidebandLO.push_back(2);
  Temperature       dewarTemperature = Temperature(10.);
  double            stability     = 1.E-4;    
  long long         stabDuration  = 123456789;
  bool              stabilityFlag = false;
  int               receiverId;
  ReceiverRow * retReceiverRow = 0;

  ReceiverRow * receiverRow = receiver.newRow( spectralWindowId,				    
					       timeInterval,
					       numLO, 
					       receiverName,
					       receiverBand, 
					       v_freqLO,
					       v_sidebandLO,
					       dewarTemperature, 
					       Interval(stabDuration),
					       stability,
					       stabilityFlag
					       );
  retReceiverRow = receiver.add(receiverRow);
  receiverId = receiverRow->getReceiverId();
  if(retReceiverRow==receiverRow)
    cout<<"New row added with receiverId="<<receiverId<<endl;
  else
    cout<<"Row already present with same value using receiverId="<<receiverId<<endl;


  receiverName  = "RX_03_11"; 
  receiverRow = receiver.newRow( spectralWindowId,				    
				 timeInterval,
				 numLO, 
				 receiverName,
				 receiverBand, 
				 v_freqLO,
				 v_sidebandLO,
				 dewarTemperature, 
				 Interval(stabDuration),
				 stability,
				 stabilityFlag
				 );
  retReceiverRow = receiver.add(receiverRow);
  receiverId = receiverRow->getReceiverId();
  if(retReceiverRow==receiverRow)
    cout<<"New row added with receiverId="<<receiverId<<endl;
  else
    cout<<"Row already present with same value using receiverId="<<receiverId<<endl;

  receiverName  = "RX_03_94"; 
  receiverRow = receiver.newRow( spectralWindowId,				    
				 timeInterval,
				 numLO, 
				 receiverName,
				 receiverBand, 
				 v_freqLO,
				 v_sidebandLO,
				 dewarTemperature, 
				 Interval(stabDuration),
				 stability,
				 stabilityFlag
				 );
  retReceiverRow = receiver.add(receiverRow);
  receiverId = receiverRow->getReceiverId();
  if(retReceiverRow==receiverRow)
    cout<<"New row added with receiverId="<<receiverId<<endl;
  else
    cout<<"Row already present with same value using receiverId="<<receiverId<<endl;

  // Write out the dataset in XML documents.
  try {
    char testdir[40];
    time_t t;
    srand((unsigned) time(&t));
    sprintf(testdir,"/tmp/asdm_%d", rand());
    dataset.toXML(string(testdir));
    cout << "The dataset has been saved in the directory " << testdir << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  	

}
