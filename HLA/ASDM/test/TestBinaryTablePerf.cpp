/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestBinaryTablePerf.cpp
 */
 using namespace std;

#include <ASDMAll.h>
using namespace asdm;

#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/util/XMLUni.hpp>	

#include <ctype.h>	

using namespace xercesc;

#define BLOCKSIZE 1024

static const char*  gMemBufId = "dummyInfo";

void xmlIze(ASDM& dataset) {
	
	XMLPlatformUtils::Initialize();
	
	XercesDOMParser *parser = new XercesDOMParser;
	DOMImplementation* implementation = DOMImplementation::getImplementation();
	
	DOMWriter         *theSerializer = ((DOMImplementationLS*)implementation)->createDOMWriter();
	theSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);
	XMLFormatTarget *myFormTarget = new StdOutFormatTarget();
	DOMNode                     *document = 0;
	
	// The container.
	string s_dataset = dataset.toXML();
	MemBufInputSource * memBufIS = new MemBufInputSource (
        (const XMLByte*)s_dataset.c_str(),
        strlen(s_dataset.c_str()),
        gMemBufId,
        false
    );
    parser->parse(*memBufIS);	
	document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);

	// The DataDescriptionTable
	string s_ddT = dataset.getDataDescription().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_ddT.c_str(),
        strlen(s_ddT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The SpectralWindowTable
	string s_spwT = dataset.getSpectralWindow().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_spwT.c_str(),
        strlen(s_spwT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The HolographyTable
	string s_holoT = dataset.getHolography().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_holoT.c_str(),
        strlen(s_holoT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	// The PolarizationTable
	string s_polT = dataset.getPolarization().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_polT.c_str(),
        strlen(s_polT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	delete theSerializer;
    delete myFormTarget;
    delete parser;
    delete memBufIS;
	XMLPlatformUtils::Terminate();		
}

void buildASDM(ASDM& dataset, int numBlock) {
	
		// Populate its Holography table with one row
		HolographyTable& holoT = dataset.getHolography();
		HolographyRow* holoR = 0;

		vector<string> typeHolo;
		typeHolo.push_back ("SS");
		typeHolo.push_back("RR");
		typeHolo.push_back("QQ");
		typeHolo.push_back("SR");
		typeHolo.push_back("SQ");
		typeHolo.push_back("QR");
		Length distance(1000.0);
		Length focus(10.0);
		bool flagRow = false;
		
		holoR = holoT.newRow(typeHolo.size(), typeHolo, distance, focus, flagRow);
		holoT.add(holoR);
		
		cout << "The dataset has " << holoT.size() << " rows in its Holography table" << endl;
		
		// Populate its Polarization table with one row
		PolarizationTable& polT = dataset.getPolarization();
		PolarizationRow* polR = 0;

		vector<int> typePol;
		typePol.push_back(9);
		typePol.push_back(10);
		typePol.push_back(11);
		typePol.push_back(12);
		
		vector<vector<int> > productPol;
		vector<int> aux;
		aux.push_back(0); aux.push_back(0);
		productPol.push_back(aux);
		aux.clear();
		
		aux.push_back(0); aux.push_back(1);
		productPol.push_back(aux);
		aux.clear();
		
		aux.push_back(1); aux.push_back(0);
		productPol.push_back(aux);
		aux.clear();	
		
		aux.push_back(1); aux.push_back(1);
		productPol.push_back(aux);
		aux.clear();	
		
		polR = polT.newRow(typePol.size(), typePol, productPol);
		polT.add(polR);
		cout << "The dataset has " << polT.size() << " rows in its Polarization table" << endl;	
		
		// Populate its SpectralWindow table with one row
		SpectralWindowTable& spwT = dataset.getSpectralWindow();
		SpectralWindowRow* spwR = 0;

		int numChan = 16;
		Frequency refFreq(1.0e9);
		vector<Frequency> chanFreq; 
		vector<Frequency> chanWidth;
		for (int i = 0; i < numChan; i++) {
			chanFreq.push_back(Frequency(1.0e9 + i*1.0e8));
			chanWidth.push_back(Frequency(1.0e8));
		}

		vector<Frequency> effectiveBw;
		vector<Frequency> resolution;
		for (int i = 0; i < numChan; i++) {
			effectiveBw.push_back(Frequency(1.0e9 + i*2.0e8));
			resolution.push_back(Frequency(2.0e8));
		}

		Frequency totBandwidth(numChan * 1.0e8);
		int netSideband = 1;
		flagRow = false;

		spwR = spwT.newRow(numChan, refFreq, chanFreq, chanWidth,  effectiveBw, resolution, totBandwidth, netSideband,   flagRow);
		spwT.add(spwR);
		cout << "The dataset has " << spwT.size() << " rows in its SpectralWindow table" << endl;	
		
		// Populate its DataDescription table
		DataDescriptionTable& ddT = dataset.getDataDescription();
		DataDescriptionRow* ddR = 0;
		
		// Here is an association to an  HolographyRow and a SpectralWindowRow
		cout << "Adding a row with a link to the Holography table to the DataDescription table." << endl;
		ddR = ddT.newRow(holoR->getHolographyId(), spwR->getSpectralWindowId());
		ddT.add(ddR);
		cout << "Here is its holographyId :" << ddR->getPolOrHoloId().toString() << endl;				
		
		// Here is an association  to a PolarizationRow and the same SpectralWindowRow
		cout << "Adding a row with a link to the Polarization table to the DataDescription table." << endl;
		ddR = ddT.newRow(polR->getPolarizationId(), spwR->getSpectralWindowId());
		ddT.add(ddR);
		cout << "Here is its polarizationId :" << ddR->getPolOrHoloId().toString() << endl;
		
		cout << "The dataset has " << ddT.size() << " rows in its DataDescription table" << endl;
		vector<DataDescriptionRow *> rows = ddT.get();
		for (unsigned int i = 0; i < ddT.size(); i++) {
			cout << "The DataDescription row #" << i << " is associated to a "<< rows[i]->getPolOrHoloId().getTagType()->toString() << endl;
		}
		
		// Populate its PointingTable with BLOCKSIZE * numBlock rows
		PointingTable & pointingT = dataset.getPointing();
		PointingRow* pRow = 0;
		
		vector<vector<Angle> > pointingDirection;
		vector<Angle> v0;
		pointingDirection.push_back(v0);
		pointingDirection.at(0).push_back(Angle(1.0));
		pointingDirection.at(0).push_back(Angle(3.0));
	
		vector<vector<Angle> > target;
		vector<Angle> v1;
		target.push_back(v1);
		target.at(0).push_back(Angle(2.0));
		target.at(0).push_back(Angle(4.0));	
		
		int numPoly = 0;
		vector<vector<Angle> > offset;
		v1.clear();
		v1.push_back(Angle(-1.)); 
		v1.push_back(Angle(1.));
		offset.push_back(v1);
		
		vector<Angle> encoder;
		encoder.push_back(Angle(5.0));
		encoder.push_back(Angle(6.0));
		
		for (int i = 0; i< numBlock; i++) {
			for (int j = 0; j < BLOCKSIZE; j++) {
				pRow = pointingT.newRow(Tag(1, TagType::Antenna),
																ArrayTimeInterval((i*BLOCKSIZE + j) * 4000000LL, 4000000LL), 1,
																0,
																ArrayTime((i * BLOCKSIZE + j) * 4000000LL),
																pointingDirection,
																target,
																offset,
																encoder,
																true);
				pointingT.add(pRow);															
			}
		}
		cout << "The dataset has " << pointingT.size() << " rows in its Pointing table" << endl;
		
		// Populate its TotaPowerMonitoringTable with BLOCKSIZE * numBlock rows
		TotalPowerTable & tpmT = dataset.getTotalPower();
		TotalPowerRow* tpmRow = 0;
		
		// In that section we define a set of constant values given to the attributes of all the rows of the TPM table except the time attribute.		
		int numAntenna   = 1;
		int numBaseband = 1;
		int numCorr = 6;
		
		Tag configDescriptionId(1, TagType::ConfigDescription);
		Tag fieldId(1, TagType::Field);
		Tag execBlockId(1,	TagType::ExecBlock);
		vector<Tag> stateId;
		stateId.push_back(Tag(1, TagType::State));
			
		int scanNumber = 1;
		int subscanNumber = 1;
		int integrationNumber = 1;		
		
		vector< vector <Length> > uvw;
		for (int i = 0; i < 3; i++) {
			vector<Length> vl; uvw.push_back(vl);
			for (int j = 0; j < numAntenna; j++)
				uvw.at(i).push_back(Length(1000.0*numAntenna + j));
		}
		vector< vector<Interval> > exposure;
		vector< vector<ArrayTime> > timeCentroid;
		for (int i = 0; i < numAntenna; i++) {
			vector<Interval> vi; exposure.push_back(vi);
			vector<ArrayTime> va; timeCentroid.push_back(va);
			for (int j = 0; j < numBaseband; j++) {
				exposure.at(i).push_back(Interval(1000000LL * numAntenna + numBaseband));
				timeCentroid.at(i).push_back(ArrayTime(2000000LL * numAntenna + numBaseband));
			}
		}
		
		vector < vector < vector <float> > > floatData;
		for (int i = 0; i < numAntenna; i++) {
			vector < vector < float> > vvf; floatData.push_back(vvf);
			for (int j = 0; j < numBaseband; j++) {
				vector <float> vf; floatData.at(i).push_back(vf);
				for (int k = 0; k < numCorr; k++) {
					floatData.at(i).at(j).push_back(numAntenna * 1000.0 + numBaseband*100. + numCorr * 1.0);		
				}
			}	
		}
		
		vector<int> flagAnt;
		for (int i = 0; i < numAntenna; i++)
			flagAnt.push_back(0);
			
		vector<vector<int> > flagPol;
		for (int i = 0; i < numAntenna; i++) {
			vector<int> vi ; flagPol.push_back(vi);
			for (int j = 0; j < numCorr; j++) 
				flagPol.at(i).push_back(0);
		}
				
		flagRow = false;
		
		Interval interval(400000);
		for (int i = 0; i < numBlock; i++)
			for (int j  = 0; j < BLOCKSIZE; j++) {
				tpmRow = tpmT.newRow(configDescriptionId,
															fieldId,
															ArrayTime((i * BLOCKSIZE + j) * 40000000LL),
															execBlockId,
															stateId,
															scanNumber, 
															subscanNumber, 
															integrationNumber, 
															uvw,
															 exposure, 
															 timeCentroid, 
															 floatData,
															 flagAnt, 
															 flagPol, 
															 flagRow, 
															 interval);
				tpmT.add(tpmRow);
			}
		cout << "The dataset has " << tpmT.size() << " rows in its TotalPower table" << endl;	
		return;
}

int main(int argc, char *argv[]) {
	
	int numBlock;
	
	// Get the number of the number of blocks from command line.
	if (argc < 2) {
			cout << "Usage : TestBinaryTablePerf <numBlock>" << endl;
			exit(-1);
	}
	
	try {
		numBlock = Integer::parseInt(argv[1]);
	}
	catch (NumberFormatException e) {
		cout << "Invalid value for numBlock (" << e.getMessage() << ")" << endl;
		exit(-1);	
	}
	
	// Build a dummy dataset.
	cout << "About to build a dummy dataset" << endl;
	ASDM dataset;
	time_t beginCreateDataSet = time(0);
	buildASDM(dataset, numBlock);
	cout << "Built a dummy dataset (" << difftime(time(0), beginCreateDataSet) << " s.)"<<  endl;

	// Let's convert its PointingTable into a MIME message
	PointingTable& pointingT  = dataset.getPointing();
	cout << "Converting its PointingTable into a MIME message" << endl;
	time_t beginConvertPointing = time(0);	
	string pointingMIME = pointingT.toMIME();
	cout << "Converted (" <<  difftime(time(0), beginConvertPointing) << " s.)"<<  endl;
	cout << "The MIME conversion has " << pointingMIME.size() << " bytes for " << pointingT.size() << " rows." <<  endl;
	
	// Let's convert its TotalPowerTable into a MIME message
	TotalPowerTable& tpmT = dataset.getTotalPower();
	cout << "Converting its TotalPowerTable into a MIME message" << endl;
	time_t beginConvertTPM = time(0);	
	string tpmMIME = tpmT.toMIME();
	cout << "Converted (" <<  difftime(time(0), beginConvertTPM) << " s.)"<<  endl;
	cout << "The MIME conversion has " << tpmMIME.size() << " bytes  for " <<  tpmT.size() << " rows." << endl;

	// A new empty dataset.	
	ASDM datasetBis;
	
	// Populate the PointingTable of datasetBis by deserializing pointingMIME.
	PointingTable & pointingBisT = datasetBis.getPointing();
	cout << "Deserializing a Pointing table from a MIME message." << endl;
	time_t beginPointingFromMIME = time(0);
	try {
		pointingBisT.setFromMIME(pointingMIME);
	}
	catch (ConversionException e) {
		cout << e.getMessage() << endl;
	}
	catch (InvalidArgumentException e) {
		cout << e.getMessage() << endl;
	}
	cout << "Deserialized (" << difftime(time(0), beginPointingFromMIME) << " s.)"<<  endl;
	cout << "The PointingTable resulting from deserialization has " << pointingBisT.size() << " rows" << endl;
	
	// Populate the PointingTable of datasetBis by deserializing tpmMIME.
	TotalPowerTable & tpmBisT = datasetBis.getTotalPower();
	cout << "Deserializing a TotalPower table from a MIME message." << endl;
	time_t beginTPMFromMIME = time(0);
	try {
		tpmBisT.setFromMIME(tpmMIME);
	}
	catch (ConversionException e) {
		cout << e.getMessage() << endl;
	}
	catch (InvalidArgumentException e) {
		cout << e.getMessage() << endl;
	}
	cout << "Deserialized (" << difftime(time(0), beginTPMFromMIME) << " s.)"<<  endl;
	cout << "The TotalPower resulting from deserialization has " << tpmBisT.size() << " rows" << endl;	
}


