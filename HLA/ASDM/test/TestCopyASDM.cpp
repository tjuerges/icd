#include "ASDM.h"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"
#include "boost/progress.hpp"
#include <iostream>

namespace fs = boost::filesystem;
using namespace asdm;

int main(int argC, char* argV[]) {
  if (argC < 2) {
    cout << "Usage: TestCopyASDM asdm_directory"<<endl;
    exit(-1);
  }
  
  ASDM dataset;
  
  try {
    cout << "Trying to copy " << argV[1] << endl;
    dataset.setFromFile(argV[1]);
  }
  catch (ConversionException e) {
    cout << e.getMessage() << endl;
    exit(-1);
  }
  catch (...) {
    cout << "Oooops unexpected problem" << endl;
  }
  
  cout << "Successfully read dataset '"  <<argV[1]  << "'" << endl;
  
  fs::path full_path(fs::initial_path());
  full_path = fs::system_complete(fs::path(argV[1]));
  

  
  cout << full_path.branch_path().native_file_string() + "copy_of_" + full_path.leaf() << endl;

  try {
    dataset.toFile(full_path.branch_path().native_file_string() + "/copy_of_" + full_path.leaf());
  }
  catch (ConversionException e) {
    cout << e.getMessage() << endl;
  }

  cout << "Successfully copied to '" << full_path.branch_path().native_file_string() + "/copy_of_" + full_path.leaf() << "'" << endl;
  

  
}
