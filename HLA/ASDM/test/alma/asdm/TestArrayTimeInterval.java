/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestArrayTimeInterval.java
 */

/**
 * @author caillat
 *
 */

package alma.asdm;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Interval;
import alma.hla.runtime.asdm.types.ArrayTimeInterval;
import java.io.File;

public class TestArrayTimeInterval {
    public static void main(String[] arg) {
        // Test the overlapping
        // yesterday noon + 12 and today 0 + 12 should overlap
        ArrayTimeInterval ait1 = new ArrayTimeInterval((new ArrayTime(2005, 4, 26,
                12, 0, 0)).getMJD(), 0.5);
        System.out.println("ait1 = (start=" + ait1.getStart() + ",end="
                + ait1.getStart() + ait1.getDuration() + ")");

        // Test the overlapping
        // yesterday noon + 12h and today 0 + 12 should overlap
        ArrayTimeInterval ait0 = new ArrayTimeInterval((new ArrayTime(2005, 4,
                27, 0, 0, 0)).getMJD(), 0.5);
        System.out.println("ait0 = (start=" + ait0.getStart() + ",end="
                + ait0.getStart() + ait0.getDuration() + ")");

        if (ait0.overlaps(ait1))
            System.out
                    .println(ait0.toString() + " overlaps " + ait1.toString());
        else
            System.out.println(ait0.toString() + " does not overlaps "
                    + ait1.toString());

        // Yesterday noon + 1/4 of day and today 0h + 12h should not overlap.
        ArrayTimeInterval ait2 = new ArrayTimeInterval((new ArrayTime(2005, 4,
                26, 12, 0, 0)).getMJD(), 0.25);
        System.out.println("ait2 = (start=" + ait2.getStart() + ",end="
                + ait2.getStart() + ait2.getDuration() + ")");

        if (ait0.overlaps(ait2))
            System.out
                    .println(ait0.toString() + " overlaps " + ait2.toString());
        else
            System.out.println(ait0.toString() + " does not overlaps "
                    + ait2.toString());

        // Today at 4 AM is in ait0
        ArrayTime thisMorning = new ArrayTime(2005, 4, 27, 4, 0, 0);
        if (ait0.contains(thisMorning))
            System.out.println("Time =" + thisMorning.get() + " is in "
                    + ait0.toString());
        else
            System.out.println("Time =" + thisMorning.get() + " is not in "
                    + ait0.toString());

        // Today at 2 PM is not in ait0
        ArrayTime thisAfternoon = new ArrayTime(2005, 4, 27, 14, 0, 0);
        if (ait0.contains(thisAfternoon))
            System.out.println("Time =" + thisAfternoon.get() + " is in "
                    + ait0.toString());
        else
            System.out.println("Time =" + thisAfternoon.get() + " is not in "
                    + ait0.toString());

        // Working with time expressed in MJD.

        // Retrieve ait0's start and duration.
        double startInMJD = ait0.getStartInMJD();
        double durationInMJD = ait0.getDurationInDays();

        // Create a new ArrayTimeInterval from these values
        ArrayTimeInterval ait3 = new ArrayTimeInterval(startInMJD,
                durationInMJD);
        System.out
                .println("An ArrayTimeInterval constructed from the MJD reprensations of ait0's start and duration");
        System.out.println(ait0.toString());

        // Create an ArrayTimeInterval providing just the start.
        ArrayTimeInterval ait4 = new ArrayTimeInterval(startInMJD);
        System.out
                .println("An ArrayTimeInterval constructed from the MJD reprensations of ait0's start ");
        System.out.println(ait4);

        // Create an ArrayTimeInterval providing nothing.
        ArrayTimeInterval ait5 = new ArrayTimeInterval();
        System.out.println("An ArrayTimeInterval constructed from nothing ");
        System.out.println(ait5);
        
        // Test the CTOR (ArrayTime, ArrayTime).
        ArrayTimeInterval ati6 = new ArrayTimeInterval(new ArrayTime(1000000000),
        		new ArrayTime(1100000000));
        System.out.println("CTOR from a pair (ArrayTime, ArrayTime), ati6 = " + ati6.toString());
        
        // Test the CTOR (ArrayTime, Interval).
        ArrayTimeInterval ati7 = new ArrayTimeInterval(new ArrayTime(1000000000),
        		new Interval(100000000));
        System.out.println("CTOR from a pair (ArrayTime, Interval), ati7 = " + ati7.toString());
    }
}
