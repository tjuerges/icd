package alma.asdm;
import alma.AtmPhaseCorrectionMod.AtmPhaseCorrection;
import alma.CalDataOriginMod.CalDataOrigin;
import alma.CalTypeMod.CalType;
import alma.InvalidatingConditionMod.InvalidatingCondition;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.MergerException;
import alma.hla.runtime.asdm.types.*;

public class TestMerger1 {
	public void go(ASDM ds1, ASDM ds2) throws IllegalAccessException, DuplicateKey, MergerException {
		Merger merger = new Merger();
		merger.merge(ds1, ds2);
	}
	
	public TestMerger1() {
		;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Let's consider an ASDM initially empty.
		ASDM ds1 = new ASDM();
		
		// Let's fill an second ASDM filled as returned by TelCal after a TelCalAmpli calibration.
		ASDM ds2 = new ASDM();
		
		CalDataTable cdT = ds2.getCalData();
		
		int scanSet[] = {1};
		CalType calType = CalType.CAL_AMPLI;
		ArrayTime startTimeObserved = new ArrayTime(1000000L);
		ArrayTime endTimeObserved = new ArrayTime(2000000L);
		CalDataOrigin calDataType = CalDataOrigin.FULL_RESOLUTION_AUTO;
		CalDataRow cdR = cdT.newRow(scanSet.length, scanSet, calType, startTimeObserved, endTimeObserved, calDataType);
		cdR = cdT.add(cdR);
		System.out.println(cdR.getCalDataId());
		
		
		CalReductionTable crT = ds2.getCalReduction();
		ArrayTime timeReduced = new ArrayTime(1500000L);
		String calAppliedArray[] = {"calApplyArray"};
		String paramSet[] = {"theParam"};
		String messages = "messages";
		String software = "software";
		String softwareVersion = "softwareVersion";
		InvalidatingCondition invalidatingCondition[] = new InvalidatingCondition[1];
		invalidatingCondition[0] = InvalidatingCondition.ANTENNA_DISCONNECT;

		CalReductionRow crR = crT.newRow(calAppliedArray.length, paramSet.length, timeReduced, calAppliedArray, paramSet, messages, software, softwareVersion, invalidatingCondition.length, invalidatingCondition);
		crR = crT.add(crR);
		
		// A first CalAmpliRow.
		CalAmpliTable caT = ds2.getCalAmpli();
		String antennaName = "ANTENNA_0";
		AtmPhaseCorrection atmPhaseCorrection[] = new AtmPhaseCorrection[1];
		atmPhaseCorrection[0] = AtmPhaseCorrection.AP_CORRECTED;
		PolarizationType[] polarizationType = new PolarizationType[1];
		polarizationType[0] = PolarizationType.L;
		ReceiverBand receiverBand = ReceiverBand.ALMA_RB_01;
		ArrayTime startValidTime = new ArrayTime(1000000L);
		ArrayTime endValidTime = new ArrayTime(2000000L);

		Frequency[] frequencyRange = {new Frequency(1000.), new Frequency(2000.)};
		float apertureEfficiencyError[] = {0.0001f};
		CalAmpliRow caR = caT.newRow(cdR.getCalDataId(), crR.getCalReductionId(), antennaName, atmPhaseCorrection.length, polarizationType.length, receiverBand, atmPhaseCorrection, polarizationType, startValidTime, endValidTime,  frequencyRange); 
		try {
			caR = caT.add(caR);
		} catch (DuplicateKey e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		// A second CalAmpli row.
		antennaName = "ANTENNA_0";
		startValidTime = new ArrayTime(2000000L);
		endValidTime = new ArrayTime(3000000L);

		caR = caT.newRow(cdR.getCalDataId(), crR.getCalReductionId(), antennaName, atmPhaseCorrection.length, polarizationType.length, receiverBand, atmPhaseCorrection, polarizationType, startValidTime, endValidTime,  frequencyRange); 
		try {
			caR = caT.add(caR);
		} catch (DuplicateKey e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		System.out.print("Before merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalAmpli().size() + " CalAmpli rows.");
		TestMerger1 tm = new TestMerger1();
		try {
			tm.go(ds1, ds2);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (DuplicateKey e) {
			e.printStackTrace();
		} catch (MergerException e) {
			e.printStackTrace();
		}
		System.out.print("After merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalAmpli().size() + " CalAmpli rows.");
	}

}
