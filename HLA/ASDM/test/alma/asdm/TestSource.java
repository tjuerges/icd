/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jul 14, 2005
 * File TestSource.java
 */

package alma.asdm;

import java.io.File;

import alma.asdmIDL.SourceTableIDL;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.UniquenessViolationException;
import alma.hla.runtime.asdm.types.*;

/**
 * @author caillat
 * 
 */
public class TestSource {

    public static void main(String[] args) {
        ASDM dataset = new ASDM();
        SourceTable source = dataset.getSource();
        SourceRow aRow = null;

        //
        // Build a source table.
        //
        System.out.println("About to populate the source table ...");

        // A first row.

        // The key section.
        Tag spwId0 = new Tag(0);
        ArrayTimeInterval ati0 = new ArrayTimeInterval(new ArrayTime(1000));

        // The required value section.
        int numLines0 = 4;
        String sourceName0 = "Source 0";
        String code0 = "Code 0";

        Angle[] direction0 = new Angle[2];
        direction0[0] = new Angle(1.);
        direction0[1] = new Angle(1.);
        AngularRate[] properMotion0 = new AngularRate[2];
        properMotion0[0] = new AngularRate((double) 0.1);
        properMotion0[1] = new AngularRate((double) 0.1);

        aRow = source.newRow(spwId0, ati0, numLines0, sourceName0,
                 code0, direction0, properMotion0);

        System.out.println("aRow built");
        source.add(aRow);
        System.out.println("sourceId has been given the value ");

        // A second row
        // Its context (spectralWindow) and start time are the same than for the
        // previous row, its value section changes
        // therefore its sourceId should be obtained by autoincrementation.

        // The key section.
        Tag spwId1 = new Tag(0);
        ArrayTimeInterval ati1 = new ArrayTimeInterval(new ArrayTime(1000));

        // The value section.
        int numLines1 = 4;
        String sourceName1 = "Source 1";
        String code1 = "Code 1";
        Angle[] direction1 = new Angle[2];
        direction1[0] = new Angle(2.);
        direction1[1] = new Angle(2.);
        AngularRate[] properMotion1 = new AngularRate[2];
        properMotion1[0] = new AngularRate((double) 0.2);
        properMotion1[1] = new AngularRate((double) 0.2);

        aRow = source.newRow(spwId1, ati1, numLines1, sourceName1,
                 code1, direction1, properMotion1);

        System.out.println("aRow built");
        source.add(aRow);
        System.out.println("sourceId has been given the value "
                + aRow.getSourceId());

        // A third row . It comes with a new starttime and the same context,
        // then it should be given the id of the 1st row (0).
        // The key section.
        Tag spwId2 = new Tag(0);
        ArrayTimeInterval ati2 = new ArrayTimeInterval(new ArrayTime(2000));

        // The value section.
        int numLines2 = 4;
        String sourceName2 = "Source 0";
        String code2 = "Code 0";
        Angle[] direction2 = new Angle[2];
        direction2[0] = new Angle(3.);
        direction2[1] = new Angle(3.);
        AngularRate[] properMotion2 = new AngularRate[2];
        properMotion2[0] = new AngularRate((double) 0.3);
        properMotion2[1] = new AngularRate((double) 0.3);

        aRow = source.newRow(spwId2, ati2, numLines2, sourceName2,
                 code2, direction2, properMotion2);

        System.out.println("aRow built");
        source.add(aRow);
        System.out.println("sourceId has been given the value "
                + aRow.getSourceId());

        // A fourth row . It comes with a new starttime and a new context, then
        // it should be given the id 0,
        // The key section.
        Tag spwId3 = new Tag(1);
        ArrayTimeInterval ati3 = new ArrayTimeInterval(new ArrayTime(1000));

        // The value section.
        int numLines3 = 3;
        String sourceName3 = "Source 0";
        String code3 = "Code 3";
        Angle[] direction3 = new Angle[2];
        direction3[0] = new Angle(3.);
        direction3[1] = new Angle(3.);
        AngularRate[] properMotion3 = new AngularRate[2];
        properMotion3[0] = new AngularRate((double) 0.3);
        properMotion3[1] = new AngularRate((double) 0.3);

        aRow = source.newRow(spwId3, ati3, numLines3, sourceName3,
                code3, direction3, properMotion3);

        System.out.println("aRow built");
        source.add(aRow);
        System.out.println("sourceId has been given the value "
                + aRow.getSourceId());

        // A fifth row . It comes with a starttime already existing and the same
        // context than above,
        // The key section.
        Tag spwId4 = new Tag(1);
        ArrayTimeInterval ati4 = new ArrayTimeInterval(new ArrayTime(1500));

        // The value section.
        int numLines4 = 5;
        String sourceName4 = "Source 0";
        String code4 = "Code 5";
        Angle[] direction4 = new Angle[2];
        direction4[0] = new Angle(5.);
        direction4[1] = new Angle(5.);
        AngularRate[] properMotion4 = new AngularRate[2];
        properMotion4[0] = new AngularRate((double) 0.3);
        properMotion4[1] = new AngularRate((double) 0.3);

        aRow = source.newRow(spwId4, ati4, numLines4, sourceName4,
                code4, direction4, properMotion4);

        System.out.println("aRow built");
        source.add(aRow);
        System.out.println("sourceId has been given the value "
                + aRow.getSourceId());

        // A sith row . It's identical to the fifth row, this should be
        // detected and return a pointer to the fifth row.
        Tag spwId5 = new Tag(1);
        ArrayTimeInterval ati5 = new ArrayTimeInterval(new ArrayTime(1500));

        // The value section.
        int numLines5 = 5;
        String sourceName5 = "Source 0";
        String code5 = "Code 5";
        Angle[] direction5 = new Angle[2];
        direction5[0] = new Angle(5.);
        direction5[1] = new Angle(5.);
        AngularRate[] properMotion5 = new AngularRate[2];
        properMotion5[0] = new AngularRate((double) 0.3);
        properMotion5[1] = new AngularRate((double) 0.3);

        aRow = source.newRow(spwId5, ati5, numLines5, sourceName5,
                code5, direction5, properMotion5);

        System.out.println("aRow built");
        SourceRow retRow = source.add(aRow);

        if (retRow != aRow) {
            System.out
                    .println("Attempt to store a row identical to a row present in the SourceTable");
            System.out.println("It's the row with id " + retRow.getSourceId());
        }

        // Let's check what is actually stored in the Source table.
        SourceRow[] myRows = source.get();
        System.out.println();
        System.out.println("There are " + source.size()
                + " rows in the Source table");
        System.out.println("Their ids are ");
        for (int i = 0; i < myRows.length; i++)
            System.out.println(myRows[i].getSourceId());

        // Convert the Source table in its XML representation.
        System.out.println();
        System.out
                .println("Here is the XML representation of the Source table:");
        String xmlSource = null;
        try {
            xmlSource = source.toXML();
            System.out.println(xmlSource);
        } catch (ConversionException e3) {
            // TODO Auto-generated catch block
            e3.printStackTrace();
        }

        // Test getRowByKey
        SourceRow qRow = myRows[0];
        int qsourceId = qRow.getSourceId();
        Tag qspwTag = qRow.getSpectralWindowId();
        ArrayTimeInterval qati = qRow.getTimeInterval();
        System.out.println();
        System.out.println("Now testing getRowBykey:");
        System.out.println("Querying for the Source row with sourceId = "
                + qsourceId + ", spectralWindowId = " + qspwTag
                + ", timeInterval = " + qati + "...");

        if ((aRow = source.getRowByKey(qsourceId, qspwTag, qati)) == null) {
            System.out
                    .println("...could not find such a row. This should not happen !");
        } else {
            System.out.println("...query successful");
            System.out.println("Found the row with sourceId = "
                    + aRow.getSourceId() + ", spectralWindowId = "
                    + aRow.getSpectralWindowId() + ", timeInterval = "
                    + (qati = aRow.getTimeInterval()));
        }

        qati.setStart(qati.getStart().get() + qati.getDuration().get() / 2);
        System.out.println("Querying again but with start time = "
                + qati.getStart() + "...");

        if ((aRow = source.getRowByKey(qsourceId, qspwTag, qati)) == null) {
            System.out.println("...could not find such a row. This should not happen !");
        } else {
            System.out.println("...query successful");
            System.out.println("Found the row with sourceId = "
                    + aRow.getSourceId() + ", spectralWindowId = "
                    + aRow.getSpectralWindowId() + ", timeInterval = "
                    + (qati = aRow.getTimeInterval()));
        }

        // Write out the dataset in XML documents.
        System.out
                .println("Now converting the dataset into XML and saving it to disk");

        try {
            java.util.Random r = new java.util.Random();
            int dummy = r.nextInt();
            File dir = new File("/tmp/asdm_" + Math.max(dummy, -dummy));
            if (!dir.mkdir())
                throw new ConversionException("Could not make directory "
                        + dir.getAbsolutePath(), null);
            dataset.toXML(dir.getAbsolutePath());
            System.out.println("Dataset written to " + dir.getAbsolutePath());
        } catch (ConversionException e) {
            e.printStackTrace();
        }

        // Let's convert the table into CORBA structures
        System.out.println("Converting the Source table into CORBA structures");
        SourceTableIDL sourceIDL = source.toIDL();

        // The other way around
        System.out
                .println("Converting the CORBA structures into a Source table");
        ASDM dataSetBis = new ASDM();
        SourceTable sourceBis = dataSetBis.getSource();
        try {
            sourceBis.fromIDL(sourceIDL);
        } catch (DuplicateKey e1) {
            e1.printStackTrace();
        } catch (ConversionException e1) {
            e1.printStackTrace();
        } catch (UniquenessViolationException e1) {
            e1.printStackTrace();
        }

        System.out.println("The Source table resulting from to conversion from the CORBA structures has "
                        + sourceBis.size() + " rows");

        myRows = sourceBis.get();
        System.out.println("Their ids are ");
        for (int i = 0; i < myRows.length; i++)
            System.out.println(myRows[i].getSourceId());

        System.out.println();

        System.out.println("Here is its XML representation :");
        String xmlSourceBis = null;
        try {
            xmlSourceBis = sourceBis.toXML();
            System.out.println(xmlSourceBis);
        } catch (ConversionException e2) {
            e2.printStackTrace();
        }
        
        System.out.println();
        System.out.println( "Comparing the two XML representations (before and after binary->CORBA structs->binary conversions)" );
        if (xmlSource.equals(xmlSourceBis) )
            System.out.println( "They are similar as expected" );
        else
            System.out.println( "Ooops, they are different. Something went wrong !" );

    }
}
