/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jul 15, 2005
 * File TestCalDevice.java
 */

package alma.asdm;
import java.io.File;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.NoSuchRow;
import alma.hla.runtime.asdm.types.*;

import alma.CalibrationDeviceMod.*;
/**
 * @author caillat
 *
 */
public class TestCalDevice {

    public static void main(String[] args) {
        ASDM dataset       = new ASDM();
        CalDeviceTable cdt = dataset.getCalDevice();
        CalDeviceRow   cdr = null;
        
        Tag antennaId = new Tag(1);
        int feedId = 1;
        Tag spectralWindowId0 = new Tag(1);
        Tag spectralWindowId1 = new Tag(2);
        ArrayTimeInterval ati0 = new ArrayTimeInterval(new ArrayTime(1000));
        ArrayTimeInterval ati1 = new ArrayTimeInterval(new ArrayTime(2000));
        int numCalload0 = 1;
        CalibrationDevice calLoadName0[] = new CalibrationDevice[numCalload0];
        calLoadName0[0] = CalibrationDevice.AMBIENT_LOAD;

        int numCalload1 = 1;
        CalibrationDevice calLoadName1[] = new CalibrationDevice[numCalload1];
        calLoadName0[0] = CalibrationDevice.COLD_LOAD;
        
        int numCalload2 = 1;        
        CalibrationDevice calLoadName2[] = new CalibrationDevice[numCalload2];
        calLoadName2[0] = CalibrationDevice.HOT_LOAD;
        
        //
        // Add three rows.
        try {
        cdr = cdt.newRow(antennaId, feedId, spectralWindowId0, ati0, numCalload0, calLoadName0);
        if (cdt.add(cdr) == cdr) 
          System.out.println("A new row has been actually added to the table" );
        }
        catch (DuplicateKey e) {
          System.out.println("Error ! " + e.getMessage() );
        }

        // Same context than above
        try {
        cdr = cdt.newRow(antennaId, feedId, spectralWindowId0, ati1, numCalload1, calLoadName1);
        if (cdt.add(cdr) == cdr) 
          System.out.println("A new row has been actually added to the table" );
        }
        catch (DuplicateKey e) {
          System.out.println("Error ! " + e.getMessage() );
        }

        // Different context than the two previous rows
        try {
        cdr = cdt.newRow(antennaId, feedId, spectralWindowId1, ati1, numCalload2, calLoadName2);
        if (cdt.add(cdr) == cdr) 
          System.out.println("A new row has been actually added to the table" );
        }
        catch (DuplicateKey e) {
          System.out.println("Error ! " + e.getMessage() );
        }
        


        // Get a row by Key
        // ... an existing row firstly...
        CalDeviceRow aRow = null;
        if ( (aRow = cdt.getRowByKey(antennaId, feedId, spectralWindowId1, ati1)) != null ) {
          System.out.println("Found a row with key (antennaId="+ antennaId
         + ", feedId=" + feedId
         + ",spectralWindowId=" + spectralWindowId1
         + ", timeInterval=" + ati1 + ")" );
          System.out.println("Its value section is numCalload = " + aRow.getNumCalload() );
        }
        else 
          System.out.println("No row with key (antennaId="+ antennaId
         + ", feedId=" + feedId
         + ",spectralWindowId="
         + spectralWindowId1 + ",timeInterval=" + ati1 + ")" );

        // ... and then a non existing row.
        if ( (aRow=cdt.getRowByKey(antennaId, feedId+4, spectralWindowId1, ati1)) != null ) {
          System.out.println("Found a row with key (antennaId="+ antennaId
         + ", feedId=" + feedId
         + ",spectralWindowId=" + spectralWindowId1
         + ", timInterval=" + ati1 + ")" );
          System.out.println("Its value section is numCalload = " + aRow.getNumCalload() );
        }
        else 
          System.out.println("No row with key (antennaId="+ antennaId
         + ", feedId=" + (feedId+4)
         + ",spectralWindowId="
         + spectralWindowId1 + ",timeInterval=" + ati1 + ")" );

        // Display the number of rows stored in the table
        System.out.println("The CalDevice table has "+ cdt.size() + " row(s)" );
        
     
        // Write out the dataset in XML documents.
        try {
            java.util.Random r = new java.util.Random();
            int dummy = r.nextInt();
            File dir = new File("/tmp/asdm_" + Math.max(dummy, -dummy));
            if (!dir.mkdir())
                throw new ConversionException("Could not make directory "
                        + dir.getAbsolutePath(), null);
            dataset.toXML(dir.getAbsolutePath());
            System.out.println("Dataset written to " + dir.getAbsolutePath());
        } catch (ConversionException e) {
            e.printStackTrace();
        }      
    }
}
