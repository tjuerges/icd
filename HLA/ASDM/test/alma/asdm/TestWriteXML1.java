/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on May 20, 2005
 * File TestWriteXML1.java
 */

package alma.asdm;

import java.io.File;

import alma.CorrelationBitMod.CorrelationBit;
import alma.NetSidebandMod.NetSideband;
import alma.PolarizationTypeMod.PolarizationType;
import alma.SidebandProcessingModeMod.SidebandProcessingMode;
import alma.StokesParameterMod.StokesParameter;
import alma.WindowFunctionMod.WindowFunction;
import alma.asdmIDL.FeedRowIDL;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.NoSuchRow;
import alma.hla.runtime.asdm.types.*;

import alma.BasebandNameMod.BasebandName;
/**
 * @author caillat
 * 
 */
public class TestWriteXML1 {

    public static void main(String[] args) {
        ASDM dataset = new ASDM();

        System.out
                .println("This program creates an ASDM dataset containing three non empty tables :");
        System.out
                .println("DataDescription, SpectralWindow and Polarization./n");
        System.out.println("The ASDM is then exported in XML representation.");
        System.out.println();
        System.out.println("About to populate the  spectral window table ...");

        SpectralWindowTable spectralWindow = dataset.getSpectralWindow();
        // Build the Spectral Window Table
        int numChan = 16;
        // This table will contain nrows rows.
        int nrows = 4;

        Frequency[] chanFreq = new Frequency[numChan];
        Frequency[] chanWidth = new Frequency[numChan];
        Frequency[] effectiveBw = new Frequency[numChan];
        Frequency[] resolution = new Frequency[numChan];

        for (int j = 0; j < nrows; j++) {
            // Populate these arrays with completely dummy values.
            for (int i = 0; i < numChan; i++) {
                chanFreq[i] = new Frequency(1000. * i + j);
                chanWidth[i] = new Frequency(100. + j);
                chanFreq[i] = new Frequency(1000. * i + j);
                effectiveBw[i] = new Frequency(500. + 1. / (j + i + 1.));
                resolution[i] = new Frequency(60. + j);
            }

            String name = new String("Spectral Window #" + j);
            String freqGroupName = new String("Frequency group name " + j);

            Frequency refFreq = new Frequency(2000.0 * j);
            Frequency totBandWidth = new Frequency(2220.0 * j);

            System.out.println("About to build a spectral window row ...");
    		NetSideband  netSideband = NetSideband.NOSB;
    		SidebandProcessingMode sidebandProcessingMode = SidebandProcessingMode.NONE;
    		boolean quantization  = false;
    		WindowFunction windowFunction = WindowFunction.UNIFORM;
    		boolean oversampling = false;
    		CorrelationBit correlationBit = CorrelationBit.BITS_2x2;
    		
            SpectralWindowRow aRow = spectralWindow.newRow(numChan, 
                    refFreq, chanFreq, chanWidth,  effectiveBw,
                    resolution, totBandWidth, netSideband, sidebandProcessingMode, quantization, windowFunction, oversampling, correlationBit,   false);
            //aRow.setBasebandName(BasebandName.BB_1);
            // Add the row to the SpectralWindow table using this Tag.
            System.out
                    .println("About to add it to its spectral window table ...");
            aRow = spectralWindow.add(aRow);
            System.out.println("it has been given the id "
                    + aRow.getSpectralWindowId());
        }

        // Build a polarization table with two rows.
        System.out.println("About to populate the polarization table ...");
        PolarizationTable polarization = dataset.getPolarization();

        for (int i = 0; i < 2; i++) {

            // Number of correlations cross products.
            int numCorr = i + 1;

            // Instantiate and populate an array for the corrType attribute
            StokesParameter corrType[] = new StokesParameter[numCorr];
            for (int j = 0; j < numCorr; j++)
                corrType[j] = StokesParameter.from_int(j);

            // Instantiate and populate a vector for the corrProduct attribute
            // Given that corrProduct is a vector of vector...this is
            // a two step process.
            PolarizationType[][] corrProduct = new PolarizationType[2][numCorr];
            for (int j = 0; j < numCorr; j++) {
                corrProduct[0][j] = PolarizationType.from_int(i+j);
                corrProduct[1][j] = PolarizationType.from_int(i+j);
            }

            // Create a new Polarization row.
            System.out.print("About to build a polarization row ...");
            PolarizationRow aRow = polarization.newRow(numCorr, corrType,
                    corrProduct);

            // Add the newly created row to the Polarization table
            // with this Tag.
            System.out.print("About to add it to its table ...");
            polarization.add(aRow);
            System.out.println("it has been given the id "
                    + aRow.getPolarizationId());
        }

        //
        // Build a DataDescription table with three rows.
        // 
        System.out.println("About to populate the datadescription table ...");
        DataDescriptionTable dataDescription = dataset.getDataDescription();

        SpectralWindowRow[] spwR = spectralWindow.get();
        PolarizationRow[] polR = polarization.get();

        System.out.println("A row for spectralwindowId = "
                + spwR[0].getSpectralWindowId()
                + ",  polarizationId = "
                + polR[0].getPolarizationId());

        DataDescriptionRow aDDRow = dataDescription.newRow(polR[0].getPolarizationId(), spwR[0].getSpectralWindowId());

        dataDescription.add(aDDRow);
        System.out.println("it has been given the id "
                + aDDRow.getDataDescriptionId());

        System.out.println("A row for spectralwindowId = "
                + spwR[0].getSpectralWindowId()
                + ",  polarizationId = "
                + polR[1].getPolarizationId());
        aDDRow = dataDescription.newRow(polR[1].getPolarizationId(), spwR[0].getSpectralWindowId());

        dataDescription.add(aDDRow);
        System.out.println("it has been given the id "
                + aDDRow.getDataDescriptionId());

        System.out.println("A row for spectralwindowId = "
                + spwR[1].getSpectralWindowId()
                + ",  polarizationId = "
                + polR[1].getPolarizationId());
        aDDRow = dataDescription.newRow(polR[1].getPolarizationId(), spwR[1].getSpectralWindowId());

        dataDescription.add(aDDRow);
        System.out.println("it has been given the id "
                + aDDRow.getDataDescriptionId());

        // Write out the dataset in XML documents.
        try {
            java.util.Random r = new java.util.Random();
            int dummy = r.nextInt();
            File dir = new File("/tmp/asdm_" + Math.max(dummy, -dummy));
            if (!dir.mkdir())
                throw new ConversionException("Could not make directory "
                        + dir.getAbsolutePath(), null);
            dataset.toXML(dir.getAbsolutePath());
            System.out.println("Dataset written to " + dir.getAbsolutePath());
            /*
            System.out.println("With creation date/time : "
                    + (java.util.Calendar.getInstance()).toString());
            */
        } catch (ConversionException e) {
            e.printStackTrace();
        }
    }
}
