package alma.asdm;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Interval;
import alma.hla.runtime.asdm.types.Length;
import alma.hla.runtime.asdm.types.Tag;
import alma.hla.runtime.asdm.types.TagType;

public class TestTotalPower {

	/**
	 * @param args
	 * @throws DuplicateKey 
	 * @throws ConversionException 
	 */
	public static void main(String[] args) throws DuplicateKey, ConversionException {
		// TODO Auto-generated method stub
		
		Tag configDescriptionId = new Tag(0, TagType.ConfigDescription);
		Tag fieldId = new Tag(0, TagType.Field);
		int scanNumber = 0;
		int subscanNumber = 0;
		int integrationNumber = 0;
		Length uvw[][] = new Length[1][1];
		uvw[0][0] = new Length(3.14159265);
		Interval exposure[][] = new Interval[1][1];
		exposure[0][0] = new Interval(100000);
		ArrayTime timeCentroid[][] = new ArrayTime[1][1];
		timeCentroid[0][0] = new ArrayTime(1000000000);
		float floatData[][][] = new float[1][1][1];
		floatData[0][0][0] = 123456789.0F;
		int flagAnt[] = {1, 2, 3, 4, 5};
		int flagPol[][] = {{1, 2}, {3, 4}};
		boolean flagRow = false;
		Interval interval = new Interval(1000000);
		Tag stateId[] = new Tag[1];
		stateId[0] = new Tag(0, TagType.State);
		Tag execBlockId = new Tag(0, TagType.ExecBlock);
		
		ASDM asdm = new ASDM();
		TotalPowerTable tT = asdm.getTotalPower();
		TotalPowerRow tR = null;
		for (int i = 0; i < 1000; i++) {
			tR = tT.newRow(new ArrayTime(i*100000), configDescriptionId, fieldId, scanNumber, subscanNumber, integrationNumber, uvw, exposure, timeCentroid, floatData, flagAnt, flagPol, flagRow, interval, stateId, execBlockId);
			tT.add(tR);
		}
		
		System.out.println("Number of rows in TotalPower " + tT.size());
		asdm.toFile("TestTotalPower");
		
	}

}
