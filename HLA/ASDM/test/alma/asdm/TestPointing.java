package alma.asdm;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.types.Angle;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.ArrayTimeInterval;
import alma.hla.runtime.asdm.types.Length;
import alma.AntennaMakeMod.AntennaMake;
import alma.AntennaTypeMod.AntennaType;
import alma.StationTypeMod.StationType;
public class TestPointing {

	/**
	 * @param args
	 * @throws DuplicateKey 
	 * @throws ConversionException 
	 * @throws IllegalAccessException 
	 */
	public static void main(String[] args) throws DuplicateKey, ConversionException, IllegalAccessException {
		// TODO Auto-generated method stub

		ArrayTime at1 = new ArrayTime(10000000);
		System.out.println(at1.toFITS());
		
		ArrayTime at2  = new ArrayTime(at1);
		System.out.println(at2.toFITS());
		
		ASDM asdm = new ASDM();
		
		StationTable sT = asdm.getStation();
		
		Length position0[] = new Length[3];
		for (int i = 0; i < position0.length; i++)
			position0[i] = new Length(1000.);
		
		StationRow sR0 = sT.newRow("Station0", position0, StationType.ANTENNA_PAD);
		sT.add(sR0);
		
		Length position1[] = new Length[3];
		for (int i = 0; i < position1.length; i++)
			position1[i] = new Length(1000.);
		
		StationRow sR1 = sT.newRow("Station1", position1, StationType.ANTENNA_PAD);
		sT.add(sR1);
		
		
		AntennaTable aT = asdm.getAntenna();	
		Length[] antPosition0 = new Length[3];
		for (int i = 0; i < antPosition0.length; i++)
			antPosition0[i] = new Length(1234.0);
		
		Length[] antOffset0 = new Length[3];
		for (int i = 0; i < antOffset0.length; i++)			
			antOffset0[i] = new Length(0.1234);
		
		AntennaRow aR0 = aT.newRow("Antenna_0", 
				AntennaMake.MITSUBISHI_12_B, 
				AntennaType.GROUND_BASED, 
				new Length(12.0), 
				antPosition0, 
				antOffset0, 
				new ArrayTime(10000000), 
				sR0.getStationId());
		aR0 = aT.add(aR0);
		
		Length[] antPosition1 = new Length[3];
		for (int i = 0; i < antPosition1.length; i++)
			antPosition1[i] = new Length(5678.0);
		
		Length[] antOffset1 = new Length[3];
		for (int i = 0; i < antOffset1.length; i++)			
			antOffset1[i] = new Length(0.5678);

		AntennaRow aR1 = aT.newRow("Antenna_1", 
				AntennaMake.MITSUBISHI_12_B, 
				AntennaType.GROUND_BASED, 
				new Length(12.0), 
				antPosition1, 
				antOffset1, 
				new ArrayTime(10000000), 
				sR1.getStationId());
		aR1 = aT.add(aR1);		
		
		
		PointingTable pT = asdm.getPointing();
		
		// Populate the pointing table with two rows for each antenna.
		PointingRow pR = null;
		int numSample = 100;
		boolean pointingTracking = false;
		boolean usePolynomials = false;
		int numTerm = 1;
		Angle[][] encoder = new Angle[numSample][2];
		Angle[][] pointingDirection = new Angle[numTerm][2];
		Angle[][] target = new Angle[numTerm][2];
		Angle[][] offset = new Angle[numTerm][2];
		int pointingModelId = -1;
		
		
		// Antenna0
		for (int i=0; i < 2; i++) {
			pointingDirection[0][0] = new Angle(1+0.5*i);
			pointingDirection[0][1] = new Angle(1+0.25*i);

			target[0][0] = new Angle(1+0.5*i);
			target[0][1] = new Angle(1+0.25*i);
			
			offset[0][0] = new Angle(0.5+0.05*i);
			offset[0][1] = new Angle(0.5+0.025*i);
			
			for (int j = 0; j < numSample; j++) {
				encoder[j][0] = new Angle(Math.random());
				encoder[j][1] = new Angle(Math.random());
			}

			pR = pT.newRow(aR0.getAntennaId(), 
					new ArrayTimeInterval(1000000+i*1000000), 
					numSample, 
					encoder, 
					pointingTracking, 
					usePolynomials, 
					new ArrayTime(20000000+i*2000000), 
					numTerm, 
					pointingDirection, 
					target, 
					offset, 
					pointingModelId);
			pR.setUsePolynomials(true);
			pT.add(pR);
		}
		
		// Antenna1
		for (int i=0; i < 2; i++) {
			pointingDirection[0][0] = new Angle(2+0.5*i);
			pointingDirection[0][1] = new Angle(2+0.25*i);

			target[0][0] = new Angle(2+0.5*i);
			target[0][1] = new Angle(2+0.25*i);
			
			offset[0][0] = new Angle(1+0.05*i);
			offset[0][1] = new Angle(1+0.025*i);
			
			for (int j = 0; j < numSample; j++) {
				encoder[j][0] = new Angle(Math.random());
				encoder[j][1] = new Angle(Math.random());
			}

			pR = pT.newRow(aR1.getAntennaId(), 
					new ArrayTimeInterval(1000000+i*1000000), 
					numSample, 
					encoder, 
					pointingTracking, 
					usePolynomials, 
					new ArrayTime(20000000+i*2000000), 
					numTerm, 
					pointingDirection, 
					target, 
					offset, 
					pointingModelId);
			pR.setUsePolynomials(true);
			pT.add(pR);
		}
		
		asdm.toFile("TestPointing");
		
	}

}
