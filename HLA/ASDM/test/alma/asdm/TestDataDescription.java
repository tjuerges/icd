package alma.asdm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import alma.CorrelationBitMod.CorrelationBit;
import alma.HolographyChannelTypeMod.HolographyChannelType;
import alma.PolarizationTypeMod.PolarizationType;
import alma.StokesParameterMod.StokesParameter;
import alma.NetSidebandMod.NetSideband;
import alma.SidebandProcessingModeMod.SidebandProcessingMode;
import alma.WindowFunctionMod.WindowFunction;

import alma.asdm.ArchiverCC;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.hla.runtime.asdm.ex.ArchiverException;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.UniquenessViolationException;
import alma.hla.runtime.asdm.types.Angle;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.ArrayTimeInterval;
import alma.hla.runtime.asdm.types.EntityId;
import alma.hla.runtime.asdm.types.Frequency;
import alma.hla.runtime.asdm.types.Interval;
import alma.hla.runtime.asdm.types.Length;
import alma.hla.runtime.asdm.types.Tag;
import alma.hla.runtime.asdm.types.TagType;

public class TestDataDescription {
	static final int BLOCKSIZE = 1024;

	public static String xmlIze(ASDM dataset) throws ConversionException {
		StringWriter out = new StringWriter();
		
		try {
			Document document = null;
			SAXBuilder saxB  = new SAXBuilder();
			XMLOutputter xmlO = new XMLOutputter(Format.getPrettyFormat());
			
			// The ASDM container.
			document = saxB.build(new StringReader(dataset.toXML()));
			xmlO.output(document, out);
			
			// The DataDescription table
			document = saxB.build(new StringReader(dataset.getDataDescription().toXML()));
			xmlO.output(document, out);
			
			// The Holography  table
			document = saxB.build(new StringReader(dataset.getHolography().toXML()));
			xmlO.output(document, out);
			
			// The Polarization  table
			document = saxB.build(new StringReader(dataset.getPolarization().toXML()));
			xmlO.output(document, out);
			
			// The SpectralWindow table
			document = saxB.build(new StringReader(dataset.getSpectralWindow().toXML()));
			xmlO.output(document, out);				
		}
		catch (IOException e) {
			System.err.println(e);
			System.exit(-1);
		} catch (JDOMException e) {
			e.printStackTrace();
			System.exit(-1);
		}	
		return out.toString();
	}
	
	public static ASDM buildASDM() throws DuplicateKey {
		// Create a new and empty ASDM
		ASDM dataset = new ASDM();

		// Populate its Holography table with one row
		HolographyTable holoT = dataset.getHolography();
		HolographyRow holoR = null;

		//String typeHolo[] =  {"SS", "RR", "QQ", "SR", "SQ", "QR"};
		HolographyChannelType typeHolo[] = new HolographyChannelType[6];
		typeHolo[0] = HolographyChannelType.S2;
		typeHolo[1] = HolographyChannelType.R2;
		typeHolo[2] = HolographyChannelType.Q2;
		typeHolo[3] = HolographyChannelType.RS;
		typeHolo[4] = HolographyChannelType.QS;
		typeHolo[5] = HolographyChannelType.QR;		
		
		Length distance= new Length(1000.0);
		Length focus = new Length(10.0);
		boolean flagRow = false;

		holoR = holoT.newRow(typeHolo.length, typeHolo, distance, focus,flagRow);
		holoT.add(holoR);
		System.out.println("The dataset has " + holoT.size() + " rows in its Holography table");	

		// Populate its Polarization table with one row
		PolarizationTable polT = dataset.getPolarization();
		PolarizationRow polR = null;

		int numCorrPol = 4;
		//int typePol[] = { 9, 10, 11, 12 };
		StokesParameter typePol[] = new StokesParameter[numCorrPol];
		typePol[0] = StokesParameter.RR;
		typePol[1] = StokesParameter.LL;
		typePol[2] = StokesParameter.RL;
		typePol[2] = StokesParameter.LR;
		
		//int productPol[][] = {{0, 0}, {0,1}, {1,0}, {1,1}};
		PolarizationType productPol[][] = new PolarizationType[numCorrPol][2];
		productPol[0][0] = PolarizationType.R;
		productPol[0][1] = PolarizationType.R;	
		
		productPol[1][0] = PolarizationType.R;
		productPol[1][1] = PolarizationType.L;
				
		productPol[2][0] = PolarizationType.L;
		productPol[2][1] = PolarizationType.R;
		
		productPol[3][0] = PolarizationType.L;
		productPol[3][1] = PolarizationType.L;
				
					
		polR = polT.newRow(typePol.length, typePol, productPol);
		polT.add(polR);
		System.out.println("The dataset has " + polT.size() + " rows in its Polarization table");

		// Populate its SpectralWindow table with one row
		SpectralWindowTable spwT = dataset.getSpectralWindow();
		SpectralWindowRow spwR = null;

		int numChan = 16;
		String name = "Dummy SpectralWindow";
		Frequency refFreq = new Frequency(1.0e9);
		Frequency chanFreq[] = new Frequency[numChan];
		Frequency chanWidth[] = new Frequency[numChan];
		for (int i = 0; i < numChan; i++) {
			chanFreq[i] = new Frequency(1.0e9 + i*1.0e8);
			chanWidth[i] = new Frequency(1.0e8);
		}
		int measFreqRef = 1000000000;
		Frequency effectiveBw[] = new Frequency[numChan];
		Frequency resolution[] = new Frequency[numChan];
		for (int i = 0; i < numChan; i++) {
			effectiveBw[i] = new Frequency(1.0e9 + i*2.0e8);
			resolution[i] = new Frequency(2.0e8);
		}

		Frequency totBandwidth = new Frequency(numChan * 1.0e8);
		NetSideband  netSideband = NetSideband.NOSB;
		SidebandProcessingMode sidebandProcessingMode = SidebandProcessingMode.NONE;
		boolean quantization  = false;
		WindowFunction windowFunction = WindowFunction.UNIFORM;
		boolean oversampling = false;
		CorrelationBit correlationBit = CorrelationBit.BITS_2x2;
		flagRow = false;

		spwR = spwT.newRow(numChan,  refFreq, chanFreq, chanWidth,  effectiveBw, resolution, totBandwidth, netSideband,  sidebandProcessingMode, quantization, windowFunction, oversampling, correlationBit, flagRow);
		spwT.add(spwR);
		System.out.println("The dataset has "+ spwT.size() + " rows in its SpectralWindow table");


		// Populate its DataDescription table
		DataDescriptionTable ddT = dataset.getDataDescription();
		DataDescriptionRow ddR = null;

		// Here is an association to an  HolographyRow and a SpectralWindowRow
		System.out.println("Adding a row with a link to the Holography table to the DataDescription table.");
		ddR = ddT.newRow(holoR.getHolographyId(), spwR.getSpectralWindowId());
		ddT.add(ddR);
		System.out.println("Here is its polOrHoloId :" + ddR.getPolOrHoloId().toString());

		// Here is an association  to a PolarizationRow and the same SpectralWindowRow
		System.out.println("Adding a row with a link to the Polarization table to the DataDescription table.");
		ddR = ddT.newRow(polR.getPolarizationId(), spwR.getSpectralWindowId());
		ddT.add(ddR);
		System.out.println("Here is its polOrHoloId :" + ddR.getPolOrHoloId().toString());		

		System.out.println("The dataset has " + ddT.size() + " rows in its DataDescription table");
		DataDescriptionRow [] rows = ddT.get();
		for (int i = 0; i < ddT.size(); i++) {
			System.out.println("The DataDescription row #" + i + " is associated to a "+ rows[i].getPolOrHoloId().getTagType().toString());
		}

		return dataset;
	}

	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws UniquenessViolationException 
	 */
	public static void main(String[] args) throws IOException, DuplicateKey, ConversionException, InterruptedException, UniquenessViolationException {

		// Build a dummy dataset
		System.out.println("About to build a dummy dataset");
		ASDM dataset = buildASDM();
		System.out.println("Built a dummy dataset");

		// Let's take a look at the XML representation of this dataset.
		String xmlDataset = xmlIze(dataset);
		System.out.println("Here is the XML representation of the dataset : ");
		System.out.println(xmlDataset);
	}
}
