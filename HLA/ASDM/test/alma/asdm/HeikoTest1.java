/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jul 21, 2005
 * File HeikoTest1.java
 */

package alma.asdm;

import alma.asdmIDL.CorrelatorModeTableIDL;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.UniquenessViolationException;


/**
 * @author caillat
 * 
 */
public class HeikoTest1 {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out
                    .println("Usage: alma.asdm.TestReadXML1 dataSetDirectory");
            System.exit(0);
        } 

        String testdir = args[0];

        ASDM dataset = null;

        try {
            dataset = ASDM.getFromXML(testdir);
        } catch (ConversionException e1) {
            e1.printStackTrace();
        }
        System.out.println("Dataset read successfully.");
        System.out.println("Its AlmaCorrelatorMode table has " 
                           + dataset.getCorrelatorMode().size()
                           + " elements");
        
        CorrelatorModeTableIDL myAlmaCorrelatorMode = null;
        myAlmaCorrelatorMode = dataset.getCorrelatorMode().toIDL();

        ASDM newDataSet = new ASDM();
        CorrelatorModeTable newACT = newDataSet.getCorrelatorMode();
        
        try {
            newACT.fromIDL(myAlmaCorrelatorMode);
            System.out.println("After conversion back and forth to/from IDL, the AlmaCorrelatorTable has " +
                    newACT.size() + " rows");
            System.out.println(newACT.toXML());
        } catch (DuplicateKey e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ConversionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UniquenessViolationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
