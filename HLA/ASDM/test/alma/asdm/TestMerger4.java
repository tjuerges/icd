package alma.asdm;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.MergerException;

public class TestMerger4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		if (args.length < 1) {
			System.out.println("A command to merge a sequence of ASDM datasets. The result is exported on disk in 'dsMerged'");
			System.out.println("java  alma.asdm.TestMerger4 asdm ...");
			System.exit(-1);
		}
		
		ASDM[] ds = new ASDM[args.length];
		
		ds[0] = new ASDM();
		try {
			ds[0].setFromFile(args[0]);
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		System.out.println("Successfully read dataset '" + args[0] + "'." );
		
		Merger merger;
		for (int i = 1; i < ds.length; i++) {
			ds[i] = new ASDM();
			try {
				ds[i].setFromFile(args[i]);
			} catch (ConversionException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}		
			System.out.println("Successfully read dataset '" + args[i] + "'." );		
		
			System.out.println("About to merge '" + args[i]);
			merger = new Merger();
			try {
				merger.merge(ds[0], ds[i]);
			} catch (IllegalAccessException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			} catch (DuplicateKey e) {
				System.out.println(e.getMessage());
				System.exit(-1);	
			} catch (MergerException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}
		
			System.out.println("Successfully merged '" + args[1] + "' into '" + args[0] + "'.");
		}
		
		
		try {
			ds[0].toXML("dsMerged");
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		System.out.println("The dataset resulting from the merge has been exported into '"+ "dsMerged" + "'.");
	}

}
