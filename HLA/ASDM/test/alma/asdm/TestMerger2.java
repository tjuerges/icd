package alma.asdm;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.MergerException;
import alma.hla.runtime.asdm.types.*;

public class TestMerger2 {
	public void go(ASDM ds1, ASDM ds2) throws IllegalAccessException, DuplicateKey, MergerException {
		Merger merger = new Merger();
		merger.merge(ds1, ds2);
	}
	
	public TestMerger2() {
		;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Let's consider an ASDM initially empty.
		ASDM ds1 = new ASDM();
		
		// Let's merge into ds1 an ASDM  returned by TelCal for a Phase Calibration.
		ASDM dsPhaseCal = new ASDM();
		try {
			dsPhaseCal.setFromFile("phaseCalResult.sdm");
		} catch (ConversionException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		TestMerger2 tm = new TestMerger2();
		
		System.out.print("Before merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalPhase().size() + " CalPhase rows.");
		try {
			tm.go(ds1, dsPhaseCal);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (DuplicateKey e) {
			e.printStackTrace();
		} catch (MergerException e) {
			e.printStackTrace();
		}
		System.out.print("After merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalPhase().size() + " CalPhase rows.");
		
		// Let's merge into ds1 an ASDM  returned by TelCal for a Curve Calibration.
		ASDM dsCurveCal = new ASDM();
		try {
			dsCurveCal.setFromFile("phaseCurveResult.sdm");
		} catch (ConversionException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
		
		System.out.print("Before merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalCurve().size() + " CalCurve rows, " + ds1.getCalSeeing().size() + " CalSeeing rows.");
		try {
			tm.go(ds1, dsCurveCal);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (DuplicateKey e) {
			e.printStackTrace();
		} catch (MergerException e) {
			e.printStackTrace();
		}
		System.out.print("After merge ds1 has " );
		System.out.println(ds1.getCalData().size() + " CalData rows, " + ds1.getCalReduction().size() + " CalReduction rows and " + ds1.getCalCurve().size() + " CalCurve rows, " + ds1.getCalSeeing().size() + " CalSeeing rows." );
		
	}

}
