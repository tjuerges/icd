/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jun 6, 2005
 * File TestReadXML1.java
 */

package alma.asdm;

import java.io.File;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Entity;
import alma.hla.runtime.asdm.types.EntityId;


/**
 * This test reads the XML representation created in TestWriteXML.
 * 
 * @version 1.00 Jun 6, 2005
 * @author caillat
 */

public class TestReadXML1 {
 
        public static void main (String[] arg) {
            
            //String testdir = "asdm_0000000000000101_00000000";
            System.out.println(arg[0]);
            
            if (arg.length < 1) {
                System.out.println("Usage: alma.asdm.TestReadXML1 dataSetDirectory");
                System.exit(0);
            }
                  
            String testdir = arg[0];
 
            ASDM dataset = null;
            
            try {
                dataset = ASDM.getFromXML(testdir);
                System.out.println("Dataset read successfully.");
            } catch (ConversionException e1) {
                e1.printStackTrace();
                System.out.println("Aborting");
                System.exit(1);
            } 
        }
}
