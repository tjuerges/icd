package alma.asdm;

import alma.BasebandNameMod.BasebandName;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.asdmIDL.SpectralWindowRowIDL;
import alma.hla.runtime.asdm.ex.ConversionException;

public class TestReadASDM {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("java  alma.asdm.TestReadASDM asdm_directory");
			System.exit(-1);
		}
		
		ASDM dataset = new ASDM();
		
		try {
			dataset.setFromFile(args[0]);
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		System.out.println("Successfully read dataset" + args[0]);
	
		
		ASDMDataSetIDL datasetIDL = dataset.toIDL();
		
		SpectralWindowRowIDL spwIDL = datasetIDL.spectralWindow.row[0];
		System.out.println("BasebandName exists IDL =" + spwIDL.basebandNameExists);		
		System.out.println("BasebandName IDL =" + spwIDL.basebandName);		
	
	}
	
	

}
