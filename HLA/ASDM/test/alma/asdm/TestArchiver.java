/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Sep 12, 2005
 * File TestArchiver.java
 */

//package alma.asdm;

package alma.asdm;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.transform.stream.StreamSource;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Text;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import alma.hla.runtime.asdm.ex.ArchiverException;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.types.EntityId;
import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
import alma.xmlstore.IdentifierPackage.NotAvailable;

import alma.asdm.ASDM;
import alma.asdm.ArchiverCC;
import alma.asdm.Archiver;
import alma.hla.runtime.asdm.types.EntityId;

import org.jdom.*;
import org.jdom.input.*;
import org.xml.sax.InputSource;

/**
 * 
 * This class proposes a collection of methods to illustrate and test different operations
 * involving ASDM datasets, the ACS Archive and the plain filesystem. 
 * <br>Usage : 
 * <br> <code> cd &lt;..&>;/HLA/ICD/test
 * <br> <code> acsStartJava alma.asdm.TestArchiver <i>command</i> [<i>arg...</i>]
 * <br><br>
 * Its main method is actually
 * a basic command interpreter that expects a command possibly followed by one or many
 * arguments and then triggers an action depending on the command. The possible actions
 * are : <br>
 * 
 * <table border=1>
 * <tr>
 * <th>
 * Command.
 * </th>
 * <th>
 * Argument(s).
 * </th>
 * <th>
 * Description.
 * </th>
 * </tr>
 * <tr>
 * <td> retr </td> <td> <i>uid</i> </td> <td> Retrieves an ASDM Dataset from the archive given its
 * UID, displays a summary of the ASDM. </td>
 * </tr>
 * <tr>
 * <td> ar2disk </td><td> <i>uid</i> </td> <td> Retrieves an ASDM Dataset from the archive givent its
 * UID and stores the ASDM on disk. The name of directory under which the ASDM is saved is derived
 * from the UID by replacing of each "/" and ":"  by a character "_".
 * </td> 
 * </tr>
 * <tr>
 * <td> disk2ar </td> <td> <i>directory</i> </td> <td> Archives an "on disk" ASDM Dataset. <i>directory</i>
 * is the path of the directory containing the XML representation of the ASDM to archived. It should be noticed
 * that in that case, the allocation of the UID of the archived ASDM is performed inside the Archiver. 
 * It's useful when the client does not want to manage the UID allocation and prefer to delegate this task
 * to the Archiver.</td>
 * </tr>
 * <tr>
 * <td> storewithuid </td> <td> <i>directory</i> </td> <td> Archives an "on disk" ASDM Dataset. <i>directory</i>
 * is the path of the directory containing the XML representation of the ASDM to be archived. It should be noticed
 * that in that case, the allocation of the UID of the archived ASDM is performed outside the Archiver.It's used
 * when the client wants to decide which UID the ASDM is going to be assigned into the Archive.</td>
 * </tr>
 * <tr>
 * <td> uids </td> <td> <i>Schema-name</i> </td> <td> Returns the Archive UIDs of all documents having schema <i>Schema-name</i>
 * (like ASDM, SpectralWindowTable or AntennaTable and so on).</td>
 * </tr>
 * <tr>
 * <td>query</td> 
 * <td>
 * <i>XPath-expression</i> <i>Schema-name</i>
 * </td>
 * <td>
 * Presents a query (<i>XPath-expression</i>) to all XML documents stored in the Archive under schema <i>Schema-name</i>
 * (like ASDM, SpectralWindowTable or AntennaTable and so on).
 * Displays the results of the query on the standard output.
 * </td>
 * </tr>
 * <tr>
 * <td>dir</td> 
 * <td>
 * <i>uid</i>
 * </td>
 * <td>
 * Displays on standard output a summary of the ASDM stored into the Archive under UID <i>uid</i>.
 * </td>
 * </tr>
 * </tr>
 * </table>
 * 
 * @author caillat
 * 
 */
public class TestArchiver {
    static private ASDM dataset = null;

    static private class TableInfo {
        /**
         * @param name
         *            The name to set.
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @param numRows
         *            The numRows to set.
         */
        public void setNumRows(int numRows) {
            this.numRows = numRows;
        }

        /**
         * @param uid
         *            The uID to set.
         */
        public void setUID(String uid) {
            UID = uid;
        }

        /**
         * @return Returns the name.
         */
        public String getName() {
            return name;
        }

        /**
         * @return Returns the numRows.
         */
        public int getNumRows() {
            return numRows;
        }

        /**
         * @return Returns the uID.
         */
        public String getUID() {
            return UID;
        }

        /**
         * @param name
         * @param numRows
         * @param uid
         * @param containerUID
         */
        public TableInfo() {
            super();
            this.name = "";
            this.numRows = numRows;
            this.UID = "";
        }

        private String name;

        private int numRows;

        private String UID;
    }

    /**
     * @param arCC
     * @param id
     * @return
     */
    public static TableInfo[] dir(ArchiverCC arCC, EntityId id)
            throws Exception {
        // Retrieve the XML content with EntityId id.
        String xml = arCC.retrieve(id.toString());

        if (xml == null || xml.equals(""))
            return null;

        // And now parse it.

        // Firstly build a document from this xml Document.
        StringReader sr = new StringReader(xml);

        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(sr);

        // Prepare XPath expressions for what we are looking for.

        XPath numRowsPath = XPath.newInstance("/ASDM/Table/NumberRows/text()");
        XPath namePath = XPath.newInstance("/ASDM/Table/Name/text()");
        XPath UIDPath = XPath.newInstance("/ASDM/Table/Entity/@entityId");

        // Retrieve the nodes.
        List numRows = numRowsPath.selectNodes(doc);
        List name = namePath.selectNodes(doc);
        List UID = UIDPath.selectNodes(doc);

        // Some checking.
        if (!((name.size() == numRows.size())) || (name.size() == 0))
            throw new Exception(
                    "The content of this document does not represent an ASDM's container.");

        // Now builds an array of TableInfo
        TableInfo result[] = new TableInfo[name.size()];

        int i = 0;
        Iterator nameIter = name.iterator();
        while (nameIter.hasNext()) {
            result[i] = new TableInfo();
            result[i++].setName(((Text) nameIter.next()).getTextTrim());
        }

        i = 0;
        Iterator numRowsIter = numRows.iterator();
        while (numRowsIter.hasNext()) {
            result[i++].setNumRows(Integer.parseInt(((Text) numRowsIter.next())
                    .getTextTrim()));
        }

        i = 0;
        Iterator UIDIter = UID.iterator();
        while (UIDIter.hasNext()) {
            if (result[i].getNumRows() != 0)
                result[i++].setUID(((Attribute) UIDIter.next()).getValue());
            else
                result[i++].setUID("");
        }

        return result;
    }

    
    static public void storewithuid(ArchiverCC arCC, String directory) {
        try {
            dataset = ASDM.getFromXML(directory);
            System.out.println("Dataset read successfully.");
        } catch (ConversionException e1) {
            e1.printStackTrace();
            System.out.println("Aborting");
            System.exit(1);
        }

        System.out.println("About to archive a dataset");
        EntityId en = null;
        
        try {
            en = new EntityId(arCC.getArchiver().getID());
            dataset.toArchive(arCC.getArchiver(), en);
        } catch (ConversionException e2) {
            e2.printStackTrace();
            System.out.println("Aborting");
            System.exit(1);
        } catch (ArchiverException e2) {
            e2.printStackTrace();
            System.out.println("Aborting");
            System.exit(1);
        } catch (NotAvailable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("Aborting");
            System.exit(1);
        }
        System.out.println("Dataset successfully archived with archive UID :"
                + en.toString());

    }
    static public void disk2ar(ArchiverCC arCC, String directory) {
        try {
            dataset = ASDM.getFromXML(directory);
            System.out.println("Dataset read successfully.");
        } catch (ConversionException e1) {
            e1.printStackTrace();
            System.out.println("Aborting");
            System.exit(1);
        }

        System.out.println("About to archive a dataset");
        EntityId en = null;
        try {
            en = dataset.toArchive(arCC.getArchiver());
        } catch (ConversionException e2) {
            e2.printStackTrace();
        } catch (ArchiverException e2) {
            e2.printStackTrace();
        }
        System.out.println("Dataset successfully archived with archive UID :"
                + en.toString());

    }

    static public void uids(ArchiverCC arCC, String entityTypeName) {
        try {
            // Retrieve the UIDs of all the stored ASDMs.
            String asdmUIDS[] = arCC.getUIDs("/"+entityTypeName, entityTypeName);
            if (asdmUIDS.length == 0) {
                System.out.println("No ASDM found in the archive");
            } else {
                for (int i = 0; i < asdmUIDS.length; i++) {
                    System.out.println(asdmUIDS[i]);
                }
            }
        } catch (ArchiveException e3) {
            // TODO Auto-generated catch block
            e3.printStackTrace();
        }
    }

    public static String query(ArchiverCC arCC, String query, String schema) {
        return arCC.query(query, schema, 1000);
    }

    static ASDM retr(ArchiverCC arCC, EntityId datasetId) {
        try {
            return ASDM.fromArchive(arCC.getArchiver(), datasetId);
        } catch (ArchiverException e) {
            System.out
                    .println("TestArchiver.ret : exception while retrieving dataset. The message is "
                            + e.getMessage() + ". I return a null value");
        } catch (ConversionException e) {
            System.out
                    .println("TestArchiver.ret : exception while retrieving dataset. The message is "
                            + e.getMessage() + ". I return a null value");
        }
        return null;
    }
    
    static ASDM ar2disk(ArchiverCC arCC, EntityId datasetId) {
        ASDM ds = null;
        try {
            // Retrieve from the Archive
            ds = ASDM.fromArchive(arCC.getArchiver(), datasetId);
            String dirname = datasetId.toString().replaceAll(":","_").replaceAll("/","_");
            System.out.println("The dataset is going to be stored on disk in the directory "+dirname);
            // Store on disk.
            ds.toXML(dirname);
            return ds;
        } catch (ArchiverException e) {
            System.out
                    .println("TestArchiver.ar2disk : an exception occurred. The message is "
                            + e.getMessage() + ". I return a null value");
        } catch (ConversionException e) {
            System.out
                    .println("TestArchiver.ar2disk : an exception occurred. The message is "
                            + e.getMessage() + ". I return a null value");
        }
        return null;
    }

    public static void main(String[] arg) {
        System.out.println(arg[0]);

        if (arg.length < 1) {
            System.out.println("Usage: alma.asdm.TestArchiver command");
            System.exit(0);
        }

        ArchiverCC arCC = null;

        try {
            Logger logger = Logger.getLogger("Archiver");
            String managerLoc = System.getProperty("ACS.manager");
            if (managerLoc == null) {
                System.out
                        .println("Java property 'ACS.manager' must be set to the corbaloc of the ACS manager!");
                System.exit(-1);
            }
            arCC = new ArchiverCC(logger, managerLoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if (arg[0].equals("storewithuid")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver store directory");
                System.exit(-1);
            }
            storewithuid(arCC, arg[1]);
            // arCC.done();
        }

        
        if (arg[0].equals("disk2ar")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver disk2ar directory");
                System.exit(-1);
            }
            disk2ar(arCC, arg[1]);
            // arCC.done();
        }

        if (arg[0].equals("uids")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver uids entityTypeName");
                System.exit(-1);
            }
            uids(arCC, arg[1]);
            // arCC.done();
        }

        if (arg[0].equals("retr")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver retr ArchiveUID");
                System.exit(-1);
            }
            String msg = null;
            if ((msg = EntityId.validate(arg[1])) != null)
                System.out.println(msg);
            else
                dataset = retr(arCC, new EntityId(arg[1]));
        }
        
        if (arg[0].equals("ar2disk")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver ar2disk ArchiveUID");
                System.exit(-1);
            }
            String msg = null;
            if ((msg = EntityId.validate(arg[1])) != null)
                System.out.println(msg);
            else 
                dataset = ar2disk(arCC, new EntityId(arg[1]));
        }

        if (arg[0].equals("query")) {
            if (arg.length < 3) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver query XPath-expression Schema-name");
                System.exit(-1);
            }
            
            String answer = query(arCC, arg[1], arg[2]);
            // System.out.println(answer);
            if (answer != null && answer.trim().length() != 0) {
                SAXBuilder builder = new SAXBuilder();
                try {
                    Document doc = builder.build(new InputSource(new StringReader(answer)));
                    XMLOutputter outp = new XMLOutputter();
                    outp.output(doc, System.out);
                } catch (JDOMException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            else {
                System.out.println("The query "+arg[1] + " " + arg[2] + " produced no result");
            }
        }

        if (arg[0].equals("dir")) {
            if (arg.length < 2) {
                System.out
                        .println("Usage: alma.asdm.TestArchiver dir ArchiveUID");
                System.exit(-1);
            }
            String msg = null;
            if ((msg = EntityId.validate(arg[1])) != null)
                System.out.println(msg);
            else
                try {
                    TableInfo[] ti = dir(arCC, new EntityId(
                            new EntityId(arg[1])));
                    for (int i = 0; i < ti.length; i++)
                        System.out.println("Table name: " + ti[i].getName()
                                + ", number of rows: " + ti[i].getNumRows()
                                + ", archive uid:" + ti[i].getUID());
                } catch (Exception e1) {
                    System.out.println(e1.getMessage());
                }
        }
    }

}
