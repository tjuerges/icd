package alma.asdm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import alma.asdmIDLTypes.IDLTag;
import alma.hla.runtime.asdm.ex.TagFormatException;
import alma.hla.runtime.asdm.types.Tag;
import alma.hla.runtime.asdm.types.TagType;

public class TestTag {
	public static void tagUseCases(Tag t) {
		System.out.println("----------------------------------------");
		if ( t == null) {
			System.out.println ("Please give a non null Object");
		}
		// Output it
		System.out.println("Tag string representation : " + t.toString());

		// Is it a "null" Tag or not ?
		if (t.isNull()) {
			System.out.println("This tag is null");
		}
		else {
			System.out.println("This tag is not null");


			// Output its (integer) value.
			System.out.println("Its value is " + t.getTagValue());

			// Output its type.
			System.out.println("Its type is " + t.getTagType().toString());
		}

		// Convert it into IDLTag
		IDLTag tIDL = t.toIDLTag();

		// Convert back to a Tag and output the string representation of the conversion
		System.out.println("Tag obtained by conversion to and from IDl " + new Tag(tIDL).toString());

		// Convert it into a sequence of bytes.
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);

		try {
			t.toBin(dos);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Output the sequence of bytes.
		byte[] tByte = bos.toByteArray();
		System.out.print("Sequence of bytes obtained by serialization of the Tag :" );
		for (int i = 0; i < tByte.length; i++)
			System.out.print(tByte[i]+ " ");
		System.out.println();

		// Converts the sequence of bytes back into a Tag and output the result of the conversion.
		ByteArrayInputStream bis = new ByteArrayInputStream(tByte);
		DataInputStream dis = new DataInputStream(bis);

		try {
			System.out.println("Tag obtained by deserialization " + Tag.fromBin(dis).toString());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TagFormatException e) {
			e.printStackTrace();
		}
	}	
	public static void main(String[] args) {

		// Let's create a Holography Tag.
		Tag holoTag = new Tag (2, TagType.Holography);
		tagUseCases(holoTag);

		// Let's create a NoType Tag.
		Tag noTypeTag = new Tag(27);		
		tagUseCases(noTypeTag);

		// Let's creat a null Tag (following the terminology introduced by Allen Farris).
		Tag nullTag = new Tag();
		tagUseCases(nullTag);

	}
}
