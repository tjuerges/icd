/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jul 31, 2005
 * File TestReceiver1.java
 */

package alma.asdm;
import java.io.File;

import alma.ReceiverBandMod.ReceiverBand;
import alma.ReceiverSidebandMod.ReceiverSideband;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.NoSuchRow;
import alma.hla.runtime.asdm.types.*;
/**
 * @author caillat
 * 
 */
public class TestReceiver1 {

public static void main(String[] args) {
        ASDM dataset = new ASDM();
        ReceiverTable  receiver = dataset.getReceiver();
        
        // Prepare one dummy receiver
        ArrayTimeInterval timeInterval = new ArrayTimeInterval(10.0);
        Tag spectralWindowId = new Tag(0);
        int      numLO         = 2;
        String   receiverName  = "RX_03_94";
        String   dewarName     = "DEWAR15";
        ReceiverBand   frequencyBand  = ReceiverBand.ALMA_RB_01;
        Frequency freqLO[] = new Frequency[2];
        freqLO[0]= new Frequency(456789000000.000000);
        freqLO[1]= new Frequency(-4000000000.000000);
        ReceiverSideband receiverSideband = ReceiverSideband.NOSB;
 
        int       sidebandLO[] = {1,2};
        Temperature  dewarTemperature = new Temperature(10.);
        double            stability     = 1.E-4;    
        long              stabDuration  = 123456789;
        boolean           stabilityFlag = false;
        int               receiverId;
        ReceiverRow  retReceiverRow = null;
        
        ReceiverRow receiverRow = receiver.newRow( spectralWindowId,                  
                   timeInterval,
                   numLO, 
                   receiverName,
                   frequencyBand, 
                   freqLO,
                   	receiverSideband,
                   sidebandLO,
                   dewarTemperature, 
                   new Interval(stabDuration),
                   stability
                   );
        retReceiverRow = receiver.add(receiverRow);
        
        receiverId = receiverRow.getReceiverId();
        if(retReceiverRow==receiverRow)
          System.out.println("New row added with receiverId="+receiverId);
        else
          System.out.println("Row already present with same value using receiverId="+receiverId);


        receiverName  = "RX_03_11"; 
        receiverRow = receiver.newRow( spectralWindowId,                    
                     timeInterval,
                     numLO, 
                     receiverName,
                     frequencyBand, 
                     freqLO,
                     receiverSideband,
                     sidebandLO,
                     dewarTemperature, 
                     new Interval(stabDuration),
                     stability
                     );
        retReceiverRow = receiver.add(receiverRow);
        receiverId = receiverRow.getReceiverId();
        if(retReceiverRow==receiverRow)
          System.out.println("New row added with receiverId="+receiverId);
        else
          System.out.println("Row already present with same value using receiverId="+receiverId);

        receiverName  = "RX_03_94"; 
        receiverRow = receiver.newRow( spectralWindowId,                    
                     timeInterval,
                     numLO, 
                     receiverName,
                     frequencyBand, 
                     freqLO,
                     receiverSideband,
                     sidebandLO,
                     dewarTemperature, 
                     new Interval(stabDuration),
                     stability
                     );
        retReceiverRow = receiver.add(receiverRow);
        receiverId = receiverRow.getReceiverId();
        if(retReceiverRow==receiverRow)
          System.out.println("New row added with receiverId="+receiverId);
        else
          System.out.println("Row already present with same value using receiverId="+receiverId);

        // Write out the dataset in XML documents.
        try {
            java.util.Random r = new java.util.Random();
            int dummy = r.nextInt();
            File dir = new File("/tmp/asdm_" + Math.max(dummy, -dummy));
            if (!dir.mkdir())
                throw new ConversionException("Could not make directory "
                        + dir.getAbsolutePath(), null);
            dataset.toXML(dir.getAbsolutePath());
            System.out.println("Dataset written to " + dir.getAbsolutePath());
        } catch (ConversionException e) {
            e.printStackTrace();
        }      
    }
}
