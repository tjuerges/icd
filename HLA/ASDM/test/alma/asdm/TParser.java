/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TParser.java
 */
package alma.asdm;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.types.Entity;

/**
 * description
 * 
 * @version 1.00 Dec 29, 2004
 * @author Allen Farris
 */
public class TParser {

	static private void error(int n) {
		System.out.println("error! [" + n + "]");
		System.exit(0);
	}
	public static void main (String[] arg) {
		
		String test1DString = "<test> 1 2 \"string 0\" \"string 1\" </test>";
		try {
			String[] value1 = Parser.get1DString("test","testtable",test1DString);
			for (int i = 0; i < value1.length; ++i)
				System.out.println("[" + i + "] " + value1[i]);
		} catch (ConversionException e) {
			e.printStackTrace();
		}
		
		String test2DString = "<test> 2 2 2 \"string 00\" \"string 01\" \"string 10\" \"string 11\" </test> ";
		try {
			String[][] value2 = Parser.get2DString("test","testtable",test2DString);
			for (int i = 0; i < value2.length; ++i)
				for (int j = 0; j < value2[i].length; ++j)
					System.out.println("[" + i + "," + j + "] " + value2[i][j]);		
		} catch (ConversionException e) {
			e.printStackTrace();
		}
		
		String test3DString = "<test> 3 2 2 2 \"string 000\" \"string 001\" \"string 010\" \"string 011\" \"string 100\" \"string 101\" \"string 110\" \"string 111\" </test> ";
		try {
			String[][][] value3 = Parser.get3DString("test","testtable",test3DString);
			for (int i = 0; i < value3.length; ++i)
				for (int j = 0; j < value3[i].length; ++j)
					for (int k = 0; k < value3[i][j].length; ++k)
						System.out.println("[" + i + "," + j + "," + k + "] " + value3[i][j][k]);
		} catch (ConversionException e) {
			e.printStackTrace();
		}
		
		String test1DInt = "<test> 1 5 98 76 54 32 10 </test>";
		try {
			int[] value4 = Parser.get1DInteger("test","testtable",test1DInt);
			for (int i = 0; i < value4.length; ++i)
				System.out.println("[" + i + "] " + value4[i]);
		} catch (ConversionException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		String test2DInt = "<test> 2 2 5 98 76 54 32 10 12 34 56 78 90 </test>";
		try {
			int[][] value4b = Parser.get2DInteger("test","testtable",test2DInt);
			for (int i = 0; i < value4b.length; ++i)
				for (int j = 0; j < value4b[i].length; ++j)
					System.out.println("[" + i + ", " + j + "] " + value4b[i][j]);
		} catch (ConversionException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		String test3DInt = "<test> 3 2 5 1 98 76 54 32 10 12 34 56 78 90 </test>";
		try {
			int[][][] value4c = Parser.get3DInteger("test","testtable",test3DInt);
			for (int i = 0; i < value4c.length; ++i)
				for (int j = 0; j < value4c[i].length; ++j)
					for (int k = 0; k < value4c[i][j].length; ++k)
						System.out.println("[" + i + ", " + j + ", " + k + "] " + value4c[i][j][k]);
		} catch (ConversionException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		String e1 = "<Entity entityId=\"uid://X0000000000000079/X00000000\" entityIdEncrypted=\"na\" entityTypeName=\"FeedTable\" schemaVersion=\"1\" documentVersion=\"1\"/>";
		String e2 = "<Entity entityId=\"uid://X0000000000000080/X00000000\" entityIdEncrypted=\"na\" entityTypeName=\"FeedTable\" schemaVersion=\"1\" documentVersion=\"1\"/>";
		String en = "<test> 1 2 " + e1 + " " + e2 + " </test>";
		try {
			Entity[] value5 = Parser.get1DEntity("test","testtable",en);
			for (int i = 0; i < value5.length; ++i)
				System.out.println("[" + i + "] " + value5[i]);
		} catch (ConversionException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		
		String test1DChar = "<test> 1 26 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z </test>";
		try {
			char[] value6 = Parser.get1DCharacter("test","testtable",test1DChar);
			for (int i = 0; i < value6.length; ++i)
				System.out.println("[" + i + "] " + value6[i]);
		} catch (ConversionException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
		
	}

}
