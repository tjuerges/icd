/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestReadXML.java
 */
package alma.asdm;

import java.io.File;

import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.types.Entity;
import alma.hla.runtime.asdm.types.EntityId;

/**
 * This test reads the XML representation created in TestWriteXML.
 * It then modifies the dataset EntityId and writes out the
 * dataset.  The written files should be identical to those
 * in the input directory.
 * 
 * @version 1.00 Dec 27, 2004
 * @author Allen Farris
 */
public class TestReadXML {

	public static void main (String[] arg) {
		
		String testdir = "asdm_0000000000000101_00000000";
		ASDM dataset = null;
		
		try {
			dataset = ASDM.getFromXML(testdir);
		} catch (ConversionException e1) {
			e1.printStackTrace();
		}
		System.out.println("Dataset read successfully.");
		
		// Change the EntityId.
		Entity ee = dataset.getEntity();
		ee.setEntityId(new EntityId("uid://X0000000000000102/X00000000"));
		dataset.setEntity(ee);
		
		// Write out the dataset.
		try {
			String id = dataset.getEntity().getEntityId().toString();
			id = id.substring(7,23) + "_" + id.substring(25,33);
			File dir = new File ("asdm_" + id);
			if (!dir.mkdir())
				throw new ConversionException ("Could not make directory " + dir.getAbsolutePath(),null);
			dataset.toXML(dir.getAbsolutePath());
			System.out.println("Dataset written to " + dir.getAbsolutePath());
		} catch (ConversionException e) {
			e.printStackTrace();
		}
		System.out.println("Dataset written successfully.");
		
	}
}
