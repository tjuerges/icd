/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Created on Jun 6, 2005
 * File TestArrayTime.java
 */

package alma.asdm;

import alma.hla.runtime.asdm.types.ArrayTime;

/**
 * @author caillat
 *
 */
public class TestArrayTime {

    public static void main(String[] args) {
        java.util.Calendar rightNow = java.util.Calendar.getInstance();
        ArrayTime at = new ArrayTime(rightNow.get(java.util.Calendar.YEAR),
                rightNow.get(java.util.Calendar.MONTH),
                rightNow.get(java.util.Calendar.DAY_OF_MONTH),
                rightNow.get(java.util.Calendar.HOUR_OF_DAY),
                rightNow.get(java.util.Calendar.MINUTE),
                (double)rightNow.get(java.util.Calendar.SECOND));
        
        
        System.out.println("Today in FITS format " + at.toFITS());
        System.out.println("Today in nanoseconds " +at.get());
        System.out.println("Today as a string " + at.toString());
    }
}
