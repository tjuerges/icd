package alma.asdm;

import java.io.IOException;
import java.util.logging.Logger;

import alma.CorrelationBitMod.CorrelationBit;
import alma.HolographyChannelTypeMod.HolographyChannelType;
import alma.NetSidebandMod.NetSideband;
import alma.PolarizationTypeMod.PolarizationType;
import alma.SidebandProcessingModeMod.SidebandProcessingMode;
import alma.StokesParameterMod.StokesParameter;
import alma.WindowFunctionMod.WindowFunction;
import alma.asdm.*;
import alma.hla.runtime.asdm.ex.ArchiverException;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.types.Angle;
import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.ArrayTimeInterval;
import alma.hla.runtime.asdm.types.EntityId;
import alma.hla.runtime.asdm.types.Frequency;
import alma.hla.runtime.asdm.types.Interval;
import alma.hla.runtime.asdm.types.Length;
import alma.hla.runtime.asdm.types.Tag;
import alma.hla.runtime.asdm.types.TagType;

public class TestASDMFilePerf {
	static final int BLOCKSIZE = 1024;
	
	public static ASDM buildASDM(int numBlock) throws DuplicateKey {
		// Create a new and empty ASDM
		ASDM dataset = new ASDM();
		
		// Populate its Holography table with one row
		HolographyTable holoT = dataset.getHolography();
		HolographyRow holoR = null;
		
		//String typeHolo[] =  {"SS", "RR", "QQ", "SR", "SQ", "QR"};
		HolographyChannelType typeHolo[] = new HolographyChannelType[6];
		typeHolo[0] = HolographyChannelType.S2;
		typeHolo[1] = HolographyChannelType.R2;
		typeHolo[2] = HolographyChannelType.Q2;
		typeHolo[3] = HolographyChannelType.RS;
		typeHolo[4] = HolographyChannelType.QS;
		typeHolo[5] = HolographyChannelType.QR;		
		
		Length distance= new Length(1000.0);
		Length focus = new Length(10.0);
		boolean flagRow = false;

		
		holoR = holoT.newRow(typeHolo.length, typeHolo, distance, focus, flagRow);
		holoT.add(holoR);
		System.out.println( "The dataset has " + holoT.size() + " rows in its Holography table" );
		
		// Populate its Polarization table with one row
		PolarizationTable polT = dataset.getPolarization();
		PolarizationRow polR = null;
		int numCorrPol = 4;
		//int typePol[] = { 9, 10, 11, 12 };
		StokesParameter typePol[] = new StokesParameter[numCorrPol];
		typePol[0] = StokesParameter.RR;
		typePol[1] = StokesParameter.LL;
		typePol[2] = StokesParameter.RL;
		typePol[2] = StokesParameter.LR;
		
		//int productPol[][] = {{0, 0}, {0,1}, {1,0}, {1,1}};
		PolarizationType productPol[][] = new PolarizationType[numCorrPol][2];
		productPol[0][0] = PolarizationType.R;
		productPol[0][1] = PolarizationType.R;	
		
		productPol[1][0] = PolarizationType.R;
		productPol[1][1] = PolarizationType.L;
				
		productPol[2][0] = PolarizationType.L;
		productPol[2][1] = PolarizationType.R;
		
		productPol[3][0] = PolarizationType.L;
		productPol[3][1] = PolarizationType.L;
				
					
		polR = polT.newRow(typePol.length, typePol, productPol);
		polT.add(polR);
		System.out.println("The dataset has " + polT.size() + " rows in its Polarization table" );	
		
		// Populate its SpectralWindow table with one row
		SpectralWindowTable spwT = dataset.getSpectralWindow();
		SpectralWindowRow spwR = null;
		
		int numChan = 16;
		Frequency refFreq = new Frequency(1.0e9);
		Frequency chanFreq[] = new Frequency[numChan];
		Frequency chanWidth[] = new Frequency[numChan];
		for (int i = 0; i < numChan; i++) {
			chanFreq[i] = new Frequency(1.0e9 + i*1.0e8);
			chanWidth[i] = new Frequency(1.0e8);
		}

		Frequency effectiveBw[] = new Frequency[numChan];
		Frequency resolution[] = new Frequency[numChan];
		for (int i = 0; i < numChan; i++) {
			effectiveBw[i] = new Frequency(1.0e9 + i*2.0e8);
			resolution[i] = new Frequency(2.0e8);
		}
		Frequency totBandwidth = new Frequency(numChan * 1.0e8);
		NetSideband  netSideband = NetSideband.NOSB;
		SidebandProcessingMode sidebandProcessingMode = SidebandProcessingMode.NONE;
		boolean quantization  = false;
		WindowFunction windowFunction = WindowFunction.UNIFORM;
		boolean oversampling = false;
		CorrelationBit correlationBit = CorrelationBit.BITS_2x2;
		flagRow = false;
		
		spwR = spwT.newRow(numChan,  refFreq, chanFreq, chanWidth,  effectiveBw, resolution, totBandwidth, netSideband,  sidebandProcessingMode, quantization, windowFunction, oversampling, correlationBit, flagRow);
		spwT.add(spwR);
		System.out.println("The dataset has " + spwT.size() + " rows in its SpectralWindow table" );			
		
		// Populate its DataDescription table
		DataDescriptionTable ddT = dataset.getDataDescription();
		DataDescriptionRow ddR = null;

		// Here is an association to an  HolographyRow and a SpectralWindow.
		System.out.println("Adding a row with a link to the Holography table to the DataDescription table." );
		ddR = ddT.newRow(holoR.getHolographyId(), spwR.getSpectralWindowId());
		ddT.add(ddR);
		System.out.println("Here is its holographyId :" + ddR.getPolOrHoloId().toString() );	
		
		// Here is an association  to a PolarizationRow and the same SpectralWindowRow
		System.out.println("Adding a row with a link to the Polarization table to the DataDescription table." );		
		ddR = ddT.newRow(polR.getPolarizationId(), spwR.getSpectralWindowId());
		ddT.add(ddR);
		System.out.println("Here is its polarizationId :" + ddR.getPolOrHoloId().toString() );
		
		System.out.println("The dataset has " +ddT.size() +" rows in its DataDescription table" );
		DataDescriptionRow[] rows = ddT.get();
		for ( int i = 0; i < ddT.size(); i++) {
			System.out.println("The DataDescription row #" +i +" is associated to a "+rows[i].getPolOrHoloId().getTagType().toString() );
		}
		
		// Populate its Pointing table with BLOCKSIZE * numBlock rows.
		PointingTable pointingT = dataset.getPointing();
		PointingRow aRow = null;

		Angle[][] pointingDirection = new Angle[1][2];
		pointingDirection[0][0] = new Angle(1.0);
		pointingDirection[0][1] = new Angle(3.0);

		Angle[][] target = new Angle[1][2];
		target[0][0] = new Angle(2.0);
		target[0][1] = new Angle(4.0);

		Angle offset[][] = new Angle[1][2];
		offset[0][0] = new Angle(-1.);
		offset[0][1] = new Angle(1.);
		
		Angle[] encoder = new Angle[2];
		encoder[0] = new Angle(5.0);
		encoder[1] = new Angle(6.0);

		long beginFillPointing = System.currentTimeMillis();
		try {
			for (long i = 0; i < numBlock; i++) {
				for (long j = 0; j < BLOCKSIZE; j++) {
					aRow = pointingT.newRow(new Tag(1, TagType.Antenna),
							new ArrayTimeInterval(
									(i * BLOCKSIZE + j) * 40000000,
									(long) 40000000), 1, 0,
									new ArrayTime(
											(i * BLOCKSIZE + j) * 40000000),
											pointingDirection, target, offset,  encoder, true);
					pointingT.add(aRow);
				}
			}
		} catch (java.lang.OutOfMemoryError e) {
			System.out.println(e.getMessage());
			System.out
			.println("Increase the memory of your JVM (export JAVA_OPTIONS=Xmx1024m)");
			System.exit(-1);
		}
		System.out.println("The dataset has " +pointingT.size() +" rows in its Pointing table" );

		// Populate its TotalPower table.
		TotalPowerTable tpmT = dataset.getTotalPower();
		TotalPowerRow aTPMRow = null;

		// In that section we define a set of constant values given to the attributes of all the rows of the TPM table except the time attribute.
		int numAntenna   = 1;
		int numBaseband = 1;
		int numCorr = 6;

		Tag configDescriptionId = new Tag(1, TagType.ConfigDescription);
		Tag fieldId = new Tag(1, TagType.Field);

		Tag execBlockId = new Tag(1, TagType.ExecBlock);
		Tag  stateId [] = new Tag[numAntenna]; 
		stateId[0] = new Tag(1, TagType.State);

		int scanNumber = 1;
		int subscanNumber = 1;
		int integrationNumber = 1;

		Length uvw[][] = new Length[numAntenna][ 3];
		for (int i = 0; i < numAntenna;  i ++)
			for (int j = 0; j < 3; j++)	
				uvw[i][j] = new Length(1000.0*numAntenna + 100 * j + i);

		Interval exposure[][] = new Interval[numAntenna][numBaseband];
		ArrayTime timeCentroid[][] = new ArrayTime[numAntenna][numBaseband];
		for (int i = 0; i < numAntenna; i++)
			for (int j = 0; j < numBaseband; j++) {
				exposure[i][j] = new Interval (1000000 * numAntenna + numBaseband);
				timeCentroid[i][j] =  new ArrayTime(2000000 * numAntenna + numBaseband);
			}

		float[][][] floatData = new float[numAntenna][numBaseband][numCorr];
		for (int i = 0; i < numAntenna; i++)
			for (int j = 0; j < numBaseband; j++)
				for (int k = 0; k < numCorr; k++)
					floatData[i][j][k] = numAntenna * 1000.0f + numBaseband*100.f + numCorr * 1.0f;

		int flagAnt[] = new int[numAntenna];
		for (int i = 0; i < numAntenna; i++)
			flagAnt[i] = 0;

		int flagPol[][] = new int[numAntenna][numCorr];
		for (int i = 0; i < numAntenna; i++)
			for (int j = 0; j < numCorr; j++)
				flagPol[i][i] = 0;

		flagRow = false;

		int subintegrationNumber = 10;
		Interval interval  = new Interval(400000);
		long beginFillTPM = System.currentTimeMillis();
		try {
			for (long i = 0; i < numBlock; i++) {
				for (long j = 0; j < BLOCKSIZE; j++) {
					aTPMRow = tpmT.newRow(configDescriptionId, fieldId, new ArrayTime((i * BLOCKSIZE + j) * 40000000), execBlockId, stateId, scanNumber, subscanNumber, integrationNumber, uvw, exposure, timeCentroid, floatData, flagAnt, flagPol, flagRow, interval);
					aTPMRow.setSubintegrationNumber(subintegrationNumber);
					tpmT.add(aTPMRow);
				}
			}
		} catch (java.lang.OutOfMemoryError e) {
			System.out.println(e.getMessage());
			System.out.println("Increase the memory of your JVM (export JAVA_OPTIONS=Xmx1024m)");
			System.exit(-1);
		} 

		System.out.println("The dataset has " + tpmT.size() + " rows in its TotalPower table" );
		return dataset;
	}
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, DuplicateKey, ConversionException, InterruptedException {
		
		if (args.length < 1) {
			System.out.println("Usage : TestASDMFilePerf <numblock> ");
			System.exit(-1);
		}
		int numBlock = 0;
		try {
			numBlock = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Invalid value for numblock (" + e.getMessage()
					+ ")");
			System.exit(-1);
		}

		
		// Build a dummy dataset
		System.out.println("About to build a dummy dataset");
		long beginCreateDataSet = System.currentTimeMillis();
		ASDM dataset = buildASDM(numBlock);
		System.out.println( "Built a dummy dataset (" + ( System.currentTimeMillis() -  beginCreateDataSet) / 1000.0 + " s.)");
		
		//		 Let's export it on disk.
		System.out.println("About to export the dataset on disk");
		long beginDataSetToFile  = System.currentTimeMillis();
		try {
			dataset.toFile("/tmp/TestASDMFile-java");
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		System.out.println( "Dataset exported on disk (" + ( System.currentTimeMillis() -  beginDataSetToFile) / 1000.0 + " s.)"); 

		// Let's define a dataset from its file storage.
		ASDM datasetBis = new ASDM();
		try {
			System.out.println( "About to set a dataset from its disk files." );	
			long beginFileToDataSet =  System.currentTimeMillis();
			datasetBis.setFromFile("/tmp/TestASDMFile-java");			
			System.out.println( "Dataset read and set ("  + (System.currentTimeMillis() -  beginFileToDataSet) / 1000.0 + " s.)");			
		}
		catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		System.out.println("The dataset read from disk has : " );
		System.out.println(" " + datasetBis.getDataDescription().size() + " rows in its DataDescriptionTable" );
		System.out.println(" " + datasetBis.getSpectralWindow().size() + " rows in its DescriptionTable" );
		System.out.println(" " + datasetBis.getHolography().size() + " rows in its Holography Table" );
		System.out.println(" " + datasetBis.getPolarization().size() + " rows in its Polarization Table" );
		System.out.println(" " + datasetBis.getPointing().size() + " rows in its Pointing Table" );
		System.out.println(" " + datasetBis.getTotalPower().size() + " rows in its TotalPower Table" );		
	}
}
