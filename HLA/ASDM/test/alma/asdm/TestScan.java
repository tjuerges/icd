package alma.asdm;
import alma.asdm.ASDM;
import alma.asdm.ScanRow;
import alma.asdm.ScanTable;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.hla.runtime.asdm.ex.ConversionException;
import alma.hla.runtime.asdm.ex.DuplicateKey;
import alma.hla.runtime.asdm.ex.UniquenessViolationException;
import alma.hla.runtime.asdm.types.TagType;
import alma.hla.runtime.asdm.types.Tag;
import alma.hla.runtime.asdm.types.ArrayTime;


import alma.hla.datamodel.enumeration.JScanIntent;
import alma.ScanIntentMod.ScanIntent;


public class TestScan {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ASDM dataset = new ASDM();
		
		ScanTable scanT = dataset.getScan();
		ScanRow scanR = null;
		
		Tag execBlockId = new Tag(0, TagType.ExecBlock);
		int scanNumber = 1;
		
		ArrayTime startTime= new ArrayTime(1000000L);
		ArrayTime endTime= new ArrayTime(1000000L);
		
		int numSubscan = 10;
		ScanIntent[] scanIntent = {ScanIntent.FOCUS, ScanIntent.HOLOGRAPHY, ScanIntent.SKYDIP, ScanIntent.TARGET};
		
		boolean flagRow = false;
		
		scanR = scanT.newRow(execBlockId, scanNumber, startTime, endTime, numSubscan, scanIntent.length, scanIntent, flagRow);
		try {
			scanT.add(scanR);
		} catch (DuplicateKey e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		
		System.out.println("The Scan table of the original dataset has " + scanT.size()  + " row(s)");
		System.out.println("The scanIntent of this row is ");
		for (int i = 0; i < scanIntent.length; i++)
			System.out.print(" "+JScanIntent.name(scanIntent[i]));
		System.out.println();
		
		// Convert the ASDM to IDL
		ASDMDataSetIDL datasetIDL = dataset.toIDL();
		
		// Convert back from IDL
		ASDM dataset2 = new ASDM();
		try {
			dataset2.fromIDL(datasetIDL);
		} catch (DuplicateKey e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		} catch (UniquenessViolationException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		
		// Consider the Scan table of this dataset.
		ScanTable scanT2 = dataset2.getScan();
		System.out.println("The Scan table of the from IDL dataset has " + scanT2.size()  + " row(s)");
		ScanRow scanR2 = scanT2.get()[0];		
		System.out.println("The scanIntent of this row is ");
		ScanIntent[] scanIntent2 = scanR2.getScanIntent();
		for (int i = 0; i < scanIntent2.length; i++)
			System.out.print(" "+JScanIntent.name(scanIntent2[i]));
		System.out.println();			
		
		
		// Convert the ASDM to XML in the filesystem.
		try {
			dataset.toFile("/tmp/TestScanJ.sdm");
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		
		// Convert back from XML.
		ASDM dataset3 = new ASDM();
		
		try {
			dataset3.setFromFile("/tmp/TestScanJ.sdm");
		} catch (ConversionException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		
		// Consider the Scan table of this dataset.
		ScanTable scanT3 = dataset3.getScan();
		System.out.println("The Scan table of the from IDL dataset has " + scanT3.size()  + " row(s)");
		ScanRow scanR3 = scanT3.get()[0];		
		System.out.println("The scanIntent of this row is ");
		ScanIntent[] scanIntent3 = scanR3.getScanIntent();
		for (int i = 0; i < scanIntent3.length; i++)
			System.out.print(" "+JScanIntent.name(scanIntent3[i]));
		System.out.println();				
	}

}
