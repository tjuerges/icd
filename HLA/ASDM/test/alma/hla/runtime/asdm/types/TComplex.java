/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TComplex.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.types.Complex;

/**
 * Test of types.Complex
 * 
 * @version 1.00 Nov 9, 2004
 * @author Allen Farris
 */
public class TComplex {

	public static void main (String[] arg) {
		Complex c = new Complex();
		System.out.println("Is c zero? " + c.isZero());
		c = new Complex (12.34,56.78);
		System.out.println("Is c zero? " + c.isZero());
		System.out.println("c = " + c);
		System.out.println("c-bar = " + Complex.conjugate(c));
		Complex d = new Complex("56.78 12.34");
		System.out.println("d = " + d);
		Complex x = new Complex (c);
		System.out.println("x = " + x);
		System.out.println("d = " + d.getReal() + " + i" + d.getImg());
		System.out.println("c == d " + c.equals(d));
		System.out.println("c == c " + c.equals(c));
		System.out.println("c + d = " + Complex.add(c,d));
		System.out.println("c - d = " + Complex.sub(c,d));
		System.out.println("c * d = " + Complex.mult(c,d));
		System.out.println("c / d = " + Complex.div(c,d));
		d.setReal(1.0);
		d.setImg(2.0);
		System.out.println("d = " + d);
	}	
}
