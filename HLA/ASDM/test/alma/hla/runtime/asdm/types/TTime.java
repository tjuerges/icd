/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TTime.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.types.ArrayTime;

import java.io.File;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Test of the types.Time.
 * 
 * @version 1.00 Nov 9, 2004
 * @author Allen Farris
 */
public class TTime {
	static private PrintStream out; 
	
	public static void display (ArrayTime t, PrintStream out) {
		out.println("Time:            " + t.toFITS());
		out.println("    units        " + t.get());
		out.println("    JD           " + t.getJD());
		out.println("    MJD          " + t.getMJD());
		out.println("    units        " + t.get());
		out.println("    time of day  " + t.getTimeOfDay());
		out.println("    time of day  " + t.timeOfDayToString());
		out.println("    day of week  " + t.getDayOfWeek());
		out.println("    day of year  " + t.getDayOfYear());
		out.println("    GMST         " + t.getGreenwichMeanSiderealTime());
		out.println("    LST at VLA   " + t.getLocalSiderealTime(4.484071979167));
	}
	
	public static void main (String[] arg) {
		System.out.println("Test of types.Time class.");
		// Create the output file.
		out = null;
		try {
			// Create the output text file.
			File outFile = new File("test","outTime.txt");
			out = new PrintStream (new FileOutputStream (outFile));
		} catch (IOException ioerr) {
			ioerr.printStackTrace();
			System.exit(0);
		}
		///////////////////////////////////////////////////////////////////
		
		ArrayTime base = new ArrayTime (1582,10,15,0,0,0.0); display(base,out);
		ArrayTime mjdBase = new ArrayTime (1858,11,17,0,0,0.0); display(mjdBase,out);
		ArrayTime x = new ArrayTime (2004,11,12,4,5,6.123456789); display(x,out);
		/**/
		x = new ArrayTime (1987,04,10,19,21,0.0); display(x,out);
		x = new ArrayTime (1987,04,10,0,0,0.0); display(x,out);
		x = new ArrayTime (1957,10,4.81); display(x,out);
		x = new ArrayTime (2000,1,1.5); display(x,out);
		x = new ArrayTime (1999,1,1.0); display(x,out);
		x = new ArrayTime (1987,1,27.0); display(x,out);
		x = new ArrayTime (1987,6,19.5); display(x,out);
		x = new ArrayTime (1988,1,27.0); display(x,out);
		x = new ArrayTime (1988,6,19.5); display(x,out);
		x = new ArrayTime (1900,1,1.0); display(x,out);
		x = new ArrayTime (1600,1,1.0); display(x,out);
		x = new ArrayTime (1600,12,31.0); display(x,out);
		x = new ArrayTime (1970,1,1.0); display(x,out);
		x = new ArrayTime (1957,10,4,19,26,24.0); display(x,out);
		x = new ArrayTime (2000,1,1,12,0,0.0); display(x,out);
		x = new ArrayTime (2004,12,14,16,2,45.67); display(x,out);
		x = new ArrayTime (4609756965670000000L); display(x,out);
		x = new ArrayTime(44239.0); display(x,out);
		x = new ArrayTime(44786.0); display(x,out);
		/**/
		x = new ArrayTime(Long.MAX_VALUE); display(x,out);

		x = new ArrayTime (2000,1,1,0,0,0.0); display(x,out);
		
		/**/
		double jdbase = ArrayTime.unitToJD(0L);
		double mjdbase = ArrayTime.unitToMJD(0L);
		long unitBase = ArrayTime.jdToUnit(2299160.5);
		long unitMBase = ArrayTime.mjdToUnit(-100840);
		out.println("jdbase = " + jdbase);
		out.println("mjdbase = " + mjdbase);
		out.println("unitBase = " + unitBase);
		out.println("unitMBase = " + unitMBase);
		
		
		
		x = new ArrayTime (0.0);
		out.println("x = " + x);
		ArrayTime y = new ArrayTime (-100840.0);
		out.println("y = " + y);
		
		ArrayTime z = new ArrayTime ();
		out.println("1. z = " + z + " " + z.toFITS());
		z = new ArrayTime ("1858-11-17 00:00:00");
		out.println("2. z = " + z + " " + z.toFITS());
		z = new ArrayTime ("0.0");
		out.println("3. z = " + z + " " + z.toFITS());
		z = new ArrayTime ("0");
		out.println("4. z = " + z + " " + z.toFITS());
		z = new ArrayTime (mjdBase);
		out.println("5. z = " + z + " " + z.toFITS());
		z = new ArrayTime (1858,11,17.5);
		out.println("6. z = " + z + " " + z.toFITS());
		z = new ArrayTime (1858,11,17,12,0,0.0);
		out.println("7. z = " + z + " " + z.toFITS());
		z = new ArrayTime (0.0);
		out.println("8. z = " + z + " " + z.toFITS());
		z = new ArrayTime (0L);
		out.println("9. z = " + z + " " + z.toFITS());
		
		
		///////////////////////////////////////////////////////////////////
		out.close();
		System.out.println("End test.");
		
	}
		
}
