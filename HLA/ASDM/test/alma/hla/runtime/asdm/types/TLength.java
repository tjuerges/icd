/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TLength.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.types.Length;

/**
 * Test of types.Length
 * 
 * @version 1.00 Nov 12, 2004
 * @author Allen Farris
 */
public class TLength {

	public static void main (String[] arg) {
		
		Length len = new Length();
		System.out.println("len = " + len);
		System.out.println("Is len zero? " + len.isZero());
		len = new Length(2124.0);
		System.out.println("len = " + len);
		len = new Length("2124.0");
		System.out.println("len = " + len);
		Length x = new Length(len);
		System.out.println("x = " + x);
		Length len2 = new Length();
		len2.set(5000.0);
		System.out.println("len = " + len.get() + " " + Length.unit());
		System.out.println("len + len2 = " + Length.add(len,len2));
		System.out.println("len2 - len = " + Length.sub(len2,len));
		System.out.println("len * 3.0 = " + Length.mult(len,3.0));
		System.out.println("len / 2.0 = " + Length.div(len,2.0));
		
		x = new Length ("2124.0","m");
		System.out.println("x = " + x.toString(Length.KILOMETER) + Length.KILOMETER);
		System.out.println("x = " + x.toString(Length.METER) + Length.METER);
		System.out.println("x = " + x.toString(Length.CENTIMETER) + Length.CENTIMETER);
		System.out.println("x = " + x.toString(Length.MILLIMETER) + Length.MILLIMETER);
		x = new Length("2.124",Length.KILOMETER);
		System.out.println("x = " + x.get() + Length.unit());	
		x = new Length("212400.0",Length.CENTIMETER);
		System.out.println("x = " + x.get() + Length.unit());
		x = new Length("2124000.0",Length.MILLIMETER);
		System.out.println("x = " + x.get() + Length.unit());
		
		String t1 = "123456";
		String t2 = "123456.0";
		String t3 = "123456789012345678901234567890";
		String t4 = "1234567890123456789";
		String t5 = "123456d";
		double tt = Double.parseDouble(t2);
		System.out.println("tt = " + tt);
		tt = Double.parseDouble(t1);
		System.out.println("tt = " + tt);
		tt = Double.parseDouble(t3);
		System.out.println("tt = " + tt);
		tt = Double.parseDouble(t5);
		System.out.println("tt = " + tt);
		long lt = Long.parseLong(t1);
		System.out.println("lt = " + lt);
		//lt = Long.parseLong(t2); << NumberFormatException
		//System.out.println("lt = " + lt);
		//lt = Long.parseLong(t3); << NumberFormatException
		//System.out.println("lt = " + lt);
		lt = Long.parseLong(t4);
		System.out.println("lt = " + lt);
		
	}
	
}
