/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TEntity.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.types.Entity;
import alma.hla.runtime.asdm.types.EntityRef;

/**
 * Test of types.Entity
 * 
 * @version 1.00 Nov 22, 2004
 * @author Allen Farris
 */
public class TEntity {

	public static void main (String[] arg) {
		
		Entity e = new Entity("uid://X0000000000000079/X00000000",
				"none", "Main", "1", "1");
		System.out.println(e.getEntityId() + ", " +
				e.getEntityIdEncrypted() + ", " + 
				e.getEntityTypeName() + ", " + 
				e.getEntityVersion() + ", " +
				e.getInstanceVersion());
		String xml = e.toXML();
		System.out.println("xml = " + xml);
		Entity x = new Entity();
		x.setFromXML(xml);
		System.out.println(x.getEntityId() + ", " +
				x.getEntityIdEncrypted() + ", " + 
				x.getEntityTypeName() + ", " + 
				x.getEntityVersion() + ", " +
				x.getInstanceVersion());

		EntityRef r = new EntityRef("uid://X0000000000000079/X00000000",
				"X00000002", "Main", "1");
		System.out.println(r.getEntityId() + ", " +
				r.getPartId() + ", " + 
				r.getEntityTypeName() + ", " + 
				r.getInstanceVersion());
		xml = r.toXML();
		System.out.println("xml = " + xml);
		EntityRef y = new EntityRef();
		y.setFromXML(xml);
		System.out.println(y.getEntityId() + ", " +
				y.getPartId() + ", " + 
				y.getEntityTypeName() + ", " + 
				y.getInstanceVersion());
		
		r = new EntityRef("uid://X0000000000000079/X00000000",
				null, "Main", "1");
		System.out.println(r.getEntityId() + ", " +
				r.getPartId() + ", " + 
				r.getEntityTypeName() + ", " + 
				r.getInstanceVersion());
		xml = r.toXML();
		System.out.println("xml = " + xml);
		y = new EntityRef();
		y.setFromXML(xml);
		System.out.println(y.getEntityId() + ", " +
				y.getPartId() + ", " + 
				y.getEntityTypeName() + ", " + 
				y.getInstanceVersion());
		
	}
	
}
