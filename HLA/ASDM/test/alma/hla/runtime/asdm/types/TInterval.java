/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TInterval.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.asdm.types.Interval;

/**
 * Test of types.Interval
 * 
 * @version 1.00 Nov 10, 2004
 * @author Allen Farris
 */
public class TInterval {

	public static void main (String[] arg) {
		
		Interval x = new Interval (123456789012345L);
		System.out.println("x = " + x);
		x = new Interval ("123456789012345");
		System.out.println("x = " + x);
		x = new Interval ();
		System.out.println("x = " + x);
		//x = Interval.fromDays(1.0);
		System.out.println("x = " + x);
		//x = Interval.fromSeconds(1);
		System.out.println("x = " + x);
		Interval y = new Interval (x);
		System.out.println("y = " + y);
		
		y.add(x);
		System.out.println("y = " + y);
		System.out.println("y.compareTo(x) = " + y.compareTo(x));
		System.out.println("y.eq(x) = " + y.eq(x));
		System.out.println("y.ne(x) = " + y.ne(x));
		System.out.println("y.lt(x) = " + y.lt(x));
		System.out.println("y.le(x) = " + y.le(x));
		System.out.println("y.gt(x) = " + y.gt(x));
		System.out.println("y.ge(x) = " + y.ge(x));
		
		
	}
		
}
