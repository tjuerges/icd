/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Antenna.cpp
 */

#include <iostream>
#include <string>
using namespace std;

#include <ASDMAll.h>
using namespace asdm;

#include <iostream>
#include <sstream>
ostringstream aux;

#include <math.h>

#define PI 3.14159265

int main(char *arg) {
  ASDM dataset;
  AntennaTable &antenna = dataset.getAntenna();
  AntennaRow* aRow = 0;
  
  //
  // Build an antenna table with 64 antennas on a circle.
  //
  cout << "About to populate the antenna table with 64 antennas..." << endl;

  string name;
  string station;
  string type;
  Length xPosition;
  Length yPosition;
  Length zPosition;
  Length xOffset;
  Length yOffset;
  Length zOffset;
  ArrayTime t((long long)5000);
  Length dishDiameter(12.0);
  bool flagRow = false;

  double radius = 10000.0;
  double deltaAngle = 2.0 * PI / 64;

  for (int i = 0; i < 64; i++) {
    aux.str(""); aux << "A_" << i;
    name = aux.str();
    aux.str(""); aux << "STATION_" << i ; 
    station = aux.str();
    type = "GROUND";
    xPosition = Length(radius * cos(i * deltaAngle));
    yPosition = Length(radius * sin(i * deltaAngle));
    zPosition = Length(5432.0);
    xOffset = Length(1.0);
    yOffset = Length(1.0);
    zOffset = Length(1.0);
    
    aRow = antenna.newRow(name,
			  station,
			  type,
			  xPosition,
			  yPosition,
			  zPosition,
			  t,
			  xOffset,
			  yOffset,
			  zOffset,
			  dishDiameter,
			  flagRow);

    AntennaRow* retRow = antenna.add(aRow);
     
    if (retRow != aRow) {
  	cout << "Attempt to store a row identical to a row present in the AntennaTable" << endl;
  	cout << "It's the row with adress " << (unsigned long) retRow << " and id " << retRow->getAntennaId() << endl;
    }
  }
  
  cout << "The antenna table has now " << antenna.size() << " elements" << endl;
  // Convert the Antenna table in its XML representation
  cout << endl;
  cout << "Here is the XML representation of the Antenna table:" << endl;
  string xmlAntenna = antenna.toXML();
  cout << xmlAntenna << endl;
	
// Write out the dataset in XML documents.
  cout << endl;
  cout << "Now converting the dataset into XML and saving it to disk" << endl;

  try {          
    srand((unsigned) time(0));
    aux.str(""); aux << "/tmp/antennaASDM_" << rand() ; 
    dataset.toXML(aux.str());
    cout << "The dataset has been saved in the directory " << aux.str() << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  	 
}
