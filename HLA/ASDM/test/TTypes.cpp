/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TTypes.cpp
 */

#include <iostream>
#include <string>

#include <Angle.h>
#include <AngularRate.h>
#include <ArrayTime.h>
#include <Complex.h>
#include <Entity.h>
#include <EntityId.h>
#include <EntityRef.h>
#include <Flux.h>
#include <Frequency.h>
#include <Humidity.h>
#include <Interval.h>
#include <Length.h>
#include <Pressure.h>
#include <Speed.h>
#include <Tag.h>
#include <Temperature.h>

#include <NumberFormatException.h>
#include <Long.h>

using namespace std;
using namespace asdm;

//---------------
// Test of Length
//---------------
int runTestLength() {
	double v1 = 2124.0123456789012345;
	double v2 = 5000.0;
	string s1 ("2124.0123456789012345");

	cout << "Test of Length." << endl;
	Length *value1 = new Length();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Length value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Length value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Length value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Length &x1 = *(new Length(v1));
	cout << "x1 = " << x1.toString() << endl;
	Length *z = new Length(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Length &x2 = *(new Length());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Length::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	try {
		Length xx ("failure");
	} catch (NumberFormatException err) {
		cout << "Error: " << err.getMessage() << endl;
	}

	cout << "End test of Length" << endl << endl;
	return 0;
}

//------------------
// Test of Angle
//------------------
int runTestAngle() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Angle." << endl;
	Angle *value1 = new Angle();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Angle value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Angle value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Angle value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Angle &x1 = *(new Angle(v1));
	cout << "x1 = " << x1.toString() << endl;
	Angle *z = new Angle(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Angle &x2 = *(new Angle());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Angle::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Angle" << endl << endl;
	return 0;
}

//------------------
// Test of AngularRate
//------------------
int runTestAngularRate() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of AngularRate." << endl;
	AngularRate *value1 = new AngularRate();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	AngularRate value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	AngularRate value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	AngularRate value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	AngularRate &x1 = *(new AngularRate(v1));
	cout << "x1 = " << x1.toString() << endl;
	AngularRate *z = new AngularRate(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	AngularRate &x2 = *(new AngularRate());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << AngularRate::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of AngularRate" << endl << endl;
	return 0;
}

//------------------
// Test of Flux
//------------------
int runTestFlux() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Flux." << endl;
	Flux *value1 = new Flux();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Flux value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Flux value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Flux value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Flux &x1 = *(new Flux(v1));
	cout << "x1 = " << x1.toString() << endl;
	Flux *z = new Flux(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Flux &x2 = *(new Flux());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Flux::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Flux" << endl << endl;
	return 0;
}

//------------------
// Test of Frequency
//------------------
int runTestFrequency() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Frequency." << endl;
	Frequency *value1 = new Frequency();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Frequency value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Frequency value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Frequency value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Frequency &x1 = *(new Frequency(v1));
	cout << "x1 = " << x1.toString() << endl;
	Frequency *z = new Frequency(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Frequency &x2 = *(new Frequency());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Frequency::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Frequency" << endl << endl;
	return 0;
}

//------------------
// Test of Humidity
//------------------
int runTestHumidity() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Humidity." << endl;
	Humidity *value1 = new Humidity();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Humidity value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Humidity value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Humidity value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Humidity &x1 = *(new Humidity(v1));
	cout << "x1 = " << x1.toString() << endl;
	Humidity *z = new Humidity(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Humidity &x2 = *(new Humidity());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Humidity::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Humidity" << endl << endl;
	return 0;
}

//------------------
// Test of Pressure
//------------------
int runTestPressure() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Pressure." << endl;
	Pressure *value1 = new Pressure();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Pressure value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Pressure value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Pressure value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Pressure &x1 = *(new Pressure(v1));
	cout << "x1 = " << x1.toString() << endl;
	Pressure *z = new Pressure(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Pressure &x2 = *(new Pressure());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Pressure::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Pressure" << endl << endl;
	return 0;
}

//------------------
// Test of Speed
//------------------
int runTestSpeed() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Speed." << endl;
	Speed *value1 = new Speed();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Speed value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Speed value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Speed value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Speed &x1 = *(new Speed(v1));
	cout << "x1 = " << x1.toString() << endl;
	Speed *z = new Speed(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Speed &x2 = *(new Speed());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Speed::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Speed" << endl << endl;
	return 0;
}

//------------------
// Test of Temperature
//------------------
int runTestTemperature() {
	double v1 = 0.78001230456789012345;
	double v2 = 0.78;
	string s1 ("0.78001230456789012345");

	cout << "Test of Temperature." << endl;
	Temperature *value1 = new Temperature();
	cout << "value1 (0) = " << *value1 << endl;
	cout << "Is value1 zero? " << value1->isZero() << endl;
	delete value1;
	
	Temperature value2 (v1);
	cout << "value2 (" << v1 << ") = " << value2 << endl;

	Temperature value3 (s1);
	cout << "value3 (" << s1 << ") = " << value3 << endl;
	string s2 = value3.toString();
	cout << "value3.toString (" << s1 << ") = " << s2 << endl;
	Temperature value4 (s2);
	cout << "value4 (from value3) = " << value4 << endl;
	cout << "value4.toString (" << s1 << ") = " << value4.toString() << endl;


	Temperature &x1 = *(new Temperature(v1));
	cout << "x1 = " << x1.toString() << endl;
	Temperature *z = new Temperature(x1);
	cout << "clone of x1 = " << z->toString() << endl;
	Temperature &x2 = *(new Temperature());
	x2 = v2;
	cout << "x2 = " << x2.toString() << endl;
	cout << "units = " << Temperature::unit() << endl;
	cout << "x1 + x2 = " << (x1 + x2) << endl;
	cout << "x2 - x1 = " << (x2 - x1) << endl;
	cout << "x1 * 3.0 = " << (x1 * 3.0) << endl;
	cout << "3.0 * x2 = " << (3.0 * x2) << endl;
	cout << "x1 / 2.0 = " << (x1 / 2.0) << endl;

	cout << "End test of Temperature" << endl << endl;
	return 0;
}

//---------------
// Test of Entity
//---------------
int runTestEntity() {
	cout << "Test of Entity." << endl;

	Entity e("uid://X0000000000000079/X00000000",
			"none", "Main", "1", "1");
	cout << e.getEntityId().toString() << ", " 
		 <<	e.getEntityIdEncrypted() << ", "
		 <<	e.getEntityTypeName() << ", "  
		 <<	e.getEntityVersion() << ", " 
		 <<	e.getInstanceVersion() << endl;
	string xml = e.toXML();
	cout << "xml = " << xml << endl;
	Entity x;
	x.setFromXML(xml);
	cout << e.getEntityId().toString() << ", "
		 <<	e.getEntityIdEncrypted() << ", " 
		 <<	e.getEntityTypeName() << ", " 
		 <<	e.getEntityVersion() << ", "
		 <<	e.getInstanceVersion() << endl;

	EntityRef *r = new EntityRef("uid://X0000000000000079/X00000000",
			"X00000002", "Main", "1");
	cout << r->getEntityId().toString() << ", "
		 <<	r->getPartId().toString() << ", " 
		 <<	r->getEntityTypeName() << ", " 
		 <<	r->getInstanceVersion() << endl;
	xml = r->toXML();
	cout << "xml = " << xml << endl;
	EntityRef y;
	y.setFromXML(xml);
	cout << y.getEntityId().toString() << ", "
		 <<	y.getPartId().toString() << ", " 
		 <<	y.getEntityTypeName() << ", " 
		 <<	y.getInstanceVersion() << endl;
	
	r = new EntityRef("uid://X0000000000000079/X00000000",
			"", "Main", "1");
	cout << r->getEntityId().toString() << ", "
		 <<	r->getPartId().toString() << ", " 
		 <<	r->getEntityTypeName() << ", " 
		 <<	r->getInstanceVersion() << endl;
	xml = r->toXML();
	cout << "xml = " << xml << endl;
	EntityRef z;
	z.setFromXML(xml);
	cout << z.getEntityId().toString() << ", "
		 <<	z.getPartId().toString() << ", " 
		 <<	z.getEntityTypeName() << ", " 
		 <<	z.getInstanceVersion() << endl;
	
	cout << "End test of Entity" << endl << endl;
	return 0;
}

//---------------
// Test of ArrayTime
//---------------
void display (ArrayTime t) {
	cout << "Time:            " << t.toFITS() << endl;
	cout << "    units        " << t.get() << endl;
	cout << "    JD           " << t.getJD() << endl;
	cout << "    MJD          " << t.getMJD() << endl;
	cout << "    units        " << t.get() << endl;
	cout << "    time of day  " << t.getTimeOfDay() << endl;
	cout << "    time of day  " << t.timeOfDayToString() << endl;
	cout << "    day of week  " << t.getDayOfWeek() << endl;
	cout << "    day of year  " << t.getDayOfYear() << endl;
	cout << "    GMST         " << t.getGreenwichMeanSiderealTime() << endl;
	cout << "    LST at VLA   " << t.getLocalSiderealTime(4.484071979167) << endl;
}

int runTestArrayTime() {
	cout << "Test of ArrayTime." << endl;

		ArrayTime *base = new ArrayTime (1582,10,15,0,0,0.0); display(*base);
		ArrayTime *mjdBase = new ArrayTime (1858,11,17,0,0,0.0); display(*mjdBase);
		ArrayTime *x = new ArrayTime (2004,11,12,4,5,6.123456789); display(*x);
		x = new ArrayTime (1987,04,10,19,21,0.0); display(*x);
		x = new ArrayTime (1987,04,10,0,0,0.0); display(*x);
		x = new ArrayTime (1957,10,4.81); display(*x);
		x = new ArrayTime (2000,1,1.5); display(*x);
		x = new ArrayTime (1999,1,1.0); display(*x);
		x = new ArrayTime (1987,1,27.0); display(*x);
		x = new ArrayTime (1987,6,19.5); display(*x);
		x = new ArrayTime (1988,1,27.0); display(*x);
		x = new ArrayTime (1988,6,19.5); display(*x);
		x = new ArrayTime (1900,1,1.0); display(*x);
		x = new ArrayTime (1600,1,1.0); display(*x);
		x = new ArrayTime (1600,12,31.0); display(*x);
		x = new ArrayTime (1970,1,1.0); display(*x);
		x = new ArrayTime (1957,10,4,19,26,24.0); display(*x);
		x = new ArrayTime (2000,1,1,12,0,0.0); display(*x);
		x = new ArrayTime (2004,12,14,16,2,45.67); display(*x);
		x = new ArrayTime (4609756965670000000LL); display(*x);
		x = new ArrayTime(44239.0); display(*x);
		x = new ArrayTime(44786.0); display(*x);
		x = new ArrayTime(Long::MAX_VALUE); display(*x);
		x = new ArrayTime (2000,1,1,0,0,0.0); display(*x);
		double jdbase = ArrayTime::unitToJD(0L);
		double mjdbase = ArrayTime::unitToMJD(0L);
		long long unitBase = ArrayTime::jdToUnit(2299160.5);
		long long unitMBase = ArrayTime::mjdToUnit(-100840);
		cout << "jdbase = " << jdbase << endl;
		cout << "mjdbase = " << mjdbase << endl;
		cout << "unitBase = " << unitBase << endl;
		cout << "unitMBase = " << unitMBase << endl;
		
		/*
		x = new ArrayTime (0.0);
		cout << "x = " << x->toString() << endl;
		ArrayTime *y = new ArrayTime (-100840.0);
		cout << "y = " << y->toString() << endl;
		
		ArrayTime *z = new ArrayTime ();
		cout << "1. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime ("1858-11-17 00:00:00");
		cout << "2. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime ("0.0");
		cout << "3. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime ("0");
		cout << "4. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime (mjdBase);
		cout << "5. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime (1858,11,17.5);
		cout << "6. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime (1858,11,17,12,0,0.0);
		cout << "7. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime (0.0);
		cout << "8. z = " << z->toString() + " " << z->toFITS() << endl;
		z = new ArrayTime (0L);
		cout << "9. z = " << z->toString() + " " << z->toFITS() << endl;
		*/
		
	cout << "End test of ArrayTime" << endl << endl;
	return 0;
}

//---------------
// Test of Complex
//---------------
int runTestComplex() {
	cout << "Test of Complex." << endl;

	Complex c1;
	cout << "Is c zero? " << c1.isZero() << endl;
	Complex c2(12.34,56.78);
	cout << "Is c zero? " << c2.isZero() << endl;
	cout << "c = " << c2 << endl;
	cout << "c-bar = " << conj(c2) << endl;
	Complex d("56.78 12.34");
	cout << "d = " << d << endl;
	Complex x(c2);
	cout << "x = " << x << endl;
	cout << "d = " << d.getReal() << " + i" << d.getImg() << endl;
	d.setReal(1.0);
	cout << "d = " << d << endl;
	d.setImg(2.0);
	cout << "d = " << d << endl;
	
	cout << "End test of Complex" << endl << endl;
	return 0;
}

int main(char *arg) {

	runTestAngle();
	runTestAngularRate();
	runTestFlux();
	runTestFrequency();
	runTestHumidity();
	runTestLength();
	runTestPressure();
	runTestSpeed();
	runTestTemperature();
	runTestEntity();
	runTestComplex();
	runTestArrayTime();

	return 0;

}
