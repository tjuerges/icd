/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestWriteXML.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

#include <Frequency.h>

using namespace asdm;

int main(char *arg) {
  ASDM dataset;

  cout << "This program creates an ASDM dataset containing three non empty tables :" << endl;
  cout << "DataDescription, SpectralWindow and Polarization.\n" << endl;
  cout << "The ASDM is then exported in XML representation." << endl;
  cout << endl;
  cout << "About to populate the  spectral window table ..." << endl;

  SpectralWindowTable &spectralWindow = dataset.getSpectralWindow();
  // Build the Spectral Window Table
  int numChan = 16;
  vector<Frequency> *chanFreqPtr    = 0;
  vector<Frequency> *chanWidthPtr   = 0;
  vector<Frequency> *effectiveBwPtr = 0;
  vector<Frequency> *resolutionPtr  = 0;
  vector<string>    *assocNaturePtr = 0;
		
  // This table will contain nrows rows. 
  int nrows = 4;
  for (int j = 0; j < nrows; j++) {
    // Instantiate vectors for some of the table attributes.
    chanFreqPtr    = new vector <Frequency> ();
    chanWidthPtr   = new vector <Frequency> ();
    effectiveBwPtr = new vector <Frequency> ();
    resolutionPtr  = new vector <Frequency> ();
    assocNaturePtr = new vector <string> ();
				
    // Populate these vectors with completely dummy values.	
    for (int i = 0; i < numChan; i++) {
      chanFreqPtr->push_back(*(new Frequency(1000. * i + j)));
      chanWidthPtr->push_back(*(new Frequency(100. + j)));
      effectiveBwPtr->push_back(*(new Frequency(500.+1./(j+i+1.))));
      resolutionPtr->push_back(*(new Frequency(60.+j)));
    }
			
			
    string* name = new string("Spectral Window #");
    char dumstr[3];
    sprintf(dumstr, "%d", j);
    name->append(dumstr);
			
    char freqGroupName[40];
    sprintf(freqGroupName, "Frequency group name  %d", j );
			 
    Frequency refFreq(2000.0*j);
    Frequency totBandWidth(2220.0*j);
			
    // Create a new row.
    /** SpectralWindowRow* SpectralWindowTable::newRow(int numChan,
     *  string name, 
     *  Frequency refFreq, 
     *  vector < Frequency > chanFreq, 
     *  vector < Frequency > chanWidth, 
     *  int measFreqRef, 
     *  vector < Frequency > effectiveBw, 
     *  vector < Frequency > resolution, 
     *  Frequency totBandwidth, 
     *  int netSideband, 
     *  int ifConvChain, 
     *  int freqGroup, 
     *  string freqGroupName, 
     *  bool flagRow)
     */
    cout << "About to build a spectral window row ..." << endl;
    SpectralWindowRow* aRow = spectralWindow.newRow(numChan,
						    *name,
						    refFreq,
						    *chanFreqPtr,
						    *chanWidthPtr,
						    2200*j,
						    *effectiveBwPtr,
						    *resolutionPtr,
						    totBandWidth,
						    1100*j,
						    3300*j,
						    j,
						    string(freqGroupName),
						    false);
			
			
    // Add the row to the SpectralWindow table using this Tag.
    cout << "About to add it to its spectral window table ...";					
    aRow = spectralWindow.add(aRow);
    cout << "it has been given the id " << aRow->getSpectralWindowId() << endl;
  }

		
  // Build a polarization table with two rows.
  cout << "About to populate the polarization table ..." << endl;
  PolarizationTable &polarization = dataset.getPolarization();	

  vector<int>          *corrTypePtr = 0;
  vector<vector<int> > *corrProductPtr = 0;
  vector<int>          *tmpVecPtr = 0;
		 
  for (int i = 0; i < 2; i++) {
		 	
    int numCorr=i+1;
		 	
    // Instantiate and populate a vector for the corrType attribute
    corrTypePtr = new vector<int>();
    for (int j = 0; j < numCorr; j++)
      corrTypePtr->push_back(i);
		 		
		 		
    // Instantiate and populate a vector for the corrProduct attribute
    // Given that corrProduct is a vector of vector...this is
    // a two step process.	
    corrProductPtr = new vector<vector<int> >();
		 	
    // 1st vector<int> of corrProduct
    tmpVecPtr = new vector<int>;
    for (int j = 0; j < numCorr; j++)
      tmpVecPtr->push_back(i+j);
    corrProductPtr->push_back(*tmpVecPtr);
		 	
    // 2nd vector<int> of corrProduct 
    tmpVecPtr = new vector<int>;
    for (int j = 0; j < numCorr; j++)
      tmpVecPtr->push_back(i+j);
    corrProductPtr->push_back(*tmpVecPtr);
		 
    // Create a new Polarization row.
    cout << "About to build a polarization row ...";
    PolarizationRow* aRow = polarization.newRow(numCorr,
						*corrTypePtr,
						*corrProductPtr,
						false);	
	cout << "it has been given the id " << aRow->getPolarizationId() << endl;
    // Add the newly created row to the Polarization table
    // with this Tag.
    cout << "About to add it to its table ..." << endl;							
    polarization.add(aRow);                                            	
  }
		 	
  //
  // Build a DataDescription table with three rows. 
  // 
  cout << "About to populate the datadescription table ..." << endl;
  DataDescriptionTable &dataDescription = dataset.getDataDescription();

  cout << "A row for spectralwindowId = " << spectralWindow.getRowByKey(Tag(0))->getSpectralWindowId()
  	   << ",  polarizationId = " << polarization.getRowByKey(Tag(0))->getPolarizationId() << endl;
  	
  DataDescriptionRow* aDDRow = dataDescription.newRow(polarization.getRowByKey(Tag(0))->getPolarizationId(),
						      spectralWindow.getRowByKey(Tag(0))->getSpectralWindowId(),
						      false);
  cout << "it has been given the id " << aDDRow->getDataDescriptionId() << endl;						      
  dataDescription.add(aDDRow); 
  
  cout << "A row for spectralwindowId = " << spectralWindow.getRowByKey(Tag(0))->getSpectralWindowId()
  	   << ",  polarizationId = " << polarization.getRowByKey(Tag(1))->getPolarizationId() << endl;
  aDDRow = dataDescription.newRow(polarization.getRowByKey(Tag(1))->getPolarizationId(),
				  spectralWindow.getRowByKey(Tag(0))->getSpectralWindowId(),
				  false);
  cout << "it has been given the id " << aDDRow->getDataDescriptionId() << endl;	
  dataDescription.add(aDDRow); 

  cout << "A row for spectralwindowId = " << spectralWindow.getRowByKey(Tag(1))->getSpectralWindowId()
  	   << ",  polarizationId = " << polarization.getRowByKey(Tag(1))->getPolarizationId() << endl;
  aDDRow = dataDescription.newRow(polarization.getRowByKey(Tag(1))->getPolarizationId(),				  
				  spectralWindow.getRowByKey(Tag(1))->getSpectralWindowId(),
				  false);
  cout << "it has been given the id " << aDDRow->getDataDescriptionId() << endl;	
  dataDescription.add(aDDRow); 
		

  // Write out the dataset in XML documents.
  try {
          char testdir[40];
          srand((unsigned) time(0));
          sprintf(testdir,"/tmp/asdm_%d", rand());
          dataset.toXML(string(testdir));
          cout << "The dataset has been saved in the directory " << testdir << endl;
        } catch (ConversionException e) {
          cout << "Error! " << e.getMessage() << endl;
        }  	

}
