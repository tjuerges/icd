/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TUtil.cpp
 */

#include <iostream>
#include <string>

#include <Angle.h>
#include <StringTokenizer.h>
#include <Double.h>
#include <Long.h>
#include <Integer.h>
#include <NumberFormatException.h>
#include <InvalidArgumentException.h>
#include <OutOfBoundsException.h>
#include <ArrayTime.h>

using namespace std;
using asdm::StringTokenizer;
using asdm::Double;
using asdm::Long;
using asdm::Integer;
using asdm::NumberFormatException;
using asdm::InvalidArgumentException;
using asdm::OutOfBoundsException;
using asdm::ArrayTime;

//-----------
// Conversion
//-----------
int runTestDouble() {
	cout << "Test of Double." << endl;

	double xval = 12345.67890123456789E21;
	string sval = "12345.67890123456789E21";
	string s = Double::toString(xval);
	cout << "converted double to string = " << s << endl;
	double z = Double::parseDouble(sval);
	cout << "converted string to double = " << z << endl;
	cout << "converted string to double = " << Double::toString(z) << endl;
	
	try {
		Double::parseDouble("not a number");
	} catch (NumberFormatException err) {
		cout << "Error" << endl;
		cout << err.getMessage() << endl;
	}
	
	cout << "max value is " << Double::MAX_VALUE << endl;
	cout << "min value is " << Double::MIN_VALUE << endl;
		
	cout << "End test of Double" << endl << endl;
	return 0;
}

int runTestLong() {
	cout << "Test of Long." << endl;

	long long xval = 123456789012345678LL;
	string sval = "123456789012345678";
	string s = Long::toString(xval);
	cout << "converted long long to string = " << s << endl;
	long long z = Long::parseLong(sval);
	cout << "converted string to long long = " << z << endl;
	cout << "converted string to long long = " << Long::toString(z) << endl;
	
	try {
		Long::parseLong("not a number");
	} catch (NumberFormatException err) {
		cout << "Error" << endl;
		cout << err.getMessage() << endl;
	}

	cout << "max value is " << Long::MAX_VALUE << endl;
	cout << "min value is " << Long::MIN_VALUE << endl;
		
	cout << "End test of Long" << endl << endl;
	return 0;
}

int runTestInteger() {
	cout << "Test of Integer." << endl;

	int xval = 123456789;
	string sval = "123456789";
	string s = Integer::toString(xval);
	cout << "converted int to string = " << s << endl;
	int z = Integer::parseInt(sval);
	cout << "converted string to int = " << z << endl;
	cout << "converted string to int = " << Integer::toString(z) << endl;
	
	try {
		Integer::parseInt("not a number");
	} catch (NumberFormatException err) {
		cout << "Error" << endl;
		cout << err.getMessage() << endl;
	}

	cout << "max value is " << Integer::MAX_VALUE << endl;
	cout << "min value is " << Integer::MIN_VALUE << endl;
		
	cout << "End test of Integer" << endl << endl;
	return 0;
}

//-------------
// Test of StringTokenizer
//-------------
int runTestStringTokenizer() {
	cout << "Test of StringTokenizer." << endl;

	string test ("This is    a \t\nstring for \n\ra test.");
	cout << "test string = [" << test << "]" << endl;
	
	StringTokenizer t1 (test);
	cout << "number of tokens = " << t1.countTokens() << endl;
	string s;
	cout << "token: ";
	while (t1.hasMoreTokens()) {
		s = t1.nextToken();
		cout << "[" << s << "] "; 
	}
	cout << endl << endl;
	
	StringTokenizer t2 (test," ",true);
	cout << "number of tokens = " << t2.countTokens() << endl;
	cout << "token: ";
	while (t2.hasMoreTokens()) {
		s = t2.nextToken();
		cout << "[" << s << "] "; 
	}
	cout << endl << endl;

	StringTokenizer t3 (test,"s");
	cout << "number of tokens = " << t3.countTokens() << endl;
	cout << "token: ";
	while (t3.hasMoreTokens()) {
		s = t3.nextToken();
		cout << "[" << s << "] "; 
	}
	cout << endl << endl;

	StringTokenizer t4 (test);
	cout << "token: ";
	s = t4.nextToken();
	cout << "[" << s << "] "; 
	s = t4.nextToken();
	cout << "[" << s << "] " << endl; 
	s = t4.nextToken("s");
	cout << "token: ";
	cout << "[" << s << "] ";
	while (t4.hasMoreTokens()) {
		s = t4.nextToken();
		cout << "[" << s << "] "; 
	}
	cout << endl;

	cout << "End test of StringTokenizer" << endl << endl;
	return 0;

}

int main(char *arg) {

	runTestDouble();

	runTestLong();

	runTestInteger();
	
	runTestStringTokenizer();

	return 0;

}
