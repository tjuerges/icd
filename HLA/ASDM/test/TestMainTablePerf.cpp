/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestMainTablePerf.cpp
 */
 using namespace std;

#include <ASDMAll.h>
using namespace asdm;

#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/util/XMLUni.hpp>	

#include <ctype.h>	

using namespace xercesc;

#define BLOCKSIZE 1024

static const char*  gMemBufId = "dummyInfo";

void xmlIze(ASDM& dataset) {
	
	XMLPlatformUtils::Initialize();
	
	XercesDOMParser *parser = new XercesDOMParser;
	DOMImplementation* implementation = DOMImplementation::getImplementation();
	
	DOMWriter         *theSerializer = ((DOMImplementationLS*)implementation)->createDOMWriter();
	theSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);
	XMLFormatTarget *myFormTarget = new StdOutFormatTarget();
	DOMNode                     *document = 0;
	
	// The container.
	string s_dataset = dataset.toXML();
	MemBufInputSource * memBufIS = new MemBufInputSource (
        (const XMLByte*)s_dataset.c_str(),
        strlen(s_dataset.c_str()),
        gMemBufId,
        false
    );
    parser->parse(*memBufIS);	
	document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);

	// The DataDescriptionTable
	string s_ddT = dataset.getDataDescription().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_ddT.c_str(),
        strlen(s_ddT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The SpectralWindowTable
	string s_spwT = dataset.getSpectralWindow().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_spwT.c_str(),
        strlen(s_spwT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The HolographyTable
	string s_holoT = dataset.getHolography().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_holoT.c_str(),
        strlen(s_holoT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	// The PolarizationTable
	string s_polT = dataset.getPolarization().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_polT.c_str(),
        strlen(s_polT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	delete theSerializer;
    delete myFormTarget;
    delete parser;
    delete memBufIS;
	XMLPlatformUtils::Terminate();		
}

void buildASDM(ASDM& dataset, int numBlock) {
	
		// Populate its MainTable with BLOCKSIZE * numBlock rows
		MainTable& mT = dataset.getMain();
		MainRow* mRow = 0;
		
		// Use some constant values for all parts of the key section except the time.
		Tag configDescriptionId(1, TagType::ConfigDescription);
		Tag fieldId(1, TagType::Field);
		
		// Define some values to build the data section.
		int numAntenna = 1;
		int numBaseband = 1;
		int numCorr = 6;
		
		Tag execBlockId(1,	TagType::ExecBlock);
		vector<Tag> stateId;
		stateId.push_back(Tag(1, TagType::State));		

		int scanNumber = 1;
		int subscanNumber = 1;
		int integrationNumber = 1;		
		
		vector< vector <Length> > uvw;
		for (int i = 0; i < 3; i++) {
			vector<Length> vl; uvw.push_back(vl);
			for (int j = 0; j < numAntenna; j++)
				uvw.at(i).push_back(Length(1000.0*numAntenna + j));
		}
		vector< vector<Interval> > exposure;
		vector< vector<ArrayTime> > timeCentroid;
		for (int i = 0; i < numAntenna; i++) {
			vector<Interval> vi; exposure.push_back(vi);
			vector<ArrayTime> va; timeCentroid.push_back(va);
			for (int j = 0; j < numBaseband; j++) {
				exposure.at(i).push_back(Interval(1000000LL * numAntenna + numBaseband));
				timeCentroid.at(i).push_back(ArrayTime(2000000LL * numAntenna + numBaseband));
			}
		}
		
		
		
		vector<int> flagAnt;
		for (int i = 0; i < numAntenna; i++)
			flagAnt.push_back(0);
			
		vector<vector<int> > flagPol;
		for (int i = 0; i < numAntenna; i++) {
			vector<int> vi ; flagPol.push_back(vi);
			for (int j = 0; j < numCorr; j++) 
				flagPol.at(i).push_back(0);
		}

		vector<vector<vector<int> > > flagBaseband;
		for (int i = 0; i < numAntenna; i++) {
			vector<vector<int> > vvi ; flagBaseband.push_back(vvi);
			for (int j = 0; j < numCorr; j++) {
				vector<int> vi; flagBaseband.at(i).push_back(vi);
				for (int k = 0; k < numBaseband; k++)
					flagBaseband.at(i).at(j).push_back(0);
			}
		}				
		
		bool flagRow = false;
		
		Interval interval(400000);
		for (int i = 0; i < numBlock; i++)
			for (int j  = 0; j < BLOCKSIZE; j++) {
				mRow = mT.newRow(configDescriptionId,
															fieldId,
															ArrayTime((i * BLOCKSIZE + j) * 40000000LL),
															execBlockId,
															stateId,
															scanNumber, 
															subscanNumber, 
															integrationNumber, 
															uvw,
															 exposure, 
															 timeCentroid, 
															 EntityRef("uid://X0000000000000001/X00000001", "X00000000", "BinaryData", "1"),
															 flagAnt, 
															 flagPol,
															 flagBaseband, 
															 flagRow, 
															 interval);
				mT.add(mRow);
			}
		cout << "The dataset has " << mT.size() << " rows in its Main table" << endl;	
		return;
}

int main(int argc, char *argv[]) {
	
	int numBlock;
	
	// Get the number of the number of blocks from command line.
	if (argc < 2) {
			cout << "Usage : TestBinaryTablePerf <numBlock>" << endl;
			exit(-1);
	}
	
	try {
		numBlock = Integer::parseInt(argv[1]);
	}
	catch (NumberFormatException e) {
		cout << "Invalid value for numBlock (" << e.getMessage() << ")" << endl;
		exit(-1);	
	}
	
	// Build a dummy dataset.
	cout << "About to build a dummy dataset" << endl;
	ASDM dataset;
	time_t beginCreateDataSet = time(0);
	try {
		buildASDM(dataset, numBlock);
	}
	catch (InvalidArgumentException e) {
		cout << e.getMessage() << endl;
		exit (-1);	
	}
	cout << "Built a dummy dataset (" << difftime(time(0), beginCreateDataSet) << " s.)"<<  endl;
	
	dataset.toFile("/tmp/MainPerf");
}


