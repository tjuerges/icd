/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestFeed.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

#include <Frequency.h>

#include <IllegalAccessException.h>

using namespace asdm;
int main(char *arg) {
  ASDM dataset;
  
  FeedTable&  feed = dataset.getFeed();

  // Prepare one dummy feed row based on numReceptors = 2.
  ArrayTimeInterval timeInterval(10.0);
  Tag antennaId(10);
  Tag spectralWindowId(11);
  int numReceptors = 2;
  int feedNum = 0;

  vector<vector<double> > beamOffset;
  vector<double> beamOffsetRow;
  beamOffsetRow.push_back((double)1.0);
  beamOffsetRow.push_back((double)1.0);
  beamOffset.push_back(beamOffsetRow);
  beamOffsetRow.clear();
  beamOffsetRow.push_back((double)2.0);
  beamOffsetRow.push_back((double)2.0);
  beamOffset.push_back(beamOffsetRow);

  vector<string> polarizationType;
  polarizationType.push_back("L");
  polarizationType.push_back("R");

  vector<vector<Complex> > polResponse;
  vector<Complex> polResponseRow;
  polResponseRow.push_back(Complex(1., 1.));
  polResponseRow.push_back(Complex(1., -1.));
  polResponse.push_back(polResponseRow);

  polResponseRow.clear();
  polResponseRow.push_back(Complex(-1., 1.));
  polResponseRow.push_back(Complex(-1., -1.));
  polResponse.push_back(polResponseRow);

  Length xPosition(1.0);
  Length yPosition(1.0);
  Length zPosition(1.0);
  
  vector<Angle> receptorAngle;
  receptorAngle.push_back(0.5);
  receptorAngle.push_back(-0.5);

  vector<Tag> beamId;
  beamId.push_back(Tag(100));
  
  vector<int> receiverId;
  receiverId.push_back(5);

  FeedRow * retFeedRow = 0;
  for (int i = 0; i < 125; i++) {
    FeedRow * feedRow = feed.newRow(antennaId,
				    spectralWindowId,
				    timeInterval,
				    beamId,
				    receiverId,
				    numReceptors,
				    feedNum,
				    beamOffset,				    
				    polarizationType,
				    polResponse,
				    xPosition + i,
				    yPosition + i,
				    zPosition + i,
				    receptorAngle);
    
    if ((retFeedRow = feed.add(feedRow)) == feedRow)
      cout << "A new row has been appended to the Feed table" << endl;
    else
      cout << "A row with these attributes was already in the Feed table" << endl;
  }

   // Write out the dataset in XML documents.
  try {
          char testdir[40];
	  time_t t;
          srand((unsigned) time(&t));
          sprintf(testdir,"/tmp/asdm_%d", rand());
          dataset.toXML(string(testdir));
          cout << "The dataset has been saved in the directory " << testdir << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  	

}
