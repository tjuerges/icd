/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestReadXML.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

using namespace asdm;

	int main(int argc, char *argv[]) {
		
		if (argc < 2) {
			cerr << "Usage : TestRead ASDM_directory" << endl;
			return 0;
		}
		
		string testdir = string(argv[1]);
		ASDM *dataset;
		
		try {
			dataset = ASDM::getFromXML(testdir);
		} 
		catch (ConversionException e) {
			cout << "Error! " << e.getMessage() << endl;
			return 0;
		}
		catch (...) {
			cout << "Unexpected exception : " <<  endl;
			return 0;	
		}
		
		cout << "Dataset read successfully." << endl;	
	}
