#include <iostream>
#include <string>
#include <ASDMAll.h>
#include "SDMBinaryBlock.h"
using namespace asdmBinary;
#include "SDMBinaryData.h"
#include "MSData.h"
using namespace asdm;


int main (int argc, char* argv[]) {

  if (argc < 2) {
    cerr << "Usage : testReadASDM asdmDir" << endl;
    return -1;
  }
  string asdmDir = string(argv[1]);

  ASDM* datasetPtr = 0;
  try{
    datasetPtr = ASDM::getFromXML(asdmDir);
  }
  catch (ConversionException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }
  catch (...) {
    cerr << "Unexpected exception " << endl;
    return -1;
  }

  MainTable& mT = datasetPtr->getMain();

  cout << "The main table has " << mT.size() << " rows" << endl;
  
  // Filters: 
  int              apcCode;
  int              corrMode;

  apcCode=0;         // retrieve only un-corrected
  apcCode=1;         // retrieve only corrected  (==> no auto-correlation output)
  apcCode=2;         // retrieve un-corrected and corrected

  corrMode=1;        // retrieve only auto-correlation       tested OK
  corrMode=0;        // retrieve only cross-correlation
  corrMode=2;        // retrieve auto and cross-correlation

  SDMBinaryData*   sdmBinDataPtr =0;
  unsigned int     numMSRows=0;    // initialization of a counter to count the number of MS rows to be produced
  int              ier;

  vector<MSData*>  v_msDataPtr;       // 1 item per MS Main table row
  vector<MainRow*> v_mrPtr = datasetPtr->getMain().get();

  for(unsigned int n=0; n<mT.size(); n++){
    //for( int n=0; n<5; n++){
    cout << "process MAIN row number " << n+1 
	 << " with dataOID=" << v_mrPtr[n]->getDataOid().getEntityId().toString() 
	 << endl;
    if(n==0){
      sdmBinDataPtr = new SDMBinaryData( datasetPtr, v_mrPtr[n], asdmDir); cout<<"done for 1st row"<<endl;
    }else{
      ier = sdmBinDataPtr->update(v_mrPtr[n]);                             cout<<"update("<<n<< ") returns "<<ier<<endl;
    }

    // import the binary content
    int iostat= sdmBinDataPtr->importDataObject();  cout << "iostat=" << iostat << endl;

    if(iostat==0){

      cout << "Ready to use getDataCols( apcCode, corrMode)" << endl;
      v_msDataPtr = sdmBinDataPtr->getData( apcCode, corrMode);

      numMSRows = numMSRows +  v_msDataPtr.size();

      cout << "There are " << v_msDataPtr.size() 
	   << " MS MAIN table rows for the ASDM MAIN table row " 
	   << n+1 << endl;
    }else{

      cout <<"WARNING: No data retrieving for the SDM row number " << endl;

    }
  } // end loop 

  cout << "Number of output MS MAIN table rows: "<<numMSRows
       << " for " << mT.size() << " input SDM Main rows" << endl;
  cout << "Number of tree hierarchies build: " << sdmBinDataPtr->numTree() << endl;

  delete sdmBinDataPtr;

  cout << "Successful test completion" << endl;

  return 0;

}
