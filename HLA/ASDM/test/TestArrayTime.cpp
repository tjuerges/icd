#include <ArrayTime.h>
#include <Interval.h>
#include <ArrayTimeInterval.h>
#include <time.h>

int main (int argc, char* argv[]) {

  using asdm::ArrayTime;

   // Define a default creation time : now.
    time_t rawtime;
    time(&rawtime);
    struct tm* timeInfo = localtime(&rawtime);
    ArrayTime at((1900+timeInfo->tm_year),
		 (timeInfo->tm_mon+1),
		 timeInfo->tm_mday,
		 timeInfo->tm_hour,
		 timeInfo->tm_min,
		 (double) timeInfo->tm_sec);
    
    cout << "Today in FITS format : " << at.toFITS() << endl;
    cout << "Today in nanoseconds :" << at.get() << endl;

}
