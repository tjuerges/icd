/*
 * $Id$
 * */

#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

int main (int argC, char *argV[]) {
	
	vector<string> tokens;
	istringstream iss("$Id$");
	
	string s;
	
	while (!iss.eof()) {
		iss >> s;
		tokens.push_back(s);
	}
		
	cout << "Hello world, my revision number is " << tokens.at(2) << endl;
}
