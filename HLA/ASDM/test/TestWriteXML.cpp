/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestWriteXML.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <TransitionTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <TransitionRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

using namespace asdm;

	void fillFeedRow(FeedRow *row, string itag, int ifeed) {
		// Populate the row.
		row->setAntennaId(*(new Tag(itag)));
		row->setAntennaLink(*(new Tag("tag2")));
		vector< vector<double> > dd(2);
		dd[0] = *(new vector<double> (2));
		dd[1] = *(new vector<double> (2));
		dd[0][0] = 1.0;
		dd[0][1] = 2.0;
		dd[1][0] = 3.0;
		dd[1][1] = 4.0;
		row->setBeamOffset(dd);
		row->setFeedId(ifeed);
		row->setFocusReference(dd);
		row->setIllumOffset(2.0F);
		row->setIllumOffsetPa(3.0F);
		row->setInterval(*(new Interval(10000000000LL)));
		row->setNumReceptors(2);
		vector<string> ss(2);
		ss[0] = "string 1";
		ss[1] = "string 2";
		row->setPolarizationType(ss);
		vector< vector<Complex> > cc(2);
		cc[0] = *(new vector<Complex> (2));
		cc[1] = *(new vector<Complex> (2));
		cc[0][0] = *(new Complex(1.0,2.0));
		cc[0][1] = *(new Complex(3.0,4.0));
		cc[1][0] = *(new Complex(5.0,6.0));
		cc[1][1] = *(new Complex(7.0,8.0));
		row->setPolResponse(cc);
		vector<Angle> aa (2);
		aa[0] = *(new Angle(0.785));
		aa[1] = *(new Angle(0.885));
		row->setReceptorAngle(aa);
		row->setSpectralWindowId(*(new Tag("tag3")));
		row->setSpectralWindowId(*(new Tag("tag4")));
		row->setTime(*(new ArrayTime (2004,12,22,11,40,0.0)));
		row->setXPosition(*(new Length(45.0)));
		row->setYPosition(*(new Length(46.0)));
		row->setZPosition(*(new Length(47.0)));
		row->addReceiverIdSet(1);
		row->addReceiverIdSet(2);
		//row->addBeamIdSet(*(new Tag("tag5")));
		//row->addBeamIdSet(*(new Tag("tag6")));
	}

	void fill(FeedTable &feed) {
		Entity ee;
		ee.setEntityId(*(new EntityId("uid://X0000000000000079/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("FeedTable");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		feed.setEntity(ee);
		
		FeedRow *row = feed.newRow();
		fillFeedRow(row,"tag1",1);
		try {
			feed.add(row);
		} catch (DuplicateKey e) {
			cout << "Error! " << e.getMessage() << endl;
		}
		/*
		// Now, convert the row to IDL ...
		cout << "converting to IDL" << endl;
		FeedRowIDL *idlrow = row->toIDL();
		cout << "OK. converting from IDL" << endl;
		// ... and create a new row from the IDL version.
		FeedRow *row2 = feed.newRow();
		cout << "got a new row. ready to set" << endl;
		row2->setFromIDL(*idlrow);
		cout << "OK. change antenna id." << endl;
		// And, add it to the table after changing its key.
		row2->setAntennaId(*(new Tag("tag99")));
		cout << "OK. Ready to add row to table." << endl;

		try {
			feed.add(row2);
			cout << "OK." << endl;
		} catch (DuplicateKey e) {
			cout << "Error! " << e.getMessage() << endl;
		}
		*/
	}



	int main(char *arg) {
		ASDM dataset;
		
		Entity ee;
		ee.setEntityId(*(new EntityId("uid://X0000000000000101/X00000000")));
		ee.setEntityIdEncrypted("na");
		ee.setEntityTypeName("ASDM");
		ee.setEntityVersion("1");
		ee.setInstanceVersion("1");
		dataset.setEntity(ee);
		dataset.setTimeOfCreation(*(new ArrayTime(2004,12,22,14,14,0.0)));
		
		FeedTable &feed = dataset.getFeed();
		fill(feed);
		
		// Display the XML version of the Feed table.
		cout << feed.toXML() << endl;
		
		// Write out the dataset.
		try {
			string testdir = "asdm_0000000000000101_00000002";
			
			dataset.toXML(testdir);
			cout << "Dataset written to " << testdir << endl;
			
			cout << dataset.toXML() << endl;
		} catch (ConversionException e) {
			cout << "Error! " << e.getMessage() << endl;
		}
	
		return 0;
	
	}
