/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TIDLTypes.cpp
 */

#include <iostream>
#include <string>
using namespace std;

#include <asdmIDLTypesC.h>
using namespace asdmIDLTypes;
#include <asdmIDLC.h>
using namespace asdmIDL;

#include <ASDM.h>
#include <FeedTable.h>
#include <FeedRow.h>
using namespace asdm;

int runTestIDL() {

	// 1. Create an ASDM and a get the Feed table.
	// -------------------------------------------
	ASDM *container = new ASDM();
	FeedTable feed = container->getFeed();
	FeedRow *row = feed.newRow();
	cout << "1. Created an ASDM and a got the Feed table." << endl;
	
	// 2. Fill a row of the Feed table with data.
	//-------------------------------------------
	row->setAntennaId(*(new Tag("itag1")));
	vector< vector<double> > dd(2);
	dd[0] = *(new vector<double> (2));
	dd[1] = *(new vector<double> (2));
	dd[0][0] = 1.0;
	dd[0][1] = 2.0;
	dd[1][0] = 3.0;
	dd[1][1] = 4.0;
	row->setBeamOffset(dd);
	row->setFeedId(3);
	row->setFocusReference(dd);
	row->setIllumOffset(2.0F);
	row->setIllumOffsetPa(3.0F);
	row->setInterval(*(new Interval(10000000000LL)));
	row->setNumReceptors(2);
	vector<string> ss(2);
	ss[0] = "string 1";
	ss[1] = "string 2";
	row->setPolarizationType(ss);
	vector< vector<Complex> > cc(2);
	cc[0] = *(new vector<Complex> (2));
	cc[1] = *(new vector<Complex> (2));
	cc[0][0] = *(new Complex(1.0,2.0));
	cc[0][1] = *(new Complex(3.0,4.0));
	cc[1][0] = *(new Complex(5.0,6.0));
	cc[1][1] = *(new Complex(7.0,8.0));
	row->setPolResponse(cc);
	vector<Angle> aa (2);
	aa[0] = *(new Angle(0.785));
	aa[1] = *(new Angle(0.885));
	row->setReceptorAngle(aa);
	row->setSpectralWindowId(*(new Tag("tag3")));
	row->setSpectralWindowLink(*(new Tag("tag4")));
	row->setTime(*(new ArrayTime (2004,12,22,11,40,0.0)));
	row->setXPosition(*(new Length(45.0)));
	row->setYPosition(*(new Length(46.0)));
	row->setZPosition(*(new Length(47.0)));
	row->addReceiverLink(*(new ArrayTime (2004,12,22,11,40,0.0)),*(new Interval(5000000000LL)));
	row->addReceiverLink(*(new ArrayTime (2004,12,22,11,40,5.0)),*(new Interval(5000000000LL)));
	row->addBeamLink(*(new Tag("tag5")));
	row->addBeamLink(*(new Tag("tag6")));
	feed.add(row);
	cout << "2. Filled a row of the Feed table with data and added it to the table." << endl;
	cout << row->toXML() << endl;
	
	// 3. Convert the row of the Feed table to IDL.
	//---------------------------------------------
	FeedRowIDL x;

	x.feedId = row->getFeedId();

	x.time = row->getTime().toIDLArrayTime();

	x.interval = row->getInterval().toIDLInterval();

	x.numReceptors = row->getNumReceptors();

	x.feedNum = row->getFeedNum();

	vector< vector< double> > beamOffset = row->getBeamOffset();
	x.beamOffset.length(beamOffset.size());
	for (unsigned int i = 0; i < beamOffset.size(); ++i)
		x.beamOffset[i].length(beamOffset[0].size());
	for (unsigned int i = 0; i < beamOffset.size(); ++i)
		for (unsigned int j = 0; j < beamOffset[0].size(); ++j)
			x.beamOffset[i][j] = beamOffset[i][j];

	vector< vector< double> > focusReference = row->getBeamOffset();
	x.focusReference.length(focusReference.size());
	for (unsigned int i = 0; i < focusReference.size(); ++i)
		x.focusReference[i].length(focusReference[0].size());
	for (unsigned int i = 0; i < focusReference.size(); ++i)
		for (unsigned int j = 0; j < focusReference[0].size(); ++j)
			x.focusReference[i][j] = focusReference[i][j];
	
	x.illumOffset = row->getIllumOffset();

	x.illumOffsetPa = row->getIllumOffsetPa();
	
	vector<string> polarizationType = row->getPolarizationType();
	x.polarizationType.length(polarizationType.size());
	for (unsigned int i = 0; i < polarizationType.size(); ++i)
		x.polarizationType[i] = CORBA::string_dup(polarizationType[i].c_str());
	
	vector< vector<Complex> > polResponse = row->getPolResponse();
	x.polResponse.length(polResponse.size());
	for (unsigned int i = 0; i < polResponse.size(); ++i)
		x.polResponse[i].length(polResponse[0].size());
	for (unsigned int i = 0; i < polResponse.size(); ++i)
		for (unsigned int j = 0; j < polResponse[0].size(); ++j)
			x.polResponse[i][j] = polResponse[i][j].toIDLComplex();

	x.xPosition = row->getXPosition().toIDLLength();

	x.yPosition = row->getYPosition().toIDLLength();

	x.zPosition = row->getZPosition().toIDLLength();

	vector<Angle> receptorAngle = row->getReceptorAngle();
	x.receptorAngle.length(receptorAngle.size());
	for (unsigned int i = 0; i < receptorAngle.size(); ++i)
		x.receptorAngle[i] = receptorAngle[i].toIDLAngle();

	x.antennaId = row->getAntennaId().toIDLTag();

	x.spectralWindowId = row->getSpectralWindowId().toIDLTag();

	vector<Tag> beamIdSet = row->getBeamIdSet();
	x.beamIdSet.length(beamIdSet.size());
	for (unsigned int i = 0; i < beamIdSet.size(); ++i)
		x.beamIdSet[i] = beamIdSet[i].toIDLTag();

	vector<ArrayTime> timeIdSet = row->getTimeIdSet();
	x.timeIdSet.length(timeIdSet.size());
	for (unsigned int i = 0; i < timeIdSet.size(); ++i)
		x.timeIdSet[i] = timeIdSet[i].toIDLArrayTime();

	vector<Interval> intervalIdSet = row->getIntervalIdSet();
	x.intervalIdSet.length(intervalIdSet.size());
	for (unsigned int i = 0; i < intervalIdSet.size(); ++i)
		x.intervalIdSet[i] = intervalIdSet[i].toIDLInterval();
	cout << "3. Created a FeedRow IDL struct and populated it with data from the row." << endl;
	
	// 4. Now, create a second row of the table and populate
	//	  it with data from the IDL structure.
	//------------------------------------------------------
	row = feed.newRow();

	row->setFeedId((int)x.feedId);

	ArrayTime time(x.time);
	row->setTime(time);

	Interval interval(x.interval);
	row->setInterval(interval);

	row->setNumReceptors(x.numReceptors);

	row->setFeedNum(x.feedNum);
	
	vector< vector< double> > beamOffsetIn(x.beamOffset.length());
	for (unsigned int i = 0; i < beamOffsetIn.size(); ++i)
		beamOffsetIn[i] = *(new vector<double> (x.beamOffset[0].length()));
	for (unsigned int i = 0; i < beamOffsetIn.size(); ++i)
		for (unsigned int j = 0; j < beamOffsetIn[0].size(); ++j)
			beamOffsetIn[i][j] = x.beamOffset[i][j];
	row->setBeamOffset(beamOffsetIn);

	vector< vector< double> > focusReferenceIn(x.focusReference.length());
	for (unsigned int i = 0; i < focusReferenceIn.size(); ++i)
		focusReferenceIn[i] = *(new vector<double> (x.focusReference[0].length()));
	for (unsigned int i = 0; i < focusReferenceIn.size(); ++i)
		for (unsigned int j = 0; j < focusReferenceIn[0].size(); ++j)
			focusReferenceIn[i][j] = x.focusReference[i][j];
	row->setFocusReference(focusReferenceIn);

	row->setIllumOffset(x.illumOffset);

	row->setIllumOffsetPa(x.illumOffsetPa);

	vector<string> polarizationTypeIn(x.polarizationType.length());
	for (unsigned int i = 0; i < polarizationTypeIn.size(); ++i) {
		polarizationTypeIn[i] = *(new string(x.polarizationType[i]));
	}
	row->setPolarizationType(polarizationTypeIn);
	
	vector< vector< Complex> > polResponseIn(x.polResponse.length());
	for (unsigned int i = 0; i < polResponseIn.size(); ++i)
		polResponseIn[i] = *(new vector<Complex> (x.polResponse[0].length()));
	for (unsigned int i = 0; i < polResponseIn.size(); ++i)
		for (unsigned int j = 0; j < polResponseIn[0].size(); ++j)
			polResponseIn[i][j] = *(new Complex(polResponse[i][j]));
	row->setPolResponse(polResponseIn);

	Length xPositionIn(x.xPosition);
	row->setXPosition(xPositionIn);

	Length yPositionIn(x.yPosition);
	row->setYPosition(yPositionIn);

	Length zPositionIn(x.zPosition);
	row->setZPosition(zPositionIn);

	vector<Angle> receptorAngleIn(x.receptorAngle.length());
	for (unsigned int i = 0; i < receptorAngleIn.size(); ++i)
		receptorAngleIn[i] = *(new Angle(x.receptorAngle[i]));
	row->setReceptorAngle(receptorAngleIn);

	// There is an error in the Tag.h file.
	string antennaIdIn(x.antennaId.value);
	row->setAntennaId(*(new Tag (antennaIdIn)));

	string spectralWindowIdIn(x.spectralWindowId.value);
	row->setSpectralWindowId(*(new Tag(spectralWindowIdIn)));

	vector<Tag> beamIdSetIn(x.beamIdSet.length());
	for (unsigned int i = 0; i < beamIdSetIn.size(); ++i) {
		string tmp(x.beamIdSet[i].value);
		beamIdSetIn[i] = *(new Tag(tmp));
	}
	row->addBeamLink(beamIdSetIn);

	vector<ArrayTime> timeIdSetIn(x.timeIdSet.length());
	for (unsigned int i = 0; i < timeIdSetIn.size(); ++i)
		timeIdSetIn[i] = *(new ArrayTime(x.timeIdSet[i]));
		
	vector<Interval> intervalIdSetIn(x.intervalIdSet.length());
	for (unsigned int i = 0; i < intervalIdSetIn.size(); ++i)
		intervalIdSetIn[i] = *(new Interval(x.intervalIdSet[i]));
		
	row->addReceiverLink(timeIdSetIn,intervalIdSetIn);
	cout << "4. Filled a row of the Feed table with data from the IDL struct." << endl;
	cout << row->toXML() << endl;
	
	// 5. Add the second row to the Feed table.
	// Change the keys.
	//-----------------------------------------
	row->setFeedId(4);
	feed.add(row);

	return 0;
}

//---------------
// Test of IDLTypes
//---------------
int runTestIDLTypes() {
	cout << "Test of IDLTypes." << endl;
	
	IDLAngle a1;
	a1.value = 1.23456789;
	Angle a2(a1);
	IDLAngle a3 = a2.toIDLAngle();
	cout << "IDLAngle1 = " << a1.value  << " Angle2 = " << a2.toString() << " IDLAngle3 = " << a3.value << endl;

	IDLAngularRate b1;
	b1.value = 12.3456789;
	AngularRate b2(b1);
	IDLAngularRate b3 = b2.toIDLAngularRate();
	cout << "IDLAngularRate1 = " << b1.value  << " AngularRate2 = " << b2.toString() << " IDLAngularRate3 = " << b3.value << endl;

	IDLArrayTime c1;
	c1.value = 12345678900LL;
	ArrayTime c2(c1);
	IDLArrayTime c3 = c2.toIDLArrayTime();
	cout << "IDLArrayTime1 = " << c1.value  << " ArrayTime2 = " << c2.get() << " IDLArrayTime3 = " << c3.value << endl;

	IDLComplex d1;
	d1.re = 123.456789;
	d1.im = 1234.56789;
	Complex d2(d1);
	IDLComplex d3 = d2.toIDLComplex();
	cout << "IDLComplex1 = " << d1.re << " " << d1.im << " " 
		 << "Complex2 = " << d2.real() << " " << d2.imag() << " " 
		 << "IDLcomplex3 = " << d3.re << " " << d3.im << endl;

	IDLEntity e1;
	e1.entityId = "uid://X0000000000000079/X00000000";
	e1.entityIdEncrypted = "entityIdEncrypted";
	e1.entityTypeName = "entityTypeName";
	e1.entityVersion = "entityVersion";
	e1.instanceVersion = "instanceVersion";
	Entity e2(e1);
	IDLEntity e3 = e2.toIDLEntity();
	cout << "IDLEntity1 = " 
		 << e1.entityId << ", " 
		 << e1.entityIdEncrypted << ", " 
		 << e1.entityTypeName << ", "
		 << e1.entityVersion << ", "
		 << e1.instanceVersion 
		 << "Entity2 = " 
		 << e2.getEntityId().toString() << ", " 
		 << e2.getEntityIdEncrypted() << ", " 
		 << e2.getEntityTypeName() << ", "
		 << e2.getEntityVersion() << ", "
		 << e2.getInstanceVersion() 
		 << "IDLEntity3 = " 
		 << e3.entityId << ", " 
		 << e3.entityIdEncrypted << ", " 
		 << e3.entityTypeName << ", "
		 << e3.entityVersion << ", "
		 << e3.instanceVersion 
		 << endl;
	
	IDLEntityId f1;
	f1.value = "uid://X0000000000000079/X00000000";
	EntityId f2(f1);
	IDLEntityId f3 = f2.toIDLEntityId();
	cout << "IDLEntityId1 = " << f1.value << " EntityId2 = " << f2.toString() << " IDLEntityId3 = " << f3.value << endl;
	
	IDLEntityRef g1;
	g1.entityId = "uid://X0000000000000079/X00000000";
	g1.partId = "X00000000";
	g1.entityTypeName = "entityTypeName";
	g1.instanceVersion = "instanceVersion";
	EntityRef g2(g1);
	IDLEntityRef g3 = g2.toIDLEntityRef();
	cout << "IDLEntityRef1 = " 
		 << g1.entityId << ", " 
		 << g1.partId << ", "
		 << g1.entityTypeName << ", "
		 << g1.instanceVersion << " "
		 << "EntityRef1 = " 
		 << g2.getEntityId().toString() << ", " 
		 << g2.getPartId().toString() << ", "
		 << g2.getEntityTypeName() << ", "
		 << g2.getInstanceVersion() << " "
		 << "IDLEntityRef3 = "
		 << g1.entityId << ", " 
		 << g1.partId << ", "
		 << g1.entityTypeName << ", "
		 << g1.instanceVersion
		 << endl;
	
	IDLFlux h1;
	h1.value = 1.23456789;
	Flux h2(h1);
	IDLFlux h3 = h2.toIDLFlux();
	cout << "IDLFlux1 = " << h1.value  << " Flux2 = " << h2.toString() << " IDLFlux3 = " << h3.value << endl;

	IDLFrequency i1;
	i1.value = 12345678.9;
	Frequency i2(i1);
	IDLFrequency i3 = i2.toIDLFrequency();
	cout << "IDLFrequency1 = " << i1.value  << " Frequency2 = " << i2.toString() << " IDLFrequency3 = " << i3.value << endl;

	IDLHumidity j1;
	j1.value = .123456789;
	Humidity j2(j1);
	IDLHumidity j3 = j2.toIDLHumidity();
	cout << "IDLHumidity1 = " << j1.value  << " Humidity2 = " << j2.toString() << " IDLHumidity3 = " << j3.value << endl;

	IDLInterval k1;
	k1.value = 1234567890L;
	Interval k2(k1);
	IDLInterval k3 = k2.toIDLInterval();
	cout << "IDLInterval1 = " << k1.value  << " Interval2 = " << k2.toString() << " IDLInterval3 = " << k3.value << endl;

	IDLLength l1;
	l1.value = 1234.56789;
	Length l2(l1);
	IDLLength l3 = l2.toIDLLength();
	cout << "IDLLength1 = " << l1.value  << " Length2 = " << l2.toString() << " IDLLength3 = " << l3.value << endl;

	IDLPressure m1;
	m1.value = 123.456789;
	Pressure m2(m1);
	IDLPressure m3 = m2.toIDLPressure();
	cout << "IDLPressure1 = " << m1.value  << " Pressure2 = " << m2.toString() << " IDLPressure3 = " << m3.value << endl;

	IDLSpeed n1;
	n1.value = 12.3456789;
	Speed n2(n1);
	IDLSpeed n3 = n2.toIDLSpeed();
	cout << "IDLSpeed1 = " << n1.value  << " Speed2 = " << n2.toString() << " IDLSpeed3 = " << n3.value << endl;

	IDLTag o1;
	o1.value = "tag";
	Tag o2(o1);
	IDLTag o3 = o2.toIDLTag();
	cout << "IDLTagId1 = " << o1.value << " Tag2 = " << o2.toString() << " IDLTag3 = " << o3.value << endl;

	IDLTemperature p1;
	p1.value = 12.3456789;
	Temperature p2(p1);
	IDLTemperature p3 = p2.toIDLTemperature();
	cout << "IDLTemperature1 = " << p1.value  << " Temperature2 = " << p2.toString() << " IDLTemperature3 = " << p3.value << endl;

	cout << "End test of IDLTypes" << endl << endl;
	return 0;
}



int main(char *arg) {
	runTestIDLTypes();
	runTestIDL();
	
	return 0;
}
