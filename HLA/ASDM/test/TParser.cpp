/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TParser.cpp
 */

#include <iostream>
#include <string>
#include <vector>

#include <Angle.h>
#include <AngularRate.h>
#include <ArrayTime.h>
#include <Complex.h>
#include <Entity.h>
#include <EntityId.h>
#include <EntityRef.h>
#include <Flux.h>
#include <Frequency.h>
#include <Humidity.h>
#include <Interval.h>
#include <Length.h>
#include <Pressure.h>
#include <Speed.h>
#include <Tag.h>
#include <Temperature.h>

#include <Parser.h>

#include <ConversionException.h>
#include <DuplicateKey.h>
#include <NoSuchRow.h>
#include <Integer.h>

using namespace std;
using asdm::Angle;
using asdm::AngularRate;
using asdm::ArrayTime;
using asdm::Complex;
using asdm::Entity;
using asdm::EntityId;
using asdm::EntityRef;
using asdm::Flux;
using asdm::Frequency;
using asdm::Humidity;
using asdm::Interval;
using asdm::Length;
using asdm::Pressure;
using asdm::Speed;
using asdm::Tag;
using asdm::Temperature;

using asdm::Parser;

using asdm::ConversionException;
using asdm::DuplicateKey;
using asdm::NoSuchRow;
using asdm::Integer;


//---------------
// Test of Parser
//---------------
int runTestParser() {
	cout << "Test of Parser." << endl;

	string s1 = "abc def ghi";
	cout << "[" << Parser::substring(s1,4,7) << "]" << endl;
	string s2 = Parser::substring(s1,3,8);
	cout << "[" << s2 << "]" << endl;
	cout << Parser::trim(s2) << endl;
	string s3 = Parser::trim(s2);
	cout << "[" << s3 << "]" << endl;

	string test1DString = "<test> 1 2 \"string 0\" \"string 1\" </test>";
	try {
		vector<string> value1 = Parser::get1DString("test","testtable",test1DString);
		for (unsigned int i = 0; i < value1.size(); ++i)
			cout << "[" << i << "] " << value1[i] << endl;
	} catch (ConversionException e) {
		cout << "Error 1. " << e.getMessage() << endl;
		return 0;
	}

	string test2DString = "<test> 2 2 2 \"string 00\" \"string 01\" \"string 10\" \"string 11\" </test> ";
	try {
		vector <vector<string> > value2 = Parser::get2DString("test","testtable",test2DString);
		for (unsigned int i = 0; i < value2.size(); ++i)
			for (unsigned int j = 0; j < value2[i].size(); ++j)
				cout << "[" << i << "," << j << "] " << value2[i][j] << endl;		
	} catch (ConversionException e) {
		cout << "Error 2. " << e.getMessage()<< endl;
		return 0;
	}

	string test3DString = "<test> 3 2 2 2 \"string 000\" \"string 001\" \"string 010\" \"string 011\" \"string 100\" \"string 101\" \"string 110\" \"string 111\" </test> ";
	try {
		vector <vector <vector<string> > > value3 = Parser::get3DString("test","testtable",test3DString);
		for (unsigned int i = 0; i < value3.size(); ++i)
			for (unsigned int j = 0; j < value3[i].size(); ++j)
				for (unsigned int k = 0; k < value3[i][j].size(); ++k)
					cout << "[" << i << "," << j << "," << k << "] " << value3[i][j][k] << endl;
	} catch (ConversionException e) {
		cout << "Error 3. " << e.getMessage() << endl;
		return 0;
	}
		
	string test1DInt = "<test> 1 5 98 76 54 32 10 </test>";
	try {
		vector<int> value4 = Parser::get1DInteger("test","testtable",test1DInt);
		for (unsigned int i = 0; i < value4.size(); ++i)
			cout << "[" << i << "] " << value4[i] << endl;
	} catch (ConversionException e) {
		cout << "Error 4. " << e.getMessage() << endl;
		return 0;
	}
	
	string test2DInt = "<test> 2 2 5 98 76 54 32 10 12 34 56 78 90 </test>";
	try {
		vector <vector<int> > value4b = Parser::get2DInteger("test","testtable",test2DInt);
		for (unsigned int i = 0; i < value4b.size(); ++i)
			for (unsigned int j = 0; j < value4b[i].size(); ++j)
				cout << "[" << i << ", " << j << "] " << value4b[i][j] << endl;
	} catch (ConversionException e) {
		cout << "Error 5. " << e.getMessage() << endl;
		return 0;
	}
	
	string test3DInt = "<test> 3 2 5 1 98 76 54 32 10 12 34 56 78 90 </test>";
	try {
		vector <vector <vector<int> > > value4c = Parser::get3DInteger("test","testtable",test3DInt);
		for (unsigned int i = 0; i < value4c.size(); ++i)
			for (unsigned int j = 0; j < value4c[i].size(); ++j)
				for (unsigned int k = 0; k < value4c[i][j].size(); ++k)
					cout << "[" << i << ", " << j << ", " << k << "] " << value4c[i][j][k] << endl;
	} catch (ConversionException e) {
		cout << "Error 6. " << e.getMessage() << endl;
		return 0;
	}
	
	/**/
	string e1 = "<Entity entityId=\"uid://X0000000000000079/X00000000\" entityIdEncrypted=\"na\" entityTypeName=\"FeedTable\" schemaVersion=\"1\" documentVersion=\"1\"/>";
	string e2 = "<Entity entityId=\"uid://X0000000000000080/X00000000\" entityIdEncrypted=\"na\" entityTypeName=\"FeedTable\" schemaVersion=\"1\" documentVersion=\"1\"/>";
	string en = "<test> 1 2 " + e1 + " " + e2 + " </test>";
	try {
		vector<Entity> value5 = Parser::get1DEntity("test","testtable",en);
		for (unsigned int i = 0; i < value5.size(); ++i)
			cout << "[" << i << "] " << value5[i].toString() << endl;
	} catch (ConversionException e) {
		cout << "Error 7. " << e.getMessage() << endl;
		return 0;
	}
	/**/
	
	string test1DChar = "<test> 1 26 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z </test>";
	try {
		vector<unsigned char> value6 = Parser::get1DCharacter("test","testtable",test1DChar);
		for (unsigned int i = 0; i < value6.size(); ++i)
			cout << "[" << i << "] " << value6[i] << endl;
	} catch (ConversionException e) {
		cout << "Error 8. " << e.getMessage() << endl;
		return 0;
	}
	
	cout << "End test of Parser" << endl << endl;
	return 0;
}

int main(char *arg) {

	runTestParser();

	return 0;

}
