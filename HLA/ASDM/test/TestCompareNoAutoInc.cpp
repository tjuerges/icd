/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestWriteXML.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDM.h>
#include <MainTable.h>
#include <AlmaCorrelatorModeTable.h>
#include <AntennaTable.h>
#include <BeamTable.h>
#include <CalDeviceTable.h>
#include <ConfigDescriptionTable.h>
#include <DataDescriptionTable.h>
#include <DopplerTable.h>
#include <EphemerisTable.h>
#include <ExecBlockTable.h>
#include <FeedTable.h>
#include <FieldTable.h>
#include <FlagCmdTable.h>
#include <FocusTable.h>
#include <FocusModelTable.h>
#include <FreqOffsetTable.h>
#include <GainTrackingTable.h>
#include <HistoryTable.h>
#include <ObservationTable.h>
#include <PointingTable.h>
#include <PointingModelTable.h>
#include <PolarizationTable.h>
#include <ProcessorTable.h>
#include <ReceiverTable.h>
#include <SBSummaryTable.h>
#include <ScanTable.h>
#include <SeeingTable.h>
#include <SourceTable.h>
#include <SourceParameterTable.h>
#include <SpectralWindowTable.h>
#include <StateTable.h>
#include <SubscanTable.h>
#include <SwitchCycleTable.h>
#include <SysCalTable.h>
#include <TotalPowerMonitoringTable.h>
#include <TransitionTable.h>
#include <WVMCalTable.h>
#include <WeatherTable.h>

#include <MainRow.h>
#include <AlmaCorrelatorModeRow.h>
#include <AntennaRow.h>
#include <BeamRow.h>
#include <CalDeviceRow.h>
#include <ConfigDescriptionRow.h>
#include <DataDescriptionRow.h>
#include <DopplerRow.h>
#include <EphemerisRow.h>
#include <ExecBlockRow.h>
#include <FeedRow.h>
#include <FieldRow.h>
#include <FlagCmdRow.h>
#include <FocusRow.h>
#include <FocusModelRow.h>
#include <FreqOffsetRow.h>
#include <GainTrackingRow.h>
#include <HistoryRow.h>
#include <ObservationRow.h>
#include <PointingRow.h>
#include <PointingModelRow.h>
#include <PolarizationRow.h>
#include <ProcessorRow.h>
#include <ReceiverRow.h>
#include <SBSummaryRow.h>
#include <ScanRow.h>
#include <SeeingRow.h>
#include <SourceRow.h>
#include <SourceParameterRow.h>
#include <SpectralWindowRow.h>
#include <StateRow.h>
#include <SubscanRow.h>
#include <SwitchCycleRow.h>
#include <SysCalRow.h>
#include <TotalPowerMonitoringRow.h>
#include <TransitionRow.h>
#include <WVMCalRow.h>
#include <WeatherRow.h>

#include <Frequency.h>

using namespace asdm;

	int main(char *arg) {
		ASDM dataset;
		
		// A first test with DataDescription.
		DataDescriptionTable &dataDescription = dataset.getDataDescription();
		
		DataDescriptionRow* aDDRow;
		bool result;
		string msg;
		
		// Let's populate the table with one row.
		string refMsg = "false, Tag(\"0\"), Tag(\"0\")";
		aDDRow = dataDescription.newRow(false, Tag("0"), Tag("0"));
		dataDescription.addAutoInc(aDDRow);
		
		// And now test the compareNoAutoInc method	on this row.
		// should say "different from"
		msg = "false, Tag(\"1\"), Tag(\"1\")";
		result = aDDRow->compareNoAutoInc(false, Tag("1"), Tag("1"));
		cout << msg << " is " << ((result)?"equal to ":"different from ") << refMsg << endl;

		// should say "equal to"
		msg = "false, Tag(\"0\"), Tag(\"0\")";
		result = aDDRow->compareNoAutoInc(false, Tag("0"), Tag("0"));
		cout << msg << " is " << ((result)?"equal to ":"different from ") << refMsg << endl;
		
		
		// A second test with Doppler.
		DopplerTable &doppler = dataset.getDoppler();
		int transitionId = 1;
		Speed veldef = Speed(1000.0);
		int sourceId = 1;
		
		// Create a FeedRow.
		DopplerRow* aDRow = doppler.newRow(transitionId, veldef);
		
		// Append it to its table with sourceId = 1.
		doppler.addAutoInc(sourceId, aDRow);
		
		refMsg = "1, 1, Speed(1000.)";
		
		// And now test the compareNoAutoInc method on this row
		// should say "different from"
		msg = "1, 1, Speed(1001.0)";
		result = aDRow->compareNoAutoInc(1, 1, Speed(1001.0));
		cout << msg << " is " << ((result)?"equal to ":"different from ") << refMsg << endl;
		
		
		// should say "equal to"
		msg = "1, 1, Speed(1000.0)";
		result = aDRow->compareNoAutoInc(1, 1, Speed(1000.0));
		cout << msg << " is " << ((result)?"equal to ":"different from ") << refMsg << endl;		
		return 0;	
	}
