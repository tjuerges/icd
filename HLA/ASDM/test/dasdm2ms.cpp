#include <iostream>
#include <sstream>
#include <vector>

#include <ASDMAll.h>
using namespace asdm;

#include <alma/MeasurementSets/G2MSFiller.h>
//using namespace casa;


G2MSFiller* ms;


void error(string message) {
  cout << message << endl;
  exit(1);
}

#include <iostream>
#include <sstream>

ostringstream err;
using namespace std;

// A facility to get rid of blanks at start and end of the a string.
// 
string lrtrim(std::string& s,const std::string& drop = " ")
  {
    std::string r=s.erase(s.find_last_not_of(drop)+1);
    return r.erase(0,r.find_first_not_of(drop));
  }


// These classes provides methods to convert from Vectors to Arrays.
class DConverter {
private :
  double* da;
  
  
public :
  ~DConverter(); 
  double* to1DArray(vector<double>);
  double* to1DArray(vector<vector<double> > v);
  double* to1DArray(vector<Frequency>);
  double* to1DArray(vector<Angle>);
  double* to1DArray(double, double, double);
  double* to1DArray(Length, Length, Length);
};

DConverter::~DConverter() {
  delete[] da;
  // cout << "DConverter has released space" << endl;
}

double* DConverter::to1DArray(vector<double> v) {
  da = new double[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    da[i] = v.at(i);

  return da;
}


double* DConverter::to1DArray(vector<vector<double> > v) {
  // Calculates the number of integers stored in v.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++) n += v.at(i).size();

  // Now we can allocate and copy with a transposition.
  da = new double[n];
  int k = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      da[k++] = v.at(i).at(j);

  return da;

}

double* DConverter::to1DArray(vector<Frequency> v) {
  da = new double[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    da[i] = v.at(i).get();

  return da;
}

double*  DConverter::to1DArray(vector<Angle> v) {
  da = new double[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    da[i] = v.at(i).get();

  return da;
}

 double* DConverter::to1DArray(double e0, double e1, double e2) {
  da = new double[3];

  da[0]=e0;
  da[1]=e1;
  da[2]=e2;
  
  return da;
}

 double* DConverter::to1DArray(Length e0, Length e1, Length e2) {
  da = new double[3];

  da[0]=e0.get();
  da[1]=e1.get();
  da[2]=e2.get();
  
  return da;
}

class IConverter {
private :
  int* ia;  
  
public :
  ~IConverter(); 
  int* to1DArray(vector<int>);
  int* to1DArray(vector<vector <int> >);
};

IConverter::~IConverter() {
  delete[] ia;
  // cout << "DConverter has released space" << endl;
}

int* IConverter::to1DArray(vector<int> v) {
  ia = new int[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    ia[i] = v.at(i);

  return ia;
}

int* IConverter::to1DArray(vector<vector<int> > v) {
  // Calculates the number of integers stored in v.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++) n += v.at(i).size();

  // Now we can allocate and copy with a transposition.
  ia = new int[n];
  int k = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      ia[k++] = v.at(i).at(j);

  return ia;
}


class SConverter {
private :
  char* sa;  
  
public :
  ~SConverter(); 
  char* to1DCharArray(vector<string>);
  char* to1DCharArray(vector<vector <string> >);
};

SConverter::~SConverter() {
  delete[] sa;
  // cout << "DConverter has released space" << endl;
}

char* SConverter::to1DCharArray(vector<string> v) {
  // Calculates the number of char to store in result.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++)
    n += v.at(i).size();

  // Reserve the appropriate number of chars.
  sa = new char[n+1];
  sa[n] = 0; // Not really useful but...who knows...

  int k = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j=0; j < v.at(i).size(); j++)
      sa[k++] = v.at(i).at(j);

  return sa;
}


char* SConverter::to1DCharArray(vector< vector<string> > v) {
  // Calculates the number of char to store in result.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      n += v.at(i).at(j).size();

  // Reserve the appropriate number of chars.
  sa = new char[n+1];
  sa[n] = 0; // Not really useful but...who knows...

  int l = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      for (unsigned int k = 0; k < v.at(i).at(j).size(); k++)
	sa[l++] = v.at(i).at(j).at(k);

  return sa;
}



class CConverter {
private :
  double* ra;
  double* ia;

    
public :
  ~CConverter(); 
  double* to1DArrayR(vector<asdm::Complex>);
  double* to1DArrayI(vector<asdm::Complex>);
  double* to1DArrayR(vector< vector<asdm::Complex> >);
  double* to1DArrayI(vector< vector<asdm::Complex> >);

};

CConverter::~CConverter() {
  if (ra != 0) delete[] ra;
  if (ia != 0) delete[] ia;
  // cout << "DConverter has released space" << endl;
}


double* CConverter::to1DArrayR(vector<asdm::Complex> v) {
  ra = new double[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    ra[i] = v.at(i).getReal();

  return ra;
}

double* CConverter::to1DArrayI(vector<asdm::Complex> v) {
  ia = new double[v.size()];
  for (unsigned int i=0; i < v.size(); i++)
    ia[i] = v.at(i).getImg();

  return ia;
}


double* CConverter::to1DArrayR(vector< vector<asdm::Complex> > v) {
  // Calculates the number of integers stored in v.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++) n += v.at(i).size();

  // Now we can allocate and copy with a transposition.
  ra = new double[n];
  int k = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      ra[k++] = v.at(i).at(j).getReal();
  
  return ra;
}


double* CConverter::to1DArrayI(vector< vector<asdm::Complex> > v) {
  // Calculates the number of integers stored in v.
  int n = 0;
  for (unsigned int i = 0; i < v.size(); i++) n += v.at(i).size();

  // Now we can allocate and copy with a transposition.
  ra = new double[n];
  int k = 0;
  for (unsigned int i=0; i < v.size(); i++)
    for (unsigned int j = 0; j < v.at(i).size(); j++)
      ra[k++] = v.at(i).at(j).getImg();
  
  return ra;
}

int main(int argc, char *argv[]) {


  //
  // Do we have the name of a DataSet passed in argument 1 ?
  if (argc < 2) {
    cout << "Usage : dasdm2ms dataSetName" << endl;
    exit(0);
  }

  //
  // Try to open a Dataset whose name has been passed in argument of the command line
  //
  const std::string drop = std::string(" ");
  string dummy = string(argv[1]);
  string dsName = lrtrim(dummy);
  if ( dsName.at(dsName.size()-1) == '/' ) dsName.erase(dsName.size()-1);

  const char* msName = dsName.c_str();

  ASDM* ds = 0;
  try {
    cout << "Taking the dataset " << dsName << " as input." << endl;
    ds = ASDM::getFromXML(dsName); 	
  }
  catch (ConversionException e) {
    cout << e.getMessage() << endl;
    exit(1);
  }
  
  //
  // Create a Measurement Set.
  cout << "About to create a new measurement set '" << msName << ".ms" << "'" << endl;
  ostringstream ost;
  ost << msName << ".ms" ;
  ms = new G2MSFiller(ost.str().c_str(), 0.0, false);
  //ms = new G2MSFiller((const char *)strcat((const char*)msName ,".ms"), 0.0, false);


  //
  // Firstly convert the basic tables.
  //
  // For purpose of simplicity we assume that in all ASDM basic tables having a Tag as their identifier
  // these Tag values are forming a sequence of integer 0 -> table.size() - 1. If that's not the case, the
  // program aborts.
  //
  //
  // Antenna table conversion.
  //  
  AntennaTable& aT = ds->getAntenna();

  //
  // Read/Write the Antenna table.
  {
    AntennaRow* r = 0;
    int nAntenna = aT.size();
    cout << "The data set has " << nAntenna << " antennas...";
    for (int i = 0; i < nAntenna; i++) {
      if ((r = aT.getRowByKey(Tag(i))) == 0){
	err.str("");
	err << "Problem while reading the Antenna table, the row with key = Tag(" << i << ") does not exist.Aborting." << endl;
	error(err.str());
      }

      ms->addAntenna(r->getName().c_str(),
		     r->getStation().c_str(),
		     r->getXPosition().get(),
		     r->getYPosition().get(),
		     r->getZPosition().get(),
		     r->getXOffset().get(),
		     r->getYOffset().get(),
		     r->getZOffset().get(),
		     (float)r->getDishDiameter().get());
    }
    if (nAntenna) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }



  SpectralWindowTable& spwT = ds->getSpectralWindow();  
  //
  // Read/Write the SpectralWindow table.
  //
  {

    SpectralWindowRow* r = 0;
    int nSpectralWindow = spwT.size();
    cout << "The data set has " << nSpectralWindow << " spectral windows..."; 
    
    for (int i = 0; i < nSpectralWindow; i++) {
      if ((r = spwT.getRowByKey(Tag(i))) == 0) {
	err.str("");
	(err << "Problem while reading the SpectralWindow table, the row with key = Tag(" << i << ") does not exist.Aborting." << endl);
	error(err.str());
      }

      ms->addSpectralWindow(r->getNumChan(),
			    r->getName().c_str(),
			    (r->getRefFreq()).get(),
			    DConverter().to1DArray(r->getChanFreq()),
			    DConverter().to1DArray(r->getChanWidth()),
			    r->getMeasFreqRef(),
			    DConverter().to1DArray(r->getEffectiveBw()),
			    DConverter().to1DArray(r->getResolution()),
			    (r->getTotBandwidth()).get(),
			    r->getNetSideband(),
			    r->getIfConvChain(),
			    r->getFreqGroup(),
			    r->getFreqGroupName().c_str());      
    }

    if (nSpectralWindow) cout << "successfully copied them into the measurement set.";
    cout << endl;

  }

  //
  // Read/Write the polarization table
  //
  PolarizationTable& polT = ds->getPolarization();  
  {
    PolarizationRow* r = 0;
    int nPolarization = polT.size();
    cout << "The data set has " << nPolarization << " polarizations..."; 
    
    for (int i = 0; i < nPolarization; i++) {
      if ((r=polT.getRowByKey(Tag(i))) == 0) {
	err.str("");
	(err << "Problem while reading the Polarization table, the row with key = Tag(" << i << ") does not exist.Aborting." << endl);
	error(err.str());
      }
      ms->addPolarization(r->getNumCorr(),
			  IConverter().to1DArray(r->getCorrType()),
			  IConverter().to1DArray(r->getCorrProduct())
			  );
    }
    if (nPolarization) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }


  //
  // Load the DataDescription table.
  DataDescriptionTable& ddT = ds->getDataDescription();
  {
    DataDescriptionRow* r = 0;
    int nDataDescription = ddT.size();
    cout << "The data set has " << nDataDescription << " data descriptions...";
    for (int i = 0; i < nDataDescription; i++) {
      if ((r=ddT.getRowByKey(Tag(i))) == 0) {
	err.str("");
	(err << "Problem while reading the DataDescription table, the row with key = Tag(" << i << ") does not exist.Aborting." << endl);
	error(err.str());
      }

      ms->addDataDescription(Integer::parseInt(r->getSpectralWindowId().getId()),
			     Integer::parseInt(r->getPolarizationId().getId()));
    }
    if (nDataDescription) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }


  //
  // Load the Feed table
  // Issues :
  //    - time (epoch) : at the moment it takes directly the time as it is stored in the ASDM.
  //    - focusLength (in AIPS++) is no defined.

  cout << "Fetching the feedTable" << endl;
  FeedTable& feedT = ds->getFeed();
  {
    FeedRow* r = 0;
    cout << "About to retrieve the Feed table size" << endl;
    int nFeed = feedT.size();
    cout << "The data set has " << nFeed << " feeds...";
    vector<FeedRow *> v = feedT.get();
    for (int i = 0; i < nFeed; i++) {
      r == v.at(i);
      

      // For now we just adapt the types of the time related informations and compute a mid-time.
      //
      double interval = ((double) r->getTimeInterval().getDuration().get()) / ArrayTime::unitsInASecond ;
      double time =  ((double) r->getTimeInterval().getStart().get()) / ArrayTime::unitsInASecond + interval/2.0;

      ms->addFeed(Integer::parseInt(r->getAntennaId().getId()),
		  r->getFeedId(),
		  Integer::parseInt(r->getSpectralWindowId().getId()),
		  time,
		  interval,
		  r->getNumReceptors(), 

		  -1,             // We ignore the beamId array
		  DConverter().to1DArray(r->getBeamOffset()),
		  SConverter().to1DCharArray(r->getPolarizationType()),
		  CConverter().to1DArrayR(r->getPolResponse()),
		  CConverter().to1DArrayI(r->getPolResponse()),
		  DConverter().to1DArray(r->getXPosition(), r->getYPosition(), r->getZPosition()),
		  DConverter().to1DArray(r->getReceptorAngle()));
    }
    if (nFeed) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

#if 0
  // Load the Field table
  {
    int nField = myDC->numField();
    cout << "The data set has " << nField << " fields...";

    // Reorder delayDir, phaseDir and referenceDir
    double delayDir[2];
    double phaseDir[2];
    double referenceDir[2];
    

    for (int i = 0; i < nField; i++) {
      asdmStructs::FieldMJDStruct_var f = myDC->getFieldMJD(i);
      delayDir[0] = f->delayDir[0]; delayDir[1] = f->delayDir[1];
      phaseDir[0] = f->phaseDir[0]; phaseDir[1] = f->phaseDir[1];
      referenceDir[0] = f->referenceDir[0]; referenceDir[1] = f->referenceDir[1];
    
      ms->addField(f->name,
		   f->code,
		   f->timeRef,
		   delayDir,
		   phaseDir,
		   referenceDir,
		   f->sourceId);
    }  
    if (nField) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

  // Load the FlagCmd table
  {
    int nFlagCmd = myDC->numFlagCmd();
    cout << "The data set has " << nFlagCmd << " flags...";

    for (int i = 0; i < nFlagCmd; i++) {
      asdmStructs::FlagCmdMJDStruct_var f = myDC->getFlagCmdMJD(i);
      
      ms->addFlagCmd(f->time,
		     f->interval,
		     f->flagType,
		     f->flagReason,
		     f->flagLevel,
		     f->flagSeverity,
		     f->flagApplied ? 1 : 0,
		     f->flagCommand);
    }
    if (nFlagCmd) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

  // Load the History table
  {
    int nHistory = myDC->numHistory();
    cout << "The data set has " << nHistory << " historys...";

    for (int i = 0; i < nHistory; i++) {
      asdmStructs::HistoryMJDStruct_var h = myDC->getHistoryMJD(i);

      ms->addHistory(h->time,
		     h->executeId,   // This should probably corrected !!! executeId is probably not observationId !!!
		     h->message,
		     h->priority,
		     h->origin,
		     -1,             // This should take h->objectId ...but now it's defined as a String not as an int...has to be corrected !!!
		     h->application,
		     h->CliCommand,
		     h->appParms);
    }
    if (nHistory) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

  // Load the Observation table
  // No observation table.


  // Load the Pointing table
  //
  {
    int nPointing = myDC->numPointing();
    cout << "The data set has " << nPointing << " pointings..."; 
    double direction[2];
    double target[2];
    double pointingOffset[2];
    double encoder[2];

    for (int i = 0; i < nPointing; i++) {
      asdmStructs::PointingMJDStruct_var p = myDC->getPointingMJD(i);
      
      direction[0] = p->pointingDirection[0];      direction[1] = p->pointingDirection[1];
      target[0] = p->pointingTarget[0];            target[1] = p->pointingTarget[1];
      pointingOffset[0] = p->pointingOffset[0];            pointingOffset[1] = p->pointingOffset[1];
      encoder[0] = p->encoder[0]; encoder[1] = p->encoder[1];

      ms->addPointing(p->antennaId,
		      p->time,
		      p->interval,
		      p->pointingName,
		      direction,
		      target,
		      pointingOffset,
		      encoder,
		      p->tracking ? 1 :0);      
    }
    if (nPointing) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

  // Load the processor table
  //
  {
  }


  // Load the processor table
  //
  {
    int nProcessor = myDC->numProcessor();
    cout << "The data set has " << nProcessor << " processors..."; 
    
    for (int i = 0; i < nProcessor; i++) {
      asdmStructs::ProcessorStruct_var p = myDC->getProcessor(i);
      
      ms->addProcessor(p->type,
		       p->subType,
		       -1,    // Since there is no typeId in the ASDM.
		       p->processorModeId);
    }
    if (nProcessor) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }
  
  
  // Load the source table
  //
  {
    int nSource = myDC->numSource();
    cout << "The data set has " << nSource << " sources...";
 
    double direction[2];
    double position[3];
    double properMotion[2];
    
    for (int i = 0; i < nSource; i++) {
      asdmStructs::SourceMJDStruct_var s = myDC->getSourceMJD(i);
      direction[0] = s->direction[0]; direction[1] = s->direction[1];
      position[0] = s->position[0]; position[1] = s->position[1]; position[2]=s->position[2];
      properMotion[0] = s->properMotion[0]; properMotion[1] = s->properMotion[1];

      char **transition = new char*[s->numLine];
      double *restFrequency = new double[s->numLine];
      double *sysVel = new double[s->numLine];

      for (int nl = 0; nl < s->numLine; nl++) {
	transition[nl] = s->transition[nl];
	restFrequency[nl] = s->restFreq[nl];
	sysVel[nl] = s->systemicVel[nl];
      }
      
      ms->addSource(s->sourceId,
		    s->time,
		    s->interval,
		    s->spectralwindowId,
		    s->numLine,
		    s->name,
		    s->calibrationGroup,
		    s->code,
		    direction,
		    position,
		    properMotion,
		    transition,
		    restFrequency,
		    sysVel);
      
      delete [] transition;
      delete [] restFrequency;
      delete [] sysVel;
    }
    if (nSource) cout << "successfully copied them into the measurement set." ;
    cout << endl;
  }

  // Load the spectralwindow table
  //
  {
    int nSpectralWindow = myDC->numSpectralWindow();
    cout << "The data set has " << nSpectralWindow << " spectral windows..."; 
    
    for (int i = 0; i < nSpectralWindow; i++) {
      asdmStructs::SpectralWindowStruct_var sw = myDC->getSpectralWindow(i);
      spwTable.push_back(sw);

      double *chanFreq    = new double[sw->numChan];
      double *chanWidth   = new double[sw->numChan];
      double *effectiveBW = new double[sw->numChan];
      double *resolution  = new double[sw->numChan];
      
      for (int nc = 0; nc < sw->numChan; nc++) {
	chanFreq[nc]  = sw->chanFreq[nc];
	chanWidth[nc] = sw->chanWidth[nc];
	effectiveBW[nc] = sw->effectiveBW[nc];
	resolution[nc] = sw->resol[nc];
      }
      
      ms->addSpectralWindow(sw->numChan,
			    sw->name,
			    sw->refFreq,
			    chanFreq,
			    chanWidth,
			    sw->measFreqRef,
			    effectiveBW,
			    resolution,
			    sw->totBW,
			    sw->netSideband,
			    sw->IFconvChain,
			    sw->freqGroup,
			    sw->freqGroupName);
      
      
      delete [] chanFreq;
      delete [] chanWidth;
      delete [] effectiveBW;
      delete [] resolution;
    }
    if (nSpectralWindow) cout << "successfully copied them into the measurement set.";
    cout << endl;
  }

  // Load the state table
  // TBD


  //
  // Load the weather table
  {
    int nWeather = myDC->numWeather();
    cout << "The data set has " << nWeather << " weathers...";
 
    for (int i = 0; i < nWeather; i++) {
      asdmStructs::WeatherMJDStruct_var w = myDC->getWeatherMJD(i);
      
      ms->addWeather(w->antennaId,
		     w->time,
		     w->time,
		     w->H2O,
		     0.0,
		     0.0,
		     w->pressure,
		     w->relHumidity,
		     w->temp,
		     w->dewPoint,
		     w->windDir,
		     w->windSpeed);
		     
    }
    if (nWeather) cout << "successfully copied them into the measurement set.";
    cout << endl;
  }

  // And then finally process the main table
  //
  {
    int nMain = myDC->numMain();
    cout << "The data set has " << nMain << " mains..."; 
    // int numDataDescId = myDC->numDataDescription();

    for (int i = 0; i < nMain; i++) {
      asdmStructs::MainMJDStruct_var m = myDC->getMainMJD(i);

      // What is the configuration description for this row of the main table ?
      //
      asdmStructs::ConfigDescriptionStruct_var c = myDC->getConfigDescription(m->configDescriptionId);
      
      // Retrieve the Antenna collection
      //
      int *antenna = new int[c->numAnt];
      for (int a = 0; a < c->numAnt; a++) antenna[a] = c->antennaArray[a];
      
      // Retrieve the DatadescId collection
      //
      // 1st compute its size
      int numDataDescId = 0;
      for (int nbb = 0; nbb < c->numBaseband; nbb++)  numDataDescId += c->numSubBand[nbb];
            
      int *dataDescId = new int[numDataDescId];
      for (int dd = 0; dd < numDataDescId; dd++) dataDescId[dd] = c->dataDescArray[dd];

      
      // Retrieve the Feed collection
      //
      int *feed = new int[c->numAnt];
      for (int f = 0; f < c->numAnt; f++) feed[f] = c->feedList[f];

      // Now we can start to populate the MS main table.
      // 
      for (int dd = 0; dd < numDataDescId; dd++) {
	// Determine the number of polarizations and spectral windows.
	//
	
	//asdmStructs::DataDescriptionStruct_var ddS = myDC->getDataDescription(dataDescId[dd]);
	asdmStructs::DataDescriptionStruct_var ddS = ddTable.at(dataDescId[dd]);
	//int nChan = myDC->getSpectralWindow(ddS->spectralwindowId)->numChan;
	int nChan = spwTable.at(ddS->spectralwindowId)->numChan;
	//int nPol  = myDC->getPolarization(ddS->polarizationId)->numPolar;
	int nPol  = pTable.at(ddS->polarizationId)->numPolar;

	// Allocate the space required to store visibilities, sigma, weight and flags 
	int nBaseLines = (c->numAnt * (c->numAnt + 1)) / 2;
	
	float *vis_r   = new float[nBaseLines * nChan * nPol];
	float *vis_i   = new float[nBaseLines * nChan * nPol];
	double *uvw    = new double[3*nBaseLines];
	float  *sigma  = new float[nBaseLines * nChan];
	float  *weight = new float[nBaseLines * nChan];
	char   *flag   = new char[nBaseLines];

	for (int i = 0; i < nBaseLines * nChan * nPol; i++) {
	  vis_r[i] = i;
	  vis_i[i] = i;
	}

	// Building UVW per baseline from the UVW per antenna.
	{ 
	  int k = 0;
	  for (int i = 0; i < c->numAnt - 1; i++)
	    for (int j = i+1; j < c->numAnt; j++){
	      uvw[k]   = m->UVW[3*i]   - m->UVW[3*j];
	      uvw[k+1] = m->UVW[3*i+1] - m->UVW[3*j+1];
	      uvw[k+2] = m->UVW[3*i+2] - m->UVW[3*j+2];
	      k += 3;
	    }
	}
	
	ms->addData(m->time,
		    m->interval,
		    10.0,  // Exposure = 10. is only a temporary value...should be extracted from the data block read into the Bulkstore.
		    100.0, // TimeCentroid = 100. same than above
		    c->numAnt,
		    antenna,
		    feed,
		    dataDescId[dd],
		    m->fieldId,
		    uvw,
		    vis_r,
		    vis_i,
		    sigma,
		    weight,
		    flag);
      delete [] vis_r;
      delete [] vis_i;
      delete [] uvw;
      delete [] sigma;
      delete [] weight;
      delete [] flag;

      }
    }
    if (nMain) cout << "successfully copied them into the measurement set.";
    cout << endl;
  }
  cout << "About to flush and close the measurement set." << endl;

  myDC->close();
  
  myDC->disconnect();
  ACE_OS::sleep(3);   
#endif
  ms->end(0.0);
}
