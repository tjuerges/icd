#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include "BaselineFlagsMetadata.h"
#include "ActualTimeDurations.h"

using namespace asdmBinary;

int main (int argc, char* argv[]) {

  cout << endl;
  cout << "programme to test the class BaselineFlagsMetadata:" << endl;
  cout << "================================================= " << endl;

  cout << endl;
  cout << "General setup:" << endl;
  cout << "------------- " << endl;

  int nbb=4;                                 cout << "Number of basebands:           " << nbb  << endl;
  int nant=5;                                cout << "Number of scheduled antennas:  " << nant << endl;
  int nbl=(nant*(nant-1))/2;                 cout << "Number of scheduled baselines: " << nbl  << endl;
	   
  vector<int> v_nbin;
  vector<int> v_npp;

  v_nbin.push_back(1);  v_npp.push_back(4);  cout << "Baseband 1:  numBin=" << v_nbin[0] << "  numPP=" <<  v_npp[0] << endl;
  v_nbin.push_back(2);  v_npp.push_back(2);  cout << "Baseband 2:  numBin=" << v_nbin[1] << "  numPP=" <<  v_npp[1] << endl;
  v_nbin.push_back(2);  v_npp.push_back(2);  cout << "Baseband 3:  numBin=" << v_nbin[2] << "  numPP=" <<  v_npp[2] << endl;
  v_nbin.push_back(1);  v_npp.push_back(4);  cout << "Baseband 4:  numBin=" << v_nbin[3] << "  numPP=" <<  v_npp[3] << endl;


  cout << endl;
  cout << "Tests:" << endl;
  cout << "===== " << endl;

  cout << endl;
  cout << "Test of the methods in the BaselineMetadata abstract class:" << endl;
  cout << "========================================================== " << endl; 

  BaselineFlagsMetadata blf( v_npp, v_nbin, nbb, nant, nbl);
  cout << "the baselineFlag object has " << blf.numAnt() << " scheduled antennas " << endl;
  cout << "its setup corresponds to correlationMode=" << blf.correlationMode() << endl;
  for(int n=0; n<nbb; n++){
    cout << "Number of polarization product in baseband " << n+1 << ": " << blf.numPolProduct(n+1) << endl;
    if(blf.correlationMode()>0)
      cout << "Number of 'auto' polarization product:     " << n+1 << ": " << blf.numAutoPolProduct(n+1) << endl;
  }
  int i,j;
  cout << endl;
  cout << "INFORM: Numbers are 1 based" << endl;
  cout << "INFORM: Indices are 0 based" << endl;
  cout << endl;

  i=0; j=1;
  cout << "The antenna pair of indices "<<i<<","<<j<<" is the baseline number "<<blf.baselineNum(i,j)<<endl;
  i=0; j=3;
  cout << "The antenna pair of indices "<<i<<","<<j<<" is the baseline number "<<blf.baselineNum(i,j)<<endl;
  i=2; j=3;
  cout << "The antenna pair of indices "<<i<<","<<j<<" is the baseline number "<<blf.baselineNum(i,j)<<endl;
  i=3; j=nant; 
  cout << "The antenna pair of indices "<<i<<","<<j<<" is the baseline number "<<blf.baselineNum(i,j)<<" as expected!" << endl;
  i=0; j=4;
  cout << "The antenna pair of indices "<<i<<","<<j<<" is the baseline number "<<blf.baselineNum(i,j)<<endl;
  i=2;
  cout << "The antenna pair of indices "<<i<<","<<i<<" is the baseline number "<<blf.baselineNum(i,i)<<endl;

  cout << endl;
  cout << "The baselineMetadata container must host " << blf.numValue() << " values " << endl;
  cout<<endl;

  int npp_, nbin_, nbb_;
  

  npp_=0;       nbin_=0;      nbb_=0;   i=0;    j=1;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl;  


  npp_=1;       nbin_=0;      nbb_=0;   i=0;    j=1;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl;  

  npp_=0;       nbin_=0;      nbb_=1;   i=0;    j=1;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl;  


  npp_=3;       nbin_=0;      nbb_=3;   i=0;    j=1;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl;  

  npp_=3;       nbin_=0;      nbb_=3;   i=3;    j=4;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl;  

  npp_=2;       nbin_=0;      nbb_=3;   i=4;    j=4;  cout << "The value for npp=" << npp_ 
							   << " nbin=" << nbin_ 
							   << " nbb=" << nbb_ 
							   << " ant index pair:"<< i <<","<< j
							   <<" is " << blf.at(npp_,nbin_,nbb_,i,j) << endl; 


  cout << endl;


  cout << "Test of the method 'at(bool cross, int i)': " << endl;
  cout << "------------------------------------------  " << endl;
  vector<vector<vector<int> > > vvv_index;

  i = 0;  vvv_index=blf.at( true, i); cout << "There are " << vvv_index.size() 
					   << " non-zero baselines involving a given antenna" << endl;
  
  cout << "Every of these " << vvv_index.size() << " baselines involves " << vvv_index[0].size() << " basebands" << endl;
  for(int i=0; i<blf.numAnt(); i++){
    vvv_index=blf.at( true, i);
    cout << "Antenna pairs involving antenna index "<<i<<":"<<endl;
    int nb=0;
    for(int na=0; na<vvv_index.size(); na++){
      if(na==i)nb=na+1;
      for(int n=0; n<vvv_index[na].size(); n++)
	cout << "basebandNum " << n+1 << ": " << vvv_index[na][n].size() 
	     << " values with index range ["
	     << vvv_index[na][n][0] <<","<< vvv_index[na][n][vvv_index[0][n].size()-1] 
	     <<"] antenna index pair "<< i <<" with "<<nb<< endl;
      nb++;
    }
    cout << endl;
  }
  cout << endl;

  i = 0;  vvv_index=blf.at( false, i); cout << "There is " << vvv_index.size() 
					   << " zero baseline involving a given antenna" << endl;
  
  cout << "Every of these " << vvv_index.size() << " baselines involves " << vvv_index[0].size() << " basebands" << endl;
  for(int i=0; i<blf.numAnt(); i++){
    vvv_index=blf.at( false, i);
    cout << "Antenna pairs involving antenna index "<<i<<":"<<endl;
    for(int na=0; na<vvv_index.size(); na++){
      for(int n=0; n<vvv_index[na].size(); n++)
	cout << "basebandNum " << n+1 << ": " << vvv_index[na][n].size() 
	     << " values with index range ["
	     << vvv_index[na][n][0] <<","<< vvv_index[na][n][vvv_index[0][n].size()-1] 
	     <<"] antenna index pair "<< i <<" with "<<i<< endl;
    }
    cout << endl;
  }
  cout << endl;

  cout << "End of tests for the methods in the abstract class BaselineMetadata" << endl;  
  cout << endl;

  cout << endl;
  cout << "Test of the class baselinesFlags:" << endl;
  cout << "================================ " << endl; 

  if(blf.isFlagged()){
    cout<<"There are flag(s) which have been set in a BaselineFlagsMetadata container"<<endl;
  }else{
    cout<<"There is no flag set"<<endl;
  }
  blf.clearBlindlyFlags();
  cout << "Number of cleared flags:                                    " << blf.clearFlags() << endl;

  unsigned long int* flags_ = blf.getBaselineFlagsContainer();
  cout << "Number of antennas antennas scheduled for data acquisition: " << blf.numAnt() << endl;
  cout << "Actual number of antennas with stored data:                 " << blf.actualNumAnt() << endl;

  cout << "Applying blindly the clearing must trigger no WARNING!" << endl;
  blf.clearBlindlyFlags();
  cout << "End of the test for the method 'clearBlindlyFlags()'" << endl;

  int  badAntIndex = 2;  
  int  flagCode    = 1;
  bool interfero   = true;
  int numFlagged = blf.setFlags( badAntIndex, interfero, flagCode);
  cout << numFlagged << " flags are set to flag all the baselines involving the antenna index "<<badAntIndex<<endl;
  cout << "Actual number of antennas with stored data:                 " << blf.actualNumAnt() << endl;

  cout << endl;
  badAntIndex = 4;  
  numFlagged = blf.setFlags( badAntIndex, interfero, flagCode);
  cout << numFlagged << " flags are added to flag all the baselines involving the antenna index "<<badAntIndex<<endl;
  cout << "Actual number of antennas with stored data:                 " << blf.actualNumAnt() << endl;

  cout << "Now clear, not blindly, the flags" << endl; 
  cout << "Number of cleared flags:                                    " << blf.clearFlags() << endl;
  cout << "Actual number of antennas with stored data:                 " << blf.actualNumAnt() << endl;


  cout << endl;
  badAntIndex = 3;  
  numFlagged = blf.setFlags( badAntIndex, interfero, flagCode);
  cout << numFlagged << " flags are added to flag all the baselines involving the antenna index "<<badAntIndex<<endl;
  cout << "Actual number of antennas with stored data:                 " << blf.actualNumAnt() << endl;

  cout << "End of tests for methods implemented in the class BaselineFlagsMetadata " << endl;  

  cout << endl;
  cout << "Test of the class actualTimeDurations:" << endl;
  cout << "===================================== " << endl; 

  nant = blf.actualNumAnt();
  nbl = (nant*(nant-1))/2;  
  //ActualTimeDurations atd(v_npp, v_nbin, nbb, nant, nbl);
  ActualTimeDurations atd(&blf);
  atd.display();
  cout<<endl;
  cout << "ActualTimesDurations containers must host   " << atd.numValue()     << " values " << endl;
  cout << "Number of scheduled antennas:               " << atd.numAnt(blf)    << endl;
  cout << "Actual number of antennas with stored data: " << atd.actualNumAnt() << endl; 

  long long* actualTimes     = atd.getActualTimesContainer();
  long long* actualDurations = atd.getActualDurationsContainer();

  long long int time=9876543;      cout<<"Set time value to:     "<<time    <<endl; 
  long long int duration=1234567;  cout<<"Set duration value to: "<<duration<<endl;
  cout << "Put " << atd.setTimeDurations(time,duration)
       << " time and duration values in the containers "<< endl;

  int ppmax;
  vector<vector<vector<vector<long long> > > > vvvv_actualTimes;
  vector<vector<vector<vector<long long> > > > vvvv_actualDurations;
  for(int na=0; na<nant; na++){
    vvvv_actualTimes = atd.getTimes(na, interfero);
    for(int bl=0; bl<vvvv_actualTimes.size(); bl++)
      for(int bb=0; bb<vvvv_actualTimes[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualTime=" << vvvv_actualTimes[bl][bb][bin][pp] << endl;    
  }
  cout<<endl;
  cout<<"And the single-dish part:"<<endl;

  interfero=false;
  for(int na=0; na<nant; na++){
    vvvv_actualTimes = atd.getTimes(na, interfero);
    for(int bl=0; bl<vvvv_actualTimes.size(); bl++)
      for(int bb=0; bb<vvvv_actualTimes[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numAutoPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualDuration=" << vvvv_actualTimes[bl][bb][bin][pp] << endl;    
  }


  cout << endl;  
  cout <<"Test the setter and getter when involving a single antenna:"<<endl;
  cout <<"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"<<endl;
  i=1;
  interfero=true;
  duration=1234000;  cout<<"Set duration value to: "<<duration
			 <<" for all baselines involving antenna index "<<i<<endl;
  int numSet=atd.setTimeDurations(i,interfero,time,duration); 
  cout<< numSet << " values have been set" << endl;

  for(int na=0; na<nant; na++){
    vvvv_actualDurations = atd.getDurations(na, interfero);
    for(int bl=0; bl<vvvv_actualDurations.size(); bl++)
      for(int bb=0; bb<vvvv_actualDurations[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualDuration=" << vvvv_actualDurations[bl][bb][bin][pp] << endl;    
  }

  cout << endl;  
  cout <<"Test of the setter and getter when involving a baseline:"<<endl;
  cout <<"- - - - - - - - - - - - - - - - - - - - - - - - - - - - "<<endl;
  j=3;
  duration=1000000;  cout<<"Set duration value to: "<<duration
			 <<" for the baseline involving antenna index "<<i<<" with "<<j<<endl;
  atd.setTimeDurations(i,j,time,duration);

  vector<vector<vector<long long> > > vvv_actualDurations = atd.getDurations(i,j);


  int Nbb =vvv_actualDurations.size();                cout << "Nbb="  << Nbb  << endl;
  int Nbin=vvv_actualDurations[Nbb-1].size();         cout << "Nbin=" << Nbin << endl;
  int Npp =vvv_actualDurations[Nbb-1][Nbin-1].size(); cout << "Npp="  << Npp  << endl;
  for(int bb=0; bb<vvv_actualDurations.size(); bb++)
    for(int bin=0; bin<atd.numBin(bb+1); bin++)
      for(int pp=0; pp<atd.numPolProduct(bb+1); pp++)
	cout<<"polProduct index: "<<pp
	    <<"  bin index: "<<bin
	    <<"  baseband index: "<<bb
	    <<"  baseline antenna index pair "<<i<<","<<j 
	    <<": actualDuration=" << vvv_actualDurations[bb][bin][pp] << endl;    
  cout << endl;

  for(int na=0; na<nant; na++){
    vvvv_actualDurations = atd.getDurations(na, interfero);
    for(int bl=0; bl<vvvv_actualTimes.size(); bl++)
      for(int bb=0; bb<vvvv_actualTimes[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualDuration=" << vvvv_actualDurations[bl][bb][bin][pp] << endl;    
  }
  cout<<endl;
  cout<<"And the single-dish part:"<<endl;

  interfero=false;
  for(int na=0; na<nant; na++){
    vvvv_actualDurations = atd.getDurations(na, interfero);
    for(int bl=0; bl<vvvv_actualDurations.size(); bl++)
      for(int bb=0; bb<vvvv_actualDurations[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numAutoPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualDuration=" << vvvv_actualDurations[bl][bb][bin][pp] << endl;    
  }



  cout << endl;
  cout<<"Display the state of the actualTimeDurations object:"<<endl;
  atd.display();
  //cout<<"Number of antenna initialy scheduled: "<< atd.numAnt(blf) <<endl; 

  cout << endl;
  cout <<"Test the setter and getter when involving a 'single-dish' antenna:"<<endl;
  i=1;
  duration=1234000+i;  cout<<"Set duration value to: "<<duration
			   <<" for the 'single dish' antenna index "<<i<<endl;
  interfero = false;
  numSet=atd.setTimeDurations(i,interfero,time,duration); 
  cout<< numSet << " values have been set" << endl;

  for(int na=0; na<nant; na++){
    vvvv_actualDurations = atd.getDurations(na, interfero);
    for(int bl=0; bl<vvvv_actualDurations.size(); bl++)
      for(int bb=0; bb<vvvv_actualDurations[bl].size(); bb++)
	for(int bin=0; bin<atd.numBin(bb+1); bin++)
	  for(int pp=0; pp<atd.numAutoPolProduct(bb+1); pp++)
	    cout<<"polProduct index: "<<pp
		<<"  bin index: "<<bin
		<<"  basebandNum: "<<bb+1
		<<"  baseline index for antIndex "<<na
		<<": "<<bl
		<<" actualDuration=" << vvvv_actualDurations[bl][bb][bin][pp] << endl;    
  }



  cout << "Delete the baselineFlags container"<<endl;
  blf.deleteBaselineFlagsContainer();

  cout << "Successful completion of the tests!" << endl;
}
