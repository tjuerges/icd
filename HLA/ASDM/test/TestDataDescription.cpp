/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestDataDescription.cpp
 */
 using namespace std;

#include <ASDMAll.h>
using namespace asdm;

#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/util/XMLUni.hpp>		

using namespace xercesc;

static const char*  gMemBufId = "dummyInfo";

void xmlIze(ASDM& dataset) {
	
	XMLPlatformUtils::Initialize();
	
	XercesDOMParser *parser = new XercesDOMParser;
	DOMImplementation* implementation = DOMImplementation::getImplementation();
	
	DOMWriter         *theSerializer = ((DOMImplementationLS*)implementation)->createDOMWriter();
	theSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);
	XMLFormatTarget *myFormTarget = new StdOutFormatTarget();
	DOMNode                     *document = 0;
	
	// The container.
	string s_dataset = dataset.toXML();
	MemBufInputSource * memBufIS = new MemBufInputSource (
        (const XMLByte*)s_dataset.c_str(),
        strlen(s_dataset.c_str()),
        gMemBufId,
        false
    );
    parser->parse(*memBufIS);	
	document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);

	// The DataDescriptionTable
	string s_ddT = dataset.getDataDescription().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_ddT.c_str(),
        strlen(s_ddT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The SpectralWindowTable
	string s_spwT = dataset.getSpectralWindow().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_spwT.c_str(),
        strlen(s_spwT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);	
	
	// The HolographyTable
	string s_holoT = dataset.getHolography().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_holoT.c_str(),
        strlen(s_holoT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	// The PolarizationTable
	string s_polT = dataset.getPolarization().toXML();
	memBufIS->resetMemBufInputSource (
		(const XMLByte*)s_polT.c_str(),
        strlen(s_polT.c_str()));
    parser->parse(*memBufIS);
    document = parser->getDocument();
	theSerializer->writeNode(myFormTarget, *document);
	
	delete theSerializer;
    delete myFormTarget;
    delete parser;
    delete memBufIS;
	XMLPlatformUtils::Terminate();		
}

void buildASDM(ASDM& dataset) {
	
		// Populate its Holography table with one row
		HolographyTable& holoT = dataset.getHolography();
		HolographyRow* holoR = 0;

		vector<string> typeHolo;
		typeHolo.push_back ("SS");
		typeHolo.push_back("RR");
		typeHolo.push_back("QQ");
		typeHolo.push_back("SR");
		typeHolo.push_back("SQ");
		typeHolo.push_back("QR");
		Length distance(1000.0);
		Length focus(10.0);
		bool flagRow = false;
		
		holoR = holoT.newRow(typeHolo.size(), typeHolo, distance, focus, flagRow);
		holoT.add(holoR);
		
		cout << "The dataset has " << holoT.size() << " rows in its Holography table" << endl;
		
		// Populate its Polarization table with one row
		PolarizationTable& polT = dataset.getPolarization();
		PolarizationRow* polR = 0;

		vector<int> typePol;
		typePol.push_back(9);
		typePol.push_back(10);
		typePol.push_back(11);
		typePol.push_back(12);
		
		vector<vector<int> > productPol;
		vector<int> aux;
		aux.push_back(0); aux.push_back(0);
		productPol.push_back(aux);
		aux.clear();
		
		aux.push_back(0); aux.push_back(1);
		productPol.push_back(aux);
		aux.clear();
		
		aux.push_back(1); aux.push_back(0);
		productPol.push_back(aux);
		aux.clear();	
		
		aux.push_back(1); aux.push_back(1);
		productPol.push_back(aux);
		aux.clear();	
		
		polR = polT.newRow(typePol.size(), typePol, productPol);
		polT.add(polR);
		cout << "The dataset has " << polT.size() << " rows in its Polarization table" << endl;	
		
		// Populate its SpectralWindow table with one row
		SpectralWindowTable& spwT = dataset.getSpectralWindow();
		SpectralWindowRow* spwR = 0;

		int numChan = 16;
		string name = "Dummy SpectralWindow";
		Frequency refFreq(1.0e9);
		vector<Frequency> chanFreq; 
		vector<Frequency> chanWidth;
		for (int i = 0; i < numChan; i++) {
			chanFreq.push_back(Frequency(1.0e9 + i*1.0e8));
			chanWidth.push_back(Frequency(1.0e8));
		}
	
		vector<Frequency> effectiveBw;
		vector<Frequency> resolution;
		for (int i = 0; i < numChan; i++) {
			effectiveBw.push_back(Frequency(1.0e9 + i*2.0e8));
			resolution.push_back(Frequency(2.0e8));
		}

		Frequency totBandwidth(numChan * 1.0e8);
		int netSideband = 1;
		flagRow = false;

		spwR = spwT.newRow(numChan,refFreq, chanFreq, chanWidth, effectiveBw, resolution, totBandwidth, netSideband,  flagRow);
		spwT.add(spwR);
		cout << "The dataset has " << spwT.size() << " rows in its SpectralWindow table" << endl;	
		
		// Populate its DataDescription table
		DataDescriptionTable& ddT = dataset.getDataDescription();
		DataDescriptionRow* ddR = 0;
		
		// Here is an association to an  HolographyRow and a SpectralWindowRow
		cout << "Adding a row with a link to the Holography table to the DataDescription table." << endl;
		ddR = ddT.newRow(holoR->getHolographyId(), spwR->getSpectralWindowId());
		ddT.add(ddR);
		cout << "Here is its polarizationId :" << ddR->getPolOrHoloId().toString() << endl;				
		
		// Here is an association  to a PolarizationRow and the same SpectralWindowRow
		cout << "Adding a row with a link to the Polarization table to the DataDescription table." << endl;
		ddR = ddT.newRow(polR->getPolarizationId(), spwR->getSpectralWindowId());
		ddT.add(ddR);
		cout << "Here is its polarizationId :" << ddR->getPolOrHoloId().toString() << endl;
		
		cout << "The dataset has " << ddT.size() << " rows in its DataDescription table" << endl;
		vector<DataDescriptionRow *> rows = ddT.get();
		for (unsigned int i = 0; i < ddT.size(); i++) {
			cout << "The DataDescription row #" << i << " is associated to a "<< rows[i]->getPolOrHoloId().getTagType()->toString() << endl;
		}
		
		return;
}

int main(int argc, char *argv[]) {
	
	// Build a dummy dataset.
	cout << "About to build a dummy dataset" << endl;
	ASDM dataset;
	buildASDM(dataset);
	cout << "Built a dummy dataset" << endl;
	
	// Let's take a look at the XML representation of this dataset.
	cout << "Here is the XML representation of the dataset: " << endl;
	xmlIze(dataset);
}

