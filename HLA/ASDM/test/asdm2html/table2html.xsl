<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	<xsl:template match="/*">
		<html>
			<header>
				<title>
					<xsl:value-of select="name()"/>
				</title>
			</header>
			<body>
				<h1>
					<xsl:value-of select="name()"/>
				</h1>
				<table border="1">
				<tr>
			<xsl:for-each select="row[position()=1]/*">	
				<th><xsl:value-of select="name()"/>
				</th>
			</xsl:for-each>
				</tr>
			<xsl:for-each select="row">	
			<tr>
			<xsl:for-each select="*">
			<td>
			<xsl:value-of select="."/>
			</td>
			</xsl:for-each>
			</tr>
				
			</xsl:for-each>				
				</table>			
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
