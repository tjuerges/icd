<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="asdm_name"></xsl:param>
	<xsl:output method="html"/>
	
	<xsl:template match="/">
	<html>
	<head>
	// Which ASDM do we want to see ?
	//
	var qsParm = new Array();
	var query = window.location.search.substring(1);
	var parms = query.split('&amp;');
	var pos = parms[0].indexOf('=');
	directory = parms[0].substring(pos+1);

	</head>
		<body>
		<h1>
		Dataset <xsl:value-of select="$asdm_name"/>
		</h1>
		<h4>
		created on <xsl:value-of select="ASDM/TimeOfCreation"/>
		</h4>
		<ul>
		<xsl:for-each select="ASDM/Table">
		<xsl:if test="normalize-space(NumberRows)!='0'">
		<LI>
	
		<A>
   		 <xsl:attribute name="href">
   		 <xsl:value-of select="concat('../', concat($asdm_name, concat('/', normalize-space(Name),'.xml')))" />
      			<!--<xsl:value-of select="concat(normalize-space(Name),'.xml')" />-->
   		 </xsl:attribute>
   		 <xsl:attribute name="target">
      			<xsl:value-of select="'Table'" />
   		 </xsl:attribute>
   
		<xsl:value-of select="Name"/> (<xsl:value-of select="NumberRows"/> rows. )
		</A>
		</LI>
		</xsl:if>
		<xsl:if test="normalize-space(NumberRows)='0'">
		<LI>			
		<xsl:value-of select="Name"/> (<xsl:value-of select="NumberRows"/> rows. )
		</LI>
		</xsl:if>		
		</xsl:for-each>
		</ul>
		
		
		</body>
	</html>
	</xsl:template>

</xsl:stylesheet>

