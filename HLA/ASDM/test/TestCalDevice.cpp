/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestCalDevice.cpp
 */

#include <iostream>
#include <string>

using namespace std;

#include <ASDMAll.h>

#include <CCalibrationDevice.h>

using namespace asdm;

int main(char *arg) {
  ASDM dataset;
  
  CalDeviceTable& cdt = dataset.getCalDevice();
  CalDeviceRow* cdr;

  Tag antennaId(1, TagType::Antenna);
  int feedId = 0;
  Tag spectralWindowId0(1, TagType::SpectralWindow);
  Tag spectralWindowId1(2, TagType::SpectralWindow);
  ArrayTimeInterval ati0 = ArrayTimeInterval(ArrayTime((long long)1000));
  ArrayTimeInterval ati1 = ArrayTimeInterval(ArrayTime((long long)2000));

  int numCalLoad0 = 2;
  vector<CalibrationDeviceMod::CalibrationDevice> calLoadName0(numCalLoad0);
  calLoadName0.push_back(CalibrationDeviceMod::AMBIENT_LOAD);
  calLoadName0.push_back(CalibrationDeviceMod::COLD_LOAD);

  int numCalLoad1 = 2;
  vector<CalibrationDeviceMod::CalibrationDevice> calLoadName1(numCalLoad1);
  calLoadName1.push_back(CalibrationDeviceMod::HOT_LOAD);
  calLoadName1.push_back(CalibrationDeviceMod::NOISE_TUBE_LOAD);

  int numCalLoad2 = 1;
  vector<CalibrationDeviceMod::CalibrationDevice> calLoadName2(numCalLoad2);
  calLoadName2.push_back(CalibrationDeviceMod::SOLAR_FILTER);

  // Add two rows.
  try {
    cdr = cdt.newRow(antennaId, feedId, spectralWindowId0, ati0, numCalLoad0, calLoadName0);
    if (cdt.add(cdr) == cdr) 
      cout << "A new row has been actually added to the table" << endl;
  }
  catch (DuplicateKey e) {
    cout << "Error ! " << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unhandled error" << endl;
  }
  
  // Same context than above
  try {
    cdr = cdt.newRow(antennaId, feedId, spectralWindowId0, ati1, numCalLoad1, calLoadName1);
    if (cdt.add(cdr) == cdr) 
      cout << "A new row has been actually added to the table" << endl;
  }
  catch (DuplicateKey e) {
    cout << "Error ! " << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unhandled error" << endl;
  }

  // Play with getRowByKey
  cout << "Querying row with timeInterval starting at " << ati1.getStart().toFITS() << endl; 
  CalDeviceRow* retRow = cdt.getRowByKey(antennaId, feedId, spectralWindowId0, ati1);
  if (retRow)
    cout << "Retrieved a row with timeInterval starting at " << retRow->getTimeInterval().getStart().toFITS() << endl;
  else
    cout << "No retrieved row" << endl;

  // Different context than the two previous rows
  try {
  cdr = cdt.newRow(antennaId, feedId, spectralWindowId1, ati1, numCalLoad0, calLoadName0);
  if (cdt.add(cdr) == cdr) 
    cout << "A new row has been actually added to the table" << endl;
  }
  catch (DuplicateKey e) {
    cout << "Error ! " << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unhandled error" << endl;
  }
 
  // Try to add which already exists.
  try {
  cdr = cdt.newRow(antennaId, feedId, spectralWindowId1, ati1, numCalLoad0, calLoadName0);
  if (cdt.add(cdr) == cdr) 
    cout << "A new row has been actually added to the table" << endl;
  else 
    cout << "This row was already stored in the table " << endl;
  }
  catch (DuplicateKey e) {
    cout << "Error ! " << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unhandled error" << endl;
  }

  // Generate a Duplicate Key exception with a new value for numCalLoad.
  try {
  cdr = cdt.newRow(antennaId, feedId, spectralWindowId1, ati1, numCalLoad2, calLoadName2);
  if (cdt.add(cdr) == cdr) 
    cout << "A new row has been actually added to the table" << endl;
  else 
    cout << "This row was already stored in the table " << endl;
  }
  catch (DuplicateKey e) {
    cout << "Error ! " << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unhandled error" << endl;
  }

  // Get a row by Key
  // ... an existing row firstly...
  CalDeviceRow* aRow = 0;
  if ( (aRow = cdt.getRowByKey(antennaId, feedId, spectralWindowId1, ati1)) != 0 ) {
    cout << "Found a row with key (antennaId="<< antennaId.toString()
	 << ", feedId=" << feedId
	 << ",spectralWindowId=" << spectralWindowId1.toString()
	 << ", timeInterval=" << ati1 << ")" << endl;
    cout << "Its value section is numCalload = " << aRow->getNumCalload() << endl;
  }
  else 
    cout << "No row with key (antennaId="<< antennaId.toString()
	 << ", feedId=" << feedId
	 << ",spectralWindowId="
	 << spectralWindowId1.toString() << ",timeInterval=" << ati1 << ")" << endl;

  // ... and then a non existing row.
  if ( (aRow=cdt.getRowByKey(antennaId, feedId+4, spectralWindowId1, ati1)) != 0 ) {
    cout << "Found a row with key (antennaId="<< antennaId.toString()
	 << ", feedId=" << feedId
	 << ",spectralWindowId=" << spectralWindowId1.toString()
	 << ", timInterval=" << ati1 << ")" << endl;
    cout << "Its value section is numCalload = " << aRow->getNumCalload() << endl;
  }
  else 
    cout << "No row with key (antennaId="<< antennaId.toString()
	 << ", feedId=" << feedId+4
	 << ",spectralWindowId="
	 << spectralWindowId1.toString() << ",timeInterval=" << ati1 << ")" << endl;

  // Display the number of rows stored in the table
  cout << "The CalDevice table has "<< cdt.size() << " row(s)" << endl;

 
   // Write out the dataset in XML documents.
  try {
          char testdir[40];
          srand((unsigned) time(0));
          sprintf(testdir,"/tmp/asdm_%d", rand());
          dataset.toXML(string(testdir));
          cout << "The dataset has been saved in the directory " << testdir << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  

  return 0;
}
