#include <time.h>
#include <iostream>
#include <ASDMAll.h>

using namespace std;
using namespace asdm;

int main (int agrc, char *argv[]) {

  ASDM ds;

  PointingB64Table& pB64T = ds.getPointingB64();
 
  unsigned int numDataPoints = 2000;
  cout << "numDatapoints " << numDataPoints << endl;
  unsigned int numPoly = 0;
  int pointingModelId = 1;
  string name = "myPointingB64_1";
  ArrayTime timeOrigin = ArrayTime(1000000LL);

  vector<vector<Angle> > pointingDirection;
  vector<vector<Angle> > target;
  vector<vector<Angle> > offset;
  vector<vector<Angle> > sourceOffset;
  vector<Angle> tmp1;
  vector<Angle> tmp2;
  vector<Angle> tmp3;
  vector<Angle> tmp4;
  for (unsigned int i = 0; i < numPoly+1; i++) {
    tmp1.clear();
    tmp2.clear();
    tmp3.clear();
    tmp4.clear();
    for (unsigned int j = 0; j < 2; j++) {
      tmp1.push_back(Angle(1000.0*j + i));
      tmp2.push_back(Angle(2000.0*j + i));
      tmp3.push_back(Angle(3000.0*j + i));
      tmp4.push_back(Angle(4000.0*j + i));
    }
    pointingDirection.push_back(tmp1);
    target.push_back(tmp2);
    offset.push_back(tmp3);
    sourceOffset.push_back(tmp4);
  }

  vector<vector <Angle> > encoder;
  vector<Angle> temp;
  for (unsigned int i = 0; i < numDataPoints; i++) {
    temp.clear();
    for (unsigned int j = 0; j < 2; j++) {
      temp.push_back(Angle(1.0 * (numDataPoints * i + j)));
    }
    encoder.push_back(temp);
  }
  
  vector<bool> pointingTracking(numDataPoints, true);
  vector<bool> phaseTracking(numDataPoints, true);
  vector<bool>flagDataPoint(numDataPoints, true);
  
  	int numRows = 100;
  	cout << "Number of PointingB64 rows " << numRows << endl;
  	time_t beginCreate = time(0);
  	// Let's fill the table with 100 rows.
	for (int i = 0; i < numRows; i++) {
		PointingB64Row* pB64R = pB64T.newRow(Tag(i),
		ArrayTimeInterval((i + 1) * 1000000LL), pointingModelId,
		name, numDataPoints, numPoly, timeOrigin,
		pointingDirection, target, offset, sourceOffset, encoder);

		pB64R->setPointingTracking(pointingTracking);
		pB64R->setPhaseTracking(phaseTracking);
		pB64R->setFlagDataPoint(flagDataPoint);
		pB64T.add(pB64R);
	}
	float durationCreate = difftime(time(0), beginCreate);
	cout << "Time to create the dataset " << durationCreate << "s." << endl;
	// Export the dataset on disk
	
	time_t beginExport = time(0);
	ds.toXML("/tmp/B64");
	float durationExport = difftime(time(0), beginExport);
	cout << "Time to convert the dataset in XML and write to disk " << durationExport << "s." << endl;
	
	// Import the dataset from disk
	time_t beginImport = time(0);
	ASDM* ds1 = ASDM::getFromXML("/tmp/B64");
	float durationImport = difftime(time(0), beginImport);
	cout << "Time to read and parse the dataset " << durationImport << "s." << endl;
}
