#include <iostream>
#include <string>
#include <ASDMAll.h>
#include "SDMBinaryExceptions.h"
#include "SDMBinaryBlock.h"
using namespace asdmBinary;
#include "Error.h"
#include "SDMBinaryData.h"

using namespace asdm;

int main (int argc, char* argv[]) {

  if (argc<3) {
    cerr << "Usage : testReadBin ASDMBinaryRootDir dataOID [EB_Directory]" << endl;
    return -1;
  }

  string asdmDir;
  ASDM   dataset;
  ASDM*  datasetPtr    = &dataset;
  string binaryRootDir = string(argv[1]);
  string dataOID       = string(argv[2]);
  if(argc==4){
    asdmDir = string(argv[3]);
    try{
      dataset.setFromFile(asdmDir);
    }
    catch (ConversionException e) {
      cerr << "ERROR " << e.getMessage() << endl;
      return -1; 
    }
    catch (...) {
      cerr << "Unexpected exception " << endl;
      return -1;
    }    
  }else{
    try{
      dataset.setFromFile(binaryRootDir);
      asdmDir=binaryRootDir;
    }
    catch (ConversionException e) {
      cerr << "ERROR " << e.getMessage() << endl;
      return -1; 
    }
    catch (...) {
      cerr << "no sdm dataset in directory " << binaryRootDir
	   << ". ==> no sdm identification" << endl;
      asdmDir="";
    }    
  }
  if(asdmDir.length())cout<<"The data can be tagged with the SDM identifiers. "<<endl;

  
  SDMBinaryData* sdmBinaryDataPtr=0;
  try{
    sdmBinaryDataPtr = new SDMBinaryData(binaryRootDir,dataOID);
  }
  catch (ConversionException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }
  catch (FromFileException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }  
  catch (...) {
    cerr << "Unexpected exception " << endl;
    return -1;
  }

  cout << "Ready to get the data tree(s)" << endl;

  cout << "Get the data tree for all the correlations" << endl;
  vector<vector<vector<vector<vector<vector<float> > > > > > 
    data_Tree = sdmBinaryDataPtr->dataTree();
  cout << "Got the data tree" << endl;

  //Output to show the structure of the tree:
  unsigned int bl, dd, bin, apc;
  cout<<endl;
  cout<<"Sizes at the positions of the nodes:"<<endl;
  cout<<"===================================="<<endl;
  int k=0;
  for(bl=0; bl<data_Tree.size(); bl++){                                            // baseline level
    for(dd=0; dd<data_Tree[bl].size(); dd++){                                        // data description level (baseband & spw)
      for(bin=0; bin<data_Tree[bl][dd].size(); bin++){                                 // bin level (SwitchCycles)
	for(apc=0; apc<data_Tree[bl][dd][bin].size(); apc++){                            // atm. phase correction level
	  k += data_Tree[bl][dd][bin][apc].size()*data_Tree[bl][dd][bin][apc][0].size();
	}
      }
    }
  }
  cout<<"Number of floats in the data structure: "<<k<<endl;
 
  if(datasetPtr){                                            cout<<"Connect the SDM root identifiers to these data"<<endl;
    vector<vector<vector<vector<int> > > > 
      id_Tree = sdmBinaryDataPtr->idTree( datasetPtr, dataOID );


    int  spwId, numSp, polId, numPp, numBin, numAPC;
    char dumstr[128];

    cout<<endl;
    cout<<"Identifiers at the positions of the nodes:"<<endl;
    cout<<"=========================================="<<endl;
    int n; 
    k=0;
    for(bl=0; bl<data_Tree.size(); bl++){
      if(id_Tree[bl][0][0].size()==6){
	cout<<"baseline index "<<bl
	    <<": antennaId_1="<<id_Tree[bl][0][0][4]
	    <<" antennaId_2="<<id_Tree[bl][0][0][5]<<endl;
	n=2;
      }else{
	cout<<"bl index "<<bl
	    <<" antennaId  ="<<id_Tree[bl][0][0][4]<<endl;
	n=1;
      }
      for(dd=0; dd<data_Tree[bl].size(); dd++){
	
	numBin = data_Tree[bl][dd].size();                 // Number of steps in the switch cycle
	numAPC = data_Tree[bl][dd][0].size();              // Size of the Atmospheric Phase Correction axis
	
	polId = id_Tree[bl][dd][0][0];                     // Polarization identifier
	numPp = id_Tree[bl][dd][0][1];                     // Number of polarization products
	
	spwId = id_Tree[bl][dd][0][2];                     // Spectral window identifier
	numSp = id_Tree[bl][dd][0][3];                     // Number spectral points in the spectral window
	
	sprintf(dumstr,
		"  dd index %2d  spwId=%3d numSp=%5d  polId=%d  numPp=%d. Baseband with numBin=%d  numAPC=%d",
		dd,spwId,numSp,polId,numPp,numBin,numAPC);
	cout<< dumstr << endl;
	for(bin=0; bin<data_Tree[bl][dd].size(); bin++){
	  for(apc=0; apc<data_Tree[bl][dd][bin].size(); apc++)
	    k += data_Tree[bl][dd][bin][apc].size()*data_Tree[bl][dd][bin][apc][0].size();
	}
      }
    }
    cout<<"Number of floats in the data structure: "<<k<<endl;
  }  // end case dataset available

  delete sdmBinaryDataPtr;

  cout << "Successful test completion" << endl;

  return 0;
}
