#include <iostream>
#include <string>
#include <ASDMAll.h>

using namespace asdm;

int main (int argc, char* argv[]) {

  if (argc < 2) {
    cerr << "Usage : testRead1ASDM asdmDir" << endl;
    return -1;
  }
  string asdmDir = string(argv[1]);

  ASDM asdm ;
  try{
    asdm.setFromFile(asdmDir);
  }
  catch (ConversionException e) {
    cerr << "ERROR " << e.getMessage() << endl;
    return -1; 
  }
  catch (...) {
    cerr << "Unexpected exception " << endl;
    return -1;
  }

  cout << "Dataset read" << endl;
  return 0;
}
