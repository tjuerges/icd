/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestScan.cpp
 */
#include <iostream>
#include <string>
using namespace std;

#include <ASDMAll.h>
using namespace asdm;

#include <iostream>
#include <sstream>
ostringstream aux;


#include "almaEnumerations_IFC.h"
#include "CScanIntent.h"

using namespace ScanIntentMod;

int main (int argC, char * argV[]) {
  ASDM dataset;

  ScanTable &scanT = dataset.getScan();

  ScanRow * scanR = 0;

  Tag execBlockId(0, TagType::ExecBlock);
  int scanNumber = 1;

  ArrayTime startTime(1000000LL);
  ArrayTime endTime(10001000LL);
  
  int numSubscan = 10;
  vector<ScanIntent> scanIntent;
  scanIntent.push_back(FOCUS);
  scanIntent.push_back(HOLOGRAPHY);
  scanIntent.push_back(SKYDIP);
  scanIntent.push_back(TARGET);
    

  bool flagRow = false;

  scanR = scanT.newRow(execBlockId, scanNumber, startTime, endTime, numSubscan, (int) scanIntent.size(), scanIntent, flagRow);
  scanT.add(scanR);

  cout << "The Scan table of the orginal dataset has " << scanT.size() << " row(s)." << endl;
  cout << "The scanIntent of this row is " ;
  for (unsigned int i = 0; i < scanIntent.size(); i++)
    cout << " " << CScanIntent::name(scanIntent.at(i));
  cout << endl;
  
  // Convert the ASDM to IDL
  ASDMDataSetIDL* datasetIDL = dataset.toIDL();

  // Convert back from IDL
  ASDM dataset2;
  dataset2.fromIDL(datasetIDL);

  // Consider the Scan table of this dataset.
  ScanTable& scanT2 = dataset2.getScan();
  
  cout << "The Scan table of the from IDL dataset has " << scanT2.size() << " row(s)." << endl;  
  ScanRow* scanR2 = scanT2.get().at(0);
  cout << "The scanIntent of this row is " ;
  
  vector<ScanIntent> scanIntent2 = scanR2->getScanIntent();
  
  for (unsigned int i = 0; i < scanIntent2.size(); i++)
    cout << " " << CScanIntent::name(scanIntent2.at(i));
  cout << endl;
  
  // Convert the ASDM to XML in the filesystem.
  dataset.toFile("/tmp/TestScan.sdm");
  
  // Convert back from XML
  ASDM dataset3;
  try {
    dataset3.setFromFile("/tmp/TestScan.sdm");
  }
  catch (ConversionException e) {
    cout << e.getMessage() << endl;
    exit (-1);
  }

  // Consider the Scan table of this dataset.
  ScanTable& scanT3 = dataset3.getScan();
  
  cout << "The Scan table of the from XML dataset has " << scanT3.size() << " row(s)." << endl;  
  ScanRow* scanR3 = scanT3.get().at(0);
  cout << "The scanIntent of this row is " ;
  
  vector<ScanIntent> scanIntent3 = scanR3->getScanIntent();
  
  for (unsigned int i = 0; i < scanIntent3.size(); i++)
    cout << " " << CScanIntent::name(scanIntent3.at(i));
  cout << endl;


}

