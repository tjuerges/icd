/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestSource.cpp
 */

#include <iostream>
#include <string>
using namespace std;

#include <ASDMAll.h>
using namespace asdm;

int main(char *arg) {
  ASDM dataset;
  SourceTable &source = dataset.getSource();
  SourceRow* aRow = 0;

  //
  // Build a source table.
  //
  cout << "About to populate the source table ..." << endl;

  
  // A first row.

  // The key section.
  Tag spwId0(0);
  ArrayTimeInterval ati0(ArrayTime((long long int)1000));

  // The required value section.
  int numLines0 = 4;
  string sourceName0("Source 0");
  int calibrationGroup0 = 0;
  string code0("Code 0");
  vector <Angle> direction0(2);
  direction0[0] = Angle(1.);
  direction0[1] = Angle(1.);
  vector <AngularRate> properMotion0(2);
  properMotion0[0] = AngularRate((double) 0.1);
  properMotion0[1] = AngularRate((double) 0.1);
  

  aRow = source.newRow(spwId0,
		       ati0,
		       numLines0, 
		       sourceName0,
		       calibrationGroup0,
		       code0,
		       direction0,
		       properMotion0);

  cout << "aRow built" << endl;
  source.add(aRow);
  cout << "sourceId has been given the value " << aRow->getSourceId() << endl;


  // A second row 
  // Its context (spectralWindow) and start time are the same than for the previous row, its value section changes
  // therefore its sourceId should be obtained by autoincrementation.

  // The key section.
  Tag spwId1(0);
  ArrayTimeInterval ati1(ArrayTime((long long int)1000));

  // The value section.
  int numLines1 = 4;
  string sourceName1("Source 1");
  int calibrationGroup1 = 0;
  string code1("Code 1");
  vector <Angle> direction1(2);
  direction1[0] = Angle(2.);
  direction1[1] = Angle(2.);
  vector <AngularRate> properMotion1(2);
  properMotion1[0] = AngularRate((double) 0.2);
  properMotion1[1] = AngularRate((double) 0.2);  

  aRow = source.newRow(spwId1,
		       ati1,
		       numLines1, 
		       sourceName1,
		       calibrationGroup1,
		       code1,
		       direction1,
		       properMotion1);

  cout << "aRow built" << endl;
  source.add(aRow);
  cout << "sourceId has been given the value " << aRow->getSourceId() << endl;

  // A third row . It comes with a new starttime and the same context, then it should be given the id of the 1st row (0).
  // The key section.
  Tag spwId2(0);
  ArrayTimeInterval ati2(ArrayTime((long long int)2000));

  // The value section.
  int numLines2 = 4;
  string sourceName2("Source 0");
  int calibrationGroup2 = 0;
  string code2("Code 0");
  vector <Angle> direction2(2);
  direction2[0] = Angle(3.);
  direction2[1] = Angle(3.);
  vector <AngularRate> properMotion2(2);
  properMotion2[0] = AngularRate((double) 0.3);
  properMotion2[1] = AngularRate((double) 0.3);  

  aRow = source.newRow(spwId2,
		       ati2,
		       numLines2, 
		       sourceName2,
		       calibrationGroup2,
		       code2,
		       direction2,
		       properMotion2);

  cout << "aRow built" << endl;
  source.add(aRow);
  cout << "sourceId has been given the value " << aRow->getSourceId() << endl;



  // A fourth row . It comes with a new starttime and a new context, then it should be given the id 0,
  // The key section.
  Tag spwId3(1);
  ArrayTimeInterval ati3(ArrayTime((long long int)1000));

  // The value section.
  int numLines3 = 5;
  string sourceName3("Source 0");
  int calibrationGroup3 = 0;
  string code3("Code 5");
  vector <Angle> direction3(2);
  direction3[0] = Angle(5.);
  direction3[1] = Angle(5.);
  vector <AngularRate> properMotion3(2);
  properMotion3[0] = AngularRate((double) 0.3);
  properMotion3[1] = AngularRate((double) 0.3);  

  aRow = source.newRow(spwId3,
		       ati3,
		       numLines3, 
		       sourceName3,
		       calibrationGroup3,
		       code3,
		       direction3,
		       properMotion3);

  cout << "aRow built" << endl;
  source.add(aRow);
  cout << "sourceId has been given the value " << aRow->getSourceId() << endl;


  // A fifth row . It comes with a starttime already existing and the same context than above, 
  // The key section.
  Tag spwId4(1);
  ArrayTimeInterval ati4(ArrayTime((long long int)1500));

  // The value section.
  int numLines4 = 5;
  string sourceName4("Source 0");
  int calibrationGroup4 = 0;
  string code4("Code 5");
  vector <Angle> direction4(2);
  direction4[0] = Angle(5.);
  direction4[1] = Angle(5.);
  vector <AngularRate> properMotion4(2);
  properMotion4[0] = AngularRate((double) 0.3);
  properMotion4[1] = AngularRate((double) 0.3);  

  aRow = source.newRow(spwId4,
		       ati4,
		       numLines4, 
		       sourceName4,
		       calibrationGroup4,
		       code4,
		       direction4,
		       properMotion4);
		       
  cout << "aRow built" << endl;
  source.add(aRow);
  cout << "sourceId has been given the value " << aRow->getSourceId() << endl;	       
		       
  // A sith row . It's identical to the fifth row, this should be 
  // detected and return a pointer to the fifth row.
  Tag spwId5(1);
  ArrayTimeInterval ati5(ArrayTime((long long int)1000));
  
 
  // The value section.
  int numLines5 = 5;
  string sourceName5("Source 0");
  int calibrationGroup5 = 0;
  string code5("Code 5");
  vector <Angle> direction5(2);
  direction5[0] = Angle(5.);
  direction5[1] = Angle(5.);
  vector <AngularRate> properMotion5(2);
  properMotion5[0] = AngularRate((double) 0.3);
  properMotion5[1] = AngularRate((double) 0.3);  

	
  aRow = source.newRow(spwId5,
  				ati5,
		       numLines5, 
		       sourceName5,
		       calibrationGroup5,
		       code5,
		       direction5,
		       properMotion5);

  SourceRow* retRow = source.add(aRow);
  		       	       
  if (retRow != aRow) {
  	cout << "Attempt to store a row identical to a row present in the SourceTable" << endl;
  	cout << "It's the row with adress " << (unsigned long) retRow << " and id " << retRow->getSourceId() << endl;
  }
 
 
 // Let's check what is actually stored in the Source table.
  vector<SourceRow *> myRows = source.get();
  cout << endl;
  cout << "There are " << myRows.size() << " rows in the Source table" << endl;
  cout << "Their addresses and ids are " << endl;
  for (int i = 0; i < myRows.size(); i++)
  	cout << (unsigned long) myRows[i] << "," << myRows[i]->getSourceId() << endl;
 
 // Convert the Source table in its XML representation
 cout << endl;
 cout << "Here is the XML representation of the Source table:" << endl;
 string xmlSource = source.toXML();
 cout << xmlSource << endl;
 
 
  	
 // Test getRowByKey
 SourceRow* qRow = myRows[0];
 int qsourceId = qRow->getSourceId();
 Tag qspwTag  = qRow->getSpectralWindowId();
 ArrayTimeInterval qati = qRow->getTimeInterval(); 
 cout << endl;
 cout << "Now testing getRowBykey:" << endl;
 cout << "Querying for the Source row with sourceId = " << qsourceId 
 	  <<", spectralWindowId = " << qspwTag
 	  <<", timeInterval = " << qati 
 	  <<"..."
 	  << endl;
 	  
 if ((aRow = source.getRowByKey(qsourceId, qspwTag, qati)) == 0) {
 	cout << "...could not find such a row. This should not happen !" << endl;
 }
 else {
 	cout << "...query successful" << endl;
 	cout << "Found the row with sourceId = " << aRow->getSourceId() 
 	     <<", spectralWindowId = " << aRow->getSpectralWindowId()
 	     <<", timeInterval = " << (qati = aRow->getTimeInterval()) 
 	     << endl;
 }	  
 
 qati.setStart(qati.getStart().get()+qati.getDuration().get()/2);
 cout << "Querying again but with start time = " << qati.getStart() << "..." << endl;
 
if ((aRow = source.getRowByKey(qsourceId, qspwTag, qati)) == 0) {
 	cout << "...could not find such a row. This should not happen !" << endl;
 }
 else {
 	cout << "...query successful" << endl;	
 	cout << "Found the row with sourceId = " << aRow->getSourceId() 
 	     <<", spectralWindowId = " << aRow->getSpectralWindowId()
 	     <<", timeInterval = " << (qati = aRow->getTimeInterval()) 
 	     << endl;
 }	 
 
 // Write out the dataset in XML documents.
  cout << endl;
  cout << "Now converting the dataset into XML and saving it to disk" << endl;
  try {
          char testdir[40];
          srand((unsigned) time(0));
          sprintf(testdir,"/tmp/asdm_%d", rand());
          dataset.toXML(string(testdir));
          cout << "The dataset has been saved in the directory " << testdir << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  	 
 
  // Let's convert the table into CORBA structures
  cout << "Converting the Source table into CORBA structures" << endl;
  SourceTableIDL* sourceIDL = source.toIDL();
  
  // The other way around
  cout << "Converting the CORBA structures into a Source table" << endl;
  ASDM dataSetBis;
  SourceTable& sourceBis = dataSetBis.getSource();
  sourceBis.fromIDL(*sourceIDL);
  	 
  cout << "The Source table resulting from to conversion from the CORBA structures has "
       << sourceBis.size() << " rows" << endl;
       
  myRows = sourceBis.get();
  cout << "Their addresses and ids are " << endl;
  for (int i = 0; i < myRows.size(); i++)
  	cout << (unsigned long) myRows[i] << "," << myRows[i]->getSourceId() << endl;

	cout << endl;
	cout << "Here is its XML representation :" << endl;
	string xmlSourceBis = sourceBis.toXML();
	cout << xmlSourceBis << endl;
	
	cout << endl;
	cout << "Comparing the two XML representations (before and after binary->CORBA structs->binary conversions)" << endl;
	if (xmlSource == xmlSourceBis) 
		cout << "They are similar as expected" << endl;
	else
		cout << "Ooops, they are different. Something went wrong !" << endl;
		   
}

