#include <iostream>
#include <sstream>
#include <vector>

#include "EndianStream.h"

#include <string>

using namespace std;

#include "Tag.h"
using namespace asdm;

void tagUseCases(Tag t) {
	cout << 	"----------------------------------------" << endl;
	
	// Output it
	cout << "Tag string representation : " + t.toString() << endl;
	
	// Is it a "null" Tag or not ?
	if (t.isNull()) {
			cout << "This tag is null" << endl;
	}
	else {
			cout << "This tag is not null" << endl;
			
			// Output its (integer) value.
			cout << "Its value is " << t.getTagValue() << endl;

			// Output its type.
			cout << "Its type is " << t.getTagType()->toString() << endl;
	}
	
	// Convert it into IDLTag
	IDLTag tIDL = t.toIDLTag();
	
	// Convert back to a Tag and output the string representation of the conversion
	cout << "Tag obtained by conversion to and from IDL " ;
	Tag tFromIDL(tIDL);
	cout << tFromIDL.toString() << endl;
	
	// Convert it into a sequence of bytes.	
	EndianOSStream eoss;
	t.toBin(eoss);
	
	// Output the sequence of bytes.
	cout << "Sequence of bytes obtained by serialization of the Tag :"  << endl;
	string s = eoss.str();
	for (unsigned int i  = 0; i < s.length(); i++)
		cout << (int) s.at(i) << " ";
	cout << endl;
	
	// Converts the sequence of bytes back into a Tag and output the result of the conversion.
	istringstream iss(stringstream::in | stringstream::binary);
	EndianISStream eiss(eoss.str());
	cout << "Tag obtained by deserialization " << Tag::fromBin(eiss).toString() << endl;
}

void tagUseCases(vector<Tag>& t) {
	cout << 	"----------------------------------------" << endl;
	cout << "Content of a vector of Tag of size " << t.size() << endl;
	// Output its elements;
	for (unsigned int i = 0; i < t.size(); i++)
		cout << t.at(i).toString() << " ";
	cout << endl;
	
	cout << 	"----------------------------------------" << endl;
	// Output its elements values and types.
	cout << "Values and types : " << endl;
	for (unsigned int i = 0; i < t.size(); i++)
		cout << "value = " << t.at(i).getTagValue() << ", type = " << ((t.at(i).getTagType() != 0) ? t.at(i).getTagType()->toString() : "null TagType") << endl;
	
	cout << 	"----------------------------------------" << endl;
	// Convert it into a sequence of bytes.
	EndianOSStream eoss;
	Tag::toBin(t, eoss);
	
	cout << "Sequence of bytes obtained by serialization of the vector of Tag :"  << endl;
	string s = eoss.str();
	for (unsigned int i  = 0; i < s.length(); i++)
		cout << (int) s.at(i) << " ";
	cout << endl;
	
	// Converts the sequence of bytes back into a vector of Tag and output the result of the conversion.
	EndianISStream eiss(eoss.str());
	vector<Tag> deserialTag = Tag::from1DBin(eiss);
	cout << 	"----------------------------------------" << endl;
	cout << "Content of a vector of Tag of size " << t.size() << " obtained by deserialization." << endl;
	for (unsigned int i = 0; i < deserialTag.size(); i++)
		cout << deserialTag.at(i).toString() << " ";
	cout << endl;	
}

int main (int argc, char* argv[]) {
	// Test an Holography Tag.
	tagUseCases(Tag(2, TagType::Holography));
	
	// Test a NoType Tag.
	tagUseCases(Tag(27));
	
	// Test a null Tag.
	tagUseCases(Tag());
	
	// Test a vector of three Tags
	vector<Tag> tagV ;
	tagV.push_back(Tag(2, TagType::Holography));
	tagV.push_back(Tag(27));
	tagV.push_back(Tag());
	tagUseCases(tagV);	
	
	// Test an empty vector of Tags.
	vector <Tag> tagV0;
	tagUseCases(tagV0);

}
