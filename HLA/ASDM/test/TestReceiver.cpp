/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestReceiver.cpp
 */

#include <iostream>
#include <string>
using namespace std;

#include <ASDMAll.h>
using namespace asdm;

int main(char *arg) {
  ASDM dataset;
  ReceiverTable &receiver = dataset.getReceiver();
  ReceiverRow* aRow = 0;
  
  //
  // Build a receiver table.
  //
  cout << "About to populate the receiver table ..." << endl;

  // A first row.
  
  // The key section.
  Tag spwId1(0);
  ArrayTimeInterval ati1(ArrayTime((long long int)1000));
  
  // The value section.
  int numLo1 = 4;
  string name1("NoName");
  string receiverBand1("T.B.D");
  vector<Frequency> freqlo1;
  for (int i = 0; i<numLo1; i++) 
  	freqlo1.push_back(Frequency(1000.+i*100.));
  vector<int> sideBandLo1;
  for (int i = 0; i<numLo1; i++)
  	sideBandLo1.push_back(i*100);
  Temperature tDewar1(1234.);
  Interval stabilityDuration1(10000);
  double stability1 = 5678.;
  bool stabilityFlag1 = false;
  
  aRow = receiver.newRow(spwId1,
			 ati1,
			 numLo1,
			 name1,
			 receiverBand1,
			 freqlo1,
			 sideBandLo1,
			 tDewar1,
			 stabilityDuration1,
			 stability1,
			 stabilityFlag1);
  
  cout << "aRow built" << endl;
  receiver.add(aRow);
  cout << "receiverId has been given the value " << aRow->getReceiverId() << endl;
  	
  // A second row identical to the 1st one, this should be 
  // detected and return a pointer to the first row.
  
 // The key section.
  Tag spwId2(0);
  ArrayTimeInterval ati2(ArrayTime((long long int)1000));
  
  // The value section.
  int numLo2 = 4;
  string name2("NoName ");
  string receiverBand2("T.B.D");
  vector<Frequency> freqlo2;
  for (int i = 0; i<numLo2; i++) 
  	freqlo2.push_back(Frequency(1000.+i*100.));
  vector<int> sideBandLo2;
  for (int i = 0; i<numLo2; i++)
  	sideBandLo2.push_back(i*100);
  Temperature tDewar2(1234.);
  Interval stabilityDuration2(10000);
  double stability2 = 5678.;
  bool stabilityFlag2 = false;

  aRow = receiver.newRow(spwId2,
			 ati2,
			 numLo2,
			 name2,
			 receiverBand2,
			 freqlo2,
			 sideBandLo2,
			 tDewar2,
			 stabilityDuration2,
			 stability2,
			 stabilityFlag2);
  
  cout << "aRow built" << endl;
  
  ReceiverRow* retRow = receiver.add(aRow);
  		       	       
  if (retRow != aRow) {
  	cout << "Attempt to store a row identical to a row present in the ReceiverTable" << endl;
  	cout << "It's the row with adress " << (unsigned long) retRow << " and id " << retRow->getReceiverId() << endl;
  }
  
  // Let's check what is actually stored in the Receiver table.
  vector<ReceiverRow *> myRows = receiver.get();
  cout << endl;
  cout << "There are " << myRows.size() << " rows in the Receiver table" << endl;
  cout << "Their addresses and ids are " << endl;
  for (int i = 0; i < myRows.size(); i++)
  	cout << (unsigned long) myRows[i] << "," << myRows[i]->getReceiverId() << endl;
 
  // Convert the Receiver table in its XML representation
  cout << endl;
  cout << "Here is the XML representation of the Receiver table:" << endl;
  string xmlReceiver = receiver.toXML();
  cout << xmlReceiver << endl;
	
// Write out the dataset in XML documents.
  cout << endl;
  cout << "Now converting the dataset into XML and saving it to disk" << endl;
  char testdir[40];
  try {
          
          srand((unsigned) time(0));
          sprintf(testdir,"/tmp/asdm_%d", rand());
          dataset.toXML(string(testdir));
          cout << "The dataset has been saved in the directory " << testdir << endl;
  } 
  catch (ConversionException e) {
    cout << "Error! " << e.getMessage() << endl;
  }  	 
  
  cout << endl;
  cout << "Creates a new dataset from the XML just stored in " << testdir << endl;
  ASDM* newASDM = 0;
  try {
  	newASDM = ASDM::getFromXML(testdir); 	
  }
  catch (ConversionException e) {
  	cout << e.getMessage() << endl;
  	exit(1);
  }
  
  ReceiverTable &newReceiver =  newASDM->getReceiver();
  
  // Let's check what is actually stored in the Receiver table.
  myRows = newReceiver.get();
  cout << endl;
  cout << "There are " << myRows.size() << " rows in the Receiver table" << endl;
  cout << "Their addresses and ids are " << endl;
  for (int i = 0; i < myRows.size(); i++)
  	cout << (unsigned long) myRows[i] << "," << myRows[i]->getReceiverId() << endl;
  
  // Convert the Receiver table in its XML representation
  cout << endl;
  cout << "Here is the XML representation of the Receiver table:" << endl;
  string xmlNewReceiver = newReceiver.toXML();
  cout << xmlNewReceiver << endl;
}
