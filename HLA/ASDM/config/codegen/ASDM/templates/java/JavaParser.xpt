�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root (AlmaTableContainer atc) FOR AlmaModel�
�FILE "src/"+atc.DirPath+"/Parser.java"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File Parser.java
 */
package �atc.Path�;

import alma.hla.runtime.asdm.types.*;
import alma.hla.runtime.asdm.types.TagType;
import alma.hla.runtime.asdm.ex.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.util.Iterator;



public class Parser {

	private String str;		// The string being parsed.
	private int pos = 0;	// The current position in the string.
	private int beg = 0;	// The beginning and end of a fragement
	private int end = 0;	//    in the string.

	public Parser(String s) {
		str = s;
	}

	/**
	 * Is s in the string being parsed?
	 */
	public boolean isStr(String s) {
		return str.indexOf(s,pos) == -1 ? false : true;
	}
	
	/**
	 * Get the portion of the string bounded by s1 and s2, inclusive.
	 * @param s1
	 * @param s2
	 * @return
	 */
	public String getElement(String s1, String s2) {
		beg = str.indexOf(s1,pos);
		if (beg == -1)
			return null;
		end = str.indexOf(s2,beg + s1.length());
		if (end == -1)
			return null;
		pos = end + s2.length();
		return str.substring(beg,end + s2.length());
	}
	
	/**
	 * Get the portion of the string bounded by s1 and s2, exclusive.
	 * @param s1
	 * @param s2
	 * @return
	 */
	public String getElementContent(String s1, String s2) {
		String s = getElement(s1,s2);
		if (s == null)
			return null;
		return str.substring(beg + s1.length(),end).trim();
	}

	public String getField(String field) {
		beg = str.indexOf("<" + field + ">");
		if (beg == -1)
			return null;
		beg += field.length() + 2;
		end = str.indexOf("</" + field + ">",beg);
		if (end == -1)
			return null;
		return str.substring(beg,end).trim();
	}

	static public String getField(String xml, String field) {
		int b = xml.indexOf("<" + field + ">");
		if (b == -1)
			return null;
		b += field.length() + 2;
		int e = xml.indexOf("</" + field + ">",b);
		if (e == -1)
			return null;
		return xml.substring(b,e).trim();
	}
	
	static public byte[] getFieldInByte(String xml, String field) {
		int b = xml.indexOf("<" + field + ">");
		if (b == -1)
			return null;
		b += field.length() + 2;
		int e = xml.indexOf("</" + field + ">",b);
		if (e == -1)
			return null;
		return xml.substring(b,e).trim().getBytes();
	}
	
	static private Tag parseTag(String xmlField, String name, String tableName) throws ConversionException {
		String typeValue[] = xmlField.split("_");
		if (typeValue.length != 2)
			throw new ConversionException("Error:  field \"" + 
				name + "\" has an  invalid content \"" + xmlField + "\"", tableName);
		
		// Determine the Tag type
		TagType type = TagType.getTagType(typeValue[0]);
		if (type == null) 
					throw new ConversionException("Error:  field \"" + 
				name + "\" has an  invalid Tag type part \"" + typeValue[0] + "\"", tableName);	
		
		// Determine the Tag value
		int value = 0;
		try {
			value = Integer.parseInt(typeValue[1]);
		}
		catch (NumberFormatException e) {
			throw new ConversionException("Error:  field \"" + 
				name + "\" has an  invalid Tag value part \"" + typeValue[1] + "\"", tableName);
		}			
		return new Tag(value, type);		
	}
	
	//////////////////////////////////////////////////////
	// The follwing is a special case.
	//////////////////////////////////////////////////////

	static public String getString(String name, String tableName, String xmlDoc) 
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null)
			throw new ConversionException("Error: Missing field \"" + 
					name + "\" or invalid syntax",tableName);

		return xmlField;

	}

	static public String[] get1DString(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name).replaceAll("&quot;","\"");
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 1) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			String[] value = new String [dim0];
			if (dim0 == 0)
				return value;
			t.nextToken("\""); // the space
			value[0] = t.nextToken();
			for (int i = 1; i < value.length; ++i) {
				t.nextToken(); // the space		
				value[i] = t.nextToken();
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Unexpected end of string", tableName);
		}
	}

	static public String[][] get2DString(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name).replaceAll("&quot;","\"");
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 2) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			String[][] value = new String [dim0] [dim1];
			if (dim0 == 0 || dim1 == 0)
				return value;
			t.nextToken("\""); // the space
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
					value[i][j] = t.nextToken();
					if (i != dim0 - 1 || j != dim1 - 1)
						t.nextToken(); // the space
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Unexpected end of string", tableName);
		}
	}

	static public String[][][] get3DString(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name).replaceAll("&quot;","\"");
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 3) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			int dim2 = Integer.parseInt(t.nextToken());
			String[][][] value = new String [dim0] [dim1] [dim2];
			if (dim0 == 0 || dim1 == 0 || dim2 == 0)
				return value;
			t.nextToken("\""); // the space
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
					for (int k = 0; k < value[i][j].length; ++k) {
						value[i][j][k] = t.nextToken();
						if (i != dim0 - 1 || j != dim1 - 1 || k != dim2 - 1)
							t.nextToken(); // the space
					}
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Unexpected end of string", tableName);
		}
	}
	
	static public String[][][][] get4DString(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name).replaceAll("&quot;","\"");
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 4) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			int dim2 = Integer.parseInt(t.nextToken());
			int dim3 = Integer.parseInt(t.nextToken());
			String[][][][] value = new String [dim0] [dim1] [dim2][dim3];
			if (dim0 == 0 || dim1 == 0 || dim2 == 0 || dim3 == 0)
				return value;
			t.nextToken("\""); // the space
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
					for (int k = 0; k < value[i][j].length; ++k) {
						for (int l = 0; l <value[i][j][k].length; ++l) {
							value[i][j][k][l] = t.nextToken();
							if (i != dim0 - 1 || j != dim1 - 1 || k != dim2 - 1 || l != dim3 - 1)
								t.nextToken(); // the space
						}
					}
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
						name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Unexpected end of string", tableName);
		}
	}

	// Generated methods for conversion to and from XML
	// data representations for all types, both primitive
	// and extended.  Also included are 1, 2, and 3 
	// dimensional arrays of these types.
	
	�EXPAND DefineType FOREACH (Set[ASDMAttribute])atc.BasicType�

	// Generated methods for conversion to and from XML
	// data representations with  a Base64 encoded content.
	// The methods are generated only for 1, 2 and 3 dimensional arrays
	// of data whose BasicType have a non null BaseWrapperName.
	// In practice this represents data whose type is one of the basic numeric types
	// or is built upon a basic numeric type. 
	�EXPAND DefineTypeBase64 FOREACH (Set[ASDMAttribute])atc.BasicType�
}

�ENDFILE�
�ENDDEFINE�

�DEFINE DefineType FOR ASDMAttribute�
	// Field type: �JavaType�

	static public void toXML(�JavaType� data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
	�IF isStringType�	
		buf.append(data);
	�ELSEIF isArrayTimeIntervalType�
		//buf.append(data.getStart().get()+" "+data.getDuration().get());
		buf.append(data.getMidPoint().get()+" "+data.getDuration().get());
	�ELSEIF isExtendedType�
		buf.append(data.toString());		
	�ELSE�
		buf.append(�WrapperType�.toString(data));
	�ENDIF�	
		buf.append(" </" + name + "> ");
	}

	�IF JavaType=="int" || JavaType== "Tag"�
		�REM� Only IntSet and TagSet are defined at the moment �ENDREM�
	static public void toXML(�TypeSet� data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
		for (Iterator iter=data.iterator(); iter.hasNext(); ) {
			buf.append(((�WrapperType�) iter.next()).toString()).append(" "); 
		}
		buf.append(" </" + name + "> ");	
	}
	�ENDIF�
	
	static public void toXML(�JavaType�[] data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
		buf.append("1 ");
		buf.append(Integer.toString(data.length));
		buf.append(" ");
		for (int i = 0; i < data.length; ++i) {
	�IF isStringType�
			buf.append("\"");	
			buf.append(data[i]);
			buf.append("\"");
	�ELSEIF isArrayTimeIntervalType�
		buf.append(data[i].getMidPoint().get()+" "+data[i].getDuration().get()+" ");						
	�ELSEIF isExtendedType�
			buf.append(data[i].toString());
	�ELSE�
			buf.append(�WrapperType�.toString(data[i]));
	�ENDIF�	
			buf.append(" ");
		}
		buf.append(" </" + name + "> ");
	}
	
	static public void toXML(�JavaType�[][] data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
		buf.append("2 ");
		buf.append(Integer.toString(data.length));
		buf.append(" ");
		buf.append(Integer.toString(data[0].length));
		buf.append(" ");
		for (int i = 0; i < data.length; ++i) {
			for (int j = 0; j < data[i].length; ++j) {
	�IF isStringType�	
				buf.append("\"");	
				buf.append(data[i][j]);
				buf.append("\"");
	�ELSEIF isArrayTimeIntervalType�
				buf.append(data[i][j].getMidPoint().get()+" "+data[i][j].getDuration().get()+" ");					
	�ELSEIF isExtendedType�
				buf.append(data[i][j].toString());
	�ELSE�
				buf.append(�WrapperType�.toString(data[i][j]));
	�ENDIF�	
				buf.append(" ");
			}
		}
		buf.append(" </" + name + "> ");
	}
	
	static public void toXML(�JavaType�[][][] data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
		buf.append("3 ");
		buf.append(Integer.toString(data.length));
		buf.append(" ");
		buf.append(Integer.toString(data[0].length));
		buf.append(" ");
		buf.append(Integer.toString(data[0][0].length));
		buf.append(" ");
		for (int i = 0; i < data.length; ++i) {
			for (int j = 0; j < data[i].length; ++j) {
				for (int k = 0; k < data[i][j].length; ++k) {
	�IF isStringType�	
					buf.append("\"");	
					buf.append(data[i][j][k]);
					buf.append("\"");
	�ELSEIF isArrayTimeIntervalType�
					buf.append(data[i][j][k].getMidPoint().get()+" "+data[i][j][k].getDuration().get()+" ");								
	�ELSEIF isExtendedType�
					buf.append(data[i][j][k].toString());
	�ELSE�
					buf.append(�WrapperType�.toString(data[i][j][k]));
	�ENDIF�	
					buf.append(" ");
				}
			}
		}
		buf.append(" </" + name + "> ");
	}
	
	static public void toXML(�JavaType�[][][][] data, String name, StringBuffer buf) {
		buf.append("<" + name + "> ");
		buf.append("4 ");
		buf.append(Integer.toString(data.length));
		buf.append(" ");
		buf.append(Integer.toString(data[0].length));
		buf.append(" ");
		buf.append(Integer.toString(data[0][0].length));
		buf.append(" ");
		buf.append(Integer.toString(data[0][0][0].length));
		buf.append(" ");
		for (int i = 0; i < data.length; ++i) {
			for (int j = 0; j < data[i].length; ++j) {
				for (int k = 0; k < data[i][j].length; ++k) {
					for (int l = 0; l < data[i][j][k].length; ++l) {
	�IF isStringType�	
						buf.append("\"");	
						buf.append(data[i][j][k][l]);
						buf.append("\"");
	�ELSEIF isArrayTimeIntervalType�
						buf.append(data[i][j][k][l].getMidPoint().get()+" "+data[i][j][k][l].getDuration().get()+" ");								
	�ELSEIF isExtendedType�
						buf.append(data[i][j][k][l].toString());
	�ELSE�
						buf.append(�WrapperType�.toString(data[i][j][k][l]));
	�ENDIF�	
						buf.append(" ");
					}
				}
			}
		}
		buf.append(" </" + name + "> ");
	}
	
	�IF !isStringType�	
	
	
	static public �JavaType� get�WrapperType�(String name, String tableName, String xmlDoc) 
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null)
			throw new ConversionException("Error: Missing field \"" + 
				name + "\" or invalid syntax",tableName);
	�IF isCharType�
			return xmlField.charAt(0);	
	�ELSEIF isArrayTimeIntervalType�
		StringTokenizer t = new StringTokenizer (xmlField," ");
		long l1  = Long.parseLong(t.nextToken());
		long l2  = Long.parseLong(t.nextToken());
		
		if ( ArrayTimeInterval.readStartTimeDurationInXML())
			return new �JavaType� (l1 , l2);
		else
			return new �JavaType� (l1 - l2/2 , l2); 
	�ELSEIF JavaType == "Tag"�
		Tag tag = null;
		try {
			tag = Tag.parseTag(xmlField);
		}
		catch (TagFormatException e) {
			throw new ConversionException ("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		}			
		return tag;			
	�ELSEIF isExtendedType�
		return new �JavaType� (xmlField);
	�ELSE�
		try {
			�JavaType� data = �WrapperType�.�ParserType�(xmlField);
			return data;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		}
	�ENDIF�	
	}

	�IF JavaType=="Tag"�
	static public TagSet getTagSet(String name, String tableName, String xmlDoc)	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		StringTokenizer t = new StringTokenizer (xmlField," ");
		TagSet result = new TagSet();
		try {
			int nextTag = Integer.parseInt(t.nextToken());
			result.add(new Tag(nextTag));
		}
		catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} 
		return result;
	}	
	�ENDIF�
	
	�IF JavaType=="int"�
	static public IntSet getIntSet(String name, String tableName, String xmlDoc) 	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		StringTokenizer t = new StringTokenizer (xmlField," ");
		IntSet result = new IntSet();
		try {
			int nextTag = Integer.parseInt(t.nextToken());
			result.add(nextTag);
		}
		catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} 
		return result;	
	}	
	�ENDIF�
	
	static public �JavaType�[] get1D�WrapperType�(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 1) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			�JavaType�[] value = new �JavaType� [dim0];
			if (dim0 == 0)
				return value;
			for (int i = 0; i < value.length; ++i) {
	�IF isArrayTimeIntervalType�
				long l1  = Long.parseLong(t.nextToken());
				long l2  = Long.parseLong(t.nextToken());
		
				if ( ArrayTimeInterval.readStartTimeDurationInXML())
					value[i] = new �JavaType� (l1 , l2);
				else
					value[i] =  new �JavaType� (l1 - l2/2 , l2); 

	�ELSEIF JavaType == "Tag"�
				value[i] = parseTag(t.nextToken(), name, tableName);						
	�ELSEIF isExtendedType�
				value[i] = �WrapperType�.get�WrapperType�(t);
	�ELSEIF isCharType�
				value[i] = t.nextToken().charAt(0);
	�ELSE�
				value[i] = �WrapperType�.�ParserType�(t.nextToken());
	�ENDIF�
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": Unexpected end of string", tableName);
		}
	}
		
	static public �JavaType�[][] get2D�WrapperType�(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 2) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			�JavaType�[][] value = new �JavaType� [dim0] [dim1];
			if (dim0 == 0 || dim1 == 0)
				return value;
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
	�IF isArrayTimeIntervalType�
					long l1  = Long.parseLong(t.nextToken());
					long l2  = Long.parseLong(t.nextToken());
		
					if ( ArrayTimeInterval.readStartTimeDurationInXML())
						value[i][j] = new �JavaType� (l1 , l2);
					else
						value[i][j] =  new �JavaType� (l1 - l2/2 , l2);
	�ELSEIF JavaType == "Tag"�
					value[i][j]  = parseTag(t.nextToken(), name, tableName);								
	�ELSEIF isExtendedType�
					value[i][j] = �WrapperType�.get�WrapperType�(t);
	�ELSEIF isCharType�
					value[i][j] = t.nextToken().charAt(0);
	�ELSE�
					value[i][j] = �WrapperType�.�ParserType�(t.nextToken());
	�ENDIF�
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": Unexpected end of string", tableName);
		}
	}
	
	static public �JavaType�[][][] get3D�WrapperType�(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 3) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			int dim2 = Integer.parseInt(t.nextToken());
			�JavaType�[][][] value = new �JavaType� [dim0] [dim1] [dim2];
			if (dim0 == 0 || dim1 == 0 || dim2 == 0)
				return value;
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
					for (int k = 0; k < value[i][j].length; ++k) {
	�IF isArrayTimeIntervalType�
						long l1  = Long.parseLong(t.nextToken());
						long l2  = Long.parseLong(t.nextToken());
		
						if ( ArrayTimeInterval.readStartTimeDurationInXML())
							value[i][j][k] = new �JavaType� (l1 , l2);
						else
							value[i][j][k] =  new �JavaType� (l1 - l2/2 , l2);

	�ELSEIF JavaType == "Tag"�
						value[i][j][k] =  parseTag(t.nextToken(), name, tableName);								
	�ELSEIF isExtendedType�
						value[i][j][k] = �WrapperType�.get�WrapperType�(t);
	�ELSEIF isCharType�
						value[i][j][k] = t.nextToken().charAt(0);
	�ELSE�
						value[i][j][k] = �WrapperType�.�ParserType�(t.nextToken());
	�ENDIF�
					}
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": Unexpected end of string", tableName);
		}
	}
	
	static public �JavaType�[][][][] get4D�WrapperType�(String name, String tableName, String xmlDoc)
	throws ConversionException {
		String xmlField = Parser.getField(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid XML syntax", tableName);
		}
		StringTokenizer t = new StringTokenizer (xmlField," ");
		try {
			int ndim = Integer.parseInt(t.nextToken());
			if (ndim != 4) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Invalid array format", tableName);
			}
			int dim0 = Integer.parseInt(t.nextToken());
			int dim1 = Integer.parseInt(t.nextToken());
			int dim2 = Integer.parseInt(t.nextToken());
			int dim3 = Integer.parseInt(t.nextToken());
			�JavaType�[][][][] value = new �JavaType� [dim0] [dim1] [dim2][dim3];
			if (dim0 == 0 || dim1 == 0 || dim2 == 0 || dim3 == 0)
				return value;
			for (int i = 0; i < value.length; ++i) {
				for (int j = 0; j < value[i].length; ++j) {
					for (int k = 0; k < value[i][j].length; ++k) {
						for (int l = 0; l < value[i][j][k].length; ++l) {
	�IF isArrayTimeIntervalType�
							long l1  = Long.parseLong(t.nextToken());
							long l2  = Long.parseLong(t.nextToken());
		
							if ( ArrayTimeInterval.readStartTimeDurationInXML())
								value[i][j][k][l] = new �JavaType� (l1 , l2);
							else
								value[i][j][k][l] =  new �JavaType� (l1 - l2/2 , l2);

	�ELSEIF JavaType == "Tag"�
							value[i][j][k][l] =  parseTag(t.nextToken(), name, tableName);							
	�ELSEIF isExtendedType�
							value[i][j][k][l]       = �WrapperType�.get�WrapperType�(t);
	�ELSEIF isCharType�
							value[i][j][k][l]       = t.nextToken().charAt(0);
	�ELSE�
							value[i][j][k][l]       = �WrapperType�.�ParserType�(t.nextToken());
	�ENDIF�
						}
					}
				}
			}
			if (t.hasMoreTokens()) {
				throw new ConversionException("Error: Field \"" + 
					name + "\": Syntax error.", tableName);
			}
			return value;
		} catch (NumberFormatException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": " + e.toString(), tableName);
		} catch (NoSuchElementException e) {
			throw new ConversionException("Error: Field \"" + 
				name + "\": Unexpected end of string", tableName);
		}
	}

	�ENDIF�	

�ENDDEFINE�

�DEFINE  DefineTypeBase64 FOR ASDMAttribute�
	�REM� We consider only the types which have DataOutputType defined �ENDREM�
	
	�IF DataOutputType != null�
	
	�REM� One dimension array base64 encoding �ENDREM�
    static public void toXMLBase64(�JavaType�[] data, String name, StringBuffer buf) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Base64.OutputStream b64os = new Base64.OutputStream(baos,  Base64.ENCODE | Base64.DONT_BREAK_LINES | Base64.GZIP);
		DataOutputStream dos = new DataOutputStream(b64os);
		buf.append("<" + name + "> ");
		int ndim = 1;
		int dim1 = data.length;

		try {
		// Write the number of dimensions first (1).
		dos.writeInt(ndim);
			
		// Write the dimension then.
		dos.writeInt(dim1);
			
		// Write the content finally.
			�IF this.isExtendedType�
		for (int i = 0; i < dim1; i++) dos.write�DataOutputType�(data[i].get());
			�ELSE�
		for (int i = 0; i < dim1; i++) dos.write�DataOutputType�(data[i]);
			�ENDIF�
		}
		catch (IOException e) {
			throw e;
		}
		finally {
            try{ dos.close();   } catch( Exception e ){}
            try{ b64os.close(); } catch( Exception e ){}
            try{ baos.close();  } catch( Exception e ){}
        }   //
		buf.append(baos.toString());		
		buf.append(" </" + name + "> ");	
	}	
    
    �REM� Two dimensions array base64 encoding �ENDREM�
    	static public void toXMLBase64(�JavaType�[][] data, String name, StringBuffer buf) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Base64.OutputStream b64os = new Base64.OutputStream(baos,  Base64.ENCODE | Base64.DONT_BREAK_LINES | Base64.GZIP);
		DataOutputStream dos = new DataOutputStream(b64os);
		buf.append("<" + name + "> ");
		int ndim = 2;
		int dim1 = data.length;
		int dim2 = data[0].length;
	
		try {
		// Write the number of dimensions first (2).
		dos.writeInt(ndim);
			
		// Write the dimensions then.
		dos.writeInt(dim1);
		dos.writeInt(dim2);
			
		// Write the content finally.
			�IF isExtendedType�
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				dos.write�DataOutputType�(data[i][j].get());
			�ELSE�
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)
				 dos.write�DataOutputType�(data[i][j]);
			�ENDIF�
		}
		catch (IOException e) {
			throw e;
		}
		finally {
            try{ dos.close();   } catch( Exception e ){}
            try{ b64os.close(); } catch( Exception e ){}
            try{ baos.close();  } catch( Exception e ){}
        }   //
		buf.append(baos.toString());		
		buf.append(" </" + name + "> ");
	}		
    
    �REM� Three dimensions array base64 encoding �ENDREM�
    static public void toXMLBase64(�JavaType�[][][] data, String name, StringBuffer buf) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Base64.OutputStream b64os = new Base64.OutputStream(baos,  Base64.ENCODE | Base64.DONT_BREAK_LINES | Base64.GZIP);
		DataOutputStream dos = new DataOutputStream(b64os);
		buf.append("<" + name + "> ");
		int ndim = 3;
		int dim1 = data.length;
		int dim2 = data[0].length;
		int dim3 = data[0][0].length;
	
		try {
		// Write the number of dimensions first (3).
		dos.writeInt(ndim);
			
		// Write the dimensions then.
		dos.writeInt(dim1);
		dos.writeInt(dim2);
		dos.writeInt(dim3);			
			
		// Write the content finally.
			�IF this.isExtendedType�
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0 ; k < dim3; k++)
					dos.write�DataOutputType�(data[i][j][k].get());
			�ELSE�
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)
				for (int k = 0 ; k < dim3; k++)				
						dos.write�DataOutputType�(data[i][j][k]);
			�ENDIF�
		}
		catch (IOException e) {
			throw e;
		}
		finally
        {
            try{ dos.close();   } catch( Exception e ){}
            try{ b64os.close(); } catch( Exception e ){}
            try{ baos.close();  } catch( Exception e ){}
        }   //

		buf.append(baos.toString());	
		buf.append(" </" + name + "> ");		
    }
    
    �REM� Three dimensions array base64 encoding �ENDREM�
    static public void toXMLBase64(�JavaType�[][][][] data, String name, StringBuffer buf) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Base64.OutputStream b64os = new Base64.OutputStream(baos,  Base64.ENCODE | Base64.DONT_BREAK_LINES );
		DataOutputStream dos = new DataOutputStream(b64os);
		buf.append("<" + name + "> ");
		int ndim = 4;
		int dim1 = data.length;
		int dim2 = data[0].length;
		int dim3 = data[0][0].length;
		int dim4 = data[0][0][0].length;
	
		try {
		// Write the number of dimensions first (3).
		dos.writeInt(ndim);
			
		// Write the dimensions then.
		dos.writeInt(dim1);
		dos.writeInt(dim2);
		dos.writeInt(dim3);
		dos.writeInt(dim4);			
			
		// Write the content finally.
			�IF this.isExtendedType�
		for (int i = 0; i < dim1; i++) 
			for (int j = 0; j < dim2; j++)
				for (int k = 0 ; k < dim3; k++)
						for (int l = 0 ; l < dim4; l++)
							dos.write�DataOutputType�(data[i][j][k][l].get());
			�ELSE�
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)
				for (int k = 0 ; k < dim3; k++)
						for (int l = 0 ; l < dim4; l++)									
							dos.write�DataOutputType�(data[i][j][k][l]);
			�ENDIF�
		}
		catch (IOException e) {
			throw e;
		}
		finally
        {
            try{ dos.close();   } catch( Exception e ){}
            try{ b64os.close(); } catch( Exception e ){}
            try{ baos.close();  } catch( Exception e ){}
        }   //

		buf.append(baos.toString());	
		buf.append(" </" + name + "> ");		
    }
    
    �REM� One dimension array decoding from Base64 �ENDREM�
    static public �JavaType�[] get1D�WrapperType�FromBase64(String name, String tableName, String xmlDoc) throws ConversionException {
    	Base64.InputStream bis = null;
		DataInputStream dis = null;
        �JavaType�[] result = null;
 		byte[] xmlField = Parser.getFieldInByte(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Missing field \"" + name + "\" or invalid syntax",tableName);
		}		
		
		//		 Open streams
        bis = new Base64.InputStream( 
                  new java.io.ByteArrayInputStream(xmlField), 
                  Base64.DECODE );
		dis = new DataInputStream(bis);
		try {
			// Read the number of dimensions
			int ndim = dis.readInt();
			if (ndim != 1) {
				throw new ConversionException("Error while decoding Base64 representation of \"" + name + "\" : found " + ndim + " for the number of dimensions, expecting 1", tableName); 
			}
			
			// Read the first dimension.
			int dim1 = dis.readInt();
			
			// Read the values.
			result = new �JavaType�[dim1];
			for (int i = 0; i < dim1; i++)
			�IF this.isExtendedType�
				result[i] = new �JavaType�(dis.read�this.DataOutputType�());
			�ELSE�
				result[i] = dis.read�this.DataOutputType�();
			�ENDIF�
		}
		catch (IOException e) {
			throw new ConversionException("Error while decoding the Base64 representation of \"" + name + "\". The message was \"" + e.getMessage()+"\"", tableName);
		}
		finally
        {
            try{ bis.close();   } catch( Exception e ){}
            try{ dis.close(); } catch( Exception e ){}
        }
		
		return result;	   
    }
    
    �REM� Two dimensions array decoding from Base64 �ENDREM�
    static public �JavaType�[][] get2D�WrapperType�FromBase64(String name, String tableName, String xmlDoc) throws ConversionException {
     	Base64.InputStream bis = null;
		DataInputStream dis = null;
   		�JavaType�[][] result = null;
 		byte[] xmlField = Parser.getFieldInByte(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Missing field \"" + name + "\" or invalid syntax",tableName);
		}
		
		//	 Open streams.
        bis = new Base64.InputStream( 
                  new java.io.ByteArrayInputStream(xmlField), 
                  Base64.DECODE );
		dis = new DataInputStream(bis);
		
		// Read and decode.		
		try {
			// Read the number of dimensions
			int ndim = dis.readInt();
			if (ndim != 2) {
				throw new ConversionException("Error while decoding Base64 representation of \"" + name + "\" : found " + ndim + " for the number of dimensions, expecting 2", tableName);
			}
			
			// Read the first dimension.
			int dim1 = dis.readInt();
			
			// Read the second dimension.
			int dim2 = dis.readInt();
			
			// Read the values.
			result = new �JavaType�[dim1][dim2];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
			�IF this.isExtendedType�
					result[i][j] = new �JavaType�(dis.read�this.DataOutputType�());
			�ELSE�
					result[i][j] = dis.read�this.DataOutputType�();
			�ENDIF�
		} 
		catch (IOException e) {
			throw new ConversionException("Error while decoding the Base64 representation of \"" + name + "\". The message was \"" + e.getMessage()+"\"", tableName);
		}
		finally
        {
            try{ bis.close();   } catch( Exception e ){}
            try{ dis.close(); } catch( Exception e ){}
        }	
		return result;	     
    }
    
  �REM� Three dimensions array decoding from Base64 �ENDREM�
    static public �JavaType�[][][] get3D�WrapperType�FromBase64(String name, String tableName, String xmlDoc) throws ConversionException {
    	Base64.InputStream bis = null;
		DataInputStream dis = null;
  		�JavaType�[][][] result = null;
 		byte[] xmlField = Parser.getFieldInByte(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Missing field \"" + name + "\" or invalid syntax", tableName);	
		}
		
		//		 Open streams.
        bis = new Base64.InputStream( 
                  new java.io.ByteArrayInputStream(xmlField), 
                  Base64.DECODE );
		dis = new DataInputStream(bis);
		
		// Read and decode.
		try {
			// Read the number of dimensions
			int ndim = dis.readInt();
			if (ndim != 3) {
				throw new ConversionException("Error while decoding Base64 representation of \"" + name + "\" : found " + ndim + " for the number of dimensions, expecting 3", tableName);
			}
			
			// Read the first dimension.
			int dim1 = dis.readInt();
			
			// Read the second dimension.
			int dim2 = dis.readInt();
			
			// Read the third dimension.
			int dim3 = dis.readInt();
			
			// Read the values.
			result = new �JavaType�[dim1][dim2][dim3];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
					for (int k = 0; k < dim3; k++)
			�IF this.isExtendedType�
					result[i][j][k] = new �JavaType�(dis.read�this.DataOutputType�());
			�ELSE�
					result[i][j][k] = dis.read�this.DataOutputType�();
			�ENDIF�
		}
		catch (IOException e) {
			throw new ConversionException("Error while decoding the Base64 representation of \"" + name + "\". The message was \"" + e.getMessage()+"\"", tableName);
		}
		finally
        {
            try{ bis.close();   } catch( Exception e ){}
            try{ dis.close(); } catch( Exception e ){}
        }	
		return result;	     
    }
  
  �REM� Three dimensions array decoding from Base64 �ENDREM�
   static public �JavaType�[][][][] get4D�WrapperType�FromBase64(String name, String tableName, String xmlDoc) throws ConversionException {
    	Base64.InputStream bis = null;
		DataInputStream dis = null;
  		�JavaType�[][][][] result = null;
 		byte[] xmlField = Parser.getFieldInByte(xmlDoc,name);
		if (xmlField == null) {
			throw new ConversionException("Error: Missing field \"" + name + "\" or invalid syntax", tableName);	
		}
		
		//		 Open streams.
        bis = new Base64.InputStream( 
                  new java.io.ByteArrayInputStream(xmlField), 
                  Base64.DECODE );
		dis = new DataInputStream(bis);
		
		// Read and decode.
		try {
			// Read the number of dimensions
			int ndim = dis.readInt();
			if (ndim != 4) {
				throw new ConversionException("Error while decoding Base64 representation of \"" + name + "\" : found " + ndim + " for the number of dimensions, expecting 4", tableName);
			}
			
			// Read the first dimension.
			int dim1 = dis.readInt();
			
			// Read the second dimension.
			int dim2 = dis.readInt();
			
			// Read the third dimension.
			int dim3 = dis.readInt();
			
			// Read the fourth dimension.
			int dim4 = dis.readInt();
			
			// Read the values.
			result = new �JavaType�[dim1][dim2][dim3][dim4];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
					for (int k = 0; k < dim3; k++)
						for (int l = 0; l < dim4; l++)
			�IF this.isExtendedType�
							result[i][j][k][l] = new �JavaType�(dis.read�this.DataOutputType�());
			�ELSE�
							result[i][j][k][l] = dis.read�this.DataOutputType�();
			�ENDIF�
		}
		catch (IOException e) {
			throw new ConversionException("Error while decoding the Base64 representation of \"" + name + "\". The message was \"" + e.getMessage()+"\"", tableName);
		}
		finally
        {
            try{ bis.close();   } catch( Exception e ){}
            try{ dis.close(); } catch( Exception e ){}
        }	
		return result;	     
    }        
    �ENDIF�
�ENDDEFINE�
