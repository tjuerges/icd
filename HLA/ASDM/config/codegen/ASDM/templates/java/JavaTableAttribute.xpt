�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

 
�DEFINE Root FOR ASDMAttribute�
	�EXPAND AnyAttribute�
�ENDDEFINE�

�DEFINE AnyAttribute FOR ASDMAttribute�

	�IF this.isReadOnly�
		�EXPAND ReadOnlyAttribute FOR this�
	�ELSE�
		�EXPAND ReadWriteAttribute FOR this�
	�ENDIF�
	
�ENDDEFINE�

�DEFINE ReadOnlyAttribute FOR ASDMAttribute�
	�REM� 
		This attribute is readonly :
		-  it must be a scalar,
		-  it must have a basic type,
		-  it *must* have an initial value provided by the model,
		-  it can't be optional,
		-  it will have no setter.
	�ENDREM�
	�IF this.isArray�
		�ERROR "The attribute " + this.QualifiedName  + " is readonly and can't be an array"�
	�ENDIF�
	�IF this.isExtendedType�
		�ERROR "The attribute " + this.QualifiedName + " is readonly and must have a basic type (I found '" + this.Type.NameS + "')"�
	�ENDIF�

	�IF !this.hasInitValue�		
		�ERROR "The attribute " + this.QualifiedName + "  is readonly it must be given an initial value"�
	�ENDIF�
		
	�IF this.isOptional�
		�ERROR "The attribute " + this.QualifiedName + " is readonly and can't be optional"�
	�ENDIF�
	
	// ===> Attribute �NameS�, which is readonly
	private final �JavaType� �NameS� = �InitValue�;
	
	/**
	  * Get �NameS� , which is readonly.
	  * @return a �JavaType�
	  *
	  */
	public  �JavaType� get�UpperCaseName�() {
	  	return �NameS�;
	  }
�ENDDEFINE�

�DEFINE ReadWriteAttribute FOR ASDMAttribute�
	�IF isOptional�
	// ===> Attribute �NameS�, which is optional
	�ELSE�
	// ===> Attribute �NameS�
	�ENDIF�
	
	�IF isOptional�
	private boolean �NameS�Exists = false;
	�ENDIF�
	
	�IF isArray�
		�IF isExtrinsic�
	private ArrayList �NameS�;
		�ELSE�
	private �JavaType� �NameS�;		
	private �JavaType� copy�UpperCaseName�(�JavaType� a) {
			�IF isOneD�
			if ( a == null ) return new �SimpleJavaType�[0];
			int dim1 = a.length;
			if (dim1 == 0) return new �SimpleJavaType�[0];
			
			�JavaType� result = new �SimpleJavaType�[dim1];
			for (int i = 0; i  < result.length; i++) 
				�IF isExtendedType || SimpleJavaType == "String" �
				result[i] = new �SimpleJavaType�(a[i]);
				�ELSE�
				result[i] = a[i];
				�ENDIF� 
				
			�ELSEIF isTwoD�
			if ( a == null ) return new �SimpleJavaType�[0][0];			
			int dim1 = a.length;
			if (dim1 == 0) return new �SimpleJavaType�[0][0];
			
			int dim2 = a[0].length;
			if (dim2 == 0) return new �SimpleJavaType�[0][0];
			
			�JavaType� result = new �SimpleJavaType�[dim1][dim2];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
				�IF isExtendedType || SimpleJavaType == "String"�
					result[i][j] = new �SimpleJavaType�(a[i][j]);
				�ELSE�
					result[i][j] = a[i][j];
				�ENDIF�			
			�ELSEIF isThreeD�
			if ( a == null ) return new �SimpleJavaType�[0][0][0];			
			int dim1 = a.length;
			if (dim1 == 0) return new �SimpleJavaType�[0][0][0];
			
			int dim2 = a[0].length;		
			if (dim2 == 0) return new �SimpleJavaType�[0][0][0];
			
			int dim3 = a[0][0].length;
			if (dim3 == 0) return new �SimpleJavaType�[0][0][0];			
			
			�JavaType� result = new �SimpleJavaType�[dim1][dim2][dim3];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
					for (int k = 0; k < dim3; k++)
				�IF isExtendedType || SimpleJavaType == "String"�
						result[i][j][k] = new �SimpleJavaType�(a[i][j][k]);
				�ELSE�
						result[i][j][k] = a[i][j][k];
				�ENDIF�
			�ELSEIF isFourD�
			if (a == null) return new �SimpleJavaType�[0][0][0][0];			
			int dim1 = a.length;
			if (dim1 == 0) return new �SimpleJavaType�[0][0][0][0];
			
			int dim2 = a[0].length;		
			if (dim2 == 0) return new �SimpleJavaType�[0][0][0][0];
			
			int dim3 = a[0][0].length;
			if (dim3 == 0) return new �SimpleJavaType�[0][0][0][0];	
			
			int dim4 = a[0][0][0].length;
			if (dim4 == 0) return new �SimpleJavaType�[0][0][0][0];					
			
			�JavaType� result = new �SimpleJavaType�[dim1][dim2][dim3][dim4];
			for (int i = 0; i < dim1; i++)
				for (int j = 0; j < dim2; j++)
					for (int k = 0; k < dim3; k++)
						for (int l = 0; l < dim4; l++)
				�IF isExtendedType || SimpleJavaType == "String"�
							result[i][j][k][l] = new �SimpleJavaType�(a[i][j][k][l]);
				�ELSE�
							result[i][j][k][l] = a[i][j][k][l];
				�ENDIF�			
			�ENDIF�
		return result;
	}
		�ENDIF�
	�ELSE�
		�REM�
		If it's an ArrayTimeInterval attribute it must be modifiable by the add methods of the container table,
		so give this attribute a package access, otherwise declare is as private.
		�ENDREM�
	 	�IF !this.isArrayTimeIntervalType�private �ENDIF� �JavaType� �NameS�; 
	�ENDIF�

	�IF isOptional�
	/**
	 * The attribute �NameS� is optional. Return true if this attribute exists.
	 * @return true if and only if the �NameS� attribute exists. 
	 */
	public boolean is�UpperCaseName�Exists () {
		return �NameS�Exists;
	}
	�ENDIF�

	�IF isOptional�
 	/**
 	 * Get �NameS�, which is optional.
 	 * @return �NameS� as �JavaType�
 	 * @throws IllegalAccessException If �NameS� does not exist.
 	 */
 	 public �JavaType� get�UpperCaseName� ()  throws IllegalAccessException {
		if (!�NameS�Exists) {
			throw new IllegalAccessException("Attempt to access a non-existent attribute.  The " + 
				�NameS� + " attribute in table �Class.Name� does not exist!");
		}
	�ELSE�
 	/**
 	 * Get �NameS�.
 	 * @return �NameS� as �JavaType�
 	 */
 	 public �JavaType� get�UpperCaseName� (){
	�ENDIF�
	
	�IF isArray�
		�IF isExtrinsic�
		�JavaType� result = new �SimpleJavaType�[this.�NameS�.size()];
		for (int i = 0; i<result.length; i++) 
			result[i] = unwrap((�WrapperType�)�NameS�.get(i));
		return result;
		�ELSE�
		return copy�UpperCaseName�(�NameS�);
		�ENDIF�
	�ELSE�
		�IF this.isEnumLiteral�
		return �NameS�;
		�ELSEIF isExtendedType || SimpleJavaType == "String" �
 		return new �JavaType�(�NameS�);
 		�ELSE�
 		return �NameS�;
 		�ENDIF� 		
  	�ENDIF�
 	}
 	
 
 	/**
 	 * Set �NameS� with the specified �JavaType� value.
 	 * @param �NameS� The �JavaType� value to which �NameS� is to be set.
		�REM��IF isKeyPart� the mandatory attributes ! not only the Key ones ! Michel Caillat - 25 Jul 2008 �ENDREM�
		�IF !isOptional�
 	 * @throw IllegalAccessException If an attempt is made to change this field after is has been added to the table.
 	 */
 	�REM� public setters are allowed only on non autoincrementable attributes otherwise they are protected.�ENDREM�
 			�IF !isAutoIncrementable� public �ELSE� protected �ENDIF� void set�UpperCaseName� (�JavaType� �NameS�) throws IllegalAccessException {
  		if (hasBeenAdded) {
  			�REM� The modification of 25 Jul 2008 has important consequences on the DC code, so relax the constraints. �ENDREM�
  			�REM� We throw an exception only for Key attributes. �ENDREM�
  				�IF isKeyPart�
 			throw new IllegalAccessException("Attempt to change the �NameS� field, which is part of the key, after this row has been added to this table.");
 				�ELSE�			  				
  			if (false) // Temporary (?) workaround to never emit the Exception.
 				throw new IllegalAccessException("Attempt to change the �NameS� field, which is a mandatory attribute, after this row has been added to this table.");
  				�ENDIF�
	
  		}
  		�ELSE�
  	 */
  	public void set�UpperCaseName� (�JavaType� �NameS�) {
		�ENDIF�
		�IF isArray�
			�IF isExtrinsic�
		if (this.�NameS� == null) 
			this.�NameS� = new ArrayList();
		else		
			this.�NameS�.clear();
		for (int i = 0; i < �NameS�.length; i++)
			this.�NameS�.add(wrap(�NameS�[i]));
			�ELSE�
			this.�NameS� = copy�UpperCaseName�(�NameS�);
			�ENDIF�		 
		�ELSE�
			�IF this.isEnumLiteral�
		this.�NameS� = �NameS�;
			�ELSEIF isExtendedType �
 		this.�NameS� = new �JavaType�(�NameS�);
 			�ELSE�
 		this.�NameS� = �NameS�;
 			�ENDIF�
 		�ENDIF�
 	
		�IF isOptional�
			�NameS�Exists = true;
		�ENDIF�
 	}


 	
	�IF isOptional�
	/**
	 * Mark �NameS�, which is an optional field, as non-existent.
	 */
	public void clear�UpperCaseName� () {
		�NameS�Exists = false;
	}
	�ENDIF�
�ENDDEFINE�