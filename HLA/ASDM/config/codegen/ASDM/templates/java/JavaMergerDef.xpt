�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root (alma::hla::datamodel::meta::asdm::AlmaTableContainer  atc) FOR AlmaModel�
	�FILE "src/"+atc.DirPath+"/Merger.java"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File Merger.java
 */
 package alma.asdm;
import java.util.Hashtable;
import alma.hla.runtime.asdm.types.*;
import alma.hla.runtime.asdm.ex.*;

/**
 * A class to merge two ASDM datasets.
 * 
 * Generated from model's revision "�atc.CVSRevision�", branch "�atc.CVSBranch�"
 */
public class Merger {
	public static abstract class TableMerger {
	
		private static �atc.NameS� ds1;
		private static �atc.NameS� ds2;
		private static Hashtable<String, Tag>  tagTag;
		private static Hashtable<String, Integer> idId;
		
		static void init(�atc.NameS� ds1_, �atc.NameS� ds2_, Hashtable<String, Tag>  tagTag_, Hashtable<String, Integer> idId_) {
			ds1 = ds1_;
		 	ds2 = ds2_;
		 	tagTag = tagTag_;
		 	idId = idId_;
			�FOREACH (Set[AlmaTable])AlmaTable AS table�
			�table.NameS�Merger.hasMerged = false;
			�ENDFOREACH�
		};
		
		abstract void merge()  throws IllegalAccessException, DuplicateKey, MergerException;

		abstract void postMerge()  throws IllegalAccessException, DuplicateKey, MergerException;
		TableMerger() {
			hasMerged = false;
		}
		
		static private Tag getTag(final Tag t, TableMerger tm) throws IllegalAccessException, DuplicateKey, MergerException {
			if (tm != null) tm.merge();

			Tag result = tagTag.get(t.toString());
			if (result == null) throw new MergerException ("Failed to find the Tag in the result dataset corresponding to the Tag " + t.toString() + " in the merged dataset");
			return result;
		}
		
		static private int getId(final String tableName, int id, TableMerger tm) throws IllegalAccessException, DuplicateKey, MergerException{ 
			if (tm != null) tm.merge();
			
			Integer I = idId.get(tableName+"_"+Integer.toString(id));
			if (I == null) throw new MergerException("Failed to find the Id  in the result dataset corresponding to the Id " + tableName+"_"+Integer.toString(id) + " in the merged dataset");
			return I.intValue();
		}
			
		boolean hasMerged = false; 

	�FOREACH (Set[AlmaTable]) AlmaTable AS table�
		public static final TableMerger �table.NameS�Merger = new TableMerger()  {
			void merge() throws IllegalAccessException, DuplicateKey, MergerException {
				if (hasMerged) return;
				
		�IF !table.NoAutoIncrementableAttribute� 
			�IF table.TheAutoIncrementableAttribute.JavaType == "Tag" �		
				if (ds2.get�table.NameS�().size() == 0) {
					for (�table.NameS�Row row1 :  	ds1.get�table.NameS�().get()) {
						tagTag.put(row1.get�table.TheAutoIncrementableAttribute.UpperCaseName�().toString(), row1.get�table.TheAutoIncrementableAttribute.UpperCaseName�());
					}
				}
			�ELSE�
				if (ds2.get�table.NameS�().size() == 0) {
					for (�table.NameS�Row row1 :  	ds1.get�table.NameS�().get()) {
						idId.put("�table.NameS�_"+Integer.toString(row1.get�table.TheAutoIncrementableAttribute.UpperCaseName�()), new Integer(row1.get�table.TheAutoIncrementableAttribute.UpperCaseName�()));
					}
				}			
			�ENDIF�
		�ENDIF�

				for (�table.NameS�Row row2 :  	ds2.get�table.NameS�().get()) {
					�table.NameS�Row row = ds1.get�table.NameS�().newRow(row2);
		�IF table.RequiredExtrinsicAttributesSet.size != 0 �
			�FOREACH table.RequiredExtrinsicAttributesSet AS att�
				�IF att.isArray�
					�REM� If the attribute is an array then process each of its elements �ENDREM�
					�att.CppBaseType� �att.NameS�2[] = row2.get�att.UpperCaseName�();
					�att.CppBaseType� �att.NameS�1[] = new �att.CppBaseType�[�att.NameS�2.length] ;
					for (int j = 0; j < �att.NameS�2.length; j++)
					�IF att.JavaBaseType == "Tag"�
						�att.NameS�1[j] = getTag(�att.NameS�2[j], TableMerger.�att.targetTable�Merger);
					�ELSEIF att.isId�
						�att.NameS�1[j] = getId("�att.targetTable�", �att.NameS�2[j], TableMerger.�att.targetTable�Merger);
					�ENDIF�
					row.set�att.UpperCaseName�(	�att.NameS�1);
				�ELSE�
					�REM� If the attribute is a simple scalar �ENDREM�
					�IF att.JavaBaseType == "Tag"�
					Tag �att.NameS�Tag = getTag(row.get�att.UpperCaseName�(),TableMerger.�att.targetTable�Merger);
					row.set�att.UpperCaseName�(�att.NameS�Tag);
				�ELSEIF att.isId�
					row.set�att.UpperCaseName�(getId("�att.targetTable�", row.get�att.UpperCaseName�(), TableMerger.�att.targetTable�Merger));
				�ENDIF�
			�ENDIF�			
			�ENDFOREACH�
		�ENDIF�
					�table.NameS�Row retRow = ds1.get�table.NameS�().add(row);
		�IF !table.NoAutoIncrementableAttribute�
			�IF table.TheAutoIncrementableAttribute.JavaType == "Tag"�
					tagTag.put(row2.get�table.TheAutoIncrementableAttribute.UpperCaseName�().toString(), retRow.get�table.TheAutoIncrementableAttribute.UpperCaseName�());
			�ELSE�
					idId.put("�table.NameS�_"+Integer.toString(row2.get�table.TheAutoIncrementableAttribute.UpperCaseName�()), new Integer(retRow.get�table.TheAutoIncrementableAttribute.UpperCaseName�()));
			�ENDIF�
		�ENDIF�			
				}
				hasMerged = true;
			}
			
			
			void postMerge()  throws IllegalAccessException, DuplicateKey, MergerException {
				if (ds2.get�table.NameS�().get().length == 0) return;
		�IF table.OptionalExtrinsicAttributesSet.size != 0�
				try {
				for (�table.NameS�Row row1 : ds1.get�table.NameS�().get()) {
			�FOREACH table.OptionalExtrinsicAttributesSet AS att�
				�IF att.isOptional�
					if (row1.is�att.UpperCaseName�Exists()) {
					�IF att.isArray�
						�REM� If the attribute is an array then process each of its elements �ENDREM�
						�att.JavaBaseType�[] �att.NameS�1 = row1.get�att.UpperCaseName�();
						�att.JavaBaseType�[] �att.NameS�1_new = new �att.JavaBaseType�[�att.NameS�1.length];
						for (int j = 0; j < �att.NameS�1.length; j++) 
						�IF att.JavaBaseType == "Tag"�
							�att.NameS�1_new[j] = getTag( �att.NameS�1[j], null);
						�ELSEIF att.isId�
							�att.NameS�1_new[j] = getId("�att.targetTable�", �att.NameS�1[j], null));
						�ENDIF�
					
						row1.set�att.UpperCaseName�(	�att.NameS�1_new);
					�ELSE�			
						�REM� If the attribute is a simple scalar �ENDREM�
						�IF att.JavaBaseType == "Tag"�
						row1.set�att.UpperCaseName�(getTag(row1.get�att.UpperCaseName�(), null));
						�ELSEIF att.isId�
						row1.set�att.UpperCaseName�(getId("�att.targetTable�", row1.get�att.UpperCaseName�(), null));
						�ENDIF�
					�ENDIF�
					}
				�ELSE�
					;
				�ENDIF�
			�ENDFOREACH�
				}
			}
			catch (IllegalAccessException e) {
				// Should never happen
			}	
		�ENDIF�
			}	
		};					
	�ENDFOREACH�
	}
	
	public Merger() {
		ds1 = null;
		ds2 = null;
		tagTag = null;
		idId = null;
	}
	
   /**
	 * The method which performs the merging.
	 * 
	 * Given two ASDM datasets ds1 and ds2, this method merges ds2 into ds1.
	 *  On return ds1 contains all the rows already present before calling the method and all the rows of ds2 whose 
	 *  content was not already present in ds1 before the merge.
	 * 
	 * @param ds1 The ASDM dataset in which ds2 is merged.
	 * @param ds2 The ASDM dataset to be merged to ds1.
	 * @throws IllegalAccessException
	 * @throws DuplicateKey
	 * @throws MergerException
	 */
	public void merge(�atc.NameS� ds1, �atc.NameS� ds2)  throws IllegalAccessException, DuplicateKey, MergerException {
		this.ds1 = ds1;
		this.ds2 = ds2;
		tagTag = new Hashtable<String, Tag>();
		idId = new Hashtable<String, Integer>();	
		
		TableMerger.init(ds1, ds2, tagTag, idId);

	�FOREACH (Set[AlmaTable])AlmaTable AS table�
		TableMerger.�table.NameS�Merger.merge();
	�ENDFOREACH�
	�FOREACH (Set[AlmaTable])AlmaTable AS table�
		TableMerger.�table.NameS�Merger.postMerge();
	�ENDFOREACH�			
	}
	
	private �atc.NameS� ds1, ds2;
	private Hashtable<String, Tag>  tagTag = null;
	
	
	private Hashtable<String, Integer> idId = null;

	
}
 	�ENDFILE�
�ENDDEFINE�