�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root (AlmaTableContainer atc) FOR AlmaTable�
�FILE "include/"+NameS+"Row.h"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File �NameS�Row.h
 */
 
#ifndef �NameS�Row_CLASS
#define �NameS�Row_CLASS

#include <vector>
#include <string>
#include <set>
using std::vector;
using std::string;
using std::set;

#ifndef WITHOUT_ACS
#include <asdmIDLC.h>
using asdmIDL::�NameS�RowIDL;
#endif

�REM� Do we have a long attribute at least , in such a case we must do the adapted include �ENDREM�
�IF this.hasLongAttribute�
#include <stdint.h>
�ENDIF�

�REM� Do we need to include files for extended types ?�ENDREM�
�FOREACH (Set[Type]) UsedExtendedTypes AS et �
#include <�et.NameS�.h>
using  asdm::�et.NameS�;
�ENDFOREACH�

�REM� Do we need to include files for enumerations �ENDREM�
�FOREACH (Set[ASDMAttribute])IntrinsicAttribute AS attr�
	�IF attr.isEnumeration�
#include "C�attr.Type.NameS�.h"
using namespace �attr.Type.NameS�Mod;
	�ENDIF�
�ENDFOREACH�

�REM� Exceptions definitions are included unconditionally. �ENDREM�
#include <ConversionException.h>
#include <NoSuchRow.h>
#include <IllegalAccessException.h>

#include <RowTransformer.h>

/*\file �NameS+".h"�
    \brief Generated from model's revision "�this.CVSRevision�", branch "�this.CVSBranch�"
*/

namespace asdm {

//class asdm::�NameS�Table;

�FOREACH (Set[TableLink])Link AS k �
// class asdm::�k.Target.NameS�Row;
class �k.Target.NameS�Row;
�ENDFOREACH�	

class �NameS�Row;
typedef void (�NameS�Row::*�NameS�AttributeFromBin) (EndianISStream& eiss);

/**
 * The �NameS�Row class is a row of a �NameS�Table.
 * 
 * Generated from model's revision "�this.CVSRevision�", branch "�this.CVSBranch�"
 *
 */
class �NameS�Row {
friend class asdm::�NameS�Table;
friend class asdm::RowTransformer<�NameS�Row>;

public:

	virtual ~�NameS�Row();

	/**
	 * Return the table to which this row belongs.
	 */
	�NameS�Table &getTable() const;
	
	/**
	 * Has this row been added to its table ?
	 * @return true if and only if it has been added.
	 */
	bool isAdded() const;
		
	////////////////////////////////
	// Intrinsic Table Attributes //
	////////////////////////////////
	�EXPAND CppTableAttributeDefGS::Root FOREACH (Set[ASDMAttribute])IntrinsicAttribute�
	////////////////////////////////
	// Extrinsic Table Attributes //
	////////////////////////////////
	�EXPAND CppTableAttributeDefGS::Root FOREACH (List[ASDMAttribute])ExtrinsicAttribute�
	///////////
	// Links //
	///////////
	�EXPAND CppTableLinksDefGS::Root FOREACH (Set[TableLink])Link�
	
	
	�IF  RequiredNoAutoIncrementableAttributesSet.size != 0�
	/**
	 * Compare each mandatory attribute except the autoincrementable one of this �NameS�Row with 
	 * the corresponding parameters and return true if there is a match and false otherwise.
	 	�FOREACH (Set[ASDMAttribute])RequiredNoAutoIncrementableAttributesSet AS attr�
	 * @param �attr.NameS�
	    �ENDFOREACH�
	 */ 
	bool compareNoAutoInc(�RequiredNoAutoIncrementableCpp�);
	
	�ENDIF�

	�IF RequiredValueAttributesSet.size != 0�
	/**
	 * Compare each mandatory value (i.e. not in the key) attribute  with 
	 * the corresponding parameters and return true if there is a match and false otherwise.
	 	�FOREACH (Set[ASDMAttribute])RequiredValueAttributesSet AS attr�
	 * @param �attr.NameS�
	    �ENDFOREACH�
	 */ 
	bool compareRequiredValue(�RequiredValueCpp�); 
	�ENDIF�	 
	
	/**
	 * Return true if all required attributes of the value part are equal to their homologues
	 * in x and false otherwise.
	 *
	 * @param x a pointer on the �NameS�Row whose required attributes of the value part 
	 * will be compared with those of this.
	 * @return a boolean.
	 */
	bool equalByRequiredValue(�NameS�Row* x) ;
	
#ifndef WITHOUT_ACS
	/**
	 * Return this row in the form of an IDL struct.
	 * @return The values of this row as a �NameS�RowIDL struct.
	 */
	�NameS�RowIDL *toIDL() const;
#endif
	
#ifndef WITHOUT_ACS
	/**
	 * Fill the values of this row from the IDL struct �NameS�RowIDL.
	 * @param x The IDL struct containing the values used to fill this row.
	 * @throws ConversionException
	 */
	void setFromIDL (�NameS�RowIDL x) ;
#endif
	
	/**
	 * Return this row in the form of an XML string.
	 * @return The values of this row as an XML string.
	 */
	string toXML() const;

	/**
	 * Fill the values of this row from an XML string 
	 * that was produced by the toXML() method.
	 * @param rowDoc the XML string being used to set the values of this row.
	 * @throws ConversionException
	 */
	void setFromXML (string rowDoc) ;	

private:
	/**
	 * The table to which this row belongs.
	 */
	�NameS�Table &table;
	/**
	 * Whether this row has been added to the table or not.
	 */
	bool hasBeenAdded;

	// This method is used by the Table class when this row is added to the table.
	void isAdded(bool added);


	/**
	 * Create a �NameS�Row.
	 * <p>
	 * This constructor is private because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 */ 
	�NameS�Row (�NameS�Table &table);

	/**
	 * Create a �NameS�Row using a copy constructor mechanism.
	 * <p>
	 * Given a �NameS�Row row and a �NameS�Table table, the method creates a new
	 * �NameS�Row owned by table. Each attribute of the created row is a copy (deep)
	 * of the corresponding attribute of row. The method does not add the created
	 * row to its table, its simply parents it to table, a call to the add method
	 * has to be done in order to get the row added (very likely after having modified
	 * some of its attributes).
	 * If row is null then the method returns a row with default values for its attributes. 
	 *
	 * This constructor is private because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 * @param row  The row which is to be copied.
	 */
	 �NameS�Row (�NameS�Table &table, �NameS�Row &row);
	 	
	////////////////////////////////
	// Intrinsic Table Attributes //
	////////////////////////////////
	�EXPAND CppTableAttributeDef::Root FOREACH (Set[ASDMAttribute])IntrinsicAttribute�
	////////////////////////////////
	// Extrinsic Table Attributes //
	////////////////////////////////
	�EXPAND CppTableAttributeDef::Root FOREACH (List[ASDMAttribute])ExtrinsicAttribute�
	///////////
	// Links //
	///////////
	�EXPAND CppTableLinksDef::Root FOREACH (Set[TableLink])Link�
	
	///////////////////////////////
	// binary-deserialization material//
	///////////////////////////////
	map<string, �NameS�AttributeFromBin> fromBinMethods;
	�EXPAND binaryDeserialize(this) FOREACH (Set[ASDMAttribute]) RequiredAttributesSet�
	�EXPAND binaryDeserialize(this) FOREACH (Set[ASDMAttribute]) OptionalValueAttributesSet�	
	
	/**
	 * Serialize this into a stream of bytes written to an EndianOSStream.
	 * @param eoss the EndianOSStream to be written to
	 */
	 void toBin(EndianOSStream& eoss);
	 	 
	 /**
	  * Deserialize a stream of bytes read from an EndianISStream to build a PointingRow.
	  * @param eiss the EndianISStream to be read.
	  * @param table the �NameS�Table to which the row built by deserialization will be parented.
	  * @param attributesSeq a vector containing the names of the attributes . The elements order defines the order 
	  * in which the attributes are written in the binary serialization.
	  */
	 static �NameS�Row* fromBin(EndianISStream& eiss, �NameS�Table& table, const vector<string>& attributesSeq);	 

};

} // End namespace asdm

#endif /* �NameS�_CLASS */
�ENDFILE�
�ENDDEFINE�

�DEFINE binaryDeserialize(AlmaTable table) FOR ASDMAttribute-�
void �NameS�FromBin( EndianISStream& eiss);
�ENDDEFINE�
