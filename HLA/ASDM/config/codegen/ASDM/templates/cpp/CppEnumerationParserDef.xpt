�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�
�IMPORT alma::hla::datamodel::enumerations::meta�
�IMPORT alma::hla::datamodel::enumerations::util�

�DEFINE Root FOR AlmaModel�
	�FILE "include/EnumerationParser.h"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * /////////////////////////////////////////////////////////////////
 * // WARNING!  DO NOT MODIFY THIS FILE!                          //
 * //  ---------------------------------------------------------  //
 * // | This is generated code!  Do not modify this file.       | //
 * // | Any changes will be lost when the file is re-generated. | //
 * //  ---------------------------------------------------------  //
 * /////////////////////////////////////////////////////////////////
 *
 * File CEnumerationParser.h
 */
 #include <string>
 #include <ConversionException.h>
 
 using namespace std;
 
 �REM� Include all enumerations include files. �ENDREM�
  �FOREACH (Set[AlmaEnumeration]) Enumerations AS enum�
 #include "C�enum.NameS�.h"
 using namespace �enum.NameS�Mod;
 �ENDFOREACH�
 
 namespace asdm {
 
 	class EnumerationParser {
 	
 	static string getField(const string &xml, const string &field);
 	
 	static string substring(const string &s, int a, int b);
 	
 	static string trim(const string &s);
	
	public:
	�EXPAND dotH FOREACH (List[AlmaEnumeration]) Enumerations�
	
	};
	
} // namespace asdm.

�ENDFILE�
�ENDDEFINE�
	
�DEFINE dotH FOR AlmaEnumeration�
	/**
	 * Returns a string which represents a XML element 
	 * with name 'elementName' and a content equal to 
	 * the string associated to 'e'
	 * @param elementName a string.
	 * @param e  �NameS�Mod::�NameS� value.
	 * @return a string.
	 */
	 static string toXML(const string& elementName, �NameS�Mod::�NameS� e);
	 
	 
	/**
	 * Returns a string which represents a XML element 
	 * with name 'elementName' and of content equal to 
	 * the number of elements of 'v_e' followed by sequence of strings associated to each element of 'v_e'
	 * @param elementName a string.
	 * @param v_e  a const reference to a vector<�NameS�Mod::�NameS�>.
	 * @return a string.
	 */
	 static string toXML(const string& elementName, const vector<�NameS�Mod::�NameS�>& v_e);
	
	/**
	 * Returns a string which represents a XML element 
	 * with name 'elementName' and of content equal to 
	 * the number of elements of 'vv_e' followed by sequence of strings associated to each element of 'vv_e'.
	 * @param elementName a string.
	 * @param vv_e  a const reference to a vector<vector<�NameS�Mod::�NameS�> >.
	 * @return a string.
	 */	
	 static string toXML(const string& elementName, const vector<vector<�NameS�Mod::�NameS�> >& vv_e); 
	 


	/**
	 * Returns a string which represents a XML element 
	 * with name 'elementName' and of content equal to 
	 * the number of elements of 'vvv_e' followed by sequence of strings associated to each element of 'vvv_e'.
	 * @param elementName a string.
	 * @param vvv_e  a const reference to a vector<vector<vector<�NameS�Mod::�NameS�> > >.
	 * @return a string.
	 */	
	 static string toXML(const string& elementName, const vector<vector<vector<�NameS�Mod::�NameS�> > >& vvv_e); 

	/**
	 * Returns a �NameS�Mod::�NameS� from a string.
	 * @param xml the string to be converted into a �NameS�Mod::�NameS�
	 * @return a �NameS�Mod::�NameS�.
	 */
	static �NameS�Mod::�NameS� get�NameS�(const string &name, const string &tableName, const string &xmlDoc);
	
	/**
	 * Returns a vector<�NameS�Mod::�NameS�> from a string.
	 * @param xml the string to be converted into a vector<�NameS�Mod::�NameS�>
	 * @return a vector<�NameS�Mod::�NameS�>.
	 */
	static vector<�NameS�Mod::�NameS�> get�NameS�1D(const string &name, const string &tableName, const string &xmlDoc);
	
	/**
	 * Returns a vector<vector<�NameS�Mod::�NameS�> > from a string.
	 * @param xml the string to be converted into a vector<vector<�NameS�Mod::�NameS�> >
	 * @return a vector<vector<�NameS�Mod::�NameS�> >.
	 */
	static vector<vector<�NameS�Mod::�NameS�> > get�NameS�2D(const string &name, const string &tableName, const string &xmlDoc);
	
	/**
	 * Returns a vector<vector<vector<�NameS�Mod::�NameS�> > > from a string.
	 * @param xml the string to be converted into a vector<vector<vector<�NameS�Mod::�NameS�> > >
	 * @return a vector<vector<vector<�NameS�Mod::�NameS�> > >.
	 */
	static vector<vector<vector<�NameS�Mod::�NameS�> > > get�NameS�3D(const string &name, const string &tableName, const string &xmlDoc);								
�ENDDEFINE�
