�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root FOR AlmaTableContainer�
�FILE "include/"+NameS+".h"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File �NameS�.h
 */
 
#ifndef �NameS�_CLASS
#define �NameS�_CLASS

#include <vector>
using std::vector;

#include <map>
using std::map;

#include <Representable.h>
#include <Entity.h>
#include <EntityId.h>
#include <ArrayTime.h>
#include <IllegalAccessException.h>
#include <InvalidArgumentException.h>

using asdm::Entity;
using asdm::EntityId;
using asdm::ArrayTime;
using asdm::IllegalAccessException;
using asdm::InvalidArgumentException;

#ifndef WITHOUT_ACS
#include <asdmIDLC.h> /// <-------------------
using namespace asdmIDL;   /// <-------------------
#endif

/*\file �NameS+".h"�
    \brief Generated from model's revision "�this.CVSRevision�", branch "�this.CVSBranch�"
*/

namespace asdm {

�FOREACH (Set[AlmaTable])AlmaTable AS table�
//class asdm::�table.Name�Table;
class �table.Name�Table;
�ENDFOREACH�

/**
 * The �NameS� class is the container for all tables.  Its instantation
 * creates a complete set of tables.
 *
 * 
 * Generated from model's revision "�this.CVSRevision�", branch "�this.CVSBranch�"
 */
//class �NameS� : public Representable {
class �NameS� {

public:
	/**
	 * Constructs an empty �NameS�.
	 */
	�NameS� ();
	
	virtual ~�NameS� ();
	
�FOREACH (Set[AlmaTable])AlmaTable AS table �
	/**
	 * Get the table �table.Name�.
	 * @return The table �table.Name� as a �table.Name�Table.
	 */
	�table.Name�Table & get�table.Name� () const;
�ENDFOREACH�
	/**
	  * Produces the XML representation of * this.
	  * @return a string containing the XML representation of this.
	  * @throws ConversionException.
	  */
	virtual string toXML();
	
	/**
	 * Write this �NameS� dataset to the specified directory
	 * as a collection of XML documents. 
	 * @param directory The directory to which this dataset is written.
	 * @throws ConversionException If any error occurs in converting the
	 * table to XML and writing it to the directory.  This method will
	 * not overwrite any existing file; a ConversionException is also
	 * thrown in this case.
	 */
	void toXML(string directory) ;
	
	/**
	 * Get an ASDM dataset, given the full path name of the 
	 * directory containing the XML version of the dataset.
	 * @param xmlDirectory The full path name of the directory
	 * containing this dataset.
	 * @return The complete dataset that belongs to the container
	 * in this directory.
	 * @throws ConversionException If any error occurs reading the 
	 * files in the directory or in converting the tables from XML.
	 */
	virtual void fromXML(string xml) ;
		
	/**
	 * Get an �NameS� dataset, given the full path name of the 
	 * directory containing the XML version of the dataset.
	 * @param xmlDirectory The full path name of the directory
	 * containing this dataset.
	 * @return The complete dataset that belongs to the container
	 * in this directory.
	 * @throws ConversionException If any error occurs reading the 
	 * files in the directory or in converting the tables from XML.
	 *
	 * @deprecated
	 */
	static �NameS� *getFromXML(string xmlDirectory) ;
	
   /**
	 * Serialize this into a stream of bytes and encapsulates that stream into a MIME message.
	 * @returns a string containing the MIME message.
	 * 
	 */
	string toMIME();
	
   /** 
     * Extracts the binary part of a MIME message and deserialize its content
	 * to fill this with the result of the deserialization. 
	 * @param mimeMsg the string containing the MIME message.
	 * @throws ConversionException
	 */
	 void setFromMIME(const string & mimeMsg);	

	/**
	 * Write this �NameS� dataset to the specified directory
	 * as a collection of files.
	 *
	 * The container itself is written into an XML file. Each table of the container
	 * having at least one row is written into a binary or an XML file depending on
	 * the value of its "fileAsBin" private field.
	 * 
	 * @param directory The directory to which this dataset is written.
	 * @throws ConversionException If any error occurs in converting the
	 * container or any of its table.  This method will
	 * not overwrite any existing file; a ConversionException is also
	 * thrown in this case.
	 */
	void toFile(string directory);

	/**
	 * Constructs totally or partially an ASDM dataset from its representation on disk.
	 *
	 * Reads and parses a file (�NameS�.xml) containing the top level element of an ASDM.
	 * Depending on the value of the boolean parameter loadTablesOnDemand the files containing the tables of
	 * of the dataset are parsed to populate the dataset in memory immediately (false) or only when an application tries
	 * to retrieve values from these tables (true).
	 *
	 * @param directory the name of the directory containing the files.
	 * @param loadTablesOnDemand the tables are read and parsed immediately (false) or only when necessary (true).
	 * @throws ConversionException If any error occurs while reading the 
	 * files in the directory or parsing them.
	 *
	 */	
	 void setFromFile(string directory, bool loadTablesOnDemand);
	 
	/**
	 * Constructs an ASDM dataset from its representation on disk.
	 *
	 * Reads and parses a file (�NameS�.xml) containing the top level element of an ASDM and then the files
	 * containing the representation on disk of the dataset's tables. On exit the dataset contains 
	 * all its tables in memory.
	 *
	 * @param directory the name of the directory containing the files.
	 * @throws ConversionException If any error occurs while reading the 
	 * files in the directory or parsing them.
	 *
	 */	
	 void setFromFile(string directory);
	
	#ifndef WITHOUT_ACS
	/**
	  * Converts this �NameS� into an �NameS�DataSetIDL CORBA structure
	  * @return a pointer to a �NameS�DataSetIDL.
	  */
	virtual ASDMDataSetIDL* toIDL();  
	
	 /**
	   * Builds an �NameS� out of its IDL representation.
	   * @param x the IDL representation of the �NameS�
	   *
	   * @throws DuplicateKey 
	   * @throws ConversionException
	   * @throws UniquenessViolationException
	   */
	virtual void fromIDL(ASDMDataSetIDL* x); 
	#endif
		
	virtual Entity getEntity() const;

	virtual void setEntity(Entity e);
	
	/**
	 * Meaningless, but required for the Representable interface.
	 */
	virtual string getName() const;
	
	/**
	 * Meaningless, but required for the Representable interface.
	 */
	virtual unsigned int size() ;
	
#ifndef WITHOUT_ACS
	/**
	 * Create an �NameS� dataset from the ALMA archive, given the
	 * entityId of its container.
	 * @param datasetId The entityId of the container of the dataset.
	 * @throws ConversionException
	 */
	static �NameS� *fromArchive(EntityId datasetId) ;
#endif

#ifndef WITHOUT_ACS
	/**
	 * Update an �NameS� dataset that already exists in the ALMA archive.
	 * @throws ConversionException
	 */
	void updateArchive() const ;
#endif
	
	/**
	 * Return the table, as a Representable object, with the
	 * specified name.
	 * @throws InvalidArgumentException
	 */
	Representable &getTable(string tableName) ;

	�EXPAND CppTableAttributeDefGS::Root FOREACH (Set[ASDMAttribute])ASDMAttributes�

	/**
	 *  \enum Origin
	 *
	 *  \brief This enumeration lists the different possible origins for an ASDM present in memory. 
	 */
	enum Origin {
		FILE,  ///< The dataset has been constructed from its representation on disk. 
		ARCHIVE, ///< The dataset has been constructed from its representation in the Archive. 
		EX_NIHILO ///< The dataset has been constructed ex nihilo.
	};
	
	/**
	 * Returns the origin of the dataset in memory.
	 *
	 * @return an �NameS�::Origin value.
	 */
	 Origin getOrigin() const ;
	 
	 /**
	  * Returns the ASDM's directory.
	  * 
	  * @return a string containing path to the directory containing the external representation of the ASDM
	  * if it has been constructed from this representation or an empty string if it has been
	  * constructed ex nihilo.
	  */
	 string getDirectory() const ;
	 
	 	
private:

	bool archiveAsBin; // If true archive binary else archive XML
	bool fileAsBin ; // If true file binary else file XML		
	bool hasBeenAdded;
	Origin origin;
	bool loadTablesOnDemand;
	string directory;
		
�FOREACH (Set[AlmaTable])AlmaTable AS table �
	/**
	 * The table �table.Name�
	 */
	�table.Name�Table * �table.LowerCase�;
�ENDFOREACH�
	/**
	 * The list of tables as Representable.
	 */
	vector<Representable *> table;
	
	/**
	 * The list of Entity objects representing the tables.
	 */
	//vector<Entity *> tableEntity;
	map<string, Entity> tableEntity;
	
	/**
	 * This Container's entity.
	 */
	Entity entity;

	�EXPAND CppTableAttributeDef::Root FOREACH (Set[ASDMAttribute])ASDMAttributes�
	
	void error() ; // throw(ConversionException);
	static string getXMLEntity(EntityId id); // throw(ConversionException);
	static void putXMLEntity(string xml); // throw(ConversionException);
	
};

} // End namespace asdm

#endif /* �NameS�_CLASS */
�ENDFILE�
�ENDDEFINE�

