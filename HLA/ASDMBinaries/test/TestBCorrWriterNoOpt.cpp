#include "SDMDataObjectWriter.h"

#ifndef WITHOUT_ACS
#include "almaEnumerations_IFC.h"
#endif

#include "CAtmPhaseCorrection.h"
#include "CCorrelationMode.h"
#include "CSpectralResolutionType.h"
#include "CStokesParameter.h"


#include <stdarg.h>

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)



using namespace CorrelationModeMod;
using namespace AtmPhaseCorrectionMod;
using namespace StokesParameterMod;

using namespace asdmbinaries;

CorrelationMode correlationMode;
SpectralResolutionType spectralResolutionType;
SDMDataObject::DataStruct dataStruct;
vector<AtmPhaseCorrection> apc;

unsigned int flagsSize;
unsigned int actualTimesSize;
unsigned int actualDurationsSize;
unsigned int crossDataSize;
unsigned int autoDataSize;
unsigned int zeroLagsSize;

SDMDataObject::BinaryPart flagsDesc;
SDMDataObject::BinaryPart actualTimesDesc;
SDMDataObject::BinaryPart actualDurationsDesc;
SDMDataObject::BinaryPart zeroLagsDesc;
SDMDataObject::BinaryPart crossDataDesc;
SDMDataObject::BinaryPart autoDataDesc;


vector<SDMDataObject::SpectralWindow> spectralWindows;
vector<StokesParameter> sdPolProducts;
vector<StokesParameter> crossPolProducts;

vector<SDMDataObject::Baseband> basebands;

string title = "ALMA Correlator_B Spectral Data";

void setupFR_CO() {
  correlationMode    = CROSS_ONLY;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 1 Baseband with 2 spectral windows.
  basebands.clear();

  spectralWindows.clear();
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 256, 2));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1024, 2));

  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart();
  actualTimesDesc = SDMDataObject::BinaryPart();
  actualDurationsDesc = SDMDataObject::BinaryPart();
  crossDataDesc = SDMDataObject::BinaryPart (40960, ENUMVEC(AxisName, "BAL BAB SPW BIN APC SPP POL"));
  autoDataDesc = SDMDataObject::BinaryPart();
  zeroLagsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"));
  
  apc = ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);  
}

void setupFR_AO() {
  correlationMode    = AUTO_ONLY;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 2 basebands with respectively 1 and 2 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 256, 2));
  
  basebands.push_back(SDMDataObject::Baseband(spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1));
  
  basebands.push_back(SDMDataObject::Baseband(spectralWindows));
  


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart();
  autoDataDesc = SDMDataObject::BinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL") );
  zeroLagsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"));
  
  apc.clear();
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);    
}


void setupFR_CA() {
  correlationMode    = CROSS_AND_AUTO;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 4 basebands with respectively 2, 1, 1 and 1 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 256, 1));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 256, 1));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  sdPolProducts = ENUMVEC(StokesParameter, "XX XY YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 512, 1));

  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1024, 1));
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  
  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 512, 1));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));
    


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart(40960, ENUMVEC(AxisName, "BAL ANT BAB SPW APC SPP POL"));
  autoDataDesc = SDMDataObject::BinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL") );
  zeroLagsDesc = SDMDataObject::BinaryPart(20, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"));
  
  // Build the DataStruct.
  apc =  ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);      
}



void setupCA_CO() {
  correlationMode    = CROSS_ONLY;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 1 Baseband with 2 spectral windows.
  basebands.clear();

  spectralWindows.clear();
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1, 2));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1, 2));

  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(16,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart (64, ENUMVEC(AxisName, "BAL BAB SPW BIN APC SPP POL"));
  autoDataDesc = SDMDataObject::BinaryPart();
  zeroLagsDesc = SDMDataObject::BinaryPart();
  
  apc = ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);  
}

void setupCA_AO() {
  correlationMode    = AUTO_ONLY;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 2 basebands with respectively 1 and 2 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 256, 2));
  
  basebands.push_back(SDMDataObject::Baseband(spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1));
  
  basebands.push_back(SDMDataObject::Baseband(spectralWindows));
  


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart();
  autoDataDesc = SDMDataObject::BinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL") );
  zeroLagsDesc = SDMDataObject::BinaryPart();
  
  apc.clear();
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);  
}

void setupCA_CA() {
  correlationMode    = CROSS_AND_AUTO;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 4 basebands with respectively 2, 1, 1 and 1 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  sdPolProducts = ENUMVEC(StokesParameter, "XX XY YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1));

  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1));
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  
  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));
    


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(38,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart(80, ENUMVEC(AxisName, "BAL ANT BAB SPW APC SPP POL"));
  autoDataDesc = SDMDataObject::BinaryPart(32, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL") );
  zeroLagsDesc = SDMDataObject::BinaryPart();
  
  // Build the DataStruct.
  apc =  ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);      
}

// A macro to properly resize a vector a binary data and give some value to its elements.
#define SETATTACH(name, value) name.clear() ; name.resize(dataStruct.name().size(), value)


string uid   = "uid://X1/X1/X1";

void writeFR_CO() {
  // Write binary data for FULL_RESOLUTION x CROSS_ONLY
  try {
    ofstream ofs("fr_co_noopt.dat");
      
    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);
      
    setupFR_CO();
      
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
      
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;
      
      
    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(zeroLags, 100*i);
      autoData.clear();
      if ( i < 2 ) {
	shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000 * i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      shortCrossData,
			      autoData);
      }
      else {
	longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000 * i);	
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      longCrossData,
			      autoData);
      }
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeFR_AO() {
  // Write binary data for FULL_RESOLUTION x AUTO_ONLY
  try {
    ofstream ofs("fr_ao_noopt.dat");
      
    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);
      
    setupFR_AO();
      
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
      
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;
      
      
    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(zeroLags, 100*i);
      shortCrossData.clear();
      SETATTACH(autoData, 10000.*i);
      sdmdow.addIntegration(i+1,
			    time,
			    interval,
			    flags,
			    actualTimes,
			    actualDurations,
			    zeroLags,
			    shortCrossData,
			    autoData);
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeFR_CA() {

  // Write binary data for FULL_RESOLUTION x CROSS_AND_AUTO
  try {
    ofstream ofs("fr_ca_noopt.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupFR_CA();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
    
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(zeroLags, 100*i);
      SETATTACH(autoData, 10000.*i);

      if (i < 1) {
	shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      shortCrossData,
			      autoData);
      }
      else {
	longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      longCrossData,
			      autoData);
      }
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_CO() {
  // Write binary data for CHANNEL_AVERAGE x CROSS_ONLY
  try {
    ofstream ofs("ca_co_noopt.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_CO();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
    
    // And now write 3 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	zeroLags.clear();
	autoData.clear();
	SETATTACH(autoData, 10000.*i);
	
	if (i < 2) {
	  shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   shortCrossData,
				   autoData);
	}
	else {
	  longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   longCrossData,
				   autoData);
	  time += interval;
	}
      }  
    }
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_AO() {
  // Write binary data for CHANNEL_AVERAGE x AUTO_ONLY
  try {
    ofstream ofs("ca_ao_noopt.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_AO();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
    
    // And now write 3 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	zeroLags.clear();
	shortCrossData.clear();
	SETATTACH(autoData, 10000.*i);
	sdmdow.addSubintegration(i+1,
				 j+1,
				 time,
				 interval,
				 flags,
				 actualTimes,
				 actualDurations,
				 zeroLags,
				 shortCrossData,
				 autoData);
	time += interval;
      }
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_CA() {
  // Write binary data for CHANNEL_AVERAGE x CROSS_AND_DATA
  try {
    ofstream ofs("ca_ca_noopt.dat");

    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_CA();

    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  spectralResolutionType,
			  dataStruct);
    
    // And now write 2 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 2; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	zeroLags.clear();
	autoData.clear();
	SETATTACH(autoData, 10000.*i);
	
	if (j < 1) {
	  shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   shortCrossData,
				   autoData);
	}
	else {
	  longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   longCrossData,
				   autoData);
	  time += interval;
	}
      }  
    }
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}


int main(int argC, char* argV[]) {
  writeFR_CO();
  writeFR_AO();
  writeFR_CA();
  writeCA_CO();
  writeCA_AO();
  writeCA_CA();
}

