#include "SDMDataObjectWriter.h"

#ifndef WITHOUT_ACS
#include "almaEnumerations_IFC.h"
#endif

#include "CStokesParameter.h"
#include "CBasebandName.h"

using namespace StokesParameterMod;
using namespace BasebandNameMod;

using namespace asdmbinaries;

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)


int main(int argC, char* argV[]) {

  // Build an SDMDataObjectWriter which will write on stdout.
  string uid   = "uid://X1/X1/X1";
  string title = "ALMA Total Power Data";
  ofstream ofs("bw_ao_noopt.dat");

  SDMDataObjectWriter sdmdow(&ofs, uid, title);

  // Prepare the description of the binary data.
  vector<SDMDataObject::Baseband> basebands;
  vector<SDMDataObject::SpectralWindow> spectralWindows;

  vector<StokesParameter> sdPolProducts; sdPolProducts.push_back(XX);

  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 1, 2) );

  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));
  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));    
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));

  vector<AxisName> flagsAxes    = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  vector<unsigned long>flags;

  vector<AxisName>actualTimesAxes = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  vector<long long>actualTimes;

  vector<AxisName> actualDurationsAxes = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  vector<long long>actualDurations;


#define DATASIZE 800
  vector<float> autoData(DATASIZE);
  for (int i = 0; i < DATASIZE; i++) autoData[i] = (float) i;
  
  sdmdow.tpData(123450000,          // startTime
		"uid://X123/X4/X5", // execBlockUID,
		1,                  // execBlockNum
		10,                 // scanNum
		3,                  // subscanNum
		100,                // number of integrations
		2,                  // number of antennas
		basebands,          // vector of basebands.
		171450000,          // time 
		96000000,           // interval
		flagsAxes,          // axes order for flags
		flags,              // flags values
		actualTimesAxes,    // axes order for actualTimes
		actualTimes,        // actualTimes values
		actualDurationsAxes,// axes order for actualDurations
		actualDurations,    // actualDurations values
		autoDataAxes,       // axes order for autoData
		autoData            // autoData values
		);
  sdmdow.done();
  ofs.close();
}

