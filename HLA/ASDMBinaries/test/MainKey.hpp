#if     !defined(_MAINKEY_HPP)
 
#include <iostream>
#include <vector>
#include <map>

#include <ASDMEntities.h>
#include <Tag.h>
#include <ArrayTime.h>

// #include "Singleton.hpp"
 
using namespace asdm;

class MainKey{
public:
  MainKey(){}
  MainKey(Tag configDescriptionId, Tag fieldId, ArrayTime time):
    configDescriptionId_(configDescriptionId),
    fieldId_(fieldId),
    time_(time)
  {}
  void      setConfigDescriptionId(Tag configDescriptionId){ configDescriptionId_=configDescriptionId; return; }
  void      setFieldId(Tag fieldId)                        { fieldId_ = fieldId;                       return; }
  void      setTime(ArrayTime time)                        { time_ = time;                             return; }
  Tag       configDescriptionId()                          { return configDescriptionId_;                      }
  Tag       fieldId()                                      { return fieldId_;                                  }
  ArrayTime time()                                         { return time_;                                     }
  vector<Tag> antennaIds(){ 
    return Singleton<ASDM>::instance()->getConfigDescription().getRowByKey(configDescriptionId_)->getAntennaId();
  }
  
private:
  Tag       configDescriptionId_;    // field of the structural and relational key 
  Tag       fieldId_;                // field of the relational key 
  ArrayTime time_;                   // field of the structural and relational key 
};


#define _MAINKEY_HPP
#endif
