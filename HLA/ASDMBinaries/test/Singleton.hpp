#if     !defined(_SINGLETON_HPP)

/** Singleton design pattern */
template <typename T>
class Singleton{
public:
  static T* instance();
};
template <typename T>
/** T is a model default constructible */
T* Singleton<T>::instance(){
  static T* instance_;
  if(instance_==0)instance_ = new T;
  return instance_;
}


#define _SINGLETON_HPP
#endif
