#include "SDMDataObjectWriter.h"
#include "SDMDataObjectParser.h"

#ifndef WITHOUT_ACS
#include "almaEnumerations_IFC.h"
#endif

#include "CAtmPhaseCorrection.h"
#include "CCorrelationMode.h"
#include "CSpectralResolutionType.h"
#include "CProcessorType.h"
#include "CStokesParameter.h"
#include "CNetSideband.h"

#include <stdarg.h>

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)



using namespace CorrelationModeMod;
using namespace AtmPhaseCorrectionMod;
using namespace StokesParameterMod;
using namespace ProcessorTypeMod;

using namespace asdmbinaries;

CorrelationMode correlationMode;
SpectralResolutionType spectralResolutionType;

SDMDataObject::DataStruct dataStruct;
vector<AtmPhaseCorrection> apc;

unsigned int flagsSize;
unsigned int actualTimesSize;
unsigned int actualDurationsSize;
unsigned int crossDataSize;
unsigned int autoDataSize;
unsigned int zeroLagsSize;

SDMDataObject::BinaryPart flagsDesc;
SDMDataObject::BinaryPart actualTimesDesc;
SDMDataObject::BinaryPart actualDurationsDesc;
SDMDataObject::ZeroLagsBinaryPart zeroLagsDesc;
SDMDataObject::BinaryPart crossDataDesc;
SDMDataObject::AutoDataBinaryPart autoDataDesc;


vector<SDMDataObject::SpectralWindow> spectralWindows;
vector<StokesParameter> sdPolProducts;
vector<StokesParameter> crossPolProducts;

vector<SDMDataObject::Baseband> basebands;

string title = "ALMA Correlator_B Spectral Data";

void setupFR_CO() {
  correlationMode    = CROSS_ONLY;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 1 Baseband with 2 spectral windows.
  basebands.clear();

  spectralWindows.clear();
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 256,  2, LSB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1024, 2, USB));

  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(16,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart (40960, ENUMVEC(AxisName, "BAL BAB SPW BIN APC SPP POL"));
  autoDataDesc = SDMDataObject::AutoDataBinaryPart();
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart(16, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"), FXF);
  
  apc = ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);

  //
  // Let's define some "has image" associations
  //
  dataStruct.imageSPW(0,0,1);
  dataStruct.imageSPW(0,1,0);
}

void setupFR_AO() {
  correlationMode    = AUTO_ONLY;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 2 basebands with respectively 1 and 2 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 256, 2, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1, USB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));
  


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart();
  autoDataDesc = SDMDataObject::AutoDataBinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL"), true );
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart(16, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"), FXF);
  
  apc.clear();
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);    
}


void setupFR_CA() {
  correlationMode    = CROSS_AND_AUTO;
  spectralResolutionType = FULL_RESOLUTION;

  // Description of 4 basebands with respectively 2, 1, 1 and 1 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 256, 1, USB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 256, 1, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  sdPolProducts = ENUMVEC(StokesParameter, "XX XY YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 512, 1, USB));

  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1024, 1, LSB));
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  
  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 512, 1, DSB));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));
    


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart(40960, ENUMVEC(AxisName, "BAL ANT BAB SPW APC SPP POL"));
  autoDataDesc = SDMDataObject::AutoDataBinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL"), true );
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart(20, ENUMVEC(AxisName, "ANT BAB SPW BIN POL"), FXF);
  
  // Build the DataStruct.
  apc =  ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct(apc,
					 basebands,
					 flagsDesc,
					 actualTimesDesc,
					 actualDurationsDesc,
					 zeroLagsDesc,
					 crossDataDesc,
					 autoDataDesc);

  //
  // Let's define some "has image" associations
  //
  dataStruct.imageSPW(0, 0, 1);
  dataStruct.imageSPW(0, 1, 0);
}



void setupCA_CO() {
  correlationMode    = CROSS_ONLY;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 1 Baseband with 2 spectral windows.
  basebands.clear();

  spectralWindows.clear();
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1, 2, USB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, 1.0, 1, 2, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));

  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(16,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(16, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart (64, ENUMVEC(AxisName, "BAL BAB SPW BIN APC SPP POL"));
  autoDataDesc = SDMDataObject::AutoDataBinaryPart();
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart();
  
  apc = ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);  

  //
  // Let's define some "has image" associations
  //
  dataStruct.imageSPW(0, 0, 1);
  dataStruct.imageSPW(0, 1, 0);
}

void setupCA_AO() {
  correlationMode    = AUTO_ONLY;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 2 basebands with respectively 1 and 2 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 256, 2, USB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1, LSB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 2048, 1, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));
  
  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(12,  ENUMVEC(AxisName, "ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(12, ENUMVEC(AxisName, "ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart();
  autoDataDesc = SDMDataObject::AutoDataBinaryPart(18432, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL"), true);
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart();
  
  apc.clear();
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);  
}

void setupCA_CA() {
  correlationMode    = CROSS_AND_AUTO;
  spectralResolutionType = CHANNEL_AVERAGE;

  // Description of 4 basebands with respectively 2, 1, 1 and 1 spectral windows.
  basebands.clear();

  sdPolProducts = ENUMVEC(StokesParameter, "XX YY");
  crossPolProducts = ENUMVEC(StokesParameter, "XX XY YX YY");

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1, USB));
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1, LSB));
  
  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));

  spectralWindows.clear();
  sdPolProducts = ENUMVEC(StokesParameter, "XX XY YY");
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1, LSB));

  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));

  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1, USB));
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  
  spectralWindows.clear();
  spectralWindows.push_back(SDMDataObject::SpectralWindow(crossPolProducts, sdPolProducts, 1., 1, 1, USB));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));
    


  // Description of binary attachments.
  flagsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualTimesDesc = SDMDataObject::BinaryPart(38,  ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  actualDurationsDesc = SDMDataObject::BinaryPart(38, ENUMVEC(AxisName, "BAL ANT BAB BIN POL"));
  crossDataDesc = SDMDataObject::BinaryPart(80, ENUMVEC(AxisName, "BAL ANT BAB SPW APC SPP POL"));
  autoDataDesc = SDMDataObject::AutoDataBinaryPart(32, ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL"),true );
  zeroLagsDesc = SDMDataObject::ZeroLagsBinaryPart();
  
  // Build the DataStruct.
  apc =  ENUMVEC(AtmPhaseCorrection, "AP_CORRECTED AP_UNCORRECTED");
  dataStruct = SDMDataObject::DataStruct (apc,
					  basebands,
					  flagsDesc,
					  actualTimesDesc,
					  actualDurationsDesc,
					  zeroLagsDesc,
					  crossDataDesc,
					  autoDataDesc);      
  //
  // Let's define some "has image" associations
  //
  dataStruct.imageSPW(0, 0, 1);
  dataStruct.imageSPW(0, 1, 0);

}

// A macro to properly resize a vector a binary data and give some value to its elements.
#define SETATTACH(name, value) name.clear() ; name.resize(dataStruct.name().size(), value)


string uid   = "uid://X1/X1/X1";

void writeFR_CO() {
  // Write binary data for FULL_RESOLUTION x CROSS_ONLY
  try {
    ofstream ofs("fr_co.dat");
      
    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);
      
    setupFR_CO();
      
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
      
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> floatCrossData;
    vector<float> autoData;
      
      
    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(flags, i);
      SETATTACH(actualTimes, 10*i);
      SETATTACH(actualDurations, 10*i);
      SETATTACH(zeroLags, 100*i);
      autoData.clear();
      if ( i == 0 ) {
	shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000 * i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      shortCrossData,
			      autoData);
      }
      else if (i == 1) {
	longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000 * i);	
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      longCrossData,
			      autoData);
      }
      else if ( i == 2 ) {
	floatCrossData.clear(); floatCrossData.resize(dataStruct.crossData().size(), 0.1 * i);	
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      floatCrossData,
			      autoData);

      }
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeFR_AO() {
  // Write binary data for FULL_RESOLUTION x AUTO_ONLY
  try {
    ofstream ofs("fr_ao.dat");
      
    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);
      
    setupFR_AO();
      
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
      
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;
      
      
    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(flags, i);
      SETATTACH(actualTimes, 10*i);
      SETATTACH(actualDurations, 10*i);
      SETATTACH(zeroLags, 100*i);
      shortCrossData.clear();
      SETATTACH(autoData, 10000.*i);
      sdmdow.addIntegration(i+1,
			    time,
			    interval,
			    flags,
			    actualTimes,
			    actualDurations,
			    zeroLags,
			    shortCrossData,
			    autoData);
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeFR_CA() {

  // Write binary data for FULL_RESOLUTION x CROSS_AND_AUTO
  try {
    ofstream ofs("fr_ca.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupFR_CA();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
    
    // And now write 3 integrations.
    unsigned long long time = 139450000 ;
    unsigned long long interval = 32000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      SETATTACH(flags, i);
      SETATTACH(actualTimes, 10*i);
      SETATTACH(actualDurations, 10*i);
      SETATTACH(zeroLags, 100*i);
      SETATTACH(autoData, 10000.*i);

      if (i < 1) {
	shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      shortCrossData,
			      autoData);
      }
      else {
	longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	sdmdow.addIntegration(i+1,
			      time,
			      interval,
			      flags,
			      actualTimes,
			      actualDurations,
			      zeroLags,
			      longCrossData,
			      autoData);
      }
      time += interval;
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_CO() {
  // Write binary data for CHANNEL_AVERAGE x CROSS_ONLY
  try {
    ofstream ofs("ca_co.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_CO();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
    
    // And now write 3 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	SETATTACH(flags, i);
	SETATTACH(actualTimes, 10*i);
	SETATTACH(actualDurations, 10*i);
	zeroLags.clear();
	autoData.clear();
	SETATTACH(autoData, 10000.*i);
	
	if (i < 2) {
	  shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   shortCrossData,
				   autoData);
	}
	else {
	  longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   longCrossData,
				   autoData);
	  time += interval;
	}
      }  
    }
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_AO() {
  // Write binary data for CHANNEL_AVERAGE x AUTO_ONLY
  try {
    ofstream ofs("ca_ao.dat");

    string title = "ALMA Correlator_B Spectral Data";
    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_AO();
    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
    
    // And now write 3 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 3; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	SETATTACH(flags, i);
	SETATTACH(actualTimes, 10*i);
	SETATTACH(actualDurations, 10*i);
	zeroLags.clear();
	shortCrossData.clear();
	SETATTACH(autoData, 10000.*i);
	sdmdow.addSubintegration(i+1,
				 j+1,
				 time,
				 interval,
				 flags,
				 actualTimes,
				 actualDurations,
				 zeroLags,
				 shortCrossData,
				 autoData);
	time += interval;
      }
    }  
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}

void writeCA_CA() {
  // Write binary data for CHANNEL_AVERAGE x CROSS_AND_DATA
  try {
    ofstream ofs("ca_ca.dat");

    SDMDataObjectWriter sdmdow(&ofs, uid, title);

    setupCA_CA();

    
    // Write the global header.
    sdmdow.corrDataHeader(123450000,          // startTime
			  "uid://X123/X4/X5", // execBlockUID,
			  1,                  // execBlockNum
			  10,                 // scanNum
			  3,                  // subscanNum
			  2,                  // numAntenna        
			  correlationMode,
			  OptionalSpectralResolutionType(spectralResolutionType),
			  dataStruct);
    
    // And now write 2 integrations x 2 subintegrations
    unsigned long long time = 131450000 ;
    unsigned long long interval = 16000000 ;
    vector<unsigned long> flags;
    vector<long long> actualTimes;
    vector<long long> actualDurations;
    vector<float> zeroLags;
    vector<short> shortCrossData;
    vector<int> longCrossData;
    vector<float> autoData;

    for (unsigned int i = 0; i < 2; i++) {
      for (unsigned int j = 0; j < 2; j++) {
	SETATTACH(flags, i);
	SETATTACH(actualTimes, 10*i);
	SETATTACH(actualDurations, 10*i);
	zeroLags.clear();
	autoData.clear();
	SETATTACH(autoData, 10000.*i);
	
	if (j < 1) {
	  shortCrossData.clear(); shortCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   shortCrossData,
				   autoData);
	}
	else {
	  longCrossData.clear(); longCrossData.resize(dataStruct.crossData().size(), 1000*i);
	  sdmdow.addSubintegration(i+1,
				   j+1,
				   time,
				   interval,
				   flags,
				   actualTimes,
				   actualDurations,
				   zeroLags,
				   longCrossData,
				   autoData);
	  time += interval;
	}
      }  
    }
    sdmdow.done();
    ofs.close();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
}


int main(int argC, char* argV[]) {
  try {
    writeFR_CO();
    writeFR_AO();
    writeFR_CA();
    writeCA_CO();
    writeCA_AO();
    writeCA_CA();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectParserException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
  catch (...) {
    cout << "Unexpected exception..." << endl;
  }
}

