#if     !defined(_ASDMREADER_HPP)

#include "Singleton.hpp"
#include "ExecBlockDir.hpp"

template<>
ASDM* Singleton<ASDM>::instance(){
  static ASDM* instance_;
  if(instance_==0){
    instance_ = new ASDM;
    try{
      instance_->setFromFile(Singleton<ExecBlockDir>::instance()->dir());
    }
    catch (ConversionException e) {
      cerr << "ERROR " << e.getMessage() << endl;
      return 0;
    }
    catch (...) {
      cerr << "Unexpected exception " << endl;
      return 0;
    }
  }
  return instance_;
}
 

#define _ASDMREADER_HPP
#endif 

//Singleton<ExecBlockDir>::instance()->dir()
