#include <iostream>
#include "SDMDataObjectReader.h"



using namespace asdmbinaries;
using namespace std;

int main (int argC, char* argV[]) {
  if (argC != 2) return (1);


  // Declare an SDMDataObjectReader.
  SDMDataObjectReader reader;
  cout << "Reading to " << argV[1] << endl;
  try {
    // And use it to read the file whose name is passed in argument on the command line.
    const SDMDataObject & sdmDataObject = reader.read(string(argV[1]));

    // Output a string representation of the global header.
    cout << sdmDataObject.toString() << endl;

    if (sdmDataObject.isCorrelation()) {
      // Output a string representation of its integration in the case of correlator data.
      cout << "Interferometric data:" << endl;
      const vector<SDMDataSubset>& sdmDataSubset = sdmDataObject.corrDataSubsets();
      for (unsigned long int i = 0; i < sdmDataSubset.size(); i++) {
	cout << endl;
	cout << sdmDataSubset.at(i).toString();
      }
    }
    else {
      cout << "Total Power data:" << endl;
      const SDMDataSubset& tpDataSubset = sdmDataObject.tpDataSubset();
      cout << tpDataSubset.toString() << endl;

      const float* autoData = 0;
      int numAutoData = tpDataSubset.autoData(autoData);
      cout << "Retrieved " << numAutoData << " float values for autoData at address " << (unsigned long int) autoData << endl;
      if (numAutoData != 0) {
	int k = 0;
	for (int i = 0; i < numAutoData / 10; i++) {
	  for (int j = 0; j < 10; j++)
	    cout << " " << autoData[k++];
	  cout << endl;
	}

	for (int i = 0; i < numAutoData % 10; i++)
	  cout << " " << autoData[k++];
	cout << endl;
	
      }
    }
  }

  catch (SDMDataObjectReaderException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectParserException e) {
    cout << e.getMessage() << endl;
  }
  catch (SDMDataObjectException e) {
    cout << e.getMessage() << endl;
  }
  catch (runtime_error& e) {
    cout << "Runtime error :" << e.what() << endl;
  }
  catch (exception& e) {
    cout << "Exception : " << e.what() << endl;
  }
  catch (...) {
    cout << "Unexpected exception." << endl;
  }


  // Let's try some calls which should trigger exception
  //

  // We are done with the reader
  reader.done();
}
