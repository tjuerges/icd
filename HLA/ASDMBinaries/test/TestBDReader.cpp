#include <iostream>
#include "SDMDataObjectReader.h"
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"


namespace bfs = boost::filesystem;

using namespace asdmbinaries;
using namespace std;



int main (int argC, char* argV[]) {
  if (argC != 2) {
    cout << "Usage : TestBDReader directory-containing-binaries" << endl;
    return (1);
  }

  // Declare an SDMDataObjectReader.
  SDMDataObjectReader reader;

  // Let's explore the directory passed in argV[1]
  string binDir(argV[1]);
  bfs::path binDirPath(binDir);
  cout << "Reading files contained into directory " << binDirPath.string() << endl;

  if (!bfs::exists(binDirPath)) {
    cout << "'" << binDir << "' no such file" << endl;
    exit(1);
  }
  
  if (!bfs::is_directory(binDirPath)) {
    cout << "'" << binDir << "' is not a directory" << endl;
    exit(1);
  }

    
  // Let's iterate over the content of binDirPath
  bfs::directory_iterator end_itr;
  
  for (bfs::directory_iterator itr(binDirPath);
       itr != end_itr;
       ++itr) {
    
    try {
      cout << "Considering file '" << itr->string() << "'" << endl;
      
      // And use it to read the file whose name is passed in argument on the command line.
      const SDMDataObject & sdmDataObject = reader.read(itr->string());
      
      // Output a string representation of the global header.
      cout << sdmDataObject.toString() << endl;
    }
    
    catch (SDMDataObjectReaderException e) {
      cout << e.getMessage() << endl;
    }
    catch (SDMDataObjectParserException e) {
      cout << e.getMessage() << endl;
    }
    catch (SDMDataObjectException e) {
      cout << e.getMessage() << endl;
    }
    catch (runtime_error& e) {
      cout << "Runtime error :" << e.what() << endl;
    }
    catch (exception& e) {
      cout << "Exception : " << e.what() << endl;
    }
    catch (...) {
      cout << "Unexpected exception." << endl;
    }

    
    // And we are done with this usage of the reader, release all the resources which were dynamically 
    // allocated to it.
    reader.done();
  }
}
