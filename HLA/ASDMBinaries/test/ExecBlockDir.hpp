#if     !defined(_EXECBLOCKDIR_HPP)

#include <sys/types.h>
#include <sys/stat.h>     // where the C function mkdir is define 
#include <dirent.h>

#include <iostream>
#include <sstream>
#include <string>


using namespace std;

class ExecBlockDir{
public:
  ExecBlockDir(){}

  void   setDir(string dir){ dir_ = dir;  }

  string newDir(bool verbose){
    static DIR* dirp;
    unsigned int ebNum=1;
    while(true){
      ostringstream dir;
      dir << "ExecBlock" << ebNum;
      dirp = opendir(dir.str().c_str());
      if(dirp){
	ebNum++;
      }else{
	if (mkdir(dir.str().c_str(), S_IRWXU) == -1){
	  cerr<<"Could not create directory "<<dir.str()<<endl;
	  return "";
	}else{
	  if(verbose)cout << dir.str() << " created " << endl;
	  dir_ = dir.str();
	  return dir_;
	}
      }
    }
  }

  bool   find(string execBlockId){
    dir_ = execBlockId;
    static DIR* dirp;
    dirp = opendir(dir_.c_str());
    if(dirp)
      return true;
    else
      cerr<<"Could not find directory "<<dir_<<endl;
    return false;
  }

  bool   find(unsigned int execBlockNum){
    ostringstream oss; oss<<"ExecBlock"<<execBlockNum;
    set<unsigned int> s_eb=execBlockDir();
    set<unsigned int>::iterator itebf=s_eb.find(execBlockNum), itebe=s_eb.end();
    if(itebf==itebe)return false;
    dir_ = oss.str();
    return true;
  }

  void   findLast(){
    set<unsigned int> s_eb=execBlockDir();
    set<unsigned int>::reverse_iterator itebrb=s_eb.rbegin(), itebre=s_eb.rend();
    ostringstream oss;
    if(itebrb==itebre)
      dir_ = "";
    else
      oss<<"ExecBlock"<<*itebrb;
    dir_ = oss.str();
    return;
  }
  string dir()             { return dir_; }

  set<unsigned int> execBlockDir(){
    static unsigned int EBNUMMAX=128;
    static DIR*         dirp;
    set<unsigned int>   s_ebNum;
    unsigned int        ebNum=1;
    while(ebNum<EBNUMMAX){
      ostringstream dir;
      dir << "ExecBlock" << ebNum;
      //cout<<dir.str()<<endl;
      dirp = opendir(dir.str().c_str());
      if(dirp)s_ebNum.insert(ebNum);
      ebNum++;
    }
    cout<<"There are "<<s_ebNum.size()<<" ExecBlock directories"<<endl;
    return s_ebNum;
  }

private:
  string            dir_;
  set<unsigned int> s_ebDir_;
};

#define _EXECBLOCKDIR_HPP
#endif

