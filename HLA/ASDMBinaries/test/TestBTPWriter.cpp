#include "SDMDataObjectWriter.h"

#ifndef WITHOUT_ACS
#include "almaEnumerations_IFC.h"
#endif

#include "CStokesParameter.h"
#include "CBasebandName.h"
#include "CNetSideband.h"

using namespace StokesParameterMod;
using namespace BasebandNameMod;
using namespace NetSidebandMod;

using namespace asdmbinaries;

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)


int main(int argC, char* argV[]) {

  // Build an SDMDataObjectWriter which will write on a file.
  string uid   = "uid://X1/X1/X1";
  string title = "ALMA Total Power Data";
  ofstream ofs("bw_ao.dat");

  SDMDataObjectWriter sdmdow(&ofs, uid, title);

  // Prepare the description of the binary data.
  vector<SDMDataObject::Baseband> basebands;
  vector<SDMDataObject::SpectralWindow> spectralWindows;

  vector<StokesParameter> sdPolProducts; sdPolProducts.push_back(XX);

  spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 1, 2, DSB) );

  basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));
  basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));    
  basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
  basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));

  vector<AxisName>flagsAxes = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  unsigned long flagsArr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
  vector<unsigned long>flags (flagsArr, flagsArr+16);

  vector<AxisName>actualTimesAxes     = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  long long actualTimesArr[] = {10000000, 20000000, 30000000, 40000000, 50000000, 60000000, 70000000, 80000000, 90000000, 100000000, 110000000, 120000000, 130000000, 140000000, 150000000, 160000000};
  vector<long long>actualTimes (actualTimesArr, actualTimesArr+16);

  vector<AxisName> actualDurationsAxes = ENUMVEC(AxisName, "BAL ANT BAB BIN POL");
  long long actualDurationsArr[] = {10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000, 10000000};
  vector<long long>actualDurations (actualDurationsArr, actualDurationsArr+16);


#define DATASIZE 800
  vector<float> autoData(DATASIZE);
  for (int i = 0; i < DATASIZE; i++) autoData[i] = (float) i;
  

  vector<AxisName> autoDataAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
  sdmdow.tpData(123450000,          // startTime
		"uid://X123/X4/X5", // execBlockUID,
		1,                  // execBlockNum
		10,                 // scanNum
		3,                  // subscanNum
		100,                // number of integrations
		2,                  // number of antennas
		basebands,          // vector of basebands.
		171450000,          // time 
		96000000,           // interval
		flagsAxes,          // axes order for flags
		flags,              // flags values
		actualTimesAxes,    // axes order for actualTimes
		actualTimes,        // actualTimes values
		actualDurationsAxes,// axes order for actualDurations
		actualDurations,    // actualDurations values
		autoDataAxes,       // axes order for autoData
		autoData            // autoData values
		);
  sdmdow.done();
  ofs.close();

  /*
   * Test the "short" version of tpData.
   */
  ofstream ofs1("bw_ao_1.dat");
  SDMDataObjectWriter sdmdow1(&ofs1, uid, title);

  sdmdow1.tpData(123450000,          // startTime
		 "uid://X123/X4/X5", // execBlockUID,
		 1,                  // execBlockNum
		 10,                 // scanNum
		 3,                  // subscanNum
		 100,                // number of integrations
		 2,                  // number of antennas
		 basebands,          // vector of basebands.
		 171450000,          // time 
		 96000000,           // interval
		 autoDataAxes,       // axes order for autoData
		 autoData            // autoData values
		 );
  sdmdow1.done();
  ofs1.close();

  // Build an SDMDataObjectWriter which will write on a file.
  ostringstream oss2;
  SDMDataObjectWriter sdmdow2(&oss2, uid, title);
  sdmdow2.tpData(123450000,          // startTime
		 "uid://X123/X4/X5", // execBlockUID,
		 1,                  // execBlockNum
		 10,                 // scanNum
		 3,                  // subscanNum
		 100,                // number of integrations
		 2,                  // number of antennas
		 basebands,          // vector of basebands.
		 171450000,          // time 
		 96000000,           // interval
		 autoDataAxes,       // axes order for autoData
		 autoData            // autoData values
		 );  
  sdmdow2.done();
  
  // Write the content of oss2 in a file.
  ofstream ofs2("bw_ao_from_osstringstream.dat");
  ofs2 << oss2.str();
  ofs2.close();

  // And clean the content of oss2.
  oss2.str("");
}

