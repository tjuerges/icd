#include "SDMDataObjectWriter.h"

#ifndef WITHOUT_ACS
#include "almaEnumerations_IFC.h"
#endif


#include "CNetSideband.h"

using namespace NetSidebandMod;

using namespace asdmbinaries;


int main(int argC, char* argV[]) {


  string uid;
  string title;
 
  string 			execBlockUID;
  unsigned int 			execBlockNum;
  unsigned int 			scanNum;
  unsigned int 			subscanNum;
  unsigned int 			numWVRData;
  unsigned int 			numAntennas;
  unsigned int 			numChannels;
  NetSideband 			netSideband;
  unsigned long long 		time;
  unsigned long long 		interval;  
  vector<float> 		wvrData;
  vector<unsigned long int>   	flags;  
  
  // For this test we shall write
  // the WVR data on a disk file.
  // 
  ofstream 	ofs1("wvr.dat");
  uid 	= "uid://X1/X1/X1";
  title = "ALMA WVR Data";


  try {
    SDMDataObjectWriter 	sdmdow1(&ofs1, uid, title);
    
    execBlockUID = "uid://X1/X1/X1";
    execBlockNum = 1;
    scanNum = 10;
    subscanNum = 7;
    numWVRData = 100;
    numAntennas = 8;
    numChannels = 4;
    netSideband = DSB;
    time = 4753350000000000000LL;
    interval = 50 * 2 * 1000000LL;
    
    wvrData.clear();
    for (unsigned int iWVRData = 0; iWVRData < numWVRData; iWVRData++)
      for (unsigned int iAntenna = 0; iAntenna < numAntennas; iAntenna++)
	for (unsigned int iChannel = 0; iChannel < numChannels; iChannel++)
	  wvrData.push_back(100 * iWVRData + 10 * iAntenna + iChannel);
    flags.clear();
    
    
    sdmdow1.wvrData(execBlockUID,
		    execBlockNum,
		    scanNum,
		    subscanNum,
		    numWVRData,
		    numAntennas,
		    numChannels,
		    netSideband,
		    time,
		    interval,
		    wvrData,
		    flags);
    sdmdow1.done();
  }
  catch (SDMDataObjectWriterException e) {
    cout << e.getMessage() << endl;
  }
  catch (exception e) {
    cout << e.what() << endl;
  }
  
  ofs1.close();
}

