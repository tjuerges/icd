#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>

using namespace std;

#include <ASDMEntities.h>  
#include <Tag.h>
#include <ArrayTime.h>
#include <ArrayTimeInterval.h>

using namespace asdm;  

#include <IllegalAccessException.h>
 
#include "Singleton.hpp"
#include "ExecBlockDir.hpp"

#include "ASDMReader.hpp"

#include "MainKey.hpp"

// Enumerations used for data selection:
#include <AtmPhaseCorrection.h>
#include <CorrelationMode.h>
#include <SpectralResolutionType.h>
#include <TimeSampling.h>
 
#include "SDMBinData.h"

using namespace sdmbin;

void usage(void)
{
  std::cout <<"Usage: testdam [options]"                                                        <<std::endl;
  std::cout <<"Examples:"                                                                       <<std::endl;
  std::cout <<" testdam (take the ExecBlock unfiltered of highest number in current directory.)"<<std::endl;
  std::cout <<" testdam -eb 3 (unfiltered ExecBlock number 3)"                                  <<std::endl;
  std::cout <<" testdam -r 2  (consider only the SDM row index 2 of the most recent ExecBlock) "<<std::endl;
  std::cout <<" testdam -d uid___X1eb_Xa30_X1 select SDM ExecBlock named uid___X1eb_Xa30_X1"    <<std::endl;
  std::cout <<" testdam -cm AUTO_ONLY "                                                         <<std::endl;
  std::cout <<" testdam -cm AUTO_ONLY CROSS_AND_AUTO"                                           <<std::endl;
  std::cout <<" testdam -sr FULL_RESOLUTION CHANNEL_AVERAGE"                                    <<std::endl;
  std::cout <<" testdam -sr FULL_RESOLUTION -cm CROSS_AND_AUTO -ts INTEG"                       <<std::endl;
  std::cout <<" testdam -sr CHANNEL_AVERAGE -cm CROSS_AND_AUTO -qcm CROSS_ONLY -qapc AP_UNCOR"  <<std::endl;
  std::cout<<"Options"                                                                          <<std::endl;
  std::cout<<"   -h    This help"                                                               <<std::endl;
  std::cout<<"   -v    Verbose"                                                                 <<std::endl;
  std::cout<<"   -d    S  To select an ExecBlock by its Id (the name of a directory)"           <<std::endl;
  std::cout<<"   -r    I  To select a specific SDM Main table row of indec I"                   <<std::endl;
  std::cout<<"   -eb   N  To select a specific ExecBlock number N "                             <<std::endl;
  std::cout<<"         Default is the ExecBlock of highest number found in current directory"   <<std::endl;
  std::cout<<"   -cm   CROSS_ONLY|AUTO_ONLY|CROSS_AND_AUTO  A list of correlation modes."       <<std::endl;
  std::cout<<"         Skip the Main table rows which do not correspond to one of these "       <<std::endl;
  std::cout<<"         in that list."                                                           <<std::endl;
  std::cout<<"         Default is no cm filtering, i.e. consider the rows whatever the "        <<std::endl;
  std::cout<<"         correlation mode used."                                                  <<std::endl;
  std::cout<<"   -ts   SUBINTEGRATION|INTEGRATION A list of time sampling qualifiers"           <<std::endl;
  std::cout<<"         Default: any kind of time sampling "                                     <<std::endl;
  std::cout<<"   -sr   FULL_RESOLUTION|CHANNEL_AVERAGE|BASEBAND_WIDE A list of spectral"        <<std::endl;
  std::cout<<"         resolution types. Skip the Main table rows which do not correspond to "  <<std::endl;
  std::cout<<"         one of these in that list."                                              <<std::endl;
  std::cout<<"         Default is no sr filtering, i.e. consider the rows whatever the "        <<std::endl;
  std::cout<<"         resolution type. "                                                       <<std::endl;
  std::cout<<"   -qcm  CROSS_ONLY|AUTO_ONLY|CROSS_AND_AUTO Select one amongst these categories" <<std::endl;
  std::cout<<"         Default is CROSS_AND_AUTO, i.e. access all. For example with CROSS_ONLY" <<std::endl;
  std::cout<<"         only the visibilities are retieve. If both auto-correlations and "       <<std::endl;
  std::cout<<"         cross-correlations are retrieved then all the auto-correlations will "   <<std::endl;
  std::cout<<"         be transformed into complex data values in any cases. If -qcm equals "   <<std::endl;
  std::cout<<"         AUTO_ONLY and there is no polarization cross-product (eg no XY) then"    <<std::endl;
  std::cout<<"         the data value will be real numbers (classic single-dish use-case)"      <<std::endl;
  std::cout<<"   -qapc AP_UNCORRECTED|AP_CORRECTED|AP_MIXED List of these qualifiers."          <<std::endl;
  std::cout<<"         Default is do not subset, i.e. access all. E.g. for AP_UNCORRECTED only" <<std::endl;
  std::cout<<"         data uncorrected fo the atmospheric phases will be retrieved."           <<std::endl;
}

int main(int argc, char* argv[]){

  bool             verbose=false;
  string           ebId;
  vector<MainRow*> v_mainRow;

  // user filtering parameters:
  unsigned int                    ebNum=999999;
  unsigned int                    sdmRowIndex=999999;
  EnumSet<CorrelationMode>        es_cm_u;
  EnumSet<SpectralResolutionType> es_sr_u;
  EnumSet<TimeSampling>           es_ts_u;
  Enum<CorrelationMode>           e_qcm_u;
  EnumSet<AtmPhaseCorrection>     es_qapc_u;

  if(argc<2){ 
    Singleton<ExecBlockDir>::instance()->findLast();
    if(Singleton<ExecBlockDir>::instance()->dir().length()==0){
      cout<<"FATAL: no ExecBlock sub-directory in the current directory"<<endl;
      return 0;
    }
  }else{
//     cout<<"argc="<<argc<<endl;
    bool brkloop =false;
    int  i=1;
    for(;i<argc && !brkloop;){
      
      switch(argv[i][0]){
      case '-'://option
	{
	  std::string opt(argv[i]+1);
// 	  cout<<"opt="<<opt<<endl;
	  if (opt=="h"){
	    usage();
	    exit(0);
	  }else if(opt=="v"){
	    verbose=true;               
	    i++;
	  }else if(opt=="r"){
	    sdmRowIndex=atoi(argv[++i]);
	    i++;
	  }else if(opt=="d"){
	    ebId = argv[++i];
	    if(!Singleton<ExecBlockDir>::instance()->find(ebId)){
	      cout<<"FATAL: ExecBlock directory with the id "<<ebId<<" not found"<<endl;
	      exit(1);
	    }
	    i++;
	  }else if(opt=="eb"){
	    ebNum=atoi(argv[++i]);
	    if(!Singleton<ExecBlockDir>::instance()->find(ebNum)){
	      cout<<"FATAL: ExecBlock directory ExecBlock"<<ebNum<<" not found"<<endl;
	      exit(1);
	    }
	    i++;
	  }else if(opt=="cm"){
	    i++;
	    bool brkloop2 =false;
	    while(!brkloop2){
	      string s(argv[i]);
// 	      cout<<s<<endl;
	      switch(argv[i][0]){
	      case 'A':
		{ 
		  es_cm_u.set(AUTO_ONLY);
		  i++;
		  break;
		}
	      case 'C':
		{ 
		  if(s.find("_AND",0)!=string::npos)
		    es_cm_u.set(CROSS_AND_AUTO);
		  else
		    es_cm_u.set(CROSS_ONLY);
		  i++;
		  break;
		}
	      default:
		brkloop2=true;
	      }
	      if(i==argc)brkloop2=true;
	    }
	  }else if(opt=="sr"){
	    i++;
	    bool brkloop2 =false;
	    while(!brkloop2){
	      string s(argv[i]);
// 	      cout<<s<<endl;
	      switch(argv[i][0]){
	      case 'F':
		{ 
		  es_sr_u.set(FULL_RESOLUTION);
		  i++;     cout<<i<<endl;
		  break;
		}
	      case 'C':
		{ 
		  es_sr_u.set(CHANNEL_AVERAGE);
		  i++;
		  break;
		}
	      case 'B':
		{ 
		  es_sr_u.set(BASEBAND_WIDE);
		  i++;
		  break;
		}
	      default:
		brkloop2=true;
	      }
	      if(i==argc)brkloop2=true;
	    }
	  }else if(opt=="ts"){
	    i++;
	    bool brkloop2 =false;
	    while(!brkloop2){
	      string s(argv[i]);
// 	      cout<<s<<endl;
	      switch(argv[i][0]){
	      case 'S':
		{ 
		  es_ts_u.set(SUBINTEGRATION);
		  i++;     cout<<i<<endl;
		  break;
		}
	      case 'I':
		{ 
		  es_ts_u.set(INTEGRATION);
		  i++;
		  break;
		}
	      default:
		brkloop2=true;
	      }
	      if(i==argc)brkloop2=true;
	    }
	  }else if(opt=="qcm"){
	    i++;
	    unsigned int k=0;
	    bool brkloop2 =false;
	    while(!brkloop2){
	      string s(argv[i]);
// 	      cout<<s<<endl;
	      switch(argv[i][0]){
	      case 'A':
		{ 
		  e_qcm_u.set(AUTO_ONLY);
		  i++;
		  k++;
		  break;
		}
	      case 'C':
		{ 
		  if(s.find("_AND",0)!=string::npos)
		    e_qcm_u.set(CROSS_AND_AUTO);
		  else
		    e_qcm_u.set(CROSS_ONLY);
		  i++;
		  k++;
		  break;
		}
	      default:
		brkloop2=true;
	      }
	      if(i==argc)brkloop2=true;
	      if(k>1){
		cerr<<"option -qcm: only one value must be given,\n"<<endl;
		cerr<<"  AUTO_ONLY or CROSS_ONLY or CROSS_AND_AUTO"<<endl;
		exit(3);
	      }
	    }
	  }else if(opt=="qapc"){
	    i++;
	    bool brkloop2 =false;
	    while(!brkloop2){
	      string s(argv[i]);
// 	      cout<<s<<endl;
	      if(s.find("_U",0)!=string::npos)
		{ 
		  es_qapc_u.set(AP_UNCORRECTED);
		  i++;
		}
	      else if(s.find("_C",0)!=string::npos)
		{ 
		  es_qapc_u.set(AP_CORRECTED);
		  i++;
		}
	      else if(s.find("_M",0)!=string::npos)
		{ 
		  es_qapc_u.set(AP_MIXED);
		  i++;
		}
	      else
		{
		brkloop2=true;
		}
	      if(i==argc)brkloop2=true;
	    }
	  }else{
	    std::cerr<<"Unknown option "<<argv[i]<<std::endl;
	    usage();
	    exit(2);
	  }
	  break;
	}
      default:
	brkloop = true;
	//end of options
	break;
      }
    }
  }
  if(verbose){
    cout<<"Input filters:"<<endl;
    cout<<"=============="<<endl;
    if(ebNum==999999)
      if(ebId.length())
	cout<<"ExecBlock:                             "<<ebId<<endl;
      else
	cout<<"ExecBlock: the most recent.            "<<endl;
    else
      cout<<"ExecBlock:                             "<<ebNum<<endl;
    if(sdmRowIndex!=999999)
      cout<<"Select only the SDM row with the index "<<sdmRowIndex<<endl;
    cout<<"BLOBs for correlation mode(s):         "<<es_cm_u.str()<<endl;
    cout<<"BLOBs for spectral resolution type(s): "<<es_sr_u.str()<<endl;
    cout<<"BLOBs for time sampling(s):            "<<es_ts_u.str()<<endl;
    cout<<"Subset of data within the BLOBs for:   "<<e_qcm_u.str()<<endl;
    cout<<"Subset of data within the BLOBs for:   "<<es_qapc_u.str()<<endl;
    cout<<endl;
  }
  if(ebId.length()){
    cout<<"Get the Main table in directory "
      <<Singleton<ExecBlockDir>::instance()->dir()<<endl; 
  }else if(ebNum==999999){
    cout<<"INFORM: About to use the most recent ExecBlock."<<endl;
    Singleton<ExecBlockDir>::instance()->findLast();
    if(Singleton<ExecBlockDir>::instance()->dir().length()==0){
      cout<<"FATAL: no ExecBlock sub-directory in the current directory"<<endl;
      return 0;
    }else
      cout<<"Get the Main table in directory "
	  <<Singleton<ExecBlockDir>::instance()->dir()<<endl; 
  }

  v_mainRow = Singleton<ASDM>::instance()->getMain().get();
  cout<<"The SDM main table has "<<v_mainRow.size()<<" rows"<<endl;

  // Default: all SDM Main table row indices:
  unsigned int nb=0;
  unsigned int ne=v_mainRow.size();
  if(sdmRowIndex!=999999){
    if(sdmRowIndex>=v_mainRow.size()){
      cerr<<"The selected sdm row index value, "<<sdmRowIndex
	  <<", exceeds the maximum index which is "<<v_mainRow.size()-1<<endl;
      exit(4);
    }
    nb = sdmRowIndex;
    ne = sdmRowIndex+1;
  }


  // Default filters:
  EnumSet<CorrelationMode>         es_cm;        es_cm.flip();
  EnumSet<TimeSampling>            es_ts;        es_ts.flip();
  EnumSet<SpectralResolutionType>  es_srt;       es_srt.fromString("FULL_RESOLUTION CHANNEL_AVERAGE", true);
//  EnumSet<SpectralResolutionType> es_srt;       es_srt.flip();  // BASEBAND_WIDE not yet supported
  Enum<CorrelationMode>            e_query_cm;   e_query_cm=CROSS_AND_AUTO; 
  EnumSet<AtmPhaseCorrection>      es_query_apc; es_query_apc.flip();


  // Set user filters:
  if(es_cm_u.count())   es_cm        = es_cm_u;
  if(es_sr_u.count())   es_srt       = es_sr_u;
  if(es_ts_u.count())   es_ts        = es_ts_u;
  if(e_qcm_u.count())   e_query_cm   = e_qcm_u; 
  if(es_qapc_u.count()) es_query_apc = es_qapc_u;


  SDMBinData sdmBinData( Singleton<ASDM>::instance(), 
			 Singleton<ExecBlockDir>::instance()->dir() );

  // Data selection (pour le moment on ignore les BASEBAND_WIDE):

  cout<<"Data selection:"<<endl;
  cout<<"---------------"<<endl;
  if(sdmRowIndex!=999999)
    cout<<"Only the SDM row with the index "<<sdmRowIndex<<" is considered"<<endl;
  cout<<es_cm.str()<<endl;
  cout<<es_srt.str()<<endl;
  cout<<es_ts.str()<<endl;
  cout<<endl;

  cout<<"Data subset query:"<<endl;
  cout<<"------------------"<<endl;
  cout<< e_query_cm.str()   <<endl;
  cout<< es_query_apc.str() <<endl;
  cout                      <<endl;


  // Define the SDM Main table subset of rows to be accepted
  sdmBinData.select( es_cm, es_srt, es_ts);   

  // Define the subset of data to be extracted from the BLOBs
  sdmBinData.selectDataSubset(e_query_cm, es_query_apc);

  // Define if it is required to create a DATA or a FLOAT_DATA column in the MS MAIN table
  bool createDataCol = sdmBinData.isComplexData();
  if(verbose){
    if(createDataCol)
      cout<<"INFORM: It is required to create a DATA column to host all the data as complex data"<<endl;
    else
      cout<<"INFORM: It is required to create a FLOAT-DATA column to host the data, these being real values"<<endl;
  }

  const VMSData* vmsDataPtr;

  unsigned int msRowIndex=0;
  unsigned int numMainRows=0;
  unsigned int numMSMainRow=0;
  unsigned int numValues;

  map<AtmPhaseCorrection,float*>::const_iterator itapc,itapcb,itapce; // for every apc there is an array of data


  for(unsigned int n=nb; n!=ne; ++n){                                                        // for the SDM Main table row(s) 

    cout<<"Main table row index: "<<n<<"  ("<<v_mainRow[n]->getTime().toString()<<")"<<endl;

    if(sdmBinData.acceptMainRow(v_mainRow[n])){                                                          // if that row pass trough the filter

      try{
	vmsDataPtr = sdmBinData.getDataCols();

	if(vmsDataPtr){
	  if(vmsDataPtr->v_time.size()){
	    numMSMainRow += vmsDataPtr->v_time.size();
	    cout<<"This SDM row with index "<<n<<" will produce "<<vmsDataPtr->v_time.size()<<" MS Maintable rows"<<endl;
	  
	    /* section to show all the data value and the number of data values: */
	    if(verbose)
	      {
		for(unsigned int  nt_nbl_ndd=0; nt_nbl_ndd<vmsDataPtr->vv_dataShape.size(); nt_nbl_ndd++){   // for every MS Main row

		  cout<<"MS row #"<<msRowIndex++<<" "
		      <<" TIME "<<(long long int) vmsDataPtr->v_time[nt_nbl_ndd]
		      <<" INTERVAL "<<vmsDataPtr->v_interval[nt_nbl_ndd]
		      <<" FIELD_ID "<<vmsDataPtr->v_fieldId[nt_nbl_ndd]
		      <<" ANTENNA1 "<<vmsDataPtr->v_antennaId1[nt_nbl_ndd]
		      <<" ANTENNA2 "<<vmsDataPtr->v_antennaId2[nt_nbl_ndd]
		      <<" DATA_DESCRIPTION_ID "<<vmsDataPtr->v_dataDescId[nt_nbl_ndd]
		      <<endl;

		  cout<<"v_numData["<<nt_nbl_ndd<<"]="<<vmsDataPtr->v_numData[nt_nbl_ndd]<<endl;

		  numValues=1;

		  cout<<"shape["<<nt_nbl_ndd<<"]=";
		  for(unsigned int nsp_npp=0; nsp_npp<vmsDataPtr->vv_dataShape[nt_nbl_ndd].size(); nsp_npp++){  // for every axis of the leaf      
		    numValues *= vmsDataPtr->vv_dataShape[nt_nbl_ndd][nsp_npp];
		    cout<<vmsDataPtr->vv_dataShape[nt_nbl_ndd][nsp_npp]<<" ";
		  }
		  if(createDataCol)numValues *= 2;   // a complex has 2 values
		  cout<<" ==> numValues="<<numValues<<" (numPolProd*numSpectralPoint*numApc=1)"<<endl;          // number of values in the leaf

		  itapcb = vmsDataPtr->v_m_data[nt_nbl_ndd].begin();
		  itapce = vmsDataPtr->v_m_data[nt_nbl_ndd].end();
		  for(itapc=itapcb; itapc!=itapce; ++itapc){                           // for every apc for this MS row (1 MS Main col per apc)
		    cout<<"Results for "<<Enum<AtmPhaseCorrection>(itapc->first).str()<<endl;  

		    for(unsigned int nv=0; nv<numValues; nv++){                                               // display the individual values
		      cout<<" v_data["<<nt_nbl_ndd<<","<<nv<<"]"<<(int)itapc->second[nv];
		      if(createDataCol)cout<<","<<(int)itapc->second[++nv];
		      cout<<endl;
		    }
		    cout<<endl;
		  }
		}
	      }
	  }else{
	    cout<<"WARNING: SDM row index "<<n<<" does not produce any  MS Maintable rows"<<endl;
	  }
	}else{
	  cout<<"vmsDataPtr=NULL"<<endl;
	}
      }
      catch (Error) {
	cerr<<"DAM failed for this SDM row"<<endl;
      }
      numMainRows++;

    }else{

       cout<<"row index "<<n<<": "
	   <<sdmBinData.reasonToReject(v_mainRow[n])
	   <<endl;

    }
    cout<<endl;
//     break;
  }
  cout<<"Number of SDM MainTable rows scanned:        "<< v_mainRow.size() <<endl;
  cout<<"Number of SDM MainTable rows processed:      "<< numMainRows      <<endl;
  cout<<"Number of MS MainTable rows to be generated: "<< numMSMainRow     <<endl;
  return 0;
}
    
