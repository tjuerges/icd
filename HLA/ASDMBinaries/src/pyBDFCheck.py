#! /bin/env python
import string
import sys
import os
import glob
from optparse import OptionParser

import PyBDFBrowser

version = "1.0"
title = "pyBDFCheck"

BDFVersion = "BDF V2"

#
# The check method.
#
def BDFCheck(filename):
    print "Checking ", filename, "...",

    ok = True
    args = PyBDFBrowser.StringVector(2)
    args[0] = "read"
    args[1] = filename
    try:
        result = browser.execute(args[0], args)
    except PyBDFBrowser.BDFBrowserException, e:
        print "%(filename)s does not seem to be conform with %(BDFVersion)s. Error message : '%(message)s'" % {'filename': filename, 'BDFVersion': BDFVersion, 'message' : e.getMessage() }
        ok = False

    if ok : 
        print "ok"
    
#
# Let's process the command line.
#
def main():
    usage = """
    usage: %prog path

'path' is a path to one file or more files (wildcard characters allowed).
"""
    
    parser = OptionParser(usage)
    (options, files) = parser.parse_args()
    if len(files) == 0:
        parser.error("incorrect number of arguments")

    #
    # Prepare a PyBDFBrowser command interface.
    #
    global browser
    browser = PyBDFBrowser.BDFBrowserOps()

    #
    # And check all the files
    #
    for filename in files:
        BDFCheck(filename)

if __name__ == "__main__":
    main()

    
