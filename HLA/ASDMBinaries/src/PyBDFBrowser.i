%module PyBDFBrowser

%include "std_vector.i"
%include "std_string.i"

%{
#include "BDFBrowser.h"
%}


%template(StringVector) std::vector<string>;

class BDFBrowserException {    
  public:
    /**
     * An empty contructor.
     */
    BDFBrowserException();

    /**
     * A constructor with a message associated with the exception.
     * @param m a string containing the message.
     */
    BDFBrowserException(std::string m);
    
    /**
     * The destructor.
     */
    virtual ~BDFBrowserException(); 

    /**
     * Returns the message associated to this exception.
     * @return a string.
     */
    std::string getMessage() const ;
};


class BDFBrowserOps {
public:
  BDFBrowserOps();
  virtual ~BDFBrowserOps();
  std::string execute(std::string op , std::vector<string> & args) throw (BDFBrowserException);
};
