#! /bin/env python
import string
import sys
from Tkinter import *
import tkFileDialog
import tkMessageBox

from ScrolledText import ScrolledText

import PyBDFBrowser

version  = "1.0"
title = "pyBDFBrowser"

def clone():
    topLevel = Toplevel(root)
    topLevel.config()
    topLevel.title(title)
    app = App(topLevel)


def apropos():
    tkMessageBox.showinfo(
                    title,
                    "A very basic BDF browser.\nRelease:  $Id$")
    
                        
class App:
    #
    # Displays the global header.
    #
    def headerBDF(self):
        args = PyBDFBrowser.StringVector(1)
        args[0] = "header"
        try:
            result = self.browser_.execute(args[0], args)
        except PyBDFBrowser.BDFBrowserException, e:
                tkMessageBox.showerror(
                    "header",
                    "Cannot display header. Error message :'%(message)s' " % {'file' : file, 'message' : e.getMessage()})
                return
        self.header_.config(state="normal")
        self.header_.delete("0.0", END)       
        self.header_.insert(END,result)
        self.header_.config(state="disabled")
        return

    def incNumData(self):
        if self.numdata_ < sys.maxint:
            self.numdata_ *= 2
            self.numData(self.numdata_)
            self.subsetBDF()

    def decNumData(self):
        if self.numdata_ > 2:
            self.numdata_ /= 2
            self.numData(self.numdata_)
            self.subsetBDF()
            
    def maxNumData(self):
        self.numdata_ = sys.maxint
        self.numData(self.numdata_)
        self.subsetBDF()
        
    #
    # Set the number of values displayed for each attachment.
    #
    def numData(self, value):
        args = PyBDFBrowser.StringVector(2)
        args[0] = "numdata"
        args[1] = str(value)
        try:
            result = self.browser_.execute(args[0], args)
        except  PyBDFBrowser.BDFBrowserException, e:
            PyBDFBrowser.showerror("numdata",e.getMessage())
        return
        
    #
    # Displays the next subset header and a resume of its binary data.
    #
    def nextBDF(self):
        self.gotoMenuIndex_ = (self.gotoMenuIndex_ + 1) % len(self.ppaths_)
        self.gotoMenu_.activate(self.gotoMenuIndex_)
        self.goto()
        return

    #
    # Displays the prev subset header and a digest of its binary data.
    #
    def prevBDF(self):
        self.gotoMenuIndex_ = self.gotoMenuIndex_ - 1
        if self.gotoMenuIndex_ < 0:
            self.gotoMenuIndex_ = len(self.ppaths_) - 1
        self.gotoMenu_.activate(self.gotoMenuIndex_)
        self.goto()
        return            
    #
    # Displays the current subset header and a digest of its binary data.
    #
    def subsetBDF(self):
        args = PyBDFBrowser.StringVector(1)
        args[0] = "subset"
        try:
            result = self.browser_.execute(args[0], args)
        except PyBDFBrowser.BDFBrowserException, e:
                tkMessageBox.showerror(
                    "subset",
                    e.getMessage())
                return
        self.subset_.config(state="normal")
        self.subset_.delete("0.0", END)
        self.subset_.insert(END,result)
        self.subset_.config(state="disabled")        
        return        

    def gotoMenuPopup(self, event):
        self.gotoMenu_.post(self.subsetLabel_.winfo_rootx(),
                            self.subsetLabel_.winfo_rooty() + self.subsetLabel_.winfo_height())
        

    def readBDF(self, file=None):
        if (file == None):
            file = tkFileDialog.askopenfilename(parent=self.master_,title='Choose a BDF file')
        if file != None:
            args = PyBDFBrowser.StringVector(2)
            args[0] = "read"
            args[1] = file
            try:
                result = self.browser_.execute(args[0], args)
            except PyBDFBrowser.BDFBrowserException, e:
                tkMessageBox.showerror(
                    "Read file",
                    "Cannot read this file\n(%(file)s). Error message :'%(message)s' " % {'file' : file, 'message' : e.getMessage()})
                return
            self.status_.config(text=file)
            self.headerBDF();
            self.numData(self.numdata_)
            self.subsetBDF();

            args[0] = "ppaths"
            try:
                result = self.browser_.execute(args[0], args)
                self.ppaths_=string.split(result, "\n")
                self.gotoMenu_.delete(0, self.gotoMenu_.index(END))
                for ppath in self.ppaths_:
                    if ppath != '':
                        self.gotoMenu_.add_command(label=ppath, command=self.goto)
                self.subsetButton_.config(text=self.ppaths_[0])
                self.gotoMenuIndex_ = 0
                            
            except PyBDFBrowser.BDFBrowserException, e:
                tkMessageBox.showerror(
                    "Read file",
                    "Cannot read this file\n(%(file)s). Error message :'%(message)s' " % {'file' : file, 'message' : e.getMessage()})
                return
        return

    def goto(self):
        args = PyBDFBrowser.StringVector(2)
        args[0] = "subset"
        args[1] = self.gotoMenu_.entrycget(self.gotoMenu_.index(ACTIVE), "label")
        try:
            result = self.browser_.execute(args[0], args)
        except PyBDFBrowser.BDFBrowserException, e:
            tkMessageBox.showerror(
                "Subset",
                "Cannot display subset with projectpath '%(projectPath)s'. Error message :'%(message)s' " % {'projectPath' : projectPath, 'message' : e.getMessage()})
            return
        self.subset_.config(state="normal")
        self.subset_.delete("0.0", END)
        self.subset_.insert(END,result)
        self.subset_.config(state="disabled")

        self.subsetButton_.config(text=self.gotoMenu_.entrycget(self.gotoMenu_.index(ACTIVE), "label"))
        self.gotoMenuIndex_ = self.gotoMenu_.index(ACTIVE)
        return        

    def close(self):
        self.master_.destroy()
        
    def __init__(self, master):
        self.master_  = master
        self.browser_ = PyBDFBrowser.BDFBrowserOps()

        # display 50 values in each attachment by default. 
        self.numdata_ = 50

        menubar = Menu(self.master_)

        # create a pulldown menu, and add it to the menu bar
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.readBDF)
        filemenu.add_command(label="New browser", command=clone)
        filemenu.add_separator()
        filemenu.add_command(label="Close", command=self.close)
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=apropos)
        menubar.add_cascade(label="Help", menu=helpmenu)

        self.status_ = Label(self.master_, text="File:", bd=1, relief=SUNKEN, anchor=W, width=100)
        self.status_.pack(side=BOTTOM, fill=X)

        # display the menu
        self.master_.config(menu=menubar, width=500)


        # A Paned window as a global frame work
        self.paned_ = PanedWindow(self.master_, orient=VERTICAL)
        self.paned_.pack(fill=BOTH, expand=1)
        

        # Prepare the header display area
        self.headerFrame_=Frame(self.paned_,bd=4)
        label=Label(self.headerFrame_,text="Header")
        label.pack(side=TOP, fill=X)
        self.header_ = ScrolledText(self.headerFrame_, bd=1,  state="disabled", background="white")
        self.header_.pack(side=TOP, fill=BOTH, expand=1)
        self.paned_.add(self.headerFrame_)

        
        # Prepare the subset display area
        self.subsetFrame_  = Frame(self.paned_,bd=4)
        f                  = Frame(self.subsetFrame_)
        self.subsetLabel_  = Label(f,text="Subset:", anchor=CENTER)
        self.subsetButton_ = Menubutton(f, text="------", relief=SUNKEN)
        self.gotoMenu_     = Menu(self.subsetButton_, tearoff = 0)
        self.subsetButton_["menu"] = self.gotoMenu_
        self.subsetLabel_.pack(side=LEFT)
        self.subsetButton_.pack(side=LEFT)
        f.pack(side=TOP)
        
        self.subset_ = ScrolledText(self.subsetFrame_, bd=1,  state="disabled", background="white", height=10)
        self.subset_.pack(side=TOP, fill=BOTH, expand=1)
        
        # Prepare the next-prev button area
        self.nextPrevFrame_=Frame(self.subsetFrame_, bd=4)
        self.prevButton_    = Button(self.nextPrevFrame_, text="prev", command=self.prevBDF)
        self.prevButton_.pack(side=LEFT)
        self.nextButton_    = Button(self.nextPrevFrame_, text="next", command=self.nextBDF)
        self.nextButton_.pack(side=LEFT)
        self.moreButton_    = Button(self.nextPrevFrame_, text="more values", command=self.incNumData)
        self.moreButton_.pack(side=LEFT)
        self.lessButton_    = Button(self.nextPrevFrame_, text="less values", command=self.decNumData)
        self.lessButton_.pack(side=LEFT)
        self.allButton_    = Button(self.nextPrevFrame_, text="all values", command=self.maxNumData)
        self.allButton_.pack(side=LEFT)        
        self.nextPrevFrame_.pack(side=TOP,fill=X)
        
        self.paned_.add(self.subsetFrame_)
        
root = Tk()
root.title(title)
app = App(root)

#
if len(sys.argv) > 1:
    app.readBDF(sys.argv[1])

root.mainloop()
