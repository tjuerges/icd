#include <utility>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>

#include <boost/algorithm/string.hpp>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>


#include "boost/filesystem/operations.hpp"
#include <boost/regex.hpp>

using namespace std;
using namespace boost;

class SDMDataObjectStreamReaderException {
public:
  SDMDataObjectStreamReaderException();
  SDMDataObjectStreamReaderException(const string& message);
  
  const string& getMessage();
  
private:
  static const string name;
  string message;
};

const string SDMDataObjectStreamReaderException::name="SDMDataObjectStreamReaderException: ";
 
SDMDataObjectStreamReaderException::SDMDataObjectStreamReaderException():message(name) {;}

SDMDataObjectStreamReaderException::SDMDataObjectStreamReaderException(const string & message): message(name + message) {;}   

const string& SDMDataObjectStreamReaderException::getMessage() { return message; }

class SDMDataObjectStreamReader {
public:
  SDMDataObjectStreamReader() ;
  virtual ~SDMDataObjectStreamReader() ;
  void open(const string& path);
  bool hasData();
  void getData();

private:
  // Private variables
  string	path;
  ifstream	f;
  string	currentLine;
  string	boundary_1;
  string        boundary_2;
  
  // Private methods
  string		nextLine();
  pair<string, string>	headerField2Pair(const string& hf);
  pair<string, string>	requireHeaderField(const string& hf);
  string		requireMIMEHeader();
  string		requireBoundaryInCT(const string& ctValue);
  void			skipUntilEmptyLine(int maxSkips);  
  string		accumulateUntilBoundary(const string& boundary, int maxLines);
  void			requireBoundary(const string&, int maxLines);
  int64_t		lookForBinaryPartSize(xmlDocPtr parent, const string& partName);
  string                requireCrossDataType(xmlNode* parent);
  map<string, int64_t>  binaryPartSize;
  void                  requireSDMDataHeaderMIMEPart();
  void                  requireSDMDataSubsetMIMEPart();

  char*                 actualTimesBuffer;
  char*                 actualDurationsBuffer;
  char*                 autoDataBuffer;
  char*                 crossDataBuffer;
  char*                 flagsBuffer;
  char*                 zeroLagsBuffer;
  
};

SDMDataObjectStreamReader::SDMDataObjectStreamReader(): boundary_1("") {;}

SDMDataObjectStreamReader::~SDMDataObjectStreamReader() {
  if (f.is_open()) f.close();
}

void SDMDataObjectStreamReader::open(const string& path) {
  this->path = path;
  f.open(path.c_str(), ifstream::in);
  if (f.fail())
    throw SDMDataObjectStreamReaderException("could not open '" + path + "'.");

  boundary_1 = requireMIMEHeader();
  cout << "Boundary = " << boundary_1 << endl;
}

bool SDMDataObjectStreamReader::hasData() {
  return (currentLine.compare("--"+boundary_1+"--")!=0);
}
 
void SDMDataObjectStreamReader::getData() {
  requireSDMDataHeaderMIMEPart();
  getline(f, currentLine);
}

string SDMDataObjectStreamReader::nextLine() {
  getline(f, currentLine);
  if (f.fail())
    throw SDMDataObjectStreamReaderException("could not read a line in '" + path + "'.");
  cout << "nexLine has read '" << currentLine << "'" << endl;
  return currentLine;
}

pair<string, string> SDMDataObjectStreamReader::headerField2Pair(const string& hf){
  string name, value;
  size_t colonIndex = hf.find(":");
  if (colonIndex == string::npos)
    throw SDMDataObjectStreamReaderException(" could not detect a well formed MIME header field in '"+hf+"'");

  if (colonIndex > 0) {
    name = hf.substr(0, colonIndex);
    trim(name);
  }

  if (colonIndex < hf.size()) {
    value = hf.substr(colonIndex+1);
    trim(value);
  }

  return make_pair<string, string>(name, value);
}

pair<string, string> SDMDataObjectStreamReader::requireHeaderField(const string & hf) {
  pair<string, string> hf2pair(headerField2Pair(nextLine()));
  cout << hf2pair.first << ", " << hf2pair.second << endl;
  if (to_upper_copy(hf2pair.first) != hf)
    throw SDMDataObjectStreamReaderException("read '" + currentLine + "'. Was expecting '" + hf + "'...");
  return hf2pair;
}

string unquote(const string& s, string& unquoted) {
  if (s.size() >= 2) 
    if (((s.at(0) == '"') && (s.at(s.size()-1) == '"')) || ((s.at(0) == '\'') && (s.at(s.size()-1) == '\''))) {
      if (s.size() == 2)
	unquoted = "";
      else
	unquoted = s.substr(1, s.size() - 2);
    }
    else
      unquoted = s;
  else
    unquoted = s;
  return unquoted;
}
  


string SDMDataObjectStreamReader::requireBoundaryInCT(const string& ctValue) {
  vector<string> cvValueItems;
 
  split (cvValueItems, ctValue, is_any_of(";"));
  vector<string> cvValueItemsNameValue;
  for ( vector<string>::const_iterator iter = cvValueItems.begin(); iter != cvValueItems.end() ; iter++ ) {
    cvValueItemsNameValue.clear();
    split(cvValueItemsNameValue, *iter, is_any_of("="));
    string boundary;
    if ((cvValueItemsNameValue.size() > 1) && (to_upper_copy(trim_copy(cvValueItemsNameValue[0])) == "BOUNDARY") && (unquote(cvValueItemsNameValue[1], boundary).size() > 0))
      return boundary;
  }
  throw SDMDataObjectStreamReaderException("could not find a boundary definition in '" + ctValue + "'.");
}

void SDMDataObjectStreamReader::skipUntilEmptyLine(int maxSkips) {
  //cout << "Entering skipUntilEmptyLine" << endl;
  int numSkips = 0;
  string line = nextLine();
  while ((line.size() != 0) && (numSkips <= maxSkips)) {
    line = nextLine();
    numSkips += 1;
  }

  if (numSkips > maxSkips) {
    ostringstream oss;
    oss << "could not find an empty line is less than " << maxSkips + 1 << " lines." << endl;
    throw SDMDataObjectStreamReaderException(oss.str());
  } 
  //cout << "Exiting skipUntilEmptyLine" << endl;
}

string SDMDataObjectStreamReader::requireMIMEHeader() {
  // MIME-Version
  pair<string, string>name_value(headerField2Pair(nextLine()));
  cout << name_value.first << "=" << name_value.second << endl;
  if (currentLine != "MIME-Version: 1.0") 
    throw SDMDataObjectStreamReaderException("'MIME-Version: 1.0' missing at the very beginning of the file.");

  // Content-Type
  boundary_1 = requireBoundaryInCT(requireHeaderField("CONTENT-TYPE").second);

  // Content-Description
  name_value = requireHeaderField("CONTENT-DESCRIPTION");

  // Content-Location
  name_value = requireHeaderField("CONTENT-LOCATION");

  // Look for an empty line in the at most 10 subsequent lines.
  skipUntilEmptyLine(10);

  return boundary_1;
}

string SDMDataObjectStreamReader::accumulateUntilBoundary(const string& boundary, int maxLines) {
  // cout << "Entering accumulateUntilBoundary with maxLines = " << maxLines << endl;
  int numLines = 0;
  string line = nextLine();
  string result;
  while ((numLines <= maxLines) && (line.find("--"+boundary) == string::npos)) {
    result += line;
    numLines++;
    line = nextLine();
  }

  if (numLines > maxLines) {
    ostringstream oss;
    oss << "could not find the boundary string '"<< boundary << "' in less than " << maxLines + 1 << " lines." << endl;
    throw SDMDataObjectStreamReaderException(oss.str());    
  }

  return result;
}

void SDMDataObjectStreamReader::requireBoundary(const string& boundary, int maxLines) {
  // cout << "Entering require boundary with boundary == '" << boundary << "' and maxLines = " << maxLines << endl; 
  int numLines = 0;
  string dashdashBoundary = "--"+boundary;
  string line = nextLine();
  while ((numLines <= maxLines) && (line.compare(dashdashBoundary) != 0)) {
    numLines++;
    line = nextLine();
  }

  if (numLines > maxLines) {
    ostringstream oss;
    oss << "could not find the boundary string '"<< boundary << "' in less than " << maxLines + 1 << " lines." << endl;
    throw SDMDataObjectStreamReaderException(oss.str());
  }
}

int64_t SDMDataObjectStreamReader::lookForBinaryPartSize(xmlDoc* doc, const string& partName) {
  xmlXPathContextPtr xpathCtx;
  xmlXPathObjectPtr xpathObj;

  /*
   * Create an evaluation context
   */
  xpathCtx = xmlXPathNewContext(doc);
  
  /*
   * Build and evaluate an XPath expression.
   */
  string stringXPathExpr = "/sdmDataHeader/dataStruct/" + partName;
  
  const xmlChar* xpathExpr = BAD_CAST stringXPathExpr.c_str();
  
  xpathObj =  xmlXPathEvalExpression(xpathExpr, xpathCtx);
  if(xpathObj == NULL) {
    xmlXPathFreeContext(xpathCtx); 
    xmlFreeDoc(doc);
    throw SDMDataObjectStreamReaderException("Unable to evaluate xpath expression '"
					     + stringXPathExpr
					     + "' while looking for the binary part size of '"
					     + partName
					     +"'.");
  }

  xmlNodeSetPtr nodes = xpathObj->nodesetval;
  int size = (nodes) ? nodes->nodeNr : 0;
  if (size == 0) return -1;


  if (size > 2) {
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    throw SDMDataObjectStreamReaderException("The evaluation of the xpath expression '"
					     + stringXPathExpr
					     + "' has returned more than one element.");
  }

  xmlNodePtr child = nodes->nodeTab[0];

  if (xmlHasProp(child, (const xmlChar*) "size")) {
    xmlChar * value = xmlGetProp(child, (const xmlChar *) "size");
    const regex UINT("[0-9]+");
    cmatch what;
    if (regex_match((char*) value, what, UINT)) {
      int64_t result = ::atoi(what[0].first);
      xmlFree(value);
      xmlXPathFreeObject(xpathObj);
      xmlXPathFreeContext(xpathCtx);
      return result;
    }
    else {
      xmlXPathFreeObject(xpathObj);
      xmlXPathFreeContext(xpathCtx);
      throw SDMDataObjectStreamReaderException("In '" + string((const char*) child->name) + "' failed to parse the value of '"+string((const char*) value)+"' as an int.");
    }    
  }
  else {
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    throw SDMDataObjectStreamReaderException("In '" + string((const char*) child->name) + "' could not find the attribute 'size'.");
  }
}

string SDMDataObjectStreamReader::requireCrossDataType(xmlNode* parent) {
  string result;

  string comparee("crossData");
  xmlNode * child = parent->children;

  while ((child != 0) && (comparee.compare((const char*) child->name) != 0))
    child = child->next;

  if ((child == 0) || (child->type != XML_ELEMENT_NODE)) {
    ostringstream oss;
    oss << "could not find the element 'crossData'." << endl;
    throw SDMDataObjectStreamReaderException(oss.str());
  }

  if (xmlHasProp(child, (const xmlChar*) "type")) {
    xmlChar * value = xmlGetProp(child, (const xmlChar *) "type");
    result = string((const char *) value);
    xmlFree(value);
    return result;
  }
  else
    throw SDMDataObjectStreamReaderException("In '" + string((const char*) child->name) + "' could not find the attribute 'type'.");
}

void SDMDataObjectStreamReader::requireSDMDataHeaderMIMEPart() {
  //
  // Requires the presense of boundary_1
  requireBoundary(boundary_1, 0);

  // Ignore header fields
  // requireHeaderField("CONTENT-TYPE")
  // requireHeaderField("CONTENT-TRANSFER-ENCODING")
  // requireHeaderField("CONTENT-LOCATION")

  //
  // Look for an empty line at most distant from 10 lines from here.
  skipUntilEmptyLine(10);
  string sdmDataHeader = accumulateUntilBoundary(boundary_1, 100);
  xmlDoc * doc = xmlReadMemory(sdmDataHeader.data(), sdmDataHeader.size(),  "SDMDataHeader.xml", NULL, XML_PARSE_NOBLANKS);
  xmlNode* root_element = xmlDocGetRootElement(doc);
  if ( root_element == NULL || root_element->type != XML_ELEMENT_NODE )
      throw SDMDataObjectStreamReaderException("Failed to parse the SDMDataHeader into a DOM structure."); 

  const char *partNames[] = {"actualDurations", "actualTimes", "autoData", "crossData", "zeroLags", "flags"};
  vector<string> v_partNames(partNames, partNames+6); 

  for ( vector<string>::iterator iter = v_partNames.begin(); iter != v_partNames.end(); iter++) {
    int64_t size = lookForBinaryPartSize(doc, *iter);
    if (size != -1) {
      cout << "size of binary part '" << *iter << "' = " << size << endl;
      binaryPartSize[*iter] = lookForBinaryPartSize(doc, *iter);
    }
  }
}

void SDMDataObjectStreamReader::requireSDMDataSubsetMIMEPart() {
  pair<string, string> name_value = requireHeaderField("CONTENT-TYPE");
  boundary_2 = requireBoundaryInCT(name_value.second);
  cout << "boundary_2 = " << boundary_2 << endl;
  name_value = requireHeaderField("CONTENT-DESCRIPTION");
  requireBoundary(boundary_2, 10);

  string sdmDataSubsetHeader = accumulateUntilBoundary(boundary_2, 100);
  xmlDoc * doc = xmlReadMemory(sdmDataSubsetHeader.data(), sdmDataSubsetHeader.size(), "SDMDataSubsetHeader.xml",  NULL, XML_PARSE_NOBLANKS);
  xmlChar* xmlBuff;
  int bufferSize;
  xmlDocDumpFormatMemory(doc, &xmlBuff, &bufferSize, 1);
  cout << string((const char*) xmlBuff, bufferSize) << endl;
  xmlFree(xmlBuff);
  xmlFree(doc);

  const regex BINARYPARTLOC("([0-9]+/)+(actualDuration|actualTimes|autoData|crossData|zeroLags|flags)\\.bin");
  bool done = false;
  while (!done) {
    name_value = requireHeaderField("CONTENT-TYPE");
    name_value = requireHeaderField("CONTENT-LOCATION");
    
    cmatch what;
    if (!regex_match(trim_copy(name_value.second).c_str(), what, BINARYPARTLOC))
      throw SDMDataObjectStreamReaderException("Invalid field : '" + name_value.first + ":" + name_value.second + "'.");

    cout << "Binary part name = " << what[1] << endl;
    string binaryPartName = string(what[1]);
    if (binaryPartSize.find(binaryPartName) == binaryPartSize.end())
      throw SDMDataObjectStreamReaderException("The size of '"+binaryPartName+"' was not announced in the data header.!");

    string crossDataType;
    xmlNode* root_element = xmlDocGetRootElement(doc);
    if ( root_element == NULL || root_element->type != XML_ELEMENT_NODE )
      throw SDMDataObjectStreamReaderException("Failed to find the root node into the SDMDataHeader."); 
    if (binaryPartName == "crossData") 
      crossDataType = requireCrossDataType(root_element);

    skipUntilEmptyLine(10);
    int numberOfCharsPerValue = 0;
    char* buffer;
    if (binaryPartName == "actualDurations") {
      buffer = actualDurationsBuffer;
      numberOfCharsPerValue = 8;
    }
    else if (binaryPartName == "actualTimes") {
      buffer = actualTimesBuffer;
      numberOfCharsPerValue = 8;
    }
    else if (binaryPartName == "autoData") {
      buffer = autoDataBuffer;
      numberOfCharsPerValue = 4;
    }
    else if (binaryPartName == "crossData") {
      buffer = crossDataBuffer;
      if (crossDataType == "INT16_TYPE")
	numberOfCharsPerValue = 2;
      else if ((crossDataType == "INT32_TYPE") || (crossDataType == "FLOAT32_TYPE"))
	numberOfCharsPerValue = 4;
    }
    else if (binaryPartName == "flags") {
      buffer = flagsBuffer;
      numberOfCharsPerValue = 4;
    }
    else if (binaryPartName == "zeroLags") {
      buffer = zeroLagsBuffer;
      numberOfCharsPerValue = 4;
    }

    int64_t numberOfCharsToRead = numberOfCharsPerValue * binaryPartSize[binaryPartName];
    buffer = (char *) malloc (numberOfCharsToRead * sizeof(char));
    if (buffer == 0) 
      throw SDMDataObjectStreamReaderException("Could not get memory to store '" + binaryPartName + "'.");
    f.read(buffer, numberOfCharsToRead * sizeof(char));
  }
  
}


int main (int argC, char* argV[]) {
  if (argC < 2) {
    cout << "a.out filename" << endl;
    exit(1);
  }

  int numIntegration = 0;
  SDMDataObjectStreamReader ssr;
  try {
    ssr.open(argV[1]);
    while (ssr.hasData()) {
      ssr.getData();
      numIntegration++;
    }
  }
  catch (SDMDataObjectStreamReaderException& e) {
    cout << e.getMessage() << endl;
  }
  
}
