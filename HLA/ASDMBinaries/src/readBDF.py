#!/usr/bin/env python
import getopt
import re
import string
import sys

from xml.dom.minidom import parse, parseString

class SDMDataObjectStreamReader:

    def nextLine(self):
        line = self.f.readline()
        if len(line) > 0 and line[len(line)-1] == "\n":
            line = line[:-1]
        self.currentLine = line
        return line

    def HeaderField2Pair(self, hf):
        name = None
        value = None
        colonIndex = string.find(hf, ":")
        if colonIndex == -1:
            return (name, value)

        if colonIndex > 0:
            name = hf[:colonIndex]

        if colonIndex < len(hf) - 1:
            value = hf[colonIndex + 1:]

        return (name, value)

    def requireHeaderField(self, hf):
        name, value = self.HeaderField2Pair(self.nextLine())
        print name, value
        if string.upper(name) != hf:
            print 'Missing "', hf, '" field.'
            sys.exit(1)
        return name, value

    def unquote(self, s):
        if len(s) < 2:
            return s
        if s[0] == '"' and s[len(s)-1] == '"':
            return s[1:-1]
        if s[0] == "'" and s[len(s)-1] == "'":
            return s[1:-1]
        return s

    def requireBoundaryInCT(self, ctValue):
        cvValueItems = [item.lstrip().rstrip() for item in ctValue.split(";")] 
        cvValueItemsNameValue = [item.partition("=") for item in cvValueItems]
        boundaryValues = [item[2] for item in cvValueItemsNameValue if string.upper(item[0]) == 'BOUNDARY' and item[2] != ''] 
        if boundaryValues == []:
            return None
        else:
            return self.unquote(boundaryValues[0])

    def skipUntilEmptyLine(self, maxSkips):
        numSkip = 0
        line = self.nextLine()
        while line != '' and numSkip <= maxSkips :
            line = self.nextLine()
            numSkip+=1
        if numSkip > maxSkips:
            print "could not find an expected empty line"
            sys.exit(1)

    def requireMIMEHeader(self):
        # MIME-Version
        line = self.nextLine()
        name, value = self.HeaderField2Pair(line)
        print name, value
        if line != "MIME-Version: 1.0":
            print 'Missing "MIME-Version: 1.0"'
            Sys.exit(1)

        # Content-Type
        name, value = self.requireHeaderField("CONTENT-TYPE")
        # boundary in value
        boundary_1 = self.requireBoundaryInCT(value)
        self.boundary_1 = boundary_1
        print boundary_1

        # Content-Description
        print self.requireHeaderField("CONTENT-DESCRIPTION")

        # Content-Location
        print self.requireHeaderField("CONTENT-LOCATION")

        # Look for an empty line during 10 lines maximum.
        self.skipUntilEmptyLine(10)

        return boundary_1

    def accumulateUntilBoundary(self, boundary, maxLines):
        numLines = 0
        line = self.nextLine()
        result = ""
        while  numLines <= maxLines and string.find(line, "--"+boundary) != 0:
            result += line
            numLines += 1
            line = self.nextLine()

        if numLines > maxLines:
            print "Could not find '--" + boundary + "' in the next " + maxLines + "."
            sys.exit(1)

        return result

    def requireBoundary(self, boundary, maxLines):
        numLines = 0
        line = self.nextLine()
        while numLines <= maxLines and line != "--"+boundary:
            numLines += 1
            line = self.nextLine()

        if numLines > maxLines:
            print "Could not read the expected boundary '"+boundary+"'"
            sys.exit(1)

    def lookForBinaryPartSize(self, dom, partName):
        result = None
        elements = dom.getElementsByTagName(partName)
        if elements != []:
            size = elements.item(0).getAttribute("size")
            if size == "":
                print "Missing 'size' attribute in element '", element.nodeName, "."
                sys.exit(1)
            result = int(size)
        return result

    def requireCrossDataType(self, dom):
        result = None
        elements = dom.getElementsByTagName("crossData")
        if elements == []:
            print "Missing  'crossData' element in '" , dom.toprettyxml() , "'"
            sys.exit(1)

        result = elements.item(0).getAttribute("type")
        if result == "":
            print "Missing 'type' attribute in element '", element.nodeName, "."
            sys.exit(1)

        return result

    def requireSDMDataHeaderMIMEPart(self):
        #
        # Require the presence of boundary
        self.requireBoundary(self.boundary_1, 0)

        #
        # Ignore header fields
        #
        #requireHeaderField(f, "CONTENT-TYPE")
        #requireHeaderField(f, "CONTENT-TRANSFER-ENCODING")
        #requireHeaderField(f, "CONTENT-LOCATION")

        self.skipUntilEmptyLine(10)
        sdmDataHeader = self.accumulateUntilBoundary(self.boundary_1, 100)
        dom = parseString(sdmDataHeader)
        self.binaryPartSize = {}

        for partName in ["actualDurations", "actualTimes", "autoData", "crossData", "zeroLags", "flags"]:
            size = self.lookForBinaryPartSize(dom, partName)
            if size != None:
                self.binaryPartSize[partName] = size

        print dom.toprettyxml()
        print self.binaryPartSize

    def requireSDMDataSubsetMIMEPart(self):
        name, value = self.requireHeaderField("CONTENT-TYPE")
        self.boundary_2 = self.requireBoundaryInCT(value)
        print self.boundary_2
        self.requireHeaderField("CONTENT-DESCRIPTION")
        self.requireBoundary(self.boundary_2, 10)
        self.skipUntilEmptyLine(10)

        sdmDataSubsetHeader = self.accumulateUntilBoundary(self.boundary_2, 100)
        dom = parseString(sdmDataSubsetHeader)
        print dom.toprettyxml()

        done = False
        while not done :
            self.requireHeaderField("CONTENT-TYPE")
            name, value = self.requireHeaderField("CONTENT-LOCATION")
            result = re.match("([0-9]+/)+(actualDuration|actualTimes|autoData|crossData|zeroLags|flags)\\.bin", value.lstrip().rstrip())
            if result == None:
                print "Could not identify the part name in '" , value , "'"
                sys.exit(1)

            binaryPartName = result.group(2)

            if not self.binaryPartSize.has_key(binaryPartName):
                print "The size of '" + binaryPartName + "' has not been announced in the  data header"
                sys.exit(1)

            if binaryPartName == "crossData":
                crossDataType = self.requireCrossDataType(dom)

            self.skipUntilEmptyLine(10)
            if binaryPartName == "actualDurations" or binaryPartName == "actualTimes":
                numberOfBytesPerValue = 8 
            elif binaryPartName == "autoData" :
                numberOfBytesPerValue = 4 
            elif binaryPartName == "crossData" :
                if crossDataType == "INT16_TYPE":
                    numberOfBytesPerValue = 2
                elif crossDataType == "INT32_TYPE" or crossDataType == "FLOAT32_TYPE" :
                    numberOfBytesPerValue = 4
            elif binaryPartName == "flags":
                numberOfBytesPerValue = 4
            elif binaryPartName == "zeroLags":
                numberOfBytesPerValue = 4

            numberOfBytesToRead = numberOfBytesPerValue * self.binaryPartSize[binaryPartName]
            bytes = self.f.read(numberOfBytesToRead) 
            if len(bytes) < numberOfBytesToRead:
                print "EOF reached while reading a binary attachment ('" + binaryPartName + "')"
                sys.exit(1)

            print "I have read " , self.binaryPartSize[binaryPartName] , " values for " , binaryPartName

            line = self.nextLine() # Absorb the nl right after the last byte of the binary attachment
            line = self.nextLine() # This should boundary_2

            if string.find(line, "--"+self.boundary_2) != 0:
                print "Unexpected '", line, "' after the binary part '", binaryPartName, "'" 
                sys.exit(1)

            done = line == "--"+self.boundary_2+"--"

    def open(self, f):
        self.f = f
        self.boundary_1 = self.requireMIMEHeader()
        self.requireSDMDataHeaderMIMEPart()

    def hasData(self):
        return self.currentLine != "--"+self.boundary_1+"--"

    def getData(self):
        self.requireSDMDataSubsetMIMEPart()
        line = self.nextLine()

    def processBDF(self, f):
        print "in processBDF"
        boundary_1 = self.requireMIMEHeader()

        self.requireSDMDataHeaderMIMEPart(boundary_1)
    

def main():
    mReader = SDMDataObjectStreamReader()
    with open(sys.argv[1]) as f:
        mReader.open(f)
        numIntegration = 0
        while mReader.hasData():
            mReader.getData()
            numIntegration += 1
        print "I have read ", numIntegration , " chunk(s) of data"

if __name__ == "__main__":
    main()
            
