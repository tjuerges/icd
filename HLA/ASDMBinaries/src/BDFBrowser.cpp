#include "BDFBrowser.h"
#include <stdexcept>
#include <algorithm>

using namespace asdmbinaries;

#define FUNDOC(key, fun, doc) {fundoc s = {fun, doc}; fundocs_[key] = s;}

//const BDFBrowser BDFBrowserOps::bdfBrowser_;
map<string, fundoc> BDFBrowserOps::fundocs_;

BDFBrowser::BDFBrowser () { numData_ = 10; }
BDFBrowser::~BDFBrowser() { ; }


string BDFBrowser::exit(vector<string>& args) {
  reader_.done();
  return "bye";
}


string BDFBrowser::help(vector<string>& args) {
  ostringstream oss;
  map<string, fundoc>::iterator iter ;
  for (iter = BDFBrowserOps::fundocs_.begin(); iter != BDFBrowserOps::fundocs_.end(); iter++)
    oss << iter->second.doc << endl;
  return oss.str();
}

string BDFBrowser::currentBDF(vector<string>& args) { return currentBDF_; }
string BDFBrowser::readBDF(vector<string>& args) {
  if (args.size() < 2) 
    throw BDFBrowserException ("a filename is missing");
  
  reader_.done();
  reader_.read(args.at(1));
  currentBDF_ = args.at(1);
  projectPaths_ = reader_.ref().projectPaths();
  projectPathIter_ = projectPaths_.begin();
  
  return "";
}

string BDFBrowser::nextSubsetBDF(vector<string>& args) {
  projectPathIter_++; 
  if (projectPathIter_ == projectPaths_.end()) projectPathIter_ = projectPaths_.begin();
  return dataBDF(args);
}

string BDFBrowser::prevSubsetBDF(vector<string>& args)  {
  if (projectPathIter_ == projectPaths_.begin()) projectPathIter_ = projectPaths_.end();
  projectPathIter_--;
  return dataBDF(args);
}

string BDFBrowser::subsetBDF(vector<string>& args) {
  if (args.size() == 1) {
    return dataBDF(args);
  }
  else {
    string result = reader_.ref().sdmDataSubset(args.at(1)).toString(numData_);
    projectPathIter_ = find(projectPaths_.begin(), projectPaths_.end(), args.at(1));
    return result;
  } 
} 

string BDFBrowser::headerBDF(vector<string>& args) {
  return reader_.ref().toString();
}

string BDFBrowser::dataBDF(vector<string>& args) {
  return (reader_.ref().sdmDataSubset(*projectPathIter_)).toString(numData_);
}

string BDFBrowser::ppathsBDF(vector<string>& args) {
  vector<string> ppaths = reader_.ref().projectPaths();
  ostringstream oss;
  for (vector<string>::iterator iter = ppaths.begin(); iter!=ppaths.end(); iter++)
    oss << *iter << "\n";
  return oss.str();
}

string BDFBrowser::numData(vector<string>& args) {
  istringstream iss;
  ostringstream oss;

  if (args.size() > 1) {
    iss.str(args.at(1));
    int x;
    iss >> x;
    if (iss.rdstate() == istream::failbit) {
      oss << "String '" << args.at(1) <<"' is invalid to represent an integer";
      return oss.str();
    }
    else if ( x <= 0 ) {
      oss << "Negative or null integer value '" << x << "' refused";
      return oss.str();
    }
    
    numData_ = x;
  }
  oss << "numdata = " << numData_;
  return oss.str();
}


BDFBrowserOps::BDFBrowserOps() {;}
BDFBrowserOps::~BDFBrowserOps() {;}

bool BDFBrowserOps::init_ = BDFBrowserOps::init();

bool BDFBrowserOps::init() {
  FUNDOC("exit",          &BDFBrowser::exit,         "exit - obvious.");
  FUNDOC("help",          &BDFBrowser::help,         "help - displays this text.");
  FUNDOC("read",          &BDFBrowser::readBDF,      "read filename - read and parse the binary data stored in filename.");
  FUNDOC("current",       &BDFBrowser::currentBDF,   "current - displays the file name of the current binary data.");
  FUNDOC("header",        &BDFBrowser::headerBDF,    "header - display the global header.");
  //  FUNDOC("data",          &BDFBrowser::dataBDF,      "data - displays a digest content of the current data subset.");
  FUNDOC("next",          &BDFBrowser::nextSubsetBDF,"next - 'moves' to the next data subset and displays its header and few data.");
  FUNDOC("numdata",       &BDFBrowser::numData,       "numdata - displays or set the maximum number of values displayed for each attachment;");
  FUNDOC("prev",          &BDFBrowser::prevSubsetBDF,"prev - 'moves' to the previous data subset and displays its header and few data.");
  FUNDOC("subset",        &BDFBrowser::subsetBDF,    "subset projectPath -  'moves' to the subset having the given projectPath and displays its header and few data.");
  FUNDOC("ppaths",        &BDFBrowser::ppathsBDF,    "ppaths - displays the projects paths of the data subsets");
  return true;
}

string BDFBrowserOps::execute(string op, vector<string>& args) throw (BDFBrowserException) {
  //  map<string, pt2Func>::iterator iter = ops_.find(op);
  map<string, fundoc>::iterator iter = fundocs_.find(op);
  
  //  if (iter == ops_.end())
  if (iter == fundocs_.end())
    throw BDFBrowserException("Unrecognized command '"+op+"'.");
  else {     
    //return (bdfBrowser_.*(iter->second))(args);
    try {
      return (bdfBrowser_.*((iter->second).fun))(args);
    }
    catch (SDMDataObjectException e) {
      throw BDFBrowserException(e.getMessage());
    }
    catch (SDMDataObjectParserException e) {
      throw BDFBrowserException (e.getMessage());
    }
    catch (SDMDataObjectReaderException e) {
      throw BDFBrowserException (e.getMessage());
    }
    catch (std::exception& e) {
      ostringstream oss;
      oss << strlen(e.what()) << ", " << e.what();
      throw BDFBrowserException (oss.str());
    }
    catch (...) {
      throw BDFBrowserException ("Unknown exception");
    }
  }
}

const map<string, fundoc>&  BDFBrowserOps::fundocs() {
  return fundocs_;
}
