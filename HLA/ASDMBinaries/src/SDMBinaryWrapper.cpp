#include <iostream>
#include "SDMDataObjectReader.h"
#include "SDMDataObjectWriter.h"
#include "SDMDataObject.h"
#include "CTimeSampling.h"
using namespace asdmbinaries;
using namespace std;
using namespace TimeSamplingMod;
#define sdm_getBinaryDataHeader sdm_getbinarydataheader_
//#define sdm_getBinaryHead sdm_getbinaryhead_
#define sdm_getBinaryDataSubset sdm_getbinarydatasubset_
#define sdm_writeBinaryDataHeader sdm_writebinarydataheader_
//#define sdm_writeBinaryHead sdm_writebinaryhead_
#define sdm_writeBinaryDataSubset sdm_writebinarydatasubset_
#define sdm_closeBinaryDataReader sdm_closebinarydatareader_
#define sdm_closeBinaryDataWriter sdm_closebinarydatawriter_
#define sdm_getTpBinary sdm_gettpbinary_
#define sdm_writeTpBinary sdm_writetpbinary_

SDMDataObjectReader * reader;
SDMDataObjectWriter * corrWriter;
SDMDataObjectWriter * tpWriter;
SDMDataObject * sdmDataObject;
int writerCorrelationMode; 
ofstream * ofs;
// ofstream * tp_ofs;
/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)


void copyLongLong(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
		  const int64_t *myData, int64_t *data) {
  if (*numData + nData > *maxData) {
    cout << " copyLongLong: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyLongLong: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

void copyLong(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
	      const int *myData, int *data) {  // mcaillat-64
  if (*numData + nData > *maxData) {
    cout << " copyLong: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyLong: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

void copyUnsignedLong(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
		      const uint32_t *myData, unsigned long int *data) { // mcaillat-64
   if (*numData + nData > *maxData) {
    cout << " copyUnsignedLong: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyUnsignedLong: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

void copyUnsignedInt(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
		      const uint32_t *myData, uint32_t *data) { // mcaillat-64
   if (*numData + nData > *maxData) {
    cout << " copyUnsignedLong: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyUnsignedLong: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

// this one also converts to long ints...
void copyShort(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
		  const short int *myData, int *data) {
  if (*numData + nData > *maxData) {
    cout << " copyShort: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyShort: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

void copyFloat(uint32_t *numData, uint32_t nData, uint32_t *maxData, 
		  const float *myData, float *data) {
  if (*numData + nData > *maxData) {
    cout << " copyFloat: Too many data points !! nData: " << nData << endl;
    nData = *maxData - *numData;
    cout << " copyFloat: nData set to: " << nData << endl;
  }
  if (nData >> 0) {
    for (uint32_t k = 0; k < nData; k++) {
      data[*numData] = myData[k];
      *numData = *numData + 1;
    }
  } 
}

//=================================================================================================
// Read  Correlator data

extern "C" int sdm_getBinaryDataHeader(char * file, 
			    char * execBlock,
			    int64_t * startTime,
			    int * scanNum, 
			    int * subscanNum, 
			    int * execBlockNum, 
			    int * numAntenna, 
			    int * correlationMode,
			    uint32_t * numAPC,
			    AtmPhaseCorrection * apc,
			    int * maxBb,
			    uint32_t * numBaseband, 
			    uint32_t * maxSpW, 
			    uint32_t * numSpectralWindow, 
			    int * numBin,
			    uint32_t * numSpectralPoint, 
			    float * scaleFactor,
			    uint32_t * maxPP, 
			    uint32_t * numCrossPolProduct, int * crossPolProducts, 
			    uint32_t * numSdPolProduct, int * sdPolProducts, 
			    uint32_t * maxActualDurations, 
			    uint32_t * maxActualTimes, 
			    uint32_t * maxFlags, 
			    uint32_t * maxZeroLags, 
			    uint32_t * maxCrossData, 
			    uint32_t * maxAutoData, 
				       uint32_t * numDataSubset);


int sdm_getBinaryDataHeader(char * file, 
			    char * execBlock,
			    int64_t * startTime,
			    int * scanNum, 
			    int * subscanNum, 
			    int * execBlockNum, 
			    int * numAntenna, 
			    int * correlationMode,
			    uint32_t * numAPC,
			    AtmPhaseCorrection * apc,
			    int * maxBb,
			    uint32_t * numBaseband, 
			    uint32_t * maxSpW, 
			    uint32_t * numSpectralWindow, 
			    int * numBin,
			    uint32_t * numSpectralPoint, 
			    float * scaleFactor,
			    uint32_t * maxPP, 
			    uint32_t * numCrossPolProduct, int * crossPolProducts, 
			    uint32_t * numSdPolProduct, int * sdPolProducts, 
			    uint32_t * maxActualDurations, 
			    uint32_t * maxActualTimes, 
			    uint32_t * maxFlags, 
			    uint32_t * maxZeroLags, 
			    uint32_t * maxCrossData, 
			    uint32_t * maxAutoData, 
			    uint32_t * numDataSubset)
{
  try {
    string fileString = string(file);
    reader = new SDMDataObjectReader();
    //     //
    //     // Declare an SDMDataObjectReader.
    try {
      // And use it to read the file whose name is passed in argument on the command line.
      sdmDataObject = new SDMDataObject ();
      *sdmDataObject = reader->read(fileString);
      strcpy(execBlock, sdmDataObject->execBlockUID().c_str());
      *startTime = sdmDataObject->startTime();
      *scanNum = sdmDataObject->scanNum();
      *subscanNum = sdmDataObject->subscanNum();
      *execBlockNum = sdmDataObject->execBlockNum();
      if (sdmDataObject->isTP()) {
	cout << " Error, TP Data file " << endl;
	return -1;
      }
	  
       *numAntenna = sdmDataObject->numAntenna();
      *correlationMode  = sdmDataObject->correlationMode();
      SDMDataObject::DataStruct theDS  = sdmDataObject->dataStruct();
      //
      const vector <AtmPhaseCorrection> theApcs = theDS.apc();
      *numAPC =   theApcs.size();
      for (uint32_t i = 0; i < *numAPC; i++) apc[i]=theApcs.at(i);
	  
      vector <SDMDataObject::Baseband> theBasebands = theDS.basebands();
      *numBaseband = theBasebands.size();
      for (uint32_t i = 0; i < *numBaseband; i++) {
	uint32_t jj = (*maxSpW) * i;
	vector <SDMDataObject::SpectralWindow> thespws = theBasebands.at(i).spectralWindows();
	numSpectralWindow[i] = thespws.size();
	    
	for (uint32_t j = 0; j < numSpectralWindow[i]; j++) {
	  if (*correlationMode != AUTO_ONLY) {
	    scaleFactor[jj] = thespws.at(j).scaleFactor();
	  }
	  numSpectralPoint[jj] = thespws.at(j).numSpectralPoint();
	  numBin[jj] = thespws.at(j).numBin();
	  // Cross Pol Products
	  if (*correlationMode != AUTO_ONLY) {
	    uint32_t kk = (* maxPP) * jj;
	    const vector <StokesParameter> theCrossPolProducts = thespws.at(j).crossPolProducts();
	    numCrossPolProduct[jj] = theCrossPolProducts.size();
	    for (uint32_t k = 0; k < numCrossPolProduct[jj]; k++) {
	      crossPolProducts[kk] = theCrossPolProducts.at(k);
	      kk = kk + 1;
	    } 	    
	  } else {
	    numCrossPolProduct[jj] = 0;
	  }
	  // Sd Pol Products
	  if (*correlationMode != CROSS_ONLY) {
	    const vector <StokesParameter> theSdPolProducts = thespws.at(j).sdPolProducts();
	    numSdPolProduct[jj] = theSdPolProducts.size();
	    uint32_t kk = (* maxPP) * jj;
	    for (uint32_t k = 0; k < numSdPolProduct[jj]; k++) {
	      sdPolProducts[kk] = theSdPolProducts.at(k);
	      kk = kk + 1;
	    }
	  } else {
	    numSdPolProduct[jj] = 0;
	  }
	  jj = jj+1;  	
	}
      }
      *maxActualTimes = sdmDataObject->dataStruct().actualTimes().size();
      *maxActualDurations = sdmDataObject->dataStruct().actualDurations().size();
      *maxFlags = sdmDataObject->dataStruct().flags().size();
      *maxZeroLags = 0;
      if (sdmDataObject->spectralResolutionType().present()) {
	if (sdmDataObject->spectralResolutionType().literal() != CHANNEL_AVERAGE) {
	  *maxZeroLags = sdmDataObject->dataStruct().zeroLags().size();
	}
      }
      
      switch (*correlationMode) {
      case CROSS_ONLY :
	*maxCrossData = sdmDataObject->dataStruct().crossData().size();
	*maxAutoData =  0;
	break;
      case AUTO_ONLY :
	*maxAutoData = sdmDataObject->dataStruct().autoData().size();
	*maxCrossData =  0;
	break;
      case CROSS_AND_AUTO :
	*maxCrossData = sdmDataObject->dataStruct().crossData().size();
	*maxAutoData = sdmDataObject->dataStruct().autoData().size();
	break;
      default:
	break;
      }
      
      *numDataSubset = sdmDataObject->corrDataSubsets().size();
      return 0;
    }
    catch (SDMDataObjectReaderException e) {
      cout << "sdm_getBinaryDataHeader "<< e.getMessage() << endl;
      return -1;
    }
    catch (SDMDataObjectParserException e) {
      cout << "sdm_getBinaryDataHeader "<< e.getMessage() << endl;
      return -1;
    }
    catch (std::exception e) {
      cout << "sdm_getBinaryDataHeader "<< "??" << e.what() << endl;
      return -1;
    }
  }
  catch (...) {
    cout << "sdm_getBinaryDataHeader "<<  "????" << "Unexpected exception." << endl;
    return -1;
  }
}
// ==============================================================================
// Get a data subset
extern "C"  
int sdm_getBinaryDataSubset(int * dataSubset, 
			    uint32_t * numActualDurations, uint32_t * maxActualDurations, int64_t  * actualDurations,
			    uint32_t * numActualTimes, uint32_t * maxActualTimes, int64_t  * actualTimes,
			    uint32_t * numFlags, uint32_t * maxFlags, uint32_t * flags,
			    uint32_t * numZeroLags, uint32_t * maxZeroLags, float * zeroLags,
			    uint32_t * numCrossData, uint32_t * maxCrossData, int * crossData,
			    uint32_t * numAutoData, uint32_t * maxAutoData, float * autoData);
int sdm_getBinaryDataSubset(int * dataSubset, 
			    uint32_t * numActualDurations, uint32_t * maxActualDurations, int64_t  * actualDurations,
			    uint32_t * numActualTimes, uint32_t * maxActualTimes, int64_t  * actualTimes,
			    uint32_t * numFlags, uint32_t * maxFlags, uint32_t * flags,
			    uint32_t * numZeroLags, uint32_t * maxZeroLags, float * zeroLags,
			    uint32_t * numCrossData, uint32_t * maxCrossData, int * crossData,
			    uint32_t * numAutoData, uint32_t * maxAutoData, float * autoData)
{
  try {
    // Binary parts (should I check axes?)
    //
    const SDMDataSubset theDataSubset = sdmDataObject->corrDataSubsets().at(*dataSubset-1);
    int correlationMode  = sdmDataObject->correlationMode();
    //
    // loop on data subsets 
    //      
    // ActualDurations
    *numActualDurations = 0;	
    const int64_t * myActualDurations = 0;
    int nActualDurations = theDataSubset.actualDurations(myActualDurations);
    copyLongLong(numActualDurations, nActualDurations, maxActualDurations, 
		 myActualDurations, actualDurations); 
    // ActualTimes
      
    *numActualTimes = 0;	
    const int64_t * myActualTimes = 0;
    int nActualTimes = theDataSubset.actualTimes(myActualTimes);
    copyLongLong(numActualTimes, nActualTimes, maxActualTimes, 
		 myActualTimes, actualTimes); 
    // autoData
    *numAutoData = 0;	
    if (correlationMode != CROSS_ONLY) {
      const float* myAutoData = 0;
      int nAutoData = theDataSubset.autoData(myAutoData);
      copyFloat(numAutoData, nAutoData, maxAutoData, 
		myAutoData, autoData);

    }
    // crossData
    *numCrossData = 0;	
    if (correlationMode != AUTO_ONLY) {
      if (theDataSubset.crossDataType() == INT16_TYPE) {
	const short int * myCrossData = 0;
	int nCrossData = theDataSubset.crossData(myCrossData);
	copyShort(numCrossData, nCrossData, maxCrossData, 
		  myCrossData, crossData);
      } else {
	const int* myCrossData = 0; // mcaillat-64
	long int nCrossData = theDataSubset.crossData(myCrossData);
	copyLong(numCrossData, nCrossData, maxCrossData, 
		 myCrossData, crossData);
      }
      // Flags
      *numFlags = 0;	
      const uint32_t * myFlags = 0; // mcaillat-64
      int nFlags = theDataSubset.flags(myFlags);
      copyUnsignedInt(numFlags, nFlags, maxFlags, 
		       myFlags, flags);
      // ZeroLags
      *numZeroLags=0;
      //const float * myZeroLags = 0;
      //int nZeroLags = theDataSubset.zeroLags(myZeroLags);
      //copyFloat(numZeroLags, nZeroLags, maxZeroLags, 
      //		myZeroLags, zeroLags);
    }
    // We are done with the reader.
    return 0;
  }
  catch (SDMDataObjectReaderException e) {
    cout << "sdm_getBinaryDataSubset "<< e.getMessage() << endl;
    return -1;
  }
  catch (SDMDataObjectParserException e) {
    cout << "sdm_getBinaryDataSubset "<< e.getMessage() << endl;
    return -1;
  }
  catch (std::exception e) {
    cout << "sdm_getBinaryDataSubset "<< "??" << e.what() << endl;
    return -1;
  }

  catch (...) {
    cout << "sdm_getBinaryDataSubset "<<  "????" << "Unexpected exception." << endl;
    return -1;
  }
}

//=================================================================================================
// Write Correlator data

extern "C"  int sdm_writeBinaryDataHeader(char * file, char * uid, 
					  char * execBlock,
					  uint64_t* startTime,
					  uint64_t* time,
					  uint64_t* interval, 
					  uint32_t * numTime,
					  uint32_t * scanNum, 
					  uint32_t * subscanNum, 
					  uint32_t * execBlockNum, 
					  uint32_t * numAntenna,
					  uint32_t * numBaseband,  
					  CorrelationMode * correlationMode,
					  SpectralResolutionType * spectralResolution,
					  ProcessorType * processorType,
					  uint32_t * numApc,
					  AtmPhaseCorrection * apc,
					  BasebandName * basebandNames,
					  uint32_t * maxSpW, 
					  uint32_t * numSpectralWindow, 
					  uint32_t * numBin,
					  NetSideband * netSideband,
					  uint32_t * imageSpectralWindow,
					  uint32_t * numSpectralPoint, 
					  float * scaleFactor,
					  uint32_t * maxPP, 
					  uint32_t * numSdPolProduct, StokesParameter * sdPolProducts,
					  uint32_t * numCrossPolProduct, StokesParameter * crossPolProducts,
					  uint32_t * numActualTimes, uint32_t * numActualDurations, 
					  uint32_t * numZeroLags, uint32_t * numFlags, 
					  uint32_t * numCrossData, uint32_t * numAutoData);

int sdm_writeBinaryDataHeader(char * file,char * uid, 
			      char * execBlock,
			      uint64_t* startTime,
			      uint64_t* time,
			      uint64_t* interval, 
			      uint32_t * numTime,
			      uint32_t * scanNum, 
			      uint32_t * subscanNum, 
			      uint32_t * execBlockNum, 
			      uint32_t * numAntenna,
			      uint32_t * numBaseband,  
			      CorrelationMode * correlationMode,
			      SpectralResolutionType * spectralResolution,
			      ProcessorType * processorType,
			      uint32_t * numApc,
			      AtmPhaseCorrection * apc,
			      BasebandName * basebandNames,
			      uint32_t * maxSpW, 
			      uint32_t * numSpectralWindow, 
			      uint32_t * numBin,
			      NetSideband  * netSideband,
			      uint32_t * imageSpectralWindow,
			      uint32_t * numSpectralPoint, 
			      float * scaleFactor,
			      uint32_t * maxPP, 
			      uint32_t * numSdPolProduct, StokesParameter * sdPolProducts,
			      uint32_t * numCrossPolProduct, StokesParameter * crossPolProducts,
			      uint32_t * numActualTimes, uint32_t * numActualDurations, 
			      uint32_t * numZeroLags, uint32_t * numFlags, 
			      uint32_t * numCrossData, uint32_t * numAutoData)  
{

  
  string uidString = string(uid);
  string fileString = string(file);
  string execBlockUIDString = string(execBlock);
  string title = "ALMA Correlator Data";
  ofs = new ofstream(file);
  //  SDMDataObjectWriter corrWriter;
  corrWriter = new SDMDataObjectWriter (ofs, uidString, title);
  writerCorrelationMode  = *correlationMode;

  // Prepare the description of the binary data.
  vector<SDMDataObject::Baseband> theBb(*numBaseband);
  for (uint32_t ibb = 0; ibb < *numBaseband; ibb++) {
    vector <SDMDataObject::SpectralWindow> theSpW(numSpectralWindow[ibb]); 
    
    for (uint32_t isw = 0; isw < numSpectralWindow[ibb]; isw++) {
      // fortran index for spectral window arrays:
      uint32_t jsw = ibb *(*maxSpW)+isw;
      // get the pol products for ths Spectral Window:
      vector <StokesParameter> theSdPP(numSdPolProduct[jsw]); 
      vector <StokesParameter> theCrossPP(numCrossPolProduct[jsw]); 
      if (*correlationMode != CROSS_ONLY) {
	for (uint32_t k=0; k < numSdPolProduct[jsw]; k++) {
	  theSdPP[k] = sdPolProducts[k+ (*maxPP)*jsw];
	}
      }
      if (*correlationMode != AUTO_ONLY) {
	for (uint32_t k=0; k < numCrossPolProduct[jsw]; k++) {
	  theCrossPP[k] = crossPolProducts[k+ (*maxPP)*jsw];
	}
      }
      // create the Spectral Window:

      // autocorrelation only implies DSB...
      if (*correlationMode == AUTO_ONLY) {
	theSpW[isw] = SDMDataObject::SpectralWindow(theSdPP, numSpectralPoint[jsw], 
						    numBin[jsw], DSB);
	
      }
      if (*correlationMode == CROSS_ONLY) {
	theSpW[isw] = SDMDataObject::SpectralWindow(theCrossPP, scaleFactor[jsw], 
						    numSpectralPoint[jsw], numBin[jsw], netSideband[jsw]);
      }

      if (*correlationMode == CROSS_AND_AUTO) {
	theSpW[isw] = SDMDataObject::SpectralWindow(theCrossPP, theSdPP, scaleFactor[jsw], 
						    numSpectralPoint[jsw], numBin[jsw], netSideband[jsw]);
      }
    }
    // create the Baseband:
    theBb[ibb] =  SDMDataObject::Baseband ( basebandNames[ibb], theSpW );  
  }
  
  // create the DataStruct:
  vector< AtmPhaseCorrection > theApc(*numApc);
  for (uint32_t i=0; i< *numApc; i++) { theApc[i] = apc[i]; }
  SDMDataObject::BinaryPart theActualTimes = SDMDataObject::BinaryPart(); 
  SDMDataObject::BinaryPart theActualDurations = SDMDataObject::BinaryPart ();;

  SDMDataObject::ZeroLagsBinaryPart theZeroLags = SDMDataObject::ZeroLagsBinaryPart (*numZeroLags, 
									     ENUMVEC(AxisName, "ANT BAB SPW BIN POL"),
									     XF);
  SDMDataObject::AutoDataBinaryPart theAutoData = SDMDataObject::AutoDataBinaryPart ();
  SDMDataObject::BinaryPart theFlags = SDMDataObject::BinaryPart ();
  SDMDataObject::BinaryPart theCrossData = SDMDataObject::BinaryPart ();
  if (*correlationMode != CROSS_ONLY) {
    theActualTimes = SDMDataObject::BinaryPart (*numActualTimes, 
						ENUMVEC(AxisName, "ANT BAB SPW BIN POL"));
    theActualDurations = SDMDataObject::BinaryPart (*numActualDurations, 
						    ENUMVEC(AxisName, "ANT SPW BAB BIN POL"));;
    theAutoData = SDMDataObject::AutoDataBinaryPart (*numAutoData, 
						     ENUMVEC(AxisName, "ANT BAB SPW BIN SPP POL"),
						     false);
    theFlags = SDMDataObject::BinaryPart (*numFlags, 
					  ENUMVEC(AxisName, "ANT BAB SPW BIN POL"));
  }
  if (*correlationMode != AUTO_ONLY) {
    theActualTimes = SDMDataObject::BinaryPart (*numActualTimes, 
						ENUMVEC(AxisName, "BAL BAB SPW BIN POL"));
    theActualDurations = SDMDataObject::BinaryPart (*numActualDurations, 
						    ENUMVEC(AxisName, "BAL BAB SPW BIN POL"));;
    theCrossData = SDMDataObject::BinaryPart (*numCrossData, 
					      ENUMVEC(AxisName, "BAL BAB SPW APC SPP POL"));
    theFlags = SDMDataObject::BinaryPart (*numFlags, 
					  ENUMVEC(AxisName, "BAL ANT BAB SPW BIN POL"));
  }
  // define the data structure:
  SDMDataObject::DataStruct theDS = SDMDataObject::DataStruct(theApc, theBb,
							      theFlags, theActualTimes, 
							      theActualDurations, theZeroLags, 
							      theCrossData, theAutoData);
  // image side bands only exist in correlation
  if (*correlationMode != AUTO_ONLY) {
    // deal with image spectral windows.
    for (uint32_t ibb = 0; ibb < *numBaseband; ibb++) {
      for (uint32_t isw1 = 0; isw1 < numSpectralWindow[ibb]; isw1++) {
	//// fortran index for spectral window arrays:
	uint32_t jsw = ibb *(*maxSpW)+isw1; 
	// the image spectral window is the fortran (one-based) index 
	// of that spectral window in the baseband.
	uint32_t isw2 = imageSpectralWindow[jsw]-1; 
	// image spectral windows MUST be in the same baseband !!!
	theDS.imageSPW(ibb, isw1, isw2);
	theDS.imageOfSPW(ibb, isw2, isw1);
      }
    }
  }
  OptionalSpectralResolutionType optsrt = OptionalSpectralResolutionType(*spectralResolution); 
  // create the header:
  corrWriter->corrDataHeader(*startTime, execBlockUIDString, *execBlockNum, *scanNum, 
			     *subscanNum, *numAntenna, 
			     *correlationMode, optsrt, theDS);
  //corrWriter->done();
  return 0;
}

extern "C" int sdm_closeBinaryDataWriter();
int sdm_closeBinaryDataWriter()
{
  corrWriter->done();
  ofs->close();
  return 0;
}

extern "C" int sdm_closeBinaryDataReader();
int sdm_closeBinaryDataReader()
{
  reader->done();
  return 0;
}


extern "C" int sdm_writeBinaryDataSubset(uint32_t * integrationNum,
					 uint32_t * subIntegrationNum,
					 TimeSampling * timeSampling,
					 uint64_t* time,
					 uint64_t* interval,
					 PrimitiveDataType* corrDataType,
					 uint32_t * numFlags, uint32_t * flags,
					 uint32_t * numActualTimes, uint64_t* actualTimes,
					 uint32_t * numActualDurations, uint64_t* actualDurations,
					 uint32_t * numZeroLags, float * zeroLags,
					 uint32_t * numCrossData, int * crossData,
					 uint32_t * numAutoData, float * autoData);

int sdm_writeBinaryDataSubset(uint32_t * integrationNum,
			      uint32_t * subIntegrationNum,
			      TimeSampling * timeSampling,
			      uint64_t* time,
			      uint64_t* interval,
			      PrimitiveDataType* corrDataType,
			      uint32_t * numFlags, uint32_t * flags,
			      uint32_t * numActualTimes, uint64_t* actualTimes,
			      uint32_t * numActualDurations, uint64_t* actualDurations,
			      uint32_t * numZeroLags, float * zeroLags,
			      uint32_t * numCrossData, int * crossData,
			      uint32_t * numAutoData, float * autoData)
{
  vector<uint32_t> myFlags(*numFlags);
  vector<int64_t> myActualTimes(*numActualTimes);
  vector<int64_t> myActualDurations(*numActualDurations);
  vector<float> myZeroLags(*numZeroLags);
  vector<float> myAutoData(*numAutoData);
  for (uint32_t i = 0; i < *numActualTimes; i++) myActualTimes[i] = actualTimes[i];
  for (uint32_t i = 0; i < *numActualDurations; i++) myActualDurations[i] = actualDurations[i];
  for (uint32_t i = 0; i < *numZeroLags; i++) myZeroLags[i] = zeroLags[i];
  if (writerCorrelationMode != CROSS_ONLY) {
    for (uint32_t i = 0; i < *numAutoData; i++) myAutoData[i] = autoData[i];
  }
  if (writerCorrelationMode != AUTO_ONLY) {
    for (uint32_t i = 0; i < *numFlags; i++) myFlags[i] = flags[i];
  }
  if (*corrDataType == INT16_TYPE) {
    vector<short int> myCrossData(*numCrossData);
    if (writerCorrelationMode != AUTO_ONLY) {
      for (uint32_t i = 0; i < *numCrossData; i++) myCrossData[i] = crossData[i];
    }
    if (*timeSampling == INTEGRATION) {
      corrWriter->addIntegration (*integrationNum, *time, *interval, 
				  myFlags, myActualTimes, myActualDurations, 
				  myZeroLags, myCrossData, myAutoData);
    }
    if (*timeSampling == SUBINTEGRATION) {
      corrWriter->addSubintegration (*integrationNum, *subIntegrationNum, *time, 
				     *interval, 
				     myFlags, myActualTimes, myActualDurations, 
				     myZeroLags, myCrossData, myAutoData);
    }
  }
  if (*corrDataType == INT32_TYPE) {
    vector<int> myCrossData(*numCrossData);
    if (writerCorrelationMode != AUTO_ONLY) {
      for (uint32_t i = 0; i < *numCrossData; i++) myCrossData[i] = crossData[i];
    }
    if (*timeSampling == INTEGRATION) {
      corrWriter->addIntegration (*integrationNum, *time, *interval, 
      				  myFlags, myActualTimes, myActualDurations, 
      				  myZeroLags, myCrossData, myAutoData);
    }
    if (*timeSampling == SUBINTEGRATION) {
      corrWriter->addSubintegration (*integrationNum, *subIntegrationNum, *time, *interval, 
				     myFlags, myActualTimes, myActualDurations, 
				     myZeroLags, myCrossData, myAutoData);
    }
  }
  return 0;
}

//=================================================================================================
// Read Total Power: a single call for the whole thing.

extern "C" int sdm_getTpBinary(char * file,
			       char * execBlock,
			       int64_t * startTime,
			       uint32_t * numTime,
			       uint32_t * scanNum, 
			       uint32_t * subscanNum, 
			       uint32_t * execBlockNum, 
			       uint32_t * numAntenna,
			       uint32_t * maxBb,
			       uint32_t * numBaseband, 
			       uint32_t * numBin,
			       uint32_t * maxPP,  
			       uint32_t * numSdPolProduct, int * sdPolProducts,
			       uint32_t * numAutoData, uint32_t * maxAutoData, float * autoData);
int sdm_getTpBinary(char * file,
		    char * execBlock,
		    int64_t * startTime,
		    uint32_t * numTime,
		    uint32_t * scanNum, 
		    uint32_t * subscanNum, 
		    uint32_t * execBlockNum, 
		    uint32_t * numAntenna,
		    uint32_t * maxBb,
		    uint32_t * numBaseband,  
		    uint32_t * numBin,
		    uint32_t * maxPP,  
		    uint32_t * numSdPolProduct, int * sdPolProducts,
		    uint32_t * numAutoData, uint32_t * maxAutoData, float * autoData)  {

  try {
    string fileString = string(file);
    reader= new SDMDataObjectReader();
    try {
      const SDMDataObject & sdmDataObject = reader->read(fileString);
      // get general parameters 
      *startTime = sdmDataObject.startTime();
      *scanNum = sdmDataObject.scanNum();
      *subscanNum = sdmDataObject.subscanNum();
      strcpy(execBlock, sdmDataObject.execBlockUID().c_str());
      *execBlockNum = sdmDataObject.execBlockNum();
      if (sdmDataObject.isTP()) {
	*numAntenna = sdmDataObject.numAntenna();
	*numTime = sdmDataObject.numTime();
	SDMDataObject::DataStruct theDS  = sdmDataObject.dataStruct();
	
	vector <SDMDataObject::Baseband> theBasebands = theDS.basebands();
	*numBaseband = theBasebands.size();
	for (uint32_t i = 0; i < *numBaseband; i++) {
	  vector <SDMDataObject::SpectralWindow> thespws = theBasebands.at(i).spectralWindows();
	  numBin[i] = thespws.at(0).numBin();
	  const vector <StokesParameter> theSdPolProducts = thespws.at(0).sdPolProducts();
	  numSdPolProduct[i] = theSdPolProducts.size();
	  uint32_t kk = (* maxPP) * i;
	  for (uint32_t k = 0; k < numSdPolProduct[i]; k++) {
	    sdPolProducts[kk] = theSdPolProducts.at(k);
	    kk = kk + 1;
	  }
	}
	//
	const SDMDataSubset& theDataSubset = sdmDataObject.tpDataSubset();
	
	// autoData
	*numAutoData= 0;
	const float* myAutoData = 0;
	int nAutoData = theDataSubset.autoData(myAutoData);
	copyFloat(numAutoData, nAutoData, maxAutoData, 
		  myAutoData, autoData);
	// finished...
	return 0;
	//
      } else {
	cout << " This is not TP data !!  " << endl;
	return -1;
      }
    }
    catch (SDMDataObjectReaderException e) {
      cout << "sdm_getTPbinary: "<<e.getMessage() << endl;
      return -1;
    }
    catch (SDMDataObjectParserException e) {
      cout << "sdm_getTPbinary: "<< e.getMessage() << endl;
      return -1;
    }
    catch (std::exception e) {
      cout << "sdm_getTPbinary: "<< "??" << e.what() << endl;
      return -1;
    }
  }
  catch (...) {
    cout <<  "sdm_getTPbinary: "<<"????" << "Unexpected exception." << endl;
    return -1;
  }
}

// Write Total Power


extern "C" int sdm_writeTpBinary(char * file, char * uid, 
				 char * execBlock,
				 uint64_t* startTime,
				 uint64_t* time,
				 uint64_t* interval, 
				 uint32_t * numTime,
				 uint32_t * scanNum, 
				 uint32_t * subscanNum, 
				 uint32_t * execBlockNum, 
				 uint32_t * numAntenna,
				 uint32_t * numbaseband, 
				 BasebandName * baseBandNames,
				 uint32_t * numBin,
				 uint32_t * numSdPolProduct, StokesParameter * sdPolProducts,
				 uint32_t * numFlags, uint32_t * flags,
				 uint32_t * numActualTimes, uint64_t* actualTimes,
				 uint32_t * numActualDurations, uint64_t* actualDurations,
				 uint32_t * numAutoData, float * autoData);
int sdm_writeTpBinary(char * file, char * uid, 
		      char * execBlock,
		      uint64_t* startTime,
		      uint64_t* time,
		      uint64_t* interval, 
		      uint32_t * numTime,
		      uint32_t * scanNum, 
		      uint32_t * subscanNum, 
		      uint32_t * execBlockNum, 
		      uint32_t * numAntenna,
		      uint32_t * numBaseband,  
		      BasebandName * basebandNames,
		      uint32_t * numBin,
		      uint32_t * numSdPolProduct, StokesParameter * sdPolProducts,
		      uint32_t * numFlags, uint32_t * flags,
		      uint32_t * numActualTimes, uint64_t* actualTimes,
		      uint32_t * numActualDurations, uint64_t* actualDurations,
		      uint32_t * numAutoData, float * autoData)  {

  
  string uidString = string(uid);
  string execBlockUIDString = string(execBlock);
  string title = "ALMA Total Power Data";
  ofstream * tp_ofs;
  tp_ofs = new ofstream(file);
  tpWriter = new SDMDataObjectWriter(tp_ofs, uidString, title);

  // Prepare the description of the binary data.
  vector<SDMDataObject::Baseband> theBb(*numBaseband);


  for (uint32_t i = 0; i < *numBaseband; i++) {
    // always one Spectral Window in TP baseband...
    // there is one TP Spectral window / baseband
    vector <SDMDataObject::SpectralWindow> theSpW(1); 
    // get the pol products
    vector <StokesParameter> theSdPP(*numSdPolProduct); 
    for (uint32_t k=0; k < *numSdPolProduct; k++) {
      theSdPP[k] = sdPolProducts[k];
    }
    // it has numSpectralPoint one 
    int  numSpectralPoint=1;
    theSpW[0] = SDMDataObject::SpectralWindow(theSdPP, numSpectralPoint, numBin[i], DSB);
    theBb[i] =  SDMDataObject::Baseband ( basebandNames[i], theSpW ); 
  }
  const vector<AxisName> flagsAxes = ENUMVEC(AxisName, "TIM  ANT BAB BIN POL");
  vector<uint32_t> myFlags (*numFlags);
  for (uint32_t i = 0; i < *numFlags; i++) myFlags[i] = flags[i];
  const vector<AxisName> actualTimesAxes     = ENUMVEC(AxisName, "TIM  ANT BAB BIN POL");
  vector<int64_t> myActualTimes(*numActualTimes);
  for (uint32_t i = 0; i < *numActualTimes; i++) 
    myActualTimes[i] = actualTimes[i];
  const vector<AxisName> actualDurationsAxes = ENUMVEC(AxisName, "TIM  ANT BAB BIN POL");
  vector<int64_t> myActualDurations(*numActualDurations);
  for (uint32_t i = 0; i < *numActualDurations; i++) 
    myActualDurations[i] = actualDurations[i];
  const vector<AxisName> autoDataAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
  vector<float> myAutoData(*numAutoData);
  for (uint32_t i = 0; i < *numAutoData; i++) myAutoData[i] = autoData[i];
  tpWriter->tpData((uint64_t) *startTime, 
		   execBlockUIDString,
		   (uint32_t) *execBlockNum,
		   (uint32_t) *scanNum, 
		   (uint32_t) *subscanNum,
		   (uint32_t) *numTime,         // number of integrations
		   (uint32_t) *numAntenna,
		   theBb,
		   (uint64_t ) *time,   // time 
		   (uint64_t ) *interval,    // interval
		   flagsAxes,          // axes order for flags
		   myFlags,              // flags values
		   actualTimesAxes,    // axes order for actualTimes
		   myActualTimes,        // actualTimes values
		   actualDurationsAxes,// axes order for actualDurations
		   myActualDurations,    // actualDurations values
		   autoDataAxes,       // axes order for autoData
		   myAutoData);            // autoData values
		   //		   axis,
		   //		   myAutoData);
  tpWriter->done();
  tp_ofs->close();
  return 0;
}

