#include "BDFBrowser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <readline/readline.h>
#include <readline/history.h>

using namespace std;
using namespace asdmbinaries;

// /* **************************************************************** */
// /*                                                                  */
// /*                  Interface to Readline Completion                */
// /*                                                                  */
// /* **************************************************************** */
// char * dupstr (char* s) {
//   char *r;

//   r = malloc (strlen (s) + 1);
//   strcpy (r, s);
//   return (r);
// }

// char *command_generator __P((const char *, int));
// char **bdfBrowser_completion __P((const char *, int, int));

// /* Tell the GNU Readline library how to complete.  We want to try to
//    complete on command names if this is the first word in the line, or
//    on filenames if not. */
// void initialize_readline () {
//   /* Allow conditional parsing of the ~/.inputrc file. */
//   rl_readline_name = "bdfBrowser";

//   /* Tell the completer that we want a crack first. */
//   rl_attempted_completion_function = bdfBrowser_completion;
// }

// /* Attempt to complete on the contents of TEXT.  START and END
//    bound the region of rl_line_buffer that contains the word to
//    complete.  TEXT is the word to complete.  We can use the entire
//    contents of rl_line_buffer in case we want to do some simple
//    parsing.  Returnthe array of matches, or NULL if there aren't any. */
// char ** bdfBrowser_completion (const char* text, int start, int end) {
//   char **matches;

//   matches = (char **)NULL;

//   /* If this word is at the start of the line, then it is a command
//      to complete.  Otherwise it is the name of a file in the current
//      directory. */
//   if (start == 0)
//     matches = rl_completion_matches (text, command_generator);

//   cout << "matches" << endl;

//   return (matches);
// }

// /* Generator function for command completion.  STATE lets us
//    know whether to start from scratch; without any state
//    (i.e. STATE == 0), then we start at the top of the list. */
// char * command_generator (const char* text, int state) {
//   static int len;
//   char *name;

//   map<string, fundoc>::const_iterator iter;

//   /* If this is a new word to complete, initialize now.  This
//      includes saving the length of TEXT for efficiency, and
//      initializing the index variable to 0. */
//   if (!state)
//     {
//       iter = BDFBrowserOps::fundocs().begin();
//       len = strlen (text);
//     }

//   /* Return the next name which partially matches from the
//      command list. */
//   while (iter != BDFBrowserOps::fundocs().end())
//     //while (name = commands[list_index])
//     {
//       name = (iter->first).c_str();
//       cout << "name=" << name << endl;
//       if (strncmp (name, text, len) == 0)
//         return (dupstr(name));
//       iter++;
//     }

//   /* If no names matched, then return NULL. */
//   return ((char *)NULL);
// }


/**
 * Let's go.
 */
int main (int argC, char* argV[]) {
  BDFBrowserOps bdfBrowserOps;
  string s;

  //  initialize_readline ();	
  
  string message = "";
  while (message != "bye") {
    char *line = readline("bdfBrowser>");
    string s(line);
    vector<string> args;
    istringstream iss(s);
    copy(istream_iterator<string>(iss),
	 istream_iterator<string>(),
	 back_inserter(args));
    if (args.size() > 0) {
      try {
	cout << (message = bdfBrowserOps.execute(args.at(0), args)) << endl;
      }
      catch (BDFBrowserException e) {
	cout << e.getMessage() << endl;
      }
      catch (SDMDataObjectException e) {
	cout << e.getMessage() << endl;
      }
      catch (SDMDataObjectReaderException e) {
	cout << e.getMessage() << endl;
      }
      add_history(line);
    }
    free(line);
  }
}

