#ifndef BDFBROWSER_H
#define BDFBROWSER_H
#include "SDMDataObjectReader.h"
#include <string>
using namespace std;
using namespace asdmbinaries;

class BDFBrowserException {    
  protected:
    string message;

  public:
    /**
     * An empty contructor.
     */
    BDFBrowserException();

    /**
     * A constructor with a message associated with the exception.
     * @param m a string containing the message.
     */
    BDFBrowserException(string m);
    
    /**
     * The destructor.
     */
    virtual ~BDFBrowserException(); 

    /**
     * Returns the message associated to this exception.
     * @return a string.
     */
    string getMessage() const ;
};

inline BDFBrowserException::BDFBrowserException() : message ("BDFBrowserException") {}
inline BDFBrowserException::BDFBrowserException(string m) : message(m) {}
inline BDFBrowserException::~BDFBrowserException() {}
inline string BDFBrowserException::getMessage() const { return "BDFBrowserException : " + message; }

class BDFBrowser {

private:
  asdmbinaries::SDMDataObjectReader reader_;
  string currentBDF_;
  vector<string> projectPaths_;
  vector<string>::const_iterator projectPathIter_;
  unsigned int numData_;

public:
  BDFBrowser ();
  virtual ~BDFBrowser();
  string exit(vector<string>& args);
  string help(vector<string>& args) ;
  string currentBDF(vector<string>& args) ;
  string readBDF(vector<string>& args)  ;
  string headerBDF(vector<string>& args) ;
  string dataBDF(vector<string>& args) ;
  string nextSubsetBDF(vector<string>& args) ;
  string prevSubsetBDF(vector<string>& args) ;
  string subsetBDF(vector<string>& args);
  string ppathsBDF(vector<string>& args);
  string numData(vector<string>& args);
};

typedef string (BDFBrowser::*pt2Func) (vector<string>&) ;

struct fundoc {
  pt2Func fun;
  string doc;
};

class BDFBrowserOps {
  friend class BDFBrowser;


private:
  //  const BDFBrowser bdfBrowser_;
  BDFBrowser bdfBrowser_;
  static map<string, fundoc> fundocs_;
  static bool init_;
  static bool init();

public:
  BDFBrowserOps();
  virtual ~BDFBrowserOps();
  string execute(string op, vector<string>& args) throw (BDFBrowserException);
  static const map<string, fundoc>& fundocs();
}
;
#endif
