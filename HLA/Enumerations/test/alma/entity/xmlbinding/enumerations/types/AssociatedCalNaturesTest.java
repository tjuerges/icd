package alma.entity.xmlbinding.enumerations.types;

import java.io.FileReader;
import java.io.StringReader;

import junit.framework.TestCase;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

public class AssociatedCalNaturesTest extends TestCase {

	private AssociatedCalNatures etype;
	
	@Override
	protected void setUp() throws Exception {
		 final int BUFSIZ = 2000;
         char[] cbuf = new char[BUFSIZ];
         StringBuffer sbuf = new StringBuffer();

         FileReader frdr = new FileReader("TestCalNaturesEnum.xml");
         int charsRead = 0;
         while((charsRead=frdr.read(cbuf, 0, BUFSIZ))!=-1) {
                 sbuf.append(cbuf, 0, charsRead);
         }

		 etype = (AssociatedCalNatures)Unmarshaller.unmarshal(AssociatedCalNatures.class, new StringReader(sbuf.toString()));
	}

//	public void testEnumerate() {
//		fail("Not yet implemented");
//	}
//
//	public void testGetType() {
//		fail("Not yet implemented");
//	}
//
//	public void testToString() {
//		fail("Not yet implemented");
//	}
//
//	public void testValueOf() {
//		fail("Not yet implemented");
//	}

	public void testEquals() {
		assertEquals(etype,AssociatedCalNatures.ASSOCIATED_EXECBLOCK);
	}

}
