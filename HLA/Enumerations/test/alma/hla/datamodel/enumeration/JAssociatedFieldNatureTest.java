package alma.hla.datamodel.enumeration;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.AssociatedFieldNatureMod.AssociatedFieldNature;

import junit.framework.TestCase;

public class JAssociatedFieldNatureTest extends TestCase {

    private AssociatedFieldNature e;
    
    public JAssociatedFieldNatureTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        e = JAssociatedFieldNature.newAssociatedFieldNature(
                JAssociatedFieldNature.sOFF);
    }

    protected void tearDown() throws Exception {
    }

    public void testIsNumber() {
        assertTrue(JAssociatedFieldNature.isNumber());
    }

    public void testIsHelp() {
        assertFalse(JAssociatedFieldNature.isHelp());
    }

    public void testName() {
        assertEquals(JAssociatedFieldNature.sOFF, JAssociatedFieldNature.name(e));
    }

    public void testToStringAssociatedFieldNature() {
        assertEquals(JAssociatedFieldNature.sOFF, JAssociatedFieldNature.toString(e));
    }
    
    public void testNewAssociatedFieldNature() {
        try {
            AssociatedFieldNature g = JAssociatedFieldNature.newAssociatedFieldNature("Dummy");
            fail();
        } catch (AcsJBadParameterEx ex) {
            ;
        }
    }

    public void testNumber() {
        assertEquals(JAssociatedFieldNature.iOFF, JAssociatedFieldNature.number(e));
    }

    public void testNewCAM() {
        try {
            AssociatedFieldNature e = JAssociatedFieldNature.newCAM(JAssociatedFieldNature.iOFF);
        } catch (AcsJBadParameterEx e) {
            fail();
        }
        try {
            AssociatedFieldNature f = JAssociatedFieldNature.newCAM(-1);
            fail();
        } catch (AcsJBadParameterEx ex) {
            ;
        }
    }

}
