/******************************************************************************** ALMA - Atacama Large Millimiter Array
* (c) Institut de Radioastronomie Millimetrique, 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dbroguie 2007-06-27  created
*/

#include <iostream>
#include <vector>
//#include <stdlib.h>

#include <almaEnumerations_IFC.h>
#include "CAssociatedFieldNature.h"

using namespace std;
using namespace AssociatedFieldNatureMod;

int main(int argc, char *argv[]) {
    cout<<" AssociatedFieldNatureTest"<<endl;

// Specifying an enumerated item
    cout<<"\nSpecifying an enumerated item"<<endl;
    AssociatedFieldNatureMod::AssociatedFieldNature a = AssociatedFieldNatureMod::PHASE_REFERENCE;   // with namespace

    cout<<"a="<<a<<endl;

// Specifying an enumerated item from string
    cout<<"\nSpecifying an enumerated item from string"<<endl;
    AssociatedFieldNature x = CAssociatedFieldNature::newAssociatedFieldNature("PHASE_REFERENCE");
    cout <<"x="<<x<<endl;

    //  Specifying an enumerated item from integer
     cout<<"\nSpecifying an enumerated item from integer"<<endl;
     int k = 1;
     AssociatedFieldNature y = CAssociatedFieldNature::newAssociatedFieldNature(k);
     cout <<"y="<<y<<endl;

// Accessing an enumerated item as a string
    cout<<"\nAccessing an enumerated item as a string with toString() method"<<endl;
    cout<<CAssociatedFieldNature::toString(a)<<endl;
    //    cout<<CAssociatedFieldNature::toString(b)<<endl;

    cout<<"\nAccessing an enumerated item as a string with name() method"<<endl;
    cout<<CAssociatedFieldNature::name(a)<<endl;
    //    cout<<CAssociatedFieldNature::name(b)<<endl;

// Accessing the set of  enumerated item
    cout<<"\nAccessing the set of  enumerated item"<<endl;
    const vector<string> enumSet = CAssociatedFieldNature::sAssociatedFieldNatureSet();
    for (unsigned i=0;i<enumSet.size();i++)
	cout<<enumSet[i]<<endl;

// Comparison operations on enumerated items
    cout<<"\nComparison operations on enumerated items"<<endl;
    if ( a == AssociatedFieldNatureMod::PHASE_REFERENCE)
	cout<<" Comparison true"<<endl;
    //    if ( b != AssociatedFieldNatureMod::ATM_Model)
    //	cout<<" Comparison true"<<endl;


// Other methods: isNumber(), isHelp()
    cout<<"\nOther methods: isNumber(), isHelp()"<<endl;
    cout<<"CAssociatedFieldNature::isNumber()="<<CAssociatedFieldNature::isNumber()<<endl;
    cout<<"CAssociatedFieldNature::isHelp()="<<CAssociatedFieldNature::isHelp()<<endl;


    exit(0);
}
