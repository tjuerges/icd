/******************************************************************************** ALMA - Atacama Large Millimiter Array
* (c) Institut de Radioastronomie Millimetrique, 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dbroguie 2007-06-27  created
*/

#include <iostream>
#include <vector>
//#include <stdlib.h>

///// not needed for stand alone test
//// #include <almaEnumerations_IFC.h>
#include "CWVRMethod.h"

using namespace std;
using namespace WVRMethodMod;

int main(int argc, char *argv[]) {
    cout<<" WVRMethodTest"<<endl;

// Specifying an enumerated item
    cout<<"\nSpecifying an enumerated item"<<endl;
    WVRMethodMod::WVRMethod a = WVRMethodMod::ATM_MODEL;   // with namespace
    WVRMethod b = EMPIRICAL;   // namespace can be omitted

    cout<<"a="<<a<<endl;
    cout<<"b="<<b<<endl;

// Specifying an enumerated item from string
    cout<<"\nSpecifying an enumerated item from string"<<endl;
    WVRMethod x = CWVRMethod::newWVRMethod("EMPIRICAL");
    cout <<"x="<<x<<endl;

// Specifying an enumerated item from integer
//     cout<<"\nSpecifying an enumerated item from integer"<<endl;
//     int k = 1;
//     WVRMethod y = CWVRMethod::newWVRMethod(1);
//     cout <<"y="<<y<<endl;

// Accessing an enumerated item as a string
    cout<<"\nAccessing an enumerated item as a string with toString() method"<<endl;
    cout<<CWVRMethod::toString(a)<<endl;
    cout<<CWVRMethod::toString(b)<<endl;

    cout<<"\nAccessing an enumerated item as a string with name() method"<<endl;
    cout<<CWVRMethod::name(a)<<endl;
    cout<<CWVRMethod::name(b)<<endl;

// Accessing the set of  enumerated item
    cout<<"\nAccessing the set of  enumerated item"<<endl;
    const vector<string> enumSet = CWVRMethod::sWVRMethodSet();
    for (unsigned i=0;i<enumSet.size();i++)
	cout<<enumSet[i]<<endl;

// Comparison operations on enumerated items
    cout<<"\nComparison operations on enumerated items"<<endl;
    if ( a == WVRMethodMod::ATM_MODEL)
	cout<<" Comparison true"<<endl;
    if ( b != WVRMethodMod::ATM_MODEL)
	cout<<" Comparison true"<<endl;


// Other methods: isNumber(), isHelp()
    cout<<"\nOther methods: isNumber(), isHelp()"<<endl;
    cout<<"CWVRMethod::isNumber()="<<CWVRMethod::isNumber()<<endl;
    cout<<"CWVRMethod::isHelp()="<<CWVRMethod::isHelp()<<endl;


    exit(0);
}
