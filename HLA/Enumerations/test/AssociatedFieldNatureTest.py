#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2007-06-15  created
#

import unittest, inspect
import PyDataModelEnumeration
from ACSErrTypeCommon import BadParameterEx

class AssociatedFieldNatureTest(unittest.TestCase):
    
    def setUp(self):
        self.mod = PyDataModelEnumeration.PyAssociatedFieldNature
        self.e1 = self.mod.On
        self.e2 = self.mod.Off
        self.e3 = self.mod.PhaseReference

    def tearDown(self): pass
    
    def testFromString(self):
        self.assertEquals(self.e1, self.mod.fromString('On'))
        self.assertEquals(self.e2, self.mod.fromString('Off'))
        self.assertEquals(self.e3, self.mod.fromString('PhaseReference'))
        
        try:
            self.assertEquals(self.e3, self.mod.fromString('Wrong'))
        except BadParameterEx, ex: pass
            
    
    def testToString(self):
        self.assertEquals('On', self.mod.toString(self.e1))
        self.assertEquals('Off', self.mod.toString(self.e2))
        self.assertEquals('PhaseReference', self.mod.toString(self.e3))
        
    def testHasHelp(self):
        if self.mod.hasHelp():
            mod_members = inspect.getmembers(self.mod)
            self.assertTrue('help' in  mod_members and inspect.isfunction(mod_members['help']))
            self.assertTrue(len(self.mod.help(self.e1)) > 0)
            self.assertTrue(len(self.mod.help(self.e2)) > 0)
            self.assertTrue(len(self.mod.help(self.e3)) > 0)
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(AssociatedFieldNatureTest("testFromString"))
    suite.addTest(AssociatedFieldNatureTest("testToString"))
    suite.addTest(AssociatedFieldNatureTest("testHasHelp"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___    
