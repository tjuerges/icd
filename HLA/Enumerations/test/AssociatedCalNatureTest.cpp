/******************************************************************************** ALMA - Atacama Large Millimiter Array
* (c) Institut de Radioastronomie Millimetrique, 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dbroguie 2007-06-27  created
*/

#include <iostream>
#include <vector>
//#include <stdlib.h>

#include <almaEnumerations_IFC.h>
#include "CAssociatedCalNature.h"

using namespace std;
using namespace AssociatedCalNatureMod;

int main(int argc, char *argv[]) {
    cout<<" AssociatedCalNatureTest"<<endl;

// Specifying an enumerated item
    cout<<"\nSpecifying an enumerated item"<<endl;
    AssociatedCalNatureMod::AssociatedCalNature a = AssociatedCalNatureMod::ASSOCIATED_EXECBLOCK;   // with namespace

    cout<<"a="<<a<<endl;

// Specifying an enumerated item from string
    cout<<"\nSpecifying an enumerated item from string"<<endl;
    AssociatedCalNature x = CAssociatedCalNature::newAssociatedCalNature("ASSOCIATED_EXECBLOCK");
    cout <<"x="<<x<<endl;

//  Specifying an enumerated item from integer
    cout<<"\nSpecifying an enumerated item from integer"<<endl;
    unsigned int k = 0;
    AssociatedCalNature y = CAssociatedCalNature::from_int(k);
    cout << "y=" << y << ", name = " << CAssociatedCalNature::name(y) << endl;

// Accessing an enumerated item as a string
    cout<<"\nAccessing an enumerated item as a string with toString() method"<<endl;
    cout<<CAssociatedCalNature::toString(a)<<endl;

    cout<<"\nAccessing an enumerated item as a string with name() method"<<endl;
    cout<<CAssociatedCalNature::name(a)<<endl;

    cout<<"\nAccessing the help string with help() method"<<endl;
    cout<<CAssociatedCalNature::help(a)<<endl;

// Accessing the set of  enumerated item
    cout<<"\nAccessing the set of  enumerated item"<<endl;
    const vector<string> enumSet = CAssociatedCalNature::sAssociatedCalNatureSet();
    for (unsigned i=0;i<enumSet.size();i++)
	cout<<enumSet[i]<<endl;

// Comparison operations on enumerated items
    cout<<"\nComparison operations on enumerated items"<<endl;
    if ( a == AssociatedCalNatureMod::ASSOCIATED_EXECBLOCK)
	cout<<" Comparison true"<<endl;
    //    if ( b != AssociatedCalNatureMod::ATM_Model)
    //	cout<<" Comparison true"<<endl;


// Other methods: isNumber(), isHelp()
    cout<<"\nOther methods: isNumber(), isHelp()"<<endl;
    cout<<"CAssociatedCalNature::isNumber()="<<CAssociatedCalNature::isNumber()<<endl;
    cout<<"CAssociatedCalNature::isHelp()="<<CAssociatedCalNature::isHelp()<<endl;


    exit(0);
}
