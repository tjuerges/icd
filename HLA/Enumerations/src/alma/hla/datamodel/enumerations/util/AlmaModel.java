/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.enumerations.util;

import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.util.MMUtil;

import alma.hla.datamodel.enumerations.meta.AlmaEnumeration;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class AlmaModel extends Model 
{
	private static String cvsRevision;
	private static String cvsBranch;
	private static String modelFile;
	private static String lastModifiedDate;
	
	private static ElementSet itsEnumerations =MMUtil.findAllInstances(AlmaEnumeration.class);

	public static final String release=release();
	
	private static String release() {
		// Computes the value of release.
		int dummy = -9999;
		for (Object o : itsEnumerations)
			dummy = Math.max(dummy, ( (AlmaEnumeration)o).version());
		return Integer.toString(dummy);		
	}
	
	public AlmaModel() {	
		;
	}
	
	public ElementSet AllElementsOfModel() {
		ElementSet es = MMUtil.findAllInstances();
		return es;
	}
	
	public ElementSet Enumerations() {
		return MMUtil.findAllInstances(AlmaEnumeration.class);
	}
	
	public ElementSet SortedEnumerations() {
		return MMUtil.sortByName(MMUtil.findAllInstances(AlmaEnumeration.class));
	}

	public static void setCvsRevision(String Version) {
		cvsRevision = Version;
	}
	
	public static String CvsRevision () {
		return cvsRevision;
	}

	public String ModelFile() {
		return modelFile;
	}

	public static void setModelFile(String modelFile) {
		AlmaModel.modelFile = modelFile;
	}

	public String LastModifiedDate() {
		return lastModifiedDate;
	}

	public static void setLastModifiedDate(String lastModifiedDate) {
		AlmaModel.lastModifiedDate = lastModifiedDate;
	}

	public static String CvsBranch() {
		return cvsBranch;
	}

	public static void setCvsBranch(String cvsBranch) {
		AlmaModel.cvsBranch = cvsBranch;
	}
}
