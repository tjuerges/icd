package alma.hla.datamodel.enumerations.meta;

import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Enumeration;
import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;
import alma.hla.datamodel.enumerations.util.AlmaModel;

import java.util.Iterator;

public class AlmaEnumeration extends Enumeration {

	// public boolean isInteger() {
	// String thisLit = "";
	// for (Iterator i=Literal().iterator();i.hasNext();) {
	// thisLit = ((EnumerationLiteral)i.next()).NameS();
	// try {
	// Integer.parseInt(thisLit);
	// } catch (NumberFormatException e) {
	// return false;
	// }
	// return true;
	// }
	// return false; // Don't see how this can happen. (alma.chk checks for > 0
	// literals)
	// }

	private EnumerationLiteral versionLiteral() {
		for (Iterator it = Literal().iterator(); it.hasNext();) {
			EnumerationLiteral eLit = (EnumerationLiteral) it.next();
			if (((AlmaEnumerationLiteral) eLit).isVersionLiteral()) {
				return eLit;
			}
		}
		return null;
	}

	public int version() {
		EnumerationLiteral eLit = versionLiteral();
		return Integer.parseInt(eLit.NameS().split("=")[1]);
	}
	
	public String xmlns() {
		for (Iterator it = Literal().iterator(); it.hasNext();) {
			EnumerationLiteral eLit = (EnumerationLiteral) it.next();
			if (((AlmaEnumerationLiteral) eLit).isXMLNSLiteral()) {
				return eLit.NameS().split("=")[1];
			}
		}
		return null;
	}

	public String revision() {
		return AlmaModel.CvsRevision();
	}

	public String release() {
		return AlmaModel.release;
	}
	
	/**
	 * 
	 * @return an ElementSet of literal for which isVersionLiteral is false.
	 * 
	 */
	public ElementSet AlmaLiteral() {
		ElementSet aeLits = new ElementSet();
		for (Iterator it = Literal().iterator(); it.hasNext();) {
			Object o = it.next();
			if (!((AlmaEnumerationLiteral) o).NameS().startsWith("$")) {
				aeLits.add(o);
			}
		}
		return aeLits;
	}

	public String LatexName() {
		return formatForLatex(NameS());
	}

	private String formatForLatex(String x) {

		return x.replaceAll("_", "\\\\_");
	}

}
