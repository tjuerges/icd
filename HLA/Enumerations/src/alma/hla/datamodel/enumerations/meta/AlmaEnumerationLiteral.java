package alma.hla.datamodel.enumerations.meta;

import java.util.Iterator;

import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;

public class AlmaEnumerationLiteral extends EnumerationLiteral {

	private int intValue;
	public boolean hasAnIntValue = false;

	public AlmaEnumerationLiteral() {
	}

	public boolean isVersionLiteral() {
		return NameS().matches("\\$VERSION\\$=[0-9]+");
	}

	public boolean isXMLNSLiteral() {
		return NameS().matches("\\$XMLNS\\$=[a-zA-Z]+");
	}
	
	public String LatexName() {
		return formatForLatex(NameS());
	}

	private String formatForLatex(String x) {
		return x.replaceAll("_", "\\\\_");
	}
}
