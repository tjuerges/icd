#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <dirent.h>

using namespace std;

/** 
    Utility to set the leading letter in a token in lower case. 
    Used to transform a type name into an element name, e.g.
    BaselineFlag into baselineFlag, the convetion being that
    types start always with a letter in upper case and object
    with  a letter in lower case
*/ 
#if     !defined(_TOUC_H)
static string touc(string s, unsigned int n) {
  if(n>s.length())n=s.length();
  string t=s.substr(0,n);
  char* ch = new char[n+1]; sprintf(ch,"%s",t.c_str());
  for(unsigned int i=0; i<n; i++)
    if(isalpha(ch[i]))ch[i]=toupper(ch[i]);
  t = string(ch)+s.substr(n,s.length()-1);
  delete ch;
  return t;
}
#define _TOUC_H
#endif

class Enumerator{
public:
  Enumerator(string name, string desc, string value):name_(name),desc_(desc),value_(value){}
  string name() { return name_;  }
  string desc() { return desc_;  }
  string value(){ return value_; }
private:
  string name_;
  string desc_;
  string value_;
};

class Enumeration{
public:
  void setTypename(string typeName)           { typeName_ = typeName;                }
  void setCompositionParam(int cp)            { compositionParam_=cp;                }
  void appendEnumerator(Enumerator enumerator){ v_enumerator_.push_back(enumerator); }
  void setOrder(string order)                 { order_    = order;                   }
  void setValueType(string vt)                { valueType_= vt;                      }
  void setNamespace(string ns)                { nameSpace_ = ns;                     }
  void setExtension(string ex)                { extension_ = ex;                     }
  void setFirst(string first)                 { first_    = first;                   }
  void setLast(string last)                   { last_     = last;                    }
  void setCategory(string category)           { category_ = category;                }
  void setDescription(string desc)            { desc_     = desc;                    }
  void setEnumerators(vector<Enumerator> v)   { v_enumerator_ = v;                   }

  int                numEnumerator()          { return v_enumerator_.size();         }
  string             typeName()               { return typeName_;                    }
  vector<Enumerator> getEnumerators()         { return v_enumerator_;                }
  string             getValueType()           { return valueType_;                   }
  string             getNamespace()           { return nameSpace_;                   }
  string             getExtension()           { return extension_;                   }
  int                getCompositionParam()    { return compositionParam_;            }
  int                numSubenumerator() {
    int k=0;
    for(int n=0; n<numEnumerator(); n++)
      if(v_enumerator_[n].name()==first_)
	k=1;
      else if(k>0&&v_enumerator_[n].name()!=last_)
	k++;
      else if(v_enumerator_[n].name()==last_)
	return k+1;
    return 0; // should never exit from here
  }

  void clear(){
    category_         = "";
    typeName_         = "";
    compositionParam_ = 0;
    v_enumerator_.clear();
    desc_             = "";
    order_            = "";
    valueType_        = "";
    nameSpace_        = "";
    extension_        = "";
    first_            = "";
    last_             = "";
  }

  string warningPreamble(){
    ostringstream os;
    os<<"/*\n"
      <<" *\n"
      <<" * /////////////////////////////////////////////////////////////////\n"
      <<" * // WARNING!  DO NOT MODIFY THIS FILE!                          //\n"
      <<" * //  ---------------------------------------------------------  //\n"
      <<" * // | This is generated code using a C++ template function!   | //\n"
      <<" * // ! Do not modify this file.                                | //\n" 
      <<" * // | Any changes will be lost when the file is re-generated. | //\n"
      <<" * //  ---------------------------------------------------------  //\n"
      <<" * /////////////////////////////////////////////////////////////////\n"
      <<" *\n"
      <<" */"
      <<"\n\n";

    return os.str();
  }

  string include_enum() { ostringstream os;
                          os<<"#if     !defined(_"<<touc(typeName_,typeName_.length())<<"_H)\n\n";
			  os<<"#include <C"<<typeName_<<".h>\n";
			  os<<"#define _"<<touc(typeName_,typeName_.length())<<"_H\n";
			  os<<"#endif \n";
			  return os.str();
  }

  string classic_enum() { ostringstream os;
                          os<<"#if     !defined(_"<<touc(typeName_,typeName_.length())<<"_H)\n\n";
			  os<<"enum "<<typeName_<<" {\n";
			  for(unsigned int n=0; n<v_enumerator_.size()-1; n++)
			    os<<"   "<<v_enumerator_[n].name()<<",\n";
			  os<<"   "<<v_enumerator_[v_enumerator_.size()-1].name()<<" };\n\n";
			  os<<"#define _"<<touc(typeName_,typeName_.length())<<"_H\n";
			  os<<"#endif";
			  return os.str();
  }

  string enum_set_traits(bool mkhpp) { ostringstream os;
                                       if(mkhpp)
					 os<<"#if     !defined(_"<<touc(typeName_,typeName_.length())<<"_HPP)\n\n";
				       else
					 os<<"#if     !defined(_"<<touc(typeName_,typeName_.length())<<"_HH)\n\n";
				       os<<"#include \"Enum.hpp\"\n\n";
				       os<<"using namespace "<<nameSpace_<<";\n\n";
				       if(valueType_!="void"){
					 os<<"#include \""<<valueType_<<"."<<extension_<<"\"\n";
					 if(nameSpace_.length())
					   os<<"using namespace "<<nameSpace_<<";\n\n";
				       }
				       os<<"template<>\n struct enum_set_traits<"<<nameSpace_<<"::"<<typeName_<<">"; 
				       return os.str();
  }

  string enum_set_traiter(){  ostringstream os;
                              os<<"enum_set_traiter<"<<nameSpace_<<"::"<<typeName_<<","
				<<compositionParam_<<","<<nameSpace_<<"::"<<last_<<">"<<" {};";
			      return os.str();  }

  string enum_map_traits() { if(!valueType_.length())valueType_="void";
                             string emt= "class enum_map_traits<"+nameSpace_+"::"+typeName_+","+valueType_+">";
                             return "template<>\n"+emt;}
  string enum_map_traiter_def(bool mkhpp){ string emt= "enum_map_traits<"+nameSpace_+"::"+typeName_+","+valueType_+">";
                                 ostringstream os; 
				 os<< "enum_map_traiter<"<<nameSpace_<<"::"<<typeName_<<","<<valueType_<<"> {\n"
				   <<"public:\n"
				   <<"  static bool   init_;\n"
				   <<"  static string typeName_;\n"
				   <<"  static string enumerationDesc_;\n"
				   <<"  static string order_;\n"
				   <<"  static string xsdBaseType_;\n"
				   <<"  static bool   init(){\n"
				   <<"    EnumPar<void> ep;\n";
				 int nb=0;
				 int ne;
				 for(int n=0; n<numEnumerator(); n++){
				   if(v_enumerator_[n].name()==first_)nb=n;
				   if(v_enumerator_[n].name()==last_)ne=n;
				 }
				 for(int n=nb; n<=ne; n++){
				   os<<"    m_.insert(pair<"<<nameSpace_<<"::"<<typeName_<<",EnumPar<"<<valueType_<<"> >\n"
				     <<"     ("<<nameSpace_<<"::"<<v_enumerator_[n].name()
				     <<",ep((int)"<<nameSpace_<<"::"<<v_enumerator_[n].name()
				     <<",\""<<v_enumerator_[n].name()<<"\",\""
				     <<v_enumerator_[n].desc()<<"\"";
				   if(valueType_!="void")
				     os<<","<<v_enumerator_[n].value();
				   os<<")));\n";
				 }
				 os<<"    return true;\n";
				 os<<"  }\n"
				   <<"  static map<"<<nameSpace_<<"::"<<typeName_<<",EnumPar<"<<valueType_<<"> > m_;\n"
				   <<"};\n";
				 if(mkhpp){
				   os<<"template<> string "<<emt<<"::typeName_=\""<<typeName_<<"\";\n"
				     <<"template<> string "<<emt<<"::enumerationDesc_=\""<<desc_<<"\";\n"
				     <<"template<> string "<<emt<<"::order_=\""<<order_<<"\";\n"
				     <<"template<> string "<<emt<<"::xsdBaseType_=\""<<valueType_<<"\";\n"
				     <<"template<> map<"<<typeName_<<",EnumPar<"<<valueType_<<"> > "<<emt<<"::m_;\n"
				     <<"template<> bool   "<<emt<<"::init_=init();\n\n";
				   os<<"#define _"<<touc(typeName_,typeName_.length())<<"_HPP\n";
				 }else{
				   os<<"#define _"<<touc(typeName_,typeName_.length())<<"_HH\n";
				 }
				 os<<"#endif";
				 return os.str();  }

  string enum_map_traiter_imp(){ string emt= "enum_map_traits<"+nameSpace_+"::"+typeName_+","+valueType_+">";
                                 ostringstream os;
                                 os<<"#include \""<<typeName_<<".h\"\n\n";
//                                  os<<"#include \""<<typeName_<<"_essai_.h\"\n\n";
				 os<<"string "<<emt<<"::typeName_=\""<<typeName_<<"\";\n"
				   <<"string "<<emt<<"::enumerationDesc_=\""<<desc_<<"\";\n"
				   <<"string "<<emt<<"::order_=\""<<order_<<"\";\n"
				   <<"string "<<emt<<"::xsdBaseType_=\""<<valueType_<<"\";\n"
				   <<"map<"<<nameSpace_<<"::"<<typeName_<<",EnumPar<"<<valueType_<<"> > "<<emt<<"::m_;\n"
				   <<"bool   "<<emt<<"::init_=init();\n\n";
				 return os.str();  }

private:
  string             category_;
  string             typeName_;
  int                compositionParam_;
  vector<Enumerator> v_enumerator_;
  string             desc_;
  string             order_;
  string             valueType_;
  string             nameSpace_;
  string             extension_;
  string             first_;
  string             last_;
};


void
usage(void)
{
  cout << "Usage:   mk_enumfromhla [option] <path>  [-v] [-hpp] "                 <<endl;
  cout << "  where <path> is the src directory path of the HLA enumerations"      <<endl;
  cout << "Options: [-help]    this help"                                         <<endl;
  cout << "Options: [-v]       mode verbose"                                      <<endl;
  cout << "         [-src2inc] to write the generated include files in ../include"<<endl;
  cout << "  the standard location relative to the src directory where should  "  <<endl;
  cout << "  be the present code generator utility in the ALMA installations.  "  <<endl;
  cout << "         [-hpp]     to tell to generate hpp include files, the default"<<endl;
  cout << "  being to generate pairs of files, the .h definition files with "     <<endl;
  cout << "  their respective .cpp implementation files."                         <<endl;
  cout << "  The convention is that with hpp files contain both the definition "  <<endl;
  cout << "  and the implementation."                                             <<endl;
  cout << "Examples:"                                                             <<endl;
  cout << " mk_enumfromhla /soft/ALMA/ICD/HLA/Enumerations/src/"                  <<endl;
  cout << " mk_enumfromhla -v /soft/ALMA/ICD/HLA/Enumerations/src/"               <<endl;
  cout << " mk_enumfromhla -src2inc -v /soft/ALMA/ICD/HLA/Enumerations/src/"      <<endl;
  cout << " mk_enumfromhla -hpp /soft/ALMA/ICD/HLA/Enumerations/src/"             <<endl;
}

int main(int argc, char *argv[]){

  string srcDir;

  bool   verbose = false;
  bool   brkloop = false;
  bool   src2inc = false;
  bool   mkhpp   = false;
  int i =1;
  for (;i<argc && !brkloop;){
    switch(argv[i][0]){
    case '-':             // option
      {
	std::string opt(argv[i]+1);
	cout<<"opt="<<opt<<endl;
	if(opt=="v"){
	  verbose = true;
	  i++;
	}else if(opt.find("hpp",0)!=string::npos){
	  mkhpp   = true;
	  i++;
	}else if(opt.find("src2inc",0)!=string::npos){
	  src2inc = true;
	  i++;
	}else if (opt.find("help",0)!=string::npos){
	  usage();
	  exit(0);
	}
	break;
      }
    default:
      brkloop = true;    // end of options
      break;
    }
  }

  if (i < argc){
    srcDir = argv[i];
    i++;
  }else{
    usage();
    return 2;
  }

  // complexType element names:
  string EnumDefinitionsN  = "enumDefinitions";
  string EnumerationsN     = "enumerations";
  string EnumerationN      = "enumeration";
  string SubenumerationN   = "subenumeration";

  // simpleContent element names:
  string DescriptionN      = "description";
  string ValueTypeN        = "valueType";
  string EnumeratorN       = "enumerator";

  bool        subenum=false;
  Enumeration enumeration;
  Enumeration subenumeration;

  // what is possibly common to several consecutive enumerations
  string             category;


  string             typeName;
  int                compositionParam;
  string             desc;
  string             order;
  string             valueType;
  string             first;
  string             last;

  vector<string> v_literalName;
  vector<string> v_literalDesc;
  string         value;

  string fileName;     // = "CAntennaType.cpp";
  string className;    // = fileName.substr(1,fileName.length()-5);
  string dfn;          //= srcDir+fileName;  cout<<dfn<<endl;

  string row;
  char   delimiter='\n';
  int    p,b,e;
  bool   endoffile=false;

  struct dirent **namelist;
  int nf;



  nf = scandir(srcDir.c_str(), &namelist, 0, alphasort);
  if (nf < 0)
    perror("scandir");
  else {
    while(nf--) {
      fileName=namelist[nf]->d_name;
      if(fileName.find("SA.cpp",0)!=string::npos){
	//cout<<fileName<<endl;

	className=fileName.substr(1,fileName.length()-8);    // _SA.cpp  requires 7
	dfn=srcDir+"/"+fileName;                          if(verbose)cout<<dfn<<endl;
	ifstream input(dfn.c_str(),ios_base::in);     
	if(!input){
	  cerr<<"WARNING:  Can not open! "<<input<<endl;
	  return -1;
	}
	subenum=false;
	category="";
	enumeration.setCategory(category);
	enumeration.setTypename(className);
	enumeration.setDescription("");

	endoffile=false;
	while(!endoffile){
	  getline(input, row,  delimiter);
// 	  cout<<row<<endl;
    
	  // get the literal names
	  p = row.find("::s",20);
	  if( row.find("const std::string&",0)!=string::npos && p!=string::npos){
	    b=row.find("\"",0); e=row.find("\"",++b);
	    v_literalName.push_back(row.substr(b,e-b));
	  }
	  // get the description of these enumerators
	  p = row.find("::h",0);
	  if( row.find("const std::string&",0)!=string::npos && p!=string::npos){
	    b=row.find("\"",0); e=row.find("\"",++b);
	    v_literalDesc.push_back(row.substr(b,e-b));
	  }
	  if(v_literalName.size()>0 && v_literalDesc.size()==v_literalName.size())break;
	  p  = row.find("::name(const",0); if(p!=string::npos)break;
	  p  = row.find("throw",0); if(p!=string::npos)break;
	}
// 	for(unsigned int i=0; i<v_literalName.size(); i++)cout<<v_literalName[i]<<endl;
// 	for(unsigned int i=0; i<v_literalDesc.size(); i++)cout<<v_literalDesc[i]<<endl;
	enumeration.setCompositionParam(v_literalName.size());       // contraint unknown ==> accept all
	enumeration.setFirst(v_literalName[0]);                      // contraint unknown
	enumeration.setLast(v_literalName[v_literalName.size()-1]);  // contraint unknown ==> assume full enum
	enumeration.setValueType("void");                            // no assoc typed values for HLA enums 
	enumeration.setNamespace(className+"Mod");                   // for the HLA there is one namspace per enum!
	enumeration.setExtension("");                                // no need as there is no assoc typed values

	value=""; // no value associated to the literals with the HLA enums
	if(v_literalDesc.size()==0)
	  for(unsigned int n=0; n<v_literalName.size(); n++)v_literalDesc.push_back("un-documented");
	for(unsigned int n=0; n<v_literalName.size(); n++){
	  enumeration.appendEnumerator(Enumerator( v_literalName[n], v_literalDesc[n], value));
	}

	string outfile;
	if(src2inc){
	  if(mkhpp)
	    outfile="../include/"+enumeration.typeName()+".hpp";
	  else
	    outfile="../include/"+enumeration.typeName()+".h";
	}else{
	  if(mkhpp)
	    outfile=enumeration.typeName()+".hpp";
	  else
	    outfile=enumeration.typeName()+".h";
	}
	ofstream output_hpp(outfile.c_str(),ios_base::out);
	if(!output_hpp){
	  cerr<<"FATAL: Can not open! "<<outfile<<endl;
	  return -1;
	}  
	output_hpp<<enumeration.warningPreamble()<<endl;
	output_hpp<<enumeration.include_enum()<<endl;
	output_hpp<<enumeration.enum_set_traits(mkhpp)
		  <<" : public "<<enumeration.enum_set_traiter()<<endl;
	output_hpp<<endl;
	output_hpp<<enumeration.enum_map_traits()
		  <<" : public "<<enumeration.enum_map_traiter_def(mkhpp)<<endl;
	output_hpp.close();

	// the implementation part (cpp file)
	if(!mkhpp){
// 	  outfile=enumeration.typeName()+"_essai_.cpp";
 	  outfile=enumeration.typeName()+".cpp";
	  ofstream output_cpp(outfile.c_str(),ios_base::out);
	  if(!output_cpp){
	    cerr<<"FATAL: Can not open! "<<outfile<<endl;
	    return -1;
	  }  
	  output_cpp<<enumeration.enum_map_traiter_imp()<<endl;
	  output_cpp.close();
	}
	enumeration.clear();
	v_literalName.clear();
	v_literalDesc.clear();

	input.close();

      }

      free(namelist[nf]);
    }
    free(namelist);
  }

  return 0;
}
