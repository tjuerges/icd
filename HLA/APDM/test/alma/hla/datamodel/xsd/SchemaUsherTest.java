/*
 * Created on Mar 28, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.xsd;

import junit.framework.TestCase;
import alma.hla.datamodel.xsd.SchemaUsher;

/**
 * @author jschwarz
 *
 * These tests start by checking the hard-coded results in SchemaUsher.
 * They will help when SchemaUsher is refactored to use a configuration file or
 * some other technique that is less ugly than what we've got now.
 */
public class SchemaUsherTest extends TestCase {
	public void testObsUnit() {
		SchemaUsher.setConfigFile("mappings/schemaUsherConfig.xml");
		String obsProject = SchemaUsher.getXsdNamespace("data/obsproject/ObsProject");
		assertEquals("Alma/ObsPrep/ObsProject",obsProject);
		String abbrev = SchemaUsher.getXsdNamespaceAlias("data/obsproject/ObsProject");
		assertEquals("prj",abbrev);
		String obsProposal = SchemaUsher.getXsdNamespace("data/obsproject/ObsProposal");
		assertEquals("Alma/ObsPrep/ObsProposal",obsProposal);
	}
}
