/*
 * Created on Sep 22, 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.runtime;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import junit.framework.TestCase;

public class DatamodelInstanceCheckerTest extends TestCase {
	
	private DatamodelInstanceChecker dic;
	
	@Override
	protected void setUp() throws Exception {
		dic = new DatamodelInstanceChecker();
		super.setUp();
	}
	
	public String getDatamodelVersion(String filename) throws IOException
	{
		final int BUFSIZ = 2000;
		char[] cbuf = new char[BUFSIZ];
		StringBuffer sbuf = new StringBuffer();

		FileReader frdr = new FileReader(filename);
		int charsRead = 0;
		while((charsRead=frdr.read(cbuf, 0, BUFSIZ))!=-1) {
			sbuf.append(cbuf, 0, charsRead);
		}

		StringReader rdr = new StringReader(sbuf.toString());
		
		return dic.getDatamodelVersion(rdr);
	}
	
	public void testExistingVersionNumber () throws IOException {
		
		String dmv = getDatamodelVersion("ObsProject.xml");
		assertEquals("1.54", dmv);
		
		dmv = getDatamodelVersion("ObsProposal.xml");
		assertEquals("1.54", dmv);
		
		dmv = getDatamodelVersion("SchedBlock0.xml");
		assertEquals("1.54", dmv);
	}
	
	public void testNoVersionNumberPresent() throws IOException {
		String dmv = getDatamodelVersion("ObsProjectNoVers.xml");
		assertEquals("-1", dmv);
		dmv = getDatamodelVersion("ObsProposalNoVers.xml");
		assertEquals("-1", dmv);
		dmv = getDatamodelVersion("SchedBlock0NoVers.xml");
		assertEquals("-1", dmv);
	}
	
	public void testBadInput() {
		try {
			dic.getDatamodelVersion(new StringReader("<xqyz/>"));
		} catch (IOException e) {
			return;
		}
		fail("No exception returned from garbage XML input");
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
