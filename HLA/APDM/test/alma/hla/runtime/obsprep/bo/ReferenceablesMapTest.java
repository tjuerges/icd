package alma.hla.runtime.obsprep.bo;

import junit.framework.TestCase;
import alma.entities.commonentity.EntityT;
import alma.hla.runtime.obsprep.util.CannotAssignIdException;
import alma.obsprep.bo.schedblock.FieldSource;
import alma.obsprep.bo.schedblock.PhaseCalParameters;
import alma.obsprep.bo.schedblock.SchedBlock;
import alma.obsprep.bo.schedblock.Target;
import alma.valuetypes.SkyCoordinates;

public class ReferenceablesMapTest extends TestCase {

	public void testMap() {

		SchedBlock sb;
		Target trg;
		PhaseCalParameters pcp;
		FieldSource fs;

		ReferenceablesMap map = new ReferenceablesMap();
		EntityIDFactory.setRuntimeImplementation(new EntityIDFactory() {
			int partIDs = 0;
			int entityIDs = 0;
			protected String doAssignUniqueEntityId(EntityT e) throws Exception {
				System.out.println("Assigned entityID " + entityIDs);
				String entityId = "id-" + entityIDs++;
				e.setEntityId(entityId);
				return entityId;
			}
			public void destroy() {
			}
			public String assignEntityPartId(EntityPart entPart)
					throws CannotAssignIdException {
				System.out.println("Assigned partID " + partIDs);
				String entityPartId = "id-" + partIDs++;
				entPart.setEntityPartId(entityPartId);
				return entityPartId;
			}
		});

		// at the beginning we don't get anything
		assertNull(map.get(null, null));
		assertNull(map.get("1", "2"));

		// Create the SB with some things inside
		sb = SchedBlock.createSchedBlock();
		sb.addTarget(trg = Target.createTarget());
		sb.addObservingParameters(pcp = PhaseCalParameters.createPhaseCalParameters());
		trg.addObservingParameters(pcp);
		sb.addFieldSource(fs = FieldSource.createFieldSource());
		fs.setSourceCoordinates(SkyCoordinates.createSkyCoordinates());

		// Simple, but effective: put the SB, retrieve it
		map.put(sb.getEntityID(), null, sb);
		assertSame(sb, map.get(sb.getEntityID(), null));

		// Put now the entity parts
		map.put(null, trg.getEntityPartId(), trg);
		map.put(null, pcp.getEntityPartId(), pcp);
		map.put(null, fs.getEntityPartId(), fs);

		// Now, let's retrieve the same things
		assertSame(trg, map.get(null, trg.getEntityPartId()));
		assertSame(pcp, map.get(null, pcp.getEntityPartId()));
		assertSame(fs, map.get(null, fs.getEntityPartId()));
		assertSame(sb, map.get(sb.getEntityID(), null));
		
	}

}
