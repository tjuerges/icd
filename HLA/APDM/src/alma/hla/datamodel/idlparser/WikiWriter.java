/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2004
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.idlparser;

import java.io.PrintWriter;

/**
 * Generates a wiki table from an <code>IdlStructInfo</code>.
 * 
 * @author hsommer
 * created 13.11.2005 17:35:39
 */
public class WikiWriter {

	private PrintWriter writer;

	WikiWriter(PrintWriter writer) {
		this.writer = writer;
	}
	
	
	/**
	 * Generates a level-3 wiki header from the qualified name of the event struct.
	 * The name is expected to consist of possibly nested module names separated by "/",
	 * and "::" to separete module from struct name.
	 * To avoid unwanted links to be created from names that contain CamelCase, 
	 * &lt;nop&gt; is inserted generously.
	 *  
	 * @param qualifiedNameWithoutPrefix
	 */
	void printEventStructHeading(String qualifiedNameWithoutPrefix) {
		String wikiQualifiedName = "<nop>" + qualifiedNameWithoutPrefix;
// Done: we now use the following two lines, and have removed the loop below,
//		since we've migrated to ACS 5.0	(JDK 1.5)
		wikiQualifiedName = wikiQualifiedName.replace("/", "/<nop>");
		wikiQualifiedName = wikiQualifiedName.replace("::", "::<nop>");		
//		StringBuffer buff = new StringBuffer(wikiQualifiedName.length());
//		for (int i = 0; i < wikiQualifiedName.length(); i++) {
//			char current = wikiQualifiedName.charAt(i);
//			if (current == '/') {
//				buff.append("/<nop>");
//			}
//			else if (current == ':' && i < wikiQualifiedName.length()-1 && wikiQualifiedName.charAt(i+1) == ':') {
//				buff.append("::<nop>");
//				i++;
//			}
//			else {
//				buff.append(current);
//			}
//		}
//		wikiQualifiedName = buff.toString();
		writer.println("---+++ " + wikiQualifiedName);
	}
	
	
	/**
	 * Produces wiki output of the kind
	 * <verbatim>
	 * ---+++ <nop>TelCalPublisher::<nop>SkydipReducedEvent
	 * | *field* | *type* | *typedef* | *single size in bytes* | *total size in bytes* |
	 * | execBlockId | string | UID | 33 | 33 |
	 * | finishedAt | ULONGLONG | Time | 8 | 8 |
	 * | scanNum | LONG | | 4 | 4 |
	 * | status | string | | ??? | ??? |
	 * | antennaList | sequence [length=???] of string | AntennaId | 33 | ??? |
	 * | teOffsetBitMask | sequence [length=4] of LONG | | 4 | 20 |
	 * |%RED% _Total size of event struct_ %ENDCOLOR%| | | | %CALC{"$SUM( $ABOVE() )"}% |
	 * </verbatim>
	 * @param structInfo
	 */
	void printEventStructTable(IdlStructInfo structInfo) {
		writer.println("| *field* | *type* | *typedef* | *single size in bytes* | *total size in bytes* |");		
		
		IdlStructMemberInfo[] structMembers = structInfo.getStructMembers();
		for (int i = 0; i < structMembers.length; i++) {
			// field name
			writer.print("| " + structMembers[i].getName() + " ");
			// name of basic type 
			writer.print("| ");
			if (structMembers[i].isSequence()) {
				writer.print("sequence [length=" + lengthToString(structMembers[i].getSequenceLength()));
				writer.print("] of ");
			}
			writer.print(structMembers[i].getUnwoundTypeName() + " ");
			// optional typedef
			writer.print("| ");
			if (structMembers[i].getDefinedTypeName() != null) {
				writer.print("<nop>" + structMembers[i].getDefinedTypeName() + " ");
			}
			// single size
			writer.print("| " + lengthToString(structMembers[i].getSingleValueSizeInOctets()) + " ");
			// total size
			writer.println("| " + lengthToString(structMembers[i].getTotalSizeInOctets()) + " |");			
		}
		
		int eventStructSize = structInfo.getSizeInOctets();			
		if (eventStructSize < 0) {
			writer.print("|%RED% _Total size of event struct_ %ENDCOLOR%");
		}
		else {
			writer.print("| _Total size of event struct_ ");
		}
		writer.println("| | | | %CALC{\"$SUM( $ABOVE() )\"}% |");
		writer.println();		
	}
	
	
	String lengthToString(int length) {
		if (length > 0) {
			return Integer.toString(length);
		}
		else {
			return "???";
		}
	}
}
