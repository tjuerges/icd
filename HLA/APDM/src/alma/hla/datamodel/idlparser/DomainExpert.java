/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.idlparser;

import org.openorb.compiler.object.IdlStruct;

/**
 * Class that encapsulates knowledge about ALMA which can't be directly derived from IDL.
 * 
 * @author hsommer
 */
public class DomainExpert {

	/**
	 * Returns the lengths of known strings that are typedef'd in the IDL.
	 * If the typedef is not known, -1 will be returned.
	 * @param typedefName
	 * @return
	 */
	int getKnownStringLength(String typedefName) {		
		if (typedefName == null) {
			return -1;
		}

		int lengthInBytes = -1;
		
		if (typedefName.equals("AntennaId") ||
			typedefName.equals("ArrayId") ||
			typedefName.equals("EntityId") ||
			typedefName.equals("ExecBlockId") ||
			typedefName.equals("QdEntityId") ||
			typedefName.equals("SessionId") ||
			typedefName.equals("UID") ) {
			return 33; // UID format will change soon, keep checking with archive team
		}
		else if (typedefName.equals("PropertyName")) {
			return 100;
		}
				
		return lengthInBytes;
	}
	
	
	boolean isEventStruct(IdlStruct struct) {		
		boolean ret = false;
		if (struct != null) {
			String name = struct.name();
			if (name != null) {
				// Take structs that have "event" in their name
				ret = ( name.toLowerCase().indexOf("event") >= 0 );
				// exclude known false hits
				String[] idComponents = struct.getIdComponents();
				if (idComponents.length == 2 && idComponents[0].equals("CosNotification")) {
					ret = false;
				}
				else if (idComponents.length == 2 && idComponents[0].equals("acsnc") && idComponents[1].equals("EventDescription")) {
					ret = false;
				}
				// include known missing structs
				else if (name.equals("ctResults_t")) { // ctResults_t seems to no longer be used as an event in CORR.... need to check
					ret = true;
				}
			}
		}
		return ret;
	}
}
