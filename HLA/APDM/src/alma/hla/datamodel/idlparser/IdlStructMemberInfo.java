/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.idlparser;


/**
 * @author hsommer
 */
public class IdlStructMemberInfo {

	private String name;
	private String definedTypeName;
	private String unwoundTypeName;
	private boolean isSequence = false;
	private int sequenceLength = -1;
	private int sizeInOctets = -1;
	private IdlStructInfo nestedStruct;

	// for type names of primitive types, see IdlScrutinizer.PrimitiveTypeInfo
	public static final String MEMBERTYPENAME_STRING = "string";
	public static final String MEMBERTYPENAME_ENUM = "enum";
	public static final String MEMBERTYPENAME_STRUCT = "struct";
	
	/**
	 * @return Returns the name.
	 */
	String getName() {
		return name;
	}

	/**
	 * @param name The name to set.
	 */
	void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the definedTypeName.
	 */
	String getDefinedTypeName() {
		return definedTypeName;
	}

	/**
	 * Sets the typedef'd name of this struct member, if applicable.
	 * <p>
	 * In case of sequences, the typedef'd name of the sequence elements must be used, 
	 * not that of the sequence itself.
	 * For example, it should be "UID" instead of "UIDSeq" for a sequence of strings that are typedef'd as "UID".
	 * @param definedTypeName The definedTypeName to set.
	 */
	void setDefinedTypeName(String definedTypeName) {
		this.definedTypeName = definedTypeName;
	}

	/**
	 * @return Returns the unwoundTypeName.
	 */
	String getUnwoundTypeName() {
		return unwoundTypeName;
	}

	/**
	 * @param unwoundTypeName The unwoundTypeName to set.
	 */
	void setUnwoundTypeName(String unwoundTypeName) {
		this.unwoundTypeName = unwoundTypeName;
	}
	
	/**
	 * @return Returns the isSequence.
	 */
	boolean isSequence() {
		return isSequence;
	}

	/**
	 * @param isSequence The isSequence to set.
	 */
	void setSequence(boolean isSequence) {
		this.isSequence = isSequence;
	}

	/**
	 * @return Returns the sequenceLength.
	 */
	int getSequenceLength() {
		return sequenceLength;
	}

	/**
	 * @param sequenceLength The sequenceLength to set.
	 */
	void setSequenceLength(int sequenceLength) {
		this.sequenceLength = sequenceLength;
	}

	/**
	 * Gets the size of the base type in octets (bytes).
	 * If the struct member is a sequence of some other type, the sequence size is not considered here.
	 * @return Returns the sizeInOctets, or -1 if it is not known.
	 */
	int getSingleValueSizeInOctets() {
		int ret = sizeInOctets;
		if (isNestedStruct()) {
			ret = getNestedStruct().getSizeInOctets();
		}
		return ret;
	}

	/**
	 * Gets the size of the base type or sequence of base types.
	 * If the size of the base type or the length of the sequence is unknown, -1 will be returned.
	 * @return
	 */
	int getTotalSizeInOctets() {
		int ret = getSingleValueSizeInOctets();
		if (isSequence()) {
			if (getSequenceLength() > 0) {
				ret *= getSequenceLength();
				ret += 4; // unsigned long inserted to hold sequence length, see CORBA spec 15.3.2.5
			}
			else {
				ret = -1;
			}
		}
		return ret;
	}
	
	/**
	 * @see #getSingleValueSizeInOctets()
	 */
	void setSingleValueSizeInOctets(int sizeInOctets) {
		this.sizeInOctets = sizeInOctets;
	}

	
	boolean isNestedStruct() {
		return (nestedStruct != null);
	}
	
	void setNestedStruct(IdlStructInfo nestedStruct) {
		this.nestedStruct = nestedStruct;
	}
	
	IdlStructInfo getNestedStruct() {
		return nestedStruct;
	}

}
