/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.idlparser;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openorb.compiler.IdlCompiler;
import org.openorb.compiler.object.IdlIdent;
import org.openorb.compiler.object.IdlObject;
import org.openorb.compiler.object.IdlSequence;
import org.openorb.compiler.object.IdlSimple;
import org.openorb.compiler.object.IdlStruct;
import org.openorb.compiler.object.IdlStructMember;
import org.openorb.compiler.object.IdlTypeDef;
import org.openorb.compiler.parser.IdlParser;
import org.openorb.compiler.parser.IdlType;


/**
 * Analyzes IDL files.
 * Currently extracts all structs that have "event" in their name, and calculates their sizes in bytes
 * as much as undefined data types such as sequences and strings allow this.
 * <p>
 * Notes on processing Alma IDL files: 
 * <ol>
 * <li> IDL files that contain error defintions are not stored in CVS, but must be generated as part of module compilation.
 *      (It's faster to get all IDL files from an integration directory, typically under $ACSROOT/idl)
 * <li> run the <code>JacPrep</code> script (that comes with ACS) on all IDL files.
 *      This is done automatically during compilation, thus IDL files from $ACSROOT/idl don't need this any more.
 * <li> comment out <code>#include &lt;enumpropMACRO.idl&gt;</code> in all IDL files, 
 *      because unlike JacORB, OpenORB fails to parse macro definitions. 
 *      The macros have already been expanded by JacPrep, thus it is safe to not include this file.
 * </ol>
 * @author hsommer
 */
public class IDLScrutinizer
{	
	protected static IdlCompiler s_idlCompiler = new IdlCompiler();

	/**
	 * Map of all event structs. Key = (String) qualified name, value = (IdlStruct) the struct.
	 */
	protected Map allEventStructs = new LinkedHashMap();

	protected DomainExpert domainExpert = new DomainExpert();

	
	
	/** 
	 * Analyzes the event structs that were collected in the previous IDL parser runs and generates descriptions.
	 * 
	 */
	public void printEventInfo(PrintWriter printWriter)
	{
		WikiWriter wikiWriter = new WikiWriter(printWriter); 

		for (Iterator iter = allEventStructs.keySet().iterator(); iter.hasNext();) {
			String qualifiedNameWithoutPrefix = (String) iter.next();
			IdlStruct eventStruct = (IdlStruct) allEventStructs.get(qualifiedNameWithoutPrefix);
			System.out.println("\nEvent struct " + qualifiedNameWithoutPrefix);			
			wikiWriter.printEventStructHeading(qualifiedNameWithoutPrefix);
			
			// process struct content
			IdlStructInfo structInfo = analyzeStruct(eventStruct, 0);

			wikiWriter.printEventStructTable(structInfo);
			
			int eventStructSize = structInfo.getSizeInOctets();			
			String msg = "Size of event struct: ";
			if (eventStructSize < 0) {
				msg += "???";
			}
			else {
				msg += eventStructSize + " bytes.";
			}
			System.out.println(msg);
		}
	}
	
	/**
	 * Analyzes the struct content and tries to add up the sizes in bytes of the struct members.
	 * Recurses into sub-structs.
	 *   
	 * @param struct
	 * @param level
	 * @return total size in bytes, or <code>-1</code> if members of unknown size were found.
	 */
	private IdlStructInfo analyzeStruct(IdlStruct struct, int level) {
		
		IdlStructInfo structInfo = new IdlStructInfo();
		struct.reset();
		
		while (!struct.end()) {
			
			IdlObject child = struct.current();

			if (child.kind() != IdlType.e_struct_member){
				System.out.println("unexpected struct child of type " + child.kind() + " found in " + struct.getId());
				continue;
			}

			IdlStructMember member = (IdlStructMember) child; // class from OpenORB
			IdlStructMemberInfo structMemberInfo = new IdlStructMemberInfo(); // our own class, better suited for reports
			structInfo.addStructMemberInfo(structMemberInfo);

			structMemberInfo.setName(member.name());

			IdlObject type = unwindType(member.type());

//			IdlObject seqElementType = null;
			if (type.kind() == IdlType.e_sequence) {
				structMemberInfo.setSequence(true);
				structMemberInfo.setSequenceLength(((IdlSequence)type).getSize());
				
				// we continue analyzing the type of which the sequence is formed
				IdlObject seqElementType = (IdlObject) ((IdlSequence)type).internal();				
				if (seqElementType.kind() == IdlType.e_ident) {
					structMemberInfo.setDefinedTypeName(((IdlIdent)seqElementType).internalObject().name());  
				}				
				type = unwindType(seqElementType);
			}
			else { // not a sequence, just plain member
				structMemberInfo.setSequence(false);
				if (member.type().kind() == IdlType.e_ident) {
					structMemberInfo.setDefinedTypeName(((IdlIdent)member.type()).internalObject().name());
				}
			}
											
			// class name as default, just to see something... should be changed in the code below
			String typeName = type.getClass().getName();
						
			if (type.kind() == IdlType.e_simple) {
				PrimitiveTypeInfo primInfo = PrimitiveTypeInfo.fromIdlPrimitive(((IdlSimple)type).primitive());
				typeName = primInfo.getName();
				structMemberInfo.setSingleValueSizeInOctets(primInfo.getLengthInOctets());
			}
			else if (type.kind() == IdlType.e_string) {
				typeName = IdlStructMemberInfo.MEMBERTYPENAME_STRING;
				// check if a typedef'd string has a known length
				if (structMemberInfo.getDefinedTypeName() != null) {
					int knownStringLength = domainExpert.getKnownStringLength(structMemberInfo.getDefinedTypeName());
					if (knownStringLength > 0) {
						structMemberInfo.setSingleValueSizeInOctets(knownStringLength);
					}					
				}
			}
			else if (type.kind() == IdlType.e_enum) {
				typeName = IdlStructMemberInfo.MEMBERTYPENAME_ENUM;
				structMemberInfo.setSingleValueSizeInOctets(4);
			}
			else if (type.kind() == IdlType.e_struct) {
				structMemberInfo.setNestedStruct(analyzeStruct((IdlStruct)type, level + 1));
				typeName = IdlStructMemberInfo.MEMBERTYPENAME_STRUCT;
			}
			structMemberInfo.setUnwoundTypeName(typeName);
			
			String msg = "\t";
			for (int i=0; i<level; i++) {
				msg += "\t";
			}
			msg += member.name() + ": ";
			
			String typeDescription = "";
			if (structMemberInfo.isSequence()) {
				typeDescription += "sequence [length=";
				if (structMemberInfo.getSequenceLength() > 0) {
					typeDescription += structMemberInfo.getSequenceLength();
				}
				else {
					typeDescription += "???";
				}
				typeDescription += "] of ";
			}
			typeDescription += typeName;
			
			msg += typeDescription;
			System.out.println(msg);
			
			struct.next();
		}
		
		return structInfo;
	}

	
	
	
	private IdlObject unwindType(IdlObject type) {
		if (type.kind() == IdlType.e_ident) {
			type = unwindType( ((IdlIdent)type).internalObject() );
		}
		else if (type.kind() == IdlType.e_typedef) {
			type = unwindType( ((IdlTypeDef)type).type() );
		}
		return type;
	}

	
	/**
	 * Traverses the tree under <code>node</code> and puts all event struct nodes into the map.
	 * <p>
	 * Also adds structs from included IDL files. 
	 * This is necessary when analyzing many files at a time, because the include flag is set at first scanning,
	 * which means that the structs from the same file will not be reconsidered when it is the "main" idl file
	 * to be analyzed.
	 *   
	 * @param node  IDL parse tree node
	 */	
	private void collectEventStructs(IdlObject node)
	{
		if (node.kind() == IdlType.e_include || 
//			node.kind() == IdlType.e_interface || 	// some events are declared inside interfaces!
			node.kind() == IdlType.e_import	) {
			return;
		}
		
		if (node.kind() == IdlType.e_struct /* && !node.included() */)
		{
			if (domainExpert.isEventStruct((IdlStruct)node)) {

				String[] idComponents = node.getIdComponents();
				String qualifiedNameWithoutPrefix = "";
				for (int i = 0; i < idComponents.length; i++) {
					qualifiedNameWithoutPrefix += idComponents[i];
					if (i < idComponents.length - 2) {
						qualifiedNameWithoutPrefix += "/";
					}
					else if (i < idComponents.length - 1) {
						qualifiedNameWithoutPrefix += "::";
					}
				}

				allEventStructs.put(qualifiedNameWithoutPrefix, node);
//				System.out.println("added event struct " + qualifiedNameWithoutPrefix);
			}
		}
		else
		{
			node.reset();
			while (!node.end())
			{
				IdlObject child = node.current();
				collectEventStructs(child);
				node.next();
			}
			// important to reset -- otherwise we'll get an ArrayIndexOutOfBounds ex later 
			// while traversing the tree in JavaPackageScout# 
			node.reset();
		}		
	}


	
	
	/**
	 * Modelled after {@link org.openorb.compiler.IdlCompiler#main}
	 * @param args 	same as for {@link IdlCompiler#scan_arguments(String[])}
	 */
	public static void main(String[] args)
	{
		try
		{
			if (args.length == 0)
				IdlCompiler.display_help();
	
			// last arg must be IDL file or directory with IDL files
			File lastArgFile = new File(args[args.length-1]);
			if (!lastArgFile.exists()) {
				System.out.println(lastArgFile.getAbsolutePath() + " does not exist!");
				return;
			}
	
			IDLScrutinizer idlCompiler = new IDLScrutinizer();
	
			String[] args2 = args;
			
			if (lastArgFile.isDirectory()) {
				// process all IDL files in this directory			
				File[] idlFiles = lastArgFile.listFiles(new FileFilter() {
					public boolean accept(File pathname) {
						return pathname.getName().toLowerCase().endsWith("idl");
					}				
				});
				args2 = new String[args.length -1 + idlFiles.length];
				for (int i = 0; i < args.length - 1; i++) {
					// copy all args except dir (last arg)
					args2[i] = args[i];
				}
				for (int i = 0; i < idlFiles.length; i++) {
					// add files instead of dir
					args2[args.length -1 + i] = idlFiles[i].getAbsolutePath();
				}
			}
			idlCompiler.processIDLFiles(args2);
			
			File wikiOutFile = new File("wikiEventPage");
			PrintWriter wikiWriter = new PrintWriter(new FileWriter(wikiOutFile));
			
			idlCompiler.printEventInfo(wikiWriter);
			wikiWriter.close();
			
			System.out.println("Done! Wiki output written to file " + wikiOutFile.getAbsolutePath() );
		}
		catch (Exception ex)
		{
			System.err.println("ACS IDL compilation failed!");
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	
	private void processIDLFiles(String[] args) {
		IdlParser parser = IdlCompiler.createIDLParser(args);

		if (IdlCompiler.idl_file_name.length == 0)
			IdlCompiler.display_help(); 
		
		// -- Starts the compilation --
		java.util.Hashtable definedCopy =
			(java.util.Hashtable) IdlCompiler.definedMacros.clone();

		for (int i = 0; i < IdlCompiler.idl_file_name.length; i++)
		{
			IdlCompiler.definedMacros = definedCopy;

			System.out.println("=========== IDL analysis of " + IdlCompiler.idl_file_name[i] + "===============");

			// -- build parse tree --
			
			IdlObject root = parser.compile_idl(IdlCompiler.idl_file_name[i]);

			if (IdlParser.totalError != 0)
			{
				String msg = "there are errors...compilation process stopped for the file " + IdlCompiler.idl_file_name[i];
				System.out.println(msg);
				IdlParser.totalError = 0;  // is not automatically reset for next IDL scan!
			}		
			else {
				collectEventStructs(root);
			}
		}
	}
	

	/**
	 * For sizes on the CORBA octet stream, see chapter 15.3 ("CDR Transfer Syntax") 
	 * of the ORB spec (3.0.3 at http://www.omg.org/cgi-bin/doc?formal/04-03-01).  
	 */
	static class PrimitiveTypeInfo {
		private String name;
		private int idlPrimitiveNumber; // see org.openorb.compiler.idl.reflect.idlPrimitive
		private int lengthInOctets;
		
		static Map typeMap;
		
		static {
			typeMap = new HashMap();
			new PrimitiveTypeInfo("VOID", 0, 0);
			new PrimitiveTypeInfo("BOOLEAN", 1, 1);
			new PrimitiveTypeInfo("FLOAT", 2, 4);
			new PrimitiveTypeInfo("DOUBLE", 3, 8);
			new PrimitiveTypeInfo("LONGDOUBLE", 4, 16);
			new PrimitiveTypeInfo("SHORT", 5, 2);
			new PrimitiveTypeInfo("USHORT", 6, 2);
			new PrimitiveTypeInfo("LONG", 7, 4);
			new PrimitiveTypeInfo("ULONG", 8, 4);
			new PrimitiveTypeInfo("LONGLONG", 9, 8);
			new PrimitiveTypeInfo("ULONGLONG", 10, 8);
			new PrimitiveTypeInfo("CHAR", 11, 1); 
			new PrimitiveTypeInfo("WCHAR", 12, 2); // see 15.3.1.6 of CORBA spec
			new PrimitiveTypeInfo("OCTET", 13, 1);
			new PrimitiveTypeInfo("OBJECT", 14, -1);
			new PrimitiveTypeInfo("ANY", 15, -1);
			new PrimitiveTypeInfo("TYPECODE", 16, -1);
			new PrimitiveTypeInfo("VALUEBASE", 17, -1);
		}
		
		PrimitiveTypeInfo(String name, int idlPrimitiveNumber, int lengthInOctets) {
			this.name = name;
			this.idlPrimitiveNumber = idlPrimitiveNumber;
			this.lengthInOctets = lengthInOctets;
			typeMap.put(new Integer(idlPrimitiveNumber), this);
		}
		
		static PrimitiveTypeInfo fromIdlPrimitive(int idlPrimitiveNumber) {
			return (PrimitiveTypeInfo) typeMap.get(new Integer(idlPrimitiveNumber));
		}
		
		String getName() {
			return name;
		}
		
		int getLengthInOctets() {
			return lengthInOctets;
		}
	}

}

