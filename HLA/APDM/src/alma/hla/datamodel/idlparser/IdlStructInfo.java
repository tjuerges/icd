/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.idlparser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author hsommer
 */
public class IdlStructInfo {
	
	private List members = new ArrayList();
	
	void addStructMemberInfo(IdlStructMemberInfo memberInfo) {
		members.add(memberInfo);
	}
	
	IdlStructMemberInfo[] getStructMembers() {
		return (IdlStructMemberInfo[]) members.toArray(new IdlStructMemberInfo[members.size()]);
	}
	
	boolean isSizeKnown() {
		return ( getSizeInOctets() >= 0 );
	}
		
	int getSizeInOctets() {
		int structSize = 0;
		for (Iterator iter = members.iterator(); iter.hasNext();) {
			IdlStructMemberInfo memberInfo = (IdlStructMemberInfo) iter.next();
			int memberSize = memberInfo.getTotalSizeInOctets();
			if (memberSize >= 0) {
				structSize += memberSize;
			}
			else {
				structSize = -1;
				break;
			}
		}
		return structSize;
	}
}
