package alma.hla.datamodel.meta;

import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;

public class AlmaEnumerationLiteral extends EnumerationLiteral {

	public String JavaName() {
		return formatAsJavaConstantPart(NameS());
	}

	public String CastorName() {
		return translateEnumValueToIdentifier(NameS());
	}

	private String formatAsJavaConstantPart(String x) {

		String ret = "";

		if (x == null || x.length() == 0)
			return ret;

		x = x.toUpperCase();

		// fix remainder
		for (int i = 0; i < x.length(); i++) {
			ret += (Character.isJavaIdentifierPart(x.charAt(i)) ? x.substring(
					i, i + 1) : "_");
		}

		return ret;
	}

	/**
	 * This method is copied from Castor's
	 * org.exolab.castor.builder.SourceFactory.java (v. 0.9.6) to match exactly
	 * Castor's behavior
	 */
	static String translateEnumValueToIdentifier(String enumValue) {
		try {
			int intVal = Integer.parseInt(enumValue);
			if (intVal >= 0)
				return "VALUE_" + intVal;
			else
				return "VALUE_NEG_" + Math.abs(intVal);
		} catch (NumberFormatException e) {
			// just keep going
		}
		StringBuffer sb = new StringBuffer(enumValue.toUpperCase());
		char c;
		for (int i = 0; i < sb.length(); i++) {
			c = sb.charAt(i);
			if ("[](){}<>'`\"".indexOf(c) >= 0) {
				sb.deleteCharAt(i);
				i--;
			} else if (Character.isWhitespace(c)
					|| "\\/?~!@#$%^&*-+=:;.,".indexOf(c) >= 0) {
				sb.setCharAt(i, '_');
			}
		}
		return sb.toString();
	} // -- translateEnumValueToIdentifier
}
