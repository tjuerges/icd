package alma.hla.datamodel.meta;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Attribute;

import alma.hla.datamodel.util.MMUtil;

public class PhysicalQuantity extends ValueType
{

	private AlmaAttribute valAttr;
	private AlmaEnumeration unitAttr;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.bmiag.genfw.meta.Element#checkConstraints()
	 */
	@Override
	public void checkConstraints()
	{
		MMUtil.assertElementCount(this, Attribute(), 2, "Physical quantities must have 2 attributes.");
		valAttr = (AlmaAttribute)findAttributeByName("value");
		Attribute unitTemp = findAttributeByName("unit");
		Checks.assertNotNull(this, valAttr, "Physical quantity must have value attribute");
		Checks.assertNotNull(this, unitTemp, "Physical quantity must have unit attribute");
		Checks.assertEquals( this, valAttr.Type().NameS(), "double" , "Physical quantity's value must be a double.");

		Checks.assertType(this, unitTemp.Type(), AlmaEnumeration.class, "Physical quantity's unit must be an enumeration.");
		Checks.assertFalse(this, ((AlmaEnumeration)unitTemp.Type()).isInteger(), "Physical quantity's unit must be a String enumeration.");
		unitAttr = (AlmaEnumeration)unitTemp.Type();
		super.checkConstraints();
		return;
	}
	
	public ElementSet Unit() {
		return unitAttr.Literal();
	}

	public String UnitSize() {
		return Integer.toString(Unit().size());
	}
}
