/*
 * Created on Dec 16, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.meta;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.meta.uml.Type;
import org.openarchitectureware.meta.uml.classifier.Attribute;
import org.openarchitectureware.meta.uml.classifier.Enumeration;

import alma.hla.datamodel.util.MMUtil;
import alma.hla.datamodel.vo.Encoder;
import alma.hla.datamodel.vo.VOConfig;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class AlmaAttribute extends Attribute
{

	public boolean isEnumeration() {
		return Type() instanceof Enumeration;
	}

	public AlmaAbstractClass AlmaClass() {
		return (AlmaAbstractClass)Class();
	}
	
	public AlmaAbstractClass AlmaType() {
		return (AlmaAbstractClass)Type();
	}
	
	public boolean isValueType()
	{
		Type candidate = Type();
		if (candidate == null) System.out.println("No type spec for attribute "+NameS());
		return ValueType.isInstance( Type() ); 
	}

	/** UML-Datatypes that come out as primitive types in the generated (Java) code 
	 * (msc, 2007-04-02) */
	public boolean translatesToPrimitiveType() {
	    boolean ret = false;
		try {
            String typeName = Type().NameS();
            ret = MMUtil.isOneOf( typeName, new String[]{"Integer", "float", "double", "long", "Boolean"} );
        } catch (RuntimeException e) {
        	Checks.warn( this, "["+Class().Name()+"] failed to check for type of attribute " + Name());
        }
		return ret;
	}

	
	public boolean isPrimitiveType()
	{
	    boolean ret = false;
		try {
            String typeName = Type().NameS();
            ret = MMUtil.isOneOf( typeName, new String[]{"Integer", "float", "String", "double", "long", "Boolean", "Date"} );
        } catch (RuntimeException e) {
        	Checks.warn( this, "["+Class().Name()+"] failed to check for primitive type of attribute " + Name());
        }
		return ret;
	}

   public String AllUpperCaseName() {
      return NameS().toUpperCase();
   }
   	
	private Encoder getVOTableEncoder() {
		String encoderClassName = getVOEncoderClassName();
		if (encoderClassName == null ) return null;
		try {
			Encoder encoder = (Encoder) java.lang.Class.forName(encoderClassName).newInstance();
			return encoder;
		} catch (Exception e) {
			e.printStackTrace();
		}
		Checks.warn( this , "Encoder "+encoderClassName+" not found!");
		return null;
	}
	
	public String VOTableType() {
		return getVOTableEncoder().encodeTypeName( Type().Name().toString() );
	}

	public String VOTableTypeWidth() {
		return getVOTableEncoder().getEncodingWidth( Type().Name().toString() );
	}

	public String Ucd() {
		org.jdom.Element fieldElem = VOConfig.getFieldElement(this);
		return fieldElem.getAttributeValue("ucd");
	}
	
	private String getVOEncoderClassName() {
		org.jdom.Element fieldElement = VOConfig.getFieldElement(this);
		if (fieldElement == null ) return null; 
		String encoderClassName = "almamm.vo.encoder."+fieldElement.getAttributeValue("encoder");
		return encoderClassName;
	}
	
	public String VOEncoderClassName() {
		return getVOEncoderClassName();
	}
	
	public String CppType() {
		if ( Type().Name().toString().equals("String") ) {
			return "string";
		}
		// todo: Boolean to bool etc
		return Type().Name().toString();
	}
	
	public String XsdType() {
		String typeName = Type().Name().toString();
		String xsdName = null;
		
		if (typeName.equals("String")) {
			xsdName = "xsd:string";
		}
		else if (typeName.equals("Integer")) {
			xsdName = "xsd:int";
		}
		else if (typeName.equals("Boolean")) {
			xsdName = "xsd:boolean";
		}
		else if (typeName.equals("Date")) {
			xsdName = "xsd:dateTime";
		}
		else if (isPrimitiveType()) {
			xsdName = "xsd:" + typeName;
		}
		else if (Type() instanceof AlmaEnumeration){
			if (((AlmaEnumeration)Type()).isInteger()) {
				xsdName = "xsd:int";
			}
			else {
				xsdName = "xsd:string";
			}
		}
		else {
			// it must be a value type from the model
			xsdName = "val:" + typeName;
		}
		return xsdName;
	}

	/*
	 * Copied the following methods from AlmaAssociationEnd so that we can allow
	 * attributes to have UML-standard multiplicities -- JS 30-Jun-2005
	 */
	
	public boolean multiplicityMinNotOne() throws NumberFormatException
	{
		String mm = MultiplicityMin();
		if (mm == null) return false;
		int multMin = Integer.parseInt(mm);
		return ( multMin != 1 );
	}

	public boolean multiplicityMaxNotOne() throws NumberFormatException
	{
		String mm = MultiplicityMax();
		if (mm == null) return false;		
		int multMax = Integer.parseInt(mm);
		return ( multMax != 1 );
	}

	public String multiplicityMaxForXML()
	{
		String mm = MultiplicityMax();
		if (mm == null) return "1";
		int multMax = Integer.parseInt(mm);
		
		return ( multMax > 0 ? mm : "unbounded");
	}
   
   public boolean MultiplicityOneToInfinity() throws NumberFormatException {
      return (MultiplicityMin().equals("1") &  MultiplicityMax().equals("-1"));
   }
   
   public boolean MultiplicityMaxIsGreaterThanOne() throws NumberFormatException {
      int max = Integer.parseInt(MultiplicityMax());
      return (max > 1 || max == -1);
   }
   /*
    * End added multiplicity methods -- JS
    */
	///////////////////////////////////////////////////////////////////////////////////////////////
	/// the methods below were taken from Allen's version of AlmaAttribute -- todo: integrate this better
	///////////////////////////////////////////////////////////////////////////////////////////////
	
   
	@Override
	public String UpperCaseName() {
		return MMUtil.UpperCaseName(Name().toString());
	}
	
	public String JavaType() {
		return MMUtil.JavaType(Type());
	}
	
	public String JavaBoxType() {
		return MMUtil.JavaBoxType(Type());
	}
	
	//isOptional
	public boolean isOptional() {
		if (MultiplicityMin() == null || MultiplicityMax() == null)
			return false;
		if (MultiplicityMin().equals("0") ) return true;
		return false;
	}
	
	//isArray
	protected boolean array = false;
	public boolean isArray() {
		return array;
	}
	
	// Size
	public String Size = "";
	public void setSize(String Size) {
		this.Size = Size;
	}
	
	//isTableKey -- Does this attribute specify a key of a table? 
	protected boolean tableKey = false;
	public boolean isTableKey() {
		return tableKey;
	}
	
	//isExtrinsic -- Does this attribute specify an extrinsic attribute? 
	protected boolean extrinsic = false;
	public boolean isExtrinsic() {
		return extrinsic;
	}

	public String SimpleUpperCaseName() {
		return MMUtil.UpperCaseName(Name().toString());
	}
	
	public String SimpleJavaType() {
		return MMUtil.JavaType(Type());
	}

	public String SimpleName() {
		return Name().toString();
	}

	// Category
	public String Category = "";
	public void setCategory(String Category) {
		this.Category = Category;
	}
	

	
}
