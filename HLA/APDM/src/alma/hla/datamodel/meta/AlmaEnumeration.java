package alma.hla.datamodel.meta;

import org.openarchitectureware.meta.uml.classifier.Enumeration;
import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;

import java.util.Iterator;

public class AlmaEnumeration extends Enumeration {
	public void DisplayLiterals() {
		for (Iterator i=Literal().iterator();i.hasNext();System.out.println("Literals "+((EnumerationLiteral)i.next()).NameS()));
	}
	
	public boolean isInteger() {
		String thisLit = "";
		for (Iterator i=Literal().iterator();i.hasNext();) {
			thisLit = ((EnumerationLiteral)i.next()).NameS();
			try {
				Integer.parseInt(thisLit);
			} catch (NumberFormatException e) {
				return false;
			}
			return true;
		}
		return false;	// Don't see how this can happen. (alma.chk checks for > 0 literals)
	}
}
