/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.meta.components;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.meta.uml.classifier.Class;

/**
 * Base class for AcsComponent, AcsEvent, NotificationChannel to share common code.
 *  
 * @author hsommer
 */
public class ComponenEventModelAbstractClass extends Class {

    /**
	 * Returns the {@link Subsystem} containing the associated <code>AcsEvent</code>, if any.
	 */
	public Subsystem Subsystem() {
		return getSubsystem(this);
	}

	/**
	 * This static methods makes the functionality usable for other classes that cannon inherit from this one,
	 * such as {@link EventOnChannel}.
	 * <p>
	 * Currently expects classes directly below a "subsystem" UML package.
	 * TODO: allow nested packages below subsystem; then traverse namespaces upward until Subsystem is found.
	 * 
	 * @param elem the model element (e.g. Class or CustomElement).
	 * @return the subsystem that owns the model element
	 */
	static Subsystem getSubsystem(Element elem) {
		if (elem.Namespace() instanceof Subsystem) {
			return (Subsystem) elem.Namespace();
		}
		else {
			Checks.warn(elem, "no associated UML Subsystem for event " + elem.NameS());
			return null;
		}
	}

}
