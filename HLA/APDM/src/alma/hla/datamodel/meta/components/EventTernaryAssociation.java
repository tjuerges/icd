/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;
import org.openarchitectureware.meta.uml.classifier.Class;

/**
 * Class that represents a ternary association involving a component, a channel, and and at least one event.
 * Instances of this meta class will generally not have a name. 
 * What matters is the type: {@link EventProduction} from UML stereotype &lt;&lt;eventProduction&gt;&gt;, 
 * or {@link EventSubscription} from &lt;&lt;eventSubscription&gt;&gt;, and of course the attached other classes.
 * <p>
 * To model the ternary association, we use a separate class instead of the native UML "n-Aray association".
 * In XMI the latter would appear anyway as an anonymous class with 3 binary associations, but it would
 * be less elegant to attach the information about the direction of the event flow.
 * With a separate "real" class, we can use stereotypes for that purpose.   
 * 
 * @author hsommer
 */
public abstract class EventTernaryAssociation extends Class {

	protected AcsComponent component;
	protected ElementSet events;
	protected NotificationChannel channel;
	
	@Override
	public void checkConstraints() {
		super.checkConstraints();

		checkAndInit();
		
		return;
	}

	/**
	 * The ternary association represented by this class requires binary associations to one or more events, 
	 * one component, and one channel.
	 * <p>
	 * We do not care about the stated multiplicities, because they are assumed 
	 * to be 1..1 anyway and thus don't get read from the model.
	 */
	private void checkAndInit() {
		component = null;
		events = new ElementSet();
		channel = null;
		
		// simply remember an illegally associated class during the check loop, 
		// so that other fields get filled first in order to produce a decent error message
		Class badAssociatedClass = null;
		
		for (Iterator assocEndIter = AssociationEnd().iterator(); assocEndIter.hasNext();) {
			AssociationEnd assocEnd = (AssociationEnd) assocEndIter.next();
			Class otherClass = assocEnd.Opposite().Class();
			if (otherClass instanceof AcsEvent) {
				if (!events.contains(otherClass)) {
					events.add(otherClass);
				}
				else {
					badAssociatedClass = otherClass;				
				}
			}
			else if (otherClass instanceof AcsComponent) {
				if (component == null) {
					component = (AcsComponent) otherClass;
				}
				else {
					badAssociatedClass = otherClass;
				}
			} 
			else if (otherClass instanceof NotificationChannel) {
				if (channel == null) {
					channel = (NotificationChannel) otherClass;
				}
				else {
					badAssociatedClass = otherClass;
				}
			}
			else {
				badAssociatedClass = otherClass;
			}
		}
		
		// assert that all required classes are associated
		String eventNames = "";
		if (!events.isEmpty()) {
			for (Iterator eventIter = events.iterator(); eventIter.hasNext();) {
				AcsEvent event = (AcsEvent) eventIter.next();
				eventNames += event.Name().toString() + ',';
			}
		}
		if (component == null || events.isEmpty() || channel == null) {
			String msg = "Missing associated class(es) for '" + associationKind() + "'-association: " +
					component + " / " + eventNames + " / " + channel;
			Checks.warn(this, msg);
		}
		
		// assert that no additional classes are associated
		if (badAssociatedClass != null) {
			String msg = "Ternary association between component/event/channel (" + component.Name() + "/" + eventNames + "/" + channel.Name() + 
							") illegally associates ";
			if (badAssociatedClass.isUnnamed()) {
				msg += "an unnamed class.";
			}
			else {
				msg += "the class '" + badAssociatedClass.QualifiedName() + "'.";
			}
			Checks.warn(this, msg);
		}
	}
	
	
    @Override
	protected void initializeModelDependencies() {
    }
    
    /**
     * @return textual description of the kind of ternary event association, used for output messages.
     * 			Subclasses return "event subscription" etc
     */
    abstract String associationKind();
    
    
    public AcsComponent AcsComponent() {
    	if (component == null) {
    		checkAndInit();
    	}
    	return component;
    }
    
    public ElementSet AcsEvent() {
    	if (events == null) {
    		checkAndInit();
    	}
    	return events;
    }

    public NotificationChannel NotificationChannel() {
    	if (channel == null) {
    		checkAndInit();
    	}
    	return channel;
    }

}
