/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;
import org.openarchitectureware.meta.uml.classifier.Class;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.MMUtil;

/**
 * Represents the UML meta class with sterotype &lt;&lt;acsevent&gt;&gt;.
 * It's an event that can be supplied or subscribed to on one or many notification channels.
 *   
 * @author hsommer
 */
public class AcsEvent extends ComponenEventModelAbstractClass {

	@Override
	public void checkConstraints() {
		super.checkConstraints();

		for (Iterator iter = AssociationEnd().iterator(); iter.hasNext();) {
			Class otherClass = ((AssociationEnd) iter.next()).Opposite().Class();
			if ( !(otherClass instanceof EventSubscription) && !(otherClass instanceof EventProduction) ) {
				Checks.warn(this, "Event '" + QualifiedName() + "' must not have an association to class " + otherClass);
			}
		}
		return;
	}
	
    @Override
	protected void initializeModelDependencies() {
    	// maybe later
    }
     
    
    public boolean isUsed() {
    	// the ctor checks already that no illegal associations can exist
    	return ( !AssociationEnd().isEmpty() );
    }
    
    /**
     * Gets all event subscriptions. These are triples of channel/component/event 
     * such that some components subscribe to this event on some channels.
     *  
     * @return list of <code>EventSubscription</code>s.
     */
    public ElementSet Subscription() {
		ElementSet subscriptions = MMUtil.filter(AssociationEnd(), new Predicate() {
			@Override
			public boolean filter(Element el) {
				AssociationEnd e = (AssociationEnd) el;
				return (e.Opposite().Class() instanceof EventSubscription);
			}
			@Override
			public Element mapElement(Element el) {
				AssociationEnd e = (AssociationEnd) el;
				return e.Opposite().Class();
			}
		});
		return subscriptions;
    }
    
    /**
     * Gets all event productions. These are triples of channel/component/event 
     * such that some components produce this event on some channels.
     *  
     * @return list of <code>EventProduction</code>s.
     */
    public ElementSet Production() {
		ElementSet productions = MMUtil.filter(AssociationEnd(), new Predicate() {
			@Override
			public boolean filter(Element el) {
				AssociationEnd e = (AssociationEnd) el;
				return (e.Opposite().Class() instanceof EventProduction);
			}
			@Override
			public Element mapElement(Element el) {
				AssociationEnd e = (AssociationEnd) el;
				return e.Opposite().Class();
			}
		});
		return productions;
    }
    
}
