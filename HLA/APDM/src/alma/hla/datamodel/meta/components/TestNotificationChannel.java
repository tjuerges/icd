/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;


/**
 * Meta class for a notification channel that is only used for engineering tests / diagnostics,
 * but not as part of the functional system.
 * <p>
 * A UML class stereotyped as &lt;&lt;testchannel&gt;&gt; can be associated to other classes
 * following the rules that apply to a normal &lt;&lt;channel&gt;&gt;.
 * 
 * @author hsommer
 */
public class TestNotificationChannel extends PrivateNotificationChannel {

	@Override
	public boolean isTestChannel() {
    	return true;
    }	
}
