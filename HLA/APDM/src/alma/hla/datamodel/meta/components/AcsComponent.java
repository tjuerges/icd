/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;


/**
 * Meta class that represents an ACS-based component from an ALMA subsystem.
 * <p>
 * Currently only used for modelling event supply/subscription.
 * 
 * @author hsommer
 */
/**
 * @author jschwarz
 *
 */
public class AcsComponent extends ComponenEventModelAbstractClass {
	
	private String IDL_Const;
	private String IDL_File;
	private String IDL_Module;
	private String Impl_File;

	@Override
	public void checkConstraints() {
		super.checkConstraints();
	}
	
    @Override
	protected void initializeModelDependencies() {
    }
    
    /**
     * Methods for tagged values (attached to stereotype "acscomponent")
     */
    
    public void setIDL_Const(String invalue) { IDL_Const = invalue; }
    
    public String IDL_Const () { return IDL_Const; }
    
    public void setIDL_File(String invalue) { IDL_File = invalue; }
    
    public String IDL_File () { return IDL_File; }
    
    public void setIDL_Module(String invalue) { IDL_Module = invalue; }
    
    public String IDL_Module () { return IDL_Module; }
    
    public void setImpl_File(String invalue) { Impl_File = invalue; }
    
    public String Impl_File () { return Impl_File; }
}
