/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;
import org.openarchitectureware.meta.uml.classifier.Class;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.MMUtil;

/**
 * Meta model class that represents a notification channel, on which components can post events,
 * while other components and clients can subscribe to these events.
 * <p>
 * A UML class stereotyped as &lt;&lt;channel&gt;&gt; can be associated with [0...*] 
 * &lt;&lt;eventSubscription&gt;&gt; or &lt;&lt;eventProduction&gt;&gt; classes.
 * 
 * @see alma.hla.datamodel.meta.components.EventSubscription
 * @see alma.hla.datamodel.meta.components.EventProduction
 * @author hsommer
 */
public class NotificationChannel extends ComponenEventModelAbstractClass {
	
	
	protected ElementSet supplier;
	protected ElementSet subscriber;

	protected ElementSet allEventsOnChannel;
	
	/**
	 * For tagged value IDL_Const, which is the name of the IDL-defined constant string which holds the channel name.
	 */
	private String IDL_Const;
	private String IDL_File;

	
	@Override
	public void checkConstraints() {
		super.checkConstraints();
		
		// can only have associations with EventSubscription or EventProduction classes
		for (Iterator assocEndIter = AssociationEnd().iterator(); assocEndIter.hasNext();) {
			Class otherClass = ((AssociationEnd) assocEndIter.next()).Opposite().Class();
			if (!(otherClass instanceof EventTernaryAssociation)) {
				Checks.warn(this, "Channel '" + QualifiedName() + "' illegally associates class '" + otherClass.QualifiedName() + "'.");
			}
		}

		// print warnings about missing subscribers/suppliers unless it's a test channel 
		if (!isTestChannel()) {
			if (AssociationEnd().size() == 0) {
				// print a warning if the channel is not used
				System.out.println("Warning: Channel '" + QualifiedName() + "' is not used at all.");
			}		
			
			for (Iterator unmatchedSupplierIter = unmatchedSuppliedEvent().iterator(); unmatchedSupplierIter.hasNext();) {
				AcsEvent unmachtedSuppliedEvent = (AcsEvent) unmatchedSupplierIter.next();
				String msg = "Channel '" + QualifiedName() + "' has supplier(s) for event '" + unmachtedSuppliedEvent.Name() + 
								"', but there are no subscribers for this event.";
				System.out.println("Warning: " + msg);
			}
			
			for (Iterator unmatchedSubscriberIter = unmatchedSubscribedEvent().iterator(); unmatchedSubscriberIter.hasNext();) {
				AcsEvent unmatchedSubscribedEvent = (AcsEvent) unmatchedSubscriberIter.next();
				String msg = "Channel '" + QualifiedName() + "' has subscriber(s) for event '" + unmatchedSubscribedEvent.NameS() + 
								"', but there are no suppliers for this event.";
				System.out.println("Warning: " + msg);
			}
		}		
		return;
	}
	

	/**
     * Sets up {@link EventOnChannel} objects for all {@link AcsEvent}s that are attached to this channel.
     * 
     * @see #EventOnChannel()
     */
    @Override
	protected void initializeModelDependencies() {
    	allEventsOnChannel = new ElementSet();
    	ElementSet allTriplets = org.openarchitectureware.core.meta.util.MMUtil.difference(Supplier(), Subscriber());
    	for (Iterator iter = allTriplets.iterator(); iter.hasNext();) {
    		ElementSet events = ((EventTernaryAssociation) iter.next()).AcsEvent();
    		for (Iterator eventIter = events.iterator(); eventIter.hasNext();) {
				AcsEvent event = (AcsEvent) eventIter.next();
    			EventOnChannel eoch = new EventOnChannel(event, this);
    			if (!allEventsOnChannel.contains(eoch)) {
    				allEventsOnChannel.add(eoch);
    			}
			}
		}
    }
    
    /**
     * Methods for tagged values (attached to stereotype "acsevent")
     */
    
    public void setIDL_Const(String invalue) { IDL_Const = invalue; }
    
    public String IDL_Const () { return IDL_Const; }
    
    public void setIDL_File(String invalue) { IDL_File = invalue; }
    
    public String IDL_File () { return IDL_File; }
     
    public boolean isUsed() {
    	return ( hasSupplier() || hasSubscriber() );
    }
    
    public boolean hasSupplier() {
    	return ( Supplier().size() > 0 );
    }
    
    public boolean hasSubscriber() {
    	return ( Subscriber().size() > 0 );
    }
    
    /**
     * @return list of <code>AcsEvent</code>s which some component supplies to this channel,
     * 			but for which no subscriber exists.
     */
    public ElementSet unmatchedSuppliedEvent() {
    	ElementSet unmatchedSuppliedEvents = new ElementSet();
		for (Iterator supplierIter = SuppliedEvent().iterator(); supplierIter.hasNext();) {
			AcsEvent suppliedEvent = (AcsEvent) supplierIter.next();
			if (!SubscribedEvent().contains(suppliedEvent)) {
				unmatchedSuppliedEvents.add(suppliedEvent);
			}
		}
    	return unmatchedSuppliedEvents;
    }
    
    /**
     * @return list of <code>AcsEvent</code>s for which some component subscribes to on this channel,
     * 			but for which no supplier exists.
     */
    public ElementSet unmatchedSubscribedEvent() {
    	ElementSet unmatchedSubscribedEvents = new ElementSet();
		for (Iterator subscriberIter = SubscribedEvent().iterator(); subscriberIter.hasNext();) {
			AcsEvent subscribedEvent = (AcsEvent) subscriberIter.next();
			if (!SuppliedEvent().contains(subscribedEvent)) {				
				unmatchedSubscribedEvents.add(subscribedEvent);
			}
		}
    	return unmatchedSubscribedEvents;
    }
    
    
    /**
     * Gets all <code>EventSubscription</code> ternary associations that are attached to this channel.
     */
    public ElementSet Subscriber() {
    	if (subscriber == null) {
			ElementSet filtered = MMUtil.filter(AssociationEnd(), new Predicate() {
				@Override
				public boolean filter(Element el) {
					AssociationEnd e = (AssociationEnd) el;
					return (e.Opposite().Class() instanceof EventSubscription);
				}
				@Override
				public Element mapElement(Element el) {
					AssociationEnd e = (AssociationEnd) el;
					return e.Opposite().Class();
				}
			});
			subscriber = filtered;		
    	}
    	return subscriber;
    }
    
    /**
     * Gets all events that components or clients receive from this channel.
     * @return A list of <code>AcsEvent</code>s, without duplicates. 
     */
    public ElementSet SubscribedEvent() {
    	ElementSet allEvents = new ElementSet();
    	for (Iterator iter = Subscriber().iterator(); iter.hasNext();) {
			ElementSet events = ((EventSubscription) iter.next()).AcsEvent();
			for (Iterator eventIter = events.iterator(); eventIter.hasNext();) {
				AcsEvent event = (AcsEvent) eventIter.next();
				if (!allEvents.contains(event)) {
					allEvents.add(event);
				}
			}
		}
		return allEvents;
    }
    
        
    /**
     * Gets all <code>EventProduction</code> ternary associations that are attached to this channel.
     */
    public ElementSet Supplier() {
    	if (supplier == null) {
			ElementSet filtered = MMUtil.filter(AssociationEnd(), new Predicate() {
				@Override
				public boolean filter(Element el) {
					AssociationEnd e = (AssociationEnd) el;
					return (e.Opposite().Class() instanceof EventProduction);
				}
				@Override
				public Element mapElement(Element el) {
					AssociationEnd e = (AssociationEnd) el;
					return e.Opposite().Class();
				}
			});
		    supplier = filtered;		
    	}    	
    	return supplier;
    }

    /**
     * Gets all events that components supply to this channel.
     * @return a list of <code>AcsEvent</code>s, without duplicates. 
     */
    public ElementSet SuppliedEvent() {    	
    	ElementSet allEvents = new ElementSet();
    	for (Iterator iter = Supplier().iterator(); iter.hasNext();) {
			ElementSet events = ((EventProduction) iter.next()).AcsEvent();
			for (Iterator eventIter = events.iterator(); eventIter.hasNext();) {
				AcsEvent event = (AcsEvent) eventIter.next();
				if (!allEvents.contains(event)) {
					allEvents.add(event);
				}
			}
		}
		return allEvents;
    }
    
    
    /**
     * Gets all events that some components subscribe or supply to this channel.
     * The returned <code>EventOnChannel</code> are not linked to other channels. 
     * Thus the components they return are guaranteed to subscribe to this channel.  
     * 
     * @return list of <code>EventOnChannel</code> objects.
     * @see #SubscribedEvent()
     * @see #initializeModelDependencies()
     */
    public ElementSet EventOnChannel() {
    	return allEventsOnChannel;
    }

    
    /**
     * To be overridden in subclass {@link PrivateNotificationChannel} to distinguish between the two.
     * @return  always true by this class. 
     */
	public boolean isPrivateChannel() {
    	return false;
    }	

    /**
     * To be overridden in subclass {@link TestNotificationChannel} to distinguish between the two.
     * @return  always true by this class. 
     */
    public boolean isTestChannel() {
    	return false;
    }
}
