package alma.hla.datamodel.meta.components;


/**
 * This class replaces the new UML 2 definition of a subsystem. For purposes of the ALMA
 * Event Model, a subsystem (stereotyped "almasubystem") has the container properties of a 
 * package.
 * @author jschwarz
 *
 */
public class Subsystem extends org.openarchitectureware.meta.uml.classifier.Package {

}
