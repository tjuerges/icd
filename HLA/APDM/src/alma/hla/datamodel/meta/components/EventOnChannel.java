/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import org.openarchitectureware.core.meta.core.CustomModelElement;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.ModelElement;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.MMUtil;


/**
 * This class is a reduced and simplified view of an {@link AcsEvent}.
 * It does not directly correspond to any UML class.
 * <p> 
 * It represents an event from the point of a particular notification channel.
 * If the corresponding <code>AcsEvent</code> gets also used on other channels, 
 * this information would not be available here.
 * <p>
 * We need this class to allow the templates to list channels, events, and all components that supply/receive
 * those events. From an <code>AcsEvent</code> we would get too many components back, 
 * because it may also "work with" other channels and other components.
 * 
 * @author hsommer
 */
public class EventOnChannel extends CustomModelElement {

	private AcsEvent event;
	private NotificationChannel channel;

	/**
	 * 
	 * @param event the event which this class wraps.
	 * @param channel the notification channel in whose relationship with <code>event</code> we are interested.
	 */
	public EventOnChannel(AcsEvent event, NotificationChannel channel) {
		this.event = event;
		this.channel = channel;
        initFrom(event);
		setName(event.NameS());
	}
	

    /**
	 * Returns the {@link Subsystem} containing the associated <code>AcsEvent</code>, if any.
	 * @see ComponenEventModelAbstractClass#getSubsystem(ModelElement)
	 */
	public Subsystem Subsystem() {
		return ComponenEventModelAbstractClass.getSubsystem(event);
	}

	
	/**
	 * Gets all components which subscribe to the event on the channel provided
	 * in the constructor.
	 * 
	 * @return list of <code>AcsComponent</code>s.
	 */
	public ElementSet SubscribingComponent() {
		ElementSet components = MMUtil.filter(event.Subscription(), new Predicate() {
			@Override
			public boolean filter(Element el) {
				EventSubscription es = (EventSubscription) el;
				return (es.NotificationChannel() == channel);
			}
			@Override
			public Element mapElement(Element el) {
				EventSubscription es = (EventSubscription) el;
				return es.AcsComponent();
			}
		});
		components = org.openarchitectureware.core.meta.util.MMUtil.sortByName(components);
		return components;
	}
	
	
	public boolean hasSubscribingComponent() {
		return ( SubscribingComponent().size() > 0 );
	}
	
	
	/**
	 * Gets all components which supply the event to the channel provided in the constructor. 
	 * 
	 * @return list of <code>AcsComponent</code>s.
	 */
	public ElementSet SupplyingComponent() {
		ElementSet components = MMUtil.filter(event.Production(), new Predicate() {
			@Override
			public boolean filter(Element el) {
				EventProduction ep = (EventProduction) el;
				return (ep.NotificationChannel() == channel);
			}
			@Override
			public Element mapElement(Element el) {
				EventProduction ep = (EventProduction) el;
				return ep.AcsComponent();
			}
		});
		components = org.openarchitectureware.core.meta.util.MMUtil.sortByName(components);
		return components;
	}

	public boolean hasSupplyingComponent() {
		return ( SupplyingComponent().size() > 0 );
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof EventOnChannel)) {
			return false;
		}
		EventOnChannel other = (EventOnChannel) obj;
		
		return (other.NameS().equals(this.NameS()) &&
				other.channel == this.channel );
	}

	@Override
	public int hashCode() {
		return (NameS() + channel.NameS()).hashCode();
	}
	

}
