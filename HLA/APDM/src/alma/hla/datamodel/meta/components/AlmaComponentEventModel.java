/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;

import alma.hla.datamodel.pred.TypePredicate;
import alma.hla.datamodel.util.MMUtil;

/**
 * @author hsommer
 */ 
public class AlmaComponentEventModel extends Model  {

	/**
	 * @return a list of {@link Subsystem} objects.
	 */
	public ElementSet Subsystem() {
		ElementSet classes = MMUtil.collectDescendents(this);
		ElementSet es = MMUtil.filter(classes, new TypePredicate(Subsystem.class));
		return es;
	}
	
	/**
	 * @return a list of {@link AcsComponent} objects.
	 */
	public ElementSet AcsComponent() {
		ElementSet classes = MMUtil.collectDescendents(this);
		ElementSet es = MMUtil.filter(classes, new TypePredicate(AcsComponent.class));
		return es;
	}
		
	/**
	 * @return a list of {@link AcsEvent} objects, sorted by name.
	 */
	public ElementSet Event() {
		ElementSet classes = MMUtil.collectDescendents(this);
		classes = MMUtil.filter(classes, new TypePredicate(AcsEvent.class));
		classes = org.openarchitectureware.core.meta.util.MMUtil.sortByName(classes);
		return classes;
	}
		
	/**
	 * @return a list of {@link NotificationChannel} objects, sorted by name.
	 */
	public ElementSet Channel() {
		ElementSet classes = MMUtil.collectDescendents(this);
		classes = MMUtil.filter(classes, new TypePredicate(NotificationChannel.class));
		classes = org.openarchitectureware.core.meta.util.MMUtil.sortByName(classes);
		return classes;
	}
	
	public ElementSet AllElementsOfModel() {
		ElementSet els = new ElementSet();
		els.addAll(Subsystem());
		els.addAll(AcsComponent());
		els.addAll(Event());
		els.addAll(Channel());
		return els;
	}

}
