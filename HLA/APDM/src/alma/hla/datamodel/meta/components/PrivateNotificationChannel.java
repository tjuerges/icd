/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.components;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;


/**
 * Meta class for a notification channel that is only used internally by one ALMA subsystem.
 * Other subsystems are not supposed to publish or subscribe to events on a channel of this type. 
 * <p>
 * A UML class stereotyped as &lt;&lt;privatechannel&gt;&gt; can be associated to other classes
 * following the rules that apply to a normal &lt;&lt;channel&gt;&gt;.
 * 
 * @author hsommer
 */
public class PrivateNotificationChannel extends NotificationChannel {

	/**
	 * Asserts that only events from the same subsystem flow on this channel.
	 * Prints a warning if a component from a different subsystem uses this channel; 
	 * this case legally happens for example on an "internal" channel used by subsytems Control and Correlator.
	 * <p>
	 * TODO: check if the semantics of "internal" channel should be used strictly, 
	 * in which case the CONTROL_REALTIME channel would not be internal to Control.
	 * If so, report an Error instead of printing the warning.
	 */
	@Override
	public void checkConstraints() {
		super.checkConstraints();
		
		Subsystem mySubsys = Subsystem();
		
		ElementSet allAssocs = Subscriber();
		allAssocs.addAll(Supplier());
		String errmsg = "The notification channel '" + NameS() + "' belongs only to subsystem '" + mySubsys.NameS() + "'. ";
		
		for (Iterator iter = allAssocs.iterator(); iter.hasNext();) {
			EventTernaryAssociation assoc = (EventTernaryAssociation) iter.next();
			AcsComponent clientComp = assoc.AcsComponent();
			if (!clientComp.Subsystem().equals(mySubsys)) {
				System.out.println("Warning: " + errmsg + "Please check if component '" 
									+ clientComp.Subsystem().NameS() + "::" + clientComp.NameS() + "' is allowed to use it.");
			}
			for (Iterator eventIter = assoc.AcsEvent().iterator(); eventIter.hasNext();) {
				AcsEvent event = (AcsEvent) eventIter.next();
				if (!event.Subsystem().equals(mySubsys)) {
					errmsg += "It must not deal with events of type '" + event.Subsystem().NameS() + "::" + event.NameS() + "'.";
					Checks.warn(this, errmsg);
				}				
			}
		}				
				
		return;
	}

	
	@Override
	public boolean isPrivateChannel() {
    	return true;
    }	
}
