/*
 * Created on Dec 17, 2003
 * 
 * To change the template for this generated file go to Window - Preferences -
 * Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.meta;

import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.Dependency;

import alma.hla.datamodel.pred.TypePredicate;
import alma.hla.datamodel.util.MMUtil;

/**
 * @author jschwarz
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class Subsystem extends AlmaAbstractClass
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see util.AlmaAbstractClass#Label()
	 */
	@Override
	public String Label()
	{
		return "[Subsystem] " + Name().toString();
	}
	
	public ElementSet CreatedEntity() {
		return getEntities( CreatesDependency.class );
	}

	public ElementSet UsedEntity() {
		return getEntities( UsesDependency.class );
	}
	
	public ElementSet getEntities( Class predicateClass ) {
		ElementSet set = Requirement();
		return MMUtil.filter( set, new TypePredicate(predicateClass) {
			@Override
			public Element mapElement(Element el) {
				Dependency dep = (Dependency)el;
				return dep.Supplier();
			}
		});
	}

}
