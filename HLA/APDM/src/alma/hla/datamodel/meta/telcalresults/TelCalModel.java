/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2004
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta.telcalresults;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.meta.uml.classifier.Attribute;
import org.openarchitectureware.meta.uml.classifier.Class;

import alma.hla.datamodel.pred.TypePredicate;
import alma.hla.datamodel.util.MMUtil;

/**
 * Custom model class for TelCal. 
 * @author hsommer
 * @see alma.hla.datamodel.util.AlmaModel
 */
public class TelCalModel extends Model 
{
	@Override
	public void checkConstraints()
	{	
		super.checkConstraints();
        
        // check constraints on attribute name/type uniqueness
        UniqueTelCalAttribute();
        
        return;
	}
	
    @Override
	protected void initializeModelDependencies() {
    }

    
	/**
     * Gets all classes from all packages in this model.
	 */
	public ElementSet TelCalClass() {        
		ElementSet classes = MMUtil.collectDescendents(this);
        return MMUtil.filter(classes, new TypePredicate(Class.class));
	}
    
    /**
     * Gets all unique attributes from all classes from all packages in this model.
     * <p>
     * Attributes from different classes which have identical type and name 
     * are only included once at their first occurance.
     * Therefore it is dangerous to navigate from any of these attributes back to their owner class. 
     * 
     * reports an  Error if any two attributes (same class or other) have the same name but different types.
     */
    public ElementSet UniqueTelCalAttribute()  {
        ElementSet classes = TelCalClass();
        ElementSet uniqueAttributes = new ElementSet();

        // iterate over classes
        for (Iterator iterClasses = classes.iterator(); iterClasses.hasNext();) {
            Class clazz = (Class) iterClasses.next();
            // iterate over class attributes
            ElementSet attributes = clazz.Attribute();
            for (Iterator iterOwnAttr = attributes.iterator(); iterOwnAttr.hasNext();) {
                Attribute attr = (Attribute) iterOwnAttr.next();
                
                // compare with other attributes collected so far -- todo: use a map if performance becomes annoying
                boolean existsAlready = false;
                for (Iterator iterOtherAttr = uniqueAttributes.iterator(); iterOtherAttr.hasNext();) {
                    Attribute otherAttr = (Attribute) iterOtherAttr.next();
                    if (otherAttr.Name().toString().equalsIgnoreCase(attr.Name().toString())) {
                        if (otherAttr.Type().toString().equalsIgnoreCase(attr.Type().toString())) {
                            existsAlready = true;
                        }
                        else {
                            String msg = "Attribute '" + attr.Name() + "' exists with different types in classes '" +
                                attr.Class().Name() + "' and '" + otherAttr.Class().Name() + "' which is illegal for the TelCal model.";
                            Checks.warn(this, msg);
                        }
                    }
                }
                if (!existsAlready) {
                    uniqueAttributes.add(attr);                        
                }
            }
        }
        return uniqueAttributes;
    }   

}
