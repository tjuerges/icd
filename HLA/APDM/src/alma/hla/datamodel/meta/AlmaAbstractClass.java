package alma.hla.datamodel.meta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;
import org.openarchitectureware.meta.uml.classifier.Class;
import org.openarchitectureware.meta.uml.classifier.Package;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.ConsistentOrderingComparator;
import alma.hla.datamodel.util.MMUtil;
import alma.hla.datamodel.vo.VOConfig;

public abstract class AlmaAbstractClass extends Class 
{
	
	private String m_qualifiedName; // cache
	private String m_baseDirectory; // cache
	private String javaPackage; // cache
	
	
	public AlmaAbstractClass() {
	}
	
	public abstract String Label();

	
	// Namespaces, Qualified Names
	// ==================================================================================
	
	
	/**
	 * Generates a '/'-separated qualified name from the UML packages and the classname.
	 */
	@Override
	public String QualifiedName() {
		if (m_qualifiedName == null) {
			StringBuffer qname = new StringBuffer();
			ElementSet scope = PackageScope();
			for (Iterator iter = scope.iterator(); iter.hasNext();) {
				Package p = (Package) iter.next();
				if ( !p.NameS().equals("Data")) qname.append( p.NameS()+"/" );
			} 
			qname.append(NameS());
			m_qualifiedName = qname.toString();
		}
		return m_qualifiedName;
	}
	/**
	 * Generates a series of '../' to find the top-level directory (inverts QualifiedName).
	 */	
	public String BaseDirectory() {
		if (m_baseDirectory == null) {
			StringBuffer bname = new StringBuffer();
			ElementSet scope = PackageScope();
			for (Iterator iter = scope.iterator(); iter.hasNext();) {
				Package p = (Package) iter.next();
				if ( !p.NameS().equals("Data")) bname.append("../");			
			}
			m_baseDirectory = bname.toString();
		}
		return m_baseDirectory;
	}
	
    public boolean NotToBeGenerated() {
        return ( Package() instanceof NonGeneratedPackage );
    }
    
	public String PackageDirectory() {
		String jpack = JavaPackage();
		return jpack.replace('.', '/');
	}
	
	
	/**
	 * Derives a Java package from the UML package of this class.
	 * <p>
	 * Note that ObsPrep wants their wrapper classes to be in packages slightly different 
	 * from the UML package structure, with an extra level ".bo" inserted before the package that corresponds to an entity;
	 * for example, with a UML package "alma/obsprep/obsproject" we get "alma.obsprep.bo.obsproject" instead of "alma.obsprep.obsproject".    
	 */
	public String JavaPackage() {
		if (javaPackage == null) {
			StringBuffer qname = new StringBuffer();
			ElementSet scope = PackageScope();
			for (Iterator iter = scope.iterator(); iter.hasNext();) {
				Package p = (Package) iter.next();
				qname.append(p.Name());
				if (p.Name().toString().equals("obsprep")) {
				    qname.append(".bo");
				}
				if (iter.hasNext()) {
					qname.append('.');
				}
			}
			javaPackage = qname.toString();
		}
		return javaPackage;
	}
	
	/**
	 * Gets the qualified class name for a generated business object (castor wrapper) class. 
	 */
	public String JavaQualClassName() {
		return JavaPackage() + "." + NameS();
	}
	
	
	public String CastorQualClassName() {
		return CastorPackage() + "." + CastorClassName();
	}
	
	public String CastorPackage() {
		return "alma.entity.xmlbinding." + NameS().toLowerCase();
	}
	
	public String CastorClassName() {
		// default, to be overridden
		return ( NameS() + 'T' );
	}
	
	/**
	 * Same as {@link ModelElement#Documentation() Documentation}, 
     * but with a naughty text substituted for a missing documentation string.
	 */
	public String Doc() {
        String doc = null;   
        if (!hasDocumentation()) {
			doc = "Hey! There's no documentation here!";
        }
        else {
            doc = Documentation().toString();
        }
		return doc;
	}
	
	
	// Hierarchy Navigation, Queries
	// ==================================================================================
	
	
    @Override
	public boolean hasDocumentation() {
    	if (Documentation() == null) return false;
        String d = Documentation().toString().trim();
        return (d != null && d.length() > 0);
    }
    
	public boolean hasAssociations() {
		return !AssociationEnd().isEmpty();
	}
	
	@Override
	public boolean hasAttributes() {
		return !Attribute().isEmpty();
	}
	
	/**
	 * Gets all non-abstract subclasses from the entire inheritance tree.
	 * No filtering by stereotype is done, so all <code>Entity</code>s, <code>DependentClass</code>es
	 * etc will be included.
	 * @see Class#SubClass()
	 */
	public ElementSet SubClassDeep()
	{
		ElementSet allsubclasses = new ElementSet();
		recursiveSubClassDeep(this, allsubclasses);
		return org.openarchitectureware.core.meta.util.MMUtil.sort(allsubclasses, ConsistentOrderingComparator.getInstance());		
	}
	
	/**
	 * Same as SubClassDeep(), but includes this class itself (at the beginning of the list) if it is not abstract.
	 */
	public ElementSet SameAndSubClassDeep() {
		ElementSet allsubclasses = SubClassDeep();
		if (!isAbstract()) {
			allsubclasses.add(0, this);
		}
		return allsubclasses;
	}

	private void recursiveSubClassDeep(Class clazz, ElementSet allsubclasses)
	{
		ElementSet subclasses = clazz.SubClass();
		
		for (Iterator iter = subclasses.iterator(); iter.hasNext();)
		{
			Class subclass = (Class) iter.next();
			if (!subclass.isAbstract())
			{
				allsubclasses.add(subclass);
			}
			recursiveSubClassDeep(subclass, allsubclasses);
		}
	}
	
	/**
	 * Returns all "parts" that are referenced by this class through composition. 
	 * @return <code>ElementSet</code> containing <code>AssociationEnd</code>s.
	 */
	public ElementSet PartAssociationEnd()
	{
		ElementSet filtered = MMUtil.filter(AssociationEnd(), new Predicate()
				{
			@Override
			public boolean filter(Element el)
			{
				AssociationEnd e = (AssociationEnd) el;
				//System.err.println( Class().Name()+"."+RoleName()+":"+e.get )
				return e.Opposite().isComposition();
			}
			@Override
			public Element mapElement(Element el)
			{
				AssociationEnd e = (AssociationEnd) el;
				return e.Opposite();
			}
		});
	    return org.openarchitectureware.core.meta.util.MMUtil.sort(filtered, ConsistentOrderingComparator.getInstance());		
	}

	
	/**
	 * Adds all <code>DependentClass</code>-subclasses of this class, 
	 * as well as all <code>DependentClass</code>s that are contained through composition. 
	 * @param parts 
	 * @param recursive
	 */
	public void collectParts(Set<DependentClass> allParts, boolean recursive)  
	{
		ElementSet parts = new ElementSet();
		for (Iterator iter = PartAssociationEnd().iterator(); iter.hasNext();) {
			AssociationEnd assocEnd = (AssociationEnd) iter.next();
			parts.add(assocEnd.Class());
		}
		for (Iterator iter = SubClass().iterator(); iter.hasNext();) {
			Class subclass = (Class) iter.next();
			if (subclass instanceof DependentClass) {
				parts.add(subclass);
			}
		}
		
		for (Iterator iter = parts.iterator(); iter.hasNext();) {
			Class clazz = (Class) iter.next();
			
			// todo: revisit aggregation, composition, inheritance...
			if (!( clazz instanceof DependentClass )) {
				Checks.warn(this, getClass().getName() + " '" + QualifiedName() + "' must not associate class '"
						+ clazz.Name() + "' (type: '" + clazz.getClass().getName() + 
						"') by composition! Normal association might be fine though.");
			}
			DependentClass part = (DependentClass) clazz;
			
			if (!allParts.contains(part)) {
				allParts.add(part);
				if (recursive) {
					part.collectParts(allParts, true);
				}				
			}
		}
	}

	
	/**
	 * Returns the inheritance-path from the base class to this class
	 * (single-inheritance is presumed, relying on whatever Class::SuperClass delivers).
	 * E.g., for a Rectangle instance this will return [TargetArea, Rectangle].
	 */
	public ElementSet SuperHierarchy() {
		// re-use existing method
		List<AlmaAbstractClass> thisAndSuper = new ArrayList<AlmaAbstractClass>();
		thisAndSuper.add(this);
		collectSuperClasses(thisAndSuper);
		// flip direction to top-down
		Collections.reverse(thisAndSuper);
		// convert result to oAW-Type
		ElementSet ret = new ElementSet();
		for (AlmaAbstractClass c : thisAndSuper)
			ret.add(c);
		return ret;
	}

	
	protected void collectSuperClasses(Collection<AlmaAbstractClass> superClasses) {
		if (hasSuperClass()) {
			AlmaAbstractClass superClass = (AlmaAbstractClass) SuperClass();
			superClasses.add(superClass);
			// upward recursion
			superClass.collectSuperClasses(superClasses);
		}	
	}
	
	public ElementSet OneToOnePartAssociationEnd() {
		ElementSet allParts = PartAssociationEnd();
		return MMUtil.filter( allParts, new Predicate() {
			@Override
			public boolean filter(Element el) {
				// already the "other" end!
				AssociationEnd e = (AssociationEnd) el;
				return MMUtil.isMultiplicityOne(e.MultiplicityMax());
			}
		});
	}
	
	public ElementSet OneToManyPartAssociationEnd() {
		ElementSet allParts = PartAssociationEnd();
		return MMUtil.filter( allParts, new Predicate() {
			@Override
			public boolean filter(Element el) {
				// already the "other" end!
				AssociationEnd e = (AssociationEnd) el;
				return MMUtil.isMultiplicityMany(e.MultiplicityMax());
			}
		});
	}
	
	/**
	 * Returns references to classes, typically entities.
	 * Only associations that are neither aggregations nor compositions are considered.
	 *  
	 * @return  list of <code>AssociationEnd</code> objects for the referenced classes.
	 */
	public ElementSet ReferenceAssociationEnd()
	{
		return MMUtil.filter(AssociationEnd(), new Predicate()
				{
			@Override
			public boolean filter(Element el)
			{
				AssociationEnd e = (AssociationEnd) el;
//				System.err.print("ReferenceAssociationEnd: " + e.Class().Name() + " - " + e.Opposite().Class().Name());
				boolean isReferenceAssoc = ( !e.isComposition() && !e.isAggregate() && 
                                !e.Opposite().isComposition() && !e.Opposite().isAggregate() && 
                                e.Opposite().isNavigable() && !((AlmaAbstractClass)e.Opposite().Class()).NotToBeGenerated());
//				System.err.println(" " + isReferenceAssoc);
				return isReferenceAssoc;
			}
			@Override
			public Element mapElement(Element el)
			{
				AssociationEnd e = (AssociationEnd) el;
				return e.Opposite();
			}
		});
	}	

	public ElementSet EntityReferenceAssociationEnd()
	{
	    ElementSet filtered = MMUtil.filter(ReferenceAssociationEnd(), new Predicate()
				{
			@Override
			public boolean filter(Element el)
			{
				AssociationEnd e = (AssociationEnd) el;
				return ( e.Class() instanceof Entity );
			}
		});		
	    return org.openarchitectureware.core.meta.util.MMUtil.sort(filtered, ConsistentOrderingComparator.getInstance());
	}
	

    public ElementSet EntityPartReferenceAssociationEnd()
    {
        ElementSet filtered = MMUtil.filter(ReferenceAssociationEnd(), new Predicate()
                {
            @Override
				public boolean filter(Element el)
            {
                AssociationEnd e = (AssociationEnd) el;
                return ( e.Class() instanceof IdentifiablePart );
            }
        });     
        return org.openarchitectureware.core.meta.util.MMUtil.sort(filtered, ConsistentOrderingComparator.getInstance());
    }

    /**
     */
    public ElementSet EntityOrEntityPartReferenceAssociationEnd() {
   	 ElementSet ret = new ElementSet();
   	 ret.addAll(EntityReferenceAssociationEnd());
   	 ret.addAll(EntityPartReferenceAssociationEnd());
   	 return ret;
    }

    
    /**
	 * @return
	 */
	public boolean hasParts()
	{
		return !PartAssociationEnd().isEmpty();
	}
	

	// Composition-Parent stuff
	// ==================================================================================
	// We offer one method that returns the Set of all parent Classes.
	// Then, we offer some methods to look up info about each single parent Class.
	
	private ElementSet parents = new ElementSet();
	private List<AlmaAssociationEnd> parentAssocEnds = new ArrayList<AlmaAssociationEnd>();
	private List<AlmaAbstractClass> classSeenByParents = new ArrayList<AlmaAbstractClass>();
	private List<String> roleNameInParents = new ArrayList<String>();
	
	private boolean parentsComputed = false;

	/** 
	 * Initializes the above fields - if necessary
	 */
	private void computeParents() {
		
		if (parentsComputed)
			return;

		// find all possible parents
		// --------------------------
		Set<AlmaAbstractClass> thisAndSuper = new HashSet<AlmaAbstractClass>();
		thisAndSuper.add(this);
		collectSuperClasses(thisAndSuper);
		
		ElementSet potentialParents = new ElementSet();
		for (Iterator<AlmaAbstractClass> iter = thisAndSuper.iterator(); iter.hasNext();) {
			AlmaAbstractClass clazz = iter.next();
			potentialParents.addFlat(MMUtil.filter(clazz.AssociationEnd(), new Predicate() {
				@Override
				public boolean filter(Element el) {
					AssociationEnd e = (AssociationEnd) el;
				// return e.Opposite().isComposition();  // good for old MD and OAW 3
					return e.isComposition();
				}
				@Override
				public Element mapElement(Element el) {
					AssociationEnd e = (AssociationEnd) el;
					return e.Opposite();
				}
			}));
		}
		
		// store info about each parent
		// -----------------------------
		for (Object pp : potentialParents) {
			AlmaAssociationEnd possibleParentAssocEnd = (AlmaAssociationEnd)pp;

			parentAssocEnds.add(possibleParentAssocEnd);
			parents.add(possibleParentAssocEnd.AlmaClass());
			
			if (possibleParentAssocEnd.Association() instanceof MixedSubclassesAssociation
			 || possibleParentAssocEnd.Opposite().MultiplicityMaxAsInt() == 1) {

				// use our base class that is contained in the parent
				classSeenByParents.add( (DependentClass) possibleParentAssocEnd.Opposite().Class() );
				roleNameInParents.add( ((AlmaAssociationEnd)possibleParentAssocEnd.Opposite()).UpperCaseRoleName() );
				
			} else {
				classSeenByParents.add( this );
				roleNameInParents.add( this.NameS() );
			}
//			System.out.println("DependentClass " + NameS() + " appears to containment parent '" 
//					+ possibleParentAssocEnd.AlmaClass().NameS() + "' as '" + possibleParentAssocEnd.Opposite().Class().NameS() + "'.");
		}
		
		parentsComputed = true;
	}
	
	/** little helper for the info-getters */
	private int indexOf (AlmaAbstractClass parent) {
		int s = parents.size();
		for (int i=0; i<s; i++) {
			if (parent == parents.get(i))
				return i;
		}
		throw new IllegalArgumentException("not a parent of "+NameS()+" :"+parent.NameS());
	}
	
	/**
	 * Returns the possible "parent" classes of this class, i.e. those
	 * classes that are composed of this class in the UML model.
	 * 
	 * Base classes are also considered in finding the parent class(es).
	 * 
	 * The returned elements are valid arguments to the other parent-
	 * related methods in this meta-class.
	 * 
	 * @return the set of AlmaAbstractClasses being the possible parents 
	 */
	public ElementSet Parents() {
		computeParents();
		return parents;		
	}

	/**
	 * True if there is one or more possible composition-parents to this class. 
	 */
	public boolean hasParent() {
		computeParents();
		return (parents.size() > 0);
	}

	
	/**
	 * Returns the AlmaAssociationEnd for the specified composition parent
	 * @return the AlmaAssociationEnd for the specified composition parent
	 */
	public AlmaAssociationEnd ParentAssocEnd (AlmaAbstractClass parent) {
		computeParents();
		int parentIndex = indexOf(parent);
		return parentAssocEnds.get(parentIndex);
	}

	/**
	 * Returns the class that the specified composition parent sees for this class.
	 * That is
	 * <ul>
	 * <li><b>this class</b> if the parent class directly contains this class.
	 * <li><b>this class</b> if the parent class indirectly contains this class through a base class, 
	 *     using a normal association from the composition parent to the base class
	 * <li><b>the directly associated base class</b> if the parent class indirectly contains this class through a base class, 
	 *     using a stereotyped as &lt;&lt;mixedSubclasses&gt;&gt; association.
	 * </ul>
	 */
	public AlmaAbstractClass ClassSeenByParent (AlmaAbstractClass parent) {
		computeParents();
		int parentIndex = indexOf(parent);
		return classSeenByParents.get(parentIndex);
	}
	
	/**
	 * Returns the rolename that the specified composition parent sees for this class.
	 */
	public String RoleNameInParent (AlmaAbstractClass parent) {
		computeParents();
		int parentIndex = indexOf(parent);
		return roleNameInParents.get(parentIndex);
	}
	
	
	
	public boolean isVOClass() {
		return VOConfig.isVOClass(this);
	}
	
	public String NewLine() {
		return "\n";
	}

	/**
	 * Gets the index of <code>childOfInterest</code> among possible other child classes 
	 * which may result in a schema <code>choice</code> element, such that the index matches 
	 * that assigned by Castor in the corresponding binding class. 
	 * @param child
	 * @return
	 */
	public String getXSDChoiceIndex(DependentClass childOfInterest)
	{
		int index = 1;
		
		if (childOfInterest.hasSubClass()) {
			// check if there are other children with subclasses, which would thus translate to xsd choice elements
			for (Iterator iter = PartAssociationEnd().iterator(); iter.hasNext();) {
				Class child = ((AssociationEnd) iter.next()).Class();
				if (child == childOfInterest) {
					break;
				}
				if (child.hasSubClass()) {
					index++;
				}
			}
		}
		
		return ( index < 2 ? "" : Integer.toString(index) );
	}

	
// these don't make sense for subsystem, which currently also inherits from this 	
//	/**
//	 * Gets the xml schema namespace to be used for this class.
//	 */
//	public abstract String XSDNamespace();
//	
//	/**
//	 * Gets the xml schema namespace alias to be used for this class.
//	 */
//	public abstract String XSDNamespaceAlias();
//	
	
	

	
}
