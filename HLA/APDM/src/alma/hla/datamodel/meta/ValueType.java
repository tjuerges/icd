package alma.hla.datamodel.meta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openarchitectureware.meta.uml.Type;



public class ValueType extends AlmaAbstractClass //Class { // TODO check deriv. from AlmaAbstractClass
{	
	private static List simpleTypes = new ArrayList();
	
	public ValueType() {
		simpleTypes.add( this );
	}
	
	public static ValueType getType( String name ) {
		Iterator i = simpleTypes.iterator();
		while (i.hasNext())
		{
			ValueType element = (ValueType) i.next();
			if ( element.NameS().equals(name)) return element;
		}
		return null;
	}
	
	public static boolean isInstance( Type candidate ) {
		return getType( candidate.NameS() ) != null;
	}
	

	public String XSDNamespace() {
		return "Alma/ValueTypes";
	}
	
	public String XSDNamespaceAlias() {
		return "val";
	}
	
	@Override
	public String Label()
	{
		return "[valuetype] " + Name().toString();
	}

	
	/**
	 * 
	 * @see alma.hla.datamodel.meta.AlmaAbstractClass#CastorPackage()
	 */
	@Override
	public String CastorPackage()
	{
		return "alma.entity.xmlbinding.valuetypes";
	}
	
	/**
	 * msc (2010-03) When generating schemas, we treat enumerations
	 * of Integers differently from enumerations of other things.
	 */
	public boolean notContainsOnlyEnumerationsOfNonIntegers (
	        org.openarchitectureware.core.meta.core.ElementSet almaAttributes) {
	    for (Object o : almaAttributes) {
	        AlmaAttribute a = (AlmaAttribute) o;
	        if (!a.isEnumeration())
	          return true;
	        AlmaEnumeration e = (AlmaEnumeration)a.Type();
	        if (e.isInteger())
	          return true;
	    }
	    return false;
	}
	
}
