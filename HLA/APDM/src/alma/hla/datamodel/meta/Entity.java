package alma.hla.datamodel.meta;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.Dependency;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.ConsistentOrderingComparator;
import alma.hla.datamodel.util.MMUtil;
import alma.hla.datamodel.xsd.SchemaUsher;

public class Entity extends AlmaAbstractClass
{
	/** 
	 * key = qualified entity name, value = Entity object 
	 */
	private static HashMap<String, Entity> s_allEntities = new HashMap<String, Entity>();
	
	private Set<DependentClass> m_ownedClasses;
	
	private static String cvsVersion;
	private static String schemaVersion;
	
	public Entity()
	{
	}
 
	public String TestProp() {
		return NameS();
	}
	
	/**
	 * Enforces the rules for associations among <code>Entity</code>s 
	 * and <code>DependentClass</code>s.
	 * Builds up a set of owned classes (parts and others) so that the templates don't need
	 * to implement the various recursions themselves.
	 * @see de.bmiag.genfw.meta.Element#checkConstraints()
	 */
	@Override
	public void checkConstraints() {
		
		super.checkConstraints();

		if (hasSuperClass() && !(SuperClass() instanceof DependentClass)) {
			Checks.warn(this, "Entity '" + Name() + "': illegal base type " + SuperClass().Name() 
					+ " found. Only DependentClass is allowed.");
		}
		
		m_ownedClasses = new HashSet<DependentClass>();

		// set this entity as the owner of all dependent classes in its package
		org.openarchitectureware.meta.uml.classifier.Package pack = Package();
		for (Iterator iter = pack.Class().iterator(); iter.hasNext();) {
			Object cand = iter.next();
			if (cand instanceof DependentClass) {
				DependentClass dep = (DependentClass) cand;
				dep.setOwnerEntity(this);
				m_ownedClasses.add(dep);
			}
			else if ((cand instanceof Entity) && cand != this) {
				String msg = "Only one Entity per UML package allowed. " + 
					"Package '" + pack + "' contains entities '" + ((Entity) cand).Name() +
					"' and '" + Name() + "'.";
				Checks.warn(this, msg);
			}
		}
		
		String entityName = QualifiedName();

		if (! (pack instanceof NonGeneratedPackage)) {
			s_allEntities.put(entityName, this);
		}
		
//		 recursively collect all DependentClasses that (indirectly) belong to this entity
//		collectParts(m_ownedClasses, true);
		
		// set this entity as the owner of all parts
//		for (Iterator iter = m_ownedClasses.iterator(); iter.hasNext();) {
//			DependentClass part = (DependentClass) iter.next();
//			part.setOwnerEntity(this, true);
//		}
//		// collect super classes of this entity's parts, together with their parts
//		// if a super class belongs to this entity, then all its parts are assumed to do so, too
//		Set moreOwnedClasses = new HashSet(); // to avoid concurrent modif. exc
//		for (Iterator iter = m_ownedClasses.iterator(); iter.hasNext();) {
//			DependentClass part = (DependentClass) iter.next();
//			Set superClasses = new HashSet();
//			part.collectSuperClasses(superClasses);
//			for (Iterator superClassIter = superClasses.iterator(); superClassIter.hasNext();) {
//				DependentClass superClass = (DependentClass) superClassIter.next();
//				if (superClass.OwnerEntity() == this) {
//					Set superParts = new HashSet();
//					superClass.collectParts(superParts, true);
//					for (Iterator superPartsIter = superParts.iterator(); superPartsIter.hasNext();) {
//						DependentClass superPart = (DependentClass) superPartsIter.next();
//						superPart.setOwnerEntity(this, false);
//					}
//					moreOwnedClasses.add(superClass);
//					moreOwnedClasses.addAll(superParts);
//				}
//			}
//		}
//		m_ownedClasses.addAll(moreOwnedClasses);
				
		System.out.print("entity "+entityName + " owns " + m_ownedClasses.size() + " classes: ");
		for (Iterator<DependentClass> iter = m_ownedClasses.iterator(); iter.hasNext();) {
			DependentClass part = iter.next();
			System.out.print(part.Name() + ", ");
		}
		System.out.println();

		//Dumper.dump(this);
		
		return;
	}

	public static Entity findEntity(String qualifiedName)
	{
		Entity e = s_allEntities.get(qualifiedName);
		return e;
	}
	
	/**
	 * Gets all dependent classes that are directly or indirectly contained through composition.
	 */
	public ElementSet OwnedClass()
	{
		ElementSet es = new ElementSet();
		es.addAll(m_ownedClasses);
	    return org.openarchitectureware.core.meta.util.MMUtil.sort(es, ConsistentOrderingComparator.getInstance());
//		return es;
	}
	
	public Set<DependentClass> IdentifiablePart() {
		Set<DependentClass> ll = m_ownedClasses;
		Set<DependentClass> ip = new HashSet<DependentClass> ();
		for (DependentClass c : ll) {
			if (c instanceof IdentifiablePart )
				ip.add(c);
		}
		return ip;

	}
	
	
	/**
	 * Gets a list of all <code>Entity</code> objects that are directly or indirectly 
	 * referenced from this <code>Entity</code> or any of its aggregated classes.
	 * <p>
	 * Currently a dummy impl that returns all Entities from the model, except itself
	 * and at most one Entity per XML Namespace... hack as can be...
	 * @return
	 */
	public ElementSet AssociatedEntity()
	{
		ElementSet es = new ElementSet();

		// todo: traverse the tree etc. instead of returning ALL other entities...
		
		// this is part of the quick hack... later all ns aliases must be distinct
		Set<String> aliases = new HashSet<String>();
		aliases.add(XSDNamespaceAlias());
		for (Iterator<Entity> iter = s_allEntities.values().iterator(); iter.hasNext();) {
			Entity entity = iter.next();
			if (entity != this) {// && !aliases.contains(entity.XSDNamespaceAlias())) {
				es.add(entity);
				aliases.add(entity.XSDNamespaceAlias());
			}
		}
		return es;
	}

	public ElementSet EntityExceptThis()
	{
		ElementSet es = new ElementSet();
		for (Iterator iter = s_allEntities.values().iterator(); iter.hasNext();) {
			Entity entity = (Entity) iter.next();
			if (entity != this) {
				es.add(entity);
			}
		}
		return es;
	}

	public String XmlSchemaName() {
		return SchemaUsher.getSchemaName(QualifiedName());
	}
	
	
	@Override
	public String Label()
	{
		return "[Entity] " + NameS();
	}

	
	public ElementSet UsingSubsystem() {
		return MMUtil.filter( Provision(), new Predicate() {
			@Override
			public boolean filter(Element el) {
				Dependency d = (Dependency) el;
				return d.Client() instanceof Subsystem;
			}
			@Override
			public Element mapElement(Element el) {
				Dependency d = (Dependency) el;
				return d.Client();
			}
		});
	}
	
	
	/**
	 * Entity classes are mapped to schema elements (not types) which for historical reasons have no "T" suffix.
	 * @see alma.hla.datamodel.meta.AlmaAbstractClass#CastorClassName()
	 */
	@Override
	public String CastorClassName() {
		return ( Name().toString() );
	}
	

	/**
	 * Gets the xml schema namespace to be used for this entity.
	 * @return
	 */
	public String XSDNamespace() {
		return SchemaUsher.getXsdNamespace(QualifiedName());
	}
	
	public String XSDNamespaceAlias() {
		return SchemaUsher.getXsdNamespaceAlias(QualifiedName());
	}

	/**
	 * @param cv CVS Version of Datamodel file
	 */
	public static void setCvsVersion(String cv) {
		cvsVersion = cv;		
	}
	
	public String CvsVersion() {
		return cvsVersion;
	}

	/**
	 * @param sv Schema Version of Datamodel file
	 */
	public static void setSchemaVersion(String sv) {
		schemaVersion = sv;		
	}
	
	public String SchemaVersion() {
		return schemaVersion;
	}
	
}
