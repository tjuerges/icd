/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.meta;

import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

/**
 * @author hsommer
 * created Dec 19, 2003 4:12:49 PM
 */
public class AlmaAssociationEnd extends AssociationEnd
{
	public AlmaAssociationEnd()
	{
		super();
//		System.out.println("AlmaAssociationEnd()");
	}

	public boolean multiplicityMinNotOne() throws NumberFormatException
	{
		int multMin = Integer.parseInt(MultiplicityMin());
		return ( multMin != 1 );
	}

	public boolean multiplicityMaxNotOne() throws NumberFormatException
	{
		int multMax = Integer.parseInt(MultiplicityMax());
		return ( multMax != 1 );
	}

	public String multiplicityMaxForXML()
	{
		int multMax = Integer.parseInt(MultiplicityMax());
		
		return ( multMax > 0 ? MultiplicityMax() : "unbounded");
	}
   
   public String UpperCaseRoleName() {
      return RoleName().toString().substring(0,1).toUpperCase()+
            RoleName().toString().substring(1);
   }

   public boolean MultiplicityOneToInfinity() throws NumberFormatException {
      return (MultiplicityMin().equals("1") &  MultiplicityMax().equals("-1"));
   }
   
   public boolean MultiplicityMaxIsGreaterThanOne() throws NumberFormatException {
      int max = Integer.parseInt(MultiplicityMax());
      return (max > 1 || max == -1);
   }
   
   public AlmaAssociationEnd AlmaOpposite() {
	   return (AlmaAssociationEnd)Opposite();
   }
   
   public AlmaAbstractClass AlmaClass() {
	   return (AlmaAbstractClass)Class();
   }
   
   public AlmaAbstractClass AlmaOppositeClass() {
	   return (AlmaAbstractClass)Opposite().Class();
   }
   
	/**
	 * Gets the index of the schema <xsd:choice> element for the 'container' class,
	 * or an empty string if there is zero or one choice element in the schema definition 
	 * of the class which contains the other class.  
	 * Used to reengineer the numbering of choice elements done by Castor.
	 */
	public String XSDChoiceIndex() 
	{
		AlmaAbstractClass container = (AlmaAbstractClass) Opposite().Class();
		DependentClass child = (DependentClass) Class();
		String index = container.getXSDChoiceIndex(child);
		return index;
	}

//    TODO: public String toString() {
//    	if ( Class() != null ) {
//            String assocStr = " connecting classes '" + Class().NameS() + "' with '" + Opposite().Class().NameS() + "'.";
//            return super.toString() + assocStr;
//    	} else {
//    		System.err.println("Class() is null: "+this.NameS());
//    		return super.toString();
//    	}
//    }

}
