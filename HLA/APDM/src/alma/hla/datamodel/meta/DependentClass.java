package alma.hla.datamodel.meta;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.util.MMUtil;


public class DependentClass extends AlmaAbstractClass 
{
	
	
	public DependentClass() {
	}
	
	@Override
	public void checkConstraints() 
	{
		super.checkConstraints();

		if (hasSuperClass() && !(SuperClass() instanceof DependentClass))
		{
			Checks.warn(this, "dependent class '" + Name() + "' can only inherit from another dependent class, " + 
					"but not from '" + SuperClass().Name() + "' of type '" + SuperClass().getClass().getName() + "'.");
		}
		
//		if (isIdenticalToSuperclass()) {
//			System.out.println("dependent class " + Name() + " can be substituted with " + SuperClass().Name());
//		}
		return;
	}

	
	@Override
	public String Label() {
		return NameS();
	}

	
	// Owner-Entity stuff
	// ==================================================================================
	
	
	private Entity m_ownerEntity;

	/**
	 * @param owner  the entity that owns this dependent class
	 * @param byComposition  true if there's an entity that uses this class as a part in its composition tree.
	 * 						 false otherwise (e.g., if this class belongs to a base class tree used for a part) 
	 */
	void setOwnerEntity(Entity owner) 
	{
		if (m_ownerEntity != null && !m_ownerEntity.equals(owner) && isPrivate()) {
			Checks.warn(this, "Part '" + Name() + "' is private and must not be contained by composition in both entity classes " +
					owner.QualifiedName() + " and " + m_ownerEntity.QualifiedName());
		}
		m_ownerEntity = owner;
	}

	public Entity OwnerEntity()
	{
	    if (m_ownerEntity == null) {
	    	Checks.warn( this, "DependentClass '" + Name() + "' does not belong to an Entity class. Make sure this class and the Entity are in the same UML package.");
	    }
		return m_ownerEntity;		
	}


	
	// Namespaces, Qualified Names
	// ==================================================================================
		
	/**
	 * Gets the xml schema namespace from the owner entity.
	 * @return
	 */
	public String XSDNamespace() {
		Entity owner = OwnerEntity();
		return owner.XSDNamespace();
	}
	
	public String XSDNamespaceAlias() {
		Entity owner = OwnerEntity();
		return owner.XSDNamespaceAlias();
	}

	/**
	 * @see alma.hla.datamodel.meta.AlmaAbstractClass#CastorPackage()
	 */
	@Override
	public String CastorPackage() {
		return OwnerEntity().CastorPackage();
	}
}