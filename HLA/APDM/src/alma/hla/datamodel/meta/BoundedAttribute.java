/*
 * Created on Dec 16, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.meta;

import alma.hla.datamodel.util.MMUtil;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class BoundedAttribute extends AlmaAttribute
{
	private String minValue;
	private String maxValue;

	@Override
	public void checkConstraints() {
		super.checkConstraints();
		String typeName = Type().NameS();
		MMUtil.assertIsOneOf( this, typeName, new String[]{"float","Integer","double","long"}, "Bounded attributes can only be one of the following." );
	}


	public void setMin(String min) {
		minValue = min;
	}

	public void setMax(String max) {
		maxValue = max;
	}
	
	public String getMin() {
		return minValue;
	}

	public String getMax() {
		return maxValue;
	}
	 
	
}
