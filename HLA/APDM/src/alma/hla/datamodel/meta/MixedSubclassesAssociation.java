/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.meta;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Association;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

/**
 * Represents a UML association with stereotype &lt;&lt;mixedSubclasses&gt;&gt;,
 * which is only allowed under the constraints that
 * <ol>
 * <li>Class A associates class B by composition
 * <li>Class B has at least one subclass (C, D, ...)
 * <li>B has MultiplicityMax > 1
 * </ol>
 * Then the association will be understood such that A has children of mixed classes (B | C | D ...)*,
 * whereas by default a homogeneous list of children (B* | C* | D* ...) would have been assumed.
 * 
 * @author hsommer
 */
public class MixedSubclassesAssociation extends Association {

	public MixedSubclassesAssociation() {
		super();
	}
	
	@Override
	public void checkConstraints() {		
		super.checkConstraints();
		
		ElementSet assocEnds = AssociationEnd();
		Checks.assertElementCount(this, assocEnds, 2, "only binary associations allowed.");
		
		AssociationEnd assocEnd1 = (AssociationEnd) assocEnds.get(0);
		AssociationEnd assocEnd2 = (AssociationEnd) assocEnds.get(1);
		
		AssociationEnd multiEnd = null;
		if (assocEnd1.isComposition()) {
			// Note that isComposition() is true on child end, not on parent end where the solid diamond is drawn
			Checks.assertTrue(this, assocEnd1.isMultiple(), "only allowed for composition with MultiplicityMax > 1");
			multiEnd = assocEnd1;
		}
		else if (assocEnd2.isComposition()) {
			Checks.assertTrue(this, assocEnd2.isMultiple(), "only allowed for composition with MultiplicityMax > 1");			
			multiEnd = assocEnd2;
		}
		else {
			Checks.error(this, "one association end must be a composition.");
		}
				
		Checks.assertTrue(this, multiEnd.Class().hasSubClass(), "composition-child class must have at least one subclass.");
		
		return;
	}
	
}
