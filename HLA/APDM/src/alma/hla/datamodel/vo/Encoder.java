/*
 * Created on Jan 14, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package alma.hla.datamodel.vo;

/**
 * @author admin
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class Encoder
{
	
	public abstract String encodeTypeName( String modelTypeName );

	public abstract String getEncodingWidth(String string);

	public abstract String encodeValue(Object data);

	public abstract String encodeValue(int data);

	public abstract String encodeValue(float data);
	
	// more encoding methods for primitive types...

}
