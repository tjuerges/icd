/*
 * Created on Jan 14, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package alma.hla.datamodel.vo;

/**
 * @author admin
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StdEncoder extends Encoder
{

	@Override
	public String encodeTypeName(String modelTypeName)
	{
		return "StdEncoder"+modelTypeName;
	}

	@Override
	public String getEncodingWidth(String string)
	{
		return "42";
	}

	@Override
	public String encodeValue(Object data)
	{
		return data.toString();
	}

	@Override
	public String encodeValue(int data)
	{
		return String.valueOf(data);
	}

	@Override
	public String encodeValue(float data)
	{
		return String.valueOf(data);
	}
	
	
}
