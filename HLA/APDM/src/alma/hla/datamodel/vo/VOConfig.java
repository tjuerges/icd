/*
 * Created on Jan 14, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package alma.hla.datamodel.vo;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.meta.uml.classifier.Class;

import alma.hla.datamodel.meta.AlmaAttribute;
import alma.hla.datamodel.util.MMUtil;

public class VOConfig
{

	private static boolean fileloaded = false;
	private static Document doc;
	
	private static Map voElements = new HashMap();
	
	private static void ensureFileLoaded() {
		if ( !fileloaded ) {
			SAXBuilder builder = new SAXBuilder();
			FileInputStream fStream;
			try
			{
				String voconfigfile = System.getProperty("alma.voconfigfile");
				if (voconfigfile == null) {
					Checks.error(null,"property 'alma.voconfigfile' not set.");
				}
				fStream = new FileInputStream(voconfigfile);
				doc = builder.build(fStream);
				Element root = doc.getRootElement();
				List children = root.getChildren("vo");
				Iterator it = children.iterator();
				while (it.hasNext()) {
					Element element = (Element) it.next();
					String classname = element.getAttributeValue("classname");
					voElements.put( classname, element );
				}
				fStream.close();
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}
	
	public static boolean isVOClass( Class cls ) {
		ensureFileLoaded();
		String name = cls.Name().toString();
		boolean yesOrNo = voElements.containsKey(name);
		return yesOrNo;
	}

	/**
	 * @param attribute
	 * @return
	 */
	public static Element getFieldElement(AlmaAttribute attribute)
	{
		Element clsElement = getClassElement(attribute.Class());
		if (clsElement == null ) return null;
		List children = clsElement.getChildren("field");
		Iterator it = children.iterator();
		while (it.hasNext())
		{
			Element fieldElement = (Element) it.next();
			if ( fieldElement.getAttributeValue("name").equals(attribute.Name().toString())) {
				return fieldElement;
			}
			
		}
		Checks.warn(attribute, "No field definition found for attribute "+MMUtil.getLocationString(attribute));
		return null;
	}
	
	public static Element getClassElement(Class cls)
	{
		String classname = cls.Name().toString();
		Element clsElement = (Element)voElements.get(classname);
		if (clsElement == null) {
			Checks.warn(cls, "No vo definition found for class "+MMUtil.getLocationString(cls));
		}
		return clsElement;
	}
	

}
