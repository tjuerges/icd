/*
 * Created on Mar 28, 2004
 * 
 * To change the template for this generated file go to Window - Preferences -
 * Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.xsd;
import java.io.File;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
/**
 * @author jschwarz
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class SchemaUsherConfigReader {
	public static HashMap getUsherMappings(File mappingsFile, boolean verbose) {
		HashMap mappings = new HashMap();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(mappingsFile);
			// normalize text representation
			doc.getDocumentElement().normalize();
			if (verbose) {
				System.out.println("Root element of the doc is "
						+ doc.getDocumentElement().getNodeName());
			}
			NodeList listOfUsherMappings = doc.getElementsByTagName("ushermapping");
			int totalUsherMappings = listOfUsherMappings.getLength();
			if (verbose) {
				System.out.println("Total no of mappings : " + totalUsherMappings);
			}
			for (int s = 0; s < listOfUsherMappings.getLength(); s++) {
				Node firstUsherMappingNode = listOfUsherMappings.item(s);
				if (firstUsherMappingNode.getNodeType() == Node.ELEMENT_NODE) {
					Element firstUsherMappingElement = (Element) firstUsherMappingNode;
					//-------
					NodeList className = firstUsherMappingElement
							.getElementsByTagName("class");
					Element classNameElement = (Element) className.item(0);
					NodeList textFNList = classNameElement.getChildNodes();
					if (verbose) {
						System.out
								.println("Class Pathname : "
										+ ((Node) textFNList.item(0))
												.getNodeValue().trim());
					}
					//-------
					NodeList ownerName = firstUsherMappingElement
							.getElementsByTagName("namespace");
					Element namespaceNameElement = (Element) ownerName.item(0);
					NodeList textNsList = namespaceNameElement.getChildNodes();
					if (verbose) {
						System.out
								.println("Schema namespace : "
										+ ((Node) textNsList.item(0))
												.getNodeValue().trim());
					}
//					mappings.put(((Node) textFNList.item(0))
//							.getNodeValue().trim(),((Node) textNsList.item(0))
//							.getNodeValue().trim());
					String schemaNamespace = ((Node) textNsList.item(0))
						.getNodeValue().trim();
					//-------
					NodeList abbrevName = firstUsherMappingElement
							.getElementsByTagName("abbreviation");
					Element abbrevNameElement = (Element) abbrevName.item(0);
					NodeList textAbList = abbrevNameElement.getChildNodes();
					if (verbose) {
						System.out
								.println("Schema namespace abbreviation : "
										+ ((Node) textAbList.item(0))
												.getNodeValue().trim());
					}
//					mappings.put(((Node) textAbList.item(0))
//							.getNodeValue().trim(),((Node) textAbList.item(0))
//							.getNodeValue().trim());
					String abbrev = ((Node) textAbList.item(0))
						.getNodeValue().trim();
					//----
					String[] pair = { schemaNamespace, abbrev };
					mappings.put( ((Node) textFNList.item(0))
							.getNodeValue().trim() , pair );
				}//end of if clause
			}//end of for loop with s var
		} catch (SAXParseException err) {
			System.err.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.err.println(" " + err.getMessage());
		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return mappings;
	}//end of main
}