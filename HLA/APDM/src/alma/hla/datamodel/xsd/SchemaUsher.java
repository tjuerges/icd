/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2002
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.xsd;

import java.io.File;
import java.util.HashMap;

/**
 * Assigns classes to schemas. 
 * @author hsommer
 * created Feb 18, 2004 7:21:35 AM
 */
public class SchemaUsher
{

	private static File s_configfile;
	
	private static HashMap schemaMap = new HashMap();
//	static {
//		String[] pair = {"Alma/ObsPrep/ObsProject", "prj"};
//		schemaMap.put("data/obsproject/ObsProject", pair);
//		pair = new String[]{"Alma/ObsPrep/ObsProposal","prp"};
//		schemaMap.put("data/obsproject/ObsProposal", pair);
//		pair = new String[]{"Alma/ObsPrep/SchedBlock","sbl"};
//		schemaMap.put("data/schedblock/SchedBlock", pair);
//		pair = new String[] {"Alma/Dunno/ExecBlock","ebl"};
//		schemaMap.put("data/output/admin/ExecBlock", pair);
//		pair = new String[] {"Alma/ObsPrep/CatalogObject","clo"};
//		schemaMap.put("data/obsproject/CatalogObject", pair);
//		pair = new String[] {"Alma/Dunno/MainTable","mnt"};
//		schemaMap.put("aedf/MainTable",pair);
//	}

	/**
	 * 
	 */
	public SchemaUsher() 
	{
	}

	public static void setConfigFile(String fileName)
	{
		File file = new File(fileName);
		if (file.exists() && !file.isDirectory()) {
			s_configfile = file;			
			schemaMap = SchemaUsherConfigReader.getUsherMappings(s_configfile, false);
		}		
	}

	/**
	 * TODO: use config file... perhaps try to derive default NS from Entity name and associated subsystem.
	 * @param entityName entity name with UML package prefix
	 * @return xml schema namespace 
	 */
	public static String getXsdNamespace(String entityName) {

		String ns = null;
		if (schemaMap.containsKey(entityName))
			ns = ((String[])(schemaMap.get(entityName)))[0];
		else {
			System.err.println("entity '" + entityName + "' unknown. Using namespace 'Alma/XXX/notFound'");
			ns = "Alma/XXX/notFound";
		}
		return ns;
	}
	
	/**
	 * TODO: use config file... perhaps try to derive default NS from Entity name and associated subsystem.
	 * @param entityName entity name with UML package prefix
	 * @return xml schema namespace 
	 */
	public static String getXsdNamespaceAlias(String entityName) {
		String ns = null;
		if (schemaMap.containsKey(entityName))
			ns = ((String[])(schemaMap.get(entityName)))[1];
		else {
			System.err.println("entity '" + entityName + "' unknown. Using namespace alias 'nsXX'");
			ns = "nsXX";
		}
		return ns;
	}

	/**
	 * @param schemaName 
	 * @return
	 */
	public static String getSchemaName(String entityName) {
		String schema = entityName.substring(entityName.lastIndexOf('/')+1) + ".xsd";
		return schema;
	}
	
//	public static boolean belongsHere( Class cls ) {
//		ensureFileLoaded();
//		String name = cls.Name().toString();
//		boolean yesOrNo = voElements.containsKey(name);
//		return yesOrNo;
//	}


}
