package alma.hla.datamodel.workflow;

import org.openarchitectureware.core.debug.dumper.Dumper;
import org.openarchitectureware.core.meta.MetaEnvironment;
import org.openarchitectureware.core.meta.util.MMUtil;
import org.openarchitectureware.workflow.WorkflowComponent;
import org.openarchitectureware.workflow.WorkflowContext;
import org.openarchitectureware.workflow.ast.parser.Location;
import org.openarchitectureware.workflow.container.CompositeComponent;
import org.openarchitectureware.workflow.issues.Issues;
import org.openarchitectureware.workflow.monitor.ProgressMonitor;

import alma.hla.datamodel.util.AlmaModel;

public class ModelVerifier implements WorkflowComponent {

	private String metaEnvSlot;
	private String almaModelSlot;

	public void setMetaEnvironmentSlot(String sn) {
		this.metaEnvSlot = sn;
	}
	
	public void setAlmaModelSlot(String almaModelSlot) {
		this.almaModelSlot = almaModelSlot;
	}
	
	public void invoke(WorkflowContext ctx, ProgressMonitor mon, Issues issues) {
		MetaEnvironment me = (MetaEnvironment)ctx.get(metaEnvSlot);
		AlmaModel model = (AlmaModel)MMUtil.findAllInstances( AlmaModel.class ).get(0);
		ctx.set( almaModelSlot, model );
		Dumper.dump(model);
	}
	
	public void checkConfiguration(Issues arg0) {
		// TODO Auto-generated method stub

	}

	public CompositeComponent getContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	public Location getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setContainer(CompositeComponent container) {
		// TODO Auto-generated method stub
		
	}

	public void setLocation(Location location) {
		// TODO Auto-generated method stub
		
	}

}
