/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.workflow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Java;
import org.openarchitectureware.workflow.ant.Param;

/**
 * Modification of {@link org.openarchitectureware.workflow.ant.WorkflowAntTask}
 * that ensures that build errors are propagated.
 * 
 * @author hsommer
 */
public class AlmaOAWWorkflowAntTask extends Java {
    private String file;
    private List params = new ArrayList();

    public String getFile() {
        return file;
    }
    

    public void setFile(String file) {
        this.file = file;
    }
    

	@Override
	public void execute() throws BuildException {
		
		setFailonerror(true);
		
		// setting of parameters is copied from base class impl
        for (Iterator iter = params.iterator(); iter.hasNext();) {
            Param param = (Param) iter.next();
            if (param!=null) {
                String paramString = "-p"+param.getName()+"="+param.getValue();
                super.createArg().setValue(paramString);
                log("Adding param: " +paramString);
            }
        }
        log("Adding param: " +this.getFile());
        super.createArg().setValue(this.getFile());
        
//        super.setClassname(WorkflowRunner.class.getName());
        // we use a workflow runner that throws an exception if there are workflow errors
        super.setClassname(ApdmWorkflowRunner.class.getName());
        
        super.execute();
	}


    /**
     * Creates a parameter. Invoked by ant for each <tt>&lt;param name="..." value="..."/&gt;</tt> 
     * statement in the Ant file.
     * @return New {@link Param} instance 
     */
    public Object createParam() {
        Param param = new Param();
        this.params.add(param);
        return param;
    }

}
