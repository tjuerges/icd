package alma.hla.datamodel.workflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.openarchitectureware.workflow.WorkflowContext;
import org.openarchitectureware.workflow.issues.Issues;
import org.openarchitectureware.workflow.lib.AbstractWorkflowComponent;
import org.openarchitectureware.workflow.monitor.ProgressMonitor;

import alma.hla.datamodel.meta.Entity;
import alma.hla.datamodel.xsd.SchemaUsher;

public class APDMWorkflowInitializer extends AbstractWorkflowComponent {

	private String SchemaUsherConfigFile;
	private String VOConfigFile;
	private String ApdmFile;
	private String CvsVersion;
	private String SchemaVersion;
	
	public void invoke(WorkflowContext ctx, ProgressMonitor monitor,
			Issues issues) {
		System.setProperty("alma.voconfigfile", VOConfigFile );
		SchemaUsher.setConfigFile(SchemaUsherConfigFile);
		Entity.setCvsVersion(CvsVersion);
		Entity.setSchemaVersion(SchemaVersion);
	}

	public void setSchemaUsherConfigFile(String cf) {
		this.SchemaUsherConfigFile = cf;
	}
	
	public void setVoConfigFile(String cf) {
		this.VOConfigFile = cf;
	}
	
	public void setApdmFile(String cf) {
		this.ApdmFile = cf;
		File f = new File(cf);
	    File entries = new File(f.getParent()+File.separator+"CVS"+File.separator+"Entries");
	    String modelName = f.getName();
	    BufferedReader rdr=null;
		try {
			rdr = new BufferedReader(new FileReader(entries));
		} catch (FileNotFoundException e) {
			CvsVersion = "-1";
			System.out.println("CVS Entries file not found");
			//return;
		}
	    String line;
	    try {
			while ((line = rdr.readLine()) != null) {
				String[] fields = line.split("/");
				if (fields[1].equals(modelName)) {
					CvsVersion = fields[2];
					System.out.println("APDM CVS Version is: "+CvsVersion);
					break;
//					return;
				}
			}
//			CvsVersion = "-1";
			//		System.out.println("Couldn't find filename "+modelName+" in CVS/Entries");
			//		return;
		} catch (IOException e) {
			CvsVersion = "-1";
			System.out.println("Error in readLine from CVS/Entries");
			//return;
		}
	    File schemavers = new File(f.getPath()+".schemaVersion");
	    try {
	        rdr = new BufferedReader(new FileReader(schemavers));
		} catch (FileNotFoundException e) {
		SchemaVersion = "-1";
		System.out.println("Schema Version file "+schemavers.getPath()+ "not found");
		return;
		}
	    try {
         	    while ((line = rdr.readLine()) != null) {
		    SchemaVersion = line;
		    System.out.println("APDM Schema Version is: "+SchemaVersion);
		    return;
		    }
	    } catch (IOException e) {
	       	SchemaVersion = "-1";
		System.out.println("Error in readLine from schemaversion file");
		return;
	    }
		
		
	}
	
	public void checkConfiguration(Issues issues) {
		if (VOConfigFile == null) issues.addError(this, "No VO Config File specified!");
		if (SchemaUsherConfigFile == null) issues.addError(this, "No Schema Usher File specified!");
	}

}
