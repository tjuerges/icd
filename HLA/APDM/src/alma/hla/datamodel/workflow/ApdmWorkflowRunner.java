/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.workflow;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.tools.ant.BuildException;
import org.openarchitectureware.workflow.WorkflowRunner;
import org.openarchitectureware.workflow.monitor.NullProgressMonitor;
import org.openarchitectureware.workflow.monitor.ProgressMonitor;
import org.openarchitectureware.workflow.util.ResourceLoaderFactory;


/**
 * @author hsommer
 *
 */
public class ApdmWorkflowRunner extends WorkflowRunner {

	private static final String PARAM = "p";

	private static final String MONITOR = "m";

	/**
	 * 
	 */
	public ApdmWorkflowRunner() {
		super();
		System.out.println("*** ApdmWorkflowRunner gets used ***");
	}

	private static void wrongCall(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java " + WorkflowRunner.class.getName(), options);
		System.exit(0);
	}

	/**
	 * Convert command line arguments into a hashmap. Each parameter is expected
	 * in format <br>
	 * <tt>-p[paramname]=[paramvalue]
	 * @param args Program arguments
	 * @return A map containing all identified parameters
	 */
	private static Map resolveParams(String[] args) {
		Map params = new HashMap();
		if (args == null) {
			return params;
		}
		for (int i = 0; i < args.length; i++) {
			String[] string = args[i].split("=");
			if (string.length != 2) {
				throw new IllegalArgumentException("wrong param syntax (-p[paramname]=[paramvalue]). was : " + args[i]);
			}
			params.put(string[0], string[1]);
		}
		return params;
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Options options = new Options();
		Option monitorOption = OptionBuilder.withArgName("ProgressMonitorImpl").isRequired(false).hasArg().withDescription(
				"specify your ProgressMonitor implementation").create(MONITOR);
		options.addOption(monitorOption);
		Option paramOption = OptionBuilder.withArgName("name=value").hasArg().withDescription("used to parametrize the workflow").create(PARAM);
		options.addOption(paramOption);

		// create the parser
		CommandLineParser parser = new PosixParser();
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (ParseException exp) {
			System.err.println("Parsing arguments failed.  Reason: " + exp.getMessage());
			wrongCall(options);
			return;
		}
		if (line.getArgs().length != 1) {
			wrongCall(options);
		} else {
			ProgressMonitor monitor = new NullProgressMonitor();

			if (line.getOptionValue(MONITOR) != null) {
				try {
					Class clazz = ResourceLoaderFactory.createResourceLoader().loadClass(line.getOptionValue(MONITOR));
					monitor = (ProgressMonitor) clazz.newInstance();
				} catch (Exception e) {
					System.err.println("Couldn't instantiate " + line.getOptionValue(MONITOR)
							+ ". Using default implementation. (Message was " + e.getMessage() + ")");
				}
			}

			Map params = resolveParams(line.getOptionValues(PARAM));
			String wfFile = line.getArgs()[0];
			boolean success = new WorkflowRunner().run(wfFile, monitor, params, null);
			
			if (!success) {
				System.out.println("**************** ApdmWorkflowRunner failed ****************");
				throw new BuildException("**************** Workflow failed ****************");
			}
			else {
				System.out.println("**************** ApdmWorkflowRunner succeeded ****************");
			}
		}		
	}

}
