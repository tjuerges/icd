/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MMUtil.java
 */
package alma.hla.datamodel.util;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Namespace;
import org.openarchitectureware.core.xpand.syntax.Identifier;
import org.openarchitectureware.meta.uml.Type;
import org.openarchitectureware.meta.uml.classifier.Attribute;

import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.pred.PredicateFilter;

/**
 * @author jschwarz
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class MMUtil
{

//	private static ModelElement metaEnvSource = null;
//
//	public static void setAlmaMetaEnvSource(ModelElement e)
//	{
//		metaEnvSource = e;
//	}

	public static void assertElementCount(Element context, ElementSet set, int count, String msg)
	{
		if (set.size() != count)
			Checks.warn(context, msg);
	}

	public static void assertName(Element el, String name, String msg)
	{
		if (!el.Name().toString().equals(name))
			Checks.warn(el, msg);
	}

	public static String getLocationString(Element el)
	{
		if (el instanceof Attribute)
		{
			Attribute a = (Attribute) el;
			return a.Class().Name().toString() + "." + a.Name().toString();
		}
		return el.Name().toString();

	}

	/**
	 * @param unitAttr
	 * @param class1
	 * @param string
	 */
	public static void assertType(Attribute unitAttr, Class class1, String msg)
	{
		if (!class1.isInstance(unitAttr))
			Checks.warn(unitAttr, msg);
	}

	public static boolean isOneOf(String candidate, String[] list)
	{
		for (int i = 0; i < list.length; i++)
		{
			String string = list[i];
			if (candidate.equals(string))
				return true;
		}
		return false;
	}

	public static void assertIsOneOf(Element context, String candidate, String[] list, String msg)
	{
		if (!isOneOf(candidate, list))
		{
			StringBuffer m = new StringBuffer(msg);
			for (int i = 0; i < list.length; i++)
			{
				if (i > 0)
					m.append(",");
				String string = list[i];
				m.append(string);
			}
			Checks.warn(context, m.toString());
		}
	}

	public static ElementSet filter(ElementSet toBeFiltered, Predicate filterCondition)
	{
		PredicateFilter f = new PredicateFilter(toBeFiltered);
		ElementSet res = f.filterBy(filterCondition);
		return res;

	}

	public static ElementSet collectDescendents(Namespace ns)
	{
		ElementSet res = new ElementSet();
		recursiveCollectDescendents(ns, res);
		return res;
	}

	private static void recursiveCollectDescendents(Namespace ns, ElementSet res)
	{
		res.add(ns);
		Iterator i = ns.OwnedElement().iterator();
		while (i.hasNext())
		{
			Element me = (Element) i.next();
			res.add(me);
			if (me instanceof Namespace)
				recursiveCollectDescendents((Namespace) me, res);
		}

	}

	public static boolean isMultiplicityOne(String maxMult)
	{
		return maxMult.trim().equals("1");
	}

	public static boolean isMultiplicityMany(String maxMult)
	{
		return !isMultiplicityOne(maxMult);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	/// the methods below were taken from Allen's version of MMUtil -- todo: integrate this better
	///////////////////////////////////////////////////////////////////////////////////////////////
	
	static public String UpperCaseName(Identifier i) {
		return UpperCaseName(i.toString());
	}

	static public String LowerCaseName(Identifier i) {
		return LowerCaseName(i.toString());
	}

	static public String UpperCaseName(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1);
	}

	static public String LowerCaseName(String s) {
		return s.substring(0,1).toLowerCase() + s.substring(1);
	}
	
	static public String JavaType(Type t) {
		String typeName = t.Name().toString();
		String ret = null;
		
		if (typeName.equals("Integer")) {
			ret = "int";
		}
		else if (typeName.equals("Boolean")) {
			ret = "boolean";
		}
		else if (typeName.equals("string")) {
			ret = "String";
		}
		else {
			ret = typeName;
		}
		return ret;
	}

	static public String JavaBoxType(Type t) {
		String typeName = t.Name().toString();
		String ret = null;
		
		if (typeName.equals("Integer")) {
			ret = "Integer";
		}
		else if (typeName.equals("Boolean")) {
			ret = "Boolean";
		}
		else if (typeName.equals("string")) {
			ret = "String";
		}
		else if (typeName.equals("double")) {
			ret = "Double";
		}
		else if (typeName.equals("float")) {
			ret = "Float";
		}
		else if (typeName.equals("long")) {
			ret = "Long";
		}
		else if (typeName.equals("Date")) {
			ret = "Date";
		}
		else {
			ret = typeName;
		}
		return ret;
	}

	static public String CppType(Type t) {
		String typeName = t.Name().toString();
		String ret = null;
		
		if (typeName.equals("Integer")) {
			ret = "int";
		}
		else if (typeName.equals("Boolean")) {
			ret = "bool";
		}
		else if (typeName.equals("boolean")) {
			ret = "bool";
		}
		else if (typeName.equals("String")) {
			ret = "string";
		}
		else if (typeName.equals("byte")) {
			ret = "char";
		}
		else if (typeName.equals("char")) {
			ret = "unsigned char";
		}
		else {
			ret = typeName;
		}
		return ret;
	}

	/**
	 * Return true if this type requires an "equals" method in comparisons.
	 * @param t
	 * @return
	 */
	static public boolean equalsComparison(String typeName) {
		if (typeName.equals("String") || typeName.equals("Time")) {
			// We need to include all primitive types that are classes.
			return true;
		}
		return false;
	}
	
	static public String simpleName(String name) {
		String s = name;
		if (s.endsWith("Id"))
			return s.substring(0,s.length() - 2);
		else if (s.endsWith("IdArray"))
			return s.substring(0,s.length() - 7);
		else if (s.endsWith("IdSet"))
			return s.substring(0,s.length() - 5);
		else if (s.endsWith("IdList"))
			return s.substring(0,s.length() - 6);
		else if (s.endsWith("Array"))
			return s.substring(0,s.length() - 5);
		else if (s.endsWith("Set"))
			return s.substring(0,s.length() - 3);
		else if (s.endsWith("List"))
			return s.substring(0,s.length() - 4);
		return s;
	}
    
    /**
     * Returns the size of an ElementSet.
     * @param s the ElementSet  
     * @return an int.
     */
    static public String size(ElementSet s) {
        return ""+s.size();
    }
}
