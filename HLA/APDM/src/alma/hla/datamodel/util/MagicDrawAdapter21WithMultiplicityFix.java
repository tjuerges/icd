/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.transform.JDOMSource;
import org.jdom.xpath.XPath;
import org.openarchitectureware.core.frontends.xmi.Constants;
import org.openarchitectureware.core.frontends.xmi.mapping.DTMWrapper;
import org.openarchitectureware.core.frontends.xmi.mapping.DXM;
import org.openarchitectureware.core.frontends.xmi.mapping.MappingContext.InstantiatorElement;
import org.openarchitectureware.core.frontends.xmi.toolsupport.uml.magicdraw.MagicDrawAdapter21;
import org.openarchitectureware.core.frontends.xmi.toolsupport.uml.magicdraw.MagicDrawModuleMerger;

/**
 * @author hsommer
 *
 */
public class MagicDrawAdapter21WithMultiplicityFix extends MagicDrawAdapter21 {
	
	private static final Log LOG = LogFactory.getLog(MagicDrawAdapter21WithMultiplicityFix.class);

	/**
	 * Implementation copied from {@link org.openarchitectureware.core.frontends.xmi.toolsupport.uml.magicdraw.MagicDrawAdapter},
	 * with an additional fix for UML attribute multiplicity bug.
	 * 
	 * @see org.openarchitectureware.core.frontends.xmi.toolsupport.AbstractToolAdapter#loadDesign(java.lang.String)
	 */
	@Override
	protected DXM loadDesign(String designFileName) throws IOException {
		DTMWrapper result = new DTMWrapper(this.env);

		String moduleFileNames = null;
		try {
			moduleFileNames = StringUtils.trimToNull(this.env
					.getPropertyProvider().getStringProperty(
							Constants.MODULES));
		} catch (Exception ignore) {
		}

		// load model using the module merger
		MagicDrawModuleMerger merger = null;
		if (moduleFileNames == null) {
			merger = new MagicDrawModuleMerger(designFileName);
		} else {
			merger = new MagicDrawModuleMerger(designFileName, moduleFileNames);
		}
		merger.execute();
		
		// todo: change merger to return a JDOMSource instead of Source,
		// or figure out how to continue cleanly with just a Source
		JDOMSource mergedModel = (JDOMSource) merger.getModel();
		
		try {
			fixAttrMultiplicities(mergedModel);
		} catch (Exception e) {
			throw new IOException(e.toString());
		}
		
		result.setRootNode(result.load(mergedModel));
		return result;
	}


	/**
	 * MagicDraw 10.5 and 11.0 (and perhaps other versions too) omit the <code>value</code> attribute of 
	 * the <code>ownedAttribute/lowerValue</code> element if <code>value==0</code>.
	 * This confuses the instantiator, which then assumes a default UML attribute multiplicity 
	 * of "1" instead of the intended "0".
	 * <p>
	 * Here we correct this before the instantiator gets to see it, by inserting the <code>value="0"</code> 
	 * attribute in the design model.
	 * 
	 * @param mergedModel
	 * @return 
	 * @throws JDOMException
	 */
	protected void fixAttrMultiplicities(JDOMSource designModel) throws JDOMException {
		Document doc = designModel.getDocument();
		
		XPath xpath = XPath.newInstance("//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and lowerValue[not(@value)]]/lowerValue");
		List lowerValueList = xpath.selectNodes(doc);
		for (Iterator iter = lowerValueList.iterator(); iter.hasNext();) {
			Element lowerValueElem = (Element) iter.next();
			lowerValueElem.setAttribute("value", "0");
			LOG.debug("Fixed MultiplicityMin for UML attribute " + ((Element)lowerValueElem.getParent()).getAttribute("name").getValue());
		}
	}

	
	/**
	 * Same as super impl, except that it sets the default MultiplicityMin to "1" instead of "0".
	 * MagicDraw XMI omits the <code>lowerValue</code> and <code>upperValue</code> content of <code>ownedAttribute</code>
	 * when the UML attribute multiplicity is not specified in the model.
	 * According to UML standard, the default attribute multiplicity is [1].
	 * It is thus not clear why OAW uses a default attribute multiplicity of [0..1].
	 * 
	 * @see org.openarchitectureware.core.frontends.xmi.toolsupport.uml.magicdraw.MagicDrawAdapter#prepareAttribute(org.openarchitectureware.core.frontends.xmi.mapping.MappingContext.InstantiatorElement)
	 */
	@Override
	public void prepareAttribute(InstantiatorElement element) {
		setDefaultValue(element, "MultiplicityMin", "1");
		
		// "setDefaultValue" only works on first invocation, so MultiplicityMin default will not be overridden
		super.prepareAttribute(element); 
	}

}
