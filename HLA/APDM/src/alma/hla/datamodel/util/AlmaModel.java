/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.util;

import alma.hla.datamodel.meta.AlmaAbstractClass;
import alma.hla.datamodel.meta.Entity;
import alma.hla.datamodel.meta.NonGeneratedPackage;
import alma.hla.datamodel.meta.PhysicalQuantity;
import alma.hla.datamodel.meta.Subsystem;
import alma.hla.datamodel.meta.ValueType;
import alma.hla.datamodel.pred.Predicate;
import alma.hla.datamodel.pred.TypePredicate;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.core.meta.core.Element;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class AlmaModel extends Model 
{
	public AlmaModel() {
//		MMUtil.setAlmaMetaEnvSource( this );
	}
	
	public ElementSet AllElementsOfModel() {
		return org.openarchitectureware.core.meta.util.MMUtil.findAllInstances();
	}
	@Override
	public void checkConstraints()
	{	
		super.checkConstraints();
	}
	
	public ElementSet PhysicalQuantity() {
		ElementSet classes = MMUtil.collectDescendents(this);
		return MMUtil.filter(classes,new TypePredicate(PhysicalQuantity.class));
	}
	
	public ElementSet ValueType() {
		ElementSet classes = MMUtil.collectDescendents(this);
		return MMUtil.filter(classes,new TypePredicate(ValueType.class));
	}
	
	/**
	 * Gets all <code>Entity</code>s except those whose package is marked as <code>nonGenerated</code>. 
	 */
	public ElementSet Entity() {
		ElementSet classes = MMUtil.collectDescendents(this);
		ElementSet allEntities = MMUtil.filter(classes,new TypePredicate(Entity.class));
		ElementSet entities = MMUtil.filter(allEntities, new Predicate() {
			@Override
			public boolean filter(Element el)
			{
				Entity entity = (Entity) el;
				return ( entity.Package() != null 
							? !(entity.Package() instanceof NonGeneratedPackage)
							: false );
			}
		});
		return entities;
	}
	
	public ElementSet Subsystem() {
		ElementSet classes = MMUtil.collectDescendents(this);
		return MMUtil.filter(classes,new TypePredicate(Subsystem.class));
	}
	
	public ElementSet AlmaAbstractClass() {
		ElementSet classes = MMUtil.collectDescendents(this);
		return MMUtil.filter(classes,new TypePredicate(AlmaAbstractClass.class));
	}

	
}
