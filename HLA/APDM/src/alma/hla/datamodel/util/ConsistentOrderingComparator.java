/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2004
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.datamodel.util;

import java.util.Comparator;

import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

/**
 * Used to impose an arbitrary but consistent order on unordered UML elements.
 * This is needed to map UML constructs like associations to XML schema elements inside a &lt;xs:sequence&gt;.
 * These schema elements necessarily define an order, which should remain the same for different code generation runs.
 * Chosing a consistent order both eliminates ordering issues during the XMI creation, as well as during generator runs for the same XMI. 
 * <p>
 * Currently uses alphabetical ordering ignoring case, until someone has a better idea.
 *   
 * @author hsommer
 * created Oct 14, 2004 2:55:06 PM
 */
public class ConsistentOrderingComparator implements Comparator {

    public static ConsistentOrderingComparator s_instance = null; 
    
    private ConsistentOrderingComparator() {        
    }
    
    public static synchronized ConsistentOrderingComparator getInstance() {
        if (s_instance == null) {
            s_instance = new ConsistentOrderingComparator();
        }
        return s_instance;
    }
    
    public int compare(Object o1, Object o2) {
        
        if (o1 instanceof AssociationEnd) {
            o1 = ((AssociationEnd)o1).Class();
        }
        if (o2 instanceof AssociationEnd) {
            o2 = ((AssociationEnd)o2).Class();
        }
        String i1 = ((Element) o1).NameS();
        String i2 = ((Element) o2).NameS();
        if (i1 == null && i2 != null) {
            return -1;
        }
        if (i1 != null && i2 == null) {
            return 1;
        }
        if (i1 == null && i2 == null) {
            return 0;
        }
            
        String n1 = i1.toString();
        String n2 = i2.toString();
        
//        System.out.println("compared '" + n1 + "' with '" + n2);
    	
    	return ( n1.toLowerCase().compareTo(n2.toLowerCase()) );
    }

}
