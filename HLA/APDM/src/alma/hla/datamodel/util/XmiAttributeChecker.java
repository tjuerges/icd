/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2005
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */

package alma.hla.datamodel.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import alma.acs.config.validators.XmlNamespaceContextContainer;

/**
 * Tool for developers and UML workers, which analyzes multiplicities of UML attributes.
 * 
 * @author hsommer
 */
public class XmiAttributeChecker {

	private Logger logger;

	public XmiAttributeChecker(Logger logger) {
		this.logger = logger;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("usage: acsStartJava alma.hla.datamodel.util.XmiAttributeChecker xmiFile");
			System.exit(1);
		}
		File xmiFile = new File(args[0]);
		XmiAttributeChecker instance = new XmiAttributeChecker(Logger.getLogger("XmiAttributeChecker"));
		instance.run(xmiFile);
	}

	
	
	public boolean run(File xmiFile) {
		if (xmiFile == null || !xmiFile.exists()) {
			throw new IllegalArgumentException("Specified XMI file '" + xmiFile + "' not found.");
		}
		try {
			// parse the XML file into a DOM
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Element rootElement = builder.parse(xmiFile).getDocumentElement();
			
			// create an XPath engine
			XPath xpathEngine = XPathFactory.newInstance().newXPath();
			
			// xmi version and namespace settings
			// (we get the xmi version without xpath (xmi:XMI/@xmi:version), since namespace/prefix mapping is not known yet)
			String xmiVersion = rootElement.getAttribute("xmi:version");
			String umlNS = rootElement.getAttribute("xmlns:uml");
			String xmiNS = rootElement.getAttribute("xmlns:xmi");
			if (xmiVersion.length() == 0 || umlNS.length() == 0 || xmiNS.length() == 0) {
				throw new IllegalArgumentException("version and namespace info could not be extracted from " + xmiFile);
			}
			XmlNamespaceContextContainer nsContext = new XmlNamespaceContextContainer();
			nsContext.addNamespace("uml", umlNS);
			nsContext.addNamespace("xmi", xmiNS);
			xpathEngine.setNamespaceContext(nsContext);
			
			XmiExpert xmiExpert = XmiExpert.getInstance(xmiVersion);

			Element umlModelElement = (Element) xpathEngine.evaluate("uml:Model", rootElement, XPathConstants.NODE);

//			int numberAllAttr = ((Double) xpathEngine.evaluate(xpathStore.getXPathAttr(), umlModelElement, XPathConstants.NUMBER)).intValue();
			List<Attr> nodesxx = attrNodeListToAttrList((NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttr(), umlModelElement, XPathConstants.NODESET));
			int numberAllAttr = nodesxx.size();
			
//			int numberAttrUndefMultip = ((Double) xpathEngine.evaluate(xpathStore.getXPathAttrUndefined(), umlModelElement, XPathConstants.NUMBER)).intValue();
			List<Attr> nodesUndefMultip = attrNodeListToAttrList((NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttrUndefined(), umlModelElement, XPathConstants.NODESET));
			int numberAttrUndefMultip = nodesUndefMultip.size();			
			
			logger.info("total number of attributes=" + numberAllAttr + ", out of which " + numberAttrUndefMultip + " have undefined multiplicity");

			NodeList attrLower0 = (NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttrLower0(), umlModelElement, XPathConstants.NODESET);
			NodeList attrLower1 = (NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttrLower1(), umlModelElement, XPathConstants.NODESET);
			NodeList attrUpper1 = (NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttrUpper1(), umlModelElement, XPathConstants.NODESET);
			NodeList attrUpperN = (NodeList) xpathEngine.evaluate(xmiExpert.getXPathAttrUpperN(), umlModelElement, XPathConstants.NODESET);
			
			// now compute the various [x..y] categories
			
			List<Attr> nodes0x = attrNodeListToAttrList(attrLower0);
			List<Attr> nodes1x = attrNodeListToAttrList(attrLower1);
			List<Attr> nodesx1 = attrNodeListToAttrList(attrUpper1);
			List<Attr> nodesxN = attrNodeListToAttrList(attrUpperN);
			// [0..1]
			Set<Attr> attributes01 = new LinkedHashSet<Attr>(nodes0x);
			attributes01.retainAll(nodesx1);
			printAttributes(attributes01, "[0..1]", xmiExpert);
			// [1..1]
			Set<Attr> attributes11 = new LinkedHashSet<Attr>(nodes1x);
			attributes11.retainAll(nodesx1);
			printAttributes(attributes11, "[1..1]", xmiExpert);
			// [0..*]
			Set<Attr> attributes0N = new LinkedHashSet<Attr>(nodes0x);
			attributes0N.retainAll(nodesxN);
			printAttributes(attributes0N, "[0..*]", xmiExpert);
			// [1..*]
			Set<Attr> attributes1N = new LinkedHashSet<Attr>(nodes1x);
			attributes1N.retainAll(nodesxN);
			printAttributes(attributes1N, "[1..*]", xmiExpert);			
			// sanity check
			int numCategorizedUmlAttr = numberAttrUndefMultip + attributes01.size() + attributes11.size() + attributes0N.size() + attributes1N.size();
			if (numCategorizedUmlAttr != numberAllAttr) {
				String msg = "Sum of categorized attributes does not match total number of attributes!";
				if (numCategorizedUmlAttr < numberAllAttr) {
					nodesxx.removeAll(nodesUndefMultip);
					nodesxx.removeAll(attributes01);
					nodesxx.removeAll(attributes11);
					nodesxx.removeAll(attributes0N);
					nodesxx.removeAll(attributes1N);
					msg += " The following " + nodesxx.size() + " attribute(s) were not categorized: ";
					for (Iterator iter = nodesxx.iterator(); iter.hasNext();) {
						Attr umlAttrName = (Attr) iter.next();
						String className = xmiExpert.getUmlClass(umlAttrName).getAttribute("name");
						msg += className + "#" + umlAttrName.getValue() + ", ";
					}
				}
				throw new IllegalArgumentException(msg);
			}

			logger.info("done");
			return false;
			
		}
		catch (Exception e) {
			logger.log(Level.WARNING, "xml file '" + xmiFile + "' failed to be parsed.", e);
			return false;
		}
	}
	
	private List<Attr> attrNodeListToAttrList(NodeList attrNodeList) {
		int len = attrNodeList.getLength();
		List<Attr> list = new ArrayList<Attr>(len);
		for (int i = 0; i < len; i++) {
			Node attrNode = attrNodeList.item(i);
			if (attrNode instanceof Attr) {
				list.add((Attr) attrNode);
			}
			else {
				throw new IllegalArgumentException("Node '" + attrNode + "' is not an attribute node!");			
			}
		}
		return list;
	}
	
	private void printAttributes(Set<Attr> attrNodes, String multiplicityName, XmiExpert xmiExpert) throws XmiProcessingException {
		String msg = "Attributes " + multiplicityName + "(" + attrNodes.size() + "): ";
		for (Iterator<Attr> iter = attrNodes.iterator(); iter.hasNext();) {
			Attr attrNode = iter.next();
			Element umlClassElem = xmiExpert.getUmlClass(attrNode);
//			String className = xpath.evaluate(xpathStore.getXPathClassName(attrNode.getOwnerElement().getAttribute("xmi:id")), rootNode);
			String className = umlClassElem.getAttribute("name");
			msg += className + "#" + attrNode.getValue() + ", ";
		}
		System.out.println(msg);
	}
	
	
	
	
	public abstract static class XmiExpert {		
		public static XmiExpert getInstance(String xmiVersion) {
			if (xmiVersion.equals("2.1")) {
				return new XmiExpertVersion21();
			}
			else {
				throw new IllegalArgumentException("xmi version '" + xmiVersion + "' is not supported.");
			}
		}
		/** selects all DOM attributes that represent UML attribute names, regardless of UML attribute multiplicity */
		public abstract String getXPathAttr();
		/** selects DOM attributes that represent UML attribute names, for UML attributes with default multiplicity */
		public abstract String getXPathAttrUndefined();
		/** selects DOM attributes that represent UML attribute names, for UML attributes with multiplicity min=0 */
		public abstract String getXPathAttrLower0();
		/** selects DOM attributes that represent UML attribute names, for UML attributes with multiplicity min=1 */
		public abstract String getXPathAttrLower1();
		/** selects DOM attributes that represent UML attribute names, for UML attributes with multiplicity max=0 */
		public abstract String getXPathAttrUpper1();
		/** selects DOM attributes that represent UML attribute names, for UML attributes with multiplicity max=N */
		public abstract String getXPathAttrUpperN();
		/** walks up the DOM to the element that represents the UML class for a given DOM attribute which represents the UML attribute's name */
		public abstract Element getUmlClass(Attr umlAttrName) throws XmiProcessingException;
	}
	
	static class XmiExpertVersion21 extends XmiExpert {
		@Override
		public String getXPathAttr() {
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association)]/@name";
		}
		@Override
		public String getXPathAttrUndefined() {
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and not(upperValue)]/@name";
		}
		@Override
		public String getXPathAttrLower0() {
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and lowerValue[not(@value)]]/@name";
		}
		@Override
		public String getXPathAttrLower1() {
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and lowerValue[@value=\"1\"]]/@name";			
		}
		@Override
		public String getXPathAttrUpper1() {
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and upperValue[@value=\"1\"]]/@name";
		}
		@Override
		public String getXPathAttrUpperN() {
			// note that models started in MD11 from scratch have value="*" instead of "-1"
			return "//*/ownedMember/ownedAttribute[@name and not(@aggregation) and not(@association) and upperValue[@value=\"-1\" or @value=\"*\"]]/@name";
		}
		@Override
		public Element getUmlClass(Attr umlAttrName) throws XmiProcessingException {
			Element umlAttr = umlAttrName.getOwnerElement();
			if (!umlAttr.getTagName().equals("ownedAttribute")) {
				throw new XmiProcessingException("XMI 2.1 error: ownedAttribute expected as DOM parent of UML attribute's name.");
			}
			Node umlClassNode = umlAttr.getParentNode();
			if (umlClassNode instanceof Element && ((Element)umlClassNode).getTagName().equals("ownedMember")) {
				Element umlClass = (Element) umlClassNode;
				String className = umlClass.getAttribute("name");
				if (className.trim().length() == 0) {
					throw new XmiProcessingException("XMI 2.1 error: 'ownedMember' should have a 'name' attribute.");
				}
				return umlClass;
			}
			else {
				throw new XmiProcessingException("XMI 2.1 error: DOM element 'ownedMember' expected as DOM parent of UML attribute.");
			}
		}
	}
	
	public static class XmiProcessingException extends Exception {
		XmiProcessingException(String msg) {
			super(msg);
		}
		XmiProcessingException(String msg, Throwable cause) {
			super(msg, cause);
		}
	}
}
