/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.datamodel.pred;

import org.openarchitectureware.core.meta.core.Element;

/**
 * @author jschwarz
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public abstract class Predicate
{
	public abstract boolean filter( Element el );
	
	public Element mapElement(Element el) {
		return el;
	}

}
