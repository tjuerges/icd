/*
 * Created on Oct 10, 2007 by mschilli
 */
package alma.hla.runtime.obsprep.util;

/**
 * Indicates that an Entity-Id couldn't be created or assigned.
 * 
 * This is the counterpart of {@link UnknownEntityException}. It can
 * happen in the factory methods for Entity BOs and for EntityPart BOs.
 * <p>
 * This is an unchecked exception. Under all normal circumstances,
 * the OT will take care that it doesn't occur: If the Archive is
 * unavailable and no real Entity-Ids can be issued, the OT is
 * supposed to make up artifical (fake) Ids. Only if that fails,
 * this exception will be raised.
 *  
 * @author mschilli
 */
public class CannotAssignIdException extends RuntimeException {

	public CannotAssignIdException () {}

	public CannotAssignIdException (String message) {
		super (message);
	}

	public CannotAssignIdException (Throwable cause) {
		super (cause);
	}

	public CannotAssignIdException (String message, Throwable cause) {
		super (message, cause);
	}

}
