/*
 * Created on May 26, 2009 by mschilli
 */
package alma.hla.runtime.obsprep.util;

import java.io.IOException;
import java.io.StringWriter;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.core.exceptions.CastorException;



/**
 * While starting to use ScienceGoals under ObsProposals in May 2009 we ran into a Castor
 * issue where it would create an automatic namespace prefix "ns1" for a ScienceGoal but
 * would omit the declaration of that namespace, which produced invalid XML. We found that
 * defining explicit namespace mappings fixes the problem and produces nicer XML anyways:
 * auto-created namespaces are embeded with every element that uses them, while explicitly
 * defined ones are declared as a nice list after the root element. This class provides a
 * Castor Marshaller that is set up with explicit namespaces already. It is supposed to be
 * used by all code that converts Castor objects to XML to make sure the produced XML
 * looks the same in all places.
 * <p>
 * This class is not intended to be thread-safe.
 *
 * @author M.Schilling, ESO
 */
public class XmlMarshaller {

	protected StringWriter w;
	protected Marshaller m;

	/**
	 * Create a new marshaller.
	 * 
	 * @throws XmlConversionException
	 */
	public XmlMarshaller() throws XmlConversionException {
		try {
			w = new StringWriter();
			m = new Marshaller(w);
			configure(m);

			/* should not happen, we're using a StringWriter */
		} catch (IOException exc) {
			throw new XmlConversionException(exc.getMessage(), exc);
		}
	}

	/** 
	 * To be overridden by subclasses if necessary.
	 */
	protected void configure (Marshaller m) {
		m.setNamespaceMapping("ent", "Alma/CommonEntity");
		m.setNamespaceMapping("val", "Alma/ValueTypes");
		m.setNamespaceMapping("prp", "Alma/ObsPrep/ObsProposal");
		m.setNamespaceMapping("orv", "Alma/ObsPrep/ObsReview");
		m.setNamespaceMapping("ps", "Alma/ObsPrep/ProjectStatus");
		m.setNamespaceMapping("oat", "Alma/ObsPrep/ObsAttachment");
		m.setNamespaceMapping("prj", "Alma/ObsPrep/ObsProject");
		m.setNamespaceMapping("sbl", "Alma/ObsPrep/SchedBlock");
	}

	/**
	 * Access to marshaller if further configuration necessary.
	 */
	public Marshaller getMarshaller() {
		return m;
	}

	/**
	 * Convenience method to run conversion in one call.
	 * 
	 * @param object - the castor object to marshal
	 * @return the stringified xml
    * @throws XmlConversionException if the conversion caused any problems
	 */
	public String marshalToString (Object object) throws XmlConversionException {
		try {
			w.getBuffer().setLength(0);
			m.marshal(object);
			return w.toString();

		} catch (CastorException exc) {
			throw new XmlConversionException(exc.getMessage(), exc);
		}
	}
	
}
