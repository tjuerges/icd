package alma.hla.runtime.obsprep.util;

import alma.entities.commonentity.EntityRefT;

/**
 * This exception gets thrown when a reference to an entitypart cannot be resolved. In
 * that respect it is analogous to the UnknownEntity exception thrown for entities.
 * Opposed to UnknownEntity this is an unchecked exception however, because accesses to
 * EntityParts are many, and forcing each caller to handle them would have massive impact
 * on the OT code.
 *
 * @author M.Schilling, ESO
 * @since August 2009
 */
public class UnknownEntityPartException extends RuntimeException {

	public UnknownEntityPartException (String message) {
		super(message);
	}

}