package alma.hla.runtime.obsprep.util;

import java.util.logging.Level;


/**
 * Logging API for the DataModel classes.
 * 
 * This is an abstract implementation used at compile-time. At run-time, the real
 * implementation must be set through a call to <code>setRuntimeImplementation()</code>.
 */
public abstract class Log {


	public static interface LoggerProvider {

		/**
		 * Provide a logger instance for a given class or object
		 */
		public Logger logger (Class<?> forWhom);
	}


	public static interface Logger {

		// allow clients to optimize
		// (not reasonable for "warning" and "severe")

		/** Invoke to test if building a costly log message is worthwile */
		public boolean trace ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean debug ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean finest ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean finer ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean fine ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean config ();

		/** Invoke to test if building a costly log message is worthwile */
		public boolean info ();
		
		/** Invoke to test if building a costly log message is worthwile */
		public boolean isLoggable (Level level);

		// msc 2007-11: when it comes to actual logging methods, the only
		// thing we can do is re-define java.util.logging.Logger's API,
		// otherwise caller inference is broken (thank you, Acs).

		/** Log an exception on an arbitrary level */
		public void log (Level level, String msg, Throwable thrown);

		/** Log a message on an arbitrary level (needed for TRACE and DEBUG) */
		public void log (Level level, String msg);

		/** Log a message on the indicated level */
		public void finest (String msg);

		/** Log a message on the indicated level */
		public void finer (String msg);

		/** Log a message on the indicated level */
		public void fine (String msg);

		/** Log a message on the indicated level */
		public void config (String msg);

		/** Log a message on the indicated level */
		public void info (String msg);

		/** Log a message on the indicated level */
		public void warning (String msg);

		/** Log a message on the indicated level */
		public void severe (String msg);

		// Also, I'm redefining here the bits and pieces needed to
		// re-create the functionality that we had before in our custom
		// log methods like trace(msg, params), fine(format, args), etc.

		/** Same as String.format(String, Object...) */
		public String sformat (String format, Object... args);

		/** Verbose formatting used for trace and debug */
		public String vformat (String message, Object... args);

	}

	// Redefining the bits and pieces needed to re-create our custom
	// log methods like trace(msg, params), fine(format, args), etc.

	/** Obsprep-custom log level for trace messages */
	public static final Level TRACE = new Level("TRACE", 100) {/* subclass */};

	/** Obsprep-custom log level for debug messages */
	public static final Level DEBUG = new Level("DEBUG", 200) {/* subclass */};



	// =================================================================
	// Forwarding to real (run-time) implementation
	// =================================================================


	private static LoggerProvider runtimeImpl;

	/**
	 * Set the LoggerProvider implementation that does the real work at runtime.
	 */
	public static void setRuntimeImplementation (LoggerProvider realImpl) {
		runtimeImpl = realImpl;
	}

	/**
	 * Returns a logger for the specified client wanting to log something.
	 * <p>
	 * This variant is needed since some pieces of the OT are implemented
	 * <code>static</code>, thus there's no <code>this</code> to pass in.
	 */
	public static Logger logger (Class<?> forWhom) {
		if (runtimeImpl == null) {
			throw new RuntimeException("need to be initialized via setRuntimeImplementation(), see class doc for details");
		}
		return runtimeImpl.logger(forWhom);
	}

	/**
	 * Returns a logger for the specified client wanting to log something.
	 */
	public static Logger logger (Object forWhom) {
		if (runtimeImpl == null) {
			throw new RuntimeException("need to be initialized via setRuntimeImplementation(), see class doc for details");
		}
		return runtimeImpl.logger(forWhom.getClass());
	}


}
