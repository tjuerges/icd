package alma.hla.runtime.obsprep.util;

import java.util.TreeMap;
import java.util.HashMap;



/**
 * An attempt to encapsulate utility information for ValueWithUnit type classes.
 * 
 * This is an abstract implementation used at compile-time.
 * At run-time, the real implementations must be set through a call
 * to <code>addUnits()</code>.
 * 
 * The original unit definitions contained in this class have gone to
 * class UnitMapConfigurator in the OBSPREP module.
 * 
 * @author from an original version by dac, Jul 3, 2003
 */


public class UnitMap extends TreeMap {

	// ====  real (run-time) implementations  ====
	
	static private HashMap runtimeImpls = new HashMap();
	
	static public void addUnits(String type, UnitMap x) {
		runtimeImpls.put(type, x);
	}
	
	static public UnitMap getUnits(String type) {
		UnitMap ret = (UnitMap)runtimeImpls.get(type);
		
		if (ret == null) {
			throw new RuntimeException("need to be initialized via addUnits(), see class doc for details");
		}
		
		return ret;
	}
	
	
	
	
    /////////////////////////////////////////////////////////
    // Implementation of near-singleton paradigm
    //
    // By "near singleton", I mean that there are a limited number of
    // instances allowed - one for each type of thing we have units for
    // (e.g. one UnitMap for angles, one for rotation speeds etc.). For
    // want of a better word, each such type of thing is termed an arena.
    // Each arena is implemented by having a private static UnitMap for it.
    /////////////////////////////////////////////////////////
    
    /**
     * Use private constructors,clients should use the static getXXXUnits() methods. 
     */
    public UnitMap() {
        super();
    }
    
   
    
 	// ===============================================================================	
 	// ===============================================================================
 	// ===============================================================================

    
    
    
    // 2004-12-14 msc
    // ======== The following API is for backward-compatibility ======
    
    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of Angle, the keys are their relative scale (eg. Arcsec = 1, Degree = 60) 
     */
    public static UnitMap getAngleUnits() {
    	return getUnits("Angle");
    }

    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of Rotation, the keys are their relative scale 
     */
    public static UnitMap getRotationUnits() {
    	return getUnits("Rotation");
    }

    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of DataRate, the keys are their relative scale 
     */
    public static UnitMap getDataRateUnits() {
    	return getUnits("DataRate");
    }

    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of StorageVolume, the keys are their relative scale 
     */
    public static UnitMap getStorageVolumeUnits() {
    	return getUnits("StorageVolume");
    }

    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of Frequency, the keys are their relative scale. 
     */
    public static UnitMap getFrequencyUnits() {
    	return getUnits("Frequency");
    }

    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of Sensitivity, the keys are their relative scale 
     */
    public static UnitMap getSensitivityUnits() {
    	return getUnits("Sensitivity");
    }

	/**
	  * @return SortedMap of String -> Double. The keys in the Map are the supported
	  * units of Temperature, the keys are their relative scale. Sadly there is no
	  * mechanism to handle offsets (eg K = C - 273.16) 
	  */
	public static UnitMap getTemperatureUnits() {
		return getUnits("Temperature");
	}

	/**
	  * @return SortedMap of String -> Double. The keys in the Map are the supported
	  * units of Time, the keys are their relative scale (eg. Second = 1, Hour = 3600) 
	  */
	public static UnitMap getTimeUnits() {
		return getUnits("Time");
	}
	
    /**
     * @return SortedMap of String -> Double. The keys in the Map are the supported
     * units of TimeInterval, the keys are their relative scale (eg. Second = 1, Hour = 3600) 
     */
   public static UnitMap getTimeIntervalUnits() {
       return getUnits("TimeInterval");
   }
   
	/**
	  * @return SortedMap of String -> Double. The keys in the Map are the supported
	  * units of Speed, the keys are their relative scale (eg. m/s = 1, km/s = 1000) 
	  */
	public static UnitMap getSpeedUnits() {
		return getUnits("Speed");
	}

	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getUnknownUnits() {
		return getUnits("Unknown");
	}

	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getSmallAngleUnits() {
		return getUnits("SmallAngle");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
    public static UnitMap getLongitudeUnits() {
    	return getUnits("Longitude");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getLatitudeUnits() {
		return getUnits("Latitude");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getLengthUnits() {
		return getUnits("Length");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getIntTimeSourceUnits() {
		return getUnits("IntTimeSource");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getIntTimeReferenceUnits() {
		return getUnits("IntTimeReference");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getFluxUnits() {
		return getUnits("Flux");
	}
	
	
	/**
	  * @return an empty SortedMap of String -> Double.  
	  */
	public static UnitMap getAngularVelocityUnits() {
		return getUnits("AngularVelocity");
	}

	
	
	// ===============================================================================	
	// ===============================================================================
	// ===============================================================================

	
	
    /////////////////////////////////////////////////////////
    // Non-static part
    /////////////////////////////////////////////////////////
    
    private String m_defaultUnit;  // The default unit to use in this arena
    
    /**
     * @return String - the name of the default unit in this arena
     */
    public String getDefaultUnit() {
        return m_defaultUnit;
    }

    /**
     * Set the name of the default unit in this arena
     * @param string
     */
    public void setDefaultUnit(String newDefault) {
        m_defaultUnit = newDefault;
    }

    /**
     * Get the scale for a unit in the arena
     * @param  unit           - the unit to look up
     * @return double         - the scale for the unit
     * @throws UnitException - if the unit is not recognised.
     */
    public double getScale(String unit)
        throws UnitException {
        Object retrieve = get(unit);
        if (retrieve == null) {
            throw new UnitException("Unknown unit - '" + unit + "'");
        }    
        return ((Double)retrieve).doubleValue();
    }

    /**
     * Get the scale for the default unit in the arena
     * @param  unit           - the unit to look up
     * @return double         - the scale for the unit
     * @throws UnitException - if the unit is not recognised.
     */
    public double getDefaultScale()
        throws UnitException {
        return getScale(getDefaultUnit());
    }

    /**
     * Get a factor to multiply values in one unit by to get the equivalent
     * value in another unit. 
     * @param  from           - the unit you want to convert from
     * @param  to             - the unit you want to convert to
     * @return double         - the conversion factor
     * @throws UnitException - if either unit is not recognised.
     */
    public double getConversionFactor(String from, String to)
        throws UnitException {
        double f = getScale(from);
        double t = getScale(to);
        return f/t;
    }
    
	/**
	 * Add a unit and its scale to the UnitMap
	 * @param  name           - the name of the unit
	 * @param  scale          - its relative scale.
	 */
	public Object put(String name, double scale) {
		return super.put(name, new Double(scale));
	}
	
	
	/**
	 * Return the list of units in this arena, sorted in terms of
	 * increasing unit size (e.g seconds, minutes, hours) 
	 * @return Object[] - the list of keys
	 */
	public Object[] getUnitsSorted() {
		Object[]  a;
		int       size;
		
		a = keySet().toArray();
		size = a.length;

		for (int i = 0; i < size-1; i++) {
			for (int j = i+1; j < size; j++) {
				Double di = (Double) get(a[i]);
				Double dj = (Double) get(a[j]);
				if (di.doubleValue() > dj.doubleValue()) {
					Object temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		
		return a;
	}
	

	
	// ===============================================================================	
	// ===============================================================================
	// ===============================================================================
	
	
	
	public static void main(String[] args) {
		UnitMap um;
		Object[] o;
		um = getAngleUnits();
		System.out.println("Angle:");
		System.out.println(um.keySet());
		o = um.getUnitsSorted();
		for (int i = 0; i < o.length; i++) System.out.println(o[i]);
		um = getFrequencyUnits();
		System.out.println("Frequency:");
		System.out.println(um.keySet());
		o = um.getUnitsSorted();
		for (int i = 0; i < o.length; i++) System.out.println(o[i]);
		um = getRotationUnits();
		System.out.println("Rotation:");
		System.out.println(um.keySet());
		o = um.getUnitsSorted();
		for (int i = 0; i < o.length; i++) System.out.println(o[i]);
		um = getSensitivityUnits();
		System.out.println("Sensitivity:");
		System.out.println(um.keySet());
		o = um.getUnitsSorted();
		for (int i = 0; i < o.length; i++) System.out.println(o[i]);
		um = getTimeUnits();
		System.out.println("Time:");
		System.out.println(um.keySet());
		o = um.getUnitsSorted();
		for (int i = 0; i < o.length; i++) System.out.println(o[i]);
	}

}

