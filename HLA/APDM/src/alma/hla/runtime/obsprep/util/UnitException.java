package alma.hla.runtime.obsprep.util;

/**
 * @author dac, created 03-Jul-03
 * @version $Revision$
 */

// $Id$

public class UnitException extends Exception
{

	/**
	 * Constructor for UnitException.
	 */
	public UnitException() {
		super();
	}

	/**
	 * Constructor for UnitException.
	 * @param message
	 */
	public UnitException(String message) {
		super(message);
	}

	/**
	 * Constructor for UnitException.
	 * @param message
	 * @param cause
	 */
	public UnitException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for UnitException.
	 * @param cause
	 */
	public UnitException(Throwable cause) {
		super(cause);
	}
}
