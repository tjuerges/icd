/*
 * Created on Feb 12, 2009 by mschilli
 */
package alma.hla.runtime.obsprep.util;



/**
 * Thrown by <code> BusinessObject.toXml() </code>
 */
public class XmlConversionException extends RuntimeException {
	/* 
	 * BusinessObject.toXml() used to throw PersistenceException
	 * but that has moved to the OBSPREP module and is no longer
	 * available in HLA/APDM. Moreover, since xml conversion
	 * in reality never fails (certainly I've never seen it), it
	 * seems more than enough to have that method throw an
	 * unchecked exception.
	 */
	public XmlConversionException (String message, Throwable cause) {
		super (message, cause);
	}
	
}


