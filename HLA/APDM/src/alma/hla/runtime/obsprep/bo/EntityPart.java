package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityRefT;

/**
 * Classes tagged with the UML-stereotype "identifiablepart"
 * will implement this interface.
 *
 * @since Aug 2007
 * @author mschilli
 */
public interface EntityPart extends IBusinessObject {
	
   public String getEntityPartId();
		
   public void setEntityPartId (String partId);

   public EntityRefT getEntityPartReference();
   
}

