package alma.hla.runtime.obsprep.bo;

import java.util.Vector;

import alma.entity.xmlbinding.valuetypes.DoubleWithUnitT;
import alma.hla.runtime.obsprep.util.UnitException;
import alma.hla.runtime.obsprep.util.UnitMap;

/**
 * A basic implementation of ValueUnitPair for use by those entity classes
 * which extend DoubleWithUnitT. 
 * <BR>
 * Implements also the CopyAndPaste interface (inherited through
 * Business Object) and a simple toString() method.
 * 
 * @author dac
 */

public abstract class AbstractDoubleWithUnit<T extends AbstractDoubleWithUnit> extends BusinessObject implements ValueUnitPair<T> {

    public static final double COMPARISON_TOLERANCE = 1.0e-5;
	
	 /* TODO: revisit the usefulness and certainly the dummy implementation of these methods...
	 * Was hastily added on 2004-08-30 to remove compile errors from OBSPREP/ObservingTool using the APDMEntities.jar.
	 * The getName method was added to old version of this file by Maurizio on July 9 (after the file was copied for the code generation effort)
	 */
	 
	/* (non-Javadoc)
	 * @see alma.obsprep.bo.ValueUnitPair#defaultUnit()
	 */
	public String defaultUnit() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see alma.obsprep.bo.ValueUnitPair#unitMap()
	 */
	public UnitMap unitMap() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * @return Always null
	 * @see alma.obsprep.bo.BusinessObject#getName()
	 */
	@Override
	public String getName() {
		return null;
	}
	
	/**
	 * Does nothing.
	 * @see alma.obsprep.bo.BusinessObject#setName(java.lang.String)
	 */
	public void setName(String name) {
	}
	
	/* (non-Javadoc)
	 * @see alma.obsprep.bo.Delegator#getDelegate()
	 */
	public Object getDelegate() {
		// TODO Auto-generated method stub
		return null;
	}
//    /** Delegate Entity object */
//	protected DoubleWithUnitT m_delegate;
	
	abstract public DoubleWithUnitT getCastorObject();
	
	protected static Vector units = null;

	/**
    * Constructor that connects this BO to a binding class.
    * this constructor is protected since it should not be used directly; use the
    * corresponding factory method instead.
    * @author hand-coded along the lines of JavaDependentClass
	 */
	protected AbstractDoubleWithUnit(DoubleWithUnitT castorObject) {
		super(castorObject);
//		this.m_delegate = castorObject;
		
		setUnitToDefault();
	}
	
    
	/**
	 * Try to ensure that the unit field is not null:
	 * 	if there's already a value then great, leave it be
	 *  if there is not, check for a defaultUnit().
	 * 		if there is one defined then set the unit to that.
	 * 		if there is not set the unit to the first of unitsEnumeration()
	 * 		if there is not, then leave well alone.
	 * @param delegate
	 */
	public void setUnitToDefault() {
		if (getUnit() == null) {
			setUnit(defaultUnit());
		}
	}

    /** Temporary implementation: throws an exception. */
    public double getContentInDefaultUnits() {
        return getContentInUnits(this.defaultUnit());
    }
    	
    /** Temporary implementation: throws an exception. */
    public double getContentInUnits(String newUnit) {
		double result = getContent();
		String   unit = getUnit();
		try {
			UnitMap arena  = unitMap();
            double factor = arena
                    .getConversionFactor(unit, newUnit);
			result *= factor;
		}
        catch( UnitException ue ) {
            throw new RuntimeException( this.getClass()
                    + ": no conversion factor from current unit '" + unit
                    + "' to '" + newUnit + "'" );
        }
		return result;
    }
    	
	public void convertToDefaultUnit() {
		double newContent = getContentInDefaultUnits();
		setUnit(null);		// Clear the old unit out
		setUnitToDefault();
		setContent(newContent);
	}


	public void convertToUnit(String newUnit) {
		double newContent = getContentInUnits(newUnit);

		setUnit(newUnit);
		setContent(newContent);
	}


	public T inDefaultUnits() {
		T vup = (T) new Copier().copy(this);
		vup.convertToDefaultUnit();
		return vup;
	}

// msc 2010-12: gets now generated by code generator
//	@Override
//	public T deepCopy() {
//		return (T)super.deepCopy();
//	}

    /**
     *  Returns "friendly" unit, i.e. the smallest unit in which the contents
     *  is greater than 1.0;
     * 
     * @return
     */
    public String getFriendlyUnit() {
    	if (isZero())
    		return getUnit(); // current
    	
    	Object[] units = unitMap().getUnitsSorted();
    	String v = null;
    	for (int i = units.length - 1; i >= 0; i--) {
    		v = (String)units[i];
    		if (getContentInUnits(v) > 1.0) {
    			return v; 
    		}
    	}

    	return v;  // returns last item (= smallest unit) 
 
    }
    
	// msc (2007-05): Fixes for mismatch between handcoded DoubleWithUnit
	// schema ("content/unit") and UML ("value/unit"). This allows at least
	// to generate the initFrom() method which relies on the UML names.
	// --------------------------------------------------------------------
	protected boolean hasValue() {
		return hasContent();
	}
	protected double getValue() {
		return getContent();
	}
	protected void setValue(double value) {
		setContent(value);
	}
	
	
	

	/**
	 * Whether <code>getContent()</code> delivers a java-default value
	 * or a value set by somebody can only be determined by querying
	 * this method (msc, 2007-03-30).
	 * @return whether content is just a java default value, or a real value.
	 */
	public boolean hasContent() {
		return getCastorObject().hasContent();
	}
	
	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#getContent()
	 */
	public double getContent() {
		return getCastorObject().getContent();
	}

	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#getUnit()
	 */
	public String getUnit() {
		return getCastorObject().getUnit();
	}

	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setContent(double)
	 */
	public void setContent(double content) {
		getCastorObject().setContent(content);
	    // amchavan, 14-Apr-2005
	    final String property = 
		"alma.hla.runtime.obsprep.bo.AbstractDoubleWithUnit.content";
	    firePropertyChangeEvent( this, property, new Double( content ));
	}

	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setUnit(String)
	 */
	public void setUnit(String unit) {
		getCastorObject().setUnit(unit);
	    // amchavan, 14-Apr-2005
	    final String property = 
		"alma.hla.runtime.obsprep.bo.AbstractDoubleWithUnit.unit";
	    firePropertyChangeEvent( this, property, unit );
	}
	
	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setContent(double)
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setUnit(String)
	 */
	public void setContentAndUnit(double content, String unit) {
		setContent(content);
		setUnit(unit);
	}

	/**
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setContent(double)
	 * @see alma.entity.xmlbinding.obsproject.DoubleWithUnitT#setUnit(String)
	 */
	public void setContentAndUnit(ValueUnitPair vup) {
		setContent(vup.getContent());
		setUnit(vup.getUnit());
	}

    /** Basic implementation.
     * @return value + unit
     */
	@Override
	public String toString() {
		return toBasicString();
	}
	
    /** Basic implementation.
     * @return value + unit
     */
	public String toBasicString() {
		return getContent() + " " + getUnit();
	}
	
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#compareTo(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
    public int compareTo(T that) {
    	final Double mine = this.getContent();
    	final Double his  = that.getContentInUnits(this.getUnit());
    	return mine.compareTo(his);
    }

    
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isGreaterThan(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
    public boolean isGreaterThan(T arg) {
        final double fInMyUnits = arg.getContentInUnits(this.getUnit());
        return this.getContent() > fInMyUnits;
    }
    
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isGreaterThanOrEqualTo(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
    public boolean isGreaterThanOrEqualTo(T arg) {
        final double fInMyUnits = arg.getContentInUnits(this.getUnit());
        return this.getContent() >= fInMyUnits;
    }
    
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isLessThan(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
    public boolean isLessThan(T arg) {
        final double fInMyUnits = arg.getContentInUnits(this.getUnit());
        return this.getContent() < fInMyUnits;
    }
    
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isLessThanOrEqualTo(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
    public boolean isLessThanOrEqualTo(T arg) {
        final double fInMyUnits = arg.getContentInUnits(this.getUnit());
        return this.getContent() <= fInMyUnits;
    }
    
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isZero()
     */
	public boolean isZero() {
		return isZero(COMPARISON_TOLERANCE);
	}
	
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isZero(double)
     */
	public boolean isZero(double torelance) {
		final double d = Math.abs(getContent());
		return d < torelance;
	}
	
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isEqual(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
	public boolean isEqual(T arg) {
		return isEqual(arg, COMPARISON_TOLERANCE);
	}
	
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#isEqual(alma.hla.runtime.obsprep.bo.ValueUnitPair, double)
     */
	public boolean isEqual(T arg, double torelance) {
		final double d = Math.abs(getContent() - arg.getContentInUnits(this.getUnit()));
		return d < torelance;
	}

    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#plus(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
	public T plus(T arg) {
		T ret = (T)this.deepCopy();
		ret.setContent(this.getContent() + arg.getContentInUnits(this.getUnit()));
		return ret;
	}
	
    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#minus(alma.hla.runtime.obsprep.bo.ValueUnitPair)
     */
	public T minus(T arg) {
		T ret = (T)this.deepCopy();
		ret.setContent(this.getContent() - arg.getContentInUnits(this.getUnit()));
		return ret;
	}

    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#multiply(double)
     */
	public T multiply(double factor) {
		T ret = (T)this.deepCopy();
		ret.setContent(this.getContent() * factor);
		return ret;
	}

    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#divide(double)
     */
	public T divide(double factor) {
		T ret = (T)this.deepCopy();
		ret.setContent(this.getContent() / factor);
		return ret;
	}

    /* (non-Javadoc)
     * @see alma.hla.runtime.obsprep.bo.ValueUnitPair#half()
     */
	public T half() {
		return divide(2.0);
	}
}
