package alma.hla.runtime.obsprep.bo;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import alma.hla.runtime.obsprep.interfaces.ModelStructureNotifier;
import alma.hla.runtime.obsprep.interfaces.PropertyChangeSystemFlags;
import alma.hla.runtime.obsprep.util.XmlConversionException;
import alma.hla.runtime.obsprep.util.XmlMarshaller;
import alma.obsprep.bo.obsproject.ObsProject;

/**
 * Superclass of all Business Objects of the Observation Preparation
 * subsystem. <BR>
 * It includes support for:
 * <UL>
 * <LI> Conversion to XML (import/export)
 * <LI> Delegator interface (abstract definitions)
 * <LI> Object naming
 * <LI> Default CopyAndPaste action.
 * </UL>
 * 
 * @author amchavan, Feb 7, 2003
 */


public abstract class BusinessObject implements IBusinessObject {

	protected BusinessObject (Object castorObject) {
		this.storageObject = castorObject;
	}

	 protected Object storageObject;
	
    
    
    //--------------------------------------------------------
    // Naming
    //--------------------------------------------------------
    

    /**
     * @return Always null. Subclasses should implement something more
     *         sensible than that.
     */
    public String getName() {
        return null;
    }
    
    /**
     * @return The final part of the class name of this BO.
     */
    public String getShortClassName() {
        String result = this.getClass().getName();
        int l = result.lastIndexOf('.');
        result = result.substring(l+1);
        return result;
    }

    /**
     * @return This BO's name, unless it doesn't have one; in that
     *         case we return a string constructed from the object's
     *         class name.
     */
    public String getNameOrDefault() {
        // Look up this object's *real* name, using the subclass'
        // getName() method -- not our default implementation.
        String name = this.getName(); 
        
        // If we found no name, make up one from the classname.
        if( name == null || name.length() == 0 || name.trim().length() == 0 ) {
            name = getShortClassName();
            if (name.startsWith("Obs") && name.length() > 3) {
                name = name.substring(3);
            }
        } 

        return name;
    }

    /**
     * @return True if this object is or contains a spatial type of thing.
     */
    public boolean isSpatial() {
        return true; // Default assumption is that this is a spatial thing.
    }

    /**
     * @return True if this object is or contains a spectral type of thing.
     */
    public boolean isSpectral() {
        return !isSpatial();
    }

    /**
     * @return The spatial thing associated with this BO
     */
    public BusinessObject getSpatial() {
	if (isSpatial()) {
	    return this;
	} else {
	    return null;
	}
    }

    /**
     * @return The spectral thing associated with this BO
     */
    public BusinessObject getSpectral() {
	if (isSpectral()) {
	    return this;
	} else {
	    return null;
	}
    }

    
    /**
     * @return A String representing this Business Object.
     */
    @Override
	public String toString() {
        StringBuilder b = new StringBuilder();

        String cn = getClass().getName();		
        b.append(cn.substring( cn.lastIndexOf('.', cn.lastIndexOf('.')-1) +1));
        b.append("[");
        b.append("name=").append(getName());

        if (this instanceof Entity)
      	 b.append(", eid=").append(((Entity)this).getEntityID());
        if (this instanceof EntityPart)
       	  b.append(", epid=").append(((EntityPart)this).getEntityPartId());

        b.append(", hash=").append(hashCode());
        b.append("]");
        return b.toString();
    }
    
    //--------------------------------------------------------
    // Conversion to XML
    //--------------------------------------------------------
    
    /**
     * Convert the Business Object to XML.
     * 
     * @return An XML representation of the Business Object
     * @throws XmlConversionException if the conversion caused any problems.
     */
    public String toXml() throws XmlConversionException {
   	  XmlMarshaller m = new XmlMarshaller();
//      m.getMarshaller().setSuppressXSIType(true);
   	  String xml = m.marshalToString(getCastorObject());
          return xml;
    }
    

    /**
     * @return A String representing this Business Object.
     *         This method is used by the tree browsers, and
     *         allows objects to present different aspects of
     *         themselves depending on the phase the tree is
     *         displaying, and on whether it is a user view
     *         or an ObsUnitSet view.
     */
    public String toTreeString(int phase, boolean userView, boolean expanded) {
        return getNameOrDefault();
    }

    
   /**
    * This is an implementation hook to tweak a BO after it has been freshly instantiated.
    * It gets generated for every Entity and every EntityPart to assign them an Id.
    * <p>
    * When overriding this, always make sure to call {@code super.initAsNew()} !
    */
    protected void initAsNew() {
    }
    
    /**
     * This method initialises this BO following the role model of the specified BO, effectively
     * making this a clone of the {@code original} BO. It gets generated for every BO. To learn
     * more about the details of the copying process, see {@link alma.hla.runtime.obsprep.bo.Copier}.
     * <p>
     * In case you need to override this, always make sure to call {@code super.initAttribsAndPartsFrom(...)} !
     */
    protected void initAttribsAndPartsFrom (IBusinessObject original, Copier copier){
   	 /* This method is defined here so any generated BO can invoke super.initAttribsAndPartsFrom().
   	  * Additional reason: needs to be accessible by Copier */
    }

    /**
     * This method initialises this BO following the role model of the specified BO, effectively
     * making this a clone of the {@code original} BO. It gets generated for every BO. To learn
     * more about the details of the copying process, see {@link alma.hla.runtime.obsprep.bo.Copier}.
     * <p>
     * In case you need to override this, always make sure to call {@code super.initEntityReferencesFrom(...)} !
     */
    protected void initEntityReferencesFrom (IBusinessObject original, Copier copier){
   	 /* This method is defined here so any generated BO can invoke super.initEntityReferencesFrom().
   	  * Additional reason: needs to be accessible by Copier */
    }

    /**
     * This method gets generated for every BO. It is a helper method for the Copier.
     */
    protected BusinessObject invokeCreateUnitialized () throws CopyException {
   	 /* This method is defined here so Copier can access it as package-local */
		throw new CopyException("no invokeCreateUnitialized() implemention available for this object: "+this);
    }

//    /**
//     * This Magic Getter gets generated for every Entity. It is a helper method for the Copier.
//     */
//    protected EntityT mgetCastorEntity() {
//   	 /* This method is defined here so
//   	  * Copier can access it as package-local */
//   	 throw new UnsupportedOperationException("no mgetCastorEntity() implementation available for this object: "+this);
//    }
    
    /**
     * This is a convenience method allowing clients to copy this BO: it merely forwards
     * to {@link Copier} to perform the copying.
     *
     * @return a copy of <code>this</code> as produced by {@link Copier}  
     * @throws CopyException as raised by {@link Copier} 
     */
    public IBusinessObject deepCopy () throws CopyException {
   	 Copier copier = new Copier();
   	 return copier.copy(this);
    }
    

    
    
	 // ========== Treewalking Support =============


    // Referrer feature
    // -------------------------------------------------
    // msc (2006-08): support for listening to structural changes within the data model
 	 // msc (2007-04): reimplemented using new referrer-feature

    public Referring referrers = new Referring();
    
    public void addedTo (IBusinessObject bo, Referring.By by, int index) {
   	 if (bo == null)
   		 throw new NullPointerException("added to null? by="+by+", at "+index);

   	 referrers.add (bo, by);

   	 // find the project, starting at the parent.  
    	 BusinessObject ancestor = ((BusinessObject)bo).findProject();
    	 if (ancestor != null && ancestor instanceof ModelStructureNotifier)
    		 ((ModelStructureNotifier)ancestor).fireBusinessObjectAddedTo (this, bo, by, index);
    }

    public void removedFrom (IBusinessObject bo, Referring.By by) {
   	 if (bo == null)
   		 throw new NullPointerException("removed from null? by="+by);

   	 referrers.remove (bo, by);

   	 // find the project, starting at the parent.  
   	 BusinessObject ancestor = ((BusinessObject)bo).findProject();
   	 // there is one case where finding the project starting at the parent doesn't work:
   	 // when the parent is actually the one to be removed, while the child remains a member
   	 // of the project. this is the case when an object backlinks into a project: then the
   	 // object is the parent (in terms of the backlink), while the backlink-target is the child.
   	 // it would be necessary to clear the backlink before removing the object. if it's done
   	 // the other way round, the object has already lost connection to the project and thereby
   	 // to the project listeners. so there will be no event about the cleared backlink. however,
   	 // this is a subtlety that one will forget easily, so i'm changing this method so
   	 // it can deal with the wrong order: if finding the project starting from parent
   	 // doesn't succeed, we try again this time starting from the child.
   	 if (ancestor == null) ancestor = this.findProject();
   	 if (ancestor != null && ancestor instanceof ModelStructureNotifier) 
      	 ((ModelStructureNotifier)ancestor).fireBusinessObjectRemovedFrom (this, bo, by);
    }
    

    /**
     * msc 2007-04: I've built cycle-detection into this method,
     * but this creates a performance penalty that I'd like to avoid.
     * TODO does a cycle-proof iteration strategy exist?
     */
    public ObsProject findProject() {

   	 // iterate upwards on the "composition" trail: this will
   	 // deliver an Entity or any other ancestor that has no parent.
   	 //
   	 // side note: usually a schedblock is not a root because it has
   	 // an obsunitset-parent. so when starting from a bo contained in a
   	 // schedblock, the composition-trail ends actually at the obsproject.
   	 BusinessObject root;
   	 BusinessObject curr = this;
   	 do {
   		 root = curr;
   		 curr = curr.getParent();
   	 } while (curr != null);

   	 // there is a good chance we are at an entity now
   	 
   	 // if it's the project, we're done
		 if (root instanceof ObsProject)
			 return (ObsProject)root;

		 // otherwise let's attempt to navigate further up

		 List<IBusinessObject> proj = root.referrers.get (ObsProject.class);
		 if (!proj.isEmpty())
			 return (ObsProject) proj.get(0);

//   	 try {
//
//   		 
//      	 if (root instanceof ObsProposal)
//      		 return ((ObsProposal)root).getObsProject();
//      	 
//      	 if (root instanceof ObsReview)
//      		 return ((ObsReview)root).getObsProject();
//      	 
//      	 if (root instanceof SchedBlock)
//      		 return ((SchedBlock)root).getObsProject();
//      	 
//      	 if (root instanceof ProjectStatus)
//      		 return ((ProjectStatus)root).getObsProject();
//      	 
//   	 } catch (UnknownEntityException exc) {
//   		 // fall through to "return null"
//   	 }

		 // probably this BO is not contained in a project, or it
		 // is a member of an entity that doesn't know its project.

   	 return null; 
    }


    public List<IBusinessObject> findPathFromRoot() {
   	 List<IBusinessObject> ret = new ArrayList<IBusinessObject>();
   	 
   	 BusinessObject curr = this;
   	 do {
   		 ret.add(curr);
   		 curr = curr.getParent();
   	 } while (curr != null);
   	 
   	 java.util.Collections.reverse(ret);
   	 return ret;
    }


    /**
     * Returns the Entity that "roots" this BO by UML Composition.
     * 
     * @return the entity, or {@code null}
     */
    public Entity findEntity() {
   	 BusinessObject curr = this;
   	 while(! (curr instanceof Entity || curr==null) ) {
   		 curr = curr.getParent();
   	 }
   	 return (Entity) curr;
    }


    /**
     * Gets the BO that "parents" this BO by UML composition.
     * <p>
     * This "happens" to implement {@code getParent()} in interface @{code CompositionChild},
     * note that only if this business object is an instance of CompositionChild, you can
     * expect this method has been filled with something reasonable by the generator. 
     *
     * @return the parent (if this is a child), or {@code null}
     * 
     * @see CompositionChild#getParent()
     */
    public BusinessObject getParent() {
        return (BusinessObject)referrers.getParent();
    }


    /* Please refer to javadoc at IBusinessObject */
    public List <IBusinessObject> referencedBOs (Referring.By ... by) {
       return new ArrayList<IBusinessObject>();
    }

	 /**
     * Returns all business objects that are associated to this object, in other
     * words this object's parts and referenced entities, but not its attributes.
     * <p>
     * Dec 2007:
     * This is a convenience method that forwards to <code> referencedBOs(...) </code>,
     * it is maintained for the time being for the sake of full backward-compatibilty.
     * 
     * @return a list of the objects (parts and entities) this object associates to 
     */
	 public List<IBusinessObject> associatedObjects () {
	 	return (List<IBusinessObject>) referencedBOs (Referring.By.UmlComposition, Referring.By.UmlUnidir);
	 }

    /**
		 * This is for use by Importer logic when constructing a structure of BOs from an
		 * imported structure of SOs. <b> Other client code should very likely not call
		 * this. </b>
		 * <p>
		 * For every composition-part and every valuetype-attribute found in the storage
		 * object, this will create and store a BO that wraps it.
		 * <p>
		 * Defined here so any BO can invoke <code>super.wrapObjects()</code>.
		 */
    public void wrapObjects(ReferenceablesMap referencedObjects, List<ReferenceableProxy<? extends IBusinessObject>> referrers) {
    }
    
	     
	/**
	 * Adds the given Object <em>o</em> to the specified Collection <em>c</em>.
	 * This is a helper method for subclass-implementations of 
	 * {@link #associatedParts()} and {@link #associatedEntities()}.
	 * <br>
	 * If <em>o</em> is an array, its elements will be added to the 
	 * <em>c</em> sequentially. Otherwise <em>o</em> will be added itself.
	 * In either case, this method will never add a <code>null</code> to the <em>c</em>.
	 * <br>
	 * Note that the actual collection implementation may have additional restrictions,
	 * e.g. a <code>Set</code> will refuse to add an object if it already contains it.    
	 * 
	 * @param c a collection to populate
	 * @param o an object to add
	 * @return whether the collection changed as a result of the call
	 */
	protected boolean addToCollection(Collection c, Object o) {

		if (o == null) {
			return false;
		}
		
		boolean ret = false;
		
		if (o instanceof Object[]) {
		
			Object[] array = ((Object[])o);
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					ret |= c.add(array[i]);
				}
			}
		
		} else {
			ret = c.add(o);
		}
		
		return ret;
	}
	 
	 
        
    // (Name, Value) pair support
    // ==========================
    /*
     * Please note that this is intended only to allow us to prototype new
     * fields that we might need to add but which we can't as the ICD freeze in
     * any given month is passed. Properties added via this mechanism should be
     * added to the data model and supported properly at the earliest time
     * possible.
     * 
     * msc (2008-06): In the course of finally fixing the tree-caption truncation problem,
     * I have started using this feature to remember the current "tree-string" of a BO.
     * This is used by the tree renderer to decide whether it is time to recompute the
     * caption width. It seems to me a very useful idea to allow clients to associate
     * objects with a BO, without needing to introduce additional hashmaps.
     */
    private Map<String, Object> nameValuePairs = new TreeMap<String, Object>();
    
    /**
     * Set the value associated with a given name
     * 
     * @param name
     * @param value
     */
    public void setNameValue(String name, Object value) {
        nameValuePairs.put(name, value);
    }
    
    /**
     * Get the value associated with a given name.
     * 
     * @param name
     * @return the Object which is the value associated with the name (will be
     *         null if no value has been associated with the name)
     */
    public Object getNameValue(String name) {
        return nameValuePairs.get(name);
    }
    
    // PropertyChangeNotifier implementation
    // =====================================
    
    
    public static boolean notificationEnabled;

	/**
	 */
	public void setNotificationEnabled (boolean flag) {
	    PropertyChangeSystemFlags.setNotificationEnabled(flag);
	}
	
	/**
	 */
	public boolean isNotificationEnabled () {
		return PropertyChangeSystemFlags.isNotificationEnabled();
	}

   private Vector<PropertyChangeListener> listeners = new Vector<PropertyChangeListener>();
	
	/**
	 */
	protected void firePropertyChangeEvent (Object source, String property, Object newValue ) {
		if (! isNotificationEnabled() || listeners == null || listeners.isEmpty() )
			return;

		PropertyChangeEvent e = new PropertyChangeEvent (source, property, null, newValue);
		for (PropertyChangeListener pcl : listeners) {
			pcl.propertyChange(e);
		}
	}
	
	/**
	 * A listener can only be added once.
	 */
	public void addPropertyChangeListener (PropertyChangeListener pcl) {
		if (!listeners.contains(pcl))
			listeners.add(pcl);
	}
	
	/**
	 */
	public void removePropertyChangeListener (PropertyChangeListener pcl) {
		listeners.remove(pcl);
	}
	
   
	
}

