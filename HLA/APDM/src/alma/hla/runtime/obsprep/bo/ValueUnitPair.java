package alma.hla.runtime.obsprep.bo; 

import alma.hla.runtime.obsprep.util.UnitMap;


/**
 * @author dac
 *
 * The interface common to all DoubleWithUnit types. This is a frig to
 * get around som features in Castor
 * 
 */

public interface ValueUnitPair<T extends ValueUnitPair> extends DeepCopiable {

    // Getters and setters for the value (content) and the unit
    double getContent();
    double getContentInDefaultUnits();
    double getContentInUnits(String newUnit);
    boolean hasContent();
    String getUnit();
    void setContent(double content);
    void setUnit(String unit);
    // A list of valid units.
    UnitMap unitMap();
    String defaultUnit();
    void setUnitToDefault();
    void convertToDefaultUnit();
    void convertToUnit(String newUnit);

    // Copying
    T inDefaultUnits();
    
    // Comparing
    public int compareTo(T that);

    
    /**
     * Test to see if <code>this</code> is greater than a given value
     * @param arg - the value against which to compare <code>this</code>
     * @return <code>true</code> if <code>this</code> is greater than arg, <code>false</code> otherwise.
     */
	public boolean isGreaterThan(T arg);

	/**
     * Test to see if <code>this</code> is greater than or equal to a given value
     * @param arg - the value against which to compare <code>this</code>
     * @return <code>true</code> if <code>this</code> is greater than or equal to arg, <code>false</code> otherwise.
     */
	public boolean isGreaterThanOrEqualTo(T arg);

    /**
     * Test to see if <code>this</code> is less than a given value
     * @param arg - the value against which to compare <code>this</code>
     * @return <code>true</code> if <code>this</code> is less than arg, <code>false</code> otherwise.
     */
	public boolean isLessThan(T arg);

    /**
     * Test to see if <code>this</code> is less than or equal to a given value
     * @param arg - the value against which to compare <code>this</code>
     * @return <code>true</code> if <code>this</code> is less than or equal to arg, <code>false</code> otherwise.
     */
	public boolean isLessThanOrEqualTo(T arg);
	
	/**
	 * Test to see if the value of <code>this</code> is zero.
	 * @return <code>true</code> if the value of <code>this</code> is zero, <code>false</false> otherwise.
	 */
	public boolean isZero();

	/**
	 * Test to see if the value of <code>this</code> is zero.
	 * @param torelance comparison torelance
	 * @return <code>true</code> if the value of <code>this</code> is less than the comparison torelanceo,
	 *  <code>false</false> otherwise.
	 */
	public boolean isZero(double torelance);

	/**
	 * Test to see if the value of <code>this</code> is equal to the given frequency.
	 * @param arg the value against which to compare <code>this</code>
	 * @return <code>true</code> if <code>this</code> is equal to the given frequency, 
	 * <code>false</code> otherwize. 
	 */
	public boolean isEqual(T arg);

	
	/**
	 * Test to see if the value of <code>this</code> is equal to the given frequency.
	 * @param arg the value against which to compare <code>this</code>
	 * @param torelance comparison torelance
	 * @return <code>true</code> if <code>this</code> is equal to the given frequency, 
	 * <code>false</code> otherwize. 
	 */
	public boolean isEqual(T arg, double torelance);

    /**
     * Returns a new object representing <code>this</code> + arg.
     * Uses the units of <code>this</code>
     * @param arg - the value to add to <code>this</code>
     * @return a new object of this type;
     */
	public T plus(T arg);
	
    /**
     * Returns a new object representing <code>this</code> - arg.
     * Uses the units of <code>this</code>
     * @param arg - the value to subtract from <code>this</code>
     * @return a new object of this type;
     */
	public T minus(T arg);

    /**
     * Returns a new object representing <code>this</code> * factor.
     * Uses the units of <code>this</code>
     * @param factor - the value by which to multiply <code>this</code>
     * @return a new object of this type;
     */
	public T multiply(double factor);
	
	
    /**
     * Returns a new object representing <code>this</code> / factor.
     * Uses the units of <code>this</code>
     * @param factor - the value by which to divide <code>this</code>
     * @return a new object of this type;
     */
	public T divide(double factor);

    /**
     * Returns a new object representing half of <code>this</code>.
     * Uses the units of <code>this</code>
     * @return a new object of this type;
     */
	public T half();

}
