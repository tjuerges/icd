package alma.hla.runtime.obsprep.bo;

import java.util.List;

import alma.hla.runtime.obsprep.interfaces.PropertyChangeNotifier;
import alma.obsprep.bo.obsproject.ObsProject;

/**
 * The interface all business objects comply to.
 * 
 * This is actually a collection of other interfaces.
 * With multiple inheritance being possible on interfaces,
 * this allows for smoother semantic annotations of data model
 * classes using interfaces. For instance, it would be easy to
 * declare that some type is a BusinessObject and a
 * Comparable at the same time.
 * 
 * @author mschilli
 */
public interface IBusinessObject	extends DeepCopiable, PropertyChangeNotifier {

   /**
    * Returns the internal storage object that holds the actual data.
    * @return The storage object wrapped by this object.
    */
   public Object getCastorObject();


   /**
    * Returns a list of business objects that are (datamodel-wise) referenced by
    * this object.
    * For the following types of reference, this typically returns: <ul>
    * <li> <i> UmlComposition </i>  -  a list of parts (each element is a CompositionChild)
    * <li> <i> UmlUnidir </i>       -  a list of entities (each element is an Entity)
    * <li> <i> UmlAttribution </i>  -  a list of valuetypes (each element is an AbstractDoubleWithUnit)
    * </ul>
    * A caller being sure about the returned element type (one that specifies only a single type argument) is free to
    * cast the returned list to a more specific subclass of IBusinessObject, like <i> List &lt;CompositionChild&gt; </i>.
    * 
    * @return a list of business objects referenced from this object.
    */
   public List<IBusinessObject> referencedBOs (Referring.By ... bys);



   /**
    * Returns the ObsProject this BO belongs to,
    * i.e. walks up to the Entity this BO belongs to,
    * and from there to the ObsProject. This takes
    * advantage of the fact that all Entities define
    * a reference to the ObsProject.
    * 
    * @return the project this bo belongs to, or <code>null</code> on failure
    */	
   public ObsProject findProject();

   public Entity findEntity();
   
}
