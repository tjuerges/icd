/*
 * Created on Apr 19, 2007 by mschilli
 */
package alma.hla.runtime.obsprep.bo;

import java.util.ArrayList;
import java.util.List;


/**
 * This is code that allows for navigation within an OT
 * datamodel instance. It is available in each BusinessObject.
 * 
 * @author mschilli
 */
public class Referring {

	// Nested Types
	// ======================================================================

	/**
	 * These tags denote UML-semantics, we don't bother about the ALMA-semantics
	 * and the contradiction it creates for Entities that are compositions-parts. 
	 */
	static public enum By {
		UmlComposition,
		UmlUnidir,
		UmlAttribution
	};

	static private class Referrer {
		IBusinessObject bo;
		By by;
		
		@Override public String toString() {
			return getClass().getSimpleName()+"[bo="+bo+", by="+by+"]";
		}
	}


	// collection, and read/write API
	// ======================================================================
   // I synchronize the public methods, so internally i don't need to
	// synchronize anything (also using ArrayList instead of Vector).

	private ArrayList<Referrer> all = new ArrayList<Referrer>();

	public synchronized void add (IBusinessObject bo, By by) {
		Referrer r = new Referrer();
		r.bo = bo;
		r.by = by;
		all.add(r);
	}

	public synchronized boolean remove (IBusinessObject bo, By by) {
		for (Referrer r : all) {
			if (r.bo == bo && r.by == by) {
				all.remove(r);
				return true;
			}
		}
		return false;
	}

	public synchronized boolean empty () {
		return (all.size() == 0);
	}

	public synchronized List<IBusinessObject> get (By by) {
		List<IBusinessObject> ret = new ArrayList<IBusinessObject>();
		for (Referrer r : all) {
			if (r.by == by)
				ret.add(r.bo);
		}
		return ret;
	}

	public synchronized List<IBusinessObject> get () {
		return (ArrayList<IBusinessObject>) all.clone();
	}

	public synchronized List<IBusinessObject> get(Class<?> t) {
		List<IBusinessObject> ret = new ArrayList<IBusinessObject>();
		for (Referrer r : all) {
			if (t.isInstance(r.bo))
				ret.add(r.bo);
		}
		return ret;
	}

	
	
	/* having this method for smoother migration of current code*/
	public synchronized IBusinessObject getParent () {
		List<IBusinessObject> pp = get (By.UmlComposition);
		return (pp.isEmpty())? null : pp.get(pp.size()-1);
	}

	/* having this method for smoother migration of current code*/
	public synchronized IBusinessObject getOne () {
		return (all.isEmpty())? null : all.get(all.size()-1).bo;
	}


}
