package alma.hla.runtime.obsprep.bo;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;

import alma.entities.commonentity.EntityRefT;
import alma.hla.runtime.obsprep.util.CannotAssignIdException;


/**
 * 
 * <h3>Concept</h3>
 * 
 * The "COMPOSITION transitive closure" is the set of all objects that can be reached from
 * the root object (the one that's to be copied) via Uml-Composition.
 * <p>
 * I'm going with the assumption that a proper clone of an object consists of its
 * COMPOSITION transitive closure. Further, that any UNIDIR links pointing to an object
 * inside that closure ("inbound" links) should remain valid through cloning, which means
 * they have to be modified to fit the context of the clone. Opposed to that, "outbound"
 * links should remain pointing to the original link target, which means they should
 * remain the same as they are in the original.
 * <p>
 * 
 * <h3>Implementation</h3>
 * 
 * When requested to copy an object, this class invokes the object's initFrom() methods.
 * The initFrom() implementation knows which sub-objects exist, and for each sub-object
 * delegates back to this class to perform the actual cloning operation. This happens
 * recursively until the deep-copy is complete.
 * <p>
 * When requested to copy an object, this class needs to decide whether it should really
 * instantiate a new clone or not. Creation should always happen for a COMPOSITION link,
 * but for UNIDIR links only if they are "inbound". To decide whether a UNIDIR link is
 * "inbound", we first create a full copy of the COMPOSITION transitive closure (pass 1),
 * and afterwards process the UNIDIR links (pass 2) - checking for each link target if it
 * had been copied in pass 1 (so it is an inbound link) or not (so it is an outbound link).
 *
 * <p><i> Side note: In previous versions, we were using a different strategy that was
 * less complex: we would copy the object in a single pass, "peeking" into the COMPOSITION
 * transitive closure whenever we needed to decide whether a UNIDIR link is "inbound" or
 * "outbound". The peeking however was based on BOs. With the addition of Status
 * entities, we were facing a situation where an object references an entity but the
 * BO for that entity would not be available (= would not exist as an object in memory). 
 * This prevented proper peeking, and the whole copying algorithm had to be turned into
 * the 2-pass strategy described above.</i>
 * 
 * <p>
 * Second, if an object has been copied before, it will not be copied again but the
 * previously created object will be re-used. This behavior is generally required to
 * correctly copy a graph. We achieve this by saving already created objects in a cache.
 * Because of the recursive nature of the deep-copy process, it is important to cache each
 * object instantly after its creation and only then start populating it by copying its
 * sub-objects. Only that way, sub-objects referring ancestor-objects can be served
 * successfully from the cache.
 * <p>
 * Overall, this class has the following things to decide upon a copy request: Should a
 * new clone be created, or should a clone be taken from cache, or should actually the
 * original object be used?
 * 
 * @author mschilli
 */
public class Copier {


	// Construction
	// ==========================================================

	/**
	 * Create a new Copier with default settings.
	 */
	public Copier() {
	}


	// API for clients that want to copy something
	// ===========================================================

	protected BusinessObject theRootObjectToCopy;

	/**
	 * Copies a business object. This is public API for clients.
	 * 
	 * @return a copy of the specified BO.
	 * @throws CopyException on problems
	 */
	public BusinessObject copy (BusinessObject obj) throws CopyException {

		theRootObjectToCopy = obj;
		map.clear(); // if no one has reaped the map by now then it's too late now, sorry.

		try {
			// pass 1 : recurse the composition tree and copy it
			BusinessObject ret = goCopy(obj);

			// pass 2 : recurse the composition tree and deal with unidirectional links
			LinkedList<IBusinessObject> elems = new LinkedList<IBusinessObject>();
			elems.add(obj);
			while (!elems.isEmpty()) {
				IBusinessObject current = elems.remove(0);
				elems.addAll (current.referencedBOs(Referring.By.UmlComposition));
				BusinessObject copy = copied.get (current);
				copy.initEntityReferencesFrom (current, this);
			}

			return ret;

		} finally {
			/* clear the hashmap in the end, in order to avoid references
			 * to BOs lingering around after copying is over */
			copied.clear();
			theRootObjectToCopy = null;
		}
	}


	protected IdentityHashMap<BusinessObject, BusinessObject> copied = new IdentityHashMap<BusinessObject, BusinessObject>();

	/**
	 * Trigger the generated copy-logic in <code>obj</code>, plus book-keeping.
	 */
	protected BusinessObject goCopy (BusinessObject obj) {
		BusinessObject ret = obj.invokeCreateUnitialized();

		// save instance first before populating it
		copied.put(obj, ret);

		ret.initAttribsAndPartsFrom(obj, this);
		return ret;
	}


	protected HashMap<String, String> map = new HashMap<String, String>();
	
	/**
	 * The returned map describes which Ids were replaced by which new Ids during copying.
	 * This is public API for clients.
	 * 
	 * Note: This method can only be called once: it will clear the map held internally by
	 * this class.
	 */
	public Map<String, String> reap () {
		Map<String, String> ret = (Map<String, String>) map.clone();
		map.clear();
		return ret;
	}


	// Callback methods used by the objects that we clone
	// ===========================================================


	/**
	 * Not public API. This is used by BusinessObject <code>initFrom()</code>-implementations.
	 * 
	 * @throws CopyException on problems
	 */
	public void mkId (EntityPart orig, EntityPart copy) throws CopyException {
		try {

		String orig_epid = orig.getEntityPartId();
		String copy_epid = EntityIDFactory.getFactory().assignEntityPartId(copy);
		map.put (orig_epid, copy_epid);

		} catch (CannotAssignIdException exc) {
			throw new CopyException("Couldn't create a new EntityId for a clone of " + orig, exc);
		}
	}

	/**
	 * Not public API. This is used by BusinessObject <code>initFrom()</code>-implementations.
	 * 
	 * @throws CopyException on problems
	 */
	public void mkId (Entity orig, Entity copy) throws CopyException {
		try {

		String orig_eid = orig.getEntityID();
		String copy_eid = EntityIDFactory.getFactory().assignUniqueEntityId(copy);
		map.put (orig_eid, copy_eid);

		} catch (CannotAssignIdException exc) {
			throw new CopyException("Couldn't create a new EntityPartId for a clone of " + orig, exc);
		}
	}

	/**
	 * Not public API.
	 * This is used by BusinessObject <code>initAttribsAndPartsFrom()</code>-implementations.
	 * @throws CopyException on problems
	 */
	public BusinessObject cpAttr (BusinessObject obj) throws CopyException {

		// check if already copied
		BusinessObject ret = copied.get(obj);
		if (ret != null)
			return ret;

		// go copy using the BO's copy-Api
		return goCopy(obj);
	}

	/**
	 * Not public API.
	 * This is used by BusinessObject <code>initAttribsAndPartsFrom()</code>-implementations.
	 * @throws CopyException on problems
	 */
	public BusinessObject cpPart (BusinessObject obj) throws CopyException {
		/*
		 * Side note on the infamous Schedblock entity that can be part
		 * of another entity: we do not distinguish between Entities and
		 * non-Entities, a composition-part is a composition-part. basta.
		 */

		// check if already copied
		BusinessObject ret = copied.get(obj);
		if (ret != null)
			return ret;

		// go copy using the BO's copy-Api
		return goCopy(obj);
	}

	/**
	 * Not public API.
	 * This is used by BusinessObject <code>initEntityReferencesFrom()</code>-implementations.
	 * Used if the referenced entity is in memory (so we can work with the entity object).
	 */
	public BusinessObject cpUnidir (BusinessObject obj) {

		// check if already copied
		// this implies it's a member of the composition tree:
		// we are currently in pass2. if it's in "copied" then
		// it was put there in pass1 which has created a copy of
		// the composition tree.
		BusinessObject ret = copied.get(obj);
		if (ret != null)
			return ret;

		// otherwise it must be an outbound link, so return as is.
		return obj;
	}

	/**
	 * Not public API.
	 * This is used by BusinessObject <code>initEntityReferencesFrom()</code>-implementations.
	 * Used for not-in-memory case (so we can only deal with the link verbatim).
	 */
	public EntityRefT cpUnidir (EntityRefT ref, EntityRefT newRef) {
	   newRef.setEntityId(ref.getEntityId());
	   newRef.setPartId(ref.getPartId());
	   newRef.setDocumentVersion(ref.getDocumentVersion());
	   return newRef;
	}

}
