package alma.hla.runtime.obsprep.bo;



/**
 * This interface exists to give a home to the deepCopy() method.
 * I would have simply added that method to the IBusinessObject interface
 * but ValueUnitPair seems to depend on it as well, and I can'tell whether
 * all ValueUnitPairs are IBusinessObjects.
 * 
 * @author mschilli
 */
public interface DeepCopiable {

	 /**
	  * Clone this object.
     */
    public DeepCopiable deepCopy () throws CopyException;
    
}
