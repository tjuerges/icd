package alma.hla.runtime.obsprep.bo;

import java.util.HashMap;
import java.util.Map;
//
//class Reference {
//
//	public String _entityRefId;
//	public String _entityPartRefId;
//
//	public Reference(String entityRefId, String entityPartRefId) {
//		_entityRefId = entityRefId;
//		_entityPartRefId = entityPartRefId;
//	}
//
//	public int hashCode() {
//		int hashCode = 37;
//		if( _entityPartRefId != null )
//			hashCode += _entityPartRefId.hashCode();
//		if( _entityRefId != null ) 
//			hashCode += _entityRefId.hashCode();
//		return hashCode;
//	}
//
//	public boolean equals(Object o) {
//		if( !(o instanceof Reference) )
//			return false;
//		
//		Reference r = (Reference)o;
//
//		if( (_entityPartRefId == null && r._entityPartRefId != null) ||
//		    (_entityPartRefId != null && r._entityPartRefId == null) ||
//		    (_entityRefId != null && r._entityRefId == null) ||
//		    (_entityRefId == null && r._entityRefId != null))
//			return false;
//
//		boolean equals = true;
//		if( _entityPartRefId != null )
//			equals = _entityPartRefId.equals(r._entityPartRefId);
//		if( _entityRefId != null )
//			equals = equals && _entityRefId.equals(r._entityRefId);
//
//		return equals;
//	}
//}

/**
 * Map that stores, for every entityID, a reference to the property Entity,
 * and its EntityParts. This is supposed to be used temporarely while wrapping
 * the Castor objects into BusinessObjects.
 *
 * @author rtobar, Feb 23rd, 2011
 *
 */
public class ReferenceablesMap {

	private Map<String, Map<String, BusinessObject>> entitiesMap = new HashMap<String, Map<String,BusinessObject>>();
	private static final long serialVersionUID = -8600814347712116625L;

	/**
	 * Puts a BusinessObject into the map, indexed with the given <code>entityRefId</code> and <code>entityPartRefId</code>
	 * @param entityRefId The entity ID of the object
	 * @param entityPartRefId The entity part ID of the object, if any; <code>null</code> otherwise
	 * @param o The business object to index
	 */
	public void put(String entityRefId, String entityPartRefId, BusinessObject o) {
		Map<String, BusinessObject> objectsForEntityRefId = entitiesMap.get(entityRefId);
		if( objectsForEntityRefId == null ) {
			objectsForEntityRefId = new HashMap<String, BusinessObject>();
			entitiesMap.put(entityRefId, objectsForEntityRefId);
		}
		objectsForEntityRefId.put(entityPartRefId, o);
	}

	/**
	 * Get the indexed business object for the given entity and entity part IDs.
	 * @param entityRefId The entity ID
	 * @param entityPartRefId The entity part ID
	 * @return The indexed business object for the given key; <code>null</code> if there is none.
	 */
	public BusinessObject get(String entityRefId, String entityPartRefId) {
		Map<String, BusinessObject> storedObjectsForEntityID = entitiesMap.get(entityRefId);
		if( storedObjectsForEntityID == null )
			return null;
		return storedObjectsForEntityID.get(entityPartRefId);
	}

}
