package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityRefT;
import alma.hla.runtime.obsprep.util.UnknownEntityException;

/**
 * <p>This class is a thin wrapper around referenceable objects.
 * This proxy is used by objects that refer to entities
 * through a {@link EntityRefT} via the corresponding underlying castor object.
 * This proxy, then, avoids the old <code>IdObjectRegistry</code> singleton.
 *
 * <p>Given a scenario where <code>A</code> might refer to an entity <code>B</code>
 * via an <code>EntityRefT</code> reference, the following points are considered:
 * 
 * <ul>
 * 	<li>When <code>A</code> is created in memory, it will contain a private attribute
 * of type <code>EntityProxy</code>, pointing to <code>null</code>.
 *  <li>If the proxy is kept as <code>null</code> during <code>A</code>'s life
 *  it means that it is actually referring to no <code>B</code> object at all.
 *  <li>If the proxy, on the other hand is not null, then it means that the reference
 *  should be satisfied, and a call to {@link #getReference()} should retrieve <code>B</code>.
 *  <li>Finally, if the proxy was created, but its inner object reference was
 *  never filled, it means that the reference cannot be satisfied, and a
 *  {@link UnknownEntityException} is thrown.
 * </ul>
 *
 * @author rtobar, Feb 16th, 2011
 *
 * @param <T> The class of the entity being wrapped by this proxy
 */
public interface ReferenceableProxy<T> {

	/**
	 * Retrieves the actual reference to the wrapped object.
	 *
	 * @return The actual reference to the wrapped object
	 * @throws UnknownEntityException If no object is currently being wrapped by this proxy
	 */
	public T getReference();
	
	/**
	 * Sets the inner object reference for this proxy
	 * @param entity The object reference to be wrapped by this proxy
	 */
	public void setReference(T entity);

	/**
	 * Gets the XML-world reference represented by this proxy
	 * @return An EntityRefT which containing the reference to the wrapped object
	 */
	public EntityRefT getEntityRefT();
}
