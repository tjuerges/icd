package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityT;
import alma.hla.runtime.obsprep.util.CannotAssignIdException;

/**
 * EntityId Management API for the DataModel classes.
 * 
 * This is an abstract implementation used at compile-time. At run-time, the real
 * implementation must be set through a call to <code>setRuntimeImplementation()</code>.
 * 
 * @author A. M. Chavan
 * @author D. Clarke
 * @author mschilli
 */

public abstract class EntityIDFactory {


	// Forwarding to real (run-time) implementation
	// ======================================================

	private static EntityIDFactory runtimeImpl;

	/**
	 * Set the subclass of this class that does the real work at runtime
	 */
	public static void setRuntimeImplementation (EntityIDFactory realImpl) {
		runtimeImpl = realImpl;
	}

	private static EntityIDFactory runtimeImplementation () {
		if (runtimeImpl == null) {
			throw new RuntimeException("need to be initialized via setRuntimeImplementation(), see class doc for details");
		}
		return runtimeImpl;
	}


	// API
	// ======================================================


	/** Singleton */
	public static EntityIDFactory getFactory () {
		return runtimeImplementation();
	}

	/** Destroy the factory, releasing all resources and connections */
	public abstract void destroy ();

	/**
	 * Write a unique ID into an EntityT object.
	 * 
	 * @param e The EntityT object to modify.
	 * @throws PersistenceException
	 */
	protected abstract String doAssignUniqueEntityId (EntityT e) throws Exception;

	/**
	 * Write a part ID into an EntityPart object.
	 * @param entPart - an EntityPart BO
	 * @return the newly assigned Id
	 * @throws CannotAssignIdException
	 */
	public abstract String assignEntityPartId (EntityPart entPart) throws CannotAssignIdException;

	/**
	 * Write a unique ID into an Entity object.
	 * 
	 * @param ent - an Entity BO
	 * @return the newly assigned Id
	 * 
	 * @throws CannotAssignIdException
	 */
	public String assignUniqueEntityId (Entity ent) throws CannotAssignIdException {
		/* msc (2007-10):
		 * Introducing this as a replacement for setUniqueID(EntityT):
		 * 1) It registers created Ids with I2C (something which all clients of
		 *    the old method did manually).
		 * 2) It only declares an unchecked exception: Id creation hardly
		 *    ever fails (thanks to the fake-Id mechanism), so why should we
		 *    force all client code to catch it.
		 * 3) It takes a BO as its argument. If we dismiss the I2C-Registry Singleton
		 *    in the future, the BO can be helpful in finding the right registry.
		 */
		try {
			EntityT so_ent = ent.getmCastorEntity();

			// assign id
			String id = doAssignUniqueEntityId(so_ent);

			// register
			//IdObjectRegistry.getInstance().addEntity(ent);

			return id;
			
		} catch (Exception exc) {
         // hardly happens since the OT has its fake-Id fallback mechanism
         throw new CannotAssignIdException("couldn't assign Entity-Id to "+ent, exc);
      }
	}

	/**
	 * Replace the existing unique ID of an Entity object. After a call to this method,
	 * the caller will also need to update any references to this Entity accordingly,
	 * otherwise those references would be left as broken.
	 *
	 * @param ent - an Entity BO
	 * @return the newly assigned Id
	 * 
	 * @throws CannotAssignIdException
	 */
	public String replaceEntityId (Entity ent) throws CannotAssignIdException {
		try {

			EntityT so_ent = ent.getmCastorEntity();
			String oldId = so_ent.getEntityId();

	      // This forwards to assignUID() in acs jcont which fails if the entity already has a UID
	      // set. The more powerful replaceUID() in jcont is not public API and maybe shouldn't be.
			so_ent.setEntityId(null);
			String newId = doAssignUniqueEntityId(so_ent);

			// update registry
			//IdObjectRegistry.getInstance().addEntity(ent);
			//IdObjectRegistry.getInstance().remove(oldId);

			return newId;

		} catch (Exception exc) {
         // hardly happens since the OT has its fake-Id fallback mechanism
         throw new CannotAssignIdException("couldn't replace Entity-Id of "+ent, exc);
      }
	}


}
