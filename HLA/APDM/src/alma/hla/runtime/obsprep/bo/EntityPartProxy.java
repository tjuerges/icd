package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityRefT;
import alma.hla.runtime.obsprep.util.UnknownEntityPartException;

public class EntityPartProxy<E extends EntityPart> implements ReferenceableProxy<E> {

	private EntityRefT _ref;
	private E _object;

	public EntityPartProxy(EntityRefT ref) {
		_ref = ref;
	}

	public E getEntityPart() throws UnknownEntityPartException {
		E object = getReference();
		if( object == null )
			throw new UnknownEntityPartException("Can't resolve entity-part '<" + _ref.getEntityId() + "," + _ref.getPartId() + ">': entity part is not in memory at all");
		return object;
	}

	@Override
	public E getReference() {
		return _object;
	}

	@Override
	public EntityRefT getEntityRefT() {
		return _ref;
	}

	@Override
	public void setReference(E entityPart) {
		_object = entityPart;
	}

}