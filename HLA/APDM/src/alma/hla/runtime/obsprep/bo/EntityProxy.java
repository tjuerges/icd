package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityRefT;
import alma.hla.runtime.obsprep.util.UnknownEntityException;

/**
 * <p>This class is a thin wrapper around {@link Entity} objects.
 * This proxy is used by objects that refer to entities
 * through a {@link EntityRefT} via the corresponding underlying castor object.
 * This proxy, then, avoids the old <code>IdObjectRegistry</code> singleton.
 *
 * <p>Given a scenario where <code>A</code> might refer to an entity <code>B</code>
 * via an <code>EntityRefT</code> reference, the following points are considered:
 * 
 * <ul>
 * 	<li>When <code>A</code> is created in memory, it will contain a private attribute
 * of type <code>EntityProxy</code>, pointing to <code>null</code>.
 *  <li>If the proxy is kept as <code>null</code> during <code>A</code>'s life
 *  it means that it is actually referring to no <code>B</code> object at all.
 *  <li>If the proxy, on the other hand is not null, then it means that the reference
 *  should be satisfied, and a call to {@link #getReference()} should retrieve <code>B</code>.
 *  <li>Finally, if the proxy was created, but its inner object reference was
 *  never filled, it means that the reference cannot be satisfied, and a
 *  {@link UnknownEntityException} is thrown.
 * </ul>
 *
 * @author rtobar, Feb 16th, 2011
 *
 * @param <E> The class of the entity being wrapped by this proxy
 */
public class EntityProxy<E extends Entity> implements ReferenceableProxy<E> {

	private EntityRefT _ref;
	private E _object;

	public EntityProxy(EntityRefT ref) {
		_ref = ref;
	}

	public E getEntity() throws UnknownEntityException {
		E object = getReference();
		if( object == null )
			throw new UnknownEntityException("Can't resolve entity-id '" + _ref.getEntityId() 
					+ "': entity is not in memory at all");
		return object;
	}

	@Override
	public E getReference() {
		return _object;
	}

	@Override
	public EntityRefT getEntityRefT() {
		return _ref;
	}

	@Override
	public void setReference(E entity) {
		_object = entity;
	}

}