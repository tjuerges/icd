/*
 * Created on Aug 1, 2006 by mschilli
 */
package alma.hla.runtime.obsprep.bo;


/**
 * An Exception thrown by copy-operations. 
 *
 * @author mschilli
 */
public class CopyException extends RuntimeException {
	
	public CopyException (BusinessObject bo, Throwable cause) {
		this("failed to copy "+bo, cause);
	}

	public CopyException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CopyException(String message) {
		super(message);
	}
		
}