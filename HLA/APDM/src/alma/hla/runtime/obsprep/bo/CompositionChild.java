/*
 * Created on Aug 9, 2006 by mschilli
 */
package alma.hla.runtime.obsprep.bo;

import java.util.List;

/**
 * Identifies a BO that is part of another BO by means of UML composition,
 * i.e. the "parent" of this object contains a method <code>addAbc</code>,
 * resp.<code>setAbc</code>, and the respective counterpart method for removal.
 * 
 * This interface is part of the "do away with instanceof cascades" initiative. 
 * 
 * @author mschilli
 */
public interface CompositionChild extends IBusinessObject {

   /**
    * Returns the "parent" BO which currently contains this BO by UML composition.
    * <p>
    * At any given time, a BO has either no parent or exactly one parent
    * (by definition of the UML composition relation).
    * <p>
    * Most BO classes accept only one type of potential parent, but a few
    * accept different types of potential parents.
    * 
    * @return the current "composition-parent" of this BO, or <code>null</code>
    */
	public BusinessObject getParent();

   /*
	 * msc 2009-08: commented out because this method's results are confusing to clients:
	 * this has been used in several places in the OT to find out the entity for a BO.
	 * However, when the BO is inside a Schedblock, this won't return the Schedblock entity
	 * (because schedblocks do have parents and thus are not roots) but instead the project
	 * entity. Thus I'm adding a dedicated method to find out the Entity for a BO, and
	 * dismiss this method.
	 * 
	 * public BusinessObject getRoot();
	 */

  /** Removes this object from its parent, provided the parent can be determined.
	 * Else, this method does nothing.
	 * 
	 * If the parent's removal operation throws an exception, it shall not be caught but
	 * propagated to the caller.
	 */
	public void removeFromParent();
	
	/**
	 * Adds this object to the specified parent, provided the
	 * parent is of correct type. Else, this method throws a
	 * ClassCastException.
	 * 
	 * If the parent's add operation throws an exception,
	 * it shall not be caught but propagated to the caller.
	 * 
	 * @throws ClassCastException
	 */
	public void addToParent(IBusinessObject p);
	
	/**
	 * Returns the possible types of parents for this object.
	 * 
	 * An object of one of the returned types will be an appropriate
	 * argument to <code>addToParent(...)</code>
	 * and to <code>setParent(...)</code>.
	 *  
	 * @return the type of the parent.
	 */
	public List<Class> getParentTypes();
	
}


