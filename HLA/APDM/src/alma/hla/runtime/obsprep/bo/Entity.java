/*
 *    ALMA - Atacama Large Millimiter Array
 *    (c) European Southern Observatory, 2004
 *    Copyright by ESO (in the framework of the ALMA collaboration),
 *    All rights reserved
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *    MA 02111-1307  USA
 */
package alma.hla.runtime.obsprep.bo;

import alma.entities.commonentity.EntityRefT;
import alma.entities.commonentity.EntityT;

/**
 * 
 * @author hsommer
 * created Jun 11, 2004 5:35:02 PM
 */
public interface Entity extends IBusinessObject {


	public String getEntityID();

   /**
    * This Magic Getter gets generated for every Entity.
    * It is a helper method for internal classes like the Copier.
    */
	public EntityT getmCastorEntity();

	// msc (2007-08): Would be nice to define this method. As of today,
	// all Entities but ObsProject itself happen to contain this method,
	// due to the fact that in the UML they all define a reference to the
	// ObsProject. So all Entities but ObsProject would comply to this
	// interface method without any effort required.
	/* public ObsProject getObsProject(); */
	
}
