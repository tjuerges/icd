package alma.hla.runtime.obsprep.interfaces;

import java.beans.PropertyChangeListener;

/**
 * The interface which must be implemented by all OBSPREP objects which wish to use
 * the Rules framework in the role of a principal.
 * 
 * @author dac
 * @version $Revision$
 */

// $Id$

public interface PropertyChangeNotifier {
	/**
	 * Set notification on or off - should be a static method really but you
	 * can't put them in interfaces. Implementers should ensure that this sets
	 * one system wide flag, not just a per instance one.
	 * @param how
	 */
	public void    setNotificationEnabled(boolean how);
	
	/**
	 * @return - whether or not notification is currently operating
	 */
	public boolean isNotificationEnabled();
	
	
	/**
	 * Register as a PropertyChangeListener on this PropertyChangeNotifier
	 * @param pcl
	 */
	public void addPropertyChangeListener(PropertyChangeListener pcl);
	
	/**
	 * Deregister as a PropertyChangeListener on this PropertyChangeNotifier
	 * @param pcl
	 */
	public void removePropertyChangeListener(PropertyChangeListener pcl);
}
