/*
 * Created on Aug 7, 2006 by mschilli
 */
package alma.hla.runtime.obsprep.interfaces;

import alma.hla.runtime.obsprep.bo.IBusinessObject;
import alma.hla.runtime.obsprep.bo.Referring;

/**
 * A Listener to listen on structural changes in the data model. 
 *
 * @author mschilli
 */
public interface ModelStructureListener {
	
	public void businessObjectAddedTo (IBusinessObject bo, IBusinessObject parent, Referring.By by, int index);
	
	public void businessObjectRemovedFrom (IBusinessObject bo, IBusinessObject parent, Referring.By by);
	
}