/*
 * Created on Aug 7, 2006 by mschilli
 */
package alma.hla.runtime.obsprep.interfaces;

import alma.hla.runtime.obsprep.bo.IBusinessObject;
import alma.hla.runtime.obsprep.bo.Referring;

/**
 * A class that knows how to notify ModelStructureListeners. 
 *
 * @author mschilli
 */
public interface ModelStructureNotifier {
	
	public void fireBusinessObjectAddedTo (IBusinessObject bo, IBusinessObject parent, Referring.By by, int index);
	
	public void fireBusinessObjectRemovedFrom (IBusinessObject bo, IBusinessObject parent, Referring.By by);
	
}