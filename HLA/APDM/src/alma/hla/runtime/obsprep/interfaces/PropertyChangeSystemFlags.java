package alma.hla.runtime.obsprep.interfaces;


/**
 * The system wide flags to be used for all PropertyChangeNotifiers.
 * 
 * @author dac
 * @version $Revision$
 */

// $Id$

public class PropertyChangeSystemFlags {

    public static boolean notificationEnabled;
    
    static {
        notificationEnabled = true;
    }

    private PropertyChangeSystemFlags() {};
    
	/* (non-Javadoc)
	 * @see alma.hla.runtime.obsprep.interfaces.PropertyChangeNotifier#setNotificationEnabled(boolean)
	 */
	public static void setNotificationEnabled(boolean flag) {
		notificationEnabled = flag;
	}
	
	/* (non-Javadoc)
	 * @see alma.hla.runtime.obsprep.interfaces.PropertyChangeNotifier#isNotificationEnabled()
	 */
	public static boolean isNotificationEnabled() {
		return notificationEnabled;
	}
	

}
