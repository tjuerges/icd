/*
 * Created on Sep 22, 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package alma.hla.runtime;

import java.io.*;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;

public class DatamodelInstanceChecker extends DefaultHandler {
	private String datamodelVersion;
	private String schemaVersion;
	private String rootElemName;

	public String getDatamodelVersion(StringReader rdr) throws IOException { 
		// Use an instance of ourselves as the SAX event handler
		// Use the default (non-validating) parser

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			datamodelVersion = "-1";
			schemaVersion = "-1";
			// Parse the input
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new InputSource(rdr), this);
			

		} catch (Throwable t) {
			t.printStackTrace();
			throw new IOException("Error while searching for datamodelVersion");
		}
		rdr.close();
		System.out.println("Datamodel version = " + datamodelVersion);
        System.out.println("Schema version = " + schemaVersion);
        rootElemName = null;
        if (schemaVersion != "-1") datamodelVersion=schemaVersion; // Schema version takes precedence
		return datamodelVersion;
	}

	// ===========================================================
	// SAX DocumentHandler methods
	// ===========================================================


	@Override
	public void startElement(String namespaceURI, String lName, // local name
			String qName, // qualified name
			Attributes attrs) throws SAXException {
		String eName = lName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false

		if (rootElemName == null) {
			rootElemName = eName;
		}
		else if (eName.endsWith(rootElemName + "Entity")) {
			if (attrs != null) {
				for (int i = 0; i < attrs.getLength(); i++) {
					String aName = attrs.getLocalName(i); // Attr name
					if ("".equals(aName))
						aName = attrs.getQName(i);
					if (aName.equals("datamodelVersion"))
						datamodelVersion = attrs.getValue(i);
					if (aName.equals("schemaVersion"))
					    schemaVersion = attrs.getValue(i);				
				}
			}
		}
	}

}