
package alma.hla.runtime.vo;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;


public abstract class VOWriter
{

	protected List buffers = new ArrayList();
	
	public VOWriter() {
	  createGenericHeader();
	  createHeader();
	}

	protected abstract void createHeader();

	private void createGenericHeader() {}
	
	private void createGenericFooter() {}
	
	
	public String getXML() {
	  createGenericFooter();
	  StringBuffer sb = new StringBuffer();
	  for (int i=0;i<buffers.size();i++) {
		sb.append( (StringBuffer)buffers.get(i) );
	  }
	  return sb.toString();
	}
	  
	public void write( OutputStream os ) {
		PrintStream p = new PrintStream(os);
		p.print( getXML() );
	}
	  
	

}
