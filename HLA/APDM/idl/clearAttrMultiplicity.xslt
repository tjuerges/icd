<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<!-- by default, copy everything over -->
	<xsl:template match="*|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- strip off attribute multiplicity info -->
	<xsl:template match="//Foundation.Core.Attribute/Foundation.Core.StructuralFeature.multiplicity">
		<xsl:comment>here xslt removed the multiplicity definition</xsl:comment>
	</xsl:template>

</xsl:stylesheet>
