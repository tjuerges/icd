�DEFINE Root FOR Model�
	�EXPAND SequenceTypedef FOREACH UniqueTelCalAttribute�
�ENDDEFINE�


�DEFINE SequenceTypedef FOR Attribute�
  �IF Type.Name == "String array 1d"�
    typedef sequence <string> �Name�Seq; 
  �ELSEIF Type.Name == "Float array 1d"�
    typedef sequence <float> �Name�Seq; 
  �ELSEIF Type.Name == "Double array 1d"�
    typedef sequence <double> �Name�Seq; 
  �ELSEIF Type.Name == "Integer array 1d"�
    typedef sequence <long> �Name�Seq; 
  �ELSEIF Type.Name == "Boolean array 1d"�
    typedef sequence <boolean> �Name�Seq; 
  �ENDIF�
�ENDDEFINE�
