�DEFINE Root FOR Model�
�FILE "idl/TelCalResults.idl"�
/*
 * ALMA - Atacama Large Millimiter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * All rights reserved
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File TelCalResults.idl
 * 
 */

#ifndef _TELCAL_RESULTS_IDL_
#define _TELCAL_RESULTS_IDL_

#include <baci.idl>
#include <acscomponent.idl>
#include <ControlInterfaces.idl>

#pragma prefix "alma"

/** @file TelCalResults.idl
 *  TelCalResults's IDL File
 *  @version 1.00, 2003
 */

module TelCalResults {
    �EXPAND IDLTypedefs::Root�
    �EXPAND Root FOREACH OwnedElement�
};
#endif // _TELCAL_RESULTS_IDL_
  �ENDFILE�
�ENDDEFINE�

�DEFINE Root FOR ModelElement�
  �REM nada -- we react only to ModelElements of type Class, see method above�
�ENDDEFINE�

�DEFINE Root FOR Package�
  �EXPAND Root FOREACH OwnedElement�
�ENDDEFINE�


�DEFINE Root FOR Class�
//////////////////////////////////////////////////////////////////////////
// �Name� Structure
//////////////////////////////////////////////////////////////////////////

struct �Name�Struct {
  �EXPAND StructMember FOREACH Attribute�
  �IF Name != "CommonResult"�
  CommonResultStruct    CommonRes;
  �ENDIF�
};
�ENDDEFINE�


�DEFINE StructMember FOR Attribute�
  �IF Type.Name == "String"�
    string	�Name�;
  �ELSEIF Type.Name == "Float"�
    float	�Name�;
  �ELSEIF Type.Name == "Integer"� 
    long	�Name�;
  �ELSEIF Type.Name == "Double"� 
    double	�Name�;
  �ELSEIF Type.Name == "String array 1d"�
    �Name�Seq	�Name�; 
  �ELSEIF Type.Name == "Integer array 1d"�
    �Name�Seq	�Name�; 
  �ELSEIF Type.Name == "Boolean array 1d"�
    �Name�Seq	�Name�; 
  �ELSEIF Type.Name == "ExecBlockId"�
    Control::�Type�	�Name�; 
  �ELSEIF Type.Name == "ScanId"�
    Control::�Type�	�Name�; 
  �ELSEIF Type.Name == "String array 1d"�
    �Name�Seq	�Name�; 
  �ELSEIF Type.Name == "Float array 1d"�
    �Name�Seq	�Name�; 
  �ELSEIF Type.Name == "Double array 1d"�
    �Name�Seq	�Name�; 
  �ELSE�
    �Type�	�Name�; 
  �ENDIF�
 �ENDDEFINE�
