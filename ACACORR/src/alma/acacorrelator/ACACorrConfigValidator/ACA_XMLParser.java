/*
ALMA - Atacama Large Millimiter Array
* (c) Nobeyama Radio Observatory - NAOJ, 2008 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  dguo
 * @version $Id: ACA_XMLParser.java,v 1.31 2011/03/31 20:25:14 dguo Exp $
 */

package alma.acacorrelator.ACACorrConfigValidator;

import alma.Correlator.*;

import alma.hla.datamodel.enumeration.*;
import alma.entity.xmlbinding.schedblock.AbstractBaseBandConfigT;
import alma.entity.xmlbinding.schedblock.AbstractCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.types.SpectralSpecTReceiverTypeType;
import alma.entity.xmlbinding.schedblock.AbstractSpectralWindowT;
import alma.entity.xmlbinding.schedblock.ACABaseBandConfigT;
import alma.entity.xmlbinding.schedblock.ACACorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.ACASpectralWindowT;
import alma.entity.xmlbinding.schedblock.ACAPhaseSwitchingConfigurationT;
import alma.entity.xmlbinding.schedblock.SpectralSpecTChoice2;
import alma.entity.xmlbinding.schedblock.AbstractSwitchingCycleT;
import alma.entity.xmlbinding.schedblock.BeamSwitchingCycleT;
import alma.entity.xmlbinding.schedblock.FrequencySwitchingCycleT;
import alma.entity.xmlbinding.schedblock.FrequencySetupT;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SpectralSpecTDescriptor;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.ChannelAverageRegionT;
import alma.ReceiverSidebandMod.ReceiverSideband;
import alma.ReceiverBandMod.ReceiverBand;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.StringReader;

// Following package contains XMLParserBase, it is the base class of this class
import alma.correlatorSrc.CorrConfigValidator.*;


// Parse Spectral spec string to obtain a correlator configuration
public class  ACA_XMLParser extends XMLParserBase
{
    private Logger m_logger;
    private boolean m_beamSwitching;
    private boolean m_frequencySwitching;
    private boolean m_unknownSwitching;
    private String pairSpecification;
    public ACA_XMLParser() {
        m_logger = Logger.getLogger("alma.acacorrelator.ACACorrConfigValidator");
    }
    
    /** Parse a scheduling block return all correlator configurations it contains.
     */
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration[] getCorrelatorConfigurations(SchedBlock schedBlk)
        throws SBConversionException
    {
        int numberSpectSpec = schedBlk.getSchedBlockChoice().getSpectralSpecCount();
        CorrelatorConfiguration[] corrConfsIDL = new CorrelatorConfiguration[numberSpectSpec];
        for (int i = 0; i < numberSpectSpec; i++)
            {
                SpectralSpecT spectralSpec = schedBlk.getSchedBlockChoice().getSpectralSpec(i);
                try
                    {
                        corrConfsIDL[i] = getOneCorrelatorConfiguration(spectralSpec);
                    }
                catch (SBConversionException ex)
                    {
                        throw ex;
                    }
            }
        return corrConfsIDL;
    }
    
    // translateSpectralSpecXml to CorrConfigIdl
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXML )
        throws SBConversionException
    {
        CorrelatorConfiguration corrConfigIDL = null;
        SpectralSpecT spectralSpec = null;
        String extracted = null;
        int firstSpectralSpec = 0;
        int secondSpectralSpec = 0;
        
        // Remove space before > and after <
        while(spectralSpecXML.contains("< ") ) {
            spectralSpecXML = spectralSpecXML.replaceAll("< ", "<");
        }
        while(spectralSpecXML.contains(" >") ) {
            spectralSpecXML = spectralSpecXML.replaceAll(" >", ">");
        }
        
        // Get SpectralSpec element
        // First index for substring
        if(spectralSpecXML.contains("<SpectralSpec")){
            firstSpectralSpec = spectralSpecXML.indexOf("<SpectralSpec"); 
        } 
        else if(spectralSpecXML.contains("<sbl:SpectralSpec")){
            firstSpectralSpec = spectralSpecXML.indexOf("<sbl:SpectralSpec"); 
        }
        else {
            throw new SBConversionException("No SpectralSpec element found");
        }
        
        // Second index for substring
        if(spectralSpecXML.contains("SpectralSpec>")) {
            secondSpectralSpec = spectralSpecXML.indexOf("SpectralSpec>") + 13;}
        else if(spectralSpecXML.contains("SpectralSpecT>")) {
            secondSpectralSpec = spectralSpecXML.indexOf("SpectralSpecT>") + 14;
        } 

	if (firstSpectralSpec < 0 || secondSpectralSpec < 0)
            {
                throw new SBConversionException("No SpectralSpecT element found");
            }

        // Check which switching cycle included
        if(spectralSpecXML.contains("NO_SWITCHING") ) {
            m_unknownSwitching = true;
        }
        else if(spectralSpecXML.contains("FREQUENCY_SWITCHING") ) {
            m_frequencySwitching = true;
        }
        else {
            m_beamSwitching = true;
        }
        
        // extract spectralSpec string from schedule block string
        extracted = spectralSpecXML.substring(firstSpectralSpec, 
                                              secondSpectralSpec);
        //        System.out.println(extracted);

        try
            {
                spectralSpec = SpectralSpecT.unmarshalSpectralSpecT(new StringReader(extracted));
            }
        catch( org.exolab.castor.xml.MarshalException mex)
            {
                throw new SBConversionException("Exception translating Spectral Spec castor unmarshalling: "+
                                                mex.toString());
            }
        catch( org.exolab.castor.xml.ValidationException vex)
            {
                throw new SBConversionException("Exception translating Spectral Spec castor validation: "+
                                                vex.toString());
            }
        
        
        // Validate against schema
        try
            {
                spectralSpec.validate();
            }
        catch (org.exolab.castor.xml.ValidationException ve)
            {
                throw new SBConversionException("Invalid SpectralSpec : "+ ve.getMessage());
            } 
        
        try
            {
                corrConfigIDL = getOneCorrelatorConfiguration(spectralSpec);
            }
        catch (SBConversionException sbcex)
            {
                throw sbcex;
            }
        catch (java.lang.NullPointerException npe)
            {
                throw new SBConversionException("Error parsing spectral spec: "+npe.toString());
            }	      
        
        m_logger.info(" ACA_XMLParser:: translateSpectralSpecXml2CorrConfigIdl function returns a CorrConfigIDL ! "); 
        return corrConfigIDL;
    }
    
    /** Return a single correlator configuration for a given spectral spect entity
     */
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration getOneCorrelatorConfiguration(SpectralSpecT spectralSpec)
    throws SBConversionException
    {
        m_logger.info(" ACA_XMLParser:: Enter getOneCorrelatorConfiguration()"); 
        ACACorrelatorConfigurationT corrConf = spectralSpec.getSpectralSpecTChoice().getACACorrelatorConfiguration();
        m_logger.info(" ACA_XMLParser:: getACACorrelatorConfiguration() returns "); 
        if( corrConf == null )
            throw new SBConversionException("No ACA correlator configuration found");

        CorrelatorConfiguration corrConfIDL = new CorrelatorConfiguration();
        String unitsStr;
        double timeUnits;
        double freqUnits;

        // Get LO1 frequency
        unitsStr = spectralSpec.getFrequencySetup().getLO1Frequency().getUnit();
        freqUnits = getFrequencyUnits(unitsStr);
        corrConfIDL.LO1FrequencyMHz = spectralSpec.getFrequencySetup().getLO1Frequency().getContent() * freqUnits;
        m_logger.info(" ACA_XMLParser:: getLO1Frequency() returns "); 
        
        // Set the durations
        unitsStr = corrConf.getIntegrationDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.integrationDuration = (long)(corrConf.getIntegrationDuration().getContent()*timeUnits);

        m_logger.info(" ACA_XMLParser:: getIntegrationDuration() returns "); 

        unitsStr = corrConf.getChannelAverageDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.channelAverageDuration = (long)(corrConf.getChannelAverageDuration().getContent()*timeUnits);
        m_logger.info(" ACA_XMLParser:: getChannelAverageDuration() returns "); 
        /*
        unitsStr = spectralSpec.getSubScanDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.subScanDuration = (long)(spectralSpec.getSubScanDuration().getContent()*timeUnits);
        */
        
        // See email 9/15/2010 
        corrConfIDL.subScanDurationACA = (long)0;

        //BL specific
        // Assign 0 for baseline 
        corrConfIDL.dumpDuration = (long)(0);

        SpectralSpecTReceiverTypeType rxType = spectralSpec.getReceiverType();
        if (rxType == SpectralSpecTReceiverTypeType.NOSB) 
            {
                corrConfIDL.receiverType = ReceiverSideband.NOSB;
                pairSpecification = "no";
            }
        else if (rxType == SpectralSpecTReceiverTypeType.DSB) 
            {
                corrConfIDL.receiverType = ReceiverSideband.DSB;
                pairSpecification = "yes";
            }
        else if (rxType == SpectralSpecTReceiverTypeType.TSB)
            {
                corrConfIDL.receiverType = ReceiverSideband.TSB;
                pairSpecification = "no";
            }
        else if (rxType == SpectralSpecTReceiverTypeType.SSB)
            {
                corrConfIDL.receiverType = ReceiverSideband.SSB;
                pairSpecification = "no";
            }
        else
            {
                throw new SBConversionException("Reciever type " + rxType + " is not recognized");
            }
        
        
	// Get and Set the receiver band
        String RecvBandStr;
	RecvBandStr = spectralSpec.getFrequencySetup().getReceiverBand().toString();

        m_logger.info(" ACA_XMLParser:: getReceiverBand() returns "); 
	try
            {
                corrConfIDL.receiverBand = JReceiverBand.newReceiverBand(RecvBandStr);
            }
	catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Receiver band");
            }
        


        // Get the APC info
        if(corrConf.getAPCDataSets().toString().equals("AP_UNCORRECTED")) {
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
        }
        else if(corrConf.getAPCDataSets().toString().equals("AP_CORRECTED")) {
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];         
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;
        }
        else if(corrConf.getAPCDataSets().toString().equals("AP_CORRECTED+AP_UNCORRECTED")) {
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[2];
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;
            corrConfIDL.APCDataSets[1] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
        }
        else {
            throw new SBConversionException("APCDataSets are not recognized");
        }
        
        // Get and set ACAPhaseSwitchingConfiguration 
        ACAPhaseSwitchingConfigurationT acaPSConf = corrConf.getACAPhaseSwitchingConfiguration();
        boolean doD180modulation = acaPSConf.getDoD180modulation();
        boolean doD180demodulation = acaPSConf.getDoD180demodulation();
        unitsStr = acaPSConf.getD180Duration().getUnit();
        double units = getTimeUnits(unitsStr);
        long d180Duration = (long)(acaPSConf.getD180Duration().getContent()* units);
        
        corrConfIDL.ACAPhaseSwConfig = new alma.Correlator.ACAPhaseSwitchingConfigurations(doD180modulation, doD180demodulation, d180Duration);
        m_logger.info(" ACA_XMLParser:: call getBaseBandConfigs() "); 
        // Parse all of the baseband entities
        try
            {
                corrConfIDL.baseBands = getBaseBandConfigs(corrConf, spectralSpec);
            }
        catch (SBConversionException ex)
            {
                throw ex;
            }
        m_logger.info(" ACA_XMLParser:: getOneCorrelatorConfiguration returns one CorrConfigIDL ---! "); 
        return corrConfIDL;
    }
    
    /**
     * Get the base band configuration IDL structures from the APDM correlator configuration.
     * 
     * This is a utility function to aid in the extraction of the CorrelatorConfiguration
     * IDL structure from the APDM.
     * 
     * @param corrConf Correlator configuration, part of the APDM
     * @return Baseband configurations, part of the CorrelatorConfiguration structure
     * @throws SBConversionException
     * @see SchedBlock.xsd
     * @see CorrConfigIDL.idl
     */
    //---------------------------------------------------------------------------------
    protected BaseBandConfig[] getBaseBandConfigs(AbstractCorrelatorConfigurationT aCorrConf, SpectralSpecT spectralSpec)
    throws SBConversionException
    {
        FrequencySetupT frequencySetup = spectralSpec.getFrequencySetup();
        // Cast the abstract config to a concrete BL correlator configuration
        ACACorrelatorConfigurationT corrConf = (ACACorrelatorConfigurationT)aCorrConf;

        // Create baseband configs IDL objects for each baseband in the configuration
        int numBBConfs = corrConf.getACABaseBandConfigCount();
        boolean enable180DWF = corrConf.getEnable180DegreeWalshFunction();
        boolean enable90DWF = corrConf.getEnable90DegreeWalshFunction();
        String loOffsettingMode = corrConf.getLOOffsettingMode().toString();
        BaseBandConfig[] baseBandConfsIDL = new BaseBandConfig[numBBConfs];
        String str;

        for (int bb = 0; bb < numBBConfs; bb++)
        {
            alma.entity.xmlbinding.schedblock.ACABaseBandConfigT bbConfig = corrConf.getACABaseBandConfig(bb);

            alma.entity.xmlbinding.schedblock.BaseBandSpecificationT bbSpec = frequencySetup.getBaseBandSpecification(bb);

            baseBandConfsIDL[bb] = new BaseBandConfig();

            // Set the baseband name
            str = bbSpec.getBaseBandName().toString();
            try
            {
                baseBandConfsIDL[bb].basebandName = JBasebandName.newBasebandName(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Baseband name");
            }
            // Set the CAM - TODO: for now leave it at the baseband config level but in future needs to move to CorrConf level
            str = corrConf.getCAM().toString();
            m_logger.info(" ACA_XMLParser:: corrConf.getCAM() ---! "); 
            try
            {
                baseBandConfsIDL[bb].CAM = JAccumMode.newAccumMode(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("accumulation mode");
            }
            // Set the Data products
            str = bbConfig.getDataProducts().toString();
            try
            {
                baseBandConfsIDL[bb].dataProducts = JCorrelationMode.newCorrelationMode(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("correlation mode");
            }
            m_logger.info(" ACA_XMLParser:: getSpectralWindows ---! "); 
            // parse spectral windows in baseband & assign to IDL element
            try
            {
                baseBandConfsIDL[bb].spectralWindows = getSpectralWindows(bbConfig);
            }
            catch (SBConversionException ex)
            {
                throw ex;
            }
            
            // Set the sideband separation mode
            if(pairSpecification.equals("no")) {
                if(enable90DWF == false && loOffsettingMode.equals("NONE"))
                    {
                        str = "NONE";
                    }
                
                else if(enable90DWF == false && !loOffsettingMode.equals("NONE") )
                    {
                        str = "FREQUENCY_OFFSET_REJECTION";
                    }
            }
            else if(pairSpecification.equals("yes")) {
                if(enable90DWF == false && loOffsettingMode.equals("NONE"))
                    {
                        str = "PHASE_SWITCH_SEPARATION";
                    }
                else {
                    throw new SBConversionException("Sideband separation setting");
                }
            }
            else {
                throw new SBConversionException("Sideband separation setting");
            }
            m_logger.info(" ACA_XMLParser:: Set the sideband separation mode  ---! "); 
            
            try  {
                baseBandConfsIDL[bb].sideBandSeparationMode = JSidebandProcessingMode.newSidebandProcessingMode(str);
            }
            catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("sideband processing");
                }

            // get boolean UseUSB          
            baseBandConfsIDL[bb].useUSB = bbSpec.getUseUSB();
            // get boolean use12GHzFilter
            baseBandConfsIDL[bb].use12GHzFilter = bbSpec. getUse12GHzFilter();

            // get LO2 frequency
            baseBandConfsIDL[bb].LO2FrequencyMHz =
                bbSpec.getLO2Frequency().getContent() *
                getFrequencyUnits(bbSpec.getLO2Frequency().getUnit());

            // Need converter function apdmToAsdmSwitchingType
            str = spectralSpec.getSwitchingType().toString();
            m_logger.info(" ACA_XMLParser:: spectralSpec.getSwitchingType called ---! "); 
            
            BinSwitching_t binSwitching = new BinSwitching_t();
            try
                {
                    binSwitching.SwitchingType = JSwitchingMode.newSwitchingMode(str);
                }
            catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("bin switching");
                }
            
            AbstractSwitchingCycleT acaSC;
            if(m_beamSwitching) {
                acaSC = spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle();
                System.out.println(" ACA_XMLParser:: getBeamSwitchingCycle called ---! "); 
                int numPos = acaSC.getNumberOfPositions();
                binSwitching.numberOfPositions = numPos;                
                binSwitching.dwellTime = new long[numPos];
                binSwitching.deadTime = new long[numPos];
                for ( int nps = 0; nps < numPos; nps++) {
                    alma.entity.xmlbinding.valuetypes.TimeT dwellTime =  spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle().getBeamSwitchingState(nps).getDwellTime();
                    alma.entity.xmlbinding.valuetypes.TimeT deadTime =  spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle().getBeamSwitchingState(nps).getTransition();
                    String dwellUnit = dwellTime.getUnit();
                    double dwellTimeFactor = getTimeUnits(dwellUnit);
                    binSwitching.dwellTime[nps] = (long)(dwellTime.getContent()* dwellTimeFactor );
                    String deadUnit = deadTime.getUnit();
                    double deadTimeFactor = getTimeUnits(deadUnit);
                    binSwitching.deadTime[nps] = (long)(deadTime.getContent()* deadTimeFactor );
                }
            }
            else if(m_frequencySwitching) {
                acaSC = spectralSpec.getSpectralSpecTChoice2().getFrequencySwitchingCycle();
                System.out.println(" ACA_XMLParser:: getFrequencySwitchingCycle called ---! "); 
                int numPos = acaSC.getNumberOfPositions();
                binSwitching.numberOfPositions = numPos;  
                binSwitching.dwellTime = new long[numPos];
                binSwitching.deadTime = new long[numPos];
                
                for ( int nps = 0; nps < numPos; nps++) {
                    alma.entity.xmlbinding.valuetypes.TimeT dwellTime =  spectralSpec.getSpectralSpecTChoice2().getFrequencySwitchingCycle().getFrequencySwitchingState(nps).getDwellTime();        
                    String dwellUnit = dwellTime.getUnit();
                    double dwellTimeFactor = getTimeUnits(dwellUnit);
                    binSwitching.dwellTime[nps] = (long)(dwellTime.getContent()* dwellTimeFactor );
                    binSwitching.deadTime[nps] = (long)(0);
                }
            }
            else {
                acaSC = null;
                binSwitching.numberOfPositions = 1;
                binSwitching.dwellTime = new long[1];
                binSwitching.deadTime = new long[1];
            }

            baseBandConfsIDL[bb].binSwitchingMode = binSwitching;

            m_logger.info(" --- get bin switching Mode -"); 
            
            // ACA Specific
            // No input in xml, temp solution
            baseBandConfsIDL[bb].polarizationMode = alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD;
            m_logger.info(" --- get polarizationMode  -"); 
            // Get centerFreqOfResidualDelayMHz
            str = bbConfig.getCenterFreqOfResidualDelay().getUnit();
            double units = getFrequencyUnits(str);
            baseBandConfsIDL[bb].centerFreqOfResidualDelayMHz = bbConfig.getCenterFreqOfResidualDelay().getContent()* units;
            // Convert to adapt to ACACORR current implementation 
            baseBandConfsIDL[bb].centerFreqOfResidualDelayMHz 
                = 4000 - baseBandConfsIDL[bb].centerFreqOfResidualDelayMHz; 
        }
        m_logger.info(" --- get baseBandConfs IDL  -"); 
        return baseBandConfsIDL;
    }
    
    /**
     * Get the spectral window IDL structures from the APDM baseband configuration.
     * 
     * @param baseBandConf Baseband configuration, part of the APDM
     * @return The spectral windows, part of the BLCorrelatorConfiguration structure
     * @throws SBConversionException
     * @see SchedBlock.xsd
     * @see CorrConfigIDL.idl
     */
    //---------------------------------------------------------------------------------
    protected SpectralWindow[] getSpectralWindows(AbstractBaseBandConfigT aBaseBandConf)
        throws SBConversionException
    {
        // Cast abstract baseband config to concrete ACA correlator bb config.
        ACABaseBandConfigT baseBandConf = (ACABaseBandConfigT)aBaseBandConf;
        
        // Parse up each spectral window in the baseband
        int numSpecWindows = baseBandConf.getACASpectralWindowCount();
        String str;
        SpectralWindow[] spectWindowsIDL = new alma.Correlator.SpectralWindow[numSpecWindows];
        //  bandwidth & frequency can be in units of GHz or MHz, must set correct scale
        double units;

        for (int ssbs = 0; ssbs < numSpecWindows; ssbs++)
        {
            // get the specific spectral window
            ACASpectralWindowT spectralWindow = baseBandConf.getACASpectralWindow(ssbs);

            // Construct an IDL version whose individual parameters are populated
            spectWindowsIDL[ssbs] = new alma.Correlator.SpectralWindow();

            // Scale the center frequency from GHz to MHz based on the units
            str =spectralWindow.getCenterFrequency().getUnit();
            // units is a scale factor, 1 for MHz input, 1000 for GHz.
            units = getFrequencyUnits(str);
            spectWindowsIDL[ssbs].centerFrequencyMHz = 4000 - spectralWindow.getCenterFrequency().getContent() * units - (double)2000/(double)1048576;
            // Scale the effective bandwidth from GHz to MHz based on the units
            str = spectralWindow.getEffectiveBandwidth().getUnit();
            units = getFrequencyUnits(str);
            spectWindowsIDL[ssbs].effectiveBandwidthMHz = spectralWindow.getEffectiveBandwidth().getContent() * units;

            spectWindowsIDL[ssbs].effectiveNumberOfChannels= spectralWindow.getEffectiveNumberOfChannels();
            spectWindowsIDL[ssbs].spectralAveragingFactor = spectralWindow.getSpectralAveragingFactor();

            // Set sideband info
            str = spectralWindow.getSideBand().toString();
            try
            {
                spectWindowsIDL[ssbs].sideBand = JNetSideband.newNetSideband(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("net sideband mode");
            }

            spectWindowsIDL[ssbs].associatedSpectralWindowNumberInPair = spectralWindow.getAssociatedSpectralWindowNumberInPair();
            spectWindowsIDL[ssbs].useThisSpectralWindow = spectralWindow.getUseThisSpectralWindow();

            // Set windowing function
            str = spectralWindow.getWindowFunction().toString();
            try
            {
                spectWindowsIDL[ssbs].windowFunction = JWindowFunction.newWindowFunction(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Window function");
            }

            // ACA specific
            spectWindowsIDL[ssbs].frqChProfReproduction = spectralWindow.getFrqChProfReproduction();

            // Set correlation bits info
            // BL specific, No entry in xml, set a value
            str = "BITS_2x2";
            try
                {
                    spectWindowsIDL[ssbs].correlationBits = JCorrelationBit.newCorrelationBit(str);
                    
                }
            catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("Correlation bit");
                }
            
            // BL specific
            //            spectWindowsIDL[ssbs].correlationNyquistOversampling = spectralWindow.getCorrelationNyquistOversampling();
            spectWindowsIDL[ssbs].correlationNyquistOversampling = false;

            // Need to build sequence here of poln products. 
            // Configuration has XX,YY,XY,YX must convert them to 4 different 
            str = spectralWindow.getPolnProducts().toString();
            // str = "XX";
            String patternStr = ",";
            String []polnProds = str.split(patternStr, -1);
            spectWindowsIDL[ssbs].polnProductsSeq = new alma.StokesParameterMod.StokesParameter[polnProds.length];

            for( int i = 0; i < polnProds.length; i++ ) {
                try {
                    spectWindowsIDL[ssbs].polnProductsSeq[i] = JStokesParameter.newStokesParameter(polnProds[i]);
                }
                catch (AcsJBadParameterEx ex)
                    {
                        throw new SBConversionException("poln products");
                    }
            }
            //            spectWindowsIDL[ssbs].quantizationCorrection = spectralWindow.getQuantizationCorrection();
            spectWindowsIDL[ssbs].quantizationCorrection = true;
            // Assign channel average regions
            try
                {
                    spectWindowsIDL[ssbs].channelAverageRegions = getChannelAvgRegions(spectralWindow);
                }
            catch (SBConversionException ex)
                {
                    throw ex;
                }
        }
        return spectWindowsIDL;
    }
    
    /**
     * Get the channel average region IDL structures from the APDM base band configuration.
     * @param baseBandConf Baseband configuration, part of the APDM
     * @return The channel average regions, part of the CorrelatorConfiguration structure
     * @throws SBConversionException
     * @see CorrConfigIDL.idl
     * @see SchedBlock.xsd
     */
    //----------------------------------------------------------------------
    protected ChannelAverageRegion[] getChannelAvgRegions(AbstractSpectralWindowT aSpecWindow)
    throws SBConversionException
    {
        ACASpectralWindowT specWindow = (ACASpectralWindowT)aSpecWindow;
        int numChannAvgRegions = specWindow.getChannelAverageRegionCount();
        ChannelAverageRegion[] channelAvgRegionsIDL = new ChannelAverageRegion[numChannAvgRegions];
        for (int cab = 0; cab < numChannAvgRegions; cab++)
            {
                ChannelAverageRegionT car = specWindow.getChannelAverageRegion(cab);
                
                channelAvgRegionsIDL[cab] = new ChannelAverageRegion();
                channelAvgRegionsIDL[cab].startChannel = car.getStartChannel();
                channelAvgRegionsIDL[cab].numberChannels = car.getNumberChannels();
            }
        return channelAvgRegionsIDL;
    }
    
    //
    private double getTimeUnits(String unitsStr)
    {
        double timeUnits = 1.0;
        unitsStr = unitsStr.toLowerCase();
        if( unitsStr.equals("sec") || unitsStr.equals("s") )
            timeUnits = 1.0e7;
        else if( unitsStr.equals("msec") || unitsStr.equals("ms") )
            timeUnits = 1.0e4;
        else if( unitsStr.equals("usec") || unitsStr.equals("us") )
            timeUnits = 1.0e1;
        return timeUnits;
    }
    
    /** Simple converter which parses units string to a units mulitiplicative factor (to ACS units)
     */
    //---------------------------------------------------------------------------------
    private double getFrequencyUnits(String unitsStr)
    {
        double frequencyUnits = 1.0;
        unitsStr = unitsStr.toLowerCase();
        if( unitsStr.equals("mhz") )
            frequencyUnits = 1.0;
        else if( unitsStr.equals("ghz") )
            frequencyUnits = 1000.0;
        else if( unitsStr.equals("hz") )
            frequencyUnits = 0.001;
        else {
            return 0;
        }
        return frequencyUnits;
    }
}
