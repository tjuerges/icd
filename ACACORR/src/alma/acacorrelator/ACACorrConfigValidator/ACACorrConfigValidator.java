/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */


package alma.acacorrelator.ACACorrConfigValidator;

//Java stuff
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;
import java.lang.Math;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.StringReader;

//Correlator stuff
import alma.Correlator.CorrelatorConfiguration;
import alma.ReceiverSidebandMod.ReceiverSideband;
import alma.AccumModeMod.AccumMode;
import alma.CorrelationModeMod.CorrelationMode;
import alma.AtmPhaseCorrectionMod.AtmPhaseCorrection;
import alma.WindowFunctionMod.WindowFunction;
import alma.BasebandNameMod.BasebandName;
import alma.SwitchingModeMod.SwitchingMode;
import alma.ACAPolarizationMod.ACAPolarization;

import alma.Correlator.CORRELATOR_12m_4QUADRANT;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_2ANTS;

import alma.entity.xmlbinding.schedblock.SchedBlock;

//IDLEntityRef
import alma.asdmIDLTypes.*;
import alma.hla.datamodel.enumeration.*;

// XMLParserBase SBConversionException location
import alma.correlatorSrc.CorrConfigValidator.*;

import alma.ACS.stringSeqHolder;

/** This class validates correlator configurations as IDL structures
 ** in ICD/CORR/idl/CorrConfig.idl. Configurations can come in the form of XML document SBs and
 ** are passed in as strings which the class ACA_XMLParser can parse and return CorrelatorConfiguration IDL structures.
 ** See validator work in "Correlator Configuration Validation and Translation to IDL": 
 ** http://almasw.hq.eso.org/almasw/pub/CORR/CorrConfigXMLVsIDL/CorrConfigXML_IDL.pdf.
 ** Documentation regarding correct values for CorrelatorConfiguration parameters can be found in the IDL file. 
 ** As HLA/Enumerations are widely used, they also provide parameter limits.
 ** 
 */
public class ACACorrConfigValidator 
{
    /////////////////////////////////////
    // Implementation of ConfigValidator
    /////////////////////////////////////
    
    private Logger m_logger;
    //private CorrConfigModeClass m_modesTable;
    Vector m_errorVector;
    private int m_outputDataSize;
    private long m_integrationDuration;
    private long m_channelAverageDuration;
    private ReceiverSideband m_receiverType;
    private int m_totalChanAverRegionsPerBaseband;

    // data rate calculation
    private int m_NumberPolns;
    private int m_numOfAntennas;
    private int m_numEffectChannels;
    private int m_numChanAveRegions;
    private int m_numAPCDataSets;
    private int m_dataProducts;

    // type of correlator in use
    private String m_correlatorType;
    // maximum values for a given correlator/software implementation
    private int m_maxNumSpectralWindows;
    private int m_maxNumAntennas;
    private int m_maxNumBasebands;
    private int m_maxNumChanAverRegions;
    private int m_maxNumAPCDatasets;
    private int m_numBins;
    private boolean m_sideBandSeparationMode;

    //-------------------------------------------------------------------------
    public ACACorrConfigValidator()
    {
        m_correlatorType = new String("ACACORRELATOR");
        m_logger = Logger.getLogger("alma.acacorrelator.ACACorrConfigValidator");
        setCorrelatorLimits();
    }
    
    //-------------------------------------------------------------------------
    public ACACorrConfigValidator(String correlatorType)
    {
        // If no correlator type passed in, default to 1-quadrant
        if( correlatorType.length() == 0 )
            m_correlatorType = new String("ACACORRELATOR");
        else
            m_correlatorType = correlatorType;
        
        m_logger = Logger.getLogger("alma.acacorrelator.ACACorrConfigValidator");
        setCorrelatorLimits();
    }

    //-------------------------------------------------------------------------
    public ACACorrConfigValidator(com.cosylab.CDB.DAL dal)
    {
        m_correlatorType = new String("ACACORRELATOR");
        m_logger = Logger.getLogger("alma.acacorrelator.ACACorrConfigValidator");
        setCorrelatorLimits();        
    }
    
    //-------------------------------------------------------------------------
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXML )
    {
        ACA_XMLParser acaXMLParser = new ACA_XMLParser();
        try
            {
                m_logger.info(" ACACorrConfigValidator:: calling XML parser translateSpectralSpecXml2CorrConfigIdl  -----"); 
                CorrelatorConfiguration corrConfigIDL = acaXMLParser.translateSpectralSpecXml2CorrConfigIdl(spectralSpecXML);
                m_logger.info(" ACACorrConfigValidator:: translateSpectralSpecXml2CorrConfigIdl called -----"); 
                return corrConfigIDL;
            }
        catch (SBConversionException sbcex)
            {
                m_errorVector.addElement("translateSpectralSpecXml2CorrConfigIdl function exception : Invalid spectral Spec: "+sbcex.toString());
                return null;
            }
        catch (Exception ex)
            {
                m_errorVector.addElement("Invalid spectral Spec: "+ex.toString());
                return null;
            }
    }
    
    /** Validate the correlator configuration in an SpectralSpec XML string. The validation
     ** is two-step: 1) Translate from XML to IDL, 2) validate the IDL
     ** @param spectralSpec SS XML string that contains a correlator configuration to validate
     ** @param numOfAntennas The number of antennas is important to determine the data rate.
     ** @param errorStringSeq A sequence of strings which hold errors encountered in the
     **	validation. Note that there can be multiple problems with the IDL and they are all
     **	added to this sequence of strings.
     ** @return True if the configuration is valid, else false. If false, the reasons are
     **	in errorStringSeq.
     */
    //-------------------------------------------------------------------------
    public boolean validateConfigurationSS(String spectralSpec, 
                                           int numOfAntennas,
                                           alma.ACS.stringSeqHolder errorStringSeq)
    {
        m_errorVector = new Vector();
        // Get the correlator configurations in the spectralSpec and convert 
        // them to IDL structures
        CorrelatorConfiguration corrConfig = translateSpectralSpecXml2CorrConfigIdl(spectralSpec);
        if( corrConfig == null )
            {
                copyErrorStrings(errorStringSeq);
                return false;
            }
        return validateConfiguration(corrConfig, numOfAntennas, errorStringSeq);
    }
    
    /** Validate the correlator configuration in a CorrelatorConfiguration IDL
     ** The code is written such that checks are made on parameters in the same order as
     ** they appear in CorrConfig.idl.
     ** @param corrConfig Correlator configuration IDL structure to validate
     ** @param numOfAntennas The number of antennas is important to determine the data rate.
     ** @param errorStringSeq A sequence of strings which hold errors encountered in the
     **	validation. Note that there can be multiple problems with the IDL and they are all
     **	added to this sequence of strings.
     ** @return True if the configuration is valid, else false. If false, the reasons are
     **	in errorStringSeq.
     */
    // Component implementation calls this function
    //-------------------------------------------------------------------------
    public boolean validateConfiguration(CorrelatorConfiguration corrConfig,
                                         int numOfAntennas, alma.ACS.stringSeqHolder errorStringSeq)
    {
        m_logger.info("ACACorrConfigValidator: Validate the Configuration IDL struct");
        
        m_errorVector = new Vector();
        boolean isValidConfig = true;
        
        // Preserve durations for various checks including data rates
        m_integrationDuration = corrConfig.integrationDuration; 
        m_receiverType = corrConfig.receiverType;
        m_outputDataSize = 0;
        m_channelAverageDuration = corrConfig.channelAverageDuration; 

        double lo1FrequencyMHz = corrConfig.LO1FrequencyMHz;
        if( lo1FrequencyMHz < 20000 || lo1FrequencyMHz > 1000000 ) {
            m_errorVector.addElement("LO1 Frequency has value: " + lo1FrequencyMHz + " It is out of range[20GHz-1THz]" );
            isValidConfig = false;
        }

        
        // check timing parameters
        // subscan duration' is an integral multiple of 'integration duration
        if( corrConfig.subScanDurationACA / m_integrationDuration != 0) {
            m_errorVector.addElement("ACA SubScan Duration "+ corrConfig.subScanDurationACA + " is not an integral multiple of the integration duration: " +
                                     m_integrationDuration);
            isValidConfig = false;
        }
        
        // subscan duration is an integral multiple of 48ms.
        if( corrConfig.subScanDurationACA / 480000 != 0) {
            m_errorVector.addElement("ACA SubScan Duration "+ corrConfig.subScanDurationACA + " is not the  multiple of 48ms ");
            isValidConfig = false;
        }
        
        // subscan duration is equal to 384ms or more.
        if( corrConfig.subScanDurationACA < 3840000 && corrConfig.subScanDurationACA != 0) {
            m_errorVector.addElement("ACA SubScan Duration "+ corrConfig.subScanDurationACA + " is less than 384 ms. ");
            isValidConfig = false;
        }

        // 1ms
        if( m_integrationDuration < 10000 ) {
            m_errorVector.addElement("Integration Duration less than "+m_integrationDuration + " is not allowed" );
            isValidConfig = false;
        }
        
        if( m_integrationDuration > (long)650000000 ) {
            
            m_errorVector.addElement("Integration Duration is larger than current allowed value: "+m_integrationDuration);
            isValidConfig = false;
        }
        
        // integrationDuration less than 1.008 second
        if( m_integrationDuration <= (long)10080000 ) {
            if(m_channelAverageDuration != m_integrationDuration) {
                m_errorVector.addElement(" channelAverageDuration is imcompatible with Integration Duration : "+m_integrationDuration);
                isValidConfig = false;
            }
        }
                
        // check channel average durations
        //
        if( m_channelAverageDuration > 0 ) {
            // chan aver duration must be less than integrations
            if( m_channelAverageDuration > m_integrationDuration )
                {
                    m_errorVector.addElement("Channel average duration "+ m_channelAverageDuration + 
                                             " is greater than the integration duration "+
                                             m_integrationDuration);
                    isValidConfig = false;
                }
            // integration duration must be a multiple of sub-integrations
            if( (m_integrationDuration % m_channelAverageDuration) != 0 )
                {
                    m_errorVector.addElement("Integration duration "+ m_integrationDuration +
                                             " is not an even multiple of the channel average duration: " +
                                             m_channelAverageDuration);
                    isValidConfig = false;
                }
        }
        
        if( m_channelAverageDuration > 10080000 ) {
            
            m_errorVector.addElement("Channel average duration "+ m_channelAverageDuration + 
                                     " 100ns is greater than 1.008 sec ");
            isValidConfig = false;
        }
        
        // data products should be same at each base band
        for ( int i = 0; i < corrConfig.baseBands.length; i++ ) {
            for ( int j = i + 1; j < corrConfig.baseBands.length; j++ )
                {
                    m_logger.info("ACACorrConfigValidator: Validate data products consistence on basebands");
                    if ( corrConfig.baseBands[i].dataProducts != corrConfig.baseBands[j].dataProducts )
                        {
                            m_errorVector.addElement("CorrConfig baseband " + corrConfig.baseBands[i].basebandName.toString() + " and " + corrConfig.baseBands[j].basebandName.toString() + " have different data products " );
                            
                            copyErrorStrings(errorStringSeq);
                            
                            return false;
                        }
                }
        }
        //
        // Check that APC has correct combinations
        //
        if( (corrConfig.APCDataSets.length < 1) || (corrConfig.APCDataSets.length > m_maxNumAPCDatasets) )
            {
            m_errorVector.addElement("CorrConfig " + ": Invalid number of APCDataSets: " + 
                                     corrConfig.APCDataSets.length + " valid range: 1 - "+m_maxNumAPCDatasets);
            isValidConfig = false;
        }
        else {
            //
            // Check for duplicates & invalid combinations
            //
            if( corrConfig.APCDataSets.length == 2 ) {
                AtmPhaseCorrection atm0 = corrConfig.APCDataSets[0];
                AtmPhaseCorrection atm1 = corrConfig.APCDataSets[1];
                if ( atm0 == atm1 )
                    {
                        m_errorVector.addElement("CorrConfig " + ": Duplicate APC Data type: " + atm0.toString());
                        isValidConfig = false;
                    }
                m_numAPCDataSets = 2;
            }
            else {
                m_numAPCDataSets = 1;
            }
        }
        
        //  <ACA correlator specific>
        if( (corrConfig.ACAPhaseSwConfig.doD180modulation) && !(corrConfig.ACAPhaseSwConfig.doD180demodulation) )
            {
                m_errorVector.addElement("CorrConfig " + ": ACAPhaseSwitchingConfigurations: " + " doD180demodulation conflicts with doD180modulation ");
                isValidConfig = false;
            }
        // d180Duration msec
        if( corrConfig.ACAPhaseSwConfig.d180Duration == 2500 ||
            corrConfig.ACAPhaseSwConfig.d180Duration == 5000 ) {}
        else {
            m_errorVector.addElement("CorrConfig " + ": ACAPhaseSwitchingConfigurations: " + " d180Duration " + corrConfig.ACAPhaseSwConfig.d180Duration + " 100ns incorrect ! ACA Only accepts 250 us and 500 us ");
            isValidConfig = false;
        }
        
        
        // make sure that the number of basebands is valid. If not, don't bother checking any more
        if( (corrConfig.baseBands.length < 1) || 
            (corrConfig.baseBands.length > m_maxNumBasebands) )
            {
                m_errorVector.addElement("CorrConfig Baseband count " + 
                                         corrConfig.baseBands.length + " is invalid. Valid range: 1 - "+m_maxNumBasebands);
                copyErrorStrings(errorStringSeq);
                return false;
            }
        
        m_numOfAntennas = numOfAntennas;
        //
        // check each baseband
        // 
        int totalNumberSpectralWindows = 0;
        for(int bbj = 0; bbj < corrConfig.baseBands.length; bbj++ ) 
            {
                totalNumberSpectralWindows += corrConfig.baseBands[bbj].spectralWindows.length;
                if ( !validateBaseband(corrConfig.baseBands[bbj]) )
                    {
                        isValidConfig = false;
                    }
                
                if ( !checkDataRate(corrConfig, m_numOfAntennas, corrConfig.baseBands[bbj].basebandName ))
                    {
                        isValidConfig = false;
                    }
            }
        
        // Must make sure that the total number of spectral windows across all basebands
        // doesn't exceed the maximum
        if( (totalNumberSpectralWindows < 1) || 
            (totalNumberSpectralWindows > (m_maxNumSpectralWindows * m_maxNumBasebands)) )
            {
                m_errorVector.addElement("CorrConfig Spectral window count across all basebands: " + totalNumberSpectralWindows + " is invalid: range: 1 - "+(m_maxNumSpectralWindows * m_maxNumBasebands));
                copyErrorStrings(errorStringSeq);
                return false;
            }
        
        // Number of antennas valid
        if( (numOfAntennas < 1) || (numOfAntennas > m_maxNumAntennas) )
            {
                m_errorVector.addElement("CorrConfig Number of antennas invalid: " + numOfAntennas +   " Valid range: 1 - "+m_maxNumAntennas);
                copyErrorStrings(errorStringSeq);
                return false;
            }	   
        
        // Copy an errors encountered into the return error string sequence
        copyErrorStrings(errorStringSeq);

        // Check ACAPhaseSwConfig
        m_logger.info("ACACorrConfigValidator:: validating CorrConfigIDL returns "); 
        return isValidConfig;
    }
    
    /** Validate a baseband.
     */
    //-------------------------------------------------------------------------
    private boolean validateBaseband( alma.Correlator.BaseBandConfig baseBand)
    {
        boolean bValidBaseBand = true;
        m_totalChanAverRegionsPerBaseband = 0;

        //
        // we only accept BB_1...BB_4
        //
        if( (baseBand.basebandName != BasebandName.BB_1) &&
            (baseBand.basebandName != BasebandName.BB_2) &&
            (baseBand.basebandName != BasebandName.BB_3) &&
            (baseBand.basebandName != BasebandName.BB_4) )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + "is invalid, we accept BB_1...BB_4 only");
                bValidBaseBand = false;
            }
        
        
        // Get parameters for data rate calculation 
        /*
        m_numEffectChannels = baseBand.spectralWindows[0].effectiveNumberOfChannels;
        m_numChanAveRegions = baseBand.spectralWindows[0].channelAverageRegions.length;
        m_numBins = baseBand.binSwitchingMode.numberOfPositions;
        */
        
        //
        // validate accumulation mode and requested product
        //
        if ( baseBand.dataProducts == CorrelationMode.CROSS_ONLY )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName +
            " Data product choice: CROSS_ONLY not allowed");
                bValidBaseBand = false;
            }
        else if ( baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO &&
                  baseBand.CAM == AccumMode.FAST )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                                         baseBand.dataProducts.toString() + " incompatible with accumulation mode " + baseBand.CAM.toString());
                bValidBaseBand = false;
            }
        
        // 
        if ( baseBand.dataProducts == CorrelationMode.AUTO_ONLY) {
            
            // Do not expect CAM=NORMAL and dataProducts=AUTO_ONLY simultaneously.
            if( baseBand.CAM == AccumMode.NORMAL ){
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + baseBand.dataProducts.toString() + " conflict with AccumMode.NORMAL " );
                bValidBaseBand = false;
            }
            
            // Integration Duration
            if(m_integrationDuration == 10000||
               m_integrationDuration == 20000||
               m_integrationDuration == 40000||
               m_integrationDuration == 80000 )
                {}
            else if(m_integrationDuration > 80000 && m_integrationDuration %160000 == 0)
                {}
            else {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                                         baseBand.dataProducts.toString() + " conflict with integration duration value " );
                bValidBaseBand = false;
            }
            // chaeck channelAverageDuration
            if((m_channelAverageDuration != 10000) &&
               (m_channelAverageDuration != 20000) &&
               (m_channelAverageDuration != 40000) &&
               (m_channelAverageDuration != 80000) &&
               (m_channelAverageDuration %160000 != 0) ) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                                         baseBand.dataProducts.toString() + " conflict with channelAverage duration value " );
                bValidBaseBand = false;
            }
            
        } // end of AUTO_ONLY
        
        //
        if ( baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO) {
            if(m_integrationDuration != 0 && m_integrationDuration %160000 != 0) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                                         baseBand.dataProducts.toString() + " conflict with integration duration value " );
                bValidBaseBand = false;
            }
            if(m_channelAverageDuration != 0 && m_channelAverageDuration %160000 != 0) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                                         baseBand.dataProducts.toString() + " channelAverage duration value should be an integral multiple of 16ms." );
                bValidBaseBand = false;
            }
        }
        
        // SidebandProcessingModeMod::SidebandProcessingMode sideBandSeparationMode;
        // Currently sideband separation does not allow FREQUENCY_OFFSET_SEPARATION and .PHASE_SWITCH_REJECTION
        if( baseBand.sideBandSeparationMode == alma.SidebandProcessingModeMod.SidebandProcessingMode.FREQUENCY_OFFSET_SEPARATION || baseBand.sideBandSeparationMode == alma.SidebandProcessingModeMod.SidebandProcessingMode.PHASE_SWITCH_REJECTION )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                         ": Sideband separation mode " +  
                                         JSidebandProcessingMode.toString(baseBand.sideBandSeparationMode) +
                                         " is not allowed for ACA-CCC. ");
                bValidBaseBand = false;
            }

        /*      
        if( baseBand.sideBandSeparationMode == alma.SidebandProcessingModeMod.SidebandProcessingMode.PHASE_SWITCH_SEPARATION ) {
            m_sideBandSeparationMode = true;
        }
        */
        //
        // validate spectral windows
        //
        if( !validateSpectralWindows(baseBand) )
            {
                bValidBaseBand = false;
            }

        
        /*
        if(baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO) {
            // Check data rate on this baseband
            if ( !checkDataRate(m_numOfAntennas, 0, baseBand.spectralWindows.length) )
                {
                    bValidBaseBand = false;
                }
        }
        else if(baseBand.dataProducts == CorrelationMode.AUTO_ONLY) {
            //
            // Check data rate on this baseband
            //
            if ( !checkDataRate(m_numOfAntennas, 1, baseBand.spectralWindows.length) )
                {
                    bValidBaseBand = false;
                }
        }
        else {
            bValidBaseBand = false;
        }
        */

        // Check that this baseband has at least one channel average region
        /* For the calibration observation, no channel average regions
           if( m_totalChanAverRegionsPerBaseband == 0 )
           {
           m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + " has no channel average regions");
           bValidBaseBand = false;
           }
        */
        
        /*
         * BinSwitching_t  binSwitchingMode;
         *  Rules: SwitchingType = NO_SWITCHING, # of positions must be 1, dwell lenghth = 1 dead times ignored
         *  SwitchingType == Nutator or Frequency switching, # of positions must be 2,3, or 4
         *    Nutator: Length of dwell & dead times must equal # of positions w/ 48ms boundary conditions
         *    Frequency: Length of dwell times must equal # of positions w/ 48ms boundary conditions dead 
         *    times must be 0 either by setting the sequence length to 0 or using values = 0
         */
        
        // NO_SWITCHING type
        if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING ) {
            // the number of positions must be 1
            if( baseBand.binSwitchingMode.numberOfPositions != 1 )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                             ": # Bin switching positions " + 
                                             baseBand.binSwitchingMode.numberOfPositions);
                    bValidBaseBand = false;
                }
            // the array lengths of dwell & dead must be 1 and equal
            else if( (baseBand.binSwitchingMode.dwellTime.length != 1) || 
                     (baseBand.binSwitchingMode.deadTime.length > 1) )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                             " [# Bin switching positions " + baseBand.binSwitchingMode.numberOfPositions +
                                             " Array lengths: Dwell time: " + baseBand.binSwitchingMode.dwellTime.length + 
                                             " Dead time: " + baseBand.binSwitchingMode.deadTime.length + 
                                             "] must all be equal");
                    bValidBaseBand = false;
                }
        }
        else { 
            // For Both NUTATOR_SWITCHING and FREQUENCY_SWITCHI
            if( (baseBand.binSwitchingMode.numberOfPositions < 2) || 
                (baseBand.binSwitchingMode.numberOfPositions > 4) )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + 
                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                             ": # Bin switching positions " + 
                                             baseBand.binSwitchingMode.numberOfPositions + " unsupported");
                    bValidBaseBand = false;
                }
            // # of dwell time entries = # of positions
            if ( baseBand.binSwitchingMode.dwellTime.length != baseBand.binSwitchingMode.numberOfPositions )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                             ": # Bin switching positions " + 
                                             baseBand.binSwitchingMode.numberOfPositions + " not equal to # of dwell time postions " +
                                             baseBand.binSwitchingMode.dwellTime.length);
                    bValidBaseBand = false;
                }

            if ( baseBand.binSwitchingMode.deadTime.length != baseBand.binSwitchingMode.dwellTime.length ) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                         ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                         " must have equal numbers of dwell time entries: " +
                                         baseBand.binSwitchingMode.dwellTime.length +
                                         " dead time entries: " +
                                         baseBand.binSwitchingMode.deadTime.length);
                bValidBaseBand = false;
            }
            ///// work place ///////////////////
            for( int i = 0; i < baseBand.binSwitchingMode.deadTime.length; i++) {
                // AUTO_ONLY
                if ( baseBand.dataProducts == CorrelationMode.AUTO_ONLY) {
                    if( (baseBand.binSwitchingMode.deadTime[i] % 1 ) != 0) 
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                     ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +  ": dead time must be 1ms multiple for AUTO_ONLY mode, but value is "
                                                     + Long.toString(baseBand.binSwitchingMode.deadTime[i]) );
                            bValidBaseBand = false;
                        }                
                    if( (baseBand.binSwitchingMode.dwellTime[i] % 1 ) != 0) 
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                     ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +  ": dwell time must be 1ms multiple for AUTO_ONLY mode, but value is "
                                                     + Long.toString(baseBand.binSwitchingMode.dwellTime[i]) );
                            bValidBaseBand = false;
                        }                
                }
                // CROSS_AND_AUTO)
                if ( baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO) {
                    if( (baseBand.binSwitchingMode.deadTime[i] % 16 ) != 0) 
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                     ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +  ": dead time must be 16 ms multiple for CROSS_AND_AUTO mode, but value is "
                                                     + Long.toString(baseBand.binSwitchingMode.deadTime[i]) );
                            bValidBaseBand = false;
                        }                
                    if( (baseBand.binSwitchingMode.dwellTime[i] % 16 ) != 0) 
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                     ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +  ": dwell time must be 16 ms multiple for CROSS_AND_AUTO, but value is "
                                                     + Long.toString(baseBand.binSwitchingMode.dwellTime[i]) );
                            bValidBaseBand = false;
                        }                
                }
            } // End of for 
            ////////////////////////////////////////////
        
            // SwitchingType NUTATOR_SWITCHING
            if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.NUTATOR_SWITCHING ) {
                // Check that dwell time + dead time must be a multiple of 48ms or 0 if it's no_switching
                if( (baseBand.binSwitchingMode.deadTime.length != 0) &&
                    (baseBand.binSwitchingMode.dwellTime.length != 0) )
                    {
                        for( int i = 0; i < baseBand.binSwitchingMode.deadTime.length; i++) {
                            // First check that NUTATOR switching have both dwell &  dead times
                            if( (baseBand.binSwitchingMode.deadTime[i] == 0) || (baseBand.binSwitchingMode.dwellTime[i] == 0) )
                                {
                                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                                             ": must have non-zero dwell ("+ Long.toString(baseBand.binSwitchingMode.dwellTime[i]) + ")"+
                                                             " and dead (" + Long.toString(baseBand.binSwitchingMode.deadTime[i]) + ") times");
                                    bValidBaseBand = false;
                                }
                            
                            // make sure dead+dwell times are non zero
                            long deadDwellTime =  baseBand.binSwitchingMode.deadTime[i] + baseBand.binSwitchingMode.dwellTime[i];
                            if( deadDwellTime == 0) 
                                {
                                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                                             ": Bin Switching dwell + dead time set [" + Integer.toString(i) +
                                                             "] is " + Long.toString(deadDwellTime) +
                                                             " is 0");
                                    bValidBaseBand = false;
                                }
                            // and are a multiple of 48ms
                            else if( (deadDwellTime % 480000) != 0 )
                                {
                                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                             ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                                             ": Bin Switching dwell + dead time set [" + Integer.toString(i) +
                                                             "] is " + Long.toString(deadDwellTime) +
                                                             " must be a multiple of 480000 100ns.");
                                    bValidBaseBand = false;
                                }
                        } // End of for loop
                    } 
            } // End of NUTATOR_SWITCHING
            
            // FREQUENCY_SWITCHING type
            else if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.FREQUENCY_SWITCHING ) 
                { // Frequency switching can't have dead time
                    for( int i = 0; i < baseBand.binSwitchingMode.deadTime.length; i++) {
                        if( baseBand.binSwitchingMode.deadTime[i] != 0 )
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                     ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                                     ": can't have dead times: [" + Integer.toString(i) +
                                                     "]: " + Long.toString(baseBand.binSwitchingMode.deadTime[i]));
                            bValidBaseBand = false;
                        }
                    } // End of dead time check for loop

                    for( int i = 0; i < baseBand.binSwitchingMode.dwellTime.length; i++) {
                        // Dwell times must be a multiple of 48ms
                        if( (baseBand.binSwitchingMode.dwellTime[i] % 480000) != 0 )
                            {
                                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                         ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                                         ": Dwell time[" + Integer.toString(i) +
                                                         "] is " + Long.toString(baseBand.binSwitchingMode.dwellTime[i] ) +
                                                         " must be a multiple of 48 ms.");
                                bValidBaseBand = false;
                            }
                    }
                } // End of FREQUENCY_SWITCHI
        } // End of  NUTATOR_SWITCHING and FREQUENCY_SWITCHI
        
        // polarizationMode must be in the range
        if( ! (baseBand.polarizationMode == ACAPolarization.ACA_STANDARD  || 
               baseBand.polarizationMode == ACAPolarization.ACA_XX_YY_SUM || 
               baseBand.polarizationMode == ACAPolarization.ACA_XX_50     || 
               baseBand.polarizationMode == ACAPolarization.ACA_YY_50 ) )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName + ": polarizationMode value " + 
                                         baseBand.polarizationMode + " is invalid.");
                bValidBaseBand = false;
            }

        if(baseBand.LO2FrequencyMHz < 8000 || baseBand.LO2FrequencyMHz > 14000 ) {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName + " LO2 Frequency has value: " + baseBand.LO2FrequencyMHz  + " It is out of range[8GHz-14GHz]" );
            bValidBaseBand = false;
        }
        
        /* The center frequency of the residual delay correction should vary 
           from -7GHz to +9GHz by 1GHz when the frequency range of the 
           baseband is defined as [0, 2]GHz.
        */
        if( (baseBand.centerFreqOfResidualDelayMHz < -7000.0) || (baseBand.centerFreqOfResidualDelayMHz > 9000.0) )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName + ": centerFreqOfResidualDelayMHz " + baseBand.centerFreqOfResidualDelayMHz + 
                                         " is out of [-7000, 9000] MHz range");
                bValidBaseBand = false;
            }
        
        return bValidBaseBand;
    }
    
    //---------------------------------------------------------------------------------
    private boolean validateSpectralWindows(alma.Correlator.BaseBandConfig baseBand )
    {
        boolean bValidSpectralWindow = true;
        String basebandName = JBasebandName.name(baseBand.basebandName);
        int totalNumberOfBasebandChannels = 0;
        int sumOfEffectiveBandwidthMHz = 0;
        
        // Make sure that the number of spectral windows is valid
        if( (baseBand.spectralWindows.length < 1) || (baseBand.spectralWindows.length > m_maxNumSpectralWindows) )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": Spectral window count " + 
                                         baseBand.spectralWindows.length + " is invalid. Valid range: 1 - "+m_maxNumSpectralWindows);
                return false;
            }
        // Loop through each SpectralWindow under this BaseBand
        for(int si = 0; si < (int)baseBand.spectralWindows.length; si++ )
            {
                int si1 = si + 1;

                /* temp disabled
                if( (baseBand.spectralWindows[si].centerFrequencyMHz < 0.0) ||
                    (baseBand.spectralWindows[si].centerFrequencyMHz > 2000.0) )
                    {
                        m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                                                 si+1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                                                 " is out of [2000, 4000] MHz range");
                        bValidSpectralWindow = false;
                    }
                */
                // Check that  spectral window w/ center frequency doesn't go below baseband (2GHz)

                //                if( (baseBand.spectralWindows[si].centerFrequencyMHz - baseBand.spectralWindows[si].effectiveBandwidthMHz/2.0) < 0.0)
                /* This validation rule needs to be clarified
                if( (baseBand.spectralWindows[si].centerFrequencyMHz - baseBand.spectralWindows[si].effectiveBandwidthMHz/2.0) < -0.0020 )
                    {
                        m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow : " + 
                                                 si+1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                                                 " effectiveBandwidthMHz " + baseBand.spectralWindows[si].effectiveBandwidthMHz +
                                                 " extends below baseband beginning");
                        bValidSpectralWindow = false;
                    }
                */
                // Check that spectral window w/ center frequency doesn't go above baseband (4GHz)

                /* temp disabled
                if( (baseBand.spectralWindows[si].centerFrequencyMHz + baseBand.spectralWindows[si].effectiveBandwidthMHz/2.0) >
                    2000.0 )
                    {
                        m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow : " + 
                                                 si+1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                                                 " bandwidthMHz " + baseBand.spectralWindows[si].effectiveBandwidthMHz +
                                                 " extends above baseband beginning");
                        bValidSpectralWindow = false;
                    }
                */

                //  Check that effective bandwidth make sense (taking into account TFB overlap
                /*
                if( (baseBand.spectralWindows[si].effectiveBandwidthMHz < 50.0) ||
                    (baseBand.spectralWindows[si].effectiveBandwidthMHz > 2000.0) )
                    {
                        m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                                                 si+1 + " effective bandwidth " + baseBand.spectralWindows[si].effectiveBandwidthMHz + 
                                                 " is out of [62.5, 2000] MHz range");
                        bValidSpectralWindow = false;
                    }
                */

                // 
                //  Check that effective # of channels make sense (taking into account TFB overlap
                if( (baseBand.spectralWindows[si].effectiveNumberOfChannels < 16) ||
                    (baseBand.spectralWindows[si].effectiveNumberOfChannels > 8192) )
                    {
                        m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                                                 si+1 + " effective number of channels " + baseBand.spectralWindows[si].effectiveNumberOfChannels + 
                                                 " is out of [16,8192] range");
                        bValidSpectralWindow = false;
                    }
                
                // spectralAveragingFactor must be a power of 2, 1, 2, 4, 8, etc to maximum # of channels
                switch( baseBand.spectralWindows[si].spectralAveragingFactor )
                    {
                    case 1:
                    case 2:
                    case 4:
                    case 8:
                    case 16:
                        break;
                    default:
                        {
                            m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                     ": Input spectralAveragingFactor " + baseBand.spectralWindows[si].spectralAveragingFactor +
                                                     ", Not a power of 2 or >= " + 16);
                            bValidSpectralWindow = false;
                        }
                    }
                // Update output binary data size - Should we consider ASDM XML header overhead? Also, 2-byte vs.
                // 4-byte visibilities are not taken into account. Not sure how to handle that.
                m_outputDataSize += (baseBand.spectralWindows[si].effectiveNumberOfChannels / baseBand.spectralWindows[si].spectralAveragingFactor);
                // Manabu: In the validation of effectiveNumberOfChannels, spectralAveragingFactor
                //    should NOT be taken into account.
                
                //                totalNumberOfBasebandChannels += (baseBand.spectralWindows[si].effectiveNumberOfChannels / baseBand.spectralWindows[si].spectralAveragingFactor);
                totalNumberOfBasebandChannels += baseBand.spectralWindows[si].effectiveNumberOfChannels;
                
                // effectiveNumberOfChannels  should be an integral multiple of 
                // the value of spectralAveragingFactor
                if( (baseBand.spectralWindows[si].effectiveNumberOfChannels %  baseBand.spectralWindows[si].spectralAveragingFactor) != 0) {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                             ": effectiveNumberOfChannels is not integral multiple of spectralAveragingFactor " );
                    bValidSpectralWindow = false;
                }
                
                // Validate associatedSpectralWindowNumberInPair for DSB receivers
                //
                if ( m_receiverType == ReceiverSideband.DSB )
                    {
                        boolean bFound = false;
                        // Scan thru spectral windows to see if one matches associatedSpectralWindowNumberInPair
                        for( int i = 0; i < baseBand.spectralWindows.length; i++ )
                            {
                                if( (i+1) == baseBand.spectralWindows[si].associatedSpectralWindowNumberInPair )
                                    {
                                        bFound = true;
                                        break;
                                    }
                            }
                        
                        if( !bFound )
                            {
                                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                         ": Spectral Window: " + si1+
                                                         ": For DSB receiver, didn't find pair spectral window # which matches " + 
                                                         baseBand.spectralWindows[si].associatedSpectralWindowNumberInPair);
                                bValidSpectralWindow = false;
                            }
                    }
                else
                    {
                        // For non-DSB receiver, must always use this spec window.
                        if( !baseBand.spectralWindows[si].useThisSpectralWindow )
                            {
                                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                         ": Spectral Window: " + si1+
                                                         " For non-DSB receiver, cannot suppress a spectral window: useThisSpectralWindow = false");
                                bValidSpectralWindow = false;
                            }
                    }
                
                // Validate sideBand. NOSB should not be used, but currently it can be allowed.
                /*
                  if(baseBand.spectralWindows[si].sideBand.name().equals(JNetSideBand.sNOSB))
                  {
                  m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                  ": sideband invalid: " + JNetSideBand.sNOSB);
                  bValidSpectralWindow = false;
                  }
                */
                
                // if channel average region(s) invalid, flag spectral window invalid
                if( !validateChannelAverageRegions(basebandName, baseBand.spectralWindows[si], si, m_errorVector) ) {
                    bValidSpectralWindow = false;
                }
                
                // ALMA-B specific, but since this is an ALMA enumeration, can't be wrong
                //CorrelationBitMod::CorrelationBit correlationBits;
                // ALMA-B specific, but since this is an ALMA enumeration, can't be wrong
                //boolean correlationNyquistOversampling;
                
                
                /* Not necessary always true, only true for compatible mode
                if( !baseBand.spectralWindows[si].frqChProfReproduction )
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                 ": Spectral Window: " + si1+
                                                 " frequency channel profile is false ");
                        bValidSpectralWindow = false;
                    }
                */

                // effectiveNumberOfChannels should be an integral multiple of the value of spectralAveragingFactor 
                if( baseBand.spectralWindows[si].effectiveNumberOfChannels % baseBand.spectralWindows[si].spectralAveragingFactor != 0 )
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                 ": Spectral Window: " + si +
                                                 " effectiveNumberOfChannels is not an integral multiple of the value of spectralAveragingFactor ");
                        bValidSpectralWindow = false;
                    }
                
                //////// work space New add ////////////
                double cenFrq = baseBand.spectralWindows[si].centerFrequencyMHz;
                double eftBdw =  baseBand.spectralWindows[si].effectiveBandwidthMHz;
                long eftNch = baseBand.spectralWindows[si].effectiveNumberOfChannels;
                double chaInt = eftBdw / eftNch;

                m_logger.info(">>> cenFrq " + Double.toString(cenFrq) + " eftBdw " + Double.toString(eftBdw) + " eftNch " + Long.toString(eftNch) + " chaInt " + Double.toString(chaInt) );

                // Start channel 
                long staCha = (long)(Math.round((cenFrq - eftBdw / 2) / chaInt));
                // End channel
                long endCha = (long)(Math.round((cenFrq + eftBdw / 2) / chaInt)) - 1;
               
                // 
                if(baseBand.spectralWindows[si].frqChProfReproduction) {
                    
                    sumOfEffectiveBandwidthMHz += eftBdw + (5 * chaInt);
                }
                else {
                    sumOfEffectiveBandwidthMHz += eftBdw;
                }

                if( staCha % 8 != 0) {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                             ": start channel " + staCha + " is not integral multiple of 8 " );
                    bValidSpectralWindow = false;
                }
                
                if( (endCha + 1) % 8 != 0) {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                             ": end channel " + endCha + " is not integral multiple of 8 - 1 " );
                    bValidSpectralWindow = false;
                }
                
                if( endCha - staCha < 15) {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                             ": end channel is too close to start channel [endCha - staCha < 15] " );
                    bValidSpectralWindow = false;
                }

                // frqChProfReproduction = true
                if( baseBand.spectralWindows[si].frqChProfReproduction ) {
                    if(chaInt <= 3.90625) {
                        if( staCha < 8) {
                            m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                     ": start channel " + staCha + " should not be less than 8 while frqChProfReproduction = true & chaInt <= 3.90625 " );
                            bValidSpectralWindow = false;
                        }
                        
                        if( endCha >= (2000/chaInt) - 8 ) {
                            m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                     ": end channel " + endCha + " is out of upper bound " + (2000/chaInt - 8) + " while frqChProfReproduction = true & chaInt <= 3.90625  ");
                            bValidSpectralWindow = false;
                        }  
                    }
                    else { // chaInt > 3.90625
                        if( staCha < 0) {
                            m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                     ": start channel " + staCha + " should not be less than 0 while frqChProfReproduction = true & chaInt > 3.90625 " );
                            bValidSpectralWindow = false;
                        }
                        
                        if( endCha >= (2000/chaInt) ) {
                            m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                     ": end channel " + endCha + " is out of upper bound " + (2000/chaInt) + " while frqChProfReproduction = true &  chaInt > 3.90625 ");
                            bValidSpectralWindow = false;
                        }
                    }
                }
                // frqChProfReproduction = false
                else if(!baseBand.spectralWindows[si].frqChProfReproduction ) {
                    
                    if( staCha < 0 ) {
                        m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                 ": start channel should not be less than 0 " );
                        bValidSpectralWindow = false;
                    }
                    
                    if( endCha >= (2000/chaInt) ) {
                        m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                                 ": end channel " + endCha + " is out of upper bound " + (2000/chaInt) );
                        bValidSpectralWindow = false;
                    }
                } 
                else {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                                             ": frqChProfReproduction should have boolean value true or false " );
                    bValidSpectralWindow = false;
                    
                }
            } // end of for loop
        
        // Check sum of effectiveBandwidthMHz
        if( sumOfEffectiveBandwidthMHz > 2250 ) {
            m_errorVector.addElement("Baseband " + basebandName +  
                                     ": sum of effectiveBandwidthMHz is out of upper bound 2250 MHz");
            bValidSpectralWindow = false;
        }
        ////////////// end of work space /////////////////
        
        
        // Maximum number of spectral channels across all spectral windows must be less than 8K
        // A better check would be to take into account the effective # of channels.

        // Consider the ACA correlator, the 
        // polarization products can not be changed by the spectral windows.
        if( baseBand.spectralWindows[0].polnProductsSeq.length == 1) {
            System.out.println(" >> polnProductsSeq " + baseBand.spectralWindows[0].polnProductsSeq[0]);
            m_NumberPolns = 1;
            if(! baseBand.spectralWindows[0].polnProductsSeq[0].toString().equals("XX")) {
                if(! baseBand.spectralWindows[0].polnProductsSeq[0].toString().equals("YY")) 
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                                 ": polarization products incorrect" );
                        bValidSpectralWindow = false;
                    }
            }
            
            if( totalNumberOfBasebandChannels > 8192 )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": has too many spectral channels defined: "+ totalNumberOfBasebandChannels);
                    bValidSpectralWindow = false;
                }
            // polnProductsSeq should be XX & YY
        } else if(baseBand.spectralWindows[0].polnProductsSeq.length == 2) {
            m_NumberPolns = 2;
            if(! baseBand.spectralWindows[0].polnProductsSeq[0].toString().equals("XX") &&
               ! baseBand.spectralWindows[0].polnProductsSeq[1].toString().equals("YY")) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                         ": polarization products incorrect" );
                bValidSpectralWindow = false;
            }
            
            if( totalNumberOfBasebandChannels > 4096 )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": has too many spectral channels defined: "+ totalNumberOfBasebandChannels);
                    bValidSpectralWindow = false;
                }
            // polnProductsSeq should be XX XY YX YY
        } else if(baseBand.spectralWindows[0].polnProductsSeq.length == 4) {
            m_NumberPolns = 4;
            if(! baseBand.spectralWindows[0].polnProductsSeq[0].toString().equals("XX") &&
               ! baseBand.spectralWindows[0].polnProductsSeq[1].toString().equals("XY") &&
               ! baseBand.spectralWindows[0].polnProductsSeq[2].toString().equals("YX") &&
               ! baseBand.spectralWindows[0].polnProductsSeq[3].toString().equals("YY")) {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                         ": polarization products incorrect" );
                bValidSpectralWindow = false;
            }

            if( totalNumberOfBasebandChannels > 2048 )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                             ": has too many spectral channels defined: "+ totalNumberOfBasebandChannels);
                    bValidSpectralWindow = false;
                }
        } else {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ":polarization products are unrecognized ");
                    bValidSpectralWindow = false;
        }
            
        // ALMA-B specific: No check needed, just a boolean
        //boolean quantizationCorrection;
        
        return bValidSpectralWindow;
    }
    
    // Loop through each ChannelAverageRegion to validate startChannel,
    //---------------------------------------------------------------------------------
    private boolean validateChannelAverageRegions( String basebandName,
                                                   alma.Correlator.SpectralWindow specWindow, int specWindowIdx,
                                                   Vector m_errorVector)
        {
            boolean bValidChanAverRegion = true;
        
        // We must have 1 channel average region in the entire baseband, but not all
        // spectral windows need one, so just check here for the maximum
        if( specWindow.channelAverageRegions.length > 10 )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": Input ChannelAverageRegions " + 
                                         specWindow.channelAverageRegions.length + ", is out of [0,10] Regions !");
                bValidChanAverRegion = false;
            }
        else
            {
                m_totalChanAverRegionsPerBaseband += specWindow.channelAverageRegions.length;
                for(int carIdx = 0; carIdx < specWindow.channelAverageRegions.length; carIdx++ )
                    {
                        // check that channel aver region start channel is w/in spectral window
                        if( (specWindow.channelAverageRegions[carIdx].startChannel < 0) || 
                            (specWindow.channelAverageRegions[carIdx].startChannel >= specWindow.effectiveNumberOfChannels) )
                            {
                                m_errorVector.addElement("BaseBand " + basebandName + 
                                                         ": ChannelAverageRegion " + carIdx+1 +
                                                         " starting channel invalid: " +
                                                         specWindow.channelAverageRegions[carIdx].startChannel);
                                bValidChanAverRegion = false;
                            }
                        // check that channel average region does not extend beyond spectral window
                        int stopChannel = specWindow.channelAverageRegions[carIdx].startChannel + 
                            specWindow.channelAverageRegions[carIdx].numberChannels - 1;
                        
                        if( stopChannel >= specWindow.effectiveNumberOfChannels)
                            {
                                m_errorVector.addElement("BaseBand " + basebandName + 
                                                         ": ChannelAverageRegion " + carIdx+1 +
                                                         ": last channel " + stopChannel + 
                                                         " extends beyond last channel of spectral window " +
                                                         specWindow.effectiveNumberOfChannels);
                                bValidChanAverRegion = false;
                            }
                        // check that number of channels > 0
                        if( specWindow.channelAverageRegions[carIdx].numberChannels < 1 )
                            {
                                m_errorVector.addElement("BaseBand " + basebandName + 
                                                         ": ChannelAverageRegion " + carIdx+1 +
                                                         ": number of channels invalid: " + 
                                                         specWindow.channelAverageRegions[carIdx].numberChannels);
                                bValidChanAverRegion = false;
                            }
                    }
            }
        return bValidChanAverRegion;
    }
    
    /** Check that the data rate does not exceed 60MB/sec. This is only looking at the 
     ** binary data and assumes that there are 4 bytes per result (some visibilities can
     ** be 2 bytes) and it ignores the ASDM binary XML headers which is usually less than
     ** 10% overhead
     */
    //---------------------------------------------------------------------------------
    private boolean checkDataRate(CorrelatorConfiguration corrConfig, long numOfAntennas, alma.BasebandNameMod.BasebandName baseBandName) {
        float dataRate = 0;
        dataRate = calculateBasebandDataRate(corrConfig, numOfAntennas, baseBandName);
        if( dataRate  > 0.9 ) { 
            String str = String.format("Data Rate %8.3f MB/s exceeds 0.9 MB/s limit: # of Antennas: %d Integration Duration: %8.3f nano seconds ",
                                       dataRate, numOfAntennas, corrConfig.integrationDuration);
            m_errorVector.addElement(str);
            return false;
        } 
        String str = String.format("Data Rate: %8.3f MB/s : # of Antennas: %d Integration Duration: %d : ", dataRate, numOfAntennas, corrConfig.integrationDuration);
        m_logger.info(str);
        return true;
    } 
    
    
    /*
    private boolean checkDataRate(int numOfAntennas, int correlationMode, int spectralWindows)
    {
        float fullResolution = 0;
        float channelAverage = 0;
        float dataRate = 0;
        float Header = 1300;
        float integrationDurationSec = (float)m_integrationDuration / 10000000;
        float channelAverageDurationSec = (float)m_channelAverageDuration / 10000000;
        
        // CROSS_AND_AUTO
        if (correlationMode == 0 ) {
            int actualTimes = 8 * (numOfAntennas * min(m_NumberPolns, 3) + numOfAntennas * (numOfAntennas -1) / 2 * m_NumberPolns);
            int actualDurations = actualTimes;
            float Flags = actualTimes / 2;
            
            if( m_sideBandSeparationMode ) {
                // Double data in sideband separation mode
                fullResolution  = 4* m_numEffectChannels *(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2 * m_numAPCDataSets) * m_NumberPolns * 2;
                System.out.println(" fullResolution " + (new Float(fullResolution)).toString());
                channelAverage = 4* m_numChanAveRegions*(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2) * m_NumberPolns * 2;
                System.out.println(" channelAverage " + (new Float(channelAverage)).toString());
            }
            else {
                fullResolution  = 4* m_numEffectChannels *(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2 * m_numAPCDataSets) * m_NumberPolns;
                System.out.println(" fullResolution " + (new Float(fullResolution)).toString());
                channelAverage = 4* m_numChanAveRegions*(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2) * m_NumberPolns;
            }
            // Calculate data rate
            dataRate = (spectralWindows * (fullResolution) + actualTimes + actualDurations + Flags + Header) / integrationDurationSec
                + (spectralWindows * (channelAverage) + actualTimes + actualDurations + Flags + Header) / channelAverageDurationSec;
            dataRate = dataRate/(1024 * 1024); 
        }
        
        // AUTO_ONLY
        if (correlationMode == 1) {
            int actualTimes = 8 * numOfAntennas * min(m_NumberPolns, 3) * m_numBins;
            int actualDurations = actualTimes;
            float Flags = actualTimes / 2;
            
            fullResolution = 4* m_numEffectChannels * numOfAntennas * m_NumberPolns * m_numBins;
            channelAverage = 4* m_numChanAveRegions * numOfAntennas * m_NumberPolns * m_numBins;
            // Calculate data rate
            dataRate = (spectralWindows * (fullResolution) + actualTimes + actualDurations + Flags + Header) / integrationDurationSec
                + (spectralWindows * (channelAverage) + actualTimes + actualDurations + Flags + Header) / channelAverageDurationSec;
            dataRate = dataRate/(1024 * 1024); 
        }
        
        if( dataRate  > 0.9 ) { 
            String str = String.format("Data Rate %8.3f MB/s exceeds 0.9 MB/s limit: # of Antennas: %d Integration Duration: %8.3fconds ",
                                       dataRate, numOfAntennas, integrationDurationSec );
            m_errorVector.addElement(str);
            return false;
        } 
        String str = String.format("Data Rate: %8.3f MB/s : # of Antennas: %d Integration Duration: %8.3f ", dataRate, numOfAntennas, integrationDurationSec);
        m_logger.info(str);
        return true;
    } 
    */
    
    // Work Space
    // Setup Parameters For Data Rate Calculation
    private void setupParametersForDRCalcul(CorrelatorConfiguration corrConfig, alma.BasebandNameMod.BasebandName baseBandName, alma.Correlator.SpectralWindow spw) 
    {
        m_integrationDuration = corrConfig.integrationDuration;
        m_channelAverageDuration = corrConfig.channelAverageDuration;
        m_numAPCDataSets = corrConfig.APCDataSets.length;
        m_numEffectChannels = spw.effectiveNumberOfChannels;
        m_numChanAveRegions = spw.channelAverageRegions.length;
        m_NumberPolns = spw.polnProductsSeq.length;
        
        for(int bbj = 0; bbj < corrConfig.baseBands.length; bbj++ ) 
            {
                if(corrConfig.baseBands[bbj].basebandName == baseBandName) {
                    m_numBins = corrConfig.baseBands[bbj].binSwitchingMode.numberOfPositions;

                    if( corrConfig.baseBands[bbj].dataProducts == CorrelationMode.CROSS_AND_AUTO ) 
                        {
                            // cross_and_auto
                            m_dataProducts = 0;
                        }
                    if( corrConfig.baseBands[bbj].dataProducts == CorrelationMode.AUTO_ONLY )
                        {
                            // auto_only
                            m_dataProducts = 1;
                        }
                    if(corrConfig.baseBands[bbj].sideBandSeparationMode == alma.SidebandProcessingModeMod.SidebandProcessingMode.PHASE_SWITCH_SEPARATION ) {
                        m_sideBandSeparationMode = true;
                    }
                }
            } 
    }

    // Calculate Configuration Total Data Rate
    float calculateConfigTotalDataRate(CorrelatorConfiguration corrConfig, long
                                       numOfAntennas) {
        float dataRateOfThisConfig = 0;
        for(int bbj = 0; bbj < corrConfig.baseBands.length; bbj++ ) {
            dataRateOfThisConfig += calculateBasebandDataRate(corrConfig, numOfAntennas, corrConfig.baseBands[bbj].basebandName);
        }
        String str = String.format(" Data Rate of Configuration: %8.3f MB/s ", dataRateOfThisConfig );
        m_logger.info(str); 
        return dataRateOfThisConfig;
    }
    
    // Calculate Baseband Data Rate
    public float calculateBasebandDataRate(CorrelatorConfiguration corrConfig, long numOfAntennas, alma.BasebandNameMod.BasebandName baseBandName) {
        float dataRateInThisBaseband = 0;
        for(int bbj = 0; bbj < corrConfig.baseBands.length; bbj++ ) 
            {
                if(corrConfig.baseBands[bbj].basebandName == baseBandName) {
                    
                    for(int sj = 0; sj < corrConfig.baseBands[bbj].spectralWindows.length; sj++ ) 
                        {
                            dataRateInThisBaseband += calculateSpectralWindowDataRate(corrConfig, numOfAntennas, baseBandName, corrConfig.baseBands[bbj].spectralWindows[sj]);
                        }
                }
            }
        String str = String.format("Data Rate: %8.3f MB/s : In the " + baseBandName, dataRateInThisBaseband );
        m_logger.info(str); 
        return dataRateInThisBaseband;
    }
    
    // Calculate SpectralWindow Data Rate
    public float calculateSpectralWindowDataRate(CorrelatorConfiguration corrConfig, long numOfAntennas, alma.BasebandNameMod.BasebandName baseBandName, alma.Correlator.SpectralWindow spw) 
    {
        float dataRateInThisSPW = 0;
        setupParametersForDRCalcul(corrConfig, baseBandName, spw);
        dataRateInThisSPW = calculateSpectralWindowDataRate((int)numOfAntennas);
        return dataRateInThisSPW;
    }

    // Internal function does the real calculation
    private float calculateSpectralWindowDataRate(int numOfAntennas) {
        float dataRate = 0;
        float fullResolution = 0;
        float channelAverage = 0;
        float Header = 1300;
        float integrationDurationSec = (float)m_integrationDuration / 10000000;
        float channelAverageDurationSec = (float)m_channelAverageDuration / 10000000;
        
        // CROSS_AND_AUTO
        if (m_dataProducts == 0 ) {
            int actualTimes = 8 * (numOfAntennas * min(m_NumberPolns, 3) + numOfAntennas * (numOfAntennas -1) / 2 * m_NumberPolns);
            int actualDurations = actualTimes;
            float Flags = actualTimes / 2;
            
            if( m_sideBandSeparationMode ) {
                // Double data in sideband separation mode
                fullResolution  = 4* m_numEffectChannels *(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2 * m_numAPCDataSets) * m_NumberPolns * 2;
                System.out.println(" fullResolution " + (new Float(fullResolution)).toString());
                channelAverage = 4* m_numChanAveRegions*(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2) * m_NumberPolns * 2;
                System.out.println(" channelAverage " + (new Float(channelAverage)).toString());
            }
            else {
                fullResolution  = 4* m_numEffectChannels *(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2 * m_numAPCDataSets) * m_NumberPolns;
                System.out.println(" fullResolution " + (new Float(fullResolution)).toString());
                channelAverage = 4* m_numChanAveRegions*(numOfAntennas + numOfAntennas*(numOfAntennas-1)/2 * 2) * m_NumberPolns;
            }
            // Calculate data rate
            dataRate = (fullResolution) / integrationDurationSec
                + (channelAverage) / channelAverageDurationSec;
            dataRate = dataRate/(1024 * 1024); 
        }
        
        // AUTO_ONLY
        if (m_dataProducts == 1) {
            int actualTimes = 8 * numOfAntennas * min(m_NumberPolns, 3) * m_numBins;
            int actualDurations = actualTimes;
            float Flags = actualTimes / 2;
            
            fullResolution = 4* m_numEffectChannels * numOfAntennas * m_NumberPolns * m_numBins;
            channelAverage = 4* m_numChanAveRegions * numOfAntennas * m_NumberPolns * m_numBins;
            // Calculate data rate
            dataRate = (fullResolution) / integrationDurationSec
                + (channelAverage) / channelAverageDurationSec;
            dataRate = dataRate/(1024 * 1024); 
        }

        String str = String.format("Data Rate: %8.3f MB/s : # of Antennas: %d In this spectral window ", dataRate, numOfAntennas );
        m_logger.info(str);     
        return dataRate;
    }
    
    /** A helper function to copy the error strings found during validation to the IDL string
     ** sequence returned to the caller.
     */
    //---------------------------------------------------------------------------------
    private void copyErrorStrings(alma.ACS.stringSeqHolder errorStringSeq)
    {
        int numErrors = m_errorVector.size();

        // If configuration invalid, add correlator type
        if( numErrors == 0 )
        {
            String[] retSeqString = new String[0];
            errorStringSeq.value = retSeqString;
            return;
        }

        String[] retSeqString = new String[numErrors+1];
        Iterator iter = m_errorVector.iterator();
        Object obj;
        int i = 0;
        retSeqString[i++] = new String("Errors found in configuration for Correlator type: ") + m_correlatorType + new String(":");
        while( iter.hasNext() )
        {
            obj = iter.next ();
            retSeqString[i++] = obj.toString().replaceAll("The following exception occured while validating field:", " ");
        }
        errorStringSeq.value = retSeqString;
    }

    /** Based on the correlator type, set various limits.
     */
    //---------------------------------------------------------------------------------
    private void setCorrelatorLimits()
    {
        System.out.println(" >> Correlator type is " + m_correlatorType );

        {
            m_maxNumSpectralWindows = 32;
            m_maxNumAntennas = 16;
            m_maxNumBasebands = 4;
            m_maxNumChanAverRegions = 32;
            m_maxNumAPCDatasets = 2;
        }
    }
    
    //
    private int min(int numOfPolns, int three) {
        if (numOfPolns < three) {
            return numOfPolns;
        } else {
            return 3;
        }
    }
}
