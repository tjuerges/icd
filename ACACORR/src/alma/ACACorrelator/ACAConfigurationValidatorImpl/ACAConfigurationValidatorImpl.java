/*
ALMA - Atacama Large Millimiter Array
* (c) Nobeyama Radio Observatory - NAOJ, 2008 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  dguo
 * @version $Id$
 * @since    
 */

package alma.ACACorrelator.ACAConfigurationValidatorImpl;

import alma.acacorrelator.ACACorrConfigValidator.ACACorrConfigValidator;
import java.util.logging.Logger;
import java.util.Vector;

import alma.ACS.ComponentStates;
import alma.ACS.stringSeqHolder;
import alma.ACS.stringSeqHelper;

import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
//import alma.acs.component.ComponentImplBase;
import alma.ACACorrelator.ACAConfigurationValidatorOperations;
// Shared file
import alma.Correlator.CorrelatorConfiguration;

import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.acs.entityutil.EntityDeserializer;


import alma.CorrConfigModeErr.wrappers.*;

/**
 * A component that implement ACAConfigValidator interface for providing 
 * Correlator Configuration Validation
 */
public class ACAConfigurationValidatorImpl implements ComponentLifecycle, ACAConfigurationValidatorOperations
{
    private ContainerServices m_containerServices;
    private Logger m_logger;
    private EntityDeserializer entityDeserializer;
    private ACACorrConfigValidator m_acavalidator;
    
    /////////////////////////////////////////////////////////////
    // Implementation of ComponentLifecycle
    /////////////////////////////////////////////////////////////
    
    public void initialize(ContainerServices containerServices) {
        m_containerServices = containerServices;
        m_logger = m_containerServices.getLogger();
        m_logger.info("initialize() called...");
        entityDeserializer = 
            EntityDeserializer.getEntityDeserializer(m_logger); 
    }
    
    public void execute()
        throws ComponentLifecycleException
    {
        m_logger.info("execute() called...");

        // instantiate the configuration validator
         try
         {
             m_acavalidator = new ACACorrConfigValidator(m_containerServices.getCDB());
         }
         catch ( Throwable thr )
         {
             throw new
                 ComponentLifecycleException("failed to get CDB", thr);
         }
    }
    
    //---------------------------------------------------------------------------------
    public void cleanUp() {
        m_logger.info("cleanUp() called..., to clean up.");
    }
    
    //---------------------------------------------------------------------------------
    public void aboutToAbort() {
        cleanUp();
        m_logger.info("managed to abort...");
    }
    
    /////////////////////////////////////////////////////////////
    // Implementation of ACSComponent
    /////////////////////////////////////////////////////////////
    
    //---------------------------------------------------------------------------------
    public ComponentStates componentState() {
        return m_containerServices.getComponentStateManager().getCurrentState();
    }
 
    //---------------------------------------------------------------------------------
    public String name() {
        return m_containerServices.getName();
    }
    
    /////////////////////////////////////////////////////////////
    // Implementation of Operations
    /////////////////////////////////////////////////////////////
    
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXml )
    {
        CorrelatorConfiguration corrConfig = new CorrelatorConfiguration();
        corrConfig = m_acavalidator.translateSpectralSpecXml2CorrConfigIdl(spectralSpecXml);
        return corrConfig;
    }

    //---------------------------------------------------------------------------------
    public boolean validateConfiguration(CorrelatorConfiguration corrConfigIDL, 
                                         int numOfAntennas, alma.ACS.stringSeqHolder errorMsgSeq)
    {
        //   Vector errorSeq = new Vector();
        boolean isValid = m_acavalidator.validateConfiguration(corrConfigIDL,
                                                            numOfAntennas,
                                                            errorMsgSeq );
        return isValid;
    }  
    
    //---------------------------------------------------------------------------------
    public boolean validateConfigurationSS( String spectralSpec,
                                            int numOfAntennas,
                                            alma.ACS.stringSeqHolder errorMsgSeq)
    {
        boolean isValid = m_acavalidator.validateConfigurationSS(spectralSpec,
                                                              numOfAntennas,
                                                              errorMsgSeq );
        return isValid;
    }
}
