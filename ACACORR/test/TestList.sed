s/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][ T][0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]\{1,3\}/----------T--:--:--.---/g
s/get local manager from [a-z,A-Z,0-9,-]*/get local manager from xxxx/g
s/Ran [0-9]* tests in [0-9]*.[0-9]*s/Ran -- tests in *.---s/g
s/Ran 1 test in [0-9]*.[0-9]*s/Ran 1 test in *.---s/g
s/Time: [0-9]*.[0-9]*/Time: *.---/g
s/=.*$/=/g
s/[A-Z][a-z][a-z] [0-9][0-9], [0-9][0-9][0-9][0-9] *[0-9]:[0-9][0-9]:[0-9][0-9] [A-Z]M/Zzz dd, yyyy hh:mm:ss --/g
