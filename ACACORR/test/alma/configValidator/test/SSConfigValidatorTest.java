
package alma.configValidator.test;

import junit.framework.*;
//Correlator
import alma.acacorrelator.ACACorrConfigValidator.*;
import alma.Correlator.*;

//For SB test
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.System;

/** This unit test tests Correlator configuration validation in a scheduling
 ** blocks found in a given directory. The directory is specified using -DdirName=<dir> 
 ** (see example in testJSbConfigValidator script).
 ** If no directory specified, uses current.
 */
public class SSConfigValidatorTest extends TestCase
{
    private ACACorrConfigValidator m_configValidator;
    
    //------------------------------------------------------------------------------
    public SSConfigValidatorTest( )
    {
    }

    //------------------------------------------------------------------------------
    protected void setUp()
    { 
        m_configValidator = new ACACorrConfigValidator();
        return;
        
    }
    
    //------------------------------------------------------------------------------
    public void testValidatorBySchedBlock() 
    {
        boolean expected = true;
        boolean isValid = true;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        String spectralSpecXML = new String();

        // If command-line has a directory specified, use it
        String dirName = System.getProperty("dirName");
        if( dirName.length() == 0 )
	    dirName = new String(".");

        System.out.println("\nReading Spectral Spec XML files from directory: "+dirName);

        String [] SBFiles = getSBFilesInDirectory(dirName);
        for( int i = 0; i < SBFiles.length; i++ )
        {
            try
            {
                spectralSpecXML = readSBFile(dirName+"/"+SBFiles[i]);
	  } 
	  catch (Exception ex)
	  {
	      fail("Error retrieving Spectral Spec from file " + SBFiles[i]);
	  }

	  try
	  {
	      isValid = m_configValidator.validateConfigurationSS(spectralSpecXML, 1, returnMsg);
	  }  
	  catch (Exception e) 
	  {
	      System.out.println(" validateConfiguration cause exception "+e.toString());
	  }
	  System.out.println("\nSpectral Spec file: "+SBFiles[i]+((isValid) ? " is valid" : " is invalid"));
	  String [] errorArray = returnMsg.value;
	  if( errorArray != null && errorArray.length > 0 )
	  {
	      for(int j = 0; j < errorArray.length; j++) 
	      {
		  System.out.println("Error " + (j+1) + " : " + errorArray[j]);
	      }
	  }
        }
    }

    //------------------------------------------------------------------------------
    public void testTranslationSpectralSpecXMLtoIDL() 
    {
        System.out.println("\nTesting spectral spec XML to IDL translation");
        CorrelatorConfiguration corrConfigIDL = null;
        String spectralSpecXML = new String();

        String dirName = System.getProperty("dirName");
        if( dirName.length() == 0 )
	    dirName = new String(".");

        try
        {
	    spectralSpecXML = readSBFile(dirName+"/ACA_SpectralSpec.xml");
        } 
        catch (Exception ex)
        {
	    fail("Error retrieving spectral spex from file: "+dirName+"/BL_SpectralSpec.xml");
        }

        corrConfigIDL = m_configValidator.translateSpectralSpecXml2CorrConfigIdl( spectralSpecXML );
        if( corrConfigIDL == null )
	    fail("Failed translating spectral spec XML to IDL");

        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();
	if( !m_configValidator.validateConfiguration(corrConfigIDL, 1, returnMsg) )
	{
	  String [] errorArray = returnMsg.value;
	  if( errorArray.length > 0 )
	  {
	      for(int j = 0; j < errorArray.length; j++) 
	      {
		  System.out.println("Error " + (j+1) + " : " + errorArray[j]);
	      }
	  }
	}
	  else
	      System.out.println("File is valid\n");

    }

    /**
     * Read SB from a file.
     * @param dirName Directory name
     * @param fileName File name
     * @return XML document
     * @throws Exception
     */
    //------------------------------------------------------------------------------
    private String readSBFile( String fileName)
	throws Exception
    {
        // Read file contents
        BufferedReader in = null;
        StringBuffer xmlDoc = null;
        String line = null;
        try
        {
            in = new BufferedReader(new FileReader(fileName));
            xmlDoc = new StringBuffer ();
            line = in.readLine();
            while (line != null)
            {
                xmlDoc.append(line + "\n");
                line = in.readLine();
            }
            in.close();
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e.getMessage());
        }
        return new String(xmlDoc);
    }	

    /** Read all SB XML files in a given directory. The filter is '.xml' & is 
     ** case sensitive.
     */
    //------------------------------------------------------------------------------
    private String [] getSBFilesInDirectory(String dirName)
    {
	// Create a filename filter for '*.xml'
	MyFileFilter filter = new MyFileFilter();
	File dir = new File(dirName);
    
	String []children = dir.list(filter);
    
	return children;
    }

    //------------------------------------------------------------------------------
    public class MyFileFilter implements FilenameFilter
    {
	public boolean accept(File dir, String name)
	{
	    return name.endsWith(".xml");
	}
    };
}
