/*
 * Created on Feb 21, 2011 by mschilli
 */
package alma.exec.extension;


/**
 * Provides hints to the plugin framework as how to treat a given plugin.
 * 
 * @author M.Schilling, ESO
 */
public final class PluginHint {

	/*
	 * Note this behaves like an enumeration, but isn't one.
	 * This is to allow changing the implementation details at
	 * a later point without breaking the plugin services api.
	 */
	private PluginHint() {}

	/** A plugin started this way will have its own window. */
	public static PluginHint IndependentWindow = new PluginHint();

	/** A plugin started this way will share its parent's window. */
	public static PluginHint OverlayWindow = new PluginHint();

	/** A plugin started this way will be rendered by its parent. */
	public static PluginHint NoWindow = new PluginHint();

}
