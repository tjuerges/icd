/*
 * Created on Nov 29, 2010 by mschilli
 */
package alma.exec.extension;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;



/**
 * Base class for plugins that want to be rendered
 * as a status widget for immediate user information.
 */
public class MiniStatus extends JLabel {


	// msc: i'm keeping the constructors simple for now.
	// i will probably come back and add more later.

	protected MiniStatus () {
		commonInit();
	}


	protected void commonInit () {
		// With rounded corners - doesn't look good
		// setBorder(new LineBorder(Color.BLACK, 1, true));
		setBorder(new CompoundBorder(LineBorder.createGrayLineBorder(), new EmptyBorder(0, 3, 0, 3)));

		// necessary for blinking
		setOpaque(true);
	}

	
	// ########################################
	// Hyperlink behaviour
	// ########################################

	
	/**
	 * Populate text and tooltip from action settings.
	 */
	public void setLink (Action linkAction) {
		this.hyperlinkAction = linkAction;

		if (linkAction != null) {
			Object linkText = linkAction.getValue(Action.NAME);
			if (linkText instanceof String)
				/* using "html" rendering mode turned out disadvantageous, because it prevents
				 * tweaking many other style settings like setEnabled() and setForeground(). */
				setText((String) linkText);

			Object tooltip = linkAction.getValue(Action.SHORT_DESCRIPTION);
			if (tooltip instanceof String)
				setToolTipText((String) tooltip);
		}

		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addMouseListener(mouseLi);

		setVisited(false);
	}


	public void unsetLink () {
		hyperlinkAction = null;
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		removeMouseListener(mouseLi);
		setVisited(true);
	}

	private MouseLi mouseLi = new MouseLi();


	// "Visited" handling
	// =========================================

	protected Icon visitedIcon;
	protected Icon unvisitedIcon;
	
	protected boolean tracksVisited = false;

	/**
	 * If set to true (default is false) this label will automatically go to "visited"
	 * state when the user activates the link.
	 */
	public void setTracksVisited (boolean b) {
		tracksVisited = b;
	}

	public void setVisited (boolean b) {
		/*
		 * couldn't work out how to render "underline" thus resorting to bold/plain face.
		 */
		if (b) {
			setFont(getFont().deriveFont(Font.PLAIN));
			setIcon (visitedIcon);
		} else {
			setFont(getFont().deriveFont(Font.BOLD));
			setIcon (unvisitedIcon);
		}
		
		Container parent = getParent();
		if (parent != null)
			parent.validate();
	}

	public void setVisitedIcon (Icon icon) {
		visitedIcon = icon;
	}

	public void setUnvisitedIcon (Icon icon) {
		unvisitedIcon = icon;
	}
	

	// Detect "link activation"
	// =========================================

	private static int actionEventId = 1000;
	protected Action hyperlinkAction;

	protected class MouseLi extends MouseAdapter {

		@Override
		public void mouseClicked (MouseEvent e) {
			if (tracksVisited)
				setVisited(true);

			if (hyperlinkAction != null) {
				ActionEvent evt = new ActionEvent(this, actionEventId++, "link", e.getModifiers());
				hyperlinkAction.actionPerformed(evt);
			}
		}
	}

	
	// ########################################
	// Blinking behaviour
	// ########################################

	private static Timer timer = new Timer(MiniStatus.class.getSimpleName()+" Timer", true);
	private TimerTask task; 

	public void setBlinking (boolean b) {
		if (b) {
			task = new TimerTask() {@Override public void run () {
				togglePaintModeTo(!doPaintBlinked);
			}};
			timer.schedule(task, 250, 750);

		} else {
			if (task != null) {
				task.cancel();
				task = null;
			}
			togglePaintModeTo(false);
		}

		if (allowsAcknowledgement)
			assignHandCursor(b);
	}

	/*
	 * When painting this as "blinked", we prevent the inherited Swing code from painting
	 * the background by turning opacity off, and instead paint the background ourselves.
	 * This is much preferable over using setBackground() to implement blinking: i) it
	 * allows setting the background without interfering with blinking, and ii) calls to
	 * setBackground() trigger a repaint which can lead to cascades of repaints.
	 */

	private boolean doPaintBlinked = false;

	private void togglePaintModeTo (boolean b) {
		doPaintBlinked = b;

		// turn default background off when blinked
		setOpaque(!doPaintBlinked);

		// have repainted in Swing thread
		repaint();
	}


	@Override
	protected void paintComponent (Graphics g) {
		if (doPaintBlinked) {
			// custom background painting
			Color orig = g.getColor();
			g.setColor(getBlinkColor());
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(orig);
		}
		super.paintComponent(g);
	}

	/**
	 * Compute the blink color, preferrably based
	 * on the current background color, or you
	 * can just return a white color instead.
	 */
	protected Color getBlinkColor() {
		Color bg = getBackground();

		// fades the existing background color. it rather subtle, but looks pretty
		if (bg != null)
			return new Color(bg.getRed(), bg.getGreen(), bg.getBlue(), 85);

		return Color.white;
	}

	// "Acknowledge"
	// =================================
	// This is similar to the "visited" behavior of a html-link.
	// It can only be enabled oneway, simply because that makes 
	// my life easier and is sufficient for my purposes.

	private boolean allowsAcknowledgement = false;

	/**
	 * If this is enabled, the user can stop the button from blinking by clicking on it.
	 */
	public void enableAllowsAcknowledgement () {
		if (allowsAcknowledgement)
			return;

		allowsAcknowledgement = true;
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked (MouseEvent e) {
				setBlinking(false);
			}
		});
	}

	protected void assignHandCursor (boolean b) {
		int c = (b) ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR;
		final Cursor cursor = Cursor.getPredefinedCursor(c);
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				public void run () {
					setCursor(cursor);
				}
			});
		} catch (Exception exc) { /* i don't care really */}
	}



//	public static void main (String[] args) {
//	JFrame f = new JFrame();
//	final MiniStatus ms = new MiniStatus();
//	
//	Action a = new AbstractAction("the name"){
//		@Override
//		public void actionPerformed (ActionEvent e) {
//			System.out.println("action link executing");
//			ms.setBlinking(false);
//		}};
//	a.putValue(Action.SHORT_DESCRIPTION, "the tooltip");
//	ms.setLink(a);
//
//	ms.setBackground(Color.green);
//	ms.setBlinking(true);
//	
//	f.add(ms);
//	f.pack();
//	f.setVisible(true);
//}

	
}


