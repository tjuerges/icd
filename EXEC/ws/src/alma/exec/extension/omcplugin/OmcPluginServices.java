/*
 * Created on Feb 21, 2011 by mschilli
 */
package alma.exec.extension.omcplugin;

import java.util.logging.Logger;

import org.omg.CORBA.ORB;

import alma.acs.container.ContainerServices;
import alma.exec.extension.IBasicPlugin;
import alma.exec.extension.PluginHint;
import alma.exec.extension.subsystemplugin.PluginContainerException;
import alma.exec.extension.subsystemplugin.SessionProperties;



/**
 * The API available to an {@link OmcPlugin}.
 *
 * @author M.Schilling, ESO
 */
public interface OmcPluginServices {

	public Logger getLogger ();

	public SessionProperties getSessionProperties();

	public void publishSelection (Object selType, String selId);
	
	public void stopPlugin (IBasicPlugin plugin) throws PluginContainerException;
	
	public void startChildPlugin(String childName, IBasicPlugin child, PluginHint hint) throws PluginContainerException;

	public ContainerServices getContainerServices();

	public ORB getOrb();

}


