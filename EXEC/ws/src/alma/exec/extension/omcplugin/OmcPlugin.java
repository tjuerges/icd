/*
 * Created on Feb 21, 2011 by mschilli
 */
package alma.exec.extension.omcplugin;

import alma.exec.extension.IBasicPlugin;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;


/**
 * A plugin to the OMC.
 * <p>
 * Opposed to {@link SubsystemPlugin}, it starts
 * instantly without waiting for ACS to be up and running. Nonetheless it can use the
 * container services once the OMC has connected to ACS.
 */
public interface OmcPlugin extends IBasicPlugin {

	/**
	 * Called by the plugin framework to provide this plugin
	 * with an API that i can use.
	 */
	public void setServices (OmcPluginServices services);

}
