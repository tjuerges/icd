/*
 * Created on Mar 07, 2006 by mschilli
 */
package alma.exec.extension.subsystemplugin;



/**
 * Interface to implement for a subsystem plugin that
 * supports pausing and resuming.
 *
 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin
 * @author mschilli
 */
public interface IPauseResume {
	
	
	/**
	 * You have been requested to pause.
	 * 
	 * <p>Suspend any activities:<ul>
	 * <li> no more polling of values
	 * <li> possibly no more updating of your display
	 * </ul></p>
	 * 
	 * <p> This may be called various times during the lifetime of an instance
	 * of your plugin class</p>
	 * <p> You may propagate exceptions that you don't want or cannot handle
	 * yourself up to the framework </p>
	 * 
	 * @throws Exception any exception you cannot recover from 
	 */
	public void pause() throws Exception;
	
	/**
	 * You have been requested to resume after a pause.
	 * 
	 * <p>Carry on with any suspended activities</p>
	 * 
	 * <p> This may be called various times during the lifetime of an instance
	 * of your plugin class</p>
	 * <p> You may propagate exceptions that you don't want or cannot handle
	 * yourself up to the framework </p>
	 * 
	 * @throws Exception any exception you cannot recover from 
	 */
	public void resume() throws Exception;
		
	
}
