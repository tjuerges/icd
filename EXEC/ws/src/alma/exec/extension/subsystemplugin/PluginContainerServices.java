/*
 * Created on Jan 26, 2005 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import java.util.logging.Logger;

import alma.acs.container.ContainerServices;


/**
 * The API that can be used by SubsystemPlugin implementations.
 * 
 * @author mschilli
 */
public interface PluginContainerServices extends ContainerServices {

	
	/**
	 * Returns a logger
	 * 
	 * @return a logger
	 */
	public Logger getExecLogger ();


	/**
	 * Returns values from the ExecConfig.
	 * 
	 * @param name the name of a config-file property
	 * @return the value of the specified config-file property
	 */
	public String getProperty (String name);

	
	/**
	 * Registers and runs a SubsystemPlugin that is a delegate of
	 * your current SubsystemPlugin.
	 * <p>
	 * The advantage of a delegate plugin is that the parent plugin has<ul>
	 *   <li> a reference to the delegate so it can communicate with it in arbitrary ways
	 *   <li> the delegate can have an arbitrary constructor which the parent invokes
	 * </ul></p>
	 * 
	 * @param childName name of delegate plugin
	 * @param child     delegate plugin instance
	 * @throws PluginContainerException if something goes wrong
	 */
	public void startChildPlugin(String childName, SubsystemPlugin child) throws PluginContainerException;

	
	/**
	 * Stops the specified plugin.
	 * <p>
	 * If the <code>plugin</code> has ever been started (i.e. its <code>start</code> method got invoked by
	 * the framework), this will result in a call to its <code>stop()</code> method and, provided that
	 * method completes successfully, make the plugin disappear from the operator application.
	 * If, opposed to that, the <code>plugin</code> has not been started yet, its <code>stop()</code>
	 * method will not be called, and it will disappear immediately.
	 * </p>
	 * 
	 * <p> 
	 * <b>Note</b><br>
	 * In this version of the Exec Plugin Framework, a plugin is <em>solely</em> allowed to stop itself,
	 * trying to stop any other plugin will raise a <code>PluginContainerException</code>.
	 * In a future version, it may become possible to use this method to stop child plugins, too.</p>
	 * 
	 * @param plugin  the plugin to stop (only self is allowed)
	 * @throws PluginContainerException if something goes wrong
	 */
	public void stopPlugin (SubsystemPlugin plugin) throws PluginContainerException;
	
	
	/**
	 * Returns a dictionary of run-time properties, like for instance 
	 * <ol>
	 * <li> the operator's name
	 * <li> the operator's privileges
	 * <li> currently selected items (SchedBlock, Antenna, etc.)
	 * </ol>
	 * See the documentation of {@link SessionProperties}.
	 */
	public SessionProperties getSessionProperties();
	
	
}

