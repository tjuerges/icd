/*
 * Created on Mar 9, 2005 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

/**
 * Thrown by {@link alma.exec.extension.subsystemplugin.PluginContainerServices} if 
 * something went wrong while executing a request of a SubsystemPlugin.
 *   
 * @author mschilli
 */
public class PluginContainerException extends AcsJContainerServicesEx {

	/**
	 * Creates a PluginContainerException.
	 * @param message the message
	 */
	public PluginContainerException(String message) {
		super();
		setContextInfo(message);
	}

	/**
	 * Creates a PluginContainerException. 
	 * @param message the message
	 * @param cause the cause
	 */
	public PluginContainerException(String message, Throwable cause) {
		super(cause);
		setContextInfo(message+": "+cause.toString());
	}

	/**
	 * Returns a somewhat detailed variation of the standard <code>toString()</code> method.
	 * @return a somewhat detailed variation of the standard <code>toString()</code> method
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName()+": "+getContextInfo();
	}
	
	
}

