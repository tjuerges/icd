package alma.exec.extension.subsystemplugin;

import java.util.Arrays;


public enum ExecRole {

	/** Undetermined role */                       UNKOWN             ("Unknown Role"), /* This is the logical null */
	/** Full control */                            MASTER_OPERATOR    ("Master Operator"),
	/** Control over specific arrays */            ARRAY_OPERATOR     ("Array Operator"),
	/** Limited control over specific arrays */    ASTRONOMER_ON_DUTY ("Astronomer On Duty"),
	/** No control privileges, monitoring only */  MONITOR            ("Monitor"),
	/** Full control, plus debugging privileges */ SOFTWARE_DEVELOPER ("Software Developer"),
	/** Full control, plus hardware privileges */  ENGINEER           ("Engineer");

	private String stringy;

	private ExecRole(String str) {
		this.stringy = str;
	}

	/** Test whether this role is among the specified roles. */
	public boolean isAmong (ExecRole... roles) {
		return (Arrays.asList(roles).contains(this));
	}

	/** @return the role name in human readable form */
	@Override
	public String toString () {
		return stringy;
	}

	/**
	 * Returns the enum constant for the specified human readable role name.
	 * Returns {@code UNKOWN} role if the role name cannot be matched to a constant.
	 * 
	 * @param str - a role string (e.g. "Master Operator")
	 * @return the corresponding role constant ( e.g. MASTER_OPERATOR),
	 *         or UNKNOWN if role string not understood.
	 */
	public static ExecRole fromString (String str) {
		for (ExecRole r : ExecRole.values()) {
			if (r.stringy.equals(str))
				return r;
		}
		return ExecRole.UNKOWN;
	}

}
