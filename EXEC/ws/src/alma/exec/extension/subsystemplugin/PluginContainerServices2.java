/*
 * Created on Jun 8, 2010 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import alma.exec.extension.IRemoteControl;
import alma.exec.extension.PluginHint;


/**
 * An extension of the standard {@link PluginContainerServices}. This offers
 * additional, advanced services to plugins.
 *
 * @since ALMA 8.0
 * @author M.Schilling, ESO
 */
public interface PluginContainerServices2 extends PluginContainerServices {

	/**
	 * Note that it only makes sense to call this method if your plugin implements
	 * the interface {@link IRemoteControl}. Otherwise the call can have no effect.
	 *
	 * @param selType - the kind of selected something (eg. a schedblock)
	 * @param selId   - the Id of the selected something (eg. the schedblock's uid)
	 */
	public void publishSelection (Object selType, String selId);

	/**
	 * Registers and runs a SubsystemPlugin that is a delegate of
	 * your current SubsystemPlugin.
	 * <p>
	 * The advantage of a delegate plugin is that the parent plugin has<ul>
	 *   <li> a reference to the delegate so it can communicate with it in arbitrary ways
	 *   <li> the delegate can have an arbitrary constructor which the parent invokes
	 * </ul></p>
	 * 
	 * As of ALMA 8.1, you can specify hints about the child plugin. The hints determine
	 * the visual appearance of the child. Hints are optional, they default to "Independent".
	 *
	 * @param childName  name of delegate plugin
	 * @param child      delegate plugin instance
	 * @param hint       how to start the plugin (defaults to "Independent")
	 * @throws PluginContainerException if something goes wrong
	 */
	public void startChildPlugin (String childName, SubsystemPlugin child, PluginHint hint) throws PluginContainerException;



}


