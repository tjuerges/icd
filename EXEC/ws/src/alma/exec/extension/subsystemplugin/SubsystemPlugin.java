/*
 * Created on Jan 25, 2005 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import alma.exec.extension.IBasicPlugin;



/**
 * The interface a subsystem plugin needs to implement.
 *  
 * <p> When your plugin gets used, this sequence will happen:<ol>
 * <li> Your constructor gets invoked
 * <li> If the plugin is <code>IStateKeeping</code>, its <code>setState()</code> implementation gets invoked
 * <li> The plugin gets shown on the screen
 * <li> Your <code>setServices()</code> implementation gets invoked
 * <li> Your <code>start()</code> implementation gets invoked
 * </ol> 
 * The amount of time between these steps can vary.
 * </p>
 * 
 * <p> Tip: In previous versions of the Plugin Framework,
 * your plugin was shown on screen only after invoking your <code>start()</code>
 * implementation. Thus, it used to make no difference whether you would
 * put together your Gui in your constructor or in your
 * <code>start()</code> method. This has changed with the Gui-2007-02 FBT.
 * As apparent from the sequence above, it is desirable to construct some or
 * all of the Gui already in the constructor, so the user has something
 * to look at even when the plugin has not yet been <code>start()</code>-ed. </p>
 *
 * <p> Tip: While typically a plugin has a Gui (i.e., the plugin is mediately or
 * immediately a descendant of <code>JComponent</code>), this is not required: it
 * is actually possible to make a <em>gui-less</em> subsystem plugin by writing
 * a class that implements the SubsystemPlugin interface but is not a descendant of 
 * <code>JComponent</code>.</p>
 * 
 *
 * @author mschilli
 */
public interface SubsystemPlugin extends IBasicPlugin {
	
	/**
	 * Provides an API to your plugin. You will want to store it in
	 * your plugin instance.
	 * 
	 * <p> This will be called once after instantiation of your
	 * plugin class, before <code>start()</code>.</p>
	 *  
	 * @param services   an API that can be used by your plugin 
	 */
	public void setServices (PluginContainerServices services);
	
	/**
	 * You have been requested to run.
	 * 
	 * <p>Connect to components, update your Gui (if any), etc.</p>
	 * 
	 * <p> This will be called once in the lifetime of an instance of your
	 * plugin class</p>
	 * <p> You may propagate exceptions that you don't want to (or cannot)
	 * handle yourself up to the framework </p>
	 *  
	 * @throws Exception any exception you cannot recover from 
	 */
	public void start() throws Exception;
	

	/**
	 * You have been requested to terminate.
	 * 
	 * <p>Release components, clean up</p>
	 * 
	 * Your subsystem plugin will disappear from screen after
	 * the framework has invoked your <code>stop()</code> implementation.
	 * 
	 * <p> This may be called more than once at the end of the lifetime of
	 * an instance of your plugin class</p>
	 * <p> You may propagate exceptions that you don't want to (or cannot)
	 * handle yourself up to the framework </p>
	 *  
	 * @throws Exception any exception you cannot recover from 
	 */
	public void stop() throws Exception;
	

	
	/**
	 * You are being requested to run in either <em>restricted</em> or
	 * <em>unrestricted</em> mode.
	 * 
	 * <p>If called with <tt>true</tt> your plugin MUST no longer allow the user
	 * "full access" to the system. Restrict the functionality of your plugin so it <ul>
	 * <li> does NOT call remote methods that "write", i.e. that change the state of remote objects 
	 * <li> does NOT display sensitive data to the user
	 * </ul></p>
	 * 
	 * <p>If called with <tt>false</tt> your plugin MAY unleash all its functionality:<ul>
	 * <li> it may call remote methods that "write", i.e. that change the state of remote objects 
	 * <li> it may display sensitive data to the user
	 * </ul></p>
	 * 
	 * <p> As indicated, your plugin MUST obey a request to run in <em>restricted</em>
	 * mode. It MAY, however, ignore a request to switch to <em>unrestricted</em> mode. To decide
	 * whether to ignore the latter kind of request, your plugin may want some more information,
	 * which it can get through the API that it is provided with in {@link #setServices(PluginContainerServices)}.
	 * Your plugin finally returns what mode it chose to run in, i.e. return <code>true</code> if you
	 * chose to run in restricted mode, whereas return <code>false</code> if you will run in unrestricted mode.
	 * 
	 * <p> Note that this may be called SEVERAL times with different arguments during the
	 * lifetime of an instance of your plugin class</p>
	 * <p> You may propagate exceptions that you don't want to (or cannot)
	 * handle yourself up to the framework </p>
	 *
	 * @param restricted  whether you must run restricted, or might run unrestricted 
	 * @return the mode your plugin chose to run in (true means restricted, false means unrestricted)
	 * 
	 * @throws Exception any exception you cannot recover from 
	 */
	public boolean runRestricted (boolean restricted) throws Exception;
	
	
	
}

