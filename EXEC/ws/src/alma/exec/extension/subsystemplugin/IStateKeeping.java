/*
 * Created on Jun 27, 2007 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import java.io.Serializable;



/**
 * Interface to implement for a subsystem plugin that
 * wants some state information to persist between runs.
 *
 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin
 * @author mschilli
 */
public interface IStateKeeping {

	/**
	 * Your plugin is requested to return its current internal state.
	 * 
	 * <p> The state is an arbitrary object that represents the state
	 * of your plugin. The object must implement the
	 * <code>java.io.Serializable</code> interface. The framework will
	 * save the state, so it can be restored during a subsequent run. </p>
	 * 
	 * <p> Tip: When you return an object of a class that you've defined
	 * yourself, you should probably add a serialVersionUID to the class.
	 * This prevents unnecessary de-serialization errors. Ask Exec if you
	 * have questions. </p>
	 * 
	 * <p> This may be called several times during the lifetime of an instance
	 * of your plugin class</p>
	 * 
	 * @return some object representing the current internal state of your plugin
	 */
	public Serializable getState();
	
	/**
	 * Your plugin is asked to initialize itself from the given state object. 
	 * 
	 * <p> Your plugin should know how to interpret the object: after all, that object
	 * has been returned by <code>getState()</code> in a previous incarnation of your
	 * plugin. Note if no previoiusly saved state exists (or could not be found by
	 * the framework), you will be passed a <code>null</code> here. In that case,
	 * you should probably fall back to some default values.</p>
	 * 
	 * <p> This will be called once at the beginning of the lifetime of an instance
	 * of your plugin class.</p>
	 * <p> You may propagate exceptions that you don't want or cannot handle
	 * yourself up to the framework </p>
	 * 
	 * @param state   the state to accept
	 * @throws Exception any exception you cannot recover from 
	 */
	public void setState (Serializable state) throws Exception;
	
}


