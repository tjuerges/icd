/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2006
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 */
package alma.exec.extension.subsystemplugin;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;


/**
 * Encodes action privileges associated to ALMA roles. It is initiated with the operator
 * role and provides methods for all relevant actions. These action methods return true if
 * the action is permitted for the role.
 * 
 * @author P.Grosbol, ESO, <pgrosbol@eso.org>
 * @author M.Schilling, ESO
 * @since Alma R6.0
 */
public class SessionProperties implements Cloneable {

	// Change Listening
	// ==========================================================

	/**
	 * Add a PropertyChangeListener. The listener is registered for all properties. The
	 * same listener may be added more than once, and will be called as many times.
	 * 
	 * @param listener The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener (PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Add a PropertyChangeListener for a specific property. The same listener may be added
	 * more than once for a property, and will be invoked as many times. If {@code
	 * propertyName} is null, the call will be ignored.
	 * 
	 * @param propertyName The name of the property to listen on.
	 * @param listener The PropertyChangeListener to be added
	 */
	public void addPropertyChangeListener (String propertyName, PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * Remove a PropertyChangeListener that was registered for all properties. If the
	 * listener was added more than once, it will be notified one less time after being
	 * removed. If {@code listener} is null, or was never added, the call will be ignored.
	 * 
	 * @param listener The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener (PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	/**
	 * Remove a PropertyChangeListener for a specific property. If the listener was added
	 * more than once for the specified property, it will be notified one less time after
	 * being removed. If {@code propertyName} is null, or {@code listener} is null or was
	 * never added for the specified property, the call will be ignored.
	 * 
	 * @param propertyName The name of the property that was listened on.
	 * @param listener The PropertyChangeListener to be removed
	 */
	public void removePropertyChangeListener (String propertyName, PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(propertyName, listener);
	}


	// Getters
	// ==========================================================

	/**
	 * The user's login name
	 * @return property "user.name"
	 */
	public String getUserName () {
		return getProperty("user.name");
	}

	/**
	 * The user's role: Engineer, Astronomer, Master, Assistant, Developer, etc.
	 * @return property "user.role"
	 */
	public String getUserRole () {
		return getProperty("user.role");
	}

	/**
	 * The array currently in the foreground of the OMC,
	 * that is the array for which a plugin gets started.
	 * @return property "array.name"
	 */
	public String getArrayName () {
		return getProperty("array.name");
	}

	/**
	 * The component name for the array currently in the foreground
	 * of the OMC, that is the array for which a plugin gets started.
	 * @return property "array.compname"
	 */
	public String getArrayCompName() {
		return getProperty("array.compname");
	}
	
	/**
	 * Whether changes of system state are allowed. Any action which changes the state
	 * of ACS, the full system, or a subsystem master-component falls in this category.
	 * @return property "lifecycle.allowed"
	 */
	public boolean isLifeCycleAllowed () {
		return Boolean.parseBoolean(getProperty("lifecycle.allowed"));
	}

	/**
	 * Whether power cycling of equipment is allowed. This action related to
	 * activation of power outlets on power distribution units through SNMP.
	 * @return property "powercycle.allowed"
	 */
	public boolean isPowerCycleAllowed () {
		return Boolean.parseBoolean(getProperty("powercycle.allowed"));
	}

	/**
	 * Whether remote execution of scripts is allowed. Execution of scripts
	 * or tasks on remote systems through SNMP is included in this action.
	 * @return property "systemscripts.allowed"
	 */
	public boolean isSystemScriptsAllowed () {
		return Boolean.parseBoolean(getProperty("systemscripts.allowed"));
	}

	/**
	 * Whether activating the EmergencyStop button is allowed. This action
	 * privilege allows to activate the Emergency stop procedure.
	 * @return property "emergencystop.allowed"
	 */
	public boolean isEmergencyStopAllowed () {
		return Boolean.parseBoolean(getProperty("emergencystop.allowed"));
	}

	/**
	 * Whether changes to the current array are allowed. Creation,
	 * change and destruction of arrays are included.
	 * @return property "array.allowed"
	 */
	public boolean isArrayAllowed () {
		return Boolean.parseBoolean(getProperty("array.allowed"));
	}

	/**
	 * Whether changes to antennas are allowed. Actions changing
	 * state for antennas are in the category.
	 * @return property "antenna.allowed"
	 */
	public boolean isAntennaAllowed () {
		return Boolean.parseBoolean(getProperty("antenna.allowed"));
	}

	/**
	 * Whether changes to SchedBlocks are allowed. Any action related
	 * to execution, termination or change of SB's is in this group.
	 * @return property "schedblock.allowed"
	 */
	public boolean isSchedBlockAllowed () {
		return Boolean.parseBoolean(getProperty("schedblock.allowed"));
	}


	/** Property containing the ID of the currently selected Schedblock */
	public static final String SEL_SCHEDBLOCK = "selection.schedblock";

	/** Property containing the ID of the currently selected Execblock */
	public static final String SEL_EXECBLOCK = "selection.execblock";

	/** Property containing the ID of the currently selected Array */
	public static final String SEL_ARRAY = "selection.array";

	/** Property containing the ID of the currently selected Antenna */
	public static final String SEL_ANTENNA = "selection.antenna";

	/** Property containing the ID of the currently selected Device */
	public static final String SEL_DEVICE = "selection.device";

	/** Property containing the ID of the currently selected Acs Container */
	public static final String SEL_ACSCONTAINER = "selection.acscontainer";

	/** Property containing the ID of the currently selected Acs Component */
	public static final String SEL_ACSCOMPONENT = "selection.acscomponent";

	/** Property containing the ID of the currently selected Subsystem */
	public static final String SEL_SUBSYSTEM = "selection.subsystem";
	
	/** Property containing the currently selected Timestamp (UTC, as OMG nanos) */
	public static final String SEL_TIMESTAMP = "selection.timestamp";

	/** Property containing the currently selected baseline (ANT1-ANT2 with alphanumeric sorting) */
	public static final String SEL_BASELINE = "selection.baseline";

	// Reading
	// ==========================================================


	/**
	 * Returns an iterator over all property names existing in this map.
	 * 
	 * @return an iterator of property names
	 */
	public Iterator<String> propertyNames () {
		return new TreeSet<String>(store.keySet()).iterator();
	}

	/**
	 * Returns the value of the specified property. A return value of null means that no
	 * such property exists.
	 * 
	 * @return the value of the specified property, or null if the property doesn't exist
	 *         in this map
	 */
	public String getProperty (String name) {
		String value = store.get(name);
		return value;
	}

	/**
	 * Returns the value of the specified property, or the specified fallback in case the
	 * property does not exist in this map.
	 * 
	 * @return the value of the specified property, or the fallback
	 */
	public String getProperty (String name, String fallback) {
		String value = store.get(name);
		if (value == null)
			return fallback;
		return value;
	}

	/**
	 * Tests whether the specified property exists in this map.
	 * 
	 * @return whether the specified property exists in this map
	 */
	public boolean containsProperty (String name) {
		return store.containsKey(name);
	}

	/**
	 * Returns a string representation of this map, of the form "{abc=123, def=456}".
	 */
	@Override
	public String toString () {
		StringBuilder sb = new StringBuilder("{");
		Iterator<String> iter = propertyNames();
		while (iter.hasNext()) {
			String n = iter.next();
			sb.append(n).append("=").append(getProperty(n));
			if (iter.hasNext())
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Returns an independent copy containing identical values, without copying
	 * any property change listeners subscribed to this object.
    * @return a copy of this, without listeners attached
	 */
 	@Override
	public SessionProperties clone() {
 		SessionProperties ret = new SessionProperties();
 		ret.store.putAll(this.store);
 		return ret;
	}


	// Internal
	// ==========================================================


	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	protected HashMap<String, String> store = new HashMap<String, String>();

	/**
	 * To fill this map. Fires a property change event if the new value differs from the
	 * old value (it may be interesting that null is always considered different from
	 * null).
	 * <p>
	 * Note that we're intending that there shouldn't be null-values in this map - we
	 * strive for the behavior of a Properties instance where a null value means a
	 * property doesn't exist, and such mappings don't appear in a property iteration.
	 * If this is called with a null-value, the mapping will be removed from this map
	 * instead of adding a null-value mapping.
	 * 
	 * @return the previous value
	 * @throws NullPointerException - if name is null
	 */
	/*
	 * maybe this method shouldn't be public but since a plugin can only ruin its own copy
	 * there shouldn't be much harm either.
	 */
	public String setProperty (String name, String value) {
		if (name == null)
			throw new NullPointerException("property name shouldn't be null");

		String old;

		if (value == null)
			old = store.remove(name);
		else
			old = store.put(name, value);

		pcs.firePropertyChange(name, old, value);
		return old;
	}

	/**
	 * Whether any listener has registered for a specific property. This
	 * ignores listeners that have unspecifically registered for all properties.
	 */
	public boolean hasPropertyChangeListener (String propertyName) {
		return (pcs.getPropertyChangeListeners(propertyName).length > 0);
	}

}
