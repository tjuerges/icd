/*
 * Created on Feb 21, 2011 by mschilli
 */
package alma.exec.extension.subsystemplugin;

import java.util.logging.Logger;

import alma.acs.container.ContainerServicesProxy;
import alma.exec.extension.PluginHint;


/**
 * Utility class for people that want to implement the {@link PluginContainerServices} interface.
 * <p>
 * This is typically only of interest for writing test cases or ad-hoc plugin runners, in other words for
 * testing purposes. By using this adapter class, such implementations are shielded up to a certain limit
 * from changes in the PluginContainerServices interface.
 * <p>
 * The other potential use is for someone wanting to extend the PluginContainerServices interface, for real
 * production use. For example if a given choice of plugins would require additional services. This would
 * be analogous to how the PluginContainerServices extend the original ContainerServices from Acs.
 * <p> 
 * The second use case is the reason for this class acting like a proxy, instead of simply providing loads
 * of empty method implementations as an adapter class would normally do.
 *
 * @author M.Schilling, ESO
 */
public class PluginContainerServicesAdapter extends ContainerServicesProxy implements PluginContainerServices2 {

	/**
	 * The PluginContainerServices for which this class acts as a proxy.
	 * <p>
	 * If you are implementing a test case, and thus are in full control over which methods will
	 * be invoked, you are free to pass in {@code null}.
	 */
	public PluginContainerServicesAdapter (PluginContainerServices delegate) {
		super(delegate);
	}

	@Override
	public Logger getExecLogger () {
		if (delegate != null)
			return ((PluginContainerServices) delegate).getExecLogger();
		return null;
	}

	@Override
	public String getProperty (String name) {
		if (delegate != null)
			return ((PluginContainerServices) delegate).getProperty(name);
		return null;
	}

	@Override
	public SessionProperties getSessionProperties () {
		if (delegate != null)
			return ((PluginContainerServices) delegate).getSessionProperties();
		return null;
	}

	@Override
	public void startChildPlugin (String childName, SubsystemPlugin child) throws PluginContainerException {
		if (delegate != null)
			((PluginContainerServices) delegate).startChildPlugin (childName, child);
	}

	@Override
	public void stopPlugin (SubsystemPlugin plugin) throws PluginContainerException {
		if (delegate != null)
			((PluginContainerServices) delegate).stopPlugin (plugin);
	}


	// PluginContainerServices2
	// -------------------------

	public void publishSelection (Object selType, String selId) {
		if (delegate != null)
			((PluginContainerServices2) delegate).publishSelection(selType, selId);
	}
	
	@Override
	public void startChildPlugin (String childName, SubsystemPlugin child, PluginHint hint) throws PluginContainerException {
		if (delegate != null)
			((PluginContainerServices2) delegate).startChildPlugin(childName, child, hint);
	}

}


