/*
 * Created on Mar 22, 2011 by mschilli
 */
package alma.exec.extension;




/**
 * By implementing this interface, a plugin indicates that it is capable of
 * remote-controlling other plugins.
 * <p>
 * When a plugin implements this interface, the plugin framework will decorate the plugin
 * with an additional widget (originally know as the "yellow arrow", later changed to the "shaking hands").
 * That widget allows the user to set up inter-plugin synchronization, which works by passing selection
 * events from the widget-containing plugin to other target plugins. The target plugins are chosen by the
 * user via the widget.
 * 
 *<p><b>Example code</b>
 * <p>
 * A plugin that wants to remote-control other plugins, needs to implement this interface,
 * and needs to send out selections. So it will do something like this:
 * <pre>
 * 
 class MyPlugin implements SubsystemPlugin, IRemoteControl {
   [...]
   public String[] getSelectionTypes() {
      return new String[]{SesionProperties.SEL_SCHEDBLOCK};
   }
   mySBList.addListSelectionListener (
    new ListSelectionListener(){
      public void valueChanged(ListSelectionEvent evt) {
         SchedBlock sel = (SchedBlock) mySBList.getSelectedValue();
         String sbId = (sel==null)? null : sel.getEntityId();
         ((PluginContainerServices2)services).publishSelection (
            SessionProperties.SEL_SCHEDBLOCK, sbId);
      }
    }
   );
}
   </pre>

 * A plugin that offers to be remote-controlled, needs to listen to selection
 * changes. So it will do something like this:
 * <pre>
public void setServices (PluginContainerServices services) {
  services.getSessionProperties().addPropertyChangeListener (
    SessionProperties.SEL_SCHEDBLOCK,
    new PropertyChangeListener(){
      public void propertyChanged (PropertyChangeEvent evt) {
        myTextfield.setText("current selection is "+evt.getNewValue());
      }
    }
  );
}
  </pre> 
 */
public interface IRemoteControl {

	public String[] getSelectionTypes ();

}
