import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import alma.common.sourcecatalog.flatfile.CalibratorFilterTest;
import alma.common.sourcecatalog.flatfile.CatalogueAccessFlatFileTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ CalibratorFilterTest.class, CatalogueAccessFlatFileTest.class })
public class AllTests {
}