package alma.common.sourcecatalog.flatfile;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alma.common.sourcecatalog.CatalogAccess;
import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;

public class CatalogueAccessFlatFileTest {
    private CatalogAccess catalog;

    @Before
    public void setUp() throws Exception {
	catalog = CatalogAccessFlatFile.getInstance();	
    }

    @Test
    public void testAllCalibratorsLoaded() throws IllegalArgumentException, IOException  {
	CatalogQueryParameters query = new CatalogQueryParameters();
	List<Measurement> calibratorList = catalog.query(query);
	assertEquals(7, calibratorList.size()); // 7 SMA Measurements of 3 sources
    }

    @Test
    public void testSourceIdsAssigned() throws IllegalArgumentException, IOException  {
	CatalogQueryParameters query = new CatalogQueryParameters();
	List<Measurement> calibratorList = catalog.query(query);
	
	Integer[] expectedIds = new Integer[] { 1, 2, 2, 2, 3, 3, 3 };
	for (int i = 0; i < calibratorList.size(); i++) {
	    assertEquals(expectedIds[i], calibratorList.get(i).getSourceId());
	}	
    }
    
    
//    public void testQueryCatalogue_PositionRadius() throws IllegalArgumentException, IOException, InvalidCoordException {
//	// Set up a test query
//	CalibratorQueryParameters q = new CalibratorQueryParameters();
//
//	// 00:05:57.1754, +38:20:15.148
//	q.setRightAscension(Convert.HHMMSSToDeg("00:05:57.1754"));
//	q.setDeclination(Convert.DDMMSSToDeg("+38:20:15.148"));
//	q.setRadius(10.0); // degrees
//
//	// Query the catalogue
//	List<Measurement> results = catalog.query(q);
//	assertEquals(3, results.size());
//
//	for (Measurement m : results) {
//	    assertEquals(1, m.getNames().size());
//	    assertTrue(m.getNames().contains("0005+383"));
//	}
//    }
//
//    public void testQueryCatalogue_FrequencyFlux_OVRO() throws IllegalArgumentException, IOException {
//	// Set up a test query
//	CalibratorQueryParameters query = new CalibratorQueryParameters();
//	query.setMinFrequency(CalibratorTable.OVRO_7MM_GHZ);
//	query.setMaxFrequency(CalibratorTable.OVRO_7MM_GHZ);
//	query.setMinFlux(19.0);
//
//	// Check that the expected calibrator has been found
//	for (Measurement m : catalog.query(query)) {
//	    assertTrue(m.getNames().contains("3C279"));
//	}
//	
//	// Second test query
//	query.setMinFrequency(CalibratorTable.FREQGHZ_1MM - 10.0);
//	query.setMaxFrequency(CalibratorTable.FREQGHZ_1MM + 10.0);
//	query.setMinFlux(4.0);
//	query.setMaxFlux(5.0);
//	// Query the catalogue
//	for (Measurement m : catalog.query(query)) {
//	    assertTrue(m.getNames().contains("3C345"));
//	}
//
//	// Second test query
//	query.setMinFrequency(CalibratorTable.OVRO_7MM_GHZ - 10.0);
//	query.setMaxFrequency(CalibratorTable.FREQGHZ_1MM + 10.0);
//	query.setMinFlux(4.0);
//	query.setMaxFlux(5.0);
//	// Query the catalogue
//	List<Measurement> calibratorList = catalog.query(query);
//	assertEquals(7, calibratorList.size());
//    }
}
