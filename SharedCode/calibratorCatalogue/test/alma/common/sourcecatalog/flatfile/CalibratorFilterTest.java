package alma.common.sourcecatalog.flatfile;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.SimpleMeasurement;
import alma.common.sourcecatalog.flatfile.MeasurementFilter;


public class CalibratorFilterTest {
    
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testConeSearch() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().ra(10.0).dec(10.0));
	pool.add(new SimpleMeasurement().ra(11.0).dec(10.0));
	pool.add(new SimpleMeasurement().ra(12.0).dec(10.0));
	pool.add(new SimpleMeasurement().ra(13.0).dec(10.0));
	pool.add(new SimpleMeasurement().ra(14.0).dec(10.0));

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setRightAscension(12);
	query.setDeclination(10);
	query.setRadius(1.5);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(3, results.size());
    }

    @Test
    public void testMaxFlux() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().flux(1.0));
	pool.add(new SimpleMeasurement().flux(2.0));
	pool.add(new SimpleMeasurement().flux(3.0));
	pool.add(new SimpleMeasurement().flux(4.0));
	pool.add(new SimpleMeasurement().flux(5.0));

	Double threshold = new Double(3.0);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setMaxFlux(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	for (Measurement m : results) {
	    assertTrue(m.getFlux().compareTo(threshold) != 1);
	}
	assertEquals(3, results.size());
    }

    @Test
    public void testMinFlux() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().flux(1.0));
	pool.add(new SimpleMeasurement().flux(2.0));
	pool.add(new SimpleMeasurement().flux(3.0));
	pool.add(new SimpleMeasurement().flux(4.0));
	pool.add(new SimpleMeasurement().flux(5.0));

	Double threshold = new Double(3.0);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setMinFlux(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	for (Measurement m : results) {
	    assertTrue(m.getFlux().compareTo(threshold) != -1);
	}
	assertEquals(3, results.size());
    }

    @Test
    public void testMaxFrequency() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().frequency(1.0));
	pool.add(new SimpleMeasurement().frequency(2.0));
	pool.add(new SimpleMeasurement().frequency(3.0));
	pool.add(new SimpleMeasurement().frequency(4.0));
	pool.add(new SimpleMeasurement().frequency(5.0));

	Double threshold = new Double(3.0);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setMaxFrequency(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	for (Measurement m : results) {
	    assertTrue(m.getFrequency().compareTo(threshold) != 1);
	}
	assertEquals(3, results.size());
    }

    @Test
    public void testMinFrequency() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().frequency(1.0));
	pool.add(new SimpleMeasurement().frequency(2.0));
	pool.add(new SimpleMeasurement().frequency(3.0));
	pool.add(new SimpleMeasurement().frequency(4.0));
	pool.add(new SimpleMeasurement().frequency(5.0));

	Double threshold = new Double(3.0);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setMinFrequency(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	for (Measurement m : results) {
	    assertTrue(m.getFrequency().compareTo(threshold) != -1);
	}
	assertEquals(3, results.size());
    }

    @Test
    public void testName() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().name("a"));
	pool.add(new SimpleMeasurement().name("b"));
	pool.add(new SimpleMeasurement().name("c"));
	pool.add(new SimpleMeasurement().name("A"));
	pool.add(new SimpleMeasurement().name("B"));
	pool.add(new SimpleMeasurement().name("C"));

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setName("a");
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(2, results.size());
	for (Measurement m : results) {
	    assertEquals(1, m.getNames().size());
	    assertEquals("a", m.getNames().get(0).toLowerCase());
	}
    }
    
    @Test
    public void testEarlier() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().date(new Date(1L)));
	pool.add(new SimpleMeasurement().date(new Date(2L)));
	pool.add(new SimpleMeasurement().date(new Date(3L)));
	pool.add(new SimpleMeasurement().date(new Date(4L)));
	pool.add(new SimpleMeasurement().date(new Date(5L)));

	Date threshold = new Date(3L);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setEarliestObservation(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(3, results.size());
	for (Measurement m : results) {
	    assertTrue(m.getDateObserved().compareTo(threshold) != -1);
	}
    }
    
    @Test
    public void testLater() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().date(new Date(1L)));
	pool.add(new SimpleMeasurement().date(new Date(2L)));
	pool.add(new SimpleMeasurement().date(new Date(3L)));
	pool.add(new SimpleMeasurement().date(new Date(4L)));
	pool.add(new SimpleMeasurement().date(new Date(5L)));

	Date threshold = new Date(3L);
	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setLatestObservation(threshold);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(3, results.size());
	for (Measurement m : results) {
	    assertTrue(m.getDateObserved().compareTo(threshold) != 1);
	}
    }
 
    @Test
    public void testMaxSources() {
	Collection<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement());
	pool.add(new SimpleMeasurement());
	pool.add(new SimpleMeasurement());
	pool.add(new SimpleMeasurement());
	pool.add(new SimpleMeasurement());

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setMaxSources(3);
	
	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(3, results.size());
    }
    
    @Test
    public void testSortyByFrequency() {
	List<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().frequency(2.0));
	pool.add(new SimpleMeasurement().frequency(4.0));
	pool.add(new SimpleMeasurement().frequency(5.0));
	pool.add(new SimpleMeasurement().frequency(1.0));
	pool.add(new SimpleMeasurement().frequency(3.0));

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setSortBy("Frequency");
	query.setSortAscending(true);

	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(5, results.size());
	Double[] expected = new Double[] { 1.0, 2.0, 3.0, 4.0, 5.0 };
	for (int i=0; i<results.size(); i++) {
	    assertEquals(expected[i], results.get(i).getFrequency());
	}
    }    

    @Test
    public void testSortyByFlux() {
	List<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().flux(2.0));
	pool.add(new SimpleMeasurement().flux(4.0));
	pool.add(new SimpleMeasurement().flux(5.0));
	pool.add(new SimpleMeasurement().flux(1.0));
	pool.add(new SimpleMeasurement().flux(3.0));

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setSortBy("Flux");
	query.setSortAscending(true);

	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(5, results.size());
	Double[] expected = new Double[] { 1.0, 2.0, 3.0, 4.0, 5.0 };
	for (int i=0; i<results.size(); i++) {
	    assertEquals(expected[i], results.get(i).getFlux());
	}
    }    

    @Test
    public void testSortAscending() {
	List<Measurement> pool = new ArrayList<Measurement>();
	pool.add(new SimpleMeasurement().flux(2.0));
	pool.add(new SimpleMeasurement().flux(4.0));
	pool.add(new SimpleMeasurement().flux(5.0));
	pool.add(new SimpleMeasurement().flux(1.0));
	pool.add(new SimpleMeasurement().flux(3.0));

	CatalogQueryParameters query = new CatalogQueryParameters();
	query.setSortBy("Flux");
	query.setSortAscending(false);

	List<Measurement> results = MeasurementFilter.filterMeasurements(pool, query);
	assertEquals(5, results.size());
	Double[] expected = new Double[] { 5.0, 4.0, 3.0, 2.0, 1.0 };
	for (int i=0; i<results.size(); i++) {
	    assertEquals(expected[i], results.get(i).getFlux());
	}
    }    
    
}


