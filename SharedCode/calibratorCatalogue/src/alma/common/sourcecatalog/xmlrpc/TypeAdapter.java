package alma.common.sourcecatalog.xmlrpc;

import alma.archive.sourcecat.dao.Type;
import alma.common.sourcecatalog.SourceType;

public class TypeAdapter implements SourceType {
    private final Type adaptee;

    public TypeAdapter(Type type) {
	adaptee = type;
    }

    @Override
    public Integer getId() {
	return adaptee.getId();
    }

    @Override
    public String getName() {
	return adaptee.getName();
    }
}
