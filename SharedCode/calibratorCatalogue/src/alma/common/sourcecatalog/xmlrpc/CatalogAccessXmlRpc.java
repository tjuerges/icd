package alma.common.sourcecatalog.xmlrpc;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.xmlrpc313.XmlRpcException;
import org.apache.xmlrpc313.client.XmlRpcClient;
import org.apache.xmlrpc313.client.XmlRpcClientConfigImpl;

import alma.archive.sourcecat.dao.Catalogue;
import alma.archive.sourcecat.dao.Source;
import alma.archive.sourcecat.dao.Type;
import alma.common.sourcecatalog.Catalog;
import alma.common.sourcecatalog.CatalogAccess;
import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.SourceType;

public class CatalogAccessXmlRpc implements CatalogAccess {
    private static final XmlRpcClient service = new XmlRpcClient();

    public CatalogAccessXmlRpc(URL url) {
	setEndpoint(url);
    }

    public CatalogAccessXmlRpc() {
	try {
	    URL url = new URL("http://asa.alma.cl/sourcecat/xmlrpc");
	    setEndpoint(url);
	} catch (MalformedURLException e) {
	    // we know this is a well-formed URL
	    throw new RuntimeException(e);
	}
    }

    public void setEndpoint(URL url) {
	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	config.setServerURL(url);
//	config.setEnabledForExtensions(true);
	service.setConfig(config);
    }

    @Override
    public Collection<Catalog> listCatalogs() throws IOException {
	Object[] response;
	try {
	    response = (Object[]) service.execute("sourcecat.listCatalogues", new Object[0]);
	} catch (XmlRpcException e) {
	    throw new IOException(e);
	}

	List<Catalogue> catalogues = Catalogue.convertResponse(response);
	Collection<Catalog> adaptees = new ArrayList<Catalog>();
	for (Catalogue c : catalogues) {
	    adaptees.add(new CatalogueAdapter(c));
	}
	return adaptees;
    }

    @Override
    public Collection<SourceType> listTypes() throws IOException {
	Object[] response;
	try {
	    response = (Object[]) service.execute("sourcecat.listTypes", new Object[0]);
	} catch (XmlRpcException e) {
	    if (e.getCause() instanceof ConnectException)
		throw new IOException("Could not contact catalog server", e);
	    throw new IOException(e);
	}

	List<Type> types = Type.convertResponse(response);
	Collection<SourceType> adapted = new ArrayList<SourceType>();
	for (Type t : types) {
	    adapted.add(new TypeAdapter(t));
	}	
	return adapted;
    }

    @Override
    public Collection<String> listSortFields() throws IOException {
	Object[] response;
	try {
	    response = (Object[]) service.execute("sourcecat.listSearchSortFields", new Object[0]);
	} catch (XmlRpcException e) {
	    if (e.getCause() instanceof ConnectException)
		throw new IOException("Could not contact catalog server", e);
	    throw new IOException(e);
	}

	Collection<String> fields = new ArrayList<String>();
	for (int i = 0; i < response.length; i++) {
	    fields.add((String) response[i]);
	}
	return Collections.unmodifiableCollection(fields);
    }

    @Override
    public alma.common.sourcecatalog.Source getSource(Integer id) throws IOException {
	Object response;
	try {
	    response = service.execute("sourcecat.getSource", new Object[] { id });
	} catch (XmlRpcException e) {
	    throw new IOException(e);
	}
	Source source = Source.convertResponse(response);
	alma.common.sourcecatalog.Source adapted = new SourceAdapter(source);
	return adapted;
    }

    @Override
    public List<Measurement> query(CatalogQueryParameters query) throws IOException {
	Object[] response;
	try {
	    Object[] args = convertToXmlRpcArgs(query);
	    response = (Object[]) service.execute("sourcecat.searchMeasurements", args);
	} catch (XmlRpcException e) {
	    throw new IOException(e);
	}

	List<Measurement> adapted = new ArrayList<Measurement>();
	for (alma.archive.sourcecat.dao.Measurement m : alma.archive.sourcecat.dao.Measurement.convertResponse(response)) {
	    adapted.add(new MeasurementAdapter(m));
	}

	return adapted;
    }

    private Collection<Integer> listCatalogIds() throws IOException {
	Collection<Integer> ids = new ArrayList<Integer>();
	for (Catalog c : listCatalogs()) {
	    ids.add(c.getId());
	}
	return ids;
    }

    private Collection<Integer> listTypeIds() throws IOException {
	Collection<Integer> ids = new ArrayList<Integer>();
	for (SourceType t : listTypes()) {
	    ids.add(t.getId());
	}
	return ids;
    }

    private Object[] convertToXmlRpcArgs(CatalogQueryParameters query) throws IOException {
	Double ra = query.hasRightAscension() ? query.getRightAscension() : -1;
	Double dec = query.hasDeclination() ? query.getDeclination() : -361;
	Double radius = query.hasRadius() ? query.getRadius() : -1;
	Double fLower = query.hasMinFrequency() ? query.getMinFrequency()*1e9 : -1;
	Double fUpper = query.hasMaxFrequency() ? query.getMaxFrequency()*1e9 : -1;
	Double fluxMin = query.hasMinFlux() ? query.getMinFlux() : -1;
	Double fluxMax = query.hasMaxFlux() ? query.getMaxFlux() : -1;
	String name = query.hasName() ? query.getName() : "";

	Integer limit = query.getMaxSources();
	String sortBy = (String) listSortFields().toArray()[0];
	boolean asc = query.getSortAscending();

	Double degreeMin = -1d;
	Double degreeMax = -1d;
	Double angleMin = -361d;
	Double angleMax = -361d;
	Object[] ranges = new Object[0];
	Object[] catalogues = listCatalogIds().toArray();
	Object[] types = listTypeIds().toArray();

	return new Object[] { limit, catalogues, types, name, ra, dec, radius, ranges, fLower, fUpper, fluxMin, fluxMax, degreeMin, degreeMax, angleMin,
		angleMax, sortBy, asc };
    }

    public static void main(String[] args) throws IOException {
	CatalogAccessXmlRpc c = new CatalogAccessXmlRpc(new URL("http://127.0.0.1:8080/sourcecat/xmlrpc"));
	CatalogQueryParameters query = new CatalogQueryParameters();
	// query.setRadius(null);
	query.setMinFrequency(99e9);
	query.setMaxFrequency(100e9);
//	 query.setName("B0212+735");
	for (Measurement m : c.query(query)) {
	    System.out.println(m.getSourceId() + ":" + m.getNames() + ":" + m.getFrequency());
	}

	alma.common.sourcecatalog.Source s = c.getSource(42);
	for (Measurement m : s.getMeasurements()) {
	    System.out.println(m.getFrequency());
	}
    }

}
