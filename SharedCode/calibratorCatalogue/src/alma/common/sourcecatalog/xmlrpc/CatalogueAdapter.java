package alma.common.sourcecatalog.xmlrpc;

import alma.archive.sourcecat.dao.Catalogue;
import alma.common.sourcecatalog.Catalog;

public class CatalogueAdapter implements Catalog {
    private final Catalogue adaptee;
    
    public CatalogueAdapter(Catalogue s) {
	this.adaptee = s;
    }
    
    @Override
    public int getId() {
	return adaptee.getId();
    }

    @Override
    public String getName() {
	return adaptee.getName();
    }

}
