package alma.common.sourcecatalog.xmlrpc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import alma.archive.sourcecat.dao.SourceName;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.utilities.TextUtils;

public class MeasurementAdapter implements Measurement {
    private final alma.archive.sourcecat.dao.Measurement adaptee;
    private final List<String> names;

    public MeasurementAdapter(alma.archive.sourcecat.dao.Measurement adaptee) {
	this.adaptee = adaptee;

	List<String> names = new ArrayList<String>();
	for (SourceName sn : adaptee.getNames()) {
	    names.add(sn.getName());
	}
	this.names = Collections.unmodifiableList(names);
    }

    @Override
    public Integer getId() {
	return adaptee.getId();
    }

    @Override
    public Integer getCatalogueId() {
	return adaptee.getCatalogueId();
    }

    @Override
    public Integer getSourceId() {
	return adaptee.getSourceId();
    }

    @Override
    public Double getRa() {
	return adaptee.getRa();
    }

    @Override
    public Double getRaUncertainty() {
	return adaptee.getRaUncertainty();
    }

    @Override
    public Double getDec() {
	return adaptee.getDec();
    }

    @Override
    public Double getDecUncertainty() {
	return adaptee.getDecUncertainty();
    }

    @Override
    public Double getFrequency() {
	return adaptee.getFrequency();
    }

    @Override
    public Double getFlux() {
	return adaptee.getFlux();
    }

    @Override
    public Double getFluxUncertainty() {
	return adaptee.getFluxUncertainty();
    }

    @Override
    public Double getPolarizationDegree() {
	return adaptee.getDegree();
    }

    @Override
    public Double getPolarizationDegreeUncertainty() {
	return adaptee.getDegreeUncertainty();
    }

    @Override
    public Double getPolarizationAngle() {
	return adaptee.getAngle();
    }

    @Override
    public Double getPolarizationAngleUncertainty() {
	return adaptee.getAngleUncertainty();
    }

    @Override
    public Date getDateObserved() {
	return adaptee.getDateObserved();
    }

    @Override
    public List<String> getNames() {
	return names;
    }

    @Override
    public String getFormattedName() {
	return TextUtils.join(getNames(), "; ");
    }
}
