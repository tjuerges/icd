package alma.common.sourcecatalog.xmlrpc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import alma.archive.sourcecat.dao.SourceName;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.Source;
import alma.common.sourcecatalog.utilities.TextUtils;

public class SourceAdapter implements Source {
    private final Collection<Measurement> measurements;
    private final List<String> names;

    public SourceAdapter(alma.archive.sourcecat.dao.Source adaptee) {
	List<String> names = new ArrayList<String>();
	for (SourceName n : adaptee.getNames()) {
	    names.add(n.getName());
	}
	this.names = Collections.unmodifiableList(names);

	Collection<Measurement> measurements = new ArrayList<Measurement>();
	for (alma.archive.sourcecat.dao.Measurement m : adaptee.getMeasurements()) {
	    measurements.add(new MeasurementAdapter(m));
	}
	this.measurements = Collections.unmodifiableCollection(measurements);
    }

    @Override
    public List<String> getNames() {
	return names;
    }

    @Override
    public Collection<Measurement> getMeasurements() {
	return measurements;
    }

    @Override
    public String getFormattedName() {
	return TextUtils.join(getNames(), "; ");
    }
}
