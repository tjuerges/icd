package alma.common.sourcecatalog.flatfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jsky.coords.CoordinateRadius;
import jsky.coords.WorldCoords;
import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;

public class MeasurementFilter {
    public static List<Measurement> filterMeasurements(Collection<Measurement> pool, CatalogQueryParameters query) {
	List<Measurement> matching = new ArrayList<Measurement>();
	matching.addAll(pool);

	if (query.hasName())
	    matching = matchName(matching, query.getName());
	if (query.hasRadius() && query.hasRightAscension() && query.hasDeclination())
	    matching = matchConeSearch(matching, query.getRightAscension(), query.getDeclination(), query.getRadius());
	if (query.hasMinFlux())
	    matching = matchFluxGreaterThan(matching, query.getMinFlux());
	if (query.hasMaxFlux())
	    matching = matchFluxLessThan(matching, query.getMaxFlux());
	if (query.hasMinFrequency())
	    matching = matchFrequencyGreaterThan(matching, query.getMinFrequency());
	if (query.hasMaxFrequency())
	    matching = matchFrequencyLessThan(matching, query.getMaxFrequency());
	if (query.hasEarliestObservation())
	    matching = matchLater(matching, query.getEarliestObservation());
	if (query.hasLatestObservation())
	    matching = matchEarlier(matching, query.getLatestObservation());
	if (query.hasSortBy())
	    matching = sortBy(matching, query.getSortBy());	
	if (query.getSortAscending() == false)
	    Collections.reverse(matching);
	
	matching = truncate(matching, query.getMaxSources());
	
	
	return matching;
    }

    private static List<Measurement> sortBy(List<Measurement> matching, String sortBy) {
	FlatFileSortField f = FlatFileSortField.byName(sortBy);
	if (f == null)
	    return matching;

	Collections.sort(matching, f.getComparator());
	return matching;
    }

    private static List<Measurement> truncate(List<Measurement> pool, Integer maxSources) {
	if (maxSources == null || pool.isEmpty())
	    return Collections.emptyList();

	if (pool.size() > maxSources) {
	    return new ArrayList<Measurement>(pool.subList(0, maxSources));
	} else {
	    return pool;
	}
    }

    private static List<Measurement> matchConeSearch(Collection<Measurement> pool, Double rightAscension, Double declination, Double radius) {
	if (rightAscension == null || declination == null || radius == null || pool.isEmpty())
	    return Collections.emptyList();

	WorldCoords coords = new WorldCoords(rightAscension, declination);
	// radius must be specified in arcminutes
	CoordinateRadius cr = new CoordinateRadius(coords, radius*60);

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Double ra = m.getRa();
	    Double dec = m.getDec();

	    if (ra != null && dec != null) {
		WorldCoords pos = new WorldCoords(ra, dec);
		if (cr.contains(pos)) {
		    matching.add(m);
		}
	    }
	}
	return matching;
    }

    private static List<Measurement> matchFluxLessThan(List<Measurement> pool, Double threshold) {
	if (threshold == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Double flux = m.getFlux();
	    if (flux != null && flux.compareTo(threshold) <= 0) {
		matching.add(m);
	    }
	}
	return matching;
    }

    private static List<Measurement> matchFluxGreaterThan(List<Measurement> pool, Double threshold) {
	if (threshold == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Double flux = m.getFlux();
	    if (flux != null && flux.compareTo(threshold) >= 0) {
		matching.add(m);
	    }
	}
	return matching;
    }

    private static List<Measurement> matchFrequencyLessThan(List<Measurement> pool, Double threshold) {
	if (threshold == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Double frequency = m.getFrequency();
	    if (frequency != null && frequency.compareTo(threshold) <= 0) {
		matching.add(m);
	    }
	}
	return matching;
    }

    private static List<Measurement> matchFrequencyGreaterThan(List<Measurement> pool, Double threshold) {
	if (threshold == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Double frequency = m.getFrequency();
	    if (frequency != null && frequency.compareTo(threshold) >= 0) {
		matching.add(m);
	    }
	}
	return matching;
    }

    private static List<Measurement> matchName(List<Measurement> pool, String name) {
	if (name == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    for (String s : m.getNames()) {
		if (s.toUpperCase().equals(name.toUpperCase())) {
		    matching.add(m);
		}
	    }
	}
	return matching;
    }

    private static List<Measurement> matchLater(List<Measurement> pool, Date date) {
	if (date == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Date observed = m.getDateObserved();
	    if (observed != null && observed.compareTo(date) != -1) {
		matching.add(m);
	    }
	}
	return matching;
    }

    private static List<Measurement> matchEarlier(List<Measurement> pool, Date date) {
	if (date == null || pool.isEmpty())
	    return Collections.emptyList();

	List<Measurement> matching = new ArrayList<Measurement>();
	for (Measurement m : pool) {
	    Date observed = m.getDateObserved();
	    if (observed != null && observed.compareTo(date) != 1) {
		matching.add(m);
	    }
	}
	return matching;
    }

}
