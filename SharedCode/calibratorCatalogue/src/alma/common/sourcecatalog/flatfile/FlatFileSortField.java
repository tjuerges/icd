package alma.common.sourcecatalog.flatfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;

import alma.common.sourcecatalog.Measurement;

public enum FlatFileSortField {
    UNSORTED("None", null), 
    FREQUENCY("Frequency", new Comparator<Measurement>() {
	@Override
	public int compare(Measurement o1, Measurement o2) {
	    return o1.getFrequency().compareTo(o2.getFrequency());
	}
    }), 
    FLUX("Flux", new Comparator<Measurement>() {
	@Override
	public int compare(Measurement o1, Measurement o2) {
	    return o1.getFlux().compareTo(o2.getFlux());
	}
    }),
    ;

    public static final EnumSet<FlatFileSortField> ALL_FIELDS = EnumSet.allOf(FlatFileSortField.class);

    private final String description;
    private final Comparator<Measurement> comparator;

    FlatFileSortField(String description, Comparator<Measurement> comparator) {
	this.description = description;
	this.comparator = comparator;
    }

    public static Collection<String> listSortFields() {
	Collection<String> names = new ArrayList<String>();
	for (FlatFileSortField f : ALL_FIELDS) {
	    names.add(f.description);
	}
	return names;
    }

    public static FlatFileSortField byName(String name) {
	for (FlatFileSortField f : ALL_FIELDS) {
	    if (f.description.equals(name))
		return f;
	}
	return null;
    }

    public Comparator<Measurement> getComparator() {
	return comparator;
    }
}
