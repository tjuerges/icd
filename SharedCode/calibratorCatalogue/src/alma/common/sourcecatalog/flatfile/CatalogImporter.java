/*
 * Created on Sep 10, 2004
 *
 */
package alma.common.sourcecatalog.flatfile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.SimpleMeasurement;
import alma.common.sourcecatalog.utilities.Convert;
import alma.common.sourcecatalog.utilities.InvalidCoordException;

/**
 * @author ab
 */
public class CatalogImporter {
    private static final String OVRO_CALTABLE = "alma/common/sourcecatalog/flatfile/ovroCalibratorTable.dat";
    private static final String SMA_CALTABLE = "alma/common/sourcecatalog/flatfile/smaCalibratorTable.dat";
    private static final Logger logger = Logger.getLogger(CatalogImporter.class.getName());

    private volatile static CatalogImporter instance;

    String tableFileName = SMA_CALTABLE;

    private final Collection<Measurement> measurements = new ArrayList<Measurement>();

    // Define some frequencies, for the OVRO catalogue
    public static final double FREQGHZ_7MM = 42.827494;
    public static final double FREQGHZ_3MM = 99.930819;
    public static final double FREQGHZ_1MM = 299.792458;

    public static final String UNKNOWN = "unknown";

    /**
     * 
     * Return instance of the CalibratorExpert
     * 
     * @return The instance of the CalibratorExpert
     * 
     **/
    public static CatalogImporter getInstance() throws IOException {
	if (instance == null) {
	    synchronized (CatalogImporter.class) {
		if (instance == null) {
		    instance = new CatalogImporter();
		}
	    }
	}
	return instance;
    }

    public Collection<Measurement> getCalibratorList() {
	return Collections.unmodifiableCollection(measurements);
    }

    private CatalogImporter() throws IOException {
	// Read the calibrator table from file
	try {
	    readCalTable();
	} catch (NumberFormatException e) {
	    logger.log(Level.WARNING, "", e);
	} catch (IOException e) {
	    logger.log(Level.SEVERE, "Catalogue unavailable", e);
	}
    }

    private void readCalTable() throws NumberFormatException, IOException, FileNotFoundException {
	if (tableFileName.equals(OVRO_CALTABLE)) {
	    readOvroCalTable();
	} else if (tableFileName.equals(SMA_CALTABLE)) {
	    readSmaCalTable();
	} else {
	    throw new FileNotFoundException("Unknown calibrator table " + tableFileName + " requested");
	}
    }

    private void readSmaCalTable() throws IOException {
	logger.log(Level.INFO, "CalibratorTable.readSmaCalTable"); // diagnostic

	/*
	 * JavaWS doesn't use the system classloader, so we must obtain the
	 * context classloader instead
	 */
	InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(SMA_CALTABLE); 
	if (is == null) 
	    return;
	
	BufferedReader in = new BufferedReader(new InputStreamReader(is));

	String target;
	// Read and discard the header (3 lines)
	// Read the first header line
	// ("   Source Name          VLA J2000 Coordinates..."
	target = in.readLine();
	// System.out.println ("First header line: " + target); // diagnostic
	// Read the second header line ("Common   IAU(J2000)      RA..."
	target = in.readLine();
	// System.out.println ("Second header line: " + target); // diagnostic
	// Read the third header line ("============================..."
	target = in.readLine();
	// System.out.println ("Third header line: " + target); // diagnostic

	// Read the calibration data
	measurements.clear();
	// String name = UNKNOWN;
	double ra = 0.0;
	double dec = 0.0;
	double frequency = 0.0;
	double flux = 0.0;
	Date date = null;

	int sourceId = 0; 
	double tolerance = 1.0 / 3600 / 1000; // 1 milliarcsecond

	while ((target = in.readLine()) != null) {
	    String[] tokens = target.split(",");

	    List<String> names = new ArrayList<String>();
	    // Name: IAU (common)
	    if (tokens[0].trim().equals("--")) {
		names.add("J" + tokens[1].trim());
	    } else {
		names.add("J" + tokens[1].trim());
		names.add(tokens[0].trim());
	    }

	    // RA
	    ra = 0.0;
	    try {
		ra = Convert.HHMMSSToDeg(tokens[2].trim());
	    } catch (NumberFormatException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    } catch (InvalidCoordException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    }

	    // Dec
	    dec = 0.0;
	    try {
		dec = Convert.DDMMSSToDeg(tokens[3].trim());
	    } catch (NumberFormatException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    } catch (InvalidCoordException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    }

	    // Flux data
	    // Wavelength (m). Convert to frequency (Hz).
	    frequency = 0.0;
	    try {
		frequency = wavelengthMToFrequencyHz(Double.parseDouble(tokens[4].trim()));
	    } catch (NumberFormatException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    }

	    // Flux density
	    flux = 0.0;
	    try {
		flux = Double.parseDouble(tokens[7].trim());
	    } catch (NumberFormatException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    }

	    // Observation date
	    try {
		// format is 07 Oct 2008
		DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ROOT);
		date = df.parse(tokens[5].trim());
	    } catch (ParseException e) {
		logger.log(Level.WARNING, "Error reading calibrator from file - discarding.", e);
		continue;
	    }

	    // Observatory
	    String obs = tokens[6].trim();
	    if (obs.length() == 0) {
		obs = UNKNOWN;
	    }

	    /*
	     * If we find measurements at the same position (within the given
	     * tolerance), get the source ID from a measurement and use it for
	     * our new Measurement too.
	     */
	    CatalogQueryParameters query = new CatalogQueryParameters();
	    query.setRightAscension(ra);
	    query.setDeclination(dec);
	    query.setRadius(tolerance);
	    
	    List<Measurement> l= MeasurementFilter.filterMeasurements(measurements, query);
	    if (l.size() > 0) {
		Measurement m = l.get(0);
		sourceId = m.getSourceId();
	    } else {
		sourceId++;
	    }
	    
	    Measurement m = new SimpleMeasurement().sourceId(sourceId).names(names).ra(ra).dec(dec).frequency(frequency).flux(flux).date(date);
	    measurements.add(m);
	}
	in.close();
    }

    private double wavelengthMToFrequencyHz(double wavelength) {
	final double C = 299792458.0;
	return (C / wavelength);
    }

    /**
     * Table 7: OVRO Phase Calibrators Fluxes (Data as of Sep. 23, 2001) Source
     * R.A. (J2000) Dec.(J2000) 7mmFlux(Jy) 3mmFlux(Jy) 1mmFlux(Jy) 3mmDate
     * 1mmDate J0005+383 00:05:57.18 +38:20:15.15 0.8 0.5 0.3 Jul01 Oct00
     */
    private void readOvroCalTable() throws NumberFormatException, IOException, FileNotFoundException {
	logger.info("CalibratorTable.readOvroCalTable"); // diagnostic

	InputStream is = getClass().getResourceAsStream(OVRO_CALTABLE);
	BufferedReader in = new BufferedReader(new InputStreamReader(is));

	String target;
	target = in.readLine();
	target = in.readLine();

	// Now start reading.
	measurements.clear();
	int i = 0;
	while ((target = in.readLine()) != null) {
	    String[] tokens = target.split("\\t");
	    String name = tokens[0].trim();
	    String s = tokens[1];
	    double ra = 0.0;
	    try {
		ra = Convert.HHMMSSToDeg(s);
	    } catch (NumberFormatException e) {
		logger.log(Level.WARNING, "", e);
	    } catch (InvalidCoordException e) {
		logger.log(Level.WARNING, "", e);
	    }
	    s = tokens[2];
	    double dec = 0.0;
	    try {
		dec = Convert.DDMMSSToDeg(s);
	    } catch (NumberFormatException e1) {
		logger.log(Level.WARNING, "", e1);
	    } catch (InvalidCoordException e1) {
		logger.log(Level.WARNING, "", e1);
	    }
	    float flux7 = (float) 0.0;
	    try {
		flux7 = Float.parseFloat(tokens[3]);
	    } catch (NumberFormatException e2) {
		logger.log(Level.FINE, "", e2);
	    } catch (RuntimeException e6) {
		logger.log(Level.FINE, "", e6);
	    }
	    float flux3 = (float) 0.0;
	    try {
		flux3 = Float.parseFloat(tokens[4]);
	    } catch (NumberFormatException e3) {
		logger.log(Level.FINE, "", e3);
	    } catch (RuntimeException e6) {
		logger.log(Level.FINE, "", e6);
	    }
	    float flux1 = (float) 0.0;
	    try {
		flux1 = Float.parseFloat(tokens[5]);
	    } catch (NumberFormatException e4) {
		logger.log(Level.FINE, "", e4);
	    } catch (RuntimeException e6) {
		logger.log(Level.FINE, "", e6);
	    }
	    String date3 = UNKNOWN;
	    try {
		date3 = tokens[6].trim();

	    } catch (RuntimeException e5) {
		logger.log(Level.FINE, "", e5);
	    }
	    String date1 = UNKNOWN;
	    try {
		date1 = tokens[7].trim();
	    } catch (RuntimeException e6) {
		logger.log(Level.FINE, "", e6);
	    }
	    String date7 = UNKNOWN;

	    Date d1 = parseDate(date1).getTime();
	    Date d3 = parseDate(date3).getTime();
	    Date d7 = parseDate(date7).getTime();

	    measurements.add(new SimpleMeasurement().name(name).ra(ra).dec(dec).frequency(CatalogImporter.FREQGHZ_7MM).flux(new Double(flux7)).date(d7));
	    measurements.add(new SimpleMeasurement().name(name).ra(ra).dec(dec).frequency(CatalogImporter.FREQGHZ_3MM).flux(new Double(flux3)).date(d3));
	    measurements.add(new SimpleMeasurement().name(name).ra(ra).dec(dec).frequency(CatalogImporter.FREQGHZ_1MM).flux(new Double(flux1)).date(d1));

	    i++;
	}
	in.close();
    }

    /**
     * 
     * @param date
     *            String, of form MmmYY (eg. Oct00)
     * @return The given date, as a Calendar
     */
    private Calendar parseDate(String date) {

	// If the date is not set, return a cleared Calendar
	if (date.equals(UNKNOWN) || date.isEmpty()) {
	    Calendar cal = Calendar.getInstance();
	    // Set all the Calendar values to undefined.
	    cal.clear();
	    return cal;
	}

	String monthStr = date.substring(0, 3).toLowerCase();
	int month;

	// The first three characters of the date string should represent the
	// month
	if (monthStr.equals("jan")) {
	    month = Calendar.JANUARY;
	} else if (monthStr.equals("feb")) {
	    month = Calendar.FEBRUARY;
	} else if (monthStr.equals("mar")) {
	    month = Calendar.MARCH;
	} else if (monthStr.equals("apr")) {
	    month = Calendar.APRIL;
	} else if (monthStr.equals("may")) {
	    month = Calendar.MAY;
	} else if (monthStr.equals("jun")) {
	    month = Calendar.JUNE;
	} else if (monthStr.equals("jul")) {
	    month = Calendar.JULY;
	} else if (monthStr.equals("aug")) {
	    month = Calendar.AUGUST;
	} else if (monthStr.equals("sep")) {
	    month = Calendar.SEPTEMBER;
	} else if (monthStr.equals("oct")) {
	    month = Calendar.OCTOBER;
	} else if (monthStr.equals("nov")) {
	    month = Calendar.NOVEMBER;
	} else if (monthStr.equals("dec")) {
	    month = Calendar.DECEMBER;
	} else {
	    return null;
	}

	// The last two characters should be the final two digits of the year
	int year = Integer.parseInt(date.substring(3));
	if (year >= 70) {
	    year += 1900;
	} else {
	    year += 2000;
	}

	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.MONTH, month);
	cal.set(Calendar.YEAR, year);

	return cal;
    }
}
