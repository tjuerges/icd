package alma.common.sourcecatalog.flatfile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alma.common.sourcecatalog.Catalog;
import alma.common.sourcecatalog.CatalogAccess;
import alma.common.sourcecatalog.CatalogQueryParameters;
import alma.common.sourcecatalog.Measurement;
import alma.common.sourcecatalog.SimpleSource;
import alma.common.sourcecatalog.Source;
import alma.common.sourcecatalog.SourceType;

public class CatalogAccessFlatFile implements CatalogAccess {
    private volatile static CatalogAccessFlatFile instance = null;
    private final Collection<Measurement> measurements;
    private final Map<Integer, Collection<Measurement>> bySource = new HashMap<Integer, Collection<Measurement>>();

    public static CatalogAccessFlatFile getInstance() {
	if (instance == null)
	    synchronized (CatalogAccessFlatFile.class) {
		instance = new CatalogAccessFlatFile();
	    }
	return instance;
    }

    private CatalogAccessFlatFile() {
	// Initialise the list of calibrators
	CatalogImporter caltab;
	Collection<Measurement> pool;
	try {
	    caltab = CatalogImporter.getInstance();
	    pool = caltab.getCalibratorList();	
	} catch (IOException e) {
	    e.printStackTrace();
	    pool = Collections.emptyList();
	}

	Collection<Measurement> measurements = new ArrayList<Measurement>();
	measurements.addAll(pool);
	this.measurements = Collections.unmodifiableCollection(measurements);

	for (Measurement m : measurements) {
	    int sourceId = m.getSourceId();
	    if (bySource.containsKey(sourceId)) {
		bySource.get(sourceId).add(m);
	    } else {
		bySource.put(sourceId, new ArrayList<Measurement>());
		bySource.get(sourceId).add(m);
	    }
	}
    }

    @Override
    public Collection<Catalog> listCatalogs() throws IOException {
	Catalog c = new Catalog() {
	    @Override
	    public int getId() {
		return 0;
	    }

	    @Override
	    public String getName() {
		return "SMA";
	    }
	};

	Collection<Catalog> catalogs = new ArrayList<Catalog>();
	catalogs.add(c);
	return Collections.unmodifiableCollection(catalogs);
    }

    @Override
    public List<Measurement> query(CatalogQueryParameters query) {
	List<Measurement> filtered = MeasurementFilter.filterMeasurements(measurements, query);
	return filtered;
    }

    @Override
    public Collection<String> listSortFields() {
	return FlatFileSortField.listSortFields();
    }

    @Override
    public Collection<SourceType> listTypes() {
	return Collections.emptyList();
    }

    @Override
    public Source getSource(Integer id) {
	if (bySource.containsKey(id)) {
	    Collection<Measurement> c = bySource.get(id);
	    return new SimpleSource(c);
	} else {
	    return null;
	}
    }
}
