package alma.common.sourcecatalog;

public interface Catalog {
    public int getId();
    public String getName();
}
