package alma.common.sourcecatalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SimpleSource implements Source {
    private final Collection<Measurement> measurements;

    public SimpleSource(Collection<Measurement> measurements) {
	if (measurements == null || measurements.isEmpty())
	    throw new IllegalArgumentException("SimpleSource must have measurements to wrap");

	Collection<Measurement> c = new ArrayList<Measurement>();
	c.addAll(measurements);
	this.measurements = Collections.unmodifiableCollection(c);
    }

    @Override
    public String getFormattedName() {
	return measurements.iterator().next().getFormattedName();
    }

    @Override
    public List<String> getNames() {
	return measurements.iterator().next().getNames();
    }

    @Override
    public Collection<Measurement> getMeasurements() {
	return measurements;
    }
}