package alma.common.sourcecatalog;

import java.util.Calendar;
import java.util.Date;

import alma.common.calibratorCatalogue.calQueryParameters.CalQueryParameters;

public class CatalogQueryParameters extends CalQueryParameters implements SourceCatalogQueryParameters {
    public static final int UNDEFINED = -999;

    private Integer maxSources = 100;
    private String sortBy;
    private String name = "";

    private boolean sortAscending = true;
    private boolean hasEarliestObservation = false;
    private boolean hasLatestObservation = false;
    private boolean hasName = false;
    private boolean hasSortBy = false;
    
    
    @Override
    public Integer getMaxSources() {
	return maxSources;
    }

    @Override
    public void setMaxSources(Integer max) {
	if (max != null) {
	    maxSources = max;
	}
    }

    @Override
    public String getName() {
	return name;
    }

    @Override
    public void setName(String name) {
	this.name = name;
	hasName = (this.name != null);
    }

    @Override
    public boolean hasName() {
	return hasName;
    }

    @Override
    public void deleteName() {
	setName(null);
    }

    @Override
    public String getSortBy() {
	return sortBy;
    }

    @Override
    public void setSortBy(String field) {
	sortBy = field;
	hasSortBy = field == null ? false : true;
    }

    @Override
    public boolean hasSortBy() {
	return hasSortBy;
    }
    
    @Override
    public void deleteSortBy() {
	setSortBy(null);
    }
    
    @Override
    public void setSortAscending(boolean state) {
	sortAscending = state;
    }

    @Override
    public boolean getSortAscending() {
	return sortAscending;
    }

    public CatalogQueryParameters() {
	super();
    }

    @Override
    public boolean hasEarliestObservation() {
	return hasEarliestObservation;
    }

    @Override
    public boolean hasLatestObservation() {
	return hasLatestObservation;
    }

    /**
     * @param earliestObservation
     *            the earliestObservation to set
     */
    public void setEarliestObservation(Calendar earliestObservation) {
	if (earliestObservation != null) {
	    setEarliestObservation(earliestObservation.getTime());
	} else {
	    super.setEarliestObservation(null);
	    hasEarliestObservation = false;
	}
    }

    @Override
    public void setEarliestObservation(Date earliestObservation) {
	super.setEarliestObservation(earliestObservation);
	if (earliestObservation != null) {
	    hasEarliestObservation = true;
	} else {
	    hasEarliestObservation = false;
	}
    }

    @Override
    public void setLatestObservation(Date latestObservation) {
	super.setLatestObservation(latestObservation);
	if (latestObservation != null) {
	    hasLatestObservation = true;
	} else {
	    hasLatestObservation = false;
	}
    }

    /**
     * @param latestObservation
     *            the latestObservation to set
     */
    public void setLatestObservation(Calendar latestObservation) {
	if (latestObservation != null) {
	    setLatestObservation(latestObservation.getTime());
	} else {
	    super.setLatestObservation(null);
	    hasLatestObservation = false;
	}
    }

    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("CalQueryParams: ");
	sb.append(hasName() ? "  Name: " + getName() : "");
	sb.append(hasRightAscension() ? "  RA(mas): " + getRightAscension() : "");
	sb.append(hasDeclination() ? "  Dec(mas): " + getDeclination() : "");
	sb.append(hasRadius() ? "  Radius: " + getRadius() : "");
	sb.append(hasMinFrequency() ? "  MinFreq: " + getMinFrequency() : "");
	sb.append(hasMaxFrequency() ? "  MaxFreq: " + getMaxFrequency() : "");
	sb.append(hasMinFlux() ? "  MinFlux: " + getMinFlux() : "");
	sb.append(hasMaxFlux() ? "  MaxFlux: " + getMaxFlux() : "");
	sb.append(hasEarliestObservation() ? "  EarliestObs: " + getEarliestObservation() : "");
	sb.append(hasLatestObservation() ? "  LatestObs: " + getLatestObservation() : "");
	sb.append("  MaxSources: " + getMaxSources());
	sb.append("  Sort By: " + getSortBy());

	return sb.toString();
    }
}
