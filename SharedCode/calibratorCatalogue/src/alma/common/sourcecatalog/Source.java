package alma.common.sourcecatalog;

import java.util.Collection;
import java.util.List;

public interface Source {
    public String getFormattedName();
    public List<String> getNames();
    public Collection<Measurement> getMeasurements();
}