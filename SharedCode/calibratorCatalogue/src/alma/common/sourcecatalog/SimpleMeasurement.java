package alma.common.sourcecatalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import alma.common.sourcecatalog.utilities.TextUtils;

public class SimpleMeasurement implements Measurement {
    private Double flux;
    private Double frequency;
    private List<String> names = new ArrayList<String>();
    private Double ra;
    private Double dec;
    private Date date;
    private Integer sourceId;
    private Integer catalogId;
    
    public SimpleMeasurement date(Date date) {
	this.date = date;
	return this;
    }

    public SimpleMeasurement names(List<String> names) {
	this.names.clear();
	this.names.addAll(names);
	return this;
    }
    
    public SimpleMeasurement name(String name) {
	names.clear();
	names.add(name);
	return this;
    }

    public SimpleMeasurement sourceId(Integer id) {
	this.sourceId = id;
	return this;
    }

    public SimpleMeasurement catalogId(Integer id) {
	this.catalogId = id;
	return this;
    }

    public SimpleMeasurement flux(Double flux) {
	this.flux = flux;
	return this;
    }

    public SimpleMeasurement ra(Double ra) {
	this.ra = ra;
	return this;
    }

    public SimpleMeasurement dec(Double dec) {
	this.dec = dec;
	return this;
    }

    public SimpleMeasurement frequency(Double frequency) {
	this.frequency = frequency;
	return this;
    }

    @Override
    public Integer getId() {
	return null;
    }

    @Override
    public Integer getCatalogueId() {
	return catalogId;
    }

    @Override
    public Integer getSourceId() {
	return sourceId;
    }

    @Override
    public Double getRa() {
	return ra;
    }

    @Override
    public Double getRaUncertainty() {
	return null;
    }

    @Override
    public Double getDec() {
	return dec;
    }

    @Override
    public Double getDecUncertainty() {
	return null;
    }

    @Override
    public Double getFrequency() {
	return frequency;
    }

    @Override
    public Double getFlux() {
	return flux;
    }

    @Override
    public Double getFluxUncertainty() {
	return null;
    }

    @Override
    public Double getPolarizationDegree() {
	return null;
    }

    @Override
    public Double getPolarizationDegreeUncertainty() {
	return null;
    }

    @Override
    public Double getPolarizationAngle() {
	return null;
    }

    @Override
    public Double getPolarizationAngleUncertainty() {
	return null;
    }

    @Override
    public Date getDateObserved() {
	return date;
    }

    @Override
    public List<String> getNames() {
	return names;
    }

    @Override
    public String getFormattedName() {
	return TextUtils.join(getNames(), "; ");
    }
}
