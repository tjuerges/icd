package alma.common.sourcecatalog;

import java.util.Date;
import java.util.List;

public interface Measurement {

    public Integer getId();

    public Integer getCatalogueId();

    public Integer getSourceId();

    public Double getRa();

    public Double getRaUncertainty();

    public Double getDec();

    public Double getDecUncertainty();

    public Double getFrequency();

    public Double getFlux();

    public Double getFluxUncertainty();

    public Double getPolarizationDegree();

    public Double getPolarizationDegreeUncertainty();

    public Double getPolarizationAngle();

    public Double getPolarizationAngleUncertainty();

    public Date getDateObserved();

    public List<String> getNames();

    public String getFormattedName();

}