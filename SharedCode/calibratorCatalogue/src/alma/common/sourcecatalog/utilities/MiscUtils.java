package alma.common.sourcecatalog.utilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Copied from alma.obsprep.util.MiscUtils
 */

/**
 * A collection of helpers.
 * 
 * This was "alma.obsprep.editors.Utils", it has now moved here under a new
 * name.
 * 
 * @author mschilli
 */

public class MiscUtils {
    

    /**
     * Finds the resource with the given name in the classpath (via the class
     * loader). If the resource is not found there, look it up in the file
     * system.
     * 
     * Before searching in the classpath, Windows-style file separators are
     * replaced by (forward) slashes.
     * 
     * <b>Note: <b><br>
     * To load a resource from the root folder of a jar, the <code>parent</code>
     * argument should be set to <code>null</code>.
     * 
     * @param parent The folder that contains the resource.
     * @param name   The resource's name.
     * 
     * @return The location of the resource; or <code>null</code> if the
     *         resource was not found
     */
    public static URL findResource (File parent, String name){

        URL ret = null;
        String path = null;

        // Some path mangling beforehand
        //-------------------------------
        if (parent == null) {
            // need to use pure name as path when
            // resource is in a jar's root-folder
            path = name;

        } else {
            String parentPath = parent.getPath();

            // append slash if necessary
            if (!parentPath.endsWith(File.separator))
                parentPath += File.separator;

            // concatenate
            path = parentPath + name;
        }

        // Look up resource in the classpath (jar file)
        //---------------------------------------------
        String t = path.replace('\\', '/');

        ClassLoader classLoader;

        // msc(2005-11-10): for benefit of custom class loaders
        //where = "classpath (ContextClassLoader)";
        classLoader = Thread.currentThread().getContextClassLoader();
        ret = classLoader.getResource(t);
        if (ret != null) {
            return ret;
        }

        // keep old behaviour to play it safe (for now)
        //where = "classpath (SystemClassLoader)";
        classLoader = MiscUtils.class.getClassLoader();
        ret = classLoader.getResource(t);
        if (ret != null) {
            return ret;
        }

        // Look up resource in the filesystem
        //-----------------------------------
        // if necessary, prepend current working directory to input path
        if (parent != null && !parent.isAbsolute()) {
            String cwd = System.getProperty("user.dir");
            path = new File(cwd, t).getAbsolutePath();
        }

        //where = "filesystem";
        File abs = new File(path);
        if (abs.canRead()) {
            try {
                ret = FileUtilities.makeURL(path);
            } catch (MalformedURLException e) {/* should never happen*/}

            if (ret != null) {
                return ret;
            }
        }

        return ret;
    }
}
