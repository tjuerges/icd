package alma.common.sourcecatalog.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


/**
 * A collection of utility methods for manipulating files; i.e., methods that
 * really belong to the File class.
 * 
 * @author amchavan
  */
public final class FileUtilities {

    public static final String jpeg = "jpeg";
    public static final String jpg = "jpg";
    public static final String gif = "gif";
    public static final String tiff = "tiff";
    public static final String tif = "tif";
    public static final String xml = "xml";
    public static final String txt = "txt";
    public static final String aot = "aot";

    private static final int BLKSIZE = 10 * 1024;             // 10 kB
    private static final int BUFSIZE = 1 * ( 1024 * 1024 );   // 1 MB
    private static byte[] smallBuf;
    private static byte[] bigBuf;
    private static String curDir = null;

    /**
     * This is a utility class with only static members, ergo hide the constructor from the public
     */
    private FileUtilities() {
        super();
    }
    
    /**
     * Allow a certain degree of control over when our buffers are created and destroyed.
     * Invoke buffersNeeded(true) before a batch of reads, and buffersNeeded(false) after.
     * The same buffers will be used throughout the batch, growing as necessary, but will
     * be released at the end. This is aimed at a compromised between not having the buffers
     * hanging around when we don't need them and not having to reallocate them of the
     * heap for each read in a batch.
     */
    private static int bufferUsers = 0;
    public static void buffersNeeded(boolean yesNo) {
        if (yesNo) {
            // Someone is saying they need the buffers;
            if (bufferUsers++ == 0) {
                // no buffers, so create them
                smallBuf = new byte[BLKSIZE];
                bigBuf   = new byte[BUFSIZE];
            }
        } else {
            if (--bufferUsers == 0) {
                // no user needs the buffers any more, so delete them
                smallBuf = null;
                bigBuf   = null;
            }
        }
    }

    
    /**
     * This class is an extension of ZipEntry,
     * that includes the actual data
     * of the Zip file entry. 
     */
    public static class ZipNtry extends ZipEntry {

        byte data[];
        
        public ZipNtry( ZipEntry e, byte data[] ) {
            super(e);
            this.data = data;
        }
        
        public byte[] getData() {
            return data;
        }
    }
    
    
    /**
     * Get the basename of the input filename; that is,
     * its name without its last extension.
     * 
     * @param filename    The filename to examine.
     * 
     * @return  The basename extension of the input filename.
     *          <br>
     *          For instance: 
     *          <UL>
     *          <LI> "file.xml" --> "file"
     *          <LI> "file" --> "file"
     *          <LI> "file." --> "file"
     *          <LI> "file.xml.gz" --> "file.xml"
     *          </UL>
     */
    public static String getBasename(String filename) {

        int i;
        
        // If the pathname ends with a slash,
        // remove that.
        //-----------------------------------
        i = filename.length() - 1;
        if (i >= 0 && filename.charAt(i) == '/') {
            filename = filename.substring(0, i);
        }
        
        // If there is no extension, just return what
        // we have.
        //-------------------------------------------
        i = filename.lastIndexOf('.');
        if (i < 0) {
            return filename;
        }
            
        final String name = filename.substring(0, i);
        return name;
    }

    /**
     * Get the basename of the file's pathname; that is,
     * its name without its last extension.
     * 
     * @param f The file to examine.
     * 
     * @return  The basename extension of the input file f.
     *          <br>
     *          For instance: 
     *          <UL>
     *          <LI> "file.xml" --> "file"
     *          <LI> "file" --> "file"
     *          <LI> "file." --> "file"
     *          <LI> "file.xml.gz" --> "file.xml"
     *          </UL>
     */
    public static String getBasename(File f) {
        final String s = f.getName();
        return getBasename(s);
    }

    /**
     * Get the basename of the URL; that is, the URL's last
     * component, without any extensions.
     * 
     * @param url The URL to examine.
     * 
     * @return  The basename extension of the input URL.
     *          <br>
     *          For instance: 
     *          <UL>
     *          <LI> "file:targets.test" --> "targets"
     *          <LI> "http://www.eso.org/" --> "www.eso"
     *          <LI> "http://www.eso.org/~amchavan/" --> "~amchavan"
     *          <LI> "http://www.eso.org/~amchavan/tmp/targets.txt" --> "targets"
     *          </UL>
     */
    public static String getBasename(URL url) {

        String s = url.toExternalForm();
        final int i = s.indexOf(":");
        if (i >= 0) {
            s = s.substring(i + 1);
        }
        final File f = new File(s);
        return getBasename(f);
    }

    /**
     * Get the extension component of the file's pathname; that is,
     * the characters following the last period char.
     * 
     * @param f The file to examine.
     * 
     * @return  The filename extension of the input file f,
     *          converted to lowercase;
     *          or null if the filename has no extension at all.
     *          <br>
     *          For instance: 
     *          <UL>
     *          <LI> "file.xml" --> "xml"
     *          <LI> "file" --> null
     *          <LI> "file." --> ""
     *          </UL>
     */
    public static String getExtension(File f) {
        final String s = f.getName();
        return getExtension(s);
    }


    /**
     * Assume the input string is a pathname, and get the
     * extension component; that is,
     * the characters following the last period char.
     * 
     * @param s The string to examine.
     * 
     * @return  The filename extension of the input string s,
     *          converted to lowercase;
     *          or null if the string has no extension at all.
     *          <br>
     *          For instance: 
     *          <UL>
     *          <LI> "file.xml" --> "xml"
     *          <LI> "file" --> null
     *          <LI> "file." --> ""
     *          </UL>
     */
    public static String getExtension(String s) {

        final int i = s.lastIndexOf('.');
        if (i < 0) {
            return null;
        }
            
        final String ext = s.substring(i + 1).toLowerCase();
        return ext;
    }


 	/** The temporary directory */
 	private static File tmpdir = null;
    
 	/**
 	 * Returns the system temporary directory
 	 * 
 	 * @return
 	 * @throws IOException
 	 */
 	public static File getTmpDir() throws IOException {
 		if (tmpdir == null) {
 			// create a temp file just to know the directory
 			File tmpfile = File.createTempFile("ObservingToolTmpFile", "");
 			tmpdir = tmpfile.getAbsoluteFile().getParentFile();
 			tmpfile.delete();
 		}
 		return tmpdir;
 	}
    
    /**
     * Return the content of a Zip file entry as an array of bytes.
     * 
     * @param zipfile  The Zip file to read
     * @param entry    The Zip file entry to read
     * 
     * @return A new array of bytes, representing the entire
     *         contents of the Zip entry.
     * 
     * @throws IOException If something went wrong while reading the file.
     */
    public static byte[] getZipEntry(ZipFile zipfile, ZipEntry entry) 
        throws IOException {
        
        // Note: when dealing with ZipFiles, method ZipEntry.getSize()
        //       returns meaningful values
        //------------------------------------------------------------
        final InputStream s = zipfile.getInputStream(entry);
        final byte[] data = readEntry(s);
        return data;
    }
    
    /**
     * Return the content of the next Zip file entry as an array of bytes.
     * 
     * @param stream The Zip stream to read
     * 
     * @return A new array of bytes, representing the entire
     *         contents of the Zip entry.
     * 
     * @throws IOException If something went wrong while reading the
     *                     input stream.
     */
    public static ZipNtry getZipEntry(ZipInputStream stream) 
        throws IOException {

        // Note: when dealing with ZipInputStreams, method 
        //       ZipEntry.getSize() always returns -1
        //-------------------------------------------------
        final ZipEntry entry = stream.getNextEntry();
        if (entry == null) {
            return null;
        }
                        
        final byte[] data = readEntry(stream);
        entry.setSize(data.length);
        final ZipNtry ret = new ZipNtry(entry, data);
        return ret;
    }

    /* Read data from an InputStream into a byte array.
     * To be used when we know beforehand how much data
     * we are going to read.
     */
//    private static void readEntry( InputStream stream, byte[] data )
//        throws IOException {
//
//        BufferedInputStream bufs = new BufferedInputStream( stream );
//        bufs.read( data, 0, data.length );
//    }
    
    private static void copyBytes(byte[] from, byte[] to, int num, int toOffset) {
        for (int i = 0; i < num; i++) {
            to[i + toOffset] = from[i];
        }
    }
    
    /**
     * Read some data from an InputStream into a byte array.
     * 
     * @param stream - the input stream from which to read
     * @return the bytes read in.
     * @throws IOException if, well, there is a probelm with the IO
     */
    public static byte[] readEntry(InputStream stream)
        throws IOException {
        
        int nTotal = 0;
        //final BufferedInputStream bufs = new BufferedInputStream(stream);
        
        buffersNeeded(true);
        // Read from the input stream into a buffer
        while (true) {
            final int n = stream.read(smallBuf, 0, smallBuf.length);
            if (n < 0) {
                break;
            }
            try {
                copyBytes(smallBuf, bigBuf, n, nTotal);
            } catch (ArrayIndexOutOfBoundsException e) {
                // We have exceded the size of the big buffer, so grow it.
                // I have chosen a pretty arbitrary factor of 2 by which to
                // grow it (which quietly assumes that bigBuf is at least
                // as long as smallBuf) - dclarke.
                byte[] newBigBuf = new byte[bigBuf.length * 2];
                copyBytes(bigBuf, newBigBuf, nTotal, 0);
                copyBytes(smallBuf, newBigBuf, n, nTotal);
                bigBuf = newBigBuf;
            }
            nTotal += n;
        }
        
        // Copy the data we read to the array we return
        final byte[] data = new byte[nTotal];
        copyBytes(bigBuf, data, nTotal, 0);

        buffersNeeded(false);
        
        // Done!
        return data;
    }
    
    
    /**
     * Write a new entry in a Zip file.
     * 
     * @param zipfile  The Zip file to write.
     * @param entry    The Zip file entry to write
     * @param data     The contents of the Zip file entry.    
     * 
     * @throws IOException If something went wrong while reading the file.
     */
    static void putZipEntry(ZipOutputStream zipfile, 
                            ZipEntry entry,
                            byte[] data)
        throws IOException {
                                        
        final int len = (int) entry.getSize();
        zipfile.putNextEntry(entry);
        zipfile.write(data, 0, len);
    }


    /**
     * Write a new entry in a Zip file.
     * 
     * @param zipfile  The Zip file to write.
     * @param path     Pathname of the Zip file entry to write
     * @param data     The contents of the Zip file entry.    
     * 
     * @throws IOException If something went wrong while reading the file.
     */
    public static void putZipEntry(ZipOutputStream zipfile, 
                                   String path,
                                   byte[] data)
        throws IOException {
                                        
        final int len = data.length;
        final ZipEntry entry = new ZipEntry(path);
        entry.setSize(len);
        putZipEntry(zipfile, entry, data);
    }
    
    /**
     * Writes a string to a file.
     * 
     * The resulting file contains only the input string.
     * 
     * @param s            String to write.
     * @param pathname     Pathname of the file to write.
     * @throws IOException If something went wrong while writing the file.
     */
    public static void stringToFile(String s, String pathname)
        throws IOException {
        final File f = new File(pathname);
        stringToFile(s, f);
    }


    
    /**
     * Writes a string to a file.
     * 
     * The resulting file contains only the input string.
     * 
     * @param s String to write.
     * @param f File to write.
     * @throws IOException If something went wrong while writing the file.
     */
    public static void stringToFile(String s, File f) throws IOException {
        final BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write(s, 0, s.length());
        bw.close();
    }

    
    /**
     * Reads a file into a string.
     * 
     * @param filename        Pathname of the file to read.
     * @return The contents of the file.
     * @throws IOException If something went wrong while reading the file.
     */
    public static String fileToString(String filename) throws IOException {
        final File f = new File(filename);
        return fileToString(f);
    }


    
    /**
     * Reads a file into a string.
     * 
     * @param f        File to read.
     * @return The contents of the file.
     * @throws IOException If something went wrong while reading the file.
     */
    public static String fileToString(File f) throws IOException {

        final int chunk = 64000;
        final StringBuffer sb = new StringBuffer();
        final char[] buff = new char[chunk];
        final BufferedReader br = new BufferedReader(new FileReader(f));

        while (br.ready()) {
            final int nread = br.read(buff, 0, buff.length);
            if (nread <= 0) {
                break;
            }
            sb.append(buff, 0, nread);
        }
        br.close();
        return sb.toString();
    }
    
    
    /**
     * Reads a binary file into a byte array.
     * 
     * @param f        File to read.
     * @return The contents of the file.
     * @throws IOException If something went wrong while reading the file.
     */
    public static byte[] fileToBytes(File f) throws IOException {
   	 
   	 BufferedInputStream in = null;
   	 try {
   		 
   		 byte[] data = new byte[ (int)f.length() ];
   		 in = new BufferedInputStream(new FileInputStream(f));

			 int totalRead = 0;
			 while (totalRead < data.length) {
				 int nRead = in.read (data, totalRead, in.available());
			    if (nRead == -1)
			   	 break;
			    totalRead += nRead;
			 }
			 return data;

		} finally {
			try {
				in.close();
			} catch (Exception exc) {/* it was worth the try ... */}
		}

    }
    
   /**
	 * Writes a byte array to a binary file.
	 * @param data - the binary data
	 * @param file - the target file
	 */
	public static void bytesToFile (byte[] data, File f) throws IOException {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(f);
			out.write(data);

		} finally {
			try {
				out.close();
			} catch (Exception exc) {/* it was worth the try ... */}
		}
	}
    
    
    
    /**
     * Convert a String to a URL: if conversion cannot be done directly, see if
     * the string represents a pathname, and if so convert that pathname to a
     * URL.
     * <BR>
     * 
     * Taken from an idea of James Gosling's:
     * <code>http://weblogs.java.net/jag/page4.html#37</code>
     * 
     * @param s  The input String.
     * @return   A URL corresponding to the input string.
     * @throws MalformedURLException  If the input string could not be
     *                                converted to a valid URL.
     */
    public static URL makeURL(String s) throws MalformedURLException {
        
        // input check
        if (s == null || s.length() == 0) {
            throw new IllegalArgumentException("Null or empty input.");
        }
        
        URL url = null;
        final String alldrives = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        boolean   winabs = false;
        
        // See if we got a Windows absolute pathname;
        // that is, a pathname beginning with something
        // like "C:".
        if (s.length() > 1) {
            final char c0 = s.charAt(0);
            final char c1 = s.charAt(1);
            if (c1 == ':' && alldrives.indexOf(c0) != -1) {
                winabs = true;
            }
        }
            
        // If we got a real pathname, or a
        // Windows absolute pathname, return that
        // as a URL.
        //---------------------------------------
        final File f = new File(s);
        if (f.exists() || winabs) {
            url = f.toURI().toURL();
            return url;
        }
        
        // If we don't have a protocol, assume this is
        // some pathname.
        //--------------------------------------------
        if (s.indexOf(':') == -1) {
            s = "file:" + s;
        }
            
        // Otherwise try to convert the input string to
        // to a URL, and return that.
        //---------------------------------------------        
        url = new URL(s);
        return url;
    }
    
    /**
     * @return The basename of the current directory. 
     * @throws IOException if there is any problem establishing where we are.
     */
    public static String getCurDirBasename() throws IOException {
        if (curDir == null) {
            String path = null;
            path = (new File(".")).getCanonicalPath();
            final String last = path.replace('\\', '/');
            final String[] t = last.split("/");
            curDir = t[t.length - 1];
        }
        return curDir;
    }
    
    /**
     * Test to see if we're in a test directory.
     * 
     * @return <code>true</code> if we are, and <code>false</code> if we ain't
     */
    public static boolean isTestDir() {
        try {
            final String base = getCurDirBasename();
            return base.equals("test");
        } catch (IOException whoops) {
            return false;
        }
    }
}
