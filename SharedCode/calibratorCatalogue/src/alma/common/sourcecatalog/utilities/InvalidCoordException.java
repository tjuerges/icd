package alma.common.sourcecatalog.utilities;

/**
 * @author amchavan 
 */


public class InvalidCoordException extends Exception {
	public InvalidCoordException()             { super(); }
	public InvalidCoordException( String msg ) { super( msg ); }
}
