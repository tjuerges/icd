package alma.common.sourcecatalog.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import alma.hla.runtime.obsprep.bo.ValueUnitPair;

/**
 * Utility methods to process text.
 * 
 * @author A M Chavan, 28-Aug-2003
 */

public class TextUtils {

    /**
     * Standard format for ISO date/time strings.
     */
    public final static String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    private static final String SPACES = "                               ";
    private static final String ZEROES = "0000000000000000000000000000000";

    /** Constant defining the single quote character. */
    public static final char QUOTE = '\'';

    public static String padded(String padding, int numDigits, long n) {
	String unpadded = new Long(n).toString();

	// string is already >= field size?
	if (numDigits <= unpadded.length()) {
	    return unpadded;
	}

	return padding.substring(0, numDigits - unpadded.length()) + unpadded;
    }

    public static String padLeft(String s, int numchars, char ch) {
	int initStrSize = s.length();
	if (initStrSize >= numchars)
	    return s; // no padding required
	if (numchars < initStrSize)
	    throw new ArrayIndexOutOfBoundsException("Error number of characters to to small");
	int numToInsert = numchars - s.length();
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < numToInsert; i++) {
	    buf.append("" + ch);
	}
	return buf.toString() + s;
    }

    public static String padded(String padding, int numChars, String unpadded) {

	// string is already >= field size?
	if (numChars <= unpadded.length()) {
	    return unpadded;
	}

	return padding.substring(0, numChars - unpadded.length()) + unpadded;
    }

    public static String rightpadded(String padding, int numChars, String unpadded) {

	// IF input string is already longer than the field size
	// THEN return input string
	if (numChars <= unpadded.length())
	    return unpadded;

	return unpadded + padding.substring(0, numChars - unpadded.length());
    }

    /**
     * Static method to "quote" single quote in a given text.
     * 
     * @param str
     *            String to be processed and "quoted".
     * 
     * @return Processed string.
     */
    public static String quoteSqlString(String str) {

	if (str == null) {
	    return str;
	}
	if (str.indexOf(QUOTE) == -1)
	    return str;

	String out_str = "";

	int len = str.length();
	char character;

	for (int i = 0; i < len; i++) {
	    character = str.charAt(i);
	    if (character == QUOTE)
		out_str = out_str + character;
	    out_str = out_str + character;
	}

	return out_str;
    }

    /**
     * Static method to avoid null values
     * 
     * @param str
     *            String to be processed.
     * 
     * @return Processed string.
     */
    public static String getEmptySqlStringIfNull(String str) {

	if (str == null) {
	    return "";
	}
	return str;
    }

    public static String spacePadded(int numDigits, long n) {
	return padded(SPACES, numDigits, n);
    }

    public static String zeroPadded(int numDigits, long n) {
	return padded(ZEROES, numDigits, n);
    }

    public static String zeroPadded(int numDigits, String s) {
	return padded(ZEROES, numDigits, s);
    }

    public static String padString(String s, int newLen) {
	if (s.length() >= newLen)
	    return s;
	StringBuffer buf = new StringBuffer(s);
	int padLen = newLen - s.length();
	for (int i = 0; i < padLen; i++)
	    buf.append(" ");
	return buf.toString();
    }

    public static String escapedString(String original) {
	final int len = original.length();
	StringBuffer buf = new StringBuffer();

	// Escape backslashes, newlines or doublequotes
	for (int k = 0; k < len; k++) {
	    char ch = original.charAt(k);
	    switch (ch) {
	    case '\\':
	    case '\"':
		buf.append("\\");
		buf.append("" + ch);
		break;

	    case '\r':
	    case '\n':
		buf.append("\\n");
		break;

	    default:
		buf.append("" + ch);
		break;
	    }
	}

	return buf.toString();
    }

    public static String revertEscapedString(String original) {
	final int len = original.length();
	StringBuffer buf = new StringBuffer();

	// Escape backslashes, newlines or doublequotes
	for (int k = 0; k < len; k++) {
	    char ch = original.charAt(k);
	    if (ch == '\\') {
		k++;
		if (k < len) // A M Chavan, 09-Aug-2001
		    ch = original.charAt(k);
		switch (ch) {
		case 'n':
		    buf.append("\n");
		    break;
		case 'r':
		    buf.append("\r");
		    break;
		default:
		    buf.append("" + ch);
		}
	    } else
		buf.append("" + ch);

	}
	return buf.toString();
    }

    /**
     * Returns the path corresponding to the package containing the given class.
     * For example, pkgToPath(org.eso.ohs.p2pp.P2PP.class) = "org/eso/ohs/p2pp"
     * 
     * @param cls
     *            Some class contained in an explicit package.
     * 
     * @return relative path to the directory containing the class
     */
    public static String pkgToPath(Class<?> cls) {
	String path = cls.getName();
	int index = path.lastIndexOf('.');

	if (index < 0) {
	    return ""; // Not contained in an explicit package
	}

	path = path.substring(0, index);

	while ((index = path.indexOf('.')) > 0) {
	    path = path.substring(0, index) + File.separator + path.substring(index + 1);
	}
	return path + File.separator;
    }

    public static String convertFileToString(File f) {
	char[] buff = new char[64000];
	StringBuffer sb = new StringBuffer();
	try {
	    BufferedReader br = new BufferedReader(new FileReader(f));
	    while (br.ready()) {
		int nread = br.read(buff, 0, buff.length);
		if (nread <= 0) {
		    break;
		}
		sb.append(buff, 0, nread);
	    }
	    br.close();
	} catch (Throwable t) {
	    sb.append("Failed to read file contents.");
	}
	return sb.toString();
    }

    /**
     * Convert a string to an array of strings. If trim is true it cuts the
     * blanks or tab, an empty string returns an empty array (0 lenght).
     * 
     * @param s
     *            the origin
     * @param sep
     *            separator
     * @return String[]
     */
    public static String[] stringToArray(String s, String sep, boolean trim) {
	// convert a String s to an Array, the elements
	// are delimited by sep
	StringBuffer buf = new StringBuffer(s);
	int arraysize = 1;
	for (int i = 0; i < buf.length(); i++) {
	    if (sep.indexOf(buf.charAt(i)) != -1)
		arraysize++;
	}
	String[] elements = new String[arraysize];
	int y, z = 0;
	if (buf.toString().indexOf(sep) != -1) {
	    while (buf.length() > 0) {
		if (buf.toString().indexOf(sep) != -1) {
		    y = buf.toString().indexOf(sep);
		    if (y != buf.toString().lastIndexOf(sep)) {
			elements[z] = buf.toString().substring(0, y);
			z++;
			buf.delete(0, y + 1);
		    } else if (buf.toString().lastIndexOf(sep) == y) {
			elements[z] = buf.toString().substring(0, buf.toString().indexOf(sep));
			z++;
			buf.delete(0, buf.toString().indexOf(sep) + 1);
			elements[z] = buf.toString();
			z++;
			buf.delete(0, buf.length());
		    }
		}
	    }
	} else {
	    elements[0] = buf.toString();
	}
	buf = null;
	if (trim) {
	    elements = trimStringArray(elements);
	}
	return elements;
    }

    private static String[] trimStringArray(String[] elements) {

	for (int i = 0; i < elements.length; i++) {
	    elements[i] = elements[i].trim();
	}
	// if trim, an ampty string returns an empty array
	if (elements.length == 1 && elements[0].length() == 0) {
	    elements = new String[0];
	}
	return elements;
    }

    /**
	 */
    public static String arrayToString(String s[], String sep, boolean trim) {
	int k;
	String result = "";
	if (s == null) {
	    k = 0;
	} else {
	    if (trim) {
		trimStringArray(s);
	    }
	    k = s.length;
	}
	if (k > 0) {
	    result = s[0];
	    for (int i = 1; i < k; i++) {
		result += sep + s[i];
	    }
	}
	return result;
    }

    public static String[] splitString(String s, int size) {
	Vector<String> strList = new Vector<String>();
	for (int i = 0; i < s.length(); i += size) {
	    String substr;
	    if (i + size > s.length()) {
		substr = s.substring(i);
	    } else {
		substr = s.substring(i, i + size);
	    }
	    strList.addElement(substr);
	}
	String[] splitLines = new String[strList.size()];
	strList.copyInto(splitLines);
	return splitLines;
    }

    /******
     * public static void main(String[] args) {
     * pkgToPath(org.eso.ohs.dfs.ObservationBlock.class);
     * pkgToPath(org.eso.ohs.icdVcs.BOB.class);
     * pkgToPath(org.eso.ohs.gui.widgets.LogWindow.class); } /
     ******/

    /**
     * Sort the strings and return them (in the same array as passed in).
     * 
     * @param in
     *            the array of Strings to be sorted.
     * @return the sorted String array
     */

    public static String[] sort(String[] in) {
	Vector<String> out = new Vector<String>();
	for (int i = 0; i < in.length; i++) {
	    String str = in[i];
	    int o = 0;
	    for (; o < i; o++) {
		if (str.compareTo(out.elementAt(o)) <= 0) {
		    break;
		}
	    }
	    if (o == i) {
		out.addElement(str);
	    } else {
		out.insertElementAt(str, o);
	    }
	}
	for (int i = 0; i < in.length; i++) {
	    in[i] = out.elementAt(i);
	}
	return in;
    }

    /**
     * Sort the array of Sortable objects, and return them (in the same array as
     * passed in).
     * 
     * @param in
     *            the array of Sortable objects to be sorted.
     * @return the sorted array of Sortable objects.
     */

    public static TextUtils.Sortable[] sort(TextUtils.Sortable[] in) {
	Vector<Sortable> out = new Vector<Sortable>();
	for (int i = 0; i < in.length; i++) {
	    String str = in[i].getSortKey();
	    int o = 0;
	    for (; o < i; o++) {
		if (str.compareTo((out.elementAt(o)).getSortKey()) <= 0) {
		    break;
		}
	    }
	    if (o == i) {
		out.addElement(in[i]);
	    } else {
		out.insertElementAt(in[i], o);
	    }
	}
	for (int i = 0; i < in.length; i++) {
	    in[i] = out.elementAt(i);
	}
	return in;
    }

    /**
     * Sort the array of Sortable objects, and return them (in the same array as
     * passed in).
     * 
     * @param in
     *            the array of SortableNumber objects to be sorted.
     * @return the sorted array of Sortable objects.
     */

    public static TextUtils.SortableNumber[] sort(TextUtils.SortableNumber[] in) {
	Vector<SortableNumber> out = new Vector<SortableNumber>();
	for (int i = 0; i < in.length; i++) {
	    double val = in[i].getSortKey();
	    int o = 0;
	    for (; o < i; o++) {
		if (val <= ((out.elementAt(o)).getSortKey())) {
		    break;
		}
	    }
	    if (o == i) {
		out.addElement(in[i]);
	    } else {
		out.insertElementAt(in[i], o);
	    }
	}
	for (int i = 0; i < in.length; i++) {
	    in[i] = out.elementAt(i);
	}
	return in;
    }

    /**
     * Interface to be implemented in order to allow sorting vie the sort()
     * routine above.
     */

    public static interface Sortable {
	public String getSortKey();
    }

    /**
     * Interface to be implemented in order to allow sorting vie the sort()
     * routine above.
     */

    public static interface SortableNumber {
	public double getSortKey();
    }

    /**
     * Checks if the first characters of the input string are ASCII printable.
     * 
     * @param s
     *            Input string.
     * @param maxn
     *            Check is performed only on the first maxn chars.
     * 
     * @return The index of the first non-ASCII printable character, if if was
     *         found among the first characters in the input string, -1
     *         otherwise.
     * 
     * @see #isASCIIPrintable(char )
     */
    public static int isASCIIPrintable(String s, int maxn) {

	int testl = Math.min(s.length(), maxn);

	for (int i = 0; i < testl; i++)
	    if (!isASCIIPrintable(s.charAt(i)))
		return (i);

	return (-1);
    }

    /**
     * Checks if the input string only contains ASCII printable characters.
     * 
     * @param s
     *            Input string.
     * 
     * @return The index of the first non-ASCII printable character, if it was
     *         found in the input string, -1 otherwise.
     * 
     * @see #isASCIIPrintable(char )
     */
    public static int isASCIIPrintable(String s) {
	return (isASCIIPrintable(s, s.length()));
    }

    /**
     * Checks if the specified character is an ASCII printable character.
     * 
     * @param c
     *            Input character.
     * 
     * @return <em>true</em> for whitespace characters (HORIZONTAL TABULATION,
     *         NEW LINE, FORM FEED, CARRIAGE RETURN, SPACE) and for characters
     *         whose ASCII (Unicode) value is between 0x21 ('!') and 0x7E ('~');
     *         <em>false</em> otherwise.
     */

    public static boolean isASCIIPrintable(char c) {

	int code = (int) c;

	if (code >= 0x20 && code < 0x7f) // SPACE and 'normal' chars
	    return true;
	if (code == 0x09 || // HORIZONTAL TABULATION
		code == 0x0a || // NEW LINE
		code == 0x0c || // FORM FEED
		code == 0x0d) // CARRIAGE RETURN

	    return true;
	return (false);
    }

    /**
     * Insert newline chars into the input string s, so that no resulting line
     * is longer than n chars -- if possible. For instance, breaking up<BR>
     * <tt>abc abc abcd abcd abcde abcde 0123456789123 abcdef</tt><BR>
     * with a limit of 10 chars will result in <BR>
     * <tt>abc abc\nabcd abcd\nabcde\nabcde\n0123456789123\nabcdef</tt>
     */
    public static String breakString(String s, int n) {

	if (s == null)
	    return s;

	int l = s.length();

	if (l <= n)
	    return s;

	int p = s.lastIndexOf(" ", n - 1); // look for the last space

	if (p == -1) { // did we find it?

	    // we could not break within
	    // the first n chars: let's
	    // break at the next blank

	    p = s.indexOf(' '); // is there another blank?
	    if (p == -1)
		return s; // not really, give up
	}

	return s.substring(0, p) + '\n' + breakString(s.substring(p + 1, l), n);
    }

    /**
     * Build a range descriptor string.
     * 
     * @param lo
     *            Lower limit of the range
     * @param hi
     *            Upper limit of the range
     * @param incLo
     *            Flag: lower limit is included in the range
     * @param incHi
     *            Flag: upper limit is included in the range
     * 
     * @return A string like <code>[0,1)</code>.
     */
    public static String range(double lo, double hi, boolean incLo, boolean incHi) {

	String sLo = shortformat(lo);
	String sHi = shortformat(hi);

	StringBuffer b = new StringBuffer();

	b.append(incLo ? "[" : "(").append(sLo).append(",").append(sHi).append(incHi ? "]" : ")");

	return b.toString();
    }

    /**
     * @return The shortest possible, accurate representation of the input
     *         value.
     */
    private static String shortformat(double d) {
	String ret = Double.toString(d);

	if (ret.endsWith(".0"))
	    ret = ret.substring(0, ret.length() - 2);
	return ret;
    }

    /**
     * A rudimentary implementation of a C-like format routine (waiting for Java
     * 1.5...). An improvement on the carefully preserved oldformat in that it
     * handles small and large values better (by which I mean it doesn't get
     * thrown by values which get rendered with an exponent) and also does some
     * rounding.
     * 
     * @param d
     *            The number to format.
     * @param b
     *            Minimum number of digits before the decimal point; padding is
     *            performed with blanks.
     * @param a
     *            Maximum number of digits after the decimal point; padding is
     *            performed with zeroes.
     * 
     * @return A textual representation of the input value.
     */
    public static String format(double d, int b, int a) {
	// Make sure that the constants such as
	// POSITIVE_INFINITY, NEGATIVE_INFINITY, NaN
	// are not formated or rounded like normal doubles.
	if (Double.isInfinite(d) || Double.isNaN(d)) {
	    return "" + d;
	}

	if (a < 0 || b < 0) {
	    throw new IllegalArgumentException("Negative number of digits");
	}

	final String format = String.format("%%%d.%df", b + a + 1, a);
	final String result = String.format(format, d);

	return result;
    }

    private static String spaces(int i) {
	StringBuffer b = new StringBuffer();

	while (i-- > 0) {
	    b.append(' ');
	}
	return b.toString();
    }

    /**
     * A rudimentary implementation of a C-like format routine (waiting for Java
     * 1.5...).
     * 
     * @param d
     *            The number to format.
     * @param b
     *            Minimum number of digits before the decimal point; padding is
     *            performed with blanks.
     * @param a
     *            Maximum number of digits after the decimal point; padding is
     *            performed with zeroes.
     * 
     * @return A textual representation of the input value.
     */
    public static String oldformat(double d, int b, int a) {

	if (a < 0 || b < 0)
	    throw new IllegalArgumentException("Negative number of digits");

	String s = Double.toString(d);
	String bs, as;

	// Separate the "before" and "after" strings
	int p = s.indexOf(".");
	if (p < 0) {
	    bs = s;
	    as = "";
	} else {
	    bs = s.substring(0, p);
	    as = s.substring(p + 1, s.length());
	}

	// Truncate the "after" string if needed
	int adiff = a - as.length();
	if (a == 0)
	    as = "";
	else if (adiff < 0)
	    as = as.substring(0, a);

	StringBuffer sb = new StringBuffer();

	// Pad the "before" string if needed
	int bdiff = b - bs.length();
	if (bdiff > 0)
	    for (int i = 0; i < bdiff; i++)
		sb.append(" ");

	sb.append(bs).append(".").append(as);

	// Pad the "after" string if needed
	if (a > 0 && adiff > 0)
	    for (int i = 0; i < adiff; i++)
		sb.append("0");

	return sb.toString();

    }

    /**
     * A rudimentary implementation of a C-like format routine (waiting for Java
     * 1.5...).
     * 
     * @param vup
     *            The valueUnitPair to format.
     * @param b
     *            Minimum number of digits before the decimal point; padding is
     *            performed with blanks.
     * @param a
     *            Maximum number of digits after the decimal point; padding is
     *            performed with zeroes.
     * 
     * @return A textual representation of the input value.
     */
    public static String format(ValueUnitPair<?> vup, int b, int a) {

	if (a < 0 || b < 0)
	    throw new IllegalArgumentException("Negative number of digits");

	StringBuffer s = new StringBuffer();

	s.append(format(vup.getContent(), b, a));
	s.append(' ');
	s.append(vup.getUnit());
	return s.toString();
    }

    /**
     * A rudimentary implementation of a C-like format routine (waiting for Java
     * 1.5...).
     * 
     * @param vup
     *            The valueUnitPair to format.
     * @param a
     *            Maximum number of digits after the decimal point; padding is
     *            performed with zeroes.
     * 
     * @return A textual representation of the input value.
     */
    public static String format(ValueUnitPair<?> vup, int a) {
	return format(vup, 3, a);
    }

    /**
     * A rudimentary implementation of a C-like format routine (waiting for Java
     * 1.5...).
     * 
     * @param vup
     *            The valueUnitPair to format.
     * 
     * @return A textual representation of the input value.
     */
    public static String format(ValueUnitPair<?> vup) {
	return format(vup, 6);
    }

    /*
     * ================================================================
     * Stripping of prefixes and suffixes
     * ================================================================
     */

    /**
     * Check a string for a certain prefix, ignoring case. This one's missing
     * from original String class. Since it requires some sanity checks to
     * implement it right, it gets its own method here.
     */
    public static boolean startsWithIgnoreCase(String string, String prefix) {
	if (string == null || prefix == null || string.length() < prefix.length())
	    return false;
	return (string.substring(0, prefix.length()).equalsIgnoreCase(prefix));
    }

    /**
     * Strip a prefix off the given string
     * 
     * @param string
     *            The string to be stripped of its prefix.
     * @param prefix
     *            The prefix to be removed.
     * 
     * @return The stripped string.
     */
    public static String stripPrefix(String string, String prefix) {
	String result;
	if (string.startsWith(prefix)) {
	    result = string.substring(prefix.length());
	} else {
	    result = string;
	}
	return result;
    }

    /**
     * Strip a suffix off the given string
     * 
     * @param string
     *            The string to be stripped of its suffix.
     * @param suffix
     *            The suffix to be removed.
     * 
     * @return The stripped string.
     */
    public static String stripSuffix(String string, String suffix) {
	String result;
	if (string.endsWith(suffix)) {
	    result = string.substring(0, string.length() - suffix.length());
	} else {
	    result = string;
	}
	return result;
    }

    /**
     * To get the choice which has the same string as the input from the given
     * list
     * 
     * @param input
     *            the string to get the
     * @param list
     *            list of the choices
     * @param caseSensitive
     *            true for case sensitive matching
     * @return matched String object in the list, null if not found
     */
    public static String getChoice(String input, String[] list, boolean caseSensitive) {
	if (input == null)
	    return null;

	String src = caseSensitive ? input : input.toLowerCase();
	String str;
	for (String s : list) {
	    str = caseSensitive ? s : s.toLowerCase();
	    if (src.equals(str)) {
		return s;
	    }
	}
	return null;
    }

    public static String stripHtmlTags(String input) {
	StringBuffer result = new StringBuffer();
	boolean skip = false;
	for (int i = 0; i < input.length(); i++) {
	    char c = input.charAt(i);
	    switch (c) {
	    case '<':
		skip = true;
		break;
	    case '>':
		skip = false;
		break;
	    default:
		if (!skip)
		    result.append(c);
		break;
	    }
	}
	return result.toString();
    }

    /**
     * Remove an item from the original array.
     * 
     * @param orig
     *            array
     * @param item
     *            item to be removed from the array
     * @return result array
     */
    public static String[] removeFromArray(String[] orig, String item) {
	return removeFromArray(orig, new String[] { item });
    }

    /**
     * Remove items from the original array.
     * 
     * @param orig
     *            array
     * @param items
     *            items to be removed fro the array
     * @return result array
     */
    public static String[] removeFromArray(String[] orig, String[] items) {
	ArrayList<String> v = new ArrayList<String>();
	v.addAll(Arrays.asList(orig));
	for (String item : items)
	    v.remove(item);
	String[] ret = new String[v.size()];
	return v.toArray(ret);
    }
    
    public static String join(Collection<String> s, String delimiter) {
	StringBuilder builder = new StringBuilder();
	Iterator<String> iter = s.iterator();
	while (iter.hasNext()) {
	    builder.append(iter.next());
	    if (!iter.hasNext()) {
		break;
	    }
	    builder.append(delimiter);
	}
	return builder.toString();
    }
    
}
