package alma.common.sourcecatalog.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Pattern;


/**
 * Utility methods to convert between data formats.
 * <P>
 * 
 * Methods accepting <strong>Declination</strong> values represented
 * as text expect the input string to be in the format:
 * <code>[-]DD:MM:SS.sss</code> or <code>[-]DDMMSS.sss</code>, 
 *               where DD is degrees (0-90), 
 *                     MM is minutes (0-59), 
 *                     SS is seconds (0-59) and 
 *                     sss is milliseconds. 
 * The leading minus sign is optional.
 * <P>
 * 
 * Methods accepting <strong>Right Ascension</strong> values represented
 * as text expect the input string to be in the format:
 * <code>HH:MM:SS.sss</code> or <code>HHMMSS.sss</code>, 
 *               where HH is hours (0-23), 
 *                     MM is minutes (0-59), 
 *                     SS is seconds (0-59) and 
 *                     sss is milliseconds.
 * <P>
 * 
 * <strong>Note</strong>: methods converting coordinates from String
 * to numeric values accept partial strings as well. Missing values are 
 * filled in as follows: <BR>
 * <UL>
 * <LI>
 *      In general,  missing elements are added to the <em>right</em> of
 *      the input string;
 *      e.g. <code>12:34</code> is equivalent to
 *      <code>&nbsp;12:34:00.000</code> (note the leading space).
 * <LI>
 *      If the input string includes colon chars (<strong>:</strong>), a
 *      leading zero is prepended where needed;
 *      e.g. <code>12:3:4</code> is equivalent to
 *      <code>&nbsp;12:03:04.000</code>;
 * <LI>
 *      If the input string does not include any colon chars,
 *      no leading zeroes are prepended;
 *      e.g. <code>123</code> is equivalent to
 *      <code>&nbsp;12:30:00.000</code>.
 * <LI>
 *      Special case: when a <em>single</em> character is given, a
 *      leading zero is prepended before filling in the missing values;
 *      e.g. <code>1</code> is equivalent to
 *      <code>&nbsp;01:00:00.000</code>.
 * </UL>
 
 *
 * @author A M Chavan, 28-Aug-2003
 */


public final class Convert {
    
    /** Number of decimal places of accuracy */
    public static final int DECIMAL_PLACES_ACCURACY = 3;
    
    /** Conversion factor, degrees to hours (15, in fact) */
    public static final double HOURS_TO_DEGREES = (360.0 / 24.0);
    
    /** Max milliseconds for DEC -- corresponds to +90:00:00.000 */
    public static final int MAX_MILLISECS_DEC = 324000000;
    
    /** Max milliseconds for RA -- corresponds to 23:59:59.999 */
    public static final int MAX_MILLISECS_RA = 86399999 * (int) HOURS_TO_DEGREES;
    
    /** Milliseconds per second */
    public static final int MILLISECS_PER_SEC = 1000;
    
    /** Seconds per minute */
    public static final int SECONDS_PER_MINUTE = 60;
    
    /** Minutes per degree */
    public static final int MINUTES_PER_DEGREE = 60;
    
    /** Minutes per hour */
    public static final int MINUTES_PER_HOUR = 60;
    
    /** Max Degrees for a DEC */
    public static final int DEC_MAX_DEGREES = 90;
    
    /** Max Hours for an RA */
    public static final int RA_MAX_HOURS = 24;
    
    /** Milliseconds per minute */
    public static final int MILLISECS_PER_MINUTE = SECONDS_PER_MINUTE * MILLISECS_PER_SEC;
    
    /** Milliseconds per unit (hour or degree) */
    public static final int MILLISECS_PER_UNIT = 60 * MILLISECS_PER_MINUTE;
    
    /** Fieldwidth for before the decimal place */
    public static final int FW_BEFORE = 2;
    
    /** Fieldwidth for fractions */
    public static final int FW_AFTER = 3;
    
    /**
     * This is a utility class which only has static methods, so we hide the constructor from the public
     */
    private Convert() {
        super();
    }
    
    /**
     * Checks that the fields in a declination have legal values.
     * If any value is illegal it throws an exception.
     *
     * @param sign      -1 if the entry had a minus sign, +1 otherwise
     * @param degrees   value in the hours field of the RA
     * @param minutes   value in the minutes field of the RA
     * @param seconds   value in the seconds field of the RA
     * @param milliSecs value in the milliseconds field of the RA
     * @throws InvalidCoordException if the coordinates are out of bounds. Returns normally otherwise. 
     */
    private static void checkBoundsDDMMSS(
            int sign,
            int degrees,
            int minutes,
            int seconds,
            int milliSecs)
    throws InvalidCoordException {
        
        if (degrees > DEC_MAX_DEGREES
                || minutes >= MINUTES_PER_DEGREE
                || seconds >= SECONDS_PER_MINUTE
                || milliSecs >= MILLISECS_PER_SEC) {
            throw new InvalidCoordException(
                    "Illegal DD:MM:SS value: "
                    + valuesToString(sign, degrees, minutes, seconds, milliSecs));
        }
        if (degrees == DEC_MAX_DEGREES && (minutes > 0 || seconds > 0 || milliSecs > 0)) {
            throw new InvalidCoordException(
                    "Illegal DD:MM:SS value: "
                    + valuesToString(sign, degrees, minutes, seconds, milliSecs));
        }
    }
    
    /**
     * Checks that the fields in a right ascension have legal values.
     * If any value is illegal, then it throws an exception.
     *
     * @param sign      must be +1
     * @param hours     value in the hours field of the RA
     * @param minutes   value in the minutes field of the RA
     * @param seconds   value in the seconds field of the RA
     * @param milliSecs value in the milliseconds field of the RA
     * @throws InvalidCoordException if the coordinates are out of bounds. Returns normally otherwise. 
     */
    private static void checkBoundsHHMMSS(
            int sign,
            int hours,
            int minutes,
            int seconds,
            int milliSecs)
    throws InvalidCoordException {
        
        if (sign < 0
                || hours >= RA_MAX_HOURS
                || minutes >= MINUTES_PER_HOUR
                || seconds >= SECONDS_PER_MINUTE
                || milliSecs >= MILLISECS_PER_SEC) {
            throw new InvalidCoordException(
                    "Invalid HH:MM:SS.SSS value: "
                    + valuesToString(sign, hours, minutes, seconds, milliSecs));
        }
    }
    
    /**
     * Convert RA or Dec from milliarcseconds to hhmmss.sss, ie to
     * hours minutes and seconds plus fractional seconds.
     * 
     * @param n right ascension or declination in 
     *          milliarcseconds.<P> <strong>Note</strong>: value is not checked.
     * 
     * @return String input argument converted to hhmmss.sss format
     */
    public static String convertToXXMMSS(long n) {
        String sign = "";
        
        if (n < 0) {
            sign = "-";
            n = -n;
        }
        
        final long millisecs = n % MILLISECS_PER_SEC;
        n /= MILLISECS_PER_SEC;
        
        final long secs = n % SECONDS_PER_MINUTE;
        n /= SECONDS_PER_MINUTE;
        
        final long mins = n % MINUTES_PER_HOUR;
        n /= MINUTES_PER_HOUR;
        
        // xxx is either hours or degrees.
        final long xxx = n; 
        
        return sign
        + TextUtils.zeroPadded(FW_BEFORE, xxx)
        + TextUtils.zeroPadded(FW_BEFORE, mins)
        + TextUtils.zeroPadded(FW_BEFORE, secs)
        + "."
        + TextUtils.zeroPadded(FW_AFTER, millisecs);
    }
    
    /**
     * Convert a Declination string to milliarcseconds.
     *
     * @param  value  String to be converted.
     * @return Input String converted to milliarcseconds
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws NumberFormatException if value is in a bad format
     */
    public static double DDMMSSToDeg(String value) 
    throws InvalidCoordException, NumberFormatException {
        
        final int marcsec = DDMMSSToMilliarcsec(value);
        final double  ret = (double) marcsec / (double) MILLISECS_PER_UNIT;
        
        return ret;
    }
    
    /**
     * Convert a Declination string to degrees., 
     *
     * @param  value  String to be converted.
     * @return Input String converted to degrees
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws NumberFormatException if value is in a bad format
     */
    public static int DDMMSSToMilliarcsec(String value) 
    throws InvalidCoordException, NumberFormatException {
        
        return stringToMilliarcsec(value, false, false);
    }
    
    /**
     * Convert a Right Ascension string to degrees.
     * Uses <code>stringToMilliarcsec()</code>. 
     *
     * @param  value  String to be converted.
     * @return Input String converted to degrees
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws NumberFormatException if value is in a bad format
     * @see    #HHMMSSToMilliarcsec(String)
     */
    public static double HHMMSSToDeg(String value)
    throws InvalidCoordException, NumberFormatException {
        
        final int marcsec = HHMMSSToMilliarcsec(value);
        final double  ret = (double) marcsec / (double) MILLISECS_PER_UNIT;
        
        return ret;
    }
    
    /**
     * Convert a Right Ascension string to milliarcseconds.
     *
     * @param  value  String to be converted.
     * @return Input String converted to milliarcseconds
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws NumberFormatException if value is in a bad format
     */
    public static int HHMMSSToMilliarcsec(String value)
    throws InvalidCoordException, NumberFormatException {
        return stringToMilliarcsec(value, true, false);
    }
    
    /**
     * Convert a Right Ascension string to seconds.
     * Uses <code>stringToMilliarcsec()</code>. 
     *
     * @param  value  String to be converted.
     * @return Input String converted to seconds
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws NumberFormatException if value is in a bad format
     * @see    #HHMMSSToMilliarcsec(String)
     */
    public static double HHMMSSToSec(String value)
    throws InvalidCoordException, NumberFormatException {
        
        final int marcsec = HHMMSSToMilliarcsec(value);
        final double  ret = (double) marcsec / (double) MILLISECS_PER_SEC;
        return ret;
    }
    
    /**
     * Check if a Dec value in milliarcseconds is within its valid
     * range.
     *
     * @param dec       Declination in milliarcseconds.
     *
     * @return true if the value is within its range, false otherwise.
     */
    public static boolean isValidDec(long dec) {
        return (-MAX_MILLISECS_DEC <= dec) && (dec <= MAX_MILLISECS_DEC);
    }
    
    /**
     * Check if an RA value in milliarcseconds is within its valid
     * range.
     *
     * @param ra       Right ascension in milliarcseconds.
     *
     * @return true if the value is within its range, false otherwise.
     */
    public static boolean isValidRA(long ra) {
        return (0 <= ra) && (ra < MAX_MILLISECS_RA);
    }
    
    /**
     * Converts degrees to a String in the format
     * "<code>+DD:MM:SS.sss</code>", where + is an optional sign,
     * DD is hours, MM is minutes, SS is seconds and sss is
     * millseconds.
     * @param value Value to be converted, in degrees between 0 and 360.
     * @return      input converted to a String
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws OutOfRangeException numerical limits are exceeded during conversion
     */
    public static String degToDDMMSS(double value) 
    throws OutOfRangeException, InvalidCoordException {
        
        final long msec = Math.round(value * MILLISECS_PER_UNIT);
        return milliarcsecToString(msec, false, false);
    }
    
    /**
     * Converts degrees to a String in the format
     * "<code>&nbsp;HH:MM:SS.sss</code>" (note the leading space),
     * where HH is hours, MM is minutes, SS is seconds and sss is
     * millseconds.
     * @param value Value to be converted, in degrees between 0 and 360.
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws OutOfRangeException numerical limits are exceeded during conversion
     * @return      input converted to a String
     */
    public static String degToHHMMSS(double value) 
    throws OutOfRangeException, InvalidCoordException {
        
        final long msec = Math.round(value * MILLISECS_PER_UNIT);
        return milliarcsecToString(msec, true, false);
    }
    
    /**
     * Converts milliarcseconds to a String in the format
     * "<code>+DD:MM:SS.sss</code>", where + is an optional sign or
     * space, DD is hours, MM is minutes, SS is seconds and sss is
     * millseconds.
     *
     * @param value value to be converted, in milliarcseconds
     *
     * @return      input converted to a String
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws OutOfRangeException numerical limits are exceeded during conversion
     */
    public static String milliarcsecToDDMMSS(long value)
    throws OutOfRangeException, InvalidCoordException {
        return (milliarcsecToString(value, false, false));
    }
    
    /**
     * Converts milliarcseconds to a String in the format
     * "<code>&nbsp;HH:MM:SS.sss</code>" (note the leading space),
     * where HH is hours, MM is minutes, SS is seconds and sss is
     * millseconds.
     *
     * @param value value to be converted, in milliarcseconds
     * @throws InvalidCoordException if somehow an invalid set of coordinates is generated
     * @throws OutOfRangeException numerical limits are exceeded during conversion
     *
     * @return      input converted to a String
     */
    public static String milliarcsecToHHMMSS(long value)
    throws OutOfRangeException, InvalidCoordException {
        return (milliarcsecToString(value, true, false));
    }
    
    
    
    /**
     * Converts milliarcseconds to a String in the format
     * +UU:MM:SS.sss, where + is an optional sign or space, UU is
     * hours or degrees, MM is minutes, SS is seconds and sss is
     * millseconds.
     *
     * @param value   value to be converted, in milliarcseconds
     * @param ra      true means convert to HH:MM:SS, 
     *                false to DD:MM:SS
     *
     * @param noCheck do validation check
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception OutOfRangeException numerical limits are exceeded during conversion
     *
     * @return input converted to a String
     */
    public static String milliarcsecToString(long value,
            boolean ra,
            boolean noCheck)
    throws OutOfRangeException, InvalidCoordException {
        
        if (!noCheck) {
            if (value > Long.MAX_VALUE || value < Long.MIN_VALUE) {
                throw new OutOfRangeException("Out of range: " + value);
            }
            if (ra) {
                if (!isValidRA(value)) {
                    throw new InvalidCoordException("Invalid masec RA: " + value);
                }
            } else if (!isValidDec(value)) {
                throw new InvalidCoordException("Invalid masec Dec: " +  value);
            }
        }
        
        // We are returning milliARCsecs, so if this is RA then we still need to convert 
        // (as this value so far will be in millisec of time)
        if (ra) {
            value /= (long) HOURS_TO_DEGREES;
        }
        
        char sign = ' ';
        if (value < 0) {
            sign = '-';
            value = -value;
        }
        
        final long units = value / MILLISECS_PER_UNIT;
        value -= units * MILLISECS_PER_UNIT;
        
        final long minutes = value / MILLISECS_PER_MINUTE;
        value -= minutes * MILLISECS_PER_MINUTE;
        
        return sign
        + TextUtils.zeroPadded(FW_BEFORE, units)
        + ":"
        + TextUtils.zeroPadded(FW_BEFORE, minutes)
        + ":"
        + TextUtils.zeroPadded(FW_BEFORE, (value / MILLISECS_PER_SEC))
        + "."
        + TextUtils.zeroPadded(FW_AFTER, (value % MILLISECS_PER_SEC));
    }
    
    /**
     * @param dec  A String representing a declination value; e.g.
     *              <code>-34:5</code>
     * @return A normalized representation of the input string; e.g.
     *              <code>-34:05:00.000</code>.
     * @exception NumberFormatException if value is in a bad format
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception OutOfRangeException numerical limits are exceeded during conversion
     */
    public static String normalizeDec(String dec)
    throws OutOfRangeException,
           InvalidCoordException,
           NumberFormatException {
        final int    m = Convert.DDMMSSToMilliarcsec(dec);
        final String t = Convert.milliarcsecToDDMMSS(m);
        return t;
    }
    
    /**
     * @param ra  A String representing a right ascension value; e.g.
     *              <code>2:3</code>
     * @return A normalized representation of the input string; e.g.
     *              <code>02:03:00.000</code>.
     * @exception NumberFormatException if value is in a bad format
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception OutOfRangeException numerical limits are exceeded during conversion
     */
    public static String normalizeRa(String ra)
    throws OutOfRangeException,
           InvalidCoordException,
           NumberFormatException {
        final int    m = Convert.HHMMSSToMilliarcsec(ra);
        final String t = Convert.milliarcsecToHHMMSS(m);
        return t;
    }
    
    /** 
     * Rounds argument db to a number of decimal places, e.g. 2.32839,1 -> 2.3.
     *
     * @param db  - a double number
     * @param places -  the number of decimal places to round to
     *
     * @return double round to specific decimal places
     */
    public static double round(double db, int places) {
        double base = 1.;
        
        for (int i = 1; i <= places; i++) {
            base *= 10.;
        }
        double tmp = db * base;
        if (tmp >= 0.) {
            tmp += 0.5;
        } else {
            tmp -= 0.5;
        }
        final int ip = (int) tmp;
        tmp = ((double) ip) / base;
        return (tmp);
    }
    
    /**
     * Converts seconds to a String in the format
     * "<code>&nbsp;HH:MM:SS.sss</code>" (note the leading space),
     * where HH is hours, MM is minutes, SS is seconds and sss is
     * millseconds. Calls milliarcsecToString and multiples the 
     * returned value by 1000 (millisecs in a sec).
     *
     * @param     value                    value to be converted, in seconds
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception OutOfRangeException numerical limits are exceeded during conversion
     *
     * @return input converted to a String
     */
    public static String secToHHMMSS(long value)
    throws OutOfRangeException, InvalidCoordException {
        final long millisecs = value * MILLISECS_PER_SEC;
        return milliarcsecToString(millisecs, false, true);
    }
    
    /**
     * Converts a String in the format +UU:MM:SS.sss to
     * milli-arcseconds, where + is an optional sign, UU is hours or
     * degrees, MM is minutes, SS is seconds and sss is millseconds.
     *
     * @param value   value to be converted, represented as a String.
     * @param ra      true means convert to HHMMSS, false to DDMMSS
     * @param noCheck true means do validation checking, false means don't
     *
     * @return      input converted to a milliarcseconds
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception NumberFormatException if value is in a bad format
     */
    private static int stringToMilliarcsec(String value,
                                           boolean ra,
                                           boolean noCheck) 
    throws InvalidCoordException, NumberFormatException {
        
        int result = 0;
        try {
            value = value.trim();
            
            // Drop useless leading "+" sign
            if (value.startsWith("+")) {
                value = value.substring(1);
            }
            
            // Let someone else deal with the no-colons case
            if (value.indexOf(':') == -1) {
                return (stringToMilliarcsecNoColons(value, ra, noCheck));
            }
            
            result = 0;
            final StringTokenizer st = new StringTokenizer(value, " :");
            
            int units;
            final String unitsString = st.nextToken();
            int minutes = 0;
            int seconds = 0;
            int milliSecs = 0;
            int sign = +1;
            
            if (st.hasMoreTokens()) {
                minutes = new Integer(st.nextToken()).intValue();
                if (st.hasMoreTokens()) {
                    seconds = new Integer(st.nextToken(": ,.")).intValue();
                    if (st.hasMoreTokens()) {
                        String ms = st.nextToken(".,");
                        for (int j = 1; j <= DECIMAL_PLACES_ACCURACY; j++) {
                            ms += "0";
                        }
                        ms = ms.substring(0, DECIMAL_PLACES_ACCURACY);
                        milliSecs = new Integer(ms).intValue();
                    }
                }
            }
            
            if (unitsString.startsWith("-")) {
                sign = -1;
                units = - new Integer(unitsString).intValue();
            } else {
                units = new Integer(unitsString).intValue();
            }
            
            if (!noCheck) {
                if (ra) {
                    checkBoundsHHMMSS(sign, units, minutes, seconds, milliSecs);
                } else {
                    checkBoundsDDMMSS(sign, units, minutes, seconds, milliSecs);
                }
            }
            
            result =
                sign
                * (units * MILLISECS_PER_UNIT
                        + minutes * MILLISECS_PER_MINUTE
                        + seconds * MILLISECS_PER_SEC
                        + milliSecs);
            // We are returning milliARCsecs, so if this is RA then we still need to convert 
            // (as this value so far will be in millisec of time)
            if (ra) {
                result = result * (int) HOURS_TO_DEGREES;
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid number: '" + e.getMessage() + "'");
        }
        
        return result;
    }
    
    /**
     * Converts a String in the format [-][UUMMS]S[.sss] to
     * milli-arcseconds, where + is an optional sign, UU is hours or
     * degrees, MM is minutes, SS is seconds and sss is millseconds.
     *
     * @param value   value to be converted, represented as a String.
     * @param ra      true means convert to HHMMSS, false to DDMMSS
     * @param noCheck true means do validation checking, false means don't
     *
     * @return      input converted to a milliarcseconds
     * @exception InvalidCoordException if somehow an invalid set of coordinates is generated
     * @exception NumberFormatException if value is in a bad format
     */
    private static int stringToMilliarcsecNoColons(String value,
                                                   boolean ra,
                                                   boolean noCheck)
    throws InvalidCoordException, NumberFormatException {
        
        final int length = 6;
        final String zeros = "000000";
        
        /** return value */
        int result = 0;
        
        /** index of decimal point */
        int iPoint = -1;
        
        /** index of sign */
        int iSign = -1;
        
        /** What sign to use */
        int sign;
        
        /** integer substring */
        String main = null;
        
        /** fractional substring */
        String frac = "000";
        
        /** Scratch variable */
        int t = 0;

        /** Scratch variable */
        int l = 0;
        
        // Isolate 'integer' and 'fractional' 
        // parts of value string:
        // +123456.789 --> 123456 and 789
        //----------------------------------------
        
        value = value.trim();
        t = value.indexOf('.');
        l = value.length();
        if (t == -1) {
            iPoint = l;
        } else {
            iPoint = t;
        }
        if (value.startsWith("-")) {
            iSign = 1;
            sign = -1;
        } else {
            iSign = 0;
            sign =  1;
        }
        main = value.substring(iSign, iPoint).trim();
        if (iPoint < l) {
            // Input string has a decimal point:
            // we pad left with zeros, and that's it
            frac = value.substring(iPoint + 1, l) + "000";
            main = TextUtils.padded(zeros, length, main);
        } else {
            // Input string has no decimal point
            // We pad right with zeroes, but for the special
            // case of a single char value.
            // n    ->  0n0000 (same as 0n:00:00)
            // nm   ->  nm0000 (same as nm:00:00)
            // nmk  ->  nmk000 (same as nm:k0:00)
            // ... and so on.
            
            if (main.length() == 1) {
                main = "0" + main;
            }
            main = TextUtils.rightpadded(zeros, length, main);
        }
        frac = frac.substring(0, DECIMAL_PLACES_ACCURACY);
        
        if (main.length() > length) {
            throw new InvalidCoordException("String too long: '" + value + "'");
        }
        
        final String uu = main.substring(0 * FW_BEFORE, 1 * FW_BEFORE);
        final String mm = main.substring(1 * FW_BEFORE, 2 * FW_BEFORE);
        final String ss = main.substring(2 * FW_BEFORE, 3 * FW_BEFORE);
        
        int units = 0;
        try {
            units = new Integer(uu).intValue();
        } catch (NumberFormatException e) {
            throw new NumberFormatException(e.getMessage());
        }
        final int minutes = new Integer(mm).intValue();
        final int seconds = new Integer(ss).intValue();
        int milliSecs;
        try {
            milliSecs = new Integer(frac).intValue();
        } catch (NumberFormatException e) {
            throw new NumberFormatException(e.getMessage());
        }
        
        if (!noCheck) {
            if (ra) {
                checkBoundsHHMMSS(sign, units, minutes, seconds, milliSecs);
            } else {
                checkBoundsDDMMSS(sign, units, minutes, seconds, milliSecs);
            }
        }
        
        result =
            sign
            * (units * MILLISECS_PER_UNIT
                    + minutes * MILLISECS_PER_MINUTE
                    + seconds * MILLISECS_PER_SEC
                    + milliSecs);
        
        // We are returning milliARCsecs, so if this is RA then we still need to convert 
        // (as this value so far will be in millisec of time)
        if (ra) {
            result = result * (int) HOURS_TO_DEGREES;
        }
        
        return result;
    }
    
    /**
     * Convert a bunch of seperate values for degrees, minutes etc into DD:MM:SS.mmm
     * 
     * @param sign      - the sign of the value being composed (<0 => -ve).
     * @param degrees   - how many degrees
     * @param minutes   - how many minutes
     * @param seconds   - how many seconds
     * @param milliSecs - how many milliseconds
     * @return A colon separated representation of its input parameter.
     */
    private static String valuesToString(
            int sign,
            int degrees,
            int minutes,
            int seconds,
            int milliSecs) {
        String ret = null;
        
        if (sign < 0) {
            ret = "-";
        } else {
            ret = "";
        }
        
        ret = ret + degrees + ":" + minutes + ":" + seconds + "." + milliSecs;
        return (ret);
    }
}


