package alma.common.sourcecatalog;

import java.util.Date;


public interface SourceCatalogQueryParameters {
    // Mandatory search arguments
    public Integer getMaxSources();
    public void setMaxSources(Integer max);

    public void setSortAscending(boolean state);
    public boolean getSortAscending();

    // Optional search arguments    
    public String getName();
    public void setName(String name);
    public boolean hasName();
    public void deleteName();

    public double getRightAscension();
    public void setRightAscension(double rightAscension);
    public boolean hasRightAscension();
    public void deleteRightAscension();
    
    public double getDeclination();
    public void setDeclination(double declination);
    public boolean hasDeclination();
    public void deleteDeclination();
    
    public double getRadius();
    public void setRadius(double radius);
    public boolean hasRadius();
    public void deleteRadius();

    public double getMinFlux();
    public void setMinFlux(double minFlux);
    public boolean hasMinFlux();
    public void deleteMinFlux();
    
    public double getMaxFlux();
    public void setMaxFlux(double maxFlux);
    public boolean hasMaxFlux();
    public void deleteMaxFlux();

    public double getMinFrequency();
    public void setMinFrequency(double frequency);
    public boolean hasMinFrequency();
    public void deleteMinFrequency();

    public double getMaxFrequency();
    public void setMaxFrequency(double frequency);
    public boolean hasMaxFrequency();
    public void deleteMaxFrequency();

    public Date getEarliestObservation();
    public void setEarliestObservation(Date earliestObservation);
    public boolean hasEarliestObservation();
    
    public Date getLatestObservation();
    public void setLatestObservation(Date latestObservation);
    public boolean hasLatestObservation();
    
    public String getSortBy();
    public void setSortBy(String field);
    public boolean hasSortBy();
    public void deleteSortBy();
}