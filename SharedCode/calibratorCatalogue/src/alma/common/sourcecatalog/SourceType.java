package alma.common.sourcecatalog;

public interface SourceType {
    public Integer getId();
    public String getName();
}
