package alma.common.sourcecatalog;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public interface CatalogAccess {
    public List<Measurement> query(CatalogQueryParameters query) throws IOException, IllegalArgumentException;
    public Source getSource(Integer id) throws IOException;
    public Collection<Catalog> listCatalogs() throws IOException;
    public Collection<String> listSortFields() throws IOException;
    public Collection<SourceType> listTypes() throws IOException;
}
