#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

# execfile('script.py')
#from sgmllib import SGMLParser
from HTMLParser import HTMLParser
import urllib
import copy

(IGNORE, ACQUIRE, INITIALIZE, SOURCE) = range(4)
(COMMON_NAME, IAU_NAME, RA, DEC, BAND, OBS_DATE, OBSERVATORY, FLUX, END) = range(9)

class SMAParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__tableNumber = 0
        self.__state = IGNORE
        self.__substate = COMMON_NAME
        self.__output = []
        self.__currentOutput = [None] * 8
        self.__IGNORE_DATA = True
        
    def handle_starttag(self, tag, attrs):
        if tag =='table' and self.__state == IGNORE:
            self.__tableNumber += 1
            if (self.__tableNumber == 4):
                self.__state = ACQUIRE

        if self.__state != IGNORE and tag == 'tr':
            for attr in attrs:
                if attr[0] == 'class' and attr[1] =='head':
                    # This is a header row do nothing
                    return
            if self.__state == ACQUIRE:
                self.__state = INITIALIZE

        if self.__state != IGNORE and tag == 'td':
            # Beginning of a new source get the repeat count
            self.__IGNORE_DATA = False
            self.__currentData = ''
            if self.__state == INITIALIZE:
                for att in attrs:
                    if att[0] == 'rowspan':
                        self.__state    = SOURCE
                        self.__substate = COMMON_NAME
                        self.repeatCount = int(att[1])
        
    def handle_endtag(self, tag):
        if tag == 'table':
            self.__state = IGNORE

        if self.__state == SOURCE and tag == 'tr':
            # End of Row
            self.repeatCount -= 1
            self.__output.append(copy.copy(self.__currentOutput))
            if self.repeatCount == 0:
                self.__state = ACQUIRE
            else:
                self.__substate = BAND

        if self.__state == SOURCE and tag ==  'td':
            if self.__substate != END:
                self.__currentOutput[self.__substate] = self.__currentData
            self.__IGNORE_DATA = True
            self.__substate += 1
                

    def handle_data(self, data):
        if self.__state == SOURCE and not self.__IGNORE_DATA:
            if self.__substate != END:
                self.__currentData += data

    def getOutput(self):
        return self.__output
        
class App:
    def __init__(self):
        self.htmlSource = None

    def downloadSMACalibratorList(self):
        smaCalListUrl = 'http://sma1.sma.hawaii.edu/callist/callist.html'
        sock = urllib.urlopen(smaCalListUrl)
        self.htmlSource = sock.read()
        sock.close()
        #print htmlSource # diagnostic

    def getSource(self):
        return self.htmlSource

class OutputFormatter:
    def __init__(self, outputData):
        self.outputData = outputData
        print "%d entries found in catalog" % len(outputData)

    def writeFile(self, filename="smaCalibratorTable.dat"):
        f = open(filename,'w')
        f.write("   Source Name          VLA J2000 Coordinates    Wavelength    Last      Obs    Flux Density(30.0 day avg)\n")
        f.write("Common   IAU(J2000)      RA             Dec         (m)      Observed              (Jy)    Error (Jy)\n")
        f.write("==========================================================================================================\n")
        for entry in self.outputData:
            f.write(self.formatEntry(entry))
        f.close()

    def formatEntry(self, entry):
        delimiter = ','
        line = ''
        entry[0] += delimiter
        line += entry[0].ljust(10)
        entry[1] += delimiter
        line += entry[1].ljust(10)
        entry[2] += delimiter
        line += entry[2].ljust(15)
        entry[3] += delimiter
        line += entry[3].ljust(15)
        
        if entry[4] == '3mm':
            line += '0.003000, '
        elif entry[4] == '2mm':
            line += '0.002000, '
        elif entry[4] == '1mm':
            line += '0.001000, '
        elif entry[4] == '850m':        
            line += '0.000850, '
        else:
            print "Error Unrecognized Band: ", entry[4]
            raise Exception()

        entry[5] += delimiter
        line += entry[5].ljust(13)
        entry[6] += delimiter
        line += entry[6].ljust(10)
        (flux, error) = entry[7].split()
        flux += delimiter
        line += flux.ljust(8)
        line += error.ljust(5)
        line += "\n"
        return line

if __name__ == "__main__":
    print "Reading SMA Calibrator Catalogue..."
    app = App()
    app.downloadSMACalibratorList()

    parser = SMAParser()
    parser.feed(app.getSource())

    output = OutputFormatter(parser.getOutput())
    output.writeFile()

#
# ___oOo___
