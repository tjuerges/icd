/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id: TmcdbAntennaCAIMap.cpp,v 1.2 2011/04/02 13:48:33 jperez Exp $"
 *
 */

#include <acsContainerServices.h>
#include <TmcdbAntennaCAIMap.h>
#include <TmcdbErrTypeC.h>
#include <TMCDBDataStructuresC.h>
#include <cmath>
#include <acsutilFindFile.h>

TmcdbAntennaCAIMap::TmcdbAntennaCAIMap(maci::ContainerServices* containerServices, 
				       CorrelatorNameMod::CorrelatorName correlator) 
{
    // TODO: what follows will be replaced with real code
    getDefaultCAIMap();
}

TmcdbAntennaCAIMap::TmcdbAntennaCAIMap(maci::ContainerServices* containerServices,
				       CorrelatorNameMod::CorrelatorName correlator,
				       const Correlator::AntennaSeq_t& antennaSeq) 
{ 
    int numAntennas = antennaSeq.length();
    // TODO: what follows will be replaced with real code
    getDefaultCAIMap();
    m_caiMap.clear();
    for ( int i = 0; i < numAntennas; i++ )
    {
	//
	// if the requested antenna is not in the default map then panic
	//
	if ( m_defaultCAIMap.count(std::string(antennaSeq[i])) == 0 )
	{
	    std::ostringstream errStream;
	    errStream << "Requested antenna " << antennaSeq[i] << " not found" << endl;
// 	    CorrErr::InvalidParameterExImpl ex(__FILE__, __LINE__,
// 					       "TmcdbAntennaCAIMap::TmcdbAntennaCAIMap");
// 	    ex.addData("Cause", errStream.str().c_str());
// 	    ex.log();
// 	    throw ex.getInvalidParameterEx();
	    cout << errStream.str();
	    return;
	}

	//
	// add this antenna to the object local map
	//
	m_caiMap[antennaSeq[i].in()] = m_defaultCAIMap[antennaSeq[i].in()];
    }

}

TmcdbAntennaCAIMap::TmcdbAntennaCAIMap(const TmcdbAntennaCAIMap& toCopy) 
{
    *this = toCopy;
}

TmcdbAntennaCAIMap::TmcdbAntennaCAIMap&
TmcdbAntennaCAIMap::operator =(const TmcdbAntennaCAIMap& rhs) 
{
    return *this;
}

TmcdbAntennaCAIMap::~TmcdbAntennaCAIMap() { }

const TmcdbAntennaCAIMap::antennaCAIMap_t TmcdbAntennaCAIMap::getCAIs()
{
    return getCAIs(m_antennaIDs);
}

const TmcdbAntennaCAIMap::antennaCAIMap_t TmcdbAntennaCAIMap::getCAIs(antennaIDs_t &antIDs)
{
    return getCAIs(antIDs);
}


void TmcdbAntennaCAIMap::getDefaultCAIMap()
{
    // TODO: The following will be replaced with the correct calls to TMCDB class
    std::ostringstream allAntsStr;
    allAntsStr << "Map of antenna CAIs: " << endl;    
    char  mode, dirMode;
    int fileSize;
    char fileBuf[200];
    char *dataFile = "config/antennaCAIMap.dat";
    FILE *fd = 0;
    std::ostringstream errStream;
    errStream << "The following errors were found in the antenna/CAI map data file: " << endl;
    bool success = true;


    if (!acsFindFile (dataFile, fileBuf, &mode, &fileSize, &dirMode))
    {
	ACS_SHORT_LOG((LM_ERROR,"file not found %s, will run with default map", dataFile));
	for ( int cai = 0; cai < 32; cai++ )
	{
	    std::ostringstream antStr;
	    antStr << setfill('0');
	    antStr << "DV" << setw(2)  << cai + 1;
	    m_antennaIDs.push_back(antStr.str());
	    m_defaultCAIMap[antStr.str()] = 2 * cai;
	    allAntsStr << antStr.str() << "cai: " << m_defaultCAIMap[antStr.str()] << endl;
	    antStr.str("");
	    antStr.clear();
	    antStr << "DA" << setw(2)  << cai + 41;
	    m_caiMap[antStr.str()] = 2 * cai + 1;
	    allAntsStr << antStr.str() << " cai: " << m_defaultCAIMap[antStr.str()] << endl;
	    if (cai < 4)
	    {
		antStr.str("");
		antStr.clear();
		antStr << "PM" << setw(2)  << cai + 1;
		m_antennaIDs.push_back(antStr.str());
		m_defaultCAIMap[antStr.str()] = cai;
		allAntsStr << antStr.str() << " cai: " << m_defaultCAIMap[antStr.str()] << endl;
	    }
	    if (cai < 12)
	    {
		antStr.str("");
		antStr.clear();
		antStr << "CM" << setw(2)  << cai + 5;
		m_defaultCAIMap[antStr.str()] = cai + 4;
		allAntsStr << antStr.str() << " cai:" << m_defaultCAIMap[antStr.str()] << endl;
	    }
	}
	// Just temporary debug logging
	cout << allAntsStr.str();
	m_caiMap = m_defaultCAIMap;
	return;
    }
    else
    { 
	// first open the file and assign a pointer called fd to it
	//right now the file name and location are hardcoded, but later
	// the file name may be parameterized
	fd = fopen(fileBuf,"r");
    }
    
    char label[100];
    char buf[101];
    int cai;
    m_defaultCAIMap.clear();
    //while the pointer to the file does not point to the end of file character
    while( fgets(buf,100,fd) )
    {
	//process the line if it does not begin with a # sign.  # denotes a comment
	if (buf[0] != '#')
	{
	    sscanf(buf, "%d, %s", &cai, label);
	    if (m_defaultCAIMap.count(std::string(label)) != 0)
	    { // antenna already on list
		errStream << "Antenna " << label << " is repeated on list with CAIs " << m_defaultCAIMap[std::string(label)] 
			  << " and " << cai << endl;
		success = false;
	    }
	    if (cai < 0 || cai >= 64)
	    { // invalid cai number
		errStream << "Invalid CAI " << cai << " in file" << endl;
		success = false;
	    }
	    if (label[0] != 'D' && label[0] != 'C' && label[0] != 'P' && label[0] != 'F')
	    { // invalid array type
		errStream << "Invalid array type: " << label[0] << " in file" << endl;
		success = false;
	    }
	    if (label[1] != 'V' && label[1] != 'A' && label[1] != 'M' && label[1] != 'O')
	    { // invalid antenna type
		errStream << "Invalid antenna type: " << label[1] << " in file" << endl;
		success = false;
	    }
	    char antNumStr[3];
	    strncpy(antNumStr, &label[2], 2);
	    antNumStr[2] = 0;
	    // string antNumStr = antNum;
	    int antNum;
	    bool validAntNum = true;
	    switch (label[1])
	    {
	    case 'V':
		// Vertex antennas are numbered 1..40
		if (antNum < 1 || antNum > 40)
		    validAntNum = false;
		break;
	    case 'A':
		// Alcatel antennas are numbered 41..80
		if (antNum < 41 || antNum > 80)
		    validAntNum = false;
		break;
	    case 'M':
		if (label[0] == 'C')
		{ // Melco compact antennas are numbered 1..4
		    if (antNum < 1 || antNum > 4)
			validAntNum = false;
		}
		else
		{ // Melco 12m antennas are numbered 1..12
		    if (antNum < 1 || antNum > 12)
			validAntNum = false;
		}
		break;
	    case 'O':
		validAntNum = true;
		break;
	    default:
		errStream << "Invalid antenna type: " << label[1] << " with number " << antNum <<" in file" << endl;
		success = false;
	    }
	    if (!validAntNum)
	    {
		errStream << "Invalid number " << antNum << " for antenna type: " << label[1] << " in file" << endl;
		success = false;
	    }
	    m_defaultCAIMap[std::string(label)] = cai;
	}
    }
    fclose(fd);
    m_caiMap = m_defaultCAIMap;
    if (!success)
    {
//         CorrErr::InvalidParameterExImpl ex(__FILE__, __LINE__,
//                                        "TmcdbAntennaCAIMap::TmcdbAntennaCAIMap");
//         ex.addData("Cause", errStream.str().c_str());
//         ex.log();
//         throw ex.getInvalidParameterEx();
	cout << errStream.str();
    }
}

void TmcdbAntennaCAIMap::getCAIsFromTMCDB(TMCDB::Access* tmcdb, antennaIDs_t &antennas)
{
    // TODO: what follows will be replaced with real code
}

void TmcdbAntennaCAIMap::getTmcdbAccessComponent(maci::ContainerServices* containerServices) 
{
    m_tmcdb = containerServices->getDefaultComponent<TMCDB::Access>("IDL:alma/TMCDB/Access:1.0");
}

// throws maciErrType::CannotReleaseComponentEx
void TmcdbAntennaCAIMap::releaseTmcdbAccessComponent(maci::ContainerServices* containerServices) 
{
    CORBA::String_var tmcdbComponentName = m_tmcdb->name();
    containerServices->releaseComponent(tmcdbComponentName.in());
}
