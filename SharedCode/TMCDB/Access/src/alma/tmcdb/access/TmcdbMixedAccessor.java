package alma.tmcdb.access;

import java.util.logging.Logger;

import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.BandFocusModel;
import alma.TMCDB.BandPointingModel;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB.XPDelay;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.acs.component.ComponentLifecycleException;
import alma.tmcdb.utils.TmcdbLoggerFactory;

/**
 * A mixed TmcdbAccessor, that will get the data from either the TMCDB database,
 * or from the $ACS_CDB/TMCDB_DATA, $ACSROOT/SIMTMCDB and science script directories,
 * which contain the data in XML files.
 * 
 * This class is temporary. Functions accessing the XML files will be replaced
 * one by one by the database version as the TMCDBExplorer is extended to allow editing
 * the data.
 * 
 * @author Rafael Hiriart (rhiriart@nrao.edu)
 *
 */
public class TmcdbMixedAccessor implements TmcdbAccessor {

    /** Main TMCDB Accessor, uses Hibernate to access the database */
    TmcdbAccessor tmcdb;

    /** The temporary XML TMCDB Accessor, taken from the simulation impl */
    TmcdbAccessor xmlTmcdb;
	
    Logger logger;
    
	public TmcdbMixedAccessor() throws ComponentLifecycleException {
	    logger = TmcdbLoggerFactory.getLogger(TmcdbMixedAccessor.class.getName());
	    logger.finest("TmcdbMixedAccessor::TmcdbMixedAccessor");
        try {
            this.tmcdb = new TmcdbHibernateAccessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ComponentLifecycleException(ex);
        }
        this.xmlTmcdb = new TmcdbXmlAccessor();
	}
	
	@Override
	public void clear() throws Exception {
        if (tmcdb != null) {
            try {
                tmcdb.clear();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
	}

	@Override
	public AntennaIDL getAntennaInfo(String antennaName)
			throws AcsJTmcdbNoSuchRowEx {
		return tmcdb.getAntennaInfo(antennaName);
	}

	@Override
	public ArrayReferenceLocation getArrayReferenceLocation() {
		return xmlTmcdb.getArrayReferenceLocation();
	}

	@Override
	public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber)
			throws AcsJTmcdbNoSuchRowEx {
		return tmcdb.getAssemblyConfigData(serialNumber);
	}

	@Override
	public AssemblyConfigXMLData getComponentConfigData(String componentName) {
		return tmcdb.getComponentConfigData(componentName);
	}

	@Override
	public String getConfigurationName() {
		return tmcdb.getConfigurationName();
	}

	@Override
	public AntennaDelays getCurrentAntennaDelays(String antenna)
	    throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
		return xmlTmcdb.getCurrentAntennaDelays(antenna);
	}

	@Override
	public BandFocusModel getCurrentAntennaFocusModel(String antennaName)
			throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
		return xmlTmcdb.getCurrentAntennaFocusModel(antennaName);
	}

    @Override
    public BandPointingModel getCurrentAntennaPointingModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
	public PadIDL getCurrentAntennaPadInfo(String antennaName)
			throws AcsJTmcdbNoSuchRowEx {
		return xmlTmcdb.getCurrentAntennaPadInfo(antennaName);
	}

	@Override
	public double[] getMetrologyCoefficients(String antenna) {
		return xmlTmcdb.getMetrologyCoefficients(antenna);
	}

	@Override
	public StartupAOSTimingIDL getStartupAOSTimingInfo()
			throws AcsJTmcdbErrorEx {
		return tmcdb.getStartupAOSTimingInfo();
	}

	@Override
	public StartupAntennaIDL[] getStartupAntennasInfo() {
		return tmcdb.getStartupAntennasInfo();
	}

	@Override
	public StartupCLOIDL getStartupCLOInfo() throws AcsJTmcdbErrorEx {
		return tmcdb.getStartupCLOInfo();
	}

	@Override
	public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo()
			throws AcsJTmcdbErrorEx {
		return xmlTmcdb.getStartupWeatherStationControllerInfo();
	}

    @Override
    public XPDelay[] getCrossPolarizationDelays() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        return null;
    }

    @Override
    public String getTelescopeName() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getAntennaLOOffsettingIndex(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }
}
