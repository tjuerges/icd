/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.access;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.Properties;

import org.exolab.castor.xml.XMLException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.utils.AssemblyDataLoader;
import alma.tmcdb.utils.ConfigurationLoader;
import alma.tmcdb.utils.HibernateUtil;
import alma.tmcdb.utils.TmcdbException;
import alma.tmcdb.utils.TmcdbLoggerFactory;
import alma.tmcdb.utils.TmcdbUtils;

public class TmcdbStandaloneHibernateAccessor extends TmcdbHibernateAccessor {
    
    public TmcdbStandaloneHibernateAccessor()
        throws Exception {
        super();
        logger = TmcdbLoggerFactory.getLogger(TmcdbStandaloneHibernateAccessor.class.getName());
    }
    
    @Override
    public void clear() throws Exception {
        //TmcdbUtils.dropTables();
        HibernateUtil.shutdown();        
    }
    
    private Reader getConfigurationFile() throws FileNotFoundException {
        // First look in the current directory
        String currdir = System.getProperty("user.dir");
        String confFileLoc = currdir + "/Configuration.xml";
        File confFile = new File(confFileLoc);
        if (confFile.exists())
            return new FileReader(confFile);
        // Then look in (ACS/INT)ROOT/config
        String introot = System.getenv("INTROOT");
        confFileLoc = introot + "/config/Configuration.xml";
        confFile = new File(confFileLoc);
        if (confFile.exists())
            return new FileReader(confFile);
        String acsroot = System.getenv("ACSROOT");
        confFileLoc = acsroot + "/config/Configuration.xml";
        confFile = new File(confFileLoc);
        if (confFile.exists())
            return new FileReader(confFile);
        throw new FileNotFoundException();
    }

    private void loadDatabaseFromXML(Reader reader)
        throws XMLException, IOException, TmcdbException, DbConfigException {
        logger.finest("TmcdbStandaloneHibernateAccessor::loadDatabaseFromXML");
        logger.fine("loading dummy records");
        setupDummyRecords();
        // LruLoader.loadAllHwConfigFiles(true);
        // AssemblyRoleLoader.loadAssemblyRoles();
        logger.fine("loading configuration file");
        (new ConfigurationLoader()).loadConfiguration(reader);
        logger.fine("loading assembly data");
        AssemblyDataLoader.loadAssemblyData(true);
    }
    
    private void setupDummyRecords() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dummy:1.0");
        session.save(compType);
        Configuration config = new Configuration();
        config.setConfigurationName("Test");
        config.setFullName("Test");
        config.setActive(true);
        config.setCreationTime(new Date());
        config.setDescription("created by standalone TMCDBComponent");
        session.save(config);
        Component component = new Component();
        component.setComponentName("DUMMY");
        component.setComponentType(compType);
        component.setConfiguration(config);
        component.setImplLang("java");
        component.setRealTime(false);
        component.setCode("");
        component.setPath("");
        component.setIsAutostart(false);
        component.setIsDefault(false);
        component.setIsControl(false);
        component.setKeepAliveTime(0);
        component.setMinLogLevel((byte) 0);
        component.setMinLogLevelLocal((byte) 0);
        session.save(component);
        tx.commit();
        session.close();
    }
    
    @Override
    protected void initDb() throws Exception {
        logger.finest("TmcdbStandaloneHibernateAccessor::initDb");
        String url, user, password;
        String debug = System.getenv("TMCDB_STANDALONE_DEBUG");
        if (debug != null && debug.equalsIgnoreCase("true")) {
            logger.config("creating disk based database");
            TmcdbDbConfig tmcdbConfig = new TmcdbDbConfig(logger);
            HibernateUtil.createConfigurationFromDbConfig(tmcdbConfig);
            url = tmcdbConfig.getConnectionUrl();
            user = tmcdbConfig.getUsername();
            password = tmcdbConfig.getPassword();
        } else {
            logger.config("creating an in-memory database");
            // Creates a database entirely in memory
            final Properties props = new Properties();
            props.setProperty("hibernate.dialect",
                              "org.hibernate.dialect.HSQLDialect");
            props.setProperty("hibernate.connection.driver_class",
                              "org.hsqldb.jdbcDriver");
            props.setProperty("hibernate.connection.url",
                              "jdbc:hsqldb:mem:ignored");
            props.setProperty("hibernate.connection.username",
                              "sa");
            props.setProperty("hibernate.connection.password",
                              "");
            HibernateUtil.createConfigurationWithProperties(props);
            url = "jdbc:hsqldb:mem:ignored";
            user = "sa";
            password = "";
        }
        try {
            TmcdbUtils.dropTables(url, user, password);
        } catch (Exception ex) {} // fine, tables haven't been loaded yet
        TmcdbUtils.createTables(url, user, password);
        loadDatabaseFromXML(getConfigurationFile());
    }
}

