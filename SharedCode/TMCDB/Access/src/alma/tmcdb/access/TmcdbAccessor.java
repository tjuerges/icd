/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.access;

import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.BandFocusModel;
import alma.TMCDB.BandPointingModel;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB.XPDelay;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;

public interface TmcdbAccessor {

	public String getConfigurationName();

	public StartupAntennaIDL[] getStartupAntennasInfo();

	public StartupAOSTimingIDL getStartupAOSTimingInfo()
	 		throws AcsJTmcdbErrorEx;

	public StartupCLOIDL getStartupCLOInfo()
	 		throws AcsJTmcdbErrorEx;

	public AntennaIDL getAntennaInfo(String antennaName)
			throws AcsJTmcdbNoSuchRowEx;

	public PadIDL getCurrentAntennaPadInfo(String antennaName)
			throws AcsJTmcdbNoSuchRowEx;

	public AssemblyConfigXMLData getAssemblyConfigData(
			String serialNumber) throws AcsJTmcdbNoSuchRowEx;

	public AntennaDelays getCurrentAntennaDelays(String antenna)
	    throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
	
	public double[] getMetrologyCoefficients(String antenna);
	
	public ArrayReferenceLocation getArrayReferenceLocation();

	public void clear() throws Exception;
	
	public AssemblyConfigXMLData getComponentConfigData(String componentName);
    
    public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo()
    	throws AcsJTmcdbErrorEx;
    
    public BandPointingModel getCurrentAntennaPointingModel(String antennaName)
    	throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public BandFocusModel getCurrentAntennaFocusModel(String antennaName)
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public XPDelay[] getCrossPolarizationDelays()
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public String getTelescopeName()
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public int getAntennaLOOffsettingIndex(String antennaName)
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
    
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx;
}