package alma.tmcdb.access;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.BandFocusModel;
import alma.TMCDB.BandPointingModel;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB.ModelTerm;
import alma.TMCDB.XPDelay;
import alma.TMCDBComponentImpl.Antenna;
import alma.TMCDBComponentImpl.AssemblyLocation;
import alma.TMCDBComponentImpl.StartupAntenna;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AntennaPointingModelIDL;
import alma.TMCDB_IDL.AntennaPointingModelTermIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.PointingModelIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupPhotonicReferenceIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.tmcdb.utils.TmcdbLoggerFactory;

public class TmcdbXmlAccessor implements TmcdbAccessor {

	abstract class GenericXmlParser extends DefaultHandler {

	    String filename;
	    Logger logger;
	    
	    public GenericXmlParser(String infilename, Logger logger) {
	        filename = infilename;
	        this.logger = logger;
	        logger.info("Hey!");
	    }
	    
	    public void parse() throws SAXException,
	        ParserConfigurationException,IOException {
	        //get a factory
	        SAXParserFactory spf = SAXParserFactory.newInstance();
	        String acsCdb = java.lang.System.getenv("ACSROOT");
	        String location = java.lang.System.getenv("LOCATION");
	        //get a new instance of parser
	        SAXParser sp = spf.newSAXParser();
	                
	        File file;
	        file = new File(acsCdb + "/config/SIMTMCDB/"+filename+"-"+location+".xml");
	        if(!file.exists()) {
	            file = new File(acsCdb + "/config/SIMTMCDB/"+filename+".xml");
	            if (!file.exists()) {
	                String trueAcsCdbDir = java.lang.System.getenv("ACS_CDB");
	                if (trueAcsCdbDir != null) {
	                    String fn = trueAcsCdbDir + "/SIMTMCDB/" + filename + ".xml";                    
	                    File f = new File(fn);
	                    if(!f.exists()) {
	                        throw new IOException("File " + fn + " not found");
	                    }
	                    //parse the file and also register this class for call backs
	                    sp.parse(f, this);
	                    logger.info("reading file " + file.getName());
	                    return;
	                }
	        
	            }
	        }
	        logger.info("reading file " + file.getName());
	        //parse the file and also register this class for call backs
	        sp.parse(file, this);
	    }
	    //Event Handlers
	    public abstract void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException;
	    
	    public abstract void characters(char[] ch, int start, int length) 
	        throws SAXException;
	    
	    public abstract void endElement(String uri, String localName, String qName) 
	        throws SAXException;
	}

	class AntennaXMLParser extends GenericXmlParser {

	    List<StartupAntenna> antennaList;
	    List<String> deviceList; 
	    String antennaName;
	    String tempVal;
	    Map<String, String>  antennaPad;

	    public AntennaXMLParser(Map<String, String> map, Logger logger) {
	        super("StartupAntenna", logger);
	        antennaList = new ArrayList<StartupAntenna>();
	        antennaPad = map;
	    }

	    public List<StartupAntenna> getAntennaList() {
	        return antennaList;
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        //reset
	        tempVal = "";
	        if(qName.equalsIgnoreCase("Antenna")) {
	                deviceList = new ArrayList<String>();
	                antennaName = attributes.getValue("name");
	        }
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Antenna")) {
	                antennaList.add(createAntennaStartUp(antennaName,
	                                        antennaPad.get(antennaName),deviceList));
	        } else if(qName.equalsIgnoreCase("Assembly")) {
	                deviceList.add(tempVal);
	        }
	    }
	}

	class AntennaPadMapXMLParser extends GenericXmlParser {
	    Map<String, String> antennaPad = new HashMap<String, String>();
	    String tempVal;
	    String antennaName;
	    String padName;

	    public AntennaPadMapXMLParser(Logger logger) {
	        super("AntennaPadMap", logger);
	    }

	    public Map<String, String> getMap() {
	        return antennaPad;
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        //reset
	        tempVal = "";
	        if(qName.equalsIgnoreCase("Map")) {
	                antennaName = attributes.getValue("antenna");
	                padName = attributes.getValue("pad");
	        }
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Map")) {
	            antennaPad.put(antennaName, padName);
	        }
	    }
	}

	class PadXMLParser extends GenericXmlParser {
	   
	    Map<String, Pad> padList;
	    String padName;
	    String x;
	    String y;
	    String z;
	    String tempVal;
	    

	    public PadXMLParser(Logger logger) {
	        super("Pad", logger);
	        padList = new HashMap<String, Pad>();
	    }

	    public PadIDL getPadIDL(String padName) {
	        return padList.get(padName).toIDL();
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        //reset
	        tempVal = "";
	        if(qName.equalsIgnoreCase("Pad")) {
	            padName = attributes.getValue("name");
	            x = attributes.getValue("x");
	            y = attributes.getValue("y");
	            z = attributes.getValue("z");
	        }
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Pad")) {
	            Pad pad = new Pad();
	            pad.setPadName(padName);
	            pad.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
	            pad.setXPosition(new alma.hla.runtime.asdm.types.Length(new Double(x).doubleValue()));
	            pad.setYPosition(new alma.hla.runtime.asdm.types.Length(new Double(y).doubleValue()));
	            pad.setZPosition(new alma.hla.runtime.asdm.types.Length(new Double(z).doubleValue()));
	            padList.put(padName, pad);
	        }
	    }
	}

	class AOSTimingXMLParser extends GenericXmlParser {

	    List<AssemblyLocationIDL> assemblyList;
	    String tempVal;

	    public AOSTimingXMLParser(Logger logger) {
	        super("AOSTiming", logger);
	        assemblyList = new ArrayList<AssemblyLocationIDL>();
	    }

	    public AssemblyLocationIDL[] getAssemblies() {
	        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        tempVal = "";
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Assembly")) {
	                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
	                assembly.assemblyRoleName = tempVal; 
	                assembly.assemblyTypeName = "none";
	                assembly.baseAddress = 0;
	                assembly.channelNumber = 0;
	                assembly.rca = 0;
	                assemblyList.add(assembly);
	        }
	    }
	}


	class CLOXMLParser extends GenericXmlParser {

	    List<AssemblyLocationIDL> assemblyList; 
	    List<StartupPhotonicReferenceIDL> photoList;
	    List<AssemblyLocationIDL> photoAssemblyList;
	    String tempVal;
	    StartupPhotonicReferenceIDL tempPhoto;

	    public CLOXMLParser(Logger logger) {
	        super("CentralLO", logger);
	        assemblyList = new ArrayList<AssemblyLocationIDL>();
	        photoList = new ArrayList<StartupPhotonicReferenceIDL>();
	    }

	    public AssemblyLocationIDL[] getAssemblies() {
	        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
	    }

	    public StartupPhotonicReferenceIDL[] getPhoto() {
	        return ( StartupPhotonicReferenceIDL[] )photoList.toArray( new StartupPhotonicReferenceIDL[ photoList.size() ] );
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        tempVal = "";
	        if(qName.equalsIgnoreCase("PhotonicReference")) {
	            tempPhoto = new StartupPhotonicReferenceIDL();
	            photoAssemblyList = new ArrayList<AssemblyLocationIDL>();
	            tempPhoto.name = attributes.getValue("name");
	        }
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Assembly")) {
	                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
	                assembly.assemblyRoleName = tempVal; 
	                assembly.assemblyTypeName = "none";
	                assembly.baseAddress = 0;
	                assembly.channelNumber = 0;
	                assembly.rca = 0;
	                if (tempPhoto == null) {
	                    assemblyList.add(assembly);
	                } else {
	                    photoAssemblyList.add(assembly);
	                }
	        } else if (qName.equalsIgnoreCase("PhotonicReference")) {
	                tempPhoto.assemblies = ( AssemblyLocationIDL[] )photoAssemblyList.toArray( new AssemblyLocationIDL[ photoAssemblyList.size() ] );
	                photoList.add(tempPhoto);
	                tempPhoto = null;
	        }
	    }
	}

	class WeatherStationControllerXMLParser extends GenericXmlParser {

	    List<AssemblyLocationIDL> assemblyList;
	    String tempVal;

	    public WeatherStationControllerXMLParser(Logger logger) {
	        super("WeatherStationController", logger);
	        assemblyList = new ArrayList<AssemblyLocationIDL>();
	    }

	    public AssemblyLocationIDL[] getAssemblies() {
	        return ( AssemblyLocationIDL[] )assemblyList.toArray( new AssemblyLocationIDL[ assemblyList.size() ] );
	    }

	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        tempVal = "";
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("Assembly")) {
	                AssemblyLocationIDL assembly = new AssemblyLocationIDL();
	                assembly.assemblyRoleName = tempVal; 
	                assembly.assemblyTypeName = "none";
	                assembly.baseAddress = 0;
	                assembly.channelNumber = 0;
	                assembly.rca = 0;
	                assemblyList.add(assembly);
	        }
	    }
	}


	class ArrayReferenceXMLParser extends GenericXmlParser {

	    String tempVal;
	    ArrayReferenceLocation loc;
	    String x;
	    String y;
	    String z;

	    public ArrayReferenceXMLParser(Logger logger) {
	        super("ArrayReference", logger);
	    }

	    public ArrayReferenceLocation getReference() {
	        return loc;
	    }


	    public void startElement(String uri, String localName, String qName, 
	                Attributes attributes) throws SAXException {
	        tempVal = "";
	        if(qName.equalsIgnoreCase("ArrayReference")) {
	            x = attributes.getValue("x");
	            y = attributes.getValue("y");
	            z = attributes.getValue("z");
	        }
	    }
	    
	    
	    public void characters(char[] ch, int start, int length) 
	        throws SAXException {
	        tempVal = new String(ch,start,length);
	    }
	    
	    public void endElement(String uri, String localName, String qName) 
	        throws SAXException {
	        if(qName.equalsIgnoreCase("ArrayReference")) {
	            loc = new ArrayReferenceLocation();
	            loc.x = new Float(x).floatValue();
	            loc.y = new Float(y).floatValue();
	            loc.z = new Float(z).floatValue();
	        }
	    }
	}

	class ModelXMLParser extends GenericXmlParser {
	    // The current antenna or band we are parsing. only one of these
	    // should be non-nill at any one time.
	    String curAntenna;
	    Integer curBand;
	    // The accumulated terms in the antenna or band we are currently parsing
	    ArrayList<ModelTerm> curModel;

	    // Once we have completed parsing an antenna or band the data is
	    // moved from the cur* member variables )above into either the
	    // antModel or bandModel maps (below).
	    Map<String, ModelTerm[]> antModel = new HashMap<String, ModelTerm[]>();
	    Map<Integer, ModelTerm[]> bandModel = new HashMap<Integer, ModelTerm[]>();
	// TODO. Add support for a separate set of band offsets in the 7m antennas. 
	// Whats commented out below is a partial implementation
//	    Map<Integer, ModelTerm[]> bandModel7m = new HashMap<Integer, ModelTerm[]>();

	    // This string is just used in error messages
	    String filename;

	    public ModelXMLParser(String filename, Logger logger) {
	        super(filename, logger);
	        this.filename = filename + ".xml file";
	    }

	    public void startElement(String uri, String localName, String qName,
	            Attributes attributes) throws SAXException {
	        try {
	            if (qName.equalsIgnoreCase("Antenna")) {
	                curAntenna = attributes.getValue("name");
	                curModel = new ArrayList<ModelTerm>();
	            } else if (qName.equalsIgnoreCase("BandOffset")) {
	                curBand = Integer.valueOf(attributes.getValue("name"));
	                curModel = new ArrayList<ModelTerm>();
//	          } else if (qName.equalsIgnoreCase("BandOffset7m")) {
//	                 curBand = Integer.valueOf(attributes.getValue("name"));
//	                 curModel = new ArrayList<ModelTerm>();
	            } else if (qName.equalsIgnoreCase("Term")) {
	                String termName = attributes.getValue("name");
	                double termValue = Double.valueOf(attributes.getValue("value")).doubleValue();
	                if (curModel == null) {
	                    final String msg = filename + " is incorrectly structured.";
	                    throw new SAXException(msg);
	                }
	                curModel.add(new ModelTerm(termName, termValue));
	            }
	        } catch (NumberFormatException ex) {
	            final String msg = filename + " contains incorrect numbers.";
	            throw new SAXException(msg, ex);
	        }
	    }

	    public void characters(char[] ch, int start, int length)
	            throws SAXException {
	    }

	    public void endElement(String uri, String localName, String qName)
	            throws SAXException {
	        if (qName.equalsIgnoreCase("Antenna")) {
	            if (curModel == null) {
	                String msg = filename + " is incorrectly structured..";
	                throw new SAXException(msg);
	            }
	            antModel.put(curAntenna, curModel.toArray(new ModelTerm[0]));
	        } else if (qName.equalsIgnoreCase("BandOffset")) {
	            if (curModel == null) {
	                String msg = filename + " is incorrectly structured...";
	                throw new SAXException(msg);
	            }
	            bandModel.put(curBand, curModel.toArray(new ModelTerm[0]));
//	         } else if (qName.equalsIgnoreCase("BandOffset7m")) {
//	             if (curModel == null) {
//	                 String msg = filename + " is incorrectly structured...";
//	                 throw new SAXException(msg);
//	             }
//	             bandModel7m.put(curBand, curModel.toArray(new ModelTerm[0]));
	        }
	    }

	    public void TMCDBParse() throws AcsJTmcdbErrorEx {
	        try {
	            super.parse();
	        } catch (Exception ex) {
	            AcsJTmcdbErrorEx jex = new AcsJTmcdbErrorEx(ex);
	            jex.log(logger);
	            throw jex;
	        }
	    }

	    protected Map<String, ModelTerm[]> getAntennaModel() {
	        return antModel;
	    }

	    protected Map<Integer, ModelTerm[]> getBandModel() {
	        return bandModel;
	    }

//	     protected Map<Integer, ModelTerm[]> getBandModel7m() {
//	         return bandModel7m;
//	     }
	}

	class FocusModelXMLParser extends ModelXMLParser {
	    public FocusModelXMLParser(Logger logger) {
	        super("FocusModel", logger);
	    }
	    
	    public Map<String, ModelTerm[]> getAntennaFocusModel() {
	        return getAntennaModel();
	    }

	    public Map<Integer, ModelTerm[]> getBandFocusModel() {
	        return getBandModel();
	    }
	}

	class PointingModelXMLParser extends ModelXMLParser {
	    public PointingModelXMLParser(Logger logger) {
	        super("PointingModel", logger);
	    }
	    
	    public Map<String, ModelTerm[]> getAntennaPointingModel() {
	        return getAntennaModel();
	    }

	    public Map<Integer, ModelTerm[]> getBandPointingModel() {
	        return getBandModel();
	    }
	}


    protected Logger logger;
    private String configurationName;
    
    // The antenna focus model, for each antenna
    private Map<String, ModelTerm[]> antennaFocusModel = null;
    // The offsets in the focus model for each band
    private Map<Integer, ModelTerm[]> bandFocusModel = null;
        
    // The antenna pointing model, for each antenna
    private Map<String, ModelTerm[]> antennaPointingModel = null;
    // The offsets in the pointing model for each band
    private Map<Integer, ModelTerm[]> bandPointingModel = null;
            
    /**Map AntennaName with current PadName */
    Map<String, String> antennaPad = new HashMap<String, String>();
    
    public TmcdbXmlAccessor() {
        logger = TmcdbLoggerFactory.getLogger(TmcdbXmlAccessor.class.getName());
        logger.finest("creating TmcdbXmlAccessor");
        configurationName = System.getenv("TMCDB_CONFIGURATION_NAME");
    }
    
    /**
     * Yet another temporary hack while we finally get the TMCDB fully
     * implemented and working.
     * This function, implemented in the TMCDB base class, is overriden 
     * in order to set the configuration that this component must provide.
     */

        
    public static StartupAntenna createAntennaStartUp(String antennaName, String
            padName, List<String> deviceList){
        StartupAntenna ant = new StartupAntenna ();
        ant.setAntennaName(antennaName);
        ant.setPadName(padName);
        if(antennaName.substring(0, 2).equals("DV") || 
           antennaName.substring(0, 2).equals("LA")){
            ant.setUiDisplayOrder((short) Integer.valueOf(antennaName.substring(2, 4)).intValue()  );
        } else if (antennaName.substring(0, 2).equals("DA")){
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()-15));
        } else if (antennaName.substring(0, 2).equals("PM")) {
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+62));
        } else {//CM case
            ant.setUiDisplayOrder((short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+50));
        }

        ant.setFrontEndName("none");

        AssemblyLocation[] FeDeviceList = new AssemblyLocation[15];
        for (int i = 0; i < FeDeviceList.length; ++i) {
            FeDeviceList[i] = new AssemblyLocation ();
            FeDeviceList[i].setAssemblyRoleName("");
            FeDeviceList[i].setAssemblyTypeName("none");
            FeDeviceList[i].setBaseAddress(0);
            FeDeviceList[i].setChannelNumber(0);
            FeDeviceList[i].setRca(0);
        }
        FeDeviceList[0].setAssemblyRoleName("ColdCart3");
        FeDeviceList[1].setAssemblyRoleName("ColdCart6");
        FeDeviceList[2].setAssemblyRoleName("ColdCart7");
        FeDeviceList[3].setAssemblyRoleName("ColdCart9");
        FeDeviceList[4].setAssemblyRoleName("Cryostat");
        FeDeviceList[5].setAssemblyRoleName("IFSwitch");
        FeDeviceList[6].setAssemblyRoleName("LPR");
        FeDeviceList[7].setAssemblyRoleName("PowerDist3");
        FeDeviceList[8].setAssemblyRoleName("PowerDist6");
        FeDeviceList[9].setAssemblyRoleName("PowerDist7");
        FeDeviceList[10].setAssemblyRoleName("PowerDist9");
        FeDeviceList[11].setAssemblyRoleName("WCA3");
        FeDeviceList[12].setAssemblyRoleName("WCA6");
        FeDeviceList[13].setAssemblyRoleName("WCA7");
        FeDeviceList[14].setAssemblyRoleName("WCA9");
        ant.setFrontEndAssembly(FeDeviceList);

        AssemblyLocation[] devices = new AssemblyLocation[deviceList.size()];
        for (int i = 0; i < devices.length; ++i) {
            devices[i] = new AssemblyLocation ();
            devices[i].setAssemblyRoleName("");
            devices[i].setAssemblyTypeName("none");
            devices[i].setBaseAddress(0);
            devices[i].setChannelNumber(0);
            devices[i].setRca(0);
        }

        for (int i=0;i<deviceList.size();i++)
            devices[i].setAssemblyRoleName(deviceList.get(i));

        ant.setAntennaAssembly(devices);
        return ant;

    }
    

    public void readAntennaPadMap() {
            //Creat antena pad Map
            AntennaPadMapXMLParser mapparser = new AntennaPadMapXMLParser(logger);
            try {
                mapparser.parse();
                antennaPad = mapparser.getMap();
            } catch (Exception e) {
                //                e.printStackTrace();
                //                logger.severe("Error while parsing Antena Pad map file");
            }
    }

    public StartupAntenna[] getStartUpAntennasInfo()  {
            List<StartupAntenna> antennaList = new ArrayList<StartupAntenna>();
           
            //Make sure we have the antenna pad map
            readAntennaPadMap();
            
            //Crean antennas
            AntennaXMLParser antparser = new AntennaXMLParser(antennaPad, logger);
            try {
                antparser.parse();
                antennaList = antparser.getAntennaList();
            } catch (Exception e) {
                //                e.printStackTrace();
                //                logger.severe("Error while parsing antenna file");
            }

            StartupAntenna[] ants = new StartupAntenna[antennaList.size()];
            for(int i=0;i < antennaList.size();i++)
                ants[i] = antennaList.get(i);
                
            return ants;
        }

    ///////////////////////////////
    // TMCDB External Operations //
    ///////////////////////////////


    public StartupAntennaIDL[] getStartupAntennasInfo() {

        StartupAntenna[] antenna = null;

        antenna = getStartUpAntennasInfo();
        StartupAntennaIDL[] list = new StartupAntennaIDL [antenna.length];
        for (int i = 0; i < list.length; ++i) {
            list[i] = new StartupAntennaIDL();
            list[i].antennaName = antenna[i].getAntennaName();
            list[i].padName = antenna[i].getPadName();
            list[i].frontEndName = antenna[i].getFrontEndName();
            list[i].uiDisplayOrder  = antenna[i].getUiDisplayOrder();
            AssemblyLocation[] loc = antenna[i].getFrontEndAssembly();
            list[i].frontEndAssembly = new AssemblyLocationIDL [loc.length];
            for (int j = 0; j < list[i].frontEndAssembly.length; ++j) {
                list[i].frontEndAssembly[j] = new AssemblyLocationIDL ();
                list[i].frontEndAssembly[j].assemblyTypeName = loc[j].getAssemblyTypeName();
                list[i].frontEndAssembly[j].assemblyRoleName = loc[j].getAssemblyRoleName();
                list[i].frontEndAssembly[j].rca = loc[j].getRca();
                list[i].frontEndAssembly[j].channelNumber = loc[j].getChannelNumber();
                list[i].frontEndAssembly[j].baseAddress = loc[j].getBaseAddress();
            }
            loc = antenna[i].getAntennaAssembly();
            list[i].antennaAssembly = new AssemblyLocationIDL [loc.length];
            for (int j = 0; j < list[i].antennaAssembly.length; ++j) {
                list[i].antennaAssembly[j] = new AssemblyLocationIDL ();
                list[i].antennaAssembly[j].assemblyTypeName = loc[j].getAssemblyTypeName();
                list[i].antennaAssembly[j].assemblyRoleName = loc[j].getAssemblyRoleName();
                list[i].antennaAssembly[j].rca = loc[j].getRca();
                list[i].antennaAssembly[j].channelNumber = loc[j].getChannelNumber();
                list[i].antennaAssembly[j].baseAddress = loc[j].getBaseAddress();
            }
        }
        return list;
    }

    @Override
    public StartupAOSTimingIDL getStartupAOSTimingInfo() throws AcsJTmcdbErrorEx {
        AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
        AOSTimingXMLParser parser = new AOSTimingXMLParser(logger);
        try {
            parser.parse();
            assemblies = parser.getAssemblies();
        } catch (Exception e) {
            //            e.printStackTrace();
            //            logger.severe("Error while parsing pad file");
        }

        return new StartupAOSTimingIDL(assemblies);
    }

    public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo() throws AcsJTmcdbErrorEx {
        AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
        WeatherStationControllerXMLParser parser = new WeatherStationControllerXMLParser(logger);
        try {
            parser.parse();
            assemblies = parser.getAssemblies();
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe("Error while parsing WeatherStationController file");
        }

        return new StartupWeatherStationControllerIDL(assemblies);
    }

    public StartupCLOIDL getStartupCLOInfo() throws AcsJTmcdbErrorEx {
        AssemblyLocationIDL[] assemblies = new AssemblyLocationIDL[0];
        StartupPhotonicReferenceIDL[] photonicRef = new StartupPhotonicReferenceIDL[0];
        CLOXMLParser parser = new CLOXMLParser(logger);
        try {
            parser.parse();
            assemblies = parser.getAssemblies();
            photonicRef = parser.getPhoto();
        } catch (Exception e) {
            //                e.printStackTrace();
            //                logger.severe("Error while parsing pad file");
        }
        return new StartupCLOIDL(assemblies, photonicRef);
    }
    
    public AntennaIDL getAntennaInfo(String antennaName) throws AcsJTmcdbNoSuchRowEx  {
        Antenna antenna = new Antenna ();
        if(antennaName.substring(0, 2).equals("DV") || 
            antennaName.substring(0, 2).equals("DA") ||
            antennaName.substring(0, 2).equals("LA")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("tewlveMeter");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2009,2,6,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(7.0));
        } else if (antennaName.substring(0, 2).equals("PM")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("totalPower");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(7.5));
        } else if(antennaName.substring(0, 2).equals("CM")) {
            antenna.setAntennaName(antennaName);
            antenna.setAntennaType("sevenMeter");
            antenna.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
            antenna.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            antenna.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antenna.setZPosition(new alma.hla.runtime.asdm.types.Length(0.0));
        }
        antenna.setComponentId(0);
        //antenna.setBaseElementId(2);
        //antenna.setComputerId(0); // TODO: Verify that removal is correct
        //antenna.setConfigurationId(1);
        antenna.setXOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        antenna.setYOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        antenna.setZOffset(new alma.hla.runtime.asdm.types.Length(0.0));
        return antenna.toIDL();
    }

    public PadIDL getCurrentAntennaPadInfo(String antennaName) throws AcsJTmcdbNoSuchRowEx  {
            String padName=null;
            PadIDL pad = new PadIDL();
            PadXMLParser parser = new PadXMLParser(logger);
            //make sure we have the antenna pad map
            readAntennaPadMap();
            try{
                padName=antennaPad.get(antennaName);
                //              System.out.println("padName="+ padName);
            }catch (java.lang.NullPointerException exce1){
                logger.severe("Antenna "+antennaName+ " doesn't exist");
                exce1.printStackTrace();
            }
            if(padName == null) {
                AcsJTmcdbNoSuchRowEx ex = new AcsJTmcdbNoSuchRowEx("There is no such antenna as " + antennaName);
                throw ex;
            }
            try {
                parser.parse();
                pad = parser.getPadIDL(padName);
            } catch (Exception e) {
                //                e.printStackTrace();
                //                logger.severe("Error while parsing pad file");
            }
            return pad;
    }

    public PointingModelIDL getPMData(String antennaName) throws AcsJTmcdbNoSuchRowEx {
        PointingModelIDL x = new PointingModelIDL ();
        x.antennaName = antennaName;
       // x.padName = antennaPad.get(antennaName);
        x.padName = getCurrentAntennaPadInfo(antennaName).PadName;
        x.pointingModel = new AntennaPointingModelIDL ();
        x.pointingModel.AntennaId = 1;
        x.pointingModel.AsdmUID = "none";
        x.pointingModel.PadId = 1;
        x.pointingModel.PointingModelId = 0;
        alma.hla.runtime.asdm.types.ArrayTime t = new alma.hla.runtime.asdm.types.ArrayTime(2006,10,10,8,0,0.0);
        x.pointingModel.StartTime = t.toIDLArrayTime();
        x.pointingModel.StartValidTime = t.toIDLArrayTime();
        x.pointingModel.EndValidTime = new alma.asdmIDLTypes.IDLArrayTime (0);          
        x.term = new AntennaPointingModelTermIDL [18];
        for (int i = 0; i < x.term.length; ++i) {
            x.term[i] = new AntennaPointingModelTermIDL ();
            x.term[i].PointingModelId = 0;
            x.term[i].CoeffError = 0.0F;
            x.term[i].CoeffValue = 0.0F;
        }
        x.term[0].CoeffName = "IA";
        x.term[1].CoeffName = "IE";
        x.term[2].CoeffName = "HASA";
        x.term[3].CoeffName = "HACA";
        x.term[4].CoeffName = "HESE";
        x.term[5].CoeffName = "HECE";
        x.term[6].CoeffName = "HESA";
        x.term[7].CoeffName = "HASA2";
        x.term[8].CoeffName = "HACA2";
        x.term[9].CoeffName = "HESA2";
        x.term[10].CoeffName = "HECA2";
        x.term[11].CoeffName = "HACA3";
        x.term[12].CoeffName = "HECA3";
        x.term[13].CoeffName = "HESA3";
        x.term[14].CoeffName = "NPAE";
        x.term[15].CoeffName = "CA";
        x.term[16].CoeffName = "AN";
        x.term[17].CoeffName = "AW";
        return x;
    }

    public PointingModelIDL getPointingModelInfo(String antennaName) throws AcsJTmcdbNoSuchRowEx {
        return getPMData(antennaName);
    }

    public PointingModelIDL getRecentPointingModelInfo(String antennaName) throws AcsJTmcdbNoSuchRowEx {
        return getPMData(antennaName);
    }

    public PointingModelIDL[] getPointingModelsInfo(String antennaName) throws AcsJTmcdbNoSuchRowEx {
        PointingModelIDL[] x = new PointingModelIDL [1];
        x[0] = getPMData(antennaName);
        return x;
    }
    
    public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber) throws AcsJTmcdbNoSuchRowEx {
        AssemblyConfigXMLData data = new AssemblyConfigXMLData();
        data.xmlDoc = "";
        data.schema = "";
        return data;
    }

    public AssemblyConfigXMLData getComponentConfigData(String componentName) {
        return null;
    }

    @Override
    public double[] getMetrologyCoefficients(String antennaName) {
        double[] coeffs = new double[2];
        coeffs[0] = 0.0;
        coeffs[1] = 0.0;
        return coeffs;
    }

    @Override
    public AntennaDelays getCurrentAntennaDelays(String antennaName) {
    	return null;
    }

    @Override
    public ArrayReferenceLocation getArrayReferenceLocation() {
        ArrayReferenceLocation loc = null;
        ArrayReferenceXMLParser parser = new ArrayReferenceXMLParser(logger);
        try {
            parser.parse();
            loc = parser.getReference();
        } catch (Exception e) {
            //            e.printStackTrace();
            //            logger.severe("Error while parsing array reference file");
        }
        if(loc == null){
            loc = new ArrayReferenceLocation();
            loc.x = 2202175.078;
            loc.y = -5445230.603;
            loc.z = -2485310.452;
        }
        return loc;
    }
    
   static public boolean isAntennaNameValid(String antennaName) {
        if (antennaName.length() != 4) {
            return false;
        }
        final String prefix = antennaName.substring(0, 2).toUpperCase();
        short number;
        try {
            number = new Integer(antennaName.substring(2, 4)).shortValue();
        } catch (NumberFormatException ex) {
            return false;
        }

        if ((prefix.equals("DV") && number >= 1 && number <= 25)
                || (prefix.equals("DA") && number >= 41 && number <= 65)
                || (prefix.equals("PM") && number >= 1 && number <= 4)
                || (prefix.equals("CM") && number >= 1 && number <= 12)) {
            return true;
        }
        return false;
    }

    public BandFocusModel getCurrentAntennaFocusModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
//        if (!TMCDBSimComponentImpl.isAntennaNameValid(antennaName)) {
//            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
//            jex.setProperty("Detail", "Antenna '" + antennaName
//                    + "' is not a recognized antenna name.");
//            jex.log(logger);
//            throw jex;
//        }
//
//        // Always reload the focus model from the TMCDB.
//        // TODO. Only load a new model if its has changed. To do this
//        // I need a function that tells me if the focus model has
//        // changed.
//        antennaFocusModel = null;
//        bandFocusModel = null;
//
//        if (antennaFocusModel == null) {
//            FocusModelXMLParser parser = new FocusModelXMLParser(logger);
//            parser.TMCDBParse();
//            antennaFocusModel = parser.getAntennaFocusModel();
//            bandFocusModel = parser.getBandFocusModel();
//        }
//        final String upCaseName = antennaName.toUpperCase();
//        if (antennaFocusModel.containsKey(upCaseName)) {
//            return antennaFocusModel.get(upCaseName);
//        } else {
//            return new ModelTerm[0];
//        }
        return null;
    }

    public ModelTerm[] getCurrentBandFocusModel(short band,
            boolean for12MAntenna) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO. Work out how to support the 7m antennas.
        // Make sure its a valid band name
        if (band < 1 || band > 10) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Band numbers must be between 1 and 10."
                    + " Band " + band + " is not allowed.");
            jex.log(logger);
            throw jex;
        }

        // Always reload the focus model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the focus model has
        // changed.
        antennaFocusModel = null;
        bandFocusModel = null;

        if (bandFocusModel == null) {
            FocusModelXMLParser parser = new FocusModelXMLParser(logger);
            parser.TMCDBParse();
            antennaFocusModel = parser.getAntennaFocusModel();
            bandFocusModel = parser.getBandFocusModel();
        }
        final Integer bandNum = (int) band;
        if (bandFocusModel.containsKey(bandNum)) {
            return bandFocusModel.get(bandNum);
        } else {
            return new ModelTerm[0];
        }
    }

    public BandPointingModel getCurrentAntennaPointingModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
//        if (!TMCDBSimComponentImpl.isAntennaNameValid(antennaName)) {
//            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
//            jex.setProperty("Detail", "Antenna '" + antennaName
//                    + "' is not a recognized antenna name.");
//            jex.log(logger);
//            throw jex;
//        }
//
//        // Always reload the pointing model from the TMCDB.
//        // TODO. Only load a new model if its has changed. To do this
//        // I need a function that tells me if the pointing model has
//        // changed.
//        antennaPointingModel = null;
//        bandPointingModel = null;
//
//        if (antennaPointingModel == null) {
//            PointingModelXMLParser parser = new PointingModelXMLParser(logger);
//            parser.TMCDBParse();
//            antennaPointingModel = parser.getAntennaPointingModel();
//            bandPointingModel = parser.getBandPointingModel();
//        }
//        final String upCaseName = antennaName.toUpperCase();
//        if (antennaPointingModel.containsKey(upCaseName)) {
//            return antennaPointingModel.get(upCaseName);
//        } else {
//            return new ModelTerm[0];
//        }
        return null;
    }

    public ModelTerm[] getCurrentBandPointingModel(short band,
            boolean for12MAntenna) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO. Work out how to support the 7m antennas.
        // Make sure its a valid band name
        if (band < 0 || band > 10) {
            AcsJTmcdbNoSuchRowEx jex = new AcsJTmcdbNoSuchRowEx();
            jex.setProperty("Detail", "Band numbers must be between 0 and 10."
                    + " Band " + band + " is not allowed.");
            jex.log(logger);
            throw jex;
        }

        // Always reload the pointing model from the TMCDB.
        // TODO. Only load a new model if its has changed. To do this
        // I need a function that tells me if the pointing model has
        // changed.
        antennaPointingModel = null;
        bandPointingModel = null;

        if (bandPointingModel == null) {
            PointingModelXMLParser parser = new PointingModelXMLParser(logger);
            parser.TMCDBParse();
            antennaPointingModel = parser.getAntennaPointingModel();
            bandPointingModel = parser.getBandPointingModel();
        }
        final Integer bandNum = (int) band;
        if (bandPointingModel.containsKey(bandNum)) {
            return bandPointingModel.get(bandNum);
        } else {
            return new ModelTerm[0];
        }
     }
	
	@Override
	public String getConfigurationName() {
		return configurationName;
	}

	@Override
	public void clear() throws Exception {
		// TODO Auto-generated method stub
		
	}

    @Override
    public XPDelay[] getCrossPolarizationDelays() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getTelescopeName() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getAntennaLOOffsettingIndex(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

}
