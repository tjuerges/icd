package alma.tmcdb.access;

import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.BandFocusModel;
import alma.TMCDB.BandPointingModel;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB.XPDelay;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.acs.component.ComponentLifecycleException;

public class TmcdbStandaloneMixedAccessor implements TmcdbAccessor {

    /** Main TMCDB Accessor, uses Hibernate to access the database */
    TmcdbAccessor tmcdb;

    /** The temporary XML TMCDB Accessor, taken from the simulation impl */
    TmcdbAccessor xmlTmcdb;
    
    public TmcdbStandaloneMixedAccessor() throws ComponentLifecycleException {
        try {
            this.tmcdb = new TmcdbStandaloneHibernateAccessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ComponentLifecycleException(ex);
        }
        this.xmlTmcdb = new TmcdbXmlAccessor();
    }

    @Override
    public void clear() throws Exception {
        if (tmcdb != null) {
            try {
                tmcdb.clear();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public AntennaIDL getAntennaInfo(String antennaName)
            throws AcsJTmcdbNoSuchRowEx {
        return tmcdb.getAntennaInfo(antennaName);
    }

    @Override
    public ArrayReferenceLocation getArrayReferenceLocation() {
        return xmlTmcdb.getArrayReferenceLocation();
    }

    @Override
    public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber)
            throws AcsJTmcdbNoSuchRowEx {
        return tmcdb.getAssemblyConfigData(serialNumber);
    }

    @Override
    public AssemblyConfigXMLData getComponentConfigData(String componentName) {
        return tmcdb.getComponentConfigData(componentName);
    }

    @Override
    public String getConfigurationName() {
        return tmcdb.getConfigurationName();
    }

    @Override
    public AntennaDelays getCurrentAntennaDelays(String antenna)
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        return xmlTmcdb.getCurrentAntennaDelays(antenna);
    }

    @Override
    public BandFocusModel getCurrentAntennaFocusModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        return xmlTmcdb.getCurrentAntennaFocusModel(antennaName);
    }

    @Override
    public PadIDL getCurrentAntennaPadInfo(String antennaName)
            throws AcsJTmcdbNoSuchRowEx {
        return xmlTmcdb.getCurrentAntennaPadInfo(antennaName);
    }

    @Override
    public BandPointingModel getCurrentAntennaPointingModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        return xmlTmcdb.getCurrentAntennaPointingModel(antennaName);
    }

    @Override
    public double[] getMetrologyCoefficients(String antenna) {
        return xmlTmcdb.getMetrologyCoefficients(antenna);
    }

    @Override
    public StartupAOSTimingIDL getStartupAOSTimingInfo()
            throws AcsJTmcdbErrorEx {
        return tmcdb.getStartupAOSTimingInfo();
    }

    @Override
    public StartupAntennaIDL[] getStartupAntennasInfo() {
        return tmcdb.getStartupAntennasInfo();
    }

    @Override
    public StartupCLOIDL getStartupCLOInfo() throws AcsJTmcdbErrorEx {
        return tmcdb.getStartupCLOInfo();
    }

    @Override
    public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo()
            throws AcsJTmcdbErrorEx {
        return xmlTmcdb.getStartupWeatherStationControllerInfo();
    }

    @Override
    public XPDelay[] getCrossPolarizationDelays() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        return null;
    }

    @Override
    public String getTelescopeName() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getAntennaLOOffsettingIndex(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        // TODO Auto-generated method stub
        return null;
    }   
}
