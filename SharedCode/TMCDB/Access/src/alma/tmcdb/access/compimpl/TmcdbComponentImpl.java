/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TMCDBComponent.java
 */
package alma.tmcdb.access.compimpl;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.TMCDB.AccessOperations;
import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.BandFocusModel;
import alma.TMCDB.BandPointingModel;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB.XPDelay;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.PointingModelIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.TmcdbDuplicateKeyEx;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.TmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.TmcdbSqlEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.tmcdb.access.TmcdbAccessor;
import alma.tmcdb.access.TmcdbHibernateAccessor;
import alma.tmcdb.access.TmcdbMixedAccessor;
import alma.tmcdb.utils.TmcdbLoggerFactory;

/**
 * The TMCDBComponent is the way the CONTROL subsystem gets its configuration
 * information.
 * 
 * @author Rafael Hiriart (rhiriart@nrao.edu)
 *
 */
public class TmcdbComponentImpl
    implements AccessOperations, ComponentLifecycle {

    /**
     * The ACS container services.
     */
    protected ContainerServices container;
    
    /**
     * The ACS Logger.
     */
    protected Logger logger;

    /** TMCDB Accessor */
    TmcdbAccessor tmcdb;
    
    /** Startup antenna information to be setup from test cases. */
    private StartupAntennaIDL[] testStartupAntennasInfo;

    /** Antenna information to be setup from test cases. */
    private Map<String, AntennaIDL> testAntennaInfo =
        new HashMap<String, AntennaIDL>();

    /** Antenna pad information to be setup from test cases. */
    private Map<String, PadIDL> testPadInfo =
        new HashMap<String, PadIDL>();

    /** Pointing model information to be setup from test cases. */
    private PointingModelIDL testPointingModelInfo;
    
    private StartupCLOIDL testCentralLOInfo;
    
    /**
     * Constructor. 
     */
    public TmcdbComponentImpl() {
    	super();
    }
    
    @Override
    public void aboutToAbort() {
        cleanUp();
    }
    
    @Override
    public void cleanUp() {
        if (tmcdb != null) {
            try {
                tmcdb.clear();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }
    
    @Override
    public void execute() throws ComponentLifecycleException {}
    
    @Override
	public AntennaIDL getAntennaInfo(String antennaName)
	    throws TmcdbSqlEx, TmcdbNoSuchRowEx, TmcdbDuplicateKeyEx {
        if (testAntennaInfo.containsKey(antennaName))
            return testAntennaInfo.get(antennaName);
	    try {
            return tmcdb.getAntennaInfo(antennaName);
        } catch (AcsJTmcdbNoSuchRowEx ex) {
            throw ex.toTmcdbNoSuchRowEx();
        }
	}
    
    @Override
    public int getAntennaLOOffsettingIndex(String antennaName)
            throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getAntennaLOOffsettingIndex(antennaName);
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }
    
    @Override
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
            throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getAntennaWalshFunctionSeqNumber(antennaName);
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }
    
    @Override
    public ArrayReferenceLocation getArrayReferenceLocation() {
    	return tmcdb.getArrayReferenceLocation();
    }

    @Override
	public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber)
	    throws TmcdbSqlEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getAssemblyConfigData(serialNumber);
        } catch (AcsJTmcdbNoSuchRowEx ex) {
            throw ex.toTmcdbNoSuchRowEx();
        }
	}
	
    @Override
    public String getConfigurationName() throws TmcdbErrorEx {
        return tmcdb.getConfigurationName();
    }

    @Override
    public XPDelay[] getCrossPolarizationDelays() throws TmcdbErrorEx,
            TmcdbNoSuchRowEx {
        try {
            return tmcdb.getCrossPolarizationDelays();
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }    
    
    @Override
    public AntennaDelays getCurrentAntennaDelays(String antennaName)
            throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getCurrentAntennaDelays(antennaName);
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }

    @Override
	public BandFocusModel getCurrentAntennaFocusModel(String antennaName)
			throws TmcdbErrorEx, TmcdbNoSuchRowEx {
		try {
			return tmcdb.getCurrentAntennaFocusModel(antennaName);
		} catch (AcsJTmcdbErrorEx e) {
			throw e.toTmcdbErrorEx();
		} catch (AcsJTmcdbNoSuchRowEx e) {
			throw e.toTmcdbNoSuchRowEx();
		}
	}

    @Override
	public PadIDL getCurrentAntennaPadInfo(String antennaName)
	    throws TmcdbSqlEx,
	           TmcdbNoSuchRowEx,
	           TmcdbDuplicateKeyEx,
	           TmcdbRowAlreadyExistsEx {
        if (testPadInfo.containsKey(antennaName))
            return testPadInfo.get(antennaName);
	    try {
            return tmcdb.getCurrentAntennaPadInfo(antennaName);
        } catch (AcsJTmcdbNoSuchRowEx ex) {
            throw ex.toTmcdbNoSuchRowEx();
        }
	}    

    @Override
	public BandPointingModel getCurrentAntennaPointingModel(String antennaName)
			throws TmcdbErrorEx, TmcdbNoSuchRowEx {
		try {
			return tmcdb.getCurrentAntennaPointingModel(antennaName);
		} catch (AcsJTmcdbErrorEx e) {
			throw e.toTmcdbErrorEx();
		} catch (AcsJTmcdbNoSuchRowEx e) {
			throw e.toTmcdbNoSuchRowEx();
		}
	}
    
	@Override
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getHolographyTowerRelativePadDirection(padName);
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }

	@Override
    public double[] getMetrologyCoefficients(String antennaName) {
        return tmcdb.getMetrologyCoefficients(antennaName);
    }

	@Override
	public StartupAntennaIDL[] getStartupAntennasInfo() throws TmcdbErrorEx {
	    return testStartupAntennasInfo == null ?
	           tmcdb.getStartupAntennasInfo() :
	           testStartupAntennasInfo;
	}

	@Override
    public StartupAOSTimingIDL getStartupAOSTimingInfo() throws TmcdbErrorEx {
    	// TODO no setter for test cases?
        try {
			return tmcdb.getStartupAOSTimingInfo();
		} catch (AcsJTmcdbErrorEx ex) {
			throw ex.toTmcdbErrorEx();
		}
    }

    @Override
    public StartupCLOIDL getStartupCLOInfo() throws TmcdbErrorEx {
        if (testCentralLOInfo != null) {
            return testCentralLOInfo;
        }
        try {
			return tmcdb.getStartupCLOInfo();
		} catch (AcsJTmcdbErrorEx ex) {
			throw ex.toTmcdbErrorEx();
		}
    }

    @Override
	public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo()
			throws TmcdbErrorEx {
		try {
			return tmcdb.getStartupWeatherStationControllerInfo();
		} catch (AcsJTmcdbErrorEx e) {
			throw e.toTmcdbErrorEx();
		}
	}

    @Override
    public String getTelescopeName() throws TmcdbErrorEx, TmcdbNoSuchRowEx {
        try {
            return tmcdb.getTelescopeName();
        } catch (AcsJTmcdbErrorEx e) {
            throw e.toTmcdbErrorEx();
        } catch (AcsJTmcdbNoSuchRowEx e) {
            throw e.toTmcdbNoSuchRowEx();
        }
    }

    @Override
    public void initialize(ContainerServices cs)
        throws ComponentLifecycleException {
        this.container = cs;
        this.logger = cs.getLogger();
        TmcdbLoggerFactory.SINGLETON.setLogger(this.logger);
        try {
			this.tmcdb = new TmcdbHibernateAccessor();
		} catch (Exception ex) {
            ex.printStackTrace();
            throw new ComponentLifecycleException(ex);
		}
    }

    @Override
    public String name() {
        return container.getName();
    }

    /**
     * Sets up the antennas information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    @Override
    public void setAntennaInfo(String an, AntennaIDL ai) {
        testAntennaInfo.put(an, ai);
    }

    /**
     * Sets up the antenna pads information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    @Override
    public void setAntennaPadInfo(String an, PadIDL api) {
        testPadInfo.put(an, api);
    }

    /**
     * Sets up the startup antennas information. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    @Override
    public void setStartupAntennasInfo(StartupAntennaIDL[] sai) {
        testStartupAntennasInfo = sai;
    }

    /**
     * Sets up the CentraLO startup info. This function provides a way to
     * set up this structure from test cases.
     * This is a temporary hack while a way to do this is implemented at the
     * TMCDB layer.
     */
    @Override
    public void setStartupCLOInfo(StartupCLOIDL clo) {
        testCentralLOInfo = clo;
        
    }
}
