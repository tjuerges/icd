/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: TmcdbHibernateAccessor.java,v 1.9 2010/12/15 21:57:28 rhiriart Exp $"
 */
package alma.tmcdb.access;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.TMCDB.AntennaDelays;
import alma.TMCDB.ArrayReferenceLocation;
import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB.HolographyTowerRelativePadDirection;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupPhotonicReferenceIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbNoSuchRowEx;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.AOSTiming;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.Assembly;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementStartupType;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.CentralLO;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.FocusModel;
import alma.tmcdb.domain.HolographyTowerToPad;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.HwSchema;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.domain.WeatherStationController;
import alma.tmcdb.domain.XPDelay;
import alma.tmcdb.translation.JavaToIdlTranslator;
import alma.tmcdb.utils.HibernateUtil;
import alma.tmcdb.utils.TmcdbLoggerFactory;

/**
 * The TMCDBAccessor that get the data from the TMCDB database using Hibernate.
 * 
 * @author Rafael Hiriart (rhiriart@nrao.edu)
 *
 */
public class TmcdbHibernateAccessor implements TmcdbAccessor {

    protected Logger logger;
    private String configurationName;
    private String startupScenarioName;
    private TmcdbDbConfig dbconf;
    
    public TmcdbHibernateAccessor() throws Exception {
        this.logger = TmcdbLoggerFactory.getLogger(TmcdbHibernateAccessor.class.getName());
        this.dbconf = new TmcdbDbConfig(logger);
        this.configurationName = System.getenv("TMCDB_CONFIGURATION_NAME");
        this.startupScenarioName = System.getenv("TMCDB_STARTUP_NAME");
        initDb();
    }
    
    public void clear() throws Exception {
        HibernateUtil.shutdown();        
    }

    @Override
    public AntennaIDL getAntennaInfo(String antennaName)
        throws AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna ant = getAntennaFromConfiguration(getHwConfiguration(session),
                                                  antennaName);
        tx.commit();
        session.close();      
        if (ant != null )
            return JavaToIdlTranslator.toIDL(ant);
        else
            throw new AcsJTmcdbNoSuchRowEx();
    }
    
    @Override
	public ArrayReferenceLocation getArrayReferenceLocation() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        HwConfiguration hwConfiguration = getHwConfiguration(session);
        ArrayReferenceLocation loc = new ArrayReferenceLocation();
        loc.x = hwConfiguration.getArrayReference().getX();
        loc.y = hwConfiguration.getArrayReference().getY();
        loc.z = hwConfiguration.getArrayReference().getZ();
        tx.commit();
        session.close();        
        return loc;
	}
    
    @Override
    public AssemblyConfigXMLData getAssemblyConfigData(String serialNumber)
        throws AcsJTmcdbNoSuchRowEx{
        AssemblyConfigXMLData data = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        String qstr = "FROM Assembly assembly " +
                      "WHERE assembly.serialNumber = :serialNumber " +
                      "AND assembly.configuration = :configuration";
        Query query = session.createQuery(qstr);
        query.setParameter("serialNumber",
                           serialNumber,
                           Hibernate.STRING);
        query.setParameter("configuration",
                           getHwConfiguration(session),
                           Hibernate.entity(HwConfiguration.class));
        Assembly assembly = (Assembly) query.uniqueResult();
        if (assembly != null) {
            
            qstr = "FROM HwSchema WHERE assemblyType = :assemblyType AND configuration = :configuration";
            query = session.createQuery(qstr);
            query.setParameter("assemblyType",
                    assembly.getAssemblyType(),
                    Hibernate.entity(AssemblyType.class));
            query.setParameter("configuration",
                    getHwConfiguration(session),
                    Hibernate.entity(HwConfiguration.class));
            HwSchema hwSchema = (HwSchema) query.list().get(0);
            if (hwSchema != null) {
                logger.finer("Assembly data: " + assembly.getData());
                logger.finer("Schema data: " + hwSchema.getSchema());
                data = new AssemblyConfigXMLData(assembly.getData(),
                                                 hwSchema.getSchema());
            } else {
            	AcsJTmcdbNoSuchRowEx ex = new AcsJTmcdbNoSuchRowEx();
            	ex.setProperty("Details", "no Schema found for " + 
            			assembly.getSerialNumber());
                throw new AcsJTmcdbNoSuchRowEx();                            
            }
        } else {
            logger.severe("no assembly data found for serial number " + serialNumber);
            throw new AcsJTmcdbNoSuchRowEx();            
        }
        tx.commit();
        session.close();      
        return data;
    }
    
    @Override
    public AssemblyConfigXMLData getComponentConfigData(String componentName) {
    	// this function is no longer supported and will go away soon
    	return new AssemblyConfigXMLData("", "");
    }
    
    public String getConfigurationName() {
        return configurationName;
    }
    
    @Override
    public alma.TMCDB.XPDelay[] getCrossPolarizationDelays() throws AcsJTmcdbErrorEx,
            AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        HwConfiguration hwConfiguration = getHwConfiguration(session);
        List<alma.TMCDB.XPDelay> idlDelays = new ArrayList<alma.TMCDB.XPDelay>();
        for (XPDelay delay : hwConfiguration.getCrossPolarizationDelays()) {
            idlDelays.add(JavaToIdlTranslator.toIDL(delay));
        }
        tx.commit();
        session.close();        
        return idlDelays.toArray(new alma.TMCDB.XPDelay[0]);
    }
    
    @Override
    public AntennaDelays getCurrentAntennaDelays(String antennaName)
        throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna antenna = getAntennaFromConfiguration(getHwConfiguration(session),
                antennaName);
        Pad pad = antenna.getCurrentPad();
        AntennaDelays idlDelays = new AntennaDelays();
        List<alma.TMCDB.FEDelay> idlFeDelays = new ArrayList<alma.TMCDB.FEDelay>(); 
        List<alma.TMCDB.IFDelay> idlIfDelays = new ArrayList<alma.TMCDB.IFDelay>(); 
        List<alma.TMCDB.LODelay> idlLoDelays = new ArrayList<alma.TMCDB.LODelay>();
        for (FEDelay delay : antenna.getFrontEndDelays()) {
            idlFeDelays.add(JavaToIdlTranslator.toIDL(delay));
        }
        for (IFDelay delay : antenna.getIfDelays()) {
            idlIfDelays.add(JavaToIdlTranslator.toIDL(delay));
        }
        for (LODelay delay : antenna.getLoDelays()) {
            idlLoDelays.add(JavaToIdlTranslator.toIDL(delay));
        }
        idlDelays.antennaDelay = antenna.getAvgDelay();
        idlDelays.padDelay = pad.getAvgDelay();
        idlDelays.feDelays = idlFeDelays.toArray(new alma.TMCDB.FEDelay[0]);
        idlDelays.ifDelays = idlIfDelays.toArray(new alma.TMCDB.IFDelay[0]);
        idlDelays.loDelays = idlLoDelays.toArray(new alma.TMCDB.LODelay[0]);
        tx.commit();
        session.close();        
        return idlDelays;
    }
    
    @Override
    public alma.TMCDB.BandFocusModel getCurrentAntennaFocusModel(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna antenna = getAntennaFromConfiguration(getHwConfiguration(session),
                antennaName);
        
        FocusModel[] focusModels = antenna.getFocusModels().toArray(new FocusModel[0]);
        if (focusModels.length == 0) {
            throw new AcsJTmcdbNoSuchRowEx();
        }
        
        alma.TMCDB.BandFocusModel idlFocusModel =
            JavaToIdlTranslator.toIDL(focusModels[0]);
        
        tx.commit();
        session.close();
        return idlFocusModel;
    }

    @Override
    public PadIDL getCurrentAntennaPadInfo(String antennaName)
        throws AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna ant = getAntennaFromConfiguration(getHwConfiguration(session),
                                                  antennaName);
        Pad pad = ant.getCurrentPad();
        tx.commit();
        session.close();
        if (pad != null)
            return JavaToIdlTranslator.toIDL(pad);
        else
            throw new AcsJTmcdbNoSuchRowEx();
    }
    
    @Override
	public alma.TMCDB.BandPointingModel getCurrentAntennaPointingModel(String antennaName)
			throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
      Session session = HibernateUtil.getSessionFactory().openSession();
      Transaction tx = session.beginTransaction();
      Antenna antenna = getAntennaFromConfiguration(getHwConfiguration(session),
              antennaName);
      
      PointingModel[] pointingModels = antenna.getPointingModels().toArray(new PointingModel[0]);
      if (pointingModels.length == 0) {
          throw new AcsJTmcdbNoSuchRowEx();
      }
      
      alma.TMCDB.BandPointingModel idlPointingModel =
          JavaToIdlTranslator.toIDL(pointingModels[0]);
      
      tx.commit();
      session.close();
      return idlPointingModel;
	}

    @Override
    public double[] getMetrologyCoefficients(String antenna) {
        double[] coeffs = new double[2];
        coeffs[0] = 0.0;
        coeffs[1] = 0.0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try {
            Antenna ant = getAntennaFromConfiguration(getHwConfiguration(session),
                                                      antenna);
            Set<AntennaToPad> a2ps = ant.getScheduledPadLocations();
            for (AntennaToPad a2p : a2ps) {
            	if (a2p.getEndTime() == null) {
            		// The current pad should be the one with null
            		// end time.
                    coeffs[0] = a2p.getMountMetrologyAN0Coeff();
                    coeffs[1] = a2p.getMountMetrologyAW0Coeff();            		
            	}
            }
        } catch (AcsJTmcdbNoSuchRowEx ex) {
            ex.printStackTrace();
        }
        tx.commit();
        session.close();        
        return coeffs;
    }
    
    @Override
    public StartupAntennaIDL[] getStartupAntennasInfo() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        
        List<StartupAntennaIDL> startupAntennasIDL =
            new ArrayList<StartupAntennaIDL>();
        StartupScenario startup = getStartup(session, getHwConfiguration(session));
        if (startup != null) {
	        for (Iterator<BaseElementStartup> iter2 = 
	             startup.getBaseElementStartups().iterator();
	             iter2.hasNext(); ) {
	            BaseElementStartup bes = iter2.next();
	            BaseElement be = bes.getBaseElement();
	            logger.info(bes.getBaseElement().getName());
	            if (be.getType() == BaseElementType.Antenna) {
	                logger.info("It's an Antenna");
	                Antenna ant = (Antenna) be;
	                StartupAntennaIDL sa = new StartupAntennaIDL();
	                sa.antennaName = ant.getName();
	                sa.uiDisplayOrder = getAntennaUIOrder(ant.getName());
	                Set<AntennaToPad> a2ps = ant.getScheduledPadLocations();
	                for (Iterator<AntennaToPad> iter3 = a2ps.iterator();
	                     iter3.hasNext(); ) {
	                    AntennaToPad a2p = iter3.next();
	                    if (a2p.getEndTime() == null) {
	                        logger.info("Pad " + a2p.getPad().getName());
	                        sa.padName = a2p.getPad().getName();
	                    }
	                }
	                
	                Set<AssemblyStartup> assemblies = bes.getAssemblyStartups();
	                // HACK - HACK - HACK - Part I
	                // Currently is expecting the TMCDBComponent to set the FrontEnd
	                // as a subdevice of the Antenna in order to start the FrontEnd.
	                // This is why it is added here as part of the antennaAssemblies,
	                // even though it is not really an assembly
	                sa.antennaAssembly = new AssemblyLocationIDL[assemblies.size() + 1];
	                int i = 0;
	                for (Iterator<AssemblyStartup> iter6 = assemblies.iterator();
	                     iter6.hasNext(); ) {
	                    AssemblyStartup as = iter6.next();
	                    AssemblyLocationIDL al = new AssemblyLocationIDL();
	                    al.assemblyRoleName = as.getAssemblyRole().getName();
	                    al.assemblyTypeName = as.getAssemblyRole().getName();
	                    al.baseAddress = 0;
	                    al.channelNumber = 0;
	                    al.rca = 0;
	                    sa.antennaAssembly[i++] = al;
	                }	                
                    // HACK - HACK - HACK - Part II
	                // Adding a FrontEnd "assembly" in the last location
	                AssemblyLocationIDL dummyFEAssLoc = new AssemblyLocationIDL();
	                dummyFEAssLoc.assemblyRoleName = "FrontEnd";
	                dummyFEAssLoc.assemblyTypeName = "FrontEnd";
	                dummyFEAssLoc.baseAddress = 0;
	                dummyFEAssLoc.channelNumber = 0;
	                dummyFEAssLoc.rca = 0;
	                sa.antennaAssembly[assemblies.size()] = dummyFEAssLoc;
	
	                sa.frontEndName = "None";
	                sa.frontEndAssembly = new AssemblyLocationIDL[0];
	                // Not compatible with TMCDB gui. The TMCDB GUI is adding
	                // FrontEnd assemblies in the Antenna.
	                Set<BaseElementStartup> chbes = bes.getChildren();
	                for (Iterator<BaseElementStartup> iter4 = chbes.iterator();
	                     iter4.hasNext(); ) {
	                    BaseElementStartup febes = iter4.next();
	                    if (febes.getType() == BaseElementStartupType.FrontEnd) {
	                        logger.info("FrontEnd here");
	                        sa.frontEndName = "Generic";
	                        assemblies = febes.getAssemblyStartups();
	                        sa.frontEndAssembly = new AssemblyLocationIDL[assemblies.size()];
	                        i = 0;
	                        for (Iterator<AssemblyStartup> iter5 = assemblies.iterator(); 
	                             iter5.hasNext(); ) {
	                            AssemblyStartup as = iter5.next();
	                            AssemblyLocationIDL al = new AssemblyLocationIDL();
	                            logger.info("Role: " + as.getAssemblyRole().getName());
	                            al.assemblyRoleName = as.getAssemblyRole().getName();
	                            al.assemblyTypeName = as.getAssemblyRole().getName();
	                            // The following aren't really used. They should
	                            // be removed.
	                            al.baseAddress = 0;
	                            al.channelNumber = 0;
	                            al.rca = 0;
	                            sa.frontEndAssembly[i++] = al;
	                        }
	                    }
	                }                    
	                startupAntennasIDL.add(sa);
	            }
	        }
        }
                
        tx.commit();
        session.close();        
        return startupAntennasIDL.toArray(new StartupAntennaIDL[0]);
    }

    @Override
    public StartupAOSTimingIDL getStartupAOSTimingInfo() {
        logger.finest("Entering getStartupAOSTimingInfo");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        StartupScenario startup = getStartup(session, getHwConfiguration(session));
        StartupAOSTimingIDL sa = null;
        if (startup != null) {
	        logger.fine("BaseElementStartup length: " + startup.getBaseElementStartups().size());
	        for (Iterator<BaseElementStartup> iter2 = 
	             startup.getBaseElementStartups().iterator();
	             iter2.hasNext(); ) {
	            BaseElementStartup bes = iter2.next();
	            BaseElement be = bes.getBaseElement();
	            logger.finer("BaseElement: " + bes.getBaseElement().getName());
	            if (be.getType() == BaseElementType.AOSTiming) {
	                logger.info("It's the Master Clock");
	                AOSTiming lo = (AOSTiming) be;
	                sa = new StartupAOSTimingIDL();
	                
	                Set<AssemblyStartup> assemblies = bes.getAssemblyStartups();
	                sa.assemblies = new AssemblyLocationIDL[assemblies.size()];
	                int i = 0;
	                for (Iterator<AssemblyStartup> iter6 = assemblies.iterator();
	                     iter6.hasNext(); ) {
	                    AssemblyStartup as = iter6.next();
	                    AssemblyLocationIDL al = new AssemblyLocationIDL();
	                    logger.info("Role: " + as.getAssemblyRole().getName());
	                    al.assemblyRoleName = as.getAssemblyRole().getName();
	                    al.assemblyTypeName = as.getAssemblyRole().getName();
	                    al.baseAddress = 0;
	                    al.channelNumber = 0;
	                    al.rca = 0;
	                    sa.assemblies[i++] = al;
	                }
	            }
	        }
        }
        tx.commit();
        session.close();        
        return sa;        
    }
    
    @Override
    public StartupCLOIDL getStartupCLOInfo() {
        logger.info("entering getStartupCLOInfo");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        StartupScenario startup = getStartup(session, getHwConfiguration(session));
        StartupCLOIDL sa = null;
        if (startup != null) {
	        for (Iterator<BaseElementStartup> iter = 
	             startup.getBaseElementStartups().iterator();
	             iter.hasNext(); ) {
	            BaseElementStartup bes = iter.next();
	            BaseElement be = bes.getBaseElement();
	            logger.info("BaseElement: " + bes.getBaseElement().getName());
	            if (be.getType() == BaseElementType.CentralLO) {
	                CentralLO lo = (CentralLO) be;
	                
	                sa = new StartupCLOIDL();
	                
	                Set<AssemblyStartup> assemblies = bes.getAssemblyStartups();
	                sa.assemblies = new AssemblyLocationIDL[assemblies.size()];
	                int i = 0;
	                for (Iterator<AssemblyStartup> iter2 = assemblies.iterator();
	                     iter2.hasNext(); ) {
	                    AssemblyStartup as = iter2.next();
	                    AssemblyLocationIDL al = new AssemblyLocationIDL();
	                    al.assemblyRoleName = as.getAssemblyRole().getName();
	                    al.assemblyTypeName = as.getAssemblyRole().getName();
	                    al.baseAddress = 0;
	                    al.channelNumber = 0;
	                    al.rca = 0;
	                    sa.assemblies[i++] = al;
	                }
	                
	                sa.photonicReferences = new StartupPhotonicReferenceIDL[bes.getChildren().size()];
	                i = 0;
	                for (Iterator<BaseElementStartup> iter3 = bes.getChildren().iterator();
	                     iter3.hasNext(); ) {
	                    BaseElementStartup phbes = iter3.next();
	                    StartupPhotonicReferenceIDL sphidl = new StartupPhotonicReferenceIDL();
	                    sphidl.name = phbes.getType().toString();
	                    Set<AssemblyStartup> phassemblies = phbes.getAssemblyStartups();
	                    sphidl.assemblies = new AssemblyLocationIDL[phassemblies.size()];
	                    int j = 0;
	                    for (Iterator<AssemblyStartup> iter4 = phassemblies.iterator();
	                         iter4.hasNext(); ) {
	                        AssemblyStartup as = iter4.next();
	                        AssemblyLocationIDL al = new AssemblyLocationIDL();
	                        al.assemblyRoleName = as.getAssemblyRole().getName();
	                        al.assemblyTypeName = as.getAssemblyRole().getName();
	                        al.baseAddress = 0;
	                        al.channelNumber = 0;
	                        al.rca = 0;
	                        sphidl.assemblies[j++] = al;
	                    }
	                    sa.photonicReferences[i++] = sphidl;
	                }
	            }
	        }
        }
        tx.commit();
        session.close();        
        return sa;        
    }
    
    @Override
	public StartupWeatherStationControllerIDL getStartupWeatherStationControllerInfo()
			throws AcsJTmcdbErrorEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        StartupScenario startup = getStartup(session, getHwConfiguration(session));
        StartupWeatherStationControllerIDL idlWsCtrl = new StartupWeatherStationControllerIDL();
        idlWsCtrl.assemblies = new AssemblyLocationIDL[0];
        if (startup != null) {
            List<AssemblyLocationIDL> idlAssemblies = new ArrayList<AssemblyLocationIDL>();
            for (Iterator<BaseElementStartup> iter = 
                 startup.getBaseElementStartups().iterator();
                 iter.hasNext(); ) {
                BaseElementStartup bes = iter.next();
                BaseElement be = bes.getBaseElement();
                logger.info("BaseElement: " + bes.getBaseElement().getName());
                if (be.getType() == BaseElementType.WeatherStationController) {
                    WeatherStationController ws = (WeatherStationController) be;
                    for (AssemblyStartup as : bes.getAssemblyStartups()) {
                        AssemblyLocationIDL idlAssembly = new AssemblyLocationIDL();
                        idlAssembly.assemblyRoleName = as.getAssemblyRole().getName();
                        idlAssembly.assemblyTypeName = "none";
                        idlAssembly.baseAddress = 0;
                        idlAssembly.channelNumber = 0;
                        idlAssembly.rca = 0;
                        idlAssemblies.add(idlAssembly);
                    }
                }
            }
            idlWsCtrl.assemblies = idlAssemblies.toArray(new AssemblyLocationIDL[0]);
        }
        tx.commit();
        session.close();        
        return idlWsCtrl;        
	}

	@Override
    public String getTelescopeName() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        HwConfiguration hwConfiguration = getHwConfiguration(session);
        String telescName = hwConfiguration.getTelescopeName();
        tx.commit();
        session.close();        
        return telescName;
    }

	private Antenna getAntennaFromConfiguration(HwConfiguration configuration,
                                                String antennaName)
        throws AcsJTmcdbNoSuchRowEx {
        for (Iterator<BaseElement> iter =
            configuration.getBaseElements().iterator();
            iter.hasNext(); ) {
           BaseElement be = iter.next();
           if (be.getType().equals(BaseElementType.Antenna) &&
               be.getName().equals(antennaName)) {
               return (Antenna) be;
           }
        }        
        throw new AcsJTmcdbNoSuchRowEx();
    }
	
    private Pad getPadFromConfiguration(HwConfiguration configuration,
            String padName) throws AcsJTmcdbNoSuchRowEx {
        for (Iterator<BaseElement> iter = configuration.getBaseElements()
                .iterator(); iter.hasNext();) {
            BaseElement be = iter.next();
            if (be.getType().equals(BaseElementType.Pad)
                    && be.getName().equals(padName)) {
                return (Pad) be;
            }
        }
        throw new AcsJTmcdbNoSuchRowEx();
    }

	private short getAntennaUIOrder(String antennaName) {
    	short retVal;
        if (antennaName.substring(0, 2).equals("DV") || antennaName.substring(0, 2).equals("LA")) {
           retVal = (short) Integer.valueOf(antennaName.substring(2, 4)).intValue();
        } else if (antennaName.substring(0, 2).equals("DA")){
           retVal = (short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()-15);
        } else if (antennaName.substring(0, 2).equals("PM")) {
           retVal = (short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+62);
        } else { //CM case
           retVal = (short) (Integer.valueOf(antennaName.substring(2, 4)).intValue()+50);
        }
        return retVal;
    }

	protected Configuration getConfiguration(Session session) {
        String qstr = "FROM Configuration WHERE configurationName = '" +
                      configurationName + "'";
        Configuration configuration =
        	(Configuration) session.createQuery(qstr).uniqueResult();
    	return configuration;
    }

	protected HwConfiguration getHwConfiguration(Session session) {
        Configuration configuration = getConfiguration(session);
        String qstr = "FROM HwConfiguration WHERE swConfiguration = :conf";
        Query query = session.createQuery(qstr);
        query.setParameter("conf", configuration, Hibernate.entity(Configuration.class));
        HwConfiguration hwc = (HwConfiguration) query.uniqueResult();
        return hwc;
    }

	protected StartupScenario getStartup(Session session, HwConfiguration configuration) {
        String qstr = "FROM StartupScenario startup " +
                      "WHERE startup.name = :startup " +
                      "AND startup.configuration = :configuration";
        logger.info("Query stmt: " + qstr);
        logger.info("Query param startup: " + startupScenarioName);
        logger.info("Query param configuration: " + configurationName);
        Query query = session.createQuery(qstr);
        query.setParameter("startup", startupScenarioName, Hibernate.STRING);
        query.setParameter("configuration", configuration, Hibernate.entity(HwConfiguration.class));
        StartupScenario startupScenario = (StartupScenario) query.uniqueResult();
        return startupScenario;
    }

    protected void initDb() throws Exception {
        HibernateUtil.createConfigurationFromDbConfig(dbconf);        
    }

    @Override
    public int getAntennaLOOffsettingIndex(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna antenna = getAntennaFromConfiguration(getHwConfiguration(session),
                antennaName);
        int index = antenna.getLoOffsettingIndex();
        tx.commit();
        session.close();
        return index;
    }

    @Override
    public int getAntennaWalshFunctionSeqNumber(String antennaName)
            throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Antenna antenna = getAntennaFromConfiguration(getHwConfiguration(session),
                antennaName);
        int index = antenna.getWalshSeq();
        tx.commit();
        session.close();
        return index;
    }

    @Override
    public HolographyTowerRelativePadDirection[] getHolographyTowerRelativePadDirection(
            String padName) throws AcsJTmcdbErrorEx, AcsJTmcdbNoSuchRowEx {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        
        Pad pad = getPadFromConfiguration(getHwConfiguration(session), padName);
        String qstr = "FROM HolographyTowerToPad WHERE pad = :pad";
        Query query = session.createQuery(qstr);
        query.setParameter("pad", pad, Hibernate.entity(Pad.class));
        List<HolographyTowerToPad> ht2pads = query.list();
        List<HolographyTowerRelativePadDirection> idlht2pads =
            new ArrayList<HolographyTowerRelativePadDirection>();
        for (HolographyTowerToPad ht2pad : ht2pads) {
            HolographyTowerRelativePadDirection idlht2pad =
                new HolographyTowerRelativePadDirection();
            idlht2pad.holographyTowerName = ht2pad.getHolographyTower().getName();
            idlht2pad.azimuth = ht2pad.getAzimuth();
            idlht2pad.elevation = ht2pad.getElevation();
            idlht2pads.add(idlht2pad);
        }
        tx.commit();
        session.close();
        return idlht2pads.toArray(new HolographyTowerRelativePadDirection[0]);
    }
}
