/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File StartupAntenna.java
 */
package alma.tmcdb.access;

/**
 * The StartupAntenna class supplies the information needed to initialize
 * an antenna.  Included are the name of the antenna, the pad on which
 * it resides, the name of front end, the assembly locations in the 
 * front end, and the assembly locations in the antenna.
 */
public class StartupAntenna {
    static private final String newline = System.getProperty("line.separator");

	private String antennaName;
    private short uiDisplayOrder;
	private String padName;
	private String frontEndName;
	private AssemblyLocation[] frontEndAssembly;
	private AssemblyLocation[] antennaAssembly;
	
	public StartupAntenna () {
	}
	
	public StartupAntenna (
			String antennaName,
			String padName,
			String frontEndName,
			AssemblyLocation[] frontEndAssembly,
			AssemblyLocation[] antennaAssembly) {
		this.antennaName = antennaName;
		this.uiDisplayOrder = 1;
		this.padName = padName;
		this.frontEndName = frontEndName;
		this.frontEndAssembly = frontEndAssembly;
		this.antennaAssembly = antennaAssembly;
	}
	
	public StartupAntenna (
			String antennaName,
			short uiDisplayOrder,
			String padName,
			String frontEndName,
			AssemblyLocation[] frontEndAssembly,
			AssemblyLocation[] antennaAssembly) {
		this.antennaName = antennaName;
        this.uiDisplayOrder = uiDisplayOrder;
		this.padName = padName;
		this.frontEndName = frontEndName;
		this.frontEndAssembly = frontEndAssembly;
		this.antennaAssembly = antennaAssembly;
	}

	public String toString() {
    	String s =  "StartupAntenna:" + newline +
    		"\tantennaName: " + antennaName + newline +
            "\tuiDisplayOrder: " + uiDisplayOrder + newline +
    		"\tpadName: " + padName + newline +
    		"\tfrontEndName: " + frontEndName + newline;
    	for (int i = 0; i < frontEndAssembly.length; ++i)
    		s += "\tfrontEndAssembly[" + i + "]: " + frontEndAssembly[i].toString() + newline;
    	for (int i = 0; i < antennaAssembly.length; ++i)
    		s += "\tantennaAssembly[" + i + "]: " + antennaAssembly[i].toString() + newline;
		return s;
	}

	public AssemblyLocation[] getAntennaAssembly() {
		return antennaAssembly;
	}

	public void setAntennaAssembly(AssemblyLocation[] antennaAssembly) {
		this.antennaAssembly = antennaAssembly;
	}

	public String getAntennaName() {
		return antennaName;
	}

	public void setAntennaName(String antennaName) {
		this.antennaName = antennaName;
	}

    public short getUiDisplayOrder() {
		return uiDisplayOrder;
	}

	public void setUiDisplayOrder(short uiDisplayOrder) {
		this.uiDisplayOrder = uiDisplayOrder;
	}

	public AssemblyLocation[] getFrontEndAssembly() {
		return frontEndAssembly;
	}

	public void setFrontEndAssembly(AssemblyLocation[] frontEndAssembly) {
		this.frontEndAssembly = frontEndAssembly;
	}

	public String getFrontEndName() {
		return frontEndName;
	}

	public void setFrontEndName(String frontEndName) {
		this.frontEndName = frontEndName;
	}

	public String getPadName() {
		return padName;
	}

	public void setPadName(String padName) {
		this.padName = padName;
	}

}
