/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id: TmcdbBasebandDelay.cpp,v 1.7 2011/03/28 18:11:12 jperez Exp $"
 *
 */

#include <acsContainerServices.h>
#include <TmcdbBasebandDelay.h>
#include <TmcdbErrTypeC.h>
#include <TMCDBDataStructuresC.h>
#include <cmath>

TmcdbBasebandDelay::TmcdbBasebandDelay(maci::ContainerServices* containerServices) {
	getTmcdbAccessComponent(containerServices);
	TMCDB::StartupAntennaSeq_var antennas = m_tmcdb->getStartupAntennasInfo();
	std::vector<std::string> antennaNames;
	for (CORBA::ULong i = 0; i < antennas->length(); i++) {
		TMCDB_IDL::StartupAntennaIDL sa = antennas[i];
		antennaNames.push_back(static_cast<const char *>(sa.antennaName));
	}
	getCrossPolarizationDelays(m_tmcdb);
	getAntennaDelays(m_tmcdb, antennaNames);
    releaseTmcdbAccessComponent(containerServices);
}

TmcdbBasebandDelay::TmcdbBasebandDelay(maci::ContainerServices* containerServices, const Correlator::AntennaSeq_t& antennaSeq) {
	getTmcdbAccessComponent(containerServices);
    std::vector<std::string> antennaNames;
	for (CORBA::ULong i = 0; i < antennaSeq.length(); i++) {
		antennaNames.push_back((const char *) antennaSeq[i]);
	}
	getCrossPolarizationDelays(m_tmcdb);
	getAntennaDelays(m_tmcdb, antennaNames);
    releaseTmcdbAccessComponent(containerServices);
}

TmcdbBasebandDelay::TmcdbBasebandDelay(const TmcdbBasebandDelay& toCopy) {
	*this = toCopy;
}

TmcdbBasebandDelay::TmcdbBasebandDelay&
TmcdbBasebandDelay::operator =(const TmcdbBasebandDelay& rhs) {
	return *this;
}

TmcdbBasebandDelay::~TmcdbBasebandDelay() { }

const TmcdbBasebandDelay::antennaBBMap_t TmcdbBasebandDelay::getBasebandDelays(
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand,  bool use12GHzFilter) {
        antennaBBMap_t retVal;
        NetSidebandMod::NetSideband tmpSideBand = sideBand;
        BasebandNameMod::BasebandName bbName[] = { BasebandNameMod::BB_1, BasebandNameMod::BB_2,
			BasebandNameMod::BB_3, BasebandNameMod::BB_4 };
        if ( tmpSideBand == NetSidebandMod::DSB) {
            tmpSideBand = NetSidebandMod::USB;
        }
        // Define the IFProc connection state, given the 12GHz filter switch and the sideband
        TMCDB::IFProcConnectionState ifpSwitch;
        if (tmpSideBand == NetSidebandMod::USB) {
                if (use12GHzFilter) {
                        ifpSwitch = TMCDB::USB_HIGH;
                } else {
                        ifpSwitch = TMCDB::USB_LOW;
                }
        } else if (tmpSideBand == NetSidebandMod::LSB) {
                if (use12GHzFilter) {
                        ifpSwitch = TMCDB::LSB_HIGH;
                } else {
                        ifpSwitch = TMCDB::LSB_LOW;
		}
	}
	for (int bbIndex = 0; bbIndex < 4; bbIndex++) {
		BasebandNameMod::BasebandName baseband = bbName[bbIndex];
		// Accumulate the different baseband delay components
		double bbDelay = 0.0;
		double polzDelay = 0.0;
		// Add the cross polarization components, common to all antennas
		for (crossPolarizationDelays_t::iterator it = m_xpDelays.begin();
				it != m_xpDelays.end(); ++it) {
			// Filter the delays given the selection parameters
			if (((*it).baseband == baseband) &&
					((*it).receiverBand == band) &&
					((*it).sideband == tmpSideBand)) {
				polzDelay = (*it).delay;
			}
		}
		// Add the antenna specific components
                for (antennaDelays_t::iterator it = m_antennaDelays.begin();
                                it != m_antennaDelays.end(); ++it) {
                        TMCDB::AntennaDelays& delays = it->second;
                        bbDelay = polzDelay;
                        // Filter the FE Delays given the selection parameters
                        for (CORBA::ULong i = 0; i < delays.feDelays.length(); i++) {
                                if ((delays.feDelays[i].polarization == polz) &&
                                                (delays.feDelays[i].receiverBand == band) &&
                                                (delays.feDelays[i].sideband == tmpSideBand)) {
                                    bbDelay += delays.feDelays[i].delay;
                                }
                        }
			// Filter the IF Delays given the selection parameters
			for (CORBA::ULong i = 0; i < delays.ifDelays.length(); i++) {
				if ((delays.ifDelays[i].baseband == baseband) &&
						(delays.ifDelays[i].polarization == polz) &&
						(delays.ifDelays[i].ifSwitch == ifpSwitch)) {
					bbDelay += delays.ifDelays[i].delay;
				}
			}
			// Filter the LO Delays given the selection parameters
			for (CORBA::ULong i = 0; i < delays.loDelays.length(); i++) {
				if (delays.loDelays[i].baseband == baseband) {
					bbDelay += delays.loDelays[i].delay;
				}
			}
// 			std::ostringstream msg;
// 			msg << "getBasebandDelays Antenna: " << it->first << " bb: " << baseband << " polz: " << (int)polz 
// 			    << " band: " << (int)band  << " sideband: " << (int)sideBand << " ifpSwitch:" << ifpSwitch
// 			    <<" delay = " << bbDelay << endl;
// 			cout << msg.str();
//			ACS_SHORT_LOG((LM_INFO, msg.str().c_str()));
			antennaBB_t key;
			key.antennaID = it->first;
			key.baseBand = baseband;
			retVal.insert(std::make_pair(key, bbDelay));
		}
	}
	return retVal;
}

const TmcdbBasebandDelay::antennaBBCoarseMap_t TmcdbBasebandDelay::getCoarseBasebandDelays(
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter) {
        antennaBBCoarseMap_t retVal;
        antennaBBMap_t bbd = getBasebandDelays(polz, band, sideBand, use12GHzFilter);
    for (TmcdbBasebandDelay::antennaBBMap_t::iterator it = bbd.begin(); it != bbd.end(); ++it) {
        antennaBB_t key = it->first;
        double bbDelay = it->second;
        double coarseDelay = std::ceil(bbDelay / COARSE_DELAY_SIZE - 0.5);
        retVal.insert(std::make_pair(key, static_cast<unsigned long>(coarseDelay)));
//      std::ostringstream msg;
//      msg << "getCoarseBasebandDelays Antenna: " << key.antennaID << " bb: " << key.baseBand << " polz: " << (int)polz 
//          << " band: " << (int)band  << " sideBand: " << (int)sideBand << " use12GHzFilter: " << use12GHzFilter
//          <<" delay = " << static_cast<unsigned long>(coarseDelay) << endl;
//      cout << msg.str();
    }
    return retVal;
}

const TmcdbBasebandDelay::antennaBBResidualMap_t TmcdbBasebandDelay::getBasebandResidualDelays(
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter) {
        antennaBBResidualMap_t retVal;
        antennaBBMap_t bbd = getBasebandDelays(polz, band, sideBand, use12GHzFilter);
    for (TmcdbBasebandDelay::antennaBBMap_t::iterator it = bbd.begin(); it != bbd.end(); ++it) {
        antennaBB_t key = it->first;
        double bbDelay = it->second;
    	double coarseDelay = std::ceil(bbDelay / COARSE_DELAY_SIZE - 0.5) * COARSE_DELAY_SIZE;
    	double residualDelay = bbDelay - coarseDelay;
    	retVal.insert(std::make_pair(key, residualDelay));
    }
	return retVal;
}

TmcdbBasebandDelay::basebandDelay_t TmcdbBasebandDelay::getBasebandDelay(
		const std::string &antennaID,
		const BasebandNameMod::BasebandName &baseBand,
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand,
                bool use12GHzFilter) {
        antennaBBMap_t bbd = getBasebandDelays(polz, band, sideBand, use12GHzFilter);
        antennaBB_t key = {antennaID, baseBand};
        double delay = bbd[key];
        return delay;
}

TmcdbBasebandDelay::basebandCoarseDelay_t TmcdbBasebandDelay::getCoarseBasebandDelay(
		const std::string &antennaID,
		const BasebandNameMod::BasebandName &baseBand,
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand,
                bool use12GHzFilter) {
        antennaBBCoarseMap_t bbd = getCoarseBasebandDelays(polz, band, sideBand,use12GHzFilter );
        antennaBB_t key = {antennaID, baseBand};
        unsigned long samples = bbd[key];
        return samples;
}

TmcdbBasebandDelay::basebandResidualDelay_t TmcdbBasebandDelay::getResidualBasebandDelay(
		const std::string &antennaID,
		const BasebandNameMod::BasebandName &baseBand,
                const PolarizationTypeMod::PolarizationType &polz,
                const ReceiverBandMod::ReceiverBand &band,
                const NetSidebandMod::NetSideband &sideBand,
                bool use12GHzFilter) {
        antennaBBResidualMap_t bbd = getBasebandResidualDelays(polz, band, sideBand, use12GHzFilter);
        antennaBB_t key = {antennaID, baseBand};
        double delay = bbd[key];
        return delay;
}

void TmcdbBasebandDelay::getCrossPolarizationDelays(TMCDB::Access* tmcdb) {
	TMCDB::XPDelaySeq_var xpDelays = m_tmcdb->getCrossPolarizationDelays();
	for (CORBA::ULong i = 0; i < xpDelays->length(); i++) {
		TMCDB::XPDelay xpDelay = xpDelays[i];
		m_xpDelays.push_back(xpDelay);
	}
}

void TmcdbBasebandDelay::getAntennaDelays(TMCDB::Access* tmcdb, std::vector<std::string> antennas) {
	for (std::vector<std::string>::iterator it = antennas.begin(); it != antennas.end(); ++it) {
    	TMCDB::AntennaDelays_var antDelays = m_tmcdb->getCurrentAntennaDelays((*it).c_str());
    	TMCDB::AntennaDelays delay = *antDelays;
    	m_antennaDelays.insert(std::make_pair(*it, delay));
	}
}

void TmcdbBasebandDelay::getTmcdbAccessComponent(maci::ContainerServices* containerServices) {
    m_tmcdb = containerServices->getDefaultComponent<TMCDB::Access>("IDL:alma/TMCDB/Access:1.0");
}

// throws maciErrType::CannotReleaseComponentEx
void TmcdbBasebandDelay::releaseTmcdbAccessComponent(maci::ContainerServices* containerServices) {
  	CORBA::String_var tmcdbComponentName = m_tmcdb->name();
   	containerServices->releaseComponent(tmcdbComponentName.in());
}
