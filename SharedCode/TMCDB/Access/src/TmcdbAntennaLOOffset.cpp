/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id: TmcdbAntennaLOOffset.cpp,v 1.2 2011/04/01 00:38:39 jperez Exp $"
 *
 */

#include <acsContainerServices.h>
#include <TmcdbAntennaLOOffset.h>
#include <TmcdbErrTypeC.h>
#include <TMCDBDataStructuresC.h>
#include <cmath>

TmcdbAntennaLOOffset::TmcdbAntennaLOOffset(maci::ContainerServices* containerServices) 
{
    // TODO: what follows will be replaced with real code
    std::ostringstream allAntsStr;
    allAntsStr << "Map of LOOffset indices: " << endl;    
    for ( int cai = 0; cai < 32; cai++ )
    {
	std::ostringstream antStr;
	antStr << setfill('0');
	antStr << "DV" << setw(2)  << cai + 1;
	m_antennaIDs.push_back(antStr.str());
	m_loOffsetIndexMap[antStr.str()] = 2 * cai;
	allAntsStr << antStr.str() << " LO Offset Multiplier: " << m_loOffsetIndexMap[antStr.str()] << endl;
	antStr.str("");
	antStr.clear();
	antStr << "DA" << setw(2)  << cai + 41;
	m_loOffsetIndexMap[antStr.str()] = 2 * cai + 1;
	allAntsStr << antStr.str() << " LO Offset Multiplier: " << m_loOffsetIndexMap[antStr.str()] << endl;
	if (cai < 4)
	{
	    antStr.str("");
	    antStr.clear();
	    antStr << "PM" << setw(2)  << cai + 1;
	    m_antennaIDs.push_back(antStr.str());
	    m_loOffsetIndexMap[antStr.str()] = cai;
	    allAntsStr << antStr.str() << " LO Offset Multiplier: " << m_loOffsetIndexMap[antStr.str()] << endl;
	}
	if (cai < 12)
	{
	    antStr.str("");
	    antStr.clear();
	    antStr << "CM" << setw(2)  << cai + 5;
	    m_loOffsetIndexMap[antStr.str()] = cai + 4;
	    allAntsStr << antStr.str() << " LO Offset Multiplier:" << m_loOffsetIndexMap[antStr.str()] << endl;
	}
    }
    // Just temporary debug logging
    cout << allAntsStr.str();
}

TmcdbAntennaLOOffset::TmcdbAntennaLOOffset(maci::ContainerServices* containerServices, 
					   const Correlator::AntennaSeq_t& antennaSeq) 
{
    // TODO: what follows will be replaced with real code
    int numAntennas = antennaSeq.length();
    std::ostringstream antsStr;
    for (int i = 0; i < numAntennas; i++)
    {
	std::string antStr = antennaSeq[i].in();
	m_antennaIDs.push_back(antStr);
	m_loOffsetIndexMap[antStr] = i;
	antsStr << antStr << " LO Offset Multiplier:" << m_loOffsetIndexMap[antStr] << endl;
    }
    // Just temporary debug logging
    cout << antsStr.str();
}

TmcdbAntennaLOOffset::TmcdbAntennaLOOffset(const TmcdbAntennaLOOffset& toCopy) 
{
    *this = toCopy;
}

TmcdbAntennaLOOffset::TmcdbAntennaLOOffset&
TmcdbAntennaLOOffset::operator =(const TmcdbAntennaLOOffset& rhs) 
{
    return *this;
}

TmcdbAntennaLOOffset::~TmcdbAntennaLOOffset() { }

const TmcdbAntennaLOOffset::loOffsetIndexMap_t TmcdbAntennaLOOffset::getLOOffsets()
{
    return m_loOffsetIndexMap;
}

const TmcdbAntennaLOOffset::loOffsetIndexMap_t TmcdbAntennaLOOffset::getLOOffsets(antennaIDs_t &antIDs)
{
    // TODO: what follows will be replaced with real code
    int numAntennas = antIDs.size();
    loOffsetIndexMap_t tmpMap;
    for (int i = 0; i < numAntennas; i++)
    {
	if (m_loOffsetIndexMap.count(antIDs[i]) != 0)
	{
	    tmpMap[antIDs[i]] = i;
	}
    }
    return tmpMap;	    
}

void TmcdbAntennaLOOffset::getLOOffsetsFromTMCDB(TMCDB::Access* tmcdb, antennaIDs_t &antennas)
{
    // TODO: what follows will be replaced with real code getting LO Offset indices from TMCDB
}

void TmcdbAntennaLOOffset::getTmcdbAccessComponent(maci::ContainerServices* containerServices) 
{
    m_tmcdb = containerServices->getDefaultComponent<TMCDB::Access>("IDL:alma/TMCDB/Access:1.0");
}

// throws maciErrType::CannotReleaseComponentEx
void TmcdbAntennaLOOffset::releaseTmcdbAccessComponent(maci::ContainerServices* containerServices) 
{
    CORBA::String_var tmcdbComponentName = m_tmcdb->name();
    containerServices->releaseComponent(tmcdbComponentName.in());
}
