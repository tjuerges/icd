printf "###############################################\n"
./scripts/testEnv start

# Run the tests
./runTmcdbComponentTest.sh; RETURN=$?
# ./runOtherTest; let "RETURN&=$?"

# Final result
if [ "$RETURN" = "0" ]; then
    printf "PASSED\n"
else
    printf "FAILED\n"
fi

printf "###############################################\n"
./scripts/testEnv stop

exit "$RETURN"
