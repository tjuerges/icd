/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#include <iostream>
#include <acsutil.h>
#include <maciSimpleClient.h>
#include <logging.h>
#include <TmcdbBasebandDelay.h>

using namespace maci;

void testTmcdbBBDelaysWithAllAntennas(ContainerServices* container) {
    TmcdbBasebandDelay tmcdbBBDelays(container);

    std::cout << "----------------------- ALL ANTENNAS ------------------------" << std::endl;
    std::cout << "----------------------- Baseband Delays ---------------------" << std::endl;
    TmcdbBasebandDelay::antennaBBMap_t map = tmcdbBBDelays.getBasebandDelays(PolarizationTypeMod::X,
                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    for (TmcdbBasebandDelay::antennaBBMap_t::iterator it = map.begin(); it != map.end(); ++it) {
        std::cout << it->first.antennaID << " " << it->first.baseBand << " "
                                << it->second << std::endl;
    }
    std::cout << "----------------------- Coarse Delays -----------------------" << std::endl;
        TmcdbBasebandDelay::antennaBBCoarseMap_t coarseDelays =
                        tmcdbBBDelays.getCoarseBasebandDelays(PolarizationTypeMod::X,
                                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
        for (TmcdbBasebandDelay::antennaBBCoarseMap_t::iterator it = coarseDelays.begin(); it
                        != coarseDelays.end(); ++it) {
                std::cout << it->first.antennaID << " " << it->first.baseBand << " "
				<< it->second << " (" << (it->second * 250E-12)<< ")" << std::endl;
	}
    std::cout << "----------------------- Residual Delays ---------------------" << std::endl;
        TmcdbBasebandDelay::antennaBBResidualMap_t residualDelays =
                        tmcdbBBDelays.getBasebandResidualDelays(PolarizationTypeMod::X,
                                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
        for (TmcdbBasebandDelay::antennaBBResidualMap_t::iterator it = residualDelays.begin(); it
                        != residualDelays.end(); ++it) {
                std::cout << it->first.antennaID << " " << it->first.baseBand << " "
				<< it->second << std::endl;
	}
    std::cout << "---- TmcdbBasebandDelay::getBasebandDelay -------------------" << std::endl;
    double delay;
    delay = tmcdbBBDelays.getBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
    std::cout << "---- TmcdbBasebandDelay::getCoarseBasebandDelay --------------" << std::endl;
    delay = tmcdbBBDelays.getCoarseBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
    std::cout << "---- TmcdbBasebandDelay::getResidualBasebandDelay ------------" << std::endl;
    delay = tmcdbBBDelays.getResidualBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
}

void testTmcdbBBDelaysWithFewAntennas(ContainerServices* container) {
    Correlator::AntennaSeq_t antennaSeq;
    antennaSeq.length(1);
    antennaSeq[0] = CORBA::string_dup("DV01");

    TmcdbBasebandDelay tmcdbBBDelays(container, antennaSeq);

    std::cout << "----------------------- ONLY DV01 ---------------------------" << std::endl;
    std::cout << "----------------------- Baseband Delays ---------------------" << std::endl;
    TmcdbBasebandDelay::antennaBBMap_t map = tmcdbBBDelays.getBasebandDelays(PolarizationTypeMod::X,
                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    for (TmcdbBasebandDelay::antennaBBMap_t::iterator it = map.begin(); it != map.end(); ++it) {
        std::cout << it->first.antennaID << " " << it->first.baseBand << " "
                                << it->second << std::endl;
    }
    std::cout << "----------------------- Coarse Delays -----------------------" << std::endl;
        TmcdbBasebandDelay::antennaBBCoarseMap_t coarseDelays =
                        tmcdbBBDelays.getCoarseBasebandDelays(PolarizationTypeMod::X,
                                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
        for (TmcdbBasebandDelay::antennaBBCoarseMap_t::iterator it = coarseDelays.begin(); it
                        != coarseDelays.end(); ++it) {
                std::cout << it->first.antennaID << " " << it->first.baseBand << " "
				<< it->second << " (" << (it->second * 250E-12)<< ")" << std::endl;
	}
    std::cout << "----------------------- Residual Delays ---------------------" << std::endl;
        TmcdbBasebandDelay::antennaBBResidualMap_t residualDelays =
                        tmcdbBBDelays.getBasebandResidualDelays(PolarizationTypeMod::X,
                                        ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
        for (TmcdbBasebandDelay::antennaBBResidualMap_t::iterator it = residualDelays.begin(); it
                        != residualDelays.end(); ++it) {
                std::cout << it->first.antennaID << " " << it->first.baseBand << " "
				<< it->second << std::endl;
	}
    std::cout << "---- TmcdbBasebandDelay::getBasebandDelay -------------------" << std::endl;
    double delay;
    delay = tmcdbBBDelays.getBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
    std::cout << "---- TmcdbBasebandDelay::getCoarseBasebandDelay --------------" << std::endl;
    delay = tmcdbBBDelays.getCoarseBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
    std::cout << "---- TmcdbBasebandDelay::getResidualBasebandDelay ------------" << std::endl;
    delay = tmcdbBBDelays.getResidualBasebandDelay("DV01", BasebandNameMod::BB_1, PolarizationTypeMod::X,
                ReceiverBandMod::ALMA_RB_01, NetSidebandMod::LSB, false);
    std::cout << delay << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "Hello world" << std::endl;

    SimpleClient client;
    if (client.init(argc, argv) == 0) {
    	ACS_SHORT_LOG((LM_ERROR, "Cannot init client"));
    	return -1;
    }
    client.login();
    testTmcdbBBDelaysWithAllAntennas(client.getContainerServices());
    testTmcdbBBDelaysWithFewAntennas(client.getContainerServices());
    client.logout();
    return 0;
}

