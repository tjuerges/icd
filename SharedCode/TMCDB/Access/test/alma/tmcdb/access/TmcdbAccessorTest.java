package alma.tmcdb.access;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;

public class TmcdbAccessorTest extends TestCase {

    private static Logger logger =
        LoggerFactory.getLogger(TmcdbAccessorTest.class);
    TmcdbHibernateAccessor tmcdb;
    
    public TmcdbAccessorTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        tmcdb = new TmcdbHibernateAccessor();
    }

    protected void tearDown() throws Exception {
        tmcdb.clear();
        super.tearDown();
    }

    public void notestGetStartupAntennasInfo() throws Exception {
    	StartupAntennaIDL[] sai = tmcdb.getStartupAntennasInfo();
    	logger.info("startup antenna length: " + sai.length);
    	for (StartupAntennaIDL sa : sai) {
    		logger.info("antennaName: " + sa.antennaName);
    		logger.info("frontEndName: " + sa.frontEndName);
    		logger.info("padName: " + sa.padName);
    		logger.info("uiDisplayOrder: " + sa.uiDisplayOrder);
    		for (AssemblyLocationIDL aa : sa.antennaAssembly) {
    			logger.info("\tassemblyRoleName: " + aa.assemblyRoleName);
    			logger.info("\tassemblyTypeName: " + aa.assemblyTypeName);
    			logger.info("\tbaseAddress: " + aa.baseAddress);
    			logger.info("\tchannelNumber: " + aa.channelNumber);
    			logger.info("\trca: " + aa.rca);
    		}
    		for (AssemblyLocationIDL aa : sa.frontEndAssembly) {
    			logger.info("\tassemblyRoleName: " + aa.assemblyRoleName);
    			logger.info("\tassemblyTypeName: " + aa.assemblyTypeName);
    			logger.info("\tbaseAddress: " + aa.baseAddress);
    			logger.info("\tchannelNumber: " + aa.channelNumber);
    			logger.info("\trca: " + aa.rca);
    		}
    	}
    }
    
    public void notestgetStartupCLOInfo() throws Exception {
    	StartupCLOIDL sci = tmcdb.getStartupCLOInfo();
    }

    public void notestgetStartupAOSTimingInfo() throws Exception {
    	StartupAOSTimingIDL sci = tmcdb.getStartupAOSTimingInfo();
    }

    public void testLala() throws Exception {
    	tmcdb.getCurrentAntennaDelays("DV01");
    }
}
