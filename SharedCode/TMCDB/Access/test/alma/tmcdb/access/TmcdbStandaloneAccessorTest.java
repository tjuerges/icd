package alma.tmcdb.access;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import alma.TMCDB.AssemblyConfigXMLData;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;

public class TmcdbStandaloneAccessorTest extends TestCase {

    private static Logger logger =
        LoggerFactory.getLogger(TmcdbStandaloneAccessorTest.class);
    TmcdbStandaloneHibernateAccessor tmcdb;
    
    public TmcdbStandaloneAccessorTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        tmcdb = new TmcdbStandaloneHibernateAccessor();
    }

    protected void tearDown() throws Exception {
        tmcdb.clear();
        super.tearDown();
    }

    public void testGetAntennaStartupIDL() throws Exception {
        StartupAntennaIDL[] sas = tmcdb.getStartupAntennasInfo();
        assertEquals(2, sas.length);
        List<String> roles = new ArrayList<String>();
        for (int i=0; i<sas.length; i++) {
            if (sas[i].antennaName.equals("DV01")) {
                logger.debug("Checking DV01");
                roles.clear();
                roles.add("Mount");
                roles.add("LO2BBpr0");
                checkAssemblies(roles, sas[i].antennaAssembly);
            } else if (sas[i].antennaName.equals("DA41")) {
                logger.debug("Checking DA41");
                roles.clear();
                roles.add("Mount");
                roles.add("LO2BBpr1");
                checkAssemblies(roles, sas[i].antennaAssembly);
                logger.debug("Checking DA41 FrontEnd");
                roles.clear();
                roles.add("LPR");
                checkAssemblies(roles, sas[i].frontEndAssembly);
            } else {
                fail("Unrecognized antenna: " + sas[i].antennaName);
            }
        }
    }

    public void testGetAOSTimingStartupIDL() throws Exception {
        StartupAOSTimingIDL aosidl = tmcdb.getStartupAOSTimingInfo();
        List<String> roles = new ArrayList<String>();
        roles.add("CRD");
        checkAssemblies(roles, aosidl.assemblies);
    }
    
    public void testGetAntennaIDL() throws Exception {
        AntennaIDL antidl = tmcdb.getAntennaInfo("DV01");
        assertEquals("DV01", antidl.AntennaName);
        assertEquals(1.0, antidl.XPosition.value);
        assertEquals(2.0, antidl.YPosition.value);
        assertEquals(3.0, antidl.ZPosition.value);
        assertEquals(4.0, antidl.XOffset.value);
        assertEquals(5.0, antidl.YOffset.value);
        assertEquals(6.0, antidl.ZOffset.value);
    }

    public void testGetCurrentPadIDL() throws Exception {
        PadIDL padidl = tmcdb.getCurrentAntennaPadInfo("DV01");
        assertEquals("Pad01", padidl.PadName);
        assertEquals(1.0, padidl.XPosition.value);
        assertEquals(2.0, padidl.YPosition.value);
        assertEquals(3.0, padidl.ZPosition.value);
    }

    public void testGetAssemblyConfigData() throws Exception {
        AssemblyConfigXMLData data =
            tmcdb.getAssemblyConfigData("0x00000000000109c8");
        assertNotNull(data);
        logger.info("XML: " + data.xmlDoc);
        logger.info("XSD: " + data.schema);
    }

    public void testMoreThanOneCall() throws Exception {
        tmcdb.getStartupAntennasInfo();
        tmcdb.getAntennaInfo("DV01");
    }
    
//    public void testGetPointingModel() throws Exception {
//        tmcdb.getPointingModelInfo("DV01");
//    }
    
    private void checkAssemblies(List<String> expAssemblies,
                                 AssemblyLocationIDL[] assemblies) {
        for (int i=0; i<assemblies.length; i++) {
            logger.debug("assemblyRoleName = " +
                         assemblies[i].assemblyRoleName);
            expAssemblies.remove(assemblies[i].assemblyRoleName);
        }
        assertEquals(0, expAssemblies.size());
    }
}
