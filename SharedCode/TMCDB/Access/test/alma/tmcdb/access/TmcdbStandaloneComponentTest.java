/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.access;

import java.util.logging.Logger;

import alma.TMCDB.Access;
import alma.TMCDB.AccessHelper;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

/**
 * Test the TmcdbStandaloneComponent component.
 * 
 * @author rhiriart
 *
 */
public class TmcdbStandaloneComponentTest extends ComponentClientTestCase {
	
    public static final String TMCDB_COMPONENT_NAME = "TMCDB";
    
    /** ACS ContainerServices */
    private ContainerServices container;
    
    /** The ubiquitous logger */
    private Logger logger;
    
    /** The TMCDB component under test here */
    private Access tmcdb;
    
    /**
     * Constructor
     * 
     * @param name Test name.
     * @throws Exception
     */
	public TmcdbStandaloneComponentTest(String name) throws Exception {
		super(name);
	}

    /**
     * Test fixture setup. This method is called before each one of the test
     * methods.
     */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
        container = getContainerServices();
        logger = container.getLogger();
        tmcdb = AccessHelper.narrow(container.getComponent(TMCDB_COMPONENT_NAME));
	}

    /**
     * Test fixture cleanup. This method is called after each one of the test
     * methods.
     */
	@Override
	protected void tearDown() throws Exception {
	    container.releaseComponent(TMCDB_COMPONENT_NAME);
		super.tearDown();
	}

    public void testLoadComponent() {}
        
}