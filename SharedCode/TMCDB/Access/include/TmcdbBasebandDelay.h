#ifndef TMCDB_BASEBAND_DELAY_H
#define TMCDB_BASEBAND_DELAY_H
// @(#) $Id: TmcdbBasebandDelay.h,v 1.4 2011/03/28 18:10:59 jperez Exp $
//
// Copyright (C) 2010
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-workers@nrao.edu

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <TMCDBAccessIFC.h>
#include "almaEnumerations_IFC.h"
#include "CorrelatorArrayC.h"

class TmcdbBasebandDelay {
public:
        /// Coarse delay value = 250 psec.
        static const double COARSE_DELAY_SIZE = 250e-12;

        /// Baseband delay in seconds
        typedef double basebandDelay_t;

	/// Baseband delay in seconds
	typedef double basebandResidualDelay_t;

	/// Baseband delay samples. Eeach sample is 250 picoseconds.
	typedef unsigned long basebandCoarseDelay_t;

	/** Structure encapsulating an antenna/baseband pair
	 */
	struct antennaBB_t {
		std::string antennaID;
		BasebandNameMod::BasebandName baseBand;
	};

	/// Used to sort delay keys based on antenna name, baseband , and polarization
	struct lt_antBBKey {
		bool operator()(const antennaBB_t &antennaBB1,
				const antennaBB_t &antennaBB2) const {
			if (antennaBB1.antennaID < antennaBB2.antennaID) {
				return true;
			} else if (antennaBB1.antennaID == antennaBB2.antennaID) {
				if (antennaBB1.baseBand < antennaBB2.baseBand) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	};
	/** Vector of antenna/baseband pairs
	 */
	typedef std::vector<antennaBB_t> antennaBBVec_t;

	/** Map of baseband delay values keyed by antenna/baseband pair
	 */
	typedef std::map<antennaBB_t, basebandDelay_t, lt_antBBKey> antennaBBMap_t;

	/** Map of baseband delay values keyed by antenna/baseband pair
	 */
	typedef std::map<antennaBB_t, basebandCoarseDelay_t, lt_antBBKey>
			antennaBBCoarseMap_t;

	/** Map of baseband delay values keyed by antenna/baseband pair
	 */
	typedef std::map<antennaBB_t, basebandResidualDelay_t, lt_antBBKey>
			antennaBBResidualMap_t;

	typedef std::map<std::string, TMCDB::AntennaDelays> antennaDelays_t;
	typedef std::vector<TMCDB::XPDelay> crossPolarizationDelays_t;

	/** Default constructor accesses the TMCDB and retrieves all baseband delay values for
	 ** all antennas, basebands, receiver bands, polarizations, sidebands and IF switch positions.
	 ** @param container ACS Container Services
	 ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
	 ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
	 ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
	 ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
 	 */
	TmcdbBasebandDelay(maci::ContainerServices* containerServices);

	/** Parametrized constructor accesses the TMCDB and retrieves all baseband delay values for
	 ** the specified antennas.
	 ** @param container ACS Container Services
	 ** @param antennaSeq Sequence of antennas
	 ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
	 ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
	 ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
	 ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
	 */
	TmcdbBasebandDelay(maci::ContainerServices* containerServices, const Correlator::AntennaSeq_t &antennaSeq);

	/// Copy constructor
	TmcdbBasebandDelay(const TmcdbBasebandDelay &toCopy);

	/// Assignment operator
	TmcdbBasebandDelay &operator =(const TmcdbBasebandDelay &rhs);

	/// Destructor
	~TmcdbBasebandDelay();

	/** Returns a map of baseband delays specified by the polarization, band,
	 ** sideband, and switch parameters.  The map is keyed by antenna/baseband pair
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return map of baseband delays, if empty the parameters passed did not correspond
         **         to any baseband in any of the antennas in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
        const antennaBBMap_t getBasebandDelays(
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

        /** Returns a map of baseband delays specified by the polarization, band,
         ** sideband, and switch parameters.  The map is keyed by antenna/baseband pair
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return map of baseband delays, if empty the parameters passed did not correspond
         **         to any baseband in any of the antennas in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
        const antennaBBCoarseMap_t getCoarseBasebandDelays(
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

        /** Returns a map of baseband delays specified by the polarization, band,
         ** sideband, and switch parameters.  The map is keyed by antenna/baseband pair
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return map of baseband delays, if empty the parameters passed did not correspond
         **         to any baseband in any of the antennas in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
        const antennaBBResidualMap_t getBasebandResidualDelays(
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

        /** Returns the baseband delay specified by the antenna, baseband, polarization, band,
         ** sideband, and switch parameters.
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return single baseband delay as specified by the passed-in parameters, if empty the parameters
         **         passed in do not correspond to any baseband delays in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
	basebandDelay_t getBasebandDelay(const std::string &antennaID,
                        const BasebandNameMod::BasebandName &baseBand,
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

        /** Returns the coarse portion of baseband delay specified by the antenna, baseband, polarization, band,
         ** sideband, and switch parameters.  This portion is the number of 250 psec in the baseband delay.
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return the coarse portion of a single baseband delay as specified by the passed-in parameters,
         **         if empty the parameters passed in do not correspond to any baseband delays in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
	basebandCoarseDelay_t getCoarseBasebandDelay(const std::string &antennaID,
                        const BasebandNameMod::BasebandName &baseBand,
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

        /** Returns the residual portion of the baseband delay specified by the antenna, baseband, polarization, band,
         ** sideband, and switch parameters.
	 ** @param antennaID antenna label
	 ** @param baseBand baseband enumerator
         ** @param polz polarization enumerator (only X or Y allowed)
         ** @param band receiver band enumerator
         ** @param sideBand sideband enumerator (only USB or LSB allowed)
         ** @param use12GHzFilter If true then the 2 by 2 matrix switch, in the IF processors for both 
         **                       polarizations, will be set to pass the signal through the 12GHz 
         **                       low-pass filter
         ** @return residual portion of a single baseband delay as specified by the passed-in parameters, if empty the parameters
         **         passed in do not correspond to any baseband delays in the current object.
         ** @exception CorrErr::InvalidParameter One of the paremeters passed is out of range.  Data should be
	 **                             added to this exception specifying shich value is out of range
	 **                             and what the range should be.
	 */
	basebandResidualDelay_t getResidualBasebandDelay(
			const std::string &antennaID,
                        const BasebandNameMod::BasebandName &baseBand,
                        const PolarizationTypeMod::PolarizationType &polz,
                        const ReceiverBandMod::ReceiverBand &band,
                        const NetSidebandMod::NetSideband &sideBand, bool use12GHzFilter);

private:

	/**
	 * Gets a reference for the TMCDB Access component.
	 * @param container ACS Container Services
	 * @exception maciErrType::CannotGetComponentEx
	 */
	void getTmcdbAccessComponent(maci::ContainerServices* container);

	/**
	 * Release reference to the TMCDB Access component.
	 * @param container ACS Container Services
	 * @exception maciErrType::CannotReleaseComponentEx
	 */
	void releaseTmcdbAccessComponent(maci::ContainerServices* container);

	/**
	 * Gets cross polarization delays from the TMCDB. It fills the
	 * m_xpDelays vector with the retrieved data.
	 * @param tmcdb TMCDB Access component
	 ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
	 ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
	 */
	void getCrossPolarizationDelays(TMCDB::Access* tmcdb);

	/**
	 * Gets the antenna delays from the TMCDB. It fills the m_antennaDelays
	 * map with the data.
	 * @param tmcdb TMCDB Access component
	 * @param antennas Antennas for which the delays should be retrieved.
	 ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
	 ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
	 */
	void getAntennaDelays(TMCDB::Access* tmcdb, std::vector<std::string> antennas);

	TMCDB::Access* m_tmcdb;
	antennaDelays_t m_antennaDelays;
	crossPolarizationDelays_t m_xpDelays;
};

#endif   // TMCDB_BASEBAND_DELAY_H
