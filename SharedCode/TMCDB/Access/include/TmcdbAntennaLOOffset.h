#ifndef TMCDB_ANTENNA_LO_OFFSET_H
#define TMCDB_ANTENNA_LO_OFFSET_H
// @(#) $Id: TmcdbAntennaLOOffset.h,v 1.2 2011/04/01 00:39:21 jperez Exp $
//
// Copyright (C) 2010
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-workers@nrao.edu

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <TMCDBAccessIFC.h>
#include "almaEnumerations_IFC.h"
#include "CorrelatorArrayC.h"

class TmcdbAntennaLOOffset {
public:
    
    /** LO offset index (multiplier) type
     */
    typedef int loOffsetIndex_t;
    /** antenna name
     */
    typedef std::string antennaID_t;
    /** Vector of antenna names
     */
    typedef std::vector<std::string> antennaIDs_t;
    
    /** Map of LO offset index (multiplier) values keyed by antenna name
     */
    typedef std::map< antennaID_t, loOffsetIndex_t > loOffsetIndexMap_t;

    /** Default constructor accesses the TMCDB and retrieves all LO offset index values for
     ** all antennas.
     ** @param container ACS Container Services
     ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
     */
    TmcdbAntennaLOOffset(maci::ContainerServices* containerServices);

    /** Parametrized constructor accesses the TMCDB and retrieves all LO offset index values for
     ** the specified antennas.
     ** @param container ACS Container Services
     ** @param antennaSeq Sequence of antennas
     ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
     */
    TmcdbAntennaLOOffset (maci::ContainerServices* containerServices, const Correlator::AntennaSeq_t &antennaSeq);

    /// Copy constructor
    TmcdbAntennaLOOffset(const TmcdbAntennaLOOffset &toCopy);

    /// Assignment operator
    TmcdbAntennaLOOffset &operator =(const TmcdbAntennaLOOffset &rhs);

    /// Destructor
    ~TmcdbAntennaLOOffset();

    /** Returns a map of LO offset indices.  The map is keyed by antenna names
     ** @return map of LO offsets, if empty there are no LO offset index values 
     ** in TMCDB
     */
    const loOffsetIndexMap_t getLOOffsets();

    /** Returns a map of LO offsets.  The map is keyed by antenna names
     ** @param antIDs vector of antenna labels
     ** @return map of LO offsets, if empty the antenna labels passed did not correspond
     **         to any LO offset indices in the TMCDB.
     ** @exception CorrErr::InvalidParameter One of the antenna names passed is invalid.  Data should be
     **                             added to this exception specifying shich antenna is invalid
     */
    const loOffsetIndexMap_t getLOOffsets(antennaIDs_t &antIDs);

private:

    /**
     * Gets a reference for the TMCDB Access component.
     * @param container ACS Container Services
     * @exception maciErrType::CannotGetComponentEx
     */
    void getTmcdbAccessComponent(maci::ContainerServices* container);

    /**
     * Release reference to the TMCDB Access component.
     * @param container ACS Container Services
     * @exception maciErrType::CannotReleaseComponentEx
     */
    void releaseTmcdbAccessComponent(maci::ContainerServices* container);

    /**
     * Gets the antenna LO offset indices from the TMCDB. It fills the m_loOffsetIndexMap
     * map with the data.
     * @param tmcdb TMCDB Access component
     * @param antennas Antennas for which the LO offset indices should be retrieved.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     */
    void getLOOffsetsFromTMCDB(TMCDB::Access* tmcdb, antennaIDs_t &antennas);

    TMCDB::Access* m_tmcdb;
    // vector of antenna labels for this instance of this class
    antennaIDs_t m_antennaIDs;
    // Map of LO offset indices for this instance of this class
    loOffsetIndexMap_t m_loOffsetIndexMap;
};

#endif   // TMCDB_ANTENNA_LO_OFFSET_H
