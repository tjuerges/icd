#ifndef TMCDB_ANTENNA_CAI_H
#define TMCDB_ANTENNA_CAI_H
// @(#) $Id: TmcdbAntennaCAIMap.h,v 1.2 2011/04/02 13:48:21 jperez Exp $
//
// Copyright (C) 2010
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-workers@nrao.edu

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <TMCDBAccessIFC.h>
#include "almaEnumerations_IFC.h"
//#include <CorrEx.h>
#include "CorrelatorArrayC.h"

class TmcdbAntennaCAIMap {
public:
    
    /** cai type
     */
    typedef int cai_t;
    /** antenna name
     */
    typedef std::string antennaID_t;
    /** Vector of antenna names
     */
    typedef std::vector<std::string> antennaIDs_t;
    
    /** Map of cais keyed by antenna name
     */
    typedef std::map< antennaID_t, cai_t > antennaCAIMap_t;

    /** Default constructor accesses the TMCDB and retrieves all cai values for
     ** all antennas.
     ** @param container ACS Container Services
     ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
     */
    TmcdbAntennaCAIMap(maci::ContainerServices* containerServices,
		       CorrelatorNameMod::CorrelatorName correlator);

    /** Parametrized constructor accesses the TMCDB and retrieves allcai values for
     ** the specified antennas.
     ** @param container ACS Container Services
     ** @param antennaSeq Sequence of antennas
     ** @param correlator Correlator type enumerator
     ** @exception maciErrType::CannotGetComponentEx In case problems accessing the TMCDB Access component.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     ** @exception maciErrType::CannotReleaseComponentEx In case of problems releasing the TMCDB Access component.
     ** @exception CorrErr::InvalidParameter One of the antenna names passed is invalid.  Data should be
     **                             added to this exception specifying which antenna(s) is/are invalid
     */
    TmcdbAntennaCAIMap (maci::ContainerServices* containerServices,
			CorrelatorNameMod::CorrelatorName correlator,
			const Correlator::AntennaSeq_t &antennaSeq);

    /// Copy constructor
    TmcdbAntennaCAIMap(const TmcdbAntennaCAIMap &toCopy);

    /// Assignment operator
    TmcdbAntennaCAIMap &operator =(const TmcdbAntennaCAIMap &rhs);

    /// Destructor
    ~TmcdbAntennaCAIMap();

    /** Returns a map of CAIs.  The map is keyed by antenna names
     ** @return map of CAIs, if empty there are no CAIs 
     ** in TMCDB
     */
    const antennaCAIMap_t getCAIs();

    /** Returns a map CAIs.  The map is keyed by antenna names
     ** @param antIDs vector of antenna labels
     ** @return map of CAIs, if empty the antenna labels passed did not correspond
     **         to any CAIs in the TMCDB.
     ** @exception CorrErr::InvalidParameter One of the antenna names passed is invalid.  Data should be
     **                             added to this exception specifying shich antenna is invalid
     */
    const  antennaCAIMap_t getCAIs(antennaIDs_t &antIDs);

private:

    /**
     * Gets a reference for the TMCDB Access component.
     * @param container ACS Container Services
     * @exception maciErrType::CannotGetComponentEx
     */
    void getTmcdbAccessComponent(maci::ContainerServices* container);

    /**
     * Release reference to the TMCDB Access component.
     * @param container ACS Container Services
     * @exception maciErrType::CannotReleaseComponentEx
     */
    void releaseTmcdbAccessComponent(maci::ContainerServices* container);

    /**
     * Gets the antenna CAIs from the TMCDB. It fills the antennaCAIMap_t
     * map with the data.
     * @param tmcdb TMCDB Access component
     * @param antennas Antennas for which the CAIs should be retrieved.
     ** @exception TmcdbErrType::TmcdbErrorEx In case of errors getting data from the TMCDB.
     ** @exception TmcdbErrType::TmcdbNoSuchRowEx In case of missing records in the TMCDB.
     */
    void getCAIsFromTMCDB(TMCDB::Access* tmcdb, antennaIDs_t &antennas);

    /** Creates a default map that includes all possible antennas
     */
    void getDefaultCAIMap();

    TMCDB::Access* m_tmcdb;
    // vector of antenna labels for this instance of this class
    antennaIDs_t m_antennaIDs;
    // Map of CAIs for this instance of this class, i.e. antennas to be used by a single array
    antennaCAIMap_t m_caiMap;
    // Map of CAIs that either include all possible ALMA antennas, those in the antennaCAIMap file
    // Or all the entries in the TMCDB
    antennaCAIMap_t m_defaultCAIMap;
};

#endif   // TMCDB_ANTENNA_CAI_H
