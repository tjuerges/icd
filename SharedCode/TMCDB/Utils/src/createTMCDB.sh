#!/bin/bash
#
# Script used to create and populate the TMCDB with the contents
# from the Simulation XML-based CDB and the TMCDB*Add.xml files
# from the CONTROL source code.
#
# This script starts the HSQLDB server, loads the table definitions,
# then populates the SW tables. Finally it populates some of the HW
# tables.
#
# The manual instructions in which this script is based can be found in
#
# http://almasw.hq.eso.org/almasw/bin/view/CONTROL/InitialTMCDBPopulation
#
# 
# Author: rtobar
#
# $Id$

# We check which kind of DB we want to use
if [ $# -lt 1 ]
then
	echo "Usage: $0 [ oracle | hsqldb ]"
	exit 1;
fi

if [ $1 != oracle -a $1 != hsqldb ]
then
	echo "Usage: $0 [ oracle | hsqldb ]"
	exit 1;
	exit 1;
fi

dbtype=$1


if [ $ACS_CDB = $ACSDATA/config/defaultCDB ]
then
	echo "You should use CONTROL's Simulation CDB, not the default CDB, will exit now"
	exit 1
fi

onICD=$(echo $PWD | grep "ICD/SharedCode/TMCDB/Utils/src")

if [ -z $onICD ]
then
	echo "You should run this script from ICD/SharedCode/TMCDB/Utils/src directly, will exit now"
	exit 1
fi

# Set relevant variables
if [ $dbtype = "hsqldb" ]
then
	TMCDB_CONNECTION="doesn't matter"
	export ORACLE_HOME="doesn't matter"
	TMCDB_USER='sa'
	TMCDB_PASS=
	TMCDB_JDBC_CONNECTION="jdbc:hsqldb:hsql://localhost:8090"
else # $dbtype = oracle
	TMCDB_CONNECTION='//localhost:1521/XE'
	TMCDB_USER='tmc90'
	TMCDB_PASS='tmc$dba'
	TMCDB_JDBC_CONNECTION="jdbc:oracle:thin:@${TMCDB_CONNECTION}"
fi

echo "Connecting to ${TMCDB_JDBC_CONNECTION}, user/pass: ${TMCDB_USER}/${TMCDB_PASS}"
echo "Press [ENTER] to continue, or Ctrl-C to cancel"
read dummy

# First, create the directory when the logs and the TMCDB DB (for hsqldb) will reside
DBDIR=$PWD/TMCDB

if [ -d $DBDIR ]
then
	echo "Directory TMCDB already exists, will exit now"
	exit 1
fi
mkdir -p $DBDIR/logs

# Create the local archiveConfig.properties file
echo "##############
# general section
archive.db.mode=operational
archive.db.connection=jdbc:oracle:thin:@//localhost:1521/XE
archive.oracle.user=alma

##############
# TMCDB section

# Service alias used by TMCDB, might be different from the one used by rest of Archive
# connection: to be adapted
archive.tmcdb.connection=$TMCDB_JDBC_CONNECTION
archive.tmcdb.user=$TMCDB_USER
archive.tmcdb.passwd=$TMCDB_PASS

###############
# relational section, ie. the rest of subsystems accessing the DB
# directly, but not monitor, log or statearchive data. In the moment, this would be shiftlog.archive.relational.user=almatest
#archive.relational.connection=jdbc:hsqldb:hsql://localhost:8090
archive.relational.connection=jdbc:oracle:thin:@//localhost:1521/XE
archive.relational.user=operlogtest
archive.relational.passwd=alma$dba

###############
#schemas
archive.bulkstore.schema=ASDMBinaryTable
archive.bulkreceiver.schema=sdmDataHeader

###############
#NGAS 
archive.ngast.servers=arch01:7777
archive.ngast.bufferDir=/archiverd
archive.ngast.interface=test:\${ACS.data}/tmp

###############
#bulkreceiver
archive.bulkreceiver.debug=True
archive.bulkreceiver.DataBufferRetry=30 
archive.bulkreceiver.DataBufferMax=10240000 
archive.bulkreceiver.BufferThreadNumber=8 
archive.bulkreceiver.BufferThreadWaitSleep=2000 
archive.bulkreceiver.FetchThreadRetry=100 
archive.bulkreceiver.FetchThreadRetrySleep=400000" > archiveConfig.properties

# Start server (if necessary) and create tanbles
if [ $dbtype = "hsqldb" ]
then
	# Start the hsqldb server
	echo "Starting HSQLDB server, logs to $DBDIR/logs/startHSQLDB.log"
	startHSQLDB $DBDIR &> $DBDIR/logs/startHSQLDB.log &

	while true
	do
		grepResult=$(grep "Startup sequence completed"  $DBDIR/logs/startHSQLDB.log)
		if [ ! -z "$grepResult" ]
		then
			break
		fi
		sleep 1
	done

	# Load the necessary schemas table definitions with sqltool
	echo "Loading Table schemas, logs to $DBDIR/logs/sqltool-createTables.log"
	echo -e "\i $ACSDATA/config/DDL/hsqldb/TMCDB_swconfigcore/CreateHsqldbTables.sql\n\i $ACSDATA/config/DDL/hsqldb/TMCDB_swconfigext/CreateHsqldbTables.sql\n\i $ACSDATA/config/DDL/hsqldb/TMCDB_hwconfigmonitoring/CreateHsqldbTables.sql" | acsStartJava org.hsqldb.cmdline.SqlTool --rcFile sqltool.rc localhost-sa &> $DBDIR/logs/sqltool-createTables.log

else # $dbtype = oracle
	echo "Clearing existing tables and creating new ones, logs to $DBDIR/logs/sqlplus-dropCreateTables.log"
	echo -e "@ $ACSDATA/config/DDL/oracle/TMCDB_hwconfigmonitoring/DropAllOracleSequences.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_hwconfigmonitoring/DropAllTables.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigext/DropAllOracleSequences.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigcore/DropAllOracleSequences.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigext/DropAllTables.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigcore/DropAllTables.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigcore/CreateOracleTables.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_swconfigext/CreateOracleTables.sql\n@ $ACSDATA/config/DDL/oracle/TMCDB_hwconfigmonitoring/CreateOracleTables.sql" | sqlplus $TMCDB_USER/"$TMCDB_PASS"@$TMCDB_CONNECTION &> $DBDIR/logs/sqlplus-dropCreateTables.log
fi

# Populate the SW contents of the TMCDB
echo "Starting Hibernate DAL, logs to  $DBDIR/logs/hibernateCdbJDal.log"
#export TMCDB_ACS_ONLY=true
export TMCDB_CONFIGURATION_NAME=Test

export JAVA_OPTIONS="-Darchive.configFile=./archiveConfig.properties"
hibernateCdbJDal -loadXMLCDB &> $DBDIR/logs/hibernateCdbJDal.log &
hdalPID=$!

echo "Waiting Hibernate DAL to load the data from XML CDB (this may take a few minutes)"

while true
do
	grepResult=$(grep "JDAL is ready and waiting"  $DBDIR/logs/hibernateCdbJDal.log)
	if [ ! -z "$grepResult" ]
	then
		break
	fi
	sleep 1
done

# Loading the contents of the HW tables
echo "Loading LRUs, logs to $DBDIR/logs/lruloader.log"
acsStartJava  -endorsed alma.tmcdb.utils.LruLoader &> $DBDIR/logs/lruloader.log
echo "Loading AssemblyRoles, logs to $DBDIR/logs/assemblyroleloader.log"
acsStartJava -endorsed alma.tmcdb.utils.AssemblyRoleLoader &> $DBDIR/logs/assemblyroleloader.log
echo "Loading Configuration and Startup, logs to $DBDIR/logs/configurationloader.log"
acsStartJava -endorsed alma.tmcdb.utils.ConfigurationLoader ../config/sampleTmcdbDatabaseConfiguration.xml &> $DBDIR/logs/configurationloader.log
echo "Loading Assembly data, logs to $DBDIR/logs/assemblydataloader.log"
acsStartJava -endorsed alma.tmcdb.utils.AssemblyDataLoader &> $DBDIR/logs/assemblydataloader.log

# Shutdown everything
rm -f hibernateCdbjDAL.gclog

superkill() {
	pid=$1
	pids=$(ps -e -o pid= -o ppid= | awk -v pid="$pid" '$2 == pid {print $1}')

	if [ ! -z $pids ]
	then
		for i in $pids
		do
			superkill $i
		done
	fi

	kill $pid
}

# Shutdown everything
echo "Shutting down Hibernate DAL"
superkill $hdalPID

if [ $dbtype = hsqldb ]
then
	echo "Shutting down HSQLDB server"
	echo "shutdown;" | acsStartJava org.hsqldb.cmdline.SqlTool --rcFile sqltool.rc localhost-sa &> $DBDIR/logs/sqltool-shutdown.log
	echo "Compressing TMCDB database into $DBDIR/TMCDB.tar.gz"
	cd TMCDB; tar czf ../TMCDB.tar.gz TMCDB
fi

echo "Done!"
