#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
#

from optparse import OptionParser
import commands
import sys

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-c", "--configuration", dest="tmcdb",
        help="from which TMCDB configuration export the pointing model.", metavar="TMCDB")
    parser.add_option("-f", "--file", dest="filename",
        help="output XML file where store the pointing model.", metavar="FILE")

    (options, args) = parser.parse_args()

    if options.tmcdb is None:
        tmcdb = str(commands.getoutput("echo $TMCDB_CONFIGURATION_NAME"))
    else:
        tmcdb = str(options.tmcdb)

    if options.filename is None:
        pid = str(commands.getoutput("echo $$"))
        filename = "PointingModel-" + pid + ".xml"
    else:
        filename = str(options.filename)
        filename = filename.rstrip(".xml") + ".xml"
        
    instruction = "acsStartJava -endorsed alma.tmcdb.utils.PointingModelExporter "
    instruction = instruction + tmcdb + " " + filename + " &> /dev/null"
    commands.getoutput(instruction)
    print "From " + tmcdb+ "  pointing models for all antennas saved in " + filename + "\n"