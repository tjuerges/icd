/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.XPDelay;
import alma.tmcdb.generated.configuration.AntennaDelaysT;
import alma.tmcdb.generated.configuration.CrossPolarizationDelaysT;
import alma.tmcdb.generated.configuration.DelayModels;
import alma.tmcdb.generated.configuration.FEDelayT;
import alma.tmcdb.generated.configuration.IFDelayT;
import alma.tmcdb.generated.configuration.LODelayT;
import alma.tmcdb.generated.configuration.XPDelayT;
import alma.tmcdb.generated.configuration.types.BasebandNameEnumT;
import alma.tmcdb.generated.configuration.types.IFProcConnectionStateEnumT;
import alma.tmcdb.generated.configuration.types.NetSidebandEnumT;
import alma.tmcdb.generated.configuration.types.PolarizationTypeEnumT;
import alma.tmcdb.generated.configuration.types.ReceiverBandEnumT;

/**
 * A utility to export the delay coefficients for all antennas in a given
 * configuration to an XML file.
 * 
 * @author rhiriart@nrao.edu
 *
 */
class DelayModelExporter {

    private Logger logger =
        TmcdbLoggerFactory.getLogger("alma.tmcdb.utils.PointingModelExporter");
    
    private Session session;
    
    public DelayModels exportDelayModels(String configuration) throws TmcdbException {
        
        Configuration cnf = null;
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction trx = session.beginTransaction();
        String query = "from Configuration where configurationname = '" + configuration + "'";
        List<Configuration> configs = session.createQuery(query).list();
        if (configs.size() == 1) {
            cnf = (Configuration) configs.get(0);
        } else {
            throw new TmcdbException("Configuration not found: " + configuration);
        }

        // Get the respective HwConfiguration given the Configuration ID. If none exists, create a new one
        HwConfiguration hwConf = null;
        Query q = session.createQuery("from HwConfiguration where swConfiguration = :conf");
        q.setParameter("conf", cnf, Hibernate.entity(Configuration.class));
        List<HwConfiguration> hwConfigs = q.list();
        if( hwConfigs.size() == 1) {
            hwConf = (HwConfiguration)hwConfigs.get(0);
        } else {
            throw new TmcdbException("HWConfiguration not found for Configuration: " + configuration);
        }        
        
        DelayModels xmlDelayModels = new DelayModels();

        CrossPolarizationDelaysT xmlXPDelays = new CrossPolarizationDelaysT();
        Set<XPDelay> xpds = hwConf.getCrossPolarizationDelays();
        for (Iterator<XPDelay> it = xpds.iterator(); it.hasNext();) {
            XPDelay xpd = it.next();
            XPDelayT xmlXPD = new XPDelayT();
            xmlXPD.setBaseband(BasebandNameEnumT.valueOf(xpd.getBaseband().toString()));
            xmlXPD.setReceiverBand(ReceiverBandEnumT.valueOf(xpd.getReceiverBand().toString()));
            xmlXPD.setSideband(NetSidebandEnumT.valueOf(xpd.getSideband().toString()));
            xmlXPD.setValue(xpd.getDelay());
            xmlXPDelays.addXPDelay(xmlXPD);
        }
        xmlDelayModels.setXPDelays(xmlXPDelays);
        
        for (BaseElement be : hwConf.getBaseElements()) {
            if (be instanceof Antenna) {
                Antenna a = (Antenna) be;
                logger.info(a.getName());
                
                if (( a.getFrontEndDelays().size() == 0 ) &&
                    ( a.getIfDelays().size() == 0 ) &&
                    ( a.getLoDelays().size() == 0 ))
                    continue;
                
                AntennaDelaysT xmlAntennaDelays = new AntennaDelaysT();
                xmlAntennaDelays.setAntenna(a.getName());
                
                Set<FEDelay> feds = a.getFrontEndDelays();
                for (Iterator<FEDelay> it = feds.iterator(); it.hasNext();) {
                    FEDelay fed = it.next();
                    FEDelayT xmlFED = new FEDelayT();
                    xmlFED.setPolarization(PolarizationTypeEnumT.valueOf(fed.getPolarization().toString()));
                    xmlFED.setReceiverBand(ReceiverBandEnumT.valueOf(fed.getReceiverBand().toString()));
                    xmlFED.setSideband(NetSidebandEnumT.valueOf(fed.getSideband().toString()));
                    xmlFED.setValue(fed.getDelay());
                    xmlAntennaDelays.addFEDelay(xmlFED);
                }
                
                Set<IFDelay> ifds = a.getIfDelays();
                for (Iterator<IFDelay> it = ifds.iterator(); it.hasNext();) {
                    IFDelay ifd = it.next();
                    IFDelayT xmlIFD = new IFDelayT();
                    xmlIFD.setBaseband(BasebandNameEnumT.valueOf(ifd.getBaseband().toString()));
                    xmlIFD.setIfswitch(IFProcConnectionStateEnumT.valueOf(ifd.getIfSwitch().toString()));
                    xmlIFD.setPolarization(PolarizationTypeEnumT.valueOf(ifd.getPolarization().toString()));
                    xmlIFD.setValue(ifd.getDelay());
                    xmlAntennaDelays.addIFDelay(xmlIFD);
                }
                
                Set<LODelay> lods = a.getLoDelays();
                for (Iterator<LODelay> it = lods.iterator(); it.hasNext();) {
                    LODelay lod = it.next();
                    LODelayT xmlLOD = new LODelayT();
                    xmlLOD.setBaseband(BasebandNameEnumT.valueOf(lod.getBaseband().toString()));
                    xmlLOD.setValue(lod.getDelay());
                    xmlAntennaDelays.addLODelay(xmlLOD);
                }
                
                xmlDelayModels.addAntennaDelays(xmlAntennaDelays);             
            }
        }
        
        trx.commit();
        session.close();
        
        return xmlDelayModels;
    }
    
    public static void main(String[] args) {
        String configuration = args[0];
        DelayModelExporter exporter = new DelayModelExporter();
        try {
            DelayModels dms = exporter.exportDelayModels(configuration);
            FileWriter out = new FileWriter(args[1]);
            dms.marshal(out);
            out.close();
        } catch (TmcdbException ex) {
            ex.printStackTrace();
        } catch (MarshalException ex) {
            ex.printStackTrace();
        } catch (ValidationException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
