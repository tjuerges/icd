/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AssemblyRoleLoader.java,v 1.23 2011/03/08 21:54:23 sharring Exp $"
 */
package alma.tmcdb.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import org.exolab.castor.xml.XMLException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyType;

/**
 * Populates in the TMCDB database the AssemblyRole and related tables.
 * It mantains this information in an internal map with hardcoded values.
 * AssemblyRoles are not dynamic, this information is not expected to change.
 * They are defined by the position that a given assembly can play in the system.
 */
public class AssemblyRoleLoader {

	static Logger logger = TmcdbLoggerFactory.getLogger(AssemblyRoleLoader.class.getName());

	/** The map that holds the roles in the system */
	public static Map<String, String> roles = new HashMap<String, String>();

	/** Role map static initialization */
	static {

		//               === Antenna ===
		//        Role Name           Assembly Type
		roles.put("DTXBBpr0",         "DTX");
		roles.put("DTXBBpr1",         "DTX");
		roles.put("DTXBBpr2",         "DTX");
		roles.put("DTXBBpr3",         "DTX");
		roles.put("DTSRBBpr0",        "DTSR");
		roles.put("DTSRBBpr1",        "DTSR");
		roles.put("DTSRBBpr2",        "DTSR");
		roles.put("DTSRBBpr3",        "DTSR");
		roles.put("DRXBBpr0",         "DRX");
		roles.put("DRXBBpr1",         "DRX");
		roles.put("DRXBBpr2",         "DRX");
		roles.put("DRXBBpr3",         "DRX");
		roles.put("SAS",              "SAS");
		roles.put("Mount",            "Mount");
		roles.put("IFProc0",          "IFProc");
		roles.put("IFProc1",          "IFProc");
		roles.put("LORR",             "LORR");
		roles.put("FLOOG",            "FLOOG");
		roles.put("FOADBBpr0",        "FOAD");
		roles.put("FOADBBpr1",        "FOAD");
		roles.put("FOADBBpr2",        "FOAD");
		roles.put("FOADBBpr3",        "FOAD");
		roles.put("DGCK",             "DGCK");
		roles.put("LO2BBpr0",         "LO2");
		roles.put("LO2BBpr1",         "LO2");
		roles.put("LO2BBpr2",         "LO2");
		roles.put("LO2BBpr3",         "LO2");
		roles.put("OpticalTelescope", "OpticalTelescope");
		roles.put("HoloRx",           "HoloRx");
		roles.put("HoloDSP",          "HoloDSP");
		roles.put("LLC",              "LLC");
		roles.put("PSA",              "PSA");
		roles.put("PSD",              "PSD");
		roles.put("WVR",              "WVR");

		//              === FrontEnd ===
		//        Role Name      Assembly Type
		roles.put("ACD",         "ACD");
		roles.put("LPR",         "LPR");
		roles.put("IFSwitch",    "IFSwitch");
		roles.put("Cryostat",    "Cryostat");
		roles.put("WCA1",        "WCA1");
		roles.put("WCA2",        "WCA2");
		roles.put("WCA3",        "WCA3");
		roles.put("WCA4",        "WCA4");
		roles.put("WCA5",        "WCA5");
		roles.put("WCA6",        "WCA6");
		roles.put("WCA7",        "WCA7");
		roles.put("WCA8",        "WCA8");
		roles.put("WCA9",        "WCA9");
		roles.put("WCA10",       "WCA10");
		roles.put("ColdCart1",   "ColdCart1");
		roles.put("ColdCart2",   "ColdCart2");
		roles.put("ColdCart3",   "ColdCart3");
		roles.put("ColdCart4",   "ColdCart4");
		roles.put("ColdCart5",   "ColdCart5");
		roles.put("ColdCart6",   "ColdCart6");
		roles.put("ColdCart7",   "ColdCart7");
		roles.put("ColdCart8",   "ColdCart8");
		roles.put("ColdCart9",   "ColdCart9");
		roles.put("ColdCart10",  "ColdCart10");
		roles.put("PowerDist1",  "PowerDist1");
		roles.put("PowerDist2",  "PowerDist2");
		roles.put("PowerDist3",  "PowerDist3");
		roles.put("PowerDist4",  "PowerDist4");
		roles.put("PowerDist5",  "PowerDist5");
		roles.put("PowerDist6",  "PowerDist6");
		roles.put("PowerDist7",  "PowerDist7");
		roles.put("PowerDist8",  "PowerDist8");
		roles.put("PowerDist9",  "PowerDist9");
		roles.put("PowerDist10", "PowerDist10");

		//         === CentralLO ===
		//        Role Name Assembly Type
		roles.put("CVR",    "CVR");
		roles.put("LS",     "LS");
		roles.put("PRD",    "PRD");
		roles.put("ML",     "ML");
		roles.put("MLD",    "MLD");
		roles.put("PSLLC1", "PSLLC1");
		roles.put("PSLLC2", "PSLLC2");
		roles.put("PSLLC3", "PSLLC3");
		roles.put("PSLLC4", "PSLLC4");
		roles.put("PSLLC5", "PSLLC5");
		roles.put("PSLLC6", "PSLLC6");
		roles.put("PSSAS1", "PSSAS1");
		roles.put("PSSAS2", "PSSAS2");

		//          === AOSTiming ===
		//        Role Name   Assembly Type
		roles.put("CRD",      "CRD");
		roles.put("GPS",      "GPS");
		roles.put("PSCR",     "PSCR");
		roles.put("LFRD",     "LFRD");

		//          === WeatherStation ===
		//        Role Name   Assembly Type
		roles.put("WSOSF",    "WSOSF");
		roles.put("WSTB1",    "WSTB1");
		roles.put("WSTB2",    "WSTB2");
    }

    /**
     * Loads all the assembly roles into the database
     * 
     * @return True if everything goes fine, false otherwise
     */
    public static boolean loadAssemblyRoles() {
    	try {
            loadAssemblyRoles(false, false);
        } catch (DbConfigException ex) {
            return false;
        } catch (FileNotFoundException ex) {
        	return false;
		} catch (XMLException ex) {
			return false;
		} catch (TmcdbException ex) {
			return false;
		}
		return true;
    }
    /**
     * Loads all the roles in the database.
     * 
     * @param addMissingComponentTypes
     *     If true, then if a record in the ComponentType table is missing, a dummy record is added.
     *     The AssemblyRole requires a record in AssemblyType, which requires a record in
     *     ComponentType. 
     * @param fakeMissingLruFile
     *     The AssemblyType and LruType tables are populated from XML files deployed in the system.
     *     If this parameter is true, then in the case of a missing file, a dummy record is created.
     * @throws DbConfigException
     * 	   In case of problems in dbConfig.properties.
     * @throws FileNotFoundException
     *     In case of problems reading a TMCDB XML configuration file, when fakeMissingLruFile
     *     has been set to false.
     * @throws XMLException
     *     In case of problems parsing a TMCDB XML configuration file, or a dummy string containing
     *     XML in the case of fakeMissingLruFile is true.
     * @throws TmcdbException
     *     In case of a missing record in the ComponentType table, when addMissingComponentTypes
     *     has been set to false.
     */
    public static void loadAssemblyRoles(boolean addMissingComponentTypes, boolean fakeMissingLruFile)
    	throws DbConfigException, FileNotFoundException, XMLException, TmcdbException {
        
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch(Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        Session session;
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction tx = session.beginTransaction();
        for (Iterator<String> iter = roles.keySet().iterator(); iter.hasNext(); ) {
        	String roleName = iter.next();
        	String assemblyTypeName = roles.get(roleName);
        	logger.info("Loading Assembly Role Type: " + assemblyTypeName);
        	loadAssemblyRole(session, assemblyTypeName, roleName, addMissingComponentTypes, fakeMissingLruFile);
        }
        tx.commit();
        session.close();
    }

    /**
     * Loads one assembly role into the database.
     * 
     * @param session
     *     Hibernate Session.
     * @param roleName
     *     Role name.
     * @param addMissingComponentTypes
     *     If true, then if a record in the ComponentType table is missing, a dummy record is added.
     *     The AssemblyRole requires a record in AssemblyType, which requires a record in
     *     ComponentType. 
     * @param fakeMissingLruFile
     *     The AssemblyType and LruType tables are populated from XML files deployed in the system.
     *     If this parameter is true, then in the case of a missing file, a dummy record is created.
     * @throws DbConfigException
     * 	   In case of problems in dbConfig.properties.
     * @throws FileNotFoundException
     *     In case of problems reading a TMCDB XML configuration file, when fakeMissingLruFile
     *     has been set to false.
     * @throws XMLException
     *     In case of problems parsing a TMCDB XML configuration file, or a dummy string containing
     *     XML in the case of fakeMissingLruFile is true.
     * @throws TmcdbException
     *     In case of a missing record in the ComponentType table, when addMissingComponentTypes
     *     has been set to false.
     */
    public static void loadAssemblyRole(Session session, String roleName,
    		boolean addMissingComponentType, boolean fakeMissingLruFile)
    	throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
    	
        String assemblyTypeName = roles.get(roleName);
        if (assemblyTypeName == null)
            throw new NullPointerException("Role not found in Role Map: " + roleName);

        loadAssemblyRole(session, assemblyTypeName, roleName, addMissingComponentType, fakeMissingLruFile);
    }

    /**
     * Loads one assembly role into the database.
     * 
     * @param session
     *     Hibernate session.
     * @param assemblyTypeName
     *     AssemblyType name.
     * @param roleName
     *     Role name.
     * @param channel
     *     CAN channel number.
     * @param node
     *     CAN node.
     * @param base
     *     CAN base address (needed for the FrontEnd).
     * @param addMissingComponentTypes
     *     If true, then if a record in the ComponentType table is missing, a dummy record is added.
     *     The AssemblyRole requires a record in AssemblyType, which requires a record in
     *     ComponentType. 
     * @param fakeMissingLruFile
     *     The AssemblyType and LruType tables are populated from XML files deployed in the system.
     *     If this parameter is true, then in the case of a missing file, a dummy record is created.
     * @throws DbConfigException
     * 	   In case of problems in dbConfig.properties.
     * @throws FileNotFoundException
     *     In case of problems reading a TMCDB XML configuration file, when fakeMissingLruFile
     *     has been set to false.
     * @throws XMLException
     *     In case of problems parsing a TMCDB XML configuration file, or a dummy string containing
     *     XML in the case of fakeMissingLruFile is true.
     * @throws TmcdbException
     *     In case of a missing record in the ComponentType table, when addMissingComponentTypes
     *     has been set to false.
     */
    public static void loadAssemblyRole(Session session,
                                        String assemblyTypeName,
                                        String roleName,
                                        boolean addMissingComponentType,
                                        boolean fakeMissingLruFile)
    	throws XMLException, DbConfigException, TmcdbException, FileNotFoundException {
    	
        AssemblyType at = (AssemblyType) 
            session.createQuery("FROM AssemblyType WHERE name = '" +
                                assemblyTypeName + "'").uniqueResult();
        if (at == null) {
            String lruConfFilePath;

			try {
				lruConfFilePath = LruLoader.findTmcdbHwConfigFile(assemblyTypeName);
		        FileReader fr = null;
				fr = new FileReader(lruConfFilePath);
				LruLoader.loadLruType(session, fr, true);
			} catch (FileNotFoundException ex) {
				if (fakeMissingLruFile) {
				logger.warning("File not found for " + assemblyTypeName);
				logger.info("Adding Dummy LRU for " + assemblyTypeName);
				StringReader sr = new StringReader(getDummyLRUDesc(assemblyTypeName));
				LruLoader.loadLruType(session, sr, true);
				} else {
					throw ex;
				}
			}
            at = (AssemblyType)
            	session.createQuery("FROM AssemblyType WHERE name = '" +
                                    assemblyTypeName + "'").uniqueResult();
        }
        AssemblyRole ar =
            new AssemblyRole(roleName);
        at.addRole(ar);
        session.save(at);
    }

    /**
     * Creates a dummy LRU XML description, following the same Schema as the
     * LRU XML configuration files created by the HW code generated framework.
     * 
     * @param assemblyTypeName Assembly type name
     * @return XML LRU description
     */
    private static String getDummyLRUDesc(String assemblyTypeName) {
    	StringBuffer sb = new StringBuffer();
    	sb.append("<?xml version='1.0' ?>");
    	sb.append("<lru-type>");
    	sb.append("<name>" + assemblyTypeName + "</name>");
    	sb.append("<fullname>Dummy LRU</fullname>");
    	sb.append("<icd>ALMA-DUMMY-ICD</icd>");
    	sb.append("<icd-date>0</icd-date>");
    	sb.append("<description>Dummy LRU</description>" );
    	sb.append("<notes/>");
    	sb.append("<assembly-type>");
    	sb.append("<name>" + assemblyTypeName + "</name>");
    	sb.append("<dev-name>Dummy LRU</dev-name>");
    	sb.append("<description>Dummy LRU</description>");
    	sb.append("<default-role>");
    	sb.append("<node>none</node>");
    	sb.append("<base-address>none</base-address>");
    	sb.append("<channel>none</channel>");
    	sb.append("</default-role>");
    	sb.append("</assembly-type>");
    	sb.append("</lru-type>");
    	return sb.toString();
    }

    /**
     * Loads all the AssemblyRoles into the database. It requires that all the
     * ComponentTypes have already been loaded into the system, and that all required
     * records are present in LRUType and AssemblyType.
     * 
     * @param args Command line arguments, ignored.
     */
    public static void main(String[] args) {
        try {
            loadAssemblyRoles(true, true);
        } catch (DbConfigException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (XMLException ex) {
			ex.printStackTrace();
		} catch (TmcdbException ex) {
			ex.printStackTrace();
		}
    }
}
