/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.hla.datamodel.enumeration.JReceiverBand;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.generated.configuration.CoeffT;
import alma.tmcdb.generated.configuration.OffsetT;
import alma.tmcdb.generated.configuration.PointingModelT;
import alma.tmcdb.generated.configuration.PointingModels;

public class PointingModelImporter {

    private Logger logger =
        TmcdbLoggerFactory.getLogger("alma.tmcdb.utils.PointingModelExporter");
    private Session session;
    
    public void addPointingModelToAntenna(Antenna antenna, Reader file)
        throws MarshalException, ValidationException, AcsJBadParameterEx {
        alma.tmcdb.generated.configuration.PointingModelT xmlPM =
            alma.tmcdb.generated.configuration.PointingModelT.unmarshalPointingModelT(file);
        addPointingModelToAntenna(antenna, xmlPM);
    }
    
    public void addPointingModelToAntenna(Antenna antenna,
            alma.tmcdb.generated.configuration.PointingModelT xmlPM)
        throws MarshalException, ValidationException, AcsJBadParameterEx {
        if (!antenna.getName().equals(xmlPM.getAntenna())) {
            AcsJBadParameterEx ex = new AcsJBadParameterEx();
            String msg = "Invalid antenna: XML file contained " + xmlPM.getAntenna() +
                          " but you are using " + antenna.getName();
            ex.setReason(msg);
            throw ex;
        }
        PointingModel pointingModel = null;
        boolean newPointingModel = false;
        Set<String> existingCoeffs = new HashSet<String>();
        Set<PointingModel> pointingModels = antenna.getPointingModels();
        // We assume that there is only one pointing model per antenna.
        Iterator<PointingModel> iterator = pointingModels.iterator();
        if (iterator.hasNext()) {
            // There is already a pointing model. Put the already existing coefficients
            //  in the existing coeff set.
            pointingModel = iterator.next();
            Iterator<String> it = pointingModel.getTerms().keySet().iterator();
            while (it.hasNext()) {
                existingCoeffs.add(it.next());
            }
        } else {
            // No pointing model, create one.
            newPointingModel = true;
            pointingModel = new PointingModel();
        }
        pointingModel.setAntenna(antenna);
        CoeffT[] iTerms = xmlPM.getCoeff();
        for (int i = 0; i < iTerms.length; i++) {
            CoeffT iTerm = iTerms[i];
            float value = (float) iTerm.getValue();
            PointingModelCoeff coeff = null;
            if ( existingCoeffs.contains(iTerm.getName()) ) {
                coeff = pointingModel.getTerm(iTerm.getName());
                coeff.setValue(value);
            } else {
                coeff = new PointingModelCoeff(iTerm.getName(), value);                
                pointingModel.addTerm(iTerm.getName(), coeff);
            }            
            OffsetT[] xmlOffsets = iTerm.getOffset();
            for (int j = 0; j < xmlOffsets.length; j++) {
                OffsetT xmlOffset = xmlOffsets[j];
                coeff.getOffsets()
                     .put(JReceiverBand.literal(xmlOffset.getReceiverBand().toString()),
                                                xmlOffset.getValue());
            }
        }
        if (newPointingModel) {
            antenna.getPointingModels().add(pointingModel);
        }
    }
    
    public void importPointingModels(String configuration, PointingModels pointingModels)
        throws TmcdbException, MarshalException, ValidationException, AcsJBadParameterEx {
        
        Configuration cnf = null;
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction trx = session.beginTransaction();
        String query = "from Configuration where configurationname = '" + configuration + "'";
        List<Configuration> configs = session.createQuery(query).list();
        if (configs.size() == 1) {
            cnf = (Configuration) configs.get(0);
        } else {
            throw new TmcdbException("Configuration not found: " + configuration);
        }

        // Get the respective HwConfiguration given the Configuration ID. If none exists, create a new one
        HwConfiguration hwConf = null;
        Query q = session.createQuery("from HwConfiguration where swConfiguration = :conf");
        q.setParameter("conf", cnf, Hibernate.entity(Configuration.class));
        List<HwConfiguration> hwConfigs = q.list();
        if( hwConfigs.size() == 1) {
            hwConf = (HwConfiguration)hwConfigs.get(0);
        } else {
            throw new TmcdbException("HWConfiguration not found for Configuration: " + configuration);
        }

        Set<BaseElement> baseElements = hwConf.getBaseElements();
        for (PointingModelT xmlpm : pointingModels.getPointingModel()) {
            String antennaName = xmlpm.getAntenna();
            for (Iterator<BaseElement> iter = baseElements.iterator(); iter.hasNext();) {
                BaseElement be = iter.next();
                if (be.getName().equals(antennaName) && (be instanceof Antenna)) {
                    Antenna a = (Antenna) be;                    
                    // create and add the pointing model
                    addPointingModelToAntenna(a, xmlpm);
                    session.saveOrUpdate(a);
                }
            }
        }
        trx.commit();
        session.close();
    }
    
    public static void main(String[] args) {
        String configuration = args[0];
        String fileName = args[1];
        try {
            FileReader reader = new FileReader(fileName);
            PointingModels pms = PointingModels.unmarshalPointingModels(reader);
            PointingModelImporter importer = new PointingModelImporter();
            importer.importPointingModels(configuration, pms);
        } catch (MarshalException ex) {
            ex.printStackTrace();
        } catch (ValidationException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (AcsJBadParameterEx ex) {
            ex.printStackTrace();
        } catch (TmcdbException ex) {
            ex.printStackTrace();
        }
    }
}
