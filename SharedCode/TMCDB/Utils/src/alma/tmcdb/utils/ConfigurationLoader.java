/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: ConfigurationLoader.java,v 1.29 2011/02/18 21:25:03 sharring Exp $"
 */
package alma.tmcdb.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.exolab.castor.xml.XMLException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.hla.datamodel.enumeration.JBasebandName;
import alma.hla.datamodel.enumeration.JNetSideband;
import alma.hla.datamodel.enumeration.JPolarizationType;
import alma.hla.datamodel.enumeration.JReceiverBand;
import alma.tmcdb.domain.AOSTiming;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.ArrayReference;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementStartupType;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.CentralLO;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.FocusModelCoeff;
import alma.tmcdb.domain.FrontEnd;
import alma.tmcdb.domain.HolographyTower;
import alma.tmcdb.domain.HolographyTowerToPad;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.IFProcConnectionState;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.PhotonicReference;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.domain.WeatherStationController;
import alma.tmcdb.domain.XPDelay;
import alma.tmcdb.generated.configuration.AOSTimingStartupT;
import alma.tmcdb.generated.configuration.AOSTimingT;
import alma.tmcdb.generated.configuration.Antenna2Pad;
import alma.tmcdb.generated.configuration.AntennaDelaysT;
import alma.tmcdb.generated.configuration.AntennaStartupT;
import alma.tmcdb.generated.configuration.AntennaT;
import alma.tmcdb.generated.configuration.AssemblyRoleT;
import alma.tmcdb.generated.configuration.CoeffT;
import alma.tmcdb.generated.configuration.CrossPolarizationDelaysT;
import alma.tmcdb.generated.configuration.FEDelayT;
import alma.tmcdb.generated.configuration.FocusModelT;
import alma.tmcdb.generated.configuration.FrontEndStartupT;
import alma.tmcdb.generated.configuration.FrontEndT;
import alma.tmcdb.generated.configuration.HolographyTower2Pad;
import alma.tmcdb.generated.configuration.HolographyTowerT;
import alma.tmcdb.generated.configuration.IFDelayT;
import alma.tmcdb.generated.configuration.LODelayT;
import alma.tmcdb.generated.configuration.LocalOscillatorStartupT;
import alma.tmcdb.generated.configuration.LocalOscillatorT;
import alma.tmcdb.generated.configuration.OffsetT;
import alma.tmcdb.generated.configuration.PadT;
import alma.tmcdb.generated.configuration.PhotonicReferenceStartupT;
import alma.tmcdb.generated.configuration.PhotonicReferenceT;
import alma.tmcdb.generated.configuration.PointingModelT;
import alma.tmcdb.generated.configuration.StartupT;
import alma.tmcdb.generated.configuration.WeatherStationControllerStartupT;
import alma.tmcdb.generated.configuration.WeatherStationControllerT;
import alma.tmcdb.generated.configuration.XPDelayT;

public class ConfigurationLoader {

	Logger logger = TmcdbLoggerFactory.getLogger("alma.tmcdb.utils.ConfigurationLoader");
    
    private Session session;
	private String[] bands = new String[] { "band1", "band2", "band3", "band4",
			"band5", "band6", "band7", "band8", "band9", "band10" };
    private Map<String, Antenna> newAntennas;
    private Map<String, CentralLO> newLOs;
    private Map<String, AOSTiming> newAOSTiming;
    private Map<String, FrontEnd> newFrontEnds;
    private Map<String, WeatherStationController> newWeatherStations;
    private Map<String, PhotonicReference> newPhotRefs;
    private Map<String, HolographyTower> newHolographyTowers;
    private Map<String, Pad> newPads;
    
    public void loadConfiguration(Reader config)
    	throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
    	loadConfiguration(config, true);
    }
    
    @SuppressWarnings("unchecked")
    public void loadConfiguration(Reader config, boolean doYourBest)
    	throws DbConfigException, FileNotFoundException, XMLException, TmcdbException {
    	
    	alma.tmcdb.generated.configuration.Configuration xmlCnf =
    		alma.tmcdb.generated.configuration.Configuration.unmarshalConfiguration(config);
        logger.info("Loading Configuration " + xmlCnf.getName());
        
        Configuration cnf = null;
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction trx = session.beginTransaction();        
        String query = "from Configuration where configurationname = '" + xmlCnf.getName() + "'";

        List<Configuration> configs = session.createQuery(query).list();
        if (configs.size() == 1) {
            cnf = (Configuration) configs.get(0);
        } else {
        	throw new TmcdbException("Configuration not found: " + xmlCnf.getName());
        }

        // Get the respective HwConfiguration given the Configuration ID. If none exists, create a new one
        HwConfiguration hwConf = null;
        Query q = session.createQuery("from HwConfiguration where swConfiguration = :conf");
        q.setParameter("conf", cnf, Hibernate.entity(Configuration.class));
        List<HwConfiguration> hwConfigs = q.list();
        if( hwConfigs.size() == 1) {
        	hwConf = (HwConfiguration)hwConfigs.get(0);
        } else {
        	logger.info("Creating new HW Configuration for configuration " + xmlCnf.getName());
        	hwConf = new HwConfiguration(cnf);
        }
        hwConf.setArrayReference(new ArrayReference(xmlCnf.getArrayReferenceX(),
                xmlCnf.getArrayReferenceY(), xmlCnf.getArrayReferenceZ()));
        hwConf.setTelescopeName(xmlCnf.getTelescopeName());

        newAntennas = new HashMap<String, Antenna>();
        AntennaT[] antennas = xmlCnf.getAntenna();
        for (int i = 0; i < antennas.length; i++) {
            AntennaT ant = antennas[i];
            alma.tmcdb.domain.Antenna antenna = addAntenna(hwConf, ant);
            newAntennas.put(antenna.getName(), antenna);
        }
        
        newLOs = new HashMap<String, CentralLO>();
        LocalOscillatorT[] los = xmlCnf.getLocalOscillator();
        for (int i = 0; i < los.length; i++) {
            LocalOscillatorT xmlLO = los[i];
            alma.tmcdb.domain.CentralLO lo =
                addLocalOscillator(hwConf, xmlLO);
            newLOs.put(lo.getName(), lo);
        }

        newAOSTiming = new HashMap<String, AOSTiming>();
        AOSTimingT[] aosts = xmlCnf.getAOSTiming();
        for (int i = 0; i < aosts.length; i++) {
            AOSTimingT xmlAOST = aosts[i];
            alma.tmcdb.domain.AOSTiming aost =
                addAOSTiming(hwConf, xmlAOST);
            newAOSTiming.put(aost.getName(), aost);
        }
        
        newFrontEnds = new HashMap<String, alma.tmcdb.domain.FrontEnd>();
        FrontEndT[] frontEnds = xmlCnf.getFrontEnd();
        for (int i = 0; i < frontEnds.length; i++) {
            FrontEndT iFrontEnd = frontEnds[i];
            FrontEnd frontEnd = addFrontEnd(hwConf, iFrontEnd);
            newFrontEnds.put(frontEnd.getName(), frontEnd);
        }

        newWeatherStations = new HashMap<String, alma.tmcdb.domain.WeatherStationController>();
        WeatherStationControllerT weatherStation = xmlCnf.getWeatherStationController();
        if(null != weatherStation)  
        {
            WeatherStationController ws = addWeatherStation(hwConf, weatherStation);
            newWeatherStations.put(ws.getName(), ws);
        }
        
        newPhotRefs = new HashMap<String, alma.tmcdb.domain.PhotonicReference>();
        PhotonicReferenceT[] photRefs = xmlCnf.getPhotonicReference();
        for (int i = 0; i < photRefs.length; i++) {
            PhotonicReferenceT iPhRef = photRefs[i];
            PhotonicReference phr = addPhotonicReference(hwConf, iPhRef);
            newPhotRefs.put(phr.getName(), phr);
        }
        
        newHolographyTowers = new HashMap<String, alma.tmcdb.domain.HolographyTower>();
        HolographyTowerT[] holoTws = xmlCnf.getHolographyTower();
        for (int i = 0; i < holoTws.length; i++) {
            HolographyTowerT iholoTw = holoTws[i];
            HolographyTower htw = addHolographyTower(hwConf, iholoTw);
            newHolographyTowers.put(htw.getName(), htw);
        }

        newPads = new HashMap<String, alma.tmcdb.domain.Pad>();
        PadT[] pads = xmlCnf.getPad();
        for (int i = 0; i < pads.length; i++) {
            PadT iPad = pads[i];
            alma.tmcdb.domain.Pad pad = addPad(hwConf, iPad);
            newPads.put(pad.getName(), pad);
        }

        session.save(hwConf);
        session.flush();
        
        // Associate antennas and pads
        Antenna2Pad[] xmlAntenna2Pads = xmlCnf.getArrayConfiguration().getAntenna2Pad();
        for (int i = 0; i < xmlAntenna2Pads.length; i++) {
            associateAntennaAndPad(xmlAntenna2Pads[i]);
        }
        
        // Associate holography towers and pads
        HolographyTower2Pad[] xmlHoloTw2Pads = xmlCnf.getHolographyTowerPadAssociation().getHolographyTower2Pad();
        for (int i = 0; i < xmlHoloTw2Pads.length; i++) {
            associateHolographyTowerToPad(xmlHoloTw2Pads[i]);
        }
        
        // Startup configurations
        StartupT[] xmlStartupConfigs = xmlCnf.getStartupConfiguration();
        for (int i = 0; i < xmlStartupConfigs.length; i++) {
            StartupT xmlStartup = xmlStartupConfigs[i];
            StartupScenario startup = addStartupConfiguration(hwConf, xmlStartup);

            session.flush();

            // Add Antenna Startup
            AntennaStartupT[] xmlAntennaStartups = xmlStartup.getAntenna();
            for (int j = 0; j < xmlAntennaStartups.length; j++) {
                AntennaStartupT xmlAntennaStartup = xmlAntennaStartups[j];
                BaseElementStartup bes = addAssembliesToAntenna(startup,
                                                                xmlAntennaStartup,
                                                                doYourBest,
                                                                doYourBest);

                // Add FrontEnd assemblies
                if (xmlAntennaStartup.getFrontEnd() != null)
                    addAssembliesToFrontEnd(bes, xmlAntennaStartup, hwConf, doYourBest, doYourBest);
            }

            // Add Central LO assemblies
            addAssembliesToLocalOscillator(startup, xmlStartup.getLocalOscillator(), doYourBest, doYourBest);
            
            // Add AOSTiming assemblies
            addAssembliesToAOSTiming(startup, xmlStartup.getAOSTiming(), doYourBest, doYourBest);
            
            addWeatherStationStartups(startup, xmlStartup.getWeatherStationController(), doYourBest, doYourBest);
        }
        
        // Add focus models
        for (FocusModelT fm : xmlCnf.getFocusModel()) {
            try {
                addFocusModel(fm);
            } catch (AcsJBadParameterEx e1) {
                e1.printStackTrace();
            }
        }

        // Add pointing model
        for (PointingModelT pm : xmlCnf.getPointingModel()) {
            try {
                addPointingModel(pm);
            } catch (AcsJBadParameterEx e1) {
                e1.printStackTrace();
            }
        }

        // Add cross polarization delays
        try {
            if(xmlCnf.getCrossPolarizationDelays() != null) {
               addXPDelays(hwConf, xmlCnf.getCrossPolarizationDelays());
            }
        } catch (AcsJBadParameterEx e) {
            e.printStackTrace();
        }
        session.saveOrUpdate(hwConf);
        
        // Add antenna delays
        AntennaDelaysT[] xmlAntDelays = xmlCnf.getAntennaDelays();
        for (int i = 0; i < xmlAntDelays.length; i++) {
            AntennaDelaysT xmlAntDelay = xmlAntDelays[i];
            try {
                addAntennaDelays(xmlAntDelay);
            } catch (AcsJBadParameterEx e) {
                e.printStackTrace();
            }
        }
        
        trx.commit();
        session.close();
        
    }

    private StartupScenario addStartupConfiguration(HwConfiguration config,
            StartupT xmlStartup) {
        StartupScenario startup = new StartupScenario(xmlStartup.getName());
        config.addStartupScenario(startup);
        session.save(config);
        return startup;
    }

    private BaseElementStartup addAssembliesToAntenna(StartupScenario startup,
            AntennaStartupT xmlAntennaStartup, boolean addMissingCompType, boolean fakeMissingLruFile)
    	throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
    	
        BaseElementStartup bes =
            new BaseElementStartup(newAntennas.get(xmlAntennaStartup.getName()), startup);
        bes.setSimulated(xmlAntennaStartup.getSimulated());
        
        // Add Assembly Startups
        AssemblyRoleT[] xmlAssemblyStartups = xmlAntennaStartup.getAssemblyRole();
        for (int i = 0; i < xmlAssemblyStartups.length; i++) {
            AssemblyRoleT xmlAssemblyRole = xmlAssemblyStartups[i];            
            AssemblyRole role = getAssemblyRole(xmlAssemblyRole.getType().toString(), addMissingCompType,
            		fakeMissingLruFile);
            AssemblyStartup assemblyStartup = new AssemblyStartup(bes, role);
            assemblyStartup.setSimulated(xmlAssemblyRole.getSimulated());
            bes.addAssemblyStartup(assemblyStartup);
        }
        return bes;
    }

    private void addAssembliesToFrontEnd(BaseElementStartup antennaStartup,
                                         AntennaStartupT xmlAntennaStartup,
                                         HwConfiguration config,
                                         boolean addMissingCompType,
                                         boolean fakeMissingLruFile)
    	throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
        
        // In this case construct a "generic" FrontEnd
        BaseElementStartup bes =
            new BaseElementStartup(BaseElementStartupType.FrontEnd);
        bes.setSimulated(xmlAntennaStartup.getSimulated());
        bes.setParent(antennaStartup);
        
        FrontEndStartupT xmlfe = xmlAntennaStartup.getFrontEnd();
        AssemblyRoleT[] xmlRoles = xmlfe.getAssemblyRole();
        for (int i = 0; i < xmlRoles.length; i++) {
            AssemblyRoleT xmlRole = xmlRoles[i];            
            AssemblyRole role = getAssemblyRole(xmlRole.getType().toString(),
            		addMissingCompType, fakeMissingLruFile);
            AssemblyStartup assemblyStartup = new AssemblyStartup(bes, role);
            assemblyStartup.setSimulated(xmlRole.getSimulated());
            bes.addAssemblyStartup(assemblyStartup);
        }
        antennaStartup.getChildren().add(bes);
    }

    private void addAssembliesToLocalOscillator(StartupScenario startup, LocalOscillatorStartupT xmllo,
    		boolean addMissingCompType, boolean fakeMissingLruFile) 
    	throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
        if (xmllo == null)
            return;
        BaseElementStartup bes =
            new BaseElementStartup(newLOs.get(xmllo.getName()), startup);
        bes.setSimulated(xmllo.getSimulated());
        
        AssemblyRoleT[] xmlRoles = xmllo.getAssemblyRole();
        for (int i = 0; i < xmlRoles.length; i++) {
            AssemblyRoleT xmlRole = xmlRoles[i];            
            AssemblyRole role = getAssemblyRole(xmlRole.getType().toString(), addMissingCompType, fakeMissingLruFile);
            AssemblyStartup assemblyStartup = new AssemblyStartup(bes, role);
            assemblyStartup.setSimulated(xmlRole.getSimulated());
            bes.addAssemblyStartup(assemblyStartup);
        }
        
        PhotonicReferenceStartupT[] prsts = xmllo.getPhotonicReference();
        for (int i = 0; i < prsts.length; i++) {
            PhotonicReferenceStartupT prst = prsts[i];
            
            // In this case construct a "generic" PhotRef BaseElementStartup
            BaseElementStartup prbes =
                new BaseElementStartup(BaseElementType.valueOf(BaseElementStartupType.class, prst.getName()));
            prbes.setSimulated(prst.getSimulated());
            
            AssemblyRoleT[] xmlPhotRefRoles = prst.getAssemblyRole();
            for (int j = 0; j < xmlPhotRefRoles.length; j++) {
                AssemblyRoleT xmlRole = xmlPhotRefRoles[j];            
                AssemblyRole role = getAssemblyRole(xmlRole.getType().toString(),
                        addMissingCompType, fakeMissingLruFile);
                AssemblyStartup assemblyStartup = new AssemblyStartup(prbes, role);
            assemblyStartup.setSimulated(xmlRole.getSimulated());
                prbes.addAssemblyStartup(assemblyStartup);
            }
            bes.getChildren().add(prbes); // necessary?
        }
    }

    private void addAssembliesToAOSTiming(StartupScenario startup, AOSTimingStartupT xmlaost,
            boolean addMissingCompType, boolean fakeMissingLruFile) 
        throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
        if (xmlaost == null)
            return;
        AOSTiming aosTiming = newAOSTiming.get(xmlaost.getName());
        if (aosTiming == null) {
            logger.warning("no AOSTiming defined under the name " + xmlaost.getName());
            return;
        }
        BaseElementStartup bes =
            new BaseElementStartup(aosTiming, startup);
        bes.setSimulated(xmlaost.getSimulated());
        
        AssemblyRoleT[] xmlRoles = xmlaost.getAssemblyRole();
        for (int i = 0; i < xmlRoles.length; i++) {
            AssemblyRoleT xmlRole = xmlRoles[i];            
            AssemblyRole role = getAssemblyRole(xmlRole.getType().toString(), addMissingCompType, fakeMissingLruFile);
            AssemblyStartup assemblyStartup = new AssemblyStartup(bes, role);
            assemblyStartup.setSimulated(xmlRole.getSimulated());
            bes.addAssemblyStartup(assemblyStartup);
        }
    }

    private void addWeatherStationStartups(StartupScenario startup, WeatherStationControllerStartupT xmlws,
            boolean addMissingCompType, boolean fakeMissingLruFile)
        throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
        if (xmlws == null)
            return;
        WeatherStationController ws = newWeatherStations.get(xmlws.getName());
        if (ws == null) {
           logger.warning("no WeatherStationController defined under the name " + xmlws.getName());
           return;
        }
        BaseElementStartup bes = new BaseElementStartup(ws, startup);
        bes.setSimulated(xmlws.getSimulated());
        
        AssemblyRoleT[] xmlRoles = xmlws.getAssemblyRole();
        for (int i = 0; i < xmlRoles.length; i++) {
            AssemblyRoleT xmlRole = xmlRoles[i];            
            AssemblyRole role = getAssemblyRole(xmlRole.getType().toString(), addMissingCompType, fakeMissingLruFile);
            AssemblyStartup assemblyStartup = new AssemblyStartup(bes, role);
            assemblyStartup.setSimulated(xmlRole.getSimulated());
            bes.addAssemblyStartup(assemblyStartup);
        }
    }
    
    private Antenna addAntenna(HwConfiguration config, AntennaT xmlAnt) {
        Antenna antenna =
            new Antenna(xmlAnt.getName(),
                        AntennaType.valueOf(xmlAnt.getType().toString()),
                        new Coordinate(xmlAnt.getXPosition(),
                                       xmlAnt.getYPosition(),
                                       xmlAnt.getZPosition()),
                        new Coordinate(xmlAnt.getXOffset(),
                                       xmlAnt.getYOffset(),
                                       xmlAnt.getZOffset()),
                        xmlAnt.getDishDiameter(),
                        xmlAnt.getCommissionDate(),
                        xmlAnt.getLoOffsettingIdx(),
                        xmlAnt.getWalshSeq());
//        if (xmlAnt.hasAvgDelay())
        antenna.setAvgDelay(xmlAnt.getAvgDelay());
        config.addBaseElement(antenna);
        return antenna;
    }

    private Pad addPad(HwConfiguration config, PadT xmlPad) {
        Date cd = xmlPad.getCommissionDate() == null ? new Date() : xmlPad.getCommissionDate();        
        Pad pad = new Pad(xmlPad.getName(),
                          new Coordinate(xmlPad.getXPosition(),
                                         xmlPad.getYPosition(),
                                         xmlPad.getZPosition()),
                          cd);
        if (xmlPad.hasAvgDelay())
            pad.setAvgDelay(xmlPad.getAvgDelay());
        config.addBaseElement(pad);
        return pad;
    }

    private FrontEnd addFrontEnd(HwConfiguration config, FrontEndT xmlFrontEnd) {
        FrontEnd frontEnd = new FrontEnd(xmlFrontEnd.getName(), 0L);
        config.addBaseElement(frontEnd);
        return frontEnd;
    }

    private WeatherStationController addWeatherStation(HwConfiguration config, WeatherStationControllerT xmlWeatherStation) {
        WeatherStationController ws = new WeatherStationController(xmlWeatherStation.getName(), 0L);
        config.addBaseElement(ws);
        return ws;
    }

    private PhotonicReference addPhotonicReference(HwConfiguration config, PhotonicReferenceT xmlPhotRef) {
        PhotonicReference phr = new PhotonicReference(xmlPhotRef.getName(), 0L);
        config.addBaseElement(phr);
        return phr;
    }

    private HolographyTower addHolographyTower(HwConfiguration config, HolographyTowerT xmlHoloTw) {
        Coordinate pos = new Coordinate(xmlHoloTw.getXPosition(), xmlHoloTw.getYPosition(),
                xmlHoloTw.getZPosition());
        HolographyTower htw = new HolographyTower(xmlHoloTw.getName(), pos, 0L);
        config.addBaseElement(htw);
        return htw;
    }
    
    private CentralLO addLocalOscillator(
            HwConfiguration config,
            LocalOscillatorT xmlLO) {
        CentralLO lo =
            new CentralLO(xmlLO.getName(), 0L);
        config.addBaseElement(lo);
        return lo;
    }
    
    private AOSTiming addAOSTiming(HwConfiguration config,
            AOSTimingT xmlAosTiming) {
        AOSTiming aost = new AOSTiming(xmlAosTiming.getName(), 0L);
        config.addBaseElement(aost);
        return aost;
    }
    
    private AntennaToPad associateAntennaAndPad(Antenna2Pad xmla2p) {
        AntennaToPad a2p = new AntennaToPad(
                newAntennas.get(xmla2p.getAntenna()),
                newPads.get(xmla2p.getPad()),
                xmla2p.getStartTime(), 
                xmla2p.getEndTime(),
                true);
        a2p.setMountMetrologyAN0Coeff(xmla2p.getAn0());
        a2p.setMountMetrologyAW0Coeff(xmla2p.getAw0());
        session.save(a2p);
        return a2p;
    }
    
    private HolographyTowerToPad associateHolographyTowerToPad(HolographyTower2Pad xmlHT2P) {
        HolographyTowerToPad ht2p = new HolographyTowerToPad(
                newHolographyTowers.get(xmlHT2P.getHolographyTower()),
                newPads.get(xmlHT2P.getPad()));
        ht2p.setAzimuth(xmlHT2P.getAzimuth());
        ht2p.setElevation(xmlHT2P.getElevation());
        session.save(ht2p);
        return ht2p;
    }

    private AssemblyRole getAssemblyRole(String assemblyRole, boolean addMissingCompType,
    		boolean fakeMissingLruFile)
        throws FileNotFoundException, XMLException, DbConfigException, TmcdbException {
    	
        String query;
        logger.fine("Querying for assembly role " + assemblyRole);
        query = "from AssemblyRole where name = '" + assemblyRole + "'";
        System.out.println("query = " + query);
        AssemblyRole role = (AssemblyRole) session.createQuery(query).uniqueResult();
        if (role == null) {
            AssemblyRoleLoader.loadAssemblyRole(session, assemblyRole, addMissingCompType, fakeMissingLruFile);
            role = (AssemblyRole) session.createQuery(query).uniqueResult();
        }
        return role;
    }

    private void addPointingModel(PointingModelT xmlPointingModel) throws AcsJBadParameterEx {
        alma.tmcdb.domain.PointingModel pointingModel = new alma.tmcdb.domain.PointingModel(
                newAntennas.get(xmlPointingModel.getAntenna()));
        CoeffT[] iTerms = xmlPointingModel.getCoeff();
        for (int i = 0; i < iTerms.length; i++) {
            CoeffT iTerm = iTerms[i];
            float value = (float) iTerm.getValue();
            PointingModelCoeff coeff = new PointingModelCoeff(iTerm.getName(), value);
            pointingModel.getTerms().put(iTerm.getName(), coeff);
            OffsetT[] xmlOffsets = iTerm.getOffset();
            for (int j = 0; j < xmlOffsets.length; j++) {
                OffsetT xmlOffset = xmlOffsets[j];
                coeff.getOffsets().put(JReceiverBand.literal(xmlOffset.getReceiverBand().toString()), xmlOffset.getValue());
            }
        }
        session.save(pointingModel);
    }

    private void addFocusModel(FocusModelT xmlFocusModel) throws AcsJBadParameterEx {
        alma.tmcdb.domain.FocusModel focusModel = new alma.tmcdb.domain.FocusModel(
                newAntennas.get(xmlFocusModel.getAntenna()));
        CoeffT[] iTerms = xmlFocusModel.getCoeff();
        for (int i = 0; i < iTerms.length; i++) {
            CoeffT iTerm = iTerms[i];
            float value = (float) iTerm.getValue();
            FocusModelCoeff coeff = new FocusModelCoeff(value);
            focusModel.getTerms().put(iTerm.getName(), coeff);
            OffsetT[] xmlOffsets = iTerm.getOffset();
            for (int j = 0; j < xmlOffsets.length; j++) {
                OffsetT xmlOffset = xmlOffsets[j];
                coeff.getOffsets().put(JReceiverBand.literal(xmlOffset.getReceiverBand().toString()),
                        xmlOffset.getValue());
            }
        }
        session.save(focusModel);
    }
    
    private void addXPDelays(HwConfiguration hwConfig, CrossPolarizationDelaysT xmlXpDelays)
        throws AcsJBadParameterEx {
        XPDelayT[] xmlds = xmlXpDelays.getXPDelay();
        for (int i = 0; i < xmlds.length; i++) {
            XPDelayT xmld = xmlds[i];
            XPDelay xpdelay = new XPDelay(JReceiverBand.literal(xmld.getReceiverBand().toString()),
                    JBasebandName.literal(xmld.getBaseband().toString()),
                    JNetSideband.literal(xmld.getSideband().toString()),
                    xmld.getValue(), hwConfig);
            hwConfig.getCrossPolarizationDelays().add(xpdelay);
        }
    }
    
    private void addAntennaDelays(AntennaDelaysT xmlAntDelays) throws AcsJBadParameterEx {
        Antenna antenna = newAntennas.get(xmlAntDelays.getAntenna());
        for (FEDelayT xmlDelay : xmlAntDelays.getFEDelay()) {
            FEDelay delay = new FEDelay(JReceiverBand.literal(xmlDelay.getReceiverBand().toString()),
                    JPolarizationType.literal(xmlDelay.getPolarization().toString()),
                    JNetSideband.literal(xmlDelay.getSideband().toString()),
                    xmlDelay.getValue());
            antenna.getFrontEndDelays().add(delay);
        }
        for (IFDelayT xmlDelay : xmlAntDelays.getIFDelay()) {
            IFDelay delay = new IFDelay(JBasebandName.literal(xmlDelay.getBaseband().toString()),
                    JPolarizationType.literal(xmlDelay.getPolarization().toString()),
                    IFProcConnectionState.valueOf(xmlDelay.getIfswitch().toString()),
                    xmlDelay.getValue());
            antenna.getIfDelays().add(delay);
        }
        for (LODelayT xmlDelay : xmlAntDelays.getLODelay()) {
            LODelay lodelay = new LODelay(JBasebandName.literal(xmlDelay.getBaseband().toString()),
                    xmlDelay.getValue());
            antenna.getLoDelays().add(lodelay);            
        }
        session.saveOrUpdate(antenna);
    }
        
    public static void main(String[] args) {

        ConfigurationLoader loader = new ConfigurationLoader();
        try {
            loader.loadConfiguration(new FileReader(args[0]));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (DbConfigException ex) {
            ex.printStackTrace();
        } catch (XMLException ex) {
			ex.printStackTrace();
		} catch (TmcdbException ex) {
			ex.printStackTrace();
		}   
    }    
}
