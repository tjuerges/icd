/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.generated.configuration.CoeffT;
import alma.tmcdb.generated.configuration.OffsetT;
import alma.tmcdb.generated.configuration.PointingModelT;
import alma.tmcdb.generated.configuration.PointingModels;
import alma.tmcdb.generated.configuration.types.ReceiverBandEnumT;

class PointingModelExporter {

    private Logger logger =
        TmcdbLoggerFactory.getLogger("alma.tmcdb.utils.PointingModelExporter");
    
    private Session session;
    
    public PointingModels exportPointingModels(String configuration) throws TmcdbException {
        
        Configuration cnf = null;
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction trx = session.beginTransaction();
        String query = "from Configuration where configurationname = '" + configuration + "'";
        List<Configuration> configs = session.createQuery(query).list();
        if (configs.size() == 1) {
            cnf = (Configuration) configs.get(0);
        } else {
            throw new TmcdbException("Configuration not found: " + configuration);
        }

        // Get the respective HwConfiguration given the Configuration ID. If none exists, create a new one
        HwConfiguration hwConf = null;
        Query q = session.createQuery("from HwConfiguration where swConfiguration = :conf");
        q.setParameter("conf", cnf, Hibernate.entity(Configuration.class));
        List<HwConfiguration> hwConfigs = q.list();
        if( hwConfigs.size() == 1) {
            hwConf = (HwConfiguration)hwConfigs.get(0);
        } else {
            throw new TmcdbException("HWConfiguration not found for Configuration: " + configuration);
        }        
        
        PointingModels xmlPointingModels = new PointingModels();
        
        for (BaseElement be : hwConf.getBaseElements()) {
            if (be instanceof Antenna) {
                Antenna a = (Antenna) be;
                logger.info(a.getName());
                Set<PointingModel> pms = a.getPointingModels();
                Iterator<PointingModel> it = pms.iterator();
                if (it.hasNext()) {
                    PointingModelT xmlPointingModel = new PointingModelT();
                    xmlPointingModel.setAntenna(a.getName());
                    PointingModel pm = it.next();
                    Map<String, PointingModelCoeff> coeffs = pm.getTerms();
                    for (String coeffName : coeffs.keySet()) {
                        CoeffT xmlCoeff = new CoeffT();
                        PointingModelCoeff coeff = coeffs.get(coeffName);
                        xmlCoeff.setName(coeffName);
                        xmlCoeff.setValue(coeff.getValue());
                        Map<ReceiverBand, Double>  offsets = coeff.getOffsets();
                        for (ReceiverBand band : offsets.keySet()) {
                            Double o = offsets.get(band);
                            OffsetT xmlOffset = new OffsetT();
                            xmlOffset.setReceiverBand(ReceiverBandEnumT.valueOf(band.toString()));
                            xmlOffset.setValue(o);
                            xmlCoeff.addOffset(xmlOffset);
                        }
                        xmlPointingModel.addCoeff(xmlCoeff);
                    }
                    xmlPointingModels.addPointingModel(xmlPointingModel);
                }
            }
        }
        
        trx.commit();
        session.close();
        
        return xmlPointingModels;
    }
    
    public static void main(String[] args) {
        String configuration = args[0];
        PointingModelExporter exporter = new PointingModelExporter();
        try {
            PointingModels pms = exporter.exportPointingModels(configuration);
            FileWriter out = new FileWriter(args[1]);
            pms.marshal(out);
            out.close();
        } catch (TmcdbException ex) {
            ex.printStackTrace();
        } catch (MarshalException ex) {
            ex.printStackTrace();
        } catch (ValidationException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
