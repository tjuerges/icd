/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AssemblyDataLoader.java,v 1.18 2011/03/04 21:21:29 sharring Exp $"
 */
package alma.tmcdb.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

import org.exolab.castor.xml.XMLException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.Assembly;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.HwSchema;
import alma.tmcdb.generated.assemblydata.AssemblyDataT;
import alma.tmcdb.generated.assemblydata.Catalog;
import alma.tmcdb.generated.assemblydata.ComponentDataT;

/**
 * Utility to load Assembly specific data into the TMCDB database.
 * 
 * @author rhiriart
 */
public class AssemblyDataLoader {

    private static Logger logger =
    	TmcdbLoggerFactory.getLogger(AssemblyDataLoader.class.getName());
    
    /**
     * Loads the Assembly data from the $ACS_CDB/TMCDB_DATA directory into
     * the TMCDB database.
     * 
     * It reads $ACS_CDB/TMCDB_DATA/Catalog.xml file, and based on its entries,
     * it updates or creates the proper records in the TMCDB. Assembly data
     * is stored in two tables; the XML Schema in the SCHEMA table and the
     * XML document in the ASSEMBLY table.
     * 
     * @param updateCompTypeURN
     *        Updates the COMPONENTTYPE.URN column for each one of the
     *        Assemblies. This is used when creating an in-memory TMCDB
     *        from XML files, as the TMCDBComponent does.
     * 
     * @throws XMLException
     *         In case of problems reading Catalog.xml.
     * @throws IOException
     *         In case of problems accessing files in the
     *         $ACS_CDB/TMCDB_DATA directory.
     * @throws TmcdbException
     *         In case of missing records in the CONFIGURATION and ASSEMBLYTYPE
     *         tables.
     * @throws DbConfigException 
     */
    public static void loadAssemblyData(boolean updateCompTypeURN)
        throws XMLException, IOException, TmcdbException, DbConfigException {
    	
        String cdbDir = System.getenv("ACS_CDB");
        String dataDir = cdbDir + "/TMCDB_DATA/";
        
        // Check that the TMCDB_DATA directory exists
        File dir = new File(dataDir);
        if (! dir.exists() ) {
        	logger.warning("Directory doesn't exist: " + dataDir);
        	return;
        }

        File cat = new File( dataDir + "Catalog.xml" );
        if (!cat.exists()) return;
        FileReader catfr = new FileReader( dataDir + "Catalog.xml" );
        Catalog catalog = Catalog.unmarshalCatalog(catfr);
        
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        Session session;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        
        String qstr = "FROM Configuration WHERE configurationname = '" +
            catalog.getConfiguration() + "'";
        Configuration configuration =
            (Configuration) session.createQuery(qstr).uniqueResult();
        if (configuration == null) {
            throw new TmcdbException("Configuration not found in TMCDB: " +
                                     catalog.getConfiguration());
        }

        qstr = "FROM HwConfiguration WHERE swconfigurationid = " + configuration.getConfigurationId();
        HwConfiguration hwConfiguration = (HwConfiguration)session.createQuery(qstr).uniqueResult();
        if ( hwConfiguration == null) {
            throw new TmcdbException("HwConfiguration not found in TMCDB: " +
                                     catalog.getConfiguration());
        }

        AssemblyDataT[] assemblyData = catalog.getAssemblyData();
        for (int i = 0; i < assemblyData.length; i++) {
            // Read the Schema file
            
            logger.fine( "XSD file: " + assemblyData[i].getXSD() );
            FileReader fr = new FileReader( dataDir + assemblyData[i].getXSD() );
            BufferedReader br = new BufferedReader(fr);
            String xsd = "";
            String line;
            while ( (line = br.readLine()) != null ) {
                xsd += line + '\n';
            }
            
            // Store the Schema in the database
            qstr = "FROM HwSchema schema " +
            		"WHERE schema.urn = :urn " +
            		"AND schema.configuration = :configuration";
            Query query = session.createQuery(qstr);
            query.setParameter("urn",
                               assemblyData[i].getURN(),
                               Hibernate.STRING);
            query.setParameter("configuration",
                               hwConfiguration,
                               Hibernate.entity(HwConfiguration.class));
            HwSchema schema = (HwSchema) query.uniqueResult();
            if (schema == null) {
                schema = new HwSchema();
                schema.setUrn(assemblyData[i].getURN());
                schema.setSchema(xsd);
                logger.fine("HwSchema '" + schema.getUrn() + "' was missing, now created");
            }
            
            // Read the XML file
            fr = new FileReader( dataDir + assemblyData[i].getXML() );
            br = new BufferedReader(fr);
            String xml = "";
            line = null;
            while ( (line = br.readLine()) != null ) {
                xml += line + '\n';
            }
            
            // Store the XML file in the database
            
            // First get the AssemblyType and connect the Schema to it
            qstr = "FROM AssemblyType WHERE name = '" +
                   assemblyData[i].getAssemblyType() + "'";
            AssemblyType assemblyType = (AssemblyType)
                session.createQuery(qstr).uniqueResult();
            if (assemblyType == null) {
                String lruConfFilePath = LruLoader.findTmcdbHwConfigFile(assemblyData[i].getAssemblyType());
                FileReader reader = new FileReader(lruConfFilePath);
                LruLoader.loadLruType(session, reader, true); // TODO expose last parameter
                assemblyType = (AssemblyType)
                    session.createQuery(qstr).uniqueResult();
                logger.fine("AssemblyType and LruType '" + assemblyType.getName() + "' were missing, created now");
                session.saveOrUpdate(assemblyType);
            }
            schema.setAssemblyType(assemblyType);
            hwConfiguration.addHwSchema(schema);
            session.saveOrUpdate(hwConfiguration); // this cascades to "schema"

            // TODO: Rafael, maybe you can decide better on this. URN has been moved
            //       from ComponentType to Component, so the following code won't compile anymore
//            if (updateCompTypeURN) {
//                ComponentType ct = assemblyType.getComponentType();
//                ct.setURN(assemblyData[i].getURN());
//                session.save(ct);
//                logger.fine("Updated ComponentType '" + ct.getIDL() + "' with URN '" + ct.getURN() + "'");
//            }

            String serialNumber = assemblyData[i]
                                  .getSerialNumber()
                                  .replaceAll(".xml$", "");
            qstr = "FROM Assembly assembly " +
                   "WHERE assembly.serialNumber = :serialNumber " +
                   "AND assembly.configuration = :configuration";
            query = session.createQuery(qstr);
            query.setParameter("serialNumber",
                               serialNumber,
                               Hibernate.STRING);
            query.setParameter("configuration",
                               hwConfiguration,
                               Hibernate.entity(HwConfiguration.class));
            Assembly assembly = (Assembly) query.uniqueResult();
            boolean created = false;
            if (assembly == null) {
                assembly = new Assembly(serialNumber,
                                        xml,
                                        assemblyType);
                hwConfiguration.addAssembly(assembly);
                created = true;
            } else {
                assembly.setAssemblyType(assemblyType);
                assembly.setData(xml);
            }
            session.saveOrUpdate(assembly);
            logger.info("Assembly " + assembly.getSerialNumber() + " has been " + (created ? "created" : "updated"));
        }
        
        // Now store the Control Component CDB extra data
        ComponentDataT[] compsData = catalog.getComponentData();
        for (ComponentDataT compData : compsData) {
        	
            logger.fine( "Component XML file: " + compData.getXML() );
            FileReader fr = new FileReader( dataDir + compData.getXML() );
            BufferedReader br = new BufferedReader(fr);
            String xml = "";
            String line;
            while ( (line = br.readLine()) != null ) {
                xml += line + '\n';
            }

        	String cn = compData.getComponentName();
        	String path = cn.substring(0, cn.lastIndexOf("/"));
        	String name = cn.substring(cn.lastIndexOf("/")+1, cn.length());
        	
        	qstr = "FROM Component component where component.componentName = :name AND " +
        			"component.path = :path and component.configuration = :conf";
        	Query query = session.createQuery(qstr);
        	query.setParameter("name", name, Hibernate.STRING);
        	query.setParameter("path", path, Hibernate.STRING);
            query.setParameter("conf", configuration, Hibernate.entity(Configuration.class));
        	Component component = (Component) query.uniqueResult();
        	if (component != null) {
        		component.setXMLDoc(xml);
        		session.saveOrUpdate(component);
        	}
        }
        
        tx.commit();
        session.close();
    }
    
    /**
     * Loads the Assembly data from the $ACS_CDB/TMCDB_DATA directory into
     * the TMCDB database.
     */
    public static void main(String[] args) {
        try {
            loadAssemblyData(false);
        } catch (XMLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (TmcdbException ex) {
            ex.printStackTrace();
        } catch (DbConfigException ex) {
            ex.printStackTrace();
        }
    }
}
