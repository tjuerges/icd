package alma.tmcdb.utils;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class TmcdbLogFormatter extends Formatter {

	@Override
	public String format(LogRecord record) {
		String log = record.getSourceClassName();
		log += ":" + record.getSourceMethodName();
		log += ": " + record.getMessage();
		return log;
	}

}
