/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.acs.tmcdb.Configuration;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.hla.datamodel.enumeration.JBasebandName;
import alma.hla.datamodel.enumeration.JNetSideband;
import alma.hla.datamodel.enumeration.JPolarizationType;
import alma.hla.datamodel.enumeration.JReceiverBand;
import alma.hla.datamodel.enumeration.JSidebandProcessingMode;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.IFProcConnectionState;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.XPDelay;
import alma.tmcdb.generated.configuration.AntennaDelaysT;
import alma.tmcdb.generated.configuration.CrossPolarizationDelaysT;
import alma.tmcdb.generated.configuration.DelayModels;
import alma.tmcdb.generated.configuration.FEDelayT;
import alma.tmcdb.generated.configuration.IFDelayT;
import alma.tmcdb.generated.configuration.LODelayT;
import alma.tmcdb.generated.configuration.XPDelayT;

public class DelayModelImporter {

    private Logger logger =
        TmcdbLoggerFactory.getLogger("alma.tmcdb.utils.DelayModelImporter");
    private Session session;
    
    public static void addDelayModelToAntenna(Antenna antenna, Reader file)
        throws MarshalException, ValidationException, AcsJBadParameterEx {
        alma.tmcdb.generated.configuration.AntennaDelaysT xmlDM =
            alma.tmcdb.generated.configuration.AntennaDelaysT.unmarshalAntennaDelaysT(file);
        addDelayModelToAntenna(antenna, xmlDM);
    }
    
    public static void addCrossPolarizationDelays(HwConfiguration hwConf,
            CrossPolarizationDelaysT xmlXPDelays) throws AcsJBadParameterEx {
        Set<XPDelay> newXPDs = new HashSet<XPDelay>();
        for (XPDelayT xmlXPD : xmlXPDelays.getXPDelay()) {
            boolean found = false;
            for (XPDelay xpd : hwConf.getCrossPolarizationDelays()) {
                if (( xpd.getBaseband().toString().equals(xmlXPD.getBaseband().toString())) &&
                    ( xpd.getReceiverBand().toString().equals(xmlXPD.getReceiverBand().toString())) &&
                    ( xpd.getSideband().toString().equals(xmlXPD.getSideband().toString()) )) {
                    xpd.setDelay(xmlXPD.getValue());
                    found = true;
                    break;
                }
            }
            if (!found) {
                XPDelay newXPD = new XPDelay();
                newXPD.setBaseband(JBasebandName.literal(xmlXPD.getBaseband().toString()));
                newXPD.setReceiverBand(JReceiverBand.literal(xmlXPD.getReceiverBand().toString()));
                newXPD.setSideband(JNetSideband.literal(xmlXPD.getSideband().toString()));
                newXPD.setDelay(xmlXPD.getValue());
                newXPD.setConfiguration(hwConf);
                newXPDs.add(newXPD);
            }
        }
        for (XPDelay xpd : newXPDs) {
            hwConf.getCrossPolarizationDelays().add(xpd);
        }
    }
    
    public static void addDelayModelToAntenna(Antenna antenna, AntennaDelaysT xmlDM)
        throws MarshalException, ValidationException, AcsJBadParameterEx {

        if (!antenna.getName().equals(xmlDM.getAntenna())) {
        	AcsJBadParameterEx ex = new AcsJBadParameterEx();
        	String msg = "Invalid antenna: XML file contained " + xmlDM.getAntenna() +
        	              " but you are using " + antenna.getName();
        	ex.setReason(msg);
        	throw ex;
        }

        // For each one of the XML FE Delays, first look if the delay already
        // exists in the Hibernate object. If it does, modify it; if it doesn't,
        // create it.        
        Set<FEDelay> newFEDs = new HashSet<FEDelay>();
        for (FEDelayT xmlFED : xmlDM.getFEDelay()) {
            boolean found = false;
            for (FEDelay fed : antenna.getFrontEndDelays()) {
                if (( fed.getPolarization().toString().equals(xmlFED.getPolarization().toString()) ) &&
                    ( fed.getReceiverBand().toString().equals(xmlFED.getReceiverBand().toString()) ) &&
                    ( fed.getSideband().toString().equals(xmlFED.getSideband().toString()))) {
                    fed.setDelay(xmlFED.getValue());
                    found = true;
                    break;
                }
            }
            if (!found) {
                FEDelay newFED = new FEDelay();
                newFED.setPolarization(JPolarizationType.literal(xmlFED.getPolarization().toString()));
                newFED.setReceiverBand(JReceiverBand.literal(xmlFED.getReceiverBand().toString()));
                newFED.setSideband(JNetSideband.literal(xmlFED.getSideband().toString()));
                newFED.setDelay(xmlFED.getValue());
                newFEDs.add(newFED);
            }
        }
        for (FEDelay fed : newFEDs) {
            antenna.getFrontEndDelays().add(fed);
        }

        // For each one of the XML IF Delays, first look if the delay already
        // exists in the Hibernate object. If it does, modify it; if it doesn't,
        // create it.        
        Set<IFDelay> newIFDs = new HashSet<IFDelay>();
        for (IFDelayT xmlIFD : xmlDM.getIFDelay()) {
            boolean found = false;
            for (IFDelay ifd : antenna.getIfDelays()) {
                if (( ifd.getPolarization().toString().equals(xmlIFD.getPolarization().toString()) ) &&
                    ( ifd.getBaseband().toString().equals(xmlIFD.getBaseband().toString()) ) &&
                    ( ifd.getIfSwitch().toString().equals(xmlIFD.getIfswitch().toString()))) {
                    ifd.setDelay(xmlIFD.getValue());
                    found = true;
                    break;
                }
            }
            if (!found) {
                IFDelay newIFD = new IFDelay();
                newIFD.setPolarization(JPolarizationType.literal(xmlIFD.getPolarization().toString()));
                newIFD.setBaseband(JBasebandName.literal(xmlIFD.getBaseband().toString()));
                newIFD.setIfSwitch(IFProcConnectionState.valueOf(xmlIFD.getIfswitch().toString()));
                newIFD.setDelay(xmlIFD.getValue());
                newIFDs.add(newIFD);
            }
        }
        for (IFDelay ifd : newIFDs) {
            antenna.getIfDelays().add(ifd);
        }
        
        // For each one of the XML LO Delays, first look if the delay already
        // exists in the Hibernate object. If it does, modify it; if it doesn't,
        // create it.        
        Set<LODelay> newLODs = new HashSet<LODelay>();
        for (LODelayT xmlLOD : xmlDM.getLODelay()) {
            boolean found = false;
            for (LODelay lod : antenna.getLoDelays()) {
                if ( lod.getBaseband().toString().equals(xmlLOD.getBaseband().toString()) )  {
                    lod.setDelay(xmlLOD.getValue());
                    found = true;
                    break;
                }
            }
            if (!found) {
                LODelay newLOD = new LODelay();
                newLOD.setBaseband(JBasebandName.literal(xmlLOD.getBaseband().toString()));
                newLOD.setDelay(xmlLOD.getValue());
                newLODs.add(newLOD);
            }
        }
        for (LODelay lod : newLODs) {
            antenna.getLoDelays().add(lod);
        }
    }
    
    public void importDelayModels(String configuration, DelayModels delayModels)
        throws TmcdbException, MarshalException, ValidationException, AcsJBadParameterEx {
    
        Configuration cnf = null;
        TmcdbDbConfig dbconf = null;
        try {
            dbconf = new TmcdbDbConfig(logger);
        } catch (Exception ex) { 
            logger.warning("Cannot create TmcdbDbConfig"); 
            ex.printStackTrace();
        }
        HibernateUtil.createConfigurationFromDbConfig(dbconf);
        session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction trx = session.beginTransaction();
        String query = "from Configuration where configurationname = '" + configuration + "'";
        List<Configuration> configs = session.createQuery(query).list();
        if (configs.size() == 1) {
            cnf = (Configuration) configs.get(0);
        } else {
            throw new TmcdbException("Configuration not found: " + configuration);
        }
    
        // Get the respective HwConfiguration given the Configuration ID. If none exists, throw an exception.
        HwConfiguration hwConf = null;
        Query q = session.createQuery("from HwConfiguration where swConfiguration = :conf");
        q.setParameter("conf", cnf, Hibernate.entity(Configuration.class));
        List<HwConfiguration> hwConfigs = q.list();
        if( hwConfigs.size() == 1) {
            hwConf = (HwConfiguration)hwConfigs.get(0);
        } else {
            throw new TmcdbException("HWConfiguration not found for Configuration: " + configuration);
        }

        Set<BaseElement> baseElements = hwConf.getBaseElements();
        for (AntennaDelaysT xmlfm : delayModels.getAntennaDelays()) {
            String antennaName = xmlfm.getAntenna();
            if (antennaName == null)
                throw new NullPointerException("Antenna name is null");
            for (Iterator<BaseElement> iter = baseElements.iterator(); iter.hasNext();) {
                BaseElement be = iter.next();
                if (be.getName().equals(antennaName) && (be instanceof Antenna)) {
                    Antenna a = (Antenna) be;                    
                    // create and add the delay model
                    addDelayModelToAntenna(a, xmlfm);
                    session.saveOrUpdate(a);
                }
            }
        }
        
        addCrossPolarizationDelays(hwConf, delayModels.getXPDelays());
        session.saveOrUpdate(hwConf);
        
        trx.commit();
        session.close();
    }
    
    public static void main(String[] args) {
        String configuration = args[0];
        String fileName = args[1];
        try {
            FileReader reader = new FileReader(fileName);
            DelayModels dms = DelayModels.unmarshalDelayModels(reader);
            DelayModelImporter importer = new DelayModelImporter();
            importer.importDelayModels(configuration, dms);
        } catch (MarshalException ex) {
            ex.printStackTrace();
        } catch (ValidationException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (AcsJBadParameterEx ex) {
            ex.printStackTrace();
        } catch (TmcdbException ex) {
            ex.printStackTrace();
        }
    }
}
