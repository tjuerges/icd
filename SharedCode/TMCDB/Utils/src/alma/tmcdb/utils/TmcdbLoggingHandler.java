package alma.tmcdb.utils;

import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class TmcdbLoggingHandler extends Handler {
	
	private static Logger logger = null;

	public static void setLogger(Logger newLogger) {
		logger = newLogger;
	}
	
	public TmcdbLoggingHandler() {
		if (logger == null)
			logger = Logger.getLogger("TmcdbLoggingHandler");		
	}

	@Override
	public void close() throws SecurityException {}

	@Override
	public void flush() {}

	@Override
	public void publish(LogRecord record) {
		logger.log(record);
	}
	
}
