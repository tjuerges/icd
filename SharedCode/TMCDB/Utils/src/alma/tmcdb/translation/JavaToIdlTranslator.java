/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: JavaToIdlTranslator.java,v 1.11 2011/05/02 08:42:42 rtobar Exp $"
 */
package alma.tmcdb.translation;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alma.ReceiverBandMod.ReceiverBand;
import alma.TMCDB.BandOffsets;
import alma.TMCDB.IFProcConnectionState;
import alma.TMCDB.ModelTerm;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.asdmIDLTypes.IDLArrayTime;
import alma.asdmIDLTypes.IDLLength;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.FocusModel;
import alma.tmcdb.domain.FocusModelCoeff;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.domain.XPDelay;

/**
 * Translates TMCDB Persistence Java classes to its IDL representation.
 * 
 * @author rhiriart
 */
public class JavaToIdlTranslator {

    private static Map<String, Short> bands = new HashMap<String, Short>();
    private static Map<String, IFProcConnectionState> IFProcConnStates =
        new HashMap<String, IFProcConnectionState>();
    static {
        bands.put("ALMA_RB_01", new Short((short)1));
        bands.put("ALMA_RB_02", new Short((short)2));
        bands.put("ALMA_RB_03", new Short((short)3));
        bands.put("ALMA_RB_04", new Short((short)4));
        bands.put("ALMA_RB_05", new Short((short)5));
        bands.put("ALMA_RB_06", new Short((short)6));
        bands.put("ALMA_RB_07", new Short((short)7));
        bands.put("ALMA_RB_08", new Short((short)8));
        bands.put("ALMA_RB_09", new Short((short)9));
        bands.put("ALMA_RB_10", new Short((short)10));
        
        IFProcConnStates.put("USB_HIGH", IFProcConnectionState.USB_HIGH);
        IFProcConnStates.put("USB_LOW", IFProcConnectionState.USB_LOW);
        IFProcConnStates.put("LSB_HIGH", IFProcConnectionState.LSB_HIGH);
        IFProcConnStates.put("LSB_LOW", IFProcConnectionState.LSB_LOW);
    }
    
    public static AntennaIDL toIDL(Antenna antenna) {
        if (antenna == null)
            throw new NullPointerException("Antenna is null");
        AntennaIDL antidl =
            new AntennaIDL(0,
                           antenna.getName(),
                           antenna.getAntennaType().toString(),
                           new IDLLength(antenna.getDiameter()),
                           new IDLArrayTime(antenna.getCommissionDate()),
                           new IDLLength(antenna.getPosition().getX()),
                           new IDLLength(antenna.getPosition().getY()), 
                           new IDLLength(antenna.getPosition().getZ()),
                           new IDLLength(antenna.getOffset().getX()),
                           new IDLLength(antenna.getOffset().getY()),
                           new IDLLength(antenna.getOffset().getZ()),
                           0);
        return antidl;
    }
    
    public static PadIDL toIDL(Pad pad) {
        if (pad == null)
            throw new NullPointerException("Pad is null");
        PadIDL padidl = new PadIDL(0,
                                   pad.getName(),
                                   new IDLArrayTime(pad.getCommissionDate()),
                                   new IDLLength(pad.getPosition().getX()),
                                   new IDLLength(pad.getPosition().getY()),
                                   new IDLLength(pad.getPosition().getZ()));

        return padidl;
    }
    
    public static alma.TMCDB.BandPointingModel toIDL(PointingModel pointingModel) {
        
        List<ModelTerm> idlTerms = new ArrayList<ModelTerm>();
        List<BandOffsets> idlAllBandOffsets = new ArrayList<BandOffsets>();
        Map<Short, List<ModelTerm>> idlBandOffsetMap = new HashMap<Short, List<ModelTerm>>();
        
        idlBandOffsetMap.put((short)1, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)2, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)3, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)4, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)5, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)6, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)7, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)8, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)9, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)10, new ArrayList<ModelTerm>());
        
        for (String termName : pointingModel.getTerms().keySet()) {
            PointingModelCoeff coeff = pointingModel.getTerms().get(termName);
            ModelTerm idlTerm = new ModelTerm();
            idlTerm.name = termName;
            idlTerm.value = coeff.getValue();
            idlTerms.add(idlTerm);
            for (ReceiverBand offsetBand : coeff.getOffsets().keySet()) {
                double offsetValue = coeff.getOffsets().get(offsetBand);
                ModelTerm idlOffsetTerm = new ModelTerm();
                idlOffsetTerm.name = termName;
                idlOffsetTerm.value = offsetValue;
                idlBandOffsetMap.get(getBandFromString(offsetBand.toString())).add(idlOffsetTerm);
            }
        }
        
        for (short band : idlBandOffsetMap.keySet()) {
            List<ModelTerm> idlOffsetTerms = idlBandOffsetMap.get(band);
            if (idlOffsetTerms.size() > 0) {
                BandOffsets idlBandOffsets = new BandOffsets();
                idlBandOffsets.bandNumber = band;
                idlBandOffsets.terms = idlOffsetTerms.toArray(new ModelTerm[0]);
                idlAllBandOffsets.add(idlBandOffsets);
            }
        }
        
        alma.TMCDB.BandPointingModel idlPointingModel = new alma.TMCDB.BandPointingModel();
        idlPointingModel.base = idlTerms.toArray(new ModelTerm[0]);
        idlPointingModel.offsets = idlAllBandOffsets.toArray(new BandOffsets[0]);
        return idlPointingModel;
    }
    
    public static alma.TMCDB.BandFocusModel toIDL(FocusModel focusModel) {
        
        List<ModelTerm> idlTerms = new ArrayList<ModelTerm>();
        List<BandOffsets> idlAllBandOffsets = new ArrayList<BandOffsets>();
        Map<Short, List<ModelTerm>> idlBandOffsetMap = new HashMap<Short, List<ModelTerm>>();
        
        idlBandOffsetMap.put((short)1, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)2, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)3, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)4, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)5, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)6, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)7, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)8, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)9, new ArrayList<ModelTerm>());
        idlBandOffsetMap.put((short)10, new ArrayList<ModelTerm>());
        
        for (String termName : focusModel.getTerms().keySet()) {
            FocusModelCoeff coeff = focusModel.getTerms().get(termName);
            ModelTerm idlTerm = new ModelTerm();
            idlTerm.name = termName;
            idlTerm.value = coeff.getValue();
            idlTerms.add(idlTerm);
            for (ReceiverBand offsetBand : coeff.getOffsets().keySet()) {
                double offsetValue = coeff.getOffsets().get(offsetBand);
                ModelTerm idlOffsetTerm = new ModelTerm();
                idlOffsetTerm.name = termName;
                idlOffsetTerm.value = offsetValue;
                idlBandOffsetMap.get(getBandFromString(offsetBand.toString())).add(idlOffsetTerm);
            }
        }
        
        for (short band : idlBandOffsetMap.keySet()) {
            List<ModelTerm> idlOffsetTerms = idlBandOffsetMap.get(band);
            if (idlOffsetTerms.size() > 0) {
                BandOffsets idlBandOffsets = new BandOffsets();
                idlBandOffsets.bandNumber = band;
                idlBandOffsets.terms = idlOffsetTerms.toArray(new ModelTerm[0]);
                idlAllBandOffsets.add(idlBandOffsets);
            }
        }
        
        alma.TMCDB.BandFocusModel idlFocusModel = new alma.TMCDB.BandFocusModel();
        idlFocusModel.base = idlTerms.toArray(new ModelTerm[0]);
        idlFocusModel.offsets = idlAllBandOffsets.toArray(new BandOffsets[0]);
        return idlFocusModel;
    }
    
    public static alma.TMCDB.FEDelay toIDL(FEDelay delay) {
        alma.TMCDB.FEDelay idlDelay = new alma.TMCDB.FEDelay();
        idlDelay.receiverBand = delay.getReceiverBand();
        idlDelay.polarization = delay.getPolarization();
        idlDelay.sideband = delay.getSideband();
        idlDelay.delay = delay.getDelay();
        return idlDelay;
    }

    public static alma.TMCDB.IFDelay toIDL(IFDelay delay) {
        alma.TMCDB.IFDelay idlDelay = new alma.TMCDB.IFDelay();
        idlDelay.baseband = delay.getBaseband();
        idlDelay.polarization = delay.getPolarization();
        idlDelay.ifSwitch = IFProcConnStates.get(delay.getIfSwitch().toString());
        idlDelay.delay = delay.getDelay();
        return idlDelay;
    }
    
    public static alma.TMCDB.LODelay toIDL(LODelay delay) {
        alma.TMCDB.LODelay idlDelay = new alma.TMCDB.LODelay();
        idlDelay.baseband = delay.getBaseband();
        idlDelay.delay = delay.getDelay();
        return idlDelay;
    }
    
    public static alma.TMCDB.XPDelay toIDL(XPDelay delay) {
        alma.TMCDB.XPDelay idlDelay = new alma.TMCDB.XPDelay();
        idlDelay.baseband = delay.getBaseband();
        idlDelay.receiverBand = delay.getReceiverBand();
        idlDelay.sideband = delay.getSideband();
        idlDelay.delay = delay.getDelay();
        return idlDelay;
    }
    
    private static Short getBandFromString(String band) {
        if (!bands.containsKey(band)) {
            throw new InvalidParameterException("" + band + " is not a valid band");
        }
        return bands.get(band);
    }
}
