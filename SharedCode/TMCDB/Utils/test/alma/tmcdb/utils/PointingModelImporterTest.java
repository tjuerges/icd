/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: PointingModelImporterTest.java,v 1.2 2011/03/08 21:55:04 sharring Exp $"
 */
package alma.tmcdb.utils;

import java.io.FileReader;

import junit.framework.TestCase;
import alma.tmcdb.domain.Antenna;

public class PointingModelImporterTest extends TestCase {

    public PointingModelImporterTest(String name) {
        super(name);
    }

	@Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testImportPointingModel() throws Exception {
        Antenna antenna = new Antenna();
        antenna.setName("DV01");
        PointingModelImporter importer = new PointingModelImporter();
        importer.addPointingModelToAntenna(antenna,
                new FileReader("TestPointingModel.xml"));
        assertEquals(1, antenna.getPointingModels().size());
    }    
}
