package alma.tmcdb.utils;

import java.io.FileReader;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.ComponentType;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;

public class LruLoaderTest extends TestCase {

    private Session session;
    
    public LruLoaderTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        TmcdbDbConfig tmcdbConfig = new TmcdbDbConfig(Logger.getAnonymousLogger());
//        HibernateUtil.createConfigurationFromDbConfig(tmcdbConfig);
        String url = tmcdbConfig.getConnectionUrl();
        String user = tmcdbConfig.getUsername();
        String password = tmcdbConfig.getPassword();
    	try {
			TmcdbUtils.dropTables(url, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		} // fine, tables hasn't been created yet 
        TmcdbUtils.createTables(url, user, password);
        session = HibernateUtil.getSessionFactory().openSession();
    }
    
    @Override
    protected void tearDown() throws Exception {
    	session.close();
    	HibernateUtil.shutdown();
    	super.tearDown();
    }
    
    public void testLoader() throws Exception {
        Transaction tx = session.beginTransaction();
        ComponentType ct = new ComponentType();
        ct.setIDL("IDL:alma/Control/IFProc:1.0");
        session.save(ct);
        LruLoader.loadLruType(session, new FileReader("LRUSample.xml"), true);
        tx.commit();
    }
    
//    public void testFindHwConfigFiles() throws Exception {
//        String[] files = LruLoader.findTmcdbHwConfigFiles();
//        for (String f : files)
//            System.out.println(f);
//    }
//    
//    public void testLoadAllHwConfigFiles() throws Exception {
//        LruLoader.loadAllHwConfigFiles(true);
//    }

}
