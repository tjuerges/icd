package alma.tmcdb.utils;

import junit.framework.TestCase;

public class AssemblyRoleLoaderTest extends TestCase {

    public AssemblyRoleLoaderTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testLoader() throws Exception {
        assertTrue(AssemblyRoleLoader.loadAssemblyRoles());
    }

}
