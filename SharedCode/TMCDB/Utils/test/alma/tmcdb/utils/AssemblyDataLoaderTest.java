/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.utils;

import java.util.HashSet;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.LruType;

public class AssemblyDataLoaderTest extends TestCase {

    public AssemblyDataLoaderTest(String name) {
        super(name);
    }

    @SuppressWarnings("serial")
	@Override
    protected void setUp() throws Exception {
        super.setUp();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        final ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dummy:1.0");
        session.save(compType);
        LruType lru = new LruType("WCA3", "WCA3", "ICD", 0L, "", "");
        new AssemblyType(lru,"WCA3", "WCA3", BaseElementType.FrontEnd, "", "", compType, "code", "simCode");
        session.save(lru);

        final Configuration swConfig = new Configuration();
        swConfig.setConfigurationName("Test");
        swConfig.setFullName("");
        swConfig.setDescription("");
        HwConfiguration config = new HwConfiguration(swConfig);

        swConfig.setComponents( new HashSet<Component>() {{ add((CloningTestUtils.createComponent("DUMMY", "", compType,swConfig)) ); }} );

        session.save(swConfig);
        session.save(config);
        tx.commit();
        session.close(); 
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testLoadAssemblyData() throws Exception {
        //AssemblyDataLoader.loadAssemblyData(true);
    }    
}
