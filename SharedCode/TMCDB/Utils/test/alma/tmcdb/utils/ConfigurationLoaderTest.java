package alma.tmcdb.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.LruType;

public class ConfigurationLoaderTest extends TestCase {

    public ConfigurationLoaderTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        
        LruType lru =
        	new LruType("Test", "Test", "ICD XXX", 0L, "", "");
        Configuration c = new Configuration();
        c.setConfigurationName("");
        c.setFullName("");
        c.setDescription("");
        HwConfiguration config =
            new HwConfiguration(c);
        ComponentType assCompType = new ComponentType();
        assCompType.setIDL("IDL:alma/Control/Test:1.0");

        session.save(assCompType);
        session.save(config);

        AssemblyType assemblyType =
            new AssemblyType("Test", "Test", BaseElementType.Antenna, "", "", assCompType, "code", "simCode");
        assemblyType.addRole(new AssemblyRole("SecondLOBBpr0"));
        assemblyType.addRole(new AssemblyRole("SecondLOBBpr1"));
        assemblyType.addRole(new AssemblyRole("Mount"));
        assemblyType.addRole(new AssemblyRole("LPR"));
        assemblyType.addRole(new AssemblyRole("CRD"));
        assemblyType.addRole(new AssemblyRole("GPS"));
        lru.addAssemblyType(assemblyType);
        session.save(lru);

        Set<Component> components = new HashSet<Component>();
        ComponentType antCompType = new ComponentType();
        antCompType.setIDL("IDL:alma/Control/Antenna:1.0");
        session.save(antCompType);
        components.add(CloningTestUtils.createComponent("DV01", "CONTROL", antCompType,config.getSwConfiguration()));
        components.add(CloningTestUtils.createComponent("DA41", "CONTROL", antCompType,config.getSwConfiguration()));

        ComponentType lo2CompType = new ComponentType();
        lo2CompType.setIDL("IDL:alma/Control/SecondLO:1.0");
        session.save(lo2CompType);
        components.add(CloningTestUtils.createComponent("SecondLO", "CONTROL/DV01", lo2CompType,config.getSwConfiguration()));
        components.add(CloningTestUtils.createComponent("SecondLO", "CONTROL/DA41", lo2CompType,config.getSwConfiguration()));

        ComponentType mountCompType = new ComponentType();
        mountCompType.setIDL("IDL:alma/Control/Mount:1.0");
        session.save(mountCompType);
        components.add(CloningTestUtils.createComponent("Mount", "CONTROL/DV01", mountCompType,config.getSwConfiguration()));
        components.add(CloningTestUtils.createComponent("Mount", "CONTROL/DA41", mountCompType,config.getSwConfiguration()));

        ComponentType loCompType = new ComponentType();
        loCompType.setIDL("IDL:alma/Control/AOSTiming:1.0");
        session.save(loCompType);
        components.add(CloningTestUtils.createComponent("AOSTiming", "CONTROL", loCompType,config.getSwConfiguration()));

        ComponentType feCompType = new ComponentType();
        feCompType.setIDL("IDL:alma/Control/FrontEnd:1.0");
        session.save(feCompType);        
        components.add(CloningTestUtils.createComponent("FrontEnd", "CONTROL/DV01", feCompType,config.getSwConfiguration()));
        components.add(CloningTestUtils.createComponent("FrontEnd", "CONTROL/DA41", feCompType,config.getSwConfiguration()));

        ComponentType lprCompType = new ComponentType();
        lprCompType.setIDL("IDL:alma/Control/LPR:1.0");
        session.save(lprCompType);        
        components.add(CloningTestUtils.createComponent("LPR", "CONTROL/DA41/FrontEnd", lprCompType,config.getSwConfiguration()));

        ComponentType crdCompType = new ComponentType();
        crdCompType.setIDL("IDL:alma/Control/CRD:1.0");
        session.save(crdCompType);        
        components.add(CloningTestUtils.createComponent("CRD", "CONTROL/AOSTiming", crdCompType,config.getSwConfiguration()));
        ComponentType gpsCompType = new ComponentType();
        gpsCompType.setIDL("IDL:alma/Control/GPS:1.0");
        session.save(gpsCompType);
        components.add(CloningTestUtils.createComponent("GPS", "CONTROL/AOSTiming", gpsCompType,config.getSwConfiguration()));

        config.getSwConfiguration().setComponents(components);
        session.save(config);
        session.flush();
        
        transaction.commit();
        session.close();
    }

	@Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testLoader() throws Exception {
        
        ConfigurationLoader loader = new ConfigurationLoader();
        try {
            loader.loadConfiguration(new FileReader("STEConfiguration.xml"));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        
    }

}
