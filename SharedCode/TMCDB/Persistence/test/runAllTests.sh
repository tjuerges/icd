#!/bin/bash

##############################################################################
# ALMA - Atacama Large Millimiter Array
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration),
# All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# $Id$
#

# Documentation about the test goes here.
#
#

declare TEST_CLASS=alma.tmcdb.AllTests
declare TEST_LOG=/dev/stdout
declare ACS_TMP=`pwd`/tmp

if test $# -ge 1; then
  TEST_LOG=$1
fi

if [ -e $ACS_TMP ]; then
  rm -rf $ACS_TMP &> /dev/null
fi
mkdir $ACS_TMP

acsStartJava -Darchive.configFile=./archiveConfig.properties -endorsed junit.textui.TestRunner "$TEST_CLASS" &> "$TEST_LOG"
RESULT=$?

if [ "$RESULT" = "0" ]; then
    echo "TEST1 $TEST_CLASS PASSED" | tee -a $TEST_LOG
else
    echo "TEST1 $TEST_CLASS FAILED" | tee -a $TEST_LOG
fi

cp hibernate.cfg.xml hibernate.cfg.xml.orig
cp hibernate.cfg.xml.sampletmcdb hibernate.cfg.xml

TEST_CLASS=alma.tmcdb.cloning.CloneWithSampleDatabaseTest
acsStartJava -Darchive.configFile=./archiveConfig.properties.sampleTmcdb -endorsed junit.textui.TestRunner "$TEST_CLASS" >> "$TEST_LOG" 2>&1
RESULT=$?

cp hibernate.cfg.xml.orig hibernate.cfg.xml

if [ "$RESULT" = "0" ]; then
    echo "TEST2 $TEST_CLASS PASSED" | tee -a $TEST_LOG
else
    echo "TEST2 $TEST_CLASS FAILED" | tee -a $TEST_LOG
fi

NUM_OK=`grep "PASSED" $TEST_LOG | wc -l`
if [ "$NUM_OK" = "2" ]; then
	echo "PASSED"
else
	echo "FAIL"
fi

# __oOo__
