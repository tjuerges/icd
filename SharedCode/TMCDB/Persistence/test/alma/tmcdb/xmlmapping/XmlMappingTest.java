package alma.tmcdb.xmlmapping;

import alma.tmcdb.domain.TmcdbTestCase;

public class XmlMappingTest extends TmcdbTestCase {

    public XmlMappingTest(String name) {
        super(name);
    }

//    protected void setUp() throws Exception {
//        super.setUp();
//    }
//
//    protected void tearDown() throws Exception {
//        super.tearDown();
//    }
//    
//    public void testSerializationWithMappingFile() throws Exception {
//        
//        String introot = System.getenv("INTROOT");
//        Mapping mapping = new Mapping();
//        String mf;
//        mf = introot + "/config/Configuration.mapping.xml";
//        mapping.loadMapping(new InputSource(new FileReader(mf)));
//        
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction transaction = session.beginTransaction();
//        
//        LruType lru = new LruType("SecondLO", "2nd LO generator", "ICD XXX",
//            0L, "Generates LO signals for second down conversion", "Notes...");
//
//        Configuration config = new Configuration("Test", "XML Mapping Test Configuration",
//            "Configuration for XML mapping testing");
//
//        
//        ComponentType assCompType = new ComponentType("IDL:alma/Control/SecondLO:1.0");
//        session.save(assCompType);
//        session.save(config);
//
//        AssemblyType assemblyType = new AssemblyType("SecondLO", "2nd LO generator",
//                BaseElementType.Antenna,
//                "Generates LO signals for second down conversion",
//                "Hard to lock",
//                assCompType);
//
//        AssemblyRole assemblyRole = new AssemblyRole("SecondLOBBpr0",
//                new CANParameters(0, "0x0", "0x1"));
//        assemblyType.addRole(assemblyRole);
//
//        lru.addAssemblyType(assemblyType);            
//
//        session.save(lru);
//
//        Assembly assembly = new Assembly("0x001", "<data/>", assemblyType);
//        config.addAssembly(assembly);
//        session.save(config);
//
//        ComponentType compType = new ComponentType("IDL:alma/Control/Antenna:1.0");
//        session.save(compType);
//        Component component = new Component("DV01", "CONTROL", compType);
//        config.addComponent(component);
//        session.save(config);
//        session.flush();
//        Antenna antenna = new Antenna("DV01",
//                AntennaType.VA, 
//                new Coordinate(0.0, 0.0, 0.0),
//                new Coordinate(0.0, 0.0, 0.0),
//                4.5,
//                0L,
//                component);
//        config.addBaseElement(antenna);
//        Antenna antenna2 = new Antenna("DV02",
//                AntennaType.VA, 
//                new Coordinate(0.0, 0.0, 0.0),
//                new Coordinate(0.0, 0.0, 0.0),
//                4.5,
//                0L,
//                component);
//        config.addBaseElement(antenna2);
//        Pad pad = new Pad("Pad01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
//        config.addBaseElement(pad);
//        session.save(config);
//        session.flush();
//        
//        StartupScenario startup = new StartupScenario("StartupConfigurationTest");
//        config.addStartupScenario(startup);
//        session.save(config);
//        session.flush();
//        
//        BaseElementStartup baseElementStartup = new BaseElementStartup(antenna, startup);
//        AssemblyStartup assemblyStartup = new AssemblyStartup(baseElementStartup,
//                                                              assemblyRole, component);
//        baseElementStartup.addAssemblyStartup(assemblyStartup);
//        session.save(startup);
//        
//        transaction.commit();
//        session.close();
//        
//        String xml = null;
//        Writer writer = new StringWriter();
//        Marshaller marshaller = new Marshaller(writer);
//        marshaller.setMapping(mapping);
//        marshaller.marshal(config);
//        xml = writer.toString();
//        System.out.println(writer.toString());
//        
//        session = HibernateUtil.getSessionFactory().openSession();
//        transaction = session.beginTransaction();
//        session.delete(assemblyStartup);
//        session.delete(baseElementStartup);
//        session.delete(startup);
//        session.delete(antenna);
//        session.delete(antenna2);
//        session.delete(pad);
//        session.delete(component);
//        session.delete(compType);
//        session.delete(assembly);
//        session.delete(assemblyType);
//        session.delete(assCompType);
//        session.delete(config);
//        session.delete(lru);
//        transaction.commit();
//        session.close();
//        
////        Writer w = new StringWriter();
////        Marshaller marsh = new Marshaller(w);
////        marsh.setMapping(mapping);
////        marsh.marshal(antenna);
////        xml = w.toString();
////        System.out.println(xml);
//        
////        Reader reader = new StringReader(xml);
////        Unmarshaller unmarshaller = new Unmarshaller();
////        unmarshaller.setMapping(mapping);
////        Antenna unmAnt = (Antenna) 
////            unmarshaller.unmarshal(reader);
////        System.out.println(unmAnt.getName());
////        System.out.println(unmAnt.getAntennaType());
////        System.out.println(unmAnt.getComponent().getId());
//        
//        Reader reader = new StringReader(xml);
//        Unmarshaller unmarshaller = new Unmarshaller();
//        unmarshaller.setMapping(mapping);
//        Configuration cfg = (Configuration) 
//            unmarshaller.unmarshal(reader);
//        System.out.println(cfg.getName());
//        System.out.println(cfg.getName());
//        System.out.println(cfg.getActive());
//        System.out.println(cfg.getBaseElements().size());
//        System.out.println(cfg.getStartupScenarios().size());
//        
//        session = HibernateUtil.getSessionFactory().openSession();
//        transaction = session.beginTransaction();
//        
//        
//        Set<StartupScenario> baseElementStartups = new HashSet<StartupScenario>();
//        for (Iterator<StartupScenario> iter = cfg.getStartupScenarios().iterator(); iter.hasNext(); ) {
//            StartupScenario ss = iter.next();
//            ss.setId(null);
//        }
//        Map<Long, Assembly> assemblies = new HashMap<Long, Assembly>();
//        for (Iterator<Assembly> iter = cfg.getAssemblies().iterator(); iter.hasNext(); ) {
//            Assembly as = iter.next();
//            assemblies.put(as.getId(), as);
//        }        
//        Map<Long, BaseElement> baseElements = new HashMap<Long, BaseElement>();
//        for (Iterator<BaseElement> iter = cfg.getBaseElements().iterator(); iter.hasNext(); ) {
//            BaseElement be = iter.next();
//            baseElements.put(be.getId(), be);
//        }
//        cfg.getStartupScenarios().clear();
//        cfg.getBaseElements().clear();
//        cfg.getAssemblies().clear();
//        session.save(cfg);
//        // Add some preliminaries that are not in the XML string
//        // In the real program we would have to look for the componenttype
//        // and component name in the database
//        ComponentType ct = new ComponentType("IDL:alma/Control/Antenna:1.0");
//        session.save(ct);
//        Component comp = new Component("DV01", "CONTROL", ct);
//        cfg.addComponent(comp);
//        session.save(cfg);
//        session.flush();
//        for (Iterator<Long> iter = baseElements.keySet().iterator(); iter.hasNext(); ) {
//            BaseElement be = baseElements.get(iter.next());
//            be.setId(null);
//            be.setConfiguration(cfg);
//            if (be instanceof Antenna)
//                ((Antenna)be).setComponent(comp);
//            cfg.addBaseElement(be);
//        }
//        session.save(cfg);
//        
//        // More preliminaries
//        lru = new LruType("SecondLO", "2nd LO generator", "ICD XXX",
//                0L, "Generates LO signals for second down conversion", "Notes...");
//        ComponentType act = new ComponentType("IDL:alma/Control/SecondLO:1.0");
//        session.save(act);
//        session.save(cfg);
//        session.flush();
//        AssemblyType at = new AssemblyType("SecondLO", "2nd LO generator",
//                    BaseElementType.Antenna,
//                    "Generates LO signals for second down conversion",
//                    "Hard to lock",
//                    act);
//        AssemblyRole ar = new AssemblyRole("SecondLOBBpr0",
//                    new CANParameters(0, "0x0", "0x1"));
//        at.addRole(ar);
//        lru.addAssemblyType(at);
//        session.save(lru);
//        for (Iterator<Long> iter = assemblies.keySet().iterator(); iter.hasNext(); ) {
//            Assembly as = assemblies.get(iter.next());
//            as.setId(null);
//            as.setConfiguration(cfg);
//            as.setAssemblyType(at);
//            cfg.addAssembly(as);
//        }
//        session.save(cfg);
//                
//        transaction.commit();
//        session.close();
//    }
}
