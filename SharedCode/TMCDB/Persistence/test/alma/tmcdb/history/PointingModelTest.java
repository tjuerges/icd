/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: PointingModelTest.java,v 1.2 2010/12/16 21:56:24 rhiriart Exp $"
 */
package alma.tmcdb.history;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ReceiverBandMod.ReceiverBand;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class PointingModelTest extends TmcdbTestCase {

    private Session session;
    private VersionKeeperInterceptor interceptor = new VersionKeeperInterceptor();
    
    public PointingModelTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
        interceptor.setSession(session);
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAddPointingModel() {
        Transaction tx = null;
        PointingModelCoeffBackLog pmbl = null;
        PointingModelCoeffOffsetBackLog pmoffbl = null;
        try {
            tx = session.beginTransaction();
            pmbl = new PointingModelCoeffBackLog();
            pmbl.setPointingModelId(new Long(0));
            pmbl.setVersion(new Long(0));
            pmbl.setModTime(new Long(System.currentTimeMillis()));
            pmbl.setOperation('I');
            pmbl.setWho("rhiriart");
            pmbl.setDescription("tests");
            pmbl.setName("IACA");
            pmbl.setValue(0.0f);
            session.save(pmbl);
            pmoffbl = new PointingModelCoeffOffsetBackLog();
            pmoffbl.setPointingModelId(new Long(0));
            pmoffbl.setVersion(new Long(0));
            pmoffbl.setModTime(new Long(System.currentTimeMillis()));
            pmoffbl.setOperation('I');
            pmoffbl.setWho("rhiriart");
            pmoffbl.setName("IACA");
            pmoffbl.setReceiverBand(ReceiverBand.ALMA_RB_01);
            pmoffbl.setOffset(0.0);
            session.save(pmoffbl);
            tx.commit();
            tx = session.beginTransaction();
            Query query = session.createQuery("from PointingModelCoeffBackLog");
            List<PointingModelCoeffBackLog> records =
                (List<PointingModelCoeffBackLog>) query.list();
            assertEquals(1, records.size());
            query = session.createQuery("from PointingModelCoeffOffsetBackLog");
            List<PointingModelCoeffOffsetBackLog> offsets =
                (List<PointingModelCoeffOffsetBackLog>) query.list();
            assertEquals(1, offsets.size());
            tx.commit();
        } finally {
            // Cleaning
            tx = session.beginTransaction();
            session.delete(pmbl);
            session.delete(pmoffbl);
            tx.commit();
        }
    }
}
