/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: VersionInfoTest.java,v 1.2 2010/12/21 22:20:13 rhiriart Exp $"
 */
package alma.tmcdb.history;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.history.VersionInfo;
import alma.tmcdb.utils.HibernateUtil;

public class VersionInfoTest extends TmcdbTestCase {

    private Session session;
    
    public VersionInfoTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();        
        super.tearDown();
    }
    
    public void testVersionInfo() {
        Transaction tx = null;
        
        tx = session.beginTransaction();
        Configuration swc;
        swc = new Configuration();
        swc.setConfigurationName("Test");
        swc.setFullName("");
        swc.setActive(true);
        swc.setCreationTime(new Date());
        swc.setDescription("");
        session.save(swc);        
        tx.commit();
        
        tx = session.beginTransaction();
        VersionInfo vi = new VersionInfo();
        vi.setName("PointingModel");
        vi.setConfigurationId(swc.getConfigurationId());
        vi.setEntityId(new Long(0));
        vi.setLock(true);
        vi.setIncreaseVersion(true);
        vi.setCurrentVersion(0);
        vi.setModifier("rhiriart");
        vi.setDescription("Setting new calibration values.");
        session.save(vi);
        tx.commit();
        try {
            // nothing here yet
        } finally {
            // Cleaning
            tx = session.beginTransaction();
            session.delete(vi);
            tx.commit();
        }
    }
}
