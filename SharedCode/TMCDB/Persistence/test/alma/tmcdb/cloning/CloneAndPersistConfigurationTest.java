package alma.tmcdb.cloning;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Set;

import org.exolab.castor.mapping.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.Assembly;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FrontEnd;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

/**
 * Tests for cloning (and saving) a configuration.
 * @author sharrington
 */
public class CloneAndPersistConfigurationTest extends TmcdbTestCase 
{
	public CloneAndPersistConfigurationTest(String name) {
		super(name);
	}

	public void testCloneSimpleHwConfiguration() throws FileNotFoundException, IOException, MappingException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Configuration swCfg = new Configuration();
        swCfg.setConfigurationName("Test-1");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration config = new HwConfiguration(swCfg);
		session.save(swCfg);
		session.save(config);

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
				config, config.getSwConfiguration().getConfigurationName());

		CloningTestUtils.compareConfigurations(config, clonedConfig);
		assertEquals(null, clonedConfig.getId());

		clonedConfig.getSwConfiguration().setConfigurationName("Copy of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 
		session.save(clonedConfig.getSwConfiguration());
		session.save(clonedConfig);

		transaction.commit();
		session.close();
	}

	public void testCloneHwConfigurationWithComponentType() throws FileNotFoundException, IOException, MappingException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Configuration swCfg = new Configuration();
        swCfg.setConfigurationName("Test-2");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration config = new HwConfiguration(swCfg);

		ComponentType assCompType = new ComponentType();
	    assCompType.setIDL("IDL:alma/Control/SecondLO:1.0");
		session.save(assCompType);

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
				config, config.getSwConfiguration().getConfigurationName());

		CloningTestUtils.compareConfigurations(config, clonedConfig);
		assertEquals(null, clonedConfig.getId());

		clonedConfig.getSwConfiguration().setConfigurationName("Copy2 of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 
		session.save(clonedConfig.getSwConfiguration());
		session.save(clonedConfig);

		transaction.commit();
		session.close();
	}

	public void testCloneHwConfigurationWithComponentTypeAndAssembly() throws FileNotFoundException, IOException, MappingException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Configuration swCfg = new Configuration();
        swCfg.setConfigurationName("Test-3");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration config = new HwConfiguration(swCfg);

		ComponentType assCompType = new ComponentType();
		assCompType.setIDL("IDL:alma/Control/SecondLO:1.0");
		session.save(assCompType);

		AssemblyType assemblyType = new AssemblyType("Test", "Test", BaseElementType.Antenna,
				"Muta d'accento - e di pensiero.", "Sempre un amabile,",
				assCompType, "", "simcode");

		AssemblyRole assemblyRole = new AssemblyRole("aRole");
		assemblyType.addRole(assemblyRole);

		LruType lru = new LruType("TestLRU", "TestLRU", "ICD XXX", 0L, "La donna e mobile",
		"Qual piuma al vento");

		lru.addAssemblyType(assemblyType);            
		session.save(lru);

		Assembly assembly = new Assembly("0x001", "<data/>", assemblyType);
		config.addAssembly(assembly);
		session.saveOrUpdate(config.getSwConfiguration());
		session.saveOrUpdate(config);
		transaction.commit();
		session.close();

		session = HibernateUtil.getSessionFactory().openSession();
		transaction = session.beginTransaction();

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), config, 
				config.getSwConfiguration().getConfigurationName());
		CloningTestUtils.compareConfigurations(config, clonedConfig);

		assertEquals(null, clonedConfig.getId());

		CloningTestUtils.compareAssemblies(config, clonedConfig);

		clonedConfig.getSwConfiguration().setConfigurationName("Copy3 of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 

		session.saveOrUpdate(clonedConfig.getSwConfiguration());
		session.saveOrUpdate(clonedConfig);
		session.flush();

		assertEquals(config.getSwConfiguration().getDescription(), clonedConfig.getSwConfiguration().getDescription());
		assertEquals(config.getSwConfiguration().getFullName(), clonedConfig.getSwConfiguration().getFullName());
		assertEquals(clonedConfig.getSwConfiguration().getConfigurationName(), clonedConfig.getSwConfiguration().getConfigurationName());
		assertEquals(config.getSwConfiguration().getActive(), clonedConfig.getSwConfiguration().getActive());
		assertEquals(clonedConfig.getSwConfiguration().getCreationTime(), clonedConfig.getSwConfiguration().getCreationTime());
		assertNotSame(null, clonedConfig.getId());
		assertNotSame(config.getId(), clonedConfig.getId());

		CloningTestUtils.compareAssemblies(config, clonedConfig);


		transaction.commit();
		session.close();
	}

	public void testCloneHwConfigurationWithComponentAndAssembly() throws FileNotFoundException, IOException, MappingException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Configuration swCfg = new Configuration();
        swCfg.setConfigurationName("Test-5");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration config = new HwConfiguration(swCfg);

		ComponentType assCompType = new ComponentType();
		assCompType.setIDL("IDL:alma/Control/SecondLO:1.0");
		session.save(assCompType);

		AssemblyType assemblyType = new AssemblyType("Test", "Test", BaseElementType.Antenna,
				"Muta d'accento - e di pensiero.", "Sempre un amabile,",
				assCompType, "", "simcode");

		AssemblyRole assemblyRole = new AssemblyRole("aRole");
		assemblyType.addRole(assemblyRole);

		LruType lru = new LruType("TestLRU", "TestLRU", "ICD XXX", 0L, "La donna e mobile",
		"Qual piuma al vento");

		lru.addAssemblyType(assemblyType);            
		session.save(lru);

		Assembly assembly = new Assembly("0x001", "<data/>", assemblyType);
		config.addAssembly(assembly);
		session.saveOrUpdate(config.getSwConfiguration());
		session.saveOrUpdate(config);

		ComponentType compType = new ComponentType();
		compType.setIDL("IDL:alma/Control/Antenna:1.0");
		session.save(compType);

		Component component = CloningTestUtils.createComponent("DV01", "CONTROL", compType, swCfg);
		config.getSwConfiguration().getComponents().add(component);

		session.update(config);
		session.flush();
		transaction.commit();
		session.close();

		session = HibernateUtil.getSessionFactory().openSession();
		transaction = session.beginTransaction();

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
				config, config.getSwConfiguration().getConfigurationName());
		CloningTestUtils.compareConfigurations(config, clonedConfig);
		assertEquals(null, clonedConfig.getId());

		CloningTestUtils.compareAssemblies(config, clonedConfig);

		CloningTestUtils.compareComponentsForHw(config, clonedConfig);

		clonedConfig.getSwConfiguration().setConfigurationName("Copy4 of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 

		session.saveOrUpdate(clonedConfig.getSwConfiguration());
		session.saveOrUpdate(clonedConfig);

		assertEquals(config.getSwConfiguration().getDescription(), clonedConfig.getSwConfiguration().getDescription());
		assertEquals(config.getSwConfiguration().getFullName(), clonedConfig.getSwConfiguration().getFullName());
		assertEquals(clonedConfig.getSwConfiguration().getConfigurationName(), clonedConfig.getSwConfiguration().getConfigurationName());
		assertEquals(config.getSwConfiguration().getActive(), clonedConfig.getSwConfiguration().getActive());
		assertNotSame(null, clonedConfig.getId());
		assertNotSame(config.getId(), clonedConfig.getId());
		assertNotSame(config.getSwConfiguration().getConfigurationId(), clonedConfig.getSwConfiguration().getConfigurationId());

		CloningTestUtils.compareComponentsForHw(config, clonedConfig);
		CloningTestUtils.compareAssemblies(config, clonedConfig);

		transaction.commit();
		session.close();
	}

	public void testCloneHwConfigurationWithComponentAndAssemblyAndAntenna() throws FileNotFoundException, IOException, MappingException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Configuration swCfg = new Configuration();
        swCfg.setConfigurationName("Test-7");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration config = new HwConfiguration(swCfg);

		ComponentType assCompType = new ComponentType();
		assCompType.setIDL("IDL:alma/Control/SecondLO:1.0");
		session.save(assCompType);

		AssemblyType assemblyType = new AssemblyType("Test", "Test", BaseElementType.Antenna,
				"Muta d'accento - e di pensiero.", "Sempre un amabile,",
				assCompType, "", "simcode");

		AssemblyRole assemblyRole = new AssemblyRole("aRole");
		assemblyType.addRole(assemblyRole);

		LruType lru = new LruType("TestLRU", "TestLRU", "ICD XXX", 0L, "La donna e mobile",
		"Qual piuma al vento");

		lru.addAssemblyType(assemblyType);            
		session.save(lru);

		Assembly assembly = new Assembly("0x001", "<data/>", assemblyType);
		config.addAssembly(assembly);
		session.saveOrUpdate(config.getSwConfiguration());
		session.saveOrUpdate(config);

		ComponentType compType = new ComponentType();
		compType.setIDL("IDL:alma/Control/Antenna:1.0");
		session.save(compType);

		Component component = CloningTestUtils.createComponent("DV01", "CONTROL", compType, swCfg);
		session.save(component);
		session.saveOrUpdate(swCfg);
		session.update(config);
		session.flush();

		Antenna antenna = new Antenna("DV01",
				AntennaType.VA, 
				new Coordinate(0.0, 0.0, 0.0),
				new Coordinate(0.0, 0.0, 0.0),
				4.5,
				0L,
				0,
				0);
		config.addBaseElement(antenna);
		Antenna antenna2 = new Antenna("DV02",
				AntennaType.VA, 
				new Coordinate(0.0, 0.0, 0.0),
				new Coordinate(0.0, 0.0, 0.0),
				4.5,
				0L,
				0,
				0);
		config.addBaseElement(antenna2);
		Pad pad = new Pad("Pad01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
		config.addBaseElement(pad);
		session.update(config);
		transaction.commit();
		session.flush();
		session.close();

		session = HibernateUtil.getSessionFactory().openSession();
		transaction = session.beginTransaction();

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
				config, config.getSwConfiguration().getConfigurationName());
		CloningTestUtils.compareConfigurations(config, clonedConfig);
		assertEquals(null, clonedConfig.getId());

		CloningTestUtils.compareAssemblies(config, clonedConfig);
		CloningTestUtils.compareComponentsForHw(config, clonedConfig);
		CloningTestUtils.compareBaseElements(config, clonedConfig);

		clonedConfig.getSwConfiguration().setConfigurationName("Copy5 of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 

		// now jump through some hoops to actually persist the cloned config; hoops necessary because
		// we cannot at present save a fully populated configuration with cascading in hibernate;
		// this requires intermediate saves, as in the following code.
		swCfg = new Configuration();
        swCfg.setConfigurationName("Test-8");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
		HwConfiguration toSaveClonedConfig = new HwConfiguration(swCfg);
		session.save(toSaveClonedConfig.getSwConfiguration());
		session.save(toSaveClonedConfig);

		for(Component compToAdd : clonedConfig.getSwConfiguration().getComponents()) {
			compToAdd.setConfiguration(toSaveClonedConfig.getSwConfiguration());
			toSaveClonedConfig.getSwConfiguration().getComponents().add(compToAdd);
			session.saveOrUpdate(compToAdd);
		}
		session.update(toSaveClonedConfig);
		session.flush();

		Set<Assembly> assembliesToSave = clonedConfig.getAssemblies();
		for(Assembly assemblyToSave: assembliesToSave) {
			toSaveClonedConfig.addAssembly(assemblyToSave);
		}
		session.update(toSaveClonedConfig);
		session.flush();

		Set<BaseElement> baseElementsToSave = clonedConfig.getBaseElements();
		for(BaseElement baseElementToSave : baseElementsToSave) {
			toSaveClonedConfig.addBaseElement(baseElementToSave);
		}
		session.update(toSaveClonedConfig);
		session.flush();

		assertNotSame(null, toSaveClonedConfig.getId());
		assertNotSame(config.getId(), toSaveClonedConfig.getId());

		// revert name && creation time for comparison, else comparison will fail.
		String cloneName = toSaveClonedConfig.getSwConfiguration().getConfigurationName();
		toSaveClonedConfig.getSwConfiguration().setConfigurationName(config.getSwConfiguration().getConfigurationName());
		toSaveClonedConfig.getSwConfiguration().setCreationTime(config.getSwConfiguration().getCreationTime());
		CloningTestUtils.compareConfigurations(config, toSaveClonedConfig);
		toSaveClonedConfig.getSwConfiguration().setConfigurationName(cloneName);
		
		transaction.commit();
		session.close();
	}

	public void testCloneHwConfigurationWithComponentAndAssemblyAndAntennaAndStartup()
	{
		Transaction tx = null;
        LruType lru = null;
        HwConfiguration config = null;
        StartupScenario startup = null;
        ComponentType compType = null;
        Component component = null;
        Antenna antenna = null;
        Pad pad = null;
        BaseElementStartup baseElementStartup = null;
        AssemblyRole assemblyRole = null;
        FrontEnd frontEnd = null;
        Configuration swCfg = null;
        
        // Preliminaries, some global objects are needed
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        swCfg = new Configuration();
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        config = new HwConfiguration(swCfg);
        session.save(config.getSwConfiguration());
        session.save(config);
        compType = new ComponentType();
        compType.setIDL("IDL:alma/Control/FOO:1.0");
        session.save(compType);

        lru = new LruType("lru", "lru", "icd", 0L, "", "");
        AssemblyType assemblyType = new AssemblyType("test",
        		"test",
        		BaseElementType.Antenna,
        		"",
        		"",
        		compType,
        		"", "simcode");
        assemblyRole = new AssemblyRole("aRole");
        assemblyType.addRole(assemblyRole);
        lru.addAssemblyType(assemblyType);
        session.save(lru);

        tx.commit();
        session.close();

        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();
        component = CloningTestUtils.createComponent("FOO", "BAR", compType, swCfg);
        config.getSwConfiguration().getComponents().add(component);    

        antenna = new Antenna("DV01",
        		AntennaType.ACA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna);
        
        pad = new Pad("PAD01", new Coordinate(1.0, 2.0, 3.0), new Long(0));
        config.addBaseElement(pad);

        @SuppressWarnings("unused") // actually used in persistence under the covers...
        AntennaToPad a2p = new AntennaToPad(antenna, pad, new Long(0), new Long(0), true);

        frontEnd = new FrontEnd("AFrontEnd", new Long(0));
        config.addBaseElement(frontEnd);

        @SuppressWarnings("unused") // actually used in persistence under the covers...
        AntennaToFrontEnd a2fe = new AntennaToFrontEnd(antenna, frontEnd, new Long(0), new Long(0));

        startup = new StartupScenario("startup");
        config.addStartupScenario(startup);
        baseElementStartup = new BaseElementStartup(antenna, startup);
        baseElementStartup.setSimulated(false);
        startup.addBaseElementStartup(baseElementStartup);

        @SuppressWarnings("unused") // actually used in persistence under the covers...
        AssemblyStartup assemblyStartup = new AssemblyStartup(baseElementStartup, assemblyRole);
        assemblyStartup.setSimulated(false);

        session.saveOrUpdate(config.getSwConfiguration());
        session.saveOrUpdate(config);
        session.flush();
        tx.commit();
        session.close();
        
		System.out.println("SLH >>>>>>>>>>>>>>> Original: " + config.toString());

		// ------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning
		// ------------------------------------------------------------------
	    interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();				
		HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
				config, config.getSwConfiguration().getConfigurationName());
		
		assertEquals(null, clonedConfig.getId());
		CloningTestUtils.compareConfigurations(config, clonedConfig);

		clonedConfig.getSwConfiguration().setConfigurationName("Copy6 of: " + config.getSwConfiguration().getConfigurationName());
		clonedConfig.getSwConfiguration().setCreationTime(new Date()); 
		
//		for(BaseElement baseElement : clonedConfig.getBaseElements()) {
//			if(baseElement instanceof Antenna) {
//				for(AntennaToPad ant2pad: ((Antenna)baseElement).getScheduledPadLocations()) {
//					a2p.setId(new AntennaToPad.Id(null, null, ant2pad.getEndTime()));
//				}
//			}
//		}
		
//		for(StartupScenario  scenarioToFix: clonedConfig.getStartupScenarios()) {
//			for(AssemblyStartup assStartupToFix : scenarioToFix.getAssemblyStartups()) {
//				assStartupToFix.setId(new AssemblyStartup.Id(null, null, assStartupToFix.getAssemblyRole().getName()));
//			}
//			for(BaseElementStartup beStartupToFix : scenarioToFix.getBaseElementStartups()) {
//				for(AssemblyStartup assemblyStartupToFix : beStartupToFix.getAssemblyStartups()) {
//					assemblyStartupToFix.setId(new AssemblyStartup.Id(null, null, assemblyStartupToFix.getAssemblyRole().getName()));
//				}
//			}
//		}
		
		System.out.println("SLH <<<<<<<<<<<<<<<<<<<, Clone: " + clonedConfig.toString());

		session.saveOrUpdate(clonedConfig.getSwConfiguration());
		session.saveOrUpdate(clonedConfig);
		session.flush();
		
		assertEquals(config.getSwConfiguration().getDescription(), clonedConfig.getSwConfiguration().getDescription());
		assertEquals(config.getSwConfiguration().getFullName(), clonedConfig.getSwConfiguration().getFullName());
		assertNotSame(config.getSwConfiguration().getConfigurationName(), clonedConfig.getSwConfiguration().getConfigurationName());
		assertEquals(config.getSwConfiguration().getActive(), clonedConfig.getSwConfiguration().getActive());
		assertEquals(clonedConfig.getSwConfiguration().getCreationTime(), clonedConfig.getSwConfiguration().getCreationTime());
		assertNotSame(null, clonedConfig.getId());
		assertNotSame(config.getId(), clonedConfig.getId());

		System.out.println("orig: " + config);
		System.out.println("--------------------");
		System.out.println("clone: " + clonedConfig);
		
		CloningTestUtils.compareComponentsForHw(config, clonedConfig);
		CloningTestUtils.compareAssemblies(config, clonedConfig);
		CloningTestUtils.compareBaseElements(config, clonedConfig);
		CloningTestUtils.compareStartupScenarios(config.getStartupScenarios(), clonedConfig.getStartupScenarios());
		
		tx.commit();
		session.close();
	}
}
