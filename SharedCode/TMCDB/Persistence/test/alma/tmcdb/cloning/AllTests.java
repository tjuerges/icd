package alma.tmcdb.cloning;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for alma.tmcdb.cloning");
        suite.addTestSuite(CloneAndPersistConfigurationTest.class);
        suite.addTestSuite(CloneAndPersistStartupScenarioTest.class);
        suite.addTestSuite(CloneAndPersistBaseElementTest.class);
        return suite;
    }
}
