package alma.tmcdb.cloning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.exolab.castor.mapping.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.archive.database.helpers.wrappers.DbConfigException;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class CloneWithSampleDatabaseTest extends TestCase 
{
    private Session session;
    private Connection conn;
    
    public CloneWithSampleDatabaseTest(String name) {
        super(name);
    }
    
    public void testCloneStartupScenario() throws FileNotFoundException, IOException, MappingException, DbConfigException
    {
        TmcdbDbConfig dbconf = new TmcdbDbConfig(Logger.getAnonymousLogger());
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction tx = session.beginTransaction();                
        
        List hwConfigs = session.createCriteria(HwConfiguration.class).list();
        assertNotNull(hwConfigs);
        assertTrue(hwConfigs.size() != 0);
        HwConfiguration config = (HwConfiguration) hwConfigs.get(0);
        assertNotNull(config);
       
        Set<StartupScenario> scenarios = config.getStartupScenarios();
        assertNotNull(scenarios);
        assertFalse(0 == scenarios.size());
        StartupScenario scenarioToClone = scenarios.iterator().next();
        assertNotNull(scenarioToClone);
        scenarioToClone = (StartupScenario) session.get(StartupScenario.class, scenarioToClone.getId());
        StartupScenario clonedStartup = CloningUtils.cloneStartupScenarioWithBeanlib(HibernateUtil.getSessionFactory(), scenarioToClone, scenarioToClone.getName());
        assertTrue(CloningTestUtils.safeEquals(scenarioToClone, clonedStartup));
        assertEquals(null, clonedStartup.getId());
        
        clonedStartup.setName("Copy of: " + clonedStartup.getName());
        clonedStartup.setConfiguration(scenarioToClone.getConfiguration());
        scenarioToClone.getConfiguration().addStartupScenario(clonedStartup);
        session.update(scenarioToClone.getConfiguration());

        tx.commit();
        session.close();
    }
    
    public void testCloneStartupScenarioWithBeanLib() throws FileNotFoundException, IOException, MappingException, DbConfigException
    {
        TmcdbDbConfig dbconf = new TmcdbDbConfig(Logger.getAnonymousLogger());
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction tx = session.beginTransaction();                
        
        List hwConfigs = session.createCriteria(HwConfiguration.class).list();
        assertNotNull(hwConfigs);
        assertTrue(hwConfigs.size() != 0);
        HwConfiguration config = (HwConfiguration) hwConfigs.get(0);
        assertNotNull(config);
       
        Set<StartupScenario> scenarios = config.getStartupScenarios();
        assertNotNull(scenarios);
        assertFalse(0 == scenarios.size());
        StartupScenario scenarioToClone = scenarios.iterator().next();
        assertNotNull(scenarioToClone);
        StartupScenario clonedStartup = CloningUtils.cloneStartupScenarioWithBeanlib(session.getSessionFactory(), scenarioToClone, scenarioToClone.getName());
        assertTrue(CloningTestUtils.safeEquals(scenarioToClone, clonedStartup));
        assertEquals(null, clonedStartup.getId());
        
        clonedStartup.setName("Copy of: " + clonedStartup.getName());
        clonedStartup.setConfiguration(scenarioToClone.getConfiguration());
        scenarioToClone.getConfiguration().addStartupScenario(clonedStartup);
        session.update(scenarioToClone.getConfiguration());

        tx.commit();
        session.close();
    }
    
    public void testCloneFullHwConfiguration() throws FileNotFoundException, IOException, MappingException, DbConfigException
    {
        TmcdbDbConfig dbconf = new TmcdbDbConfig(Logger.getAnonymousLogger());
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction tx = session.beginTransaction();                
        
        List hwConfigs = session.createCriteria(HwConfiguration.class).list();
        assertNotNull(hwConfigs);
        assertTrue(hwConfigs.size() != 0);
        HwConfiguration config = (HwConfiguration) hwConfigs.get(0);
        assertNotNull(config);
        HwConfiguration clonedConfig = CloningUtils.cloneConfiguration(HibernateUtil.getSessionFactory(), 
        		config, config.getSwConfiguration().getConfigurationName());
        
        assertEquals(null, clonedConfig.getId());
        CloningTestUtils.compareConfigurations(config, clonedConfig);
        
        clonedConfig.getSwConfiguration().setConfigurationName("Clone of: " + config.getSwConfiguration().getConfigurationName());

        session.save(clonedConfig.getSwConfiguration());
        tx.commit();
        session.close();
        
        interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();    
        session.save(clonedConfig);
        tx.commit();
        session.close();
    }
    
    protected void setUp() throws Exception {
        System.out.println("SLH: setUp");
        CloningTestUtils.unzipSampleTmcdbDatabase();
        CloningTestUtils.untarSampleTmcdbDatabase();
        
        Class.forName(TmcdbTestCase.HSQLDB_JDBC_DRIVER);   
        conn = DriverManager.getConnection(TmcdbTestCase.HSQLDB_FILE_URL, 
                TmcdbTestCase.HSQLDB_USER, 
                TmcdbTestCase.HSQLDB_PASSWORD);
        //conn.close();
    }

    protected void tearDown() throws Exception {
        System.out.println("SLH: tearDown");
        conn = DriverManager.getConnection(TmcdbTestCase.HSQLDB_FILE_URL, 
        		TmcdbTestCase.HSQLDB_USER, 
        		TmcdbTestCase.HSQLDB_PASSWORD);

        conn.close();
        CloningTestUtils.removeSampleTmcdbDatabase();
    }
}
