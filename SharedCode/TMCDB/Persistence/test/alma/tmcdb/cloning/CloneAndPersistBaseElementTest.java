package alma.tmcdb.cloning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.exolab.castor.mapping.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.ComponentType;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FocusModel;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

/**
 * Test for cloning of base elements.
 * @author sharring
 */
public class CloneAndPersistBaseElementTest extends TmcdbTestCase 
{
	/**
	 * Constructor.
	 * @param name the name of the test
	 */
	public CloneAndPersistBaseElementTest(String name) {
		super(name);
	}

	public void testCloningBaseElement() throws FileNotFoundException, IOException, MappingException
	{	
		BaseElement originalBaseElement = createBaseElementForCloning();
        
		CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction transaction = session.beginTransaction();
        
        BaseElement clonedBaseElement = CloningUtils.cloneBaseElement(HibernateUtil.getSessionFactory(), originalBaseElement, originalBaseElement.getName());
        
		if(originalBaseElement == clonedBaseElement) {
			fail();; // shouldn't happen; object identities == means it's the same object (no actual clone)!
		}

		System.out.println("originalBaseElement id: " + originalBaseElement.getId() + " name: " + originalBaseElement.getName() + " and config id: " + originalBaseElement.getConfiguration().getId());
		System.out.println("clonedBaseElement id: " + clonedBaseElement.getId() + " name: " + clonedBaseElement.getName() + " and config id: " + clonedBaseElement.getConfiguration().getId());
		System.out.println("--------------------------------------");
		System.out.println("orig: " + originalBaseElement);
		System.out.println("--------------------------------------");
		System.out.println("clone: " + clonedBaseElement);

		assertEquals(true, CloningTestUtils.safeEquals(originalBaseElement, clonedBaseElement));
		assertEquals(null, clonedBaseElement.getId());
		
		// now, change the name to avoid uniqueness constraints on name (which don't seem to be working?)
		clonedBaseElement.setName("Copy of: " + clonedBaseElement.getName());
		
		session.saveOrUpdate(originalBaseElement.getConfiguration());
				
		transaction.commit();
		session.close();	
	}
	
	private BaseElement createBaseElementForCloning() 
	{
		HwConfiguration config = CloningTestUtils.createConfiguration("test");
		Antenna newBaseElement = new Antenna();
		
		newBaseElement.setAntennaType(AntennaType.VA);
		newBaseElement.setAvgDelay(10.0);
		newBaseElement.setCommissionDate(System.currentTimeMillis());
		newBaseElement.setDiameter(12.0);
		newBaseElement.setName("DV09");
		newBaseElement.setOffset(new Coordinate(1,2,3));
		newBaseElement.setPosition(new Coordinate(4, 5, 6));
		newBaseElement.setType(BaseElementType.Antenna);
		newBaseElement.setLoOffsettingIndex(1);
		newBaseElement.setWalshSeq(2);
		Set<FocusModel> focusModels = new HashSet<FocusModel>();
		newBaseElement.setFocusModels(focusModels);
		Set<AntennaToFrontEnd> scheduledFrontEnds = new HashSet<AntennaToFrontEnd>();
		newBaseElement.setScheduledFrontEnds(scheduledFrontEnds);
		Set<AntennaToPad> scheduledPadLocations = new HashSet<AntennaToPad>();
		newBaseElement.setScheduledPadLocations(scheduledPadLocations);

		ComponentType compType = new ComponentType();
		compType.setIDL("IDL://www.www.www");
		
		CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(compType);
        transaction.commit();
		session.close();
		
		config.addBaseElement(newBaseElement);
		
		interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        transaction = session.beginTransaction();
        
        session.saveOrUpdate(newBaseElement.getConfiguration().getSwConfiguration());
        session.saveOrUpdate(newBaseElement.getConfiguration());
        transaction.commit();
		session.close();	
		
		return newBaseElement;
	}
}
