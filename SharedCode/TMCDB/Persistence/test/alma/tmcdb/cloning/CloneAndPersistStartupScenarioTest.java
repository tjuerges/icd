package alma.tmcdb.cloning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;

import org.exolab.castor.mapping.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FrontEnd;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.domain.TmcdbTestCase;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

/**
 * Tests the cloning (and persisting) of a startup scenario within an existing configuration.
 * @author sharrington
 */
public class CloneAndPersistStartupScenarioTest extends TmcdbTestCase 
{
	/**
	 * Constructor
	 * @param name the name of the test
	 */
	public CloneAndPersistStartupScenarioTest(String name) {
		super(name);
	}
	
	public void testCloningStartupScenarioWithoutBeanlib() throws FileNotFoundException, IOException, MappingException
	{	
		StartupScenario startup = createStartupForCloning();
     
		// ----------------------------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning of a startup scenario
		// ----------------------------------------------------------------------------------------
        
		CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction transaction = session.beginTransaction();
        
        StartupScenario clonedStartup = CloningUtils.cloneStartupScenario(startup, startup.getName());
        
		if(startup == clonedStartup) {
			fail();; // shouldn't happen; object identities == means it's the same object (no actual clone)!
		}
		assertEquals(true, CloningTestUtils.safeEquals(startup, clonedStartup));
		assertEquals(null, clonedStartup.getId());
		
		System.out.println("orig startup id: " + startup.getId() + " name: " + startup.getName() + " and config id: " + startup.getConfiguration().getId());
		System.out.println("cloned startup id: " + clonedStartup.getId() + " name: " + clonedStartup.getName() + " and config id: " + clonedStartup.getConfiguration().getId());
			
		// change the name to avoid uniqueness constraints on name
		clonedStartup.setName("Copy of: " + clonedStartup.getName());
		
		System.out.println("--------------------------------------");
		System.out.println("orig: " + startup);
		System.out.println("--------------------------------------");
		System.out.println("clone: " + clonedStartup);

		session.saveOrUpdate(startup.getConfiguration());
				
		transaction.commit();
		session.close();	
	}
	
	public void testCloningStartupScenarioWithBeanlib() throws FileNotFoundException, IOException, MappingException
	{	
		StartupScenario startup = createStartupForCloning();
        
		// ----------------------------------------------------------------------------------------
		// now that we have a configuration persisted, let's test the cloning of a startup scenario
		// ----------------------------------------------------------------------------------------
        
		CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        Transaction transaction = session.beginTransaction();
        
        StartupScenario clonedStartup = CloningUtils.cloneStartupScenarioWithBeanlib(HibernateUtil.getSessionFactory(), startup, startup.getName());
        
		if(startup == clonedStartup) {
			fail();; // shouldn't happen; object identities == means it's the same object (no actual clone)!
		}

		System.out.println("orig startup id: " + startup.getId() + " name: " + startup.getName() + " and config id: " + startup.getConfiguration().getId());
		System.out.println("cloned startup id: " + clonedStartup.getId() + " name: " + clonedStartup.getName() + " and config id: " + clonedStartup.getConfiguration().getId());
		System.out.println("--------------------------------------");
		System.out.println("orig: " + startup);
		System.out.println("--------------------------------------");
		System.out.println("clone: " + clonedStartup);

		assertEquals(true, CloningTestUtils.safeEquals(startup, clonedStartup));
		assertEquals(null, clonedStartup.getId());
		
		// change the name to avoid uniqueness constraints on name
		clonedStartup.setName("Copy of: " + clonedStartup.getName());
		
		session.saveOrUpdate(startup.getConfiguration());
				
		transaction.commit();
		session.close();	
	}

	private StartupScenario createStartupForCloning()
	{
		Transaction tx = null;
        LruType lru = null;
        HwConfiguration config = null;
        StartupScenario startup = null;
        ComponentType compType = null;
        Antenna antenna = null;
        Pad pad = null;
        BaseElementStartup baseElementStartup = null;
        AssemblyRole assemblyRole = null;
        FrontEnd frontEnd = null;
        Configuration swCfg = null;
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        swCfg = new Configuration();
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        config = new HwConfiguration(swCfg);
        session.save(swCfg);
        session.save(config);
        compType = new ComponentType();
        compType.setIDL("IDL:alma/Control/FOO:1.0");
        session.save(compType);

        lru = new LruType("lru", "lru", "icd", 0L, "", "");
        AssemblyType assemblyType = new AssemblyType("test",
        		"test",
        		BaseElementType.Antenna,
        		"",
        		"",
        		compType,
        		"", "simcode");
        assemblyRole = new AssemblyRole("aRole");
        assemblyType.addRole(assemblyRole);
        lru.addAssemblyType(assemblyType);
        session.save(lru);

        tx.commit();
        session.close();
        
        interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();
        antenna = new Antenna("DV01",
        		AntennaType.ACA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna);
        pad = new Pad("PAD01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
        config.addBaseElement(pad);

        @SuppressWarnings("unused") // actually used in persistence under the covers...
        AntennaToPad a2p = new AntennaToPad(antenna, pad, new Long(0), new Long(0), true);

        frontEnd = new FrontEnd("AFrontEnd", new Long(0));
        config.addBaseElement(frontEnd);

        @SuppressWarnings("unused") // actually used in persistence under the covers...
        AntennaToFrontEnd a2fe = new AntennaToFrontEnd(antenna, frontEnd, new Long(0), new Long(0));

        startup = new StartupScenario("startup");
        config.addStartupScenario(startup);
        baseElementStartup = new BaseElementStartup(antenna, startup);
        baseElementStartup.setSimulated(false);
        startup.addBaseElementStartup(baseElementStartup);

        AssemblyStartup assemblyStartup = new AssemblyStartup(baseElementStartup, assemblyRole);
        assemblyStartup.setSimulated(false);
        HashSet<AssemblyStartup> startups = new HashSet<AssemblyStartup>();
        startups.add(assemblyStartup);
        startup.setAssemblyStartups(startups);

        session.saveOrUpdate(config.getSwConfiguration());
        session.saveOrUpdate(config);
        tx.commit();
        session.close();
        
        return startup;
	}
}
