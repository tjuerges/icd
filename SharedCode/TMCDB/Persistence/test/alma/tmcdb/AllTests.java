package alma.tmcdb;
	
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for alma.tmcdb.domain");
        suite.addTest(alma.tmcdb.domain.AllTests.suite());
        suite.addTest(alma.tmcdb.cloning.AllTests.suite());
        return suite;
    }
}
