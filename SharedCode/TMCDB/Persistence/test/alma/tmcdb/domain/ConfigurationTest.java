/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: ConfigurationTest.java,v 1.9 2011/02/18 00:14:36 sharring Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class ConfigurationTest extends TmcdbTestCase {
    
    private Session session;
    
    public ConfigurationTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @SuppressWarnings("unchecked")
	public void testAddStartupScenario() {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            Transaction transaction = session.beginTransaction();
            
            // --- SW scaffolding ---
            Configuration swCfg;
            swCfg = new Configuration();
            swCfg.setConfigurationName("Test");
            swCfg.setFullName("");
            swCfg.setActive(true);
            swCfg.setCreationTime(new Date());
            swCfg.setDescription("");
            session.save(swCfg);
            
            ComponentType compType = new ComponentType();
            compType.setIDL("IDL:alma/Dodo/Foo:1.0");
            session.save(compType);

            LoggingConfig logCfg = new LoggingConfig();
            session.save(logCfg);
            
            Container cont = new Container();
            cont.setLoggingConfig(logCfg);
            cont.setContainerName("javaContainer");
            cont.setPath("foo/bar");
            cont.setImplLang("java");
            cont.setConfiguration(swCfg);
            swCfg.getContainers().add(cont);
            session.save(cont);
            
            Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, swCfg);
            swCfg.getComponents().add(comp);
            session.save(comp);
            
            // --- end SW scaffolding ---
            
            HwConfiguration config = new HwConfiguration(swCfg);
            StartupScenario startup = new StartupScenario("StartupConfigurationTest");
            config.addStartupScenario(startup);
            session.save(config);
            transaction.commit();
        
        } finally {
        
            Transaction transaction = session.beginTransaction();
            List<HwConfiguration> confs = session.createQuery(
                    "from HwConfiguration where swConfiguration.configurationName = 'ConfigurationTest'").list();
            if (confs.size() > 0) {
                HwConfiguration c = confs.get(0);
                session.delete(c);
            }
            transaction.commit();
        }
        
        session.close();
        HibernateUtil.shutdown();        
    }
    
    public void testCompleteConfiguration() {
        Transaction tx = null;
        LruType lru = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        StartupScenario startup = null;
        Antenna antenna = null;
        Pad pad = null;
        BaseElementStartup baseElementStartup = null;
        AssemblyRole assemblyRole = null;
        AntennaToPad a2p = null;
        FrontEnd frontEnd = null;
        AntennaToFrontEnd a2fe = null;
        
        // Preliminaries, some global objects are needed
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        // --- SW scaffolding ---
        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, config.getSwConfiguration());
        swCfg.getComponents().add(comp);
        session.save(comp);

        // --- end SW scaffolding ---

        lru = new LruType("lru", "lru", "icd", 0L, "", "");
        AssemblyType assemblyType = new AssemblyType("test",
        		"test",
        		BaseElementType.Antenna,
        		"",
        		"",
        		compType,
        		"", "simcode");
        assemblyRole = new AssemblyRole("aRole");
        assemblyType.addRole(assemblyRole);
        lru.addAssemblyType(assemblyType);
        session.save(lru);

        tx.commit();
        session.close();
        
        try {
            interceptor = new CompositeIdentifierInterceptor();
            session = HibernateUtil.getSessionFactory().openSession(interceptor);
            tx = session.beginTransaction();
            antenna = new Antenna("DV01",
                                  AntennaType.ACA,
                                  new Coordinate(0.0, 0.0, 0.0),
                                  new Coordinate(0.0, 0.0, 0.0),
                                  4.5,
                                  0L,
                                  0,
                                  0);
            config.addBaseElement(antenna);
            pad = new Pad("PAD01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
            config.addBaseElement(pad);
            a2p = new AntennaToPad(antenna, pad, new Long(0), new Long(0), true);
            frontEnd = new FrontEnd("AFrontEnd", new Long(0));
            config.addBaseElement(frontEnd);
            a2fe = new AntennaToFrontEnd(antenna, frontEnd, new Long(0), new Long(0));
            startup = new StartupScenario("startup");
            config.addStartupScenario(startup);
            baseElementStartup = new BaseElementStartup(antenna, startup);
            baseElementStartup.setSimulated(true);
            startup.addBaseElementStartup(baseElementStartup);
            AssemblyStartup as = new AssemblyStartup(baseElementStartup, assemblyRole);
            as.setSimulated(true); 
            session.saveOrUpdate(config);
            tx.commit();
            session.close();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            tx.rollback();
            fail();
        }
        
        try {
            // test something here
        } finally {
            // Cleaning
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            session.delete(baseElementStartup); // <-- Necessary to break a circular dependency:
                                                //       AssemblyType needs ComponentType
                                                //       AssemblyStartup needs AssemblyRole
            session.delete(lru);
            session.delete(a2p);
            session.delete(a2fe);
            session.delete(antenna);
            session.delete(frontEnd);
            session.delete(pad);
            session.delete(config);
            ctx.commit();
            session.close();
        }
    }
}

