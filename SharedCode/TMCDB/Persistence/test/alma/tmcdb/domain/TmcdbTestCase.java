package alma.tmcdb.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.jar.JarInputStream;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

import alma.acs.logging.ClientLogManager;

import junit.framework.TestCase;

public class TmcdbTestCase extends TestCase {

    /** Password for connecting to the HSQLDB server */
    public static final String HSQLDB_PASSWORD = "";

    /** Username for connecting to the HSQLDB server */
    public static final String HSQLDB_USER = "sa";

    /** Basic URL for an HSQLDB file-based database */
    public static final String HSQLDB_FILE_URL = "jdbc:hsqldb:file:TMCDB/TMCDB";

    /** Basic URL for an HSQLDB in-memory database */
    public static final String HSQLDB_MEMORY_URL = "jdbc:hsqldb:mem:ignored";
    
    /** JDBC driver for HSQLDB */
    public static final String HSQLDB_JDBC_DRIVER = "org.hsqldb.jdbcDriver";
    
    /** TMCDB Jar library file */
    public static final String TMCDB_JAR_FILE = "TMCDB.jar";
    
    /** HSQLDB DDL script. It should be in TMCDB_JAR_FILE. */
    public static final String HSQLDB_CREATE_SQL_SCRIPT = "CreateHsqldbTables.sql";
    
    /** HSQLDB DB cleaning script. It should be in TMCDB_JAR_FILE. */
    public static final String HSQLDB_DELETE_SQL_SCRIPT = "DropAllTables.sql";

    /* --- new DDL scripts --- */
    
    /** Base path, relative to $ACSDATA, for the DDL SQL scripts */
    public static final String HSQLDB_DDL_BASE_PATH = "/config/DDL/hsqldb/";

    public static final String HSQLDB_SWCORE_CREATE_SQL_SCRIPT = "TMCDB_swconfigcore/CreateHsqldbTables.sql";
    public static final String HSQLDB_SWEXT_CREATE_SQL_SCRIPT = "TMCDB_swconfigext/CreateHsqldbTables.sql";
    public static final String HSQLDB_HWMON_CREATE_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/CreateHsqldbTables.sql";

    public static final String HSQLDB_SWCORE_DELETE_SQL_SCRIPT = "TMCDB_swconfigcore/DropAllTables.sql";
    public static final String HSQLDB_SWEXT_DELETE_SQL_SCRIPT = "TMCDB_swconfigext/DropAllTables.sql";
    public static final String HSQLDB_HWMON_DELETE_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/DropAllTables.sql";

    protected Logger logger = ClientLogManager.getAcsLogManager().getLoggerForApplication(this.getClass().getSimpleName(), false);

    public TmcdbTestCase(String name) {
        super(name);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        Class.forName(HSQLDB_JDBC_DRIVER);
        String ddl1 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWCORE_CREATE_SQL_SCRIPT));
        String ddl2 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWEXT_CREATE_SQL_SCRIPT));
        String ddl3 = readConfigFile(findAcsDataConfigFile(HSQLDB_HWMON_CREATE_SQL_SCRIPT));
        String del1 = readConfigFile(findAcsDataConfigFile(HSQLDB_HWMON_DELETE_SQL_SCRIPT));
        String del2 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWEXT_DELETE_SQL_SCRIPT));
        String del3 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWCORE_DELETE_SQL_SCRIPT));
        
        Connection conn = DriverManager.getConnection(HSQLDB_MEMORY_URL, 
                HSQLDB_USER, 
                HSQLDB_PASSWORD);
        try {
            runScript(del1, conn);
            runScript(del2, conn);
            runScript(del3, conn);
        } catch ( Exception ex ) {} // fine, tables have not been constructed yet
        runScript(ddl1, conn);
        runScript(ddl2, conn);
        runScript(ddl3, conn);
        conn.close();
    }

    protected void tearDown() throws Exception {
        String del1 = readConfigFile(findAcsDataConfigFile(HSQLDB_HWMON_DELETE_SQL_SCRIPT));
        String del2 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWEXT_DELETE_SQL_SCRIPT));
        String del3 = readConfigFile(findAcsDataConfigFile(HSQLDB_SWCORE_DELETE_SQL_SCRIPT));
        Connection conn = DriverManager.getConnection(HSQLDB_MEMORY_URL, 
                HSQLDB_USER, 
                HSQLDB_PASSWORD);
        runScript(del1, conn);
        runScript(del2, conn);
        runScript(del3, conn);
        conn.close();
        super.tearDown();
    }
    
    /**
     * Execute an SQL script.
     * @param script  The SQL script, as a single string
     * @param conn    Connection to the DB server
     * @throws SQLException
     */
    protected void runScript( String script, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement();
        String[] statements = script.split( ";", -1 );
        for( int i = 0; i < statements.length; i++ ) {
            String statement = statements[i].trim();
            if( statement.length() == 0 ) {
                // skip empty lines
                continue;
            }
            stmt.execute( statement );
        }
    }
    
    /**
     * Searches for a library file in ACS library locations, first in ACSROOT
     * and second in INTROOT.
     * @param lib Library name
     * @return File path to the library, null if it is not in ACS library locations
     */
    protected String findAcsLibrary(String lib) {
        String[] acsDirs = new String[] {"INTROOT", "ACSROOT"};
        for (String d : acsDirs) {
            String dir = System.getenv(d);
            if (dir != null) {
                String jar = dir + "/lib/" + lib;
                File f = new File(jar);
                if (f.exists()) return jar;
            }            
        }
        return null;
    }
    
    /**
     * Extracts a text file from a Jar library. Returns its contents as a string.
     * 
     * @param jar Jar library
     * @param file File to read from the Jar file
     * @return file contents
     * @throws IOException
     */
    protected String readFileFromJar(String jar, String file)
        throws IOException {
        FileInputStream in = new FileInputStream(findAcsLibrary(jar));
        JarInputStream jarin = new JarInputStream(in);
        ZipEntry ze = jarin.getNextEntry();
        while (ze != null) {
            if (ze.getName().equals(file))
                break;
            ze = jarin.getNextEntry();
        }
        InputStreamReader converter = new InputStreamReader(jarin);
        BufferedReader reader = new BufferedReader(converter);

        StringBuffer ddlbuff = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            ddlbuff.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return new String(ddlbuff);
    }
    
    /**
     * Searches for a library file in ACS config locations, first in INTROOT
     * and second in ACSROOT.
     * @param file Configuration file name
     * @return File path to the configuration file, null if it is not in
     * ACS configuration locations
     */
    protected String findAcsConfigFile(String file) {
        String[] acsDirs = new String[] {"INTROOT", "ACSROOT"};
        for (String d : acsDirs) {
            String dir = System.getenv(d);
            if (dir != null) {
                String cfgf = dir + "/config/" + file;
                File f = new File(cfgf);
                if (f.exists()) return cfgf;
            }            
        }
        return null;        
    }
    
    protected String findAcsDataConfigFile(String file) {
        String dir = System.getenv("ACSDATA");
        if (dir != null) {
            String cfgf = dir + HSQLDB_DDL_BASE_PATH + file;
            File f = new File(cfgf);
            if (f.exists()) return cfgf;
        }            
        return null;        
    }
    
    /**
     * Read a configuration file from ACS standard configuration locations.
     * Returns the file contents as a String.
     * 
     * @param file Configuratio file name
     * @return file contents
     * @throws IOException
     */
    protected String readConfigFile(String file)
        throws IOException {
        FileInputStream in = new FileInputStream(file);
        InputStreamReader converter = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(converter);
        
        StringBuffer ddlbuff = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            ddlbuff.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return new String(ddlbuff);
    }
}
