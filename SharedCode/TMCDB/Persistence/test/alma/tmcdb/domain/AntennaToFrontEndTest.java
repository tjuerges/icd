/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AntennaToFrontEndTest.java,v 1.4 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.acs.tmcdb.ComponentType;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FrontEnd;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class AntennaToFrontEndTest extends TmcdbTestCase {
    
    public AntennaToFrontEndTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAssociateAntennaToFrontEnd() {
        
        Transaction tx = null;
        HwConfiguration config = null;
        Antenna antenna = null;
        FrontEnd frontEnd = null;
        AntennaToFrontEnd a2fe = null;
        Configuration swCfg = null;
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction(); 

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---

        antenna = new Antenna("DV01",
        		AntennaType.ACA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna);
        frontEnd = new FrontEnd("AFrontEnd", new Long(0));
        config.addBaseElement(frontEnd);
        a2fe = new AntennaToFrontEnd(antenna, frontEnd, new Long(0), new Long(0));
        session.save(config);
        tx.commit();
        session.close();
        
        try {
            // Test something here...
        } finally {
            // Cleaning
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            antenna.getScheduledFrontEnds().remove(a2fe);
            frontEnd.getScheduledAntennaInstallations().remove(a2fe);
            session.delete(a2fe);
            session.delete(antenna);
            session.delete(frontEnd);
            session.delete(config);
            ctx.commit();
            session.close();
        }
    }    

}
