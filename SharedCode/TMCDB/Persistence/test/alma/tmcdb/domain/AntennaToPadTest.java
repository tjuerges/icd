/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AntennaToPadTest.java,v 1.8 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class AntennaToPadTest extends TmcdbTestCase {

    public AntennaToPadTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAssociateAntennaToPad() {
        Transaction tx = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna antenna = null;
        Pad pad = null;
        AntennaToPad a2p = null;
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---

        antenna = new Antenna("DV01", AntennaType.AEC,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0), 4.5, 0L, 0, 0);
        config.addBaseElement(antenna);
        pad = new Pad("PAD01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
        config.addBaseElement(pad);
        a2p = new AntennaToPad(antenna, pad, new Long(0), new Long(0), true);
        session.save(config);
        tx.commit();
        session.close();
        
        try {
            // Test something here
        } finally {
            // Deleting the a2p, just to be sure it can be deleted alone
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            antenna.getScheduledPadLocations().remove(a2p);
            pad.getScheduledAntennas().remove(a2p);
            session.delete(a2p);
            ctx.commit();
            // Deleting the rest
            ctx = session.beginTransaction();
            session.delete(antenna);
            session.delete(pad);
            session.delete(config);
            ctx.commit();
            session.close();
        }
    }    
    
    public void testAssociateAntennaToPadAndUpdateEndTime() 
    {
        Transaction tx = null;
        Configuration swCfg = null;
        HwConfiguration config = null;
        Antenna antenna = null;
        Pad pad = null;
        AntennaToPad a2p = null;
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, config.getSwConfiguration());
        swCfg.getComponents().add(comp);
        session.save(comp);

        // --- end SW scaffolding ---

        antenna = new Antenna("DV01", AntennaType.AEC,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0), 4.5, 0L, 0, 0);
        config.addBaseElement(antenna);
        pad = new Pad("PAD01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
        config.addBaseElement(pad);
        a2p = new AntennaToPad(antenna, pad, new Long(0), new Long(0), true);
        session.save(config);
        tx.commit();
        session.close();
        
        try {
        	// now let's update the antenna to pad end time, persist it, and see if it worked.
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            
            System.out.println("original a2p end time for a2p with antenna id of:  " + a2p.getAntenna().getId() 
            		+ " and pad id of: " + a2p.getPad().getId() + " is: " + a2p.getEndTime());
            
            session.update(a2p);
            
            a2p.setEndTime(System.currentTimeMillis());
            
            System.out.println("updated a2p end time for a2p with antenna id of:  " + a2p.getAntenna().getId() 
            		+ " and pad id of: " + a2p.getPad().getId() + " is: " + a2p.getEndTime());
            
            // commit transaction, flush session, close session; new end time should be persisted!
            ctx.commit();
            session.flush();
            session.close();
            
            // verify that the end time was properly persisted
            Session session2 = HibernateUtil.getSessionFactory().openSession();
            ctx = session2.beginTransaction();
            Antenna antennaRead = (Antenna)session2.get(Antenna.class, antenna.getId());
            AntennaToPad a2pRead = antennaRead.getScheduledPadLocations().iterator().next();
            
            System.out.println("after re-reading from db, a2p end time for a2p with antenna id of:  " + a2p.getAntenna().getId() 
            		+ " and pad id of: " + a2p.getPad().getId() + " is: " + a2p.getEndTime());
            
            assertEquals(a2pRead.getEndTime(), a2p.getEndTime());
            ctx.commit();
            session2.close();
        } finally {
        	// Deleting the a2p, just to be sure it can be deleted alone
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            antenna.getScheduledPadLocations().remove(a2p);
            pad.getScheduledAntennas().remove(a2p);
            session.delete(a2p);
            ctx.commit();
            // Deleting the rest
            ctx = session.beginTransaction();
            session.delete(antenna);
            session.delete(pad);
            session.delete(config);
            ctx.commit();
            session.close();
        }
    } 
}
