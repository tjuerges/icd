/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AssemblyStartupTest.java,v 1.10 2011/02/18 00:14:36 sharring Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaType;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.domain.StartupScenario;
import alma.tmcdb.utils.CompositeIdentifierInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class AssemblyStartupTest extends TmcdbTestCase {

    public AssemblyStartupTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testCreateAssemblyStartup() {
        Transaction tx = null;
        LruType lru = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        StartupScenario startup = null;
        Antenna antenna = null;
        AssemblyStartup assemblyStartup3 = null;
        BaseElementStartup baseElementStartup = null;
        AssemblyRole assemblyRole = null;
        AssemblyRole assemblyRole2 = null;
        AssemblyRole assemblyRole3 = null;
        
        // Preliminaries, some global objects are needed
        CompositeIdentifierInterceptor interceptor = new CompositeIdentifierInterceptor();
        Session session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---            

        session.save(config);

        lru = new LruType("lru", "lru", "icd", 0L, "", "");
        AssemblyType assemblyType = new AssemblyType("test",
        		"test",
        		BaseElementType.Antenna,
        		"",
        		"",
        		compType,
        		"", "simcode");
        assemblyRole = new AssemblyRole("aRole");
        assemblyType.addRole(assemblyRole);
        assemblyRole2 = new AssemblyRole("aRole2");
        assemblyType.addRole(assemblyRole2);
        assemblyRole3 = new AssemblyRole("aRole3");
        assemblyType.addRole(assemblyRole3);
        lru.addAssemblyType(assemblyType);
        session.save(lru);

        tx.commit();
        session.close();
        
        interceptor = new CompositeIdentifierInterceptor();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        tx = session.beginTransaction();
        antenna = new Antenna("DV01",
        		AntennaType.ACA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna); 
        startup = new StartupScenario("startup");
        config.addStartupScenario(startup);
        baseElementStartup = new BaseElementStartup(antenna, startup);
        baseElementStartup.setSimulated(false);
        startup.addBaseElementStartup(baseElementStartup);
        AssemblyStartup as1 = new AssemblyStartup(baseElementStartup, assemblyRole);
        as1.setSimulated(false);
        AssemblyStartup as2 = new AssemblyStartup(baseElementStartup, assemblyRole2);
        as2.setSimulated(false);
        assemblyStartup3 = new AssemblyStartup(baseElementStartup, assemblyRole3);
        assemblyStartup3.setSimulated(false);
        assertEquals(3, baseElementStartup.getAssemblyStartups().size());
        baseElementStartup.getAssemblyStartups().remove(assemblyStartup3);
        assertEquals(2, baseElementStartup.getAssemblyStartups().size());
        assemblyStartup3 = new AssemblyStartup(baseElementStartup, assemblyRole3);
        assemblyStartup3.setSimulated(false);
        assertEquals(3, baseElementStartup.getAssemblyStartups().size());
        baseElementStartup.getAssemblyStartups().remove(assemblyStartup3);
        assertEquals(2, baseElementStartup.getAssemblyStartups().size());
        session.saveOrUpdate(config);
        tx.commit();
        session.close();
        
        try {
            // Iterate through
            System.out.println("Iterating through...");
            Set<BaseElementStartup> baseElementStartups = startup.getBaseElementStartups();
            System.out.println(baseElementStartups.size() + " base element startup(s) found.");
            for (Iterator<BaseElementStartup> iter = baseElementStartups.iterator(); iter.hasNext(); ) {
                BaseElementStartup bes = iter.next();
                Set<AssemblyStartup> startupAssemblies = bes.getAssemblyStartups();
                System.out.println(startupAssemblies.size() + " startup assemblies found.");
                for (Iterator<AssemblyStartup> iter2 = startupAssemblies.iterator(); iter2.hasNext(); ) {
                    AssemblyStartup as = iter2.next();
                    System.out.println("Assembly Role: " + as.getAssemblyRole().getName());
                }
            }            
        } finally {
            // Cleaning
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction ctx = session.beginTransaction();
            session.delete(baseElementStartup); // <-- Necessary to break a circular dependency:
                                                //       AssemblyType needs ComponentType
                                                //       AssemblyStartup needs AssemblyRole
            session.delete(lru);
            session.delete(antenna);
            session.delete(config);
            ctx.commit();
            session.close();
        }
    }
}
