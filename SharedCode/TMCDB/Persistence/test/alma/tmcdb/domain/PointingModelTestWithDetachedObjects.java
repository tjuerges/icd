/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.HibernateUtil;

public class PointingModelTestWithDetachedObjects extends TmcdbTestCase {

    private Session session;
    
    public PointingModelTestWithDetachedObjects(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAddPointingModel() 
    {
        Transaction creationTransaction = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna antenna = null;
        Pad newPad = null;
        AntennaToPad a2p = null;
        // save some things into the DB
        creationTransaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding --- 

        config = new HwConfiguration(swCfg);
        antenna = new Antenna("DV01",
        		AntennaType.VA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna);
        newPad = new Pad("PAD01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
        config.addBaseElement(newPad);
        session.save(config);
        session.flush();

        a2p = new AntennaToPad(antenna, newPad, new Long(0), null, true);

//        pm = new PointingModel("band6", new Long(0), new Long(0),
//        "uid://X0/X0/X0");
//        pm.getTerms().put("IAC", new Term(1.2341f, 0.00001f));
//        pm.getTerms().put("IAC0", new Term(1.2341f, 0.00001f));
//        pm.getTerms().put("IAC1", new Term(1.2341f, 0.00001f));
//        pm.getTerms().put("IAC2", new Term(1.2341f, 0.00001f));
//        pm.getTerms().put("IAC3", new Term(1.2341f, 0.00001f));
//        a2p.addPointingModel(pm);
//
//        // save, commit, and close the session; after the session is closed the objects 
//        // are in "detached" state from hibernate's perspective
//        session.save(a2p);
//        creationTransaction.commit();
//        session.close();
//
//        // start a new session, working with (initially) detached objects
//        session = HibernateUtil.getSessionFactory().openSession();
//        creationTransaction = session.beginTransaction();
//
//        // session.update will reattach the detached objects
//        // (as well as schedule an update to the db)
//        session.update(a2p);
//
//        // now, add a new pointing model to the reattached AntennaToPad object
//        pm2 = new PointingModel("band1",
//        		new Date(),
//        		null,
//        		"uid://X0/X0/X0");
//        pm2.getTerms().put("IAC2", new Term(1.2341f, 0.00001f));
//        pm2.getTerms().put("IAC3", new Term(1.2341f, 0.00001f));
//        pm2.setA2p(a2p);
//        a2p.addPointingModel(pm2);

        // update the configuration, which should cascade down the hierarchy
        session.update(config);

        creationTransaction.commit();
        
        try {
            Pad pad = antenna.getCurrentPad();
            assertNotNull(pad);

            session.close();
            session = HibernateUtil.getSessionFactory().openSession();

            // verify that the new pointing model was saved
            Transaction verificationTransaction = session.beginTransaction();
            AntennaToPad antToPad = (AntennaToPad)session.load(AntennaToPad.class, a2p.getAntennaToPadId());
            assertNotNull(antToPad);

            verificationTransaction.commit();
            session.close();
        } finally {
            // Cleaning
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction cleaningTransaction = session.beginTransaction();
            session.delete(a2p);
            session.delete(antenna);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }    
}
