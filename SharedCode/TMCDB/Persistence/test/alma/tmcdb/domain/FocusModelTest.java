/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: FocusModelTest.java,v 1.6 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.HibernateUtil;

public class FocusModelTest extends TmcdbTestCase {
    private Session session;

    public FocusModelTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }

    public void testAddFocusModel() {
        Transaction creationTransaction = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna newAntenna = null;
        FocusModel fm = null;
        creationTransaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, config.getSwConfiguration());
        swCfg.getComponents().add(comp);
        session.save(comp);

        // --- end SW scaffolding ---
        
        newAntenna = new Antenna("DV01",
        		AntennaType.VA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0), 4.5, 0L,  0, 0);
        config.addBaseElement(newAntenna);
        session.save(config);
        session.flush();
        fm = new FocusModel(newAntenna);
        fm.getTerms().put("FOO", new FocusModelCoeff(1.2341f));
        fm.getTerms().get("FOO").getOffsets().put(ReceiverBand.ALMA_RB_01, 0.3);
        session.save(fm);

        creationTransaction.commit();

        try {
            // Iterate through
            System.out.println("Test something here...");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            newAntenna.getFocusModels().remove(fm);
            session.delete(fm);
            session.delete(newAntenna);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }
}
