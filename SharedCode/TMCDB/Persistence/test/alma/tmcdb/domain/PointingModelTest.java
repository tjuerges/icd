/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: PointingModelTest.java,v 1.12 2010/12/17 00:07:22 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.history.BackLog;
import alma.tmcdb.history.HistoryRecord;
import alma.tmcdb.history.PointingModelCoeffBackLog;
import alma.tmcdb.history.PointingModelCoeffOffsetBackLog;
import alma.tmcdb.history.PointingModelHistorian;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor;
import alma.tmcdb.utils.HibernateUtil;

public class PointingModelTest extends TmcdbTestCase {

    private Session session;
    private VersionKeeperInterceptor interceptor = new VersionKeeperInterceptor();
    
    public PointingModelTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
//        session = HibernateUtil.getSessionFactory().openSession();
        session = HibernateUtil.getSessionFactory().openSession(interceptor);
        interceptor.setSession(session);
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAddPointingModel() {
        Transaction tx = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna antenna = null;
        PointingModel pm = null;
        
        PointingModelHistorian historian = new PointingModelHistorian(session);
                
        tx = session.beginTransaction();

        // --- SW scaffolding ---

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---
        
        interceptor.createNewVersion("lala", "Original version");

        antenna = new Antenna("DV01",
                AntennaType.VA,
                new Coordinate(0.0, 0.0, 0.0),
                new Coordinate(0.0, 0.0, 0.0), 4.5, 0L,  0, 0);
        config.addBaseElement(antenna);
        session.save(config);
        session.flush();
        pm = new PointingModel(antenna);
        pm.addTerm("FOO", new PointingModelCoeff("FOO", 1.2341f));
        pm.addTerm("BAR", new PointingModelCoeff("BAR", 2.3f));
        pm.getTerms().get("FOO").getOffsets().put(ReceiverBand.ALMA_RB_02, 0.3);
        pm.getTerm("FOO").setValue(3.14f);
        session.save(pm);
        tx.commit();
        session.flush();
        
//        tx = session.beginTransaction();
//        PointingModelCoeff coeff = pm.getTerms().remove("BAR");
//        session.delete(coeff);
//        session.save(pm);
//        tx.commit();
//        session.flush();

        interceptor.createNewVersion("lalo", "Several updates");

        tx = session.beginTransaction();
        pm.getTerms().get("FOO").setValue(6.32f);
        session.saveOrUpdate(pm);
        tx.commit();
        session.flush();

        tx = session.beginTransaction();
        pm.getTerm("FOO").getOffsets().remove(ReceiverBand.ALMA_RB_02);
        session.saveOrUpdate(pm);
        tx.commit();
        session.flush();
        
        tx = session.beginTransaction();
        pm.getTerms().get("FOO").getOffsets().put(ReceiverBand.ALMA_RB_02, 0.4);
        tx.commit();

        interceptor.createNewVersion("gaga", "Even more updates");

        tx = session.beginTransaction();
        pm.addTerm("HACA", new PointingModelCoeff("HACA", 1.0f));
        pm.addTerm("HACA1", new PointingModelCoeff("HACA1", 2.0f));
        pm.addTerm("IEC", new PointingModelCoeff("IEC", 3.0f));
        PointingModelCoeff coeff = pm.getTerms().remove("FOO");
        session.delete(coeff);
        session.save(pm);
        tx.commit();
        
        try {            
            System.out.println("full backlog table -------------------");
            List<BackLog> backlogs = historian.getHistoricRecords(0, pm);
            for (BackLog bl : backlogs) {
                System.out.print("" + bl.getVersion() + " " + bl.getModTime() + " " +
                        bl.getOperation() + " " + bl.getWho() + " " + bl.getDescription() + " ");
                if (bl instanceof PointingModelCoeffBackLog) {
                    PointingModelCoeffBackLog pmcbl = (PointingModelCoeffBackLog)bl;
                    System.out.println(pmcbl.getName() + " " + pmcbl.getValue());
                } else if (bl instanceof PointingModelCoeffOffsetBackLog) {
                    PointingModelCoeffOffsetBackLog pmcobl = (PointingModelCoeffOffsetBackLog)bl;
                    System.out.println(pmcobl.getName() + " " + pmcobl.getReceiverBand() + " " +
                            pmcobl.getOffset());                    
                }
            }
            
            System.out.println("historical record table ---------------");
            List<HistoryRecord> histRecords = historian.getHistory(pm);
            for (HistoryRecord hr : histRecords) {
                System.out.println("" + hr.getVersion() + " " + hr.getTimestamp() + " " +
                        hr.getModifier() + " " + hr.getDescription());
            }
            
            System.out.println("recreating previous version -----------");
            PointingModel pastPointingModel = historian.recreate(1, pm);
            System.out.println(pastPointingModel.toString());
            
        } finally {
            // Cleaning
            tx = session.beginTransaction();
            session.delete(pm);
            session.delete(antenna);
            session.delete(config);
            tx.commit();
        }
    }
}
