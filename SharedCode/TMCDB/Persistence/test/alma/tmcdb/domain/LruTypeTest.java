/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.tmcdb.domain.LruType;
import alma.tmcdb.utils.HibernateUtil;

public class LruTypeTest extends TmcdbTestCase {

    private Session session;
    
    public LruTypeTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }

    public void testLruType() {
        
        Transaction creationTransaction = null;
        LruType lru = null;
        try {
            creationTransaction = session.beginTransaction();
            lru = new LruType("TestLRU", "TestLRU", "ICD XXX", 0L, "La donna e mobile",
                    "Qual piuma al vento");
            session.save(lru);
            creationTransaction.commit();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            creationTransaction.rollback();
            fail("An exception has been thrown so the transaction has been rolled back");
        }
        
        try {
            System.out.println("Test something here...");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            session.delete(lru);
            cleaningTransaction.commit();
        }
    }
}
