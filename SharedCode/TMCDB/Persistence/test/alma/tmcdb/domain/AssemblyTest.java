/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.domain.Assembly;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.utils.HibernateUtil;

public class AssemblyTest extends TmcdbTestCase {

    private Session session;
    
    public AssemblyTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }
    
    public void testAssembly() {
        
        Transaction creationTransaction = null;
        LruType lru = null;
        Configuration swCfg = null;
        HwConfiguration config = null;
        Assembly newAssembly = null;
        creationTransaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, config.getSwConfiguration());
        swCfg.getComponents().add(comp);
        session.save(comp);

        // --- end SW scaffolding ---            

        lru = new LruType("TestLRU", "TestLRU", "ICD XXX", 0L, "La donna e mobile",
        "Qual piuma al vento");

        session.save(config);

        AssemblyType assemblyType = new AssemblyType("Test", "Test", BaseElementType.Antenna,
        		"Muta d'accento - e di pensiero.", "Sempre un amabile,",
        		compType, "code", "simcode");

        AssemblyRole assemblyRole = new AssemblyRole("aRole");
        assemblyType.addRole(assemblyRole);

        lru.addAssemblyType(assemblyType);            

        session.save(lru);

        newAssembly = new Assembly("0x001", "<data/>", assemblyType);
        config.addAssembly(newAssembly);
        session.save(config);

        creationTransaction.commit();
        
        try {
            System.out.println("Test something here...");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            session.delete(newAssembly);
            session.delete(lru);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }
}
