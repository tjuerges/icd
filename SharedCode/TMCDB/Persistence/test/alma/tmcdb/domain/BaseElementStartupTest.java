/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: BaseElementStartupTest.java,v 1.6 2011/02/18 00:14:36 sharring Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.HibernateUtil;

public class BaseElementStartupTest extends TmcdbTestCase {
    
    private Session session;

    public BaseElementStartupTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();
        super.tearDown();
    }

    @SuppressWarnings("unchecked")
	public void notestNavigateBaseElementStartup() {
        Transaction newTransaction = session.beginTransaction();
        
        // Get a Configuration. The first found is fine. 
        List<HwConfiguration> configurations = session.createQuery("from HwConfiguration").list();
        System.out.println(configurations.size() + " configuration(s) found.");
        HwConfiguration config = (HwConfiguration) configurations.get(0);
        
        Set<StartupScenario> startupConfigs = config.getStartupScenarios();
        System.out.println(startupConfigs.size() + " startup configuration(s) found:");
        for (Iterator iter = startupConfigs.iterator(); iter.hasNext(); ) {
            StartupScenario sc = (StartupScenario) iter.next();
            System.out.println(sc.getName());
            Set bss = sc.getBaseElementStartups();
            System.out.println(bss.size() + " base element startup found:");
            for (Iterator iter2 = bss.iterator(); iter2.hasNext(); ) {
                BaseElementStartup baseElementStartup = (BaseElementStartup)
                    iter2.next();
                System.out.println(baseElementStartup.getBaseElement().getName());
            }
        }
        
        newTransaction.commit();
    }
    
    @SuppressWarnings("unchecked")
	public void notestNavigateToAssemblyStartup() {
        Transaction newTransaction = session.beginTransaction();
        
        // Get a Configuration. The first found is fine. 
        List<HwConfiguration> configurations = session.createQuery("from Configuration").list();
        System.out.println(configurations.size() + " configuration(s) found.");
        HwConfiguration config = (HwConfiguration) configurations.get(0);
        
        Set<StartupScenario> startupConfigs = config.getStartupScenarios();
        System.out.println(startupConfigs.size() + " startup configuration(s) found:");
        for (Iterator<StartupScenario> iter = startupConfigs.iterator(); iter.hasNext(); ) {
            StartupScenario sc = iter.next();
            System.out.println(sc.getName());
            Set<BaseElementStartup> bss = sc.getBaseElementStartups();
            System.out.println(bss.size() + " base element startup found:");
            for (Iterator<BaseElementStartup> iter2 = bss.iterator(); iter2.hasNext(); ) {
                BaseElementStartup baseElementStartup = iter2.next();
                System.out.println(baseElementStartup.getBaseElement().getName());
                Set assemblyStartups = baseElementStartup.getAssemblyStartups();
                System.out.println(assemblyStartups.size() + " assembly startups found.");
            }
        }
        
        newTransaction.commit();        
    }
    
    public void testAddBaseElementStartup() {
        Transaction creationTransaction = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna antenna = null;
        StartupScenario startup = null;
        BaseElementStartup beStartup = null;
        creationTransaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        Component comp = CloningTestUtils.createComponent("FOO", "/FOO", compType, config.getSwConfiguration());
        swCfg.getComponents().add(comp);
        session.save(comp);

        // --- end SW scaffolding ---            

        antenna = new Antenna("DV01",
        		AntennaType.VA,
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna);
        startup = new StartupScenario("StartupConfigurationTest");
        config.addStartupScenario(startup);
        beStartup = new BaseElementStartup(antenna, startup);
        beStartup.setSimulated(false);

        // Add a generic front-end base element startup to the antenna
        // base element startup.
        BaseElementStartup child =
        	new BaseElementStartup(BaseElementStartupType.FrontEnd);
        child.setParent(beStartup);
        child.setSimulated(false);
        beStartup.getChildren().add(child);

        session.save(config);
        creationTransaction.commit();
        
        try {
            System.out.println("Test something here...");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            // Care needs to be taken with unidireccional many-to-one associations.
            // The many side needs to be deleted first.
            // In this test we have this problem with Antenna to Component; and
            // Component to ComponentType.
            session.delete(startup);
            session.delete(antenna);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }
}
