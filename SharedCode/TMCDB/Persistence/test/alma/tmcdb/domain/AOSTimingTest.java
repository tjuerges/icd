/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AOSTimingTest.java,v 1.3 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.Iterator;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.utils.HibernateUtil;

public class AOSTimingTest extends TmcdbTestCase {
    
    private Session session;
    
    public AOSTimingTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();        
        super.tearDown();
    }

    public void testAOSTiming() {
        Transaction transaction = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        AOSTiming aost = null;
        try {
            transaction = session.beginTransaction();

            // --- SW scaffolding ---
            
            swCfg = new Configuration();
            swCfg.setConfigurationName("Test");
            swCfg.setFullName("");
            swCfg.setActive(true);
            swCfg.setCreationTime(new Date());
            swCfg.setDescription("");
            session.save(swCfg);
            
            ComponentType compType = new ComponentType();
            compType.setIDL("IDL:alma/Dodo/Foo:1.0");
            session.save(compType);

            LoggingConfig logCfg = new LoggingConfig();
            session.save(logCfg);
            
            Container cont = new Container();
            cont.setLoggingConfig(logCfg);
            cont.setContainerName("javaContainer");
            cont.setPath("foo/bar");
            cont.setImplLang("java");
            cont.setConfiguration(swCfg);
            swCfg.getContainers().add(cont);
            session.save(cont);
            
            // --- end SW scaffolding ---            
            
            config = new HwConfiguration(swCfg);
            aost = new AOSTiming("AOSTiming", new Long(0));
            config.addBaseElement(aost);
            
            StartupScenario startup = new StartupScenario("StartupConfigurationTest");
            config.addStartupScenario(startup);
            new BaseElementStartup(aost, startup);

            session.save(config);            
            transaction.commit();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            transaction.rollback();
            fail("An exception has been thrown so the transaction has been rolled back");
        }
        
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Iterator<StartupScenario> iter = config.getStartupScenarios().iterator();
            StartupScenario startup = iter.next();
            assertEquals(1, startup.getBaseElementStartups().size());
            BaseElementStartup bes = startup.getBaseElementStartups().iterator().next();
            System.out.println("type: " + bes.getType());
            tx.commit();
        } catch (Exception ex) { 
            ex.printStackTrace();
            tx.rollback();
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            // Care needs to be taken with unidireccional many-to-one associations.
            // The many side needs to be deleted first.
            // In this test we have this problem with Antenna to Component; and
            // Component to ComponentType.
            session.delete(aost);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }    
}
