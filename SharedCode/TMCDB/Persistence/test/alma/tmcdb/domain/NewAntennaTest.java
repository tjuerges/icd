/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: NewAntennaTest.java,v 1.5 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.utils.HibernateUtil;

public class NewAntennaTest extends TmcdbTestCase {

    private Session session;
    
    public NewAntennaTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();        
        super.tearDown();
    }
    
    public void testAddAntenna() {
        Transaction transaction = null;
        Configuration cfg = null;
        HwConfiguration hwCfg = null;
        Antenna antenna = null;
        try {
            transaction = session.beginTransaction();
            
            cfg = new Configuration();
            cfg.setConfigurationName("Test");
            cfg.setFullName("");
            cfg.setActive(true);
            cfg.setCreationTime(new Date());
            cfg.setDescription("");
            session.save(cfg);
            
            ComponentType compType = new ComponentType();
            compType.setIDL("IDL:alma/Dodo/Foo:1.0");
            session.save(compType);

            LoggingConfig logCfg = new LoggingConfig();
            session.save(logCfg);
            
            Container cont = new Container();
            cont.setLoggingConfig(logCfg);
            cont.setContainerName("javaContainer");
            cont.setPath("foo/bar");
            cont.setImplLang("java");
            cont.setConfiguration(cfg);
            cfg.getContainers().add(cont);
            session.save(cont);
            
            hwCfg = new HwConfiguration(cfg);

            antenna = new Antenna("DV01",
                    AntennaType.AEC, 
                    new Coordinate(0.0, 0.0, 0.0),
                    new Coordinate(0.0, 0.0, 0.0),
                    4.5,
                    0L,
                    0,
                    0);
            hwCfg.addBaseElement(antenna);
            session.save(hwCfg);
            
            transaction.commit();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            transaction.rollback();
            fail("An exception has been thrown so the transaction has been rolled back");
        }
        
        try {
            logger.info("something here");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            // Care needs to be taken with unidireccional many-to-one associations.
            // The many side needs to be deleted first.
            // In this test we have this problem with Antenna to Component; and
            // Component to ComponentType.
//            session.delete(antenna);
//            session.delete(component);
            session.delete(hwCfg);
            cleaningTransaction.commit();
        }
    }    
}
