package alma.tmcdb.domain;
	
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for alma.tmcdb.domain");
        suite.addTestSuite(AntennaTest.class);
        suite.addTestSuite(AntennaToFrontEndTest.class);
        suite.addTestSuite(AntennaToPadTest.class);
        suite.addTestSuite(AssemblyStartupTest.class);
        suite.addTestSuite(AssemblyTest.class);
        suite.addTestSuite(AssemblyTypeTest.class);
        suite.addTestSuite(BaseElementStartupTest.class);
        suite.addTestSuite(ConfigurationTest.class);
        suite.addTestSuite(FocusModelTest.class);
        suite.addTestSuite(FrontEndTest.class);
        suite.addTestSuite(CentralLOTest.class);
        suite.addTestSuite(LruTypeTest.class);
        suite.addTestSuite(PadTest.class);
        suite.addTestSuite(PointingModelTest.class);
        // suite.addTestSuite(PointingModelTestWithDetachedObjects.class);
        return suite;
    }
}
