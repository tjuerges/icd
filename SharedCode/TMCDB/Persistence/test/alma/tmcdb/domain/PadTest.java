/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.utils.HibernateUtil;

public class PadTest extends TmcdbTestCase {

    private Session session;
    
    public PadTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    @SuppressWarnings("unchecked")
	protected void tearDown() throws Exception {
        Transaction transaction = session.beginTransaction();
        List<HwConfiguration> confs = session.createQuery(
                "from HwConfiguration where swConfiguration.configurationName = 'ConfigurationTest'").list();
        if (confs.size() > 0) {
            HwConfiguration c = confs.get(0);
            session.delete(c);
        }
        transaction.commit();
        session.close();
        HibernateUtil.shutdown();        
        super.tearDown();
    }

    public void testCreatePad() {
        Transaction transaction = session.beginTransaction();
        
        // --- SW scaffolding ---
        Configuration swCfg;
        swCfg = new Configuration();
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);
        // --- end SW scaffolding ---
        
        
        HwConfiguration config = new HwConfiguration(swCfg);
        Pad newPad = new Pad("Pad01", new Coordinate(0.0, 0.0, 0.0), new Long(0));
        config.addBaseElement(newPad);
        session.save(config);
        
        transaction.commit();        
    }
}
