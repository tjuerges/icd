/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AntennaTest.java,v 1.9 2010/10/27 20:22:38 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import alma.BasebandNameMod.BasebandName;
import alma.NetSidebandMod.NetSideband;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.LoggingConfig;
import alma.tmcdb.cloning.CloningTestUtils;
import alma.tmcdb.utils.HibernateUtil;

public class AntennaTest extends TmcdbTestCase {

    private Session session;
    
    public AntennaTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void tearDown() throws Exception {
        session.close();
        HibernateUtil.shutdown();        
        super.tearDown();
    }
    
    public void testAddAntenna() {
        Transaction transaction = null;
        HwConfiguration config = null;
        Configuration swCfg = null;
        Antenna antenna = null;
        transaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---            

        antenna = new Antenna("DV01",
        		AntennaType.AEC, 
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0, 0);
        FEDelay fedelay = new FEDelay(ReceiverBand.ALMA_RB_01, PolarizationType.X, NetSideband.USB, 3.14);
        antenna.getFrontEndDelays().add(fedelay);
        IFDelay ifdelay = new IFDelay(BasebandName.BB_1, PolarizationType.X, IFProcConnectionState.LSB_HIGH, 3.14);
        antenna.getIfDelays().add(ifdelay);
        LODelay lodelay = new LODelay(BasebandName.BB_2, 3.14);
        antenna.getLoDelays().add(lodelay);
        config.addBaseElement(antenna);
        
        XPDelay xpdelay = new XPDelay(ReceiverBand.ALMA_RB_01, BasebandName.BB_1,
                NetSideband.LSB, 3.14, config);
        xpdelay.setConfiguration(config);
        config.getCrossPolarizationDelays().add(xpdelay);
        
        session.save(config);

        transaction.commit();

        
        try {
            logger.info("something here");
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            // Care needs to be taken with unidireccional many-to-one associations.
            // The many side needs to be deleted first.
            // In this test we have this problem with Antenna to Component; and
            // Component to ComponentType.
            session.delete(antenna);
            session.delete(config);
            cleaningTransaction.commit();
        }
    }

    public void notestAddMultipleAntennas() {
        Transaction transaction = null;
        Configuration swCfg = null;
        HwConfiguration config = null;
        Antenna antenna1 = null;
        Antenna antenna2 = null;
        transaction = session.beginTransaction();

        // --- SW scaffolding ---

        swCfg = new Configuration();
        config = new HwConfiguration(swCfg);
        swCfg.setConfigurationName("Test");
        swCfg.setFullName("");
        swCfg.setActive(true);
        swCfg.setCreationTime(new Date());
        swCfg.setDescription("");
        session.save(swCfg);

        ComponentType compType = new ComponentType();
        compType.setIDL("IDL:alma/Dodo/Foo:1.0");
        session.save(compType);

        LoggingConfig logCfg = new LoggingConfig();
        session.save(logCfg);

        Container cont = new Container();
        cont.setLoggingConfig(logCfg);
        cont.setContainerName("javaContainer");
        cont.setPath("foo/bar");
        cont.setImplLang("java");
        cont.setConfiguration(swCfg);
        swCfg.getContainers().add(cont);
        session.save(cont);

        // --- end SW scaffolding ---            

        antenna1 = new Antenna("DV01",
        		AntennaType.VA, 
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna1);
        antenna2 = new Antenna("DA41",
        		AntennaType.AEC, 
        		new Coordinate(0.0, 0.0, 0.0),
        		new Coordinate(0.0, 0.0, 0.0),
        		4.5,
        		0L,
        		0,
        		0);
        config.addBaseElement(antenna2);
        session.save(config);            
        transaction.commit();
        
        try {
            transaction = session.beginTransaction();            
            assertEquals(2, session.createQuery("from Antenna").list().size());
            String query = "from Antenna antenna " +
                "where antenna.name like '%DV01%' " +
                "order by antenna.id";
            assertEquals(1, session.createQuery(query).list().size());
            transaction.commit();
        } finally {
            // Cleaning
            Transaction cleaningTransaction = session.beginTransaction();
            session.delete(antenna1);
            session.delete(antenna2);
            session.delete(config);
            cleaningTransaction.commit();
        }        
    }
    
}
