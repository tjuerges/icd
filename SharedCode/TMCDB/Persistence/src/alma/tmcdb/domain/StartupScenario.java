/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class StartupScenario implements Serializable {
    
    private static final long serialVersionUID = -1256689525882415794L;
    private Long id;
    private String name;
    private HwConfiguration configuration;
    private Set<BaseElementStartup> baseElementStartups = new HashSet<BaseElementStartup>();
    private Set<AssemblyStartup> assemblyStartups = new HashSet<AssemblyStartup>();
    
    public StartupScenario() {}
    
    public StartupScenario(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof StartupScenario))
            return false;
        StartupScenario startup = (StartupScenario) o;
        return (getConfiguration() == null ? startup.getConfiguration() == null : getConfiguration().equals(startup.getConfiguration())) &&
               (getName() == null ? startup.getName() == null : getName().equals(startup.getName()));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((getConfiguration() == null) ? 0 : getConfiguration().hashCode());
        result = 31 * result + ((getName() == null) ? 0 : getName().hashCode());
        return result;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public HwConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(HwConfiguration configuration) {
        this.configuration = configuration;
    }

    public Set<BaseElementStartup> getBaseElementStartups() {
        return baseElementStartups;
    }

    public void setBaseElementStartups(Set<BaseElementStartup> baseElementStartup) {
        this.baseElementStartups = baseElementStartup;
    }

    public void addBaseElementStartup(BaseElementStartup baseElementStartup) {
        baseElementStartup.setStartup(this);
        baseElementStartups.add(baseElementStartup);
    }
    
    public Set<AssemblyStartup> getAssemblyStartups() {
        return assemblyStartups;
    }

    public void setAssemblyStartups(Set<AssemblyStartup> assemblyStartups) {
        this.assemblyStartups = assemblyStartups;
    }
}
