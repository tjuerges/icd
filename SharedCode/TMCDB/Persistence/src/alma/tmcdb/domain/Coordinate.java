/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

public class Coordinate {

    private double x;
    private double y;
    private double z;
    
    public Coordinate() {}
    
    public Coordinate(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Coordinate))
            return false;
        Coordinate coord = (Coordinate) o;
        return (Double.compare(x, coord.x) == 0) &&
               (Double.compare(y, coord.y) == 0) &&
               (Double.compare(z, coord.z) == 0);
    }

    @Override
    public int hashCode() {
        int result = 17;
        long f;
        f = Double.doubleToLongBits(x);
        result = 31 * result + (int) (f ^ f >>> 32); 
        f = Double.doubleToLongBits(y);
        result = 31 * result + (int) (f ^ f >>> 32); 
        f = Double.doubleToLongBits(z);
        result = 31 * result + (int) (f ^ f >>> 32); 
        return result;
    }
    
    public double getX() {
        return x;
    }
    
    public void setX(double x) {
        this.x = x;
    }
    
    public double getY() {
        return y;
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public double getZ() {
        return z;
    }
    
    public void setZ(double z) {
        this.z = z;
    }
}
