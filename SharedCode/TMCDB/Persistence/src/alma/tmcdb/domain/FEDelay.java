/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import alma.NetSidebandMod.NetSideband;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;

public class FEDelay {

    private Long id;
    private ReceiverBand receiverBand;
    private PolarizationType polarization;
    private NetSideband sideband;
    private Double delay;
    
    public FEDelay() {}

    public FEDelay(ReceiverBand receiverBand, PolarizationType polarization, NetSideband sideband,
            double delay) {
        this.receiverBand = receiverBand;
        this.polarization = polarization;
        this.sideband = sideband;
        this.delay = delay;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public ReceiverBand getReceiverBand() {
        return receiverBand;
    }
    
    public void setReceiverBand(ReceiverBand receiverBand) {
        this.receiverBand = receiverBand;
    }
    
    public PolarizationType getPolarization() {
        return polarization;
    }
    
    public void setPolarization(PolarizationType polarization) {
        this.polarization = polarization;
    }
    
    public NetSideband getSideband() {
        return sideband;
    }
    
    public void setSideband(NetSideband sideband) {
        this.sideband = sideband;
    }
    
    public Double getDelay() {
        return delay;
    }
    
    public void setDelay(Double delay) {
        this.delay = delay;
    }
}
