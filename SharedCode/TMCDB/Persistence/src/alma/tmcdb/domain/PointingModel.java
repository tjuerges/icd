/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: PointingModel.java,v 1.7 2010/11/15 23:51:32 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.HashMap;
import java.util.Map;

import alma.ReceiverBandMod.ReceiverBand;

public class PointingModel {
    
    private Long id;
    private Antenna antenna;
    private Map<String, PointingModelCoeff> terms = new HashMap<String, PointingModelCoeff>();
    
    public PointingModel() {}

    public PointingModel(Antenna antenna) {
        this.antenna = antenna;
        antenna.getPointingModels().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PointingModel))
            return false;
        PointingModel pm = (PointingModel) o;
        return (antenna == null ? pm.antenna == null : antenna.equals(pm.antenna));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((antenna == null) ? 0 : antenna.hashCode());
        return result;
    }
    
    public Antenna getAntenna() {
        return antenna;
    }

    public void setAntenna(Antenna antenna) {
        this.antenna = antenna;
    }

    public Map<String, PointingModelCoeff> getTerms() {
        return terms;
    }

    public void setTerms(Map<String, PointingModelCoeff> coeffs) {
        this.terms = coeffs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addTerm(String coeffname, PointingModelCoeff coeff) {
        coeff.setPointingModel(this);
        coeff.setName(coeffname);
        terms.put(coeffname, coeff);
    }
    
    public PointingModelCoeff getTerm(String coeffname) {
        return terms.get(coeffname);
    }

    @Override
    public String toString() {
        String repr = "PointingModel {\n";
        repr += "\tTerms {\n";
        for (String name : terms.keySet()) {
            repr += "\t\t" + name + ": " + terms.get(name).getValue() + "\n";
            repr += "\t\tOffsets {\n";
            for (ReceiverBand band : terms.get(name).getOffsets().keySet()) {
                repr += "\t\t\t" + band.toString() + ": " + terms.get(name).getOffsets().get(band) + "\n";
            }
            repr += "\t\t}\n";
        }
        repr += "\t}\n";
        repr += "}\n";
        return repr;
    }
    
}
