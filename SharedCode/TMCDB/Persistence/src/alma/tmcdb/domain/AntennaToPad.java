/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AntennaToPad.java,v 1.15 2011/05/02 08:47:48 rtobar Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import alma.acs.tmcdb.translator.TmcdbObject;
import alma.acs.util.UTCUtility;

public class AntennaToPad extends TmcdbObject {
    
    private Integer antennaToPadId;
    private Long startTime;
    private Long endTime;
    private Boolean planned;
    private Double mountMetrologyAN0Coeff;
    private Double mountMetrologyAW0Coeff;
    
    private Antenna antenna;
    private Pad pad;
    
    public AntennaToPad() {}

    public AntennaToPad(Antenna antenna, Pad pad, Date startTime, Date endTime, boolean planned) {
        this(antenna,
             pad,
             UTCUtility.utcJavaToOmg(startTime.getTime()),
             (endTime != null ? UTCUtility.utcJavaToOmg(endTime.getTime()) : null),
             planned);
    }
    
    public AntennaToPad(Antenna antenna, Pad pad, Long startTime, Long endTime, boolean planned) {
        
        this.antenna = antenna;
        this.pad = pad;
        this.startTime = startTime;
        this.endTime = endTime;
        this.planned = planned;
        
        this.mountMetrologyAN0Coeff = 0.0;
        this.mountMetrologyAW0Coeff = 0.0;
        
        this.pad.getScheduledAntennas().add(this);
    }

    @Override
    public boolean equalsContent(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AntennaToPad))
            return false;
        AntennaToPad a2p = (AntennaToPad) o;
        return (antenna == null ? a2p.antenna == null :
                antenna.equals(a2p.antenna)) &&
               (pad == null ? a2p.pad == null :
                pad.equals(a2p.pad)) && 
               (startTime == null ? a2p.startTime == null :
                startTime.equals(a2p.startTime));
    }

    @Override
    public int hashCodeContent() {
        int result = 17;
        result = 31 * result + ((antenna == null) ? 0 : antenna.hashCode());
        result = 31 * result + ((pad == null) ? 0 : pad.hashCode());
        result = 31 * result + ((startTime == null) ? 0 : startTime.hashCode());
        return result;
    }
    
    public Integer getAntennaToPadId() {
        return antennaToPadId;
    }

    public void setAntennaToPadId(Integer id) {
        this.antennaToPadId = id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    
    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Boolean getPlanned() {
        return planned;
    }

    public void setPlanned(Boolean planned) {
        this.planned = planned;
    }

    public Double getMountMetrologyAN0Coeff() {
		return mountMetrologyAN0Coeff;
	}

	public void setMountMetrologyAN0Coeff(Double mountMetrologyAN0Coeff) {
		this.mountMetrologyAN0Coeff = mountMetrologyAN0Coeff;
	}

	public Double getMountMetrologyAW0Coeff() {
		return mountMetrologyAW0Coeff;
	}

	public void setMountMetrologyAW0Coeff(Double mountMetrologyAW0Coeff) {
		this.mountMetrologyAW0Coeff = mountMetrologyAW0Coeff;
	}

	public Antenna getAntenna() {
        return antenna;
    }

    public void setAntenna(Antenna antenna) {
        this.antenna = antenna;
    }

    public Pad getPad() {
        return pad;
    }

    public void setPad(Pad pad) {
        this.pad = pad;
    }
}
