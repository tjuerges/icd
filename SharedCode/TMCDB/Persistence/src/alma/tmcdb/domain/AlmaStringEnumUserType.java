/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AlmaStringEnumUserType.java,v 1.2 2010/10/15 01:14:44 sharring Exp $"
 */
package alma.tmcdb.domain;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.util.ReflectHelper;

/**
 * Custom mapping type for string-backed enumerations.
 *
 * Taken from "Java Persistence with Hibernate", Christian Bauer and
 * Gavin King, Manning, ISBN 1-932394-88-5.
 *
 * This class will probably be replace in the future to use ICD/HLA/Enumerations,
 * which are not Java Enums.
 */
@SuppressWarnings("unchecked")
public class AlmaStringEnumUserType implements EnhancedUserType, ParameterizedType {

	private Class enumClass;
	private Class corbaEnumClass;
	private Method literalMethod;
	private Method nameMethod;

    public void setParameterValues(Properties parameters) {
        String enumClassName =
            parameters.getProperty("enumClassName");
        String corbaEnumClassName =
            parameters.getProperty("corbaEnumClassName");
        try {
            enumClass = ReflectHelper.classForName(enumClassName);
            corbaEnumClass = ReflectHelper.classForName(corbaEnumClassName);
            literalMethod = enumClass.getMethod("literal", new Class[] {String.class});
            nameMethod = enumClass.getMethod("name", new Class[] {corbaEnumClass});
        } catch (ClassNotFoundException cnfe) {
            throw new HibernateException("Enum class not found", cnfe);
        } catch (SecurityException se) {
            throw new HibernateException("Security exception", se);
        } catch (NoSuchMethodException nsme) {
            throw new HibernateException("Method not found", nsme);
        }
    }

    public Class returnedClass() {
        return enumClass;
    }

    public int[] sqlTypes() {
        return new int[] { Hibernate.STRING.sqlType() };
    }

    public boolean isMutable() {
        return false;
    }

    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        return cached;
    }

    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return original;
    }

    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y)
            return true;
        if (x == null || y == null)
            return false;
        return x.equals(y);
    }

    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    public Object fromXMLString(String xmlValue) {
        try {
            return literalMethod.invoke(null, xmlValue);
        } catch (IllegalArgumentException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (IllegalAccessException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (InvocationTargetException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        }
    }

    public String objectToSQLString(Object value) {
        String repr = null;
        try {
            repr = (String) nameMethod.invoke(null, value);
        } catch (IllegalArgumentException e) {
            throw new HibernateException("Illegal argument converting ALMA Enum to SQL", e);
        } catch (IllegalAccessException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (InvocationTargetException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        }
        return '\'' + repr + '\'';
    }

    public String toXMLString(Object value) {
        String repr = null;
        try {
            repr = (String) nameMethod.invoke(null, value);
        } catch (IllegalArgumentException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (IllegalAccessException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (InvocationTargetException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        }
        return repr;
    }

    public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
            throws HibernateException, SQLException {
        String name = rs.getString(names[0]);
        Object literal = null;
        try {
            literal = literalMethod.invoke(null, name);
        } catch (IllegalArgumentException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (IllegalAccessException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (InvocationTargetException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        }        
        return rs.wasNull() ? null : literal;
    }

    public void nullSafeSet(PreparedStatement st, Object value, int index)
            throws HibernateException, SQLException {
        String name = null;
        try {
            name = (String) nameMethod.invoke(null, value);
        } catch (IllegalArgumentException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (IllegalAccessException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        } catch (InvocationTargetException e) {
            throw new HibernateException("ALMA Enum conversion error", e);
        }        
        if (value == null) {
            st.setNull(index, Hibernate.STRING.sqlType());
        } else {
            st.setString(index, name);
        }
    }
}