/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.HashMap;
import java.util.Map;

public class FocusModel {

    private Long id;
    private Antenna antenna;
    private Map<String, FocusModelCoeff> terms = new HashMap<String, FocusModelCoeff>();
    
    public FocusModel() {}

    public FocusModel(Antenna antenna) {
        this.antenna = antenna;
        antenna.getFocusModels().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof FocusModel))
            return false;
        FocusModel fm = (FocusModel) o;
        return (antenna == null ? fm.antenna == null : antenna.equals(fm.antenna));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((antenna == null) ? 0 : antenna.hashCode());
        return result;
    }
    
    public Antenna getAntenna() {
        return antenna;
    }

    public void setAntenna(Antenna antenna) {
        this.antenna = antenna;
    }

    public Map<String, FocusModelCoeff> getTerms() {
        return terms;
    }

    public void setTerms(Map<String, FocusModelCoeff> coeffs) {
        this.terms = coeffs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
