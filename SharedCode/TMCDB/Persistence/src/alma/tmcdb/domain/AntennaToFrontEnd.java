/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: AntennaToFrontEnd.java,v 1.9 2011/05/02 08:47:47 rtobar Exp $"
 */
package alma.tmcdb.domain;

import java.io.Serializable;
import java.util.Date;

import alma.acs.util.UTCUtility;
import alma.tmcdb.utils.CompositeIdentifierUpdateable;

public class AntennaToFrontEnd implements CompositeIdentifierUpdateable {
    
    public static class Id implements Serializable {
        
        private static final long serialVersionUID = -5355455428971674738L;
        private Long antennaId;
        private Long frontEndId;
        private Long startTime;
        
        public Id() {}

        public Id(Long antennaId, Long padId, Long startTime) {
            this.antennaId = antennaId;
            this.frontEndId = padId;
            this.startTime = startTime;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (!(o instanceof Id))
                return false;
            Id that = (Id) o;
            return (this.antennaId == null ? that.antennaId == null : this.antennaId.equals(that.antennaId)) &&
                (this.frontEndId == null ? that.frontEndId == null : this.frontEndId.equals(that.frontEndId)) &&
                (this.startTime == null ? that.startTime == null : this.startTime.equals(that.startTime));
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + ((antennaId == null) ? 0 : antennaId.hashCode());
            result = 31 * result + ((frontEndId == null) ? 0 : frontEndId.hashCode());
            result = 31 * result + ((startTime == null) ? 0 : startTime.hashCode());
            return result;
        }

        public Long getAntennaId() {
            return antennaId;
        }

        public void setAntennaId(Long antennaId) {
            this.antennaId = antennaId;
        }

        public Long getFrontEndId() {
            return frontEndId;
        }

        public void setFrontEndId(Long frontEndId) {
            this.frontEndId = frontEndId;
        }

        public Long getStartTime() {
            return startTime;
        }

        public void setStartTime(Long startTime) {
            this.startTime = startTime;
        }
    }
    
    private Id id = new Id();
    private Long startTime;
    private Long endTime;
    
    private Antenna antenna;
    private FrontEnd frontEnd;
    
    public AntennaToFrontEnd() {}

    public AntennaToFrontEnd(Antenna antenna, FrontEnd frontEnd, Date startTime, Date endTime) {
        this(antenna, frontEnd, UTCUtility.utcJavaToOmg(startTime.getTime()),
        		UTCUtility.utcJavaToOmg(endTime.getTime()));
    }
    
    public AntennaToFrontEnd(Antenna antenna, FrontEnd frontEnd, Long startTime, Long endTime) {        
        this.antenna = antenna;
        this.frontEnd = frontEnd;
        this.startTime = startTime;
        this.endTime = endTime;
        
        this.id.antennaId = antenna.getId();
        this.id.frontEndId = frontEnd.getId();
        this.id.startTime = startTime;
        
        this.antenna.getScheduledFrontEnds().add(this);
        this.frontEnd.getScheduledAntennaInstallations().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AntennaToFrontEnd))
            return false;
        AntennaToFrontEnd that = (AntennaToFrontEnd) o;
        return (antenna == null ? that.antenna == null :
                antenna.equals(that.antenna)) &&
               (frontEnd == null ? that.frontEnd == null :
                frontEnd.equals(that.frontEnd)) &&
               (startTime == null ? that.startTime == null :
                startTime.equals(that.startTime));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((antenna == null) ? 0 : antenna.hashCode());
        result = 31 * result + ((frontEnd == null) ? 0 : frontEnd.hashCode());
        result = 31 * result + ((startTime == null) ? 0 : startTime.hashCode());
        return result;
    }
    
    public Id getId() {
        return id;
    }
    
    public void setId(Id id) {
        this.id = id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.id.setStartTime(startTime);
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }
    
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
    
    public Antenna getAntenna() {
        return antenna;
    }
    
    public void setAntenna(Antenna antenna) {
        this.id.setAntennaId(antenna.getId());
        this.antenna = antenna;
    }
    
    public FrontEnd getFrontEnd() {
        return frontEnd;
    }
    
    public void setFrontEnd(FrontEnd frontEnd) {
        this.id.setFrontEndId(frontEnd.getId());
        this.frontEnd = frontEnd;
    }

    @Override
    public void updateId() {
        this.id.setAntennaId(antenna.getId());
        this.id.setFrontEndId(frontEnd.getId());
    }
}
