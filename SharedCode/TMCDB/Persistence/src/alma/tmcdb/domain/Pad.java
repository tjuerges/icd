/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import alma.acs.util.UTCUtility;

public class Pad extends BaseElement {

    private Long commissionDate;
    private Coordinate position;
    private Set<AntennaToPad> scheduledAntennas = new HashSet<AntennaToPad>();
    private Set<HolographyTowerToPad> holographyTowers = new HashSet<HolographyTowerToPad>();
    
    /**************************************************************************************
     * The delay model is computed as
     * delay_total = delay_pad + delay_antenna + { baseband delays }
     *
     * The delay_pad is (here) in the Pad class, the delay_antenna is in the Antenna class.
     *
     * In other words, what it used to be the "cable delay" was divided in two components, 
     * one related with the pad which represent the path from the pad to the correelator; 
     * and the other related with the antenna which represents the path from the antenna 
     * reception to the pad.
     *
     * This design is based on ALMA-80.00.00.00-0015-A-SPE, "Instrumental Delay", by R. Sramek. 
     **/
    private Double avgDelay;

    public Pad() {}

    public Pad(String name, Coordinate position, Date commissionDate) {
        this(name, position, UTCUtility.utcJavaToOmg(commissionDate.getTime()));
    }
    
    public Pad(String name, Coordinate position, Long commissionDate) {
        super(null, name, BaseElementType.Pad);
        this.commissionDate = commissionDate;
        this.position = position;
        this.avgDelay = 0.0;
    }
    
    public Long getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Long commissionDate) {
        this.commissionDate = commissionDate;
    }

    public Coordinate getPosition() {
        return position;
    }

    public void setPosition(Coordinate position) {
        this.position = position;
    }

    public Set<AntennaToPad> getScheduledAntennas() {
        return scheduledAntennas;
    }

    public void setScheduledAntennas(Set<AntennaToPad> scheduledAntennas) {
        this.scheduledAntennas = scheduledAntennas;
    }
    
    public  Double getAvgDelay() {
        return avgDelay;
    }

    public void setAvgDelay(Double avgDelay) {
        this.avgDelay = avgDelay;
    }

    public Set<HolographyTowerToPad> getHolographyTowers() {
        return holographyTowers;
    }

    public void setHolographyTowers(Set<HolographyTowerToPad> holographyTowers) {
        this.holographyTowers = holographyTowers;
    }
}
