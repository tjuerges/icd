/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Schemas;
import alma.acs.tmcdb.Component;

public class HwConfiguration implements Serializable {
    
    private static final long serialVersionUID = -1805846186509252907L;
    private Long id;
    private Configuration swConfiguration;
    private String telescopeName;
    private ArrayReference arrayReference;
    private Set<StartupScenario> startupScenarios = new HashSet<StartupScenario>();
    private Set<BaseElement> baseElements = new HashSet<BaseElement>();
    private Set<Assembly> assemblies = new HashSet<Assembly>();
    private Set<HwSchema> hwSchemas = new HashSet<HwSchema>();
    private Set<XPDelay> crossPolarizationDelays = new HashSet<XPDelay>();
    
    public HwConfiguration() {}
    
    public HwConfiguration(Configuration swConfiguration) {
        this.swConfiguration = swConfiguration;
        this.telescopeName = "OSF";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof HwConfiguration))
            return false;
        
        HwConfiguration cnf = (HwConfiguration) o;
        return ((getSwConfiguration() == null || getSwConfiguration().getConfigurationName() == null)
        		? (cnf.getSwConfiguration() == null || cnf.getSwConfiguration().getConfigurationName() == null) 
        		: getSwConfiguration().getConfigurationName().equals(cnf.getSwConfiguration().getConfigurationName()));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((getSwConfiguration() == null) ? 0 : getSwConfiguration().hashCode());
        return result;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public ArrayReference getArrayReference() {
		return arrayReference;
	}

	public void setArrayReference(ArrayReference arrayReference) {
		this.arrayReference = arrayReference;
	}

    public Set<StartupScenario> getStartupScenarios() {
        return startupScenarios;
    }

    public void setStartupScenarios(Set<StartupScenario> startupScenarios) {
        this.startupScenarios = startupScenarios;
    }
    
    public void addStartupScenario(StartupScenario startupScenario) {
        startupScenario.setConfiguration(this);
        startupScenarios.add(startupScenario);
    }

    public Set<BaseElement> getBaseElements() {
        return baseElements;
    }

    public void setBaseElements(Set<BaseElement> baseElements) {
        this.baseElements = baseElements;
    }
    
    public void addBaseElement(BaseElement baseElement) {
        baseElement.setConfiguration(this);
        baseElements.add(baseElement);
    }

	public Set<Assembly> getAssemblies() {
        return assemblies;
    }

    public void setAssemblies(Set<Assembly> assemblies) {
        this.assemblies = assemblies;
    }
    
    public void addAssembly(Assembly assembly) {
        assembly.setConfiguration(this);
        assemblies.add(assembly);
    }

    public Set<HwSchema> getHwSchemas() {
		return hwSchemas;
	}

	public void setHwSchemas(Set<HwSchema> hwSchemas) {
		this.hwSchemas = hwSchemas;
	}

	public void addHwSchema(HwSchema schema) {
		schema.setConfiguration(this);
		hwSchemas.add(schema);
	}
	
	public Configuration getSwConfiguration() {
        return swConfiguration;
    }

    public void setSwConfiguration(Configuration swConfiguration) {
        this.swConfiguration = swConfiguration;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the schemas of the configuration.
     */
    public Set<Schemas> getSchemas() 
    {
       Set<Schemas> retVal = new HashSet<Schemas>();
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getSchemases();
       }
       return retVal;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the components of the configuration.
     */
    public Set<Component> getComponents() 
    {
       Set<Component> retVal = new HashSet<Component>();
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getComponents();
       }
       return retVal;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the full name of the configuration.
     */
    public String getFullName() 
    {
       String retVal = null;
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getFullName();
       }
       return retVal;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the active flag of the configuration.
     */
    public Boolean getActive() 
    {
       Boolean retVal = null;
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getActive();
       }
       return retVal;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the creation time of the configuration.
     */
    public Date getCreationTime() 
    {
       Date retVal = null;
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getCreationTime();
       }
       return retVal;
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the name of the configuration.
     */
    public String getName() 
    {
       String retVal = null;
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getConfigurationName();
       }
       return retVal;
    }

    /**
     * Setter (pass-through as a facade to Configuration) for the name of the configuration.
     */
    public void setName(String description)
    {
       if(null != this.getSwConfiguration()) {
          this.getSwConfiguration().setConfigurationName(description);
       }
    }

    /**
     * Getter (pass-through as a facade to Configuration) for the description of the configuration.
     */
    public String getDescription()
    {
       String retVal = null;
       if(null != this.getSwConfiguration()) {
          retVal = this.getSwConfiguration().getDescription();
       }
       return retVal;
    }

    /**
     * Setter (pass-through as a facade to Configuration) for the description of the configuration.
     */
    public void setDescription(String description)
    {
       if(null != this.getSwConfiguration()) {
          this.getSwConfiguration().setDescription(description);
       }
    }

    public String getTelescopeName() {
        return telescopeName;
    }

    public void setTelescopeName(String telescopeName) {
        this.telescopeName = telescopeName;
    }

    public Set<XPDelay> getCrossPolarizationDelays() {
        return crossPolarizationDelays;
    }

    public void setCrossPolarizationDelays(Set<XPDelay> crossPolarizationDelays) {
        this.crossPolarizationDelays = crossPolarizationDelays;
    }
}
