/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * LRU Type represents the types of Line Replaceable Units (LRU). 
 * These are hardware units that are taken out of field, carried 
 * back to the lab, repaired or replaced and brought back to the field.
 * All LRUs are made up of one or more assemblies.
 * 
 * @author rkurowsk, Dec 11, 2008
 * @version $Revision$
 */
public class LruType {

    private String name;
    private String fullName;
    private String icd;
    private long icdDate;
    private String description;
    private String notes;
    private Set<AssemblyType> assemblyTypes = new HashSet<AssemblyType>();
    
    /**
     *  Zero-arg public constructor 
     */
    public LruType() {
    }

    /**
     * Public constructor 
     * 
     * @param lruName
     * @param fullName
     * @param icd
     * @param icdDate
     * @param description
     * @param notes
     */
	public LruType(String name, String fullName, String icd, long icdDate,
			String description, String notes) {
		super();
		this.name = name;
		this.fullName = fullName;
		this.icd = icd;
		this.icdDate = icdDate;
		this.description = description;
		this.notes = notes;
	}

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof LruType))
            return false;
        LruType lru = (LruType) o;
        return (this.getName() == null ? lru.getName() == null : this.getName().equals(lru.getName()));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((this.getName() == null) ? 0 : this.getName().hashCode());
        return result;
    }

	/**
	 * @return the lruName
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param lruName the lruName to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the icd
	 */
	public String getIcd() {
		return icd;
	}

	/**
	 * @param icd the icd to set
	 */
	public void setIcd(String icd) {
		this.icd = icd;
	}

	/**
	 * @return the icdDate
	 */
	public long getIcdDate() {
		return icdDate;
	}

	/**
	 * @param icdDate the icdDate to set
	 */
	public void setIcdDate(long icdDate) {
		this.icdDate = icdDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return assembly type collection
	 */
    public Set<AssemblyType> getAssemblyTypes() {
        return assemblyTypes;
    }

    /**
     * @param assemblyTypes assembly type collection to set
     */
    public void setAssemblyTypes(Set<AssemblyType> assemblyTypes) {
        this.assemblyTypes = assemblyTypes;
    }
    
    /**
     * Adds an AssemblyType to the collection.
     * 
     * @param assemblyType assembly type to add
     */
    public void addAssemblyType(AssemblyType assemblyType) {
        assemblyType.setLruType(this);
        assemblyTypes.add(assemblyType);
    }
}
