/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: CentralLO.java,v 1.3 2011/05/02 08:47:48 rtobar Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import alma.acs.util.UTCUtility;

/**
 * This BaseElement should be renamed CentralLO.
 * The CentralLO is a collection of devices that generate the Local Oscillator (LO)
 * signals to be used in the antennas to downconvert the observing frequency to
 * the intermediate frequencies (IF). A maximum of 6 independent central LO signals
 * can be generated, and this sets the limit to the number of Arrays that can be
 * tuned in the ALMA telescope.
 *   The CentralLO reference signals are used in the antennas to downconvert the
 * observed signal twice. Two LO signals are generated, LO1 and LO2. The first
 * downconversion uses the LO1, and is performed in the receivers. It converts
 * the observed frequency to an IF signal in the range between 4 to 8 GHz. The second
 * downconversion, which uses the LO2 generated frequency, converts the IF signal
 * to a baseband signal, between 2 and 4 GHz.
 *   The generation of the reference signals in the CentralLO requires two
 * devices: a Laser Synthesizer and a Central Variable Reference. This two devices
 * can be seen as a group under the name Photonic Reference. In the complete CentralLO
 * there are six of them.
 * @author rhiriart
 *
 */
public class CentralLO extends BaseElement {
    
    private Long commissionDate;

    public CentralLO() {}

    public CentralLO(String name, Date commissionDate) {
        this(name, UTCUtility.utcJavaToOmg(commissionDate.getTime()));
    }
    
    public CentralLO(String name, Long commissionDate) {
        super(null, name, BaseElementType.CentralLO);
        this.name = name;
        this.commissionDate = commissionDate;
    }
    
    public Long getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Long commissionDate) {
        this.commissionDate = commissionDate;
    }
}
