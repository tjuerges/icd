<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE hibernate-mapping PUBLIC
    "-//Hibernate/Hibernate Mapping DTD//EN"
    "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <typedef class="alma.tmcdb.domain.StringEnumUserType"
        name="AlmaAntennaType">
        <param name="enumClassName">alma.tmcdb.domain.AntennaType</param>
    </typedef>
    <typedef class="alma.tmcdb.domain.StringEnumUserType"
        name="TmcdbBaseElementType">
        <param name="enumClassName">alma.tmcdb.domain.BaseElementType</param>
    </typedef>
    <class
        name="alma.tmcdb.domain.BaseElement"
        table="BASEELEMENT">
        
        <id
            name="id"
            column="BASEELEMENTID">
            <generator class="native"><param name="sequence">BaseElement_seq</param></generator>
        </id>
        
        <property name="name" column="BASEELEMENTNAME"/>
        <property name="type" column="BASETYPE"
            type="TmcdbBaseElementType" not-null="true"/>
        
        <many-to-one name="configuration" column="CONFIGURATIONID"
            class="alma.tmcdb.domain.HwConfiguration" not-null="true"/>        

        <joined-subclass name="alma.tmcdb.domain.Antenna" table="ANTENNA">
            <key column="BASEELEMENTID"/>
            <component name="position" class="alma.tmcdb.domain.Coordinate">
                <property name="x" type="double" column="XPOSITION" not-null="true"/>
                <property name="y" type="double" column="YPOSITION" not-null="true"/>
                <property name="z" type="double" column="ZPOSITION" not-null="true"/>
            </component>
            <component name="offset" class="alma.tmcdb.domain.Coordinate">
                <property name="x" type="double" column="XOFFSET" not-null="true"/>
                <property name="y" type="double" column="YOFFSET" not-null="true"/>
                <property name="z" type="double" column="ZOFFSET" not-null="true"/>
            </component>
            <property name="diameter" column="DISHDIAMETER"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
            <property name="antennaType" column="ANTENNATYPE"
                type="AlmaAntennaType" not-null="true"/>
            <property name="avgDelay" column="DELAY"/>                
            <property name="loOffsettingIndex" column="LOOFFSETTINGINDEX"/>                
            <property name="walshSeq" column="WALSHSEQ"/>                
            <set name="scheduledPadLocations" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID"/>
                <one-to-many class="alma.tmcdb.domain.AntennaToPad"/>
            </set>            
            <set name="scheduledFrontEnds" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID"/>
                <one-to-many class="alma.tmcdb.domain.AntennaToFrontEnd"/>
            </set>            
            <set name="focusModels" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID"/>
                <one-to-many class="alma.tmcdb.domain.FocusModel"/>
            </set>
            <set name="pointingModels" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID"/>
                <one-to-many class="alma.tmcdb.domain.PointingModel"/>
            </set>
            <set name="frontEndDelays" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID" not-null="true"/>
                <one-to-many class="alma.tmcdb.domain.FEDelay"/>
            </set>
            <set name="ifDelays" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID" not-null="true"/>
                <one-to-many class="alma.tmcdb.domain.IFDelay"/>
            </set>
            <set name="loDelays" cascade="save-update, delete, lock, merge">
                <key column="ANTENNAID" not-null="true"/>
                <one-to-many class="alma.tmcdb.domain.LODelay"/>
            </set>
        </joined-subclass>
        
        <joined-subclass name="alma.tmcdb.domain.Pad" table="Pad">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
            <property name="avgDelay" column="DELAY"/>                
            <component name="position" class="alma.tmcdb.domain.Coordinate">
                <property name="x" type="double" column="XPOSITION" not-null="true"/>
                <property name="y" type="double" column="YPOSITION" not-null="true"/>
                <property name="z" type="double" column="ZPOSITION" not-null="true"/>
            </component>
            <set name="scheduledAntennas" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="PADID"/>
                <one-to-many class="alma.tmcdb.domain.AntennaToPad"/>
            </set>
            <set name="holographyTowers" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="PADID"/>
                <one-to-many class="alma.tmcdb.domain.HolographyTowerToPad"/>
            </set>            
        </joined-subclass>
        
        <joined-subclass name="alma.tmcdb.domain.FrontEnd" table="FrontEnd">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
            <set name="scheduledAntennaInstallations" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="FRONTENDID"/>
                <one-to-many class="alma.tmcdb.domain.AntennaToFrontEnd"/>
            </set>            
        </joined-subclass>

        <joined-subclass name="alma.tmcdb.domain.CentralLO" table="CentralLO">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
        </joined-subclass>

        <joined-subclass name="alma.tmcdb.domain.AOSTiming" table="AOSTiming">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
        </joined-subclass>

        <joined-subclass name="alma.tmcdb.domain.HolographyTower" table="HolographyTower">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
            <component name="position" class="alma.tmcdb.domain.Coordinate">
                <property name="x" type="double" column="XPOSITION" not-null="true"/>
                <property name="y" type="double" column="YPOSITION" not-null="true"/>
                <property name="z" type="double" column="ZPOSITION" not-null="true"/>
            </component>
            <set name="associatedPads" inverse="true" cascade="save-update, delete, lock, merge">
                <key column="HOLOGRAPHYTOWERID"/>
                <one-to-many class="alma.tmcdb.domain.HolographyTowerToPad"/>
            </set>            
        </joined-subclass>
        
        <joined-subclass name="alma.tmcdb.domain.WeatherStationController" table="WeatherStationController">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
        </joined-subclass>
        
        <joined-subclass name="alma.tmcdb.domain.PhotonicReference" table="PhotonicReference">
            <key column="BASEELEMENTID"/>
            <property name="commissionDate" column="COMMISSIONDATE" not-null="true"/>
        </joined-subclass>
    </class>        
</hibernate-mapping>    
