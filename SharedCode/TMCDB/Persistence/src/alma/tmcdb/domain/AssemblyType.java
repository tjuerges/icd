/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.HashSet;
import java.util.Set;
import alma.acs.tmcdb.ComponentType;

/**
 * AssemblyType represents assemblies that are part of an LRU.  
 * All LRUs are made up of one or more assemblies.  
 * All monitored properties are tied to specific assemblies.
 * AssemblyName is the unique key.  
 * This requires that names of assemblies be unique across all 
 * hardware devices and the entire database.  
 * 
 * @author rkurowsk, Dec 11, 2008
 * @version $Revision$
 */
public class AssemblyType {

    private String name;
    
    private LruType lruType;
    
    private BaseElementType baseElementType;
    
    private String fullName;
    
    private String description;
    
    private String notes;
    
    private ComponentType componentType;
    
    private String productionCode;
    
    private String simulatedCode;

    private Set<AssemblyRole> roles = new HashSet<AssemblyRole>();
        
    /** 
     * Zero-arg public constructor 
     */
    public AssemblyType() {}

    /**
     * Public constructor.
     * 
     * @param name
     * @param fullName
     * @param description
     * @param notes
     * @param componentType
     */
    public AssemblyType(String name, String fullName, BaseElementType baseElementType,
            String description, String notes, ComponentType componentType, String productionCode, String simulationCode) {
        this(null, name, fullName, baseElementType, description, notes, componentType, productionCode, simulationCode);
    }
    
    /**
     * Public constructor
     * 
     * This constructor adds the new AssemblyType into the collection
     * maintained by its parent LruType, which is passed as parameter.
     *
     * @param lru LruType parent
     * @param name Name. Must be unique.
     * @param fullName Full name
     * @param description Description
     * @param notes Notes
     * @param componentType ComponentType
     */
    public AssemblyType(LruType lru, String name, String fullName, BaseElementType baseElementType,
            String description, String notes, ComponentType componentType, String productionCode, String simulationCode) {
        super();
        this.name = name;
        this.baseElementType = baseElementType;
        this.fullName = fullName;
        this.description = description;
        this.notes = notes;
        this.componentType = componentType;
        this.productionCode = productionCode;
        this.simulatedCode = simulationCode;
        if (lru != null)
            lru.addAssemblyType(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AssemblyType))
            return false;
        AssemblyType at = (AssemblyType) o;
        return (name == null ? at.name == null : name.equals(at.name));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
	/**
	 * @return the assemblyName
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param assemblyName the assemblyName to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the lruType
	 */
	public LruType getLruType() {
		return lruType;
	}

	/**
	 * @param lruType the lruType to set
	 */
	public void setLruType(LruType lruType) {
		this.lruType = lruType;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the componentType
	 */
	public ComponentType getComponentType() {
		return componentType;
	}

	/**
	 * @param componentType the componentType to set
	 */
	public void setComponentType(ComponentType componentType) {
		this.componentType = componentType;
	}

    public BaseElementType getBaseElementType() {
        return baseElementType;
    }

    public void setBaseElementType(BaseElementType baseElementType) {
        this.baseElementType = baseElementType;
    }

    public Set<AssemblyRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AssemblyRole> roles) {
        this.roles = roles;
    }
    
    public void addRole(AssemblyRole role) {
        role.setAssemblyType(this);
        roles.add(role);
    }

    public String getProductionCode() {
        return productionCode;
    }

    public void setProductionCode(String productionCode) {
        this.productionCode = productionCode;
    }

    public String getSimulatedCode() {
        return simulatedCode;
    }

    public void setSimulatedCode(String simulatedCode) {
        this.simulatedCode = simulatedCode;
    }

}