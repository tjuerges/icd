/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: XPDelay.java,v 1.5 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import alma.BasebandNameMod.BasebandName;
import alma.NetSidebandMod.NetSideband;
import alma.ReceiverBandMod.ReceiverBand;

public class XPDelay {

    private HwConfiguration configuration;
    private Long id;
    private ReceiverBand receiverBand;
    private BasebandName baseband;
    private NetSideband sideband;
    private Double delay;
    
    public XPDelay() {}

    public XPDelay(ReceiverBand receiverBand, BasebandName baseband, NetSideband sideband,
            double delay, HwConfiguration config) 
    {
        this.receiverBand = receiverBand;
        this.baseband = baseband;
        this.sideband = sideband;
        this.delay = delay;
        this.configuration = config;
    }
    
    public HwConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(HwConfiguration configuration) {
        this.configuration = configuration;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public BasebandName getBaseband() {
        return baseband;
    }
    
    public void setBaseband(BasebandName baseband) {
        this.baseband = baseband;
    }
    
    public NetSideband getSideband() {
        return sideband;
    }
    
    public void setSideband(NetSideband sideband) {
        this.sideband = sideband;
    }
    
    public Double getDelay() {
        return delay;
    }
    
    public void setDelay(Double delay) {
        this.delay = delay;
    }

    public ReceiverBand getReceiverBand() {
        return receiverBand;
    }

    public void setReceiverBand(ReceiverBand receiverBand) {
        this.receiverBand = receiverBand;
    }
}
