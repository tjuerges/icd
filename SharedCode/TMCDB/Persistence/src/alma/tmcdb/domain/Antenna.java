/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import alma.acs.util.UTCUtility;

/**
 * The Antenna table represents the general properties of an ALMA antenna. The
 * x-y-z position is the position from the pad position to the point of rotation
 * of the antenna. The x-y-z offset is the offset, if any, from that position to
 * the point from which the feeds offsets are measured. 
 * 
 * @author rhiriart, Mar 9, 2009
 * @version $Revision$
 */
public class Antenna extends BaseElement {
    
    /** Coordinates from the pad location to the center of rotation of the antenna */
    private Coordinate position;
    
    /** Offset coordinates for antenna position */
    private Coordinate offset;
    
    /** Antenna diameter */
    private Double diameter;
    
    /** Antenna comission date, in ACS Time units */
    private Long commissionDate;
    
    /** Antenna type */
    // TODO change this to antennaMake
    private AntennaType antennaType;
    
    /**************************************************************************************
     * The delay model is computed as
     * delay_total = delay_pad + delay_antenna + { baseband delays }
     *
     * The delay_pad is in the Pad class, the delay_antenna is (here) in the Antenna class.
     *
     * In other words, what it used to be the "cable delay" was divided in two components, 
     * one related with the pad which represent the path from the pad to the correelator; 
     * and the other related with the antenna which represents the path from the antenna 
     * reception to the pad.
     *
     * This design is based on ALMA-80.00.00.00-0015-A-SPE, "Instrumental Delay", by R. Sramek. 
     **/
    private Double avgDelay;
        
    private Integer loOffsettingIndex;
    
    private Integer walshSeq;
    
    private Set<AntennaToPad> scheduledPadLocations = new HashSet<AntennaToPad>();
    private Set<AntennaToFrontEnd> scheduledFrontEnds = new HashSet<AntennaToFrontEnd>();
    private Set<PointingModel> pointingModels = new HashSet<PointingModel>();
    private Set<FocusModel> focusModels = new HashSet<FocusModel>();
    private Set<FEDelay> frontEndDelays = new HashSet<FEDelay>();
    private Set<IFDelay> ifDelays = new HashSet<IFDelay>();
    private Set<LODelay> loDelays = new HashSet<LODelay>();
    
    /** Zero-arg public constructor */
    public Antenna() {
        super();
    }
    
    /**
     * Public constructor.
     * 
     * @param name Antenna name
     * @param type Antenna type
     * @param position Antenna position
     * @param offset Antenna position offset
     * @param diameter Antenna diameter
     * @param commissionDate Antenna comission date
     */
    public Antenna(String name, AntennaType type, Coordinate position, Coordinate offset,
            double diameter, Date commissionDate, int loOffsettingIndex,
            int walshSeq) {
        this(name, type, position, offset, diameter,
             UTCUtility.utcJavaToOmg(commissionDate.getTime()), loOffsettingIndex,
             walshSeq);
    }
    
    /**
     * Public constructor.
     * 
     * @param name Antenna name
     * @param type Antenna type
     * @param position Antenna position
     * @param offset Antenna position offset
     * @param diameter Antenna diameter
     * @param commissionDate Antenna comission date
     */
    public Antenna(String name, AntennaType type, Coordinate position, Coordinate offset,
            double diameter, long commissionDate, int loOffsettingIndex,
            int walshSeq) {        
        super(null, name, BaseElementType.Antenna);
        this.position = position;
        this.offset = offset;
        this.diameter = new Double(diameter);
        this.commissionDate = new Long(commissionDate);
        this.avgDelay = 0.0;
        this.antennaType = type;
        this.loOffsettingIndex = loOffsettingIndex;
        this.walshSeq = walshSeq;
    }

    public Coordinate getPosition() {
        return position;
    }

    public void setPosition(Coordinate position) {
        this.position = position;
    }

    public Coordinate getOffset() {
        return offset;
    }

    public void setOffset(Coordinate offset) {
        this.offset = offset;
    }

    public Double getDiameter() {
        return diameter;
    }

    public void setDiameter(Double diameter) {
        this.diameter = diameter;
    }

    public Long getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Long commissionDate) {
        this.commissionDate = commissionDate;
    }

    public AntennaType getAntennaType() {
        return antennaType;
    }

    public void setAntennaType(AntennaType type) {
        this.antennaType = type;
    }

    public Set<AntennaToPad> getScheduledPadLocations() {
        return scheduledPadLocations;
    }

    public void setScheduledPadLocations(Set<AntennaToPad> scheduledPadLocations) {
        this.scheduledPadLocations = scheduledPadLocations;
    }

    public Set<AntennaToFrontEnd> getScheduledFrontEnds() {
        return scheduledFrontEnds;
    }

    public void setScheduledFrontEnds(Set<AntennaToFrontEnd> scheduledFrontEnds) {
        this.scheduledFrontEnds = scheduledFrontEnds;
    }

    public Set<FocusModel> getFocusModels() {
        return focusModels;
    }

    public void setFocusModels(Set<FocusModel> focusModels) {
        this.focusModels = focusModels;
    }

    public  Double getAvgDelay() {
        return avgDelay;
    }

    public void setAvgDelay(Double avgDelay) {
        this.avgDelay = avgDelay;
    }
    
    /**
     * Returns the Pad where, according to the TMCDB configuration, the
     * Antenna is installed. By convention, this is the record in the
     * AntennaToPad table that has a null endTime for a given Antenna and
     * Pad.
     * @return Current Pad, or null if there is no such record in the
     * database. 
     */
    public Pad getCurrentPad() {
        for (Iterator<AntennaToPad> iter = scheduledPadLocations.iterator();
             iter.hasNext();) {
            AntennaToPad a2p = iter.next();
            if (a2p.getEndTime() == null)
                return a2p.getPad();
        }
        return null;
    }

    public Integer getLoOffsettingIndex() {
        return loOffsettingIndex;
    }

    public void setLoOffsettingIndex(Integer loOffsettingIndex) {
        this.loOffsettingIndex = loOffsettingIndex;
    }

    public Integer getWalshSeq() {
        return walshSeq;
    }

    public void setWalshSeq(Integer walshSeq) {
        this.walshSeq = walshSeq;
    }

    public Set<PointingModel> getPointingModels() {
        return pointingModels;
    }

    public void setPointingModels(Set<PointingModel> pointingModels) {
        this.pointingModels = pointingModels;
    }

    public Set<FEDelay> getFrontEndDelays() {
        return frontEndDelays;
    }

    public void setFrontEndDelays(Set<FEDelay> frontEndDelays) {
        this.frontEndDelays = frontEndDelays;
    }

    public Set<IFDelay> getIfDelays() {
        return ifDelays;
    }

    public void setIfDelays(Set<IFDelay> ifDelays) {
        this.ifDelays = ifDelays;
    }

    public Set<LODelay> getLoDelays() {
        return loDelays;
    }

    public void setLoDelays(Set<LODelay> loDelays) {
        this.loDelays = loDelays;
    }
    
}
