/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: HolographyTowerToPad.java,v 1.2 2010/11/16 01:18:49 sharring Exp $"
 */
package alma.tmcdb.domain;


public class HolographyTowerToPad {
    
    private HolographyTower holographyTower;
    private Pad pad;
    private Double azimuth;
    private Double elevation;
    private Integer holographyTowerToPadId;
    
    public HolographyTowerToPad() {}
    
    public HolographyTowerToPad(HolographyTower holographyTower, Pad pad) {
        this.holographyTower = holographyTower;
        this.pad = pad;
        this.azimuth = 0.0;
        this.elevation = 0.0;
        this.holographyTower.getAssociatedPads().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof HolographyTowerToPad))
            return false;
        HolographyTowerToPad a2p = (HolographyTowerToPad) o;
        return (holographyTower == null ? a2p.holographyTower == null :
            holographyTower.equals(a2p.holographyTower)) &&
               (pad == null ? a2p.pad == null :
                pad.equals(a2p.pad));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((holographyTower == null) ? 0 : holographyTower.hashCode());
        result = 31 * result + ((pad == null) ? 0 : pad.hashCode());
        return result;
    }
    
    public Integer getHolographyTowerToPadId() {
        return holographyTowerToPadId;
    }

    public void setHolographyTowerToPadId(Integer id) {
       this.holographyTowerToPadId = id;
    }

    public HolographyTower getHolographyTower() {
        return holographyTower;
    }

    public void setHolographyTower(HolographyTower holographyTower) {
        this.holographyTower = holographyTower;
    }

    public Pad getPad() {
        return pad;
    }

    public void setPad(Pad pad) {
        this.pad = pad;
    }
    
    public Double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(Double azimuth) {
        this.azimuth = azimuth;
    }

    public Double getElevation() {
        return elevation;
    }

    public void setElevation(Double elevation) {
        this.elevation = elevation;
    }
}
