/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: WeatherStationController.java,v 1.3 2011/05/02 08:47:48 rtobar Exp $"
 */
package alma.tmcdb.domain;

import java.util.Date;

import alma.acs.util.UTCUtility;

public class WeatherStationController extends BaseElement 
{
    private Long commissionDate;
    
    public WeatherStationController() {}

    public WeatherStationController(String name, Date commissionDate) {
        this(name, UTCUtility.utcJavaToOmg(commissionDate.getTime()));
    }
    
    public WeatherStationController(String name, Long commissionDate) {
        super(null, name, BaseElementType.WeatherStationController);
        this.commissionDate = commissionDate;
    }
    
    public Long getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Long commissionDate) {
        this.commissionDate = commissionDate;
    }
}
