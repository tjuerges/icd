/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: PointingModelCoeff.java,v 1.5 2010/12/16 21:56:05 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import java.util.HashMap;
import java.util.Map;

import alma.ReceiverBandMod.ReceiverBand;
import alma.tmcdb.history.interceptor.PointingModelCoeffVersionKeeper;
import alma.tmcdb.history.interceptor.Versionable;

public class PointingModelCoeff extends Versionable {
    
    private Long id;
    private PointingModel pointingModel;
    private String name;
    private float value;
    private Map<ReceiverBand, Double> offsets = new HashMap<ReceiverBand, Double>();

    public PointingModelCoeff() { }

    public PointingModelCoeff(String name, float value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PointingModelCoeff))
            return false;
        PointingModelCoeff other = (PointingModelCoeff) o;
        if (pointingModel == null) {
            if (other.pointingModel != null)
                return false;
        } else if (!pointingModel.equals(other.pointingModel))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;        
        if (!(Double.compare(value, other.value) == 0))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + pointingModel.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + Float.floatToIntBits(value); 
        return result;
    }
    
    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Map<ReceiverBand, Double> getOffsets() {
        return offsets;
    }

    public void setOffsets(Map<ReceiverBand, Double> offsets) {
        this.offsets = offsets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Class getVersionWriterClass() {
        return PointingModelCoeffVersionKeeper.class;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PointingModel getPointingModel() {
        return pointingModel;
    }

    public void setPointingModel(PointingModel pointingModel) {
        this.pointingModel = pointingModel;
    }
}
