/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: BaseElementStartup.java,v 1.17 2011/02/17 22:25:53 sharring Exp $"
 */
package alma.tmcdb.domain;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.HashSet;
import java.util.Set;

public class BaseElementStartup implements Serializable {
    
    private static final long serialVersionUID = -2950551800985491826L;
        
    private Long id;
    private StartupScenario startup;
    private BaseElement baseElement;
    private BaseElementStartupType type;
    private Set<AssemblyStartup> assemblyStartups = new HashSet<AssemblyStartup>();
    private Set<BaseElementStartup> children = new HashSet<BaseElementStartup>();
    private BaseElementStartup parent;
    private String generic;
    private Boolean simulated;

    public BaseElementStartup() {};
    
    /**
     * Creates a concrete BaseElementStartup. With a concrete BaseElementStartup
     * the specific BaseElement that needs to be started is specified. These
     * BaseElementStartups are always related with a StartupScenario.
     * @param baseElement BaseElement to start
     * @param startup Startup scenario
     */
    public BaseElementStartup(BaseElement baseElement, StartupScenario startup) {
        super();
        this.baseElement = baseElement;
        this.startup = startup;
        if ((baseElement.getType() != BaseElementType.PhotonicReference) &&
                (baseElement.getType() != BaseElementType.FrontEnd))
            this.type = BaseElementStartupType.valueOf(baseElement.getType().toString());
        else
            throw new InvalidParameterException("BaseElement parameter must be non-generic");
        this.generic = "false";
        startup.addBaseElementStartup(this);
    }

    /**
     * Creates a generic BaseElementStartup. A generic BaseElementStartup doesn't
     * specify the specific BaseElement to start. This is the case of the FrontEnds
     * and the PhotonicReceivers.
     * A generic BaseElementStartup always has a concrete BaseElementStartup as
     * parent, and is not related directly with a StartupScenario. 
     * @param type
     */
    public BaseElementStartup(BaseElementStartupType type) {
        super();
        this.type = type;
        this.generic = "true";
    }
    
    @Override
    public boolean equals(Object o) 
    {
        boolean retVal = false;
        if (o == this)
            return true;

        if (!(o instanceof BaseElementStartup))
            return false;

        BaseElementStartup bes = (BaseElementStartup) o;

        // handle generic baseelementstartups by comparing their types & parents
        if(getBaseElement() == null && bes.getBaseElement() == null)
        {
           if(getType().equals(bes.getType()))
           { 
               if(null == getParent() && null == bes.getParent())
               {
                  retVal = true;
               }
               else if(null != getParent() && getParent().equals(bes.getParent())) 
               {
                  retVal = true;
               }
               else {
                  retVal = false;
               }
           }
           else 
           {
               retVal = false;
           }
        }
        else  
        {
           retVal = (getBaseElement() == null ? bes.getBaseElement() == null : getBaseElement().equals(bes.getBaseElement())) && 
              (getStartup() == null ? bes.getStartup() == null : getStartup().equals(bes.getStartup())) &&
              (getType().name().equals(bes.getType().name())); 
        } 

        return retVal;
    }

    @Override
    public int hashCode() 
    {
        int result = 17;
        result = 31 * result + ((getBaseElement() == null) ? ((getParent() == null) ? 0 : getParent().hashCode()) : getBaseElement().hashCode());
        result = 31 * result + ((getStartup() == null) ? 0 : getStartup().hashCode());
        result = 31 * result + ((getType() == null) ? 0 : getType().hashCode());
        return result;
    }
    
    public StartupScenario getStartup() {
        return startup;
    }

    public void setStartup(StartupScenario startup) {
        this.startup = startup;
    }

    public BaseElement getBaseElement() {
        return baseElement;
    }

    public void setBaseElement(BaseElement baseElement) {
        this.baseElement = baseElement;
    }

    public Set<AssemblyStartup> getAssemblyStartups() {
        return assemblyStartups;
    }

    public void setAssemblyStartups(Set<AssemblyStartup> assemblyStartups) {
        this.assemblyStartups = assemblyStartups;
    }
    
    public void addAssemblyStartup(AssemblyStartup assemblyStartup) {
        assemblyStartup.setBaseElementStartup(this);
        assemblyStartups.add(assemblyStartup);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BaseElementStartupType getType() {
        return type;
    }

    public void setType(BaseElementStartupType type) {
        this.type = type;
    }

    public Set<BaseElementStartup> getChildren() {
        return children;
    }

    public void setChildren(Set<BaseElementStartup> children) {
        this.children = children;
    }

    public String isGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }
 
    public BaseElementStartup getParent() {
        return parent;
    }
 
    public void setParent(BaseElementStartup parent) {
        this.parent = parent;
    }

    public Boolean getSimulated() {
        return simulated;
    }

    public void setSimulated(Boolean sim) {
        this.simulated = sim;
    }
}
