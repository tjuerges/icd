/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: BaseElement.java,v 1.10 2010/10/26 23:05:48 rhiriart Exp $"
 */
package alma.tmcdb.domain;

public class BaseElement {
    
    protected Long id;
    
    protected String name;

    private HwConfiguration configuration;
    
    private BaseElementType type;

    public BaseElement() {}
    
    public BaseElement(HwConfiguration conf, String name, BaseElementType type) {
        this.name = name;
        this.type = type;
        if (conf != null)
            conf.addBaseElement(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof BaseElement))
            return false;
        BaseElement be = (BaseElement) o;
        return (getConfiguration() == null ? be.getConfiguration() == null :
                getConfiguration().equals(be.getConfiguration())) &&
               (getName() == null ? be.getName() == null : getName().equals(be.getName())) &&
               (getType() == null ? be.getType() == null : getType().name().equals(be.getType().name())) ;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((getConfiguration() == null) ? 0 :
                 getConfiguration().hashCode());
        result = 31 * result + ((getName() == null) ? 0 : getName().hashCode());
        result = 31 * result + ((getType() == null) ? 0 : getType().name().hashCode());
        return result;
    }
    
    public HwConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(HwConfiguration configuration) {
        this.configuration = configuration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseElementType getType() {
        return type;
    }

    public void setType(BaseElementType type) {
        this.type = type;
    }
    
}
