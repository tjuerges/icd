/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: IFDelay.java,v 1.4 2010/10/27 20:22:38 rhiriart Exp $"
 */
package alma.tmcdb.domain;

import alma.BasebandNameMod.BasebandName;
import alma.PolarizationTypeMod.PolarizationType;

public class IFDelay {

    private Long id;
    private BasebandName baseband;
    private PolarizationType polarization;
    private IFProcConnectionState ifSwitch;
    private Double delay;
    
    public IFDelay() {}

    public IFDelay(BasebandName baseband, PolarizationType polarization, IFProcConnectionState ifSwitch,
            double delay) {
        this.baseband = baseband;
        this.polarization = polarization;
        this.ifSwitch = ifSwitch;
        this.delay = delay;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public BasebandName getBaseband() {
        return baseband;
    }
    
    public void setBaseband(BasebandName baseband) {
        this.baseband = baseband;
    }
    
    public PolarizationType getPolarization() {
        return polarization;
    }
    
    public void setPolarization(PolarizationType polarization) {
        this.polarization = polarization;
    }
    
    public Double getDelay() {
        return delay;
    }
    
    public void setDelay(Double delay) {
        this.delay = delay;
    }

    public IFProcConnectionState getIfSwitch() {
        return ifSwitch;
    }

    public void setIfSwitch(IFProcConnectionState ifSwitch) {
        this.ifSwitch = ifSwitch;
    }
}
