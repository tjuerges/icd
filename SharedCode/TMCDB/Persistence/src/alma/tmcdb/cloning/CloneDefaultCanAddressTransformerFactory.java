package alma.tmcdb.cloning;

import java.util.Set;

import alma.acs.tmcdb.Component;
import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;

public class CloneDefaultCanAddressTransformerFactory implements Factory {

	private Set<Component> _candidateComponents;
	private String         _originalPathSubstring;
	private String         _finalPathSubstring;

	public CloneDefaultCanAddressTransformerFactory(Set<Component> candidateComponents,
			String originalPathSubstring, String finalPathSubstring) {
		_candidateComponents = candidateComponents;
		_originalPathSubstring = originalPathSubstring;
		_finalPathSubstring = finalPathSubstring;
	}

	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(
			BeanTransformerSpi contextBeanTransformer) {
		return new CloneDefaultCanAddressTransformer(contextBeanTransformer,
		       _candidateComponents, _originalPathSubstring, _finalPathSubstring);
	}

}
