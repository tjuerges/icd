package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;
import alma.acs.tmcdb.Configuration;

/**
 * Transformer factory used when cloning base elements using beanlib.
 * @author sharring
 */
public class CloneContainersTransformerFactory implements Factory 
{

	private String oldName;
	private String newName;
	private Configuration addToConfiguration;

	public CloneContainersTransformerFactory(String oldName, String newName, Configuration addToConfiguration) {
		this.oldName = oldName;
		this.newName = newName;
		this.addToConfiguration = addToConfiguration;
	}

	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(
			BeanTransformerSpi contextBeanTransformer) 
	{
		return new CloneContainersTransformer(contextBeanTransformer, oldName, newName, addToConfiguration);
	}

}
