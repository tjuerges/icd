package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;


/**
 * Factory to return a transformer which will 'pass through' (i.e. return the identical object reference)
 * any 'global' domain classes.
 * 
 * @author sharrington
 */
public class StartupScenarioTransformerFactory implements CustomBeanTransformerSpi.Factory 
{
	public CustomBeanTransformerSpi newCustomBeanTransformer(BeanTransformerSpi beanTransformer) {
		return new StartupScenarioTransformer(beanTransformer);
	}
}
