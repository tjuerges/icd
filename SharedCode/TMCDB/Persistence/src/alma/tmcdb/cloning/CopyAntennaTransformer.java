package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.Pad;

public class CopyAntennaTransformer extends ConfigurationGlobalTransformer
{
	private HwConfiguration addToConfiguration;
	private String newName;
	
	public CopyAntennaTransformer(BeanTransformerSpi beanTransformer, HwConfiguration addToConfiguration, String newName) 
	{
		super(beanTransformer);
		this.newName = newName;
		this.addToConfiguration = addToConfiguration;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}
		else if(toClass == HwConfiguration.class || 
				toClass == Configuration.class ||
				toClass == Component.class || 
				toClass == Container.class || 
				toClass == Pad.class ||
				toClass == AntennaToPad.class || 
				toClass == AntennaToFrontEnd.class ||
				toClass == Antenna.class) 
		{
			retVal = true;
		} 

		return retVal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		T retVal = null;

		// 1) reassign hwconfiguration to the new hwconfiguration
		//    into which we're copying this antenna
		if(HwConfiguration.class.isAssignableFrom(toClass) && null != in) 
		{
			retVal = (T)addToConfiguration;
		} 
		
		// 2) reassign sw configuration to the new sw configuration
		//    into which we're copying this antenna
		else if(Configuration.class.isAssignableFrom(toClass) && null != in) {
			retVal = (T)addToConfiguration.getSwConfiguration();
		}
		
		// 3) don't copy pad, antennatopad, antennatofrontend, container objects at all  
		else if(toClass == Pad.class || toClass == AntennaToPad.class || 
                        toClass == AntennaToFrontEnd.class || Container.class.isAssignableFrom(toClass))
		{
			retVal = null;	
		}
		
		// 4) handle antenna object in a special way: clear out the collections of a2p, & a2fe objects
		else if(toClass == Antenna.class) {
			retVal =  (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, in.getClass());
			Antenna antenna = (Antenna) retVal;
			antenna.getScheduledFrontEnds().clear();
			antenna.getScheduledPadLocations().clear();
			CloneAntennaTransformer.zeroOutDelayPointingAndFocusModel(antenna);
		}
		
		// clone the component, and rename
		else if(toClass == Component.class && in != null) {
			retVal = (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, in.getClass());
			Component comp = (Component)retVal;
			comp.setComponentName( comp.getComponentName().replaceAll(comp.getComponentName(), newName) );
		}
		
		else {
			retVal = super.transform(in, toClass, propertyInfo);
		}
		
		return retVal;
	}
}
