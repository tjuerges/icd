package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;
import alma.acs.tmcdb.Component;

/**
 * Factory to create transformer objects used during cloning of baci properties (part of antenna cloning).
 * @author sharring
 */
public class CloneBACIPropertiesTransformerFactory implements Factory 
{
	private Component component;
	
	public CloneBACIPropertiesTransformerFactory(Component component) 
	{
		this.component = component;
	}
	
	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(BeanTransformerSpi contextBeanTransformer) 
	{
		return new CloneBACIPropertiesTransformer(contextBeanTransformer,  component);
	}
}
