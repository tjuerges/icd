package alma.tmcdb.cloning;

import java.util.Set;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;

import org.hibernate.collection.PersistentSet;

import alma.tmcdb.domain.AbstractBaseElementStartup;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.GenericBaseElementStartup;
import alma.tmcdb.domain.StartupScenario;

/**
 * Used in cloning; custom transformer to handle startup scenarios; specifically, reuses many of the 'global' 
 * (and/or other items that can be shared within a configuration) from the startup scenario that is being cloned,
 * rather than blindly cloning everything recursively.
 * 
 * @author sharrington
 */
public class StartupScenarioTransformer extends ConfigurationGlobalTransformer
{
	public StartupScenarioTransformer(BeanTransformerSpi contextBeanTransformer) 
	{
		super(contextBeanTransformer);
		// TODO Auto-generated constructor stub
	}

	@Override
	@SuppressWarnings("unchecked")
	public  boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		// this transformer handles a) anything that is 'global' (handled by our super class)
		// plus anything other than things ending in the word 'startup'. In other words, when
		// cloning a startup, we reuse all the things within a configuration (plus the globals)
		// and only clone the BaseElementStartup, AssemblyStartup, and StartupScenario objects.

		if(super.isTransformable(from, toClass, propertyInfo)) {
			retVal = true;
		}

		if(toClass == AssemblyStartup.class || toClass == BaseElementStartup.class || toClass == Set.class || 
				toClass == StartupScenario.class || toClass == PersistentSet.class ||
				toClass == AbstractBaseElementStartup.class || toClass == GenericBaseElementStartup.class ) 
		{
			retVal = false;
		} else {
			retVal = true;
		}
		
		return retVal;
	}
}
