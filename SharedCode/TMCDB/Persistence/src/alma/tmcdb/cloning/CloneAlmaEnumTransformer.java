package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import alma.BasebandNameMod.BasebandName;
import alma.NetSidebandMod.NetSideband;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;

public class CloneAlmaEnumTransformer implements CustomBeanTransformerSpi
{
   protected BeanTransformerSpi defaultBeanTransformer;

   public CloneAlmaEnumTransformer(BeanTransformerSpi beanTransformer)
   {
      this.defaultBeanTransformer = beanTransformer;
   }

   public boolean isTransformable(Object from, Class<?> toClass, PropertyInfo propertyInfo) 
   {
      return (toClass == ReceiverBand.class ||
    		  toClass == BasebandName.class || 
    		  toClass == PolarizationType.class ||
    		  toClass == NetSideband.class);
   }

   @SuppressWarnings("unchecked")
   public <T> T transform(Object in, Class<T> toClass, PropertyInfo propertyInfo) 
   {
	   // Default behavior: global tables' rows are just copied
	   T retVal = (T)in;
	   return retVal;
   }
}
