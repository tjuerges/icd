package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.BACIProperty;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;

/**
 * Beanlib transformer used when cloning components.
 * 
 * To clone a component, the path+name must be changed somehow. Either way, we will end up in a
 * constraint violation error. This transformer replaces a given part of the path of the component for a new one.
 * 
 * @author rtobar, Mar 30, 2010
 */
public class CloneComponentsTransformer extends ConfigurationGlobalTransformer  
{

	private Configuration _addToConfiguration;
	private ComponentNameReplacer _replacer;

	/**
	 * Constructor.
	 */
	public CloneComponentsTransformer(BeanTransformerSpi beanTransformer, ComponentNameReplacer replacer, Configuration addToConfiguration) 
	{
		super(beanTransformer);
		_addToConfiguration = addToConfiguration;
		_replacer = replacer;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}
		// We check with isAssignableFrom instead of doing == between the classes
		// because we may get hibernate proxies instead of the actual instances
		// of our domain classes
		else if(Configuration.class.isAssignableFrom(toClass) ||
				Component.class.isAssignableFrom(toClass) || 
				Container.class.isAssignableFrom(toClass) ||
				BACIProperty.class.isAssignableFrom(toClass) ) 
		{
			retVal = true;
		} 

		return retVal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		T retVal = (T)in;

		// Replace the path
		if(Component.class.isAssignableFrom(toClass) && in != null) 
		{
			retVal = (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, Component.class);
			Component comp = (Component)retVal;
			_replacer.replaceName(comp);
		} 

		if(Configuration.class.isAssignableFrom(toClass) && in != null) {
			retVal = (T) _addToConfiguration;
		}

		return retVal;
	}
}