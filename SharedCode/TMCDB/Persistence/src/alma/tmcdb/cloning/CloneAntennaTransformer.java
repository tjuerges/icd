package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.AntennaToFrontEnd;
import alma.tmcdb.domain.AntennaToPad;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.Pad;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import alma.tmcdb.domain.FEDelay;
import alma.tmcdb.domain.IFDelay;
import alma.tmcdb.domain.LODelay;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.domain.FocusModel;
import alma.tmcdb.domain.FocusModelCoeff;
import alma.ReceiverBandMod.ReceiverBand;

/**
 * Beanlib transformer used when cloning base elements.
 * @author sharring
 */
public class CloneAntennaTransformer extends ConfigurationGlobalTransformer  
{

	private String oldName;
	private String newName;

	/**
	 * Constructor.
	 */
	public CloneAntennaTransformer(BeanTransformerSpi beanTransformer, String oldName, String newName) 
	{
		super(beanTransformer);
		this.oldName = oldName;
		this.newName = newName;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}
		else if(toClass == HwConfiguration.class || 
				toClass == Component.class || 
				toClass == Container.class ||
				toClass == Configuration.class ||
				toClass == Pad.class ||
				toClass == AntennaToPad.class || 
				toClass == AntennaToFrontEnd.class ||
				toClass == Antenna.class) 
		{
			retVal = true;
		} 

		return retVal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		// 1) reuse as-is (without cloning) the existing elements of: 
		//    HwConfiguration, Configuration, Pad
		T retVal = (T)in;

		// 2) skip cloning (altogether) AntennaToPad, & AntennaToFrontEnd mapping classes
		if((toClass == AntennaToPad.class || toClass == AntennaToFrontEnd.class) && null != in) 
		{
			retVal = null;
		}
		
		// 3) skip cloning the antenna's antenna-to-pad and antenna-to-frontend collections
		//    but clone everything else about the antenna
		else if(toClass == Antenna.class && null != in)
		{
			retVal =  (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, in.getClass());
			Antenna antenna = (Antenna) retVal;
			antenna.setName( antenna.getName().replaceAll(oldName, newName) );
			antenna.getScheduledFrontEnds().clear();
			antenna.getScheduledPadLocations().clear();
                        zeroOutDelayPointingAndFocusModel(antenna);
		}

		return retVal;
	}

	// Per COMP-5071, don't copy delay model, pointing model, or focus model
	static void zeroOutDelayPointingAndFocusModel(Antenna antenna)
	{
		Set<IFDelay> ifdelays = antenna.getIfDelays();
		for(IFDelay delay : ifdelays)
		{
			delay.setDelay(0.0);	
		}
		Set<FEDelay> fedelays = antenna.getFrontEndDelays();
		for(FEDelay delay : fedelays)
		{
			delay.setDelay(0.0);	
		}
		Set<LODelay> lodelays = antenna.getLoDelays();
		for(LODelay delay : lodelays)
		{
			delay.setDelay(0.0);	
		}

		Set<PointingModel> pointingModels = antenna.getPointingModels();
		if(pointingModels.size() > 0) 
		{
			PointingModel pm = pointingModels.iterator().next();
			Map<String, PointingModelCoeff> terms = pm.getTerms();
			for(PointingModelCoeff term : terms.values())
			{
				term.setValue(0.0f);
				Map<ReceiverBand, Double> offsets = term.getOffsets();
				for(Entry<ReceiverBand, Double> entry : offsets.entrySet()) {
					entry.setValue(0.0);
				}
			}
		}
		
		Set<FocusModel> focusModels = antenna.getFocusModels();
		if(focusModels.size() > 0) 
		{
			FocusModel pm = focusModels.iterator().next();
			Map<String, FocusModelCoeff> terms = pm.getTerms();
			for(FocusModelCoeff term : terms.values())
			{
				term.setValue(0.0f);
				Map<ReceiverBand, Double> offsets = term.getOffsets();
				for(Entry<ReceiverBand, Double> entry : offsets.entrySet()) {
					entry.setValue(0.0);
				}
			}
		}
	}
}
