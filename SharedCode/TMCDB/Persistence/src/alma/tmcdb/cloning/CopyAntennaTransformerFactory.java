package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;
import alma.tmcdb.domain.HwConfiguration;

public class CopyAntennaTransformerFactory implements Factory 
{
	private HwConfiguration addToConfiguration;
	private String newName;
	
	public CopyAntennaTransformerFactory(HwConfiguration addToConfiguration, String newName) 
	{
		this.addToConfiguration = addToConfiguration;
		this.newName = newName;
	}
	
	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(BeanTransformerSpi contextBeanTransformer) 
	{
		return new CopyAntennaTransformer(contextBeanTransformer, addToConfiguration, newName);
	}
}
