package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.BACIProperty;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;

/**
 * Transformer for copying baci properties (used during cloning of antenna baseelement, e.g.)
 * @author sharring
 */
public class CloneBACIPropertiesTransformer extends	ConfigurationGlobalTransformer 
{
	private Component component;
	
	/**
	 * Constructor.
	 */
	public CloneBACIPropertiesTransformer(BeanTransformerSpi beanTransformer, Component component) 
	{
		super(beanTransformer);
		this.component = component;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}
		// We check with isAssignableFrom instead of doing == between the classes
		// because we may get hibernate proxies instead of the actual instances
		// of our domain classes
		else if(Configuration.class.isAssignableFrom(toClass) ||
				Component.class.isAssignableFrom(toClass) || 
				BACIProperty.class.isAssignableFrom(toClass) ) 
		{
			retVal = true;
		} 

		return retVal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		T retVal = (T)in;

		if(BACIProperty.class.isAssignableFrom(toClass) && in != null) 
		{
			retVal = (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, BACIProperty.class);
		}
		
		if(Component.class.isAssignableFrom(toClass) && in != null) {
			retVal = (T)component;
		}

		return retVal;
	}

}
