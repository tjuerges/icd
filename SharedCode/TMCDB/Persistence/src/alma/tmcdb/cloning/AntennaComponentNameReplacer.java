package alma.tmcdb.cloning;

import alma.acs.tmcdb.Component;

/**
 * A {@link ComponentNameReplacer} that replaces a component's name based on
 * an old and new antenna names.
 *
 * @author rtobar, Apr 28th, 2011
 *
 */
public class AntennaComponentNameReplacer implements ComponentNameReplacer {

	private String _oldAntennaName;
	private String _newAntennaName;

	public AntennaComponentNameReplacer(String oldAntennaName, String newAntennaName) {
		_oldAntennaName = oldAntennaName;
		_newAntennaName = newAntennaName;
	}

	@Override
	public void replaceName(Component comp) {
		if( comp.getComponentName().equals(_oldAntennaName) )
			comp.setComponentName(_newAntennaName);
		else
			comp.setPath( comp.getPath().replaceAll(_oldAntennaName, _newAntennaName) );
	}

}
