package alma.tmcdb.cloning;

import alma.acs.tmcdb.Component;

/**
 * A {@link ComponentNameReplacer} that always replaces the component's name
 * and path for the given ones at construction time, provided that their values
 * are not null.
 *
 * @author rtobar, Apr 28th, 2011
 *
 */
public class NameAndPathComponentNameReplacer implements ComponentNameReplacer {

	private String _newName;
	private String _newPath;

	public NameAndPathComponentNameReplacer(String newName, String newPath) {
		if( newName == null && newPath == null )
			throw new RuntimeException("Cannot replace noth name and path in cloned component with null");

		_newName = newName;
		_newPath = newPath;
	}

	@Override
	public void replaceName(Component comp) {
		if( _newName != null )
			comp.setComponentName(_newName);
		if( _newPath != null )
			comp.setPath(_newPath);
	}

}
