package alma.tmcdb.cloning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import junit.framework.TestCase;
import alma.acs.tmcdb.AcsService;
import alma.acs.tmcdb.AlarmDefinition;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Computer;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Contact;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.ContainerStartupOption;
import alma.acs.tmcdb.FaultCode;
import alma.acs.tmcdb.FaultFamily;
import alma.acs.tmcdb.FaultMember;
import alma.acs.tmcdb.Location;
import alma.acs.tmcdb.LoggingConfig;
import alma.acs.tmcdb.NamedLoggerConfig;
import alma.acs.tmcdb.NetworkDevice;
import alma.acs.tmcdb.ReductionLink;
import alma.acs.tmcdb.ReductionThreshold;
import alma.acs.tmcdb.Schemas;
import alma.tmcdb.domain.Antenna;
import alma.tmcdb.domain.Assembly;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.BaseElementType;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.FrontEnd;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.CentralLO;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.domain.Pad;
import alma.tmcdb.domain.StartupScenario;

import com.ice.tar.TarArchive;

/**
 * Utiility class for all cloning related tests, containing methods
 * for comparing domain entities (necessary because domain entities' 
 * equals methods are not useful for content comparison; they are useful
 * primarily in a hibernate context in that they often only check for the
 * object identity being equal).
 * 
 * @author sharrington
 *
 */
public class CloningTestUtils 
{
	private static final String ACSROOT = "ACSROOT";
	private static final String INTROOT = "INTROOT";
	private static final String USER_DIR = "user.dir";
	private static final String TMCDBSAMPLE_TAR_GZ = "TMCDBSample.tar.gz";
	private static final String TMCDB = "TMCDB";
	private static final String TMCDBSAMPLE_TAR = "TMCDBSample.tar";
	
	private CloningTestUtils() 
	{
		// private constructor to enforce static-methods-only utility class.
	}
	
	public static boolean safeEquals(Long one, Long two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}

		return retVal;
	}

	public static boolean safeEquals(Integer one, Integer two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}

		return retVal;
	}

	public static boolean safeEquals(String one, String two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}

		return retVal;
	}

	public static boolean safeEquals(Schemas one, Schemas two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getSchema(), two.getSchema()) && 
					safeEquals(one.getURN(), two.getURN()) )
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(LruType one, LruType two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getDescription(), two.getDescription()) &&
					safeEquals(one.getFullName(), two.getFullName()) &&
					safeEquals(one.getIcd(), two.getIcd()) &&
					safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getNotes(), two.getNotes()) &&
					one.getIcdDate() == two.getIcdDate())
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(ComponentType one, ComponentType two)
	{	
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getIDL(), two.getIDL()))
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(BaseElementType one, BaseElementType two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.name(), two.name()))
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(Coordinate one, Coordinate two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(one.getX() == two.getX() &&
					one.getY() == two.getY() &&
					one.getZ() == two.getZ() )
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(Pad one, Pad two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getPosition(), two.getPosition()) &&
					one.getType().equals(two.getType()) &&
					one.getCommissionDate().longValue() == two.getCommissionDate().longValue()) 
				// TODO: scheduled antennas?
						{
				retVal = true;
						}
		}

		return retVal;
	}

	public static boolean safeEquals(Component one, Component two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		}
		else 
		{
			if(!safeEquals(one.getCode(), two.getCode())) {
				retVal = false; 
			} else if(!safeEquals(one.getImplLang(), two.getImplLang())) {
				retVal = false;
			} else if(!safeEquals(one.getComponentName(), two.getComponentName())) {
				retVal = false;
			} else if(!safeEquals(one.getPath(), two.getPath())) {
				retVal = false;
			/*} else if(!safeEquals(one.getURN(), two.getURN())) {
				retVal = false; */
			} else if(!safeEquals(one.getXMLDoc(), two.getXMLDoc())) {
				retVal = false;
			} else if(!safeEquals(one.getMinLogLevel(), two.getMinLogLevel())) {
				retVal = false;
			} else if(!safeEquals(one.getKeepAliveTime(), two.getKeepAliveTime())) {
				retVal = false;
			} else if(!safeEquals(one.getMinLogLevel(), two.getMinLogLevel())) {
				retVal = false;
			} else if(!safeEquals(one.getComponentType(), two.getComponentType())) {
				retVal = false;
			} else {
				retVal = true;
			}
		}
		
		return retVal;
	}

	public static boolean safeEquals(Container one, Container two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		}
		else 
		{
			if( !safeEquals(one.getAutoloadSharedLibs(), two.getAutoloadSharedLibs()) ||
			    !safeEquals(one.getCallTimeout(), two.getCallTimeout()) ||
			    !safeEquals(one.getComputer(), two.getComputer()) ||
			    !safeEquals(one.getContainerName(), two.getContainerName()) ||
			    !safeEquals(one.getImplLang(), two.getImplLang()) ||
			    !safeEquals(one.getKeepAliveTime(), two.getKeepAliveTime()) ||
			    !safeEquals(one.getKernelModule(), two.getKernelModule()) ||
			    !safeEquals(one.getKernelModuleLocation(), two.getKernelModuleLocation()) ||
			    !safeEquals(one.getManagerRetry(), two.getManagerRetry()) ||
			    !safeEquals(one.getPath(), two.getPath()) ||
			    !safeEquals(one.getPingInterval(), two.getPingInterval()) ||
			    !safeEquals(one.getRealTime(), two.getRealTime()) ||
			    !safeEquals(one.getRealTimeType(), two.getRealTimeType()) ||
			    !safeEquals(one.getRecovery(), two.getRecovery()) ||
			    !safeEquals(one.getServerThreads(), two.getServerThreads()) ||
			    !safeEquals(one.getStartOnDemand(), two.getStartOnDemand()) ||
			    !safeEquals(one.getTypeModifiers(), two.getTypeModifiers()) ||
			    !safeEquals(one.getLoggingConfig(), two.getLoggingConfig()) )
				retVal = false;
			else {
				retVal = true;
			}
		}
		if( retVal )
			compareContainerStartupOptions(one, two);
		
		return retVal;
	}

	private static boolean safeEquals(LoggingConfig one, LoggingConfig two) {

		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		}
		else 
		{
			if( !safeEquals(one.getCentralizedLogger(), two.getCentralizedLogger()) ||
			    !safeEquals(one.getDispatchPacketSize(), two.getDispatchPacketSize()) ||
			    !safeEquals(one.getFlushPeriodSeconds(), two.getFlushPeriodSeconds()) ||
			    !safeEquals(one.getImmediateDispatchLevel(), two.getImmediateDispatchLevel()) ||
			    !safeEquals(one.getMaxLogQueueSize(), two.getMaxLogQueueSize()) ||
			    !safeEquals(one.getMinLogLevelDefault(), two.getMinLogLevelDefault()) ||
			    !safeEquals(one.getMinLogLevelLocalDefault(), two.getMinLogLevelLocalDefault()) )
				retVal = false;
			else {
				retVal = true;
			}
		}
		if( retVal )
			compareNamedLoggerConfigs(one, two);
		
		return retVal;
	}

	private static void compareNamedLoggerConfigs(LoggingConfig one,
			LoggingConfig two) {

		Set<NamedLoggerConfig> origNLCs = one.getNamedLoggerConfigs();
		for(NamedLoggerConfig origNLC : origNLCs) 
		{
			boolean found = false;
			Set<NamedLoggerConfig> clonedNLCs = two.getNamedLoggerConfigs();
			TestCase.assertEquals(origNLCs.size(), clonedNLCs.size());
			for(NamedLoggerConfig clonedNLC : clonedNLCs)
			{
				if(safeEquals(origNLC, clonedNLC))
				{
					found = true;
					break;
				}
			}
			TestCase.assertTrue(found);
		}
	}

	private static boolean safeEquals(NamedLoggerConfig one, NamedLoggerConfig two) {

		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		}
		else 
		{
			if( !safeEquals(one.getName(), two.getName()) ||
			    !safeEquals(one.getMinLogLevel(), two.getMinLogLevel()) ||
			    !safeEquals(one.getMinLogLevelLocal(), two.getMinLogLevelLocal()) )
				retVal = false;
			else {
				retVal = true;
			}
		}
		
		return retVal;
	}

	private static void compareContainerStartupOptions(Container one, Container two) {

		Set<ContainerStartupOption> origCSOs = one.getContainerStartupOptions();
		for(ContainerStartupOption origCSO : origCSOs) 
		{
			boolean found = false;
			Set<ContainerStartupOption> clonedCSOs = two.getContainerStartupOptions();
			TestCase.assertEquals(origCSOs.size(), clonedCSOs.size());
			for(ContainerStartupOption clonedCSO : clonedCSOs)
			{
				if(safeEquals(origCSO, clonedCSO))
				{
					found = true;
					break;
				}
			}
			TestCase.assertTrue(found);
		}
	}

	private static boolean safeEquals(ContainerStartupOption one, ContainerStartupOption two) {

		boolean retVal = false;
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		}
		else 
		{
			if( !safeEquals(one.getOptionName(), two.getOptionName()) ||
			    !safeEquals(one.getOptionType(), two.getOptionType()) ||
			    !safeEquals(one.getOptionValue(), two.getOptionValue()) )
				retVal = false;
			else {
				retVal = true;
			}
		}
		
		return retVal;
	}

	private static boolean safeEquals(Byte one, Byte two) {
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}

		return retVal;
	}

	public static boolean safeEquals(Antenna one, Antenna two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getType(), two.getType()) &&
					one.getAntennaType().equals(two.getAntennaType()) &&
					one.getCommissionDate().equals(two.getCommissionDate()) &&
					one.getDiameter().equals(two.getDiameter()) &&
					safeEquals(one.getOffset(), two.getOffset()) &&
					safeEquals(one.getPosition(), two.getPosition()))
				// TODO: scheduled pads?
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(FrontEnd one, FrontEnd two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getType(), two.getType()) &&
					one.getCommissionDate().equals(two.getCommissionDate())) 
				// TODO: antenna installations
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(CentralLO one, CentralLO two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getType(), two.getType()) &&
					one.getCommissionDate().equals(two.getCommissionDate()))
			{
				retVal = true;
			}
		}

		return retVal;
	}
	
	public static boolean safeEquals(BaseElement one, BaseElement two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()) &&
					safeEquals(one.getType(), two.getType())) 
			{
				// TODO: specialize for additional base element types
				if(one instanceof Antenna) {
					Antenna antOne = (Antenna)one;
					Antenna antTwo = (Antenna)two;
					retVal = safeEquals(antOne, antTwo);
				} else if(one instanceof Pad) {
					Pad padOne = (Pad) one;
					Pad padTwo = (Pad) two;
					retVal = safeEquals(padOne, padTwo);
				} else if(one instanceof FrontEnd) {
					FrontEnd frontEndOne = (FrontEnd) one;
					FrontEnd frontEndTwo = (FrontEnd) two;
					retVal = (safeEquals(frontEndOne, frontEndTwo));
				} else if(one instanceof CentralLO) {
					CentralLO loOne = (CentralLO) one;
					CentralLO loTwo = (CentralLO) two;
					retVal = safeEquals(loOne, loTwo);
				} 
				// TODO: other base element types: CorrHWConfiguration, Array, CentralRack, MasterClock, WeatherStation, HolographyTower. 
				// e.g. else if(one instanceof CorrHWConfiguration) etc...
				else {
					retVal = true;
				}
			}
		}

		return retVal;
	}

	public static boolean safeEquals(AssemblyRole one, AssemblyRole two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName()))
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(AssemblyType one, AssemblyType two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getDescription(), two.getDescription()) && 
					safeEquals(one.getFullName(), two.getFullName()) &&
					safeEquals(one.getName(), two.getName()) && 
					safeEquals(one.getNotes(), two.getNotes()) &&
					safeEquals(one.getComponentType(), two.getComponentType()) &&
					safeEquals(one.getBaseElementType(), two.getBaseElementType()) &&
					safeEquals(one.getLruType(), two.getLruType()) )
			{
				Set<AssemblyRole> oneRoles = one.getRoles();
				Set<AssemblyRole> twoRoles = two.getRoles();
				for(AssemblyRole roleOne: oneRoles) {
					boolean found = false;  
					for(AssemblyRole roleTwo: twoRoles) {
						if(safeEquals(roleOne, roleTwo)) {
							found = true;
							break;
						}
					}
					if(!found) {
						retVal = false;
						return retVal;
					}
				}
			}
		}

		return retVal;
	}

	public static boolean safeEquals(AssemblyStartup one, AssemblyStartup two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getAssemblyRole(), two.getAssemblyRole()))
			{
				retVal = true;
			}
		}
		return retVal;
	}

	public static boolean safeEquals(BaseElementStartup one, BaseElementStartup two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getBaseElement(), two.getBaseElement())) 
			{
				retVal = true;
			}
		}

		return retVal;
	}

	public static boolean safeEquals(StartupScenario one, StartupScenario two)
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else 
		{
			if(safeEquals(one.getName(), two.getName())) 
			{
				Set<AssemblyStartup> aStartups1 = one.getAssemblyStartups();
				Set<AssemblyStartup> aStartups2 = two.getAssemblyStartups();

				for(AssemblyStartup aStartup1 : aStartups1) {
					boolean found = false;
					for(AssemblyStartup aStartup2 : aStartups2) {
						if(safeEquals(aStartup1, aStartup2) && aStartup1 != aStartup2) {
							found = true;
						}
					}
					if(!found) {
						return false;
					}
				}

				Set<BaseElementStartup> bStartups1 = one.getBaseElementStartups();
				Set<BaseElementStartup> bStartups2 = two.getBaseElementStartups();
				for(BaseElementStartup bStartup1 : bStartups1) {
					boolean found = false;
					for(BaseElementStartup bStartup2 : bStartups2) {
						if(safeEquals(bStartup1, bStartup2) && bStartup1 != bStartup2) {
							found = true;
							break;
						}
					}
					if(!found) {
						return false;
					} 
				}
				retVal = true;
			}
		}

		return retVal;
	}

	public static void compareStartupScenarios(Set<StartupScenario> startups1, Set<StartupScenario> startups2)
	{
		for(StartupScenario startup1: startups1) {
			boolean found = false;
			for(StartupScenario startup2: startups2) {
				if(safeEquals(startup1, startup2)) {
					found = true;
				}
			}
			TestCase.assertEquals(true, found);
		}
	}

	public static void compareBaseElements(HwConfiguration config1, HwConfiguration config2)
	{
		Set<BaseElement> baseElements1 = config1.getBaseElements();
		Set<BaseElement> baseElements2 = config2.getBaseElements();
		for(BaseElement element1: baseElements1) {
			boolean found = false;
			for(BaseElement element2: baseElements2) {
				if(safeEquals(element1, element2)) {
					found = true;
					break;
				}
			}
			TestCase.assertEquals(true, found);
		}
	}

	public static void compareComponentsForHw(HwConfiguration config, HwConfiguration config2) 
	{
		compareComponentsForSw(config.getSwConfiguration(), config2.getSwConfiguration());
	}

	public static void compareSwConfigurations(Configuration one, Configuration two)
	{
		TestCase.assertTrue(safeEquals(one, two));
	}
	
	private static boolean safeEquals(Configuration one, Configuration two) 
	{
		boolean retVal;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else
		{
			retVal = true;
		}
		if(retVal == true) {
			compareContainerForSw(one, two);
			compareComponentsForSw(one, two);
			compareNetworkDevicesForSw(one, two);
			compareAlarmsForSw(one, two);
			compareAcsServicesForSw(one, two);
		}
		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static void compareAcsServicesForSw(Configuration one, Configuration two) {

		Set<AcsService> origAcsServices = one.getAcsServices();
		Set<AcsService> clonedAcsServices = two.getAcsServices();

		TestCase.assertEquals(origAcsServices.size(), clonedAcsServices.size());

		for(AcsService origAcsService: origAcsServices) {
			boolean found = false;
			for(AcsService clonedAcsService: clonedAcsServices) {
				if( safeEquals(origAcsService, clonedAcsService) ) {
					found = true;
					break;
				}
			}
			TestCase.assertTrue(found);
		}
	}

	private static void compareNetworkDevicesForSw(Configuration one, Configuration two) {

		Set<NetworkDevice> origAcsServices = one.getNetworkDevices();
		Set<NetworkDevice> clonedAcsServices = two.getNetworkDevices();

		TestCase.assertEquals(origAcsServices.size(), clonedAcsServices.size());

		for(NetworkDevice origAcsService: origAcsServices) {
			if( origAcsService instanceof Computer ) {
				boolean found = false;
				for(NetworkDevice clonedAcsService: clonedAcsServices) {
					if( clonedAcsService instanceof Computer &&
					    safeEquals((Computer)origAcsService, (Computer)clonedAcsService) ) {
						found = true;
						break;
					}
				}
				TestCase.assertTrue(found);
			}
		}
	}

	private static void compareAlarmsForSw(Configuration one, Configuration two) {

		Set<ReductionLink> origLinks = one.getReductionLinks();
		Set<ReductionLink> clonedLinks = two.getReductionLinks();

		TestCase.assertEquals(origLinks.size(), clonedLinks.size());

		for(ReductionLink origLink: origLinks) {
			boolean found = false;
			for(ReductionLink clonedLink: clonedLinks) {
				if( safeEquals(origLink, clonedLink) ) {
					found = true;
					break;
				}
			}
			TestCase.assertEquals(true, found);
		}
	}

	private static boolean safeEquals(AcsService one, AcsService two) {

		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getServiceInstanceName(), two.getServiceInstanceName())) &&
				(safeEquals(one.getServiceType(), two.getServiceType())) &&
				(safeEquals(one.getComputer(), two.getComputer())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		return retVal;
	}

	private static boolean safeEquals(Computer one, Computer two) {

		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getDiskless(), two.getDiskless())) &&
				(safeEquals(one.getName(), two.getName())) &&
				(safeEquals(one.getNetworkName(), two.getNetworkName())) &&
				(safeEquals(one.getPhysicalLocation(), two.getPhysicalLocation())) &&
				(safeEquals(one.getProcessorType(), two.getProcessorType())) &&
				(safeEquals(one.getRealTime(), two.getRealTime())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		return retVal;

	}

	private static boolean safeEquals(ReductionLink one, ReductionLink two) {

		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getAction(), two.getAction())) &&
				(safeEquals(one.getType(), two.getType())) &&
				(safeEquals(one.getAlarmDefinitionByChildalarmdefid(), two.getAlarmDefinitionByChildalarmdefid())) &&
				(safeEquals(one.getAlarmDefinitionByParentalarmdefid(), two.getAlarmDefinitionByParentalarmdefid())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		return retVal;
	}

	private static boolean safeEquals( AlarmDefinition one, AlarmDefinition two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getFaultCode(), two.getFaultCode())) &&
				(safeEquals(one.getFaultMember(), two.getFaultMember())) &&
				(safeEquals (one.getReductionThreshold(), two.getReductionThreshold())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(ReductionThreshold one, ReductionThreshold two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getValue(), two.getValue())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(FaultMember one, FaultMember two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getMemberName(), two.getMemberName())) &&
				(safeEquals(one.getLocation(), two.getLocation())) &&
				(safeEquals(one.getFaultFamily(), two.getFaultFamily())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(Location one, Location two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getBuilding(), two.getBuilding())) &&
				(safeEquals(one.getFloor(), two.getFloor())) &&
				(safeEquals(one.getLocationPosition(), two.getLocationPosition())) &&
				(safeEquals(one.getMnemonic(), two.getMnemonic())) &&
				(safeEquals(one.getRoom(), two.getRoom())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(FaultCode one, FaultCode two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getAction(), two.getAction())) &&
				(safeEquals(one.getCause(), two.getCause())) &&
				(safeEquals (one.getConsequence(), two.getConsequence())) &&
				(safeEquals(one.getCodeValue(), two.getCodeValue())) &&
				(safeEquals(one.getIsInstant(), two.getIsInstant())) &&
				(safeEquals (one.getPriority(), two.getPriority())) && 
				(safeEquals(one.getProblemDescription(), two.getProblemDescription())) && 
				(safeEquals(one.getFaultFamily(), two.getFaultFamily())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(FaultFamily one, FaultFamily two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getAlarmSource(), two.getAlarmSource())) &&
				(safeEquals(one.getFamilyName(), two.getFamilyName())) &&
				(safeEquals (one.getHelpURL(), two.getHelpURL())) &&
				(safeEquals(one.getContact(), two.getContact())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static boolean safeEquals(Contact one, Contact two) {
		boolean retVal;
		
		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if((safeEquals(one.getContactName(), two.getContactName())) &&
				(safeEquals(one.getEmail(), two.getEmail())) &&
				(safeEquals (one.getGsm(), two.getGsm())) )
		{
			retVal = true;
		}
		else   {
			retVal = false;
		}

		// TODO: compare remainder of sw stuff
		return retVal;
	}

	private static void compareContainerForSw(Configuration one, Configuration two) {

		Set<Container> origContainers = one.getContainers();
		for(Container origContainer : origContainers) 
		{
			boolean found = false;
			Set<Container> clonedContainers = two.getContainers();
			TestCase.assertEquals(origContainers.size(), clonedContainers.size());
			for(Container clonedContainer : clonedContainers)
			{
				if(safeEquals(origContainer, clonedContainer))
				{
					found = true;
					break;
				}
			}
			TestCase.assertTrue(found);
		}	
	}
	
	private static void compareComponentsForSw(Configuration one, Configuration two) 
	{
		Set<Component> origComponents = one.getComponents();
		for(Component origComponent : origComponents) 
		{
			boolean found = false;
			Set<Component> clonedComponents = two.getComponents();
			TestCase.assertEquals(origComponents.size(), clonedComponents.size());
			for(Component clonedComponent : clonedComponents)
			{
				if(safeEquals(origComponent, clonedComponent))
				{
					found = true;
					break;
				}
			}
			TestCase.assertEquals(true, found);
		}
	}

	@SuppressWarnings("unused")
	private static boolean safeEquals(Date one, Date two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}
		
		return retVal;
	}

	private static boolean safeEquals(Boolean one, Boolean two) 
	{
		boolean retVal = false;

		if(null == one && null == two) {
			retVal = true;
		} else if(null == one && null != two) {
			retVal = false;
		} else if(null != one && null == two) {
			retVal = false;
		} else if(one.equals(two)) {
			retVal = true;
		} else {
			retVal = false;
		}
		
		return retVal;
	}

	public static void compareAssemblies(HwConfiguration config1, HwConfiguration config2) 
	{
		for(Assembly origAssembly : config1.getAssemblies()) 
		{
			for(Assembly clonedAssembly: config2.getAssemblies()) 
			{
				if(safeEquals(clonedAssembly.getAssemblyType(), origAssembly.getAssemblyType()) &&
						clonedAssembly.getAssemblyType().equals(origAssembly.getAssemblyType()) &&
						clonedAssembly.getAssemblyType().getLruType().equals(origAssembly.getAssemblyType().getLruType()) &&
						clonedAssembly.getAssemblyType().getRoles().size() == origAssembly.getAssemblyType().getRoles().size() &&
						safeEquals(origAssembly.getSerialNumber(), clonedAssembly.getSerialNumber()) &&
						safeEquals(origAssembly.getData(), clonedAssembly.getData()) && 
						clonedAssembly.getConfiguration().equals(config2))
				{
					Set<AssemblyRole> origRoles = origAssembly.getAssemblyType().getRoles();

					for(AssemblyRole origRole : origRoles) 
					{
						boolean found = false;
						for(AssemblyRole clonedRole : clonedAssembly.getAssemblyType().getRoles()) 
						{
							if(safeEquals(origRole, clonedRole)) 
							{
								found = true;
								break;
							}
						}
						TestCase.assertEquals(true, found);
					}

				}
			}
		}
	}

	public static void compareConfigurations(HwConfiguration config, HwConfiguration clonedConfig) {
		compareAssemblies(config, clonedConfig);
		compareSwConfigurations(config.getSwConfiguration(), clonedConfig.getSwConfiguration());
		compareBaseElements(config, clonedConfig);
		compareStartupScenarios(config.getStartupScenarios(), clonedConfig.getStartupScenarios());
		compareHwConfigurationAttributes(config, clonedConfig);
	}
	
	public static void compareHwConfigurationAttributes(HwConfiguration config, HwConfiguration clonedConfig)
	{
		TestCase.assertTrue(config.getDescription().equals(clonedConfig.getDescription()));
		TestCase.assertTrue(config.getFullName().equals(clonedConfig.getFullName()));
		TestCase.assertTrue(config.getTelescopeName().equals(clonedConfig.getTelescopeName()));
		TestCase.assertTrue(config.getActive().equals(clonedConfig.getActive()));
		if(null != config.getArrayReference() && null != clonedConfig.getArrayReference()) {
			TestCase.assertTrue(config.getArrayReference().getX().equals(clonedConfig.getArrayReference().getX()));
			TestCase.assertTrue(config.getArrayReference().getY().equals(clonedConfig.getArrayReference().getY()));
			TestCase.assertTrue(config.getArrayReference().getZ().equals(clonedConfig.getArrayReference().getZ()));
		}
		else {
			TestCase.assertTrue(config.getArrayReference() == clonedConfig.getArrayReference());
		}
		
	}
	
	public static void removeSampleTmcdbDatabase()
	{
		// delete the directory into which we untarred the sample db
		String tmpDir = System.getProperty(USER_DIR);
		File zipDir = new File(tmpDir + File.separator + TMCDB);
		@SuppressWarnings("unused")
		boolean deletedDir = deleteDir(zipDir);
		//TestCase.assertEquals(true, deletedDir);
		
		// delete the tar file into which we gunzipped the tar.gz file
		File zipFile = new File(tmpDir + File.separator + TMCDBSAMPLE_TAR);
		@SuppressWarnings("unused")
		boolean deletedFile = zipFile.delete();
		//TestCase.assertEquals(true, deletedFile);
	}
	
	private static boolean deleteDir(File dir) 
	{
		if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
    
        // The directory is now empty so delete it
        return dir.delete();
	}
	
	public static void unzipSampleTmcdbDatabase() throws IOException
	{
		// Open the compressed file
		String pathPrefix = getPathPrefix();
		String path = pathPrefix + File.separator + "config" + File.separator + TMCDBSAMPLE_TAR_GZ;
		GZIPInputStream in = new GZIPInputStream(new FileInputStream(path));

		// Open the output file && unzip it from a tar.gz to a plain (uncompressed) tar file
		String tmpDir = System.getProperty(USER_DIR);
		OutputStream out = new FileOutputStream(tmpDir + File.separator + TMCDBSAMPLE_TAR);

		 copyInputStream(in, out);
	}
	
	public static void untarSampleTmcdbDatabase() throws IOException
	{
		// untar the tar file
		String tmpDir = System.getProperty(USER_DIR);
		String path = tmpDir + File.separator + TMCDBSAMPLE_TAR; 
		FileInputStream fis = new FileInputStream(path);
		TarArchive tarArchive = new TarArchive(fis);
		tarArchive.extractContents(new File(System.getProperty(USER_DIR)));
	}

	private static String getPathPrefix() 
	{
		String acsroot = null;
		String introot = System.getenv(INTROOT);
		if(null == introot) {
			acsroot = System.getenv(ACSROOT);
		}
		String pathPrefix = introot == null ? acsroot : introot;
		return pathPrefix;
	}
	
	private static void copyInputStream(InputStream in, OutputStream out)
	  throws IOException
	  {
	    byte[] buffer = new byte[1024];
	    int len;

	    while((len = in.read(buffer)) >= 0)
	      out.write(buffer, 0, len);

	    in.close();
	    out.close();
	  }
	
	public static Component createComponent(String name, String path,
			ComponentType compType, String urn, Configuration config)
	{
		Component comp = new Component();
		comp.setConfiguration(config);
		comp.setComponentName(name);
		comp.setComponentType(compType);
		comp.setPath(path);
		// comp.setURN(urn);
		comp.setImplLang("java");
		comp.setRealTime(false);
		comp.setCode("LIB");
		comp.setIsAutostart(false);
		comp.setIsDefault(false);
		comp.setIsControl(false);
		comp.setKeepAliveTime(0);
		comp.setMinLogLevel((byte)-1);
		comp.setMinLogLevelLocal((byte)-1);
		config.getComponents().add(comp);
		return comp;
	}
	
	public static HwConfiguration createConfiguration(String name)
	{
		Configuration swCfg = new Configuration();
		swCfg.setConfigurationName(name);
		swCfg.setFullName("full name of: " + name);
		swCfg.setActive(true);
		swCfg.setCreationTime(new Date());
		swCfg.setDescription("description of: " + name);
		HwConfiguration config = new HwConfiguration(swCfg);
		return config;
	}

	public static Component createComponent(String name, String path,
			ComponentType compType, Configuration config) {
		return createComponent(name, path, compType, "urn", config);
	}
}
