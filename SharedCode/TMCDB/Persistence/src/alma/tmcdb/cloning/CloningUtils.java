package alma.tmcdb.cloning;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import net.sf.beanlib.hibernate.HibernateBeanReplicator;
import net.sf.beanlib.hibernate.UnEnhancer;
import net.sf.beanlib.hibernate3.Hibernate3BeanReplicator;

import org.hibernate.SessionFactory;

import alma.acs.tmcdb.BACIProperty;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;
import alma.acs.tmcdb.DefaultCanAddress;
import alma.tmcdb.domain.AssemblyStartup;
import alma.tmcdb.domain.BaseElement;
import alma.tmcdb.domain.BaseElementStartup;
import alma.tmcdb.domain.HwConfiguration;
import alma.tmcdb.domain.StartupScenario;

/**
 * Utility methods for cloning items of interest: StartupScenario and Configuration, for use by TMCDBExplorer GUI.
 * @author sharrington
 */
public class CloningUtils 
{   
   private static final String COPY_OF = "Copy of: ";
   private static final String CLONE_OF = "Clone of: ";

   /**
    * Clones a startup scenario within a configuration.
    * @param startup the startup to clone.
    * @param clonedStartupName the name of the cloned startup (must be unique within a configuration, i.e. cannot
    * be the same name as the scenario being cloned, nor the same name as any other startup in the configuration).
    * @return the cloned startup, *NOT* persisted. 
    */
   public static StartupScenario cloneStartupScenario(StartupScenario startup, String clonedStartupName)
   {   
      StartupScenario clonedStartup = new StartupScenario();
        clonedStartup.setConfiguration(startup.getConfiguration());
        clonedStartup.setId(null);
        Set<AssemblyStartup> clonedAssemblyStartups = new HashSet<AssemblyStartup>();
        for(BaseElementStartup bes : startup.getBaseElementStartups()) 
        {
           BaseElementStartup besClone = new BaseElementStartup();
           besClone.setBaseElement(bes.getBaseElement());
           besClone.setStartup(clonedStartup);
           besClone.setType(bes.getType());
           besClone.setGeneric(bes.isGeneric());
           
           for(AssemblyStartup ast: bes.getAssemblyStartups()) {
              AssemblyStartup astClone = new AssemblyStartup();
              astClone.setBaseElementStartup(besClone);
              astClone.setAssemblyRole(ast.getAssemblyRole());
              clonedAssemblyStartups.add(astClone);
              besClone.addAssemblyStartup(astClone);
           }
            clonedStartup.addBaseElementStartup(besClone);
        }
        clonedStartup.setAssemblyStartups(clonedAssemblyStartups);
        clonedStartup.setName(clonedStartupName);
      
        startup.getConfiguration().addStartupScenario(clonedStartup);
      return clonedStartup;
   }
   
   /**
    * Clones a startup scenario within a configuration, using the beanlib library. NOTE: this is the preferred approach because
    * it involves less code and is in theory less fragile to changes in the domain - the other method (cloneStartupScenario) 
    * will be replaced by this one as soon as we have 100 percent confidence that this method is working as expected.
    * 
    * @param sessionFactory the hibernate session factory used when dealing with persistent objects in hibernate.
    * @param startup the startup to clone.
    * @param clonedStartupName the name of the cloned startup (must be unique within a configuration, i.e. cannot
    * be the same name as the scenario being cloned, nor the same name as any other startup in the configuration).
    * @return the cloned startup, *NOT* persisted. 
    */
   public static StartupScenario cloneStartupScenarioWithBeanlib(SessionFactory sessionFactory, StartupScenario startup, String clonedStartupName) 
   {   
      HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));      
      replicator.initCustomTransformerFactory(new StartupScenarioTransformerFactory());
      StartupScenario clonedStartup = replicator.copy(startup);

      if(null != clonedStartupName) {
           clonedStartup.setName(clonedStartupName);
        } else {
           clonedStartup.setName(CLONE_OF + startup.getName());
        }
      
      startup.getConfiguration().addStartupScenario(clonedStartup);
      return clonedStartup;
   }
   
   /**
    * Clones a configuration.
    * @param sessionFactory the hibernate session factory used when dealing with persistent objects in hibernate.
    * @param config the configuration to clone.
    * @param clonedConfigName the name of the new configuration; must be unique.
    * @return the new (cloned) configuration *NOT* persisted.
    */
   public static HwConfiguration cloneConfiguration(SessionFactory sessionFactory, HwConfiguration config, String clonedConfigName)
   {
      UnEnhancer.setDefaultCheckCGLib(false);  // globally disable checking 
      HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));
      replicator.initCustomTransformerFactory(new ConfigurationGlobalTransformerFactory(), new CloneAlmaEnumTransformerFactory());
      HwConfiguration clonedConfiguration = replicator.copy(config);
      if(null != clonedConfigName) {
         clonedConfiguration.getSwConfiguration().setConfigurationName(clonedConfigName);
      } else {
         clonedConfiguration.getSwConfiguration().setConfigurationName(CLONE_OF + config.getSwConfiguration().getConfigurationName());
      }
      return clonedConfiguration;
   }

   /**
    * Clones a set of DefaultCanAddress objects. While cloning, it also associate
    * the right component from the <code>candidateComponents</code> set, so it
    * reuses the already existing components.
    * 
    * <p>While looking for the component to be used in a cloned instance of a DefaultCanAddress object,
    * the two arguments <code>originalPathSubstring</code> and <code>finalPathSubstring</code> can be used
    * to substitute a substring in the original component's path in order to look for matches in the
    * <code>candidateComponents</code>. This must be used, for example, if we intend to clone DCAs from antenna A
    * to antenna B. In this case, the original components will have a path like <code>CONTROL/A</code>, while the
    * candidates will have a path like <code>CONTROL/B</code>.
    * 
    * @param sessionFactory the hibernate session factory used when dealing with persistent objects in hibernate.
    * @param defaultCanAddresses the set of DefaultCanAddress objects to clone
    * @param candidateComponents the set of Component objects used while cloning to properly link the cloned DefaultCanAddress
    *                            objects
    * @param originalPathSubstring a substring on the path of the components associated to the original DCAs that can be
    *                              replaced by the <code>finalPathSubstring</code> when looking for the right component
    *                              to link with the cloned DCA. If null, no substitution takes place, and the candidate components
    *                              are compared with their original values for name and path.
    * @param finalPathSubstring a substring on the path of the candidate components to be used by the cloned instances of the
    *                           DCAs. If null, no substitution takes place, and the candidate components are compared with their
    *                           original values for name and path
    * @return the new (cloned) set of DefaultCanAddress objects *NOT* persisted.
    */
   public static Set<DefaultCanAddress> cloneDefaultCanAddressesForConfiguration(SessionFactory sessionFactory,
		       Set<DefaultCanAddress> defaultCanAddresses, Set<Component> candidateComponents,
		       String originalPathSubstring, String finalPathSubstring)
   {
	  UnEnhancer.setDefaultCheckCGLib(false);  // globally disable checking 
      HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));
      replicator.initCustomTransformerFactory(new CloneDefaultCanAddressTransformerFactory(candidateComponents, originalPathSubstring, finalPathSubstring));
      Set<DefaultCanAddress> clonedDefaulCanAddresses = replicator.copy(defaultCanAddresses);
      return clonedDefaulCanAddresses;
   }

   /**
    * Clones a base element.
    * @param sessionFactory the hibernate session factory used when dealing with persistent objects in hibernate.
    * @param element the base element to clone.
    * @param clonedElementName the name of the new base element; must be unique (within a configuration).
    * @return the new (cloned) base element *NOT* persisted.
    */
   public static BaseElement cloneBaseElement(SessionFactory sessionFactory, BaseElement element, String clonedElementName)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));      
	   replicator.initCustomTransformerFactory(new CloneAntennaTransformerFactory(element.getName(), clonedElementName), new CloneAlmaEnumTransformerFactory());
	   BaseElement clonedBaseElement = replicator.copy(element);
	   element.getConfiguration().addBaseElement(clonedBaseElement);
	   return clonedBaseElement;
   }

   /**
    * Copies a base element.
    * @param sessionFactory the hibernate session factory used when dealing with persistent objects in hibernate.
    * @param element the base element to clone.
    * @param copiedElementName the name of the new base element; must be unique (within a configuration).
    * @param addToConfiguration the configuration to which to add the new base element.
    * @return the new (copied) base element *NOT* persisted.
    */
   public static BaseElement copyBaseElement(SessionFactory sessionFactory, BaseElement element, String copiedElementName, HwConfiguration addToConfiguration)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));      
	   replicator.initCustomTransformerFactory(new CopyAntennaTransformerFactory(addToConfiguration, copiedElementName), new CloneAlmaEnumTransformerFactory());
	   BaseElement copiedBaseElement = replicator.copy(element);

	   if(null != copiedElementName) 
		   copiedBaseElement.setName(copiedElementName);
	   else
		   copiedBaseElement.setName(COPY_OF + element.getName());
	   
	   addToConfiguration.addBaseElement(copiedBaseElement);
	   return copiedBaseElement;
   }


   /**
    * Clones a bunch of components that belong to an antenna. This method will create a duplicate
    * of the components arrays given as parameter, with their paths changed. For instance,
    * if the original components were part of the antenna DV01, and we are cloning them
    * for the antenna DV23, all the paths in the components will be changed from "CONTROL/DV01"
    * to "CONTROL/DV23".
    * 
    * @param sessionFactory The Hibernate Session Factory object
    * @param components A set of components to be cloned
    * @param oldName The old name that must be replaced in the path of both components and containers
    * @param newName The new name that must replace the old one in the path of both components and containers
    * @param addToConfiguration the configuration to which the components will be added
    */
   public static Collection<Component> cloneComponentsForAntenna(SessionFactory sessionFactory,
		   Collection<Component> components, String oldName, String newName, Configuration addToConfiguration)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));
	   ComponentNameReplacer replacer = new AntennaComponentNameReplacer(oldName, newName);
	   replicator.initCustomTransformerFactory(new CloneComponentsTransformerFactory(replacer, addToConfiguration));
	   return replicator.copy(components);
   }

   /**
    * Clones a component, changing its name and path to the given ones.
    * 
    * @param sessionFactory The Hibernate Session Factory object
    * @param components A set of components to be cloned
    * @param newName The new name for the component. If <code>null</code>, then the name shouldn't be changed
    * @param newPath The new path for the component. If <code>null</code>, then the path shouldn't be changed
    * @param targetConfiguration the configuration to which the component will belong
    */
   public static Component cloneComponent(SessionFactory sessionFactory,
		   Component component, String newName, String newPath, Configuration targetConfiguration)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));
	   ComponentNameReplacer replacer = new NameAndPathComponentNameReplacer(newName, newPath);
	   replicator.initCustomTransformerFactory(new CloneComponentsTransformerFactory(replacer, targetConfiguration));
	   return replicator.copy(component);
   }

   /**
    * Clones a bunch of containers that belong to an antenna. This method will create a duplicate
    * of the containers given as parameter, with their paths changed. For instance,
    * if the original containers were part of the antenna DV01, and we are cloning them
    * for the antenna DV23, all the paths in the containers will be changed from "CONTROL/DV01"
    * to "CONTROL/DV23".
    * 
    * @param sessionFactory The Hibernate Session Factory object
    * @param containers A set of containers to be cloned
    * @param oldName The old name that must be replaced in the path of both components and containers
    * @param newName The new name that must replace the old one in the path of both components and containers
    * @param addToConfiguration the configuration to which the containers will be added
    */
   public static Collection<Container> cloneContainersForAntenna(SessionFactory sessionFactory,
		   Collection<Container> containers, String oldName, String newName, Configuration addToConfiguration)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));      
	   replicator.initCustomTransformerFactory(new CloneContainersTransformerFactory(oldName, newName, addToConfiguration));
	   return replicator.copy(containers);
   }
   
   /**
    * Clones a bunch of BACIProperty objects that belong to a component. This method will create a duplicate
    * of the BACIProperty objects associated with the component which is given as parameter.
    * 
    * @param sessionFactory The Hibernate Session Factory object
    * @param component A component for which we want to clone the baci properties.
    */
   public static void cloneBACIPropertiesForComponent(SessionFactory sessionFactory, Component component)
   {
	   HibernateBeanReplicator replicator = new Hibernate3BeanReplicator(null, null, new HibernateIdFieldVetoer(sessionFactory));      
	   replicator.initCustomTransformerFactory(new CloneBACIPropertiesTransformerFactory(component));
	   Set<BACIProperty> copiedBaciProps = replicator.copy(component.getBACIProperties());
	   for(BACIProperty prop : copiedBaciProps) {
		   component.addBACIPropertyToBACIProperties(prop);
		   prop.setComponent(component);
	   }
   }
}
