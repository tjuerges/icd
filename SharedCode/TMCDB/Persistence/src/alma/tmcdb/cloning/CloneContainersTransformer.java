package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.Computer;
import alma.acs.tmcdb.Configuration;
import alma.acs.tmcdb.Container;

/**
 * Beanlib transformer used when cloning containers.
 * 
 * To clone a container, the path+name must be changed somehow. Either way, we will end up in a
 * constraint violation error. This transformer replaces a given part of the path of the container for a new one.
 * 
 * @author rtobar, Mar 30, 2010
 */
public class CloneContainersTransformer extends ConfigurationGlobalTransformer  
{

	private String oldName;
	private String newName;
	private Configuration addToConfiguration;

	/**
	 * Constructor.
	 */
	public CloneContainersTransformer(BeanTransformerSpi beanTransformer, String oldName, String newName, Configuration addToConfiguration) 
	{
		super(beanTransformer);
		this.oldName = oldName;
		this.newName = newName;
		this.addToConfiguration = addToConfiguration;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}
		// We check with isAssignableFrom instead of doing == between the classes
		// because we may get hibernate proxies instead of the actual instances
		// of our domain classes
		else if(Configuration.class.isAssignableFrom(toClass) || 
				Computer.class.isAssignableFrom(toClass) || 
				Container.class.isAssignableFrom(toClass) || 
				Component.class.isAssignableFrom(toClass) ) 
		{
			retVal = true;
		} 

		return retVal;
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		T retVal = (T)in;

		if( Component.class.isAssignableFrom(toClass) )
			return null;

		if( Container.class.isAssignableFrom(toClass) && in != null ) {
			retVal = (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, Container.class);
			Container cont = (Container)retVal;
			cont.setPath( cont.getPath().replaceAll(oldName, newName) );
			if( cont.getComponents() != null ) {
				cont.getComponents().clear();
			}
			else
				cont.setComponents(null);
		}
		
		if(Configuration.class.isAssignableFrom(toClass) && null != in) {
			retVal = (T) addToConfiguration;
		}

		return retVal;
	}
}
