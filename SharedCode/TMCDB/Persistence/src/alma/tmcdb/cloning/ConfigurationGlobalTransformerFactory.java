package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;

/**
 * Used in cloning; factory to return a transformer which will 'pass through' (i.e. return the identical object reference)
 * any 'global' domain classes.
 * 
 * @author sharrington
 */
public class ConfigurationGlobalTransformerFactory implements CustomBeanTransformerSpi.Factory 
{	
	public ConfigurationGlobalTransformerFactory()
	{
	}
	
	public CustomBeanTransformerSpi newCustomBeanTransformer(BeanTransformerSpi beanTransformer) {
		return new ConfigurationGlobalTransformer(beanTransformer);
	}
}