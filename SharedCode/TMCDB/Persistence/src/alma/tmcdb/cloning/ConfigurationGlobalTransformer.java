package alma.tmcdb.cloning;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import alma.acs.tmcdb.ComponentType;
import alma.acs.tmcdb.Contact;
import alma.acs.tmcdb.Location;
import alma.tmcdb.domain.ArrayReference;
import alma.tmcdb.domain.AssemblyRole;
import alma.tmcdb.domain.AssemblyType;
import alma.tmcdb.domain.Coordinate;
import alma.tmcdb.domain.LruType;
import alma.tmcdb.domain.Term;

/**
 * Used in cloning; transformer to 'pass through' (i.e. return the identical object reference)
 * any 'global' domain classes; in this case, the global classes are those
 * that are not associated with a configuration: LruType, AssemblyType,
 * PropertyType, and AssemblyRole.
 *
 * @author sharrington
 */
public class ConfigurationGlobalTransformer implements CustomBeanTransformerSpi
{
	protected BeanTransformerSpi defaultBeanTransformer;

	public ConfigurationGlobalTransformer(BeanTransformerSpi beanTransformer)
	{
		this.defaultBeanTransformer = beanTransformer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) {
		if (toClass == LruType.class ||
				toClass == Term.class ||
				toClass == ArrayReference.class ||
                toClass == Coordinate.class ||
                toClass == AssemblyType.class ||
                toClass == AssemblyRole.class ||
                toClass == ComponentType.class ||
                toClass == Location.class ||
                toClass == Contact.class
				)
			return true;

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass, PropertyInfo propertyInfo)
	{
		// Default behavior: global tables' rows are just copied
		T retVal = (T)in;
		return retVal;
	}
}