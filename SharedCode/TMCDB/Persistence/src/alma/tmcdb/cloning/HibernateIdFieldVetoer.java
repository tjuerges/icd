package alma.tmcdb.cloning;

import java.lang.reflect.Method;
import java.util.Collection;

import net.sf.beanlib.spi.PropertyFilter;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.repackage.cglib.proxy.Enhancer;


/**
 * Utility class used in cloning; vetos (to prevent copying) any fields which are hibernate identifier fields.
 * @author sharrington
 */
public class HibernateIdFieldVetoer implements PropertyFilter
{
	private SessionFactory sessionFactory;

	public HibernateIdFieldVetoer(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean propagate(String propertyName, Method readerMethod)
	{
		boolean retVal = false;

		Class c = readerMethod.getDeclaringClass();
		ClassMetadata classMetaData = sessionFactory.getClassMetadata(c);
		if (Enhancer.isEnhanced(c)) {
			// figure out the pre-enhanced class
			c = c.getSuperclass();
		}

		// Clone the collections
		if (Collection.class.isAssignableFrom(readerMethod.getReturnType())) {
			retVal = true;
		}
		else {
			// Skip populating if it is an identifier property.      
			if(null != classMetaData) 
			{
				if(classMetaData.hasIdentifierProperty()) 
				{
					retVal = !classMetaData.getIdentifierPropertyName().equals(propertyName);
				} 
				else if(classMetaData.hasNaturalIdentifier()) 
				{
					retVal = true;
					String[] propertyNames = classMetaData.getPropertyNames();
					int[] propertyLocations = classMetaData.getNaturalIdentifierProperties();
					for(int i : propertyLocations) {
						if(propertyNames[i].equals(propertyName)) {
							retVal = false;
						}
					}
				}
			}
		} 
		return retVal;
	}
}
