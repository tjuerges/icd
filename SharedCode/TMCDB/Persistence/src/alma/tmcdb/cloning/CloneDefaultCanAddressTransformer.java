package alma.tmcdb.cloning;

import java.util.Set;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.spi.BeanTransformerSpi;
import alma.acs.tmcdb.Component;
import alma.acs.tmcdb.DefaultCanAddress;

/**
 * Beanlib transformer used when cloning DefaultCanAddress objects.
 * 
 * When cloning a DefaultCanAddress, the associated Component is not cloned,
 * but instead it is picked up from a list of candidate components, which
 * in turn have been already cloned when cloning their host configuration
 * 
 * @author rtobar, Jul 21, 2010
 */
public class CloneDefaultCanAddressTransformer extends ConfigurationGlobalTransformer  
{
	private Set<Component> _candidateComponents;
	private String         _originalPathSubstring;
	private String         _finalPathSubstring;

	/**
	 * Constructor.
	 */
	public CloneDefaultCanAddressTransformer(BeanTransformerSpi beanTransformer, Set<Component> candidateComponents, String originalPathSubstring, String finalPathSubstring) 
	{
		super(beanTransformer);
		_candidateComponents = candidateComponents;
		_originalPathSubstring = originalPathSubstring;
		_finalPathSubstring = finalPathSubstring;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean isTransformable(Object from, Class toClass, PropertyInfo propertyInfo) 
	{
		boolean retVal = false;

		// We check with isAssignableFrom instead of doing == between the classes
		// because we may get hibernate proxies instead of the actual instances
		// of our domain classes
		if(DefaultCanAddress.class.isAssignableFrom(toClass) || 
				Component.class.isAssignableFrom(toClass) ) 
		{
			retVal = true;
		}
		else if(super.isTransformable(from, toClass, propertyInfo))
		{
			retVal = true;
		}

		return retVal;
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(Object in, Class<T> toClass,
			PropertyInfo propertyInfo)
	{
		T retVal = (T)in;

		if(DefaultCanAddress.class.isAssignableFrom(toClass) && null != in) {
			retVal = (T)defaultBeanTransformer.getBeanReplicatable().replicateBean(in, DefaultCanAddress.class);
			DefaultCanAddress dca = (DefaultCanAddress)retVal;

			Component c = dca.getComponent();
			String originalPath = c.getPath();
			for(Component candidate: _candidateComponents) {
				
				if( _originalPathSubstring != null && _finalPathSubstring != null &&
				    !_originalPathSubstring.equals(_finalPathSubstring) )
					originalPath = originalPath.replace(_originalPathSubstring, _finalPathSubstring);

				if( candidate.getComponentName().equals(c.getComponentName()) &&
				    candidate.getPath().equals(originalPath)) {
					dca.setComponent(candidate);
					break;
				}
			}
		}

		return retVal;
	}
}
