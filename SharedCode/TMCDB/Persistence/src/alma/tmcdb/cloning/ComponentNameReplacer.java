package alma.tmcdb.cloning;

import alma.acs.tmcdb.Component;


/**
 * This interface defines a common base for classes that change a Component's
 * name fields (e.g., <code>componetName</code> or <code>path</code>).
 * It is used by the Component cloner transformer, in order to separate the
 * cloning logic from the name replacement logic.
 *
 * @author rtobar, Apr 28th, 2011
 *
 */
public interface ComponentNameReplacer {

	/**
	 * Replaces the necessary name fields for the given Component.
	 * 
	 * @param comp The component whose name fields will be changed
	 */
	public void replaceName(Component comp);
}
