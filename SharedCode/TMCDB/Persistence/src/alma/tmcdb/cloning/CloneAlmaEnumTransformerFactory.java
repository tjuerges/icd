package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;

/**
 * Transformer factory used when cloning alma enums, which don't have a no-args constructor, using beanlib.
 * @author sharring
 */
public class CloneAlmaEnumTransformerFactory implements Factory 
{
	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(
			BeanTransformerSpi contextBeanTransformer) 
	{
		return new CloneAlmaEnumTransformer(contextBeanTransformer);
	}
}

