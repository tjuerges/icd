package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;

/**
 * Transformer factory used when cloning base elements using beanlib.
 * @author sharring
 */
public class CloneAntennaTransformerFactory implements Factory 
{

	private String oldName;
	private String newName;

	public CloneAntennaTransformerFactory(String oldName, String newName) {
		this.oldName = oldName;
		this.newName = newName;
	}

	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(
			BeanTransformerSpi contextBeanTransformer) 
	{
		return new CloneAntennaTransformer(contextBeanTransformer, oldName, newName);
	}

}
