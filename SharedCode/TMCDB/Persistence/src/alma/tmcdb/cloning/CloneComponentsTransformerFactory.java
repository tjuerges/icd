package alma.tmcdb.cloning;

import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi.Factory;
import alma.acs.tmcdb.Configuration;

/**
 * Transformer factory used when cloning base elements using beanlib.
 * @author sharring
 */
public class CloneComponentsTransformerFactory implements Factory 
{

	private ComponentNameReplacer _replacer;
	private Configuration _addToConfiguration;

	public CloneComponentsTransformerFactory(ComponentNameReplacer replacer, Configuration addToConfiguration) {
		_addToConfiguration = addToConfiguration;
		_replacer = replacer;
	}

	@Override
	public CustomBeanTransformerSpi newCustomBeanTransformer(
			BeanTransformerSpi contextBeanTransformer) 
	{
		return new CloneComponentsTransformer(contextBeanTransformer, _replacer, _addToConfiguration);
	}

}
