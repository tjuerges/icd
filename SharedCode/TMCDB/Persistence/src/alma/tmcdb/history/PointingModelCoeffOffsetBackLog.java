/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.history;

import java.io.Serializable;

import alma.ReceiverBandMod.ReceiverBand;

public class PointingModelCoeffOffsetBackLog extends BackLog implements Serializable {

    private static final long serialVersionUID = 7278728897566340559L;

    private Long pointingModelId;
    
    private String name;
    
    private ReceiverBand receiverBand;
    
    private Double offset;

    public Long getPointingModelId() {
        return pointingModelId;
    }

    public void setPointingModelId(Long pointingModelId) {
        this.pointingModelId = pointingModelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ReceiverBand getReceiverBand() {
        return receiverBand;
    }

    public void setReceiverBand(ReceiverBand receiverBand) {
        this.receiverBand = receiverBand;
    }

    public Double getOffset() {
        return offset;
    }

    public void setOffset(Double offset) {
        this.offset = offset;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((pointingModelId == null) ? 0 : pointingModelId.hashCode());
        result = prime * result
                + ((receiverBand == null) ? 0 : new Integer(receiverBand.value()).hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PointingModelCoeffOffsetBackLog other = (PointingModelCoeffOffsetBackLog) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (pointingModelId == null) {
            if (other.pointingModelId != null)
                return false;
        } else if (!pointingModelId.equals(other.pointingModelId))
            return false;
        if (receiverBand == null) {
            if (other.receiverBand != null)
                return false;
        } else if (!(receiverBand.value() == other.receiverBand.value()))
            return false;
        return true;
    }
}
