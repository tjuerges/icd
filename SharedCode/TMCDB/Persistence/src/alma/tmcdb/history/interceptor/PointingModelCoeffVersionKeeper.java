/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.history.interceptor;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import alma.ReceiverBandMod.ReceiverBand;
import alma.tmcdb.domain.PointingModelCoeff;
import alma.tmcdb.history.PointingModelCoeffBackLog;
import alma.tmcdb.history.PointingModelCoeffOffsetBackLog;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor.CollectionUpdate;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor.Delete;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor.Insert;
import alma.tmcdb.history.interceptor.VersionKeeperInterceptor.Update;
import alma.tmcdb.utils.HibernateUtil;

public class PointingModelCoeffVersionKeeper implements VersionKeeper {
    
    public void onInsert(Connection connection, Insert insert, VersionKeeperInterceptor interceptor) {
        PointingModelCoeff coeff = (PointingModelCoeff) insert.getEntity();
        Session tempSession =
            HibernateUtil.getSessionFactory().openSession(connection);
        try {
            Long version = getLatestVersion(tempSession);
            PointingModelCoeffBackLog blPMCoeff = new PointingModelCoeffBackLog();
            blPMCoeff.setName(coeff.getName());
            blPMCoeff.setModTime(insert.getTimestamp());
            blPMCoeff.setPointingModelId(coeff.getPointingModel().getId());
            blPMCoeff.setVersion(version);
            blPMCoeff.setWho(VersionKeeperInterceptor.getModifier());
            blPMCoeff.setValue(coeff.getValue());
            blPMCoeff.setOperation('I');
            blPMCoeff.setDescription(VersionKeeperInterceptor.getChangeDesc());
            tempSession.save(blPMCoeff);
            for (ReceiverBand band : coeff.getOffsets().keySet()) {
                Double offset = coeff.getOffsets().get(band);
                PointingModelCoeffOffsetBackLog blPMCoeffOff = new PointingModelCoeffOffsetBackLog();
                blPMCoeffOff.setName(coeff.getName());
                blPMCoeffOff.setModTime(insert.getTimestamp());
                blPMCoeffOff.setPointingModelId(coeff.getPointingModel().getId());
                blPMCoeffOff.setReceiverBand(band);
                blPMCoeffOff.setVersion(version);
                blPMCoeffOff.setOperation('I');
                blPMCoeffOff.setWho(VersionKeeperInterceptor.getModifier());
                blPMCoeffOff.setDescription(VersionKeeperInterceptor.getChangeDesc());
                blPMCoeffOff.setOffset(offset);
                tempSession.save(blPMCoeffOff);
            }
            tempSession.flush();
        } finally {
            tempSession.close();
        }
    }
    
    public void onDelete(Connection connection, Delete delete,
            VersionKeeperInterceptor interceptor) {
        PointingModelCoeff coeff = (PointingModelCoeff) delete.getEntity();
        Session tempSession =
            HibernateUtil.getSessionFactory().openSession(connection);
        try {
            Long pointingModelId = coeff.getPointingModel().getId();
            Long version = getLatestVersion(tempSession);
            PointingModelCoeffBackLog blPMCoeff = new PointingModelCoeffBackLog();
            blPMCoeff.setName(coeff.getName());
            blPMCoeff.setModTime(delete.getTimestamp());
            blPMCoeff.setPointingModelId(pointingModelId);
            blPMCoeff.setVersion(version);
            blPMCoeff.setWho(VersionKeeperInterceptor.getModifier());
            blPMCoeff.setValue(coeff.getValue());
            blPMCoeff.setOperation('D');
            blPMCoeff.setDescription(VersionKeeperInterceptor.getChangeDesc());
            tempSession.save(blPMCoeff);
            for (ReceiverBand band : coeff.getOffsets().keySet()) {
                Double offset = coeff.getOffsets().get(band);
                PointingModelCoeffOffsetBackLog blPMCoeffOff = new PointingModelCoeffOffsetBackLog();
                blPMCoeffOff.setName(coeff.getName());
                blPMCoeffOff.setModTime(delete.getTimestamp());
                blPMCoeffOff.setPointingModelId(pointingModelId);
                blPMCoeffOff.setReceiverBand(band);
                blPMCoeffOff.setVersion(version);
                blPMCoeffOff.setOperation('D');
                blPMCoeffOff.setWho(VersionKeeperInterceptor.getModifier());
                blPMCoeffOff.setDescription(VersionKeeperInterceptor.getChangeDesc());
                blPMCoeffOff.setOffset(offset);
                tempSession.save(blPMCoeffOff);
            }
            tempSession.flush();
        } finally {
            tempSession.close();
        }
    }

    @Override
    public void onUpdate(Connection connection, Update update,
            VersionKeeperInterceptor interceptor) {
        Map previousState = new HashMap();
        for (int i = 0; i < update.getPropertyNames().length; i++) {
            previousState.put(update.getPropertyNames()[i], update.getPreviousState()[i]);
        }
        Session tempSession =
            HibernateUtil.getSessionFactory().openSession(connection);
        try {
            PointingModelCoeff coeff =
                (PointingModelCoeff)tempSession.get(PointingModelCoeff.class, update.getId());
            Long pointingModelId = coeff.getPointingModel().getId();
            Long version = getLatestVersion(tempSession);
            PointingModelCoeffBackLog blPMCoeff = new PointingModelCoeffBackLog();
            blPMCoeff.setName((String)previousState.get("name"));
            blPMCoeff.setModTime(update.getTimestamp());
            blPMCoeff.setPointingModelId(pointingModelId);
            blPMCoeff.setVersion(version);
            blPMCoeff.setWho(VersionKeeperInterceptor.getModifier());
            blPMCoeff.setValue((Float)previousState.get("value"));
            blPMCoeff.setOperation('U');
            blPMCoeff.setDescription(VersionKeeperInterceptor.getChangeDesc());
            tempSession.save(blPMCoeff);
            tempSession.flush();
        } finally {
            tempSession.close();
        }
    }                    
    
    public void onCollectionChange(Connection connection, CollectionUpdate update,
            VersionKeeperInterceptor interceptor) {
        if (update.getPropertyName().equals("alma.tmcdb.domain.PointingModelCoeff.offsets")) {
            PointingModelCoeff coeff = (PointingModelCoeff)update.getOwner();
            // find all objects that were added or updated
            for (Object newValue : update.getNewValues().keySet()) {
               if (!update.getPreviousValues().keySet().contains(newValue)) {
                   // A newValue was added
                   Double value = (Double)update.getNewValues().get(newValue);
                   Session tempSession =
                       HibernateUtil.getSessionFactory().openSession(connection);
                   try {
                       Long pointingModelId = coeff.getPointingModel().getId();
                       PointingModelCoeffOffsetBackLog blPMCoeffOff = new PointingModelCoeffOffsetBackLog();
                       blPMCoeffOff.setName(coeff.getName());
                       blPMCoeffOff.setModTime(update.getTimestamp());
                       blPMCoeffOff.setPointingModelId(pointingModelId);
                       blPMCoeffOff.setReceiverBand((ReceiverBand)newValue);
                       blPMCoeffOff.setVersion(getLatestVersion(tempSession));
                       blPMCoeffOff.setOperation('I');
                       blPMCoeffOff.setWho(VersionKeeperInterceptor.getModifier());
                       blPMCoeffOff.setDescription(VersionKeeperInterceptor.getChangeDesc());
                       blPMCoeffOff.setOffset(value);
                       tempSession.save(blPMCoeffOff);
                       tempSession.flush();
                   } finally {
                       tempSession.close();
                   }                  
               } else {
                   // Value with key newValue was updated
                   Double value = (Double)update.getPreviousValues().get(newValue);
                   Session tempSession =
                       HibernateUtil.getSessionFactory().openSession(connection);
                   try {
                       Long pointingModelId = coeff.getPointingModel().getId();
                       PointingModelCoeffOffsetBackLog blPMCoeffOff = new PointingModelCoeffOffsetBackLog();
                       blPMCoeffOff.setName(coeff.getName());
                       blPMCoeffOff.setModTime(update.getTimestamp());
                       blPMCoeffOff.setPointingModelId(pointingModelId);
                       blPMCoeffOff.setReceiverBand((ReceiverBand)newValue);
                       blPMCoeffOff.setVersion(getLatestVersion(tempSession));
                       blPMCoeffOff.setOperation('U');
                       blPMCoeffOff.setWho(VersionKeeperInterceptor.getModifier());
                       blPMCoeffOff.setDescription(VersionKeeperInterceptor.getChangeDesc());
                       blPMCoeffOff.setOffset(value);
                       tempSession.save(blPMCoeffOff);
                       tempSession.flush();
                   } finally {
                       tempSession.close();
                   }
               }
            }
            // find all objects that were deleted
            for (Object oldValue : update.getPreviousValues().keySet()) {
               if (!update.getNewValues().keySet().contains(oldValue)) {
                  Double value = (Double)update.getPreviousValues().get(oldValue);
                  Session tempSession =
                      HibernateUtil.getSessionFactory().openSession(connection);
                  try {
                      Long pointingModelId = coeff.getPointingModel().getId();
                      PointingModelCoeffOffsetBackLog blPMCoeffOff = new PointingModelCoeffOffsetBackLog();
                      blPMCoeffOff.setName(coeff.getName());
                      blPMCoeffOff.setModTime(update.getTimestamp());
                      blPMCoeffOff.setPointingModelId(pointingModelId);
                      blPMCoeffOff.setReceiverBand((ReceiverBand)oldValue);
                      blPMCoeffOff.setVersion(getLatestVersion(tempSession));
                      blPMCoeffOff.setOperation('D');
                      blPMCoeffOff.setWho(VersionKeeperInterceptor.getModifier());
                      blPMCoeffOff.setDescription(VersionKeeperInterceptor.getChangeDesc());
                      blPMCoeffOff.setOffset(value);
                      tempSession.save(blPMCoeffOff);
                      tempSession.flush();
                  } finally {
                      tempSession.close();
                  }
                  
               }
            }
        }
    }
    
    private Long getLatestVersion(Session session) {
        long version;
        Query query =
            session.createQuery("select max(coeff.version) from PointingModelCoeffBackLog coeff");
        Object tmp = query.uniqueResult();
        if (tmp != null)
            version = (Long)tmp;
        else
            version = 0;
        if (VersionKeeperInterceptor.getInitiateNewVersion()) {
            version++;
            VersionKeeperInterceptor.setInitiateNewVersion(false);
        }
        return version;
    }
}
