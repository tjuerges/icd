/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: VersionKeeperInterceptor.java,v 1.1 2010/12/16 21:56:05 rhiriart Exp $"
 */
package alma.tmcdb.history.interceptor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Session;
import org.hibernate.collection.PersistentMap;
import org.hibernate.collection.PersistentSet;
import org.hibernate.type.Type;


public class VersionKeeperInterceptor extends EmptyInterceptor {

    protected class Insert {
        private long timestamp;
        private Versionable entity;
        public long getTimestamp() {
            return timestamp;
        }
        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        public Versionable getEntity() {
            return entity;
        }
        public void setEntity(Versionable entity) {
            this.entity = entity;
        }
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((entity == null) ? 0 : entity.hashCode());
            result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
            return result;
        }
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Insert other = (Insert) obj;
            if (entity == null) {
                if (other.entity != null)
                    return false;
            } else if (!entity.equals(other.entity))
                return false;
            if (timestamp != other.timestamp)
                return false;
            return true;
        }
    }

    protected class Delete {
        private long timestamp;
        private Versionable entity;
        public long getTimestamp() {
            return timestamp;
        }
        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        public Versionable getEntity() {
            return entity;
        }
        public void setEntity(Versionable entity) {
            this.entity = entity;
        }
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((entity == null) ? 0 : entity.hashCode());
            result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
            return result;
        }
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Delete other = (Delete) obj;
            if (entity == null) {
                if (other.entity != null)
                    return false;
            } else if (!entity.equals(other.entity))
                return false;
            if (timestamp != other.timestamp)
                return false;
            return true;
        }
    }
    
    protected class Update {
        private Serializable id;
        private Versionable entity;
        private Object[] currentState;
        private Object[] previousState;
        private String[] propertyNames;
        private Type[] propertyTypes;
        long timestamp;

        public Serializable getId() {
            return id;
        }

        public void setId(Serializable id) {
            this.id = id;
        }

        public Versionable getEntity() {
            return entity;
        }

        public void setEntity(Versionable entity) {
            this.entity = entity;
        }

        public Object[] getCurrentState() {
            return currentState;
        }

        public void setCurrentState(Object[] currentState) {
            this.currentState = currentState;
        }

        public Object[] getPreviousState() {
            return previousState;
        }

        public void setPreviousState(Object[] previousState) {
            this.previousState = previousState;
        }

        public String[] getPropertyNames() {
            return propertyNames;
        }

        public void setPropertyNames(String[] propertyNames) {
            this.propertyNames = propertyNames;
        }

        public Type[] getPropertyTypes() {
            return propertyTypes;
        }

        public void setPropertyTypes(Type[] propertyTypes) {
            this.propertyTypes = propertyTypes;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((entity == null) ? 0 : entity.hashCode());
            result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            result = prime * result + Arrays.hashCode(currentState);
            result = prime * result + Arrays.hashCode(previousState);
            result = prime * result + Arrays.hashCode(propertyNames);
            result = prime * result + Arrays.hashCode(propertyTypes);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Update other = (Update) obj;
            if (entity == null) {
                if (other.entity != null)
                    return false;
            } else if (!entity.equals(other.entity))
                return false;
            if (timestamp != other.timestamp)
                return false;
            if (id == null) {
                if (other.id != null)
                    return false;
            } else if (!id.equals(other.id))
                return false;
            if (!Arrays.equals(currentState, other.currentState))
                return false;
            if (!Arrays.equals(previousState, other.previousState))
                return false;
            if (!Arrays.equals(propertyNames, other.propertyNames))
                return false;
            if (!Arrays.equals(propertyTypes, other.propertyTypes))
                return false;
            return true;
        }
    }
    
    protected class CollectionUpdate {
        Versionable owner;
        String propertyName;
        Map newValues;
        Map previousValues;
        long timestamp;
        
        public Versionable getOwner() {
            return owner;
        }

        public void setOwner(Versionable owner) {
            this.owner = owner;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }
        
        public Map getNewValues() {
            return newValues;
        }
        
        public void setNewValues(Map newValues) {
            this.newValues = newValues;
        }
        
        public Map getPreviousValues() {
            return previousValues;
        }
        
        public void setPreviousValues(Map previousValues) {
            this.previousValues = previousValues;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((newValues == null) ? 0 : newValues.hashCode());
            result = prime * result + ((owner == null) ? 0 : owner.hashCode());
            result = prime
                    * result
                    + ((previousValues == null) ? 0 : previousValues.hashCode());
            result = prime * result
                    + ((propertyName == null) ? 0 : propertyName.hashCode());
            result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CollectionUpdate other = (CollectionUpdate) obj;
            if (newValues == null) {
                if (other.newValues != null)
                    return false;
            } else if (!newValues.equals(other.newValues))
                return false;
            if (owner == null) {
                if (other.owner != null)
                    return false;
            } else if (!owner.equals(other.owner))
                return false;
            if (previousValues == null) {
                if (other.previousValues != null)
                    return false;
            } else if (!previousValues.equals(other.previousValues))
                return false;
            if (propertyName == null) {
                if (other.propertyName != null)
                    return false;
            } else if (!propertyName.equals(other.propertyName))
                return false;
            if (timestamp != other.timestamp)
                return false;
            return true;
        }
    }
    
    private static final long serialVersionUID = -1758564208183208352L;
    
    private static String modifier = "unknown";
    private static String changeDesc = "unknown";
    private static boolean initiateNewVersion;
    
    private Session session;
    
    private Set inserts = new HashSet();
    private Set updates = new HashSet();
    private Set deletes = new HashSet();
    private Set collUpdates = new HashSet();
    
    public void setSession(Session session) {
        this.session = session;
    }
    
    public static void setModifier(String userModifier) {
        modifier = userModifier;
    }
    
    public static String getModifier() {
        return modifier;
    }
    
    public static void setChangeDesc(String description) {
        changeDesc = description;
    }
    
    public static String getChangeDesc() {
        return changeDesc;
    }
    
    public static void setInitiateNewVersion(boolean newVersion) {
        initiateNewVersion = newVersion;
    }
    
    public static boolean getInitiateNewVersion() {
        return initiateNewVersion;
    }
    
    public static void createNewVersion(String who, String description) {
        setInitiateNewVersion(true);
        setModifier(who);
        setChangeDesc(description);
    }
    
    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
        if (entity instanceof Versionable) {
            Insert insert = new Insert();
            insert.setTimestamp(System.currentTimeMillis());
            insert.setEntity((Versionable)entity);
            inserts.add(insert);
        }
        return false;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id,
            Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) {
        if (entity instanceof Versionable) {
            Update upd = new Update();
            upd.setId(id);
            upd.setEntity((Versionable)entity);
            upd.setCurrentState(currentState);
            upd.setPreviousState(previousState);
            upd.setPropertyNames(propertyNames);
            upd.setPropertyTypes(types);
            upd.setTimestamp(System.currentTimeMillis());
            updates.add(upd);
        }
        return false;
    }

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
        if (entity instanceof Versionable) {
            Delete delete = new Delete();
            delete.setTimestamp(System.currentTimeMillis());
            delete.setEntity((Versionable)entity);
            deletes.add(delete);
        }
    }

    @Override
    public void onCollectionUpdate(Object collection, Serializable key)
            throws CallbackException {
        if (collection instanceof PersistentMap) {
            PersistentMap newValues = (PersistentMap) collection;          
            Object owner = newValues.getOwner();
            if (owner instanceof Versionable) {
                Versionable entity = (Versionable) owner;
                Class writerClass = entity.getVersionWriterClass();
                Map oldValues = (Map)newValues.getStoredSnapshot();
                CollectionUpdate upd = new CollectionUpdate();
                upd.setOwner(entity);
                upd.setPropertyName(newValues.getRole());
                upd.setPreviousValues(oldValues);
                upd.setNewValues(newValues);
                upd.setTimestamp(System.currentTimeMillis());
                collUpdates.add(upd);
            }
         }
    }
    
    @Override
    public void postFlush(Iterator entities) {
        try {
            for (Iterator it = inserts.iterator(); it.hasNext();) {
                Insert insert = (Insert) it.next();
                Class writerClass = insert.getEntity().getVersionWriterClass();
                try {
                    VersionKeeper writer = (VersionKeeper) writerClass.newInstance();
                    writer.onInsert(session.connection(), insert, this);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            for (Iterator it = updates.iterator(); it.hasNext();) {
                Update update = (Update) it.next();
                Class writerClass = update.getEntity().getVersionWriterClass();
                try {
                    VersionKeeper writer = (VersionKeeper) writerClass.newInstance();
                    writer.onUpdate(session.connection(), update, this);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            for (Iterator it = deletes.iterator(); it.hasNext();) {
                Delete delete = (Delete) it.next();
                Class writerClass = delete.getEntity().getVersionWriterClass();
                try {
                    VersionKeeper writer = (VersionKeeper) writerClass.newInstance();
                    writer.onDelete(session.connection(), delete, this);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            for (Iterator it = collUpdates.iterator(); it.hasNext();) {
                CollectionUpdate update = (CollectionUpdate) it.next();
                Class writerClass = update.getOwner().getVersionWriterClass();
                try {
                    VersionKeeper writer = (VersionKeeper) writerClass.newInstance();
                    writer.onCollectionChange(session.connection(), update, this);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        } finally {
            inserts.clear();
            updates.clear();
            deletes.clear();
            collUpdates.clear();
        }
    }
}
