/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.history;

import java.io.Serializable;

/**
 * This persistent class maintains several fields necessary to control versions
 * on tables under version control.
 */
public class VersionInfo implements Serializable {

    private static final long serialVersionUID = 2490989175075850072L;

    /**
     * The name of the entity type which is under version control.
     * This field must be the name of a table in the TMCDB database as it will
     * be used by the database history triggers to manage version numbers.
     */
    private String name;

    /**
     * Configuration identifier.
     */
    private Integer configurationId;
    
    /**
     * Entity identifier.
     */
    private Long entityId;
    
    /**
     * Whether to increase the version number in the next transactions committed
     * in a table under version control.
     * Setting this field to True will make the database triggers to increase the
     * version number in the BackLog tables.
     */
    private Boolean increaseVersion;
    
    /**
     * The current version for a table under version control.
     * This field will be managed by the database triggers.
     */
    private Integer currentVersion;
    
    /**
     * The name of the person who is about to commit modifications in this table.
     * This field should be set before committing changes in the corresponding
     * entity.
     */
    private String modifier;

    /**
     * A description for a set of modifications committed in an entity.
     * This field should be set before committing changes in the corresponding
     * entity.
     */
    private String description;

    private Boolean lock;
    
    public VersionInfo() { }
        
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIncreaseVersion() {
        return increaseVersion;
    }

    public void setIncreaseVersion(Boolean increaseVersion) {
        this.increaseVersion = increaseVersion;
    }

    public Integer getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(Integer currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(Integer configurationId) {
        this.configurationId = configurationId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Boolean getLock() {
        return lock;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }
}
