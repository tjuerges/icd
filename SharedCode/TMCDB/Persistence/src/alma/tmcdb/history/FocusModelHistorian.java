/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.history;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.FocusModel;
import alma.tmcdb.domain.FocusModelCoeff;
import alma.tmcdb.domain.FocusModel;

public class FocusModelHistorian {
    
    private Session session;
    
    public FocusModelHistorian(Session session) {
        this.session = session;
    }
    
    public FocusModel recreate(long version, FocusModel currentFocusModel) {
        FocusModel retVal = clone(currentFocusModel);
        List<BackLog> records = getHistoricRecords(version, currentFocusModel);
        for (Object record : records) {
            if (record instanceof FocusModelCoeffBackLog) {
                reverse((FocusModelCoeffBackLog)record, retVal);
            } else if (record instanceof FocusModelCoeffOffsetBackLog) {
                reverse((FocusModelCoeffOffsetBackLog)record, retVal);
            }
        }
        return retVal;
    }

    public List<HistoryRecord> getHistory(FocusModel focusModel) {
        List<HistoryRecord> retVal = new ArrayList<HistoryRecord>();
        List<BackLog> records = getHistoricRecords(0, focusModel); // get all records
        Long version = Long.MAX_VALUE;
        Long recVersion = new Long(0);
        Date recModTime = null;
        String who = null;
        String description = null;
        for (BackLog record : records) {
            BackLog bl = (BackLog)record;
            recVersion = bl.getVersion();
            recModTime = new Date(bl.getModTime());
            who = bl.getWho();
            description = bl.getDescription();
            if (recVersion < version) {
                version = recVersion;
                HistoryRecord hr = new HistoryRecord();
                hr.setVersion(version);
                hr.setTimestamp(recModTime);
                hr.setModifier(who);
                hr.setDescription(description);
                retVal.add(hr);
            }
        }
        return retVal;
    }

    public List<BackLog> getHistoricRecords(long version, FocusModel fm) {
        Long fmId = fm.getId();
        List<BackLog> list = new ArrayList<BackLog>();
        Query query =
            session.createQuery("from FocusModelCoeffBackLog c where c.version > :version and focusModelId = :fmId");
        query.setParameter("version", version, Hibernate.LONG);
        query.setParameter("fmId", fmId, Hibernate.LONG);
        List<FocusModelCoeffBackLog> records = (List<FocusModelCoeffBackLog>) query.list();
        for (FocusModelCoeffBackLog r : records)
            list.add(r);

        query = session.createQuery("from FocusModelCoeffOffsetBackLog o where o.version > :version and focusModelId = :fmId");
        query.setParameter("version", version, Hibernate.LONG);
        query.setParameter("fmId", fmId, Hibernate.LONG);
        List<FocusModelCoeffOffsetBackLog> offsets = (List<FocusModelCoeffOffsetBackLog>) query.list();
        for (FocusModelCoeffOffsetBackLog o : offsets)
            list.add(o);

        Collections.sort(list, new BackLogComparator());
        return list;
    }

    public boolean prepareSave(Configuration cfg, FocusModel ent, String who, String description) {
        VersionInfo vi = null;
        if (cfg == null) throw new NullPointerException("Configuration is null");
        if (cfg.getConfigurationId() == null)
            throw new NullPointerException("ConfigurationId is null");
        if (ent == null) throw new NullPointerException("FocusModel is null");
        if (who == null) who = "";
        if (description == null) description = "";

        // If the Entity ID is null it means that it is the original save.
        // There is no need to save the original version in the BackLog table.
        // It will get saved at the next modification.
        if (ent.getId() != null) {
            String qstr = "from VersionInfo where name = :name and " +
                    "configurationId = :configurationId and entityId = :entityId";
            Query query = session.createQuery(qstr);
            query.setParameter("name", "FocusModel", Hibernate.STRING);
            query.setParameter("configurationId", cfg.getConfigurationId(), Hibernate.INTEGER);
            query.setParameter("entityId", ent.getId(), Hibernate.LONG);
            vi = (VersionInfo) query.uniqueResult();
            if (vi == null) {
                vi = new VersionInfo();
                vi.setName("FocusModel");
                vi.setCurrentVersion(0);
                vi.setConfigurationId(cfg.getConfigurationId());
                vi.setEntityId(ent.getId());
            } else {
                // Somebody else is already about to commit another change in the
                // FocusModel table.
                if (vi.getIncreaseVersion()) return false;
            }
            vi.setIncreaseVersion(true);
            vi.setModifier(who);
            vi.setDescription(description);
            session.saveOrUpdate(vi);
        }
        return true;
    }
    
    private void reverse(FocusModelCoeffBackLog change, FocusModel fm) {
        char op = change.getOperation();
        if (op == 'D') {
            FocusModelCoeff coeff = new FocusModelCoeff(change.getValue());
            fm.getTerms().put(change.getName(), coeff);
        } else if (op == 'I') {
            fm.getTerms().remove(change.getName());
        } else if (op == 'U') {
            FocusModelCoeff coeff = fm.getTerms().get(change.getName());
            if (coeff != null) {
                coeff.setValue(change.getValue());
            }
        }
    }

    private void reverse(FocusModelCoeffOffsetBackLog change, FocusModel fm) {
        char op = change.getOperation();
        FocusModelCoeff coeff = fm.getTerms().get(change.getName());
        if (coeff != null) {
            if (op == 'D') {
                    coeff.getOffsets().put(change.getReceiverBand(), change.getOffset());
            } else if (op == 'I') {
                coeff.getOffsets().remove(change.getReceiverBand());
            } else if (op == 'U') {
                coeff.getOffsets().put(change.getReceiverBand(), change.getOffset());
            }
        }
    }

    private FocusModel clone(FocusModel fm) {
        FocusModel retVal = new FocusModel();
        for (String coeffName : fm.getTerms().keySet()) {
            FocusModelCoeff coeff = fm.getTerms().get(coeffName);
            FocusModelCoeff newCoeff = new FocusModelCoeff(coeff.getValue());
            retVal.getTerms().put(coeffName, newCoeff);
            for (ReceiverBand band : coeff.getOffsets().keySet()) {
                retVal.getTerms().get(coeffName).getOffsets()
                                 .put(band, coeff.getOffsets()
                                 .get(band));
            }
        }
        return retVal;
    }    
}
