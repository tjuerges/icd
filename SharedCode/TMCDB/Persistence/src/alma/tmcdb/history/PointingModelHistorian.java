/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.tmcdb.history;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.tmcdb.Configuration;
import alma.tmcdb.domain.PointingModel;
import alma.tmcdb.domain.PointingModelCoeff;

public class PointingModelHistorian {
    
    private Session session;
    
    public PointingModelHistorian(Session session) {
        this.session = session;
    }
    
    public PointingModel recreate(long version, PointingModel currentPointingModel) {
        PointingModel retVal = clone(currentPointingModel);
        List<BackLog> records = getHistoricRecords(version, currentPointingModel);
        for (Object record : records) {
            if (record instanceof PointingModelCoeffBackLog) {
                reverse((PointingModelCoeffBackLog)record, retVal);
            } else if (record instanceof PointingModelCoeffOffsetBackLog) {
                reverse((PointingModelCoeffOffsetBackLog)record, retVal);
            }
        }
        return retVal;
    }

    public List<HistoryRecord> getHistory(PointingModel pointingModel) {
        List<HistoryRecord> retVal = new ArrayList<HistoryRecord>();
        List<BackLog> records = getHistoricRecords(0, pointingModel); // get all records
        Long version = Long.MAX_VALUE;
        Long recVersion = new Long(0);
        Date recModTime = null;
        String who = null;
        String description = null;
        for (BackLog record : records) {
            BackLog bl = (BackLog)record;
            recVersion = bl.getVersion();
            recModTime = new Date(bl.getModTime());
            who = bl.getWho();
            description = bl.getDescription();
            if (recVersion < version) {
                version = recVersion;
                HistoryRecord hr = new HistoryRecord();
                hr.setVersion(version);
                hr.setTimestamp(recModTime);
                hr.setModifier(who);
                hr.setDescription(description);
                retVal.add(hr);
            }
        }
        return retVal;
    }

    public List<BackLog> getHistoricRecords(long version, PointingModel pm) {
        Long pmId = pm.getId();
        List<BackLog> list = new ArrayList<BackLog>();
        Query query =
            session.createQuery("from PointingModelCoeffBackLog c where c.version > :version and pointingModelId = :pmId");
        query.setParameter("version", version, Hibernate.LONG);
        query.setParameter("pmId", pmId, Hibernate.LONG);
        List<PointingModelCoeffBackLog> records = (List<PointingModelCoeffBackLog>) query.list();
        for (PointingModelCoeffBackLog r : records)
            list.add(r);

        query = session.createQuery("from PointingModelCoeffOffsetBackLog o where o.version > :version and pointingModelId = :pmId");
        query.setParameter("version", version, Hibernate.LONG);
        query.setParameter("pmId", pmId, Hibernate.LONG);
        List<PointingModelCoeffOffsetBackLog> offsets = (List<PointingModelCoeffOffsetBackLog>) query.list();
        for (PointingModelCoeffOffsetBackLog o : offsets)
            list.add(o);

        Collections.sort(list, new BackLogComparator());
        return list;
    }

    public boolean prepareSave(Configuration cfg, PointingModel ent, String who, String description) {
        VersionInfo vi = null;
        if (cfg == null) throw new NullPointerException("Configuration is null");
        if (cfg.getConfigurationId() == null)
            throw new NullPointerException("ConfigurationId is null");
        if (ent == null) throw new NullPointerException("PointingModel is null");
        if (who == null) who = "";
        if (description == null) description = "";

        // If the Entity ID is null it means that it is the first save.
        if (ent.getId() != null) {
            String qstr = "from VersionInfo where name = :name and " +
            		"configurationId = :configurationId and entityId = :entityId";
            Query query = session.createQuery(qstr);
            query.setParameter("name", "PointingModel", Hibernate.STRING);
            query.setParameter("configurationId", cfg.getConfigurationId(), Hibernate.INTEGER);
            query.setParameter("entityId", ent.getId(), Hibernate.LONG);
            vi = (VersionInfo) query.uniqueResult();
            if (vi == null) {
                // Somehow there is no version info, even though the Entity ID is not null.
                // Create a new record in this case.
                vi = new VersionInfo();
                vi.setName("PointingModel");
                vi.setCurrentVersion(0);
                vi.setConfigurationId(cfg.getConfigurationId());
                vi.setEntityId(ent.getId());
            } else {
                // Somebody else is already about to commit another change in the
                // PointingModel table.
                if (vi.getLock() || vi.getIncreaseVersion()) return false;
            }
            vi.setIncreaseVersion(true);
            vi.setLock(true);
            vi.setModifier(who);
            vi.setDescription(description);
            session.saveOrUpdate(vi);
        } else {
            // EntityId is null. This should only happen when saving a PointingModel
            // for the first time.
            vi = new VersionInfo();
            vi.setName("PointingModel");
            vi.setCurrentVersion(0);
            vi.setConfigurationId(cfg.getConfigurationId());
            // EntityId is not yet known. A trigger in PointingModel will set it up
            // after it has been saved.
            vi.setEntityId(new Long(-1));
            vi.setIncreaseVersion(true);
            vi.setLock(true);
            vi.setModifier(who);
            vi.setDescription(description);
            session.saveOrUpdate(vi);            
        }
        return true;
    }
    
    public void endSave(Configuration cfg, PointingModel ent) {
        String qstr = "from VersionInfo where name = :name and " +
            "configurationId = :configurationId and entityId = :entityId";
        Query query = session.createQuery(qstr);
        query.setParameter("name", "PointingModel", Hibernate.STRING);
        query.setParameter("configurationId", cfg.getConfigurationId(), Hibernate.INTEGER);
        query.setParameter("entityId", ent.getId(), Hibernate.LONG);
        VersionInfo vi = (VersionInfo) query.uniqueResult();
        if(null != vi)
        {
        	vi.setLock(false);
        	vi.setIncreaseVersion(false);
        	session.saveOrUpdate(vi);
        }
    }
    
    private void reverse(PointingModelCoeffBackLog change, PointingModel pm) {
        char op = change.getOperation();
        if (op == 'D') {
            PointingModelCoeff coeff = new PointingModelCoeff(change.getName(), change.getValue());
            pm.addTerm(change.getName(), coeff);
        } else if (op == 'I') {
            pm.getTerms().remove(change.getName());
        } else if (op == 'U') {
            PointingModelCoeff coeff = pm.getTerms().get(change.getName());
            if (coeff != null) {
                coeff.setValue(change.getValue());
            }
        }
    }

    private void reverse(PointingModelCoeffOffsetBackLog change, PointingModel pm) {
        char op = change.getOperation();
        PointingModelCoeff coeff = pm.getTerm(change.getName());
        if (coeff != null) {
            if (op == 'D') {
                    coeff.getOffsets().put(change.getReceiverBand(), change.getOffset());
            } else if (op == 'I') {
                coeff.getOffsets().remove(change.getReceiverBand());
            } else if (op == 'U') {
                coeff.getOffsets().put(change.getReceiverBand(), change.getOffset());
            }
        }
    }

    private PointingModel clone(PointingModel pm) {
        PointingModel retVal = new PointingModel();
        for (PointingModelCoeff coeff : pm.getTerms().values()) {
            PointingModelCoeff newCoeff = new PointingModelCoeff(coeff.getName(), coeff.getValue());
            retVal.addTerm(coeff.getName(), newCoeff);
            for (ReceiverBand band : coeff.getOffsets().keySet()) {
                retVal.getTerm(coeff.getName()).getOffsets().put(band, coeff.getOffsets().get(band));
            }
        }
        return retVal;
    }    
}
