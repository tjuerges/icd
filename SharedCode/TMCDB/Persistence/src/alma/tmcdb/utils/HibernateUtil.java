package alma.tmcdb.utils;

import java.util.Properties;
import alma.archive.database.helpers.wrappers.TmcdbDbConfig;

import org.hibernate.*;
import org.hibernate.cfg.*;

public class HibernateUtil {

    private static Configuration configuration;
    private static SessionFactory sessionFactory;
    
    public static SessionFactory getSessionFactory() {
        // Alternatively, you could look up in JNDI here
        if (sessionFactory == null)
            createTestConfiguration();
        return sessionFactory;
    }
    
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

    /**
     * Creates a configuration exclusively from hibernate.cfg.xml file.
     * This function is provided for unit tests, where hibernate.cfg.xml
     * is installed in the directory from which tests are run.
     */
    private static void createTestConfiguration() {
      try {
          configuration = new AnnotationConfiguration();
          sessionFactory = configuration.configure().buildSessionFactory();
      } catch (Throwable ex) {
          throw new ExceptionInInitializerError(ex);
      }
    }
    
    /**
     * Creates an Hibernate configuration adding properties to the default
     * configuration. A session factory is created from the combined configuration.
     * 
     * @param properties Extra properties.
     */
    public static void createConfigurationWithProperties(Properties properties) {
        if (sessionFactory == null) {
            try {
                Configuration cnf = new AnnotationConfiguration();
                cnf.configure("tmcdb.hibernate.cfg.xml");
                cnf.addProperties(properties);
                configuration = cnf;
                sessionFactory = configuration.buildSessionFactory();
            } catch (Throwable ex) {
                throw new ExceptionInInitializerError(ex);
            }
        }
    }
    
    /**
     * Creates an Hibernate configuration setting the connection and
     * dialect properties from the TmcdbDbConfig configurator object,
     * which in turns reads the file DbConfig.properties.
     * 
     * @param conf TMCDB DbConfig Configurator object
     */
    public static void createConfigurationFromDbConfig(TmcdbDbConfig conf) {
        final Properties props = new Properties();
        props.setProperty("hibernate.dialect",
                          conf.getDialect());
        props.setProperty("hibernate.connection.driver_class",
                          conf.getDriver());
        props.setProperty("hibernate.connection.url",
                          conf.getConnectionUrl());
        props.setProperty("hibernate.connection.username",
                          conf.getUsername());
        props.setProperty("hibernate.connection.password",
                          conf.getPassword());
        createConfigurationWithProperties(props);
    }
}
