package alma.tmcdb.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

public class TmcdbUtils {

    /** Password for connecting to the HSQLDB server */
    public static final String HSQLDB_PASSWORD = "";

    /** Username for connecting to the HSQLDB server */
    public static final String HSQLDB_USER = "sa";

    /** Basic URL for an HSQLDB file-based database */
    public static final String HSQLDB_FILE_URL = "jdbc:hsqldb:file:";

    /** Basic URL for an HSQLDB in-memory database */
    public static final String HSQLDB_MEMORY_URL = "jdbc:hsqldb:mem:ignored";
    
    /** JDBC driver for HSQLDB */
    public static final String HSQLDB_JDBC_DRIVER = "org.hsqldb.jdbcDriver";
    
    /** TMCDB Jar library file */
    public static final String TMCDB_JAR_FILE = "TMCDB.jar";
    
    /** HSQLDB DDL scripts. */
    public static final String HSQLDB_SWCONFIGCORE_CREATE_SQL_SCRIPT = "TMCDB_swconfigcore/CreateHsqldbTables.sql";
    public static final String HSQLDB_SWCONFIGEXT_CREATE_SQL_SCRIPT = "TMCDB_swconfigext/CreateHsqldbTables.sql";
    public static final String HSQLDB_HWCONFIGMONITORING_CREATE_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/CreateHsqldbTables.sql";
    public static final String HSQLDB_HWCONFIGMONITORING_CREATE_TRIGGERS_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/CreateHsqldbTriggers.sql";
    
    /** HSQLDB DB cleaning scripts. */
    public static final String HSQLDB_SWCONFIGCORE_DELETE_SQL_SCRIPT = "TMCDB_swconfigcore/DropAllTables.sql";
    public static final String HSQLDB_SWCONFIGEXT_DELETE_SQL_SCRIPT = "TMCDB_swconfigext/DropAllTables.sql";
    public static final String HSQLDB_HWCONFIGMONITORING_DELETE_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/DropAllTables.sql";
    public static final String HSQLDB_HWCONFIGMONITORING_DELETE_TRIGGERS_SQL_SCRIPT = "TMCDB_hwconfigmonitoring/DropHsqldbTriggers.sql";
    
    
    public static void createTables(String url, String user, String password) throws Exception {
        Class.forName(HSQLDB_JDBC_DRIVER);
        String ddl;
        Connection conn;
        ddl = readTmcdbDDLFile(HSQLDB_SWCONFIGCORE_CREATE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
        ddl = readTmcdbDDLFile(HSQLDB_SWCONFIGEXT_CREATE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
        ddl = readTmcdbDDLFile(HSQLDB_HWCONFIGMONITORING_CREATE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
//        try {
//            ddl = readTmcdbDDLFile(HSQLDB_HWCONFIGMONITORING_CREATE_TRIGGERS_SQL_SCRIPT);
//            conn = DriverManager.getConnection(url, user, password);
//            runScript(ddl, conn);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
        conn.close();
    }

    public static void dropTables(String url, String user, String password) throws Exception {
    	Class.forName(HSQLDB_JDBC_DRIVER);
        String ddl;
        Connection conn;
//        try {
//            ddl = readTmcdbDDLFile(HSQLDB_HWCONFIGMONITORING_DELETE_TRIGGERS_SQL_SCRIPT);
//            conn = DriverManager.getConnection(url, user, password);
//            runScript(ddl, conn);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
        ddl = readTmcdbDDLFile(HSQLDB_HWCONFIGMONITORING_DELETE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
        ddl = readTmcdbDDLFile(HSQLDB_SWCONFIGEXT_DELETE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
        ddl = readTmcdbDDLFile(HSQLDB_SWCONFIGCORE_DELETE_SQL_SCRIPT);
        conn = DriverManager.getConnection(url, user, password);
        runScript(ddl, conn);
        conn.close();
    }
    
    /**
     * Execute an SQL script.
     * @param script  The SQL script, as a single string
     * @param conn    Connection to the DB server
     * @throws SQLException
     */
    protected static void runScript( String script, Connection conn )
            throws SQLException {
    
        Statement stmt = conn.createStatement();
        String[] statements = script.split( ";", -1 );
        for( int i = 0; i < statements.length; i++ ) {
            String statement = statements[i].trim();
            if( statement.length() == 0 ) {
                // skip empty lines
                continue;
            }
            stmt.execute( statement );
        }
    }
    
    /**
     * Searches for a library file in ACS library locations, first in ACSROOT
     * and second in INTROOT.
     * @param lib Library name
     * @return File path to the library, null if it is not in ACS library locations
     */
    protected static String findAcsLibrary(String lib) {
        String[] acsDirs = new String[] {"ACSROOT", "INTROOT"};
        for (String d : acsDirs) {
            String dir = System.getenv(d);
            if (dir != null) {
                String jar = dir + "/lib/" + lib;
                File f = new File(jar);
                if (f.exists()) return jar;
            }            
        }
        return null;
    }
    
    /**
     * Searches for a library file in ACS config locations, first in INTROOT
     * and second in ACSROOT.
     * @param file Configuration file name
     * @return File path to the configuration file, null if it is not in
     * ACS configuration locations
     */
    protected static String findAcsConfigFile(String file) {
        String[] acsDirs = new String[] {"INTROOT", "ACSROOT"};
        for (String d : acsDirs) {
            String dir = System.getenv(d);
            if (dir != null) {
                String cfgf = dir + "/config/" + file;
                File f = new File(cfgf);
                if (f.exists()) return cfgf;
            }            
        }
        return null;        
    }
    
    /**
     * Extracts a text file from a Jar library. Returns its contents as a string.
     * 
     * @param jar Jar library
     * @param file File to read from the Jar file
     * @return file contents
     * @throws IOException
     */
    protected static String readFileFromJar(String jar, String file)
        throws IOException {
        FileInputStream in = new FileInputStream(findAcsLibrary(jar));
        JarInputStream jarin = new JarInputStream(in);
        ZipEntry ze = jarin.getNextEntry();
        while (ze != null) {
            if (ze.getName().equals(file))
                break;
            ze = jarin.getNextEntry();
        }
        InputStreamReader converter = new InputStreamReader(jarin);
        BufferedReader reader = new BufferedReader(converter);

        StringBuffer ddlbuff = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            ddlbuff.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return new String(ddlbuff);
    }
    
    /**
     * Read a configuration file from ACS standard configuration locations.
     * Returns the file contents as a String.
     * 
     * @param file Configuratio file name
     * @return file contents
     * @throws IOException
     */
    protected static String readConfigFile(String file)
        throws IOException {
        FileInputStream in = new FileInputStream(findAcsConfigFile(file));
        InputStreamReader converter = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(converter);
        
        StringBuffer ddlbuff = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            ddlbuff.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return new String(ddlbuff);
    }
    
    protected static String readTmcdbDDLFile(String relativeFilePathName)
        throws IOException {
        String acsDataDir = System.getenv("ACSDATA");
        String ddlDataDir = acsDataDir + "/config/DDL/hsqldb/";
        FileInputStream in = new FileInputStream(ddlDataDir + relativeFilePathName);
        InputStreamReader converter = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(converter);
        
        StringBuffer ddlbuff = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            ddlbuff.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return new String(ddlbuff);        
    }
}
