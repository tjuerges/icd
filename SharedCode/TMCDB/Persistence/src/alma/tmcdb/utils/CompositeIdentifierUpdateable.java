/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.tmcdb.utils;

/**
 * This interface must be implemented by Hibernate persistent classes that
 * define a composite identifier class to map a composite primary key in the
 * database model. This is the case of association tables with attributes, where
 * their primary keys are composed from the primary keys of the tables they associate.
 * As the mapping for these tables also provide navigation to the associated tables,
 * the persistent objects duplicate these ID's: they participate in their composite id and
 * as a navigation field.
 * 
 * If objects of these classes are constructed from objects that hasn't been persisted
 * yet, then when they are persisted their identifiers are null even though the navigation
 * attributes have been persisted and contain a valid identifier, causing the persist
 * operation to fail.
 * 
 * This interface allows these classes to update the composite identifier just before
 * saving them to the database, by means of an Hibernate Interceptor.
 * 
 * @author rhiriart
 * @see CompositeIdentifierInterceptor
 *
 */
public interface CompositeIdentifierUpdateable {
    
    /**
     * Update the composite identifier fields corresponding to associated objects.
     */
    void updateId(); 
}
