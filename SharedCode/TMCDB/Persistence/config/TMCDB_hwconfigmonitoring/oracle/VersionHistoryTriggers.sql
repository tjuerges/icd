create or replace function date_to_unixts(oracle_date in date) return FLOAT is
  unix_epoch date := to_date('19700101000000','YYYYMMDDHH24MISS');
  unix_ts FLOAT;
begin
  unix_ts := (oracle_date - unix_epoch) * 86400.0;
  return(unix_ts);
end;
/

create or replace trigger PMHistTrig
after insert on PointingModel
for each row
declare
  swConf Configuration.ConfigurationId%TYPE;
begin
  select hc.SwConfigurationId into swConf
    from HwConfiguration hc, BaseElement be, Antenna an
    where be.BaseElementId = an.BaseElementId and be.ConfigurationId = hc.ConfigurationId
    and an.BaseElementId = :new.AntennaId;
  update BL_VersionInfo set EntityId = :new.PointingModelId where
    TableName = 'PointingModel' and SwConfigurationId = swConf and EntityId = -1;
end;
/

create or replace trigger PointingModelHistTrig
before insert or update or delete on PointingModelCoeff
for each row
declare
  versionInfo BL_VersionInfo%ROWTYPE;
  version BL_PointingModelCoeff.Version%TYPE;
  swConf Configuration.ConfigurationId%TYPE;
  oper char(1);
  entityId number(10);
  coeffName varchar2(128);
  coeffValue binary_double;
begin
  if inserting then
    oper := 'I';
    entityId := :new.PointingModelId;
    coeffName := :new.CoeffName;
    coeffValue := :new.CoeffValue;
  elsif updating then
    oper := 'U';
    entityId := :old.PointingModelId;
    coeffName := :old.CoeffName;
    coeffValue := :old.CoeffValue;
  elsif deleting then
    oper := 'D';
    entityId := :old.PointingModelId;
    coeffName := :old.CoeffName;
    coeffValue := :old.CoeffValue;
  end if;
  select hc.SwConfigurationId into swConf
    from HwConfiguration hc, BaseElement be, Antenna an, PointingModel pm
    where be.BaseElementId = an.BaseElementId and pm.AntennaId = be.BaseElementId
    and be.ConfigurationId = hc.ConfigurationId and pm.PointingModelId = entityId;
  select * into versionInfo from BL_VersionInfo where
    TableName = 'PointingModel' and SwConfigurationId = swConf and EntityId = entityId;
  version := versionInfo.CurrentVersion;
  if versionInfo.IncreaseVersion = '1' then
    version := version + 1;
    update BL_VersionInfo set CurrentVersion = version;
    update BL_VersionInfo set IncreaseVersion = '0';
  end if;
  insert into BL_PointingModelCoeff values (
    version,
    date_to_unixts(sysdate),
    oper,
    versionInfo.Who,
    versionInfo.ChangeDesc,
    entityId,
    coeffName,
    coeffValue);
end;
/

create or replace trigger PMCoeffOffsetHistTrig
before insert or update or delete on PointingModelCoeffOffset
for each row
declare
  versionInfo BL_VersionInfo%ROWTYPE;
  version BL_PointingModelCoeff.Version%TYPE;
  swConf Configuration.ConfigurationId%TYPE;
  coeff PointingModelCoeff%ROWTYPE;
  oper char(1);
  coeffId number(10);
  band varchar2(128);
  offset binary_double;
begin
  if inserting then
    oper := 'I';
    coeffId := :new.PointingModelCoeffId;
    band := :new.ReceiverBand;
    offset := :new.Offset;
  elsif updating then
    oper := 'U';
    coeffId := :old.PointingModelCoeffId;
    band := :old.ReceiverBand;
    offset := :old.Offset;
  elsif deleting then
    oper := 'D';
    coeffId := :old.PointingModelCoeffId;
    band := :old.ReceiverBand;
    offset := :old.Offset;
  end if;
  select * into coeff from PointingModelCoeff where
    PointingModelCoeffId = coeffId;
  select hc.SwConfigurationId into swConf
    from HwConfiguration hc, BaseElement be, Antenna an, PointingModel pm
    where be.BaseElementId = an.BaseElementId and pm.AntennaId = be.BaseElementId
    and be.ConfigurationId = hc.ConfigurationId and pm.PointingModelId = coeff.PointingModelId;
  select * into versionInfo from BL_VersionInfo where
    TableName = 'PointingModel' and SwConfigurationId = swConf and EntityId = coeff.PointingModelId;
  version := versionInfo.CurrentVersion;
  if versionInfo.IncreaseVersion = '1' then
    version := version + 1;
    update BL_VersionInfo set CurrentVersion = version;
    update BL_VersionInfo set IncreaseVersion = '0';
  end if;
  insert into BL_PointingModelCoeffOffset values (
    version,
    date_to_unixts(sysdate),
    oper,
    versionInfo.Who,
    versionInfo.ChangeDesc,
    coeff.PointingModelId,
    coeff.CoeffName,
    band,
    offset);
end;
/