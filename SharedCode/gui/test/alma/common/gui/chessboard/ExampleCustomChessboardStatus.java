package alma.common.gui.chessboard;

import java.awt.Color;

/**
 * This class is an example of how one can create a custom class for unique status 
 * values if one doesn't want to use the default status enum found in 
 * <code>DefaultChessboardStatus</code>.<br><br>
 * 
 * NOTE: it is a conscious choice to use the typesafe enum pattern (i.e. a class) rather 
 * than Java 5.0's builtin (and superior in most cases) enum mechanism because this will 
 * allow more flexibility for others to extend the enumerated types for custom uses of the 
 * chessboard - for example if the status values are different in some contexts (such as
 * Scheduling).<br><br>
 * 
 * @author Steve Harrington
 * 
 * @see DefaultChessboardStatus
 * @see ChessboardStatus
 */
public class ExampleCustomChessboardStatus implements ChessboardStatus 
{
	private static final String HORRIFIC_STRING = "I LIKE PUDDIN";
	private static final String WORSE_STRING = "WORSE";
	private static final String BAD_STRING = "BAD";
	private static final String BETTER_STRING = "BETTER";
	private static final String GOOD_STRING = "GOOD";
	
	private boolean shouldFlash;
	private String description;
	private Color bgColor;
	private Color fgColor;
	private boolean isSelectable;
	private static ChessboardStatus[] values = null;
	
	/**
	 * Private constructor enforces typesafe enum pattern.
	 * @param description the description of the enum
	 */
	private ExampleCustomChessboardStatus(String description)
	{
		this.bgColor = null;
		this.fgColor = null;
		this.description = description;
		this.shouldFlash = false;
		this.isSelectable = false;
		
		// configure the status with proper color and/or flashing attributes
		if(description.equals(GOOD_STRING)) {
			this.bgColor = null;
			this.fgColor = null;
			this.isSelectable = true;
		} 
		else if(description.equals(BETTER_STRING)) {
			this.bgColor = Color.CYAN;
			this.fgColor = Color.BLACK;
		} 
		else if(this.description.equals(HORRIFIC_STRING)) {
			this.shouldFlash = true;
			this.bgColor = Color.PINK;
			this.fgColor = Color.BLACK;
		} 
		else if(this.description.equals(BAD_STRING)) {
			this.bgColor = Color.ORANGE;
			this.fgColor = Color.WHITE;
		} 
		else if(this.description.equals(WORSE_STRING)) {
			this.bgColor = Color.MAGENTA;
			this.fgColor = Color.BLACK;
		}
	}
	
	// Create the fixed set of allowed instances (these will be the enum values).
	/** 
	 * Example custom status
	 */
	public final static ExampleCustomChessboardStatus GOOD = 
		new ExampleCustomChessboardStatus(GOOD_STRING);
	
	/** 
	 * Example custom status
	 */
	public final static ExampleCustomChessboardStatus BETTER = 
		new ExampleCustomChessboardStatus(BETTER_STRING);
	
	/** 
	 * Example custom status
	 */
	public final static ExampleCustomChessboardStatus BAD =
		new ExampleCustomChessboardStatus(BAD_STRING);
	
	/** 
	 * Example custom status
	 */
	public final static ExampleCustomChessboardStatus WORSE =
		new ExampleCustomChessboardStatus(WORSE_STRING);
	
	/** 
	 * Example custom status
	 */
	public final static ExampleCustomChessboardStatus HORRIFIC =
		new ExampleCustomChessboardStatus(HORRIFIC_STRING);
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the background color that should be used to render the status in the user interface.
	 */
	public Color getBgColor() {
		return this.bgColor;
	}

	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the foreground color that should be used to render the status in the user interface.
	 */
	public Color getFgColor() {
		return this.fgColor;
	}
	
	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be selectable in the UI.
	 */
	public boolean isSelectable() 
	{
		return this.isSelectable;
	}
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the description of the status.
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Overriding generic toString method from <code>Object</code> to get
	 * something more user-friendly.
	 * 
	 * @return a string representation of this status which is the same as the status name.
	 */
	@Override
	public String toString() {
		return this.description;
	}
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether the status should be rendered
	 * in the user interface as a flashing cell.
	 */
	public boolean shouldFlash() {
		return this.shouldFlash;
	}

	/** 
	 * Required method of the <code>ChessboardStatus</code> interface. Returns
	 * all of the valid instances of this enum class. This is mostly just used 
	 * for testing. Method is synchronized because it employs a singleton
	 * for the values array. For an example of its use in a test setting, 
	 * see the <code>ChessboardTest</code> and <code>ExampleChessboardStatusProvider</code> classes.
	 * 
	 * @return the full set of statuses for this enum.
	 */
	public synchronized ChessboardStatus[] values() 
	{
		// use a singleton-style mechanism to return the values
		if(null == ExampleCustomChessboardStatus.values) 
		{
			ExampleCustomChessboardStatus.values = new ChessboardStatus[5];

			ExampleCustomChessboardStatus.values[0] = ExampleCustomChessboardStatus.BETTER;
			ExampleCustomChessboardStatus.values[1] = ExampleCustomChessboardStatus.BAD;
			ExampleCustomChessboardStatus.values[2] = ExampleCustomChessboardStatus.WORSE;
			ExampleCustomChessboardStatus.values[3] = ExampleCustomChessboardStatus.HORRIFIC;
			ExampleCustomChessboardStatus.values[4] = ExampleCustomChessboardStatus.GOOD;
		}
		
		return ExampleCustomChessboardStatus.values;
	}
}


