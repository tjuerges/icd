package alma.common.gui.chessboard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuItem;

import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.internals.ChessboardFrame;

/**
 * The <code>ChessboardTest</code> class is used to test the chessboard components and 
 * can also be used as an example by those interested in using the <code>ChessboardFrame</code>
 * in other contexts.
 * 
 * @author Steve Harrington
 * @see ChessboardFrame
 * @see ChessboardEntry
 * @see ExampleChessboardStatusProvider
 */
public class ChessboardTest 
{
    /**
     * Main method used for testing and rapid prototyping.
     * @param args the command line arguments.
     */
    public static void main(String[] args) 
    {
    	// for this test, we'll make up some fake data
    	
    	// first, for use in testing a chessboard with 1-D array in the constructor, we
    	// will create a 1-D array
    	dataOneDimensional = new ChessboardEntry[13];
    	dataOneDimensional[0] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "1");
    	dataOneDimensional[1] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "2");
    	dataOneDimensional[2] = new ChessboardEntry(DefaultChessboardStatus.INFO, "3");
    	dataOneDimensional[3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "4");
    	dataOneDimensional[4] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_OFFLINE, "5");
    	dataOneDimensional[5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "6");
    	dataOneDimensional[6] = new ChessboardEntry(DefaultChessboardStatus.SEVERE_ERROR, "7");
    	dataOneDimensional[7] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "8");
    	dataOneDimensional[8] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "9");
    	dataOneDimensional[9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "10");
    	dataOneDimensional[10] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_NOT_IN_ARRAY, "11");
    	dataOneDimensional[11] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "12");
    	dataOneDimensional[12] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "13");
    	
    	// second, we create a 2-D array to test the alternate constructor of the ChessboardFrame
    	// this dataTwoDimensional is used to test a 'large' or 'full' sized chessboard
    	dataTwoDimensional = new ChessboardEntry[7][10];
    	
    	dataTwoDimensional[0][0] = new ChessboardEntry(DefaultChessboardStatus.FATAL_ERROR, "1");
    	dataTwoDimensional[0][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "2");
    	dataTwoDimensional[0][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "3");
    	dataTwoDimensional[0][3] = new ChessboardEntry(DefaultChessboardStatus.INFO, "4");
    	dataTwoDimensional[0][4] = new ChessboardEntry(DefaultChessboardStatus.FATAL_ERROR, "5");
    	dataTwoDimensional[0][5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "6");
    	dataTwoDimensional[0][6] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "7");
    	dataTwoDimensional[0][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "8");
    	dataTwoDimensional[0][8] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "9");
    	dataTwoDimensional[0][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "10");
    	
    	dataTwoDimensional[1][0] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "11");
    	dataTwoDimensional[1][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "12");
    	dataTwoDimensional[1][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "13");
    	dataTwoDimensional[1][3] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_NOT_INSTALLED, null);
    	dataTwoDimensional[1][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "15");
    	dataTwoDimensional[1][5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "16");
    	dataTwoDimensional[1][6] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_NOT_IN_ARRAY, null);
    	dataTwoDimensional[1][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "18");
    	dataTwoDimensional[1][8] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "19");
    	dataTwoDimensional[1][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "20");
    	
    	dataTwoDimensional[2][0] = new ChessboardEntry(DefaultChessboardStatus.FATAL_ERROR, "21");
    	dataTwoDimensional[2][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "22");
    	dataTwoDimensional[2][2] = new ChessboardEntry(DefaultChessboardStatus.INFO, "23");
    	dataTwoDimensional[2][3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "24");
    	dataTwoDimensional[2][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "25");
    	dataTwoDimensional[2][5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "26");
    	dataTwoDimensional[2][6] = new ChessboardEntry(DefaultChessboardStatus.FATAL_ERROR, "27");
    	dataTwoDimensional[2][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "28");
    	dataTwoDimensional[2][8] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_OFFLINE, "29");
    	dataTwoDimensional[2][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "30");
    	
    	dataTwoDimensional[3][0] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "31");
    	dataTwoDimensional[3][1] = new ChessboardEntry(DefaultChessboardStatus.SEVERE_ERROR, "32");
    	dataTwoDimensional[3][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "33");
    	dataTwoDimensional[3][3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "34");
    	dataTwoDimensional[3][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "35");
    	dataTwoDimensional[3][5] = new ChessboardEntry(DefaultChessboardStatus.WARNING, "36");
    	dataTwoDimensional[3][6] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "37");
    	dataTwoDimensional[3][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "38");
    	dataTwoDimensional[3][8] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "39");
    	dataTwoDimensional[3][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "40");
    	
    	dataTwoDimensional[4][0] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_OFFLINE, "41");
    	dataTwoDimensional[4][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "42");
    	dataTwoDimensional[4][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "43");
    	dataTwoDimensional[4][3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "44");
    	dataTwoDimensional[4][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "45");
    	dataTwoDimensional[4][5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "46");
    	dataTwoDimensional[4][6] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "47");
    	dataTwoDimensional[4][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "48");
    	dataTwoDimensional[4][8] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "49");
    	dataTwoDimensional[4][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "50");
    	
    	dataTwoDimensional[5][0] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S1");
    	dataTwoDimensional[5][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S2");
    	dataTwoDimensional[5][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S3");
    	dataTwoDimensional[5][3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S4");
    	dataTwoDimensional[5][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S5");
    	dataTwoDimensional[5][5] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S6");
    	dataTwoDimensional[5][6] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S7");
    	dataTwoDimensional[5][7] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S8");
    	dataTwoDimensional[5][8] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S9");
    	dataTwoDimensional[5][9] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S10");
    	
    	dataTwoDimensional[6][0] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S11");
    	dataTwoDimensional[6][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "S12");
    	dataTwoDimensional[6][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "T1");
    	dataTwoDimensional[6][3] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "T2");
    	dataTwoDimensional[6][4] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "T3");
    	dataTwoDimensional[6][5] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_NOT_INSTALLED, "T4");
    	dataTwoDimensional[6][6] = null;
    	dataTwoDimensional[6][7] = null;
    	dataTwoDimensional[6][8] = null;
    	dataTwoDimensional[6][9] = null;
    	
    	/* this dataTwoDimensional can be used to test a 'small' size chessboard
    	dataTwoDimensional = new ChessboardEntry[1][4];
    	dataTwoDimensional[0][0] = new ChessboardEntry(DefaultChessboardStatus.FATAL_ERROR, "1");
    	dataTwoDimensional[0][1] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "2");
    	dataTwoDimensional[0][2] = new ChessboardEntry(DefaultChessboardStatus.NORMAL, "3");
    	dataTwoDimensional[0][3] = new ChessboardEntry(DefaultChessboardStatus.INFO, "4");
    	*/
    	
    	// third, we create a 2-D array to test the custom status capabilities of the chessboard
    	dataCustomStatus = new ChessboardEntry[2][8];
    
    	dataCustomStatus[0][0] = new ChessboardEntry(ExampleCustomChessboardStatus.GOOD, "1");
    	dataCustomStatus[0][1] = new ChessboardEntry(ExampleCustomChessboardStatus.BETTER, "2");
    	dataCustomStatus[0][2] = new ChessboardEntry(ExampleCustomChessboardStatus.HORRIFIC, "3");
    	dataCustomStatus[0][3] = new ChessboardEntry(ExampleCustomChessboardStatus.WORSE, "4");
    	dataCustomStatus[0][4] = new ChessboardEntry(ExampleCustomChessboardStatus.GOOD, "5");
    	dataCustomStatus[0][5] = new ChessboardEntry(ExampleCustomChessboardStatus.BETTER, "6");
    	dataCustomStatus[0][6] = new ChessboardEntry(ExampleCustomChessboardStatus.HORRIFIC, "7");
    	dataCustomStatus[0][7] = new ChessboardEntry(ExampleCustomChessboardStatus.WORSE, "8");
    	
    	dataCustomStatus[1][0] = new ChessboardEntry(ExampleCustomChessboardStatus.GOOD, "9");
    	dataCustomStatus[1][1] = new ChessboardEntry(ExampleCustomChessboardStatus.BETTER, "10");
    	dataCustomStatus[1][2] = new ChessboardEntry(ExampleCustomChessboardStatus.HORRIFIC, "11");
    	dataCustomStatus[1][3] = new ChessboardEntry(ExampleCustomChessboardStatus.WORSE, "12");
    	dataCustomStatus[1][4] = new ChessboardEntry(ExampleCustomChessboardStatus.GOOD, "13");
    	dataCustomStatus[1][5] = new ChessboardEntry(ExampleCustomChessboardStatus.BETTER, "14");
    	dataCustomStatus[1][6] = new ChessboardEntry(ExampleCustomChessboardStatus.HORRIFIC, "15");
    	dataCustomStatus[1][7] = new ChessboardEntry(ExampleCustomChessboardStatus.BAD, "16");
    	
    	int numberOfRuns = 0;
    	if(args.length == 2 && (args[0].equals(NUM_RUNS_SHORT_ARG) || args[0].equals(NUM_RUNS_LONG_ARG))) {
    		try {
    			numberOfRuns = Integer.parseInt(args[1]);	
    		}
    	   	catch(NumberFormatException ex) {
    	   		printUsage();
    	   		System.exit(-1);
    	   	}
    	}
    	
    	// make it final so the thread can access it
    	final int numRuns = numberOfRuns;
    	
    	//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater
        (
        	new Runnable() {
        		public void run() {
        			createAndShowGUI(numRuns);
        		}
        	}
        );
    }
 
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread. Used by the main method for rapid prototyping/testing.
     */
    @SuppressWarnings("serial")
	private static void createAndShowGUI(int numberOfRuns) 
    { 
    	// Test case 1: test the chessboard frame created with a 2-D array for the initial data
    	
    	// Ask for window decorations provided by the look and feel.
    	//JFrame.setDefaultLookAndFeelDecorated(true);
    	
        // Create and set up the window.
        ChessboardFrame frame1 = new ChessboardFrame("Chessboard 2-D constructor", ChessboardTest.dataTwoDimensional, 
        		new ExampleChessboardDetailsPluginFactory(), false, true, null);  
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
        // Create a test 'status provider' which will publish
        // state changes, and register the chessboard as a listener to this publisher
        // so that our chessboard will update for some fake/test state changes. When using
        // the Chessboard in a 'real world' setting rather than a test case,
        // we would register as a listener to a real data source rather than
        // this test data source.
        ExampleChessboardStatusProvider testChanger1 = new ExampleChessboardStatusProvider(numberOfRuns,  
        			DefaultChessboardStatus.NORMAL.values(), ChessboardTest.dataTwoDimensional);
         
        testChanger1.addChessboardStatusListener(frame1);
        
        // Display the window.
        frame1.pack();
        frame1.setVisible(true);
        
        // Test case 2: test a smaller chessboard frame
        
        // Create and set up the window. This particular test doesn't update the cells w/ status changes, unlike the other 2 tests.
        ChessboardFrame frame2 = 
        	new ChessboardFrame("Chessboard 1-D constructor", ChessboardTest.dataOneDimensional, 2, 7, 
        	// construct a class (ExampleChessboardDetailsPluginFactory) 
        	// which implements the ChessboardDetailsPluginFactory interface, 
        	// which we pass as a parameter to the constructor of the ChessboardFrame; 
        	// it will be used to display the details about a chessboard cell.
        	new ExampleChessboardDetailsPluginFactory(), true, null);  

        //frame2.setEnabled(false);
        
        //Display the window.
        frame2.pack();
        frame2.setVisible(true);
        
        // Test case 3: test chessboard frame using custom status class
        
        // Create and set up the window. This test doesn't update the cells w/ status changes.
        final ChessboardFrame frame3 = new ChessboardFrame("Chessboard Custom Status", ChessboardTest.dataCustomStatus, 
        			null, true, false, null);  
        
        // this test disables details display for the chessboard; this feature may be useful for scheduling for example.
        frame3.setDetailsDisplayEnabled(false);
        
        // Create a test 'status provider' which will publish
        // state changes, and register the chessboard as a listener to this publisher
        // so that our chessboard will update for some fake/test state changes. When using
        // the Chessboard in a 'real world' setting rather than a test case,
        // we would register as a listener to a real data source rather than
        // this test data source.
        ExampleChessboardStatusProvider testChanger3 = new ExampleChessboardStatusProvider(numberOfRuns, 
        		ExampleCustomChessboardStatus.GOOD.values(), ChessboardTest.dataCustomStatus);
           
        testChanger3.addChessboardStatusListener(frame3);
        
        //Display the window.
        frame3.pack();
        frame3.setVisible(true);
        
        // Test case 4: test chessboard frame using custom popup menu items
        /**
         * private class for testing popup menu on chessboard.
         */
        @SuppressWarnings("serial")
		class DoSomethingMenuItem extends JMenuItem
        {
        	private ChessboardFrame owningFrame;
        	
        	/**
        	 * Constructor
        	 * @param itemName the name of the menu item, which will be displayed on the popup menu to the user.
        	 */
        	public DoSomethingMenuItem(final String itemName)
        	{
        		super(itemName);
        		this.addActionListener(new ActionListener() {
        			public void actionPerformed(ActionEvent evt) {
        				System.out.println(itemName + " for items: ");
        				for(ChessboardEntry entry : owningFrame.getSelectedEntries())
        				{
        					System.out.println(entry.getDisplayName());
        				}
        			}
        		});
        	}
        	
        	public void setOwningChessboardFrame(ChessboardFrame owningFrame)
        	{
        		this.owningFrame = owningFrame;
        	}
        }
        
        ChessboardFrame frame4 = null;

        final DoSomethingMenuItem[] menuItems = new DoSomethingMenuItem[3];
        menuItems[0] = new DoSomethingMenuItem("Something");        
        menuItems[1] = new DoSomethingMenuItem("Something else");
        menuItems[2] = new DoSomethingMenuItem("Not available for flashing cells");
        ChessboardPopupMenu menu = new ChessboardPopupMenu() 
        {
			@Override
			public void enableOrDisableChoicesForSelections(ChessboardEntry[] selectedEntries) 
			{
				if(null != selectedEntries)
				{
					menuItems[0].setEnabled(true);
					menuItems[1].setEnabled(true);
					menuItems[2].setEnabled(true);
					for(ChessboardEntry entry: selectedEntries)
					{
						if(entry.shouldFlash()) 
						{
							menuItems[2].setEnabled(false);
						}
					}
				}
			}
        };
        for(JMenuItem menuItem: menuItems) {
        	menu.add(menuItem);
        }
        
        // Create and set up the window.
        frame4 = new ChessboardFrame("Chessboard custom popup menu", ChessboardTest.dataTwoDimensional, 
        		new ExampleChessboardDetailsPluginFactory(), true, true, null, menu);  
    	        
        menuItems[0].setOwningChessboardFrame(frame4);
        menuItems[1].setOwningChessboardFrame(frame4);
        menuItems[2].setOwningChessboardFrame(frame4);
        
        // Create a test 'status provider' which will publish
        // state changes, and register the chessboard as a listener to this publisher
        // so that our chessboard will update for some fake/test state changes. When using
        // the Chessboard in a 'real world' setting rather than a test case,
        // we would register as a listener to a real data source rather than
        // this test data source.
        ExampleChessboardStatusProvider testChanger4 = new ExampleChessboardStatusProvider(numberOfRuns,  
        			DefaultChessboardStatus.NORMAL.values(), ChessboardTest.dataTwoDimensional);

	    testChanger4.addChessboardStatusListener(frame1);
        
        // Display the window.
        frame4.pack();
        frame4.setVisible(true);	
    }

    private static void printUsage()
    {
    	System.out.println("Usage: ChessboardTest <NumberOfPasses>\n\n");
    	System.out.println("where: NumberOfPasses is an integer indicating how many times to walk through the cells of the chessboard");
    }
    
    private static ChessboardEntry[][] dataTwoDimensional;
    private static ChessboardEntry[] dataOneDimensional;
    private static ChessboardEntry[][] dataCustomStatus;
    private static final String NUM_RUNS_LONG_ARG = "--numRuns";
	private static final String NUM_RUNS_SHORT_ARG = "-n";
}



