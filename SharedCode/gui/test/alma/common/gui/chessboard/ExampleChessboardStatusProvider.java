package alma.common.gui.chessboard;

import alma.common.gui.chessboard.ChessboardStatusEvent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Timer;

/**
 * An example implementation of a publisher for status changes, for use in
 * chessboard testing. 
 * 
 * @author Steve Harrington
 *
 * @see ChessboardStatusAdapter
 * @see ChessboardStatusListener
 * @see ChessboardStatusEvent
 */
public class ExampleChessboardStatusProvider extends ChessboardStatusAdapter 
{
	/**
	 * Start the tests, which will send mock events/updates to the registered listeners
	 * which in this case is a chessboard frame - see the <code>ChessboardTest</code>
	 * class for the code which connects/registers the listeners with this class.
	 * 
	 * TODO: consider passing in a 2-D array of ChessboardEntries to the constructor
	 * rather than all the separate args (rows, cols, names)?
	 * 
	 * @param numRuns the number of passes to make changing the status of cells
	 * throughout the chessboard.
	 * @param status an array of ChessboardStatus indicating all the valid values for the chessboard;
	 * this will be used to 'walk' the chessboard cells through each state.
	 * @param dummyInitialData the initial state of the chessboard cells.
	 */
	public ExampleChessboardStatusProvider(int numRuns, ChessboardStatus[] status, ChessboardEntry[][] dummyInitialData)
	{
		this.status = status;
		this.numberOfRuns = numRuns;
		this.numberOfRows = dummyInitialData.length;
		this.numberOfColumns = dummyInitialData[0].length;
		this.initialData = dummyInitialData;
		this.cellNames = this.getNamesForDataTwoDimensional(dummyInitialData);
		driveTests();
	}

	/**
	 * Method which returns the initial (current) state of a chessboard, used to
	 * populate a chessboard upon startup, after which a publish/subscribe (observer pattern)
	 * mechanism is used to keep the chessboard informed of status changes.
	 * 
	 * @return a 2-D array of ChessboardEntry objects indicating both the dimensions
	 * of the chessboard (implicit in the size of the 2-d array) and the initial states
	 * of each cell.
	 */
	public ChessboardEntry[][] getStates() 
	{
		// this simply returns the 'dummy' data that was passed in the constructor;
		// in the 'real' world, this might actually do something like interrogate some
		// hardware, etc. in order to get the initial state.
		return initialData;
	}
	
	/**
	 * Required method of the ChessboardStatusProvider interface.
	 */
	public void stop() {
		// noop
	}
	
	/**
	 * Runs through all the cells of the test chessboard and updates the status for each cell.
	 */
	private void driveTests()
	{
		int padPerPass = (numberOfRows * numberOfColumns) + 1;
		
		for(int i = 0; i < numberOfRuns; i++)
		{
			int sleepOffset = i * (padPerPass*status.length);
			for(int j = 0; j < status.length; j++) 
			{
				changeStates(status[j], status[j].getDescription(), (padPerPass*j) + sleepOffset);
			}
		}
	}
	
	/**
	 * Posts state change events to the registered listeners (in our case, a <code>ChessboardFrame</code>)
	 * 
	 * @param status the status to use for the event
	 * @param hoverText the text to use for the tool tip
	 * @param sleepPadding the amount of time to pad the sleep before sending the event
	 */
	private void changeStates(final ChessboardStatus status, final String hoverText, final int sleepPadding)
	{
		final int delay = 1000; //milliseconds
		ActionListener taskPerformer = new ActionListener() 
		{
			public void actionPerformed(ActionEvent evt) 
			{
				int count = 0;
				String name = null;
				for(int i = 0; i < numberOfRows; i++) 
				{
					for(int j = 0; j < numberOfColumns; j++) 
					{
						name = cellNames[count++];
						ChessboardStatusEvent myEvent = new ChessboardStatusEvent(name, status, name + " is in state: " + hoverText);
						Thread t = new WorkerThread(myEvent, count + sleepPadding); 
						t.start();
					}
				}
			}
		};
		Timer mainTimer =  new Timer(delay, taskPerformer);
		//System.out.println("Starting taskTimer");
		mainTimer.setRepeats(false);
		mainTimer.start();
	}

	/**
	 * A thread which awakens after some specified time
	 * to notify listeners, so that all of our test state changes don't 
	 * arrive simultaneously and we can 'walk' through the cells on the 
	 * chessboard over time.
	 * 
	 * @author Steve Harrington
	 * @see Thread
	 */
	private class WorkerThread extends Thread 
	{
		ChessboardStatusEvent myStatusEvent;
		int sleepCount;
		
		/**
		 * Constructor for the worker thread.
		 * @param statEvent the event to post to all the listeners
		 * @param sleepCount the amount of time to sleep before posting event
		 */
		public WorkerThread(ChessboardStatusEvent statEvent, int sleepCount)
		{
			this.myStatusEvent = statEvent;
			this.sleepCount = sleepCount;
		}
		
		/**
		 * Sleep for a while, and then awaken to notify the listeners
		 */
		@Override
		public void run() {
			try {
				//System.out.println("WorkerThread sleeping...");
				Thread.sleep(100 * sleepCount);
				//System.out.println("WorkerThread awake and notifyingListeners for event: " + myStatusEvent.getName());
				notifyListeners(myStatusEvent);
			}
			catch (InterruptedException ex){ ex.printStackTrace();}
		}
	}
	
	/**
     * Private method to get all the names for the chessboard entries.
     * @return an array of strings representing the names of all the chessboard entries.
     */
    private String[] getNamesForDataTwoDimensional(ChessboardEntry[][] data)
    {
    	ArrayList<String> namesList = new ArrayList<String>();
    	for(int i = 0; i < data.length; i++) {
    		for(int j = 0; j < data[i].length; j++) {
    			String nameToAdd = null;
    			if(data[i][j] != null) {
    				nameToAdd = data[i][j].getDisplayName();	
    			}
    			namesList.add(nameToAdd);
    		}
    	}
    	
    	String [] retVal = new String[namesList.size()]; 
    	Iterator<String> listIter = namesList.iterator();
    	int count = 0;
    	while(listIter.hasNext()) {
    		String currentEntry = listIter.next();
    		retVal[count++] = currentEntry;
    	}
    	return retVal;
    }
	
	private int numberOfRuns, numberOfRows, numberOfColumns;
	private String[] cellNames;
	private ChessboardStatus[] status;
	private ChessboardEntry[][] initialData;
}



