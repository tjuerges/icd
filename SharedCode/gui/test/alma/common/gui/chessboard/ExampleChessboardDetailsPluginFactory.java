package alma.common.gui.chessboard;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Example implementation of the ChessboardDetailsPluginFactory interface. A 'real' implementation would be much more sophisticated.
 * <br><br>
 * <br>
 * TODO - refactor this to NOT use JDialog, so as to be more like a "real world" implementation.
 * NOTE: this example is a bit misleading as most people will NOT use a dialog in this context.
 * 
 * @author Steve Harrington
 * @see ChessboardDetailsPluginFactory
 * @see ChessboardTest
 */
public class ExampleChessboardDetailsPluginFactory implements ChessboardDetailsPluginFactory
{
	private static final String DETAILED_INFO_STRING = "This is some detailed info about Antenna: ";
		
	/**
	 * Required method of the <code>ChessboardDetailsPluginFactory</code> interface. Used to
	 * display details about cells in a chessboard.
	 * 
	 * @param names the names of the cells for which to show the details.
	 * @param containerServices the container services provided by the executive subsystem,
	 * which can be used, as needed, by the details provider - not used in this example.
	 * @param pluginTitle the unique title of the plugin (to be used in the title bar of the plugin window on screen).
	 * @param selectionTitle the title of the cell that was selected in the chessboard when this details plugin was launched;
	 *        this may not be unique if, for example, multiple instances of a details display have been shown for a single 
	 *        cell in the chessboard; say, cell S1 might have: selectionTitle of 'S1', pluginTitle values of: 'S1 - 1', 'S1 - 2', 'S1 - 3', etc.
	 * @return the child plugin that was displayed.
	 */
	public ChessboardDetailsPlugin[] instantiateDetailsPlugin(final String[] names, 
			PluginContainerServices containerServices,
			ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle) 
	{
		ChessboardDetailsPlugin[] retPlugins = new ExampleChessboardSubsystemPlugin[2];
		
		retPlugins[0] = new ExampleChessboardSubsystemPlugin(listener, containerServices, names, pluginTitle, selectionTitle);

		// NOTE:
		// This code is needed because for our test case we don't have any container services
		// with which to start the plugin (i.e. we're running our test case outside the regular OMC 
		// infrastructure. As such, most "real" implementations will not need the following lines which
		// invoke the start() method by hand. Usual implementations of this method would just instantiate
		// a plugin, and then return it!
		try {
			retPlugins[0].start();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return retPlugins;
	}
	
	@SuppressWarnings("serial")
	private class ExampleChessboardSubsystemPlugin extends ChessboardDetailsPlugin
	{
		private String[] names;
		private JDialog detailsDialog = null;
		
		/**
		 * Constructor
		 * @param owningChessboard the chessboard creating us.
		 * @param services the container services.
		 * @param names the names that were selected in the chessboard.
		 * @param pluginTitle the title of the plugin.
		 * @param selectionTitle the title of the item selected in the chessboard.
		 */
		public ExampleChessboardSubsystemPlugin(ChessboardDetailsPluginListener owningChessboard,
				PluginContainerServices services, String[] names, String pluginTitle, String selectionTitle)
		{
			super(names, owningChessboard, services, pluginTitle, selectionTitle);
			this.names = names;
		}
		
		@Override
		public void replaceContents(String[] newAntennaNames) 
		{
			this.specializedStop();
			this.names = newAntennaNames;
			try {
				this.start();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		public String getPluginName() {
			// TODO Auto-generated method stub
			return "Example chessboard";
		}

		@Override
		public void specializedStop() 
		{
			if(null != detailsDialog)
			{
				detailsDialog.setVisible(false);
				detailsDialog.dispose();
				detailsDialog = null;
			}
		}

		public boolean runRestricted(boolean restricted) throws Exception {
			// TODO Auto-generated method stub
			return false;
		}

		public void specializedStart() throws Exception 
		{
			if(null != names)
			{
				detailsDialog = new JDialog((JFrame)null, "DetailsDialog", false);
				StringBuffer allNames = new StringBuffer(names[0]);
				for(int i = 1; i < names.length; i++)
				{
					allNames.append(", ").append(names[i]); 
				}
				JLabel label = new JLabel(DETAILED_INFO_STRING + allNames.toString());

				detailsDialog.addWindowListener(new WindowAdapter() 
				{
					//
					// Invoked when a window has been closed
					//
					@Override
					public void windowClosing(WindowEvent evt) 
					{
						try {
							stop();
						}
						catch(Exception ex) 
						{
							specializedStop();
						}
					}
				});

				detailsDialog.getContentPane().add(label);
				detailsDialog.pack();
				detailsDialog.setVisible(true);
				detailsDialog.toFront();
			}	
		}
	}
}    


