
package alma.common.gui.astrotime;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


/**
 * @deprecated This class has moved to package {@code alma.common.gui.components.astrotime}.
 */
public class AstroTimeDisplay extends JPanel {

	protected JLabel lblUTC;
	protected JLabel lblLST;
	protected JComboBox cmbLocation;

	public AstroTimeDisplay() {

		this.lblUTC = new JLabel();
		this.lblLST = new JLabel();
		this.cmbLocation = new JComboBox();

		
		// predefined locations
		// ---------------------
		this.cmbLocation.addItem(new Location("ATF", 107.61778));
		this.cmbLocation.addItem(new Location("Chaj", 67.75917));

		
		// layout
		// -------
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.add(lblUTC);
		this.add(Box.createHorizontalStrut(8));
		this.add(lblLST);
		this.add(Box.createHorizontalStrut(8));
		this.add(cmbLocation);

		
		// adjust height of combobox to labels
		// ------------------------------------
		lblUTC.setText(" ");
		Dimension d = new Dimension(cmbLocation.getPreferredSize().width, lblUTC.getPreferredSize().height);
		cmbLocation.setPreferredSize(d);
		cmbLocation.setBorder(new EmptyBorder(0, 0, 0, 0));

		
		// refresh when user selects another location
		// -------------------------------------------
		this.cmbLocation.addItemListener(new ItemListener() {
			public void itemStateChanged (ItemEvent e) {
				refresh();
			}
		});
		
		
		// initial update
		// ---------------
		refresh();
	}


	public void refresh () {
		Date now = new Date();
		String utc = AstroTime.toSexagesimal(AstroTime.getUTC(now));

		Location loc = (Location) cmbLocation.getSelectedItem();
		String lst = AstroTime.toSexagesimal(AstroTime.getLST(now, loc.longitude));

		lblUTC.setText("UT:" + utc);
		lblLST.setText("LST:" + lst);
	}


	/**
	 * Little struct to feed into the ComboBox.
	 */
	private class Location {

		String name;
		double longitude;

		Location(String name, double longitude) {
			this.name = name;
			this.longitude = longitude;
		}

		@Override
		public String toString () {
			return name;
		}

	}


	// Testing
	// ===================================================================
	
	public static void main (String[] args) {
		AstroTimeDisplay inst = new AstroTimeDisplay();
		
		javax.swing.JFrame f = new javax.swing.JFrame();
		f.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(inst);
		f.pack();
		f.setVisible(true);
	}
}
