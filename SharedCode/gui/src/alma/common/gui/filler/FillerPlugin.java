/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.gui.filler;

import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

import java.awt.Dimension;

import javax.swing.JPanel;

/**
 * FillerPlugin is a OMC plug in that allows users to influence the layout of
 * panels in the OMC.
 * 
 * This class implements the interface required by the OMC. See:
 * http://almasw.hq.eso.org/almasw/bin/view/EXEC/SubsystemPlugins
 */
@SuppressWarnings("serial")
public class FillerPlugin extends JPanel implements SubsystemPlugin {

    // Used by Swing to size the filler panel.
    Dimension minimumSize;
    Dimension preferredSize;

    /**
     * No-args constructor required by OMC plug in framework.
     */
    public FillerPlugin() {
        // Minimum and preferred sizes are the result of guesses and tests.
        minimumSize = new Dimension(10, 10);
        preferredSize = new Dimension(100, 100);

        // Tell Swing what we want.
        this.setMinimumSize(minimumSize);
        this.setPreferredSize(preferredSize);
    } // method

    public boolean runRestricted(boolean restricted) throws Exception {
        // This class only fills space in the OMC.
        // Instances always run restricted.
        return true;
    } // method

    public void setServices(PluginContainerServices services) {
        // This class does not use OMC services.
    } // method

    public void start() throws Exception {
        // This class only fills space in the OMC.
    } // method

    public void stop() throws Exception {
        // This class only fills space in the OMC.
    } // method

} // class

//
// O_o
