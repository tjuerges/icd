/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2007
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

/** 
 * @author  acaproni   
 * @version $Id$
 * @since    
 */

package alma.common.gui.components.selector;

import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogLevel;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;

/**
 * A JPanel showing a selector for a component whose type is specified
 * by its (wildcarded) IDL (for example *Mount*).
 * 
 * The panel is composed of an editable combobox where the user can directly
 * write the name of the compnent or select one component from the drop list.
 * 
 * Another text filed accepts a wildcard espression to filter the entriesd in
 * the drop list in such a way only the names of the components matching such expression
 * appear in the list.
 * To filter the content of the combo box, the Filter button must be pressed.
 * 
 * The connect button is enabled only if the text in the editable filed of the
 * combo box contains a valid component name for the given type.
 * 
 * The name of the select component, can be retrieved by means of 
 * getSelectedComponentName().
 * 
 * When the user slects a component, the selector disappears and sends a message
 * to the listener 
 * 
 * Example of usage of this panel:
 * 1 create a dialog with the selector panel and a Cancel button
 * 2 the user selects a component from the drop down list, triggering a
 *   message to the listener 
 * 
 */
public class ComponentSelector extends JPanel implements ActionListener{
	/**
	 * The model for the ComboBox (to add/remove items)
	 */
	public class ComponentsComboBoxModel extends DefaultComboBoxModel {
		/**
		 * TODO - something here by acaproni; puttin this here to suppress warnings.
		 */
		public void fire() {
			fireContentsChanged(this,0,getSize()-1);
		}
	}
	
	//	 The wildcarded IDL (i.e. the type of the component) to select
	private String componentIDL;
	
	// The container services
	private ContainerServices acsCS;
	
	// The logger
	private Logger logger;
	
	// The widget with the list of the components
	private JComboBox componentsList=new JComboBox();
	
	// The button to refresh the list of components 
	// The components are read again from the container services
	// and filtered out with the filter
	private JButton applyFilterB = new JButton("Apply filter");
	
	/**
	 * The model with the content of the components combo box
	 */
	private ComponentsComboBoxModel model=new ComponentsComboBoxModel();
	
	//	The filter to apply to the name of the components
	private JTextField filterTF = new JTextField("*",10);
	
	// The listener to Connect events
	// A mesage is sent to this listener when the 
	// action is set to MGS_TO_LISTENER_ON_CONNECT
	private SelectorComponentListener selectorListener;
	
	/**
	 * Constructor of the selector with the default action (nothing) when 
	 * the user presses Connect.
	 * 
	 * @param componentType The type of the component with wildcards 
	 *                      (i.e.its IDL)
	 * @param cs The container Services
	 * @param listener object to be notified of changes to the component selector selections.
	 */
	public ComponentSelector(String componentType, ContainerServices cs, SelectorComponentListener listener) {
		super();
		if (cs==null) {
			throw new IllegalArgumentException("Illegal null ContainerServices in constructor");
		}
		if (componentType==null || componentType.length()==0) {
			throw new IllegalArgumentException("Illegal component type in constructor");
		}
		if (listener==null) {
			throw new IllegalArgumentException("Invalid null listener");
		}
		acsCS=cs;
		logger=acsCS.getLogger();
		componentIDL=componentType;
		selectorListener=listener;
		initialize();
	}
	
	/**
	 * Initialize the GUI
	 *
	 */
	private void initialize() {
		
		JPanel filterPanel = new JPanel();
		filterPanel.setBorder(new TitledBorder("Filter names by"));
		
        setLayout(new FlowLayout(FlowLayout.LEFT));
        applyFilterB.setToolTipText("Apply the filter to the list of the names of the components");
        filterPanel.add(applyFilterB);
        filterTF.setToolTipText("Wildcard filter");
        
        filterPanel.add(filterTF);
        add(filterPanel);
        
        JPanel connectPanel = new JPanel();
        connectPanel.setBorder(new TitledBorder("Select component name"));
        
        fillComponentsInCB();
        componentsList.setMaximumRowCount(6);
        componentsList.setEditable(false);
        componentsList.setModel(model);
        componentsList.setToolTipText("Select a component name");
        connectPanel.add(componentsList);
        add(connectPanel);
        
        // Setup the listeners
        applyFilterB.addActionListener(this);
        componentsList.addActionListener(this);
        JTextComponent editor = (JTextComponent)componentsList.getEditor().getEditorComponent();
        componentsList.getEditor().addActionListener(this);
	}
	
	/**
	 * Filters out of the vector the entries that do not match with the
	 * filter in the filterTF
	 */
	private void filterEntries(Vector<String> strs) {
		if (strs==null) {
			throw new IllegalArgumentException("Invalid null vector");
		}
		if (strs.isEmpty()) {
			return;
		}
		String filter = filterTF.getText();
		if (filter.length()==0) {
			filter="*";
			filterTF.setText(("*"));
			return;
		}
		// Transform the wildcard string in regular expression
		filter=filter.replace('?','.');
		filter=filter.replaceAll("\\*",".*");
		Iterator<String> iter = strs.iterator();
		while (iter.hasNext()) {
			String str = iter.next();
			try {
				if (!Pattern.matches(filter,str)) {
					iter.remove();
				} 
			} catch (Exception e) {
				// An error: probably the regular expression is wrong
				filter="*";
				filterTF.setText(("*"));
			}
		}
	}
	
	/**
	 * Get the components with the MOUNT_IDL interface
	 */
	private Vector<String> getComponentsNames() {
		Vector<String> ret = new Vector<String>();
		String[] comps=null;
		try {
			comps = acsCS.findComponents("*",componentIDL);
		} catch (Exception e) {
			logger.log(AcsLogLevel.ERROR,"Error getting components: "+e.getMessage());
		}
		for (int t=0; t<comps.length; t++) {
			ret.add(comps[t]);
		}
		Collections.sort(ret);
		return ret;
	}
	
	/**
	 * @see ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==applyFilterB) {
			// Filter button pressed
			fillComponentsInCB();
		} else if (e.getSource()==componentsList) {
			// The user selected an item from the list in the ComboBox
			Object selObj = componentsList.getSelectedItem();
			if (selObj==null) {
				// The filter chnaged the list and an event is generated
				// but the selected object is null
				return;
			}
			if (selObj.toString().trim().length()!=0) {
				executeConnectAction(selObj.toString().trim());
			}
		} else {
			System.err.println("Unknown event: Source: "+e.getSource()+"\nEvent: "+e);
		}
	}
	
	/**
	 * 
	 * @return The text in the combo box
	 *         null in case of error
	 */
	private String getCBContent() {
		String str=null;
		try {
			JTextComponent editor = (JTextComponent)componentsList.getEditor().getEditorComponent();
			str=editor.getDocument().getText(0,editor.getDocument().getLength());
		} catch (Exception ex ) {
			System.err.println(ex);
		}
		return str;
	}
	
	/**
	 * Add the names of the components in the ComboBox
	 *
	 */
	private void fillComponentsInCB() {
		Vector<String> comps = getComponentsNames();
		filterEntries(comps);
		model.removeAllElements();
		model.addElement("");
		for (String str: comps) {
			model.addElement(str);
		}
		model.fire();
	}
	
	/**
	 * Execute the specified action 
	 *
	 */
	private void executeConnectAction(String compName) {
		if (compName==null || compName.length()==0) {
			throw new IllegalStateException("Performing an action without a selected component");
		}
		setVisible(false);
		SelectorComponentEvent sce = new SelectorComponentEvent(this,compName); 
		selectorListener.connectPerformed(sce);
	}
	
}
