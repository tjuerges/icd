/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2007
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

/** 
 * @author  caproni   
 * @version $Id$
 * @since    
 */

package alma.common.gui.components.selector;

import java.util.EventObject;

/**
 * The event generated by the selector component when the user
 * presses Connect
 *
 */
public class SelectorComponentEvent extends EventObject {
	
	// The selected name of the component
	private String componentName;
	
	/**
	 * Constructor 
	 * 
	 * @param obj The ComponentSelector that generated this event
	 * @param name The name of the component to connect to
	 */
	public SelectorComponentEvent(Object obj, String name) {
		super(obj);
		if (name==null || name.length()==0) {
			throw new IllegalArgumentException("Invalid component name");
		}
		componentName=name;
	}
	
	/**
	 * 
	 * @return The name of the component
	 */
	public String getComponentName() {
		return componentName;
	}
}
