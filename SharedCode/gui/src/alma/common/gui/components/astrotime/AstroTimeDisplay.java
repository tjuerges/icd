/*
 * Created on Sep 18, 2007 by mschilli
 */
package alma.common.gui.components.astrotime;

import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Panel that displays the current Time,
 * in LST (Local Sidereal Time) and UTC for a selected Location.
 * <p>
 * This class does not contain a thread itself. To make the
 * time display progress, users of this class need to implement
 * a mechanism to periodically call {@link #refresh()}. A simple
 * way to achieve this is the following:
 * 
   <pre>
   java.util.Timer t = new java.util.Timer("astro clock", true);
   t.schedule (
      new java.util.TimerTask() {
         public void run () { astroTimeDisplay.refresh(); }
      }
      , 1000, 1000);
   </pre>
 * 
 * <p>
 * Until ALMA R7.1 (April 2010), this class used to provide a drop down
 * list to the user from which they could choose different locations
 * (ATF, OSF). Meanwhile, this feature was removed. If it is ever desired
 * to add it again, please refer to the CVS history of this class.
 * </p>
 * 
 * @author mschilli
 */
public class AstroTimeDisplay extends JPanel {

	protected JLabel lblUTC;
	protected JLabel lblLST;
	protected Location chajnantor;

	public AstroTimeDisplay() {

		this.lblUTC = new JLabel();
		this.lblLST = new JLabel();

		
		// predefined locations
		// ---------------------

		/* this.cmbLocation.addItem(new Location("ATF", 107.61778)); --> ATF is closed <-- */
		chajnantor = new Location("Chaj", 67.75917);

		
		// layout
		// -------
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.add(lblUTC);
		this.add(Box.createHorizontalStrut(8));
		this.add(lblLST);

		
		// initial update
		// ---------------
		refresh();
	}


	public void refresh () {
		Date now = new Date();
		String utc = AstroTime.toSexagesimal(AstroTime.getUTC(now));

		Location loc = chajnantor;
		String lst = AstroTime.toSexagesimal(AstroTime.getLST(now, loc.longitude));

		lblUTC.setText("UT:" + utc);
		lblLST.setText("LST:" + lst);
	}


	/**
	 * Little struct to describe a Location, in earlier versions
	 * there were several ones of these fed into a ComboBox.
	 */
	private class Location {

		String name;
		double longitude;

		Location(String name, double longitude) {
			this.name = name;
			this.longitude = longitude;
		}

		@Override
		public String toString () {
			return name;
		}

	}


	// Testing
	// ===================================================================
	
	public static void main (String[] args) {
		AstroTimeDisplay inst = new AstroTimeDisplay();
		
		javax.swing.JFrame f = new javax.swing.JFrame();
		f.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(inst);
		f.pack();
		f.setVisible(true);
	}
}
