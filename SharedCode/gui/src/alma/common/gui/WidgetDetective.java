package alma.common.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;


/**
 * This is a debugging helper, it allows to examine the structure of a Swing UI
 * while an application is running. The intended usage is:
 * 
 * <pre>
 *  JFrame f = ...
 *  WidgetDetective det = new WidgetDetective(f);
 * 
 *  Action a = new AbstractAction() {
 *     public void actionPerformed (ActionEvent e) {
 *        det.on();
 *     }
 *  };
 * </pre>
 * 
 * @author mschilli (originally for EXEC)
 */
public class WidgetDetective {

	/**
	 * Creates an instance for a given specified top-level container. It is possible to
	 * pass in (and thus to analyse):
	 * <ul>
	 * <li> JFrame
	 * <li> JDialog
	 * <li> JInternalFrame
	 * <li> JWindow
	 * <li> JApplet
	 * </ul>
	 */
	public WidgetDetective(RootPaneContainer frame) {
		this.frame = frame;
	}

	protected RootPaneContainer frame;

	protected Component glassPane;
	protected Container contentPane;
	protected MouseListener mouseLi;

	protected Color savedbackground;
	protected boolean savedOpacity;
	protected Component currentComp;


	/**
	 */
	public boolean isOn () {
		return (mouseLi != null);
	}

	/**
	 * Activate. The user can turn off by right-clicking.
	 */
	public void on () {

		glassPane = frame.getGlassPane();
		contentPane = frame.getContentPane();
		mouseLi = new MouseAdapter() {

			@Override
			public void mousePressed (MouseEvent e) {

				if (SwingUtilities.isRightMouseButton(e)) {
					off();

				} else {
					// code from: comp.lang.java.gui, 2003-01-22
					Point glassPanePoint = e.getPoint();
					Point containerPoint = SwingUtilities.convertPoint(glassPane, glassPanePoint, contentPane);
					currentComp = (Component) SwingUtilities.getDeepestComponentAt(contentPane, containerPoint.x, containerPoint.y);

					Component c = currentComp;
					StringBuilder msg = new StringBuilder("Widget Detective:");
					StringBuilder indent = new StringBuilder("\n");
					while (c != null) {
						msg.append(indent);
						msg.append(c.getClass().getSimpleName());
						msg.append("@" + c.hashCode());
						msg.append(",name='" + c.getName() + "'");
						msg.append(" || More details: " + c);
						c = c.getParent();
						indent.append(" ");
					}
					System.out.println(msg.toString());

					savedOpacity = currentComp.isOpaque();
					if (currentComp instanceof JComponent) {
						((JComponent) currentComp).setOpaque(true);
					}
					savedbackground = currentComp.getBackground();
					currentComp.setBackground(Color.red);
				}
			}

			@Override
			public void mouseReleased (MouseEvent e) {
				if (currentComp instanceof JComponent) {
					((JComponent) currentComp).setOpaque(savedOpacity);
				}
				currentComp.setBackground(savedbackground);
			}
		};
		
		
		glassPane.addMouseListener(mouseLi);
		say("Widget Detective activated. Turn off with right-click.");
		glassPane.setVisible(true);
	}

	/**
	 * Deactivate. This is usually invoked from within this class, so needs not be called
	 * programmatically.
	 */
	public void off () {
		glassPane.removeMouseListener(mouseLi);
		glassPane.setVisible(false);
		mouseLi = null;
		say("Widget Detective deactivated");
	}

	protected void say (String msg) {
		System.out.println(msg);
		JOptionPane.showMessageDialog((Component) frame, msg);
	}

}
