package alma.common.gui.chessboard;

import java.util.ArrayList;

/**
 * An adapter class (using Swing terminology such as MouseAdapter, not gang of four terminology for 
 * the adapter pattern) which provides default implementations for the methods needed to publish
 * status changes to <code>ChessboardStatusListener</code> objects. This is useful when using a
 * <code>ChessboardFrame</code> or <code>ChessboardPanelWithButtons</code> for example, because these
 * objects implement the <code>ChessboardStatusListener</code> interface and can be notified of
 * changes provided that the proper registration tying the listener and subject have been done.
 * For an example of tying a listener and subject together, see the <code>ChessboardTest</code>
 * test case.
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardStatusListener
 * @see ChessboardStatusEvent
 * @see ChessboardFrame
 * @see ChessboardPanelWithButtons
 */
public abstract class ChessboardStatusAdapter implements ChessboardStatusProvider
{
	/* (non-javadoc)
	 * @see alma.common.gui.chessboard.ChessboardStatusProvider#addChessboardDataListener(alma.common.gui.chessboard.ChessboardStatusListener)
	 */
	public void addChessboardStatusListener(ChessboardStatusListener listener) 
	{
		listeners.add(listener);
	}

	/* (non-javadoc)
	 * @see alma.common.gui.chessboard.ChessboardStatusProvider#removeChessboardDataListener(alma.common.gui.chessboard.ChessboardStatusListener)
	 */
	public void removeChessboardStatusListener(ChessboardStatusListener listener) 
	{
		listeners.remove(listener);
	}

	/**
	 * This method is not formally part of the <code>ChessboardStatusProvider</code> interface, but
	 * is implemented here for convenience for those who choose to use this class rather than
	 * writing their own implementation of the <code>ChessboardStatusProvider</code> interface.
	 * It's made public, because some may choose to use delegation rather than inheritance to
	 * reuse this class; logically, it really should be a private method (i.e. if one were
	 * to implement the <code>ChessboardStatusListener</code> interface directly then one would
	 * probably have a method - similar to this one (could be named something different) - 
	 * which was private and used only by the class internally. Because this class is potentially
	 * to be either subclassed or delegated to, this method is provided for convenience and is public.
	 * 
	 * @param evt the event to push to all the listeners that are registered.
	 */
	public void notifyListeners(ChessboardStatusEvent evt) 
	{
		if(null != listeners)
		{
			for(ChessboardStatusListener listener: listeners)
			{
				listener.processStatusChange(evt);
			}
		}
	}
	
	private ArrayList<ChessboardStatusListener> listeners = new ArrayList<ChessboardStatusListener>();
}



