package alma.common.gui.chessboard.internals;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;

import alma.common.gui.chessboard.ChessboardEntry;

/**
 * Custom table cell renderer for drawing flashing cells in a chessboard; extends
 * <code>DefaultTableCellRenderer</code>. <br><br>
 *
 * NOTE: most people should never be concerned
 * with this class as it is used internally by the Chessboard and should be transparent
 * to the end user.
 * 
 * @author Steve Harrington
 *
 * @see ChessboardTable
 * @see ChessboardTableFlashingCellRenderer
 * @see DefaultTableCellRenderer
 */
public class ChessboardTableFlashingCellRenderer extends DefaultTableCellRenderer
{
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;
	
	private static Timer singletonTimerInstance = null;
	private static Vector<ChessboardTableFlashingCellRenderer> flashingCells = 
		new Vector<ChessboardTableFlashingCellRenderer>();
	
	private ChessboardEntry myEntry;
	private ChessboardTable myTable = null;
	private ChessboardTableModel myModel = null;
	private final Color defaultBgColor = getBackground();
	private final Color defaultFgColor = getForeground();
	private Color bgFlashColor = null;
	private Color fgFlashColor = null;

	/**
	 * Constructor for a flashing cell renderer with the default (red) as the flashing color.
	 * 
	 * @param owningTable the table associated with this renderer.
	 * @param owningModel the table model associated with this renderer.
	 */
	public ChessboardTableFlashingCellRenderer(ChessboardTable owningTable, ChessboardTableModel owningModel)
	{
		this(owningTable, owningModel, Color.RED, Color.WHITE);
	}
	
	/**
	 * Constructor for a flashing cell renderer with a user-specified flashing color.
	 * 
	 * @param owningTable the table associated with this renderer.
	 * @param owningModel the table model associated with this renderer.
	 * @param bgColorToFlash the background color to use for the flashing cell.
	 * @param fgColorToFlash the foreground color to use for the flashing cell.
	 */
	public ChessboardTableFlashingCellRenderer(ChessboardTable owningTable, ChessboardTableModel owningModel, Color bgColorToFlash, Color fgColorToFlash)
	{
		myTable = owningTable;
		myModel = owningModel;
		bgFlashColor = bgColorToFlash;
		fgFlashColor = fgColorToFlash;
		initFlashing();
	}
	
	/**
	 * Overriding <code>DefaultTableCellRenderer</code> method to get tool tip text.
	 */
	@Override
	public void setValue(Object value)
	{
		if(value instanceof ChessboardEntry)
		{
			myEntry = (ChessboardEntry) value;
			super.setValue(myEntry.getDisplayName());
			this.setToolTipText(myEntry.getToolTipText());
		}
	}
	
	/**
	 * Overriding <code>DefaultTableCellRenderer</code> method for two reasons:
	 * 1) to provide custom rendering of borders.
	 * 2) to properly handle cells that are selected/deselected.
	 *
	 * @param table  the <code>JTable</code>
	 * @param value  the value to assign to the cell at
	 *			<code>[row, column]</code>
	 * @param isSelected true if cell is selected
	 * @param hasFocus true if cell has focus
	 * @param row  the row of the cell to render
	 * @param column the column of the cell to render
	 * @return the default table cell renderer
	 * 
	 * @see DefaultTableCellRenderer
	 */ 
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) 
	{
		// Call the superclass method, but pass in false for the 'isSelected' parameter. This will
		// ensure that we never have (flashing) cells drawn in the 'selected' color of the table
		// which would prevent proper flashing. 
		Component retVal = super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
		
		// Draw a border around the cell if it is selected.
		if(isSelected)
		{
			Border border = null;
			if (isSelected) {
				border = UIManager.getBorder("Table.focusSelectedCellHighlightBorder");
			}
			if (border == null) {
				border = UIManager.getBorder("Table.focusCellHighlightBorder");
			}
			retVal.setBackground(table.getSelectionBackground());
			retVal.setForeground(table.getSelectionForeground());
			setBorder(border);
		}
		else
		{
			setBorder(null);
		}
		return retVal;
	}	
	
	/**
	 * Private (static) method called by a timer-run thread which periodically 
	 * flashes all the cells that are in the flashing state.
	 */
	private static void flashCells()
	{	
		ChessboardTableFlashingCellRenderer.stopTimerIfEmpty();
		
		Enumeration<ChessboardTableFlashingCellRenderer> flashersEnum 
			= ChessboardTableFlashingCellRenderer.flashingCells.elements();
		
		ArrayList<ChessboardTableFlashingCellRenderer> itemsToRemove = null;
		
		//System.out.println("flashing cells ********************");
		while(flashersEnum.hasMoreElements())
		{	
			//System.out.println("flashing cells list not empty");
			ChessboardTableFlashingCellRenderer currentFlasher = flashersEnum.nextElement();
			
			if(currentFlasher.myEntry.shouldFlash() != true)
			{
				if(null == itemsToRemove)
				{
					itemsToRemove = new ArrayList<ChessboardTableFlashingCellRenderer>();
				}
				itemsToRemove.add(currentFlasher);
			}
			else 
			{
				currentFlasher.flash();
			}	
		}
		if(null != itemsToRemove)
		{
			//System.out.println("removing some (flashing) cells");
			Iterator<ChessboardTableFlashingCellRenderer> iter = itemsToRemove.iterator();
			while(iter.hasNext())
			{
				ChessboardTableFlashingCellRenderer rendererToRemove = iter.next();
				ChessboardTableFlashingCellRenderer.flashingCells.remove(rendererToRemove);
				rendererToRemove.myTable.removeFlashingRenderer(rendererToRemove.myModel.getRowForEntry(rendererToRemove.myEntry), 
						rendererToRemove.myModel.getColumnForEntry(rendererToRemove.myEntry));
			}
		}
	}

	/**
	 * Private (static) method to stop the timer which is used to flash the cells 
	 * (if there are no cells to flash at present).
	 */
	private synchronized static void stopTimerIfEmpty() {
		if(ChessboardTableFlashingCellRenderer.flashingCells.isEmpty() &&
			ChessboardTableFlashingCellRenderer.singletonTimerInstance != null)
		{
			ChessboardTableFlashingCellRenderer.singletonTimerInstance.stop();
			ChessboardTableFlashingCellRenderer.singletonTimerInstance = null;
		}
	}
	
	/**
	 * Private method to flash an individual cell.
	 */
	private void flash()
	{
		Color currentBgColor = getBackground();
		
		if(currentBgColor != bgFlashColor)
		{
			//System.out.println("Setting bg to red.");
			setBackground(bgFlashColor);
			setForeground(fgFlashColor);
		}
		else
		{
			//System.out.println("resetting bg to default.");
			setBackground(defaultBgColor);
			setForeground(defaultFgColor);
		}

		Integer column = myModel.getColumnForEntry(myEntry);
		Integer row = myModel.getRowForEntry(myEntry);
		//System.out.println("flashing cell for row: " + row + " and column: " + column);
		
		TableModelEvent event = new TableModelEvent(myModel, row, row, column);
		myTable.tableChanged(event);
	}
	
	/**
	 * Private method which adds us to the list of flashing cells,
	 * then initializes the flashing timer, if necessary.
	 */
	private void initFlashing() 
	{
		//String name = (myEntry == null)? "null" : myEntry.getDisplayName();
		//System.out.println("initFlashing called; adding renderer to flashers list for: " + name);
		ChessboardTableFlashingCellRenderer.flashingCells.add(this);
		startTimer();
	}
	
	/**
	 * Private method to start up the timer which will 'go off' periodically and
	 * flash the cells that are in the flashing state.
	 */
	synchronized private void startTimer()
	{
		// See if the singleton needs to be instantiated 
		if(null == ChessboardTableFlashingCellRenderer.singletonTimerInstance)
		{
			//System.out.println("starting the timer thread for flashing !!!!!!!!!!!!!!!!!!!!!!");
			int delay = 500; //milliseconds
			ActionListener taskPerformer = new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{
					ChessboardTableFlashingCellRenderer.flashCells();
				}
			};
			ChessboardTableFlashingCellRenderer.singletonTimerInstance = new Timer(delay, taskPerformer);
			ChessboardTableFlashingCellRenderer.singletonTimerInstance.setRepeats(true);
			ChessboardTableFlashingCellRenderer.singletonTimerInstance.start();
		}
	}
}


