package alma.common.gui.chessboard.internals;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardPanel;
import alma.common.gui.chessboard.ChessboardPanelWithButtons;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusListener;
import alma.common.gui.chessboard.ChessboardStatusProvider;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;

/**
 * The <code>ChessboardFrame</code> is a frame with a chessboard in it, for use
 * when top-level display of a chessboard is needed. Implements ChessboardStatusListener
 * in order to be notified of status changes and update the UI accordingly.<br><br>
 * 
 * NOTE: It is required that the names of the entries in a chessboard are unique (within
 * that chessboard).
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardEntry
 * @see ChessboardStatusListener
 * @see JFrame
 */
public class ChessboardFrame extends JFrame implements ChessboardStatusListener
{
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;
	private ChessboardPanelBase panel = null;

	/**
	 * Constructor to create a chessboard frame with the given title, using the provided ChessboardStatusProvider to get
	 * the initial state.
	 * 
	 * @param title the title of the chessboard
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param statusProvider provider to use to get the initial chessboard state and also to register with to 
	 * receive updates on status changes. NOTE: NULL is not allowed for this parameter and will result in an exception.
	 * @param allowMultiCellSelection indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param useButtons whether the chessboard will have buttons for 'new' and 'replace' to launch details panels.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardFrame(String title, ChessboardDetailsPluginFactory detailsProvider, ChessboardStatusProvider statusProvider, 
			boolean allowMultiCellSelection, boolean useButtons, PluginContainerServices containerServices)
	{
		super(title);

		if(useButtons) {
			// Create and set up the content pane.
	        panel = new ChessboardPanelWithButtons(allowMultiCellSelection, 
	        				detailsProvider, statusProvider, containerServices);
		} else {
			panel = new ChessboardPanel(allowMultiCellSelection, 
						detailsProvider, statusProvider, containerServices);
		}
        initialize();
	}
	
	/**
	 * Constructor to create a chessboard frame with the given title, using the provided ChessboardStatusProvider to get
	 * the initial state. <br><br>
	 * 
	 * NOTE: using this constructor creates a chessboard frame which does NOT allow multi-cell-selection and DOES use buttons;
	 * if this is not desired, use one of the other constructors. 
	 * 
	 * @param title the title of the chessboard
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param statusProvider provider to use to get the initial chessboard state and also to register with to 
	 * receive updates on status changes. NOTE: NULL is not allowed for this parameter and will result in an exception.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardFrame(String title, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices)
	{
		this(title, detailsProvider, statusProvider, false, true, containerServices);
	}
	
	/**
	 * Constructor to create a chessboard with the given title and the given initial data. <br><br>
	 * 
	 * NOTE: using this constructor (and all the other constructors which take ChessboardEntry[][] arrays) one must register this class with
	 * any ChessboardStatusProvider's that you wish to listen to, manually. The constructors which take a 
	 * <code>ChessboardStatusProvider</code> do this connection for you, automatically. 
	 * 
	 * @param title the title of the chessboard
	 * @param initialData the initial data with which to populate the chessboard.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param allowMultiCellSelection indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param useButtons whether to have buttons on the frame or not (e.g. for 'new' and 'replace' - launching
	 * of details panels).
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu a popu menu to add to a right click context menu.
	 */
	public ChessboardFrame(String title, ChessboardEntry[][] initialData, ChessboardDetailsPluginFactory detailsProvider, 
			boolean allowMultiCellSelection, boolean useButtons, PluginContainerServices containerServices, JPopupMenu popupMenu)
	{
		super(title);
		if(useButtons) {
			// Create and set up the content pane.
	        panel = new ChessboardPanelWithButtons(initialData, allowMultiCellSelection, detailsProvider, containerServices, popupMenu);
		} else {
			panel = new ChessboardPanel(initialData, allowMultiCellSelection, detailsProvider, containerServices, popupMenu);
		}
        initialize();
	}

	/**
	 * Constructor to create a chessboard with the given title and the given initial data. <br><br>
	 * 
	 * NOTE: using this constructor (and all the other constructors which take ChessboardEntry[][] arrays) one must register this class with
	 * any ChessboardStatusProvider's that you wish to listen to, manually. The constructors which take a 
	 * <code>ChessboardStatusProvider</code> do this connection for you, automatically. 
	 * 
	 * @param title the title of the chessboard
	 * @param initialData the initial data with which to populate the chessboard.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param allowMultiCellSelection indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param useButtons whether to have buttons on the frame or not (e.g. for 'new' and 'replace' - launching
	 * of details panels).
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardFrame(String title, ChessboardEntry[][] initialData, ChessboardDetailsPluginFactory detailsProvider, 
			boolean allowMultiCellSelection, boolean useButtons, PluginContainerServices containerServices)
	{
		this(title, initialData, detailsProvider, allowMultiCellSelection, useButtons, containerServices, null);
	}
	
	/**
	 * Alternate constructor using a 1-dimensional array for the initial data, used 
	 * to create a chessboard with the given title and the given initial data. NOTE: using this constructor
	 * (and all the other constructors which take ChessboardEntry[][] arrays) one must register this class with
	 * any ChessboardStatusProvider's that you wish to listen to, manually. The constructors which take a 
	 * <code>ChessboardStatusProvider</code> do this connection for you, automatically. 
	 * 
	 * @param title the title of the chessboard
	 * @param initialData the initial data with which to populate the chessboard.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param allowMultiCellSelection indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param useButtons boolean indicating whether to place 'new' and 'replace' buttons on the chessboard (true)
	 * or not (false).
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardFrame(String title, ChessboardEntry[] initialData, ChessboardDetailsPluginFactory detailsProvider, 
			boolean allowMultiCellSelection, boolean useButtons, PluginContainerServices containerServices)
	{
		super(title);
		if(useButtons) {
			// Create and set up the content pane.
	        panel = new ChessboardPanelWithButtons(initialData, allowMultiCellSelection, detailsProvider, containerServices);
		} else {
			panel = new ChessboardPanel(initialData, allowMultiCellSelection, detailsProvider, containerServices);
		}
        initialize();
	}
	
	/**
	 * Alternate constructor using a 1-dimensional array for the initial data, used 
	 * to create a chessboard with the given title and the given initial data. NOTE: using this constructor
	 * (and all the other constructors which take ChessboardEntry[][] arrays) one must register this class with
	 * any ChessboardStatusProvider's that you wish to listen to, manually. The constructors which take a 
	 * <code>ChessboardStatusProvider</code> do this connection for you, automatically. 
	 * 
	 * @param title the title of the chessboard
	 * @param initialData the initial data with which to populate the chessboard.
	 * @param rows the number of desired rows in the chessboard.
	 * @param columns the number of desired columns in the chessboard.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param allowMultiCellSelection indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardFrame(String title, ChessboardEntry[] initialData, int rows, int columns, 
			ChessboardDetailsPluginFactory detailsProvider, 
			boolean allowMultiCellSelection, PluginContainerServices containerServices)
	{
		super(title);
		// Create and set up the content pane.
        panel = new ChessboardPanelWithButtons(initialData, rows, columns, allowMultiCellSelection, 
        			detailsProvider, containerServices);
        initialize();
	}

	/**
	 * Required method of the ChessboardStatusListener interface; used to
	 * communicate status changes from the 'outside world' to the GUI,
	 * so that the GUI can be updated to reflect the changes.
	 * 
	 * @param event the event containing the new status information.
	 * @see ChessboardStatusEvent
	 */
	public void processStatusChange(ChessboardStatusEvent event) 
	{
		panel.processStatusChange(event);
	}
	
	/**
	 * Used to set the plugin container services, which are made available to the details panels
	 * launched from the chessboard. This method is useful if it wasn't possible to pass the plugin
	 * container services in at the time of construction (i.e. in the constructor) and null was 
	 * passed to the constructor for example. This method merely delegates on to the 
	 * <code>ChessboardPanelBase<code> class method of the same name.
	 * 
	 * @param containerServices the container services which will be available to the details panels
	 * that are launched from the chessboard.
	 */
	public void setPluginContainerServices(PluginContainerServices containerServices)
	{
		panel.setPluginContainerServices(containerServices);
	}
	
	/**
	 * Enables or disables the ability to launch details displays (e.g. panels) when the user double-clicks 
	 * cells in the chessboard. This is intended to be used in situations such as Scheduling,
	 * when details panels are not appropriate and/or desired. This method just delegates through to
	 * the <code>ChessboardPanelBase</code> method of the same name.
	 * 
	 * @param enabled boolean indicating whether details display should be enabled (true) or not (false).
	 */
	public void setDetailsDisplayEnabled(boolean enabled)
	{
		panel.setDetailsDisplayEnabled(enabled);
	}
	
	/**
	 * Gets an array of <code>ChessboardEntry</code> objects for each selected entry in the chessboard 
	 * or null if there are no cells selected. This is a facade that merely passes through to the underlying
	 * ChessboardTable method of the same name.
	 * 
	 * @return array containing the selected entries or null if none selected.
	 */
	public ChessboardEntry[] getSelectedEntries()
	{
		ChessboardEntry[] retVal = panel.getSelectedEntries();
		return retVal;
	}
	
	/**
	 * Initializes the <code>ChessboardFrame</code>
	 *
	 */
	private void initialize() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);   
        setContentPane(panel);
        panel.setOpaque(true); //content panes must be opaque
        
        // Set the frame's icon to an image loaded from a file.
        setIconImage(new ImageIcon("alma/common.gui/chessboard/images/2x2chessboard.png").getImage());
	}
}



