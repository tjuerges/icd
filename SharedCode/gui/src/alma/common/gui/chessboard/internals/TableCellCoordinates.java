package alma.common.gui.chessboard.internals;

/**
 * Utility class used to store both a row and a column as a single object in various contexts.
 * Can be used as an index (i.e. as a 'key') in a HashMap. For example, the HashMap 
 * (flashingCellRendererMap declared in <code>ChessboardTable</code>) exists so that we can 
 * lookup and see if we've already instantiated a flashing cell renderer for a 
 * given cell and, if so, reuse it rather than instantiating a new one (when a cell has 
 * transitioned to a flashing state).
 */
public class TableCellCoordinates 
{
	// These are stored as Integer objects rather than int primitives
	// to facilitate implementation of the hashcode method.
    private Integer row, column;
    
    /**
	 * Constructor
	 * 
	 * @param rowNum the row number of the cell 
	 * @param colNum the column number of the cell 
	 */
	public TableCellCoordinates(int rowNum, int colNum)
	{
		this.row = new Integer(rowNum);
		this.column = new Integer(colNum);
	}
	
	/**
	 * As this class may be used in a HashMap, we need to define equals and hashCode methods.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		boolean retVal = false;
		if(obj instanceof TableCellCoordinates)
		{
			TableCellCoordinates value = (TableCellCoordinates) obj;
			if(value.row.equals(this.row) && value.column.equals(this.column))
			{
				retVal = true;
			}
		}
		return retVal;
	}
	
	/**
	 * As this is class may be used in a HashMap, we need to define equals and hashCode methods.
	 */
	@Override
	public int hashCode()
	{
		int hash = 7;
		int retVal = 31 * 7 + column.intValue();
		retVal = 31 * hash + row.hashCode();
		return retVal;
	}
			
	
	/**
	 * Getter for the row value.
	 * @return the number of the row for this cell.
	 */
	public int getRow(){
        return row.intValue();
    }
	
	/**
	 * Getter for the column value.
	 * @return the number of the column for this cell.
	 */
    public int getCol(){
        return column.intValue();
    }
}