package alma.common.gui.chessboard.internals;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardPanel;
import alma.common.gui.chessboard.ChessboardPanelWithButtons;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusListener;
import alma.common.gui.chessboard.ChessboardStatusProvider;
import alma.common.gui.chessboard.ChessboardUtilities;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Abstract base class for chessboard panels, containing logic common to all chessboard panels.
 * 
 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
 * 
 * @author Steve Harrington
 * @see ChessboardPanelWithButtons
 * @see ChessboardPanel
 */
public abstract class ChessboardPanelBase extends JPanel implements ChessboardStatusListener
{
	/**
	 * Constant which defines the minimum height of a chessboard.
	 */
	public static final int MIN_HEIGHT = 450;
	
	/**
	 * Constant which defines the minimum width of a chessboard.
	 */
	public static final int MIN_WIDTH = MIN_HEIGHT;
	
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
	protected ChessboardTable table = null;
	protected ChessboardTableModel tableModel = null;  
	
	/**
	 * The (preferred) constructor to create a <code>ChessboardPanelWithButtons</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu a popup menu be added to a right click context menu.
	 */
	public ChessboardPanelBase(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices,
			JPopupMenu popupMenu) 
	{
		// NOTE: this will throw a NullPointerException if statusProvider is NULL, a value which is not allowed.
		this(statusProvider.getStates(), allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
		
		// connect the listener and the publisher so that status changes are handled properly
		statusProvider.addChessboardStatusListener(this);
	}
	
	/**
	 * The (preferred) constructor to create a <code>ChessboardPanelWithButtons</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelBase(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices) 
	{
		this(allowMultiCellSelect, detailsProvider, statusProvider, containerServices, null);
	}
	
	/**
	 * The constructor to create a <code>ChessboardPanelWithButtons</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu a popup menu to be added to a right-click context menu.
	 */
	public ChessboardPanelBase(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices,
			JPopupMenu popupMenu) 
	{
		initialize(initialData, allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
	}
	
	/**
	 * The constructor to create a <code>ChessboardPanelWithButtons</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelBase(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices) 
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}

	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu a popup menu to add to a right-click context menu.
	 */
	public ChessboardPanelBase(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices, JPopupMenu popupMenu)
	{
		ChessboardEntry[][] twoDimensionalData = 
			ChessboardUtilities.convertOneDimensionalToTwoDimensional(initialData);
		
		// normal initialization method called with 2-D array
		initialize(twoDimensionalData, allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelBase(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}
	
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows in the chessboard.
	 * @param columns the number of columns in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu popup menu to add to a right-click context menu.
	 */
	public ChessboardPanelBase(ChessboardEntry[] initialData, int rows, int columns, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices,
			JPopupMenu popupMenu)
	{
		// convert the 1-D array to 2-D and then proceed as usual
		ChessboardEntry[][] twoDimensionalData = 
			ChessboardUtilities.convertOneDimensionalToTwoDimensional(initialData, rows, columns);
		
		// normal initialization method called with 2-D array
		initialize(twoDimensionalData, allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 *
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows in the chessboard.
	 * @param columns the number of columns in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelBase(ChessboardEntry[] initialData, int rows, int columns, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, rows, columns, allowMultiCellSelect, detailsProvider, containerServices, null);
	}

	/**
	 * Required method of the <code>ChessboardStatusListener</code> interface; used
	 * to communicate changes in status to this <code>ChessboardPanelWithButtons</code> so that
	 * it remains up to date as changes occur. Note that the connection between the provider  
	 * (i.e. publisher) and the listener is done automatically by the constructor for
	 * the chessboard panel which takes a <code>ChessboardStatusProvider</code> as one
	 * of its arguments. If you use one of the other constructors, which does not take
	 * a <code>ChessboardStatusProvider</code> as an argument, the connection between
	 * publisher and subscriber is your responsibility; i.e. somewhere you'll need
	 * to call the <code>ChessboardStatusProvider.addChessboardDataListener</code> method
	 * by hand.
	 * 
	 * @param event the event which contains the updated status information.
	 * @see ChessboardStatusEvent
	 */
	public void processStatusChange(ChessboardStatusEvent event) 
	{
		tableModel.processStatusChange(event);
	}

	/**
	 * Returns an array of <code>ChessboardEntry</code> objects which represent the items in 
	 * the chessboard that are currently selected.
	 * 
	 * @return an array of <code>ChessboardEntry</code> objects representing the entries 
	 * in the <code>ChessboardPanelWithButtons</code> (and its underlying <code>ChessboardTable</code>)
	 * that are currently selected.
	 */
	public ChessboardEntry[] getSelectedEntries()
	{
		return table.getSelectedEntries();
	}
	
	/**
	 * Used to set the plugin container services, which are made available to the details panels
	 * launched from the chessboard. This method is useful if it wasn't possible to pass the plugin
	 * container services in at the time of construction (i.e. in the constructor) and null was 
	 * passed to the constructor for example. This method merely delegates on to the 
	 * <code>ChessboardTable<code> class method of the same name.
	 * 
	 * @param containerServices the container services which will be available to the details panels
	 * that are launched from the chessboard.
	 */
	public void setPluginContainerServices(PluginContainerServices containerServices)
	{
		table.setPluginContainerServices(containerServices);
	}
	
	/**
	 * Enables or disables the ability to launch details displays (e.g. panels) when the user double-clicks 
	 * cells in the chessboard. This is intended to be used in situations such as Scheduling,
	 * when details panels are not appropriate and/or desired. This method just delegates through to
	 * the <code>ChessboardTable</code> method of the same name.
	 * 
	 * @param enabled boolean indicating whether details display should be enabled (true) or not (false).
	 */
	public void setDetailsDisplayEnabled(boolean enabled)
	{
		table.setDetailsDisplayEnabled(enabled);
	}
	
	/** 
	 * Used to enable or disable the chessboard.
	 * 
	 * @param enabled boolean indicating whether to disable (e.g. disable selections, details panels, etc.) or enable the chessboard.
	 */
	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		if(null != table) {
			table.setEnabled(enabled);
			table.setCellSelectionEnabled(false);
			table.setRowSelectionAllowed(false);
			table.setColumnSelectionAllowed(false);
		}
	}
	
	/**
	 * Returns the number of cells in the chessboard.
	 * 
	 * @return the total number of cells in the chessboard.
	 */
	public int getNumberOfChessboardCells()
	{
		int retVal = 0;
		int rows = this.table.getRowCount();
		int cols = this.table.getColumnCount();
		retVal = rows * cols;
		return retVal;
	}
	

	/**
	 * This method initializes this object, the details of which should be uninteresting to 
	 * outside parties.
	 */
	abstract protected void initialize(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			final ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices, JPopupMenu menu); 
}
