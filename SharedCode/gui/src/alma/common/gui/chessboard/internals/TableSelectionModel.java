package alma.common.gui.chessboard.internals;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import alma.common.gui.chessboard.ChessboardEntry;

/**
 * This class is used to support a multi-cell 
 * selection mechanism (with non-contiguous cells)
 * within JTable. It was originally authored by 
 * Jan-Friedrich Mutter (jmutter@bigfoot.de) but
 * adapted for use by ALMA.
 * 
 * This class doesn't change the L&F of the JTable but
 * listens to mouseclicks and updates the TableSelectionModel.
 * 
 * @author Jan-Friedrich Mutter (jmutter@bigfoot.de) - original author 
 * @author Steve Harrington - adaptations for use by ALMA
 * 
 * @see MultiCellSelectionTableUI
 * @see TableSelectionListener
 * @see TableSelectionEvent
 */
public class TableSelectionModel implements PropertyChangeListener,
		ListSelectionListener, TableModelListener 
{
	/** List of Listeners which will be notified when the selection value changes */
	protected EventListenerList listenerList = new EventListenerList();
	
	/** contains a ListSelectionModel for each column */
	protected ArrayList<ListSelectionModel> listSelectionModels = new ArrayList<ListSelectionModel>();

	/**
	 * Constructor.
	 */
	public TableSelectionModel() {
	}

	
	/**
	 * Getter for the selected items.
	 * @param tableModel the model for the table.
	 * @return an array of the selected items.
	 */
	public Object[] getSelectedItems(TableModel tableModel)
	{
		ArrayList<ChessboardEntry> retList = new ArrayList<ChessboardEntry>();
		Iterator<ListSelectionModel> selectionModelsIter = listSelectionModels.iterator();
		int currentColumnIndex = 0;
		while(selectionModelsIter.hasNext())
		{
			ListSelectionModel selModel = selectionModelsIter.next();
			// Get the min and max ranges of selected cells
			int rowIndexStart = selModel.getMinSelectionIndex();
			int rowIndexEnd = selModel.getMaxSelectionIndex();
		
			// Check each cell in the range
			for (int r=rowIndexStart; r<=rowIndexEnd; r++)
			{
				if (selModel.isSelectedIndex(r)) 
				{
					Object currEntry = tableModel.getValueAt(r, currentColumnIndex);
					if(null != currEntry) {
						ChessboardEntry entryToAdd = (ChessboardEntry)currEntry;
						if(entryToAdd.isSelectable()) {
							retList.add(entryToAdd);
						}
					}
				}
			} 
			currentColumnIndex++;
		}
		return retList.toArray();
	}
	
	/**
	 * Forwards the request to the ListSelectionModel
	 * at the specified column.
	 * @param row the row of interest
	 * @param column the column of interest
	 */
	public void addSelection(int row, int column) {
		ListSelectionModel lsm = getListSelectionModelAt(column);
		lsm.addSelectionInterval(row, row);
	}

	/**
	 * Forwards the request to the ListSelectionModel
	 * at the specified column.
	 * @param row the row of interest
	 * @param column the column of interest
	 */
	public void setSelection(int row, int column) {
        clearSelection();
		ListSelectionModel lsm = getListSelectionModelAt(column);
		lsm.setSelectionInterval(row, row);
	}

	/**
	 * Forwards the request to the ListSelectionModel
	 * at the specified column.
	 * @param row1 the starting row
	 * @param row2 the ending row
	 * @param column the column of interest
	 */
	public void setSelectionInterval(int row1, int row2, int column) {
        clearSelection();
		ListSelectionModel lsm = getListSelectionModelAt(column);
		lsm.setSelectionInterval(row1, row2);
	}

	/**
	 * Setter for the selection interval.
	 * @param cells the cells selected.
	 */
    public void setSelectionInterval(ArrayList<TableCellCoordinates> cells){
	    ListSelectionModel lsm;
        int tmp;
        int firstRow = cells.get(0).getRow();
        int lastRow = cells.get(cells.size() - 1).getRow();
        int firstCol = cells.get(0).getCol();
        int lastCol = cells.get(cells.size() - 1).getCol();
        clearSelection();
        if(firstCol == lastCol){
            lsm= getListSelectionModelAt(firstCol);
		    lsm.setSelectionInterval(firstRow, lastRow);
        } else if(firstCol < lastCol) {
            if(firstRow == lastRow){
                tmp = firstCol;
                while(tmp < (lastCol+1)){
                    lsm= getListSelectionModelAt(tmp);
    		        lsm.setSelectionInterval(firstRow, firstRow);
                    tmp++;
                }
            } else if(firstRow < lastRow){
                tmp = firstCol;
                while(tmp < (lastCol+1)){
                    lsm= getListSelectionModelAt(tmp);
                    for(int i=firstRow; i <= lastRow; i++){
        		        lsm.addSelectionInterval(i,i);
                    }
                    tmp++;
                }
            } else if(firstRow > lastRow){
                tmp = firstCol;
                while(tmp < (lastCol+1)){
                    lsm= getListSelectionModelAt(tmp);
                    for(int i=lastRow; i <= firstRow; i++){
        		        lsm.addSelectionInterval(i,i);
                    }
                    tmp++;
                }
            }
        } else if(lastCol < firstCol) {
            if(firstRow == lastRow){
                tmp = firstCol;
                while((lastCol-1) < tmp){
                    lsm= getListSelectionModelAt(tmp);
		            lsm.setSelectionInterval(firstRow, firstRow);
                    tmp--;
                }
            } else if(firstRow < lastRow){
                tmp = firstCol;
                while((lastCol-1) < tmp){
                    lsm= getListSelectionModelAt(tmp);
                    for(int i=firstRow; i <= lastRow; i++){
        		        lsm.addSelectionInterval(i,i);
                    }
                    tmp--;
                }
            } else if(firstRow > lastRow){
                tmp = firstCol;
                while((lastCol-1) < tmp){
                    lsm= getListSelectionModelAt(tmp);
                    for(int i=lastRow; i <= firstRow; i++){
        		        lsm.addSelectionInterval(i,i);
                    }
                    tmp--;
                }
            }
        }  
    }
	/**
	 * Forwards the request to the ListSelectionModel
	 * at the specified column.
	 * @param row the row for the lead selection.
	 * @param column the column for the lead selection.
	 */
	public void setLeadSelectionIndex(int row, int column) {
		ListSelectionModel lsm = getListSelectionModelAt(column);
		if (lsm.isSelectionEmpty())
			lsm.setSelectionInterval(row, row);
		else
			//calling that method throws an IndexOutOfBoundsException when selection is empty (?, JDK 1.1.8, Swing 1.1)
			lsm.setLeadSelectionIndex(row);
	}

	/**
	 * Forwards the request to the ListSelectionModel
	 * at the specified column.
	 * @param row the row number of the item to remove selection of.
	 * @param column the column number for the item to remove the selection from.
	 */
	public void removeSelection(int row, int column) {
		ListSelectionModel lsm = getListSelectionModelAt(column);
		lsm.removeSelectionInterval(row, row);
	}

	/**
	 * Calls clearSelection() of all ListSelectionModels.
	 */
	public void clearSelection() {
		for(Iterator<ListSelectionModel> iter=listSelectionModels.iterator();
                iter.hasNext();) {

			ListSelectionModel lm = iter.next();
			lm.clearSelection();
		}
	}

	/**
	 * Indicates whether an item is selected (true) or not (false).
	 * @param row the row of the item for which we wish to know the selected state.
	 * @param column the column of the item for which we wish to know the selected state.
	 * @return true, if the specified cell is selected.
	 * 
	 */
	public boolean isSelected(int row, int column) {
		ListSelectionModel lsm = getListSelectionModelAt(column);
		return lsm.isSelectedIndex(row);
	}

	/**
	 * Returns the ListSelectionModel at the specified column
	 * @param index the column
	 * @return the list selection model for the index given.
	 */
	public ListSelectionModel getListSelectionModelAt(int index) {
		return (ListSelectionModel)(listSelectionModels.get(index));
	}

	/**
	 * Set the number of columns.
	 * @param count the number of columns
	 */
	public void setColumns(int count) {
		listSelectionModels = new ArrayList<ListSelectionModel>();
		for (int i=0; i<count; i++) {
			addColumn();
		}
	}

		/**
	 * implements PropertyChangeListener; when the TableModel changes, 
	 * the TableSelectionModel has to adapt to the new Model. This method is 
	 * called if a new TableModel is set to the JTable.
	 */
	public void propertyChange(PropertyChangeEvent evt) 
	{
		if ("model".equals(evt.getPropertyName())) {
			TableModel newModel = (TableModel)(evt.getNewValue());
			setColumns(newModel.getColumnCount());
			TableModel oldModel = (TableModel)(evt.getOldValue());
			if (oldModel != null) {
				oldModel.removeTableModelListener(this);
			}
			//TableSelectionModel must be aware of changes in the TableModel
			newModel.addTableModelListener(this);
		}
	}

	/**
	 * Add a listener to the list that's notified each time a
	 * change to the selection occurs.
	 * @param listener the listener being added to the list of those notified of changes.
	 */
	public void addTableSelectionListener(TableSelectionListener listener) {
		listenerList.add(TableSelectionListener.class, listener);
	}

	/**
	 * Remove a listener from the list that's notified each time a
	 * change to the selection occurs.
	 * @param listener the listener being removed from the list of those notified of changes.
	 */
	public void removeTableSelectionListener(TableSelectionListener listener) {
		listenerList.remove(TableSelectionListener.class, listener);
	}

	/**
	 * Is called when the TableModel changes. If the number of columns
	 * had changed this class will adapt to it.
	 */
	//implements TableModelListener
	public void tableChanged(TableModelEvent e) {
		TableModel tm = (TableModel)e.getSource();
		int count = listSelectionModels.size();
		int tmCount = tm.getColumnCount();
		//works, because you can't insert columns into a TableModel (only add/romove(?)):
		//if columns were removed from the TableModel
		while (count-- > tmCount) {
			removeColumn();
		}
		//count == tmCount if was in the loop, else count < tmCount
		//if columns were added to the TableModel
		while (tmCount > count++) {
			addColumn();
		}
	}

	/**
	 * Is called when the selection of a ListSelectionModel
	 * of a column has changed.
	 * @see #fireValueChanged(Object source, int firstIndex, int lastIndex, int columnIndex, boolean isAdjusting)
	 */
	//implements ListSelectionListener
	public void valueChanged(ListSelectionEvent e) {
		ListSelectionModel lsm = (ListSelectionModel)e.getSource();
		int columnIndex = listSelectionModels.lastIndexOf(lsm);
		if (columnIndex > -1) {
			fireValueChanged(this, e.getFirstIndex(), e.getLastIndex(), columnIndex, e.getValueIsAdjusting());
		}
	}

	@Override
	public String toString() {
		String ret = "[\n";
		for (int col=0; col<listSelectionModels.size(); col++) {
			ret += "\'"+col+"\'={";
			ListSelectionModel lsm = getListSelectionModelAt(col);
			int startRow = lsm.getMinSelectionIndex();
			int endRow = lsm.getMaxSelectionIndex();
			for (int row=startRow; row<endRow; row++) {
				if (lsm.isSelectedIndex(row))
					ret += row + ", ";
			}
			if (lsm.isSelectedIndex(endRow))
				ret += endRow;
			ret += "}\n";
		}
		ret += "]";
		/*String ret = "";
	    for (int col=0; col<listSelectionModels.size(); col++) {
	      ret += "\'"+col+"\'={"+getListSelectionModelAt(col)+"}";
	    }*/
		return ret;
	}
	
	/**
	 * Add a column to the end of the model.
	 */
	protected void addColumn() {
		DefaultListSelectionModel newListModel = new DefaultListSelectionModel();
		listSelectionModels.add(newListModel);
		newListModel.addListSelectionListener(this);
	}

	/**
	 * Remove last column from model.
	 */
	protected void removeColumn() {
		//get last element
		DefaultListSelectionModel removedModel = (DefaultListSelectionModel)listSelectionModels.get(listSelectionModels.size() - 1);
		removedModel.removeListSelectionListener(this);
		listSelectionModels.remove(removedModel);

	}

	/**
	 * Notify listeners that we have ended a series of adjustments.
	 */
	protected void fireValueChanged(Object source, int firstIndex, int lastIndex, int columnIndex, boolean isAdjusting) {
		Object[] listeners = listenerList.getListenerList();
		TableSelectionEvent e = null;

		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == TableSelectionListener.class) {
				if (e == null) {
					e = new TableSelectionEvent(source, firstIndex, lastIndex, columnIndex, false);
				} ((TableSelectionListener)listeners[i+1]).valueChanged(e);
			}
		}
	}
}


