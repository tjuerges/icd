package alma.common.gui.chessboard.internals;

import java.util.HashMap;

import javax.swing.table.AbstractTableModel;

import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusListener;

/**
 * The <code>ChessboardTableModel</code> class is used to store the model
 * for a chessboard table. It implements the <code>ChessboardStatusListener</code>
 * interface in order to be notified of state changes.
 * 
 * @author Steve Harrington
 *
 * @see ChessboardTable
 * @see AbstractTableModel
 * @see ChessboardStatusListener
 */
public class ChessboardTableModel extends AbstractTableModel
	implements ChessboardStatusListener
{

	/**
	 * Constructor for the table model, with the given initial data.
	 * @param initialData the data with which to populate the table model
	 */
	public ChessboardTableModel(ChessboardEntry[][] initialData) {
		super();
		initialize(initialData);
	}

	/**
	 * Required method of ChessboardStatusListener interface, used to 
	 * notify us of changes in state for some entry in the table model.
	 * @param event an event containing updated information for an entry in the model.
	 */
	public void processStatusChange(ChessboardStatusEvent event) 
	{
		//System.out.println("Received ChessboardStatusEvent");
		ChessboardEntry entry = entryMap.get(event.getAntennaName());
		
		// Commented out debug information printing:
		//int row = this.getRowForEntry(entry).intValue();
		//int column = this.getColumnForEntry(entry).intValue();
		//System.out.println("processing status change event for: " + event.getAntennaName() + 
		//	" at row: " + row + " and col: " + column);
		
		if(entry != null)
		{
			if (null != event.getNewAntennaName()) {
				// new name for an antenna; update the entry map to reflect the new name
			
				// remove the old item in the map (for the previous antenna name)
				entryMap.remove(entry.getDisplayName());
				
				// add the new item to the map (for the new antenna name)
				entryMap.put(event.getNewAntennaName(), entry);
				
				// set the new antenna name for the entry object
				entry.setDisplayName(event.getNewAntennaName());
			}
			
			// NOTE: tool tip text should be set *after* setting the new status (if default tool tip text is being used
			// i.e. if the new tool tip text is 'null'). In other words, don't rearrange the order
			// of the calls to the setters in the following 2 lines, as they are in this order for a reason. 
			// I don't particularly like this dependency, but it's not clear how to get around it without 
			// being too invasive and it doesn't seem like too big a deal.

			// set the new status; note this must be done before setting new tool tip text, for reasons mentioned before
			entry.setCurrentStatus(event.getStatus());

			// set the new tool tip text; note this must happen after setting new status, for reasons mentioned before.
			if(!event.keepExistingToolTipText()) 
			{
				entry.setToolTipText(event.getToolTipText());
			}

			// fire the event which will result in the table being properly updated
			this.fireTableCellUpdated(this.getRowForEntry(entry).intValue(), this.getColumnForEntry(entry).intValue());
		}
		else 
		{
			//System.out.println("null entry for: " + event.getName());
		}
	}
	
	/**
	 * Returns the column count.
	 * @see AbstractTableModel
	 */
	public int getColumnCount() {
		return data[0].length;
	}

	/**
	 * Returns the row count.
	 * @see AbstractTableModel
	 */
	public int getRowCount() {
		return data.length;
	}

	/**
	 * Gets the entry at the given row and column.
	 * @see AbstractTableModel
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data[rowIndex][columnIndex];
     }
	
	/**
	 * Returns the row associated with a given entry or null if none found.
	 * 
	 * @param item the entry for which we want to know the row.
	 * @return an Integer designating the row found, or null if none found.
	 */
	public Integer getRowForEntry(ChessboardEntry item)
	{
		//System.out.println("getting row for entry: " + item.getDisplayName() + " result: " + rowMap.get(item));
		return rowMap.get(item); 
	}
	
	/**
	 * Returns the column associated with a given entry or null if none found.
	 * @param item the entry for which we want to know the column.
	 * @return an Integer designating the column found, or null if none found.
	 */
	public Integer getColumnForEntry(ChessboardEntry item)
	{
		//System.out.println("getting col for entry: " + item.getDisplayName() + " result: " + columnMap.get(item));
		return columnMap.get(item); 
	}
	
     /**
      * JTable uses this method to determine the default renderer/
      * editor for each cell.  
      * @see AbstractTableModel
      */
     @Override
	public Class<?> getColumnClass(int c) {
    	 return getValueAt(0, c).getClass();
     }
	
    /**
     * Private method to initialize the model with the given data.
     * @param initialData the data with which to populate the model.
     */
	private void initialize(ChessboardEntry[][] initialData)
	{
		data = initialData;
		
		// construct various hash maps in order to lookup entries by different keys,
		// needed in some contexts in order to locate an entry
		for(int i = 0; i < initialData.length; i++) 
		{
			for(int j = 0; j < initialData[i].length; j++)
			{
				rowMap.put(initialData[i][j], new Integer(i));
				columnMap.put(initialData[i][j], new Integer(j));
				if(null != initialData[i][j])
				{
					entryMap.put(initialData[i][j].getDisplayName(), initialData[i][j]);
				}
				else
				{
					entryMap.put(null, initialData[i][j]);	
				}
			}
		}
	}
	
	private ChessboardEntry[][] data;
	private HashMap<ChessboardEntry, Integer> columnMap = new HashMap<ChessboardEntry, Integer>();
	private HashMap<ChessboardEntry, Integer> rowMap = new HashMap<ChessboardEntry, Integer>();
	private HashMap<String, ChessboardEntry> entryMap = new HashMap<String, ChessboardEntry>();
	
	private final static long serialVersionUID = 0; // TODO
}


