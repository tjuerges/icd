package alma.common.gui.chessboard.internals;

import javax.swing.event.ListSelectionEvent;


/**
 * This class is used to support a multi-cell 
 * selection mechanism (with non-contiguous cells)
 * within JTable. It was originally authored by 
 * Jan-Friedrich Mutter (jmutter@bigfoot.de) but
 * adapted for use by ALMA.
 * 
 * @author Jan-Friedrich Mutter (jmutter@bigfoot.de) - original author 
 * 
 * @see TableSelectionModel
 * @see TableSelectionListener
 * @see MultiCellSelectionTableUI
 */
public class TableSelectionEvent extends ListSelectionEvent 
{
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The index of the column whose selection has changed.
	 */
	protected int columnIndex;

	/**
	 * TODO - document this...
	 * @param source TODO 
	 * @param firstRowIndex TODO
	 * @param lastRowIndex TODO
	 * @param columnIndex TODO
	 * @param isAdjusting TODO
	 */
	public TableSelectionEvent(Object source, int firstRowIndex, int lastRowIndex,
			int columnIndex, boolean isAdjusting) {

		super(source, firstRowIndex, lastRowIndex, isAdjusting);
		this.columnIndex = columnIndex;
	}

	/**
	 * Returns the index of the column whose selection has changed.
	 * @return The last column whose selection value has changed, where zero is the first column.
	 */
	public int getColumnIndex() {
		return columnIndex;
	}
}


