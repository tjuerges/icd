package alma.common.gui.chessboard.internals;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

import alma.common.gui.chessboard.ChessboardEntry;

/**
 * Custom table cell renderer for drawing cells in a chessboard; extends
 * <code>DefaultTableCellRenderer</code>.  <br><br>
 *
 * NOTE: most people should never be concerned
 * with this class as it is used internally by the Chessboard and should be transparent
 * to the end user.
 * 
 * @author Steve Harrington
 *
 * @see ChessboardTable
 * @see ChessboardTableFlashingCellRenderer
 * @see DefaultTableCellRenderer
 */
public class ChessboardTableCellRenderer extends DefaultTableCellRenderer 
{
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;
	private JTable owningTable;
	
	/**
	 * Constructor.
	 * @param owningTable the table which 'owns' this renderer.
	 */
	public ChessboardTableCellRenderer(JTable owningTable) 
	{
		this.owningTable = owningTable;
	}
	
	/**
	 * Overriding <code>DefaultTableCellRenderer</code> method to provide custom
	 * drawing/rendering of our cells, tool tip text, etc.
	 * 
	 * @see DefaultTableCellRenderer
	 */
	@Override
	public void setValue(Object value)
	{
		if(value instanceof ChessboardEntry)
		{
			ChessboardEntry entry = (ChessboardEntry) value;
			Color background = entry.getBgColor();
			Color foreground = entry.getFgColor();
			if(background == null) {
				background = owningTable.getBackground();
			} 
			if(foreground == null) {
				foreground = owningTable.getForeground();
			}
			this.setBackground(background);
			this.setForeground(foreground);
			this.setToolTipText(entry.getToolTipText());
			this.setHorizontalAlignment(CENTER);
			super.setValue(entry.getDisplayName());
		}
		else if(value == null)
		{
			super.setValue(null);
			this.setBackground(null);
			this.setToolTipText(null);
		}
	}
	
   /**
    * Overriding <code>DefaultTableCellRenderer</code> method to 
    * provide custom rendering of borders, returns the default table cell renderer.
    *
    * @param table  the <code>JTable</code>
    * @param value  the value to assign to the cell at
    *			<code>[row, column]</code>
    * @param isSelected true if cell is selected
    * @param hasFocus true if cell has focus
    * @param row  the row of the cell to render
    * @param column the column of the cell to render
    * @return the default table cell renderer
    * 
    * @see DefaultTableCellRenderer
    */
   @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
                         boolean isSelected, boolean hasFocus, int row, int column) 
   {
	   Component retVal = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	   if(isSelected)
	   {
		   Border border = null;
           if (isSelected) {
               border = UIManager.getBorder("Table.focusSelectedCellHighlightBorder");
           }
           if (border == null) {
               border = UIManager.getBorder("Table.focusCellHighlightBorder");
           }
           setBorder(border);
           // set the cell's colors to indicate that it is selected
           retVal.setForeground(table.getSelectionForeground());
           retVal.setBackground(table.getSelectionBackground());
	   }
	   else
	   {
		   setBorder(null);
	   }
	   return retVal;
   }
}


