/*
 * ALMA - Atacama Large Millimiter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MapToNumber.java
 */

package alma.common.gui.chessboard.internals;

import java.util.Arrays;
import java.util.Map;
import java.util.LinkedHashMap;

/**
  * Given an array of antenna names, sort them alphabetically and map 
  * them to the simplified/short strings (numbers) which are displayed on the chessboards.
  * 
  * @author Sohaila Lucero
  */
public class MapToNumber 
{
	// TODO - create an enum or find one from CONTROL subsystem...
	//	public enum AntennaType 
	//	{
	//	TOTAL_POWER, SEVEN_METER, TWELVE_METER
	//	}
	
    private static final String TOTAL_POWER = "totalPower";
	private static final String SEVEN_METER = "sevenMeter";
	private static final String TWELVE_METER = "twelveMeter";

	/**
      * Given the list of antenna names provided by the TMCDB (or whomever) and the type
      * of antennas they are (Choices include twelveMeter, sevenMeter, totalPower), the
      * names are sorted alphabetically and a mapping of name (antennaName) to number 
      * (whats displayed on the chessboard) is created and return in the form of a Map.
      * 
      * @param antennaNames An array of string which represent the antenna names.
      * @param antennaType Denotes that these antennas are either twelveMeter, sevenMeter
      *                    or totalPower antennas
      *                    
      * @return Map A map where the antenna name is the key and the number to display is
      *             the value. Returns null if either the antennaNames is empty or the
      *             antennaType does not match twelveMeter, sevenMeter or totalPower
      *             The mapping will be of length 50 if it is of type twelveMeter
      *             The mapping will be of length 12 if it is of type sevenMeter
      *             The mapping will be of length 4 if it is of type totalPower
      */
    public static Map<String,String> createMapping(String[] antennaNames, String antennaType)
    {
        if(antennaNames.length == 0 || antennaNames == null){
            return null;
        }
        if( !antennaType.equals(TWELVE_METER) 
                && !antennaType.equals(SEVEN_METER)
                && !antennaType.equals(TOTAL_POWER) ){
            System.out.println("Wrong antenna type: "+antennaType);
            return null;
        }
                
        String[] sorted = MapToNumber.sortStrings(antennaNames);
        if(antennaType.equals(TWELVE_METER)) {
            return createTwelveMeterMapping(sorted);
        } else if(antennaType.equals(SEVEN_METER)) {
            return createSevenMeterMapping(sorted);
        } else if(antennaType.equals(TOTAL_POWER)) {
            return createTotalPowerMapping(sorted);
        } else { //shouldn't get this coz it should have been covered above.
            return null;
        }
    }

    /**
      * Creates a mapping for the twelve meter antennas. If less than 50 antennas are available
      * the additional mappings are filled with generic names
      * @param antennas Array of antenna names, presumably gotten from TMCDB
      * @return LinkedHashMap<String><String> Mapping with key = antenna name and value = CB display name
      */
    private static LinkedHashMap<String,String> createTwelveMeterMapping(String[] antennas)
    {
        LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
        int tmpCtr=0;
        for(int i=0; i < antennas.length; i++){
           // System.out.println("making map of "+antennas[i]+" to "+String.valueOf(i+1)); 
            map.put( antennas[i], String.valueOf(i+1));
            tmpCtr++;
        }
        if(tmpCtr < 50){
            for(int i=tmpCtr; i < 50; i++){
                map.put( String.valueOf(i+1), String.valueOf(i+1));
            }
        }
        return map;
    }
    
    /**
      * Creates a mapping for the seven meter antennas. If less than 12 antennas are available
      * the additional mappings are filled with generic names
      * @param antennas Array of antenna names, presumably gotten from TMCDB
      * @return LinkedHashMap<String><String> Mapping with key = antenna name and value = CB display name
      // SLH - commenting out this method, which was written by Sohaila, but never gets called. 
      // TODO: decide whether we should just delete this, it's not currently called - unless there is 
      // future need for it, then we should get rid of it.
      */ //_KA: we need this for the ACA 7m antennas
    private static LinkedHashMap<String,String> createSevenMeterMapping(String[] antennas)
    {
        LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
        int tmpCtr = 0;
        for(int i=0; i < 12; i++){
            map.put( antennas[i], String.valueOf(i+1));
            tmpCtr++;
        }
        if (tmpCtr < 12){
            for(int i=tmpCtr; i < 12; i++){
                map.put( String.valueOf(i+1), String.valueOf(i+1));
            }
        }
        return map;
    }
    
    /**
      * Creates a mapping for the total power antennas. If less than 4 antennas are available
      * the additional mappings are filled with generic names
      * @param antennas Array of antenna names, presumably gotten from TMCDB
      * @return LinkedHashMap<String><String> Mapping with key = antenna name and value = CB display name
      */
    private static LinkedHashMap<String,String> createTotalPowerMapping(String[] antennas)
    {
        LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
        int tmpCtr =0;
        for(int i=0; i < 4; i++){
            map.put( antennas[i], String.valueOf(i+1));
            tmpCtr++;
        }
        if (tmpCtr < 4){
            for(int i=tmpCtr; i < 4; i++){
                map.put( String.valueOf(i+1), String.valueOf(i+1));
            }
        }
        return map;
    }

    /**
      * If an empty map is needed this will provide a generic map of the given length
      * with values the same as the key
      * @param length Size of the 'empty' mapping needed
      * @return Map Map of the given length with values all equal to its key
      */
    public static Map<String,String> createEmptyMap(int length){
        LinkedHashMap<String, String> foo = new LinkedHashMap<String, String>();
        for(int i=0; i < length; i++){
            foo.put( String.valueOf(i+1), String.valueOf(i+1));
        }
        return foo;
    }

    /**
      * Sorts the given strings into alphabetical order
      * @param s Array of strings to be ordered
      * @return String[] Sorted array of strings
      */
    public static String[] sortStrings(String[] s) 
    {
    	String [] retVal = new String[s.length];
        System.arraycopy(s, 0, retVal, 0, s.length );
        Arrays.sort(retVal);
    	return retVal;
     }

    /**
     * For testing only.
     * @param args the command line arguments.
     */
    public static void main(String[] args) 
    {
        String[] s = {"ALMA02","ALMA04","ALMA01","ALMA02"};

        System.out.print("Before: ");
        for(int i=0; i < s.length; i++){
            System.out.print(s[i]+"; ");
        }
        s = MapToNumber.sortStrings(s);
        System.out.println();
        System.out.print("After: ");
        for(int i=0; i < s.length; i++){
            System.out.print(s[i]+"; ");
        }
        System.out.println();
    }  
}
