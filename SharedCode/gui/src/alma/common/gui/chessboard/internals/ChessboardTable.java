package alma.common.gui.chessboard.internals;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Logger;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.common.gui.chessboard.ChessboardPopupMenu;
import alma.common.gui.chessboard.DefaultChessboardStatus;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * The <code>ChessboardTable</code> is a custom extension of <code>JTable</code> which
 * supports entries with color-coded (and potentially flashing) status, the ability to
 * launch detail information (via dialogs, panels, frames) when a user 'drills down' using
 * the mouse, and a custom table selection model supporting the (optional) ability to 
 * multi-select cells in the table.<br><br>
 * 
 * NOTE: It is required that the names of the entries in a chessboard are unique (within
 * that chessboard).
 * 
 * @author Steve Harrington
 * 
 * @see JTable
 * @see ChessboardEntry
 * @see ChessboardTableModel
 */
public class ChessboardTable extends JTable implements ChessboardDetailsPluginListener
{
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;
	private static final String IS_IN_STATE = " is in state: ";
	private static final String STANDALONE_LOGGER_NAME = "ChessboardTableLogger";
	
	private ChessboardCellsSelectedListener chessboardCellsSelectedListener = null;
	
	private Logger logger;

	// hash map used to lookup flashing cell renderers and reuse them if they have already been instantiated for 
	// particular cell (i.e. a cell at a particular row/column).
	private HashMap<TableCellCoordinates, ChessboardTableFlashingCellRenderer> flashingCellRendererMap = new HashMap<TableCellCoordinates, ChessboardTableFlashingCellRenderer>();
	
	// the custom selection model used to enable non-contiguous/random multi-cell selections in JTable
	private TableSelectionModel tableSelectionModel = null;
	
	// the class to use when launching a details window (e.g. after the user double-clicks on a cell in the table)
	private ChessboardDetailsPluginFactory detailsProvider;
	
	// the container services which will be passed to the details providers
	private PluginContainerServices containerServices;
	
	// boolean indicating whether details displays are enabled or not. If disabled, double-clicking may result in
	// nothing happening, for example.
	protected boolean detailsDisplayEnabled;

	// used to track the previously displayed details displays; can be useful when 'replacing' 
	// an existing details display (rather than opening a new details display).
	private Stack<ChessboardDetailsPlugin> displayedDetailsPlugins = new Stack<ChessboardDetailsPlugin>();
	
	// used to track the number of instances of a particular details panel have been shown; this is used
	// in order to allow multiple details panels for a cell to be displayed simultaneously. The
	// plugin infrastructure of the EXEC system requires that each plugin has a unique name. When we display
	// a plugin/details panel for the first instance of a selection, that name may be something like the name
	// of the cell chosen, e.g. <cell name>. The next time we attempt to display the details for that same cell, we must create
	// a unique name (say <cell name> appended with a number, e.g. T12 - 1, as opposed to T12 for the first instance). Failure
	// to append the number will result in the EXEC system rejecting the plugin as not unique. This map allows us to track how
	// many instances of a given title have been shown; the key is the title, the value is the number of instances.
	private Map<String, Integer> displayedDetailsPluginsMap = new HashMap<String, Integer>();
	
	// we need two maps - one to track the net count of displays shown for a given title, one to keep a running (monotonically 
	// inreasing) counter. When the first count (total number displayed) reaches zero, we reset the running counter. 
	// Otherwise, the monotonic counter increases for each details display shown for a given cell in the chessboard. 
	private Map<String, Integer> displayedDetailsPluginsMonotonicCountMap = new HashMap<String, Integer>();
	
	private int childPluginsStartingCount = 0;

	/**
	 * Constructor taking a <code>ChessboardTableModel</code> and a flag indicating whether to
	 * allow multi-selection of cells or not.
	 * 
	 * @param model the model for the table
	 * @param allowMultiSelect boolean indicating whether to allow multiple selection of cells or not -
	 * it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the 
	 * sub (details) panels.
	 * @see ChessboardTableModel
	 * @see JTable
	 */
	public ChessboardTable(ChessboardTableModel model, boolean allowMultiSelect, 
				ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(model, allowMultiSelect, detailsProvider, containerServices, null);
	}
	
	/**
	 * Constructor taking a <code>ChessboardTableModel</code> and a flag indicating whether to
	 * allow multi-selection of cells or not.
	 * 
	 * @param model the model for the table
	 * @param allowMultiSelect boolean indicating whether to allow multiple selection of cells or not -
	 * it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that will be passed thru to the details panels.
	 * @param popupMenu a popup menu to add as a context menu (shown on right-click on the chessboard)
	 * @see ChessboardTableModel
	 * @see JTable
	 */
	public ChessboardTable(ChessboardTableModel model, boolean allowMultiSelect, 
			ChessboardDetailsPluginFactory detailsProvider, 
			PluginContainerServices containerServices,
			JPopupMenu popupMenu)
	{
		super(model);
		this.detailsProvider = detailsProvider;
		this.containerServices = containerServices;
		setDefaultRenderer(ChessboardEntry.class, new ChessboardTableCellRenderer(this));
		initialize(allowMultiSelect, true, popupMenu);
	}
	
	/**
	 * Overriding <code>JTable</code> method in order to get custom renderer for cells in the chessboard.
	 * 
	 * @param row the row of the cell for which to get the cell renderer.
	 * @param column the column of the cell for which to get the cell renderer.
	 * 
	 * @see JTable
	 */
	@Override
	public TableCellRenderer getCellRenderer(int row, int column) 
	{
		TableCellRenderer retVal = super.getCellRenderer(row, column);
		
        Object value = this.getValueAt(row, column);
        if(value instanceof ChessboardEntry)
        {
        	ChessboardEntry entry = (ChessboardEntry)value;
            if(entry.shouldFlash())
            {
            	TableCellCoordinates cell = new TableCellCoordinates(row, column);
            	ChessboardTableFlashingCellRenderer existingRenderer = flashingCellRendererMap.get(cell); 
            	if(null == existingRenderer)
            	{
            		//System.out.println("adding flashing renderer for row: " + row + " col: " + column);	
            		ChessboardTableFlashingCellRenderer renderer = 
            			new ChessboardTableFlashingCellRenderer(this, (ChessboardTableModel)this.getModel(), entry.getBgColor(), entry.getFgColor());
            		flashingCellRendererMap.put(cell, renderer);
            		renderer.setEnabled(this.isEnabled());
            		retVal = renderer;
            	}
            	else
            	{
            		existingRenderer.setEnabled(this.isEnabled());
            		retVal = existingRenderer;
            	}
            }
            if(retVal instanceof ChessboardTableCellRenderer) {
            	((ChessboardTableCellRenderer)retVal).setEnabled(this.isEnabled()); 
            }
        }
        return retVal;
    }	
	
	/**
	 * Overriding <code>JTable</code> method to prevent editing of cells.
	 * 
	 * @param row the row of the cell in question.
	 * @param column the column of the cell in question.
	 * 
	 * @return whether editing of cells is allowed; hard-coded to false (for all cells) here. 
	 */
	@Override
	public boolean isCellEditable(int row, int column)
	{
		return false;
	}
	
    /**
     * Overriding <code>JTable</code> method to ascertain whether the cell is selected.
     * Delegates to the TableSelectionModel, if appropriate, which is responsible for 
     * maintaining the table's selection state if multi-cell-selection is enabled.
     * 
     * @param row the row of the column in question.
     * @param column the column of the cell in question.
     * 
     * @see TableSelectionModel
     * @see JTable
     */
	@Override
	public boolean isCellSelected(int row, int column) {
		boolean retVal = false;
		if(this.isEnabled()) 
		{
			if(null != tableSelectionModel)
			{
				int columnIndex = convertColumnIndexToModel(column);
				retVal = tableSelectionModel.isSelected(row, columnIndex);
				if(this.getModel().getValueAt(row, column) instanceof ChessboardEntry) {
					ChessboardEntry entry = (ChessboardEntry)this.getModel().getValueAt(row, column);
					if(!entry.isSelectable()) {
						retVal = false;
					}
				}
				
			}
			else {
				retVal = super.isCellSelected(row, column);
				if(this.getModel().getValueAt(row, column) instanceof ChessboardEntry) {
					ChessboardEntry entry = (ChessboardEntry)this.getModel().getValueAt(row, column);
					if(!entry.isSelectable()) {
						retVal = false;
					}
				}
			}
		}
		return retVal;
	}

	/**
	 * Gets an array of <code>ChessboardEntry</code> objects for each selected entry in the chessboard 
	 * or null if there are no cells selected.
	 * 
	 * @return array containing the selected entries or null if none selected.
	 */
	public ChessboardEntry[] getSelectedEntries()
	{
		ChessboardEntry[] retVal = null;
		if(tableSelectionModel == null) {
			retVal = this.getSelectedEntriesSingleSelectMode();
		}
		else {
			retVal = this.getSelectedEntriesMultiSelectMode();
		}
		return retVal;
	}
	
	/**
	 * Gets an array of <code>String</code> objects for each selected entry in the chessboard 
	 * or null if there are no cells selected.
	 * 
	 * @return array containing the selected entry names or null if none selected.
	 */
	public String[] getSelectedNames()
	{
		String[] retVal = null;
		ChessboardEntry[] selectedEntries = this.getSelectedEntries();
		if(null != selectedEntries)
		{
			retVal = new String[selectedEntries.length];
			for(int i = 0; i < selectedEntries.length; i++) 
			{
				retVal[i] = selectedEntries[i].getDisplayName();
			}
		}
		if(null != retVal) {
			Arrays.sort(retVal);
		}
		return retVal;
	}
	
	/**
	 * Used to set the plugin container services, which are made available to the details panels
	 * launched from the chessboard. This method is useful if it wasn't possible to pass the plugin
	 * container services in at the time of construction (i.e. in the constructor) and null was 
	 * passed to the constructor for example.
	 * 
	 * @param containerServices the container services which will be available to the details panels
	 * that are launched from the chessboard.
	 */
	public void setPluginContainerServices(PluginContainerServices containerServices)
	{
		this.containerServices = containerServices;
	}
	
	/**
	 * This method can be used to enable or disable the display of details information,
	 * which is launched in response to user interaction (e.g. double-clicking a cell)
	 * in the chessboard.
	 * 
	 * @param newVal the boolean indicating whether details should be enabled (true) or not (false).
	 */
	public void setDetailsDisplayEnabled(boolean newVal)
	{
		this.detailsDisplayEnabled = newVal;
	}
	
	/**
     * Required method of the <code>ChessboardDetailsPluginListener</code> interface.
     * Used to notify us that a details panel is closing, using standard
     * observer pattern.
     * @param plugin the plugin that is closing.
     */
	public void pluginClosing(ChessboardDetailsPlugin plugin)
	{
		this.removeDisplayedDetailsPlugin(plugin);
	}
	
	/**
     * Required method of the <code>ChessboardDetailsPluginListener</code> interface.
     * Used to notify us that a details panel is up and running, using standard
     * observer pattern. This can be used for things like displaying/removing wait cursors.
     */
	public void pluginStarted()
	{
		this.decrementChildPluginsStartingCount();
	}
	
	/**
	 * Method to increment the count of details displays that are visible for a given title / cell / antenna name.
	 * @param title the title of the cell (e.g. antenna name).
	 */
	public void incrementNumberOfDetailsDisplaysVisibleForTitle(String title)
	{
		Integer numInstances = this.displayedDetailsPluginsMap.get(title);
		if(null != numInstances) 
		{
			this.displayedDetailsPluginsMap.put(title, numInstances.intValue() + 1);
			logger.finest("incrementing count for details display: " + title);
		}
		else {
			this.displayedDetailsPluginsMap.put(title, 1);
			logger.finest("initializing count for details display: " + title);
		}
		
		Integer numMonotonicInstances = this.displayedDetailsPluginsMonotonicCountMap.get(title);
		if(null != numMonotonicInstances) 
		{
			this.displayedDetailsPluginsMonotonicCountMap.put(title, numMonotonicInstances.intValue() + 1);
			logger.finest("incrementing monotonic count for details display: " + title);
		}
		else {
			this.displayedDetailsPluginsMonotonicCountMap.put(title, 1);
			logger.finest("initializing monotonic counter for details display: " + title);
		}
	}
	
	/**
	 * Method to decrement the count of details displays that are visible for a given title (e.g. antenna name).
	 * @param title the name of the cell / selection (e.g. antenna name).
	 */
	public void decrementNumberOfDetailsDisplaysVisibleForTitle(String title)
	{
		Integer numInstances = this.displayedDetailsPluginsMap.get(title);
		if(null != numInstances) 
		{
			if(numInstances - 1 == 0) {
				this.resetNumberOfDetailsDisplaysVisibleForTitle(title);
			} else {
				this.displayedDetailsPluginsMap.put(title, numInstances.intValue() - 1);
				logger.finest("decrementing count for details display: " + title);	
			}
		}
	}
	
	/**
	 * Method to reset the counter which keeps track of the number of details displays visible for a given cell.
	 * @param title the title of the cell.
	 */
	public void resetNumberOfDetailsDisplaysVisibleForTitle(String title)
	{
		this.displayedDetailsPluginsMap.remove(title);
		this.displayedDetailsPluginsMonotonicCountMap.remove(title);
		logger.finest("resetting count for details display: " + title);
	}
	
	/**
	 * Getter for the number of details displays visible for a given cell.
	 * @param title the name of the cell (e.g. antenna name).
	 * @return the number of details displays shown for this title.
	 */
	public int getNumberOfDetailsDisplaysVisibleForTitle(String title)
	{
		int retVal = 0;
		Integer numInstances = this.displayedDetailsPluginsMap.get(title);
		if(null != numInstances) {
			retVal = numInstances;
		}
		logger.finest("number of instances for title: " + title + " is " + numInstances);
		return retVal;
	}
	
	/**
	 * Method to push a details display onto the stack of those shown.
	 * @param displayedPlugin the new plugin that has been shown.
	 */
	public void pushDisplayedDetailsPlugins(ChessboardDetailsPlugin displayedPlugin) 
	{
		if(null != displayedPlugin) {
			this.displayedDetailsPlugins.push(displayedPlugin);
			String title = displayedPlugin.getSelectionTitle();
			this.incrementNumberOfDetailsDisplaysVisibleForTitle(title);		
		}
	}
	
	/**
	 * Method to peek and see what is the top of the stack of displayed details plugins.
	 * @return the top of the stack, w/o popping it off.
	 */
	public ChessboardDetailsPlugin peekDisplayedDetailsPlugins()
	{
		return (displayedDetailsPlugins.isEmpty()) ? null : this.displayedDetailsPlugins.peek();
	}
	
	/**
	 * Shows a child plugin (details plugin) for a selection in the chessboard.
	 * @param plugin the plugin to show.
	 * @param uniqueTitle the title (must be unique for the JIDE docking framework) for the plugin.
	 */
	public void showPlugin(ChessboardDetailsPlugin plugin, String uniqueTitle) {
		if(null != plugin) {
			try {
				if(null != containerServices) {
					this.incrementChildPluginsStartingCount();
					containerServices.startChildPlugin(plugin.getPluginName() + uniqueTitle, plugin);
				}
				pushDisplayedDetailsPlugins(plugin);
				clearSelection();
			} catch(Exception ex) {
				this.decrementChildPluginsStartingCount();
				this.pluginClosing(plugin);
				logger.warning("Exception caught starting child plugin for cell: " + plugin.getPluginName());
			}
		}
	}
	
	
	/**
	 * Method to get (create) a unique name for the currently selected chessboard cell - necessary
	 * in order to allow the EXEC infrastructure to display multiple copies of details displays for
	 * a single cell, because the exec requires each plugin to have a unique name.
	 * 
	 * @return the unique name
	 */
	public String getUniqueTitleForCurrentSelection()
	{
		StringBuffer retVal = new StringBuffer(this.getSelectedNames().length == 1 ? this.getSelectedNames()[0] : this.getSelectedNames().toString());
		Integer monotonicCount = this.displayedDetailsPluginsMonotonicCountMap.get(retVal.toString());
		if(null != monotonicCount) {
			retVal.append(" - ");
			retVal.append(Integer.toString(monotonicCount));
		}
		else {
			// this.incrementNumberOfDetailsDisplaysVisibleForTitle(retVal.toString());
			// make unique without appending a number, just use a space.
			retVal.append(" ");
		}
		return retVal.toString();
	}
	
	/**
	 * Getter for the name of the current cell selected.
	 * @return the name of the current cell selection (e.g. antenna name) in the chessboard.
	 */
	public String getCurrentSelectionString()
	{
		String retVal = this.getSelectedNames().length == 1 ? this.getSelectedNames()[0] : this.getSelectedNames().toString();
		return retVal;
	}
	

	@Override
	public void clearSelection()
	{
		ChessboardEntry selectedEntries[] = this.getSelectedEntries();
		super.clearSelection();
		if(null != getTableSelectionModel()) {
			getTableSelectionModel().clearSelection();	
		}
		// fire events to redraw the table cells that were affected (unselected)
		if(null != selectedEntries && selectedEntries.length > 0) 
		{
			for(ChessboardEntry myEntry : selectedEntries)
			{
				Integer column = ((ChessboardTableModel) getModel()).getColumnForEntry(myEntry);
				Integer row = ((ChessboardTableModel)getModel()).getRowForEntry(myEntry);
				if(null != row && null != column) {
					TableModelEvent event = new TableModelEvent(this.getModel(), row, row, column);
					tableChanged(event);
				}
			}	
		}
		this.informChessboardCellSelectionListener();
	}
	
	private void removeDisplayedDetailsPlugin(ChessboardDetailsPlugin removedPlugin)
	{
		if(null != removedPlugin) {
			logger.finest("removing plugin: " + removedPlugin.getUniqueName() + " with original title: " + removedPlugin.getSelectionTitle());
			this.displayedDetailsPlugins.remove(removedPlugin);
			Integer numDisplayedInteger = this.displayedDetailsPluginsMap.get(removedPlugin.getSelectionTitle());
			if(null != numDisplayedInteger) {
				int numDisplayed = numDisplayedInteger.intValue();
				if(1 == numDisplayed)
				{
					logger.finest("count has lowered to one for: " + removedPlugin.getSelectionTitle());
					this.resetNumberOfDetailsDisplaysVisibleForTitle(removedPlugin.getSelectionTitle());
				} else {
					logger.finest("count is: " + numDisplayed + " for title: " + removedPlugin.getSelectionTitle());
					this.displayedDetailsPluginsMap.put(removedPlugin.getSelectionTitle(), --numDisplayed);
				}
			}
		}
	}

	/**
	 * Package-scoped method to remove a flashing cell renderer when it is no longer necessary.
	 * 
	 * @param row the row for the cell which was flashing but no longer needs to flash.
	 * @param col the column for the cell which was flashing but no longer needs to flash.
	 */
	synchronized void removeFlashingRenderer(int row, int col)
	{
		TableCellCoordinates cell = new TableCellCoordinates(row, col);
		this.flashingCellRendererMap.remove(cell);
	}
		
	/**
	 * Getter/accessor for the selection model of the table if multi-cell selection is enabled 
	 * or null if multi-cell selection is disabled.
	 * 
	 * @return the current TableSelectionModel.
	 */
	TableSelectionModel getTableSelectionModel() {
		return tableSelectionModel;
	}
	
	/**
	 * Provides the custom cell selection logic needed by the chessboard. The
	 * TableSelectionModel allows non-contiguous cells to be selected which is not
	 * possible using standard Swing/JTable selection model capabilities (which only
	 * allow 3 selection modes: SINGLE_SELECTION, SINGLE_INTERVAL_SELECTION, and 
	 * MULTIPLE_INTERVAL_SELECTION, all of which involve either single cells or a 
	 * contiguous range of cells but not a set of random cells with gaps between them).
	 */
	private void setTableSelectionModel(TableSelectionModel newModel) 
	{
		//the TableSelectionModel shouldn't be null
		if (newModel == null) {
			throw new IllegalArgumentException("Cannot set a null TableSelectionModel");
		}

		//set the new Model
		this.tableSelectionModel = newModel;
		
		//The model needs to know how many columns there are
		newModel.setColumns(getColumnModel().getColumnCount());
		
		//Register to be notified of changes in the table's data model
		getModel().addTableModelListener(newModel);

		// The table selection model will listen for property changes of the chessboard table
		addPropertyChangeListener(newModel);

		//firePropertyChange("tableSelectionModel", null, newModel);
	}

	/**
	 * private method used to determine the selected entries if the table has
	 * multi-cell selection enabled.
	 * 
	 * @return array of the selected entries or null if nothing selected.
	 */
	private ChessboardEntry[] getSelectedEntriesMultiSelectMode()
	{
		// delegate to the table selection model
		ChessboardEntry[] retVal = null;
		Object[] objArray = this.tableSelectionModel.getSelectedItems(getModel());
		if(null != objArray && objArray.length > 0) {
			retVal = new ChessboardEntry[objArray.length];
			for(int i = 0; i < objArray.length; i++)
			{
				if(objArray[i] instanceof ChessboardEntry) {
					retVal[i] = (ChessboardEntry)objArray[i];	
				}
			}
		}
		return retVal;
	}
	
	/**
	 * private method used to determine the selected entries if the table has
	 * multi-cell selection disabled.
	 * 
	 * @return array of the selected entries or null if nothing selected.
	 */
	private ChessboardEntry[] getSelectedEntriesSingleSelectMode()
	{
		ChessboardEntry[] retVal = null;

		if (getCellSelectionEnabled()) 
		{
			// Individual cell selection is enabled
			int rowIndex = getSelectedRow();
			int colIndex = getSelectedColumn();
			
			if(-1 != rowIndex && -1 != colIndex) {
				Object value = this.getModel().getValueAt(rowIndex, colIndex);
				if(value instanceof ChessboardEntry) {
					ChessboardEntry cbEntry = (ChessboardEntry)this.getModel().getValueAt(rowIndex, colIndex);
					if(cbEntry.isSelectable()) {
						retVal = new ChessboardEntry[1];
						retVal[0] = cbEntry;
					}
				}
			}
		}
		return retVal;
	}
	
	/**
	 * Private method to initialize the table.
	 * 
	 * @param allowMultiSelect whether or not the table supports multiple selection of cells.
	 * @param enablePopupMenu whether or not to enable a popup-menu on the chessboard
	 * @param menu the popup menu to add to the chessboard as a right-click context menu; if enablePopupMenu is true.
	 */
	private void initialize(boolean allowMultiSelect, boolean enablePopupMenu, JPopupMenu menu)
	{
		// initialize the logger which will be used for all of our logging message.
		// if we don't have a pluginContainerServices reference, then just
		// use the global logger - this will usually only be true if we're testing
		// e.g. running outside of the EXEC plugin infrastructure.
		logger = (this.containerServices == null) ? 
			Logger.getLogger(STANDALONE_LOGGER_NAME) : 
			containerServices.getLogger();
		this.setOpaque(true);
		if(allowMultiSelect) 
		{
			TableSelectionModel tsm = new TableSelectionModel();
			setTableSelectionModel(tsm);
			setUI(new MultiCellSelectionTableUI());
		}
		else {
			this.tableSelectionModel = null;
		}

		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setRowSelectionAllowed(false);
		this.setColumnSelectionAllowed(false);
		this.setCellSelectionEnabled(true);
		this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.setAutoCreateColumnsFromModel(false);
		
		int columnsForPreferredWidth = 5;
		if(this.getColumnCount() > 5) {
			columnsForPreferredWidth = this.getColumnCount();
		}
		this.setIntercellSpacing(new Dimension(2,2));	

		// Add a listener for mouse events; this is used to launch another GUI 
		// component when the user double clicks a cell in the table, for example.
		if(enablePopupMenu) {
			this.addMouseListener(new ChessboardTablePopupMouseAdapter(this, menu));
		} else {
			this.addMouseListener(new ChessboardTableMouseAdapter(this)); 			
		}
		
		// Add a listener for component events so that we can perform custom resizing.
		this.addComponentListener(new ChessboardTableComponentAdapter());
		
		// enable details display
		this.detailsDisplayEnabled = true;
	}

	/**
	 * Private class used to capture mouse clicks and launch things when appropriate (e.g. double click)
	 * may launch a details panel about the cell that was clicked.
	 * 
	 * @author Steve Harrington
	 */
	private class ChessboardTableMouseAdapter extends MouseAdapter
	{
		protected static final String DETAILS_STRING = "Details";
		protected ChessboardTable enclosingTable;
		
		/**
		 * Constructor.
		 * @param enclosingTable the table that is constructing us.
		 */
		public ChessboardTableMouseAdapter(ChessboardTable enclosingTable) {
			super();
			this.enclosingTable = enclosingTable;
		}
		
		@Override
		public void mouseClicked(MouseEvent event)
		{
			Point clickPoint = event.getPoint();
			int row = rowAtPoint(clickPoint);
			int column = columnAtPoint(clickPoint);
			if(row == -1 || column == -1) 
			{
				// no cell found
				return; 
			}

			//System.out.println("clickCount is: " + e.getClickCount());
			Object clickedItem = getModel().getValueAt(row, column);
			
			// handle double-click
			if(clickedItem instanceof ChessboardEntry && 
					event.getButton() == MouseEvent.BUTTON1 && 
					event.getClickCount() == 2 && 
					isEnabled()) 
			{
				ChessboardEntry clickedEntry = (ChessboardEntry) clickedItem;
				if(null != detailsProvider && null != clickedEntry && clickedEntry.isSelectable()) {
					// if using a user-provided subpanel, launch it
					String[] arrayToPass = new String[1];
					arrayToPass[0] = clickedEntry.getDisplayName();
					String uniqueTitle = getUniqueTitleForCurrentSelection();
					ChessboardDetailsPlugin[] plugins = detailsProvider.instantiateDetailsPlugin(arrayToPass, containerServices, enclosingTable,
							uniqueTitle, arrayToPass[0]);
					
					showPlugins(plugins, uniqueTitle);
				}
				else if(detailsDisplayEnabled && null != clickedEntry && clickedEntry.isSelectable()) {
					// else, the default behavior (if no details component has been provided) is to pop up a semi-generic dialog box
					int dialogType = JOptionPane.INFORMATION_MESSAGE;
					String dialogTitle = DETAILS_STRING;
					if(clickedEntry.getCurrentStatus() == DefaultChessboardStatus.WARNING) {
						dialogType = JOptionPane.WARNING_MESSAGE;
					} else if(clickedEntry.getCurrentStatus() == DefaultChessboardStatus.FATAL_ERROR || 
							  clickedEntry.getCurrentStatus() == DefaultChessboardStatus.SEVERE_ERROR) {
						dialogType = JOptionPane.ERROR_MESSAGE;
					}
					
					JOptionPane.showMessageDialog(null, clickedEntry.getDisplayName() + IS_IN_STATE + clickedEntry.getCurrentStatus(), dialogTitle, dialogType);
				}
			}
			informChessboardCellSelectionListener();
			// SLH - commented this out, but leaving it here because it can be 
			// useful for debugging selection issues 
//			ChessboardEntry[] selectedItems = getSelectedEntries();
//			if(null != selectedItems) {
//				for(int i = 0; i < selectedItems.length; i++)
//				{
//					System.out.println("DEBUG -- selected entry[" + i + "] is: " + selectedItems[i].getDisplayName());
//				}
//			}
		}
	}
	
	/**
	 * Private class which can be used for a mouse adapter that listens to mouse events on the chessboard and 
	 * handles double-clicks, right-clicks, etc. This adapter adds a popup menu to the chessboard upon right-click.
	 * 
	 * @author Steve Harrington
	 */
	private class ChessboardTablePopupMouseAdapter extends alma.common.gui.chessboard.internals.ChessboardTable.ChessboardTableMouseAdapter
	{		
		private static final String SELECTED_STRING = "You selected: ";
		private static final String DISPLAY_SELECTED = "Launch Selected";
		private static final String CANCEL = "Cancel";
		private JPopupMenu popupMenu;

		private void createPopupMenu(JPopupMenu menu) 
		{
			if(null != menu)
		    {
				// Use the popupMenu passed in
				popupMenu = menu;
		    }
			else 
			{
				//Create a new popup menu.
				popupMenu = new JPopupMenu();
			}
	        
	        // Add a menu item for displaying the selected items in the chessboard
	        JMenuItem menuItem = new JMenuItem(DISPLAY_SELECTED);
	        menuItem.addActionListener(new ActionListener() 
	        {
	        	// add an action listener to the popup menu, to
	        	// handle things if the user selects an item from the menu
	        	public void actionPerformed(ActionEvent e) {
					if(e.getActionCommand().equals(DISPLAY_SELECTED))
					{
						String[] names = getSelectedNames();
						if(null != detailsProvider && null != names) 
						{
							String uniqueTitle = getUniqueTitleForCurrentSelection();
							// if we have a details provider, use it to display the details
							ChessboardDetailsPlugin[] plugins = detailsProvider.instantiateDetailsPlugin(names, containerServices, enclosingTable,
									uniqueTitle, getCurrentSelectionString());
							
							showPlugins(plugins, uniqueTitle);
						} 
						else if(detailsDisplayEnabled) {
							// otherwise, if no details provider, just throw up a generic dialog indicating what was chosen
							if(null != names) {
								int dialogType = JOptionPane.INFORMATION_MESSAGE;
								StringBuffer dialogString = new StringBuffer(SELECTED_STRING);
								dialogString.append(names[0]);
								for(int i = 1; i < names.length; i++) {
									dialogString.append(", ").append(names[i]);
								}
								JOptionPane.showMessageDialog(null, dialogString, DETAILS_STRING, dialogType);
							}
						}
					}
				}
	        });
	        popupMenu.insert(menuItem, 0);
	         
	        // add a menu item to cancel
	        JMenuItem menuItem2 = new JMenuItem(CANCEL);
	        menuItem2.addActionListener(new ActionListener() 
	        {
	        	// add an action which simply hides the popup if the user chooses cancel
	        	public void actionPerformed(ActionEvent e) {
					if(e.getActionCommand().equals(CANCEL))
					{
						popupMenu.setVisible(false);
					}
				}
	        });
	        popupMenu.add(menuItem2);
	    }
		
		/**
		 * Constructor
		 * @param enclosingTable the table that is constructing us.
		 * @param menu popup menu to add to as right-click context menu (or null if none)
		 */
		public ChessboardTablePopupMouseAdapter(ChessboardTable enclosingTable, JPopupMenu menu)
		{
			super(enclosingTable);
			createPopupMenu(menu);
		}
		
		@Override
		public void mousePressed(MouseEvent event) 
		{
			if(event.isPopupTrigger() && getSelectedEntries() != null) 
			{
				if(popupMenu instanceof ChessboardPopupMenu) {
					((ChessboardPopupMenu)popupMenu).enableOrDisableChoicesForSelections(getSelectedEntries());
				}
				popupMenu.show(event.getComponent(), event.getX(), event.getY());
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent event) 
		{
			if(event.isPopupTrigger()) 
			{
				if(popupMenu instanceof ChessboardPopupMenu) {
					((ChessboardPopupMenu)popupMenu).enableOrDisableChoicesForSelections(getSelectedEntries());
				}
				popupMenu.show(event.getComponent(), event.getX(), event.getY());
			}
		}
	}
	 
	/**
	 * Private class to handle resizing. Resizing is customized
	 * so that both the column width and the cell height are resized. 
	 * Without this custom sizing, resizing would stretch/contract column/cell width
	 * but not affect row/cell height, leaving blank space at the bottom of the table
	 * and cells of a fixed height. With this customization, we get nice, scaled cells
	 * in both height and width.
	 */
	private class ChessboardTableComponentAdapter extends ComponentAdapter
	{
		@Override
		public void componentResized(ComponentEvent e)
		{
			int tableHeight = getHeight();
			int rowHeight = tableHeight / getRowCount();
			int remainder = tableHeight - (getRowCount() * rowHeight);
			int portionOfRemainder = remainder / getRowCount();
			try {
				if(portionOfRemainder == 0)
				{
					setRowHeight(rowHeight);
					setRowHeight(getRowCount()-1, rowHeight + remainder);
				}
				else
				{
					setRowHeight(rowHeight + portionOfRemainder);	
				}
			}
			catch(Exception ex) {
				// ignore any exceptions
			}
		}
	}

	/**
	 * Setter for the listener that wishes to be notified of selection state.
	 * @param chessboardCellsSelectedListener the listener which will be informed of selection state.
	 */
	public void setChessboardCellsSelectedListener(ChessboardCellsSelectedListener chessboardCellsSelectedListener) {
		this.chessboardCellsSelectedListener = chessboardCellsSelectedListener;
		informChessboardCellSelectionListener();
	}
	
	/**
	 * Private method to notify interested listener of the state of cell selection.
	 */
	private void informChessboardCellSelectionListener()
	{
		if(null != chessboardCellsSelectedListener) {
			boolean somethingSelected = getSelectedEntries() == null ? false : true;
			if(somethingSelected) {
				chessboardCellsSelectedListener.somethingSelected();
			} else {
				chessboardCellsSelectedListener.nothingSelected();
			}
		}
	}
	
	private synchronized void decrementChildPluginsStartingCount()
	{
		if(this.childPluginsStartingCount > 0)
		{
			this.childPluginsStartingCount--;
		}
		if(0 == childPluginsStartingCount)
		{
			this.setCursor(Cursor.getDefaultCursor());
		}
	}
	
	private synchronized void incrementChildPluginsStartingCount()
	{
		this.childPluginsStartingCount++;
		this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	}
	
	private void showPlugins(ChessboardDetailsPlugin[] plugins, String pluginTitle)
	{
		int count = 0;
		for(ChessboardDetailsPlugin plugin: plugins) {
			String uniquePluginTitle = pluginTitle + "." + Integer.toString(count++);
			showPlugin(plugin, uniquePluginTitle);
		}
	}
}


