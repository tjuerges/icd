package alma.common.gui.chessboard.internals;

import java.util.EventListener;


/**
 * The listener that's notified when a table selection value changes.
 * 
 * This class is used to support a multi-cell 
 * selection mechanism (with non-contiguous cells)
 * within JTable. It was originally authored by 
 * Jan-Friedrich Mutter (jmutter@bigfoot.de) but
 * adapted for use by ALMA.
 * 
 * @author Jan-Friedrich Mutter (jmutter@bigfoot.de) - original author 
 * 
 * @see TableSelectionEvent
 * @see TableSelectionModel
 * @see MultiCellSelectionTableUI
 */
public interface TableSelectionListener extends EventListener {
  
	/**
     * Called whenever the value of the selection changes.
     * @param evt the event indicating what has changed.
     */
	public void valueChanged(TableSelectionEvent evt);
}

