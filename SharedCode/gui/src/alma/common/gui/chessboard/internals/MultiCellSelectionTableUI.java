package alma.common.gui.chessboard.internals;

import java.awt.Point;
import java.util.ArrayList;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellEditor;


/**
 * This class is used to support a multi-cell 
 * selection mechanism (with non-contiguous cells)
 * within JTable. It was originally authored by 
 * Jan-Friedrich Mutter (jmutter@bigfoot.de) but
 * adapted for use by ALMA.
 * 
 * This class doesn't change the L&F of the JTable but
 * listens to mouseclicks and updates the TableSelectionModel.
 * @author Jan-Friedrich Mutter (jmutter@bigfoot.de) - original author 
 * 
 * @see TableSelectionModel
 * @see TableSelectionListener
 * @see TableSelectionEvent
 */
public class MultiCellSelectionTableUI extends BasicTableUI 
{
	public static ComponentUI createUI(JComponent c) 
	{
		return new MultiCellSelectionTableUI();
	}

	@Override
	protected MouseInputListener createMouseInputListener() {
		return new MultiCellSelectionMouseInputHandler();
	}

	/**
	 * to get access to the table from the inner class MyMouseInputHandler
	 */
	protected JTable getTable() {
		return table;
	}

    private ArrayList<TableCellCoordinates> cells = new ArrayList<TableCellCoordinates>();
    
	/**
	 * updates the TableSelectionModel.
	 */
	protected void updateTableSelectionModel(int row, int column,
			boolean ctrlDown, boolean shiftDown) {

		ChessboardTable t = (ChessboardTable)getTable();
		column = t.convertColumnIndexToModel(column);
		TableSelectionModel tsm = t.getTableSelectionModel();

		if (ctrlDown) {
			if (tsm.isSelected(row, column)) {
                cells.remove(new TableCellCoordinates(row,column));
				tsm.removeSelection(row, column);
			} else {
                cells.add(new TableCellCoordinates(row,column));
				tsm.addSelection(row, column);
			}
		} else if ((shiftDown) ){
            if(cells.size() == 2){
                cells.set(1, new TableCellCoordinates(row,column));
            } else {
                cells.add(new TableCellCoordinates(row,column));
            }
            tsm.setSelectionInterval(cells);
		} else {
            cells.clear();
            tsm.clearSelection();
			tsm.setSelection(row, column);
            cells.add(new TableCellCoordinates(row,column));
		}
	} //updateTableSelectionModel()

	/**
	 * Almost the same implementation as its super class.
	 * Except updating the TableSelectionModel rather than the
	 * default ListSelectionModel.
	 */
	//Some methods which are called in the super class are private.
	//Thus I couldn't call them. Calling the method of the super
	//class itself should do it, but you never know. Sideeffects may occur...
	public class MultiCellSelectionMouseInputHandler extends MouseInputHandler {

		@Override
		public void mousePressed(MouseEvent e) {
			super.mousePressed(e);

			if (!SwingUtilities.isLeftMouseButton(e)) {
				return;
			}

			/*if (phantomMousePressed == true) {
	        return;
	      }
	      phantomMousePressed = true;*/

			Point p = e.getPoint();
			int row = getTable().rowAtPoint(p);
			int column = getTable().columnAtPoint(p);
			// The autoscroller can generate drag events outside the Table's range.
			if ((column == -1) || (row == -1)) {
				return;
			}

			/* Adjust the selection if the event was not forwarded 
			 * to the editor above *or* the editor declares that it 
			 * should change selection even when events are forwarded 
			 * to it.
			 */
			// PENDING(philip): Ought to convert mouse event, e, here. 
			//if (!repostEvent || table.getCellEditor().shouldSelectCell(e)) {
			TableCellEditor tce = getTable().getCellEditor();
			if ((tce==null) || (tce.shouldSelectCell(e))) {
				getTable().requestFocus();
				//setValueIsAdjusting(true);
				//if (getTable().getValueAt(row, column) != null) {
				updateTableSelectionModel(row, column, e.isControlDown(), e.isShiftDown());
				//need to do this because JTable is not painted properly when
				//user does something on a column where a not selected
				//cell is the anchor ...
				getTable().repaint();
			}
		}//mousePressed()
	}

       
}


