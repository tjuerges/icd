package alma.common.gui.chessboard.internals;

/**
 * Interface used to communicate when the chessboard table has nothing / something selected,
 * so that buttons (for example) can be enabled and/or disabled accordingly.
 * @author sharring
 */
public interface ChessboardCellsSelectedListener 
{
	/**
	 * Called when no (zero) cells are selected in the chessboard table.
	 */
    public void nothingSelected();
    
    /**
     * Called when one (or more) cells in the chessboard table are selected.
     */
    public void somethingSelected();
}
