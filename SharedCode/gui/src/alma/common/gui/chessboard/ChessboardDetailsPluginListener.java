package alma.common.gui.chessboard;

/**
 * Interface used to notify interested parties
 * that a chessboard details plugin is closing.
 * Standard observer pattern mechanism.
 *
 * @author Steve Harrington
 */
public interface ChessboardDetailsPluginListener 
{
	/**
     * Used to notify interested parties that a details panel is closing, 
     * using standard observer pattern.
     * @param plugin the plugin that is closing.
     */
	public void pluginClosing(ChessboardDetailsPlugin plugin);
	
	/**
     * Used to notify interested parties that a plugin is up and running. 
     * Can be used for things like displaying/removing a wait cursor, etc.
     */
	public void pluginStarted();
}
