package alma.common.gui.chessboard;

/**
 * Interface used to publish (push) status changes to a chessboard; also can be used in
 * a 'pull' mode, to get the initial state of a chessboard's cells. Any entity 
 * which wishes to update a chessboard must implement this interface, and have the chessboard 
 * register as a listener. There is a default implementation of the methods defined in this 
 * interface in the <code>ChessboardStatusAdapter</code> class; you may wish to use that class 
 * either by extending it or by delegating to it, but you are always free to implement the methods 
 * yourself as well.
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardStatusAdapter 
 * @see ChessboardStatusListener
 * @see ChessboardStatusEvent
 */
public interface ChessboardStatusProvider 
{
	/**
	 * add a listener to the list of objects that will be notified upon change of a chessboard cell's status.
	 * @param listener the listener to add; must implement <code>ChessboardStatusListener</code> interface.
	 * @see ChessboardStatusListener
	 */
	public abstract void addChessboardStatusListener(ChessboardStatusListener listener);
	
	/**
	 * Used to 'unregister' a previously registered status change listener.
	 * @param listener the (previously registered) listener to remove from
	 * the list of objects which will be notified of status change events.
	 */
	public abstract void removeChessboardStatusListener(ChessboardStatusListener listener);

	/**
	 * Method which returns the initial (current) state of the cells for a chessboard, used to
	 * populate a chessboard upon startup via a "pull" model and is called only once (when the chessboard
	 * is first created) after which a publish/subscribe (observer pattern)
	 * mechanism is used to keep the chessboard informed of status changes.
	 * 
	 * @return a 2-D array of ChessboardEntry objects indicating both the dimensions
	 * of the chessboard (implicit in the size of the 2-d array) and the initial states
	 * of each cell.
	 */
	public abstract ChessboardEntry[][] getStates();
	
	/**
	 * Method to stop the status provider (used if a particular implementation may have a thread running,
	 * for example).
	 */
	public abstract void stop();
}

