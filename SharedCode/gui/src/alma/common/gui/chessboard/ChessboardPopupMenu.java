package alma.common.gui.chessboard;

import javax.swing.JPopupMenu;

/**
 * Class which can be used to add a right-click popup context menu to a chessboard which 
 * is sensitive to the states of the selected cells, in other words which can enable or disable
 * choices on the popup menu depending on what type of cells are selected. For popup menus on 
 * chessboards which do not enable/disable choices on the menu depending on what is selected in 
 * the chessboard (i.e. for popup menus that have all choices enabled at all times) then one can
 * simply use JPopupMenu and ignore this class. However, if you need menu items in the popup menu
 * to be enabled or disabled based on what type of cells are selected in the chessboard, you will
 * need to use a subclass of this class, overriding the enableOrDisableChoicesForSelections() 
 * method according to your needs.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public abstract class ChessboardPopupMenu extends JPopupMenu 
{
	/**
	 * This method should be implemented such that it enables or disables the menu 
	 * items, as appropriate, based on the items which are selected in the chessboard (passed in
	 * as the selectedEntries parameter).
	 * 
	 * @param selectedEntries the status of the cell for which the menu will pop up.
	 */
	public abstract void enableOrDisableChoicesForSelections(ChessboardEntry[] selectedEntries);
}
