package alma.common.gui.chessboard;

import java.awt.Color;

/**
 * This interface is intended to facilitate custom chessboard status values so that
 * the chessboard can be used in other contexts, such as Scheduling, where the values
 * enumerated in <code>DefaultChessboardStatus</code> are not sufficient. 
 * 
 * @author sharring
 * @see DefaultChessboardStatus
 * @see ChessboardEntry
 */
public interface ChessboardStatus 
{
	/**
	 * Getter/accessor for the background color associated with the status.
	 * Each status must be associated with a color for rendering in the chessboard;
	 * this can be set to null to indicate that the default background color should
	 * be used for painting the cell.
	 * 
	 * @return the background color to use when rendering the status in the chessboard
	 */
	public Color getBgColor();
	
	/**
	 * Getter/accessor for the foreground color associated with the status.
	 * Each status must be associated with a color for rendering in the chessboard;
	 * this can be set to null to indicate that the default background color should
	 * be used for painting the cell.
	 * 
	 * @return the foreground color to use when rendering the status in the chessboard
	 */
	public Color getFgColor();
		
	/**
	 * Getter/accessor for the description of the status.
	 * 
	 * @return the description of the status, e.g. WARNING, OFFLINE, etc.
	 */
	public String getDescription();
	
	/**
	 * Getter/accessor for a boolean that indicates whether the status should
	 * be rendered as 'flashing' in the chessboard.
	 * 
	 * @return boolean indicating whether items of this status should flash (true) or not (false).
	 */
	public boolean shouldFlash(); 
	
	/**
	 * Getter/accessor for a boolean that indicates whether the status should be 
	 * selectable in the UI.
	 * 
	 * @return boolean indicating whether items of this status should be selectable (true) or not (false).
	 */
	public boolean isSelectable();
	
	/**
	 * Getter/accessor to get the list of valid values, returned as an array.
	 * Should return the list of all the 'public final static' instances of the enum.
	 * This is primarily used for testing at the present, but could be useful in some 
	 * other situations potentially. For an example of its use in a test setting, 
	 * see the <code>ChessboardTest</code> and <code>ExampleChessboardStatusProvider</code> classes.
	 * @return an array of chessboar status objects indicating all the possible statuses possible;
	 * used only in testing.
	 */
	public ChessboardStatus[] values();
}


