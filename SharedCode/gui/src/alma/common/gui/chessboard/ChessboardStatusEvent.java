package alma.common.gui.chessboard;

/**
 * The <code>ChessboardStatusEvent</code> class is used to communicate
 * changes in status to <code>ChessboardStatusListener</code> objects.
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardStatusListener
 * @see ChessboardStatusAdapter
 */
public class ChessboardStatusEvent 
{
	private ChessboardStatus status;
	private String antennaName;
	private String newAntennaName;
	private String toolTipText;
	private boolean keepExistingToolTipText;
	
	
	/**
	 * Constructor for a new <code>ChessboardStatusEvent</code> that can be used when
	 * the name of an antenna is changed. If you are not changing the name of an antenna, use
	 * one of the other constructors.
	 * 
	 * @param antennaName the name of the cell in the chessboard (for example an antenna name)
	 * for which the status is being changed. Recall, as mentioned elsewhere in
	 * the javadoc, that the names of cells in a chessboard must be unique 
	 * (within a chessboard); it is not valid to have 2 (or more) cells in a 
	 * chessboard with the same name.
	 * 
	 * @param newAntennaName this can be used to change the name of a cell in the chessboard from
	 *        some previous value; the antennaName must contain the 'original' or 'previous'
	 *        value, while this parameter may contain the new value. If this functionality is not
	 *        needed, use one of the other constructors.
	 *        
	 * @param newStatus the new status.
	 * @param newToolTipText the new tool tip text, which is displayed when the user
	 * hovers the mouse over the cell. NOTE: passing null will result in default tool tip
	 * tip text along the lines of 'X is in state Y'. 
	 */
	public ChessboardStatusEvent( String antennaName, String newAntennaName, ChessboardStatus newStatus, String newToolTipText)
	{
		this.status = newStatus;
		this.antennaName = antennaName;
		this.newAntennaName = newAntennaName;
		this.toolTipText = newToolTipText;
		this.keepExistingToolTipText = false;
	}
	
	/**
	 * Constructor for a new <code>ChessboardStatusEvent</code>
	 * 
	 * @param antennaName the name of the cell in the chessboard (for example an antenna name)
	 * for which the status is being changed. Recall, as mentioned elsewhere in
	 * the javadoc, that the names of cells in a chessboard must be unique 
	 * (within a chessboard); it is not valid to have 2 (or more) cells in a 
	 * chessboard with the same name.
	 * 
	 * @param newStatus the new status.
	 * @param newToolTipText the new tool tip text, which is displayed when the user
	 * hovers the mouse over the cell. NOTE: passing null will result in default tool tip
	 * tip text along the lines of 'X is in state Y'. 
	 */
	public ChessboardStatusEvent( String antennaName, ChessboardStatus newStatus, String newToolTipText)
	{
		this.status = newStatus;
		this.antennaName = antennaName;
		this.toolTipText = newToolTipText;
		this.keepExistingToolTipText = false;
	}

	/**
	 * Constructor for a new <code>ChessboardStatusEvent</code> which can be used 
	 * when one wants to keep the previous hover text while only changing the status.
	 * 
	 * @param name the name of the cell in the chessboard (for example an antenna name)
	 * for which the status is being changed. Recall, as mentioned elsewhere in
	 * the javadoc, that the names of cells in a chessboard must be unique 
	 * (within a chessboard); it is not valid to have 2 (or more) cells in a 
	 * chessboard with the same name.
	 * 
	 * @param newStatus the new status. 
	 */
	public ChessboardStatusEvent( String name, ChessboardStatus newStatus)
	{
		this.status = newStatus;
		this.antennaName = name;
		this.toolTipText = null;
		this.keepExistingToolTipText = true;
	}
	
	/**
	 * Accessor/getter for the name of the cell for which the status is being changed.
	 * 
	 * @return the name of the cell (such as an antenna name) for which the status 
	 * has changed.
	 */
	public String getAntennaName() {
		return antennaName;
	}

	/**
	 * Accessor/getter for the new antenna name of the cell for which the status is being changed. 
	 * 
	 * @return the new antenna name of the cell (such as an antenna name) for which the status 
	 * has changed.
	 */
	public String getNewAntennaName() {
		return newAntennaName;
	}
	
	/**
	 * Accessor/getter for the new status of the cell.
	 * 
	 * @return the new status of the cell that has changed.
	 */
	public ChessboardStatus getStatus() {
		return status;
	}
	
	/**
	 * The text to display when the user hovers the mouse over this cell
	 * in the chessboard. 
	 * 
	 * @return the text displayed upon hover-over by the mouse
	 */
	public String getToolTipText() {
		return toolTipText;
	}

	/**
	 * Returns a boolean indicating whether the chessboard should keep (retain) the existing 
	 * tool tip text, rather than replacing it with the text from the event.
	 * 
	 * @return boolean indicating whether to keep existing tool tip text (true) or not (false).
	 */
	public boolean keepExistingToolTipText() {
		return keepExistingToolTipText;
	}	
}


