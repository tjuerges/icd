package alma.common.gui.chessboard;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import alma.common.gui.chessboard.internals.ChessboardPanelBase;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

/**
 * Base class for chessboard plugins. Specific chessboards
 * should extend this class, implementing the initialize method.
 * 
 * @author sharring
 *
 */
@SuppressWarnings("serial")
public abstract class ChessboardPlugin extends JPanel implements SubsystemPlugin 
{
	protected PluginContainerServices pluginContainerServices;
	protected ChessboardStatusProvider statusProvider = null;
	protected ChessboardDetailsPluginFactory detailsProvider = null;
	protected ChessboardPanelBase chessboardPanel;
	protected JPopupMenu optionalRightClickMenu = null;
	protected boolean allowMultiCellSelect = false;
	
	/**
	 * Constructor; creates a chessboard plugin with single-cell selection.
	 *
	 */
	public ChessboardPlugin()
	{
		this(false);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param allowMultiCellSelect (value of true) indicates multiple cells in the chessboard plugin can be selected concurrently; 
	 *        (value of false) indicates that only 1 cell at a time can be selected.
	 *
	 */
	public ChessboardPlugin(boolean allowMultiCellSelect)
	{
	    this.allowMultiCellSelect = allowMultiCellSelect;
	    // Ensure new chessboards are readable.
	    // TODO: Replace hard coded minimum size with a computed size.
	    this.setMinimumSize(new Dimension(358, 269));
	    this.setPreferredSize(getMinimumSize());
	}
	
	/**
	 * Required method of the SubsystemPlugin interface
	 * @return boolean indicating restricted state
	 */
	public boolean runRestricted(boolean restricted) throws Exception {
		return false;
	}

	/**
	 * Required method of the SubsystemPlugin interface.
	 */
	public void setServices(PluginContainerServices services) {
		this.pluginContainerServices = services;
	}

	/**
	 * Required method of the SubsystemPlugin interface.
	 */
	public void start() throws Exception 
	{
		if(null == this.optionalRightClickMenu)
		{
			initialize();
		}
		else 
		{
			initializeWithPopupMenu();
		}
	}

	/**
	 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin
	 */
	public void stop() throws Exception 
	{
		if(null != statusProvider) {
			statusProvider.stop();
			statusProvider = null;
		}
		chessboardPanel = null;
	}
	
	/**
	 * Facade method that merely passes through to the underlying chessboard panel method of the same name. 
	 * @return the chessboard entries that are presently selected, if any.
	 */
	public ChessboardEntry[] getSelectedEntries()
	{
		return this.chessboardPanel.getSelectedEntries();
	}
	
	/**
	 * Setter for optional popup menu to add to as a right-click popup menu on the chessboard. NOTE: for this to 
	 * work properly, it must be called after constructor but before start() method of the plugin. 
	 * 
	 * @param menu a popup menu which will be added to a popup menu on the chessboard.
	 */
	public void setOptionalRightClickPopupMenu(JPopupMenu menu)
	{
		this.optionalRightClickMenu = menu;
	}
	
	/**
	 * Method which should return a details provider for the chessboard.
	 * 
	 * @see OpticalTelescopeChessboardPanel
	 * @see ChessboardStatusProvider
	 * 
	 * @return the new chessboard's status provider
	 */
	abstract protected ChessboardStatusProvider getStatusProvider();
	
	/**
	 * Method which should return a details provider for the chessboard.
	 * @return the chessboard's details provider.
	 * @see ChessboardDetailsPluginFactory
	 */
	abstract protected ChessboardDetailsPluginFactory getDetailsProvider();

	private void initialize()
	{
		statusProvider = getStatusProvider();
		detailsProvider = getDetailsProvider();
		
		chessboardPanel = new ChessboardPanelWithButtons(allowMultiCellSelect, detailsProvider,
				statusProvider, pluginContainerServices);	
		
		commonInit();
	}
	
	private void initializeWithPopupMenu()
	{
		statusProvider = getStatusProvider();
		detailsProvider = getDetailsProvider();
		
		chessboardPanel = new ChessboardPanelWithButtons(allowMultiCellSelect, detailsProvider,
				statusProvider, pluginContainerServices, this.optionalRightClickMenu);	

		commonInit();
	}
	
	private void commonInit()
	{
		this.setLayout(new BorderLayout());
		
		if(null == chessboardPanel || chessboardPanel.getNumberOfChessboardCells() == 0) 
		{
			JLabel warningLabel = new JLabel("No antennas found");
			this.add(warningLabel, BorderLayout.CENTER);
		} 
		else 
		{
			this.add(chessboardPanel, BorderLayout.CENTER);	
		}
		
		this.validate();
	}
}
