package alma.common.gui.chessboard;

/**
 * The <code>ChessboardStatusListener</code> interface should be implemented
 * by any class which wishes to be notified when status changes occur.
 * NOTE: the listeners and the subjects must be 'hooked up' via a call
 * to the addChessboardDataListener method in the
 * <code>ChessboardStatusAdapter</code> class. This hookup will be handled
 * for you automatically if you use a constructor for <code>ChessboardPanelWithButtons</code> or 
 * <code>ChessboardFrame</code> which takes a <code>ChessboardStatusProvider</code>
 * as a parameter; if you use one of the other constructors, you'll be responsible
 * for the hookup by hand. 
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardStatusAdapter
 * @see ChessboardStatusEvent
 */
public interface ChessboardStatusListener 
{
	/**
	 * Method to handle <code>ChessboardStatusEvent</code> events, which are
	 * published when the chessboard must update the state of a cell.
	 * 
	 * @param event the <code>ChessboardStatusEvent</code> that contains
	 * the information about a state change for which the listener is interested.
	 */
	public void processStatusChange(ChessboardStatusEvent event);
}


