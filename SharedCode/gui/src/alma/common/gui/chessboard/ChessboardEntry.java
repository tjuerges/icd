package alma.common.gui.chessboard;

import java.awt.Color;

import alma.common.gui.chessboard.internals.ChessboardFrame;


/**
 * The <code>ChessboardEntry</code> class contains information about a single entry in a 
 * <code>ChessboardTable</code>, including the (current) state of the entry, the name of the entry,
 * and the toolTipText to use for mouse-over hovering in the UI. 
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardFrame
 * @see ChessboardPanelWithButtons
 */
public class ChessboardEntry implements Comparable<ChessboardEntry>
{		
	private String displayName = null;
	private ChessboardStatus currentStatus = null;
	private String toolTipText = null;
	
	private static final String IS_IN_STATE = " is in state: ";
	  
	/**
	 * Constructor which creates an entry with default toolTipText.
	 * 
	 * @param initialStatus the status of the entry that is being created.
	 * @param name the name of the entry that is being created.
	 */
	public ChessboardEntry(ChessboardStatus initialStatus, String name)
	{
		this(initialStatus, name, null);
	}
	
	/**
	 * Constructor for creating a new <code>ChessboardEntry</code> object with the
	 * designated status, name, and tool tip text.
	 * 
	 * @param initialStatus the status of the entry that is being created.
	 * @param name the name of the entry that is being created.
	 * @param hover the text to be displayed in the UI when the mouse 'hovers' over the entry.
	 */
	public ChessboardEntry(ChessboardStatus initialStatus, String name, String hover)
	{
		this.setCurrentStatus(initialStatus);
		this.setDisplayName(name);

		// NOTE: tool tip text should be set after status if default tool tip text is being used
		// i.e. if 'null' is passed as the hover parameter. In other words, don't rearrange the order
		// of the calls in this constructor, as they are in this order for a reason.
		this.setToolTipText(hover);
	}
	
	@Override
	public boolean equals(Object entry) 
	{
		boolean retVal = false;
		
		if(entry instanceof ChessboardEntry) 
		{
			ChessboardEntry cbEntry = (ChessboardEntry) entry;
			if(cbEntry.currentStatus.equals(this.currentStatus) 
				&& ((cbEntry.displayName == null && this.displayName == null) || (cbEntry.displayName != null && cbEntry.displayName.equals(this.displayName)) ) 
				&& ((cbEntry.toolTipText == null && this.toolTipText == null) || (cbEntry.toolTipText != null && cbEntry.toolTipText.equals(this.toolTipText)))) 
			{
				retVal = true;
			}
		}
		return retVal;
	}
	
	/**
	 * Getter/accessor for the display name.
	 * @return the name which is used to display the entry in the GUI
	 */
	public String getDisplayName() 
	{
		return displayName;
	}
	
	/**
	 * Standard toString method of Object base class; overridden to return displayName.
	 * @return the string used to print this object.
	 * @see Object 
	 */
	@Override
	public String toString()
	{
		return getDisplayName();
	}

	/**
	 * Getter/accessor for the current status.
	 * @return the current status of the entry.
	 */
	public ChessboardStatus getCurrentStatus() 
	{
		return currentStatus;
	}
	
	/**
	 * Getter/accessor for the hover text, which is the text to display when a user hovers the mouse
	 * over the entry in the GUI.
	 * 
	 * @return the tool tip text which will be displayed in the GUI when a user hovers the mouse over 
	 * the entry. 
	 */
	public String getToolTipText() 
	{
		return toolTipText;
	}

	/**
	 * Getter/accessor for the shouldFlash attribute which indicates whether this
	 * entry should flash in the UI (true) or not (false).
	 * 
	 * @return boolean indicating whether the entry should have a visual 'flashing' 
	 * representation in the UI (true) or not (false).
	 */
	public boolean shouldFlash()
	{
		return currentStatus.shouldFlash();
	}
	
	/**
	 * Setter/mutator for the current status of the entry. NOTE: this also
	 * updates a private boolean flag indicating whether the entry should flash 
	 * in the UI. 
	 * 
	 * @param currentStatus the status to change the entry to.
	 */
	public void setCurrentStatus(ChessboardStatus currentStatus) {
		this.currentStatus = currentStatus;
	}
	
	/**
	 * Getter for the background color for the chessboard entry.
	 * @return the background color of the chessboard entry.
	 */
	public Color getBgColor()
	{
		return currentStatus.getBgColor();
	}

	/**
	 * Getter for the foreground color for the chessboard entry.
	 * @return the foreground color of the chessboard entry.
	 */
	public Color getFgColor()
	{
		return currentStatus.getFgColor();
	}
	
	/**
	 * Setter/mutator for the hover text, which is the text 
	 * displayed when a user hovers the mouse over the entry in the GUI.
	 * 
	 * @param toolTipText the text to use when the mouse hovers over the entry. NOTE: passing a null
	 *                    will result in a default tool tip text being used which reads something like:
	 *                    'X is in state Y'.
	 */
	public void setToolTipText(String toolTipText) {
		if(null == toolTipText) {
			setDefaultToolTipText();
		}
		else {
			this.toolTipText = toolTipText;
		}
	}
	
	/**
	 * Setter/mutator for the display name.
	 * @param name the name of the entry.
	 */
	public void setDisplayName(String name) {
		this.displayName = name;
	}

	/**
	 * Can be used to assign a default tool tip text to the chessboard entry, which
	 * reads "X is in state Y" or something similar upon mouse-hover.
	 */
	public void setDefaultToolTipText()
	{
		if(this.displayName != null) {
			toolTipText = this.displayName + IS_IN_STATE + this.getCurrentStatus().getDescription();
		} else {
			toolTipText = this.getCurrentStatus().getDescription();
		}
	}

	/**
	 * Required method of the <code>Comparable</code> interface.
	 * 
	 * @return int indicating whether this object is less than (<0), greater than (>0), 
	 * or equal to (=0) the compared item.
	 * 
	 * @see Comparable
	 */
	public int compareTo(ChessboardEntry obj) {
		assert (obj instanceof ChessboardEntry) : "Cannot compare non-ChessboardEntry object to ChessboardEntry object.";
		String compareString = ((ChessboardEntry)obj).getDisplayName();
		return this.getDisplayName().compareTo(compareString);
	}

	/**
	 * Getter for whether the entry should be selectable in the UI.
	 * @return whether the entry can be selected in the UI.
	 */
	public boolean isSelectable() {
		return this.currentStatus.isSelectable();
	}
}


