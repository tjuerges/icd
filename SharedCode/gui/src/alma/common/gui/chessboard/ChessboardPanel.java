package alma.common.gui.chessboard;

import java.awt.GridLayout;
import javax.swing.JPopupMenu;
import alma.common.gui.chessboard.internals.ChessboardPanelBase;
import alma.common.gui.chessboard.internals.ChessboardTable;
import alma.common.gui.chessboard.internals.ChessboardTableModel;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * An undecorated ChessboardPanel (without buttons) which can be used when the buttons are not desired, such as is the case for Scheduling.
 * 
 * @author Steve Harrington
 * @see ChessboardPanelWithButtons
 */
public class ChessboardPanel extends ChessboardPanelBase 
{
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	/**
	 * The (preferred) constructor to create a <code>ChessboardPanel</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add to a right click context menu.
	 */
	public ChessboardPanel(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices,
			JPopupMenu menu) 
	{
		super(allowMultiCellSelect, detailsProvider, statusProvider, containerServices, menu);
	}
	
	/**
	 * The (preferred) constructor to create a <code>ChessboardPanel</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanel(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices) 
	{
		this(allowMultiCellSelect, detailsProvider, statusProvider, containerServices, null);
	}
	
	/**
	 * The constructor to create a <code>ChessboardPanel</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add as a right click context menu.
	 */
	public ChessboardPanel(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices,
			JPopupMenu menu) 
	{
		super(initialData, allowMultiCellSelect, detailsProvider, containerServices, menu);
	}
	
	/**
	 * The constructor to create a <code>ChessboardPanel</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanel(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices) 
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}

	/** 
	 * Alternate constructor to create a <code>ChessboardPanel</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 *  
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add to a right click context menu.
	 */
	public ChessboardPanel(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices,
			JPopupMenu menu)
	{
		super(initialData, allowMultiCellSelect, detailsProvider, containerServices, menu);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanel</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 *  
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanel(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanel</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
     *
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br> 
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows desired in the chessboard.
	 * @param columns the number of columns desired in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add to a right click context menu.
	 */
	public ChessboardPanel(ChessboardEntry[] initialData, int rows, int columns, 
			boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			PluginContainerServices containerServices,
			JPopupMenu menu)
	{
		super(initialData, rows, columns, allowMultiCellSelect, detailsProvider, containerServices, menu);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanel</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
     *
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br> 
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows desired in the chessboard.
	 * @param columns the number of columns desired in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanel(ChessboardEntry[] initialData, int rows, int columns, 
			boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, rows, columns, allowMultiCellSelect, detailsProvider, containerServices, null);
	}
	
	/**
	 * This method initializes this object, the details of which should be uninteresting to 
	 * outside parties.
	 */
	@Override
	protected void initialize(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			final ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices, JPopupMenu popupMenu) 
	{
		 // SLH - commenting this out; this is for a plain chessboard (i.e. no buttons)
		tableModel = new ChessboardTableModel(initialData);
		table = new ChessboardTable(tableModel, allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
		table.setBackground(this.getBackground());
		table.setForeground(this.getForeground());
		this.setLayout(new GridLayout(1, 1));
		this.add(table);
	}	

	/**
	 * Method needed to reset the selected items in the chessboard to nothing selected.
	 */
    public void clearSelection(){
        if(table !=null){
            table.clearSelection();
        }
    }
}
