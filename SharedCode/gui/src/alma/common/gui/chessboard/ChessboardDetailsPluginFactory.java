package alma.common.gui.chessboard;

import alma.common.gui.chessboard.internals.ChessboardFrame;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * This interface is used to allow third-party users of chessboards to provide (to the chessboard infrastructure)
 * a means for the chessboard to display the 'details' about cells in a chessboard. The display of details would, 
 * typically, involve the display of a GUI component containing details about a cell, in response to a mouse click or double-click on an
 * individual cell in a chessboard, although it could also occur potentially in response to a menu choice, a
 * button click, or some other interaction with the user. When instantiating either <code>ChessboardFrame</code>
 * <code>ChessboardPanelWithButtons</code>, one will need to provide to the constructors an object that implements
 * this interface.
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardFrame
 * @see ChessboardPanelWithButtons
 *
 */
public interface ChessboardDetailsPluginFactory 
{
	/**
	 * Method to instantiate one ore more details plugin which will be used to display the details 
	 * about some aspect of an individual cell in a chessboard, in response to some interaction 
	 * (typically double-click) from the user.
	 * 
	 * @param names the names of the cells (typically Antenna names, for example) for which details are to
	 * be displayed.
	 * 
	 * @param containerServices the container services provided by the executive subsystem,
	 * which can be used, as needed, by the details provider.
	 * 
	 * @param listener a listener which is notified when this plugin closes; needed to allow the 
	 * child plugin to notify the owning chessboard panel when the child plugin is stopped.
	 * 
	 * @param pluginTitle the title of the plugin (to be used in the title bar of the plugin window on screen)
	 *        (should be passed to EXEC subsystem when calling the <code>PluginContainerServices</code>'s 
	 *        startChildPlugin method.)
     * 
     * @param selectionTitle the title of the cell that was selected in the chessboard when this details plugin was launched;
	 *        this may not be unique if, for example, multiple instances of a details display have been shown for a single 
	 *        cell in the chessboard; say, cell S1 might have selectionTitle value of: S1, 
	 *        and pluginTitle values of: S1 - 1, S1 - 2, S1 - 3, etc.

     * @return an array of plugin instances that should be displayed as a response to the method invocation.
	 */
	public ChessboardDetailsPlugin[] instantiateDetailsPlugin(String[] names, 
			PluginContainerServices containerServices, 
			ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle);
}


