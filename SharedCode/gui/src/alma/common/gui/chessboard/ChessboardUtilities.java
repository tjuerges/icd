package alma.common.gui.chessboard;

/**
 * The <code>ChessboardUtilities</code> class contains useful utilities for dealing with the
 * <code>ChessboardPanel</code> and its related classes. 
 * 
 * @author Steve Harrington
 */
public class ChessboardUtilities 
{
	/**
	 * Utility method to convert a 1-D array of <code>ChessboardEntry</code> objects to a 
	 * 2-D array, using a crude heuristic to try to make the chessboard as "square" as possible.
	 * 
	 * @param initialData a 1-D array representing the initial data for the chessboard.
	 * @return a 2-D array with the designated number of rows and columns.
	 */
	public static ChessboardEntry[][] convertOneDimensionalToTwoDimensional(ChessboardEntry[] initialData) 
	{	
		// Calculate something that will be approximately square in size, more or less
		// then convert the 1-D array to a 2-D array and proceed as normal
		
		// determine the number of rows
		int oneDimensionalArraySize = initialData.length;
		double squareRoot = Math.sqrt(oneDimensionalArraySize);
		int rows = (int)Math.floor(squareRoot);
		
		// determine the number of columns
		double columnsDouble = (double)oneDimensionalArraySize / (double)rows;
		int columns = (int)Math.ceil(columnsDouble);
		
		
		//	now, convert the 1-D array to a 2-D array
		ChessboardEntry[][] twoDimensionalData = new ChessboardEntry[rows][columns];
		int count = 0;
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < columns; j++) {
				if(count < initialData.length)  {
					twoDimensionalData[i][j] = initialData[count++];
				} 
				else {
					twoDimensionalData[i][j] = null;
				}
			}
		}
		return twoDimensionalData;
	}
	
	/**
	 * Utility method to convert a 1-D array of <code>ChessboardEntry</code> objects to a 2-D array.
	 * 
	 * @param initialData a 1-D array representing the initial data for the chessboard.
	 * @param rows the number of rows to use in the conversion process (for the 2-D array).
	 * @param columns the number of columns to use in the conversion (for the 2-D array).
	 * @return a 2-D array with the designated number of rows and columns.
	 */
	public static ChessboardEntry[][] convertOneDimensionalToTwoDimensional(ChessboardEntry[] initialData, int rows, int columns) 
	{	
		// convert the 1-D array to a 2-D array
		ChessboardEntry[][] twoDimensionalData = new ChessboardEntry[rows][columns];
		int count = 0;
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < columns; j++) {
				if(count < initialData.length)  {
					twoDimensionalData[i][j] = initialData[count++];
				} 
				else {
					twoDimensionalData[i][j] = null;
				}
			}
		}
		return twoDimensionalData;
	}
}
