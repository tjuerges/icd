package alma.common.gui.chessboard;

import java.awt.Color;

/**
 * Class (employing the typesafe enum pattern) for the status of an entry in a chessboard. 
 * This enum maps each status to a display color for use in the chessboard, as well as 
 * indicating whether a status should 'flash'. <br><br>
 * 
 * NOTE: it is a conscious choice to use the typesafe enum pattern (i.e. a class) rather 
 * than Java 5.0's builtin (and superior in most cases) enum mechanism because this will 
 * allow more flexibility for others to extend the enumerated types for custom uses of the 
 * chessboard - for example if the status values are different in some contexts (such as
 * Scheduling).<br><br>
 * 
 * To use your own custom status values, you will need to create your own class, e.g. 
 * MyChessboardStatus, which implements the <code>ChessboardStatus</code> interface. 
 * You will then be able to use the various pieces of the chessboard, e.g. 
 * <code>ChessboardFrame</code> and/or <code>ChessboardPanelWithButtons</code> with your status 
 * values/colors, by creating <code>ChessboardEntry</code> items using your own status class
 * rather than this default status class. It is recommended that you follow the pattern
 * shown in this class if you create your own status class; specifically, using a private 
 * constructor with a fixed number of (i.e. enumerated) public final static instances. This
 * is known as the 'typesafe enum pattern' advocated by Josh Bloch in 'Effective Java'
 * and was widely used prior to Java 5.0's builtin enum. It is still useful for our purposes.
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardStatus
 */

public class DefaultChessboardStatus implements ChessboardStatus
{
	private static final String ANTENNA_NOT_INSTALLED_STRING = "ANTENNA_NOT_INSTALLED";
	private static final String ANTENNA_NOT_IN_ARRAY_STRING = "ANTENNA_NOT_IN_ARRAY";
	private static final String DEVICE_NOT_INSTALLED_STRING = "DEVICE_NOT_INSTALLED";
	private static final String NORMAL_STRING = "NORMAL";
	private static final String OFFLINE_STRING = "OFFLINE";
	private static final String INFO_STRING = "INFO";
	private static final String WARNING_STRING = "WARNING";
	private static final String SEVERE_ERROR_STRING = "SEVERE_ERROR";
	private static final String FATAL_ERROR_STRING = "FATAL_ERROR";
	
	private String description;
	private Color bgColor;
	private Color fgColor;
	private boolean colorFlashes;
	private boolean isSelectable = true;
	private static ChessboardStatus[] values = null;
	
	/**
	 * Private constructor enforces typesafe enum pattern and
	 * makes it impossible for status values other than those 
	 * allowed (i.e. the predefined public final static instances above)
	 * to be created.
	 * 
	 * @param name the name of the status
	 */
	private DefaultChessboardStatus(String name)
	{
		this.isSelectable = true;
		this.colorFlashes = false;
		this.description = name;
		
		// configure the status with proper color and/or flashing attributes
		if(name.equals(FATAL_ERROR_STRING)) {
			this.colorFlashes = true;
			this.bgColor = Color.RED;
			this.fgColor = Color.WHITE;
		} else if(description.equals(ANTENNA_NOT_INSTALLED_STRING)) {
			// use the default bg/fg colors
			this.bgColor = null;
			this.fgColor = null;
			this.isSelectable = false;
		} else if(description.equals(ANTENNA_NOT_IN_ARRAY_STRING)) {
			this.bgColor = Color.LIGHT_GRAY;
			this.fgColor = Color.DARK_GRAY;
			this.isSelectable = false;
		} else if(description.equals(DEVICE_NOT_INSTALLED_STRING)) {
			this.bgColor = Color.DARK_GRAY;
			this.fgColor = Color.WHITE;
			this.isSelectable = false;
		} else if(description.equals(SEVERE_ERROR_STRING)) {
			this.bgColor = Color.RED;
			this.fgColor = Color.WHITE;
		} else if(description.equals(WARNING_STRING)) {
			this.bgColor = Color.YELLOW;
			this.fgColor = Color.BLACK;
		} else if(name.equals(INFO_STRING)) {
			this.bgColor = Color.WHITE;
			this.fgColor = Color.BLACK;
		} else if(name.equals(OFFLINE_STRING)) {
			this.bgColor = Color.LIGHT_GRAY;
			this.fgColor = Color.DARK_GRAY;
		} else if(description.equals(NORMAL_STRING)) {
			this.bgColor = Color.GREEN;
			this.fgColor = Color.BLACK;
		}
	}
	
	// Create the fixed set of allowed instances (these will be the enum values).
	/**
	 * Status indicating that the antenna is not installed / not in the system.
	 */
	public static final DefaultChessboardStatus ANTENNA_NOT_INSTALLED = 
		new DefaultChessboardStatus(ANTENNA_NOT_INSTALLED_STRING);
	
	/**
	 * Status indicating that the antenna is installed, but not present in the selected array.
	 */
	public static final DefaultChessboardStatus ANTENNA_NOT_IN_ARRAY = 
		new DefaultChessboardStatus(ANTENNA_NOT_IN_ARRAY_STRING);
	
	/**
	 * Status indicating that the device of interest is not present in the antenna.
	 */
	public static final DefaultChessboardStatus DEVICE_NOT_INSTALLED = 
		new DefaultChessboardStatus(DEVICE_NOT_INSTALLED_STRING);
	
	/**
	 * Status indicating a fatal error - this is more severe than 'severe' error status.
	 */
	public static final DefaultChessboardStatus FATAL_ERROR = 
		new DefaultChessboardStatus(FATAL_ERROR_STRING); 
	
	/**
	 * Status indicating a severe error.
	 */
	public static final DefaultChessboardStatus SEVERE_ERROR = 
		new DefaultChessboardStatus(SEVERE_ERROR_STRING);
	
	/**
	 * Status indicating an error - less severe than 'severe' and 'fatal' errors.
	 */
	public static final DefaultChessboardStatus WARNING = 
		new DefaultChessboardStatus(WARNING_STRING);
	
	/**
	 * Status indicating some information is available.
	 */
	public static final DefaultChessboardStatus INFO = 
		new DefaultChessboardStatus(INFO_STRING);
	
	/**
	 * Status indicating the antenna is offline.
	 */
	public static final DefaultChessboardStatus ANTENNA_OFFLINE = 
		new DefaultChessboardStatus(OFFLINE_STRING);
	
	/**
	 * Status indicating normal operational status.
	 */
	public static final DefaultChessboardStatus NORMAL = 
		new DefaultChessboardStatus(NORMAL_STRING);
		
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the background color that should be used to render the status in the user interface.
	 */
	public Color getBgColor()
	{
		return this.bgColor;
	}
		
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the foreground color that should be used to render the background color for the status in the user interface.
	 */
	public Color getFgColor()
	{
		return this.fgColor;
	}
	
	/**
	 * Overriding generic toString method from <code>Object</code> to get
	 * something more user-friendly.
	 * 
	 * @return a string representation of this status which is the same as the status name.
	 */
	@Override
	public String toString()
	{
		return this.description;
	}
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the description of the status.
	 */
	public String getDescription()
	{
		return this.description;
	}
	
	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be rendered as flashing in the
	 * user interface (visual representation).
 	 */
	public boolean shouldFlash() 
	{
		return this.colorFlashes;
	}

	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be selectable in the UI.
	 */
	public boolean isSelectable()
	{
		return this.isSelectable;
	}
	
	/** 
	 * Required method of the <code>ChessboardStatus</code> interface. Returns
	 * all of the valid instances of this enum class. This is primarily used
	 * for testing at the present, could be useful in some other situations potentially.
	 * Method is synchronized because it employs a singleton
	 * for the values array. For an example of its use in a test setting, 
	 * see the <code>ChessboardTest</code> and <code>ExampleChessboardStatusProvider</code> classes.
	 * 
	 * @return the full set of statuses for this enum.
	 */
	public synchronized ChessboardStatus[] values() 
	{
		// use a singleton-style mechanism to return the values
		if(DefaultChessboardStatus.values == null)
		{
			DefaultChessboardStatus.values = new ChessboardStatus[9];

			DefaultChessboardStatus.values[0] = DefaultChessboardStatus.INFO;
			DefaultChessboardStatus.values[1] = DefaultChessboardStatus.WARNING;
			DefaultChessboardStatus.values[2] = DefaultChessboardStatus.SEVERE_ERROR;
			DefaultChessboardStatus.values[3] = DefaultChessboardStatus.ANTENNA_OFFLINE;
			DefaultChessboardStatus.values[4] = DefaultChessboardStatus.FATAL_ERROR;
			DefaultChessboardStatus.values[5] = DefaultChessboardStatus.NORMAL;
			DefaultChessboardStatus.values[6] = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;
			DefaultChessboardStatus.values[7] = DefaultChessboardStatus.ANTENNA_NOT_IN_ARRAY;
			DefaultChessboardStatus.values[8] = DefaultChessboardStatus.DEVICE_NOT_INSTALLED;
		}
		
		return DefaultChessboardStatus.values;
	}
}


