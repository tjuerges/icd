package alma.common.gui.chessboard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;

import alma.common.gui.chessboard.internals.ChessboardCellsSelectedListener;
import alma.common.gui.chessboard.internals.ChessboardPanelBase;
import alma.common.gui.chessboard.internals.ChessboardTable;
import alma.common.gui.chessboard.internals.ChessboardTableModel;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * The <code>ChessboardPanelWithButtons</code> can be (re-)used when a chessboard-style component is needed;
 * it contains a single chessboard (table) with buttons at the bottom, and implements the 
 * <code>ChessboardStatusListener</code> interface in order to be able to remain updated as the state of 
 * items represented in the chessboard change.<br><br>
 * 
 * NOTE: It is required that the names of the entries in a chessboard are unique (within
 * that chessboard).
 * 
 * @author Steve Harrington
 * 
 * @see ChessboardEntry
 * @see ChessboardStatusListener
 * @see JPanel
 */
public class ChessboardPanelWithButtons extends ChessboardPanelBase implements ChessboardCellsSelectedListener
{
	// TODO - serial version uid 
	private static final long serialVersionUID = 1L;
	private static final String REPLACE = "Replace";
	private static final String NEW = "New";
	private static final String CLEAR = "Clear";
	private JButton newButton;
	private JButton replaceButton;
	private JButton clearButton;
	
	/**
	 * The (preferred) constructor to create a <code>ChessboardPanelWithButtons</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelWithButtons(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices) 
	{
		this(allowMultiCellSelect, detailsProvider, statusProvider, containerServices, null);
	}
	
	/**
	 * The (preferred) constructor to create a <code>ChessboardPanelWithButtons</code> with cells/states 
	 * that are populated using the provided <code>ChessboardStatusProvider</code> instance.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param allowMultiCellSelect boolean indicating whether this chessboard should allow multi-selection or not.
	 * @param detailsProvider the <code>ChessboardDetailsPluginFactory which will be used to display details
	 * about individual cells in the chessboard panel, e.g. when a cell is clicked by the user.
	 * @param statusProvider the <code>ChessboardStatusProvider</code> which will provide the
	 * initial status, as well as updates, to the chessboard panel. NOTE: this value CANNOT be null;
	 * if a null is passed in, a NullPointerException will result.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param popupMenu a popup menu to be added to a right-click context menu.
	 */
	public ChessboardPanelWithButtons(boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, 
			ChessboardStatusProvider statusProvider, PluginContainerServices containerServices, JPopupMenu popupMenu) 
	{
		super(allowMultiCellSelect, detailsProvider, statusProvider, containerServices, popupMenu);
		this.table.setChessboardCellsSelectedListener(this);
	}
	
	/**
	 * The constructor to create a <code>ChessboardPanelWithButtons</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add as a right click context menu. 
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices, JPopupMenu menu) 
	{
		super(initialData, allowMultiCellSelect, detailsProvider, containerServices, menu);
		this.table.setChessboardCellsSelectedListener(this);
	}

	/**
	 * The constructor to create a <code>ChessboardPanelWithButtons</code> populated with the given
	 * initial data. NOTE: when using this constructor (and all of the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels. 
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices) 
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add to a right click context menu. 
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices, JPopupMenu menu)
	{
		super(initialData, allowMultiCellSelect, detailsProvider, containerServices, menu);
		this.table.setChessboardCellsSelectedListener(this);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data. NOTE: this constructor gives the user no control
	 * over the shape/size of the chessboard; an attempt is made to create one that is approximately
	 * square in shape, depending on the number of entries given as initialData. Thus, if you need
	 * complete control over the shape/size (number of rows and columns) in the chessboard, 
	 * you should use one of the other constructors. NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell.
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[] initialData, boolean allowMultiCellSelect, 
			ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, allowMultiCellSelect, detailsProvider, containerServices, null);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows in the chessboard.
	 * @param columns the number of columns in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 * @param menu a popup menu to add to a right click context menu.
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[] initialData, int rows, int columns, 
			boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices,
			JPopupMenu menu)
	{
		super(initialData, rows, columns, allowMultiCellSelect, detailsProvider, containerServices, menu);
		this.table.setChessboardCellsSelectedListener(this);
	}
	
	/** 
	 * Alternate constructor to create a <code>ChessboardPanelWithButtons</code> for use by those that prefer a 1-D array rather
	 * than a 2-D array for the initial data, but with the added ability to specify the number of rows and columns.
	 * NOTE: when using this constructor (and all the other constructors which take 
	 * ChessboardEntry arrays) one must register this class with any ChessboardStatusProvider's 
	 * that you wish to listen to, manually. The constructor which takes a <code>ChessboardStatusProvider</code> 
	 * does this connection for you, automatically, and is easier in that regard.
	 * 
	 * <br><br>NOTE: it is required that within a chessboard, the names of the cells are unique.<br><br>
	 * 
	 * @param initialData the data with which to populate the chessboard.
	 * @param rows the number of rows in the chessboard.
	 * @param columns the number of columns in the chessboard.
	 * @param allowMultiCellSelect indicates whether more than one cell can be selected at a time (true)
	 * or not (false) - it is expected that there will be some cases, for example in Scheduling, where multiple cell
	 * selection is warranted, while most uses of the chessboard may disable it.
	 * @param detailsProvider an object which implements the <code>ChessboardDetailsPluginFactory</code> interface which
	 * provides a means/method to display the details about a cell in the chessboard (typically displayed when
	 * a user double-clicks, for example); this parameter can be null, in which case a default dialog box will 
	 * be displayed with semi-generic information about the cell. 
	 * @param containerServices the container services that may be used, as needed, by the chessboard and/or 
	 * its sub/details panels.
	 */
	public ChessboardPanelWithButtons(ChessboardEntry[] initialData, int rows, int columns, 
			boolean allowMultiCellSelect, ChessboardDetailsPluginFactory detailsProvider, PluginContainerServices containerServices)
	{
		this(initialData, rows, columns, allowMultiCellSelect, detailsProvider, containerServices, null);
	}

	/**
	 * This method is overridden here to ensure that chessboard panels (with buttons) don't attempt
	 * to disable details - this wouldn't make sense given that the sole purpose of the buttons
	 * is to display details. As such, anyone wanting to disable details should use <code>ChessboardPanel</code>
	 * and not <code>ChessboardPanelWithButtons</code>.
	 * 
	 * @param enabled this value is ignored for the <code>ChessboardPanelWithButtons</code>!
	 */
	@Override
	public void setDetailsDisplayEnabled(boolean enabled)
	{
		// always pass true here because this class uses buttons and false would mean that the 
		// buttons would be useless. people wanting to disable details for a chessboard should 
		// actually be using ChessboardPanel and not ChessboardPanelWithButtons!
		table.setDetailsDisplayEnabled(true);
	}
	
	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		newButton.setEnabled(enabled);
		replaceButton.setEnabled(enabled);
	}
	
	/**
	 * This method initializes this object, the details of which should be uninteresting to 
	 * outside parties.
	 */
	@Override
	protected void initialize(ChessboardEntry[][] initialData, boolean allowMultiCellSelect, 
			final ChessboardDetailsPluginFactory detailsProvider, final PluginContainerServices containerServices,
			JPopupMenu popupMenu) 
	{
		if(null == detailsProvider) {
			throw new IllegalArgumentException("ChessboardPanelWithButtons requires non-null ChessboardDetailsPluginFactory; ChessboardPanel allows null.");
		}
		
		// build a chessboard with buttons
		GridBagLayout mainLayoutMgr = new GridBagLayout();
		this.setLayout(mainLayoutMgr);
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(1,1));
		tableModel = new ChessboardTableModel(initialData);
		table = new ChessboardTable(tableModel, allowMultiCellSelect, detailsProvider, containerServices, popupMenu);
		table.setBackground(this.getBackground());
		table.setForeground(this.getForeground());
		topPanel.add(table);
		
		GridBagConstraints topPanelConstraints = new GridBagConstraints();
		topPanelConstraints.gridx = 0;
		topPanelConstraints.gridy = 0;
		topPanelConstraints.weightx = 1;
		topPanelConstraints.weighty = 1;
		topPanelConstraints.fill = GridBagConstraints.BOTH;
		topPanel.setBorder(new EmptyBorder(0,0,0,0));
		this.add(topPanel, topPanelConstraints);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,2, 4, 0));
		buttonPanel.setBorder(new EmptyBorder(2,1,1,1));
		
		newButton = new JButton(NEW);
		buttonPanel.add(newButton);
		newButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(table.getSelectedEntries() != null && table.getSelectedEntries().length != 0)
				{
					String pluginTitle = table.getUniqueTitleForCurrentSelection();
					String selectionTitle = table.getCurrentSelectionString();
					
					ChessboardDetailsPlugin[] plugins = detailsProvider.instantiateDetailsPlugin(table.getSelectedNames(), 
							containerServices, table,
							pluginTitle, selectionTitle);
					
					showPlugins(plugins, pluginTitle);
				}
			}
		});
		
		replaceButton = new JButton(REPLACE);
		replaceButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(table.getSelectedEntries() != null && table.getSelectedEntries().length != 0)
				{
					String newPluginTitle = table.getCurrentSelectionString();
					ChessboardDetailsPlugin existingPlugin = table.peekDisplayedDetailsPlugins();
					if(null != existingPlugin) 
					{
						if(!existingPlugin.getSelectionTitle().equals(newPluginTitle)) {
							int numAlreadyDisplayed = table.getNumberOfDetailsDisplaysVisibleForTitle(newPluginTitle);
							if(numAlreadyDisplayed > 0) {
								newPluginTitle += " - ";
								newPluginTitle += Integer.toString(numAlreadyDisplayed);
							} else {
								newPluginTitle += " ";
							}
							table.incrementNumberOfDetailsDisplaysVisibleForTitle(table.getCurrentSelectionString());
							table.decrementNumberOfDetailsDisplaysVisibleForTitle(existingPlugin.getSelectionTitle());
							existingPlugin.setUniqueName(newPluginTitle);
							existingPlugin.setSelectionTitle(table.getCurrentSelectionString());
						}	
						existingPlugin.replaceContents(table.getSelectedNames());
					} 
					else 
					{
						ChessboardDetailsPlugin[] newPlugins = detailsProvider.instantiateDetailsPlugin(table.getSelectedNames(), 
								containerServices, table, newPluginTitle, newPluginTitle);
						
						showPlugins(newPlugins, newPluginTitle);
					}
					table.clearSelection();
				}
			}
		});
		buttonPanel.add(replaceButton);
		
		clearButton = new JButton(CLEAR);
		buttonPanel.add(clearButton);
		clearButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				table.clearSelection();
			}
		});
		
		GridBagConstraints bottomPanelConstraints = new GridBagConstraints();
		bottomPanelConstraints.gridheight = GridBagConstraints.REMAINDER;
		bottomPanelConstraints.gridy = 1;
		bottomPanelConstraints.weightx = 0;
		bottomPanelConstraints.weighty = 0;
		bottomPanelConstraints.fill = GridBagConstraints.NONE;
		bottomPanelConstraints.anchor = GridBagConstraints.NORTHWEST;
		this.add(buttonPanel, bottomPanelConstraints);
	}

	/**
	 * Required method of the <code>ChessboardCellsSelectedListener</code> interface.
	 */
	public void nothingSelected() {
		this.newButton.setEnabled(false);
		this.replaceButton.setEnabled(false);
		this.clearButton.setEnabled(false);
	}

	/**
	 * Required method of the <code>ChessboardCellsSelectedListener</code> interface.
	 */
	public void somethingSelected() {
		this.newButton.setEnabled(true);
		this.replaceButton.setEnabled(true);
		this.clearButton.setEnabled(true);
	}
	
	private void showPlugins(ChessboardDetailsPlugin[] plugins, String pluginTitle)
	{
		int count = 0;
		for(ChessboardDetailsPlugin plugin: plugins) {
			pluginTitle += "." + Integer.toString(count++);
			table.showPlugin(plugin, pluginTitle);
		}
	}
}


