/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) Associated Universities Inc., 2002 
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/
package alma.common.gui.chessboard;

import javax.swing.JPanel;

import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

/**
 * Abstract class which should be extended by the details displays that
 * are shown when a user asks for "details" on an individual cell in a chessboard.

 * @author Steve Harrington
 * 
 * @see SubsystemPlugin
 * @see JPanel
 * @see ChessboardDetailsPluginFactory
 */
public abstract class ChessboardDetailsPlugin 
	extends JPanel implements SubsystemPlugin 
{
	protected ChessboardDetailsPluginListener owningChessboard;
	protected PluginContainerServices pluginContainerServices;
	protected String uniqueName;
	protected String selectionTitle;
	protected String[] antennaNames;
	
	// TODO - something with the serialVersionUID?
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param antennaNames the names of the antennas that were selected
	 * @param owningChessboard the chessboard which 'owns' this plugin 
	 * (in other words, the chessboard which launched this details plugin).
	 * @param services the plugin container services.
	 * @param pluginTitle the unique title of the plugin (to be used in the title bar of the plugin window on screen) and
	 *        by the docking framework of EXEC to give the plugin a name which must be unique.
	 * @param selectionTitle the title of the cell that was selected in the chessboard when this details plugin was launched;
	 *        this may not be unique if, for example, multiple instances of a details display have been shown for a single 
	 *        cell in the chessboard; say, cell S1 might have selectionTitle value of 'S1', pluginTitle values of:
	 *        'S1 - 1', 'S1 - 2', 'S1 - 3', etc.
	 */
	public ChessboardDetailsPlugin(String antennaNames[], ChessboardDetailsPluginListener owningChessboard,
			PluginContainerServices services, String pluginTitle, String selectionTitle)
	{
		this.owningChessboard = owningChessboard;
		this.pluginContainerServices = services;
		this.uniqueName = pluginTitle;
		this.selectionTitle = selectionTitle;
		this.antennaNames = antennaNames;
		assert pluginTitle != null;
		assert selectionTitle != null;
	}
	
	/**
	 * Required method of the <code>SubsystemPlugin</code> interface.
	 */
	public void setServices(PluginContainerServices services) 
	{
		this.pluginContainerServices = services;
	}
	
	/**
	 * Returns a title string to be used on the title bar of the window shown for the plugin,
	 * for example "Optical Telescope" or "Power Supply" or whatever is appropriate for the 
	 * particular chessboard in question. 
	 * 
	 * @return string indicating the title for the plugin.
	 */
	public abstract String getPluginName();

	/**
	 * Subclasses must implement this to perform operations to start the plugin.
	 */
	public abstract void specializedStart() throws Exception;
	
	/**
	 * Subclasses must implement this to perform operations to shutdown/stop the plugin.
	 */
	public abstract void specializedStop();
	
	/**
	 * Subclasses must implement this to replace the contents of a previously displayed
	 * plugin display.
	 * 
	 * @param newAntennaNames the name(s) of the antennas to display over the previous display.
	 */
	public abstract void replaceContents(String[] newAntennaNames);
	
	/**
	 * Implementing this method as final so that no one can change/override. This
	 * means that subclasses must put their start logic into the specializedStart
	 * method.
	 * 
	 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin#start()
	 * @see alma.common.gui.chessboard.ChessboardDetailsPlugin#specializedStart()
	 */
	public final void start() throws Exception 
	{
		specializedStart();
		if(null != owningChessboard) {
			owningChessboard.pluginStarted();
		}
	}
	
	/**
	 * Implementing this method as final so that no one can change/override. This
	 * means that subclasses must put their stop logic into the specializedStop 
	 * method.
	 * 
	 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin#stop()
	 * @see alma.common.gui.chessboard.ChessboardDetailsPlugin#specializedStop()
	 */
	public final void stop() throws Exception 
	{
		if(null != owningChessboard) {
			owningChessboard.pluginClosing(this);
		}
		specializedStop();
	}

	/**
	 * Getter for the unique name of the plugin; this name will be used by the EXEC infrastructure (and JIDE docking framework)
	 * to give a 'title' to the dialog that is associated with the plugin and must be unique - such that if multiple instances
	 * of an item have a plugin associated, they must be made unique by naming them say "Optical telescope ALMA01 - 1", "Optical
	 * Telescope ALMA01 - 2", and so forth.
	 * 
	 * @return the unique name of the plugin
	 */
	public String getUniqueName() {
		return uniqueName;
	}
	
	/**
	 * Setter for the unique name of the plugin; this name will be used by the EXEC infrastructure (and JIDE docking framework)
	 * to give a 'title' to the dialog that is associated with the plugin and must be unique - such that if multiple instances
	 * of an item have a plugin associated, they must be made unique by naming them say "Optical telescope ALMA01 - 1", "Optical
	 * Telescope ALMA01 - 2", and so forth.
	 * 
	 * @param newName the (new) unique name of the plugin
	 */
	public void setUniqueName(String newName) {
		assert newName != null;
		uniqueName = newName;
	}

	/**
	 * Getter for the selection title - this is not necessarily a unique name depending on whether multiple instances of a given 
	 * cell in the chessboard have had details shown - for example, it might be "Optical Telescope ALMA01" where the actual plugin
	 * names (see uniqueName attribute) are "Optical Telescope ALMA01 - 1" and "Optical Telescope ALMA01 - 2" in the case of 2 details
	 * having been shown for a given cell.
	 * 
	 * @return the selection title, not necessarily unique.
	 */
	public String getSelectionTitle() {
		return selectionTitle;
	}
	
	/**
	 * Setter for the selection title - this is not necessarily a unique name depending on whether multiple instances of a given 
	 * cell in the chessboard have had details shown - for example, it might be "Optical Telescope ALMA01" where the actual plugin
	 * names (see uniqueName attribute) are "Optical Telescope ALMA01 - 1" and "Optical Telescope ALMA01 - 2" in the case of 2 details
	 * having been shown for a given cell.
	 * 
	 * @param newTitle the selection title, not necessarily unique.
	 */
	public void setSelectionTitle(String newTitle) {
		this.selectionTitle = newTitle;
	}

}
