/*
 * Created on Jul 12, 2007 by mschilli
 */
package alma.common.log;

import java.util.logging.LogManager;


/**
 * Turns an Exception into a String so it can be easily logged.
 * 
 * <p> <b>Sample usage </b><br>
 * <pre>
 * [...]
 * catch (Exception exc) {
 *    logger.severe ("failed to foo internal bar" + ExcLog.details(exc));
 * }
 * </pre></p>
 * 
 * <p><b> Enable / Disable </b><br>
 * The stacktraces can be enabled or disabled at run-time through
 * both a java log property or a java system property. By default,
 * they are disabled. Put
 * <tt>alma.common.log.ExcLog=true</tt> <ul> 
 * <li> either into your java.util.logging.config file
 * <li> or into your $JAVA_OPTIONS variable (prepend with "-D")
 * </ul>
 * If either reads "true" (case-insensitive),
 * stacktraces will be enabled.
 * <br>
 * You can also redefine these properties on-the-fly, either programmatically
 * (like <code>System.setProperty("alma.common.log.ExcLog", "true");</code>),
 * or using the VM Tool that come with ACS (see acsStartJava -h).
 * </p>
 * 
 * 
 * @author mschilli
 */
public class ExcLog {


	// Static stuff, for convenience
	// ===========================================================
	
	private static final ExcLog global = new ExcLog("alma.common.log.ExcLog");
	
	/**
	 * This turns the given Throwable into a String that can simply be logged.
	 * <p>
	 * The verboseness of this method is system-widely enabled or
	 * disabled using either the log property or the system property 
	 * <code>alma.common.log.ExcLog</code>.</p>
	 */
	public static String details (Throwable thrown) {
		return global.toDetails(thrown);
	}

	// Instance
	// ===========================================================
	
	protected String propertyName;
	
	/**
	 * Allows to create a proprietary instance, which can then be
	 * enabled or disabled through the specified property name.
	 * 
	 * @param propertyName  the property which controls this instance
	 */
	protected ExcLog (String propertyName) {
		this.propertyName = propertyName;
	}

	protected boolean isEnabled () {
		return "true".equalsIgnoreCase(LogManager.getLogManager().getProperty(propertyName))
				|| "true".equalsIgnoreCase(System.getProperty(propertyName));
	}
	
	protected String toDetails (Throwable thrown) {
		if (! isEnabled()) {
			return "";
		}

		return  "\n ~ In Thread: " + Thread.currentThread().getName()
				+ "\n ~ Throwable was: " + thrown.getClass().getName()
				+ toMessage(thrown)
				+ toStacktrace(thrown);
	}

	
	protected String toMessage (Throwable t) {
		String msg = t.getMessage();
		
		// Throwable does some magic if no real "message" was set:
		// it will create a "message" consisting of cause-name + cause-message.
		// this doesn't match my aestethic desires, so i try to undo it here.
		Throwable cause = t.getCause();
		if (cause != null && msg != null && msg.startsWith(cause.getClass().getName())) {
			msg = null;
		}
		
		// do some "formatting"
		return (msg == null) ? "" : ": " + msg;
	}
	
	
	protected String toStacktrace (Throwable t) {
		StringBuffer ret = new StringBuffer(1000);
		StackTraceElement[] s = t.getStackTrace();
		for (int i = 0; i < s.length; i++) {
			ret.append("\n ~   ");
			ret.append(s[i].toString());
		}
		
		Throwable cause = t.getCause();
		if (cause != null) {
			ret.append("\n ~ due to: "+ cause.getClass().getName());
			ret.append(toMessage(cause));
			ret.append(toStacktrace(cause));
		}
		
		return ret.toString();
	}

	
}


