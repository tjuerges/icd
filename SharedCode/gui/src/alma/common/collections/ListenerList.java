package alma.common.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * Little helper class that works with the IDENTITY relation. Also, this list is optimized
 * for fast reading (and it is thread-safe).
 * 
 * @author mschilli, originally written under EXEC
 */
public class ListenerList<L> {

	private ArrayList<L> listeners = new ArrayList<L>();
	private List<L> listenersReadonly = new ArrayList<L>(0);

	protected final Object lock = new Object();

	/**
	 * Be sure to call this only from synchronized sections.
	 * <p>
	 * This iterates backwards - under the assumption that typically younger listeners are
	 * removed earlier.
	 */
	protected final int indexOf (L listener) {
		int nListeners = listeners.size();
		for (int i = nListeners-1; i >= 0; i--) {
			if (listeners.get (i) == listener)
				return i;
		}
		return -1;
	}

	@SuppressWarnings("unchecked") // can't help it: clone() is an unchecked operation
	private void listenersModified() {
		listenersReadonly = Collections.unmodifiableList((ArrayList<L>) listeners.clone());
	}

	/**
	 * Adds a listener.
	 * 
	 * @param listener
	 */
	public void add (L listener) {
		synchronized (lock) {
			listeners.add(listener);
			listenersModified();
		}
	}

	/**
	 * Removes a listener if it's contained. Otherwise this call is ignored.
	 * 
	 * @param listener
	 */
	public void remove (L listener) {
		synchronized (lock) {
			int index = indexOf(listener);
			if (index > -1) {
				listeners.remove(index);
				listenersModified();
			}
		}
	}

	/**
	 * Returns an unmodifiable copy of the list of current listeners.
	 * 
	 * @return an unmodifiable copy of the list of current listeners.
	 */
	public List<L> getAll () {
		return listenersReadonly;
	}

	
	/**
	 * Returns the number of listeners currently contained.
	 * 
	 * @return the number of listeners currently contained.
	 */
	public int size() {
		return listenersReadonly.size();
	}


	/**
	 * Adds a listener if it's not yet contained.
	 * 
	 * @param listener
	 */
	public void addIfNotPresent (L listener) {
		synchronized (lock) {
			if (indexOf(listener) == -1) {
				add(listener);
			}
		}
	}

	/**
	 * Returns an unmodifiable copy of the list of current listeners, and removes all
	 * current listeners.
	 * 
	 * @return an unmodifiable copy of the list of current listeners
	 */
	public List<L> getAllClear () {
		synchronized (lock) {
			List<L> ret = listenersReadonly;
			listeners.clear();
			listenersModified();
			return ret;
		}
	}

}
