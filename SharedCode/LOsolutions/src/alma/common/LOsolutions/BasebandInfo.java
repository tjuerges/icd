/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import java.util.Vector;

import alma.Control.BasebandSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.SidebandPreference;
import alma.Control.SSBbasebandSpec;



// A class to encapsulate a pair containing [skyFreq, basebandIndex]
class FreqBBindex {
    double skyFreqHz;
    int index;
    FreqBBindex(double f, int i) {
        skyFreqHz = f;
        index     = i;
    }    
}    

// A class to encapsulate all the info about a baseband
class BasebandInfo {
    SSBbasebandInfo upper = null;
    SSBbasebandInfo lower = null;
    Vector<SSBbasebandInfo> sb = new Vector<SSBbasebandInfo>();
    SidebandSelect  sbSelect;
    boolean         isUsed   = false;
    int             index; // baseband index
    // SSB Use Case
    BasebandInfo(SSBbasebandSpec basebandSpec, int index) {
        upper          = new SSBbasebandInfo(basebandSpec);
        upper.sbSelect = SidebandSelect.Upper;
        upper.bbIndex  = index;
        lower          = new SSBbasebandInfo(basebandSpec);
        lower.sbSelect = SidebandSelect.Lower;
        lower.bbIndex  = index;
        sbSelect   = SidebandSelect.sidebandSelect(basebandSpec.sidebandPref);
        this.index = index;
        switch (sbSelect) {
            case Upper: removeLSB();
                        break;
            case Lower: removeUSB();
                        break;
            default:    reconstruct();
                        break;
        }               
    }   
    BasebandInfo(DSBbasebandSpec basebandSpec, int index) {
        upper          = new SSBbasebandInfo(basebandSpec.USB);
        upper.sbSelect = SidebandSelect.Upper;
        upper.bbIndex  = index;
        lower          = new SSBbasebandInfo(basebandSpec.LSB);
        lower.sbSelect = SidebandSelect.Lower;
        lower.bbIndex  = index;
        this.index     = index;
        reconstruct();  
        //spew("Make DSB, sbSel: " + sbSelect.toShortString());
    } 
    BasebandInfo(SSBbasebandInfo ssbBaseband) {
        index = ssbBaseband.bbIndex;
        if (ssbBaseband.sbSelect == SidebandSelect.Upper) {
            upper = new SSBbasebandInfo(ssbBaseband);
        }
        else {    
            lower = new SSBbasebandInfo(ssbBaseband);
        }
        reconstruct();
    }    
    BasebandInfo(int index) {
        upper          = new SSBbasebandInfo();
        upper.sbSelect = SidebandSelect.Upper;
        upper.bbIndex  = index;
        lower          = new SSBbasebandInfo();
        lower.sbSelect = SidebandSelect.Lower;
        lower.bbIndex  = index;
        this.index     = index;
        reconstruct();  
    } 
    void removeUSB() {
        upper = null;
        reconstruct();
    }    
    void removeLSB() {
        lower = null;
        reconstruct();
    } 
    boolean isDSB() {
        return (sbSelect == SidebandSelect.Both);
    }       
    // Returns sum of weights of both SB in DSB case
    double weight() {
        if (isDSB()) {
            return (upper.weight + lower.weight);
        }
        else {
            return (sb.elementAt(0).weight);
        }
    }    
    static boolean isOnlySSB(Vector<BasebandInfo> bbinfo) {
        for (BasebandInfo bb: bbinfo) {
            if (bb.isUsed && (bb.sbSelect == SidebandSelect.Both)) return false;
        }
        return true;
    }       
    // Updates internal structure; you can delete a baseband sb by
    // setting lower or upper to null or the freq to zero. This method
    // will then go through and make the rest of the object self-consistent.
    void reconstruct() {
        isUsed = false;
        sb.clear();
        int nSidebands = 0;
        if ((upper != null) && (upper.isUsed())) {
            sbSelect = SidebandSelect.Upper;
            nSidebands++;
            sb.add(upper);
            isUsed = true;
        }
        else {
            upper = null;
        }    
        if ((lower != null) && (lower.isUsed())) {
            sbSelect = SidebandSelect.Lower;
            nSidebands++;
            sb.add(lower);
            isUsed = true;
        }
        else {
            lower = null;
        }    
        if (nSidebands == 2) {
            if (Math.abs(upper.skyFreqHz - lower.skyFreqHz) < 1e6) {
                sbSelect   = SidebandSelect.Either; // Same freq
            }    
            else {  
                // Two diff freq's, one for each sideband
                sbSelect   = SidebandSelect.Both;  
            }    
        }
    } 
    static Vector<Double> freqVec(Vector<BasebandInfo> bbinfo) {
        Vector<Double> rtn = new Vector<Double>();
        for (BasebandInfo b: bbinfo) {
            if (b != null) {
                if (SidebandSelect.numSidebandFreqs(b.sbSelect) == 1) {
                    rtn.add(b.sb.elementAt(0).skyFreqHz);
                }
                else {    
                    for (SSBbasebandInfo s: b.sb) {
                        rtn.add(s.skyFreqHz);
                    }
                }
            }
        }
        return rtn;        
    } 
    // return baseband with specified index 
    static BasebandInfo basebandWithIndex(Vector<BasebandInfo> bbinfo, 
                                             int index) {
        for (BasebandInfo b: bbinfo) {
           if (b.index == index) return b;
        }
        return null;        
    }       
    // return a vector of all used single sidebands  
    static Vector<SSBbasebandInfo> sidebandVec(Vector<BasebandInfo> bbinfo) {
        Vector<SSBbasebandInfo> rtn = new Vector<SSBbasebandInfo>();
        for (BasebandInfo b: bbinfo) {
           if ((b != null) && b.isUsed) {   
                for (SSBbasebandInfo s: b.sb) {
                    if (s != null) rtn.add(s);
                }
            }
        }
        return rtn;        
    }   
    // return a vector of all unique single sidebands.
    // Basically turns either into upper.  
    static Vector<SSBbasebandInfo> uniqueSidebandVec(Vector<BasebandInfo> bbi) {
        Vector<SSBbasebandInfo> rtn = new Vector<SSBbasebandInfo>();
        for (BasebandInfo b: bbi) {
            if ((b != null) && b.isUsed) {   
                if (b.sbSelect == SidebandSelect.Either) {
                    if (b.upper != null) rtn.add(b.upper);
                    else if (b.lower != null) rtn.add(b.lower);
                }
                else if (b.sbSelect == SidebandSelect.Upper) {
                    if (b.upper != null) rtn.add(b.upper);
                }
                else if (b.sbSelect == SidebandSelect.Lower) {
                    if (b.lower != null) rtn.add(b.lower);
                }
                else if (b.sbSelect == SidebandSelect.Both) {
                    if (b.upper != null) rtn.add(b.upper);
                    if (b.lower != null) rtn.add(b.lower);
                }
            }
        }
        return rtn;        
    }   
    // Return a vector of all dsb basebands
    static Vector<BasebandInfo> dsbBasebandVec(Vector<BasebandInfo> bbinfo) { 
        Vector<BasebandInfo> dsbBB = new Vector<BasebandInfo>();
        for (BasebandInfo bb: bbinfo) {
             if ((bb != null) && bb.isUsed) {   
                 if (bb.sbSelect == SidebandSelect.Both)dsbBB.add(bb);
             }    
        }
        return dsbBB;
    }
    // Return a vector of all ssb basebands for specified sideband
    // Specific sideband should only be Upper, Lower or Both (not Either)
    // No DSB baseband sidebands are returned.
    static Vector<SSBbasebandInfo> ssbBasebandVec(Vector<BasebandInfo> bbinfo,
                                               SidebandSelect sbSel) 
             throws Exception { 
        if (sbSel ==  SidebandSelect.Either) {
            throw new Exception("Cannot request sideband Either" +
                                " for ssbBasebandVec()");  
        }                           
        Vector<SSBbasebandInfo> ssbBB = new Vector<SSBbasebandInfo>();
        for (BasebandInfo bb: bbinfo) {
           boolean isNotDSB = (bb.sbSelect != SidebandSelect.Both);
           if ((bb != null) && bb.isUsed && isNotDSB) {   
                for (SSBbasebandInfo s: bb.sb) {
                    if (s != null) {
                        if ((sbSel == SidebandSelect.Both) ||
                            (s.sbSelect == sbSel)) {
                                ssbBB.add(s);
                        }
                    }
                }
            }
        }
        return ssbBB;
    }

    // Return a vector of all basebands for specified sideband
    // Specified sideband should only be Upper, Lower or Both (not Either)
    // Both DSB basebands and SSB basebands are returned.
    static Vector<BasebandInfo> basebandVec(Vector<BasebandInfo> bbinfo,
                                            SidebandSelect sbSel) 
             throws Exception { 
        if (sbSel ==  SidebandSelect.Either) {
            throw new Exception("Cannot request sideband Either" +
                                " for basebandVec()");  
        }                           
        Vector<BasebandInfo> selbb = new Vector<BasebandInfo>();
        if (sbSel == SidebandSelect.Both) {
            // DSB case is simple
            for (BasebandInfo bb: bbinfo) {
                if ((bb != null) && bb.isUsed) selbb.add(bb);
            }
            return selbb;
        }
        
        // SSB case
        Vector<SSBbasebandInfo> ssbBB = ssbBasebandVec(bbinfo, sbSel);       
        for (SSBbasebandInfo sb: ssbBB) {
           selbb.add(new BasebandInfo(sb));
        }
        return selbb;
    }

    static Vector<SSBbasebandInfo> sidebandVec(Vector<BasebandInfo> bbinfo,
            SidebandSelect lo1sb) {
        Vector<SSBbasebandInfo> rtn = new Vector<SSBbasebandInfo>();
        for (BasebandInfo b: bbinfo) {
            if ((b != null) && b.isUsed ) {  
                //spew("Band" + b.index + " used, requesting " + lo1sb); 
                for (SSBbasebandInfo s: b.sb) {
                    if (s != null) { 
                        boolean match = true;
                        if ((lo1sb == SidebandSelect.Upper) || 
                            (lo1sb == SidebandSelect.Lower)) {
                            // Upper/Lower requested, do this match req?
                            match = (lo1sb == s.sbSelect);  
                        }
                        else {
                            // Both or Either requested, take anything
                            match = true; 
                        }    
                        if (match) {
                            rtn.add(s);
                        }
                    }    
                }
            }
        }
        return rtn;        
    }   
    static Vector<FreqBBindex> freqBBindexVec(Vector<BasebandInfo> bbinfo) {
        Vector<FreqBBindex> rtn = new Vector<FreqBBindex>();
        for (BasebandInfo b: bbinfo) {
            if (b != null) {
                if (SidebandSelect.numSidebandFreqs(b.sbSelect) == 1) {
                    SSBbasebandInfo s = b.sb.elementAt(0);
                    rtn.add(new FreqBBindex(s.skyFreqHz, b.index));
                }
                else {    
                    for (SSBbasebandInfo s: b.sb) {
                        rtn.add(new FreqBBindex(s.skyFreqHz, b.index));
                    }
                }
            }
        }
        return rtn;        
    } 

    static void spew(String s) {System.out.println(s); }  
}    
 
// Class used as a building block for BasebandInfo        
class SSBbasebandInfo {
    double             skyFreqHz;
    double             weight;
    double             ifFreqHz;
    SidebandSelect     sbSelect; // Only Upper or Lower
    int                bbIndex;
    
    SSBbasebandInfo() {
        skyFreqHz = 0.0;
    }     
    SSBbasebandInfo(BasebandSpec basebandSpec) {
        skyFreqHz = basebandSpec.skyFreqHz; 
        weight    = basebandSpec.weight;
        ifFreqHz  = basebandSpec.ifFreqHz;
    }     
    SSBbasebandInfo(SSBbasebandSpec ssbBasebandSpec) {
        skyFreqHz = ssbBasebandSpec.skyFreqHz; 
        weight    = ssbBasebandSpec.weight;
        ifFreqHz  = ssbBasebandSpec.ifFreqHz;
    }
    SSBbasebandInfo(SSBbasebandInfo ssbBB) {
        skyFreqHz = ssbBB.skyFreqHz; 
        weight    = ssbBB.weight;
        ifFreqHz  = ssbBB.ifFreqHz;
        sbSelect  = ssbBB.sbSelect;
        bbIndex   = ssbBB.bbIndex;
    } 
 
    boolean isUsed() {
        return (skyFreqHz > 1e6);
    }        
}          
         
