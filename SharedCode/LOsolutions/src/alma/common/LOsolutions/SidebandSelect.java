/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import alma.Control.SidebandPreference;

/**
 * Simple enumeration class to encapsulate sideband choices.
 */
public enum SidebandSelect {
    Upper,
    Lower,
    Either,  // Can be upper or lower, but not both at once
    Both,    // Both upper and lower must be used together
    Unknown;
    // Translate from SidebandPreference enums
    static SidebandSelect sidebandSelect(SidebandPreference sbPref) {
        if (sbPref == SidebandPreference.Upper)        return Upper;
        if (sbPref == SidebandPreference.Lower)        return Lower;
        if (sbPref == SidebandPreference.NoPreference) return Either;
        return Upper;
    } 
    static int numSidebandFreqs(SidebandSelect sbPref) {
        if (sbPref == Both) return 2;
        return 1;
    } 
    String toShortString() {
        switch(this) {
            case Upper:  return "U";                  
            case Lower:  return "L";                  
            case Either: return "2";                  
            case Both  : return "D";
        }
        return "?";
    }                          
}          
         
