/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import java.util.logging.Logger;
import java.util.Vector;

import alma.Control.TuningParameters;

/**
 * Extends the LOsolutions class for the peculiar case of the ATF
 */ 
public class ATFLOsolutions extends LOsolutions {
   
    // --------------------- Public interface --------------------------
    
    /**
     * Constructor
     * @param logger 
     */
    public ATFLOsolutions(Logger logger) {
        super(logger);
        systemName = "ATF";
    }

    // The ATF only wants lower sideband FTS1 (tuneHigh = false)
    protected void editSolutions(Vector<BasebandInfo> bbinfo, boolean verbose) 
            throws Exception {
        // Exit quietly if there are no solutions
        if (numberOfSolutions() == 0) return;    
        // Grab rx band arbitrarily from first solution 
        Band b = Band.band(tuningSolutions.elementAt(0).receiverBand);
        Vector<TuningParameters> tp = new Vector<TuningParameters>();
        for (TuningParameters t: tuningSolutions) {
            if (t.tuneHigh == false) tp.add(t);
        }
        tuningSolutions = tp;    
    } 
    // The ATF has 1stLO, 2LO, intermediateLO, and digitizer pseudoLO
    @Override
    protected int numLO() {
        // The ATF has 1stLO, 2LO, intermediateLO, and digitizer pseudoLO
        return 4; 
    }          
    // The ATF has an intermediate LO as a first downcoversion
    protected double intermediateLO(Band band) {
        return 12.408e9;
    }    

    // IF range values from Peter Napier email 2008-03-07
    @Override
    protected double IFlowFreq(Band band) {
        if (band == Band.Band3) return 4.0e9;
        else                    return super.IFlowFreq(band);
    }           
    @Override
    protected double IFhighFreq(Band band) {
        if (band == Band.Band3) return 9.6e9;
        else                    return super.IFhighFreq(band);
    } 
    // ATF limits from a Bill Shillue email dated 2007-12-27
    @Override
    protected double lodriverLowFreq(Band band) {
        if (band == Band.Band3) return 74e9;
        else                    return super.lodriverLowFreq(band);
    }    
    // High value changed from Bill's 88GHz after testing at the ATF 
    // by Ralph Marson on 2008-02-13.
    // The WCA on the AEC antenna does not lock above about 84GHz
    @Override
    protected double lodriverHighFreq(Band band) {
        if (band == Band.Band3) return 84e9;
        else                    return super.lodriverHighFreq(band);
    } 
    @Override
    protected SB sb(Band band) {
        if (band == Band.Band3) return SB.USB;
        else                    return super.sb(band);
    } 
    // The ATF only has band 3 and maybe band 6 support 
    @Override
    protected double bandLowFreq(Band band) {
        if (band == Band.Band3) return super.bandLowFreq(band);
        if (band == Band.Band6) return super.bandLowFreq(band);
        return 0.0;
    }   
    // From the sum of the LOdriver, LOint, IFhigh 
    @Override
    protected double bandHighFreq(Band band) {
        if (band == Band.Band3) {
            double hf = lodriverHighFreq(band) + 
                            intermediateLO(band) + 
                            IFhighFreq(band); 
            return hf;                
        }                    
        if (band == Band.Band6) return super.bandHighFreq(band);
        return 0.0;
    }     

//==========================================================================

    /**
     * Internal, just for testing
     */
    ATFLOsolutions() {
        super();
        systemName = "ATF";
    }

}
