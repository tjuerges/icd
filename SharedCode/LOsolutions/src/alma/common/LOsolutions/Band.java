/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import alma.ReceiverBandMod.ReceiverBand;

//------------------------------- BANDS --------------------------------
    // Enumeration of all bands and associated parameters
    // Eventually the parameters should be gotten from the TMCDB or from the
    // components. At that time, convert this to an object with the same name
    // and methods.  
    // When the data are changed remember to update the blockDiagramRevision
    // further down in the file. 
    // Band6 LO driver limits differ from the System Block Diagram which has
    // [73.7,88.3] which is not quite enough to cover the rx band.
    enum Band {
  //       receiverband           band               IF   warm cold lodriver
  //                            low   high     SB  low hi  mul  mul low    hi
Band1( ReceiverBand.ALMA_RB_01, 31.3, 45.0, SB.USB, 4, 12,  1,   1, 27.3, 33.0),
Band2( ReceiverBand.ALMA_RB_02, 67.0, 90.0, SB.LSB, 4, 12,  6,   1, 79.0, 94.0),
Band3( ReceiverBand.ALMA_RB_03, 84.0,116.0, SB.SB2, 4,  8,  6,   1, 92.0,108.0),
Band4( ReceiverBand.ALMA_RB_04,125.0,163.0, SB.SB2, 4,  8,  3,   2, 66.5, 77.5),
Band5( ReceiverBand.ALMA_RB_05,163.0,211.0, SB.SB2, 4,  8,  2,   6, 28.5, 34.0),
Band6( ReceiverBand.ALMA_RB_06,211.0,275.0, SB.SB2, 5, 10,  6,   3, 73.6, 88.4),
Band7( ReceiverBand.ALMA_RB_07,275.0,373.0, SB.SB2, 4,  8,  6,   3, 94.3,121.7),
Band8( ReceiverBand.ALMA_RB_08,385.0,500.0, SB.SB2, 4,  8,  3,   6, 65.5, 82.0),
Band9( ReceiverBand.ALMA_RB_09,602.0,720.0, SB.DSB, 4, 12,  3,   9, 67.8, 79.1),
Band10(ReceiverBand.ALMA_RB_10,787.0,950.0, SB.DSB, 4, 12,  6,   9, 83.3,104.7);
        
        private final ReceiverBand receiverBand;       // The interface enum
        private final double       bandLowFreq;        // Hz
        private final double       bandHighFreq;       // Hz
        private final SB           sb;
        private final double       IFlowFreq;          // Hz
        private final double       IFhighFreq;         // Hz
        private final short        warmMultiplier;
        private final short        coldMultiplier;
        private final double       lodriverLowFreq;    // Hz
        private final double       lodriverHighFreq;   // Hz
        private final ReceiverBand RB01 = ReceiverBand.ALMA_RB_01;
        
        //Constructor
        Band(ReceiverBand receiverBand,
                double bandLowFreq,     double bandHighFreq,
                SB sb,
                double IFlowFreq,       double IFhighFreq,
                int  warmMultiplier,    int coldMultiplier, 
                double lodriverLowFreq, double lodriverHighFreq) {
            this.receiverBand       = receiverBand;
            this.bandLowFreq        = bandLowFreq*1e9;
            this.bandHighFreq       = bandHighFreq*1e9;
            this.sb                 = sb;
            this.IFlowFreq          = IFlowFreq*1e9;
            this.IFhighFreq         = IFhighFreq*1e9;
            this.warmMultiplier     = (short)warmMultiplier;
            this.coldMultiplier     = (short)coldMultiplier;
            this.lodriverLowFreq    = lodriverLowFreq*1e9;
            this.lodriverHighFreq   = lodriverHighFreq*1e9;
        }
        static Band band(ReceiverBand receiverBand) 
                throws Exception {
            for (Band b: Band.values()) {
                if (b.receiverBand() == receiverBand) return b;
            }
            String msg = "Band for \'" + receiverBand + "\' not found";
            throw new Exception(msg);
        }        
        ReceiverBand receiverBand() {
            return receiverBand;
        }
        // The warm multiplier is not used by any software at this level
        short warmMultiplier() {
            return warmMultiplier;
        }
        short coldMultiplier() {
            return coldMultiplier;
        }
        double bandLowFreq() {
            return bandLowFreq;
        }
        double bandHighFreq() {
            return bandHighFreq;
        }
        double IFlowFreq() {
            return IFlowFreq;
        }
        double IFhighFreq() {
            return IFhighFreq;
        }
        double lodriverLowFreq() {
            return lodriverLowFreq;
        }
        double lodriverHighFreq() {
            return lodriverHighFreq;
        }
        // Hardware range of LO1
        double lo1HighFreq() {
            return coldMultiplier*lodriverHighFreq;
        }
        double lo1LowFreq() {
            return coldMultiplier*lodriverLowFreq;
        }
        SB sb() {
            return sb;
        }
        // All frequencies in GHz
        static String bandHardwareParameterString(
                    Band   band, 
                    double lowFreq,
                    double highFreq,
                    SB     sideband,
                    double IFlowFreq,
                    double IFhighFreq,
                    short  warmMultiplier,
                    short  coldMultiplier,
                    double lodriverLowFreq,
                    double lodriverHighFreq,
                    boolean extended) {
            String fmt = 
                "%-6s %5.1f %5.1f %3s %3.1f %4.1f %3d %4d  %4.1f %5.1f";
            String s = String.format(fmt,
                band,
                lowFreq*1e-9,
                highFreq*1e-9,
                sideband,
                IFlowFreq*1e-9,
                IFhighFreq*1e-9,
                warmMultiplier,
                coldMultiplier,
                lodriverLowFreq*1e-9,
                lodriverHighFreq*1e-9);
            if (extended) {
                fmt = "%s %5.1f %5.1f ";
                double IFoff = IFhighFreq;
                if (sideband == SB.USB) IFoff = -IFlowFreq;
                double lorangeLow  = (lowFreq + IFoff)/coldMultiplier;
                IFoff = -IFhighFreq;
                if (sideband == SB.LSB) IFoff = IFlowFreq;
                double lorangeHigh = (highFreq + IFoff)/coldMultiplier;
                s = String.format(fmt, s, lorangeLow*1e-9, lorangeHigh*1e-9);
            }   
            return s + '\n';
        }        
        // Return a string with a table of hardware values for all bands
        static String hardwareParameterString(LOsolutions loSolutions,
                boolean extended) {
            String s = Band.headerString(loSolutions, extended);
            for (Band b: Band.values()) {
                s += Band.bandHardwareParameterString(
                         b, 
                         loSolutions.bandLowFreq(b),
                         loSolutions.bandHighFreq(b),
                         loSolutions.sb(b),
                         loSolutions.IFlowFreq(b),
                         loSolutions.IFhighFreq(b),
                         loSolutions.warmMultiplier(b),
                         loSolutions.coldMultiplier(b),
                         loSolutions.lodriverLowFreq(b),
                         loSolutions.lodriverHighFreq(b),
                         extended);
            }              
            return s;
        }    
        // Return a string with a table of hardware values for all bands,
        // without extended content
        static String hardwareParameterString(LOsolutions loSolutions) {
            return hardwareParameterString(loSolutions, false);
        } 
        static String dump(Band band) {
            return bandHardwareParameterString(
                band,
                band.bandLowFreq(),
                band.bandHighFreq(),
                band.sb(),
                band.IFlowFreq(),
                band.IFhighFreq(),
                band.warmMultiplier(),
                band.coldMultiplier(),
                band.lodriverLowFreq(),
                band.lodriverHighFreq(), 
                false);
        }
        // Update this when necessary
        static String blockDiagramRev() {
            return "Q";
        }    
        static String systemDescriptionString(String systemName) {
            String s = "";
            s += "Hardware parameters, based on System Block Diagram Rev ";
            s += blockDiagramRev(); 
            s += ", for system '" + systemName + "'\n";
            return s;
        }
        static String headerString(String systemName, boolean extended) {
            String s = systemDescriptionString(systemName);
            s += columnHeadings(extended);
            return s;
        }
        static String columnHeadings(boolean extended) { 
            String s = "";   
            s += " Band     BandFreq  SB  IFfreq    Mult     LOdriver";
            if (extended) s += "   RxLOrange";
            s += "\n";
            s += "         low  high     low high warm cold  low  high";
            if (extended) s += "   low  high";
            s += "\n";
            return s;
        }
        static String headerString(LOsolutions loSolutions, boolean extended) {
            return headerString(loSolutions.systemName(), extended);
        }
        static String headerString(LOsolutions loSolutions) {
            return headerString(loSolutions, false);
        }
        static String dumpAll() {
            String s = systemDescriptionString("ALMA");
            s += columnHeadings(false);
            for (Band b: Band.values()) {
                s += dump(b);
            }
            return s;
        }
    }
    
    
//------------------------------ SIDEBANDS -----------------------------
    // Enumeration of sideband capabilities of the frontends
    enum SB {
        USB,
        LSB,
        SB2,
        DSB;
        public String toString() {
            if (this == SB2) return "2SB";
            return super.toString();
        }    
    }    
    
