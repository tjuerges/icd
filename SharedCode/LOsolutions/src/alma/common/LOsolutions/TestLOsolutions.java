/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import java.lang.Math;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import alma.Control.TuningParameters;
import alma.Control.LO2Parameters;

import alma.Control.Band2Band3Overlap; 
import alma.Control.SidebandPreference;
import alma.Control.BasebandSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.SSBbasebandSpec;


/// Class containing test routines, including a text based UI for
/// selecting and running tests.
class TestLOsolutions {
    private LOsolutions los = null;
      
    // All input baseband frequencies in GHz
    private short testComputeSolutions(double f1, double f2, 
                                       double f3, double f4, 
                                       boolean verbose,
                                       boolean dumpBB)
    {
        double[] f = new double[4];     
        double[] w = new double[4];     
        f[0] = f1;
        f[1] = f2;
        f[2] = f3;
        f[3] = f4;
        for (int i=0; i<4; i++) w[i] = 100.0;
        return testComputeSolutions(f, w, verbose, dumpBB);
    }                                       
    // All input baseband frequencies in GHz
    private short testComputeSolutions(double f1, double f2, 
                                       double f3, double f4, 
                                       boolean verbose)
    {
        boolean dumpBB = true;
        return testComputeSolutions(f1,f2,f3,f4, verbose, dumpBB);
    }                                       
    // All input baseband frequencies in GHz
    private short testComputeSolutions(double[] BBfreq, double[] BBweight,
                                       boolean verbose, 
                                       boolean dumpBB) {
        double[] f = new double[4];     
        Vector<SSBbasebandSpec> specs = new Vector<SSBbasebandSpec>();
        for (int i=0; i<4; i++) {
            f[i] = BBfreq[i] *1e9; 
            specs.add(LOsolutions.ssbBasebandSpec(f[i], BBweight[i], 0)); 
        }        
        try {
            return los.computeSolutions(specs, verbose, dumpBB);
        } catch (Exception e) {
            spew("Bummer: computeSolutions failed");
            e.printStackTrace();
        } 
        return 0;
    }
    // All input baseband frequencies in GHz
    private short testComputeSolutions(double[] BBfreq, double[] BBweight,
                                       boolean verbose) {
        boolean dumpBB = true;
        try {
            return testComputeSolutions(BBfreq,BBweight, verbose, dumpBB);
        } catch (Exception e) {
            spew("Bummer: computeSolutions failed");
            e.printStackTrace();
        } 
        return 0;
    }

    // Steps through all freqs in all bands
    void checkTuningTest(boolean verbose) {
        final boolean dumpBB = false;
        final double inc    = 1e6;  // Search increment in Hz 
        String fmt3  = "Testing with increment: %.3f MHz";
        for (Band b: Band.values()) {
            String str = "Starting " + b;
            spew(str);
            double f   = los.bandLowFreq(b)  + LOsolutions.BandOffset;   
            double top = los.bandHighFreq(b) - LOsolutions.BandOffset;   
            int nConsecutiveNoSols = 0;
            double botNoSol = 0;
            double topNoSol = 0;
            while (f <= top) {
                try {
                    double g=f*1e-9;
                    testComputeSolutions(g,0,0,0, verbose, dumpBB);
                    int nSols = los.numberOfSolutions();
                    String fmt2 = " freq: %7.3f  solutions:%3d";
                    //spew(String.format(fmt2, f*1e-9, nSols));
                    if (nSols <= 0) {
                        if (nConsecutiveNoSols == 0) {
                            botNoSol = f;
                            topNoSol = f;
                        }
                        else topNoSol = f;
                        nConsecutiveNoSols += 1;
                    }
                    else {
                        if (nConsecutiveNoSols == 1) {        
                            String fmt = "No solutions: %7.6f"; 
                            String s = String.format(fmt, 1e-9*botNoSol);
                            spew(s);
                        }    
                        if (nConsecutiveNoSols > 1) {        
                            String fmt = "No solutions: %7.3f  through %7.3f"; 
                            String s = String.format(fmt, 
                                           1e-9*botNoSol, 1e-9*topNoSol);
                            spew(s);               
                        } 
                        nConsecutiveNoSols = 0;   
                    }                 
                } catch (Exception e){
                    spew("Trouble: checkTuning, freq=" + 1e-9*f);
                    spew(e.toString());
                }
                f += inc;
            }
            if (nConsecutiveNoSols == 1) {        
                String fmt = "No solutions:  %7.6f"; 
                String s = String.format(fmt, 1e-9*f);
                spew(s);
            }    
            if (nConsecutiveNoSols > 1) {        
                String fmt = "No solutions: %7.3f  through %7.3f"; 
                String s = String.format(fmt, 1e-9*botNoSol, 1e-9*topNoSol);
                spew(s);               
           }               
        }
    } 
    // Sample some representative frequencies in each band
    void sampleTuningTest(boolean verbose, boolean physicalMode, 
                          boolean freqMode) {
        final boolean dumpBB = false;
        final double offset   =  200e6;  // Offset from band edge
        final double numsamps =  5;  // number of samples in a band
        Misc.tuningHeaders(physicalMode, freqMode); 
        for (Band b: Band.values()) {
            double f   = los.bandLowFreq(b)  + LOsolutions.BandOffset + offset;
            double top = los.bandHighFreq(b) - LOsolutions.BandOffset - offset;  
            double inc = (top - f)/(numsamps -1);
            while (f <= top) {
                try {
                    double g = f*1e-9;
                    testComputeSolutions(g,0,0,0, verbose, dumpBB);
                    int nSols = los.numberOfSolutions();
                    String fmt2 = " freq: %7.3f  solutions:%3d";
                    if (nSols <= 0) {
                        String fmt = "No solutions: %7.6f"; 
                        String s = String.format(fmt, 1e-9*f);
                        spew(s);
                    }
                    else {
                        short prefSol      = los.preferredTuningSolution();
                        TuningParameters t = los.getTuningSolution(prefSol);
                        los.dumpTuning(t, physicalMode, freqMode);        
                    }    
                } catch (Exception e){
                    spew("Trouble: checkTuning, freq=" + 1e-9*f);
                    spew(e.toString());
                }
                f += inc;
            }
        }
    } 
    
    // Do some tuning calcs for some freq specified in loops below
    void loopTest(boolean verbose, boolean physicalMode, boolean freqMode) {
        final boolean dumpBB = false;
        try {
            for (int i=0; i < 100; i++) {
                double f = 101 + i*0.001;
                String msg = "BBand0 " + f + " GHz ";
                try {
                    testComputeSolutions(100.00,f,102.00,0, verbose, dumpBB);
                    int nSols = los.numberOfSolutions();  
                    if (verbose & (nSols > 0)) {
                        los.dumpTunings(physicalMode, freqMode); 
                    }
                    spew(msg + " numTunings:" + nSols);
                    short prefSol      = los.preferredTuningSolution();
                    TuningParameters t = los.getTuningSolution(prefSol);
                    los.dumpTuning(t, physicalMode, freqMode);        
                    //sols.dumpFreqs();
                } catch (Exception e){ 
                    spew(msg + "failed!");
                    spew(e.toString());
                }
            }    
        } catch (Exception e) {
            spew("Bummer: " + e);
            e.printStackTrace();
        } 
    } 

    
    // Test the edges of band9
    void band9Test(boolean verbose, boolean physicalMode, boolean freqMode) {
        boolean dumpBB = false;
        if (freqMode) {
            Misc.freqHeader();
        }
        else {
            Misc.tuningHeaders();
        } 
        try {
            for (int i=603; i < 625; i++) {
                double f = i;
                try {
                    testComputeSolutions(f,0,0,0, verbose, dumpBB);
                    int nSols = los.numberOfSolutions();  
                    short prefSol      = los.preferredTuningSolution();
                    TuningParameters t = los.getTuningSolution(prefSol);
                    los.dumpTuning(t, physicalMode, freqMode);        
                    //sols.dumpFreqs();
                } catch (Exception e){ 
                    spew(i + " failed!");
                    spew(e.toString());
                }
            }    
            for (int i=696; i < 720; i++) {
                double f = i;
                try {
                    testComputeSolutions(f,0,0,0, verbose, dumpBB);
                    int nSols = los.numberOfSolutions();  
                    short prefSol      = los.preferredTuningSolution();
                    TuningParameters t = los.getTuningSolution(prefSol);
                    los.dumpTuning(t, physicalMode, freqMode);        
                    //sols.dumpFreqs();
                } catch (Exception e){ 
                    spew(i + " failed!");
                    spew(e.toString());
                }
            }    
        } catch (Exception e) {
            spew("Bummer: " + e);
            e.printStackTrace();
        } 
    } 

    // IF freq test helper 
    void ifTestHelper(int fskyLow, int fskyHigh) {
        boolean dumpBB = false;
        try {
            for (int i=fskyLow+1; i < fskyHigh; i++) {
                double f = 1.0*i;
                final String fmt = "%6.1f  %6.2f  %5.2f";
                try {
                    testComputeSolutions(f, 0, 0, 0, false, dumpBB);
                    short            pref = los.preferredTuningSolution();
                    TuningParameters t    = los.getTuningSolution(pref);
                    LO2Parameters    lo2p = t.LO2[0];
                    double ifFreq         = los.IFfrequency(lo2p);
                    double lo1freq        = los.LO1Freq(t);
                    spew(String.format(fmt, f, lo1freq*1e-9, ifFreq*1e-9));
                } catch (Exception e){ 
                    spew("Failed!");
                    spew(e.toString());
                }
            }    
        } catch (Exception e) {
            spew("Bummer: " + e);
            e.printStackTrace();
        } 
    }
    // Do some tuning calcs for some freq specified in loops below
    void tIFfreq(boolean verbose) {
        spew("  Fsky     Flo    Fif");
        ifTestHelper( 84, 116);
        spew("-------------");
        ifTestHelper(211, 275);
        spew("-------------");
        ifTestHelper(277, 373);
        spew("-------------");
        ifTestHelper(602, 720);
    }  
    
    // Helper method for errorTest()
    private void compErr(double[] f, double[] w, int n) {
        boolean dumpBB = false;
        double e = 100e6;
        try {
            short nSols = testComputeSolutions(f, w, verbose, dumpBB); 
            short p     = los.preferredTuningSolution();
            for (short t=0; t<nSols; t++) {
                e = Math.min(e, los.getTuningSolution(t).weightedError);
            }
            //e = Math.min(e, los.getTuningSolution(p).weightedError);
        } catch (Exception ex) {
            spew("Bummer: " + ex);
            ex.printStackTrace();
        } 
        int d1 = (int) Math.round(1e3*(f[1]-f[0]));
        int d2 = (int) Math.round(1e3*(f[2]-f[0]));
        int d3 = (int) Math.round(1e3*(f[3]-f[0]));
        String s2 = String.format("%4d", d2);
        String s3 = String.format("%4d", d3);
        if (n == 2) s2 = s3 = "   ";
        else if (n == 3) s3 = "   ";       
        String fmt = "%5.2f %4d %s %s"; 
        double err = 1e-6*e;
        // Only print the largest errors, which we have empirically determined
        if (((n==2)&&(err>3.4)) ||((n==3)&&(err>=5.7))||((n==4)&&(err>=5.75)))
            spew(String.format(fmt, err, d1, s2, s3));           
    }               
    // Check size of errors as a function of basebands and thier offsets
    void errorTest(boolean verbose) {
        spew("Large weighted ave err in MHz as a function" +
             " of baseband freq offsets (modulo 125MHz)");
        spew("The cases of 3 and 4 bands take quite a while to run");     
        final double baseFreq = 99.0;
        spew(String.format("Base frequency=%.3fGHz", baseFreq));
        spew("  Err  BB0  BB1  BB2  BB3");
        double[] f = new double[4];
        double[] w = new double[4];
        f[0] = baseFreq;
        w[0] = w[1] = 1;
        w[2] = w[3] = 0;
        for (int r=0; r<3; r++) {
            for (int i=1; i<125; i++) {
                f[1] = baseFreq + 1e-3*i;
                if (r == 0) {
                    compErr(f, w, 2);
                }    
                else {    
                    for (int j=i+1; j<125; j++) {
                        f[2] = baseFreq + 1e-3*j;
                        if (r == 1) {
                            w[2] = 1;
                            compErr(f, w, 3);
                        } 
                        else {   
                            for (int k=j+1; k<125; k++) {
                                f[3] = baseFreq + 1e-3*k;
                                w[3] = 1;
                                compErr(f, w, 4);
                            }
                        }
                    }
                }       
            } 
        }       
    }
    // We have some special shortcuts defined for common test freqs,
    // using the first few letters of the alphabet, e.g. 'a', 'b', ...
    // Otherwise, doubles separated by spaces are parsed from the string
    // A w in any string means it is a weight and is skipped
    private double[] parseFreqs(String in) 
            throws Exception {
        String s = in.trim();
        final int size = 4;
        double[] ret = new double[size];
        for (int i=0; i<size; i++) ret[i] = -1;
        // Special cases
        if (s.indexOf("aa") != -1) {
            ret[1] = 103.33375;
            return ret;
        }    
        else if (s.indexOf('a') != -1) {
            ret[0] = 103.33375;
            return ret;
        }    
        else if (s.indexOf('b') != -1) {
            ret[0] = 98.016;
            ret[1] = 98.044;
            ret[2] = 98.057;
            ret[3] = 98.099;
            return ret;
        }     
        else if (s.indexOf('c') != -1) {
            ret[0] = 98.016;
            ret[1] = 98.044;
            ret[2] = 98.057;
            ret[3] = 98.099;
            return ret;
        }     
        int i = 0;
        while (s.length() > 0) {
            int endOffset = s.indexOf(' ');
            String next = s;
            if (endOffset == -1) {
                s = "";
            }
            else {
                next = s.substring(0, endOffset);
                s    = s.substring(endOffset);
                s    = s.trim();
            }
            if (next.indexOf('w') == -1) {
                double f = Double.parseDouble(next); 
                if (i > (size-1)) 
                    throw new Exception("Only " + size + " freqs allowed");
                ret[i++] = f; 
            }      
        }  
        return ret;  
    } 
    
    private Vector<Double> parseFreqsVec(String in) 
            throws Exception {
        double f[] = parseFreqs(in);
        Vector<Double> v = new Vector<Double>();
        for (int i=0; i<f.length; i++) {
            if (f[i] > -1) v.add(f[i]);            
        }
        return v;
    }    

    private double[] parseWeights(String in) 
            throws Exception {
        String s = in.trim();
        final int size = 4;
        double[] ret = new double[size];
        for (int i=0; i<size; i++) ret[i] = 1.0;

        int i = 0;
        while (s.length() > 0) {
            int endOffset = s.indexOf(' ');
            String next = s;
            if (endOffset == -1) {
                s = "";
            }
            else {
                next = s.substring(0, endOffset);
                s = s.substring(endOffset);
                s = s.trim();
            }
            int indexOfW = next.indexOf('w');
            if (indexOfW != -1) {
                next = next.substring(0, indexOfW);
                double w = Double.parseDouble(next); 
                if (i > (size-1)) 
                    throw new Exception("Only " + size + " weights allowed");
                ret[i++] = w; 
            }      
        }  
        return ret;  
    } 
  
    private void interactiveTest(boolean verbose, boolean physical,
            boolean freqMode) {
        final boolean allSolutions   = true;
        boolean       keepGoing      = true;
        InteractiveInput input = new InteractiveInput();        
        spew("Type \'m\' to return to the menu at any time");        
        while (keepGoing) {
            try {
                System.out.print("Frequency(GHz); ");
                double[] f = new double[4];
                double[] w = new double[4];
                boolean validFreq = true;
                if (input.readAndCheckFor("mq")) {
                    keepGoing = false;
                    validFreq = false;
                }
                else {    
                    try {
                        input.computeSolutions(los, verbose);
                    }
                    catch (Exception e) {
                        final Writer result = new StringWriter();
                        final PrintWriter printWriter = new PrintWriter(result);
                        e.printStackTrace(printWriter);
                        String trace = result.toString();
                        spew("Trouble with input:\n" + e + "\n" + trace 
                              + "  (type q to quit)");
                        validFreq = false;
                    }
                }
                if (validFreq) {
                    //spew("Input string:" + s + "   double:" + f);
                    short nSols = los.numberOfSolutions();
                    if (nSols <= 0) {
                        spew("No solutions");
                    }
                    else {
                        String st = "Number of solutions: " + nSols + "     ";
                        st += "Preferred solution: " +
                                  los.preferredTuningSolution();
                        st += "  AveSkyFreq: ";
                        st += String.format("%.2f", 
                                            1e-9*los.averageSkyFrequency());
                        spew(st);
                        if (allSolutions) los.dumpTunings(physical, freqMode);
                        short prefSol      = los.preferredTuningSolution();
                        TuningParameters t = los.getTuningSolution(prefSol);
                        Misc.tuningHeaders(physical, freqMode);        
                        los.dumpTuning(t, physical, freqMode);        
                    }            
                }    
            }
            catch (Exception e) {
                spew("Exception:" + e);
            }
        }
    }
    
    private void t4(boolean verbose) {        
        double[] f = new double[4];
        double[] w = new double[4];
        for (int i=0; i<4; i++) w[i] = 1;
        for (int i=0; i<4; i++) f[i] = 100.0 + 0.05*i;
        f[0] = 103.33375;
        try {
            testComputeSolutions(f, w, verbose);
        } catch(Exception e) {
            spew ("Exception in testComputeSolutions: " + e);
        }                
    }
    
    private void printMenuOptions(boolean verbose, boolean physical) {
        spew("====================================================");
        spew("Current modes: ");
        spew("  " + (verbose?"Verbose":"not-Verbose(normal)"));
        spew("  " + (physical?"Hardware(physical)":"non-Hardware(logical)")) ;
        spew("  " + (freqMode?"Frequency":"non-Frequency(normal)"));
        spew("Options:");
        spew("---------------------- Mode control --------------------");
        spew(" v - verbose: output all tested tuning solutions for interactive");
        spew("!v - verbose: turn off verbose");
        spew(" f - frequency: output LO1 and IF frequencies");
        spew("!f - frequency: turn off frequency mode");
        spew(" h - hardware (physical) mode");
        spew("!h - hardware: turn off hardware mode -> logical basebands output");
        spew("---------------------- Tests --------------------");
        spew(" i - interactive: input frequenc(y/ies) - see parse below");
        spew(" a - all: tests all frequencies in all bands");
        spew(" e - Frequency error test");
        spew(" s - sampled: several sampled frequencies in each band");
        spew(" l - loop: hardcoded loop with solutions printed"); 
        spew(" t - test: test error messages"); 
        spew(" z - IFfreq: loop and print IF frequencies");
        spew(" 9 - band9: test edges of band9");
        spew("----------------------- Misc --------------------");
        spew(" p - parse: print out parsing rules for interactive input");
        spew(" d - dump: print band parameters");
        spew(" q - quit: exit");
        spew("====================================================");
    } 
    static boolean physicalMode = false;
    static boolean verbose      = false;
    static boolean freqMode     = false;   
    private void menu() {
        los = new LOsolutions(); 
        boolean keepGoing = true;
        printMenuOptions(verbose, physicalMode);
        BufferedReader userInput = 
                new BufferedReader(new InputStreamReader(System.in));
        while (keepGoing) {
            spew("Type \'m\' to see menu options");
            String s = "";
            try {
                System.out.print("Option: ");
                s = userInput.readLine().toLowerCase();
            } catch (Exception e) {
                spew(e.toString());
            }        
            if (s.indexOf('q') == 0) {
                keepGoing = false;
                spew("Quitting");
            }
            else if (s.indexOf('m') == 0) {
                printMenuOptions(verbose, physicalMode);
            }
            else if (s.indexOf("!v") == 0) {
                spew("Clearing verbose mode");
                verbose = false;
            }
            else if (s.indexOf('v') == 0) {
                spew("Setting verbose mode");
                verbose = true;
            }
            else if (s.indexOf("!f") == 0) {
                spew("Clearing frequency mode");
                freqMode = false;
            }
            else if (s.indexOf('f') == 0) {
                spew("Setting frequency mode");
                freqMode = true;
            }
            else if (s.indexOf('i') == 0) {
                interactiveTest(verbose, physicalMode, freqMode);
            }
            else if (s.indexOf('a') == 0) {
                checkTuningTest(false);
            }
            else if (s.indexOf('e') == 0) {
                errorTest(verbose);
            }
            else if (s.indexOf('s') == 0) {
                sampleTuningTest(false, physicalMode, freqMode);
            }
            else if (s.indexOf('l') == 0) {
                loopTest(false, physicalMode, freqMode);
            }
            else if (s.indexOf("!h") == 0) {
                spew("Clearing hardware (physical) mode; now in logical mode");
                physicalMode = false;
            }
            else if (s.indexOf('h') == 0) {
                spew("Setting hardware (physical) mode");
                physicalMode = true;
            }
            else if (s.indexOf('d') == 0) {
                boolean extendedResults = true;
                spew(los.hardwareParameterString(extendedResults));
            }
            else if (s.indexOf('p') == 0) {
                spew(InteractiveInput.printParsingRules());
            }
            else if (s.indexOf('t') == 0) {
                t4(verbose);
            }
            else if (s.indexOf('9') == 0) {
                band9Test(verbose, physicalMode, freqMode);
            }
            else if (s.indexOf('z') == 0) {
                tIFfreq(verbose);
            }
            else {
                spew("Input not understood.");
            }
            
        }
    } 
        
    void spew(String s) {
        System.out.println(s);
    }           
//----------------------------------- MAIN ---------------------------------
    // Used for running tests. To run these tests.
    // 1. make all
    // 2. cd CONTROL/Array/object/ControlArray
    // 3. acsStartJava -endorsed alma.Control.Array.LOsolutions
    public static void main(String[] args) {
        LOsolutions los = new LOsolutions();
        los.spew(los.hardwareParameterString());
        TestLOsolutions tlo = new TestLOsolutions();
        tlo.menu();
    }
}

//------------------------ Keyboard input processing --------------------------
// parses keyboard input for freq, weights, etc
// Rules:
//  input consists of:
//    square braces, e.g. '[' or ']'
//    numbers followed by single chars 'ulo'
//    white space or comma separators
//  freq is specified as number followed by 'u' or 'l' or nothing,
//    with nothing meaning SSB NoPreference or DSB both sidebands
//  baseband spec is:
//    freq or, [freq,weight] or, [freq,weight,ifFreq]
//  DSB spec is freqs only in braces or two levels of braces:
//    [f] or, [fu] or, [fu fl] or, [fl fu] or, [[fl w] fu], [[fu w][fl w]] etc
//  Band2Band3Overlap is spec'ed with:
//    2o or 3o, with 0o=autochoice
class InteractiveInput {
    private BufferedReader userInput_;
    private String  in_    = null;
    private boolean isSSB_ = false;
    private Band2Band3Overlap band2band3olap_;
    private Vector<SSBbasebandSpec> ssbBasebandSpecVec_;
    private Vector<DSBbasebandSpec> dsbBasebandSpecVec_;
    // From System.in
    InteractiveInput() {
        userInput_ = new BufferedReader(new InputStreamReader(System.in));
        ssbBasebandSpecVec_ = new Vector<SSBbasebandSpec>();
        dsbBasebandSpecVec_ = new Vector<DSBbasebandSpec>();
    }
    // From specified input
    InteractiveInput(InputStream in) {
        userInput_ = new BufferedReader(new InputStreamReader(in));
        ssbBasebandSpecVec_ = new Vector<SSBbasebandSpec>();
        dsbBasebandSpecVec_ = new Vector<DSBbasebandSpec>();
    }
    void read()  throws Exception {
        in_ = userInput_.readLine().toLowerCase().replace(',', ' ');
        in_ = in_.trim();        
    }
    // Checks to see if any of a set of characters are present in input
    boolean readAndCheckFor(String chars) throws Exception {
        read();
        for (int i=0; i<chars.length(); i++) {
            if (in_.indexOf(chars.charAt(i)) != -1) return true;
        }
        return false;    
    }
    boolean isSSB() throws Exception {
        parse();
        return isSSB_;
    }
    boolean isDSB() throws Exception {
        return !isSSB();
    }  
    short computeSolutions(LOsolutions los, boolean verbose) throws Exception {
        short r = 0;
        if (verbose) Misc.tuningHeaders();  
        if (isSSB()) {
            r = los.computeSolutions(ssbBasebandSpecVec_, 
                                     band2band3olap_, verbose);
        }
        else {
            r = los.computeSolutionsDSB(dsbBasebandSpecVec_, 
                                        band2band3olap_, verbose);
        } 
        return r;                           
    }
    static String printParsingRules() {
        String s = "";
        s += ">>Input consists of:\n";
        s += ">>   -square braces, e.g. '[' or ']'\n";
        s += ">>   -numbers followed by single chars 'ulo'\n";
        s += ">>   -white space or comma separators\n";
        s += ">>Freq is a number followed by 'u' or 'l' or nothing,\n";
        s += ">>  with nothing meaning SSB NoPreference or DSB both sidebands\n";
        s += ">>Baseband spec is:\n";
        s += ">>  freq or, [freq,weight] or, [freq,weight,ifFreq]\n";
        s += ">>DSB spec is freqs only in braces or two levels of braces:\n";
        s += ">> [fu fl] or, [fl fu] or, [[fl w] fu], \n";
        s += ">. [[fu w][fl w]] or, [[fu w iffreq] fl], etc.\n";
        s += ">>Band2Band3Overlap is spec'ed with:\n";
        s += ">>    2o or 3o, with 0o=autochoice\n";  
        return s;  
    }
          
    private void parse() throws Exception {
        // Initialize
        isSSB_          = true;
        ssbBasebandSpecVec_.clear();
        dsbBasebandSpecVec_.clear();
        checkMatchedBraces();
        Vector<String> chunk = splitChunks(in_);
        // Get olap (if any) and remove that chunk, leaving only basebands
        band2band3olap_ = overlap(chunk);
        if (true) spew(inputSynopsis(chunk, band2band3olap_));
        Vector bbspec   = new Vector();
        for (String b: chunk) {
            bbspec.add(parseBaseband(b));
        }    
        for (Object b: bbspec) {
            if (isSSB_) {
                ssbBasebandSpecVec_.add((SSBbasebandSpec)b);
            }
            else {
                if (b instanceof SSBbasebandSpec) {
                    dsbBasebandSpecVec_.add(makeDSBbasebandSpec(b));
                }
                else {
                    dsbBasebandSpecVec_.add((DSBbasebandSpec)b);
                }
            }    
        }    
        if (true) spew(basebandSpecSynopsis());   
        //spew(in_);
    }
    private String basebandSpecSynopsis() {
        String s = "";
        String interline = "";
        for(SSBbasebandSpec spec: ssbBasebandSpecVec_) {
            s += interline + toString(spec);
            interline = "\n";
        }    
        for(DSBbasebandSpec spec: dsbBasebandSpecVec_) {
            s += interline + toString(spec);
            interline = "\n";
        }    
        return s;   
    }
    private String inputSynopsis(Vector<String> chunk, Band2Band3Overlap olap){
        String s = "  Input: ";
        for (String c: chunk) s += "'"+c+"' ";
        s += "   Overlap:" + olap;
        return s;
    }    
    private Object parseBaseband(String in) {
        // If it is a null, then that is what it stays
        if (in == "null") return null;
        // If it doesn't start with '[' then it is a standalone frequency
        if (!in.startsWith("[")) {
            return parseFreq(in);
        }
        // OK, strip off the first level of braces
        in = stripBraces(in);
        Vector<String> piece = splitChunks(in);
        int nPieces = piece.size();
        if (isDSBspec(piece)) {
            // This is a DSB config
            isSSB_ = false;
            if (in.indexOf("[") == -1) {
                // No more braces
                SSBbasebandSpec spec1 = parseFreq(piece.elementAt(0));
                if (nPieces == 1) {
                    return makeDSBbasebandSpecSSB(spec1);
                }
                SSBbasebandSpec spec2 = parseFreq(piece.elementAt(1));
                return makeDSBbasebandSpec(spec1, spec2);
            } 
            // Must be two pieces, at least one of which has braces
            String p1 = stripBraces(piece.elementAt(0));
            String p2 = stripBraces(piece.elementAt(1));
            SSBbasebandSpec spec1 = parseSSB(splitChunks(p1));
            SSBbasebandSpec spec2 = parseSSB(splitChunks(p2));
            return makeDSBbasebandSpec(spec1, spec2);
        }
        else {
            return parseSSB(piece);
        }        
    }
    String stripBraces(String in) {
        if (in.startsWith("[")) return in.substring(1, in.length()-1).trim();
        return in;
    }    
    // Strings with freq, [weight, [ifFreq]]
    SSBbasebandSpec parseSSB(Vector<String> piece) {
        String fString = piece.elementAt(0);
        SSBbasebandSpec spec = parseFreq(fString);
        switch (piece.size()) {
            case 3: 
                String ifString = piece.elementAt(2);
                spec.ifFreqHz = Double.parseDouble(ifString)*1e9;
            case 2:    
                String wString = piece.elementAt(1);
                spec.weight = Double.parseDouble(wString);
        }
        return spec;
    }
    //Checks to see if a set of pieces from inside braces is DSB.
    // If there is another brace then it is a 2nd level of braces, or
    // if both u/l are spec'ed. 
    private boolean isDSBspec(Vector<String> piece) {
        for (String p: piece) {
            if (p.indexOf("[") != -1) return true;
        }    
        if (piece.size() == 1) return false;
        if (piece.size() != 2) return false;
        return (specifiesSideband(piece.elementAt(0)) && 
                specifiesSideband(piece.elementAt(1)));
    }
    // Specifies a specific sideband
    private boolean specifiesSideband(String s) {
        SSBbasebandSpec spec = parseFreq(s);
        return (spec.sidebandPref != SidebandPreference.NoPreference);
    }    
    private String toString(SSBbasebandSpec spec) {
        String f = "  SSB: %7.3f   %5.1f   %6.3f   %s";
        return String.format(f, spec.skyFreqHz*1e-9, spec. weight,
                spec.ifFreqHz*1e-9, spec.sidebandPref);
    }
    private String toString(DSBbasebandSpec spec) {
        String f = "  DSB: %7.3f/%7.3f   %5.1f/%5.1f   %6.3f/%6.3f";
        return String.format(f, 
                spec.USB.skyFreqHz*1e-9, spec.LSB.skyFreqHz*1e-9,  
                spec.USB.weight,         spec.LSB.weight,
                spec.USB.ifFreqHz*1e-9,  spec.LSB.ifFreqHz*1e-9);
    }
    // Putting result into a SSBspec is convenient as there is a place for
    // the freq and sb pref. 
    private SSBbasebandSpec parseFreq(String in) {
        SidebandPreference sbpref = SidebandPreference.NoPreference;
        if (in.endsWith("u")) {
            sbpref = SidebandPreference.Upper;
            in = in.substring(0, in.length() - 1); // Strip off the 'u'
        }
        else if (in.endsWith("l")) {
            sbpref = SidebandPreference.Lower;
            in = in.substring(0, in.length() - 1);
        }
        return LOsolutions.ssbBasebandSpec(Double.parseDouble(in)*1e9, sbpref);
    }
    private BasebandSpec makeBasebandSpec(SSBbasebandSpec ssb) {
        BasebandSpec bb = new BasebandSpec();
        bb.skyFreqHz = ssb.skyFreqHz;
        bb.weight    = ssb.weight;
        bb.ifFreqHz  = ssb.ifFreqHz;
        return bb;
    }        
    private BasebandSpec makeBasebandSpec() {
        BasebandSpec bb = new BasebandSpec();
        bb.skyFreqHz = 0;
        bb.weight    = 0;
        bb.ifFreqHz  = 0;
        return bb;
    }                
    private DSBbasebandSpec makeDSBbasebandSpecSSB(SSBbasebandSpec ssbspec) {
        DSBbasebandSpec dsbspec = new DSBbasebandSpec();
        if (ssbspec.sidebandPref == SidebandPreference.Upper) {
            dsbspec.USB = makeBasebandSpec(ssbspec);
            dsbspec.LSB = makeBasebandSpec();
        }
        else if (ssbspec.sidebandPref == SidebandPreference.Lower) {
            dsbspec.USB = makeBasebandSpec();
            dsbspec.LSB = makeBasebandSpec(ssbspec);
        }
        else if (ssbspec.sidebandPref == SidebandPreference.NoPreference) {
            dsbspec.USB = makeBasebandSpec(ssbspec);
            dsbspec.LSB = makeBasebandSpec(ssbspec);
        }
        return dsbspec;                
    }    
    private DSBbasebandSpec makeDSBbasebandSpec(Object spec) 
            throws Exception {
        if (!(spec instanceof SSBbasebandSpec)) {
            throw new Exception("makeDSBbasebandSpec(): " + 
                        "input is not instance of SSBbasebandSpec");
        }
        return makeDSBbasebandSpecSSB((SSBbasebandSpec)spec);                 
    }    
            
    private DSBbasebandSpec makeDSBbasebandSpec(SSBbasebandSpec spec1, 
                                                SSBbasebandSpec spec2) {
        DSBbasebandSpec dsbspec = new DSBbasebandSpec();
     
        if (spec1.sidebandPref == SidebandPreference.Upper) {
            dsbspec.USB = makeBasebandSpec(spec1);
            dsbspec.LSB = makeBasebandSpec();
        }
        else if (spec1.sidebandPref == SidebandPreference.Lower) {
            dsbspec.USB = makeBasebandSpec();
            dsbspec.LSB = makeBasebandSpec(spec1);
        }
        else if (spec1.sidebandPref == SidebandPreference.NoPreference) {
            dsbspec.USB = makeBasebandSpec(spec1);
            dsbspec.LSB = makeBasebandSpec(spec1);
        }
        if (spec2.sidebandPref == SidebandPreference.Upper) {
            dsbspec.USB = makeBasebandSpec(spec2);
        }
        else if (spec2.sidebandPref == SidebandPreference.Lower) {
            dsbspec.LSB = makeBasebandSpec(spec2);
        }
        else if (spec2.sidebandPref == SidebandPreference.NoPreference) {
            dsbspec.USB = makeBasebandSpec(spec2);
            dsbspec.LSB = makeBasebandSpec(spec2);
        }
        return dsbspec;                
    }    
            

    // Break input into space or brace delimited chunks (usually basebands, olap)
    private Vector<String> splitChunks(String in) {
        Vector<String> c = new Vector<String>();
        String work = in;
        int next = nextPiece(work);
        boolean done = false;
        while (!done) {
            int len = work.length();
            String s;
            if (next == -1) {
                s = work.trim();
                done = true;
            }
            else {    
                s = work.substring(0, next).trim();
                work = work.substring(next).trim();
                next = nextPiece(work);
            }
            if (s.length() > 0) c.add(s);    
        }
        return c;
    }
    // Return index of start of next piece (may have whitespace in front)
    private int nextPiece(String s) {
        int next = -1;
        if (s.startsWith("[")) {
            next = indexOfMatchingBrace(s);
        }
        else {
            next = s.indexOf(' ');
        } 
        if (next != -1) next++;
        return next;
            
    }
    // Input string should start with opening brace
    private int indexOfMatchingBrace(String s) {
        int pos = 1; // skip over first char
        int nestingLevel = 1;
        while (nestingLevel != 0) {
            int indexOpen  = s.indexOf("[", pos);   
            int indexClose = s.indexOf("]", pos);   
            if ((indexOpen != -1) && (indexOpen < indexClose)) {
                nestingLevel++;
                pos = indexOpen+1;
            }
            else {
                nestingLevel--;
                if (nestingLevel == 0) return indexClose;
                pos = indexClose + 1;
            } 
        }
        return 1; // Erroneous, but need to return something
    }           
              
    // Returns overlap state, removing string where it was spec'ed (if any)
    private Band2Band3Overlap overlap(Vector<String> chunk) throws Exception {
        Band2Band3Overlap olap = Band2Band3Overlap.AutoChoice; // Default
        int index = 0;
        for (String c: chunk) {
            if (c.endsWith("o")) {
                c = c.substring(0, c.length()-1);
                int band;
                try {
                    band = Integer.parseInt(c);
                } catch(Exception e) {
                    String s = "Overlap specified with a string(";
                    s += c + ") that cannot";
                    s += " be converted to an integer";
                    throw new Exception(s);
                }
                switch(band) {
                    case 0: olap = Band2Band3Overlap.AutoChoice;
                            break;
                    case 2: olap = Band2Band3Overlap.Band2;
                            break;
                    case 3: olap = Band2Band3Overlap.Band3;
                            break;
                    default:
                        String s = "Overlap specified with a band number(";
                        s += band + ") that is not 2 or 3.";
                        throw new Exception(s);                    
                }         
                chunk.remove(index);
                return olap;    
            } 
            index++;    
        }
        return olap;
    }            
    private void checkMatchedBraces() throws Exception {
        int nOpenBrace  = countChar('[');
        int nCloseBrace = countChar(']');
        if (nOpenBrace != nCloseBrace) {
            throw new Exception("Number of '[' and ']' must be equal");
        }
    }
    // Counts number of occurrences of specific character
    private int countChar(int c) {
        int count = 0;
        int pos = 0;
        while(pos != -1) {
            pos = in_.indexOf(c, pos) ;
            if (pos != -1) count++;
            if (pos != -1) pos++;
        }
        return count;
    }
    private void spew(String s) {
        System.out.println(s);
    }    
}
