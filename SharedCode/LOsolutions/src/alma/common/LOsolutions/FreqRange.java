/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

/**
 * A simple helper class that holds two frequencies that represent a 
 * frequency range (upper freq, lower freq).
 */    
class FreqRange {
    double low;
    double high;
    FreqRange(double low, double high) {
        this.low  = low;
        this.high = high;
    } 
    FreqRange(double f) {
        low  = f;
        high = f;
    } 
    FreqRange() {
        low  = 1e20;
        high = 0;
    } 
    void add(double f) {
        if (f < low)  low  = f;   
        if (f > high) high = f; 
    }
    boolean isWithin(double f) {
        if (f < low)  return false;
        if (f > high) return false;
        return true;
    } 
    double span() {
        return high - low;
    }
    FreqRange intersection(FreqRange other) {
        double low  = Math.max(this.low,  other.low);
        double high = Math.min(this.high, other.high);
        return new FreqRange(low, high);
    }   
    boolean isValidIntersection() {
        if (span()  < 0) return false;
        return true;
    } 
    @Override   
    public String toString() {
        return toString(3);
    }         
    public String toString(int precision) {
        String fmt1 = String.format("%%%d.%df", 4+precision, precision);
        String fmt2 = String.format("[%s, %s]", fmt1, fmt1); 
        return String.format(fmt2, low*1e-9, high*1e-9);
    }         
}
    
class LOrange extends FreqRange {
    // The selected sky sideband for this LO
    SidebandSelect sbSelect = SidebandSelect.Upper;
    
    LOrange(double low, double high) {
        super(low, high);
    }
    LOrange(double f) {
        super(f);
    }
    LOrange() {
        super();
    }
    LOrange intersection(LOrange other) {
        double low   = Math.max(this.low,  other.low);
        double high  = Math.min(this.high, other.high);
        LOrange ret  = new LOrange(low, high);
        ret.sbSelect = this.sbSelect;
        return ret;
    }   
            
}

    
    
