/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import java.util.Vector;

import alma.Control.LO2Parameters;
import alma.Control.SkyFreqSideband;
import alma.Control.TuningParameters;
import alma.ReceiverBandMod.ReceiverBand;

class Misc {        
    static void spew(String s) {
        System.out.println(s);
    } 


    // ======================== Printing, formatting ======================== 
                                      
    /**
     *  Return a string for the tuning params for a single solution,
     *  single baseband.
     *  @param bbIndex index of baseband to be displayed (starts at 0)
     *  @param sb sideband info; if null, physical output is generated
     *  @param displayBB controls the 'BBx' at the start of the line
     */
    static String tuningString(TuningParameters t, int bbIndex,
                SSBbasebandInfo sb, boolean displayBB, boolean freqMode) {
        boolean dumpSidebandRanges = false;
        boolean physical = (sb == null);  
        LO2Parameters p = t.LO2[bbIndex];
        String FTS1tuneHigh = (t.tuneHigh)? "H":"L";
        String FTS2tuneHigh = (p.tuneHigh)? "H":"L";
        String f = "%6.3f %7.3f";
        f += "  %6.3f    %s    %7.3f  %d %7.3f    %s   %5.1f %5.2f";
        String bandString      = "error/";
        String blankBandString = "      ";
        try {
            bandString = Band.band(t.receiverBand).toString() + "/";
        } catch(Exception e){ /*Just swallow it*/ }   
        if (displayBB == false) {
            bandString = blankBandString;
            if (t.receiverBand == Band.Band10.receiverBand()) {
                bandString      += " ";
                blankBandString += " ";
            }
        }
        String output = String.format("%sBB%d: ", bandString, bbIndex);
        String ifpsb = "L";
        if (((bbIndex <= 1) && t.band0band1selectUSB) ||
            ((bbIndex >  1) && t.band2band3selectUSB)) ifpsb = "U";
        String usedstr = "Yes";
        if (!p.isUsed) usedstr = "No";     
        SidebandSelect sbsel = null;
        double actSkyFreq = 0.0;
        double loDriver   = 0.0;
        double lo1Freq    = 0.0;
        try {
            if (!physical) {
                actSkyFreq = LOsolutions.actualSkyFreq(t, bbIndex, sb.sbSelect);
            }
            loDriver   = LOsolutions.LOdriver(t);
            lo1Freq    = LOsolutions.LO1Freq(t);
        }  catch (Exception e) { 
            spew(e.toString()); 
        }              
        if (physical) {
            output += String.format("%3s %3s ", usedstr, ifpsb);
        }
        else {
             sbsel = sb.sbSelect;
             String sbstr = "X"; 
             if (sbsel == SidebandSelect.Upper) sbstr = "U";
             else                               sbstr = "L";
             if (freqMode) {
                 output += String.format("%7.3f  %s %7.3f %6.3f %5.1f %6.3f",
                           1e-9*sb.skyFreqHz,
                           sbstr,
                           1e-9*lo1Freq,
                           1e-9*LOsolutions.IFfrequency(p),
                           1e-6*t.weightedError,
                           t.score);
                if (dumpSidebandRanges) {
                    try {
                        output +=  String.format(" %7.3f %7.3f %7.3f %7.3f",
                                       1e-9*LOsolutions.getLSBlowFreq(t),
                                       1e-9*LOsolutions.getLSBhighFreq(t),
                                       1e-9*LOsolutions.getUSBlowFreq(t),
                                       1e-9*LOsolutions.getUSBhighFreq(t));
                    } catch(Exception e) {}
                }
             }
             else {
                 output += String.format("%7.3f %7.3f %5.1f %8.3f %s ",
                           1e-9*sb.skyFreqHz,
                           1e-9*actSkyFreq,
                           sb.weight,
                           1e-6*(actSkyFreq-sb.skyFreqHz), 
                           sbstr); 
            }
        }
        String s = String.format(
                           f,
                           1e-9*LOsolutions.LO2Freq(p), 
                           1e-9*t.LSFrequency, 
                           1e-6*t.FLOOGFrequency,
                           FTS1tuneHigh,
                           1e-9*loDriver,
                           t.coldMultiplier,
                           1e-6*t.LO2[bbIndex].FTSFrequency, 
                           FTS2tuneHigh,
                           1e-6*t.weightedError,
                           t.score); 
        if (!freqMode) output += s;
        return output;      
    }  
                                        
    /**
     *  Return vector of strings for the tuning params for a single solution,
     *  multiple basebands. Each baseband sideband is on a separate line.
     *  @param displayBB controls the 'BBx' at the start of the line
     */
    static Vector<String> tuningStringsLogical(TuningParameters t, 
                BasebandInfo bb, boolean displayBB, boolean freqMode) {

        Vector<String> lines = new Vector<String>();
          
        Vector<SSBbasebandInfo> sbands = new Vector<SSBbasebandInfo>();
        int bbIndex = bb.index;          
        if (t.LO2[bbIndex].skyFreqSideband == SkyFreqSideband.USB) {
            sbands.add(bb.upper);
        }    
        else if (t.LO2[bbIndex].skyFreqSideband == SkyFreqSideband.LSB) {
            sbands.add(bb.lower);
        }    
        else if (t.LO2[bbIndex].skyFreqSideband == SkyFreqSideband.DSB) {
            sbands = bb.sb;
        }    
        else {
            lines.add("Tuning soln sideband(" + 
                       t.LO2[bbIndex].skyFreqSideband +
                       ") not recognized");
            return lines;                     
        } 
        for (SSBbasebandInfo sb: sbands) {   
            lines.add(tuningString(t, bbIndex, sb, displayBB, freqMode));
        }                           
        return lines;
    }    

    // Print out a single tuning solution.
    // Can preceed output line with tuning solution index (inhibit with -1).
    // The physical parameter controls display of all BB as hw or only those 
    // that are input in the BasebandInfo vector and are used.
    static Vector<String>  tuningStrings(TuningParameters t, 
                    Vector<BasebandInfo> baseband, int solIndex, 
                    boolean physical, boolean freqMode) {
        Vector<String> lines = new Vector<String>();
        String prefix = String.format("%4d ", solIndex); 
        String subsequentPrefix = "     "; 
        if (solIndex < 0) {
            prefix = subsequentPrefix = "";
        } 
        boolean firstLine = true;    
        
        if (physical) {
            for (int bbi=0; bbi<4; bbi++) {
                String l = prefix;
                l += tuningString(t, bbi, null, firstLine, freqMode);
                lines.add(l);
                prefix = subsequentPrefix;
                firstLine = false;
            }
            return lines;   
        } 
        // Logical bb section
        if (baseband == null) {
            lines.add("baseband parameter is null");
            return lines;
        }
        for (BasebandInfo bb: baseband) {
            if (bb.isUsed) {
                Vector<String> logicalLines = 
                    tuningStringsLogical(t, bb, firstLine, freqMode);
                for (String line: logicalLines) {
                    lines.add(prefix + line);
                    prefix = subsequentPrefix;
                }
                firstLine = false;
            }
        }
        return lines;
    }
    static Vector<String>  tuningStringsPhysical(TuningParameters t,
                    boolean freqMode) {
        boolean physical = true;
        return tuningStrings(t, null, -1, physical, freqMode);
    }
    // Convert a single tuning solution into a vector of strings.
    // Can preceed output line with tuning solution index (inhibit with -1).
    // The physical parameter controls display of all BB as hw or only those 
    // that are used.
    static void dumpTuning(TuningParameters t, 
                    Vector<BasebandInfo> baseband, int solIndex, 
                    boolean physical, boolean freqMode) {
        Vector <String> lines = 
                tuningStrings(t, baseband, solIndex, physical, freqMode);
        for (String l: lines) {
            spew(l);
        }   

    }
    // Display a single tuning solution without solution index
    static void dumpTuning(TuningParameters t, Vector<BasebandInfo> baseband,
            boolean physicalMode, boolean freqMode) {
        int tuningIndex = -1;
        dumpTuning(t, baseband, tuningIndex, physicalMode, freqMode);
    }    
    // Display a single tuning solution but only logical basebands 
    static void dumpTuning(TuningParameters t, Vector<BasebandInfo> baseband,
            boolean freqMode) {
        int tuningIndex = -1;
        boolean physical = false;
        dumpTuning(t, baseband, tuningIndex, physical, freqMode);
    }    
    // Display a single tuning solution, all physical basebands, no tuningIndex
    static void dumpTuning(TuningParameters t) {
        Vector<BasebandInfo> baseband = null;
        int tuningIndex = -1;
        boolean physical = true;
        boolean freqMode = false;
        dumpTuning(t, baseband, tuningIndex, physical, freqMode);
    }    
    static String tuningHeaderFullString(boolean physical) {
        String s = "   Band ";
        if (physical) s += "  Used IFP  ";
        else s += "     req     act     wgt  err-MHz sb ";
        s += " LO2     LS   ";
        s += "FTS1MHz FTS1sb loDriver Nc FTS2MHz FTS2sb  wErr score";
        return s;
    }  
    static void tuningHeaders(boolean physical, boolean freqMode) {
        spew(tuningHeaderString(physical, freqMode));
    } 
    static void tuningHeaders(boolean physical) {
        boolean freqMode = false;
        tuningHeaders(physical, freqMode);
    } 
    static void tuningHeaders() {
        boolean physical = false;
        tuningHeaders(physical);
    } 
    static String freqHeaderString() {
        String s = "   Band      Fsky  sb   LO1   IFfreq  wErr  score";
        return s;
    }
    static void freqHeader() {
        spew(freqHeaderString());
    }
    static String tuningHeaderString(boolean physical, boolean freqMode) {
        if (freqMode) return freqHeaderString();
        return tuningHeaderFullString(physical);
    }
    // Print out a header followed by all tuning solutions.
    // The 'displayALl' param controls whether all basebands are displayed
    // or only those that are used     
    static void dumpTunings(Vector<TuningParameters> tuningSolutions,
                            Vector<BasebandInfo> baseband, 
                            boolean physical, boolean freqMode) {
        spew("Sol# " + tuningHeaderString(physical, freqMode));
        int tuneIndex = 0;
        for (TuningParameters t: tuningSolutions) {
            dumpTuning(t, baseband, tuneIndex++, physical, freqMode);
        }    
    }
      
}


