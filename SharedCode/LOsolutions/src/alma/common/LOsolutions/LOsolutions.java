/* $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.common.LOsolutions;

import java.lang.Math;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;

// Input parameters
import alma.Control.Band2Band3Overlap; 
import alma.Control.BasebandSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.SidebandPreference;
import alma.Control.SkyFreqSideband;
import alma.Control.SSBbasebandSpec;
import alma.ReceiverBandMod.ReceiverBand;

// Output parameters
import alma.Control.TuningParameters;
import alma.Control.LO2Parameters;

// Audience logging support
import alma.acs.logging.AcsLogLevel;
import alma.acs.logging.AcsLogger;
import alma.log_audience.OPERATOR;

/**
 * Holds and performs calculations for the tuning solutions used 
 * to setup the LO chain.
 *
 */ 
public class LOsolutions {
        
    // A list of all the current tuning solutions for the specified observing
    // frequencies. Not all these solutions will result in an observing 
    // frequencies that are exactly centered on the correlated band. 
    // However they will all be as close as the hardware allows.
    Vector<TuningParameters> tuningSolutions = new Vector<TuningParameters>();

    // tuningSolutions vector index of preferred solution.
    private int preferredSolution = -1;

    // The baseband bandwidth.
    static   final double BasebandBW = 2E9;
    
    // The baseband band edge is offset by half the bandwidth.
    // We subtract just a little to avoid floating point comparison failures. 
    static   final double BandOffset = BasebandBW/2 - 0.1;

    // ALMA has two Local Oscillator (LO) stages, except for the ATF
    // which has 3.
    // In addition, we have a pseudo-LO at 4GHz that corresponds to the
    // sampler clock. So the number of LOs is 3 (ALMA) or 4 (ATF)
    static    final int numLO = 3; // ALMA is default for all parameters

    // FTS2 avoidance range (+/-) around the bad spots
    static    final int    nFTS2Avoidance = 6;
    static    final double[] fts2AvoidanceRange = new double[nFTS2Avoidance];
    static    final double[] fts2AvoidanceFrequency = new double[nFTS2Avoidance];

    // Second LO Parameters. These should really come from a data base
    static    final double DigitizerFreq  =   4e9;
    // The FTS synthesizers need some headroom for freq offsetting.
    // These values come from P. Napier, 2008-04-03
    static    final double FTS1headroom   =  2e6;
    static    final double FTS2headroom   = FTS1headroom/2;
    // FTS range from Chip Scott, 2008-04-03, was 20-45MHz
    // FTS2 range from 2nd LO ICD, 2008-03-26, was 20-42MHz
    // FTS ranges below from Jeff Kern, 2009-05-22
    static    final double FTS1HwLowFreq  =  20.0e6;
    static    final double FTS1HwHighFreq =  45.0e6;
    static    final double FTS2HwLowFreq  =  20.0e6;
    static    final double FTS2HwHighFreq =  42.5e6;
    static    final double FTS1HighFreq   = FTS1HwHighFreq - FTS1headroom;
    static    final double FTS1LowFreq    = FTS1HwLowFreq  + FTS1headroom;
    static    final double FTS2HighFreq   = FTS2HwHighFreq - FTS2headroom;
    static    final double FTS2LowFreq    = FTS2HwLowFreq  + FTS2headroom;
    static    final double FTS2CenterFreq = (FTS2LowFreq + FTS2HighFreq)/2;
    static    final double LO2CombSpacing = 125e6;
    static    final double DYTOLowFreq    =   8e9;
    static    final double DYTOHighFreq   =  14e9;
    // Requested by Bill Brundage, AIV-2057
    static    final double Use12GHzFilterThreshold = 10.5e9;

    // Source: TeraXion LORTM Manual, 2008-12-02
    static private final double LSOffsetFreq    =  -125e6;
    //
    static private final double FLOOGLowFreq    = FTS1LowFreq;
    static private final double FLOOGHighFreq   = FTS1HighFreq;
    static private final double FLOOGCenterFreq = (FLOOGLowFreq+FLOOGHighFreq)/2;

    // ------------------------ Algorithm Constants ------------------------
    // Constants used in the algorithm that may need to be changed
    // Max LO search range in DSB mode
    static private final double dsbLOsearchSpan = 400e6; // 400 MHz
    // Relative weight of sky frequency error
    static private final double scoreA = 5.0;
    // Relative weight of IF freq error
    static private final double scoreB = 1.0; 
      // Use these values to allow the full range of LO1
        // Relative weight of LO1 OT compatible term
        //static private final double scoreC = 0.01;
    // Use these values to keep LO1 at least IFmax from the rx band edge,
    // as preferred by the OT. This will be at the expense of having the
    // baseband centered in the IFband (or as close as possible for multiple
    // basebands. 
         static private final double scoreC = 3.0; 
    // Normalization of weighted average frequency error in SSB use case
    // Set to about 4x peak when all lock sidebands are available
    static private final double wErrNormSSB = 25e6;  //  25 MHz 
    // Normalization of weighted average frequency error in DSB use case
    // Arbitrarily set to be as large as a couple of possible solutions
    static private final double wErrNormDSB = 200e6; // 200 MHz 
    // Maximum score
    static private final double scoreScale  = 10.0;  
    // ---------------------- End Algorithm Constants ------------------------

    // Count of number of solutions that are rejected because 
    // hardware parameters are out of range.
    private int rejectionCount = 0;

    // Properties
    // A display only name associated with a particular hw system (config)
    // Should be uniquely set by any class that inherits from this base class
    static protected String  systemName; 
    protected Logger  logger;
    protected AcsLogger acsLogger;
    protected boolean testMode;

    // Input parameters and a few things directly inferred from them
    // The four physical basebands
    private BasebandInfo[] basebandInfo = new BasebandInfo[4];
    // Only basebands currently used by the system
    private Vector<BasebandInfo> baseband = new Vector<BasebandInfo>();  
          

    // =================================================================   
    // --------------------- Public interface --------------------------
    
    /**
     * Constructor
     * @param logger reference to a logger
     * This is a Java Logger but note that an AcsLogger is also a Java Logger
     * and will work fine.
     */
    public LOsolutions(Logger logger) {
        final boolean testMode = false;
        constructorInitialization(logger, "ALMA", testMode);
    }
    
    /**
     * Get the number of solutions from the last computeSolutions
     * @return number of solutions
     */
    public short numberOfSolutions() {
        this.logger.finest(this.getClass().getName() + ".numberOfSolutions");
        return new Integer(this.tuningSolutions.size()).shortValue();
    }

    /**
     * Returns the index for the preferred tuning solution
     * @return Index of preferred tuning solution
     */
    public short preferredTuningSolution() {
        logger.finest(getClass().getName() + ".preferredTuningSolution");
        return new Integer(this.preferredSolution).shortValue();
    }

    /**
     * Get a specific tuning solution.
     */
    public TuningParameters getTuningSolution(short soln)
            throws Exception {
        this.logger.finest(this.getClass().getName() + ".getTuningSolution");
        final int nSols = numberOfSolutions();

        if (nSols <= 0) {
            String errMsg = "There are no tuning solutions!" +
                     " Have you called computeSolutions() or setFrequency()?";
            this.logger.severe(errMsg);
            throw new Exception(errMsg);
        }

        if (soln < 0 || soln >= nSols) {
            String errMsg = "Only solutions in the range 0 - " + (nSols - 1)
                    + " are available and you have requested solution " + soln;
            this.logger.severe(errMsg);
            throw new Exception(errMsg);
        }
        return this.tuningSolutions.elementAt(soln);
    }
    public Vector<TuningParameters> getAllTuningSolutions()
            throws Exception {
        this.logger.finest(this.getClass().getName() + ".getAllTuningSolution");
        final int nSols = numberOfSolutions();

        if (nSols <= 0) {
            String errMsg = "There are no tuning solutions!" +
                     " Have you called computeSolutions() or setFrequency()?";
            this.logger.severe(errMsg);
            throw new Exception(errMsg);
        }

        return tuningSolutions;
    }

    /**
     * Compute the observing frequencies and store them and 
     * their parameters internally.
     * This method should be used when all the basebands are using a
     * single sideband use case.
     * The hardware does not always allow all frequencies to be 
     * set precisely as requested. The weight parameters are used
     * to apportion the errors accordingly. The sideband prefences allow
     * selection of the 1st LO sideband. A choice of NoPreference will
     * try to use both sidebands when supported by the hardware. Choosing
     * a specific sideband will eliminate the other from the solution set.
     * @param bbspec vector of SSBbasebandSpec structures containing
     *        skyFreq, weight, ifFreq, and sidebandPref
     * @param band2band3overlap specifies frontend preference when sky freq
     *        is the region where band2 and band3 overlap
     * @return number of solutions
     */
    public short computeSolutions(Vector<SSBbasebandSpec> bbspec, 
                                  Band2Band3Overlap       band2band3overlap) 
            throws Exception {
        final boolean verbose = false;               
        return computeSolutions(bbspec, band2band3overlap, verbose);
    } 
    /**
     * Another version of computeSolutions for the single sideband 
     * use case with the band2/band3 overlap set to AutoChoice. 
     * This should be the method most often used.
     * @see computeSolutions
     */
    public short computeSolutions(Vector<SSBbasebandSpec> bbspec) 
            throws Exception {
        return computeSolutions(bbspec, Band2Band3Overlap.AutoChoice);
    }        
       
    /**
     * Another version of computeSolutionsDSB for the single sideband 
     * use case with the band2/band3 overlap set to AutoChoice. 
     * This should be the method most often used.
     * @see computeSolutionsDSB
     */
    public short computeSolutionsDSB(Vector<DSBbasebandSpec> bbspec) 
            throws Exception {
        return computeSolutionsDSB(bbspec, Band2Band3Overlap.AutoChoice);
    }  
    /**
     * Compute the observing frequencies and store them and 
     * their parameters internally.
     * This method should be used when any baseband is using a
     * double sideband use case.
     * The input DSBbasebandSpec structures can be used for either
     * double or single sideband basebands. When double sideband
     * basebands are input the ifFreq is ignorned. 
     * <p>
     * The hardware does not always allow all frequencies to be 
     * set precisely as requested. The weight parameters are used
     * to apportion the errors accordingly. 
     * <p>
     * The sideband preferences allow selection of the 1st LO sideband
     * for single sideband basebands.
     * A choice of NoPreference will
     * try to use both sidebands when supported by the hardware. Choosing
     * a specific sideband will eliminate the other from the solution set.
     * @param bbspec vector of DSBbasebandSpec structures containing
     *        skyFreq, weight, ifFreq, and sidebandPref
     * @param band2band3overlap specifies frontend preference when sky freq
     *        is the region where band2 and band3 overlap
     * @return number of solutions
     */
    public short computeSolutionsDSB(Vector<DSBbasebandSpec> bbspec, 
                                     Band2Band3Overlap band2band3overlap) 
            throws Exception {
        final boolean verbose = false;               
        return computeSolutionsDSB(bbspec, band2band3overlap, verbose);
    }  
      
    /**
     * Return the system name for this LO solutions instance.
     * The system name, such as "ALMA" or "ATF" is associated with
     * a specific set of hardware parameters on which the solutions are based.
     */
    public String systemName() {
        return systemName;
    }    
   
    /**
     * Calculate the actual second LO frequency given the specified parameters.
     * An exception is thrown if the parameters are out of range
     */
    static public double getLO2Freq(LO2Parameters parameters) {
        return LO2Freq(parameters);
    }    
    
    static public double getLSOffsetFreq() {
        return LSOffsetFreq;
    } 
    
    public int getNumLO() {
        return numLO();
    }          
    public String getHardwareParameters()
            throws Exception {
        return hardwareParameterString();
    }       
    static public short getColdMultiplier(ReceiverBand receiverBand) 
            throws Exception {
        Band b = Band.band(receiverBand);
        return b.coldMultiplier();
    }    
    static public double LO2CombSpacing() {
        return LO2CombSpacing;
    }
    /// The average of the requested sky frequency across all used bands
    /// @return average sky frequency in Hz
    public double averageSkyFrequency() {
        double tot   = 0;
        int    count = 0;
        for (BasebandInfo b: baseband) {
            for (SSBbasebandInfo s: b.sb) {
                if (s != null) {
                     tot += s.skyFreqHz;
                     count++;
                }
            }
        }
        return tot/count;
    }
    
    static public SSBbasebandSpec ssbBasebandSpec(
                                           double skyFreqHz,
                                           double weight,
                                           double ifFreqHz,
                                           SidebandPreference sidebandPref)
    {
        SSBbasebandSpec spec = new SSBbasebandSpec();
        spec.skyFreqHz    = skyFreqHz;
        spec.weight       = weight;
        spec.ifFreqHz     = ifFreqHz;
        spec.sidebandPref = sidebandPref;
        return spec;
    }                                       
    static public SSBbasebandSpec ssbBasebandSpec(
                                           double skyFreqHz,
                                           double weight,
                                           SidebandPreference sidebandPref)
    {
        return ssbBasebandSpec(skyFreqHz, weight, 0.0, sidebandPref);
    }                                       
    static public SSBbasebandSpec ssbBasebandSpec(double skyFreqHz,
                                                  double weight,
                                                  double ifFreqHz)
    {
        return ssbBasebandSpec(skyFreqHz, weight, ifFreqHz,
                               SidebandPreference.NoPreference);
    }                                       
    static public SSBbasebandSpec ssbBasebandSpec(double skyFreqHz,
                                                  double weight)
    {
        return ssbBasebandSpec(skyFreqHz, weight, 0.0);
    }                                       
    static public SSBbasebandSpec ssbBasebandSpec(double skyFreqHz)
    {
        return ssbBasebandSpec(skyFreqHz, 100.0);
    }                                       
    static public SSBbasebandSpec ssbBasebandSpec(
                                           double skyFreqHz,
                                           SidebandPreference sidebandPref)
    {
        return ssbBasebandSpec(skyFreqHz, 100.0, 0.0, sidebandPref);
    } 
    static public DSBbasebandSpec dsbBasebandSpec(double skyFreqHzUpper,
                                                  double weightUpper,
                                                  double ifFreqHzUpper,
                                                  double skyFreqHzLower,
                                                  double weightLower,
                                                  double ifFreqHzLower)
    {
        DSBbasebandSpec spec = new DSBbasebandSpec();
        spec.USB             = new BasebandSpec();
        spec.LSB             = new BasebandSpec();
        spec.USB.skyFreqHz   = skyFreqHzUpper;
        spec.USB.weight      = weightUpper;
        spec.USB.ifFreqHz    = ifFreqHzUpper;
        spec.LSB.skyFreqHz   = skyFreqHzLower;
        spec.LSB.weight      = weightLower;
        spec.LSB.ifFreqHz    = ifFreqHzLower;
        return spec;
    }  
    static public DSBbasebandSpec dsbBasebandSpec(double skyFreqHzUpper,
                                                  double weightUpper,
                                                  double skyFreqHzLower,
                                                  double weightLower)
    {
        return dsbBasebandSpec(skyFreqHzUpper, weightUpper, 0.0,
                               skyFreqHzLower, weightLower, 0.0);
    }                                     
    static public DSBbasebandSpec dsbBasebandSpec(double skyFreqHzUpper,
                                                  double skyFreqHzLower)
    {
        return dsbBasebandSpec(skyFreqHzUpper, 0.0, skyFreqHzLower, 0.0);
    } 
    
    /**
     * Get the laser synthesizer output frequency.
     */
    static public double laserSynth(TuningParameters p) {
        return p.LSFrequency;
    }

    /**
     * Calculate the actual first LO frequency given the specified
     * parameters.
     * @param parameters tuningParameters
     * @return LO1 frequency in Hz 
     */
    static public double LO1Freq(TuningParameters parameters) {
        final double ls = laserSynth(parameters);
        final double nc = parameters.coldMultiplier;
        double lo1 = 0;
        if (parameters.tuneHigh) {
            lo1 = (ls + parameters.FLOOGFrequency) * nc;
        } else {
            lo1 = (ls - parameters.FLOOGFrequency) * nc;
        }
        return lo1;
    }

    /**
     * Calculate the LS frequency given the receiver band, LO1 frequency,
     * floog frequency, and floog tuneHigh.
     * @param rb receiver band
     * @param lo1 LO1 frequency in Hz
     * @param floog FLOOG frequency in Hz
     * @param tuneHigh FLOOG sideband (boolean)
     * @return Laser Synthesizer (LS) frequency in Hz
     */
    static public double LSfreqFromLO1(ReceiverBand rb, double lo1, 
            double floog, boolean tuneHigh) throws Exception {
        final double  coldX = Band.band(rb).coldMultiplier();
        double ls = 0;
        if (tuneHigh) {
            ls = lo1/coldX - floog;
        } else {
            ls = lo1/coldX + floog;
        }
        return ls;
    }

    /**
     * Choose 2nd LO tuneHigh given the receiver band and LO2 frequency.
     * This makes a simple choice to keep the DYTO toward the center of
     * the IF band so that it does not exceed the DYTO limits.
     * @param rb receiver band
     * @param lo2 LO2 frequency in Hz
     * @return tuneHigh boolean for FTS2
     */
    static public boolean tuneHighFromLO2(ReceiverBand b, double lo2) 
            throws Exception {
        final double  IFhigh = Band.band(b).IFhighFreq();
        final double  IFlow  = Band.band(b).IFlowFreq();
        final double  IFmid  = (IFhigh+IFlow)/2;
        if (lo2 > IFmid) return true;
        return false;
    }

    /**
     * Calculate the actual sky frequency for the center of the baseband
     * given the tuning parameters.
     * An exception is thrown if the parameters are out of range
     */
    static public double actualSkyFreq(TuningParameters t, int bbIndex,
                SidebandSelect sb)
            throws Exception {
        int    sbsign = (sb == SidebandSelect.Upper)? 1: -1;
        double lo2 = LO2Freq(t.LO2[bbIndex]);    
        Band band  = Band.band(t.receiverBand);
        double fI  = lo2 - (DigitizerFreq - BandOffset);
        double skyFreq = LO1Freq(t) + sbsign*fI;
        if (false) {
            String s = 1e-9*LO1Freq(t) + " " + sbsign + " fSky=" + 1e-9*skyFreq;
            s += " LO2:" + 1e-9*lo2;
            System.out.println(s);
        }
        return skyFreq;
    } 

    /// Get the range of the sidebands for a given tuning.
    /// A return value of zero means the sideband is not supported.
    static public double getUSBhighFreq(TuningParameters t) 
            throws Exception {
        Band band  = Band.band(t.receiverBand);
        if (band.sb() == SB.LSB) return 0;
        double f = LO1Freq(t) + band.IFhighFreq();
        if (f > band.bandHighFreq()) return band.bandHighFreq();
        return f;
    }                                       
    static public double getUSBlowFreq(TuningParameters t) 
            throws Exception {
        Band band  = Band.band(t.receiverBand);
        if (band.sb() == SB.LSB) return 0;
        double f = LO1Freq(t) + band.IFlowFreq();
        if (f > band.bandHighFreq()) return band.bandHighFreq();
        return f;
    }                                       
    static public double getLSBhighFreq(TuningParameters t) 
            throws Exception {
        Band band  = Band.band(t.receiverBand);
        if (band.sb() == SB.USB) return 0;
        double f = LO1Freq(t) - band.IFlowFreq();
        if (f < band.bandLowFreq()) return band.bandLowFreq();
        return f;
    }                                       
    static public double getLSBlowFreq(TuningParameters t) 
            throws Exception {
        Band band  = Band.band(t.receiverBand);
        if (band.sb() == SB.USB) return 0;
        double f = LO1Freq(t) - band.IFhighFreq();
        if (f < band.bandLowFreq()) return band.bandLowFreq();
        return f;
    }                                       
    // Used in debugging from other packages  
    public void dumpTuning(TuningParameters t, 
            boolean physicalMode, boolean freqMode) {
        Misc.dumpTuning(t, baseband, physicalMode, freqMode);
    }
    public void dumpTuning(TuningParameters t) {
        boolean physicalMode = false;
        boolean freqMode     = false;
        dumpTuning(t, physicalMode, freqMode);
    }
    public void dumpTuningPhysical(TuningParameters t) {
        boolean physicalMode = true;
        boolean freqMode     = false;
        dumpTuning(t, physicalMode, freqMode);
    }
    /// Tuning headers as string
    static public void tuningHeaders(boolean physical) {
        Misc.tuningHeaders(physical);
    }
    /// Print tuning headers
    static public String tuningHeaderString(boolean physical) {
        boolean freqMode = false;
        return Misc.tuningHeaderString(physical, freqMode);
    }
    /// Print tuning headers, logical mode
    static public void tuningHeaders() {
        Misc.tuningHeaders();
    }
    static public Vector<String> tuningStringsPhysical(TuningParameters t) {
        boolean freqMode = false;
        return Misc.tuningStringsPhysical(t, freqMode);
    }
                                    
        
    // ---------------------- Internal methods ----------------------

    // SSB Use Case 
    short computeSolutions(Vector<SSBbasebandSpec> bbspec, 
                           Band2Band3Overlap       band2band3overlap,
                           boolean                 verbose, 
                           boolean                 dumpBasebands) 
            throws Exception {        
        // Convert input into generic Baseband
        int index = 0;
        Vector<BasebandInfo> bbInfo = new Vector<BasebandInfo>();
        for (SSBbasebandSpec b: bbspec) {
            bbInfo.add(new BasebandInfo(b, index++));
        }                
        return doComputeSolutions(bbInfo, band2band3overlap, verbose,
                    dumpBasebands);
    }        
    // SSB Use Case 
    short computeSolutions(Vector<SSBbasebandSpec> bbspec, 
                           Band2Band3Overlap       band2band3overlap,
                           boolean                 verbose) 
            throws Exception {  
        return computeSolutions(bbspec, band2band3overlap, verbose, true);
    }        
    // SSB Use Case 
    short computeSolutions(Vector<SSBbasebandSpec> bbspec, 
                           boolean                 verbose, 
                           boolean                 dumpBasebands) 
            throws Exception {  
        return computeSolutions(bbspec, Band2Band3Overlap.AutoChoice, verbose,
                     dumpBasebands);
    }   
              
    // SSB Use Case 
    short computeSolutions(Vector<SSBbasebandSpec> bbspec, boolean verbose) 
            throws Exception {                    
        return computeSolutions(bbspec, Band2Band3Overlap.AutoChoice, verbose);
    }    
    // DSB Use Case 
    short computeSolutionsDSB(Vector<DSBbasebandSpec> bbspec, 
                              Band2Band3Overlap band2band3overlap,
                              boolean verbose) 
            throws Exception {        
        // Convert input into generic Baseband
        int index = 0;
        Vector<BasebandInfo> bbInfo = new Vector<BasebandInfo>();
        for (DSBbasebandSpec b: bbspec) {
            bbInfo.add(new BasebandInfo(b, index++));
        }                
        return doComputeSolutions(bbInfo, band2band3overlap, verbose, true);
    }
    
    // The real work horse; works on SSB or DSB
    short doComputeSolutions(Vector<BasebandInfo> bbspec, 
                             Band2Band3Overlap band2band3overlap,
                             boolean verbose, boolean dumpBasebands) 
           throws Exception { 
               
        tuningSolutions.clear();
        this.preferredSolution = -1;
        
        // get a vector of basebands that are actually used
        Vector<BasebandInfo> bbinfo = selectBasebands(bbspec, verbose); 
        validateBasebandFreq(bbinfo, verbose); 
        Band band = selectBand(bbinfo, band2band3overlap, verbose);
        if (false) dumpBB(band, bbinfo);
        // Assign, remove, or leave sidebands marked as 'Either'
        // The 'doBothSB' means that independent solutions must be done
        // for both sb
        boolean doBothSB = assignEitherBasebands(band, bbinfo, verbose);
        //spew("doBothSB: " + doBothSB);
        if (dumpBasebands) dumpBB(band, bbinfo);
        
        LOrange[] loRanges = 
                validateSidebands(bbinfo, band, doBothSB, verbose);
        if (false) {
            String rangeMsg = "LOranges: " + loRanges[0];
            if (loRanges.length > 1) rangeMsg += "   " + loRanges[1]; 
            spew(rangeMsg);  
        }     
        for (LOrange loRange: loRanges) {
            int nBasebands = bbinfo.size();
            int nCombinations = 1 << nBasebands;
            for (int c=0; c<nCombinations; c++) {
                int[] fts2sb = new int[4]; // Accessed using bbIndex as index
                int i = 0;
                for (BasebandInfo bb: bbinfo) {
                    boolean isZero = (c & (1 << i)) == 0;
                    // This parameter is +1 or -1 for USB/LSB
                    fts2sb[bb.index] = (isZero?-1:1);
                    i++;
                }
                SidebandSelect lo1sb = loRange.sbSelect;
                double loOffset = 
                           selectLO1offset(band, bbinfo, fts2sb, lo1sb, verbose);
                findSolutions(band, bbinfo, loOffset, fts2sb, loRange, 
                    tuningSolutions, verbose);  
            }           
        }
        editSolutions(bbinfo,  verbose);
        preferredSolution = scoreSolutions(bbinfo, band, verbose);
        selectIFprocessors(band);
        setTuningParameterIndices();
        // Now setup the class variables to echo the input parameters
        baseband = bbinfo;
        for (int b=0; b<4; b++) basebandInfo[b] = new BasebandInfo(b); //Init
        for (BasebandInfo bb: baseband) basebandInfo[bb.index] = bb;   //Copy
        // Get the return value
        int nTunings = tuningSolutions.size();
        return (short)nTunings;       
    } 
           
    /// Check bands for legit freq ( > 1 MHz) and return a vector of
    /// legitimate bands.
    private Vector<BasebandInfo> selectBasebands(Vector<BasebandInfo> bbspec,   
                                                 boolean verbose) 
            throws Exception {
        Vector<BasebandInfo> bbinfo = new Vector<BasebandInfo>();
        int index = 0;
        for (BasebandInfo b: bbspec) {
            if (b != null) {
                if (b.isUsed) bbinfo.add(b);
            }
        }  
        if (bbinfo.size() == 0) {
            throw new Exception("Must have at least one valid band");
        } 
        return bbinfo;     
    } 
    
    // Checks that all bb freqs are within ALMA range and within a rx band;
    // throws if not        
    private void validateBasebandFreq(Vector<BasebandInfo> bbinfo, 
            boolean verbose) 
            throws Exception {
        int numBadFreqs = 0;
        // True if fails because it is within the BandOffset of the edge
        boolean closeToEdge = false; 
        String  badFreqs = "";
        // Get lowest/highest freqs used in system by going through each band
        double systemHighFreq = 0;
        double systemLowFreq  = 1000e9;
        for (Band b: Band.values()) {
            double low  = bandLowFreq(b);
            double high = bandHighFreq(b);
            // Low/high freqs are set to 0 when a band is not used; skip these
            if ((low > 1) && (high > 1)) {
                systemLowFreq  = Math.min(low,  systemLowFreq);
                systemHighFreq = Math.max(high, systemHighFreq);
            }    
        }
        // Debugging output
        if (false) {
            for (BasebandInfo b: bbinfo) {
                spew("*** bbindex="+b.index + "  sbselect="+b.sbSelect);
            } 
            for (SSBbasebandInfo s: BasebandInfo.uniqueSidebandVec(bbinfo)){
                spew("*** sbselect="+s.sbSelect + "  f="+s.skyFreqHz);
            }
        }
        // Check each baseband freq to be in system range and within a FE band
        for (SSBbasebandInfo s: BasebandInfo.uniqueSidebandVec(bbinfo)) { 
            // True if it fails because it is within the BandOffset of the edge
            boolean thisBandCloseToEdge = false; 
            double f = s.skyFreqHz;
            // Check against lowest/highest freqs in system
            if ( (f < systemLowFreq) || (f > systemHighFreq) ) {
                numBadFreqs += 1;
                if (numBadFreqs > 1) badFreqs += "\n";
                String fmt = "Baseband%d, with center frequency %.3f GHz, ";
                fmt += "is not within the observing range of " + systemName;
                fmt += " [%.1f GHz, %.1f GHz]";
                badFreqs += String.format(fmt, s.bbIndex, 1e-9*f, 
                    1e-9*systemLowFreq, 1e-9*systemHighFreq);
            } 
            else {
                // Now check that freq is within some band
                boolean freqInBand = false;
                String bandName  = "Band unknown";
                double closeLow  = 0.0;
                double closeHigh = 1000e9;
                for (Band b: Band.values()) {
                    double low  = bandLowFreq(b)  + BandOffset;
                    double high = bandHighFreq(b) - BandOffset;
                    if ((f >= low) && (f <= high)) {
                        freqInBand = true;
                        break;
                    } 
                    else if (((f>=bandLowFreq(b)) && (f<low)) ||
                             ((f<=bandHighFreq(b)) && (f>high))) {
                        closeToEdge = true; 
                        thisBandCloseToEdge = true;
                        bandName  = b.name();
                        closeLow  = bandLowFreq(b);
                        closeHigh = bandHighFreq(b);     
                    }
                }

                if (freqInBand == false) {
                    numBadFreqs += 1;
                    if (numBadFreqs > 1) badFreqs += "\n";
                    String fmt = 
                            "Baseband%d, with center frequency %.3f GHz, is ";
                    String msg = String.format(fmt, s.bbIndex, 1e-9*f);
                    if (thisBandCloseToEdge) {
                        String m = msg + "not completely within " + bandName;
                        fmt = "%s [%.1f, %.1f]";
                        msg = String.format("%s [%.1f, %.1f]", m,
                                    1e-9*closeLow, 1e-9*closeHigh);
                    }
                    else {
                        msg += "not within any band of " + systemName;
                    }
                    badFreqs += msg;
                }   
            }
        }
        if (numBadFreqs >= 1) {
            if (closeToEdge) {
                badFreqs += "\nNote that baseband frequencies are center ";
                badFreqs += "frequencies and must be a least 1GHz from the ";
                badFreqs += "band edge to place the full 2GHz wide baseband ";
                badFreqs += "within the front end band!";
            } 
            if (numBadFreqs > 1) badFreqs = '\n' + badFreqs;   
            this.logger.severe(badFreqs);
            throw new Exception(badFreqs);
        }
    }
    // Select a rx band.
    // Heuristics are used to choose between band 2 and 3 where they
    // overlap if no preference is given. 
    // Currently the heuristic is to always choose band3.
    private Band selectBand(Vector<BasebandInfo> bbinfo, 
                            Band2Band3Overlap band2band3overlap, 
                            boolean verbose) 
            throws Exception {
        int numBadFreqs = 0;
        String  badFreqs = "";
        Vector<Band> bands = new Vector<Band>();
        for (FreqBBindex pair: BasebandInfo.freqBBindexVec(bbinfo)) {
            double f       = pair.skyFreqHz;
            int    bbIndex = pair.index;
            //spew(bb + "  " + f*1e-9);
            Vector<Band> goodBands = new Vector<Band>();
            for (Band b: Band.values()) {
                if ((f >= bandLowFreq(b)  + BandOffset) &&
                    (f <= bandHighFreq(b) - BandOffset)) 
                {
                    goodBands.add(b);
                }
            }
            int nBands = goodBands.size();
            switch (nBands) {
                case 0 :
                    numBadFreqs += 1;
                    if (numBadFreqs > 1) badFreqs += "\n";
                    String fmt = "Baseband%d frequency (%7.3 GHz) ";
                    fmt += "is not within any band of ALMA";
                    badFreqs += String.format("%7.3f", bbIndex, 1e-9*f);
                    break;
                case 1 :
                    bands.add(goodBands.elementAt(0));
                    break;
                case 2 :
                    Band bb1 = goodBands.elementAt(0);
                    Band bb2 = goodBands.elementAt(1);
                    if (((bb1 == Band.Band2) && (bb2 == Band.Band3)) ||
                        ((bb1 == Band.Band3) && (bb2 == Band.Band2))) 
                    {
                        if (band2band3overlap == Band2Band3Overlap.Band2) {
                            bands.add(Band.Band2);
                        }    
                        if (band2band3overlap == Band2Band3Overlap.Band3) {
                            bands.add(Band.Band3);
                        } 
                        // The Automatic choice is done here:
                        // Band3 is chosen because Band2 will not be built
                        // for a long time...   
                        if (band2band3overlap == Band2Band3Overlap.AutoChoice) {
                            bands.add(Band.Band3);
                        }    
                        break;
                    }
                    else {
                        // This will happen if the configuration has two bands 
                        // other than 2/3 overlapping in frequency
                        fmt = 
                            "selectBands() found two conflicting bands (%s,%s)"
                            + " for baseband%d at frequency(%7.3f GHz)";
                        String m = String.format(fmt, nBands, bb1, bb2, f);
                        throw new Exception(m);
                    }    
                default:
                    // This will happen if the configuration has more than 
                    // two bands overlapping in frequency
                    fmt = 
                        "selectBands() found an illegal number of bands (%d)"
                        + " for baseband%d at this frequency(%7.3f GHz)";
                    String m = String.format(fmt, nBands, bbIndex, f);
                    throw new Exception(m);
            }       
        }
        if (numBadFreqs >= 1) {
            logger.severe(badFreqs);
            throw new Exception(badFreqs);
        } 
        Band selectedBand     = null;  
        boolean multipleBands = false;     
        for (Band b: bands) {
            if (selectedBand == null)selectedBand = b;
            else {
                if (b != selectedBand) multipleBands = true;
            }
        }
        // Error message and exception if multiple bands specified
        if (multipleBands) {
            String s = "Requested baseband frequencies (";
            boolean firstTime = true;
            for (Double f: BasebandInfo.freqVec(bbinfo)) { 
                if (firstTime) firstTime = false;
                else           s += ", ";
                s += String.format("%.3f", 1e-9*f);
            } 
            s += " GHz) are spread across multiple bands ("; 
            boolean first = true;  
            for (Band b: bands) {
                if (first == false) s += ", ";
                s += b; 
                first = false;               
            }
            s += "). This is not allowed.";
            logger.severe(s);
            throw new Exception(s);
        }
        return selectedBand;        
    }
    
        
    // Sorts through the 'Either' basebands and removes sidebands that won't
    // work (if any) with other basebands that request a specific sideband.
    // First does an assignment for ssb frontends, including throwing an
    // exception if a sideband has been requested that is not supported by
    // the frontend.
    // Then uses the frontend band IF params to potentially eliminate
    // one of the 'Either' sidebands.
    // Returns true if both sidebands should be tried for all basebands,
    // which is only true if all basebands are 'Either'.
    // Does not check that freqs are sensible (usb > lsb, etc).
    // Returns a boolean indicating that solutions should be tried for
    // two independent sidebands (only true if all remaining bands are 'Either')
    private boolean assignEitherBasebands(Band band, 
                                          Vector<BasebandInfo> bbinfo,
                                          boolean verbose) 
             throws Exception                             
        {
        // Check for SSB frontend
        String emsg = "";
        boolean error = false;
        for (BasebandInfo b: bbinfo) {
            switch (band.sb()) {
                case USB:
                    switch (b.sbSelect) {
                        case Either: 
                            b.removeLSB(); // Ditch inappropriate sideband
                            break;
                        case Both:
                            emsg += "Baseband" + b.index + " attempts to ";
                            emsg += "assign a DSB frequency to a ";
                            emsg += "single sideband (USB) frontend.\n";
                            error = true; 
                            break;
                        case Lower:
                            emsg += "Baseband" + b.index + " attempts to ";
                            emsg += "assign an LSB frequency to "; 
                            emsg += "a USB frontend\n";
                            error = true; 
                            break;
                    }  // End switch on sbSelect        
                case LSB:
                    switch (b.sbSelect) {
                        case Either: 
                            b.removeUSB(); // Ditch inappropriate sideband
                            break;
                        case Both:
                            emsg += "Baseband" + b.index + " attempts to ";
                            emsg += "assign a DSB frequency to a ";
                            emsg += "single sideband (LSB) frontend.\n";
                            error = true; 
                            break;
                        case Lower:
                            emsg += "Baseband" + b.index + " attempts to ";
                            emsg += "assign a USB frequency to "; 
                            emsg += "an LSB frontend\n";
                            error = true; 
                            break;
                   }  // End switch on sbSelect       
            }         // End switch on sideband
        }             // End for loop over basebands
        if (error) {
            emsg = emsg.substring(0, emsg.length()-1);
            throw new Exception(emsg);
        } 
        
        boolean lowerSpecified = false;
        boolean upperSpecified = false;
        for (BasebandInfo b: bbinfo) {
            if (b.sbSelect == SidebandSelect.Upper) upperSpecified = true;
            if (b.sbSelect == SidebandSelect.Lower) lowerSpecified = true;
        }
        // If both Upper and Lower are specified then we have a problem
        if (lowerSpecified && upperSpecified) {
            String msg = "Conflict in sideband specification: ";
            msg += " Both Lower and Upper Sideband have ";
            msg += " been specified for different basebands";
            msg += "\nA sideband should only be specified when there is an";
            msg += " ambiguity in the assignment of a sideband for a given";
            msg += " baseband center frequency. If different sidebands are";
            msg += " specified then a conflict may occur, so it is disallowed.";
            throw new Exception(msg);
        }    
        
        if (lowerSpecified || upperSpecified) {
            for (BasebandInfo b: bbinfo) {
                if (b.sbSelect == SidebandSelect.Either) {
                    if (upperSpecified) b.removeLSB();
                    else                b.removeUSB();
                }
            }
            
        }
        
        // Get full range of requested frequencies   
        FreqRange range = basebandRange(bbinfo);
        //spew("Full sky freq range of all basebands: " + range);

        double highFreq = range.high;
        double lowFreq  = range.low;
        double fskySpan = highFreq - lowFreq; 
        double ifSpan   = IFhighFreq(band) - IFlowFreq(band) - 2*BandOffset; 
        //spew ("RFrange: " + 1e-9*range + "   IFrange: " + 1e-9*ifRange); 
        // All freqs fit within a single sideband IF; either can be used
        // unless some baseband calls for only one sideband.
        if (fskySpan <= ifSpan) {
            boolean allAreEither = true;
            for (BasebandInfo b: bbinfo) {
                if (b.sbSelect != SidebandSelect.Either) allAreEither = false;
            }
            if (allAreEither) return true;
        }        
        // Assign 'either' basebands to a sideband
        // - Get extreme ranges for skyFreq sidebands
        FreqRange usbRange = new FreqRange(highFreq - ifSpan, highFreq);
        FreqRange lsbRange = new FreqRange(lowFreq, lowFreq + ifSpan);
        // Remove sidebands where freq won't fit
        for (BasebandInfo b: bbinfo) {
            if (b.sbSelect == SidebandSelect.Either) {
                double f = b.upper.skyFreqHz;
                if (!usbRange.isWithin(f)) b.removeUSB();
                if (!lsbRange.isWithin(f)) b.removeLSB();
                if (!b.isUsed) {
                    String t = "Baseband%d frequency (%.3fGHz) won't fit in";
                    t += " a sideband of frontend with frequencies spanning ";
                    t += "[%.1f, %.1f]GHz";
                    String s = String.format(t, b.index, 
                                   1e-9*f, 1e-9*highFreq, 1e-9*lowFreq);
                    throw new Exception(s);
                }
            }
        }                    
        return false;
    }

    // Check to see if some of the baseband frequency differences just 
    // slightly exceed the IF range. This can be caused by doppler shifting
    // rest frequencies that were setup to span the IF edge to edge. An
    // approproriate warning is returned, or a null string.
    private String dopplerWarning(Vector<BasebandInfo> bbinfo, double ifSpan) {
        double MAXDOPFAC = 1.007; // Max Doppler factor used in comparison
        Vector<FreqBBindex> fi = BasebandInfo.freqBBindexVec(bbinfo); 
        int bbidx1 = 0;
        int bbidx2 = 0;
        Vector<String> bbpairs = new Vector<String>();
        for (FreqBBindex bb1: fi) {
            bbidx1 = bb1.index;
            for (FreqBBindex bb2: fi) {
                bbidx2 = bb2.index;
                if (bbidx2 > bbidx1) {
                    double d = Math.abs(bb1.skyFreqHz-bb2.skyFreqHz);
                    if ((d > ifSpan) && (d < 1.001*ifSpan)) {
                        bbpairs.add(
                            new String("Baseband"+bbidx1+"-Baseband"+bbidx2));
                    }
                }
            }
        }
        int n = bbpairs.size();
        if (n == 0) return "";
        String m = "\n WARNING: ";
        m += "The sky frequency difference between the baseband pair";
        if (n > 1) m += "s";
        String separator = " ";
        for (String s: bbpairs) {
            m += separator + "[" + s + "]";
            separator = ",";
        }
        m += " only slightly exceed";
        if (n == 1) m +="s";
        m += " the IF bandwidth and may be caused by a Doppler shift of";
        m += " rest frequencies that just barely spanned the IF band before";
        m += " the shift.";
        //spew("DOPPLERWARN"+bbpairs); 
        return m;                           
    }

    // A helper to format the frequency range within a sideband
    private String rangeRepresentation(double low, double high, int precision) {
        double absdiff = Math.abs(high-low);
        String f = String.format("%%.%dfGHz", precision);
        if (absdiff < 10000.0) { // Freqs are considered same if within 1KHz
            return String.format(f, high*1e-9);
        }
        else {
            String fmt = String.format("[%s, %s]GHz", f, f);
            return String.format(fmt, 1e-9*low, 1e-9*high);
        }
    }
    // Make sure all freqs will fit within IF sidebands of selected frontend.
    // Throws if requested freqs won't fit. Returns LO1 range(s).
    // Also check for IF processor violations and throw if found.
    private LOrange[] validateSidebands(Vector<BasebandInfo> bbinfo, 
            Band band, boolean useBothSidebands, boolean verbose) 
                throws Exception {        
        FreqRange ifRange  = ifRange(band);   
        double    ifSpan   = ifRange.span(); 
        boolean   usbUsed  = false;    
        boolean   lsbUsed  = false; 
        FreqRange usbRange = new FreqRange();
        FreqRange lsbRange = new FreqRange();
        // Get sideband freq ranges
        Vector<SSBbasebandInfo> sbinfo = BasebandInfo.sidebandVec(bbinfo); 
        for (SSBbasebandInfo s: sbinfo) {
            switch (s.sbSelect) {
                case Upper:
                     usbRange.add(s.skyFreqHz);
                     usbUsed = true;
                     break;
                case Lower:
                     lsbRange.add(s.skyFreqHz);
                     lsbUsed = true;
                     break;
            }         
        }
                      
        // Check to see if sidebands exceed IF bandwidth
        boolean error   = false;
        String  emsg    = "";
        double  usbSpan = usbRange.span();   
        double  lsbSpan = lsbRange.span(); 
        String  fmt  = "Span of requested %s sky frequencies(%.3fGHz) exceeds";
                fmt += " usable IF bandwidth(%.1fGHz)";  
        if (usbUsed && (usbSpan > ifSpan)) {
            error = true;
            emsg += String.format(fmt, "USB", 1e-9*usbSpan, 1e-9*ifSpan); 
        }     
        if (lsbUsed && (lsbSpan > ifSpan)) {
            error = true;
            emsg += String.format(fmt, "LSB", 1e-9*lsbSpan, 1e-9*ifSpan); 
        } 
        if (error) {
            emsg += dopplerWarning(bbinfo, ifSpan);
            throw new Exception(emsg);
        }        
        // Get LOrange for each sideband
        LOrange usbLoRange  = new LOrange(usbRange.high - ifRange.high,
                                          usbRange.low  - ifRange.low);
        usbLoRange.sbSelect = SidebandSelect.Upper;
        LOrange lsbLoRange  = new LOrange(lsbRange.high + ifRange.low,
                                          lsbRange.low  + ifRange.high);
        lsbLoRange.sbSelect = SidebandSelect.Lower;
        // If only one sideband is used then we are done
        if (!lsbUsed || !usbUsed) {
            LOrange lor[]       = new LOrange[1];
            if (lsbUsed) lor[0] = lsbLoRange;
            else         lor[0] = usbLoRange;
            return lor;
        }    
        // See if the ranges intersect
        LOrange loRange  = usbLoRange.intersection(lsbLoRange);
        loRange.sbSelect = SidebandSelect.Both;
        boolean singleLOrange = loRange.isValidIntersection();
        //spew("usbLOrange:" + usbLoRange);
        //spew("lsbLOrange:" + lsbLoRange);
        //spew("LOrange:" + loRange + "  singleLOrange:" singleLOrange);
        if (useBothSidebands) {
            if (singleLOrange) {
                emsg  = "Use of both sidebands inconsistent with ";
                emsg += "a single LO range: programming error!";
                throw new Exception(emsg);
            }
        }
        else if (!singleLOrange) {
            String f = "All the requested frequencies in the two sidebands ";
            f += "covering ranges LSB=%s  USB=%s ";
            f += "cannot be placed in the IF range [%.1f, %.1f]GHz ";
            f += "at the same time.";
            final int SKYFREQ_RANGE_PRECISION = 3;
            final int p = SKYFREQ_RANGE_PRECISION;
            emsg = String.format(f, 
                        rangeRepresentation(lsbRange.low, lsbRange.high, p), 
                        rangeRepresentation(usbRange.low, usbRange.high, p),
                        1e-9*ifRange.low,  1e-9*ifRange.high);
            emsg += dopplerWarning(bbinfo, ifSpan);
            throw new Exception(emsg);
        } 
        // Handle DSB use case, which limits the LOrange to a predefined
        // region about the center.
        if (!BasebandInfo.isOnlySSB(bbinfo)) {
            for (BasebandInfo bb: bbinfo) {
                if (bb.isDSB()) {
                    // Search a small fixed range around the middle of u/l
                    // perhaps further reduced by physical range of LO
                    double loCenter = (bb.upper.skyFreqHz+bb.lower.skyFreqHz)/2;
                    final double halfSpan = 0.5*dsbLOsearchSpan;
                    LOrange dsbLOrange =
                        new LOrange(loCenter-halfSpan, loCenter+halfSpan);
                    loRange = loRange.intersection(dsbLOrange);    
                    //spew("LOrange:" + loRange+ "   " + singleLOrange);
                    // Only first DSB baseband is used for this limitation
                    break;
                }    
            }
        }
        LOrange lor[];
        if (singleLOrange) {
            lor    = new LOrange[1];
            lor[0] = loRange;
        }    
        else {
            lor    = new LOrange[2];
            lor[0] = usbLoRange;
            lor[1] = lsbLoRange;
        }
        
        // Now check for IF processor violations; the band pairs 0/1 & 2/3 
        // must come from the same sideband for 2SB receivers
        if (band.sb() == SB.SB2) {
            SidebandSelect unk = SidebandSelect.Unknown;
            SidebandSelect sb[] = new SidebandSelect[4];
            for (int i=0; i < 4; i++) sb[i] = unk;
            for (BasebandInfo b: bbinfo) {
                sb[b.index] = b.sbSelect;
            }
            error = false;
            String msg0 = "";
            String msg1 = "";
            if ((sb[0] != unk) && (sb[1] != unk) && (sb[0] != sb[1])) {
                error = true;
                msg0  = " basebands 0 & 1 use " + sb[0] + " and " + sb[1];
            }
            if ((sb[2] != unk) && (sb[3] != unk) && (sb[2] != sb[3])) {
                error = true;
                msg1  = " basebands 2 & 3 use " + sb[2] + " and " + sb[3];
            }
            if (error) {
                emsg = "Basebands pairs 0/1 and 2/3 must use ";
                emsg += "the same sideband witnin the pair, but\n";
                emsg += msg0;
                if ((msg0 != "") && (msg1 != "")) emsg += " and\n";
                emsg += msg1;
                throw new Exception(emsg);
            }
        }
        
        return lor;                    
    } 

    private double lo2EdgeCorrection(double lo2) {
	    if ((lo2 < DYTOLowFreq) && (lo2 > (DYTOLowFreq - LO2CombSpacing))) {
            return DYTOLowFreq;
        }
	    if ((lo2 > DYTOHighFreq) && (lo2 < (DYTOHighFreq + LO2CombSpacing))) {
            return DYTOHighFreq;
        }
        return lo2;
	} 

    // Input nominal FTS2 freq and a new FTS2 freq will be returned that is not
    // in avoidance range
    private double fts2Avoidance(double fts2nominal) { 
        final boolean verbose = false;
        for (int i = 0; i < nFTS2Avoidance; i++) {
            double f   = fts2AvoidanceFrequency[i];
            double flo = f - fts2AvoidanceRange[i];
            double fhi = f + fts2AvoidanceRange[i];
            if ((fts2nominal > flo) && (fts2nominal < fhi)) {
                if (fts2nominal >= f) {
                    if (verbose)
                        spew("Returning:" + fhi+ "    fts2nom="+fts2nominal);
                    return fhi;
                }
                else {
                    if (verbose)
                        spew("Returning:" + flo+ "    fts2nom="+fts2nominal);
                    return flo;
                }
            }
        }
        return fts2nominal;
    }
         


    // Compute a simple minded LO1 offset for a single baseband.
    // Assumes that fts2 is in the middle of its range.  
    private double LO1offset(Band band, BasebandInfo bb, int fts2sign) {
        double fts2      = fts2sign*fts2Avoidance(FTS2CenterFreq);
        double loOffset  = 0;
        if (bb.isDSB()) {
             double LO1 = (bb.upper.skyFreqHz + bb.lower.skyFreqHz)/2.0;
             double wu  = bb.upper.weight;
             double wl  = bb.lower.weight;
             double fts2Offset = fts2*(wl-wu)/(wu+wl);
             loOffset = (LO1 + fts2Offset)%LO2CombSpacing;
        }
        else {
            int sbSign   = 1;
            SSBbasebandInfo sb = bb.sb.elementAt(0);
            if (sb.sbSelect == SidebandSelect.Lower) sbSign = -1;
            else                                     sbSign =  1;
            // BB sky freqs modulo 125MHz
            double fsky = sb.skyFreqHz;
            loOffset    = (fsky - sbSign*fts2)%LO2CombSpacing;
            if (false) {
                String f  = "%6sfsky=%7.3f fts2sb=%2d skysb=%2d ";
                       f +="loOff=%7.3f loOffNorm=%7.3f"; 
                String s = String.format(f, "****", 1e-9*fsky, fts2sign, sbSign, 
                               1e-6*loOffset, 1e-6*loOffsetNormalize(loOffset));
                spew(s);         
            }            
        }  
        double loOffsetNorm = loOffsetNormalize(loOffset);
        return loOffsetNorm;         
    }
    // LO2 has < 40% frequency coverage, so we need to choose LO1 so that
    // LO2 can come as close to the requested freqs as possible. LO2 has
    // a comb spacing of 125MHz, so it repeats at this spacing. 
    // Here we loop over all selected basebands to find the LO1 offset
    // that will minimize the final freq error when we place LO2.  
    // The basebands must be assigned to specific sidebands because this
    // affects the offset (different signs for usb/lsb). 
    // The lo1 offset value that is returned is
    // in the range of +/- LO2CombSpacing/2   
    private double selectLO1offset(Band band, Vector<BasebandInfo> bbinfo, 
            int[] fts2sb, SidebandSelect lo1sb, boolean verbose) 
                throws Exception { 
        // Simple class to store pairs of doubles, like
        // [freq, metric1]
        // Can be used for weighted errors, etc       
        class Metric {
            double[] m = new double[2];
            Metric(double m1, double m2) {
                m[0] = m1;
                m[1] = m2;
            }            
            // This assumes the metric is in Hz 
            public String toString() {
                String s = String.format("%7.3f ", 1e-6*m[0]);
                s       += String.format("%6.2f ", 1e-6*m[1]);
                return s;
            }       
        } 

        // Weighted errors as a function of lo1 offset frequency
        Vector<Metric> metrics = new Vector<Metric>();   
        
        // Grab all basebands associated with the sb for this loRange
        Vector<BasebandInfo> bbs = BasebandInfo.basebandVec(bbinfo, lo1sb);
        // Number of basebands actually used
        int nBB = bbs.size(); 
                      
        // The simple minded apparent LOoffset for each baseband
        double[] loBBoffset = new double[nBB];
        // Calc LOoffset for each baseband
        double totalW    = 0;
        int    bbSBindex = 0;
        for (BasebandInfo bb: bbs) {
            int fts2sign = fts2sb[bb.index];
            double loOffset = LO1offset(band, bb, fts2sign);  
            if (false) {
                spew(String.format("BB%d(%s)  fts2sb:%2d " +
                    " normOffset:%7.3f",
                    bb.index, bb.sbSelect, fts2sign,
                    1e-6*loOffset));
            }        
            loBBoffset[bbSBindex] = loOffset;    
            totalW += bb.weight();
            bbSBindex++;
        }
        
        // This is the only hard part. The baseband LOoffsets need to be
        // averaged (with weighting) over all basebandSBs to pick a single  
        // LOoffset, but the offsets are cyclical functions so all combinations 
        // on either wrap must be considered.        
        
        // Get LO1offsets for all combinations
        int combinations = (1 << nBB);
        for (int c=0; c<combinations; c++) {
            // Compute ave LO for this combination of wraps
            double totalWO = 0;
            int bc = 0;
            for (BasebandInfo bb: bbs) {
                int bbIdx        = bb.index;
                double weight    = bb.weight();
                double off       = loBBoffset[bc];
                int    bitSelect = (1 << bc);
                if ((c & bitSelect) != 0) {
                    if (off < 0) {
                        off += LO2CombSpacing;  // Add 2pi if neg
                    }    
                } 
                if (false) {
                    spew(String.format("  bb%d(%s): off:%5.1f  off+wrap:%5.1f",
                         bbIdx, bb.sbSelect, 1e-6*loBBoffset[bc], 1e-6*off)); 
                }         
                totalWO      += weight*off;
                bc++;
            } 
            double aveLOoffset = totalWO/totalW; 
            if (false) spew("  aveLOoff:"+1e-6*aveLOoffset); 
            // Now convert back to +/- pi range
            if (aveLOoffset >= LO2CombSpacing/2) {
                aveLOoffset -= LO2CombSpacing;
            } 
            // Compute error for this ave offset 
            double totalWE = 0;
            bc = 0;
            for (BasebandInfo bb: bbs) {
                double weight = bb.weight();
                double off    = loBBoffset[bc];
                double err    = Math.abs(off - aveLOoffset);
                // Error can never be greater than pi
                if (err > LO2CombSpacing/2) {
                    err -= LO2CombSpacing;
                    err = Math.abs(err);
                }    
                totalWE      += weight*err;
                bc++;
            } 
            // Insert into table of LOoffsets and errors
            metrics.add(new Metric(aveLOoffset, totalWE/totalW));               
        }

        // Now pick best FTS offset; first one with lowest merit
        double lowestMerit = metrics.elementAt(0).m[1];
        double lo1Offset   = metrics.elementAt(0).m[0];
        for (Metric m: metrics) { 
            if (m.m[1] < lowestMerit) {
                lowestMerit = m.m[1];
                lo1Offset   = m.m[0];
            }    
        } 
        if (lo1Offset > LO2CombSpacing/2) lo1Offset -= LO2CombSpacing; 
        if (false) {               
            for (Metric m: metrics) { 
                spew("Metrics:" + m.toString());
            }
            String f = "LO1 offset: %7.2f MHz  weightedError:%5.2f MHz";
            String s = String.format(f,  lo1Offset*1e-6, lowestMerit*1e-6);
            spew(s);
        } 
        return lo1Offset;     
    } 
    // Find solutions and append to input vector of solutions
    private void findSolutions(Band band, Vector<BasebandInfo> bbinfo,
            double lo1Offset, int[] fts2sb, LOrange loRange,
            Vector<TuningParameters> sol,
            boolean verbose) throws Exception {
        final double loLow  = loRange.low ;   
        final double loHigh = loRange.high ;  
        
        if (verbose)spew(String.format("****LO1offset: %5.1f", 1e-6*lo1Offset)); 
        double flo1 = lo1Offset + 
                          LO2CombSpacing* Math.floor(loLow/LO2CombSpacing) ;
        double fstop = lo1Offset + 
                           LO2CombSpacing*Math.ceil(loHigh/LO2CombSpacing) ;
        final double if2   = DigitizerFreq - BandOffset;                   
        final double floog = FLOOGCenterFreq;
        // The solutions will go in this vector
        Vector<TuningParameters> trial = new Vector<TuningParameters>();
        // Loop over full range of LO1 frequencies
        while (flo1 <= fstop) {
            // Loop over both FTS1 sidebands    
            for (int i = 0; i<2; i++) {
                TuningParameters t = new TuningParameters();
                t.LO2 = new LO2Parameters[4];
                for (int lo2=0; lo2<4; lo2++) {
                    t.LO2[lo2] = new LO2Parameters();
                }    
                
                int fts1sb           = i*2 - 1;
                boolean fts1Tunehigh = (fts1sb == 1);
                short coldX          = coldMultiplier(band);
                double flodriver     = flo1/coldX;
                    
                t.receiverBand     = band.receiverBand();
                t.LSFrequency      = flodriver - fts1sb*floog;
                t.FLOOGFrequency   = floog;
                t.tuneHigh         = fts1Tunehigh; 
                t.coldMultiplier   = coldX;
                
                // Initialize
                double sumW    = 0;
                double sumWerr = 0; 
                int sbsign     = 1;
                // Do DSB basebands   
                for (BasebandInfo bb: BasebandInfo.dsbBasebandVec(bbinfo)) {
                    if (false) {
                        String s  = "DSB loop, BBindex:" + bb.index ; 
                               s += " sb:"     + bb.sbSelect;
                               s += " loRangeSB:"+loRange.sbSelect;    
                        spew(s);
                    }           
                    int fts2sign = fts2sb[bb.index];
                    LO2Parameters lo2p = t.LO2[bb.index];
                    double wu   = bb.upper.weight;
                    double wl   = bb.lower.weight;
                    sumW       += wu + wl;
                    double lo2u = (bb.upper.skyFreqHz-flo1)  + if2;
                    double lo2l = (flo1-bb.lower.skyFreqHz)  + if2;
                    double lo2  = (wu*lo2u+wl*lo2l)/(wu+wl);
                    loadLO2params(lo2p, lo2, fts2sign, SidebandSelect.Both);
                    double lo2act = LO2Freq(lo2p);
                    sumWerr += wu*Math.abs(lo2u - lo2act);
                    sumWerr += wl*Math.abs(lo2l - lo2act);
                    if (verbose) {
                        spew(String.format(
                             "   UpperLO2req:%7.3f LowerLO2req: %7.3f" + 
                             " lo2act:%7.3f", 
                             1e-9*lo2u, 1e-9*lo2l, 1e-9*lo2act));
                        String m  = " BB%d: FTS1sb:%2d LO2:%6.3f";
                               m += " FskyUpper:%7.3f FskyLower:%7.3f ";
                               m += " LO1:%7.3f LO1off:%4.1f";
                        String s  = 
                            String.format(m, bb.index, fts1sb,
                                              lo2*1e-9, 
                                              1e-9*bb.upper.skyFreqHz,
                                              1e-9*bb.lower.skyFreqHz,
                                              flo1*1e-9, lo1Offset*1e-6);
                            spew(s);
                    }       

                }                   
                for (SSBbasebandInfo sb: 
                        BasebandInfo.ssbBasebandVec(bbinfo, loRange.sbSelect)) {
                    int bbIndex = sb.bbIndex;
                    int fts2sign       = fts2sb[bbIndex];
                    if (verbose) {
                        String s  = "SSB loop for BBindex:" + bbIndex ; 
                               s += " sb:"     + sb.sbSelect;
                               s += String.format(" lo1:%.3f", 1e-9*flo1);
                               s += " loRange:" + loRange.toString(9);
                               s += " loRangeSB:"+loRange.sbSelect; 
                               s += " FTS2sb:"+fts2sign;   
                        spew(s);
                    }           
                    LO2Parameters lo2p = t.LO2[bbIndex];
                    double w  = sb.weight;
                    sumW     += w;
                    if (sb.sbSelect == SidebandSelect.Upper) sbsign =  1;
                    else                                     sbsign = -1;
                    // LO2 stuff
                    double lo2ideal = sbsign*(sb.skyFreqHz-flo1) + if2;
                    double lo2 = lo2EdgeCorrection(lo2ideal);
                    loadLO2params(lo2p, lo2, fts2sign, sb.sbSelect);
                    double lo2act = LO2Freq(lo2p);
                    sumWerr += w*Math.abs(lo2ideal - lo2act);
                    if (false) {
                        spew(String.format(" SBand:" + sb.sbSelect + 
                             "  lo2req:%7.3f lo2act:%7.3f", 
                             1e-9*lo2, 1e-9*lo2act));
                        String m  = " BB%d: LO1sb:%2d FTS1sb:%2d";
                               m += " LO2:%6.3f Fsky:%7.3f";
                               m += " LO1:%7.3f LO1off:%4.1f";
                        String s  = 
                            String.format(m, bbIndex, sbsign, fts1sb,
                                              lo2*1e-9, 
                                              1e-9*sb.skyFreqHz,
                                              flo1*1e-9, lo1Offset*1e-6);
                            spew(s);
                    }       

                } // End loop over SSBbasebands  
                double werr = sumWerr/sumW;                 
                t.weightedError = werr; 
                fixUnusedBasebands(t, bbinfo);                
                trial.add(t); 
                if (false && verbose) {
                    spew(String.format("WeightedError: %5.2f MHz", 1e-6*werr));
                }    
            } // End loop over fts1 sidebands
            flo1 += LO2CombSpacing;
        }
        // Add legitimate solutions to tuning solutions set
        for (TuningParameters t: trial) {
            if (legitimateSolution(t, verbose)) tuningSolutions.add(t);
        }     
    } 

    /**
     * Over-ride with configuration specific solution scoring if desired. 
     * Returns index of first solution with highest score.
     * The scoring here is different for SSB and DSB use cases 
     * (DSB use case used when *any* baseband is DSB).
     * SSB Use Case:
     *  Current SSB metric has the following terms: 
     *   1) Weighted error
     *   2) Distance from IF center
     *   3) LO1 OT compatible
     *  Score = 10*(A*weightedError+B*IFerror+C*LO1OTcompat)/(A+B+C)
     *  Using A= 5, B = 1, C=0.01
     * DSB Use Case:
     *  Only freq error is used, normalized by 200MHz.
     *  Score = 10*(1-weightedError/200MHz)
     *
     * Detailed description of the SSB terms:
     *  1) Weighted error
     *  The weighted error term takes the weightedError and normalizes by
     *  the max error and subtracts this from one so that the range is from 0 to
     *  unity.     
     *  2) Distance from IF center
     *  The IF error is normalized by max IF range/2 and is then subtracted
     *  from one so that the max is about unity. If a baseband is outside the
     *  IF range then it will get the max error of IFrange/2 and its weight will
     *  increase in proportion to the error with steps at 100MHz and 500Mhz. 
     *  3) LO1 OT compatible
     *  The OT expects the LO1 to be at least IFhigh from the receiver band
     *  edge. This term is 1 when it satisfies this constraint and 0 when it
     *  does not.
     */
    protected short scoreSolutions(Vector<BasebandInfo> bbinfo, Band band,
            boolean verbose) {
        double maxScore          = 0;
        int    indexMaxScore     = 0;
        double LSforMaxScore     = 0;  

        final double ifHigh      = IFhighFreq(band);
        final double ifLow       = IFlowFreq(band);
        final double ifErrorNorm = (ifHigh - ifLow)/2 ;
        final double rxbandtop   = bandHighFreq(band);
        final double rxbandbot   = bandLowFreq(band);
        double IFcenter          = (ifLow + ifHigh)/2;  
        double wErrNorm          = 0.0;
        if (BasebandInfo.isOnlySSB(bbinfo)) {
            wErrNorm = wErrNormSSB;
        }
        else {    
            wErrNorm = wErrNormDSB;
        }
        int index = 0;  
        for (TuningParameters t: tuningSolutions) {
            double sumW      = 0.0;
            double sumWifErr = 0.0;
            for (SSBbasebandInfo b: BasebandInfo.sidebandVec(bbinfo)) {
                if ((t.LO2[b.bbIndex].skyFreqSideband == SkyFreqSideband.LSB) &&
                    (b.sbSelect != SidebandSelect.Lower)) continue;
                if ((t.LO2[b.bbIndex].skyFreqSideband != SkyFreqSideband.LSB) &&
                    (b.sbSelect == SidebandSelect.Lower)) continue;
                double w            = b.weight;
                LO2Parameters lo2p  = t.LO2[b.bbIndex];
                double ifFreqReq    = b.ifFreqHz;
                // If requested IFfreq is essentially zero, default to center
                if (ifFreqReq < 100) ifFreqReq = IFcenter;
                double ifFreq       = IFfrequency(lo2p);
                double absErr = Math.abs(ifFreqReq - ifFreq);
                // The slight non-linear increase in the error:
                // o prevents baseband combos with large and small errors from 
                // looking as good as a combo with all errors in the middle.
                // o downweights larger errors
                double ifError = Math.pow(absErr/ifErrorNorm, 1.1);
                sumW      += w;
                sumWifErr += w*ifError ;
                if (false) {
                    String s = String.format(
						"  BB:%d %s used:%b IFreq:%.2f IF:%.2f IFerr:%.2f" +
                        " absErr:%.3f W:%.1f sumW:%.1f sumWifErr:%.1f",  
                        b.bbIndex,
                        b.sbSelect,
						b.isUsed(),
                        ifFreqReq*1e-9,
                        ifFreq*1e-9,
                        ifError,
	                    absErr*1e-9,
                        w, sumW, sumWifErr);
                    spew(s);
                }
            }
            // Big errors => low score (low ifErrTerm)
            double ifErrTerm = 1.0 - sumWifErr/sumW;
            if (false) spew("IFerrTerm: " + ifErrTerm);
            // Now the term for the frequency errors
            double wErr = t.weightedError;
            if (wErr > wErrNorm) wErr = wErrNorm; // Prevent negative scores
            double wErrTerm  = (wErrNorm - wErr)/wErrNorm;
            // Now the LO1 OT compatible term
            final double lo1f = LO1Freq(t);
            // LO1 distance from band edge
            final double distFromBandEdge = 
                        Math.min((rxbandtop - lo1f),(lo1f - rxbandbot));
            final double possibleExtend = FTS1HighFreq*t.coldMultiplier;
            final double ifEffectiveTop = ifHigh - possibleExtend;
            double lo1OTcompat = 0.0;
            // This gives 1.0 if far enough away, otherwise sloping to 0.5
            // at the extended value.
            if (distFromBandEdge > ifHigh) {
                lo1OTcompat = 1.0;
            }
            else if (distFromBandEdge > ifEffectiveTop) {
                lo1OTcompat = 1.0 - 0.5*(ifHigh - distFromBandEdge)/
                                        (ifHigh - ifEffectiveTop);
            }
            if (false) {
                String m = String.format("LO1:%7.3f  lo1OTcompat:%.2f ",
                     1e-9*lo1f, lo1OTcompat);
                spew(m);
            }
            // Now the total score
            double score = 0.0;
            if (BasebandInfo.isOnlySSB(bbinfo)) {
                double scoreComps = 0.0;
                scoreComps += scoreA*wErrTerm;
                scoreComps += scoreB*ifErrTerm;
                scoreComps += scoreC*lo1OTcompat;
                if (false) {
                    String m = String.format("Scores: %7.3f  %7.3f  %7.3f",
                                scoreA*wErrTerm,scoreB*ifErrTerm,
                                scoreC*lo1OTcompat);
                    spew(m);
                }
                score = scoreScale*scoreComps/(scoreA+scoreB+scoreC);
            }
            else {
                score = scoreScale*wErrTerm;
            } 
            // Find best score                   
            if ((score-maxScore) > 0.0001) {
                maxScore      = score;
                indexMaxScore = index;
                LSforMaxScore = t.LSFrequency;
            }  
            else if (Math.abs(score-maxScore) < 0.0001) {
                // Score is essentially same as previous best
                // Keep one with lowest LS to avoid some LS range problems
                if (t.LSFrequency < LSforMaxScore) {
                    maxScore      = score;
                    indexMaxScore = index;
                    LSforMaxScore = t.LSFrequency;        
                }  
            }
            if (false) {
                String f = "i:%3d  ferrTerm:%5.3f   iferrTerm:%5.3f  OTcomoat:%5.3f";
                String s = String.format(f, index, wErrTerm, ifErrTerm,
                            lo1OTcompat);
                spew(s);
            }                   
            t.score = score;
            index++;
        }
        return (short)indexMaxScore;
    }
    
    // Over-ride with configuration specific solution editing 
    protected void editSolutions(Vector<BasebandInfo> bbinfo, boolean verbose) 
            throws Exception {
    } 
    // Goes through the vector of solutions and inserts the solutionIndex
    private void setTuningParameterIndices() {
        short solutionIndex = 0;
        for (TuningParameters t: tuningSolutions) t.index = solutionIndex++;
    }
    // Goes through the vector of solutions and assigns IF processors
    private void selectIFprocessors(Band band) {
        for (TuningParameters t: tuningSolutions) {
            if (band.sb() == SB.SB2) {
                // The two sideband receivers must select a sideband
                boolean firstBB = true;
                for (int b = 0; b < 4; b++) {
                    if ((t.LO2[b] != null) && t.LO2[b].isUsed) {
                        boolean selUSB = 
                            t.LO2[b].skyFreqSideband == SkyFreqSideband.USB;
                        if (firstBB) {
                            firstBB = false;
                            t.band0band1selectUSB = selUSB;
                            t.band2band3selectUSB = selUSB;
                        }
                        else {
                            if (b <= 1) t.band0band1selectUSB = selUSB; 
                            else        t.band2band3selectUSB = selUSB; 
                        }
                    }
                }
            }
            else {
                // Single and Double Sideband Receiver switch selection...
                // Yes, System Block Diagram rev P says even band 2 (all LSB)
                // goes to the USB input.
                t.band0band1selectUSB = true;
                t.band2band3selectUSB = true;                
            }
        }
    }
    
    // Fills in unused basebands with setup from first used baseband, in pairs      
    private void fixUnusedBasebands(TuningParameters t, 
                        Vector<BasebandInfo> bbinfo) {
        if (bbinfo.size() == 4) return; // There are no unused basebands
        int g = bbinfo.elementAt(0).index;
        LO2Parameters lo2pPair0 = t.LO2[g];
        LO2Parameters lo2pPair1 = t.LO2[g];
        for (BasebandInfo bb: bbinfo){
            g = bb.index;
            if (g <= 1) lo2pPair0 = t.LO2[g];
            if (g >  1) lo2pPair1 = t.LO2[g];
        }
        //double f = bb.skyFreqHz;
        for (int b=0; b<4; b++) {
            if (basebandIsUsed(bbinfo, b) == false) {
                if (b <= 1) t.LO2[b] = cloneLO2Parameters(lo2pPair0); 
                else        t.LO2[b] = cloneLO2Parameters(lo2pPair1); 
                t.LO2[b].isUsed = false;
            }    
        }
    }



    //======================== Helpers ========================== 

    // Helper
    private boolean basebandIsUsed(Vector<BasebandInfo> bbinfo, int bbIndex) {
        for (BasebandInfo b: bbinfo) {
            if ((b.index == bbIndex) && b.isUsed) return true;
        }
        return false;         
    }
    // Done as one single string so that all lines are together in the logs
    private void dumpBB(Band band, Vector<BasebandInfo> bbinfo) {
        //String st = "------------------------------------";
        String st = "================"+band+"================\n";
        st += "Baseband    Freq   SB  Weight   SBsel\n";
        st += "--------  ------- ---  ------  ------\n";
        for (SSBbasebandInfo s: BasebandInfo.sidebandVec(bbinfo)) {   
            String fmt = "    %d     %7.3f   %s  %6.2f  %6s\n"; 
            String str = String.format(fmt,
                    s.bbIndex, 1e-9*s.skyFreqHz, 
                    s.sbSelect.toShortString(), s.weight, 
                    BasebandInfo.basebandWithIndex(bbinfo, s.bbIndex).
                            sbSelect.toString());
            st += str;         
        }         
        st += "=====================================";
        spewToOperator(st);
    } 

    static double LO2Freq(LO2Parameters parameters) {
        return parameters.DYTOFrequency;
    } 
    
    // Normalizes an LO offset to range +/- LO2CombSpacing/2
    double loOffsetNormalize(double loOffset) {
        double harmonic = loOffset/LO2CombSpacing;
        double normUnity = harmonic%1.0;
        if (normUnity > 0.5) normUnity -= 1.0;
        else if (normUnity < -0.5) normUnity += 1.0;
        double newLOoffset = normUnity*LO2CombSpacing;
        if (false) {
            spew("h:"+harmonic+" n:" + normUnity + " loOffin:" + 1e-6*loOffset +
             " loOffout:" + 1e-6*newLOoffset);
        }     
        return newLOoffset;
    }     
        
    /**
     * Return the IF frequency for the band center in Hz.
     */
    static public double IFfrequency(LO2Parameters lo2p) {
        final double lo2 = LO2Freq(lo2p);
        return lo2 - DigitizerFreq + BandOffset;
    }
        
    /**
     * Check that IF freq is within the IF range.
     */
    private boolean checkIFrange(LO2Parameters lo2p, Band band) {
        double ifCenter = IFfrequency(lo2p);
        double IFlow    = ifCenter - BandOffset; // Low IF freq of band
        double IFhigh   = ifCenter + BandOffset; // High IF freq of band
        boolean accept  = ((IFlow  >= IFlowFreq(band)) &&
                           (IFhigh <= IFhighFreq(band)));
                
        if (false) spew ("LO2 IF range check: accept=" + accept + 
                        " LO2="   + LO2Freq(lo2p) + 
                        " IFlow=" + IFlow + 
                        " IFhi="  + IFhigh);
        return accept;                
    }
       
    /**
     * Check a tuning solution for legitimacy, by checking all hw params
     * to make sure they are in range.
     */
    private boolean legitimateSolution(TuningParameters t, boolean verbose) {
        final String emsg = "LegitimateSolution test failed: ";
        if (verbose) dumpTuning(t);
        Band band = Band.Band1;
        try {
            band = Band.band(t.receiverBand);
        } catch (Exception e) { 
            spew("Trouble in legitimateSolution translating receiver band" +
                  e.toString());
            return false;      
        }    

        double floog = t.FLOOGFrequency;
        if ((floog > FLOOGHighFreq) || (floog < FLOOGLowFreq)) {
            String f = "FLOOG (%.3f MHz) out of range.";
            if (verbose)spew(emsg + String.format(f, 1e-6*floog));
            return false;
        }  
        for (int i=0; i<4; i++) {   
            double fts2 = t.LO2[i].FTSFrequency;
            if ((fts2 > FTS2HighFreq) || (fts2 < FTS2LowFreq)) {
                String f = "Baseband%d FTS2(%.3f MHz) out of range.";
                if (verbose)spew(emsg + String.format(f, i, 1e-6*fts2));
                return false;
            } 
            double dyto = t.LO2[i].DYTOFrequency;
            if ((dyto > DYTOHighFreq) || (dyto < DYTOLowFreq)) {
                String f = "Baseband%d DYTO(%.3f GHz) out of range.";
                if (verbose)spew(emsg + String.format(f, i, 1e-9*dyto));
                return false;
            } 
        } 
        // The LOdriver bounds are not hard, give them a tiny bit extra 
        double loDriverFudge = 2e6;       
        double loDriver   = LOdriver(t);
        double driverLow  = lodriverLowFreq(band)  - loDriverFudge;
        double driverHigh = lodriverHighFreq(band) + loDriverFudge;

        if ((loDriver < driverLow) ||
            (loDriver > driverHigh)) {
            final String f = "LOdriver (%.3f GHz) out of range (%.3f:%.3f)";
            if (verbose) {
                spew(emsg+String.format(f, 1e-9*loDriver,
                    1e-9*driverLow, 1e-9*driverHigh));
            }        
            return false;
        }    

        if (verbose) spew ("Tuning accepted");
        return true;
    } 
    
    // Returns description of underlying hardware parameters 
    // When extended in true you get extra inferred limits
    String hardwareParameterString(boolean extended) {
        return Band.hardwareParameterString(this, extended);
    }    
    
    // Returns non-extended rendition of the hardware parameters
    String hardwareParameterString() {
        return hardwareParameterString(false);
    }    


    /**
     * Calculate the actual LOdriver (WCA) output frequency.
     */
    static double LOdriver(TuningParameters p) {
        double sign = p.tuneHigh? 1: -1;
        return laserSynth(p) + sign * p.FLOOGFrequency;
 
    }

    /**
     * Return a vector contaning all the receiver bands that can observe at the
     * specified frequency. The vector will be of length 0, 1 or 2.
     */
    private Vector<Band> selectBands(double frequencyInHz) {
        this.logger.finest(this.getClass().getName() + ".selectBand");

        Vector<Band> returnVal = new Vector<Band>();
        for (Band b: Band.values()) {
            if ((frequencyInHz >= bandLowFreq(b)  + BandOffset) &&
                (frequencyInHz <= bandHighFreq(b) - BandOffset)) {
                    returnVal.add(b);
            }
        }
        return returnVal;
    }
    // Take a desired lo2 freq and load up LO2 parameters, fixing for hw lims
    void loadLO2params(LO2Parameters lo2p, 
            double lo2, int fts2sign, SidebandSelect sbSelect) {
        double h = (lo2-fts2sign*FTS2CenterFreq)/LO2CombSpacing;
        double combHarm  = Math.rint(h); // Round
        double comb      = LO2CombSpacing*combHarm;
        double fts2trial = Math.abs(lo2-comb);
        double fts2      = fts2trial;
        if (fts2 < FTS2LowFreq) {
            fts2 = FTS2LowFreq; 
        }
        else if (fts2 > FTS2HighFreq) {
            fts2 = FTS2HighFreq; 
        }
        fts2 = fts2Avoidance(fts2);

        lo2p.DYTOFrequency = comb+fts2sign*fts2; // 2nd LO frequency
        lo2p.FTSFrequency  = fts2;
        lo2p.tuneHigh      = (fts2sign == 1);
        if (sbSelect == SidebandSelect.Upper) {
            lo2p.skyFreqSideband = SkyFreqSideband.USB;
        }
        else if (sbSelect == SidebandSelect.Lower) {
            lo2p.skyFreqSideband = SkyFreqSideband.LSB;
        }
        else if (sbSelect == SidebandSelect.Both) {
            lo2p.skyFreqSideband = SkyFreqSideband.DSB;
        }
        else {
            spew("sbSelect("+sbSelect+") not supported");
        }
        lo2p.use12GHzFilter = (lo2p.DYTOFrequency > Use12GHzFilterThreshold);
        lo2p.isUsed = true;
    }
    
    LO2Parameters cloneLO2Parameters(LO2Parameters lo2p) {
        LO2Parameters l = new LO2Parameters();
        l.DYTOFrequency   = lo2p.DYTOFrequency; 
        l.FTSFrequency    = lo2p.FTSFrequency;
        l.tuneHigh        = lo2p.tuneHigh;
        l.skyFreqSideband = lo2p.skyFreqSideband;
        l.use12GHzFilter  = lo2p.use12GHzFilter;
        l.isUsed          = lo2p.isUsed;
        return l;
    }    
      
    // Go through all bb and sb to determine total freq range
    private FreqRange basebandRange(Vector<BasebandInfo> bbinfo) {
        FreqRange range = new FreqRange();
        for (BasebandInfo b: bbinfo) {
            for (SSBbasebandInfo s: b.sb) {
                if (s != null) range.add(s.skyFreqHz);
            }
        }
        return range;
    }            
    private String sbToString(int sb) {
        if (sb ==  1) return "USB";
        if (sb == -1) return "LSB";
        if (sb ==  0) return "2SB";
        return "???";
    } 

    private FreqRange ifRange(Band band) {
        double ifLow   = IFlowFreq(band)  + BandOffset;
        double ifHigh  = IFhighFreq(band) - BandOffset;
        return new FreqRange(ifLow, ifHigh);
    }

// ===========================================================================
//======================= Protected methods ==================================
    // Different configurations (e.g. ATF) inherit from this class and 
    // over-ride some of these methods to describe differences in the hardware.
    
    protected int numLO() {
        return numLO;
    }          
    // Over-ride with configuration specific implementation
    protected  short warmMultiplier(Band band) {
        return band.warmMultiplier();
    }
    // Over-ride with configuration specific implementation
    protected  short coldMultiplier(Band band) {
        return band.coldMultiplier();
    }
    // Over-ride with configuration specific implementation
    protected  double bandLowFreq(Band band) {
        return band.bandLowFreq();
    }
    // Over-ride with configuration specific implementation
    protected  double bandHighFreq(Band band) {
        return band.bandHighFreq();
    }    
    // Over-ride with configuration specific implementation
    protected double IFlowFreq(Band band) {
        return band.IFlowFreq();
    }           
    // Over-ride with configuration specific implementation
    protected double IFhighFreq(Band band) {
        return band.IFhighFreq();
    } 
    // Over-ride with configuration specific implementation
    protected double lodriverLowFreq(Band band) {
        return band.lodriverLowFreq();
    }    
    // Over-ride with configuration specific implementation
    protected double lodriverHighFreq(Band band) {
        return band.lodriverHighFreq();
    } 
    // Over-ride with configuration specific implementation
    protected double lo1LowFreq(Band band) {
        return band.lo1LowFreq();
    }    
    // Over-ride with configuration specific implementation
    protected double lo1HighFreq(Band band) {
        return band.lo1HighFreq();
    } 
    // Over-ride with configuration specific implementation
    protected SB sb(Band band) {
        return band.sb();
    }       

//==========================================================================

   /**
     * Internal, just for testing. Has a null logger and directs debugging
     * output to System.out
     */
    protected LOsolutions() {
        constructorInitialization((Logger) null, "ALMA", true);
    }
    
    /**
     * Provides common initialization for all constructors
     * @param logger reference to a logger, null allowed
     * @param systemName used to identify hardware parameters
     * @param testMode directs debugging output (spew) to System.out
     */
    private void constructorInitialization(Logger logger,
            String systemName, boolean testMode) {
        fts2AvoidanceFrequency[0] = 31.250e6;
        fts2AvoidanceFrequency[1] = 25.000e6;
        fts2AvoidanceFrequency[2] = 35.714e6;
        fts2AvoidanceFrequency[3] = 20.833e6;
        fts2AvoidanceFrequency[4] = 27.778e6;
        fts2AvoidanceFrequency[5] = 37.500e6;
        fts2AvoidanceRange[0]     = 100.0e3;
        fts2AvoidanceRange[1]     =  80.0e3;
        fts2AvoidanceRange[2]     =  57.1e3;
        fts2AvoidanceRange[3]     =  66.7e3;
        fts2AvoidanceRange[4]     =  44.4e3;
        fts2AvoidanceRange[5]     =  40.0e3;
        tuningSolutions.clear();
        this.systemName = systemName;
        this.testMode = testMode;
        if (testMode || (logger == null)) {
            this.logger = Logger.getLogger("global");
        }    
        else {
	    this.logger = logger; 
	    this.acsLogger = AcsLogger.fromJdkLogger(logger, null);
	}
     }
    
    void spew(String s) {
        if (testMode) System.out.println(s);
        else          this.logger.info(s);
    } 

    void spewToOperator(String s) {
        if (testMode) System.out.println(s);
        else          this.acsLogger.logToAudience(Level.INFO, s, OPERATOR.value);
    } 

   
//=============================== Misc ===========================  
    void dumpTunings(boolean physical, boolean freqMode) {
        Misc.dumpTunings(tuningSolutions, baseband, physical, freqMode);
    }
    void dumpTunings(boolean physical) {
        boolean freqMode = false;
        dumpTunings(physical, freqMode);
    }
    void dumpTunings() {
        boolean physicalMode = false;
        dumpTunings(physicalMode);
    }        
      
}


