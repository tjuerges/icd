/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * $Id$
 */

package alma.correlatorSrc.CorrConfigValidator;

import alma.Correlator.*;
import alma.hla.datamodel.enumeration.*;
import alma.entity.xmlbinding.schedblock.AbstractBaseBandConfigT;
import alma.entity.xmlbinding.schedblock.AbstractCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.AbstractSpectralWindowT;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SpectralSpecTDescriptor;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.ChannelAverageRegionT;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;

/** This abstract base class provides a definition for derived classes to
 ** parse a SpectralSpec XML instance which contains a correlator configuration.
 ** The intention is to derive from this class and to implement the specific
 ** generated classes in the APDM for either the BL or ACA correlator configuration.
 ** Usage is to instantiate a derived object and then invoke the various functions.
 */
public abstract class XMLParserBase
{
    /** Translate a spectral spec XML string to a correlator configuration. This is needed by 
     ** the CCC computer to parse the XML and allow for it to pass a CorrelatorConfiguration
     ** IDL struct to the CDP Master (& consequently to the CDP Nodes)
     */
    public abstract CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXML )
	throws SBConversionException;

    /** Get the correlator configuration for a spectral spec.
     ** @param spectralSpec The spectral spec to parse.
     ** @return The correlator configuration in the spectral spec.
     */
    public abstract CorrelatorConfiguration getOneCorrelatorConfiguration(SpectralSpecT spectralSpec)
	throws SBConversionException;

    /** Get the baseband configurations in the correlator configuration
     ** @param corrConfig The CorrelatorConfiguration XML entity to parse
     ** @param spectralSpec The spectral spec which contains references to entities needed in the
     **	correlator configuration
     ** @return An array of BaseBandConfig objects (from ICD/CORR/idl/CorrConfig.idl)
     */
    protected abstract BaseBandConfig[] getBaseBandConfigs(AbstractCorrelatorConfigurationT corrConfig,
					        SpectralSpecT spectralSpec)
	throws SBConversionException;

    /** Get the spectral windows in a baseband config.
     ** @param baseBandConf The baseband configuration XML entity to parse
     ** @return The array of spectral windows in the baseband (from ICD/CORR/idl/CorrConfig.idl)
     */
    protected abstract SpectralWindow[] getSpectralWindows(AbstractBaseBandConfigT baseBandConf)
	throws SBConversionException;

    /** Get the channel average regions in a spectral window
     ** @param specWindow The spectral window XML entity to parse
     ** @return The array of channel average regions in the spectral window  (from ICD/CORR/idl/CorrConfig.idl)
     */
    protected abstract ChannelAverageRegion[] getChannelAvgRegions(AbstractSpectralWindowT specWindow)
	throws SBConversionException;
}
