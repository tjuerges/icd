/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * $Id: BL_XMLParser.java,v 1.23 2011/03/31 21:22:43 dguo Exp $
 */

package alma.correlatorSrc.CorrConfigValidator;

import alma.Correlator.*;
import alma.hla.datamodel.enumeration.*;
import alma.entity.xmlbinding.schedblock.AbstractBaseBandConfigT;
import alma.entity.xmlbinding.schedblock.AbstractCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.types.SpectralSpecTReceiverTypeType;
import alma.entity.xmlbinding.schedblock.AbstractSpectralWindowT;
import alma.entity.xmlbinding.schedblock.BLBaseBandConfigT;
import alma.entity.xmlbinding.schedblock.BLCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.BLSpectralWindowT;
import alma.entity.xmlbinding.schedblock.SpectralSpecTChoice2;
import alma.entity.xmlbinding.schedblock.AbstractSwitchingCycleT;
import alma.entity.xmlbinding.schedblock.BeamSwitchingCycleT;
import alma.entity.xmlbinding.schedblock.FrequencySwitchingCycleT;
import alma.entity.xmlbinding.schedblock.FrequencySetupT;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SpectralSpecTDescriptor;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.ChannelAverageRegionT;
import alma.entity.xmlbinding.schedblock.AbstractCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.types.AbstractCorrelatorConfigurationTAPCDataSetsType;
import alma.ReceiverSidebandMod.ReceiverSideband;
import alma.ReceiverBandMod.ReceiverBand;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import java.util.Vector;
import java.io.StringReader;

/** This class implements the APDM Spectral spec parser to obtain a BL correlator configuration
 */
public class  BL_XMLParser extends XMLParserBase
{
    /** Translate a spectral spec XML string to a correlator configuration. This is needed by 
     ** the CCC computer to parse the XML and allow for it to pass a CorrelatorConfiguration
     ** IDL struct to the CDP Master (& consequently to the CDP Nodes)
     */
    private boolean m_beamSwitching;
    private boolean m_frequencySwitching;
    private boolean m_unknownSwitching;
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXML )
    throws SBConversionException
    {
        CorrelatorConfiguration corrConfigIDL = null;
        SpectralSpecT spectralSpec = null;
        String extracted = null;
        int firstSpectralSpec = 0;
        int secondSpectralSpec = 0;

        // Remove space before >
        while(spectralSpecXML.contains("< ") ) {
            spectralSpecXML = spectralSpecXML.replaceAll("< ", "<");
        }
        while(spectralSpecXML.contains(" >") ) {
            spectralSpecXML = spectralSpecXML.replaceAll(" >", ">");
        }

        // Get SpectralSpec element
        if(spectralSpecXML.contains("<SpectralSpec")){
            firstSpectralSpec = spectralSpecXML.indexOf("<SpectralSpec"); 
        } 
        else if(spectralSpecXML.contains("<sbl:SpectralSpec")){
            firstSpectralSpec = spectralSpecXML.indexOf("<sbl:SpectralSpec"); 
        }
        else {
            throw new SBConversionException("No SpectralSpec element found");
        }
        
        // Second index for substring
        if(spectralSpecXML.contains("SpectralSpec>")) {
            secondSpectralSpec = spectralSpecXML.indexOf("SpectralSpec>") + 13;}
        else if(spectralSpecXML.contains("SpectralSpecT>")) {
            secondSpectralSpec = spectralSpecXML.indexOf("SpectralSpecT>") + 14;
        } 

	if (firstSpectralSpec < 0 || secondSpectralSpec < 0)
            {
                throw new SBConversionException("No SpectralSpecT element found");
            }

        // Check which switching cycle included
        if(spectralSpecXML.contains("NUTATOR_SWITCHING") ) {
            m_beamSwitching = true;
        }
        else if(spectralSpecXML.contains("FREQUENCY_SWITCHING") ) {
            m_frequencySwitching = true;
        }
        else {
            m_unknownSwitching = true;
        }

        extracted = spectralSpecXML.substring(firstSpectralSpec, 
                                              secondSpectralSpec);
        // Debug        
        //        System.out.println(extracted);

        try
        {
            spectralSpec = SpectralSpecT.unmarshalSpectralSpecT(new StringReader(extracted));
        }
        catch( org.exolab.castor.xml.MarshalException mex)
        {
            throw new SBConversionException("unmarshalling: "+
                                            mex.getMessage() );
        }
        catch( org.exolab.castor.xml.ValidationException vex)
        {
            throw new SBConversionException("validation: "+
                    vex.getMessage());
        }
        catch (java.lang.NoClassDefFoundError ncdfex)
        {
            throw new SBConversionException("translating: "+ ncdfex.toString());
        }
        catch( Exception ex )
        {
            throw new SBConversionException("Exception translating Spectral Spec: "+
                                            ex.toString());
        }
        
        try
            {
                System.out.println(" >>>>>>> " + spectralSpec.getName());
                spectralSpec.validate();
            }
        catch( org.exolab.castor.xml.ValidationException vex)
            {
                throw new SBConversionException("SpectralSpec validation Exception: "+
                                                vex.getMessage());
            }
        
        try
        {
            corrConfigIDL = getOneCorrelatorConfiguration(spectralSpec);
        }
        catch (SBConversionException sbcex)
        {
            throw sbcex;
        }
        catch (java.lang.NullPointerException npe)
        {
            throw new SBConversionException("Error parsing spectral spec: "+npe.toString());
        }	      
        return corrConfigIDL;
    }

    /** Return a single correlator configuration for a given spectral spect entity
     */
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration getOneCorrelatorConfiguration(SpectralSpecT spectralSpec)
    throws SBConversionException
    {
        BLCorrelatorConfigurationT corrConf  = spectralSpec.getSpectralSpecTChoice().getBLCorrelatorConfiguration();
        if( corrConf == null )
            throw new SBConversionException("No BL correlator configuration found");

        // Instantiate the correlator configuration which is then populated item by item
        CorrelatorConfiguration corrConfIDL = new CorrelatorConfiguration();
        String unitsStr;
        double timeUnits;
        double freqUnits;

        //
        // set LO1 frequency
        //
        unitsStr = spectralSpec.getFrequencySetup().getLO1Frequency().getUnit();
        freqUnits = getFrequencyUnits(unitsStr);
        corrConfIDL.LO1FrequencyMHz = spectralSpec.getFrequencySetup().getLO1Frequency().getContent() * freqUnits;;

        // Set the durations
        unitsStr = corrConf.getIntegrationDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.integrationDuration = (long)(corrConf.getIntegrationDuration().getContent()*timeUnits);

        unitsStr = corrConf.getChannelAverageDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.channelAverageDuration = (long)(corrConf.getChannelAverageDuration().getContent()*timeUnits);

        unitsStr = corrConf.getDumpDuration().getUnit();
        timeUnits = getTimeUnits(unitsStr);
        corrConfIDL.dumpDuration = (long)(corrConf.getDumpDuration().getContent()*timeUnits);

        SpectralSpecTReceiverTypeType rxType = spectralSpec.getReceiverType();
        if( rxType == SpectralSpecTReceiverTypeType.NOSB )
            corrConfIDL.receiverType = ReceiverSideband.NOSB;
        else if( rxType == SpectralSpecTReceiverTypeType.DSB)
            corrConfIDL.receiverType = ReceiverSideband.DSB;
        else if( rxType == SpectralSpecTReceiverTypeType.TSB )
            corrConfIDL.receiverType = ReceiverSideband.TSB;
        else if( rxType == SpectralSpecTReceiverTypeType.SSB )
            corrConfIDL.receiverType = ReceiverSideband.SSB;
        else
        {
            throw new SBConversionException("Reciever type " + rxType + " is not recognized");
        }

	// Set the receiver band
        String str;
	str = spectralSpec.getFrequencySetup().getReceiverBand().toString();
	try
	{
	    corrConfIDL.receiverBand = JReceiverBand.newReceiverBand(str);
	}
	catch (AcsJBadParameterEx ex)
	{
	    throw new SBConversionException("Receiver band");
	}

        // Set the APC info
        switch ( corrConf.getAPCDataSets().getType() )
        {
        case AbstractCorrelatorConfigurationTAPCDataSetsType.AP_UNCORRECTED_TYPE:
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
            break;
        case AbstractCorrelatorConfigurationTAPCDataSetsType.AP_CORRECTED_TYPE:
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;
            break;
        case AbstractCorrelatorConfigurationTAPCDataSetsType.AP_CORRECTED_AP_UNCORRECTED_TYPE:
            corrConfIDL.APCDataSets = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[2];
            corrConfIDL.APCDataSets[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
            corrConfIDL.APCDataSets[1] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;
            break;
        }

        // For the baseline correlator just put empty values in here. 
        corrConfIDL.ACAPhaseSwConfig = new alma.Correlator.ACAPhaseSwitchingConfigurations(false, false, 0);

        // Parse all of the baseband entities
        try
        {
            corrConfIDL.baseBands = getBaseBandConfigs(corrConf,spectralSpec);
        }
        catch (SBConversionException ex)
        {
            throw ex;
        }
        return corrConfIDL;
    }

    /**
     * Get the base band configuration IDL structures from the APDM correlator configuration.
     * 
     * This is a utility function to aid in the extraction of the CorrelatorConfiguration
     * IDL structure from the APDM.
     * 
     * @param corrConf Correlator configuration, part of the APDM
     * @return Baseband configurations, part of the CorrelatorConfiguration structure
     * @throws SBConversionException
     * @see SchedBlock.xsd
     * @see CorrConfigIDL.idl
     */
    //---------------------------------------------------------------------------------
    protected BaseBandConfig[] getBaseBandConfigs(AbstractCorrelatorConfigurationT aCorrConf,
            SpectralSpecT spectralSpec)
    throws SBConversionException
    {
        FrequencySetupT frequencySetup = spectralSpec.getFrequencySetup();

        // For verify default value
        //        System.out.println("DS check point1::IsUserSpecifiedLO1: " + frequencySetup.getIsUserSpecifiedLO1());
        //        System.out.println("DS check point2::Floog value: "+ frequencySetup.getFloog().getContent());

        // Cast the abstract config to a concrete BL correlator configuration
        BLCorrelatorConfigurationT corrConf = (BLCorrelatorConfigurationT)aCorrConf;

        // Create baseband configs IDL objects for each baseband in the configuration
        int numBBConfs = corrConf.getBLBaseBandConfigCount();
        boolean enable180DWF = corrConf.getEnable180DegreeWalshFunction();
        /* Overriding these because they shouldn't matter */
        enable180DWF = false;

        String loOffsettingMode = corrConf.getLOOffsettingMode().toString();
        loOffsettingMode = "NONE";
        boolean enable90DWF = corrConf.getEnable90DegreeWalshFunction();

        BaseBandConfig[] baseBandConfsIDL = new BaseBandConfig[numBBConfs];
        String str;

        for (int bb = 0; bb < numBBConfs; bb++)
        {
            alma.entity.xmlbinding.schedblock.BLBaseBandConfigT bbConfig = corrConf.getBLBaseBandConfig(bb);
            alma.entity.xmlbinding.schedblock.BaseBandSpecificationT bbSpec = frequencySetup.getBaseBandSpecification(bb);
            baseBandConfsIDL[bb] = new BaseBandConfig();

            // Set the baseband name
            str = bbSpec.getBaseBandName().toString();
            try
            {
                baseBandConfsIDL[bb].basebandName = JBasebandName.newBasebandName(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Baseband name");
            }
            // Set the CAM - TODO: for now leave it at the baseband config level but in future needs to move to CorrConf level
            str = corrConf.getCAM().toString();
            try
            {
                baseBandConfsIDL[bb].CAM = JAccumMode.newAccumMode(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("accumulation mode");
            }
            // Set the Data products
            str = bbConfig.getDataProducts().toString();
            try
            {
                baseBandConfsIDL[bb].dataProducts = JCorrelationMode.newCorrelationMode(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("correlation mode");
            }
            // Set the sideband separation mode
            if(enable180DWF == false && enable90DWF == false && loOffsettingMode.equals("NONE"))
                {
                    str = "NONE";
                }

            else if(enable90DWF == true && loOffsettingMode.equals("TWO_LOS"))
                {
                    /* Over riding this option because we need to be able to                                                                         support it.         
                       str = "PHASE_SWITCH_SEPARATION";
                    */
                    str = "NONE";
                }
            
            else if(enable90DWF == true && loOffsettingMode.equals("THREE_LOS"))
                {
                    str = "FREQUENCY_OFFSET_SEPARATION";
                }
            else {

                throw new SBConversionException("Sideband separation setting");
            }

            try
                {
                    baseBandConfsIDL[bb].sideBandSeparationMode = JSidebandProcessingMode.newSidebandProcessingMode(str);
                }
            catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("sideband processing");
                }
            // parse spectral windows in baseband & assign to IDL element
            try
            {
                baseBandConfsIDL[bb].spectralWindows = getSpectralWindows(bbConfig);
            }
            catch (SBConversionException ex)
            {
                throw ex;
            }
            
            // get boolean UseUSB          
            baseBandConfsIDL[bb].useUSB = bbSpec.getUseUSB();
            // get boolean use12GHzFilter
            baseBandConfsIDL[bb].use12GHzFilter = bbSpec. getUse12GHzFilter();
            //
            // get LO2 frequency
            //
            baseBandConfsIDL[bb].LO2FrequencyMHz =
                bbSpec.getLO2Frequency().getContent() *
                getFrequencyUnits(bbSpec.getLO2Frequency().getUnit());

            // Need converter function apdmToAsdmSwitchingType
            str = spectralSpec.getSwitchingType().toString();

            BinSwitching_t binSwitching = new BinSwitching_t();
            try
                {
                    binSwitching.SwitchingType = JSwitchingMode.newSwitchingMode(str);
                }
            catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("bin switching");
                }

            // work space
            
            AbstractSwitchingCycleT acaSC;
            if(m_beamSwitching) {
                acaSC = spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle();
                System.out.println(" ACA_XMLParser:: getBeamSwitchingCycle called ---! "); 
                int numPos = acaSC.getNumberOfPositions();
                binSwitching.numberOfPositions = numPos;                
                binSwitching.dwellTime = new long[numPos];
                binSwitching.deadTime = new long[numPos];
                for ( int nps = 0; nps < numPos; nps++) {
                    alma.entity.xmlbinding.valuetypes.TimeT dwellTime =  spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle().getBeamSwitchingState(nps).getDwellTime();
                    alma.entity.xmlbinding.valuetypes.TimeT deadTime =  spectralSpec.getSpectralSpecTChoice2().getBeamSwitchingCycle().getBeamSwitchingState(nps).getTransition();
                    String dwellUnit = dwellTime.getUnit();
                    double dwellTimeFactor = getTimeUnits(dwellUnit);
                    binSwitching.dwellTime[nps] = (long)(dwellTime.getContent()* dwellTimeFactor );
                    String deadUnit = deadTime.getUnit();
                    double deadTimeFactor = getTimeUnits(deadUnit);
                    binSwitching.deadTime[nps] = (long)(deadTime.getContent()* deadTimeFactor );
                }
            }
            else if(m_frequencySwitching) {
                acaSC = spectralSpec.getSpectralSpecTChoice2().getFrequencySwitchingCycle();
                System.out.println(" ACA_XMLParser:: getFrequencySwitchingCycle called ---! "); 
                int numPos = acaSC.getNumberOfPositions();
                binSwitching.numberOfPositions = numPos;  
                binSwitching.dwellTime = new long[numPos];
                binSwitching.deadTime = new long[numPos];
                
                for ( int nps = 0; nps < numPos; nps++) {
                    alma.entity.xmlbinding.valuetypes.TimeT dwellTime =  spectralSpec.getSpectralSpecTChoice2().getFrequencySwitchingCycle().getFrequencySwitchingState(nps).getDwellTime();        
                    String dwellUnit = dwellTime.getUnit();
                    double dwellTimeFactor = getTimeUnits(dwellUnit);
                    binSwitching.dwellTime[nps] = (long)(dwellTime.getContent()* dwellTimeFactor );
                    binSwitching.deadTime[nps] = (long)(0);
                }
            }
            else {
                acaSC = null;
                binSwitching.numberOfPositions = 1;
                binSwitching.dwellTime = new long[1];
                binSwitching.deadTime = new long[1];
            }
            
            baseBandConfsIDL[bb].binSwitchingMode = binSwitching;        
            // End of work space
            
            baseBandConfsIDL[bb].polarizationMode = alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD;
            baseBandConfsIDL[bb].centerFreqOfResidualDelayMHz = 0.0;
        }
        return baseBandConfsIDL;
    }

    /**
     * Get the spectral window IDL structures from the APDM baseband configuration.
     * 
     * @param baseBandConf Baseband configuration, part of the APDM
     * @return The spectral windows, part of the BLCorrelatorConfiguration structure
     * @throws SBConversionException
     * @see SchedBlock.xsd
     * @see CorrConfigIDL.idl
     */
    //---------------------------------------------------------------------------------
    protected SpectralWindow[] getSpectralWindows(AbstractBaseBandConfigT aBaseBandConf)
    throws SBConversionException
    {
        // Cast abstract baseband config to concrete BL correlator bb config.
        BLBaseBandConfigT baseBandConf = (BLBaseBandConfigT)aBaseBandConf;

        // Parse up each spectral window in the baseband
        int numSpecWindows = baseBandConf.getBLSpectralWindowCount();
        String str;
        SpectralWindow[] spectWindowsIDL = new alma.Correlator.SpectralWindow[numSpecWindows];
        //  bandwidth & frequency can be in units of GHz or MHz, must set correct scale
        double units;

        for (int ssbs = 0; ssbs < numSpecWindows; ssbs++)
        {
            // get the specific spectral window
            BLSpectralWindowT spectralWindow = baseBandConf.getBLSpectralWindow(ssbs);

            // Construct an IDL version whose individual parameters are populated
            spectWindowsIDL[ssbs] = new alma.Correlator.SpectralWindow();

            // Scale the center frequency from GHz or Hz to MHz based on the units
            str = spectralWindow.getCenterFrequency().getUnit();
            units = getFrequencyUnits(str);
            spectWindowsIDL[ssbs].centerFrequencyMHz = spectralWindow.getCenterFrequency().getContent() * units;

            // Scale the effective bandwidth from GHz or Hz to MHz based on the units
            str = spectralWindow.getEffectiveBandwidth().getUnit();
            units = getFrequencyUnits(str);
            spectWindowsIDL[ssbs].effectiveBandwidthMHz = spectralWindow.getEffectiveBandwidth().getContent() * units;

            spectWindowsIDL[ssbs].effectiveNumberOfChannels= spectralWindow.getEffectiveNumberOfChannels();
            spectWindowsIDL[ssbs].spectralAveragingFactor = spectralWindow.getSpectralAveragingFactor();

            // Set sideband info
            str = spectralWindow.getSideBand().toString();
            try
            {
                spectWindowsIDL[ssbs].sideBand = JNetSideband.newNetSideband(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("net sideband mode");
            }

            spectWindowsIDL[ssbs].associatedSpectralWindowNumberInPair = spectralWindow.getAssociatedSpectralWindowNumberInPair();
            spectWindowsIDL[ssbs].useThisSpectralWindow = spectralWindow.getUseThisSpectralWindow();

            // Set windowing function
            str = spectralWindow.getWindowFunction().toString();
            try
            {
                spectWindowsIDL[ssbs].windowFunction = JWindowFunction.newWindowFunction(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Window function");
            }
            // Set correlation bits info
            str = spectralWindow.getCorrelationBits().toString();
            try
            {
                spectWindowsIDL[ssbs].correlationBits = JCorrelationBit.newCorrelationBit(str);
            }
            catch (AcsJBadParameterEx ex)
            {
                throw new SBConversionException("Correlation bit");
            }

            spectWindowsIDL[ssbs].correlationNyquistOversampling = spectralWindow.getCorrelationNyquistOversampling();

            // Need to build sequence here of poln products. 
            // Configuration has XX,YY,XY,YX must convert them to 4 different 
            str = spectralWindow.getPolnProducts().toString();
            String patternStr = ",";
            String []polnProds = str.split(patternStr, -1);
            spectWindowsIDL[ssbs].polnProductsSeq = new alma.StokesParameterMod.StokesParameter[polnProds.length];

            for( int i = 0; i < polnProds.length; i++ )
            {
                try
                {
                    spectWindowsIDL[ssbs].polnProductsSeq[i] = JStokesParameter.newStokesParameter(polnProds[i]);
                }
                catch (AcsJBadParameterEx ex)
                {
                    throw new SBConversionException("poln products");
                }
            }
            spectWindowsIDL[ssbs].quantizationCorrection = spectralWindow.getQuantizationCorrection();
            // Assign channel average regions
            try
            {
                spectWindowsIDL[ssbs].channelAverageRegions = getChannelAvgRegions(spectralWindow);
            }
            catch (SBConversionException ex)
            {
                throw ex;
            }
        }
        return spectWindowsIDL;
    }

    /**
     * Get the channel average region IDL structures from the APDM base band configuration.
     * @param baseBandConf Baseband configuration, part of the APDM
     * @return The channel average regions, part of the CorrelatorConfiguration structure
     * @throws SBConversionException
     * @see CorrConfigIDL.idl
     * @see SchedBlock.xsd
     */
    //---------------------------------------------------------------------------------
    protected ChannelAverageRegion[] getChannelAvgRegions(AbstractSpectralWindowT aSpecWindow)
    throws SBConversionException
    {
        BLSpectralWindowT specWindow = (BLSpectralWindowT)aSpecWindow;
        int numChannAvgRegions = specWindow.getChannelAverageRegionCount();
        ChannelAverageRegion[] channelAvgRegionsIDL = new ChannelAverageRegion[numChannAvgRegions];
        for (int cab = 0; cab < numChannAvgRegions; cab++)
        {
            ChannelAverageRegionT car = specWindow.getChannelAverageRegion(cab);

            channelAvgRegionsIDL[cab] = new ChannelAverageRegion();
            channelAvgRegionsIDL[cab].startChannel = car.getStartChannel();
            channelAvgRegionsIDL[cab].numberChannels = car.getNumberChannels();
        }
        return channelAvgRegionsIDL;
    }

    /** Simple converter which parses units string to a units mulitiplicative factor (to ACS units)
     */
    //---------------------------------------------------------------------------------
    private double getTimeUnits(String unitsStr)
    {
        double timeUnits = 1.0;
        unitsStr = unitsStr.toLowerCase();
        if( unitsStr.equals("s") )
            timeUnits = 1.0e7;
        else if( unitsStr.equals("ms") )
            timeUnits = 1.0e4;
        else if( unitsStr.equals("ns") )
            timeUnits = 1.0e-2;
        else if( unitsStr.equals("us") )
            timeUnits = 1.0e1;
        else if( unitsStr.equals("m") )
            timeUnits = 60.0e7;
        else if( unitsStr.equals("h") )
            timeUnits = 3600.0e7;

        return timeUnits;
    }

    /** Simple converter which parses units string to a units mulitiplicative factor in MHz
     */
    //---------------------------------------------------------------------------------
    private double getFrequencyUnits(String unitsStr)
    {
        double frequencyUnits = 1.0;
        // unitsStr = unitsStr.toLowerCase();
        if( unitsStr.equals("MHz") )
            frequencyUnits = 1.0;
        else if( unitsStr.equals("GHz") )
            frequencyUnits = 1.0e3;
        else if( unitsStr.equals("Hz") )
            frequencyUnits = 1.0e-6;

        return frequencyUnits;
    }
}
