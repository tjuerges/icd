/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * $Id: CorrConfigValidator.java,v 1.42 2011/04/01 18:08:58 dguo Exp $
 */

//Our package
package alma.correlatorSrc.CorrConfigValidator;

//Java stuff
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;
import java.lang.Math;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.StringReader;
// ACS stuff
import com.cosylab.CDB.*;
import alma.ACS.stringSeqHolder;

import alma.AccumModeMod.AccumMode;
import alma.asdmIDLTypes.*;
import alma.AtmPhaseCorrectionMod.AtmPhaseCorrection;
import alma.BasebandNameMod.BasebandName;
import alma.CorrConfigModeErr.wrappers.*;
import alma.CorrelationBitMod.CorrelationBit;
import alma.CorrelationModeMod.CorrelationMode;
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;
import alma.Correlator.ConfigMode;
import alma.Correlator.eFilterMode;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2ANTS;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;
import alma.Correlator.CorrelatorConfiguration;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.hla.datamodel.enumeration.*;
import alma.ReceiverSidebandMod.ReceiverSideband;
import alma.NetSidebandMod.NetSideband;
import alma.SidebandProcessingModeMod.SidebandProcessingMode;
import alma.StokesParameterMod.StokesParameter;
import alma.SwitchingModeMod.SwitchingMode;
import alma.WindowFunctionMod.WindowFunction;

/** This class validates correlator configurations. It validates them as IDL structures
 ** in ICD/CORR/idl/CorrConfig.idl. Configurations can come in the form of XML document SBs and
 ** are passed in as strings which the class APDMUtil can parse and return CorrelatorConfiguration
 ** IDL structures.
 ** An overview document of how the validator works can be found in "Correlator Configuration
 ** Validation and Translation to IDL": 
 ** http://almasw.hq.eso.org/almasw/pub/CORR/CorrConfigXMLVsIDL/CorrConfigXML_IDL.pdf.
 ** Documentation regarding correct values for CorrelatorConfiguration parameters can be found in
 ** the IDL file. As HLA/Enumerations are widely used, they also provide parameter limits.
 ** 
 ** Validation is done against a specific correlator which are defined in the CorrConfigModeTable class.
 */
public class CorrConfigValidator 
{
    /////////////////////////////////////
    // Implementation of ConfigValidator
    /////////////////////////////////////

    private Logger m_logger;
    private CorrConfigModeClass m_modesTable;
    Vector m_errorVector;
    private int m_outputDataSize;
    private long m_integrationDuration;
    private long m_dumpDuration;
    private ReceiverSideband m_receiverType;
    private int m_totalChanAverRegionsPerBaseband;

    // type of correlator in use
    private String m_correlatorType;
    // maximum values for a given correlator/software implementation
    private int m_maxNumSpectralWindowsPerBaseband;
    private int m_maxNumAntennas;
    private int m_maxNumBasebands;
    private int m_maxNumChanAverRegions;
    private int m_maxNumAPCDatasets;
    private String m_dumpDurationSelection = "dumpTypeOne";
    private Vector m_sideBandProcessingModesAllowed;

    //---------------------------------------------------------------------------------
    public CorrConfigValidator()
    throws AcsJConstructorFailureEx
    {
        m_correlatorType = new String("CORRELATOR_12m_1QUADRANT");
        m_logger = Logger.getLogger("alma.correlator.CorrConfigValidator");

        //
        // instantiate the modes table based on a full correlator
        //
        try
        {
            m_modesTable = new CorrConfigModeClass(m_correlatorType);
            setCorrelatorLimits();
        }
        catch ( AcsJConstructorFailureEx ex )
        {
            m_logger.log(Level.SEVERE,"failed to instantiate modes table for correlator type: "+m_correlatorType, ex);
            throw ex;
        }
    }

    //---------------------------------------------------------------------------------
    public CorrConfigValidator(String correlatorType)
    throws AcsJConstructorFailureEx
    {
        // If no correlator type passed in, default to 1-quadrant
        if( correlatorType.length() == 0 )
            m_correlatorType = new String("CORRELATOR_12m_1QUADRANT");
        else
            m_correlatorType = correlatorType;

        m_logger = Logger.getLogger("alma.correlator.CorrConfigValidator");

        //
        // instantiate the modes table based on a full correlator
        //
        try
        {
            m_modesTable = new CorrConfigModeClass(m_correlatorType);
            // set various limits based upon the type of correlator
            setCorrelatorLimits();
        }
        catch ( AcsJConstructorFailureEx ex )
        {
            m_logger.log(Level.SEVERE,"failed to instantiate modes table for correlator type: "+m_correlatorType, ex);
            throw ex;
        }
    }

    //---------------------------------------------------------------------------------
    public CorrConfigValidator(com.cosylab.CDB.DAL dal)
        throws AcsJConstructorFailureEx,AcsJReadTypeEx
    {
        m_logger = Logger.getLogger("alma.correlator.CorrConfigValidator");

        //
        // instantiate the modes table based on what's written in the cdb
        //
        try
        {
            m_modesTable = new CorrConfigModeClass(dal);
            // Get the correlator type
            m_correlatorType = m_modesTable.getType();
            // set various limits based upon the type of correlator
            setCorrelatorLimits();
        }
        catch ( AcsJConstructorFailureEx ex )
        {
            m_logger.log(Level.SEVERE, "failed to instantiate modes table.",ex);
            throw ex;
        }
        try {
            DAO dao = dal.get_DAO_Servant("alma/CORR/CONFIGURATION_VALIDATOR");
            // access field
            m_dumpDurationSelection = dao.get_string("dumpDurationSelection");
            System.out.println(" >> Dump duration is " + m_dumpDurationSelection );
        }
        catch ( Throwable thr )
            {
                throw new AcsJReadTypeEx("access to cdb atribute thrown unknown exception");
            }
    }

    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXML )
    {
        BL_XMLParser blXMLParser = new BL_XMLParser();
        m_errorVector = new Vector();
        try
        {
            CorrelatorConfiguration corrConfigIDL = blXMLParser.translateSpectralSpecXml2CorrConfigIdl(spectralSpecXML);
            return corrConfigIDL;
        }
        catch (SBConversionException sbcex)
        {
            m_errorVector.addElement(sbcex.getMessage());
            return null;
        }
        catch (Exception ex)
        {
            m_errorVector.addElement("Invalid spectral Spec: "+ex.toString());
            return null;
        }
    }

    /** Validate the correlator configuration in an SpectralSpec XML string. The validation
     ** is two-step: 1) Translate from XML to IDL, 2) validate the IDL
     ** @param spectralSpec SS XML string that contains a correlator configuration to validate
     ** @param numOfAntennas The number of antennas is important to determine the data rate.
     ** @param errorStringSeq A sequence of strings which hold errors encountered in the
     **	validation. Note that there can be multiple problems with the IDL and they are all
     **	added to this sequence of strings.
     ** @return True if the configuration is valid, else false. If false, the reasons are
     **	in errorStringSeq.
     */
    //---------------------------------------------------------------------------------
    public boolean validateConfigurationSS(String spectralSpec, int numOfAntennas,
            alma.ACS.stringSeqHolder errorStringSeq)
    {
        m_errorVector = new Vector();
        // Get the correlator configurations in the SB and convert them to 
        // IDL structures

        CorrelatorConfiguration corrConfig = translateSpectralSpecXml2CorrConfigIdl(spectralSpec);
        if( corrConfig == null )
        {
            copyErrorStrings(errorStringSeq);
            return false;
        }
        return validateConfiguration(corrConfig, numOfAntennas, errorStringSeq);
    }

    /** Validate the correlator configuration in a CorrelatorConfiguration IDL
     ** The code is written such that checks are made on parameters in the same order as
     ** they appear in CorrConfig.idl.
     ** @param corrConfig Correlator configuration IDL structure to validate
     ** @param numOfAntennas The number of antennas is important to determine the data rate.
     ** @param errorStringSeq A sequence of strings which hold errors encountered in the
     **	validation. Note that there can be multiple problems with the IDL and they are all
     **	added to this sequence of strings.
     ** @return True if the configuration is valid, else false. If false, the reasons are
     **	in errorStringSeq.
     */
    //--------------------------------------------------------------------------------
    public boolean validateConfiguration(CorrelatorConfiguration corrConfig,
            int numOfAntennas, alma.ACS.stringSeqHolder errorStringSeq)
    {
        // m_logger.info("CorrConfigValidator: Validate the Configuration IDL struct");
        m_errorVector = new Vector();
        boolean isValidConfig = true;

        // Preserve durations for various checks including data rates
        m_integrationDuration = corrConfig.integrationDuration; 
        m_receiverType = corrConfig.receiverType;
        m_dumpDuration = corrConfig.dumpDuration;
        m_outputDataSize = 0;

        // Check that durations are valid
        isValidConfig =  validateDurations( corrConfig);


        //
        // Check that APC has correct combinations
        //
        if( (corrConfig.APCDataSets.length < 1) || (corrConfig.APCDataSets.length > m_maxNumAPCDatasets) )
        {
            m_errorVector.addElement("CorrConfig " + ": Invalid number of APCDataSets: " + 
                    corrConfig.APCDataSets.length + " valid range: 1 - "+m_maxNumAPCDatasets);
            isValidConfig = false;
        }
        else
        {
            //
            // Check for duplicates & invalid combinations
            //
            if( corrConfig.APCDataSets.length == 2 )
            {
                AtmPhaseCorrection atm0 = corrConfig.APCDataSets[0];
                AtmPhaseCorrection atm1 = corrConfig.APCDataSets[1];
                if ( atm0 == atm1 )
                {
                    m_errorVector.addElement("CorrConfig " + ": Duplicate APC Data type: " + atm0.toString());
                    isValidConfig = false;
                }
            }
        }

        // make sure that the number of basebands is valid. If not, don't bother checking any more
        if( (corrConfig.baseBands.length < 1) || 
                (corrConfig.baseBands.length > m_maxNumBasebands) )
        {
            m_errorVector.addElement("CorrConfig Baseband count " + 
                    corrConfig.baseBands.length + " is invalid. Valid range: 1 - "+m_maxNumBasebands);
            copyErrorStrings(errorStringSeq);
            return false;
        }

        //
        // repeated base-bands is invalid
        //
        for ( int i = 0; i < corrConfig.baseBands.length; i++ ) 
        {
            for ( int j = i + 1; j < corrConfig.baseBands.length; j++ )
            {
                if ( corrConfig.baseBands[i].basebandName == corrConfig.baseBands[j].basebandName )
                {
                    m_errorVector.addElement("CorrConfig baseband " + corrConfig.baseBands[i].basebandName.toString() + " appers more than once");
                    
                    copyErrorStrings(errorStringSeq);
                    
                    return false;
                }
            }
        }

        // data products should be same at each base band
        for ( int i = 0; i < corrConfig.baseBands.length; i++ ) {
            for ( int j = i + 1; j < corrConfig.baseBands.length; j++ )
                {
                    if ( corrConfig.baseBands[i].dataProducts != corrConfig.baseBands[j].dataProducts )
                        {
                            m_errorVector.addElement("CorrConfig baseband " + corrConfig.baseBands[i].basebandName.toString() + " and " + corrConfig.baseBands[j].basebandName.toString() + " have different data products " );
                            
                            copyErrorStrings(errorStringSeq);
                            
                            return false;
                        }
                }
        }
        
        //
        // check each baseband
        // 
        for(int bbj = 0; bbj < corrConfig.baseBands.length; bbj++ ) 
        {
            if ( !validateBaseband(corrConfig.baseBands[bbj], numOfAntennas) )
            {
                isValidConfig = false;
            }
        }
        // Number of antennas valid
        if( (numOfAntennas < 1) || (numOfAntennas > m_maxNumAntennas) )
        {
            m_errorVector.addElement("CorrConfig Number of antennas invalid: " + numOfAntennas +
                    " Valid range: 1 - "+m_maxNumAntennas);
            copyErrorStrings(errorStringSeq);
            return false;
        }	   
        //
        // Check data rate across all basebands
        //
        if ( !checkDataRate(numOfAntennas) )
        {
            isValidConfig = false;
        }

        // Copy an errors encountered into the return error string sequence
        copyErrorStrings(errorStringSeq);

        return isValidConfig;
    }

    /** Validate subscan durations
     */
    //---------------------------------------------------------------------------------
    private boolean validateDurations( CorrelatorConfiguration corrConfig )
    {
        boolean bValidDurations = true;

        long channelAverageDuration = corrConfig.channelAverageDuration; 
        //
        // check timing parameters
        //
        if ( m_dumpDuration < 10000 )
        {
            m_errorVector.addElement("Dump duration is too short: "+m_dumpDuration);
            bValidDurations = false;
        }
        if( m_dumpDurationSelection.equals("dumpTypeOne") ) {
            if ( m_dumpDuration != 10000 &&
                 m_dumpDuration != 160000 &&
                 (m_dumpDuration % 480000) != 0 )
        {
                    m_errorVector.addElement("Dump duration must be 1ms or 16ms or a multiple of 48 ms.");
                    bValidDurations = false;
                }
        } else if (m_dumpDurationSelection.equals("dumpTypeTwo") ) {
            if ( m_dumpDuration != 10000 &&
                 (m_dumpDuration % 160000) != 0 )
                {
                    m_errorVector.addElement("This type Dump duration must be 1ms or a multiple of 16 ms.");
            bValidDurations = false;
        }
        } else {
            m_errorVector.addElement("Configuration Validator CDB does not provide correct dump duration type.");
            bValidDurations = false;
        }
        
        // 
        if( m_integrationDuration < 10000 )
        {
            m_errorVector.addElement("Integration Duration is too short: "+m_integrationDuration);
            bValidDurations = false;
        }
        else if( (m_dumpDuration != 0) && (m_integrationDuration % m_dumpDuration) != 0 )
        {
            m_errorVector.addElement("Integration Duration "+ m_integrationDuration + " is not a multiple of the dump duration: " +
                    m_dumpDuration);
            bValidDurations = false;
        }

        //
        // check channel average durations
        //
        if( channelAverageDuration > 0 )
        {
            // chan aver duration must be less than integrations
            if( channelAverageDuration > m_integrationDuration )
            {
                m_errorVector.addElement("Channel average duration "+ channelAverageDuration + 
                        " is greater than the integration duration "+
                        m_integrationDuration);
                bValidDurations = false;
            }
            // chan aver duration must be integral multiple of dump duration
            if( (m_dumpDuration != 0) && (channelAverageDuration % m_dumpDuration) != 0 )
            {
                m_errorVector.addElement("Channel average duration "+ channelAverageDuration + " is not an even multiple of the dump duration: " +
                        m_dumpDuration);
                bValidDurations = false;
            }
            // integration duration must be a multiple of sub-integrations
            if( (m_integrationDuration % channelAverageDuration) != 0 )
            {
                m_errorVector.addElement("Integration duration "+ m_integrationDuration +
                        " is not an even multiple of the sub-integration durations: " +
                        channelAverageDuration);
                bValidDurations = false;
            }
        }
        return bValidDurations;

    }	
    /** Validate a baseband.
     */
    //---------------------------------------------------------------------------------
    private boolean validateBaseband( alma.Correlator.BaseBandConfig baseBand, int numOfAntennas)
    {
        boolean bValidBaseBand = true;
        m_totalChanAverRegionsPerBaseband = 0;

        // Which baseband numbers are allowed depend on correator type
        //
        if( m_correlatorType.equals(CORRELATOR_12m_2ANTS.value) &&
                ((baseBand.basebandName != BasebandName.BB_1) && (baseBand.basebandName != BasebandName.BB_2)) )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + " for correlator " +
                    CORRELATOR_12m_2ANTS.value + " is invalid, only BB_1 & BB_2 valid");
            bValidBaseBand = false;
        }

        else if( (baseBand.basebandName != BasebandName.BB_1) &&
                (baseBand.basebandName != BasebandName.BB_2) &&
                (baseBand.basebandName != BasebandName.BB_3) &&
                (baseBand.basebandName != BasebandName.BB_4) )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + 
            " is invalid, only BB_1...BB_4 valid");
            bValidBaseBand = false;
        }

        if( !validateCamAndDataProduct(baseBand, numOfAntennas) )
            bValidBaseBand = false;
        //
        // validate spectral windows
        //
        if( !validateSpectralWindows(baseBand) )
        {
            bValidBaseBand = false;
        }

        // Check that this baseband has at least one channel average region
        if( m_totalChanAverRegionsPerBaseband == 0 )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + " has no channel average regions");
            bValidBaseBand = false;
        }

        /*
         * BinSwitching_t  binSwitchingMode;
         *  Rules: SwitchingType = NO_SWITCHING, # of positions must be 1, dwell lenghth = 1 dead times ignored
         *  SwitchingType == Nutator or Frequency switching, # of positions must be 2,3, or 4
         *    Nutator: Length of dwell & dead times must equal # of positions w/ 48ms boundary conditions
         *    Frequency: Length of dwell times must equal # of positions w/ 48ms boundary conditions dead 
         *    times must be 0 either by setting the sequence length to 0 or using values = 0
         */
        if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING )
        {
            // the number of positions must be 1
            if( baseBand.binSwitchingMode.numberOfPositions != 1 )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                        ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                        ": # Bin switching positions " + 
                        baseBand.binSwitchingMode.numberOfPositions);
                bValidBaseBand = false;
            }
            // the array lengths of dwell & dead must be 1 and equal
            else if( (baseBand.binSwitchingMode.dwellTime.length != 1) || 
                    (baseBand.binSwitchingMode.deadTime.length > 1) )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                        ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                        " [# Bin switching positions " + baseBand.binSwitchingMode.numberOfPositions +
                        " Array lengths: Dwell time: " + baseBand.binSwitchingMode.dwellTime.length + 
                        " Dead time: " + baseBand.binSwitchingMode.deadTime.length + 
                "] must all be equal");
                bValidBaseBand = false;
            }
        }
        // handle Frequency or nutator switching only. Ignore load switching
        else if( baseBand.binSwitchingMode.SwitchingType != alma.SwitchingModeMod.SwitchingMode.LOAD_SWITCHING )
        {
            // There are some common things to both nutator & frequency switching
            if( (baseBand.binSwitchingMode.numberOfPositions < 2) || 
                    (baseBand.binSwitchingMode.numberOfPositions > 4) )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + 
                        ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                        ": # Bin switching positions " + 
                        baseBand.binSwitchingMode.numberOfPositions + " unsupported");
                bValidBaseBand = false;
            }
            // # of dwell time entries = # of positions
            if ( baseBand.binSwitchingMode.dwellTime.length != baseBand.binSwitchingMode.numberOfPositions )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                        ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                        ": # Bin switching positions " + 
                        baseBand.binSwitchingMode.numberOfPositions + " not equal to # of dwell time postions " +
                        baseBand.binSwitchingMode.dwellTime.length);
                bValidBaseBand = false;
            }
            else if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.NUTATOR_SWITCHING )
            {
                if ( baseBand.binSwitchingMode.deadTime.length != baseBand.binSwitchingMode.dwellTime.length )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                            ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                            " must have equal numbers of dwell time entries: " +
                            baseBand.binSwitchingMode.dwellTime.length +
                            " dead time entries: " +
                            baseBand.binSwitchingMode.deadTime.length);
                    bValidBaseBand = false;
                }
                // Check that dwell time + dead time must be a multiple of 48ms or 0 if it's no_switching
                if( (baseBand.binSwitchingMode.deadTime.length != 0) &&
                        (baseBand.binSwitchingMode.dwellTime.length != 0) )
                {
                    for( int i = 0; i < baseBand.binSwitchingMode.deadTime.length; i++)
                    {
                        // First check that NUTATOR switching have both dwell &  dead times
                        if( (baseBand.binSwitchingMode.deadTime[i] == 0) || (baseBand.binSwitchingMode.dwellTime[i] == 0) )
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                    ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                    ": must have non-zero dwell ("+ Long.toString(baseBand.binSwitchingMode.dwellTime[i]) + ")"+
                                    " and dead (" + Long.toString(baseBand.binSwitchingMode.deadTime[i]) + ") times");
                            bValidBaseBand = false;
                        }

                        // make sure dead+dwell times are non zero
                        long deadDwellTime =  baseBand.binSwitchingMode.deadTime[i] + baseBand.binSwitchingMode.dwellTime[i];
                        if( deadDwellTime == 0) 
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                    ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                    ": Bin Switching dwell + dead time set [" + Integer.toString(i) +
                                    "] is " + Long.toString(deadDwellTime) +
                            " is 0");
                            bValidBaseBand = false;
                        }
                        // and are a multiple of 48ms
                        else if( (deadDwellTime % 480000) != 0 )
                        {
                            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                    ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                    ": Bin Switching dwell + dead time set [" + Integer.toString(i) +
                                    "] is " + Long.toString(deadDwellTime) +
                            " must be a multiple of 480000 100ns.");
                            bValidBaseBand = false;
                        }
                    }
                }
            }
            else if( baseBand.binSwitchingMode.SwitchingType == alma.SwitchingModeMod.SwitchingMode.FREQUENCY_SWITCHING )
            {
                // Frequency switching can't have dead time
                for( int i = 0; i < baseBand.binSwitchingMode.deadTime.length; i++)
                {
                    if( baseBand.binSwitchingMode.deadTime[i] != 0 )
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                ": can't have dead times: [" + Integer.toString(i) +
                                "]: " + Long.toString(baseBand.binSwitchingMode.deadTime[i]));
                        bValidBaseBand = false;
                    }
                }
                for( int i = 0; i < baseBand.binSwitchingMode.dwellTime.length; i++)
                {
                    // Dwell times must be a multiple of 48ms
                    if( (baseBand.binSwitchingMode.dwellTime[i] % 480000) != 0 )
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                ": Bin Switching mode: " + baseBand.binSwitchingMode.SwitchingType.toString() +
                                ": Dwell time[" + Integer.toString(i) +
                                "] is " + Long.toString(baseBand.binSwitchingMode.dwellTime[i] ) +
                        " must be a multiple of 48 ms.");
                        bValidBaseBand = false;
                    }
                }
            }
        }
        return bValidBaseBand;
    }

    //---------------------------------------------------------------------------------
    private boolean validateSpectralWindows(alma.Correlator.BaseBandConfig baseBand )
    {
        boolean bValidSpectralWindow = true;
        String basebandName = JBasebandName.name(baseBand.basebandName);
        int totalNumberOfBasebandChannels = 0;
        double totalEffectiveBW = 0.0;
        CorrelationBit correlationBits;
        StokesParameter[] polarizationProducts;

        // Make sure that the number of spectral windows is valid
        if( (baseBand.spectralWindows.length < 1) || (baseBand.spectralWindows.length > m_maxNumSpectralWindowsPerBaseband) )
        {
            m_errorVector.addElement("BaseBand " + basebandName + ": Spectral window count " + 
                    baseBand.spectralWindows.length + " is invalid. Valid range: 1 - "+m_maxNumSpectralWindowsPerBaseband);
            return false;
        }
        // To check the polarization & correlation bits across all spectral windows, get the first window's values &
        correlationBits = baseBand.spectralWindows[0].correlationBits;
        polarizationProducts = baseBand.spectralWindows[0].polnProductsSeq;

        // Loop through each SpectralWindow under this BaseBand
        for(int si = 0; si < (int)baseBand.spectralWindows.length; si++ )
        {
            int si1 = si + 1;
            if( (baseBand.spectralWindows[si].centerFrequencyMHz < 2000.0) ||
                    (baseBand.spectralWindows[si].centerFrequencyMHz > 4000.0) )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                        si1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                " is out of [2000, 4000] MHz range");
                bValidSpectralWindow = false;
            }

            // Check that  spectral window w/ center frequency doesn't go below baseband (2GHz)
            if( (baseBand.spectralWindows[si].centerFrequencyMHz - baseBand.spectralWindows[si].effectiveBandwidthMHz/2.0) <
                    2000.0)
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow : " + 
                        si1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                        " bandwidthMHz " + baseBand.spectralWindows[si].effectiveBandwidthMHz +
                " extends below baseband beginning");
                bValidSpectralWindow = false;
            }

            // Check that spectral window w/ center frequency doesn't go above baseband (4GHz)
            if( (baseBand.spectralWindows[si].centerFrequencyMHz + baseBand.spectralWindows[si].effectiveBandwidthMHz/2.0) >
            4000.0 )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow : " + 
                        si1 + " centerFrequencyMHz " + baseBand.spectralWindows[si].centerFrequencyMHz + 
                        " bandwidthMHz " + baseBand.spectralWindows[si].effectiveBandwidthMHz +
                " extends above baseband beginning");
                bValidSpectralWindow = false;
            }

            //  Check that effective bandwidth make sense (taking into account TFB overlap
            if( (baseBand.spectralWindows[si].effectiveBandwidthMHz < 50.0) ||
                    (baseBand.spectralWindows[si].effectiveBandwidthMHz > 2000.0) )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                        si1 + " effective bandwidth " + baseBand.spectralWindows[si].effectiveBandwidthMHz + 
                " is out of [62.5, 2000] MHz range");
                bValidSpectralWindow = false;
            }

            //  Check that effective # of channels make sense (taking into account TFB overlap
            if( (baseBand.spectralWindows[si].effectiveNumberOfChannels < 32) ||
                    (baseBand.spectralWindows[si].effectiveNumberOfChannels > 8192) )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                        si1 + " effective number of channels " + baseBand.spectralWindows[si].effectiveNumberOfChannels + 
                " is out of [32,8192] range");
                bValidSpectralWindow = false;
            }

            // spectralAveragingFactor must be a power of 2, 1, 2, 4, 8, etc to maximum # of channels
            switch( baseBand.spectralWindows[si].spectralAveragingFactor )
            {
                case 1:
                case 2:
                case 4:
                case 8:
                case 16:
                    break;
                default:
                {
                    m_errorVector.addElement("Baseband " + basebandName + ": SpectralWindow " + si1 + 
                            ": Input spectralAveragingFactor " + baseBand.spectralWindows[si].spectralAveragingFactor +
                            ", Not a power of 2 or >= " + 16);
                    bValidSpectralWindow = false;
                }
            }
            // Update output binary data size - Should we consider ASDM XML header overhead? Also, 2-byte vs.
            // 4-byte visibilities are not taken into account. Not sure how to handle that.
            m_outputDataSize += (baseBand.spectralWindows[si].effectiveNumberOfChannels / baseBand.spectralWindows[si].spectralAveragingFactor);
            totalNumberOfBasebandChannels += (baseBand.spectralWindows[si].effectiveNumberOfChannels / baseBand.spectralWindows[si].spectralAveragingFactor);
            totalEffectiveBW += baseBand.spectralWindows[si].effectiveBandwidthMHz;         
            //
            // Validate associatedSpectralWindowNumberInPair for DSB receivers
            //
            if ( m_receiverType == ReceiverSideband.DSB )
            {
                boolean bFound = false;
                // Scan thru spectral windows to see if one matches associatedSpectralWindowNumberInPair
                for( int i = 0; i < baseBand.spectralWindows.length; i++ )
                {
                    if( (i+1) == baseBand.spectralWindows[si].associatedSpectralWindowNumberInPair )
                    {
                        bFound = true;
                        break;
                    }
                }

                if( !bFound )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                            ": Spectral Window: " + si1+
                            ": For DSB receiver, didn't find pair spectral window # which matches " + 
                            baseBand.spectralWindows[si].associatedSpectralWindowNumberInPair);
                    bValidSpectralWindow = false;
                }
            }
            else
            {
                // For non-DSB receiver, must always use this spec window.
                if( !baseBand.spectralWindows[si].useThisSpectralWindow )
                {
                    m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                            ": Spectral Window: " + si1+
                    " For non-DSB receiver, cannot suppress a spectral window: useThisSpectralWindow = false");
                    bValidSpectralWindow = false;
                }
            }

            // if channel average region(s) invalid, flag spectral window invalid
            if( !validateChannelAverageRegions(basebandName, baseBand.spectralWindows[si],
                    si, m_errorVector) )
            {
                bValidSpectralWindow = false;
            }

            // ACA-specific
            //        boolean frqChProfReproduction;
            // ALMA-B specific, but since this is an ALMA enumeration, can't be wrong
            //CorrelationBitMod::CorrelationBit correlationBits;
            // ALMA-B specific, but since this is an ALMA enumeration, can't be wrong
            //boolean correlationNyquistOversampling;

            // Size check for Correlator::StokesParameterSeq polnProductsSeq; 
            if( baseBand.spectralWindows[si].polnProductsSeq.length != 1 &&
                    baseBand.spectralWindows[si].polnProductsSeq.length != 2 &&
                    baseBand.spectralWindows[si].polnProductsSeq.length != 4 )
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                        ": Spectral Window: " + si1+
                        " invalid number of polarization products defined: "+
                        baseBand.spectralWindows[si].polnProductsSeq.length);
                bValidSpectralWindow = false;
            }

            //
            // Check for duplicatations Correlator::StokesParameterSeq
            // polnProductsSeq; 
            //
            for ( int pIdx1 = 0; pIdx1 < baseBand.spectralWindows[si].polnProductsSeq.length;
            pIdx1++ )
            {
                for ( int pIdx2 = pIdx1+1; pIdx2 < baseBand.spectralWindows[si].polnProductsSeq.length;
                pIdx2++ )
                {
                    if( baseBand.spectralWindows[si].polnProductsSeq[pIdx1] ==
                        baseBand.spectralWindows[si].polnProductsSeq[pIdx2] )
                    {
                        m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                ": Spectral Window: " + si1 +
                                " has duplicated Stokes parameter at polnProducts indexes " +
                                pIdx1 + " and " + pIdx2);
                        bValidSpectralWindow = false;
                    }
                }
            }

            // Check that we have received a valid sideBand
            if( baseBand.spectralWindows[si].sideBand == NetSideband.NOSB)
            {
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                                         ": Spectral Window: " + si1 +
                                         " does not specify a sideband ");
                bValidSpectralWindow = false;
            }
            // check that polarization products, bandwidth, resolution make sense.
            int modeNumber;
            ConfigMode m = null;
            try
            {
                modeNumber = m_modesTable.findModeFromEffective(
                        (float)baseBand.spectralWindows[si].effectiveBandwidthMHz,
                        (long)baseBand.spectralWindows[si].effectiveNumberOfChannels,
                        baseBand.spectralWindows[si].correlationBits,
                        baseBand.spectralWindows[si].correlationNyquistOversampling,
                        baseBand.spectralWindows[si].polnProductsSeq);

        
                //
                // get the mode from the table
                //
                try
                {
                    m = m_modesTable.getMode(modeNumber);
                }
                catch(AcsJInvalidKeyEx ex)
                {
                    throw new AcsJInvalidModeEx("No mode exists with these already validated parameters."); 
                }
            }
            catch( AcsJInvalidModeEx ex)
            {
                String polnProdsStr = new String();
                for( int i = 0; i < baseBand.spectralWindows[si].polnProductsSeq.length; i++ )
                {
                    polnProdsStr += JStokesParameter.toString(baseBand.spectralWindows[si].polnProductsSeq[i]) + " ";
                }
                m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                        ": Spectral Window: " + si1+
                        ": has invalid combination of:\nEffective BW: "+ baseBand.spectralWindows[si].effectiveBandwidthMHz +
                        " # channels: " + baseBand.spectralWindows[si].effectiveNumberOfChannels +
                        " Correlation Bits: " + baseBand.spectralWindows[si].correlationBits +
                        " Oversampling: " + baseBand.spectralWindows[si].correlationNyquistOversampling +
                        " Poln Products: " + polnProdsStr + "\nException: "+ex.toString());
                bValidSpectralWindow = false;
            }

            if (m != null && m.filterMode == eFilterMode.FILTER_FDM)
            {
                // Check that center frequency is an integer multiple of the TFB DDS step frequency only for FDM modes

                if ( java.lang.Math.IEEEremainder(baseBand.spectralWindows[si].centerFrequencyMHz,
                                                  0.030517578125)
                     != 0.0 )
                {
                    m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow : " + 
                                             si1 + " centerFrequencyMHz " + 
                                             baseBand.spectralWindows[si].centerFrequencyMHz +
                                             " is not an integer multiple of the TFB DDS step frequency (30.517578125 KHz) has remainder of " +
                                             java.lang.Math.IEEEremainder(baseBand.spectralWindows[si].centerFrequencyMHz,
                                                                          0.030517578125) + " MHz");
                    bValidSpectralWindow = false;
                }
            }

            if( correlationBits != baseBand.spectralWindows[si].correlationBits)
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                        si1 + " correlationBits " + baseBand.spectralWindows[si].correlationBits + 
                        " is inconsistent with other spectral windows: "+correlationBits);
                bValidSpectralWindow = false;
            }

            if( polarizationProducts.length != baseBand.spectralWindows[si].polnProductsSeq.length )
            {
                m_errorVector.addElement("BaseBand " + basebandName + ": SpectralWindow " + ": " + 
                        si1 + " number of polarization products " + polarizationProducts.length + 
                        " is correlationBits: "+ polarizationProducts.length);
                bValidSpectralWindow = false;
            }
        }
        // Maximum number of spectral channels across all spectral windows must be less than 8K
        // A better check would be to take into account the effective # of channels.
        // Note: Not sure the purpose of this rule, Comment out 

        /*
        if( totalNumberOfBasebandChannels > 7680 )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                    ": has too many spectral channels defined: "+ totalNumberOfBasebandChannels);
            bValidSpectralWindow = false;
        }
        */

        // Check that effective BW doesn't exceed max for baseband. Use 2 GHz for TDM mode
        // Note: Not sure the purpose of this rule, comment out 
        /*
        if( (totalEffectiveBW < 58.5) || (totalEffectiveBW > 2000.0) )
        {
            m_errorVector.addElement("Total # of effective bandwidth across all basebands: "+
                    totalEffectiveBW + " MHz invalid. Valid range 58.6 MHz - 1.876 GHz");
            bValidSpectralWindow = false;
        }
        */

        // ALMA-B specific: No check needed, just a boolean
        //boolean quantizationCorrection;

        return bValidSpectralWindow;
    }

    // Loop through each ChannelAverageRegion to validate startChannel,
    //---------------------------------------------------------------------------------
    private boolean validateChannelAverageRegions( String basebandName, //int basebandName,
            alma.Correlator.SpectralWindow specWindow, int specWindowIdx,
            Vector m_errorVector)
    {
        boolean bValidChanAverRegion = true;

        // We must have 1 channel average region in the entire baseband, but not all
        // spectral windows need one, so just check here for the maximum
        if( specWindow.channelAverageRegions.length > 10 )
        {
            m_errorVector.addElement("BaseBand " + basebandName + ": Input ChannelAverageRegions " + 
                    specWindow.channelAverageRegions.length + ", is out of [0,10] Regions !");
            bValidChanAverRegion = false;
        }
        else
        {
            m_totalChanAverRegionsPerBaseband += specWindow.channelAverageRegions.length;
            for(int carIdx = 0; carIdx < specWindow.channelAverageRegions.length; carIdx++ )
            {
                // check that channel aver region start channel is w/in spectral window
                if( (specWindow.channelAverageRegions[carIdx].startChannel < 0) || 
                        (specWindow.channelAverageRegions[carIdx].startChannel >= specWindow.effectiveNumberOfChannels-1) )
                {
                    m_errorVector.addElement("BaseBand " + basebandName + 
                            ": ChannelAverageRegion " + carIdx+1 +
                            " starting channel invalid: " +
                            specWindow.channelAverageRegions[carIdx].startChannel);
                    bValidChanAverRegion = false;
                }
                // check that channel average region does not extend beyond spectral window
                int stopChannel = specWindow.channelAverageRegions[carIdx].startChannel + 
                specWindow.channelAverageRegions[carIdx].numberChannels;

                if( stopChannel > specWindow.effectiveNumberOfChannels)
                {
                    m_errorVector.addElement("BaseBand " + basebandName + 
                            ": ChannelAverageRegion " + carIdx+1 +
                            ": last channel " + stopChannel + 
                            " extends beyond last channel of spectral window " +
                            specWindow.effectiveNumberOfChannels);
                    bValidChanAverRegion = false;
                }
                // check that number of channels > 0
                if( specWindow.channelAverageRegions[carIdx].numberChannels < 1 )
                {
                    m_errorVector.addElement("BaseBand " + basebandName + 
                            ": ChannelAverageRegion " + carIdx+1 +
                            ": number of channels invalid: " + 
                            specWindow.channelAverageRegions[carIdx].numberChannels);
                    bValidChanAverRegion = false;
                }
            }
        }
        return bValidChanAverRegion;
    }

    //---------------------------------------------------------------------------------
    private boolean validateCamAndDataProduct(alma.Correlator.BaseBandConfig baseBand, 
            int numOfAntennas)
    {
        boolean bValidCamAndDataProduct = true;
        //
        // validate accumulation mode and requested product
        //
        if ( baseBand.dataProducts == CorrelationMode.CROSS_ONLY )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName +
            " Data product choice: CROSS_ONLY not allowed");
            bValidCamAndDataProduct = false;
        }
        else if ( baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO &&
                baseBand.CAM == AccumMode.FAST )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() + ": data products " + 
                    baseBand.dataProducts.toString() + " incompatible with accumulation mode " + baseBand.CAM.toString());
            bValidCamAndDataProduct = false;
        }

        //
        // CAM = FAST, dump duration must be 1ms
        //
        if( (baseBand.CAM == AccumMode.FAST) && (m_dumpDuration != 10000) )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                    ": Input dump duration " + m_dumpDuration + ", For the CAM = 'FAST', must be 1 ms");
            bValidCamAndDataProduct = false;
        }

        //
        //  CAM = NORMAL - allow for a single antenna to use CAM = NORMAL
        //
        if ( baseBand.CAM == alma.AccumModeMod.AccumMode.NORMAL &&
                (m_dumpDuration % 160000) != 0 )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                    ": Input dump duration " + m_dumpDuration +
            ", For the CAM = 'NORMAL', must be a multiple of 16 ms");
            bValidCamAndDataProduct = false;
        }

        //
        // for auto & cross results then the number of antennas must be greater
        // than 1.
        //
        if( baseBand.dataProducts == CorrelationMode.CROSS_AND_AUTO &&
                numOfAntennas == 1 )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
            ", For dataProducts = CROSS_AND_AUTO, number of antennas in array must be > 1");
            bValidCamAndDataProduct = false;
        }

        // SidebandProcessingModeMod::SidebandProcessingMode sideBandSeparationMode;
        // Currently no sideband separation allowed (should use CorrConfigMode class for this?)
        if( !isSidebandProcessingModeAllowed(baseBand.sideBandSeparationMode) )
        {
            m_errorVector.addElement("BaseBand " + baseBand.basebandName.toString() +
                    ": Sideband separation mode " +  
                    JSidebandProcessingMode.toString(baseBand.sideBandSeparationMode) +
            " is not allowed.");
            bValidCamAndDataProduct = false;
        }
        return bValidCamAndDataProduct;
    }

    /** Check that the data rate does not exceed 60MB/sec. This is only looking at the 
     ** binary data and assumes that there are 4 bytes per result (some visibilities can
     ** be 2 bytes) and it ignores the ASDM binary XML headers which is usually less than
     ** 10% overhead
     */
    //---------------------------------------------------------------------------------
    private boolean checkDataRate(int numOfAntennas)
    {
        double selfBaselines = (double)numOfAntennas;
        double crossBaselines = (selfBaselines * (selfBaselines-1))/2.0;
        double numBytesPerResult = 4.0;
        double intDurationSecs = (double)m_integrationDuration / 1.0e7;
        double dataRate = ((double)m_outputDataSize / intDurationSecs) * selfBaselines * numBytesPerResult;
        dataRate += ((double)m_outputDataSize / intDurationSecs) * crossBaselines * numBytesPerResult * 2.0;

        if( dataRate  > 60.0e6 )
        { 
            String str = String.format("Data Rate exceeds 60 MB/s: %8.3f MB/s # of Antennas: %d Integration Duration: %6.3f Total Spectral points across all basebands: %d",
                    dataRate/1.0e6, numOfAntennas, intDurationSecs, m_outputDataSize);
            m_errorVector.addElement(str);
            return false;
        }
        return true;
    }

    /** A helper function to copy the error strings found during validation to the IDL string
     ** sequence returned to the caller.
     */
    //---------------------------------------------------------------------------------
    private void copyErrorStrings(alma.ACS.stringSeqHolder errorStringSeq)
    {
        int numErrors = m_errorVector.size();

        // If configuration invalid, add correlator type
        if( numErrors == 0 )
        {
            String[] retSeqString = new String[0];
            errorStringSeq.value = retSeqString;
            return;
        }

        String[] retSeqString = new String[numErrors+1];
        Iterator iter = m_errorVector.iterator();
        Object obj;
        int i = 0;
        retSeqString[i++] = new String("Errors found in configuration for Correlator type: ") + m_correlatorType + new String(":");
        while( iter.hasNext() )
        {
            obj = iter.next ();
            retSeqString[i++] = obj.toString().replaceAll("The following exception occured while validating field:", " ");
        }
        errorStringSeq.value = retSeqString;
    }

    //---------------------------------------------------------------------------------
    private boolean isSidebandProcessingModeAllowed(SidebandProcessingMode SidebandProcessingModeToCheck )
    {
        Iterator itr = m_sideBandProcessingModesAllowed.iterator();
        while( itr.hasNext() )
        {
            if( (SidebandProcessingMode)itr.next() == SidebandProcessingModeToCheck )
                return true;
        }
        return false;
    }

    /** Based on the correlator type, set various limits.
     */
    //---------------------------------------------------------------------------------
    private void setCorrelatorLimits()
    {
        System.out.println(" >> Correlator type is " + m_correlatorType );

        // all allow none
        m_sideBandProcessingModesAllowed = new Vector();
        m_sideBandProcessingModesAllowed.addElement(alma.SidebandProcessingModeMod.SidebandProcessingMode.NONE);
        if( m_correlatorType.equals(CORRELATOR_12m_1QUADRANT.value) )
        {
            m_maxNumSpectralWindowsPerBaseband = 1;
            m_maxNumAntennas = 16;
            m_maxNumBasebands = 4;
            m_maxNumChanAverRegions = 32;
            m_maxNumAPCDatasets = 2;
        }
        else if( m_correlatorType.equals(CORRELATOR_12m_2QUADRANT.value) )
        {
            m_maxNumSpectralWindowsPerBaseband = 1;
            m_maxNumAntennas = 32;
            m_maxNumBasebands = 4;
            m_maxNumChanAverRegions = 32;
            m_maxNumAPCDatasets = 2;
        }
        // The final system should be able to handle all options
        else if( m_correlatorType.equals(CORRELATOR_12m_4QUADRANT.value) )
        {
            m_maxNumSpectralWindowsPerBaseband = 4;
            m_maxNumAntennas = 64;
            m_maxNumBasebands = 4;
            m_maxNumChanAverRegions = 32;
            m_maxNumAPCDatasets = 2;
            // JIRA COMP-3115 allow all sideband processing modes for final correlator
            m_sideBandProcessingModesAllowed.addElement(SidebandProcessingMode.PHASE_SWITCH_SEPARATION);
            m_sideBandProcessingModesAllowed.addElement(SidebandProcessingMode.FREQUENCY_OFFSET_SEPARATION);
            m_sideBandProcessingModesAllowed.addElement(SidebandProcessingMode.PHASE_SWITCH_REJECTION);
            m_sideBandProcessingModesAllowed.addElement(SidebandProcessingMode.FREQUENCY_OFFSET_REJECTION);
        }
        else
        {
            // Default to 2-antenna case
            m_maxNumSpectralWindowsPerBaseband = 1;
            m_maxNumAntennas = 2;
            m_maxNumBasebands = 2;
            m_maxNumChanAverRegions = 10;
            m_maxNumAPCDatasets = 2;
        }
    }
}

/*
Table 5 Multi-resolution modes with one baseband channel per quadrant being processed.
Mode # Minimum size  Number of sub-channel Total            Number of           Correlation     Sample Factor
       of correlator   filters      Bandwidth  Spectral Points     
2       1/2          16                  1 GHz        8192                2-bit x 2-bit  Nyquist
3         1/4          8                   500 MHz      8192                2-bit x 2-bit  Nyquist
4         1/8          4                   250 MHz      8192                2-bit x 2-bit  Nyquist
5         1/16          2                   125 MHz     8192                 2-bit x 2-bit Nyquist
6         1/32         1                   62.5 MHz     8192                2-bit x 2-bit  Nyquist
25         1/32         1                   31.25 MHz     8192                2-bit x 2-bit  Twice Nyquist


Table 6 Multi-resolution modes with two baseband channels per quadrant with no polarization cross products.
9         1/2         8                   500 MHz     4096                2-bit x 2-bit  Nyquist
10         1/4         4                   250 MHz     4096                2-bit x 2-bit  Nyquist
11         1/8         2                   125 MHz     4096                2-bit x 2-bit  Nyquist
12         1/16         1                   62.5 MHz     4096                2-bit x 2-bit  Nyquist
31         1/16         1                   31.25 MHz     4096                2-bit x 2-bit  Twice Nyquist


Table 7 Multi-resolution modes with two baseband channels per quadrant with polarization cross products.
16         1/2         4                   250 MHz     2048                2-bit x 2-bit  Nyquist
17         1/4         2                   125 MHz     2048                2-bit x 2-bit  Nyquist
18         1/8         1                   62.5 MHz     2048                2-bit x 2-bit  Nyquist
37         1/8         1                   31.25 MHz     2048                2-bit x 2-bit  Twice Nyquist
 */
