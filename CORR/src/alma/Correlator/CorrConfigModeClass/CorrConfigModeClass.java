/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.CorrConfigModeClass;

//
// System stuff
//
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.IOException;

//
// ACS stuff
//
import com.cosylab.CDB.*;
import alma.acstime.Duration;

//
// ICD stuff
//
import alma.CorrelationBitMod.CorrelationBit;
import alma.StokesParameterMod.StokesParameter;
import alma.CorrConfigModeErr.wrappers.*;
import alma.Correlator.ConfigMode;
import alma.Correlator.CORRELATOR_12m_2ANTS;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;
import alma.Correlator.ePolarization;
import alma.Correlator.eFilterMode;
import alma.Correlator.eFraction;
import alma.PolarizationTypeMod.PolarizationType;

public class CorrConfigModeClass
{
    //
    // these macros hardcode the parameters used for computing TFB sub-band
    // frequency centers
    //
    private final float CORR_CONFIG_MODE_TFB_DDS_STEP = (float)0.030517578125;
    private final float CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW = (float)31.25;

    /** TFB drop-off fraction.
     ** In TFB mode the output spectra is obtained by aggregating side by
     ** side (stitching) a number of sub-band filters. In order to 
     ** reduce spectral artifacts at the edge of these passband filters
     ** their center frequencies are set such that they do overlap and
     ** the cdp stitches them accordingly. The net effect is that the 
     ** skirts of the filters are effectively dropped and the artifacts
     ** eliminated. As a side effect of this procedure the total bandwidth
     ** of any TFB mode is thus reduced. If 'O' is the bandwidth fraction that
     ** TFB sub-bands are overlaped then the reduction of the stitched
     ** spectrum is a fraction equal to 2*O. The following constant defines
     ** the value used for 2*O. Its optimal setting should be investigated.
     */
    private final float CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION = (float)0.0625;

    /** Vs constant.
     ** This is the Vs value for an integration of 1ms. This value matches
     ** the blanking time in the ALMA1 chip, therefore, any update to the
     ** firmware in this respects would require an update of this constant
     ** here. For 16ms modes we have:
     ** Vs = 16ms x (1/512ms) x 4.5 x (125,000 - 270)
     ** For 1, 2, 4, or 8 ms modes we have
     ** Vs = Nms x (1/32ms) x 4.5 x (125,000 - 270)
     ** 270 = blanking time
     ** The division by 1.0e4 is to scale to ACS 100ns units and the subtraction
     ** is to compensate (in average) for the truncation in the ALMA1 chip.
     */
    private final float CORR_CONFIG_MODE_Vs_CONST =
        (float)(4.5 * (125000.0 - 270.0) / 32.0 - 0.5) / (float)1.0e4;

    /** reads correlator type from cdb.
     ** this constructor accepts as parameter a DAL pointer, from which it will
     ** get a DAO reference to the attribute from where the correlator type is
     ** supposed to be available. Hardcoded to read from alma/CORR_MASTER_COMP
     ** and the atribute name is corrType.
     ** @param dal_p DAL reference
     */
    public CorrConfigModeClass(DAL dal)
        throws AcsJConstructorFailureEx
    {
        //
        // populate the set of know correlator types
        //
        setKnownHWTypes();
 
        //
        // read correlator type from TMCDB, this makes possible to be sure
        // that the running software is in synch with what this class provides
        // as valid modes
        //
        try
        {
            readType(dal);
        }
        catch ( AcsJReadTypeEx ex)
        {
            throw new AcsJConstructorFailureEx(ex);
        }

        //
        // if the retreived type does not match then bail out
        //
        if ( !m_knownTypes.contains(m_type) )
        {
            AcsJInvalidTypeEx _ex =
                new AcsJInvalidTypeEx(m_type + " not a valid correlator type");
            
            throw new AcsJConstructorFailureEx("cdb reports invalid correlator type",
                                               _ex.getCause());
        }

        //
        // read all modes table from file and build current table based on
        // current type
        //
        try
        {
            generateModesTable();
        }
        catch ( AcsJAccessFileEx ex )
        {
            throw new AcsJConstructorFailureEx(ex);
        }
        catch ( AcsJInvalidFormatEx ex )
        {
            throw new AcsJConstructorFailureEx(ex);
        }
        catch ( AcsJEmptyTableEx ex)
        {
            throw new AcsJConstructorFailureEx(ex);
        }
    }

    /** use given correlator type.
     ** this constructor validates the received string agains the known set
     ** of correlator types and initializes the table accordingly.
     ** @param corrType intended correlator type name
     */
    public CorrConfigModeClass(String corrType)
        throws AcsJConstructorFailureEx
    {
        //
        // populate the set of know correlator types
        //
        setKnownHWTypes();
 
        //
        // if the received type does not match then bail out
        //
        if ( !m_knownTypes.contains(corrType) )
        {
            AcsJInvalidTypeEx _ex =
                new AcsJInvalidTypeEx(corrType + " not a valid correlator type");
            
            
            throw new AcsJConstructorFailureEx("constructor received unknow correlator type",
                                             _ex.getCause());
        }

        //
        // copy received type into our variable
        //
        m_type = corrType;

        //
        // read all modes table from file and build current table based on
        // current type
        //
        try
        {
            generateModesTable();
        }
        catch ( AcsJAccessFileEx ex)
        {
            throw new AcsJConstructorFailureEx(ex);
        }
        catch ( AcsJInvalidFormatEx ex)
        {
            throw new AcsJConstructorFailureEx(ex);
        }
        catch ( AcsJEmptyTableEx ex)
        {
            throw new AcsJConstructorFailureEx(ex);
        }
    }

    /** gets a copy of the correlator type name.
     ** @param t string by reference where to copy the name
     */
    public String getType()
    { 
        return m_type;
    }

    /** gets size of the modes table for the current correlator type.
     ** @return number of modes in the table
     */
    public int getSize()
    {
        return m_table.size();
    }

    /** finds the associated mode key from a set of specific parameters.
     ** Provided the mode's parameters that are part of the correlator
     ** configuration IDL this method discovers the key associated to the
     ** mode that those parameters are addressing.
     ** This findMode version assumes that the bandwidth and channels are
     ** nominal values, that is, without any TFB overlapping for stitching.
     */
    public int findModeFromNominal(float          nominalBandwidthMHz,
                                   long           nominalChannels,
                                   CorrelationBit bits,
                                   boolean        overSampled,
                                   StokesParameter[] polarizationProducts)
        throws AcsJInvalidModeEx
    {
        ePolarization polz;
        Iterator keyIter = m_table.keySet().iterator();
        
        //
        // decode the polarization Stokes sequence into our workable enum
        //
        try
        {
            polz = decodePolzSeq(polarizationProducts);
        }
        catch ( AcsJInvalidStokesParameterEx ex)
        {
            throw new AcsJInvalidModeEx("invalid sequence", ex.getCause());
        }

        //
        // the procedure works by brute force, which should not be a big issue
        // given that the table is not that big
        //
        while( keyIter.hasNext() )
        {
            int key;
            ConfigMode m;

            //
            // key value
            //
            key = ((Integer)keyIter.next()).intValue();

            //
            // mode value from the table for the current key
            //
            m = m_table.get(key);

            //
            // if it matches then we got it
            //
            if ( m.bandWidth            == nominalBandwidthMHz      &&
                 m.channels             == nominalChannels          &&
                 m.bits                 == bits                     &&
                 m.overSampled          == overSampled              &&
                 m.polarizationProducts == polz )
            {
                return key;
            }
        }
        
        throw new AcsJInvalidModeEx("parameters do not map to any valid mode");
    }

    /** finds the associated mode key from a set of specific parameters.
     ** Provided the mode's parameters that are part of the correlator
     ** configuration IDL this method discovers the key associated to the
     ** mode that those parameters are addressing.
     ** This findMode version assumes that the bandwidth and channels are
     ** effective values and checks that they do make sense based on the
     ** current TFB drop-off fraction.
     */
    public int findModeFromEffective(float          effectiveBandwidthMHz,
                                     long           effectiveChannels,
                                     CorrelationBit bits,
                                     boolean        overSampled,
                                     StokesParameter[] polarizationProducts)
        throws AcsJInvalidModeEx
    {
        int n = -1;
        int o = -1;
        int w = -1;
        ePolarization polz;

        //
        // decode the polarization Stokes sequence into our workable enum
        //
        try
        {
            polz = decodePolzSeq(polarizationProducts);
        }
        catch ( AcsJInvalidStokesParameterEx ex)
        {
            throw new AcsJInvalidModeEx("invalid sequence", ex.getCause());
        }

        //
        // first we need to infer the nominal values:
        //
        //          2^(n-1) < effectiveChannels <= 2^n
        //
        // where 2^n is the nominal value. Now, the actual dropout is restricted
        // to a set of fractions that effectively reduce the number of channels and
        // bandwith in the following way:
        //
        //          E = N * (1 - f)
        //
        // where 'f' is the dropout fraction. If we express f as 2^o then the
        // afore mentioned resstriction is such that 'o' must be an item of
        // the following set:
        //
        //          [8, 16, 32, 64]
        //
        // that is, one eigth or one sixteenth or 1 over 32 or 1 over 64.
        // For checking for this condition it is convenient to elaborate the
        // effective expression as it follows:
        //
        //          E = N * (1 - f)
        //          E = N * (1 - 1 / 2^o)
        //          N - E = N - N * (1 - 1 / 2^o)
        //          N - E = N * (1 - (1 - 1 / 2^o))
        //          N - E = N * (1 / 2^o)
        //          2^n - E = 2^n / 2^o
        //          2^n - E = 2^(n-o)
        //
        // Therefore, the validation consist on verifying which 'o' value from
        // the set shown above makes possible a nominal minus effective difference
        // as shown in the last expression. The good news here is that there
        // is no floating point arithmetic, which is desirable just in case some
        // rounding could start playing games.
        //
        // On the other hand, the bandwith accepts a similar procedure. The expected
        // nominal bandwidth W is such that
        //
        //          W = 31.25 * (2 ^ w)
        //
        // where 'w' is an integer in the set [0, 1, 2, 3, 4, 5, 6]. This means
        // that the nominal bandwidth for this mode is defined by the 'w' integer
        // that satifies:
        //
        //          E = 31.25 * (2^w - 2^(w-o))
        //
        // NOTE: all this expedient would work only under the assumtion that
        // the dropout fraction is small enough as not to confuse the 'n' 
        // computation with a number that is 2 times smaller than the real
        // one we are looking for. This assumption is enforced by restricting
        // the valid values of 'o' to the [8, 16, 32, 64] set. As to visualize
        // this issue let's have the following table of possible channels:
        //
        //  channels/o      8       16      32      64
        //  8192            7168    7680
        //  4096            3584
        //  2048
        //  1024
        //  512
        //  256
        //

        //
        // if this is TDM then we can return immediately, note that in TDM
        // nominal is always effective.
        //
        if ( (effectiveChannels == 256        &&
              bits == CorrelationBit.BITS_2x2 &&
              !overSampled                    &&
              (polz == ePolarization.POLZ_SINGLE_X ||
               polz == ePolarization.POLZ_SINGLE_Y)) 
             ||
             (effectiveChannels == 64 || effectiveChannels == 128) )
        {
            //
            // make sure that the number of channels really makes sense for TDM
            //
            if ( effectiveChannels != 256 &&
                 effectiveChannels != 128 &&
                 effectiveChannels != 64 )
            {
                throw new AcsJInvalidModeEx("invalid TDM mode number of channels");
            }

            //
            // everything looks in place for TDM
            //
            return findModeFromNominal(effectiveBandwidthMHz,
                                       effectiveChannels,
                                       bits,
                                       overSampled,
                                       polarizationProducts);
        }

        //
        // infer 'n' as explained above
        //
        for ( int i = 8; i <= 13; i++ )
        {
            if ( effectiveChannels <= (0x1 << i) )
            {
                n = i;
                
                break;
            }
        }
        
        //
        // validate 'n'
        //
        if ( n == -1 )
        {
            throw new AcsJInvalidModeEx("invalid effective number of channels");
        }

        //
        // if nominal equals effective then return now
        //
        if ( (0x1 << n) == effectiveChannels )
        {
            return findModeFromNominal(effectiveBandwidthMHz,
                                       effectiveChannels,
                                       bits,
                                       overSampled,
                                       polarizationProducts);
        }

        //
        // infer 'o' as explained before
        //
        for ( int i = 3; i <= 6; i++ )
        {
            if ( (0x1 << n) - effectiveChannels == (0x1 << (n - i)) )
            {
                o = i;
            }
        }

        //
        // validate 'o'
        //
        if ( o == -1 || (0x1 << o) != (int)((float)1.0 / CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) )
        {
            throw new AcsJInvalidModeEx("effective channels do not mach the drop-offfraction");
        }

        //
        // infer 'w'
        //
        for ( int i = 0; i < m_knownNominalBandwidths.length; i++ )
        {
            if ( effectiveBandwidthMHz == m_knownNominalBandwidths[i] * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) )
            {
                w = i;
            }
        }

        //
        // validate 'w'
        //
        if ( w == -1 )
        {
            throw new AcsJInvalidModeEx("effective bandwidth does not mach the drop-off fraction");
        }

        return findModeFromNominal(CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW * (float)(0x1 << w),
                                   effectiveChannels + (0x1 << (n - o)),
                                   bits,
                                   overSampled,
                                   polarizationProducts);
    }

    /** equivalent to bracket operator in C++, retrieves the corresponding
     ** entry of the modes table.
     ** The returned structure is defined in CorrConfigMode.idl.
     ** @return configuration mode structure
     */
    public ConfigMode getMode(int key)
        throws AcsJInvalidKeyEx
    {
        ConfigMode m = m_table.get(key);

        if ( m == null )
        {
            throw new AcsJInvalidKeyEx("invalid key " + key);
        }

        return m;
    }

    /** it generates an array of available keys.
     ** The purpose of this method is to provide an easy mapping between a
     ** zero based index (up to the size of the table minus one) an the 
     ** current modes, which is useful for some clients of this class. The
     ** keys are sorted before going into the array, which is convenient
     ** in some cases but not really mandatory.
     ** @return an array of mode's keys
     */
    public int[] getKeys()
    {
        int[] keys = new int[m_table.size()];

        //
        // go through the table. The hash map not necessarily preserves
        // any order, but it is better to assume here that the key are
        // in assending order within the array.
        //
        Map sortedTable = new TreeMap(m_table);
        Iterator it = sortedTable.entrySet().iterator();
        int i = 0;
        while ( it.hasNext() )
        {
            keys[i++] = ((Integer)((Map.Entry)it.next()).getKey()).intValue();
        }

        return keys;
    }

    /** produces a printable representation of the table.
     ** @return string representation of the table
     */
    public String asString()
    {
        String s;
        
        s = "# " + m_type + "\n";
        s += "# size " + m_table.size() + "\n";
        s += "# mode\tbw\tchan\tbits\t2nyq.\tpolz\tfilterMode" + "\n";

        //
        // go through the table. The hash map not necessarily preserves
        // any order, therefore, we must explicitly enforce a printout
        // from smaller to bigger keys.
        //
        Map sortedTable = new TreeMap(m_table);
        Iterator it = sortedTable.entrySet().iterator();
        while ( it.hasNext() )
        {
            //
            // copy key and value into local variables, this ought to be done
            // at one because 'next' seems to advance the iterator
            //
            Map.Entry entry = (Map.Entry)it.next();
            ConfigMode m = (ConfigMode)(entry.getValue());

            s += "  " + entry.getKey() + "\t";
            s += m.bandWidth + "\t";
            s += m.channels + "\t";

            //
            // ALMA1 number of bits
            //
            s += m.bits.toString() + "\t"; 

            //
            // band-width half of that required for sampling rate
            //
            if ( m.overSampled )
            {
                s += "true" + "\t";
            }
            else
            {
                s += "false" + "\t";
            }

            //
            // type of polarization products
            //
            s += enumAsString(m.polarizationProducts) + "\t";

            //
            // TFB mode
            //
            s += enumAsString(m.filterMode) + "\n";
        }
        
        return s;
    }

    
    /** computes correlator resources fraction for a given mode's key.
     ** Multiple resolution modes are implemented in hardware such that
     ** the actual number of lags is for any given mode is sacrified to
     ** a smaller value, freeing in the exercise hardware resources thus
     ** available for an additional concurrent spectral window. The reduction
     ** of lags happens such the new value is either half, or one eighth,
     ** or one fourth of the full mode.
     ** @param key mode identifier.
     ** @return correlator fraction as defined by the Correlator::eFraction enum; 
     ** if the mode is not multi-resolution savvy then FULL is returned.
     */
    public eFraction getFraction(int key)
        throws AcsJInvalidKeyEx, AcsJModeInconsistencyEx
    {
        ConfigMode m;
    
        //
        // get mode from table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }
    
        //
        // multiple resolution modes are implemented only for a subset of
        // all possible modes
        //
        if ( m.filterMode == eFilterMode.FILTER_TDM ||
             m.bits != CorrelationBit.BITS_2x2 )
        {
            return eFraction.FRACTION_FULL;
        }

        //
        // in full mode this is the number of total channels. Declared as
        // unsigned for simplified operations.
        //
        int fullModeChannels;

        //
        // compute full-mode number of channels
        //
        switch ( m.polarizationProducts.value() )
        {
        case ePolarization._POLZ_SINGLE_X:
        case ePolarization._POLZ_SINGLE_Y:
            fullModeChannels = 8192;
            break;
        case ePolarization._POLZ_DOUBLE:
            fullModeChannels = 4096;
            break;
        case ePolarization._POLZ_FULL:
            fullModeChannels = 2048;
            break;
        default:
            throw new AcsJModeInconsistencyEx("unexpected polarization products");
        }
        
        //
        // compare current mode of channels with full-mode, the quotient
        // is all what it takes to say the fraction
        //
        if ( m.channels == fullModeChannels )
        {
            return eFraction.FRACTION_FULL;
        }
        else if ( (m.channels << 1) == fullModeChannels )
        {
            return eFraction.FRACTION_ONE_HALF;
        }
        else if ( (m.channels << 2) == fullModeChannels )
        {
            return eFraction.FRACTION_ONE_FOURTH;
        }
        else if ( (m.channels << 3) == fullModeChannels )
        {
            return eFraction.FRACTION_ONE_EIGHTH;
        }
        else
        {
            throw new AcsJModeInconsistencyEx("unexpected number of channels for a multi-resolution mode");
        }
    }

    /** computes number of TFB sub-bands for a given mode's key.
     */
    public int getNumberSubBands(int key)
        throws AcsJInvalidKeyEx, AcsJModeInconsistencyEx
    {
        ConfigMode m;
        
        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // TFB sub-bands are meaningful only for FDM modes
        //
        if ( m.filterMode != eFilterMode.FILTER_FDM )
        {
            throw new AcsJModeInconsistencyEx("this mode does not involve any TFB sub-band");
        }

        //
        // decoding of the table, hardcoded and based on the band-width
        //
        if ( m.bandWidth == 2000.0 )
        {
            return 32;
        }
        else if ( m.bandWidth == 1000.0 )
        {
            return 16;
        }
        else if ( m.bandWidth == 500.0 )
        {
            return 8;
        }
        else if ( m.bandWidth == 250.0 )
        {
            return 4;
        }
        else if ( m.bandWidth == 125.0 )
        {
            return 2;
        }
        else if ( m.bandWidth == 62.5 || m.bandWidth == 31.25 )
        {
            return 1;
        }
        else
        {
            throw new AcsJModeInconsistencyEx("unexpected band-width " + m.bandWidth);
        }
    }   
    
    /** computes number of polarization products for a given mode's key.
     */
    public int getNumberPolzProducts(int key)
        throws AcsJInvalidKeyEx
    {
        ConfigMode m;

        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // from the enum value compute the number of involved products
        //
        if ( m.polarizationProducts == ePolarization.POLZ_SINGLE_X ||
             m.polarizationProducts == ePolarization.POLZ_SINGLE_Y )
        {
            return 1;
        }
        else if ( m.polarizationProducts == ePolarization.POLZ_DOUBLE )
        {
            return 2;
        }
        
        return 4;
    }
    
    /** returns the polarization products as a sequence of polarization
     ** types pairs.
     ** A helper method for building a sequence of polarization types
     ** that encodes the same information as the sequence of Stokes
     ** parameters found in the correlator configuration structure.
     ** The mapping between the feasible polarization products and 
     ** the output of this method is like this:
     **
     **         single X = [[X, X]]
     **         ngle Y = [[Y, Y]]
     **         double = [[X, X], [Y, Y]]
     **         full = [[X, X], [X, Y], [Y, X], [Y, Y]]
     **
     ** @param key mode identifier.
     ** @return sequence of polarization types pairs.
     */
    public PolarizationType[][] getPolzProductTypes(int key)
        throws AcsJInvalidKeyEx
    {
        ConfigMode m;
        PolarizationType[] pair = new PolarizationType[2];
        PolarizationType[][] ret = null;

        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // from the encoded polarization product type build the output
        // sequence of pairs.
        //
        switch ( m.polarizationProducts.value() )
        {
        case ePolarization._POLZ_SINGLE_X:
            ret = new PolarizationType[1][2];
            
            pair[0] = pair[1] = PolarizationType.X;
            System.arraycopy(pair, 0, ret[0], 0, 2);
            
            break;
        case ePolarization._POLZ_SINGLE_Y:
            ret = new PolarizationType[1][2];

            pair[0] = pair[1] = PolarizationType.Y;
            System.arraycopy(pair, 0, ret[0], 0, 2);

            break;
        case ePolarization._POLZ_DOUBLE:
            ret = new PolarizationType[2][2];

            pair[0] = pair[1] = PolarizationType.X;
            System.arraycopy(pair, 0, ret[0], 0, 2);

            pair[0] = pair[1] = PolarizationType.Y;
            System.arraycopy(pair, 0, ret[1], 0, 2);

            break;
        case ePolarization._POLZ_FULL:
            ret = new PolarizationType[4][2];

            pair[0] = pair[1] = PolarizationType.X;
            System.arraycopy(pair, 0, ret[0], 0, 2);

            pair[0] = PolarizationType.X;
            pair[1] = PolarizationType.Y;
            System.arraycopy(pair, 0, ret[1], 0, 2);

            pair[0] = PolarizationType.Y;
            pair[1] = PolarizationType.X;
            System.arraycopy(pair, 0, ret[2], 0, 2);

            pair[0] = pair[1] = PolarizationType.Y;
            System.arraycopy(pair, 0, ret[3], 0, 2);

            break;        
        }

        return ret;
    }

    /** computes Vs which is a function of the mode and integration time.
     ** There are a few aspects involved in the computation of Vs, like
     ** the number of adder planes (when applicable) and the number of bits
     ** used for representing the lags values correlated by means of ALMA1.
     ** @param key mode identifier.
     ** @param dumpDuration hardware integration time in ACS units
     ** @return Vs, that is, accumulated value over a dump duration interval
     ** for of a null signal at correlator input.
     */
    public float getVs(int key, Duration dumpDuration)
        throws AcsJInvalidKeyEx
    {
        ConfigMode m;
        int factor;
        float vs;

        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // this is the base Vs, common to all modes, the rest consist in 
        // finding out the factor by which this base value must be scaled.
        // This is one of those place that will require some 'tuning' at the
        // moment the ACS units of time is changed. Vs is calculated differently
        // for auto-only modes with 1, 2, 4, or 8ms dump durations vs. cross & auto
        // modes of 16ms intervals. 
        //
        vs = CORR_CONFIG_MODE_Vs_CONST * (float)dumpDuration.value;
        if ( dumpDuration.value >= 160000 )
        {
            vs /= (float)16.0;
        }

        //
        // the factor depends on the filter mode, the number of bits, over
        /// sampling flag and band-width
        //
        if ( m.filterMode == eFilterMode.FILTER_TDM )
        {
            if ( m.bits == CorrelationBit.BITS_3x3 )
            {
                //
                // this will probably change, ask Alejandro.
                //
                factor = 800;
            }
            else
            {
                factor = 32;
            }
        }
        // FDM
        else
        {
            if ( m.bits == CorrelationBit.BITS_2x2 )
            {
                if ( m.overSampled )
                {
                    if ( m.bandWidth == 31.25 )
                    {
                        // TFB - 2x2 - 2Nyq - 31.25MHz 
                        factor = 1;
                    }
                    else
                    {
                        // TFB - 2x2 - 2Nyq - !=31.25MHz
                        factor = 2;
                    }
                }
                else
                {
                    // TFB - 2x2 - Nyq - any band-width
                    factor = 1;
                }
            }
            // TFB 4x4
            else
            {
                if ( m.overSampled )
                {
                    if ( m.bandWidth == 31.25 )
                    {
                        // TFB - 4x4 - 2Nyq - 31.25MHz
                        factor = 25;
                    }
                    else
                    {
                        // TFB - 4x4 - 2Nyq - !=31.25MHz
                        factor = 50;
                    }
                }
                else
                {
                    // TFB - 4x4 - Nyq - any band-width
                    factor = 25;
                }
            }
        }
        
        return vs * (float)factor; 
    }
    
    /** computes TFB sub-bands frequency center.
     ** For all sub-bands in any given TFB mode this method computes
     ** individual sub-band center frequencies and returns them as a
     ** vector of float values. Provided that fc is an integer multiple 
     ** of the Direct Digital Synthesizer (DDS) step all computed frequency
     ** centers are integral multiples of that step as well. This is a strong
     ** requirement for a proper configuration of the DDS frequency register.
     ** This also means that the observer is constrained to a finite set of
     ** fc values across the 2GHz band width.
     ** The algorithm for locating all sub-bands around fc is such that a
     ** fixed (not configurable) amount of band width is overlapped among
     ** consecutive sub-bands. The rule is to drop in total one sixteenth 
     ** of the total band width of each sub-band, therefore, they overlap
     ** in an ammount equal to one sixteenth of their band width.
     ** Overlapping happens only when more than one TFB sub-band is involved
     ** and for those modes the total band width of each sub-band is 62.5MHz.
     ** Therefore, 3.90625 MHz are effectively dropped per sub-band, and
     ** consecutive sub-bands ovelapped by 1.953125 MHz.
     ** Overlapping is used as a means for getting rid of those skirt artifacts
     ** present at both extremes of a TFB sub-band, and it requires proper
     ** stitching within the cdp software.
     ** Modes that involve only one sub-band are such that fc coincides with
     ** that value returned as a vector of unitary dimension.
     ** This method always checks that fc is an integer number of the DDS step,
     ** failing to prove that an exception is thrown.
     ** The algorithm works such that sub-bands to the left of fc are squeezed
     ** together to the right, and those to the right of fc are squeezed to
     ** the left. And given that the number of sub-bands is an even number (except
     ** for those modes that include one single TFB sub-band) then it results
     ** that fc is always at the center of the overlapping area of the 2 
     ** sub-bands at the center of the spectral window.
     */
    public float[] getSubBandsFc(int key, float fc)
        throws AcsJInvalidKeyEx, AcsJInvalidFcEx, AcsJFcOutOfRangeEx, AcsJModeInconsistencyEx
    {
        ConfigMode m;
        int Ns;
        float hebw;

        //
        // fc must be an integer multiple of the DDS step
        //
        if ( java.lang.Math.IEEEremainder((double)fc,
                                          (double)CORR_CONFIG_MODE_TFB_DDS_STEP)
             != 0.0 )
        {
            throw new AcsJInvalidFcEx("invalid fc value " + fc);
        }

        //
        // get the mode
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // if this is not a FDM mode then complain
        //
        if ( m.filterMode != eFilterMode.FILTER_FDM )
        {
            throw new AcsJModeInconsistencyEx("not an FDM mode for frequency center computation");
        }

        //
        // get number of TFB sub-bands
        //
        try
        {
            Ns = getNumberSubBands(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }
        catch ( AcsJModeInconsistencyEx ex )
        {
            throw ex;
        }

        //
        // get the effective bandwidth and divide it by 2, which is the actual
        // value required afterwards
        //
        try
        {
            hebw = getEffectiveBandWidth(key) / (float)2.0;
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }
        catch ( AcsJModeInconsistencyEx ex )
        {
            throw ex;
        }

        //
        // if received fc is incompatible with the effective bandwidth
        // then complain.
        //
        if ( fc - hebw < 0.0 || fc +  hebw > 2000.0 )
        {
            throw new AcsJModeInconsistencyEx("frequency center incompatible for effective bandwidth " + key + "/" + fc + "/" + (float)2.0 * hebw);
        }

        //
        // resize output parameter accordingly
        //
        float[] freqs = new float[Ns];

        //
        // the algorithm. Here the band width amount dropped away is hardcoded
        // to one sixteenth. ABaudry indicates this is the right 'rule' to apply,
        // any change or intention to make this 'rule' an actual parameter
        // would require the needed changes.
        // Note: it works for Ns=1 as well.
        //
        for ( int i = 0; i < Ns; i++ )
        {
            freqs[i] = fc + CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW * ((float)1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) * (float)(2 * i + 1 - Ns);
        }

        return freqs;
    }

    /** returns the effective band width of the given mode.
     ** TFB modes are implemented such that the spectral window is built
     ** based on 1 or more sub-bands that are stitched together as to form
     ** the output spectral window. For stitching to work it is required that
     ** individual sub-bands are actually overlapped in frequency, getting
     ** rid with this of the skirts artifacts at every sub-band filter edge.
     ** Therefore, the effective obtained band width is sligthly narrower
     ** than the nominal one, same applies for the number of spectral channels.
     ** For any other mode than TFB the effective band-width is the same as
     ** the nominal one.
     ** @param key mode identifier.
     ** @return effective band width in MHz.
     */
    public float getEffectiveBandWidth(int key)
        throws AcsJInvalidKeyEx, AcsJModeInconsistencyEx
    {
        ConfigMode m;
        
        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }
        
        //
        // if this is not an FDM mode then return the nominal band width
        //
        if ( m.filterMode != eFilterMode.FILTER_FDM )
        {
            return m.bandWidth;
        }
        
        return m.bandWidth * ((float)1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION);
    }

    /** returns the effective number of spectral channels for the given mode.
     ** See getEffectiveBandWidth for a proper description of the difference
     ** between nominal and effective figures.
     ** @param key mode identifier.
     ** @return effective number of spectral channels.
     */
    public int getEffectiveChannels(int key)
        throws AcsJInvalidKeyEx, AcsJModeInconsistencyEx
    {
        ConfigMode m;
        
        //
        // get the mode from the table
        //
        try
        {
            m = getMode(key);
        }
        catch ( AcsJInvalidKeyEx ex )
        {
            throw ex;
        }

        //
        // if this is not an FDM mode then return the nominal channels
        //
        if ( m.filterMode != eFilterMode.FILTER_FDM )
        {
            return m.channels;
        }

        //
        // if the nominal band width is not divisible by the overlapping fraction
        // then something is out of whack
        //
        if ( (float)m.channels * CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION -
             (long)((float)m.channels * CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION)
             != 0 )
        {
            throw new AcsJModeInconsistencyEx("nominal channels and overlapping fraction do not match");
        }

        return (int)(m.channels * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION));
    }

    //
    // variable used internally
    //
    private HashSet m_knownTypes = new HashSet();
    private String m_type;
    private Map<Integer, ConfigMode> m_table = new HashMap<Integer, ConfigMode>();
    private float[] m_knownNominalBandwidths;
    private int[] m_knownNominalChannels;

    /** defines the set of known correlator type names, nominal bandwidths and
     ** nominal channels.
     */
    private void setKnownHWTypes()
    {
        //
        // correlator configurations
        //
        m_knownTypes.add(CORRELATOR_12m_2ANTS.value);
        m_knownTypes.add(CORRELATOR_12m_1QUADRANT.value);
        m_knownTypes.add(CORRELATOR_12m_2QUADRANT.value);
        m_knownTypes.add(CORRELATOR_12m_4QUADRANT.value);

        //
        // bandwidths
        //
        m_knownNominalBandwidths = new float[7];
        for ( int i = 0; i < m_knownNominalBandwidths.length; i++ )
        {
            m_knownNominalBandwidths[i] =
                (0x1 << i) * CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW;
        }

        //
        // channels
        //
        m_knownNominalChannels = new int[8];
        for ( int i = 0; i < m_knownNominalChannels.length; i++ )
        {
            m_knownNominalChannels[i] = (0x1 << i) * 64;
        }
    }

    /** reads the correlator type.
     ** access the CDB in search for an attribute under the Correlator Master
     ** component from where to read a string with the name of the current
     ** correlator type.
     */
    private void readType(DAL dal)
        throws AcsJReadTypeEx
    {
        //
        // normally we should access the TMCDB, but for now let's read a 
        // string field under correlator master component
        //

        try
        {
            //
            // get cbd object to master record
            //
            DAO dao = dal.get_DAO_Servant("alma/CORR_MASTER_COMP");
        
            //
            // access field
            //
            m_type = dao.get_string("corrType");
        }
        catch ( Throwable thr )
        {
            throw new AcsJReadTypeEx("access to cdb atribute thrown unknown exception");
        }
    }

    /** helper function for converting a polarization enum into a string.
     ** @param polz polarization product
     ** @return string representation of that enum
     */
    private static String enumAsString(final ePolarization polz)
    {
        if ( polz == ePolarization.POLZ_SINGLE_X )
        {
            return new String("X-X");
        }
        else if ( polz == ePolarization.POLZ_SINGLE_Y )
        {
            return new String("Y-Y");
        }
        else if ( polz == ePolarization.POLZ_DOUBLE )
        {
            return new String("DOUBLE");
        }

        return new String("FULL");
    }

    /** helper function for converting a filter mode enum into a string.
     ** @param filter filter mode enum
     ** @return string representation of that enum
     */
    private static String enumAsString(final eFilterMode filter)
    {
        if ( filter == eFilterMode.FILTER_FDM )
        {
            return new String("FDM");
        }
        
        return new String("TDM");
    }
    
    /** helper function for converting a fraction enum into a string.
     ** @param fraction mode enum
     ** @return string representation of that enum
     */
    private static String enumAsString(final eFraction fraction)
    {
        if ( fraction == eFraction.FRACTION_FULL )
        {
            return new String("FULL");
        }
        else if ( fraction == eFraction.FRACTION_ONE_HALF )
        {
            return new String("ONE_HALF");
        }
        else if ( fraction == eFraction.FRACTION_ONE_FOURTH )
        {
            return new String("ONE_FOURTH");
        }

        return new String("ONE_EIGHTH");
    }
    
    /** helper function for decoding a Stokes parma sequence into an enum.
     ** The idl configuration defines the polarization products as a sequence
     ** of the involved Stokes components. This method decode the sequence
     ** into an enum variable which is the actual type used by the table
     ** for mapping specific polarization product results (SINGLE_X, SINGLE_Y,
     ** DOUBLE or FULL).
     ** @param polz sequence of Stokes parameters as coming with a configuration.
     ** @return enum that matches the sequence.
     */
    private static ePolarization decodePolzSeq(final StokesParameter[] polz)
        throws AcsJInvalidStokesParameterEx
    {
        //
        // the length must be 1, 2 or 4
        //
        if ( polz.length != 1 &&
             polz.length != 2 &&
             polz.length != 4 )
        {
            throw new AcsJInvalidStokesParameterEx("invalid length");
        }

        //
        // check the sequence is valid
        //
        for ( int i = 0; i < polz.length; i++ )
        {
            //
            // all individual params must be XX or YY or XY or YX
            //
            if ( polz[i] != StokesParameter.XX
                 &&
                 polz[i] != StokesParameter.YY
                 &&
                 polz[i] != StokesParameter.XY
                 &&
                 polz[i] != StokesParameter.YX )
            {
                throw new AcsJInvalidStokesParameterEx("at least one Stokes param is not in the valid set");
            }

            //
            // parameters in the sequence must not repeat
            //
            for ( int j = i + 1; j < polz.length; j++ )
            {
                if ( polz[i] == polz[j] )
                {
                    throw new AcsJInvalidStokesParameterEx("at least two Stokes param repeat in the sequence");
                }
            }
        }

        //
        // final decode
        //
        if ( polz.length == 1 )
        {
            if ( polz[0] == StokesParameter.XX )
            {
                return ePolarization.POLZ_SINGLE_X;
            }
            else
            {
                return ePolarization.POLZ_SINGLE_Y;
            }
        }
        else if ( polz.length == 2 )
        {
            return ePolarization.POLZ_DOUBLE;
        }

        return ePolarization.POLZ_FULL;
    }

    /** a helper method for checking the existence of a given file.
     ** @param name full path to a file.
     ** @return a File object or null when the file does not exist.
     */
    private File getFile(String name)
    {
        File file;

        //
        // this one has little chances to throw an exception, it seems that the
        // only case for which it will throw an exception is when the argument
        // is null.
        //
        try
        {
            file = new File(name);

        }
        catch ( Throwable th )
        {
            return null;
        }

        //
        // if the file is there then return the File object
        //
        if ( file.exists() )
        {
            return file;
        }

        return null;
    }

    /** it reads the consolidated table from file and picks the modes applicable
     ** only for the current type.
     ** This method accesses an ascii file in which all possible modes
     ** are described for each key. This consolidated table defines the
     ** 'TFBMode' values applicable afterwards to any hardware type. The method
     ** filters out those modes not applicable for the current correlator
     ** hardware type.
     ** The the name of the ascii file is corrConfigModes.txt, which is embedded
     ** into the jar file for this class.
     */
    private void generateModesTable()
        throws AcsJAccessFileEx, AcsJInvalidFormatEx, AcsJEmptyTableEx
    {
        boolean ingestFlag;

        //
        // make sure the table is empty before starting
        //
        m_table.clear();

        //
        // get access to the ascii file embedded into the jar
        //
        InputStream is = getClass().getResourceAsStream("corrConfigModes.txt");
        
        //
        // check that we did find the file
        //
        if ( is == null )
        {
            throw new AcsJAccessFileEx("cannot find modes file in jar");
        }
  
        //
        // use these variables for accessing the file
        //
        String stringRead = null;
        BufferedReader br = null;
        
        //
        // reader object
        //
        try
        {
            br = new BufferedReader(new InputStreamReader(is));
        }
        catch ( Throwable thr )
        {
            throw new AcsJAccessFileEx("access to modes file thrown exception");
        }

        //
        // prime the read
        //
        try
        {
            stringRead = br.readLine();
            
        }
        catch ( IOException ioex )
        {
            throw new AcsJAccessFileEx("IO exception when reading line");
        }

        //
        // loop through the modes for our type
        //
        int i, tmp, modeKey = -1;
        String str;
        String[] modeStr;
        while ( stringRead != null )
        {
            //
            // skip every line that starts with '#'
            //
            if ( stringRead.startsWith("#") )
            {
                //
                // read next line
                //
                try
                {
                    stringRead = br.readLine();                
                }
                catch ( IOException ioex )
                {
                    throw new AcsJAccessFileEx("IO exception when reading line");
                }

                continue;
            }

            //
            // this is the object that goes into the map, therefore, a 
            // new one must be created for each record in the table
            //
            ConfigMode mode = new ConfigMode();

            try
            {
                //
                // split the current line into the expected mode's params
                //
                modeStr = stringRead.split("[ \\t]+");

                //
                // the regular expression used above is such that it 
                // should discover 1 extra empty token (the first entry
                // in the array). Check that the number of tokens
                // is the expected one
                //
                if ( modeStr.length != 8 )
                {
                    throw new AcsJInvalidFormatEx("invalid number of fields " +
                                                  modeStr.length);
                }

                //
                // this variable points to the current entry in the tokens
                // array, it must reset on ech iteration
                //
                i = 1;

                //
                // mode key, we expect this key to be an incresing integer
                // from 1 to the size of the table
                //
                tmp = Integer.parseInt(modeStr[i++]);

                //
                // check that the key matches what we expect
                //
                if ( modeKey != -1 && tmp <= modeKey )
                {
                    throw new AcsJInvalidFormatEx("invalid key field " + tmp);
                }
                modeKey = tmp;

                //
                // band-width
                //
                mode.bandWidth = Float.parseFloat(modeStr[i++]);

                //
                // number of spectral channels
                //
                mode.channels = Integer.parseInt(modeStr[i++]);

                //
                // ALMA1 bits resolution
                //
                str = modeStr[i++];
                if ( str.equals("2x2") )
                {
                    mode.bits = CorrelationBit.from_int(CorrelationBit._BITS_2x2);
                }
                else if ( str.equals("3x3") )
                {
                    mode.bits = CorrelationBit.from_int(CorrelationBit._BITS_3x3);
                }
                else if ( str.equals("4x4") )
                {
                    mode.bits = CorrelationBit.from_int(CorrelationBit._BITS_4x4);
                }
                else
                {
                    throw new AcsJInvalidFormatEx("invalid bits field " + str);
                }

                //
                // over sampled
                //
                str = modeStr[i++];
                if ( str.equals("true") )
                {
                    mode.overSampled = true;
                }
                else if ( str.equals("false") )
                {
                    mode.overSampled = false;
                }
                else
                {
                    throw new AcsJInvalidFormatEx("invalid over-sampled field " + str);
                }
            
                //
                // polarization product
                //
                str = modeStr[i++];
                if ( str.equals("X-X") )
                {
                    mode.polarizationProducts = ePolarization.POLZ_SINGLE_X;
                }
                else if ( str.equals("Y-Y") )
                {
                    mode.polarizationProducts = ePolarization.POLZ_SINGLE_Y;
                }
                else if ( str.equals("DOUBLE") )
                {
                    mode.polarizationProducts = ePolarization.POLZ_DOUBLE;
                }
                else if ( str.equals("FULL") )
                {
                    mode.polarizationProducts = ePolarization.POLZ_FULL;
                }
                else
                {
                    throw new AcsJInvalidFormatEx("invalid polarizations field " + str);
                }
            
                //
                // filter mode
                //
                str = modeStr[i++];
                if ( str.equals("TDM") )
                {
                    mode.filterMode = eFilterMode.FILTER_TDM;
                }
                else if ( str.equals("FDM") )
                {
                    mode.filterMode = eFilterMode.FILTER_FDM;
                }
                else
                {
                    throw new AcsJInvalidFormatEx("invalid filter mode field " + str);
                }
            }
            catch ( NumberFormatException nfe )
            {
                throw new AcsJInvalidFormatEx("some numeric field contains a non numeric value");
            }
            catch ( AcsJInvalidFormatEx ex )
            {
                throw ex;
            }

            //
            // ingest the mode into the table
            //
            if ( m_table.put(modeKey, mode) != null )
            {
                throw new AcsJInvalidFormatEx("table record duplication");
            }

            //
            // read next line
            //
            try
            {
                stringRead = br.readLine();                
            }
            catch ( IOException ioex )
            {
                throw new AcsJAccessFileEx("IO exception when reading line");
            }
        } /* while stringRead != null */

        //
        // loop through the modes in the file skimming those that do not
        // apply for the current correlator mode.
        //
        for ( Iterator pair = m_table.entrySet().iterator(); pair.hasNext(); )
        {
            Map.Entry e = (Map.Entry)pair.next();
            ConfigMode mode = (ConfigMode)e.getValue();
            int key = ((Integer)e.getKey()).intValue();

            //
            // get mode structure to be used below
            //
            try
            {
                mode = getMode(key);
            }
            catch ( Throwable th )
            {
                throw new AcsJInvalidFormatEx("cannot get already ingested mode " + key);
            }

            //
            // get fractional mode to be use below for removing other than
            // 100% modes from certain configuration types.
            //
            eFraction frac;
            try
            {
                frac = getFraction(key);
            }
            catch ( Throwable th )
            {
                throw new AcsJInvalidFormatEx("cannot get fraction for key " + key);
            }
            
            //
            // the 2ANTS system supports only 2x2, no oversampling and
            // no fractional correlator usage.
            //
            if ( m_type.equals(CORRELATOR_12m_2ANTS.value) 
                 &&
                 (mode.bits != CorrelationBit.BITS_2x2 ||
                  mode.overSampled                     ||
                  frac.value() != eFraction._FRACTION_FULL) )
            {
                pair.remove();
            }
            //
            // Note: for the time being remove fractional modes also from the
            // first quadrant and two quadrants configurations. There is no
            // support yet for spectral windows capable of doing so.
            //
            else if ( (m_type.equals(CORRELATOR_12m_1QUADRANT.value) ||
                       m_type.equals(CORRELATOR_12m_2QUADRANT.value))
                      &&
                      (mode.bits != CorrelationBit.BITS_2x2 ||
                       frac.value() != eFraction._FRACTION_FULL) )
            {
                pair.remove();
            }
        }
        
        //
        // close the file
        //
        try
        {
            br.close();
        }
        catch ( IOException ioex )
        {
            throw new AcsJAccessFileEx("IO exception when closing file");
        }

        //
        // if the parsing of the file ended up with an empty table
        // then complain
        //
        if ( m_table.size() == 0 )
        {
            throw new AcsJEmptyTableEx("parsed an empty table");
        }
    }
} // CorrConfigModeClass

/*___oOo___*/

    
