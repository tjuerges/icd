/** 
    ALMA - Atacama Large Millimiter Array
    * (c) Associated Universities Inc., 2006 
    * 
    * This library is free software; you can redistribute it and/or
    * modify it under the terms of the GNU Lesser General Public
    * License as published by the Free Software Foundation; either
    * version 2.1 of the License, or (at your option) any later version.
    * 
    * This library is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    * Lesser General Public License for more details.
    * 
    * You should have received a copy of the GNU Lesser General Public
    * License along with this library; if not, write to the Free Software
    * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
    * @author  dguo
    * $Id$
    */
package alma.Correlator.ConfigurationValidatorImpl;

import alma.correlatorSrc.CorrConfigValidator.CorrConfigValidator;
import alma.correlatorSrc.CorrConfigValidator.SBConversionException;
import java.util.logging.Logger;
import java.util.Vector;

import alma.ACS.ComponentStates;
import alma.ACS.stringSeqHolder;
import alma.ACS.stringSeqHelper;

import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;

import alma.Correlator.ConfigurationValidatorOperations;
import alma.Correlator.CorrelatorConfiguration;

import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.acs.entityutil.EntityDeserializer;

import alma.CorrConfigModeErr.wrappers.*;

/**
 * A component that implement ConfigValidator interface for providing 
 * Correlator Configuration Validation
 */
//public class ConfigValidatorImpl extends ComponentImplBase
public class ConfigurationValidatorImpl implements ComponentLifecycle, ConfigurationValidatorOperations
{
    private ContainerServices m_containerServices;
    private Logger m_logger;
    private EntityDeserializer entityDeserializer;
    private CorrConfigValidator m_validator;
    
    /////////////////////////////////////////////////////////////
    // Implementation of ComponentLifecycle
    /////////////////////////////////////////////////////////////
    
    //---------------------------------------------------------------------------------
    public void initialize(ContainerServices containerServices) 
    {
        m_containerServices = containerServices;
        m_logger = m_containerServices.getLogger();
        entityDeserializer = EntityDeserializer.getEntityDeserializer(m_logger); 
    }
    
    //---------------------------------------------------------------------------------
    public void execute()
        throws ComponentLifecycleException
    {
        //
        // instantiate the configuration validator class to be used all
        // along our life cycle
        //
         try
         {
             m_validator = new CorrConfigValidator(m_containerServices.getCDB());
         }
         catch ( AcsJConstructorFailureEx ex )
         {
             throw new ComponentLifecycleException("failed to create validator object",
                                             ex); 
         }
         catch ( Throwable thr )
         {
             throw new ComponentLifecycleException("failed to get CDB", thr);
         }
    }
    
    //---------------------------------------------------------------------------------
    public void cleanUp()
    {
    }
    
    //---------------------------------------------------------------------------------
    public void aboutToAbort() 
    {
        cleanUp();
    }
    
    /////////////////////////////////////////////////////////////
    // Implementation of ACSComponent
    /////////////////////////////////////////////////////////////
    
    //---------------------------------------------------------------------------------
    public ComponentStates componentState() 
    {
        return m_containerServices.getComponentStateManager().getCurrentState();
    }
 
    //---------------------------------------------------------------------------------
    public String name() 
    {
        return m_containerServices.getName();
    }
    
    /////////////////////////////////////////////////////////////
    // Implementation of Operations
    /////////////////////////////////////////////////////////////
    
    //---------------------------------------------------------------------------------
    public CorrelatorConfiguration translateSpectralSpecXml2CorrConfigIdl(String spectralSpecXml)
    {
         CorrelatorConfiguration corrConfig = new CorrelatorConfiguration();
         try
         {
	     corrConfig =  m_validator.translateSpectralSpecXml2CorrConfigIdl(spectralSpecXml);
	     return corrConfig;
         }
         catch ( Throwable thr )
         {
	     m_logger.severe("Exception caught: "+thr.toString());
	     return null;
         }
    }

    //---------------------------------------------------------------------------------
    public boolean validateConfiguration(CorrelatorConfiguration corrConfigIDL, 
				int numOfAntennas, alma.ACS.stringSeqHolder errorMsgSeq)
    {
        boolean isValid = m_validator.validateConfiguration(corrConfigIDL,
                                                            numOfAntennas,
                                                            errorMsgSeq );
        return isValid;
    }  

    //---------------------------------------------------------------------------------
    public boolean validateConfigurationSS( String spectralSpec,
                                           int numOfAntennas,
                                           alma.ACS.stringSeqHolder errorMsgSeq)
    {
        boolean isValid = m_validator.validateConfigurationSS(spectralSpec,
                                                            numOfAntennas,
                                                            errorMsgSeq );
        return isValid;
    }
}
