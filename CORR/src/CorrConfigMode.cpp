/* @(#) $Id$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

//
// System stuff
//
#include <math.h>
#include <fstream>

//
// ICD stuff
//
#include <CCorrelationBit.h>
#include <CStokesParameter.h>
#include <CorrConfigC.h>

//
// Local stuff
//
#include "CorrConfigMode.h"
#include "getGeneratedTable.cpp"

using namespace std;

//---------------------------------------------------------------------------------
CorrConfigMode::CorrConfigMode(CDB::DAL_ptr dal_p)
{
    //
    // populate the set of know correlator types
    //
    setKnownValues();
 
    //
    // read correlator type from TMCDB, this makes possible to be sure
    // that the running software is in synch with what this class provides
    // as valid modes
    //
    try
    {
        readType(dal_p);
    }
    catch ( CorrConfigModeErr::ReadTypeExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( ... )
    {
        CorrConfigModeErr::ConstructorFailureExImpl ex =
            CorrConfigModeErr::ConstructorFailureExImpl(__FILE__,
                                                        __LINE__,
                                                        __FUNCTION__);

        ex.setReason("readType thrown unknown expection");

        throw ex;
    }

    //
    // if the retreived type does not match then bail out
    //
    if ( m_knownTypes.find(m_type.c_str()) == m_knownTypes.end() )
    {
        CorrConfigModeErr::InvalidTypeExImpl _ex =
            CorrConfigModeErr::InvalidTypeExImpl(__FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);

        _ex.setTypeName(m_type.c_str());

        CorrConfigModeErr::ConstructorFailureExImpl ex =
            CorrConfigModeErr::ConstructorFailureExImpl(_ex,
                                                        __FILE__,
                                                        __LINE__,
                                                        __FUNCTION__);
        
        ex.setReason("cdb reports invalid correlator type");

        throw ex;
    }

    //
    // read all modes table from file and build current table based on
    // current type
    //
    try
    {
        generateModesTable();
    }
    catch ( CorrConfigModeErr::AccessFileExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( CorrConfigModeErr::InvalidFormatExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( CorrConfigModeErr::EmptyTableExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( ... )
    {
        m_table.clear();

        CorrConfigModeErr::ConstructorFailureExImpl ex =
            CorrConfigModeErr::ConstructorFailureExImpl(__FILE__,
                                                        __LINE__,
                                                        __FUNCTION__);

        ex.setReason("readAllModesTable thrown unknown expection");

        throw ex;
    }
}

//---------------------------------------------------------------------------------
CorrConfigMode::CorrConfigMode(const char *corrType)
{
    //
    // populate the set of know correlator types
    //
    setKnownValues();

    //
    // if the received type does not match then bail out
    //
    if ( m_knownTypes.find(corrType) == m_knownTypes.end() )
    {
        CorrConfigModeErr::InvalidTypeExImpl _ex =
            CorrConfigModeErr::InvalidTypeExImpl(__FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);

        _ex.setTypeName(corrType);

        CorrConfigModeErr::ConstructorFailureExImpl ex =
            CorrConfigModeErr::ConstructorFailureExImpl(__FILE__,
                                                        __LINE__,
                                                        __FUNCTION__);

        ex.setReason("constructor received unknow correlator type");

        throw ex;
    }

    //
    // copy received type into our variable
    //
    m_type = corrType;

    //
    // read all modes table from file and build current table based on
    // current type
    //
    try
    {
        generateModesTable();
    }
    catch ( CorrConfigModeErr::AccessFileExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);

    }
    catch ( CorrConfigModeErr::InvalidFormatExImpl &ex)
    {
        throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( CorrConfigModeErr::EmptyTableExImpl &ex)
    {
         throw CorrConfigModeErr::ConstructorFailureExImpl(ex,
                                                          __FILE__,
                                                          __LINE__,
                                                          __FUNCTION__);
    }
    catch ( ... )
    {
        m_table.clear();

        CorrConfigModeErr::ConstructorFailureExImpl ex =
            CorrConfigModeErr::ConstructorFailureExImpl(__FILE__,
                                                        __LINE__,
                                                        __FUNCTION__);

        ex.setReason("buildTable thrown unknown expection");

        throw ex;
    }
}

//---------------------------------------------------------------------------------
CorrConfigMode::~CorrConfigMode()
{
    m_knownTypes.clear();
    m_type.clear();
    m_table.clear();
}

//---------------------------------------------------------------------------------
void CorrConfigMode::readType(CDB::DAL_ptr dal_p)
{
    //
    // normally we should access the TMCDB, but for now let's read a 
    // string filed under correlator master component
    //

    try
    {
        //
        // get cbd object to master record
        //
        CDB::DAO_ptr dao_p = dal_p->get_DAO_Servant("alma/CORR_MASTER_COMP");
        
        //
        // access field
        //
        ACE_CString value = dao_p->get_string("corrType");

        //
        // copy string to our memory
        //
        m_type = string(value.c_str());
    }
    catch ( ... )
    {
        CorrConfigModeErr::ReadTypeExImpl ex =
            CorrConfigModeErr::ReadTypeExImpl(__FILE__, __LINE__, __FUNCTION__);

        ex.setPath("CORR_MASTER_COMP");
        ex.setField("corrType");

        throw ex;
    }
}

//---------------------------------------------------------------------------------
int CorrConfigMode::findModeFromNominal(float nominalBandwidthMHz,
                                        long  nominalChannels,
                                        CorrelationBitMod::CorrelationBit bits,
                                        bool  overSampled,
                                        Correlator::StokesParameterSeq polarizationProducts)
{
    Correlator::ePolarization polz;

    //
    // decode the polarization Stokes sequence into our workable enum
    //
    try
    {
        polz = decodePolzSeq(polarizationProducts);
    }
    catch ( CorrConfigModeErr::InvalidStokesParameterExImpl &_ex)
    {
        CorrConfigModeErr::InvalidModeExImpl ex =
            CorrConfigModeErr::InvalidModeExImpl(_ex,
                                                 __FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);
        
        ex.setBandwidthMHz(nominalBandwidthMHz);
        ex.setChannels(nominalChannels);
        ex.setBits(CCorrelationBit::toString(bits).c_str());
        ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
        ex.setPolarizationProducts("invalid sequence");

        throw ex;
    }

    //
    // the procedure works by brute force, which should not be a big issue
    // given that the table is not that big
    //
    for ( map<int, Correlator::ConfigMode>::const_iterator m = m_table.begin();
          m != m_table.end(); 
          m++ )
    {
        if ( m->second.bandWidth            == nominalBandwidthMHz      &&
             m->second.channels             == nominalChannels          &&
             m->second.bits                 == bits                     &&
             m->second.overSampled          == overSampled              &&
             m->second.polarizationProducts == polz )
        {
            return m->first;
        }
    }
    
    //
    // if we reached here is because the params are not mappable to any valid mode
    //
    CorrConfigModeErr::InvalidModeExImpl ex =
        CorrConfigModeErr::InvalidModeExImpl(__FILE__, __LINE__, __FUNCTION__);
    
    ex.setBandwidthMHz(nominalBandwidthMHz);
    ex.setChannels(nominalChannels);
    ex.setBits(CCorrelationBit::toString(bits).c_str());
    ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
    ex.setPolarizationProducts(enumAsString(polz).c_str());

    throw ex;
}

//---------------------------------------------------------------------------------
int CorrConfigMode::findModeFromEffective(float effectiveBandwidthMHz,
                                          long  effectiveChannels,
                                          CorrelationBitMod::CorrelationBit bits,
                                          bool  overSampled,
                                          Correlator::StokesParameterSeq polarizationProducts)
{
    int n = -1;
    int o = -1;
    int w = -1;
    Correlator::ePolarization polz;

    //
    // decode the polarization Stokes sequence into our workable enum
    //
    try
    {
        polz = decodePolzSeq(polarizationProducts);
    }
    catch ( CorrConfigModeErr::InvalidStokesParameterExImpl &_ex)
    {
        CorrConfigModeErr::InvalidModeExImpl ex =
            CorrConfigModeErr::InvalidModeExImpl(_ex,
                                                 __FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);
        
        ex.setBandwidthMHz(effectiveBandwidthMHz);
        ex.setChannels(effectiveChannels);
        ex.setBits(CCorrelationBit::toString(bits).c_str());
        ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
        ex.setPolarizationProducts("invalid sequence");

        throw ex;
    }

    //
    // first we need to infer the nominal values:
    //
    //          2^(n-1) < effectiveChannels <= 2^n
    //
    // where 2^n is the nominal value. Now, the actual dropout is restricted
    // to a set of fractions that effectively reduce the number of channels and
    // bandwith in the following way:
    //
    //          E = N * (1 - f)
    //
    // where 'f' is the dropout fraction. If we express f as 2^o then the
    // afore mentioned resstriction is such that 'o' must be an item of
    // the following set:
    //
    //          [8, 16, 32, 64]
    //
    // that is, one eigth or one sixteenth or 1 over 32 or 1 over 64.
    // For checking for this condition it is convenient to elaborate the
    // effective expression as it follows:
    //
    //          E = N * (1 - f)
    //          E = N * (1 - 1 / 2^o)
    //          N - E = N - N * (1 - 1 / 2^o)
    //          N - E = N * (1 - (1 - 1 / 2^o))
    //          N - E = N * (1 / 2^o)
    //          2^n - E = 2^n / 2^o
    //          2^n - E = 2^(n-o)
    //
    // Therefore, the validation consist on verifying which 'o' value from
    // the set shown above makes possible a nominal minus effective difference
    // as shown in the last expression. The good news here is that there
    // is no floating point arithmetic, which is desirable just in case some
    // rounding could start playing games.
    //
    // On the other hand, the bandwith accepts a similar procedure. The expected
    // nominal bandwidth W is such that
    //
    //          W = 31.25 * (2 ^ w)
    //
    // where 'w' is an integer in the set [0, 1, 2, 3, 4, 5, 6]. This means
    // that the nominal bandwidth for this mode is defined by the 'w' integer
    // that satifies:
    //
    //          E = 31.25 * (2^w - 2^(w-o))
    //
    // NOTE: all this expedient would work only under the assumtion that
    // the dropout fraction is small enough as not to confuse the 'n' 
    // computation with a number that is 2 times smaller than the real
    // one we are looking for. This assumption is enforced by restricting
    // the valid values of 'o' to the [8, 16, 32, 64] set. As to visualize
    // this issue let's have the following table of possible channels:
    //
    //  channels/o      8       16      32      64
    //  8192            7168    7680
    //  4096            3584
    //  2048
    //  1024
    //  512
    //  256
    //

    //
    // if this is TDM then we can return immediately, note that in TDM
    // nominal is always effective.
    //
    if ( (effectiveChannels == 256            &&
          bits == CorrelationBitMod::BITS_2x2 &&
          !overSampled                        &&
          (polz == Correlator::POLZ_SINGLE_X ||
           polz == Correlator::POLZ_SINGLE_Y)) 
         ||
         (effectiveChannels == 64 || effectiveChannels == 128) )
    {
        //
        // make sure that the number of channels really makes sense for TDM
        //
        if ( effectiveChannels != 256 &&
             effectiveChannels != 128 &&
             effectiveChannels != 64 )
        {
            CorrConfigModeErr::InvalidModeExImpl ex =
                CorrConfigModeErr::InvalidModeExImpl(__FILE__,
                                                     __LINE__,
                                                     __FUNCTION__);
            
            ex.setBandwidthMHz(effectiveBandwidthMHz);
            ex.setChannels(effectiveChannels);
            ex.setBits(CCorrelationBit::toString(bits).c_str());
            ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
            ex.setPolarizationProducts(enumAsString(polz).c_str());
            
            throw ex;
        }

        //
        // everything looks in place for TDM
        //
        return findModeFromNominal(effectiveBandwidthMHz,
                                   effectiveChannels,
                                   bits,
                                   overSampled,
                                   polarizationProducts);
    }

    //
    // infer 'n' as explained above
    //
    for ( unsigned int i = 8; i <= 13; i++ )
    {
        if ( effectiveChannels <= (0x1 << i) )
        {
            n = i;

            break;
        }
    }

    //
    // validate 'n'
    //
    if ( n == -1 )
    {
        CorrConfigModeErr::InvalidModeExImpl ex =
            CorrConfigModeErr::InvalidModeExImpl(__FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);
        
        ex.setBandwidthMHz(effectiveBandwidthMHz);
        ex.setChannels(effectiveChannels);
        ex.setBits(CCorrelationBit::toString(bits).c_str());
        ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
        ex.setPolarizationProducts(enumAsString(polz).c_str());
        
        throw ex;
    }

    //
    // if nominal equals effective then return now
    //
    if ( (0x1 << n) == effectiveChannels )
    {
        return findModeFromNominal(effectiveBandwidthMHz,
                                   effectiveChannels,
                                   bits,
                                   overSampled,
                                   polarizationProducts);
    }

    //
    // infer 'o' as explained before
    //
    for ( unsigned int i = 3; i <= 6; i++ )
    {
        if ( (0x1 << n) - effectiveChannels == (0x1 << (n - i)) )
        {
            o = i;
        }
    }

    //
    // validate 'o'
    //
    if ( o == -1 || (0x1 << o) != int(1.0 / CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) )
    {
        CorrConfigModeErr::InvalidModeExImpl ex =
            CorrConfigModeErr::InvalidModeExImpl(__FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);
        
        ex.setBandwidthMHz(effectiveBandwidthMHz);
        ex.setChannels(effectiveChannels);
        ex.setBits(CCorrelationBit::toString(bits).c_str());
        ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
        ex.setPolarizationProducts(enumAsString(polz).c_str());
        
        throw ex;
    }

    //
    // infer 'w'
    //
    for ( unsigned int i = 0; i < m_knownNominalBandwidths.size(); i++ )
    {
        if ( effectiveBandwidthMHz == m_knownNominalBandwidths[i] * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) )
        {
            w = i;
        }
    }

    //
    // validate 'w'
    //
    if ( w == -1 )
    {
        CorrConfigModeErr::InvalidModeExImpl ex =
            CorrConfigModeErr::InvalidModeExImpl(__FILE__,
                                                 __LINE__,
                                                 __FUNCTION__);
        
        ex.setBandwidthMHz(effectiveBandwidthMHz);
        ex.setChannels(effectiveChannels);
        ex.setBits(CCorrelationBit::toString(bits).c_str());
        ex.setOverSampled(overSampled ? "2Nyquist" : "Nyquist");
        ex.setPolarizationProducts(enumAsString(polz).c_str());
        
        throw ex;
    }

    return findModeFromNominal(CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW * (float)(0x1 << w),
                               effectiveChannels + (0x1 << (n - o)),
                               bits,
                               overSampled,
                               polarizationProducts);
}

//---------------------------------------------------------------------------------
const Correlator::ConfigMode &CorrConfigMode::operator[](const int &key)
{
    //
    // key not in the table
    //
    if ( m_table.find(key) == m_table.end() )
    {
        CorrConfigModeErr::InvalidKeyExImpl ex =
            CorrConfigModeErr::InvalidKeyExImpl(__FILE__, __LINE__, __FUNCTION__);

        ex.setKey(key);

        throw ex;
    }

    return m_table[key];
}

//----------------------------------------------------------------------------------
vector<int> CorrConfigMode::getKeys()
{
    vector<int> v;

    for ( map<int, Correlator::ConfigMode>::const_iterator m = m_table.begin();
          m != m_table.end(); 
          m++ )
    {
        v.push_back(m->first);
    }

    return v;
}

//----------------------------------------------------------------------------------
string CorrConfigMode::asString()
{
    stringstream s;

    s << "# " << m_type << endl; 
    s << "# size " << m_table.size() << endl;
    s << "# mode\tbw\tchan\tbits\t\t2nyq.\tpolz\tfilterMode" << endl;

    //
    // floating point format (band-width)
    //
    s.setf(ios_base::fixed);
    s.setf(ios_base::showpoint);

    for ( map<int, Correlator::ConfigMode>::const_iterator m = m_table.begin();
          m != m_table.end(); 
          m++ )
    {
        //
        // enough! This permits numbers like 2000.0, 62.5 and 31.25 to be 
        // printed exactly like that!
        //
        if ( round(10 * m->second.bandWidth) == 10 * m->second.bandWidth )
        {
            s.precision(1);
        }
        else
        {
            s.precision(2);
        }

        s << "  " <<
            m->first << "\t" <<
            m->second.bandWidth << "\t" << 
            m->second.channels << "\t" << 
            CCorrelationBit::toString(m->second.bits) << "\t" << 
            ((m->second.overSampled) ? "true" : "false") << "\t" <<
            enumAsString(m->second.polarizationProducts) << "\t" << 
            enumAsString(m->second.filterMode) << endl;
    }

    return s.str();
}

//---------------------------------------------------------------------------------
Correlator::eFraction CorrConfigMode::getFraction(int key)
{
    Correlator::ConfigMode m;
    
    //
    // get mode from table
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }
    
    //
    // multiple resolution modes are implemented only for a subset of
    // all possible modes
    //
    if ( m.filterMode == Correlator::FILTER_TDM ||
         m.bits != CorrelationBitMod::BITS_2x2 )
    {
        return Correlator::FRACTION_FULL;
    }

    //
    // in full mode this is the number of total channels. Declared as
    // unsigned for simplified operations.
    //
    int fullModeChannels;

    //
    // compute full-mode number of channels
    //
    switch ( m.polarizationProducts )
    {
    case Correlator::POLZ_SINGLE_X:
    case Correlator::POLZ_SINGLE_Y:
        fullModeChannels = 8192;
        break;
    case Correlator::POLZ_DOUBLE:
        fullModeChannels = 4096;
        break;
    case Correlator::POLZ_FULL:
        fullModeChannels = 2048;
        break;
    default:
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("unexpected polarization for a multi-resolution mode");

        throw ex;
    }
    
    //
    // compare current mode of channels with full-mode, the quotient
    // is all what it takes to say the fraction
    //
    if ( m.channels == fullModeChannels )
    {
        return Correlator::FRACTION_FULL;
    }
    else if ( (m.channels << 1) == fullModeChannels )
    {
        return Correlator::FRACTION_ONE_HALF;
    }
    else if ( (m.channels << 2) == fullModeChannels )
    {
        return Correlator::FRACTION_ONE_FOURTH;
    }
    else if ( (m.channels << 3) == fullModeChannels )
    {
        return Correlator::FRACTION_ONE_EIGHTH;
    }
    else
    {
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("unexpected number of channels for a multi-resolution mode");

        throw ex;
    }
}

//---------------------------------------------------------------------------------
int CorrConfigMode::getNumberSubBands(int key)
{
    Correlator::ConfigMode m;

    //
    // get the mode from the table
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // TFB sub-bands are meaningful only for FDM modes
    //
    if ( m.filterMode != Correlator::FILTER_FDM )
    {
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("this mode does not involve any TFB sub-band");

        throw ex;
    }

    //
    // decoding of the table, hardcoded and based on the band-width
    //
    if ( m.bandWidth == 2000.0 )
    {
        return 32;
    }
    else if ( m.bandWidth == 1000.0 )
    {
        return 16;
    }
    else if ( m.bandWidth == 500.0 )
    {
        return 8;
    }
    else if ( m.bandWidth == 250.0 )
    {
        return 4;
    }
    else if ( m.bandWidth == 125.0 )
    {
        return 2;
    }
    else if ( m.bandWidth == 62.5 || m.bandWidth == 31.25 )
    {
        return 1;
    }
    else
    {
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("unexpected band-width");

        throw ex;
    }
}   

//---------------------------------------------------------------------------------
int CorrConfigMode::getNumberPolzProducts(int key)
{
    Correlator::ConfigMode m;

    //
    // get the mode from the table
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // from the enum value compute the number of involved products
    //
    if ( m.polarizationProducts == Correlator::POLZ_SINGLE_X ||
         m.polarizationProducts == Correlator::POLZ_SINGLE_Y )
    {
        return 1;
    }
    else if ( m.polarizationProducts == Correlator::POLZ_DOUBLE )
    {
        return 2;
    }

    return 4;
}

//----------------------------------------------------------------------------------
Control::PolarizationTypeSeqSeq CorrConfigMode::getPolzProductTypes(int key)
{
    Correlator::ConfigMode m;
    Control::PolarizationTypeSeq pair;
    Control::PolarizationTypeSeqSeq ret;

    pair.length(2);

    //
    // get the mode from the table
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // from the encoded polarization product type build the output
    // sequence of pairs.
    //
    switch ( m.polarizationProducts )
    {
    case Correlator::POLZ_SINGLE_X:
        ret.length(1);

        pair[0] = pair[1] = PolarizationTypeMod::X;
        ret[0] = pair;

        break;
    case Correlator::POLZ_SINGLE_Y:
        ret.length(1);

        pair[0] = pair[1] = PolarizationTypeMod::Y;
        ret[0] = pair;

        break;
    case Correlator::POLZ_DOUBLE:
        ret.length(2);

        pair[0] = pair[1] = PolarizationTypeMod::X;
        ret[0] = pair;

        pair[0] = pair[1] = PolarizationTypeMod::Y;
        ret[1] = pair;

        break;
    case Correlator::POLZ_FULL:
        ret.length(4);

        pair[0] = pair[1] = PolarizationTypeMod::X;
        ret[0] = pair;

        pair[0] = PolarizationTypeMod::X;
        pair[1] = PolarizationTypeMod::Y;
        ret[1] = pair;

        pair[0] = PolarizationTypeMod::Y;
        pair[1] = PolarizationTypeMod::X;
        ret[2] = pair;

        pair[0] = pair[1] = PolarizationTypeMod::Y;
        ret[3] = pair;

        break;        
    }

    return ret;
}

//----------------------------------------------------------------------------------
float CorrConfigMode::getVs(int key, ACS::TimeInterval dumpDuration)
{
    Correlator::ConfigMode m;
    int factor;
    float vs;

    //
    // get the mode
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // this is the base Vs, common to all modes, the rest consist in 
    // finding out the factor by which this base value must be scaled.
    // This is one of those place that will require some 'tuning' at the
    // moment the ACS units of time is changed. Vs is calculated differently
    // for auto-only modes with 1, 2, 4, or 8ms dump durations vs. cross & auto
    // modes of 16ms intervals. 
    //
    vs = CORR_CONFIG_MODE_Vs_CONST * dumpDuration;
    if ( dumpDuration >= 160000 )
    {
        vs /= 16.0;
    }

    //
    // the factor depends on the filter mode, the number of bits, over-sampling
    // flag and band-width
    //
    if ( m.filterMode == Correlator::FILTER_TDM )
    {
        if ( m.bits == CorrelationBitMod::BITS_3x3 )
        {
            //
            // this will probably change, ask Alejandro.
            //
            factor = 800;
        }
        else
        {
            factor = 32;
        }
    }
    // FDM
    else
    {
        if ( m.bits == CorrelationBitMod::BITS_2x2 )
        {
            if ( m.overSampled )
            {
                if ( m.bandWidth == 31.25 )
                {
                    // TFB - 2x2 - 2Nyq - 31.25MHz 
                    factor = 1;
                }
                else
                {
                    // TFB - 2x2 - 2Nyq - !=31.25MHz
                    factor = 2;
                }
            }
            else
            {
                // TFB - 2x2 - Nyq - any band-width
                factor = 1;
            }
        }
        // TFB 4x4
        else
        {
            if ( m.overSampled )
            {
                if ( m.bandWidth == 31.25 )
                {
                    // TFB - 4x4 - 2Nyq - 31.25MHz
                    factor = 25;
                }
                else
                {
                    // TFB - 4x4 - 2Nyq - !=31.25MHz
                    factor = 50;
                }
            }
            else
            {
                // TFB - 4x4 - Nyq - any band-width
                factor = 25;
            }
        }
    }

    return vs * (float)factor; 
}

//---------------------------------------------------------------------------------
vector<float> CorrConfigMode::getSubBandsFc(int key, float fc)
{
    Correlator::ConfigMode m;
    int Ns;
    vector<float> freqs;
    float dummy, hebw;

    //
    // fc must be an integer multiple of the DDS step
    //
    if ( modff(fc / CORR_CONFIG_MODE_TFB_DDS_STEP, &dummy) != 0.0 )
    {
        CorrConfigModeErr::InvalidFcExImpl ex =
            CorrConfigModeErr::InvalidFcExImpl(__FILE__, __LINE__, __FUNCTION__);
        
        ex.setFc(fc);

        throw ex;        
    }

    //
    // get the mode
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // if this is not a FDM mode then complain
    //
    if ( m.filterMode != Correlator::FILTER_FDM )
    {
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("not an FDM mode for frequency center computation");

        throw ex;
    }

    //
    // get number of TFB sub-bands
    //
    try
    {
        Ns = getNumberSubBands(key);
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }
    catch ( CorrConfigModeErr::ModeInconsistencyExImpl &ex )
    {
        throw ex;
    }

    //
    // get the effective bandwidth and divide it by 2, which is the actual
    // value required afterwards
    //
    try
    {
        hebw = getEffectiveBandWidth(key) / 2.0;
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }
    catch ( CorrConfigModeErr::ModeInconsistencyExImpl &ex )
    {
        throw ex;
    }

    //
    // if received fc is incompatible with the effective bandwidth
    // then complain.
    //
    if ( fc - hebw < 0.0 || fc +  hebw > 2000.0 )
    {
        CorrConfigModeErr::FcOutOfRangeExImpl ex =
            CorrConfigModeErr::FcOutOfRangeExImpl(__FILE__,
                                                  __LINE__,
                                                  __FUNCTION__);
        
        ex.setKey(key);
        ex.setFc(fc);
        ex.setEffectiveBandwidth(2.0 * hebw);

        throw ex;
    }

    //
    // resize output parameter accordingly
    //
    freqs.resize(Ns);

    //
    // the algorithm. Here the band width amount dropped away is hardcoded
    // to one sixteenth. ABaudry indicates this is the right 'rule' to apply,
    // any change or intention to make this 'rule' an actual parameter
    // would require the needed changes.
    // Note: it works for Ns=1 as well.
    //
    for ( int i = 0; i < Ns; i++ )
    {
        freqs[i] = fc + CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION) * (2 * i + 1 - Ns);
    }

    return freqs;
}

//---------------------------------------------------------------------------------
float CorrConfigMode::getEffectiveBandWidth(int key)
{
    Correlator::ConfigMode m;

    //
    // get the mode
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // if this is not an FDM mode then return the nominal band width
    //
    if ( m.filterMode != Correlator::FILTER_FDM )
    {
        return m.bandWidth;
    }

    return m.bandWidth * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION);
}

//---------------------------------------------------------------------------------
int CorrConfigMode::getEffectiveChannels(int key)
{
    Correlator::ConfigMode m;
    float dummy;

    //
    // get the mode
    //
    try
    {
        m = (*this)[key];
    }
    catch ( CorrConfigModeErr::InvalidKeyExImpl &ex )
    {
        throw ex;
    }

    //
    // if this is not an FDM mode then return the nominal channels
    //
    if ( m.filterMode != Correlator::FILTER_FDM )
    {
        return m.channels;
    }

    //
    // if the nominal band width is not divisible by the overlapping fraction
    // then something is out of whack
    //
    if ( modff(m.channels * CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION, &dummy) != 0.0 )
    {
        CorrConfigModeErr::ModeInconsistencyExImpl ex =
            CorrConfigModeErr::ModeInconsistencyExImpl(__FILE__,
                                                       __LINE__,
                                                       __FUNCTION__);
        
        ex.setKey(key);
        ex.setDetail("nominal channels and overlapping fraction do not match");

        throw ex;
    }

    return int(m.channels * (1.0 - CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION));
}

//---------------------------------------------------------------------------------
void CorrConfigMode::setKnownValues()
{
    //
    // correlator configurations
    //
    m_knownTypes.insert(Correlator::CORRELATOR_12m_2ANTS);
    m_knownTypes.insert(Correlator::CORRELATOR_12m_1QUADRANT);
    m_knownTypes.insert(Correlator::CORRELATOR_12m_2QUADRANT);
    m_knownTypes.insert(Correlator::CORRELATOR_12m_4QUADRANT);

    //
    // bandwidths
    //
    m_knownNominalBandwidths.resize(7);
    for ( unsigned int i = 0; i < m_knownNominalBandwidths.size(); i++ )
    {
        m_knownNominalBandwidths[i] =
            (0x1 << i) * CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW;
    }

    //
    // channels
    //
    m_knownNominalChannels.resize(8);
    for ( unsigned int i = 0; i < m_knownNominalChannels.size(); i++ )
    {
        m_knownNominalChannels[i] = (0x1 << i) * 64;
    }
}

//---------------------------------------------------------------------------------
string CorrConfigMode::enumAsString(const Correlator::ePolarization &polz)
{
    if ( polz == Correlator::POLZ_SINGLE_X )
    {
        return string("X-X");
    }
    else if ( polz == Correlator::POLZ_SINGLE_Y )
    {
        return string("Y-Y");
    }
    else if ( polz == Correlator::POLZ_DOUBLE )
    {
        return string("DOUBLE");
    }

    return string("FULL");
}

//---------------------------------------------------------------------------------
string CorrConfigMode::enumAsString(const Correlator::eFilterMode &filter)
{
    if ( filter == Correlator::FILTER_FDM )
    {
        return string("FDM");
    }

    return string("TDM");
}

//---------------------------------------------------------------------------------
string CorrConfigMode::enumAsString(const Correlator::eFraction &fraction)
{
    if ( fraction == Correlator::FRACTION_FULL )
    {
        return string("FULL");
    }
    else if ( fraction == Correlator::FRACTION_ONE_HALF )
    {
        return string("ONE_HALF");
    }
    else if ( fraction == Correlator::FRACTION_ONE_FOURTH )
    {
        return string("ONE_FOURTH");
    }

    return string("ONE_EIGHTH");
}

//---------------------------------------------------------------------------------
Correlator::ePolarization CorrConfigMode::decodePolzSeq(const Correlator::StokesParameterSeq &polz)
{
    //
    // the length must be 1, 2 or 4
    //
    if ( polz.length() != 1 && polz.length() != 2 && polz.length() != 4 )
    {
        CorrConfigModeErr::InvalidStokesParameterExImpl ex =
            CorrConfigModeErr::InvalidStokesParameterExImpl(__FILE__,
                                                            __LINE__,
                                                            __FUNCTION__);

        ex.setLength(polz.length());
        ex.setDetail("invalid length");

        throw ex;
    }

    //
    // check the sequence is valid
    //
    for ( unsigned int i = 0; i < polz.length(); i++ )
    {
        //
        // all individual params must be XX or YY or XY or YX
        //
        if ( polz[i] != StokesParameterMod::XX
             &&
             polz[i] != StokesParameterMod::YY
             &&
             polz[i] != StokesParameterMod::XY
             &&
             polz[i] != StokesParameterMod::YX )
        {
            CorrConfigModeErr::InvalidStokesParameterExImpl ex =
                CorrConfigModeErr::InvalidStokesParameterExImpl(__FILE__,
                                                                __LINE__,
                                                                __FUNCTION__);

            ex.setLength(polz.length());
            ex.setDetail("at least one Stokes param is not in the valid set");

            throw ex;
        }

        //
        // parameters in the sequence must not repeat
        //
        for ( unsigned int j = i + 1; j < polz.length(); j++ )
        {
            if ( polz[i] == polz[j] )
            {
                CorrConfigModeErr::InvalidStokesParameterExImpl ex =
                    CorrConfigModeErr::InvalidStokesParameterExImpl(__FILE__,
                                                                    __LINE__,
                                                                    __FUNCTION__);
                
                ex.setLength(polz.length());
                ex.setDetail("at least two Stokes param repeat in the sequence");
                
                throw ex;
            }
        }
    }

    //
    // final decode
    //
    if ( polz.length() == 1 )
    {
        if ( polz[0] == StokesParameterMod::XX )
        {
            return Correlator::POLZ_SINGLE_X;
        }
        else
        {
            return Correlator::POLZ_SINGLE_Y;
        }
    }
    else if ( polz.length() == 2 )
    {
        return Correlator::POLZ_DOUBLE;
    }

    return Correlator::POLZ_FULL;
}

//---------------------------------------------------------------------------------
void CorrConfigMode::generateModesTable()
{
    //
    // make sure the table is empty before starting
    //
    m_table.clear();

    //
    // get generated table using generated function (see getGeneratedTable.cpp)
    //
    getGeneratedTable(m_table);

    //
    // loop through the modes in the file skimming those that do not
    // apply for the current correlator mode.
    //
    for ( map<int, Correlator::ConfigMode>::iterator m = m_table.begin();
          m != m_table.end(); )
    {
        //
        // the 2ANTS system supports only 2x2, no oversampling and
        // no fractional correlator usage.
        //
        if ( m_type == Correlator::CORRELATOR_12m_2ANTS
             &&
             (m->second.bits != CorrelationBitMod::BITS_2x2       ||
              m->second.overSampled                               ||
              getFraction(m->first) != Correlator::FRACTION_FULL) )
        {
            m_table.erase(m++);
                
            continue;
        }
        //
        // Note: for the time being remove fractional moves also from the
        // first quadrant and two quadrants configurations. There is no support
        // yet for spectral windows capable of doing so.
        //
        else if ( (m_type == Correlator::CORRELATOR_12m_1QUADRANT ||
                   m_type == Correlator::CORRELATOR_12m_2QUADRANT)
                  &&
                  (m->second.bits != CorrelationBitMod::BITS_2x2  ||
                   getFraction(m->first) != Correlator::FRACTION_FULL) )
        {
            m_table.erase(m++);
            
            continue;
        }

        //
        // increment the map iterator. We need to do this here instead of
        // in the for-loop statement itself due to the 'erase' usage above.
        //
        m++;
    }

    //
    // if the parsing of the file ended up with an empty table
    // then complain
    //
    if ( m_table.size() == 0 )
    {
        CorrConfigModeErr::EmptyTableExImpl ex =
            CorrConfigModeErr::EmptyTableExImpl(__FILE__, __LINE__, __FUNCTION__);
        
        throw ex;
    }
}

/*___oOo___*/
