#ifndef CorrConfigMode_H
#define CorrConfigMode_H
/*
 * @(#) $Id$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * ramestic  2007-05-01  created 
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

//
// System stuff
//
#include <set>
#include <map>
#include <vector>
#include <sstream>

//
// ACS stuff
//
#include <baciDB.h>
#include <acsutilFindFile.h>

//
// ICD stuff
//
#include <CorrConfigModeErr.h>
#include <CorrConfigModeC.h>
#include <ControlDataInterfacesC.h>

//---------------------------------------------------------------------------------
class CorrConfigMode
{
public:
    //
    // these constants hardcode the parameters used for computing TFB sub-band
    // frequency centers
    //
    static const float CORR_CONFIG_MODE_TFB_DDS_STEP = 0.030517578125;
    static const float CORR_CONFIG_MODE_TFB_SUBBAND_HALF_BW = 31.25;

    /** default constructor is not very useful.
     ** the default constructor initializes the table to empty.
     */
    CorrConfigMode()
    {
        m_type = std::string("");
        m_table.clear();
    }

    /** reads correlator type from cdb.
     ** this constructor accepts as parameter a DAL pointer, from which it will
     ** get a DAO reference to the attribute from where the correlator type is
     ** supposed to be available. Hardcoded to read from alma/CORR_MASTER_COMP
     ** and the atribute name is corrType.
     ** @param dal_p DAL reference
     **         throw ( CorrConfigModeErr::ConstructorFailureExImpl );
     */
    CorrConfigMode(CDB::DAL_ptr dal_p);
    
    /** use given correlator type.
     ** this constructor validates the received string agains the known set
     ** of correlator types and initializes the table accordingly.
     ** @param corrType intended correlator type name
     **         throw ( CorrConfigModeErr::ConstructorFailureExImpl );
     */
    CorrConfigMode(const char *corrType);

    /** copy constructor.
     */
    CorrConfigMode(const CorrConfigMode &source)
	{ *this = source; }
    
    /** destructor cleans the table.
     */
    virtual ~CorrConfigMode();

    /** gets a copy of the correlator type name.
     ** @return name of current correlator type
     */
    std::string getType() { return m_type; }

    /** gets size of the modes table for the current correlator type.
     ** @return number of modes in the table
     */
    int getSize() { return m_table.size(); }

    /** assignment operator.
     */
    CorrConfigMode &operator =(const CorrConfigMode &source)
    {
        m_knownTypes = source.m_knownTypes;
        m_knownNominalBandwidths = source.m_knownNominalBandwidths;
        m_knownNominalChannels = source.m_knownNominalChannels;
        m_type = source.m_type;
        m_table = source.m_table;

        return *this;
    }

    /** finds the associated mode key from a set of specific parameters.
     ** Provided the mode's parameters that are part of the correlator
     ** configuration IDL this method discovers the key associated to the
     ** mode that those parameters are addressing.
     ** This findMode version assumes that the bandwidth and channels are
     ** nominal values, that is, without any TFB overlapping for stitching.
     **         throw ( CorrConfigModeErr::InvalidModeExImpl );
     */
    int findModeFromNominal(float nominalBandwidthMHz,
                            long  nominalChannels,
                            CorrelationBitMod::CorrelationBit bits,
                            bool  overSampled,
                            Correlator::StokesParameterSeq polarizationProducts);

    /** finds the associated mode key from a set of specific parameters.
     ** Provided the mode's parameters that are part of the correlator
     ** configuration IDL this method discovers the key associated to the
     ** mode that those parameters are addressing.
     ** This findMode version assumes that the bandwidth and channels are
     ** effective values and checks that they do make sense based on the
     ** current TFB drop-off fraction.
     **         throw ( CorrConfigModeErr::InvalidModeExImpl );
     */
    int findModeFromEffective(float effectiveBandwidthMHz,
                              long  effectiveChannels,
                              CorrelationBitMod::CorrelationBit bits,
                              bool  overSampled,
                              Correlator::StokesParameterSeq polarizationProducts);
    
    /** bracket operator retrieves the corresponding entry of the modes table.
     ** The returned structure is defined in CorrConfigMode.idl.
     ** @return configuration mode structure
     **         throw ( CorrConfigModeErr::InvalidKeyExImpl );
     */
    const Correlator::ConfigMode &operator[](const int &key);

    /** it generates a vector of available keys.
     ** The purpose of this method is to provide an easy mapping between a
     ** zero based index (up to the size of the table minus one) an the 
     ** current modes, which is useful for some clients of this class
     ** @return an array of mode's keys
     */
    std::vector<int> getKeys();

    /** computes correlator resources fraction for a given mode's key.
     ** Multiple resolution modes are implemented in hardware such that
     ** the actual number of lags for any given mode is sacrified to
     ** a smaller value, freeing in the exercise hardware resources thus
     ** available for an additional concurrent spectral window. The reduction
     ** of lags happens such that the new value is either half, or one eighth,
     ** or one fourth of the full mode.
     ** @param key mode identifier.
     ** @return correlator fraction as defined by the Correlator::eFraction enum; 
     ** if the mode is not multi-resolution savvy then FULL is returned.
        throw ( CorrConfigModeErr::InvalidKeyExImpl,
                CorrConfigModeErr::ModeInconsistencyExImpl );
     */
    Correlator::eFraction getFraction(int key);

    /** computes number of TFB sub-bands for a given mode's key.
     ** If the mode in the key is not an actual TFB mode then an
     ** exception is thrown (ModeInconsistencyExImpl).
     ** @param key mode identifier.
     ** @return number of TFB sub-bands for the mode in the key.
        throw ( CorrConfigModeErr::InvalidKeyExImpl,
                CorrConfigModeErr::ModeInconsistencyExImpl);
     */
    int getNumberSubBands(int key);

    /** computes number of polarization products for a given mode's key.
     ** @param key mode identifier.
     ** @return number of polarization products
     **         throw ( CorrConfigModeErr::InvalidKeyExImpl );
     */
    int getNumberPolzProducts(int key);

    /** returns the polarization products as a sequence of polarization
     ** types pairs.
     ** A helper method for building a sequence of polarization types
     ** that encodes the same information as the sequence of Stokes
     ** parameters found in the correlator configuration structure.
     ** The mapping between the feasible polarization products and 
     ** the output of this method is like this:
     **
     **         single X = [[X, X]]
     **         ngle Y = [[Y, Y]]
     **         double = [[X, X], [Y, Y]]
     **         full = [[X, X], [X, Y], [Y, X], [Y, Y]]
     **
     ** @param key mode identifier.
     ** @return sequence of polarization types pairs.
     **         throw ( CorrConfigModeErr::InvalidKeyExImpl );
     */
    Control::PolarizationTypeSeqSeq getPolzProductTypes(int key);

    /** computes Vs which is a function of the mode and integration time.
     ** There are a few aspects involved in the computation of Vs, like
     ** the number of adder planes (when applicable) and the number of bits
     ** used for representing the lags values correlated by means of ALMA1.
     ** @param key mode identifier.
     ** @param dumpDuration hardware integration time in ACS units
     ** @return Vs, that is, accumulated value over a dump duration interval
     ** for of a null signal at correlator input.
     **         throw ( CorrConfigModeErr::InvalidKeyExImpl );
     */
    float getVs(int key, ACS::TimeInterval dumpDuration);

    /** computes TFB sub-bands frequency center.
     ** For all sub-bands in any given TFB mode this method computes
     ** individual sub-band center frequencies and returns them as a
     ** vector of float values. Provided that fc is an integer multiple 
     ** of the Direct Digital Synthesizer (DDS) step all computed frequency
     ** centers are integral multiples of that step as well. This is a strong
     ** requirement for a proper configuration of the TFB DDS frequency register.
     ** This also means that the observer is constrained to a finite set of
     ** fc values across the 2GHz band width.
     ** The algorithm for locating all sub-bands around fc is such that a
     ** fixed (not configurable) amount of band width is overlapped among
     ** consecutive sub-bands. The rule is to drop in total one sixteenth 
     ** of the total band width of each sub-band, therefore, they overlap
     ** in an amount equal to 1/32 of their band width.
     ** Overlapping happens only when more than one TFB sub-band is involved,
     ** for modes involving only one TFB sub-band there is no effective
     ** stictching but still one sixteenth of the sub-band is dropped, half of
     ** it at each extreme of its bandwidth.
     ** Overlapping is used as a means for getting rid of those skirt artifacts
     ** present at both extremes of a TFB sub-band, and it requires proper
     ** stitching within the cdp software.
     ** Modes that involve only one sub-band are such that fc coincides with
     ** that value returned as a vector of unitary dimension.
     ** This method always checks that fc is an integer number of the DDS step,
     ** failing to prove that an exception is thrown.
     ** The algorithm works such that sub-bands to the left of fc are squeezed
     ** together to the right, and those to the right of fc are squeezed to
     ** the left. And given that the number of sub-bands is an even number (except
     ** for those modes that include one single TFB sub-band) then it results
     ** that fc is always at the center of the overlapping area of the 2 
     ** sub-bands at the center of the spectral window.
     ** @param key mode identifier.
     ** @param fc spectral window frequency center in MHz.
     ** @return vector of frequency centers in MHz, the size of the vector matches
     ** number of TFB sub-bands for the 'key' mode.
        throw ( CorrConfigModeErr::InvalidKeyExImpl, 
                CorrConfigModeErr::InvalidFcExImpl,
                CorrConfigModeErr::FcOutOfRangeExImpl,
                CorrConfigModeErr::ModeInconsistencyExImpl );
     */
    std::vector<float> getSubBandsFc(int key, float fc);

    /** returns the effective band width of the given mode.
     ** TFB modes are implemented such that the spectral window is built
     ** based on 1 or more sub-bands that are stitched together as to form
     ** the output spectral window. For stitching to work it is required that
     ** individual sub-bands are actually overlapped in frequency, getting
     ** rid with this of the skirts artifacts at every sub-band filter edge.
     ** Therefore, the effective obtained band width is sligthly narrower
     ** than the nominal one, same applies for the number of spectral channels.
     ** For any other mode than TFB the effective band-width is the same as
     ** the nominal one.
     ** @param key mode identifier.
     ** @return effective band width in MHz.
        throw ( CorrConfigModeErr::InvalidKeyExImpl, 
                CorrConfigModeErr::ModeInconsistencyExImpl );
     */
    float getEffectiveBandWidth(int key);

    /** returns the effective number of spectral channels for the given mode.
     ** See getEffectiveBandWidth for a proper description of the difference
     ** between nominal and effective figures.
     ** @param key mode identifier.
     ** @return effective number of spectral channels.
        throw ( CorrConfigModeErr::InvalidKeyExImpl, 
                CorrConfigModeErr::ModeInconsistencyExImpl );
     */
    int getEffectiveChannels(int key);

    /** produces a printable representation of the table.
     ** @return string representation of the table
     */
    std::string asString();

private:
    /** TFB drop-off fraction.
     ** In TFB mode the output spectra is obtained by aggregating side by
     ** side (stitching) a number of sub-band filters. In order to 
     ** reduce spectral artifacts at the edge of these passband filters
     ** their center frequencies are set such that they do overlap and
     ** the cdp stitches them accordingly. The net effect is that the 
     ** skirts of the filters are effectively dropped and the artifacts
     ** eliminated. As a side effect of this procedure the total bandwidth
     ** of any TFB mode is thus reduced. If 'O' is the bandwidth fraction that
     ** TFB sub-bands are overlaped then the reduction of the stitched
     ** spectrum is a fraction equal to 2*O. The following constant defines
     ** the value used for 2*O. Its optimal setting should be investigated.
     */
    static const float CORR_CONFIG_MODE_TFB_DROPOFF_FRACTION = 0.0625;

    /** Vs constant.
     ** This is the Vs value for an integration of 1ms. This value matches
     ** the blanking time in the ALMA1 chip, therefore, any update to the
     ** firmware in this respects would require an update of this constant
     ** here. For 16ms modes we have:
     ** Vs = 16ms x (1/512ms) x 4.5 x (125,000 - 270)
     ** For 1, 2, 4, or 8 ms modes we have
     ** Vs = Nms x (1/32ms) x 4.5 x (125,000 - 270)
     ** 270 = blanking time
     ** The division by 1.0e4 is to scale to ACS 100ns units and the subtraction
     ** is to compensate (in average) for the truncation in the ALMA1 chip.
     */
    static const float CORR_CONFIG_MODE_Vs_CONST =
        (4.5 * (125000.0 - 270.0) / 32.0 - 0.5) / 1.0e4;

    /** @struct KnownTypesCompare
     ** this structure is internally used for handling the set of known types
     */
    struct KnownTypesCompare
    {
        //
        // is s1 < s2 ?
        //
        // Note that we return false for strings that look the same. See
        // Item 21 in Effective STL for details.
        //
        bool operator()(const char *s1, const char *s2)
        {
            int cmp = strcmp(s1, s2);

            if ( cmp < 0 )
            {
                return true;
            }
            
            return false;
        }
    };
    
    /** @var m_knownTypes
     ** this set variable holds the know type names, anything that's not in the
     ** set is treated as an invalid type
     */
    std::set<const char *, KnownTypesCompare> m_knownTypes;

    /** @var m_type 
     ** human readable name of the current correaltor configuration
     */
    std::string m_type;

    /** @var m_table
     ** a map keyed with a correlator index, as TFBMode from CorrConfigMode.ild,
     ** and containing a Correlator::ConfigMode structure for each key.
     ** This table contains only the available modes for the current type.
     */
    std::map<int, Correlator::ConfigMode> m_table;

    /** @var m_knownNominalBandWidths
     ** a vector a known nominal bandwidths in MHz
     */
    std::vector<float> m_knownNominalBandwidths;

    /** @var m_knownNominalBandWidth
     ** a vector a known nominal bandwidths in MHz
     */
    std::vector<int> m_knownNominalChannels;

    /** defines the set of known correlator type names.
     */
    void setKnownValues();

    /** reads the correlator type.
     ** access the CDB in search for an attribute under the Correlator Master
     ** component from where to read a string with the name of the current
     ** correlator type.
     **         throw ( CorrConfigModeErr::ReadTypeExImpl );
     */
    void readType(CDB::DAL_ptr dal_p);

    /** helper function for converting a polarization enum into a string.
     ** @param polz polarization product
     ** @return string representation of that enum
     */
    static std::string enumAsString(const Correlator::ePolarization &polz);

    /** helper function for converting a filter mode enum into a string.
     ** @param filter filter mode enum
     ** @return string representation of that enum
     */
    static std::string enumAsString(const Correlator::eFilterMode &filter);

    /** helper function for converting a fraction enum into a string.
     ** @param fraction mode enum
     ** @return string representation of that enum
     */
    static std::string enumAsString(const Correlator::eFraction &fraction);

    /** helper function for decoding a Stokes parma sequence into an enum.
     ** The idl configuration defines the polarization products as a sequence
     ** of the involved Stokes components. This method decode the sequence
     ** into an enum variable which is the actual type used by the table
     ** for mapping specific polarization product results (SINGLE_X, SINGLE_Y,
     ** DOUBLE or FULL).
     ** @param polz sequence of Stokes parameters as coming with a configuration.
     ** @return enum that matches the sequence.
     **         throw ( CorrConfigModeErr::InvalidStokesParameterExImpl );
     */
    static Correlator::ePolarization decodePolzSeq(const Correlator::StokesParameterSeq &polz);

    /** generates a modes table applicable to the current correlator type.
     ** This method obtains a full table from the code generated from 
     ** an ascii file and then it filters out those modes not applicable for
     ** the current correlator hardware type. The name of the originating
     ** ascii file is corrConfigModes.txt, which is installed by the Makefile
     ** in the 'config' directory. However, note that the file is never use
     ** at run time, it is there only for reference.
        throw ( CorrConfigModeErr::EmptyTableExImpl,
                CorrConfigModeErr::ModeInconsistencyExImpl );
     */
    void generateModesTable();
};

#endif /* CorrConfigMode_H */
