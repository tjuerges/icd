/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>

#include "CorrConfigMode.h"

using namespace std;

template <class T, class U>
struct TypeList
{
    typedef T Head;
    typedef U Tail;
};

typedef TypeList<char, TypeList<int, float> > MyTypeList;

template <class T>
class ExtendedMode: public CorrConfigMode
{
public:
    ExtendedMode():
        CorrConfigMode()
    {
    }

    struct mode_t 
    {
        Correlator::ConfigMode mode1;
        T mode2;
    };
};

struct modeExtension_t
{
    int corrMode;
    int whatever;
    Correlator::ConfigMode baseMode;
};

void typedTable(const char *corrType)
{
    try 
    {
        CorrConfigMode m(corrType);

        //
        // try an invalid bbc
        //
        try
        {
            cout << m[0].bandWidth << endl;
        }
        catch ( CorrConfigModeErr::InvalidKeyExImpl &ex)
        {
            cout << "OK: got exception for not valid mode=" << ex.getKey() << endl;
        }
        
        //
        // print the table
        //
        cout << m.asString();
    }
    catch ( CorrConfigModeErr::ConstructorFailureExImpl &ex )
    {
        ex.log();
        cout << ex.getReason() << endl;
        cout << "test aborted after exception in constructor" << endl;
        return;
    }    
}

int main(int argc, char *argv[])
{
    typedTable(Correlator::CORRELATOR_12m_2ANTS);
    typedTable(Correlator::CORRELATOR_12m_1QUADRANT);
    typedTable(Correlator::CORRELATOR_12m_4QUADRANT);

//     CorrConfigMode m(Correlator::CORRELATOR_12m_2ANTS);
//     cout << m.getType() << endl;
//     cout << m.getNumberSubBands(1) << endl;
/*
//    ExtendedMode<CorrelatorConfigModes::mode_t, ExtendedMode::mode_t> e;
//    ExtendedMode<int, ExtendedMode::mode_t> e;
    
    MyTypeList::Head zxz = 'c';
    MyTypeList::Tail::Head zxz2 = 1;
    MyTypeList::Tail::Tail zxz3 = 1.2;

    //
    // extend the table with additional information
    //
    map<int, modeExtension_t> extendedTable;
    for ( int i = 0; i < m.getSize(); i++ )
    {
        modeExtension_t extendedMode;

        extendedMode.corrMode = 1000 + i + 1;
        extendedMode.whatever = 1000 + i + 1;
        extendedMode.baseMode = m[i + 1];

        extendedTable[i + 1] = extendedMode;
        }*/

    return 0;
}
/*___oOo___*/
