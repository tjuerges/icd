/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>
#include <iomanip>

#include <CPolarizationType.h>
#include <ControlDataInterfacesC.h>

#include "CorrConfigMode.h"

using namespace std;

string pair2str(const Control::PolarizationTypeSeq &pair)
{
    stringstream ret("");

    ret << "[" << CPolarizationType::name(pair[0]) << ", " << CPolarizationType::name(pair[1]) << "]";

    return ret.str();
}

string polz2str(const Control::PolarizationTypeSeqSeq &polz)
{
    stringstream ret("");

    ret << "[";

    for ( unsigned int i = 0; i < polz.length(); i++ )
    {
        ret << pair2str(polz[i]);

        if ( i + 1 != polz.length() )
        {
            ret << ", ";
        }
    }
    
    ret << "]";

    return ret.str();
}

int main(int argc, char *argv[])
{
    CorrConfigMode m(Correlator::CORRELATOR_12m_4QUADRANT);
    vector<int> keys = m.getKeys();

    cout << m.getType() << endl;
    cout << "size " << m.getSize() << endl;

    for ( vector<int>::const_iterator k = keys.begin(); k != keys.end(); k++ )
    {
        Control::PolarizationTypeSeqSeq polz;

        polz = m.getPolzProductTypes(*k);

        cout << *k << "\t" << ((m[*k].polarizationProducts == Correlator::POLZ_SINGLE_X) ? "X-X" : (m[*k].polarizationProducts == Correlator::POLZ_SINGLE_Y) ? "Y-Y" : (m[*k].polarizationProducts == Correlator::POLZ_DOUBLE) ? "DOUBLE" : "FULL")
             << "\t" << polz2str(polz) << endl;
    }

    return 0;
}
/*___oOo___*/
