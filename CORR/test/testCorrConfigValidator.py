#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id: testCorrConfigValidator.py,v 1.10 2010/12/16 05:26:57 ramestic Exp $"
#
# who       when      what
# --------  --------  ----------------------------------------------
# dguo  2006-03-20  created
#
'''
Test for the correlator configuration validator component
'''
import unittest
import sys
import time
import string
import ACS
import Correlator
import xmlentity

from ACS import stringSeq

from xml.dom import minidom, Node
#from time import sleep
from Acspy.Clients.SimpleClient import PySimpleClient

class CorrConfigValidatorTestCase(unittest.TestCase):

    #------------------------------------------------------------------
    def setUp(self):
        if len(sys.argv) > 1:
            self.inputFileName = sys.argv[1]
            self.inputFile = file(sys.argv[1],'r')
        else:
            self.inputFileName = 'BL_SpectralSpec.xml'
            
        try:
            self._client=PySimpleClient('CorrConfigValidator')
            self._ref= self._client.getComponent("CORR/CONFIGURATION_VALIDATOR")
        except:
            print 'Could not get Component..'
            exit(1)
            
        self.numOfAnts = 2
    #------------------------------------------------------------------
    def tearDown(self):
        try:
            self._client.releaseComponent("CORR/CONFIGURATION_VALIDATOR")
        except:
            print 'Could not release component'
        self._client.disconnect()

    #------------------------------------------------------------------
    def testTranslationSpectralSpecXMLtoIDL(self):

        print " \n Test testTranslationSpectralSpecXMLtoIDL"

        ssFile = open(self.inputFileName, 'r')
        ssDataXML = ssFile.read()
        ssFile.close()
            
        try:
            self.corrConfig = self._ref.translateSpectralSpecXml2CorrConfigIdl(ssDataXML)
            self.showConfigDataLocal()
            
        except Exception, e:
            assert False, 'Failure translating '+self.inputFileName+' '+str(e)

        (isv, errorMsgs) = self._ref.validateConfigurationSS(ssDataXML, self.numOfAnts)
        if isv == 0:
            for i in range(len(errorMsgs)):
                print 'Error ', i, ' ', errorMsgs[i]
        assert isv == 1, 'Invalid Configuration'

    #------------------------------------------------------------------
    def testConfigValidatorSS(self):
        # Read the SchedBlock xml file
        ssFile = open(self.inputFileName, 'r')
        lines = ssFile.readlines()
        ssFile.close()
        ssDataXML = ""
        for line in lines:
            ssDataXML = ssDataXML + line

        print " \n Test validateConfigurationSS"
        (isv, errorMsgs) = self._ref.validateConfigurationSS(ssDataXML, self.numOfAnts)
        if isv == 0:
            for i in range(len(errorMsgs)):
                print 'Error ', i, ' ', errorMsgs[i]
        assert isv == 1, 'Invalid Configuration'

    #------------------------------------------------------------------
    def showConfigDataLocal(self):
        print '--- Show Configuration data locally --- '
        print 'dumpDuration:          ',self.corrConfig.dumpDuration
        print 'integrationDuration:   ', self.corrConfig.integrationDuration
        print 'channelAverageDuration:', self.corrConfig.channelAverageDuration
        print 'receiverType:          ', self.corrConfig.receiverType
        print 'LO1FrequencyMHz:       ', self.corrConfig.LO1FrequencyMHz
        
        print 'baseBands[0].basebandName:          ', self.corrConfig.baseBands[0].basebandName
        print 'baseBands[0].CAM:                   ', self.corrConfig.baseBands[0].CAM
        print 'baseBands[0].dataProducts:          ', self.corrConfig.baseBands[0].dataProducts
        print 'baseBands[0].sideBandSeparationMode:', self.corrConfig.baseBands[0].sideBandSeparationMode

        print 'baseBands[0].spectralWindows[0].centerFrequencyMHz:                  ', self.corrConfig.baseBands[0].spectralWindows[0].centerFrequencyMHz
        print 'baseBands[0].spectralWindows[0].effectiveBandwidthMHz:               ', self.corrConfig.baseBands[0].spectralWindows[0].effectiveBandwidthMHz
        print 'baseBands[0].spectralWindows[0].effectiveNumberOfChannels:           ', self.corrConfig.baseBands[0].spectralWindows[0].effectiveNumberOfChannels
        print 'baseBands[0].spectralWindows[0].spectralAveragingFactor:             ', self.corrConfig.baseBands[0].spectralWindows[0].spectralAveragingFactor
        print 'baseBands[0].spectralWindows[0].sideBand:                            ', self.corrConfig.baseBands[0].spectralWindows[0].sideBand
        print 'baseBands[0].spectralWindows[0].associatedSpectralWindowNumberInPair:', self.corrConfig.baseBands[0].spectralWindows[0].associatedSpectralWindowNumberInPair
        print 'baseBands[0].spectralWindows[0].useThisSpectralWindow:               ', self.corrConfig.baseBands[0].spectralWindows[0].useThisSpectralWindow
        print 'baseBands[0].spectralWindows[0].windowFunction:                      ', self.corrConfig.baseBands[0].spectralWindows[0].windowFunction
        print 'baseBands[0].spectralWindows[0].correlationBits:                     ', self.corrConfig.baseBands[0].spectralWindows[0].correlationBits
        print 'baseBands[0].spectralWindows[0].correlationNyquistOversampling:      ', self.corrConfig.baseBands[0].spectralWindows[0].correlationNyquistOversampling
        print 'baseBands[0].spectralWindows[0].quantizationCorrection:              ', self.corrConfig.baseBands[0].spectralWindows[0].quantizationCorrection
        print 'baseBands[0].spectralWindows[0].channelAverageRegions[0].startChannel:  ', self.corrConfig.baseBands[0].spectralWindows[0].channelAverageRegions[0].startChannel
        print 'baseBands[0].spectralWindows[0].channelAverageRegions[0].numberChannels:', self.corrConfig.baseBands[0].spectralWindows[0].channelAverageRegions[0].numberChannels

        print 'baseBands[0].binSwitchingMode.SwitchingType:    ', self.corrConfig.baseBands[0].binSwitchingMode.SwitchingType
        print 'baseBands[0].binSwitchingMode.numberOfPositions:', self.corrConfig.baseBands[0].binSwitchingMode.numberOfPositions
        print 'baseBands[0].binSwitchingMode.dwellTime:        ', self.corrConfig.baseBands[0].binSwitchingMode.dwellTime
        print 'baseBands[0].binSwitchingMode.deadTime:         ', self.corrConfig.baseBands[0].binSwitchingMode.deadTime
        print 'baseBands[0].LO2FrequencyMHz:                   ', self.corrConfig.baseBands[0].LO2FrequencyMHz
        
        print

#------------------------------------------------------------------
if __name__ == '__main__':
    suite = unittest.makeSuite(CorrConfigValidatorTestCase)
    unittest.TextTestRunner().run(suite)
