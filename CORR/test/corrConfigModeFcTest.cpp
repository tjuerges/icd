/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>

#include "CorrConfigMode.h"

using namespace std;

int main(int argc, char *argv[])
{
    CorrConfigMode m(Correlator::CORRELATOR_12m_4QUADRANT);
    vector<int> keys = m.getKeys();
    vector<float> freqs;
    float fc = 1000.0;

    cout << m.getType() << endl;
    cout << "size " << m.getSize() << endl;

    //
    // this should throw an exception (fc out of range)
    //
    try 
    {
        freqs = m.getSubBandsFc(keys[0], 0.0);
    }
    catch ( CorrConfigModeErr::FcOutOfRangeExImpl ex )
    {
        cout << "OK: got exception for out of range Fc" << ex.getFc() << endl;
    }
    
    //
    // this should throw an exception (fc is not a DDS step multiple)
    //
    try 
    {
        freqs = m.getSubBandsFc(keys[0], 1000.123456);
    }
    catch ( CorrConfigModeErr::InvalidFcExImpl &ex )
    {
        cout << "OK: got exception for invalid Fc" << endl;
    }
    
    for ( vector<int>::const_iterator key = keys.begin(); 
          key != keys.end();
          key++ )
    {
        try
        {
            freqs = m.getSubBandsFc(*key, fc);
        }
        catch ( CorrConfigModeErr::ModeInconsistencyExImpl &ex )
        {
            continue;
        }

        cout << "key\tbw\tebw\tch\tech\tNs\tFc" << endl;
        cout << *key << "\t" << m[*key].bandWidth << "\t" << m.getEffectiveBandWidth(*key) << "\t" << m[*key].channels << "\t" << m.getEffectiveChannels(*key) << "\t" << m.getNumberSubBands(*key) << "\t" << fc << endl;
        cout << "sb\tFc(i)" << endl;

        for ( unsigned int i = 0; i < freqs.size(); i++ )
        {
            cout << i << "\t" << freqs[i] << endl;
        }
    }

    return 0;
}
/*___oOo___*/
