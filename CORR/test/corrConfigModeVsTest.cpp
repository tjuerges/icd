/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>
#include <iomanip>

#include "CorrConfigMode.h"

using namespace std;

int main(int argc, char *argv[])
{
    CorrConfigMode m(Correlator::CORRELATOR_12m_4QUADRANT);
    vector<int> keys = m.getKeys();
    vector<int> integrationTimes;
    integrationTimes.push_back(1);
    integrationTimes.push_back(2);
    integrationTimes.push_back(4);
    integrationTimes.push_back(8);
    integrationTimes.push_back(16);
    integrationTimes.push_back(1008);

    cout.setf(ios::fixed, ios::floatfield);
    cout.setf(ios::showpoint);

    for ( vector<int>::const_iterator it = integrationTimes.begin();
          it != integrationTimes.end();
          it++ )
    {
        cout << m.getType() << endl;
        cout << *it << " [ms]" << endl;
        cout << "size " << m.getSize() << endl;
        cout << "key\tbw\tch\tbits\t2nyq\tfilter\tfrac\tVs" << endl;
        for ( vector<int>::const_iterator key = keys.begin(); 
              key != keys.end();
              key++ )
        {
            Correlator::eFraction frac = m.getFraction(*key);
            
            cout << *key << "\t";
            cout << setprecision(2) << m[*key].bandWidth << "\t";
            cout << m[*key].channels << "\t";
            switch ( m[*key].bits )
            {
            case CorrelationBitMod::BITS_2x2:
                cout << "2x2" << "\t";
                break;
            case CorrelationBitMod::BITS_3x3:
                cout << "3x3" << "\t";
                break;
            case CorrelationBitMod::BITS_4x4:
                cout << "4x4" << "\t";
                break;
            default:
                cout << "???" << "\t";
            }
            if ( m[*key].overSampled )
            {
                cout << "true" << "\t";
            }
            else
            {
                cout << "false" << "\t";
            }
            switch ( m[*key].filterMode )
            {
            case Correlator::FILTER_TDM:
                cout << "TDM" << "\t";
                break;
            case Correlator::FILTER_FDM:
                cout << "FDM" << "\t";
                break;
            default:
                cout << "???" << "\t";
                break;
            }
            switch ( frac )
            {
            case Correlator::FRACTION_FULL:
                cout << "1/1" << "\t";
                break;
            case Correlator::FRACTION_ONE_HALF:
                cout << "1/2" << "\t";
                break;
            case Correlator::FRACTION_ONE_FOURTH:
                cout << "1/4" << "\t";
                break;
            case Correlator::FRACTION_ONE_EIGHTH:
                cout << "1/8" << "\t";
                break;
            default:
                cout << "???" << "\t";
                break;
            }
            cout << setprecision(5) << m.getVs(*key, *it * 10000) << endl;
        }
    }

    return 0;
}
/*___oOo___*/
