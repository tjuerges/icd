/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>

#include "CorrConfigMode.h"

using namespace std;

int main(int argc, char *argv[])
{
    int  key;
    CorrConfigMode m(Correlator::CORRELATOR_12m_4QUADRANT);
    Correlator::StokesParameterSeq polzSeq;

    //
    // get available mode keys
    //
    vector<int> keys = m.getKeys();
    
    cout << m.getType() << endl;
    cout << "size " << m.getSize() << endl;
    
    //
    // all modes should be found successfully
    //
    for ( vector<int>::const_iterator key = keys.begin(); 
          key != keys.end();
          key++ )
    {
        int _key;

        //
        // construct the Stokes sequence based on the polarization type
        //
        if ( m[*key].polarizationProducts == Correlator::POLZ_SINGLE_X )
        {
            polzSeq.length(1);

            polzSeq[0] = StokesParameterMod::XX;
        }
        else if ( m[*key].polarizationProducts == Correlator::POLZ_SINGLE_Y )
        {
            polzSeq.length(1);

            polzSeq[0] = StokesParameterMod::YY;
        }
        else if ( m[*key].polarizationProducts == Correlator::POLZ_DOUBLE )
        {
            polzSeq.length(2);

            polzSeq[0] = StokesParameterMod::XX;

            polzSeq[1] = StokesParameterMod::YY;
        }
        else
        {
            polzSeq.length(4);

            polzSeq[0] = StokesParameterMod::XX;

            polzSeq[1] = StokesParameterMod::XY;

            polzSeq[2] = StokesParameterMod::YX;

            polzSeq[3] = StokesParameterMod::YY;
        }

        //
        // find from nominal params
        //
        _key = m.findModeFromNominal(m[*key].bandWidth,
                                     m[*key].channels,
                                     m[*key].bits,
                                     m[*key].overSampled,
                                     polzSeq);

        //
        // findMode should have returned the same key value
        //
        if ( _key != *key )
        {
            cout << "nominal: mode " << *key << " incorrectly mapped to " << _key << endl;
        }

        //
        // find from effective params
        //
        if ( m[*key].filterMode != Correlator::FILTER_TDM )
        {
            _key = m.findModeFromEffective(m[*key].bandWidth * 0.9375,
                                           int(m[*key].channels * 0.9375),
                                           m[*key].bits,
                                           m[*key].overSampled,
                                           polzSeq);
        }
        else
        {
            _key = m.findModeFromEffective(m[*key].bandWidth,
                                           int(m[*key].channels),
                                           m[*key].bits,
                                           m[*key].overSampled,
                                           polzSeq);
        }

        //
        // findMode should have returned the same key value
        //
        if ( _key != *key )
        {
            cout << "effective: mode " << *key << " incorrectly mapped to " << _key << endl;
        }
    }

    //
    // this one is an invalid mode, it should throw an exception
    // 
    try
    {
        polzSeq.length(1);
        polzSeq[0] = StokesParameterMod::XX;

        key =
            m.findModeFromNominal(2000.1, // <-- not good
                                  8192,
                                  CorrelationBitMod::BITS_2x2,
                                  false,
                                  polzSeq);
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }

    //
    // this one is an invalid mode, it should throw an exception
    // 
    try
    {
        polzSeq.length(1);
        polzSeq[0] = StokesParameterMod::YY;

        key =
            m.findModeFromNominal(2000.0,
                                  8193, // <-- not good
                                  CorrelationBitMod::BITS_2x2,
                                  false,
                                  polzSeq);
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }

    //
    // this one is an invalid mode, it should throw an exception
    // 
    try
    {
        polzSeq.length(1);
        polzSeq[0] = StokesParameterMod::XX;

        key =
            m.findModeFromNominal(2000.0,
                                  8192,
                                  CorrelationBitMod::BITS_3x3, // <-- not good
                                  false,
                                  polzSeq);
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }
    //
    // this one is an invalid mode, it should throw an exception
    // 
    try
    {
        polzSeq.length(2);
        polzSeq[0] = StokesParameterMod::XX;
        polzSeq[1] = StokesParameterMod::YY;

        key =
            m.findModeFromNominal(2000.0,                        // <-+
                                  8192,                          //   |
                                  CorrelationBitMod::BITS_2x2,   //   |
                                  true,                          // <-+--- not good 
                                  polzSeq);                      // <-+
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }
    //
    // this one is an invalid mode, it should throw an exception
    // 
    try
    {
        polzSeq.length(4);
        polzSeq[0] = StokesParameterMod::XX;
        polzSeq[1] = StokesParameterMod::XY;
        polzSeq[2] = StokesParameterMod::YX;
        polzSeq[3] = StokesParameterMod::YY;

        key =
            m.findModeFromNominal(2000.0,                        // <-+
                                  8192,                          //   |
                                  CorrelationBitMod::BITS_2x2,   //   |
                                  true,                          // <-+--- not good 
                                  polzSeq);                      // <-+
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }

    //
    // we know that at this moment that the dropoff fraction is one sixteenth.
    //
    polzSeq.length(1);
    polzSeq[0] = StokesParameterMod::XX;
    if ( m.findModeFromNominal(2000.0,
                               8192,
                               CorrelationBitMod::BITS_2x2,
                               false,
                               polzSeq)
         !=
         m.findModeFromEffective(1875,
                                 7680,
                                 CorrelationBitMod::BITS_2x2,
                                 false,
                                 polzSeq) )
    {
        cout << "failed to compare nominal and effective mode" << endl;
    }

    //
    // this effective mode is invalid
    //
    try
    {
        polzSeq.length(1);
        polzSeq[0] = StokesParameterMod::XX;

        key =
            m.findModeFromEffective(1875,
                                    7681,       // <- not good
                                    CorrelationBitMod::BITS_2x2,
                                    false,
                                    polzSeq);
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid effective mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }

    //
    // this effective mode is invalid
    //
    try
    {
        polzSeq.length(1);
        polzSeq[0] = StokesParameterMod::XX;

        key =
            m.findModeFromEffective(1875.1,       // <- not good
                                    7680,
                                    CorrelationBitMod::BITS_2x2,
                                    false,
                                    polzSeq);
    }
    catch ( CorrConfigModeErr::InvalidModeExImpl &ex)
    {
        cout << "OK: got exception for invalid effective mode="
             << ex.getBandwidthMHz() << "/"
             << ex.getChannels() << "/"
             << ex.getBits() << "/"
             << ex.getOverSampled() << "/"
             << ex.getPolarizationProducts() << "/"
             << ex.getFilterMode() << endl;
    }

    return 0;
}

/*___oOo___*/
