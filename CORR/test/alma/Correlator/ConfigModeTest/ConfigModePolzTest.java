/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;

//
// ICD stuff
//
import alma.CorrConfigModeErr.wrappers.*;
import alma.Correlator.CORRELATOR_12m_2ANTS;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;
import alma.PolarizationTypeMod.PolarizationType;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;
import alma.Correlator.ePolarization;

/**
 * test ConfigPolzMode class
 */
public class ConfigModePolzTest
{
    private static String pair2str(PolarizationType[] pair)
    {
        return "[" + pair[0].toString() + ", " + pair[1].toString() + "]";
    }
    
    private static String polz2str(PolarizationType[][] polz)
    {
        String ret;
        
        ret = "[";
        
        for ( int i = 0; i < polz.length; i++ )
        {
            ret += pair2str(polz[i]);
            
            if ( i + 1 != polz.length )
            {
                ret += ", ";
            }
        }
    
        ret += "]";

        return ret;
    }

    public static void main(String[] args)
    {
        try
        {
            CorrConfigModeClass m = new CorrConfigModeClass(CORRELATOR_12m_4QUADRANT.value);
            int[] keys = m.getKeys();
            
            System.out.println(m.getType());
            System.out.println("size " + m.getSize());
            
            for ( int key: keys )
            {
                String s = "";
                PolarizationType[][] polz;
                
                polz = m.getPolzProductTypes(key);
            
                if ( m.getMode(key).polarizationProducts == ePolarization.POLZ_SINGLE_X )
                {
                    s += "X-X";
                }
                else if ( m.getMode(key).polarizationProducts == ePolarization.POLZ_SINGLE_Y )
                {
                    s += "Y-Y";
                }
                else if ( m.getMode(key).polarizationProducts == ePolarization.POLZ_DOUBLE )
                {
                    s += "DOUBLE";
                }
                else
                {
                    s += "FULL";
                }
                
                System.out.println(key + "\t" + s + "\t" + polz2str(polz));
            }
        }
        catch ( Throwable thr )
        {
            System.out.println("got exception");
        }
    }
}

/*___oOo___*/
