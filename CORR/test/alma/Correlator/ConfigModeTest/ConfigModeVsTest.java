/*
  ALMA - Atacama Large Millimiter Array
  * (c) Associated Universities Inc., 2007 
  * 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
  */
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;

//
// ICD stuff
//
import alma.CorrConfigModeErr.wrappers.*;
import alma.CorrelationBitMod.CorrelationBit;
import alma.Correlator.CORRELATOR_12m_2ANTS;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;
import alma.Correlator.ePolarization;
import alma.Correlator.eFilterMode;
import alma.Correlator.eFraction;

/**
 * test CorrConfigMode class
 */
public class ConfigModeVsTest
{
    public static void main(String[] args)
    {
        try
        {
            CorrConfigModeClass m = new CorrConfigModeClass(CORRELATOR_12m_4QUADRANT.value);
            int[] keys = m.getKeys();
            int[] integrationTimes = new int[] {1, 2, 4, 8, 16, 1008};

            for ( int it: integrationTimes )
            {
                System.out.println(m.getType());
                System.out.println(it + " [ms]");
                System.out.println("size " + m.getSize());
                System.out.println("key\tbw\tch\tbits\t2nyq\tfilter\tfrac\tVs");
                
                for ( int key: keys )
                {
                    eFraction frac = m.getFraction(key);
                    
                    System.out.print(key + "\t");
                    System.out.printf("%.2f\t", m.getMode(key).bandWidth);
                    System.out.print(m.getMode(key).channels + "\t");
                    switch ( m.getMode(key).bits.value() )
                    {
                    case CorrelationBit._BITS_2x2:
                        System.out.print("2x2" + "\t");
                        break;
                    case CorrelationBit._BITS_3x3:
                        System.out.print("3x3" + "\t");
                        break;
                    case CorrelationBit._BITS_4x4:
                        System.out.print("4x4" + "\t");
                        break;
                    default:
                        System.out.print("???" + "\t");
                        break;
                    }
                    if ( m.getMode(key).overSampled )
                    {
                        System.out.print("true" + "\t");
                    }
                    else
                    {
                        System.out.print("false" + "\t");
                    }
                    switch ( m.getMode(key).filterMode.value() )
                    {
                    case eFilterMode._FILTER_TDM:
                        System.out.print("TDM" + "\t");
                        break;
                    case eFilterMode._FILTER_FDM:
                        System.out.print("FDM" + "\t");
                        break;
                    default:
                        System.out.print("???" + "\t");
                        break;
                    }
                    switch ( frac.value() )
                    {
                    case eFraction._FRACTION_FULL:
                        System.out.print("1/1" + "\t");
                        break;
                    case eFraction._FRACTION_ONE_HALF:
                        System.out.print("1/2" + "\t");
                        break;
                    case eFraction._FRACTION_ONE_FOURTH:
                        System.out.print("1/4" + "\t");
                        break;
                    case eFraction._FRACTION_ONE_EIGHTH:
                        System.out.print("1/8" + "\t");
                        break;
                    default:
                        System.out.print("???" + "\t");
                        break;
                    }
                    System.out.printf("%.5f\n", m.getVs(key, new alma.acstime.Duration(it * 10000)));
                }
            }       
        }
        catch ( Throwable thr )
        {
            System.out.println("got exception");
        }
    }
}

/*___oOo___*/
