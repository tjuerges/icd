/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;

//
// ICD stuff
//
import alma.CorrelationBitMod.CorrelationBit;
import alma.StokesParameterMod.StokesParameter;
import alma.Correlator.StokesParameterSeqHolder;
import alma.Correlator.ConfigMode;
import alma.Correlator.ePolarization;
import alma.Correlator.eFilterMode;
import alma.CorrConfigModeErr.wrappers.*;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;

/**
 * test CorrConfigMode class
 */
public class ConfigModeFindTest
{
    public static void main(String[] args)
    {
        int key;
        CorrConfigModeClass modes;
        StokesParameter[] polzSeq;

        try
        {
            modes = new CorrConfigModeClass(CORRELATOR_12m_4QUADRANT.value);
        }
        catch ( Throwable th)
        {
            System.out.println("failed to create modes object");

            return;
        }

        //
        // get available mode keys
        //
        int[] keys;
        try
        {
            keys = modes.getKeys();
        }
        catch ( Throwable th )
        {
            System.out.println("failed to get array of keys");

            return;
        }
    
        System.out.println(modes.getType());
        System.out.println(modes.getSize());
    
        //
        // all modes should be found successfully
        //
        for ( int i = 0; i < keys.length; i++ )
        {
            int _key;
            ConfigMode m;

            try
            {
                m = modes.getMode(keys[i]);
            }
            catch ( Throwable th )
            {
                System.out.println("failed to get mode " + keys[i]);

                return;
            }

            //
            // construct the Stokes sequence based on the polarization type
            //
            if ( m.polarizationProducts == ePolarization.POLZ_SINGLE_X )
            {
                polzSeq = new StokesParameter[1];
                
                polzSeq[0] = StokesParameter.XX;
            }
            else if ( m.polarizationProducts == ePolarization.POLZ_SINGLE_Y )
            {
                polzSeq = new StokesParameter[1];
                
                polzSeq[0] = StokesParameter.YY;
            }
            else if ( m.polarizationProducts == ePolarization.POLZ_DOUBLE )
            {
                polzSeq = new StokesParameter[2];
                
                polzSeq[0] = StokesParameter.XX;
                
                polzSeq[1] = StokesParameter.YY;
            }
            else
            {
                polzSeq = new StokesParameter[4];
                
                polzSeq[0] = StokesParameter.XX;
                
                polzSeq[1] = StokesParameter.XY;
                
                polzSeq[2] = StokesParameter.YX;
                
                polzSeq[3] = StokesParameter.YY;
            }
            
            try
            {
                _key = modes.findModeFromNominal(m.bandWidth,
                                                 m.channels,
                                                 m.bits,
                                                 m.overSampled,
                                                 polzSeq);
            }
            catch ( Throwable th )
            {
                System.out.println("nominal: failed to find mode " + keys[i]);

                return;
            }

            //
            // findMode should have returned the same key value
            //
            if ( _key != keys[i] )
            {
                System.out.println("nominal: mode " + keys[i] + " incorrectly mapped to " + _key);
                return;
            }


            //
            // find from effective params
            //
            try
            {
                if ( m.filterMode != eFilterMode.FILTER_TDM )
                {
                    _key = modes.findModeFromEffective((float)(m.bandWidth * 0.9375),
                                                       (int)(m.channels * 0.9375),
                                                       m.bits,
                                                       m.overSampled,
                                                       polzSeq);
                }
                else
                {
                    _key = modes.findModeFromEffective(m.bandWidth,
                                                       m.channels,
                                                       m.bits,
                                                       m.overSampled,
                                                       polzSeq);
                }
            }
            catch ( Throwable th )
            {
                System.out.println("effective: failed to find mode " + keys[i]);
                
                return;
            }
                
            //
            // findMode should have returned the same key value
            //
            if ( _key != keys[i] )
            {
                System.out.println("effective: mode " + keys[i] + " incorrectly mapped to " + _key);
                return;
            }
        }
        
        //
        // this one is an invalid mode, it should throw an exception
        // 
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.XX;

            key =
                modes.findModeFromNominal((float)2000.1, // <-- not good
                                          (long)8192,
                                          CorrelationBit.BITS_2x2,
                                          false,
                                          polzSeq);
        }
        catch ( AcsJInvalidModeEx ex)
        {
            System.out.println("OK: got exception for invalid mode");
        }

        //
        // this one is an invalid mode, it should throw an exception
        // 
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.YY;

            key =
                modes.findModeFromNominal((float)2000.0,
                                          (long)8193, // <-- not good
                                          CorrelationBit.BITS_2x2,
                                          false,
                                          polzSeq);
        }
        catch ( AcsJInvalidModeEx ex)
        {
            System.out.println("OK: got exception for invalid mode");
        }
        
        //
        // this one is an invalid mode, it should throw an exception
        // 
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.XX;

            key =
                modes.findModeFromNominal((float)2000.0,
                                          (long)8192,
                                          CorrelationBit.BITS_3x3, // <-- not good
                                          false,
                                          polzSeq);
        }
        catch ( AcsJInvalidModeEx ex)
        {
            System.out.println("OK: got exception for invalid mode");
        }
        //
        // this one is an invalid mode, it should throw an exception
        // 
        try
        {
            polzSeq = new StokesParameter[2];
            polzSeq[0] = StokesParameter.XX;
            polzSeq[1] = StokesParameter.YY;

            key =
                modes.findModeFromNominal((float)2000.0,             // <-+
                                          (long)8192,                //   |
                                          CorrelationBit.BITS_2x2,   //   |
                                          true,                      // <-+--- not good 
                                          polzSeq);                  // <-+
        }
        catch ( AcsJInvalidModeEx ex)
        {
            System.out.println("OK: got exception for invalid mode");
        }
        //
        // this one is an invalid mode, it should throw an exception
        // 
        try
        {
            polzSeq = new StokesParameter[4];
            polzSeq[0] = StokesParameter.XX;
            polzSeq[1] = StokesParameter.XY;
            polzSeq[2] = StokesParameter.YX;
            polzSeq[3] = StokesParameter.YY;

            key =
                modes.findModeFromNominal((float)2000.0,             // <-+
                                          (long)8192,                //   |
                                          CorrelationBit.BITS_2x2,   //   |
                                          true,                      // <-+--- not good 
                                          polzSeq);                  // <-+
        }
        catch ( AcsJInvalidModeEx ex)
        {
            System.out.println("OK: got exception for invalid mode");
        }

        //
        // we know that at this moment that the dropoff fraction is one sixteenth.
        //
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.XX;

            if ( modes.findModeFromNominal((float)2000.0, 
                                           (long)8192,
                                           CorrelationBit.BITS_2x2,
                                           false,
                                           polzSeq)
                 !=
                 modes.findModeFromEffective((float)1875,
                                             (long)7680,
                                             CorrelationBit.BITS_2x2,
                                             false,
                                             polzSeq) )
            {
                System.out.println("effective mode render unexpected nominal one");
            }
        }
        catch ( Throwable th )
        {
            System.out.println("failed to compare nominal and effective mode");
            
            return;
        }

        //
        // this effective mode is invalid
        //
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.XX;

            key =
                modes.findModeFromEffective((float)1875,
                                            (long)7681,       // <- not good
                                            CorrelationBit.BITS_2x2,
                                            false,
                                            polzSeq);
        }
        catch ( Throwable th )
        {
            System.out.println("OK: got exception for invalid effective mode");
        }

        //
        // this effective mode is invalid
        //
        try
        {
            polzSeq = new StokesParameter[1];
            polzSeq[0] = StokesParameter.XX;
            
            key =
                modes.findModeFromEffective((float)1875.1,       // <- not good
                                            (long)7680,
                                            CorrelationBit.BITS_2x2,
                                            false,
                                            polzSeq);
        }
        catch ( Throwable th )
        {
            System.out.println("OK: got exception for invalid effective mode");
        }
    }
}

/*___oOo___*/
