/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;
import java.util.logging.Logger;

//
// ACS stuff
//
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;

//
// ICD stuff
//
import alma.CorrConfigModeErr.ConstructorFailureEx;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;

/**
 * test CorrConfigMode class
 */
public class ConfigModeCDBTest extends ComponentClient
{
    private Logger m_logger;
    
    ConfigModeCDBTest(String managerLoc, String clientName)
        throws Exception
    {
        super(null, managerLoc, clientName);
        
        //
        // get a logger
        //
        m_logger = Logger.getLogger(clientName);

    }

    public com.cosylab.CDB.DAL getCDB()
        throws Exception
    {
        try
        {
            return getContainerServices().getCDB();
        }
        catch ( Exception ex )
        {
            throw new Exception("failed to get CDB");
        }
    }

    public static void main(String[] args)
    {
        //
        // get ACS manager corbaloc from java properties
        //
        String managerLoc = System.getProperty("ACS.manager");
        if ( managerLoc == null)
        {
            System.out.println("Java property 'ACS.manager' " + 
                               " must be set to the corbaloc of the ACS manager!");
            System.exit(-1);
        }
		
        //
        // our name
        //
        String clientName = "CorrConfigModeCDBTest";

        try
        {
            ConfigModeCDBTest client = new ConfigModeCDBTest(managerLoc,
                                                             clientName);

            //
            // instantiate the table
            //
            CorrConfigModeClass m = new CorrConfigModeClass(client.getCDB());

            //
            // print the table
            //
            System.out.print(m.asString());            

            //
            // get rid of the client
            //
            client.tearDown();
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
            
            return;
        }
    }
}

/*___oOo___*/
