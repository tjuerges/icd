/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;

//
// ICD stuff
//
import alma.CorrConfigModeErr.wrappers.*;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;

/**
 * test CorrConfigMode class
 */
public class UsageExample
{
    public static void main(String[] args)
        throws AcsJConstructorFailureEx, AcsJInvalidKeyEx, AcsJModeInconsistencyEx
    {
        CorrConfigModeClass m;
        String corrType = CORRELATOR_12m_4QUADRANT.value;

        m = new CorrConfigModeClass(corrType);

        //
        // let's pull all available modes for the corrType used above
        //
        int[] keys = m.getKeys();

        //
        // now let's say we present these modes to the user infront
        // of the screen, by means of a gui or whatever. but here just
        // for this example's sake let's simply print out the nominal
        // bandwidth and number of channels of all available modes.
        //
        for ( int i = 0; i < keys.length; i++ )
        {
            System.out.println(keys[i] + "\t" + m.getMode(keys[i]).bandWidth + "\t" + m.getMode(keys[i]).channels);
        }

        //
        // now let's assume that from the above 'presentation' the user decided 
        // to choose for mode '13', therefore, now our task is to construct
        // a correlator configuration that represents mode '13', plus the
        // myriad of other details that are not part of the 'mode' as
        // represented by the table implemented by CorrConfigMode, like accumulation
        // mode, correlation mode products, number of spectral windows and so so
        // and so on. Just for the sake of the example, let's simply say that
        // we want to extract from '13' the effective bandwidth and effective
        // channels that are to go into the correlator configuration object.
        //
        System.out.println("band width = " +
                           m.getMode(13).bandWidth +
                           "/" +
                           m.getEffectiveBandWidth(13));
        System.out.println("channels = " +
                           m.getMode(13).channels +
                           "/" +
                           m.getEffectiveChannels(13));
    }
}

/*___oOo___*/
