/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  ramestic
 * @version $Id$
 * @since    
 */

package alma.Correlator.ConfigModeTest;

//
// System stuff
//
import java.util.*;

//
// ICD stuff
//
import alma.CorrConfigModeErr.wrappers.*;
import alma.Correlator.CORRELATOR_12m_2ANTS;
import alma.Correlator.CORRELATOR_12m_1QUADRANT;
import alma.Correlator.CORRELATOR_12m_2QUADRANT;
import alma.Correlator.CORRELATOR_12m_4QUADRANT;

//
// Local stuff
//
import alma.Correlator.CorrConfigModeClass.CorrConfigModeClass;

/**
 * test CorrConfigMode class
 */
public class ConfigModeTest
{
    public static void typedTable(String corrType)
    {
        CorrConfigModeClass m;
        
        try
        {
            m = new CorrConfigModeClass(corrType);
        }
        catch ( AcsJConstructorFailureEx ex )
        {
            System.out.println("got a constructor exception for type " +
                               corrType);
            ex.printStackTrace();
            return;
        }
        
        //
        // try an invalid bbc
        //
        try
        {
            System.out.println(m.getMode(0).bandWidth);
        }
        catch ( AcsJInvalidKeyEx ex)
        {
            System.out.println("OK: got exception for not valid mode=0");
        }

        //
        // print the table
        //
        System.out.print(m.asString());
    }

    public static void main(String[] args)
    {
        typedTable(CORRELATOR_12m_2ANTS.value);
        typedTable(CORRELATOR_12m_1QUADRANT.value);
        typedTable(CORRELATOR_12m_4QUADRANT.value);
    }
}

/*___oOo___*/
