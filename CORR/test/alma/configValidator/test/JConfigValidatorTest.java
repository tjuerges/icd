/* $Id: JConfigValidatorTest.java,v 1.19 2011/03/30 03:09:29 dguo Exp $
 */
package alma.configValidator.test;

import junit.framework.*;
//Java
import java.util.Vector;
//Correlator
import alma.correlatorSrc.CorrConfigValidator.*;
import alma.AtmPhaseCorrectionMod.AtmPhaseCorrection;
import alma.asdmIDLTypes.*;
import alma.hla.datamodel.enumeration.*;
import alma.Correlator.*;
import alma.CorrConfigModeErr.wrappers.*;
import alma.SidebandProcessingModeMod.SidebandProcessingMode;

//For SB test
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import alma.xmlentity.XmlEntityStruct;
import alma.entity.xmlbinding.schedblock.*;
import alma.acs.entityutil.EntityDeserializer;
import alma.entities.commonentity.EntityT;

/** This unit test only tests the two validateConfiguration method
 ** Not for component test
 ** The way that this test works is that it creates a simple correct
 ** Correlator configuration IDL in the setUp() method. Then each test
 ** sets some parameters to invalid values or combinations and then runs
 ** the configuration through the configuration validator. The validator
 ** should return a false with an invalid response plus one or more strings
 ** describing the invalid parameters.
 ** The unit tests are broken down into spectral windows, basebands and 
 ** correlator configuration which correspond to the main data strucutures
 ** in the correlator configuration IDL.
 */
public class JConfigValidatorTest extends TestCase
{
    private CorrConfigValidator m_cv;
    private CorrelatorConfiguration m_corrConfig;

    // Spectral window parameters
    private double m_centerFreqMHz;
    private double m_effectiveBwMHz;
    private int m_effectiveNumberOfChannels;
    private int m_spectralAveragingFactor;
    private alma.NetSidebandMod.NetSideband m_netSideBand;
    private int m_associatedSpectralWindowNumberInPair;
    private boolean m_useThisSpectralWindow;
    private alma.WindowFunctionMod.WindowFunction m_windowFunction;
    private boolean m_frqChProfReproduction = false;
    private alma.CorrelationBitMod.CorrelationBit m_correlationBits;
    private boolean m_correlationNyquistOversampling;
    private alma.StokesParameterMod.StokesParameter[] m_polnProducts;
    private boolean m_quantizationCorrection;
    private alma.Correlator.SpectralWindow[] m_spectralWindows;

    // Channel average regions
    private int m_chanAverRegionStartChannel;
    private int m_chanAverRegionsNumberChannels;
    private int m_numChanAverRegions;
    private alma.Correlator.ChannelAverageRegion[] m_channelAverageRegions;

    // Baseband parameters
    private boolean m_use12GHzFilter;
    private boolean m_useUSB;
    private alma.AccumModeMod.AccumMode m_CAM;
    private alma.CorrelationModeMod.CorrelationMode m_dataProducts;
    private AtmPhaseCorrection[] m_atmPhaseCorrections;
    private alma.SidebandProcessingModeMod.SidebandProcessingMode m_sideBandProcessingMode;
    private double m_LO2FrequencyMHz;
    private alma.Correlator.BinSwitching_t m_binSwitching;
    private alma.Correlator.BaseBandConfig[] m_baseBands;

    // Correlator Configuration parameters
    private long m_dumpDuration;
    private long m_integrationDuration;
    private long m_channelAverageDuration;
    private alma.ReceiverSidebandMod.ReceiverSideband m_receiverSideband;
    private double m_LO1FrequencyMHz;
    private alma.Correlator.ACAPhaseSwitchingConfigurations m_acaPhaseSwitchingConfig;

    //------------------------------------------------------------------------------
    public JConfigValidatorTest()
    {
        super("CorrConfigValidator Test!");
    }

    //------------------------------------------------------------------------------
    protected void setUp()
    { 
        try
        {
            m_cv = new CorrConfigValidator();
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
            return;
        }

        //////////////////////////////////////////////////////////
        ///      Construct default CorrelatorConfiguration     ///
        //////////////////////////////////////////////////////////

        // ChannelAverageRegions
        m_chanAverRegionStartChannel = 0;
        m_chanAverRegionsNumberChannels = 8192*15/16;
        m_numChanAverRegions = 1;
        m_centerFreqMHz = 3000.0;
        m_effectiveBwMHz = 2.0e3 * (15.0/16.0);
        m_effectiveNumberOfChannels = 8192 * 15 / 16;
        m_spectralAveragingFactor = 1;
        m_netSideBand =  alma.NetSidebandMod.NetSideband.DSB;
        m_associatedSpectralWindowNumberInPair = 1;
        m_useThisSpectralWindow = true;
        m_windowFunction = alma.WindowFunctionMod.WindowFunction.HANNING;
        m_frqChProfReproduction = false;
        m_correlationBits = alma.CorrelationBitMod.CorrelationBit.BITS_2x2;
        m_correlationNyquistOversampling = false;
        m_polnProducts = new alma.StokesParameterMod.StokesParameter[1];
        m_polnProducts[0] = alma.StokesParameterMod.StokesParameter.XX;
        m_quantizationCorrection = true;

        // BaseBandConfig
        m_use12GHzFilter = true;
        m_useUSB = true;
        m_CAM = alma.AccumModeMod.AccumMode.NORMAL;
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.CROSS_AND_AUTO;
        m_atmPhaseCorrections = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];
        m_atmPhaseCorrections[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
        m_sideBandProcessingMode = alma.SidebandProcessingModeMod.SidebandProcessingMode.NONE;
        m_LO2FrequencyMHz = 8.98;
        long []emptySeq = new long[0];
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING,
                1, new long[1], emptySeq);

        // CorrelatorConfiguration
        m_dumpDuration = 10080000;
        m_integrationDuration = 10080000;
        m_channelAverageDuration = m_integrationDuration;
        m_LO1FrequencyMHz = 94.02;
        m_receiverSideband =  alma.ReceiverSidebandMod.ReceiverSideband.DSB;
        // Build a configuration with 1 baseband & 1 spectral window
        buildCorrConfigIDL(1,1);

    }
    //------------------------------------------------------------------------------
    public void testValidatorByCorrConfigIDL()
    {
        // validate a simple configuration
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
    }

    //------------------------------------------------------------------------------
    /** This checks spectral window parameters
     */
    public void testValidatorSpectralWindow()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // Check for max # of spectral windows
        m_centerFreqMHz = 3000.0;
        m_effectiveBwMHz = 2.0e3 * (15.0/16.0);
        m_chanAverRegionStartChannel = 0;
        m_effectiveNumberOfChannels = (256*15)/16;
        m_chanAverRegionsNumberChannels = m_effectiveNumberOfChannels;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        // Check for too few spectral windows
        m_effectiveNumberOfChannels = 8192 * 15 / 16;
        buildCorrConfigIDL(1,0);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for too many spectral windows
        buildCorrConfigIDL(1,2);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for too many spectral points across baseband
        m_effectiveNumberOfChannels = 8192 * 15 / 16;
        buildCorrConfigIDL(1,2);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check invalid center frequency
        m_centerFreqMHz = 1999.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check invalid center frequency
        m_centerFreqMHz = 4001.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check valid center frequency that extends beyond spectral window

        m_centerFreqMHz = 2000.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check valid center frequency that extends beyond spectral window
        m_centerFreqMHz = 3999.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        m_centerFreqMHz = 3000.0;
        // Test invalid bandwidth (too small)
        m_effectiveBwMHz = 0.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Test invalid bandwidth (too big)
        m_effectiveBwMHz = 2.0e3+1.0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_effectiveBwMHz = 2.0e3;

        // Test invalid # of channels
        m_effectiveNumberOfChannels = 0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        m_effectiveNumberOfChannels = 8193;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_effectiveNumberOfChannels = 8192 * 15 / 16;

        checkChannelAverageRegions();
    }

    //------------------------------------------------------------------------------
    /** This checks baseband parameters
     */
    public void testValidatorBasebands()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // Check for max # basebands
        buildCorrConfigIDL(4,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        // check for repeated base-bands
        buildCorrConfigIDL(4,1);
        m_corrConfig.baseBands[0].basebandName = m_corrConfig.baseBands[1].basebandName;
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for too few basebands
        buildCorrConfigIDL(0,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for too many basebands
        buildCorrConfigIDL(5,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for max # spectral windows across all basebands
        buildCorrConfigIDL(4,9);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Test for 2 antenna correlator that basebands are 0 & 1
        try
        {
            m_cv = new CorrConfigValidator(CORRELATOR_12m_2ANTS.value);
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
        }
        alma.BasebandNameMod.BasebandName []baseBands = new alma.BasebandNameMod.BasebandName[2];

        for( int bbIdx = 0; bbIdx < 2; bbIdx++ )
        {
            baseBands[bbIdx] = alma.BasebandNameMod.BasebandName.from_int(bbIdx+1);
        }

        buildCorrConfigIDL(baseBands,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
        try
        {
            m_cv = new CorrConfigValidator();
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
        }

        // Check that CAM & dataProducts match
        checkCAM();

        // Check bin switching
        checkBinSwitching();

    }

    //------------------------------------------------------------------------------
    public void testValidatorSidebandProcessingModes()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        try
        {
            m_cv = new CorrConfigValidator(CORRELATOR_12m_2ANTS.value);
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
        }
        m_sideBandProcessingMode = SidebandProcessingMode.PHASE_SWITCH_SEPARATION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_sideBandProcessingMode = SidebandProcessingMode.FREQUENCY_OFFSET_SEPARATION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_sideBandProcessingMode = SidebandProcessingMode.PHASE_SWITCH_REJECTION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_sideBandProcessingMode = SidebandProcessingMode.FREQUENCY_OFFSET_REJECTION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Make sure all sideband processing modes allowed for final 4 quad system
        try
        {
            m_cv = new CorrConfigValidator(CORRELATOR_12m_4QUADRANT.value);
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
        }

        m_sideBandProcessingMode = SidebandProcessingMode.PHASE_SWITCH_SEPARATION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
        m_sideBandProcessingMode = SidebandProcessingMode.FREQUENCY_OFFSET_SEPARATION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
        m_sideBandProcessingMode = SidebandProcessingMode.PHASE_SWITCH_REJECTION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
        m_sideBandProcessingMode = SidebandProcessingMode.FREQUENCY_OFFSET_REJECTION;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        try
        {
            m_cv = new CorrConfigValidator();
        }
        catch ( alma.CorrConfigModeErr.wrappers.AcsJConstructorFailureEx ex )
        {
            ex.printStackTrace();
        }
    }

    //------------------------------------------------------------------------------
    public void testValidatorCorrelatorConfiguration()
    {
        // Validate the CorrelatorConfiguraiton
        checkTimes();

        // Check APC
        checkAPC();

        checkDataRates();
    }

    //------------------------------------------------------------------------------
    private void checkCAM()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // CAM = FAST only for AUTO_ONLY
        m_CAM = alma.AccumModeMod.AccumMode.FAST;
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.AUTO_ONLY;
        m_dumpDuration = 10000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        m_CAM = alma.AccumModeMod.AccumMode.FAST;
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.CROSS_AND_AUTO;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.CROSS_ONLY;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        m_CAM = alma.AccumModeMod.AccumMode.NORMAL;
        m_dumpDuration = 10080000;
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.CROSS_ONLY;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Reset CAM & data products to default values
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.CROSS_AND_AUTO;
    }
    //------------------------------------------------------------------------------
    private void checkBinSwitching()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // NO_SWITCHING
        long []emptySeq = new long[0];
        alma.SwitchingModeMod.SwitchingMode switchingMode = alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING;
        int numPositions = 0;
        long []dwellTime = new long[0];
        long []deadTime = new long[0];
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        numPositions = 1;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        numPositions = 1;
        dwellTime = new long[1];
        deadTime = new long[2];
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Nutator switching
        switchingMode = alma.SwitchingModeMod.SwitchingMode.NUTATOR_SWITCHING;
        // Valid case
        numPositions = 2;
        dwellTime = new long[2];
        deadTime = new long[2];
        deadTime[0] = deadTime[1] = 100000;
        dwellTime[0] = dwellTime[1] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        // only 1 position
        numPositions = 1;
        dwellTime = new long[1];
        deadTime = new long[1];
        deadTime[0] = 100000;
        dwellTime[0] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // INVALID: 5 positions
        numPositions = 5;
        dwellTime = new long[5];
        deadTime = new long[5];
        deadTime[0] = 100000;
        dwellTime[0] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Inequal arrays of dead & dwell times
        numPositions = 2;
        dwellTime = new long[2];
        deadTime = new long[1];
        deadTime[0] = 100000;
        dwellTime[0] = dwellTime[1] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // times not 48ms multiple
        deadTime = new long[2];
        deadTime[1] = 100000;
        dwellTime[1] = 480000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // No dead time for nutator switching
        deadTime[0] = 0;
        dwellTime[0] = 480000;
        deadTime[1] = 100000;
        dwellTime[1] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // No dwell time for nutator switching
        deadTime[0] = 480000;
        dwellTime[0] = 0;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Frequency switching tests
        switchingMode = alma.SwitchingModeMod.SwitchingMode.FREQUENCY_SWITCHING;
        // Valid case
        numPositions = 2;
        dwellTime = new long[2];
        deadTime = new long[0];
        dwellTime[0] = dwellTime[1] = 480000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);

        // Only 1 position
        numPositions = 1;
        dwellTime = new long[1];
        deadTime = new long[1];
        deadTime[0] = 100000;
        dwellTime[0] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // INVALID: 5 positions
        numPositions = 5;
        dwellTime = new long[5];
        deadTime = new long[5];
        deadTime[0] = 100000;
        dwellTime[0] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // INVALID: dwell time length != # positions
        numPositions = 2;
        dwellTime = new long[3];
        dwellTime[0] = dwellTime[1] = dwellTime[2] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // No dead time in frequency switching
        numPositions = 2;
        dwellTime = new long[2];
        deadTime = new long[1];
        deadTime[0] = 100000;
        dwellTime[0] = dwellTime[1] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // switching time no 48ms multiple
        numPositions = 2;
        dwellTime = new long[2];
        deadTime = new long[0];
        dwellTime[0] = dwellTime[1] = 380000;
        m_binSwitching = new alma.Correlator.BinSwitching_t(
                switchingMode, numPositions, dwellTime, deadTime);
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
    }

    //------------------------------------------------------------------------------
    private void checkChannelAverageRegions()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        m_centerFreqMHz = 3000.0;
        m_effectiveBwMHz = 2.0e3 * (15.0/16.0);
        m_effectiveNumberOfChannels = 8192 * 15 / 16;

        // Start Channel negative
        m_chanAverRegionStartChannel = -1;

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Start Channel at bandwidth edge
        m_chanAverRegionStartChannel = m_effectiveNumberOfChannels*16/15;

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);

        assertEquals(isValid, false);

        m_chanAverRegionStartChannel = 0;

        // # of channels negative
        m_chanAverRegionsNumberChannels = -1;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // # of channels 0
        m_chanAverRegionsNumberChannels = 0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Channel aver region beyond band
        m_chanAverRegionsNumberChannels = m_effectiveNumberOfChannels*16/15+1;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check for valid number of channel averge regions
        m_chanAverRegionStartChannel = 0;
        m_chanAverRegionsNumberChannels = m_effectiveNumberOfChannels;

        // Too many
        m_numChanAverRegions = 11;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Must have at least 1 ch. aver region
        m_numChanAverRegions = 0;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Just right
        m_numChanAverRegions = 10;

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, true);
    }

    //------------------------------------------------------------------------------
    private void checkTimes()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // Dump duration not multiple of 16ms for CAM = NORMAL
        m_dumpDuration = 240000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Dump duration not multiple of 1ms for CAM = FAST
        m_CAM = alma.AccumModeMod.AccumMode.FAST;
        m_dataProducts = alma.CorrelationModeMod.CorrelationMode.AUTO_ONLY;
        m_dumpDuration = 20000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Check integration duration
        // Not Multiple of dump duration
        m_CAM = alma.AccumModeMod.AccumMode.NORMAL;
        // Set dump duration & channel average duration to values which allow
        // checks only on the integration duration
        m_dumpDuration = 480000;
        m_channelAverageDuration = 480000;
        m_integrationDuration = 10080000 * 2/5;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Integration duration < dump duration (also causes error w/ chan aver duration)
        m_integrationDuration = m_dumpDuration / 2;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_dumpDuration = 480000;
        m_integrationDuration = 10080000;

        // Check channel average durations
        // Multiple of dump duration
        m_channelAverageDuration = 490000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // less than dump duration
        m_channelAverageDuration = 320000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Greater than integration duration
        m_channelAverageDuration = m_integrationDuration * 2;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
        m_channelAverageDuration = m_integrationDuration;

        // Integration duration not a multiple of chan aver durations
        m_channelAverageDuration = 4800000;
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);
    }
    //------------------------------------------------------------------------------
    private void checkAPC()
    {
        boolean isValid;
        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();

        // Both uncorrected
        m_atmPhaseCorrections = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[2];
        m_atmPhaseCorrections[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
        m_atmPhaseCorrections[1] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // Both corrected
        m_atmPhaseCorrections[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;
        m_atmPhaseCorrections[1] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED;

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,2);
        assertEquals(isValid, false);

        // reset at end of test
        m_atmPhaseCorrections = new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[1];
        m_atmPhaseCorrections[0] = alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_UNCORRECTED;
    }
    //------------------------------------------------------------------------------
    private void checkDataRates()
    {
        boolean isValid;

        alma.ACS.stringSeqHolder returnMsg = new alma.ACS.stringSeqHolder();
        // 8K lags across 64 antennas requires about 10 second integrations
        m_integrationDuration = 100800000L;
        buildCorrConfigIDL(4,1);
        isValid = validateConfiguration(returnMsg,16);
        //assertEquals(isValid, true);

        // for a 1/2 second integration can only have 16 antennas
        m_integrationDuration = 5760000L; //10080000L;
        m_channelAverageDuration = m_integrationDuration;

        buildCorrConfigIDL(4,1);
        isValid = validateConfiguration(returnMsg,16);
        assertEquals(isValid, true);

        buildCorrConfigIDL(4,1);
        isValid = validateConfiguration(returnMsg,23);
        assertEquals(isValid, false);

        // Invalid # of antennas
        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,0);
        assertEquals(isValid, false);

        buildCorrConfigIDL(1,1);
        isValid = validateConfiguration(returnMsg,65);
        assertEquals(isValid, false);

    }

    //------------------------------------------------------------------------------
    private void buildCorrConfigIDL(int numBasebands, int numSpecWindows)
    {
        buildBaseband(numBasebands, numSpecWindows);

        alma.Correlator.ACAPhaseSwitchingConfigurations acaPhaseSwitchingConfig = 
            new alma.Correlator.ACAPhaseSwitchingConfigurations( false, false, 0);

        m_corrConfig = new alma.Correlator.CorrelatorConfiguration(
            m_dumpDuration, 
            m_integrationDuration,
            m_channelAverageDuration,
            0, /* subScanDurationACA */
            alma.ReceiverBandMod.ReceiverBand.ALMA_RB_01,
            alma.ReceiverSidebandMod.ReceiverSideband.DSB,
            m_baseBands,
            m_atmPhaseCorrections,
            m_LO1FrequencyMHz,
            acaPhaseSwitchingConfig);
    }

    //------------------------------------------------------------------------------
    private void buildCorrConfigIDL(alma.BasebandNameMod.BasebandName [] basebands, int numSpecWindows)
    {
        buildSpectralWindows(numSpecWindows);
        m_baseBands = new alma.Correlator.BaseBandConfig[basebands.length];
        for( int bbIdx = 0; bbIdx < basebands.length; bbIdx++ )
        {
            m_baseBands[bbIdx] = new alma.Correlator.BaseBandConfig(
                basebands[bbIdx],
                m_use12GHzFilter,
                m_useUSB,
                m_CAM,
                m_dataProducts,
                m_sideBandProcessingMode, 
                m_spectralWindows,
                m_binSwitching,
                m_LO2FrequencyMHz,
                alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD,
                0.0);
        }
        alma.Correlator.ACAPhaseSwitchingConfigurations acaPhaseSwitchingConfig = 
            new alma.Correlator.ACAPhaseSwitchingConfigurations( false, false, 0);

        m_corrConfig = new alma.Correlator.CorrelatorConfiguration(
            m_dumpDuration, 
            m_integrationDuration,
            m_channelAverageDuration,
            0, /* subScanDurationACA */
            alma.ReceiverBandMod.ReceiverBand.ALMA_RB_01,
            alma.ReceiverSidebandMod.ReceiverSideband.DSB,
            m_baseBands,
            m_atmPhaseCorrections,
            m_LO1FrequencyMHz,
            acaPhaseSwitchingConfig);
    }

    //------------------------------------------------------------------------------
    private void buildBaseband( int numBasebands, int numSpecWindows)
    {
        buildSpectralWindows(numSpecWindows);
        m_baseBands = new alma.Correlator.BaseBandConfig[numBasebands];
        alma.BasebandNameMod.BasebandName bbn;
        for( int bbIdx = 0; bbIdx < numBasebands; bbIdx++ )
        {
            bbn = alma.BasebandNameMod.BasebandName.from_int(bbIdx+1);
            m_baseBands[bbIdx] = new alma.Correlator.BaseBandConfig(
                bbn,
                m_use12GHzFilter,
                m_useUSB,
                m_CAM,
                m_dataProducts,
                m_sideBandProcessingMode, 
                m_spectralWindows,
                m_binSwitching,
                m_LO2FrequencyMHz,
                alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD,
                0.0);
        }
    }

    //------------------------------------------------------------------------------
    private void buildSpectralWindows(int numSpecWindows)
    {
        // Build Channel average region
        m_channelAverageRegions = new alma.Correlator.ChannelAverageRegion[m_numChanAverRegions];
        for( int carIdx = 0; carIdx < m_numChanAverRegions; carIdx++ )
        {
            m_channelAverageRegions[carIdx] = new alma.Correlator.ChannelAverageRegion(m_chanAverRegionStartChannel, m_chanAverRegionsNumberChannels);
        }
        // SpectralWindows;
        m_spectralWindows = new alma.Correlator.SpectralWindow[numSpecWindows];
        for( int spWin = 0; spWin < numSpecWindows; spWin++ )
        {
            m_spectralWindows[spWin] = new alma.Correlator.SpectralWindow(
                    m_centerFreqMHz, m_effectiveBwMHz, m_effectiveNumberOfChannels, m_spectralAveragingFactor,
                    m_netSideBand, m_associatedSpectralWindowNumberInPair, m_useThisSpectralWindow,
                    m_windowFunction, m_channelAverageRegions, m_frqChProfReproduction,
                    m_correlationBits, m_correlationNyquistOversampling, m_polnProducts,
                    m_quantizationCorrection);
        }
    }

    //------------------------------------------------------------------------------
    private boolean validateConfiguration(alma.ACS.stringSeqHolder returnMsg, int numAntennas)
    {
        boolean isValid = m_cv.validateConfiguration(m_corrConfig, numAntennas, returnMsg);

        if( !isValid )
        {
            System.out.print("Invalid configuration:\n");
            String [] errorArray = returnMsg.value;
            for (int i = 0; i < errorArray.length; i++)
            {
                System.out.println(errorArray[i]);
            }
            assertFalse(errorArray.length == 0);
        }
        return isValid;
    }

    //------------------------------------------------------------------------------
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(JConfigValidatorTest.class);
    }
}      
