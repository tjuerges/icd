/************************************************************************
* "@(#) $Id$"
*/

#include <iostream>

#include <maciSimpleClient.h>

#include "CorrConfigMode.h"

using namespace std;

int main(int argc, char *argv[])
{
    //
    // creates and initializes the SimpleClient object
    //
    maci::SimpleClient client;
    if (client.init(argc,argv) == 0)
    {
	return -1;
    }
    else
    {
        //
	// Must log into manager before we can really do anything
        //
	client.login();
    }

    //
    // get dal object
    //
    CDB::DAL_ptr dal_p;
    try
    {
        dal_p = client.getComponent<CDB::DAL>("CDB", 0, false);
    }
    catch ( ... )
    {
        cout << "got an exception while trying to get a DAL reference" << endl;
        
        return 1;
    }

    //
    // instantiate the table
    //
    try 
    {
        CorrConfigMode m(dal_p);

        //
        // print the table
        //
        cout << m.asString();

    }
    catch ( CorrConfigModeErr::ConstructorFailureExImpl &ex )
    {
        ex.log();
        cout << ex.getReason() << endl;
        cout << "test aborted after exception in constructor" << endl;

        return 1;
    }    

    //
    // logout from ACS manager
    //
    client.logout();

    return 0;
}
/*___oOo___*/
